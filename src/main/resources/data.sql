INSERT INTO tool (name) VALUES ('URL Auditor');
INSERT INTO tool (name) VALUES ('Keyword Combinator');
INSERT INTO tool (name) VALUES ('ROI Calculator');
INSERT INTO tool (name) VALUES ('Keyword Performance Analyzer');
INSERT INTO tool (name) VALUES ('Amazon Price Analyzer');



update tool set img_path='url-validator-icon-small.jpg' where id=1;
update tool set img_path='keyword-icon-small.jpg'  where id=2;
update tool set img_path='roi-icon-small.jpg' where id=3;
update tool set img_path ='New KPA Icon 3_small.png' where id=4;
update tool set img_path ='APT_icon_small.png' where id=5;
update tool set name = 'Amazon Price Analyzer' where id=5;


UPDATE tool SET img_path='URL_icon_small.jpg' WHERE id='1';
UPDATE tool SET img_path='KCT_icon_small.jpg' WHERE id='2';
UPDATE tool SET img_path='ROI_icon_small.jpg' WHERE id='3';
UPDATE tool SET img_path='KPA_icon_small.png' WHERE id='4';


UPDATE tool SET name='PPC Keyword Performance Analyzer' WHERE id='4';
UPDATE tool SET name='PPC Keyword Combinator' WHERE id='2';


--**************APR 23 2012*********************---
UPDATE `lxrmarketplace`.`tool` SET `is_free`='1' WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `is_free`='1' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `is_free`='1' WHERE `id`='3';
UPDATE `lxrmarketplace`.`tool` SET `is_free`='1' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `is_free`='1' WHERE `id`='5';
UPDATE `lxrmarketplace`.`tool` SET `tagline`='Compute Profitability' WHERE `id`='3';
UPDATE `lxrmarketplace`.`tool` SET `tagline`='Build Keyword Lists' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `tagline`='Categorize Keyword Performances' WHERE `id`='4';

UPDATE `lxrmarketplace`.`tool` SET `tagline`='Eliminate Problem URLs' WHERE `id`='1';

UPDATE `lxrmarketplace`.`tool` SET `tagline`='Compare and Evaluate Product Prices' WHERE `id`='5';

UPDATE `lxrmarketplace`.`tool` SET `description`='Analyze the keywords of your current marketing campaigns to easily separate the top performers from the non-performers.' WHERE `id`='4';

UPDATE `lxrmarketplace`.`tool` SET `description`='Quickly compare your product prices to those of your competitors to effectively compete in the common marketplace.' WHERE `id`='5';
UPDATE `lxrmarketplace`.`tool` SET `description`='Generate thousands of keywords in Adwords editor and AdCenter desktop compatible formats.' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `description`='Analyze your marketing campaigns URLs to quickly find and fix those that are broken.' WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `description`='Calculate how much money your online marketing campaign is making for you, in seconds!' WHERE `id`='3';




UPDATE tool SET img_path='URL' WHERE id='1';
UPDATE tool SET img_path='KCT' WHERE id='2';
UPDATE tool SET img_path='ROI' WHERE id='3';
UPDATE tool SET img_path='KPA' WHERE id='4';
UPDATE tool SET img_path='APT' WHERE id='5';

UPDATE `lxrmarketplace`.`tool` SET `cat_id`='1' WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `cat_id`='1' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `cat_id`='1' WHERE `id`='3';
UPDATE `lxrmarketplace`.`tool` SET `cat_id`='1' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `cat_id`='2' WHERE `id`='5';


UPDATE `lxrmarketplace`.`tool` SET `desc_link`='urlDescription.html' WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='keyCombDescription.html' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='roiDescription.html' WHERE `id`='3';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='keywordAnalyzer.html' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='amazonPriceAnalyzer.html' WHERE `id`='5';


UPDATE `lxrmarketplace`.`tool` SET `tool_link`='urlvalidator.html' WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `tool_link`='keywordcombination.html' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `tool_link`='roicalculator.html' WHERE `id`='3';
UPDATE `lxrmarketplace`.`tool` SET `tool_link`='keywordanalyzertool.html' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `tool_link`='amazonpriceanalyzertool.html' WHERE `id`='5';

INSERT INTO amazonPA_reco (reco) VALUES ("Your product is not available for this product category.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your product is not available for this product category.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your product is not available and no competitors found for this product.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your product price is not available.");
INSERT INTO amazonPA_reco (reco) VALUES ("Both your product price and competitors price are not available.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your product price is not available and no competitors found.");
INSERT INTO amazonPA_reco (reco) VALUES ("You do not have any competitors for this product.");
INSERT INTO amazonPA_reco (reco) VALUES ("Competitors product price is not available.");
INSERT INTO amazonPA_reco (reco) VALUES ("Reduce your product price in order to compete with your competitor.");
INSERT INTO amazonPA_reco (reco) VALUES ("Reduce your product price.");
INSERT INTO amazonPA_reco (reco) VALUES ("You are selling at a slightly higher price than your competitor.");
INSERT INTO amazonPA_reco (reco) VALUES ("Increase your product price.");
INSERT INTO amazonPA_reco (reco) VALUES ("There is scope to increase your product price.");
INSERT INTO amazonPA_reco (reco) VALUES ("You product price is best in the market.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your competitor's price is the same as yours.");
INSERT INTO amazonPA_reco (reco) VALUES ("Your product is not found, Please provide your product title as in Amazon.");
INSERT INTO amazonPA_reco (reco) VALUES ("We are unable to find data for this product, Please try again.");

UPDATE tool SET `description`='Analyze your marketing campaigns\' URLs to quickly find and fix those that are broken.' WHERE `id`='1';




INSERT INTO feedback  (user_id,tool_id,rating,comments) VALUES(2,1,5.0,' very good');
INSERT INTO feedback  (user_id,tool_id,rating,comments) VALUES(2,2,5.0,' very good');
INSERT INTO feedback  (user_id,tool_id,rating,comments) VALUES(2,3,5.0,' very good');
INSERT INTO feedback  (user_id,tool_id,rating,comments) VALUES(2,4,5.0,' very good');
INSERT INTO feedback  (user_id,tool_id,rating,comments) VALUES(2,5,5.0,' very good');




--****************to filter local users on june 21*******************--
insert into localuser_filter(filter,filter_type) values('183.82.97.223',1);
insert into localuser_filter(filter,filter_type) values('122.169.242.198',1);
insert into localuser_filter(filter,filter_type) values('122.169.242.199',1);
insert into localuser_filter(filter,filter_type) values('netelixir',2);


--****************** for sitemapbuider july 2nd******************--
insert into tool (id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link)  values(6,'Sitemap Builder','sitemapbuilder','SMB',1,'sitemapbuilder',3,'siteMapBuilder.html','sitemapbuildertool.html');
insert into feedback(user_id,tool_id,rating,title,comments,feedback_date)  values(2,6,3.5,'It is good','It is very usefull to build sitemap.xml','2012-05-28 17:30:05');
UPDATE `lxrmarketplace`.`tool` SET `tagline`='Generate Website Sitemaps' WHERE `id`='6';
UPDATE `lxrmarketplace`.`tool` SET `description`='Automatically generate sitemaps for your website whenever you make changes or update the content on your site.' WHERE `id`='6';


--****************** for Competitor Analysis Tool August 22nd******************--

insert into tool (id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) 
values(7,'Competitor Analysis Tool','Quickly analyze how search-friendly your webpage is compared to your competitors\' webpage.','CPA',1,'Compare and Analyze SEO Metrics',3,'competitorAnalysis.html','competitoranalysistool.html');

UPDATE  tool   SET  name = 'SEO Competitor Analysis Tool'  WHERE  id = 7;

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date)  values(2,7,3.5,'It is good','It is very usefull tool','2012-05-28 17:30:05');

--****************** for Spider View Tool Oct 9th******************--

insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) values ('Spider View ',' This tool will help you achieve higher search engine rankings.', 'SVT' , 1 , 'Analyze SEO Metrics',3, 'spiderView.html', ' seowebpageanalysistool.html');

insert into feedback(user_id,tool_id,rating,title) values (2,8,3.5,'Tool is very usefull');

UPDATE `lxrmarketplace`.`tool` SET `name`='SEO Webpage Analysis Tool' WHERE `id`='8';

UPDATE `lxrmarketplace`.`tool` SET `description`='Analyze various on-page SEO factors on your website to judge how well it is SEO optimized and whether the SEO elements are in place or not.', `tagline`='Simulate Search Engine Crawlers' WHERE `id`='8';


--****************** for Stop Words on  Oct 9th******************--

LOAD  DATA  LOCAL  INFILE  '/home/netelixir/marketplace/Marketplace/stopwords.txt' INTO TABLE  stop_words ( stop_word);

UPDATE `lxrmarketplace`.`tool` SET `desc_link`='seoWebpageAnalysis.html', `tool_link`='seowebpageanalysistool.html' WHERE `id`='8';

----************* for SEO purpose **************------------------------

UPDATE `lxrmarketplace`.`tool` SET `name`='PPC Performance Tool : Keyword Analyzer' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `name`='PPC Tool : Keyword Combinator' WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `name`='Amazon Marketplace Tool : Price Analyzer' WHERE `id`='5';

-------**********nov 26 for removing amazon price analyzer**********-------------
UPDATE `lxrmarketplace`.`tool` SET `is_deleted`='1' WHERE `id`='5';


--------------------******* dec 10 for url changes *******---------------------

UPDATE `lxrmarketplace`.`tool` SET `desc_link`='ppc-keyword-performance-analyzer.html', `tool_link`='ppc-keyword-performance-analyzer-tool.html' WHERE `id`='4';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='ppc-keyword-combination.html', `tool_link`='ppc-keyword-combination-tool.html'  WHERE `id`='2';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='url-auditor.html', `tool_link`='url-auditor-tool.html'  WHERE `id`='1';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='seo-sitemap-builder.html', `tool_link`='seo-sitemap-builder-tool.html'  WHERE `id`='6';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='seo-competitor-analysis.html', `tool_link`='seo-competitor-analysis-tool.html'  WHERE `id`='7';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='return-on-investment-calculator.html', `tool_link`='return-on-investment-calculator-tool.html'  WHERE `id`='3';
update tool set desc_link = 'seo-webpage-analysis.html' , tool_link = 'seo-webpage-analysis-tool.html' where id = 8;
UPDATE `lxrmarketplace`.`tool` SET `name`='SEO Sitemap Builder' WHERE `id`='6';

------------******jan 28 for competitor webpage monitor*******-------------------


insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) values ('Competitor Webpage Monitor ',' Track and monitor your competitors webpages easily and automatically by setting up email alerts with this tool.', 'CWM' , 1 , 'Track Competitor Webpages',3, 'competitorWebpageMonitor.html', ' competitorwebpagemonitortool.html');

insert into feedback(user_id,tool_id,rating,title) values (2,9,3.5,'Tool is very usefull');


-----------***May 13 2013 for Keyword Rank checker ******-------------------

insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) values
 ('SEO Rank Checker',' gives your keyword ranks.', 'SRC' , 1 , 'Gives keyword rank',3, 'keywordRankChecker.html', ' keywordrankcheckertool.html');

insert into feedback(user_id,tool_id,rating,title) values (2,10,3.5,'Tool is very usefull');

UPDATE `lxrmarketplace`.`tool` SET `desc_link`='seoRankChecker.html', `tool_link`='seorankcheckertool.html' WHERE `id`='10';

UPDATE `lxrmarketplace`.`tool` SET name='Daily SEO Rank Checker', `description`='Monitor and analyze daily website rankings in Google and Bing and compare with competitors\'.', `tagline`='Track, Analyze and Compare website rankings' WHERE `id`='10';

------------***Sep 23 2013 for LXR SEO ******......................

insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) values ('Site Grader ',' ', 'SG' , 1 , '',3, '',  '', 0);
insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) values ('LXR SEO ',' ', 'SG' , 1 , '',3, '',  '', 0);
insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) values ('Meta Tag Generator','Meta-tags are used frequently by most search engines. They identify your website content using your meta-tags. This simple tool will allow you to generate them easily!', 'MTG' , 0 , 'Generate Meta Keywords in HTML format',6, 'LXRSEO/Tools/seo-meta-tag-generator-tool.html', 'LXRSEO/Tools/seo-meta-tag-generator-tool.html', 0);
insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) values ('Inbound Link Checker','See all the inbound links (backlinks) of your site or your competitor’s site. Check the number and quality of the links pointing back to your website.', 'ILC' , 0 , 'Find out top linking websites to your domain',6, 'LXRSEO/Tools/seo-inbound-link-checker-tool.html', 'LXRSEO/Tools/seo-inbound-link-checker-tool.html', 0);

--------------------------------* Feedback Questions *-----------------------------------
INSERT INTO `lxrmarketplace`.`seo_fb_questions`
(`fb_q_id`,`fb_question`,`fb_q_type`,`is_active`)VALUES('1', 'Does all the content of this screen useful and easy to understand? Please quote your experience.', '1', '1');
INSERT INTO `lxrmarketplace`.`seo_fb_questions`
(`fb_q_id`,`fb_question`,`fb_q_type`,`is_active`)VALUES('2', 'How did the content of this screen add value to your business?', '1', '1');
INSERT INTO `lxrmarketplace`.`seo_fb_questions`
(`fb_q_id`,`fb_question`,`fb_q_type`,`is_active`)VALUES('3', 'As a Small Business User, suggest your recommendations to improve this screen.', '1', '1');
INSERT INTO `lxrmarketplace`.`seo_fb_questions`
(`fb_q_id`,`fb_question`,`fb_q_type`,`is_active`)VALUES('4', 'Rate your overall experience on the screen.', '3', '1');

UPDATE seo_tasks SET tool_id = 17 WHERE task_id = 1;
UPDATE seo_tasks set tool_id = 13 WHERE task_id = 2;
UPDATE seo_tasks set tool_id = 0 WHERE task_id = 8;
UPDATE seo_tasks set tool_id = 0 WHERE task_id = 14;
UPDATE seo_tasks set tool_id = 0 WHERE task_id = 15;
UPDATE seo_tasks set tool_id = 0 WHERE task_id = 23;
UPDATE seo_tasks set tool_id = 14 WHERE task_id = 37;

--------------------------------* Tool insert query *-----------------------------------
insert into tool(id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home)
values (15, 'SEO Dashboard', '', '', 0, '', 0, '', '', 0);

insert into tool(id, name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) 
values (16, 'Competitor Research Tool','Get in depth analysis about your competitor’s ranking & backlinks, along with detailed website information.', 
'' , 0 , 'tagline',3, 'LXRSEO/Tools/competitorResearchtool.html', 
'LXRSEO/Tools/competitorResearchtool.html', 0);

insert into tool(id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) 
values (17,'Website Auditor Tool','Check URL status and Meta Tag Descriptions for your website.', 
'WAT' , 0 , 'Find out all META Tags and URL Status of your website.',3, 'LXRSEO/Tools/websiteAuditorTool.html', 
'LXRSEO/Tools/websiteAuditorTool.html', 0);

-------------------------------*Dashly For Magento*----------------------------------------------
insert into tool(id, name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) values (18,'Dashly For Magento','Dashly is a Mobile Dashboard and Admin Android application for Magento ecommerce stores. ', 'DFM' , 0 , 'Android application for Magento ecommerce stores.',6,'dashlyForMagento.html', 'dashlyForMagento.html', 1);
INSERT INTO feedback (`user_id`, `tool_id`, `rating`, `title`, `feedback_date`) VALUES ('1', '18', '5.0', '', '2013-10-30 17:30:05');

UPDATE `seo_fb_questions` SET `fb_question`='How can we further improve this screen?' WHERE `fb_q_id`='3';
UPDATE `seo_fb_questions` SET `fb_question`='Was this screen useful and easy to understand?' WHERE `fb_q_id`='1';
UPDATE `seo_fb_questions` SET `fb_question`='What did you not like on this screen?' WHERE `fb_q_id`='2';

insert into tool(id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link, show_in_home) 
values (19,'LXRSEO Sitemap Builder','Automatically generate sitemaps for your website whenever you make changes or update the content on your site.', 
'SBT' , 0 , 'Generate detailed sitemaps for any website.',3, 'LXRSEO/Tools/seo-sitemap-builder-tool.html', 
'LXRSEO/Tools/seo-sitemap-builder-tool.html', 0);


INSERT INTO `promotion_partner` (`partner_name`) VALUES ('lxrmarketplace');
INSERT INTO `promotion_partner` (`partner_name`) VALUES ('nurturetalent');
INSERT INTO `promotional_offers` (`partner_id`, `tool_id`, `promotion_name`, `start_date`, `end_date`, `is_active`, `price`, `no_of_times`, `no_of_months`, `display_string`) VALUES ('2', '12', 'nurturetalent', '2014-02-18 00:00:01', '2014-06-18 23:59:59', '1', '39', '3', '1', 'Nurturetalent');


----------***On Feb 20 In bound Link checker ***---------------------

insert into tool(name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) values
 ('Inbound Link Checker',' See all the inbound links (backlinks) of your site or your competitor’s site. Check the number and quality of the links pointing back to your website.',
 'ILC' , 1 ,'Track and analyse the backlinks to a website.', 3,
 'seo-inbound-link-checker.html', 'seo-inbound-link-checker-tool.html');

insert into feedback(user_id,tool_id,rating,title) values (2,20,3.5,'Tool is very usefull');

insert into tools_pricing(tool_id,display_string,report_type,amount) values (8,"Download Report for $1.99","SWA PDF report",1.99);

insert into tools_pricing(tool_id,display_string,report_type,amount) values (20,"Download 100 Back Links Report for $2.99","100 Back Links XLS report",2.99);

insert into tools_pricing(tool_id,display_string,report_type,amount) values (20,"Download 500 Back Links Report for $4.99","500 Back Links XLS report",4.99);



UPDATE `lxrmarketplace`.`tool` SET `desc_link`='competitor-webpage-monitor.html', `tool_link`='competitor-webpage-monitor-tool.html' WHERE `id`='9';
UPDATE `lxrmarketplace`.`tool` SET `desc_link`='seo-rank-checker.html', `tool_link`='seo-rank-checker-tool.html' WHERE `id`='10';

update tools_pricing set report_type='500backlinks' where id =3;
update tools_pricing set report_type='100backlinks' where id =2;
update tools_pricing set report_type='SWAreport' where id =1;


/******************************	LXRSEO PROMOTIONAL OFFERS	*******************************/
INSERT INTO `promotional_offers` (`promotion_id`, `partner_id`, `tool_id`, `promotion_name`, `start_date`, `end_date`, `is_active`, `price`, `no_of_times`, `no_of_months`, `display_string`) VALUES ('2', '1', '12', 'netelixir-monthly', '2014-03-01 00:00:01', '2019-12-31 23:59:59', '1', '49.00', '-1', '1', '$49 per Month');
INSERT INTO `promotional_offers` (`promotion_id`, `partner_id`, `tool_id`, `promotion_name`, `start_date`, `end_date`, `is_active`, `price`, `no_of_times`, `no_of_months`, `display_string`) VALUES ('3', '1', '12', 'netelixir-6months', '2014-03-01 00:00:01', '2019-12-31 23:59:59', '1', '270.00', '1', '6', '$270 for 6 Months');
INSERT INTO `promotional_offers` (`promotion_id`, `partner_id`, `tool_id`, `promotion_name`, `start_date`, `end_date`, `is_active`, `price`, `no_of_times`, `no_of_months`, `display_string`) VALUES ('4', '1', '12', 'netelixir-12months', '2014-03-01 00:00:01', '2019-12-31 23:59:59', '1', '475.00', '1', '12', '$475  for 12 Months');
UPDATE `promotional_offers` SET `display_string`='$39 per Month (Special Offer for Nurturetalent Customers for first 3 months, then only $49 per month)' WHERE `promotion_id`='1';

UPDATE promotional_offers SET no_of_months = 1 WHERE promotion_id = 1;
UPDATE promotional_offers SET no_of_months = 1 WHERE promotion_id = 2;
UPDATE promotional_offers SET no_of_months = 6 WHERE promotion_id = 3;
UPDATE promotional_offers SET no_of_months = 12 WHERE promotion_id = 4;



/******************************	REPORT DOWNLOAD STATS	*******************************/
INSERT INTO `lxr_reports` (`report_id`, `tool_id`, `report_name`) VALUES ('', '11', 'Site Grader Report');
INSERT INTO `lxr_reports` (`tool_id`, `report_name`) VALUES ('15', 'Dashboard Report');

/******************************	For First Visit Tracking	*******************************/
insert into urls (url_id, url, added_date) VALUES (1, "/LXRSEO/seo-site-check-up-dashboard.html", "2014-05-16 00:00:00");
insert into urls (url_id, url, added_date) VALUES (2, "/LXRSEO/seo-tasks-management.html", "2014-06-16 00:00:00");

/*************************** Domain Age Checker Tool ********************************/
insert into tool (id,name,description,img_path,is_free,tagline,cat_id,desc_link,tool_link) 
values(21,'Domain Age Checker','Generates a complete report to help you identify the Website Domain Age, Registration Information, Administrative and Technical contact details.',
'DAC',1,'Checks  Whois Website Domain Registration Information.',3,'domain-age-checker.html','domain-age-checker-tool.html');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date)
 values(2,21,3.5,'It is good','It is very usefull tool','2014-07-28 17:30:05');

/**************************Loggically mapped tools ***********************************/

INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('1', '2,4');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('2', '1,4');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('3', '2,4');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('4', '1,2');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('6', '20,21');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('7', '8,9');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('8', '7,10');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('9', '7,10');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('10', '8,9');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('20', '6,21');
INSERT INTO `lxrm_logically_mapped_tools` (`tool_id`, `mapped_id`)
 VALUES ('21', '6,20');

/***********-------Stroing mobile users data-------********************/

UPDATE `mobile_user` SET `mobile_tool_name`='SG' WHERE `id`>= 1;


/***************-------E-COmmerce mobile apps----------*******************8/

insert into tool(name,description,img_path,is_free,tagline,
cat_id,desc_link,tool_link, show_in_home) values
 ('Dashly For Bigcommerce','Bigcommerce Mobile Dashboard is a Mobile Dashboard and Admin Android application for Bigcommerce ecommerce stores.',
 'DFB' , 0 , 'Android Application for Bigcommerce ecommerce stores.',6,'dashlyForBigcommerce.html','dashlyForBigcommerce.html', 1);

insert into tool(name,description,img_path,is_free,tagline,
cat_id,desc_link,tool_link, show_in_home) values
 ('Dashly For PrestaShop','PrestaShop Mobile Dashboard is a Mobile Dashboard and Admin Android application for PrestaShop ecommerce stores.',
 'DFP' , 0 , 'Android Application for PrestaShop ecommerce stores.',6,'dashlyForPrestaShop.html','dashlyForPrestaShop.html', 1);


insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,22,4.0,'','','2014-09-10 10:30:05');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,23,4.0,'','','2014-09-10 10:30:05');

/***********----Sep18 Robot.txt Generator Tool -------**************/

insert into tool(name,description,img_path,is_free,tagline,
cat_id,desc_link,tool_link, show_in_home) values
 ('Robots.txt Generator','Save Time with our stress-free robots.txt generator.',
 'RGT' , 0 , 'By using the quick and efficient robots.txt generator, you can easily generate an edited or new robots.txt file for your website.',
3,'robots-txt-generator-tool.html','robots-txt-generator-tool.html', 1);


insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,24,4.0,'','','2014-09-18 10:30:05');


/***********----Oct08 On Page Recommendations for "www.lxrmarketplace.com  -------**************/

UPDATE `tool` SET `desc_link`='daily-keyword-rank-checker.html',name='Daily Keyword Rank Checker',`tool_link`='daily-keyword-rank-checker-tool.html' WHERE `id`='10';


/***********----Oct29 changed overview page URLs-------**************/
UPDATE `tool` SET `desc_link`='domain-age-checker-tool.html?query=\'desc\'' WHERE `id`='21';
UPDATE `tool` SET `desc_link`='seo-inbound-link-checker-tool.html?query=\'desc\'' WHERE `id`='20';
UPDATE `tool` SET `desc_link`='daily-keyword-rank-checker-tool.html?query=\'desc\'' WHERE `id`='10';
UPDATE `tool` SET `desc_link`='competitor-webpage-monitor-tool.html?query=\'desc\'' WHERE `id`='9';
UPDATE `tool` SET `desc_link`='seo-webpage-analysis-tool.html?query=\'desc\'' WHERE `id`='8';
UPDATE `tool` SET `desc_link`='seo-competitor-analysis.html?query=\'desc\'' WHERE `id`='7';
UPDATE `tool` SET `desc_link`='seo-sitemap-builder-tool.html?query=\'desc\'' WHERE `id`='6';
UPDATE `tool` SET `desc_link`='ppc-keyword-performance-analyzer-tool.html?query=\'desc\'' WHERE `id`='4';
UPDATE `tool` SET `desc_link`='return-on-investment-calculator-tool.html?query=\'desc\'' WHERE `id`='3';
UPDATE `tool` SET `desc_link`='ppc-keyword-combination-tool.html?query=\'desc\'' WHERE `id`='2';
UPDATE `tool` SET `desc_link`='url-auditor-tool.html?query=\'desc\'' WHERE `id`='1';
UPDATE `tool` SET `desc_link`='robots-txt-generator-tool.html?query=\'desc\'' WHERE `id`='24';

/*********** Nov 4 2014 Robots txt validator Tool *****************/

INSERT INTO `tool` (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`,
 `is_deleted`, `show_in_home`) VALUES ('25', 'Robots.txt Validator', 'A quick and easy way to analyze the syntax errors of a robots.txt file for your website! ',
 'RVT', '0', 'Validates robots.txt file syntax.', '3',
 'robots-txt-validator-tool.html?query=\'desc\'', 'robots-txt-validator-tool.html', '0', '1');

 insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,25,4.7,'','','2014-11-04 10:30:05');

UPDATE `lxrm_logically_mapped_tools` SET `mapped_id`='6,25' WHERE `id`='13';
INSERT INTO `lxrm_logically_mapped_tools` (`id`, `tool_id`, `mapped_id`) VALUES ('14', '25', '24,21');

/*********** NOV 20 2014 Meta Tag Generator Tool *****************/
INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`) 
VALUES ('27', 'Meta Tag Generator', 'Meta-tags are used frequently by most search engines. They identify your website content using your meta-tags. This simple tool will allow you to generate them easily!', 'MTG', '1', 'Generate Meta Keywords in HTML format', '3', 'meta-tag-generator-tool.html?query=\'desc\'', 'meta-tag-generator-tool.html', '0', '1');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,27,4.7,'','','2014-12-20 10:30:05');

/*********** DEC 23 2014 DNS Lookup Tool *****************/

INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`)
 VALUES ('28', 'DNS Lookup', 'Check and analyse DNS records returned from a DNS server to a website.', 'DLT', '1', 'Track and analyse DNS records to a domain', '3', 'dns-lookup-tool.html?query=\'desc\'', 'dns-lookup-tool.html', '0', '1');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,28,4.7,'','','2015-02-04 10:30:05');

INSERT INTO `lxrm_logically_mapped_tools` (`id`, `tool_id`, `mapped_id`) VALUES ('15', '28', '21,25');
UPDATE `lxrm_logically_mapped_tools` SET `mapped_id`='24,20' WHERE `id`='14';
UPDATE `lxrm_logically_mapped_tools` SET `mapped_id`='6,28' WHERE `id`='11';
UPDATE `lxrm_logically_mapped_tools` SET `mapped_id`='20,28' WHERE `id`='5';

/*********** Mar 20 2015 Social Media Tool *****************/
 INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`) 
VALUES ('29', 'Social Media', 'Check and analyse Social Media records returned from a Social Media to a website.', 'DLT', '1', 
'Track and analyse Social Media records to a domain.', '3', 'social-media-tool.html?query=\'desc\'', 'social-media-tool.html', '0', '1');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,29,4.7,'','','2015-03-02 17:30:05');

/*********** APR 9th 2015 Top Websites  *****************/

 INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`) 
VALUES ('30', 'Top Ranked Websites by Keyword', 'Gives top ranked website for a keyword', 'TRWK', '1', 
'Gives top ranked website', '3', 'top-ranked-websites-bykeword-tool.html?query=\'desc\'',
 'top-ranked-websites-bykeword-tool.html', '0', '1');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,30,4.7,'','','2015-04-09 19:30:05');

/********* APR 16th 2015 Broken Link Checker **************/

 INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, 
`cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`) 
VALUES ('31', 'Broken Link Checker', 'Check and analyse broken links present in a website', 'BRLC', '1', 
'Track and analyse broken links for a domain.', '3','broken-link-checker-tool.html?query=\'desc\'','broken-link-checker-tool.html', '0', '1');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,31,4.7,'','','2015-04-16 19:30:05');


/********JAN 21st, 2016 Updated Homepage tagline and description of all tools in LXRMarketplace **********************/

UPDATE seomarketplace.tool SET description = "Analyze your marketing campaign's URLs to quickly find and fix any broken links." , tagline = "Fix Broken URLs" where id = 1;

UPDATE seomarketplace.tool SET description = "Generate thousands of keywords in formats that are compatible with AdWords Editor and AdCenter Desktop." , tagline = "Build Keyword Lists" where id = 2;

UPDATE seomarketplace.tool SET description = "Calculate how much money your online marketing campaign is making, in seconds." , tagline = "Measure Profits" where id = 3;

UPDATE seomarketplace.tool SET description = "Analyze the keywords of your current marketing campaigns to easily separate the top performers from the non-performers." , tagline = "Report Keyword Performances" where id = 4;

UPDATE seomarketplace.tool SET description = "Automatically generate sitemaps for your website whenever you make changes or update the content on your site." , tagline = "Generate Website Sitemaps" where id = 6;

UPDATE seomarketplace.tool SET description = "Quickly analyze your site's search ranking against competitors." , tagline = "Compare Search Engine Rankings" where id = 7;

UPDATE seomarketplace.tool SET description = "Analyze your site's on-page SEO factors to see whether or not your page is SEO optimized." , tagline = "Simulate Search Engine Crawlers" where id = 8;

UPDATE seomarketplace.tool SET description = "Track and monitor competitor websites automatically by setting up email alerts." , tagline = "Track Competitor Websites" where id = 9;

UPDATE seomarketplace.tool SET description = "Monitor and analyze daily website rankings in Google and Bing and compare with competitors'." , tagline = "Track, Monitor, and Analyze Website Rankings" where id = 10;

UPDATE seomarketplace.tool SET description = "Dashly for Magento is a mobile dashboard and admin Android application for Magento Ecommerce stores." , tagline = "Android Application For Magento" where id = 18;

UPDATE seomarketplace.tool SET description = "See all inbound links coming to your site or your competitor's site, and monitor and analyze the number and quality of such links." , tagline = "Track, Monitor, and Analyze Website Rankings" where id = 20;

UPDATE seomarketplace.tool SET description = "Generate a complete report to help you identify the website's domain age, registration information, administrative and technical contact details." , tagline = "Check WhoIs Website Domain Registration Info" where id = 21;

UPDATE seomarketplace.tool SET description = "Dashly for BigCommerce is a mobile dashboard and admin Android application for BigCommerce Ecommerce stores." , tagline = "Android Application For BigCommerce" where id = 22;

UPDATE seomarketplace.tool SET description = "Dashly for PrestaShop is a mobile dashboard and admin Android application for PrestaShop Ecommerce stores." , tagline = "Android Application For PrestaShop" where id = 23;

UPDATE seomarketplace.tool SET description = "Generate an edited or new robot.txt file for your website with the quick and efficient Robot.txt Generator." , tagline = "Save Time With A Stress Free robots.txt Generator" where id = 24;

UPDATE seomarketplace.tool SET description = "A quick and easy way to analyze the syntax errors of a robots.txt file for your website!" , tagline = "Validate robots.txt File Syntax" where id = 25;

UPDATE seomarketplace.tool SET description = "A quick and easy way to generate an edited/new meta tags for your webpage!" , tagline = "Generate Meta Tags In HTML Format" where id = 27;

UPDATE seomarketplace.tool SET description = "Quick and easy way to collect your DNS (Domain Name System) records." , tagline = "Collects Your DNS Records" where id = 28;

UPDATE seomarketplace.tool SET description = "Get an easy-to-understand report of your social media reach and engagement by each of your social media channels!" , tagline = "Quickly Analyze Your Social Media Reach" where id = 29;

UPDATE seomarketplace.tool SET description = "Find out who your top competitors are for any keyword!" , tagline = "Provides Top Ranked Websites By Keyword" where id = 30;

UPDATE seomarketplace.tool SET description = "Quickly identify and analyze broken hyperlinks for any website" , tagline = "Identify And Track Broken Links For Any Website." where id = 31;

INSERT INTO `lxrmarketplace`.`tool` (`name`, `description`, `img_path`, `is_free`, `tagline`, `cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`) VALUES ('LXRPlugin', 'Adwords Analysis and Reporting for excel', 'LEP', '0', 'Google AdWords Excel Plugin', '1', '/lxr-plugin-tool.html', '/lxr-plugin-tool.html', '0', '1');

/********FEB 1st, 2016 LXRPlugin home page URL change**********************/
UPDATE `seomarketplace`.`tool` SET `desc_link`='/lxr-adwords-plugin.html', `tool_link`='/lxr-adwords-plugin.html' WHERE `id`='32';

/********FEB 18th, 2016 change of Daily Keyword Rank Checker to Weekly keyword Rank Checker and 
Keyword combinator tool name **********************/


UPDATE `seomarketplace`.`tool` SET name='Weekly Keyword Rank Checker',
description='Monitor and analyze weekly website rankings in Google and Bing and compare with competitors\'.', 
desc_link='weekly-keyword-rank-checker-tool.html?query=\'desc\'', tool_link='weekly-keyword-rank-checker-tool.html' WHERE `id`='10';

UPDATE `seomarketplace`.`tool` SET name = 'Keyword Combinator' where `id`='2';


/* May 26, 2016 insert WooCommerce tool into LXRMarketplace */
insert into tool (id, name, description, img_path, is_free, tagline, cat_id, desc_link, tool_link, is_deleted,show_in_home) 
values (33, 'Dashly For Woocommerce', 'Dashly for Woocommerce is a mobile dashboard and admin Android application for PrestaShop Ecommerce stores.', 'DFW', 0, 
'Android Application For Woocommerce', 6, 'dashlyForWoocommerce.html', 'dashlyForWoocommerce.html',0 , 1);

/* May 26, 2016 insert WooCommerce default data into feedback table */
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1,33,5.0,'Test','Useful Tool',NOW());

/* June 07, 2016  Logically Mapped Tools Grouping */
insert into tools_grouping (tool_id, correlated_tool_id, tools_order) values 
(31,21,1),(31,30,2),(31,20,3),  (31,29,4),(31,8,5),(31,10,6),  (31,1,7),(31,28,8),(31,25,9),(31,7,10),
(7,8,1),(7,30,2),(7,29,3),  (7,21,4),(7,10,5),(7,20,6),  (7,1,7),(7,2,8),(7,31,9),(7,6,10),
(9,30,1),(9,7,2),(9,20,3),  (9,1,4),(9,8,5),(9,10,6),  (9,2,7),(9,29,8),(9,21,9),(9,31,10),
(10,8,1),(10,30,2),(10,2,3),  (10,7,4),(10,29,5),(10,31,6),  (10,27,7),(10,21,8),(10,1,9),(10,6,10),
(28,21,1),(28,30,2),(28,1,3),  (28,20,4),(28,10,5),(28,29,6),  (28,8,7),(28,25,8),(28,6,9),(28,31,10),
(21,20,1),(21,28,2),(21,31,3),  (21,29,4),(21,8,5),(21,30,6),  (21,7,7),(21,10,8),(21,1,9),(21,25,10),  (21,6,11),
(2,30,1),(2,8,2),(2,1,3),  (2,7,4),(2,10,5),(2,6,6),  (2,9,7),(2,31,8),(2,20,9),(2,25,10),
(27,8,1),(27,30,2),(27,10,3),  (27,25,4),(27,6,5),(27,31,6),  (27,1,7),(27,21,8),(27,29,9),(27,7,10),
(20,21,1),(20,30,2),(20,29,3),  (20,8,4),(20,31,5),(20,7,6),  (20,1,7),(20,10,8),(20,25,9),(20,28,10),
(24,25,1),(24,31,2),(24,21,3),  (24,10,4),(24,8,5),
(25,24,1),(25,8,2),(25,30,3),  (25,31,4),(25,20,5),(25,29,6),  (25,10,7),(25,1,8),(25,27,9),(25,21,10),  (25,6,11),
(3,10,1),(3,1,2),(3,31,3),
(8,10,1),(8,30,2),(8,7,3),  (8,21,4),(8,29,5),(8,20,6),  (8,1,7),(8,25,8),(8,6,9),(8,31,10),
(6,24,1),(6,25,2),(6,30,3),  (6,10,4),(6,8,5),(6,29,6),  (6,1,7),(6,2,8),(6,31,9),(6,7,10),  (6,27,11),(6,20,12),
(29,30,1),(29,21,2),(29,20,3),  (29,8,4),(29,7,5),(29,31,6),  (29,10,7),(29,1,8),(29,6,9),(29,25,10),
(30,10,1),(30,7,2),(30,20,3),  (30,8,4),(30,27,5),(30,6,6),  (30,24,7),(30,25,8),(30,31,9),
(1,30,1),(1,8,2),(1,10,3),  (1,29,4),(1,7,5),(1,31,6),  (1,2,7),(1,20,8),(1,6,9),(1,28,10);

-- June 13, 2016 upadte tool_date column in tool table for all tools
update tool SET tool_date = '2013-12-20' where id = 1;
update tool SET tool_date = '2013-12-20' where id = 2;
update tool SET tool_date = '2013-12-20' where id = 3;
update tool SET tool_date = '2013-12-20' where id = 4;
update tool SET tool_date = '2013-12-20' where id = 6;
update tool SET tool_date = '2013-12-20' where id = 7;
update tool SET tool_date = '2013-12-20' where id = 8;
update tool SET tool_date = '2013-12-20' where id = 9;
update tool SET tool_date = '2013-12-20' where id = 10;
update tool SET tool_date = '2013-12-20' where id = 18;
update tool SET tool_date = '2014-03-04' where id = 20;
update tool SET tool_date = '2014-07-28' where id = 21;
update tool SET tool_date = '2014-09-10' where id = 22;
update tool SET tool_date = '2015-12-20' where id = 23;
update tool SET tool_date = '2014-09-18' where id = 24;
update tool SET tool_date = '2014-11-04' where id = 25;
update tool SET tool_date = '2014-12-20' where id = 27;
update tool SET tool_date = '2014-12-20' where id = 28;
update tool SET tool_date = '2015-03-02' where id = 29;
update tool SET tool_date = '2015-04-09' where id = 30;
update tool SET tool_date = '2015-04-16' where id = 31;
update tool SET tool_date = '2015-10-20' where id = 32;
update tool SET tool_date = '2016-06-21' where id = 33;

-- June 13, 2016 upadte default values into lxrm_tool_sorting table
-- insert into lxrm_tool_sorting (tool_id) 
-- (select id from tool where tool_date >= date_sub(current_date(),interval 6 month) 
-- and is_deleted=0 and show_in_home=1 order by tool_date desc limit 20) union 
-- (select t.id from tool t left join (select tool_id, count(distinct user_id) as 'usage' 
-- from tool_usage, session where tool_usage.session_id = session.id group by tool_id order by tool_id) as tu 
-- on t.id = tu.tool_id where t.is_deleted = 0 and t.show_in_home = 1 and 
-- t.tool_date<=date_sub(current_date(),interval 6 month) order by tu.usage desc)
-- ;
insert into lxrm_tool_sorting(sorting_order_id,tool_id)
(select @curRank := @curRank + 1 AS rank , id from tool,
(SELECT @curRank := 0) q where tool_date >= date_sub(current_date(),interval 6 month)
and is_deleted=0 and show_in_home=1 order by tool_date desc limit 20)
union
(select @curRank := @curRank + 1 AS rank, t.id
from tool t left join (select tool_id, count(distinct user_id) as 'usage'
from tool_usage, session,(SELECT @curRank := 0) q
 where tool_usage.session_id = session.id group by tool_id order by tool_id) as tu
on t.id = tu.tool_id where t.is_deleted = 0 and t.show_in_home = 1 and
t.tool_date<=date_sub(current_date(),interval 6 month) order by tu.usage desc);

-- For Ask The Expert need to added like a tool for payment related issues
insert into tool (id, name, description, img_path, is_free, tagline, cat_id, desc_link, tool_link, is_deleted,show_in_home, tool_date) 
values (0, 'Ask The Expert', '', '', 0, '', 6, '', '',0 , 0,NOW());

-- For Ask The Expert question price insert into tools_pricing table

insert into tools_pricing (tool_id, display_string, report_type, amount) values(0, 'Ask Expert Question', 'Question', 2.99);


/********* Aug 24th 2016 Most Used Keywords **************/
 INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, 
`cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`,`tool_date`) 
VALUES ('34', 'Most Used Keywords', 'Check and analyse most used keywords present in a website', 'MUK', '1', 
'Track and analyse used keywords for a domain.', '3','\most-used-keywords-tool?query=\'desc\'','\most-used-keywords-tool.html', '0', '1','2016-10-26');

insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,34,4.9,'','','2016-10-26 00:00:00');

insert into tools_pricing (tool_id, display_string, report_type, amount) values(0, 'Ask Expert Question', 'Question', 7.99);

/***Tool id 34 Most used keywords related tools maping query Oct 10,2016.***/
insert into tools_grouping (tool_id, correlated_tool_id, tools_order) values (34,30,1),(34,7,2),(34,10,3),(34,9,4),(34,8,5);

/* Oct 5th updating the aquisition_type column
Note: Before adding this column we have only one type i.e SEO Tip, 
so updating that column with "SEO Tip" after adding that column */
UPDATE lxrm_user_aquisition SET aquisition_type = 'SEO Tip' WHERE id > 0;

/* Oct 6, 2016 inserting ebook record in lxrm_user_aquisition_popup table */
INSERT INTO lxrm_user_aquisition_popup(popup_name, is_active, created_date, mail_attachment_name) 
VALUES('Ebook', 1, NOW(), 'lxrm-top10-seo-tactics.pdf');

INSERT INTO `lxrm_promos` (`promo_code`, `start_date`, `end_date`, `promo_name`, `reusable_count`) VALUES ('FreeFromUPS', '2016-10-17', '2016-10-20', 'Please click on SUBMIT to ask our experts.', '1');

-- Oct 27th inserting expert data into lxrm_ate_experts table
INSERT INTO `lxrm_ate_experts` (`expert_id`, `first_name`, `last_name`, `email`, `role`, `is_deleted`) VALUES ('1', 'Neelakantha Rao', 'Yathavakilla', 'neelakantha.y@netelixir.com', 'expert', '0');
INSERT INTO `lxrm_ate_experts` (`expert_id`, `first_name`, `last_name`, `email`, `role`, `is_deleted`) VALUES ('2', 'Imran', 'Ahmed Khan', 'imran.khan@netelixir.com', 'expert', '0');
INSERT INTO `lxrm_ate_experts` (`expert_id`, `first_name`, `last_name`, `email`, `role`, `is_deleted`) VALUES ('3', 'Praveen Kumar ', 'Burri', 'praveen.burri@netelixir.com', 'reviewer', '0');


/*Nov 11, 2016 Ask the expert Answer templates, Refer Tool Based Analysis sheet.*/
/*Template id =1 Inbound Link Checker.*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(20,'Fixing #Count_of_Bad_Backlinks of bad backlinks',
'<ul style=\"margin-left: 4%;padding:0;\">
<li>Get the complete list of Bad backlinks (Needs to be sent to user as an attachment).</li>
<li>Scan the list of bad backlinks and highlight a few within the response.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about fixing your bad backlinks!</p>
<p style="margin: 0 0 10px 0;">Backlinks are an extremely important factor for your website&rsquo;s overall SEO ranking. However, these days it&rsquo;s not about the number of backlinks that your website has, but more about the quality and authority of websites linking to you.</p>
<p style="margin: 0 0 10px 0;">Google tends to penalize websites with an unnatural backlinks profile. Websites with too many backlinks that Google deems &ldquo;low quality&rdquo; will have a more difficult time making it in to the top Google results.</p>
<p style="margin: 0 0 10px 0;">Attached is a list of all of the websites providing backlinks to your site and how they rank online. Using the attached list, follow these three steps to remove bad backlinks to your website:</p>
<ol style="margin: 0 0 10px 5px; padding: 0;">
<li>Identify bad backlinks
<p>You can definitely use your judgment as to what links might be low quality, but we recommend that any website with a referring URL rating of 5 or less can be considered a bad backlink.</p>
</li>
<li>Request removal of bad backlinks
<p>Unfortunately, this step can be a bit time consuming, but it is important. To remove backlinks from your site, you will need to contact the webmasters of the website and ask them to delete the link pointed to your site.</p>
<p style="margin: 10px 0;">You&rsquo;re probably thinking that the webmasters aren&rsquo;t going to respond to you or remove your link, and there is always that possibility, but by putting in the time and effort to do this step, it will make it easier to disavow a link with Google. More on that in step three.</p>
<p style="margin: 0 0 10px 0;">For now, back to step two.</p>
<ul style="padding: 0px; margin: 0 0 10px 40px; list-style-type: disc;">
<li>Identify the high level domains of the links you want to remove. In the link, this will be everything before the first backslash&mdash;everything from the www to the .com.</li>
<li>Find the respective webmaster contact information for each link. We can make it easy for you with the<a href="\&quot;http://www.lxrmarketplace.com/domain-age-checker-tool.html\&quot;"> Domain Age Checker</a> on the LXRMarketplace.</li>
<li>Send an email to the webmaster requesting them to remove the backlink. You can use this template:
<div style="color: #999999; margin: 20px 0 20px 20px;">
<p style="margin: 0 0 10px 0;">My name is [YOUR_NAME] and I work with [YOUR_WEBSITE]. I wanted to thank you for linking to our website from [URL_OF_THE_PAGE_LINKING_TO_YOU].</p>
<p>We are currently in the process of removing backlinks to our website. Therefore, I will have to ask you kindly to remove this backlink from your site.</p>
<p style="margin: 0 0 10px 0;">The link is located here: [URL_OF_THE_PAGE_LINKING_TO_YOU]</p>
<p style="margin: 0 0 10px 0;">It links to [URL_OF_YOUR_SITE]&gt;</p>
<p style="margin: 0 0 10px 0;">And it&rsquo;s using the anchor text [ANCHOR_TEXT_USED_FOR_YOUR_LINK].</p>
<p style="margin: 0 0 10px 0;">Thank you for your time.</p>
<p style="margin: 0 0 10px 0;">Regards,</p>
</div>
</li>
<li>Send a follow-up email if you do not receive a reply within 5 to 10 business days. Make sure to keep the records of all of the emails you have sent.</li>
</ul>
</li>
<li>Disavow links with Google.</li>
</ol>
<p style="margin: 0 0 10px 0;">Remember when I said that some webmasters might not respond to your emails? Well, if you did the appropriate leg work outlined in step two, you should easily be able to disavow those links with Google.</p>
<p style="margin: 0 0 10px 0;">Create a text file listing out the bad backlinks that you couldn&rsquo;t get removed along with comments for Google to prove the work you did to get them removed. All of your comments should begin with a hashtag and could look something like this:</p>
<p style="margin: 0 0 10px 0;">Make sure to include all of your unresolved bad backlinks. Once you&rsquo;ve completed the file, upload it to the<a href="https://www.google.com/webmasters/tools/disavow-links-main?pli=1"> Disavow tool</a> in the Google Webmaster tools. The updates are not going to happen right away, and it could take a few weeks for Google to process your request.</p>'
);

/*Template id =2 Inbound Link Checker.*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(20,'Little variation in anchor texts of backlinks. \"<\'ANCHOR_TEXT\'>\" repeating more than 20% of the times',
'<ol style=\"margin-left: 4%;padding:0;\">
<li>Get the complete list of Bad backlinks (Needs to be sent to user as an attachment).</li>
<li>Identify types of anchor text
<ol style=\"list-style-type: lower-alpha;margin-left: 4%;padding:0;\">
<li>Branded Anchors.</li>
<li>Generic Anchors.</li>
<li>Naked Link Anchors.</li>
<li>Image Anchors.</li>
<li>Brand + Keyword Anchors.</li>
<li>LSI Anchors.</li>
<li>Partial Match Anchors.</li>
<li>Long Tail Anchors.</li>
<li>Exact Match Anchors.</li>
</ol>
</li>
<li>Provide suggestions on which links should be modified using the benchmark ratio
<ol style=\"list-style-type: lower-alpha;margin-left: 4%;padding:0;\">
<li>Branded Anchors: 40%.</li>
<li>Unique/Other anchors: 25%.</li>
<li>Naked link anchors: 15%.</li>
<li>Brand + keyword anchors: 5%.</li>
<li>Partial match anchors: 5%.</li>
<li>Generic anchors: 1-5%.</li>
<li>Long tail anchors: 2-4%.</li>
<li>Exact Match Anchors: Less Than 1%.</li>
</ol>
</li>
</ol>',
'<p style="margin:0 0 10px 0;">Hi! Thanks for your question about adding variation to your backlinks anchor text!</p>
<p style="margin:0 0 10px 0;">Anchor text is the highlighted, often underlined, clickable word or phrase that is used instead of just sharing the full URL on a webpage. Search engines use anchor texts much like keywords, as an indicator of the topic of the webpage that is being linked to.</p>
<p style="margin:0 0 10px 0;">You want to make sure that other websites are linking to your webpage or product, producing inbound links to you site. To go one step further on this, you want to make sure that the sites that are linking to your pages are not using the same phrase over, and over, and over again. Too much of the same anchor text could be viewed as SPAM or link buying by Google, and your site could end up getting penalized.</p>
<p style="margin:0 0 10px 0;">As a reputable website, you should encourage a range of different types of anchor texts focused around your brand.</p>
<p style="margin:0 0 10px 0;">Attached to this message is a breakdown of your inbound anchor texts and also a breakdown of the industry standard of anchor text ratios. These reports show that you need more<span style="font-weight: 400; color: #ff0000;"> &lt;&lt;type_of_more_anchors_needed&gt;&gt;</span> and less<span style="font-weight: 400; color: #ff0000;">&lt;&lt;type_of_less_anchors_needed&gt;&gt;</span> leading you your website.</p>
<p style="margin:0 0 10px 0;">In order to make these adjustments, you should reach out to the webmasters of the sites linking to you asking them o update their anchor texts. Since they are already providing a backlink, they shouldn&rsquo;t mind updating the anchor text.</p>
<ul style="margin:15px 0 0 40px;padding:0;">
<li>Identify the high level domains of the links you want to remove. In the link, this will be everything before the first backslash&mdash;everything from the www to the .com.</li>
<li>Find the respective webmaster contact information for each link. We can make it easy for you with the<a href=\"http://www.lxrmarketplace.com/domain-age-checker-tool.html\"> Domain Age Checker</a> on the LXRMarketplace.</li>
<li>Send an email to the webmaster requesting them to remove the backlink. You can use this template:</li>
</ul>
<div style="color: #999999;margin:20px 0 20px 60px;">
<p style="margin:0 0 10px 0;"> My name is [YOUR_NAME] and I work with [YOUR_WEBSITE].</p>
<p style="margin:0 0 10px 0;">I wanted to thank you for linking to our website from [URL_OF_THE_PAGE_LINKING_TO_YOU].</p>
<p style="margin:0 0 10px 0;">It has come to our attention that it&rsquo;s best to update anchor text of this backlink &ldquo;[TEXT_TO_BE_UPDATE]&rdquo;. Therefore, I will have to ask you kindly to update the anchor text of this backlink from your site.</p>
<p style="margin:0 0 10px 0;">The link is located here: [URL_OF_THE_PAGE_LINKING_TO_YOU].</p>
<p style="margin:0 0 10px 0;">It links to [URL_OF_YOUR_SITE].</p>
<p style="margin:0 0 10px 0;">And it&rsquo;s using the anchor text [ANCHOR_TEXT_USED_FOR_YOUR_LINK].</p>
<p style="margin:0 0 10px 0;">Thank you for your time.</p>
<p style="margin:0 0 10px 0;">Regards,</p>
</div>
<p style="margin:0 0 10px 0;">If you do not get a reply within 5 to 10 business days, send a follow up email again asking for a change of the anchor text. Feel free to follow up multiple times since most times, people need to be continually reminded.</p>');

/*Template id =3 Inbound Link Checker.*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(20,'Increase DoFollow backlinks',
'<ul style="margin-left: 4%;padding:0;">
<li>Get the complete list of Bad backlinks (Needs to be sent to user as an attachment).</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about increasing your DoFollow backlinks!</p>
<p style="margin: 0 0 10px 0;">Before we start talking about DoFollow links, let&rsquo;s chat a bit first about NoFollow links. A NoFollow link tells search engine spiders that a link on a website should not count as an inbound link and should not affect the overall ranking of the webpage the original site is linking to.</p>
<p style="margin: 0 0 10px 0;">NoFollow links are intended to reduce the effectiveness of certain search engine SPAM and improve the quality of search engine results, but they can also be doing harm to reputable websites.</p>
<p style="margin: 0 0 10px 0;">To create quality DoFollow links, try some of the following technique</p>
<ul style="list-style-type: disc;padding:0;margin:0 0 0 40px;">
<li>Add your link in your email signature.</li>
<li>Link to your site in all your social media platforms.</li>
<li>Link to your site on your business cards that you will naturally give out at industry events.</li>
<li>Comment on relevant blogs and site.
<ul style="list-style-type: circle;padding:0;margin:0 0 0 30px;">
<li>Only use relevant sites and forums. Google has begun penalizing sites for posting on non-relevant forums as well as on &ldquo;link farms&rdquo;.</li>
</ul>
</li>
<li>Ask to guest post. Find a relevant blog and ask to post with relevant content that will help promote your site.</li>
<li>Interview someone. Interviewees usually link back to these interviews, and they&rsquo;re a great way to get to know people in your industry.</li>
<li>Write a testimonial or a review of a product.</li>
<li>Leave reviews for local businesses you&rsquo;ve visited on sites like Yelp.</li>
<li>Give feedback online through social media. If you like an article, tweet that to the writer. If you had a great hotel stay, put it on their Facebook page.</li>
</ul>
<p style="margin: 10px 0 0 0;">When using the above techniques, make sure to incorporate your link in a natural manner. If you can not work the link into the actual content, use it in your signature line. These techniques should not only get you an automatic quality DoFollow backlink, but should also encourage others to visit and link to your page.</p>'
);

/*Template id =4 Broken link checker*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(31,'Fixing 302 link re-directions',
'<ul style=\"margin-left: 4%;padding:0;list-style-type: disc;\">
<li>Get the complete list of 302 redirects by using the  Broken Link Checker Tool (Needs to be sent to user as an attachment).</li>
<li>Analyze list of 302 redirects.</li>
<li>If any links to be changed to 301 suggest the same. Also, provide a generic way in which one can make 302 links as 301 links..</li>
<li>Attach the complete list of links that should be converted into 301.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about website redirects!</p>
<p style="margin: 0 0 10px 0;">Before we jump into analyzing your website, let me give you a little background info first. A 301 redirect means that your webpage has packed up, moved for good, and will never be returning to its original URL. A 302 redirect means that your webpage is purely on vacation and will eventually return to its original URL.</p>
<p style="margin: 0 0 10px 0;">These specific redirects tell search engines whether or not the webpage has been replaced. The main difference is that 301 redirects will pass on link juice, or positive ranking factor that a link passes from one page to another, on to the new, redirected link. A 302 redirect will not, so you&rsquo;ll have to start from zero.</p>
<p style="margin: 0 0 10px 0;">We have checked out your site, and attached to this message is the complete list of 302 redirects on your website. These redirects may be appearing because of one of the following reasons:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>The link is broken.</li>
<li>The page is no longer active.</li>
<li>The page is under construction and you are detouring visitors.</li>
<li>The new page is being A/B tested for design and functionality.</li>
<li>The old website is being redirected to a new website.</li>
</ul>
<p style="margin: 0 0 10px 0;">After looking over the attached file, we believe that <span style="color: #ff0000;">&lt;&lt;#_of_302_redirects&gt;&gt;</span> 302 redirects would work better as 301 redirects. The specific links are highlighted in the attachment.</p>
<p style="margin: 0 0 10px 0;">Take a look at the links that we highlighted and determine if they fall into any of the above redirection reasons. Then you will need to decide whether or not you want to change the 302&nbsp;redirects to a 301 redirect.</p>
<p style="margin: 0 0 10px 0;">Once you have the verified the complete list of links that you want to mark as 301, put the following .php code into each of those webpages. The following code should be placed after the &lt;body&gt; tag.</p>
<div style="margin: 0 0 10px 40px; color: #808080;">
<p><em>&lt;?php $redirectlink = &lsquo;http://www.put-the-redirected-webpage-url-here.com&lsquo;</em></p>
<p>//do not edit below here</p>
<p>header (&lsquo;HTTP/1.1 301 Moved Permanently&rsquo;);</p>
<p>header(&lsquo;Location: &lsquo;.$redirectlink);</p>
<p>exit;</p>
<p>?&gt;</p>
</div>
<p style="margin: 0 0 10px 0;">The filename of this .php should be the URL slug of the page you want to redirect. For example: if the webpage file name is <span style="text-decoration: underline;"><span style="color: #3366ff; text-decoration: underline;">http://www.domain-name/WEBPAGE_FILE_NAME.html,</span></span> then name the file as WEBPAGE_FILE_NAME.php.</p>
<p style="margin: 0 0 10px 0;">The above code will redirect the webpage as a 301 redirect. This code will use a 301 redirect to the URL that you desire.</p>'
);

/*Template id =5 Broken link checker*/
insert into lxrm_ate_answer_templates (tool_id,question,steps,answer)
values(31,'Fixing 404 missing page errors','<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>Get the complete list of 404 errors by using the Broken Link Checker Tool (Needs to be sent to user as an attachment).</li>
<li>Analyze list of 404 errors.</li>
<li>Tell users why they have 404 errors. Possible issues are below.Accordingly suggest to users.
<ul style="margin: 0 0 0 40px; padding: 0px; style: circle;">
<li>A manually mistyped URL path.</li>
<li>Third-party sites linking to nonexistent or removed pages.</li>
<li>Old links from social media platforms (scroll down your Facebook page to 2006, some links you shared might not be valid anymore!).</li>
<li>Errors with internal links.</li>
</ul>
</li>
</ul>','<p style="margin: 0 0 10px 0; padding: 0px;">Hi! Thanks for your question about fixing your 404 error pages!</p>
<p style="margin: 0 0 10px 0; padding: 0px;">A 404 error page is not a death sentence. But it is definitely something you want to avoid as much as possible on your website.</p>
<p style="margin: 0 0 10px 0; padding: 0px;">404 is a standard response from your websites saying that the visitor was able to get to your site (so it is not a server issue) but were not able to access the specific page that they were looking for. This could be for a majority of reasons:</p>
<ul style="margin: 0 0 10px 45px; padding: 0px;">
<li>The visitor misspelled the link they wanted to visit.</li>
<li>Other websites are linking to a page on your website that you moved or deleted, creating a broken backlink situation).</li>
<li>You linked something wrong on one of your other pages.</li>
</ul>
<p style="margin: 0 0 10px 0; padding: 0px;">Attached is the complete list of broken links on your website that would lead to a 404 error page. According to the document, you have &lt;&lt;#_of_404_pages&gt;&gt; links that are leading to an error page. [First you want to make sure that those internal errors are taken care of. Spend some time fixing any internal linking issues to ensure that your site is as clean as possible.] OR [Congratulations! Your website is currently free of any internal issues leading to a 404 page. Keep doing whatever you are doing to maintain this.]</p>
<p style="margin: 0 0 10px 0; padding: 0px;">Now comes the fun part…</p>
<p style="margin: 0 0 10px 0; padding: 0px;">There are 3 steps that you will need to fix your 404 errors that might be outside of your control.</p>
<ol style="margin: 0 0 10px 45px; padding: 0px;">
<li><strong>Make sure you have a functioning error page.</strong>
<p style="margin: 0 0 10px 0; padding: 0px;">Not all 404 pages are created equal. Some have a fun graphic, others include a search engine to help users find a similar page on the site, and some are just a plain white page with bold black letters. You will need to first decide what will work best for your website.</p>
<p style="margin: 0 0 10px 0; padding: 0px;">Consider including some the following when creating or updating your 404 page:</p>
<ul style="list-style-type: disc;margin: 0 0 10px 45px; padding: 0px;">
<li>Your companys name and logo.</li>
<li>An explanation of why the visitor might be seeing this page.</li>
<li>A list of common mistakes that may explain the problem.</li>
<li>Links back to the homepage and/or other pages that might be relevant.</li>
<li>A search engine that visitors can use to find the right information.</li>
<li>An email link so that visitors can report problems, missing pages, and so on.</li>
</ul>
</li>
<li><strong>Keep track of your error page traffic using Google Analytics.</strong>
<p>It could be difficult to fix external 404 visits if you do not know how they are coming to you. If you do not already use Google Analytics, we would suggest setting up an account. Once you have an account, you can set up alerts or track when visitors arrive at a 404 error page.</p>
<p style="margin: 0 0 10px 0; padding: 0px;">You can set up an alert by completing the following:</p>
<ul style="list-style-type: disc;margin: 0 0 10px 45px; padding: 0px;">
<li>Log in to your Google Analytics account.</li>
<li>Select the “Reporting” tab.</li>
<li>Choose one of the “Intelligence Events” reports (Overview, Daily, Weekly, or Monthly events).</li>
<li>Select the “Custom Alerts” tab.</li>
<li>Click “Manage Custom Alerts”.</li>
<li>Click the red “+New Alert” button.</li>
<li>Duplicate the following to set up an alert.</li>
</ul>
<div style="text-align: center; "><img src="/ate_images/ate_template_images/5_1.png"></div><p>You can also set up a report to track errors by creating a goal:</p>
</li><ul style="list-style-type: disc; margin: 0 0 10px 45px; padding: 0px;">
<li>Log in to your Google Analytics account.</li>
<li>Select the “Admin” tab.</li>
<li>In “View” column, select “Goals”.</li>
<li>Duplicate the following to set up a goal to track 404 errors.</li>
</ul></li><div style="text-align: center; "><img src="/ate_images/ate_template_images/5_2.png"><br></div><li><strong>Fix any errors</strong>
<p>Now that you have discovered how people are ending up on your 404 page, you can use this data to limit this number.</p>
<ol style="margin: 0 0 10px 45px; padding: 0px;style:number">
<li>Redirect the error somewhere else:<p style="padding: 0px;">If you notice that a lot of people are using the same misspelling of a link, you can create this link on your website and have it automatically redirected to the correctly spelled link.</p>
</li>
<li>Restore deleted pages:<p style="padding: 0px;">If enough people are trying to access a page that you deleted, it may be beneficial to either recreate the page, or try redirecting that address to an active page with similar content.</p> 
</li>
</ol>
</li>');

/*Template id =6 Broken link checker*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(31,'Fixing server 500 errors',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>Get the complete list of 500 internal server errors by using the Broken Link Checker Tool (Needs to be sent to user as an attachment).</li>
<li>This one is a standard answer. Just use the following generic answer.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about 500 errors!</p>
<p style="margin: 0 0 10px 0;">For starters, your website is okay. This error has nothing to do with the functionality of your website. It&rsquo;s your server, or the system where your website is stored, that is causing this error.</p>
<p style="margin: 0 0 10px 0;">When someone visits your link, they are asking the server to display a page. If your visitor gets a 500 error, there is something stopping the server from being able to display the page. This &ldquo;something&rdquo; is NOT because of their computer, their internet connection, or a misspelled link.</p>
<p style="margin: 0 0 10px 0;">Here is where we get a little technical.</p>
<p style="margin: 0 0 10px 0;">There are many reasons why you site might be showing a 500 error. Here are the most common:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>The visitor doesn&rsquo;t have permission to visit your site. Some of the files on your site might not have a set permission. You want to make sure these files are set to permissions 0775 (-rwxr-xr-x), which allows visitors to visit and view the site.</li>
<li>The external source you are using to host images, videos, or additional data is causing a timeout error. Setting up your server&rsquo;s code to reset these external sources more frequently should do the trick.</li>
<li>There are some formatting issues in your .htaccess file. Your .htaccess file needs to follow an exact structure. Make sure that this file is properly structured.</li>
</ul>
<p style="margin: 0 0 10px 0;">These updates will unfortunately differ from server to server, so you may need to reach out to your individual server&rsquo;s support for advanced technical support.</p>'
);


/*Template id =7 Broken link checker*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(31,'Fixing redirections issues',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>Get the complete list of Too Many Redirections by using the Broken Link Checker Tool (Needs to be sent to user as an attachment).</li>
<li>This is a standard answer to all users.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about website redirects!</p>
<p style="margin: 0 0 10px 0;">A redirected link should be direct and continuous. So sometimes when we get a little over-zealous with our redirects, we could unintentionally end up creating an indefinite loop of redirects.</p>
<p style="margin: 0 0 10px 0;">Depending on the type of browser that your website visitor is using, they may receive an error message designed by that browser to break the cycle of too many redirects. According to the attached document, you currently have <span style="color: #ff0000;">&lt;&lt;_of_too_many_redirects&gt;&gt;</span> redirects that may be causing this error.</p>
<p style="margin: 0 0 10px 0;color: #ff0000;">[You will need to make a few edits to clean up your redirects.] OR [You currently do not need to make any changes to your redirects.]</p>
<p style="margin: 0 0 10px 0;">First, you&rsquo;ll find any links with too many redirects outlined in the attached document. If you are unable to correct the issue from that list, the best thing for you to do would be to access your .htaccess file and check for any cyclic or too-many-redirects codes. You want to make sure that this file is as clean as possible to avoid confusion on your site.</p>'
);

/*Template id =8 SEO SiteMap Builder*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(6,'Fixing missing links in sitemap',
'<ul style="list-style-type: disc;margin: 0 0 0 40px; padding: 0;">
<li>Get the Sitemap file generated using sitemap builder tool (Needs to be sent to user as an attachment).</li>
<li>Extract the zipped Sitemap file.</li>
<li>Go through the file sitemap.err.</li>
<li>Analyze the issues shown within the .err file and provide the answer as per the following template.
<ul style="list-style-type: circle;margin: 0 0 0 20px; padding: 0;">
<li>There could be either 500 (Internal server errors) or 404 (Page not found errors).</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Sitemaps are an organizational file that lists every page of your website that is available for search engines to find, and they allow search engines to better understand the overall structure of your website. There is no downside of having an XML Sitemap and having one can improve your SEO.</p>
<p style="margin: 0 0 10px 0;">By analyzing your sitemap generated through our Sitemap Generator tool we see following issues.</p>
<p style="margin: 0 0 10px 0;">There are<span style="color: #ff0000;"> &lt;#NUM_OF_500_ERRORS&gt;&nbsp;</span>&nbsp;of pages which are not getting included in your sitemap because of Internal server errors.</p>
<p style="margin: 0 0 10px 0;">The error is not because of your computer, your internet connection, or because you misspelled the link, the error is a problem with the website&rsquo;s server. When you attempt to access a particular URL or link, you are requesting the server, the system where your website is stored, to display a page. If the server is unable to show you that page it tells you there&rsquo;s an error.</p>
<p style="margin: 0 0 10px 0;">We have also found <span style="color: #ff0000;">&lt;#NUM_OF_404_ERRORS&gt;</span>&nbsp;of pages missing on your webpage and therefore not getting included in the sitemap. The 404-error message is a standard response that says the user is able to access the overall website, but cannot access the specific page they were searching for.</p>
');

/*Template id =9 SEO SiteMap Builder*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(6,'Submitting my sitemap to Google and Bing',
'<ul style="list-style-type: disc;margin: 0 0 0 40px; padding: 0;">
<li>Get the Sitemap file generated using sitemap builder tool (Needs to be sent to user as an attachment).</li>
<li>Go through the file sitemap.err.</li>
<li>Verify whether there are any issues and accordingly provide answer using this link.</li>
<li>Check for what search engines user is looking to upload his sitemaps.
</li>
</ul>',
'<p style="margin: 0 0 10px 0;"><span style="font-weight: 400;">Sitemaps are an organizational file that lists every page of your website that is available for search engines to find, and they allow search engines to better understand the overall structure of your website. There is no downside of having an XML Sitemap and having one can improve your SEO.</span></p>
<p style="margin: 0 0 10px 0;"><span style="font-weight: 400;">There are three ways in which you can submit sitemaps to search engines.</span></p>
<ol style="padding:0;margin:0 0 0 40px;">
<li>Ping your sitemap to search engines
<ol style="list-style-type: lower-alpha;padding:0;margin:0 0 0 40px;">
<li>Once your sitemap is uploaded to your site, copy the following URL, update the information with your sitemap&rsquo;s URL, and enter it in a web browser <a href="http://www.google.com/ping?sitemap=http://www.yourdomain.com/sitemap.xml">http://www.google.com/ping?sitemap=http://www.yourdomain.com/sitemap.xml</a></li>
</ol>
</li>
<li>Add your sitemap to your robots.txt file
<ol style="list-style-type: lower-alpha;padding:0;margin:0 0 0 40px;">
<li>If your website has a robots.txt file, add your sitemap&rsquo;s URL to the file, allowing it to be accessed by search engines.</li>
<li>If you do not have a robots.txt file, you can create on using the<a href="http://www.lxrmarketplace.com/robots-txt-generator-tool.html"> robots.txt generator</a> on the LXRMarketplace.</li>
</ol>
</li>
<li>Submit your sitemap manually on webmaster accounts
<ol style="list-style-type: lower-alpha;padding:0;margin:0 0 0 40px;">
<li><a href="http://www.google.com/webmasters">Google</a> and<a href="http://www.bing.com/webmaster"> Bing</a> allow you to upload your sitemap directly to their webmaster accounts.</li>
</ol>
</li>
</ol>
');

/*Template id =10 Robots.txt Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(24,'Creating Robots.txt file from my website',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator. (Needs to be sent to user as an attachment).</li>
<li>Go to user&rsquo;s website and analyze it for possible links that need to be included in the Robots.txt. Here you are more or less verifying whether the generated Robots.txt file is correct and needs any additional inclusions in it..</li>
<li>Analyze the issues shown within the .err file and provide the answer as per the following templateYou can refer to following links in changing the Robots.txt file [Or] simply use our Robots.txt Generator tool:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href="http://www.robotstxt.org/robotstxt.html" target="_blank">http://www.robotstxt.org/robotstxt.html</a></li>
<li><a href="https://en.wikipedia.org/wiki/Robots_exclusion_standard" target="_blank">https://en.wikipedia.org/wiki/Robots_exclusion_standard</a></li>
<li><a href="https://varvy.com/robottxt.html" target="_blank">https://varvy.com/robottxt.html</a></li>
</ul>
</li>
</ul>',
'<p  style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul  style="margin: 0 0 10px 40px;padding:0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="color:red;margin: 0 0 10px 0;">[[VERIFY USER&rsquo;S WEBPAGE AND SEE WHETHER SOME OF THE IMPORTANT WEBPAGES ARE MISSING IN ROBOTS.TXT OR NOT. Accordingly add the same or exclude the same from the robots.txt file.]]</p>
<p style="margin: 0 0 10px 0;">Attached is a robots.txt file modified to fit the current SEO best practices.</p>
<p style="margin: 0 0 10px 0;">The following links should be included in your robots.txt file.</p>
<ul style="margin: 0 0 10px 40px;padding:0;color:red;">
<li>[Link-1]</li>
<li>[Link-2]</li>
<li>[Link-3] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;">The following links should be excluded from your robots.txt.</p>
<ul style="margin: 0 0 10px 40px;padding:0;color:red;">
<li>[Link-1]</li>
<li>[Link-2]</li>
<li>[Link-3] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;">You can exclude links using the following language:</p>
<p style="color:red;">[Lines from Robots.txt file]</p>'
);

/*Template id =11 Robots.txt Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(24,'Fixing errors and warnings from Robots.txt file',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator/validator. (Needs to be sent to user as an attachment)
.</li>
<li>Warnings and Errors are clearly highlighted in the final output.</li>
<li>You can refer to the following links in understanding the syntax of Robots.txt file:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href=\"https://varvy.com/robottxt.html\" target="_blank">https://varvy.com/robottxt.html</a></li>
<li><a href=\"http://www.robotstxt.org/orig.html\" target="_blank">http://www.robotstxt.org/orig.html</a></li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are syntax errors]]</p>
<p style="margin: 0 0 10px 0;">The format and structure of a robots.txt file are as follows:</p>
<ol style="margin: 0 0 10px 40px; padding: 0;">
<li>The file consists of one or more records separated by one or more blank lines (terminated by CR,CR/NL, or NL). Each record contains lines of the form "&lt;field&gt;:&lt;optionalspace&gt;&lt;value&gt;&lt;optionalspace&gt;". The field name is case insensitive.</li>
<li>Comments can be included in file using the \'#\' character.</li>
<li>The record starts with one or more User-agent lines, followed by one or more Disallow lines.</li>
</ol>
<p style="margin: 0 0 10px 0;">Your Robots.txt file seem to be not following above syntax at the following places.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - Line 1] - [Syntax Error]</li>
<li>[Robots.txt - Line 2] - [Syntax Error]</li>
<li>[Robots.txt - Line 3] - [Syntax Error] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are logic errors]]</p>
<p style="margin: 0 0 10px 0;">The official standard does not include Allow directive even though major search engines like Google and Bing support it. You are allowing as well as disallowing the following URLs within robots.txt file which is a logical error.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - URL 1]</li>
<li>[Robots.txt - URL 2]</li>
<li>[Robots.txt - URL 3] &hellip;</li>
</ul>
<p>Depending on your goal, remove the above disallow or allow rules.</p>'
);

/*Template id =12 Robots.txt Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(24,'Validating links that are excluded in Robots.txt file',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator/validator. (Needs to be sent to user as an attachment)
.</li>
<li>Warnings and Errors are clearly highlighted in the final output.</li>
<li>You can refer to the following links in understanding the syntax of Robots.txt file:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href=\"https://varvy.com/robottxt.html\" target="_blank">https://varvy.com/robottxt.html</a></li>
<li><a href=\"http://www.robotstxt.org/orig.html\" target="_blank">http://www.robotstxt.org/orig.html</a></li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are syntax errors]]</p>
<p style="margin: 0 0 10px 0;">The format and structure of a robots.txt file are as follows:</p>
<ol style="margin: 0 0 10px 40px; padding: 0;">
<li>The file consists of one or more records separated by one or more blank lines (terminated by CR,CR/NL, or NL). Each record contains lines of the form "&lt;field&gt;:&lt;optionalspace&gt;&lt;value&gt;&lt;optionalspace&gt;". The field name is case insensitive.</li>
<li>Comments can be included in file using the \'#\' character.</li>
<li>The record starts with one or more User-agent lines, followed by one or more Disallow lines.</li>
</ol>
<p style="margin: 0 0 10px 0;">Your Robots.txt file seem to be not following above syntax at the following places.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - Line 1] - [Syntax Error]</li>
<li>[Robots.txt - Line 2] - [Syntax Error]</li>
<li>[Robots.txt - Line 3] - [Syntax Error] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are logic errors]]</p>
<p style="margin: 0 0 10px 0;">The official standard does not include Allow directive even though major search engines like Google and Bing support it. You are allowing as well as disallowing the following URLs within robots.txt file which is a logical error.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - URL 1]</li>
<li>[Robots.txt - URL 2]</li>
<li>[Robots.txt - URL 3] &hellip;</li>
</ul>
<p>Depending on your goal, remove the above disallow or allow rules.</p>'
);


/*Template id =13 Robots.txt Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(24,'Validating links that are included in Robots.txt file',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator/validator. (Needs to be sent to user as an attachment)
.</li>
<li>Warnings and Errors are clearly highlighted in the final output.</li>
<li>You can refer to the following links in understanding the syntax of Robots.txt file:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href=\"https://varvy.com/robottxt.html\" target="_blank">https://varvy.com/robottxt.html</a></li>
<li><a href=\"http://www.robotstxt.org/orig.html\" target="_blank">http://www.robotstxt.org/orig.html</a></li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are syntax errors]]</p>
<p style="margin: 0 0 10px 0;">The format and structure of a robots.txt file are as follows:</p>
<ol style="margin: 0 0 10px 40px; padding: 0;">
<li>The file consists of one or more records separated by one or more blank lines (terminated by CR,CR/NL, or NL). Each record contains lines of the form "&lt;field&gt;:&lt;optionalspace&gt;&lt;value&gt;&lt;optionalspace&gt;". The field name is case insensitive.</li>
<li>Comments can be included in file using the \'#\' character.</li>
<li>The record starts with one or more User-agent lines, followed by one or more Disallow lines.</li>
</ol>
<p style="margin: 0 0 10px 0;">Your Robots.txt file seem to be not following above syntax at the following places.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - Line 1] - [Syntax Error]</li>
<li>[Robots.txt - Line 2] - [Syntax Error]</li>
<li>[Robots.txt - Line 3] - [Syntax Error] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are logic errors]]</p>
<p style="margin: 0 0 10px 0;">The official standard does not include Allow directive even though major search engines like Google and Bing support it. You are allowing as well as disallowing the following URLs within robots.txt file which is a logical error.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - URL 1]</li>
<li>[Robots.txt - URL 2]</li>
<li>[Robots.txt - URL 3] &hellip;</li>
</ul>
<p>Depending on your goal, remove the above disallow or allow rules.</p>'
);

/*Template id =14 Robots.txt Validator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(25,'Creating Robots.txt file from my website',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator. (Needs to be sent to user as an attachment).</li>
<li>Go to user&rsquo;s website and analyze it for possible links that need to be included in the Robots.txt. Here you are more or less verifying whether the generated Robots.txt file is correct and needs any additional inclusions in it..</li>
<li>Analyze the issues shown within the .err file and provide the answer as per the following templateYou can refer to following links in changing the Robots.txt file [Or] simply use our Robots.txt Generator tool:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href="http://www.robotstxt.org/robotstxt.html" target="_blank">http://www.robotstxt.org/robotstxt.html</a></li>
<li><a href="https://en.wikipedia.org/wiki/Robots_exclusion_standard" target="_blank">https://en.wikipedia.org/wiki/Robots_exclusion_standard</a></li>
<li><a href="https://varvy.com/robottxt.html" target="_blank">https://varvy.com/robottxt.html</a></li>
</ul>
</li>
</ul>',
'<p  style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul  style="margin: 0 0 10px 40px;padding:0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="color:red;margin: 0 0 10px 0;">[[VERIFY USER&rsquo;S WEBPAGE AND SEE WHETHER SOME OF THE IMPORTANT WEBPAGES ARE MISSING IN ROBOTS.TXT OR NOT. Accordingly add the same or exclude the same from the robots.txt file.]]</p>
<p style="margin: 0 0 10px 0;">Attached is a robots.txt file modified to fit the current SEO best practices.</p>
<p style="margin: 0 0 10px 0;">The following links should be included in your robots.txt file.</p>
<ul style="margin: 0 0 10px 40px;padding:0;color:red;">
<li>[Link-1]</li>
<li>[Link-2]</li>
<li>[Link-3] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;">The following links should be excluded from your robots.txt.</p>
<ul style="margin: 0 0 10px 40px;padding:0;color:red;">
<li>[Link-1]</li>
<li>[Link-2]</li>
<li>[Link-3] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;">You can exclude links using the following language:</p>
<p style="color:red;">[Lines from Robots.txt file]</p>'
);

/*Template id =15 Robots.txt Validator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(25,'Fixing errors and warnings from Robots.txt file',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Get the Robots.txt file using Robots.txt file generator/validator. (Needs to be sent to user as an attachment)
.</li>
<li>Warnings and Errors are clearly highlighted in the final output.</li>
<li>You can refer to the following links in understanding the syntax of Robots.txt file:
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li><a href=\"https://varvy.com/robottxt.html\" target="_blank">https://varvy.com/robottxt.html</a></li>
<li><a href=\"http://www.robotstxt.org/orig.html\" target="_blank">http://www.robotstxt.org/orig.html</a></li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">The robots.txt file is a simple text file placed on your web server which tells search engines how they are able to access and interact with your webpages. Your website should have a robots.txt file if you fit into one of the following criteria:</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;">
<li>You have content that you do not want search engines to index.</li>
<li>You are using paid links or advertisements that need special instructions for robots.</li>
<li>You want to limit access to your site from reputable robots.</li>
<li>You are developing a site that is live, but you do not want search engines to index it yet.</li>
<li>You do not have full access to your webserver and how it is configured.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are syntax errors]]</p>
<p style="margin: 0 0 10px 0;">The format and structure of a robots.txt file are as follows:</p>
<ol style="margin: 0 0 10px 40px; padding: 0;">
<li>The file consists of one or more records separated by one or more blank lines (terminated by CR,CR/NL, or NL). Each record contains lines of the form "&lt;field&gt;:&lt;optionalspace&gt;&lt;value&gt;&lt;optionalspace&gt;". The field name is case insensitive.</li>
<li>Comments can be included in file using the \'#\' character.</li>
<li>The record starts with one or more User-agent lines, followed by one or more Disallow lines.</li>
</ol>
<p style="margin: 0 0 10px 0;">Your Robots.txt file seem to be not following above syntax at the following places.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - Line 1] - [Syntax Error]</li>
<li>[Robots.txt - Line 2] - [Syntax Error]</li>
<li>[Robots.txt - Line 3] - [Syntax Error] &hellip;.</li>
</ul>
<p style="margin: 0 0 10px 0;color: #ff0000;">[[If there are logic errors]]</p>
<p style="margin: 0 0 10px 0;">The official standard does not include Allow directive even though major search engines like Google and Bing support it. You are allowing as well as disallowing the following URLs within robots.txt file which is a logical error.</p>
<ul style="list-style-type: disc; margin: 0 0 10px 40px; padding: 0;color: #ff0000;">
<li>[Robots.txt - URL 1]</li>
<li>[Robots.txt - URL 2]</li>
<li>[Robots.txt - URL 3] &hellip;</li>
</ul>
<p>Depending on your goal, remove the above disallow or allow rules.</p>'
);

/*Template id =16 Meta Tag Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(27,'Fix issues with Title',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Check whether the user&rsquo;s webpage has any meta tags or not.</li>
<li>Go through his website and get some keywords using our SEO Website Analysis Tool (<a href=\"http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html\" target="_blank">http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html</a>).</li>
<li>Verify all the four elements of meta tag and see whether you can make any suggestions accordingly
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li>Meta Keywords Attribute.</li>
<li>Title Tag.</li>
<li>Meta Description Attribute.</li>
<li>Meta Robots Attribute.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about your meta tags!</p>
<p style="margin: 0 0 10px 0;">Meta tags are important because they provide search engines with information about your website, such as titles, keywords, and descriptions. Your website should include meta descriptions, meta keywords, and a title.</p>
<p style="margin: 0 0 10px 0;">A good Meta description tags should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use keywords relevant to your webpage.</li>
<li>Not overly repeat keywords but tries to use multiple syntaxes.</li>
<li>Not exceed 150 characters.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good meta keyword tag should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use synonyms of the keywords.</li>
<li>Use unique keywords across different webpages.</li>
<li>Not repeat any given phrase.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good title for any webpage should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Be unique across different webpages.</li>
<li>Contain a Primary Keyword Phrase in every title of every page (if possible).</li>
<li>Include your company name towards the end of the title.</li>
</ul>
<p style="margin: 0 0 10px 0;">Our analysis of your webpage shows us that you are using the following keywords and phrases on your website:</p>
 <p style="margin: 10px 0 0 0;">Webpage URL: <span style="color: #ff0000;">[[User&rsquo;s webpage URL]]</span></p>
  <p style="margin: 0;">Keywords list: <span style="color: #ff0000;">[[Keywords list]]</span></p>
  <p style="margin: 0 0 10px 0;">Keyword Phrases: <span style="color: #ff0000;">[[Keywords Phrases]]</span></p>
<p style="margin: 0 0 10px 0;">Based off of this information, we suggest that you to use the following meta description and meta keywords.</p>
<p style="margin: 0;">Meta Descriptions:</p>
<div style="color: #ff0000; margin: 0 0 10px 0;">
<p style="margin: 0;">[[Description-1]]</p>
<p style="margin: 0;">[[Description-2]]</p>
<p style="margin:0;">[[Description-3]]</p>
</div>
<p style="margin:0;">Meta Keywords:</p>
<p style="color: #ff0000;margin:0 0 10px 0;">[[Comma separated Keywords List]]</p>
<p style="margin:0;">TItle:</p>
<div style="color: #ff0000;">
<p style="margin: 0;">[[Primary Keyword Phrase]]</p>
<p style="margin: 0;">[[Secondary Keyword Phrase]]</p>
</div>
');

/*Template id =17 Meta Tag Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(27,'Optimizing Meta Description length conforming with SEO best practices',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Check whether the user&rsquo;s webpage has any meta tags or not.</li>
<li>Go through his website and get some keywords using our SEO Website Analysis Tool (<a href=\"http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html\" target="_blank">http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html</a>).</li>
<li>Verify all the four elements of meta tag and see whether you can make any suggestions accordingly
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li>Meta Keywords Attribute.</li>
<li>Title Tag.</li>
<li>Meta Description Attribute.</li>
<li>Meta Robots Attribute.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about your meta tags!</p>
<p style="margin: 0 0 10px 0;">Meta tags are important because they provide search engines with information about your website, such as titles, keywords, and descriptions. Your website should include meta descriptions, meta keywords, and a title.</p>
<p style="margin: 0 0 10px 0;">A good Meta description tags should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use keywords relevant to your webpage.</li>
<li>Not overly repeat keywords but tries to use multiple syntaxes.</li>
<li>Not exceed 150 characters.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good meta keyword tag should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use synonyms of the keywords.</li>
<li>Use unique keywords across different webpages.</li>
<li>Not repeat any given phrase.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good title for any webpage should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Be unique across different webpages.</li>
<li>Contain a Primary Keyword Phrase in every title of every page (if possible).</li>
<li>Include your company name towards the end of the title.</li>
</ul>
<p style="margin: 0 0 10px 0;">Our analysis of your webpage shows us that you are using the following keywords and phrases on your website:</p>
 <p style="margin: 10px 0 0 0;">Webpage URL: <span style="color: #ff0000;">[[User&rsquo;s webpage URL]]</span></p>
  <p style="margin: 0;">Keywords list: <span style="color: #ff0000;">[[Keywords list]]</span></p>
  <p style="margin: 0 0 10px 0;">Keyword Phrases: <span style="color: #ff0000;">[[Keywords Phrases]]</span></p>
<p style="margin: 0 0 10px 0;">Based off of this information, we suggest that you to use the following meta description and meta keywords.</p>
<p style="margin: 0;">Meta Descriptions:</p>
<div style="color: #ff0000; margin: 0 0 10px 0;">
<p style="margin: 0;">[[Description-1]]</p>
<p style="margin: 0;">[[Description-2]]</p>
<p style="margin:0;">[[Description-3]]</p>
</div>
<p style="margin:0;">Meta Keywords:</p>
<p style="color: #ff0000;margin:0 0 10px 0;">[[Comma separated Keywords List]]</p>
<p style="margin:0;">TItle:</p>
<div style="color: #ff0000;">
<p style="margin: 0;">[[Primary Keyword Phrase]]</p>
<p style="margin: 0;">[[Secondary Keyword Phrase]]</p>
</div>
');


/*Template id =18 Meta Tag Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(27,'Creating Meta Description length conforming with SEO best practices',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Check whether the user&rsquo;s webpage has any meta tags or not.</li>
<li>Go through his website and get some keywords using our SEO Website Analysis Tool (<a href=\"http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html\" target="_blank">http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html</a>).</li>
<li>Verify all the four elements of meta tag and see whether you can make any suggestions accordingly
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li>Meta Keywords Attribute.</li>
<li>Title Tag.</li>
<li>Meta Description Attribute.</li>
<li>Meta Robots Attribute.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about your meta tags!</p>
<p style="margin: 0 0 10px 0;">Meta tags are important because they provide search engines with information about your website, such as titles, keywords, and descriptions. Your website should include meta descriptions, meta keywords, and a title.</p>
<p style="margin: 0 0 10px 0;">A good Meta description tags should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use keywords relevant to your webpage.</li>
<li>Not overly repeat keywords but tries to use multiple syntaxes.</li>
<li>Not exceed 150 characters.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good meta keyword tag should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use synonyms of the keywords.</li>
<li>Use unique keywords across different webpages.</li>
<li>Not repeat any given phrase.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good title for any webpage should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Be unique across different webpages.</li>
<li>Contain a Primary Keyword Phrase in every title of every page (if possible).</li>
<li>Include your company name towards the end of the title.</li>
</ul>
<p style="margin: 0 0 10px 0;">Our analysis of your webpage shows us that you are using the following keywords and phrases on your website:</p>
 <p style="margin: 10px 0 0 0;">Webpage URL: <span style="color: #ff0000;">[[User&rsquo;s webpage URL]]</span></p>
  <p style="margin: 0;">Keywords list: <span style="color: #ff0000;">[[Keywords list]]</span></p>
  <p style="margin: 0 0 10px 0;">Keyword Phrases: <span style="color: #ff0000;">[[Keywords Phrases]]</span></p>
<p style="margin: 0 0 10px 0;">Based off of this information, we suggest that you to use the following meta description and meta keywords.</p>
<p style="margin: 0;">Meta Descriptions:</p>
<div style="color: #ff0000; margin: 0 0 10px 0;">
<p style="margin: 0;">[[Description-1]]</p>
<p style="margin: 0;">[[Description-2]]</p>
<p style="margin:0;">[[Description-3]]</p>
</div>
<p style="margin:0;">Meta Keywords:</p>
<p style="color: #ff0000;margin:0 0 10px 0;">[[Comma separated Keywords List]]</p>
<p style="margin:0;">TItle:</p>
<div style="color: #ff0000;">
<p style="margin: 0;">[[Primary Keyword Phrase]]</p>
<p style="margin: 0;">[[Secondary Keyword Phrase]]</p>
</div>
');

/*Template id =19 Meta Tag Generator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(27,'Creating Meta Tags conforming with SEO best practices',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Check whether the user&rsquo;s webpage has any meta tags or not.</li>
<li>Go through his website and get some keywords using our SEO Website Analysis Tool (<a href=\"http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html\" target="_blank">http://www.lxrmarketplace.com/seo-webpage-analysis-tool.html</a>).</li>
<li>Verify all the four elements of meta tag and see whether you can make any suggestions accordingly
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li>Meta Keywords Attribute.</li>
<li>Title Tag.</li>
<li>Meta Description Attribute.</li>
<li>Meta Robots Attribute.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Hi! Thanks for your question about your meta tags!</p>
<p style="margin: 0 0 10px 0;">Meta tags are important because they provide search engines with information about your website, such as titles, keywords, and descriptions. Your website should include meta descriptions, meta keywords, and a title.</p>
<p style="margin: 0 0 10px 0;">A good Meta description tags should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use keywords relevant to your webpage.</li>
<li>Not overly repeat keywords but tries to use multiple syntaxes.</li>
<li>Not exceed 150 characters.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good meta keyword tag should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Use synonyms of the keywords.</li>
<li>Use unique keywords across different webpages.</li>
<li>Not repeat any given phrase.</li>
</ul>
<p style="margin: 0 0 10px 0;">A good title for any webpage should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Be unique across different webpages.</li>
<li>Contain a Primary Keyword Phrase in every title of every page (if possible).</li>
<li>Include your company name towards the end of the title.</li>
</ul>
<p style="margin: 0 0 10px 0;">Our analysis of your webpage shows us that you are using the following keywords and phrases on your website:</p>
 <p style="margin: 10px 0 0 0;">Webpage URL: <span style="color: #ff0000;">[[User&rsquo;s webpage URL]]</span></p>
  <p style="margin: 0;">Keywords list: <span style="color: #ff0000;">[[Keywords list]]</span></p>
  <p style="margin: 0 0 10px 0;">Keyword Phrases: <span style="color: #ff0000;">[[Keywords Phrases]]</span></p>
<p style="margin: 0 0 10px 0;">Based off of this information, we suggest that you to use the following meta description and meta keywords.</p>
<p style="margin: 0;">Meta Descriptions:</p>
<div style="color: #ff0000; margin: 0 0 10px 0;">
<p style="margin: 0;">[[Description-1]]</p>
<p style="margin: 0;">[[Description-2]]</p>
<p style="margin:0;">[[Description-3]]</p>
</div>
<p style="margin:0;">Meta Keywords:</p>
<p style="color: #ff0000;margin:0 0 10px 0;">[[Comma separated Keywords List]]</p>
<p style="margin:0;">TItle:</p>
<div style="color: #ff0000;">
<p style="margin: 0;">[[Primary Keyword Phrase]]</p>
<p style="margin: 0;">[[Secondary Keyword Phrase]]</p>
</div>
');


/*Template id =20 Social Media Analyzer*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(29,'Researching relevant Hashtags for Twitter',
'<ul style="list-style-type: disc; margin: 0 0 0 40px; padding: 0;">
<li>Go through user’s website.</li>
<li>Understand the high-level keywords using Website Analysis tool.</li>
<li>Search for relevant hashtags
<ul style="list-style-type: circle; margin: 0 0 0 30px; padding: 0;">
<li>Tips, Hacks, Chat etc.</li>
<li>influencer, Competitors, followers etc.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thank you for your question on hashtags.</p>
<p style="margin: 0 0 10px 0;">A hashtag is used to index keywords or topics on Twitter, and was created to allow people to easily follow topics that they are interested in.</p>
<p style="margin: 0 0 10px 0;">What hashtags should your social media account be using?</p>
<p style="margin: 0 0 10px 0;">First, search your keywords on Twitter. Add on extensions such as &ldquo;chat&rdquo;, &ldquo;tips&rdquo;, &ldquo;hacks&rdquo;, or &ldquo;trends&rdquo; with your keywords to expand your keyword base to find out what types of keywords your customers are following.</p>
<p style="margin: 0 0 10px 0;">You are currently targeting these keywords on your website:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;color:#ff0000">
<li>[[Keyword List]]</li>
</ul>
<p style="margin: 0 0 10px 0;">Based off of these keywords, you should be using the following hashtags</p>
<ul style="margin: 0 0 10px 40px; padding: 0;color:#ff0000">
<li>[[List of Hashtags]]</li>
</ul>
<p style="margin: 0 0 10px 0;">Next, follow influencers, competitors, and prospective customers on Twitter and use the hashtags that they are using.</p>
<p style="margin: 0 0 10px 0;">Based off of your competitors, we believe that the following hashtags would be relevant to you:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;color:#ff0000">
<li>[[List of Relevant Hashtags]]</li>
</ul>');

/*Template id =21 Social Media Analyzer*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(29,'Providing/auditing Bio of Twitter',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Go through user’s website.</li>
<li>Understand the high-level keywords using Website Analysis tool.</li>
<li>Use the provided bio suggestions below.</li>
<li>Audit the bio for address and keywords, and compare the same with competitors</li>
</ul>',
'<p style="margin: 0 0 10px 0;">You have 160 characters to tell your business&rsquo; story. Let people know what makes your business special and why they should follow you.</p>
<p style="margin: 0 0 10px 0;">Keep the information in your bio updated so that it always reflects the current state of your business. You should include useful information such as your location or your business hours and a link to your website.</p>
<p style="margin: 0 0 10px 0;">Your bio should:</p>
<ul style="margin: 0 0 10px 40px; padding: 0;">
<li>Contain keywords.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website contains the following keywords.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li span style="color: #ff0000;">[[Keyword Set]].</li>
</ul>
</li>
<li>They should be prevalent in your Twitter bio as well.</li>
</ul>
</li>
<li>Personify your brand.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Include the following branded keywords
<ul style="margin: 0 0 0 40px; padding: 0;">
<li  style="color: #ff0000;">[[Branded Keywords Set]].</li>
</ul>
</li>
</ul>
</li>
<li>Accurately explain who you are and what your business is
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Avoid using misleading text
<ul style="margin: 0 0 0 40px; padding: 0;">
<li style="color: #ff0000;">[[Look for issues and misleading text. Eg: ecommerce site that does not include product offerings]].</li>
</ul>
</li>
</ul>
</li>
</ul>');

/*Template id =22 Social Media Analyzer*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(29,'Strategy to increase Twitter Followers',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Go through user’s website.</li>
<li>Understand the high-level keywords using Website analysis tool.</li>
<li>Use the following ways to audit user’s Twitter handle.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">There is no clear-cut way to increase your Twitter followers, and a lot of these techniques will be trial and error. It is important to make sure to try as many of these techniques.</p>
<ol style="margin: 0 0 0 40px; padding: 0;">
<li>Make sure your account is in tip-top shape!
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>Make sure your username and profile photo are unique to your business and accurately describe who you are and what you do. Make sure your bio is clean and concise, but tells your business&rsquo; story. Keep your bio updated with any changes that may be happening in your business.</li>
</ol>
</li>
<li>Use relevant hashtags
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>Using hashtags that are relevant and popular will help get your business out in front of more people, therefore increasing the likelihood of a follow. Also keep an eye on what hashtags are currently trending and hop on them if it can be relevant to your business.</li>
</ol>
</li>
<li>Create interesting and enticing content
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>Keeping your content relevant to your business, make sure that it is interesting, enticing, and above all else sharable. Make content that people will want to retweet and share with their friends who may otherwise not have known your business exists.</li>
</ol>
</li>
<li>Follow influencers, competitors, and prospective customers
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>When you follow accounts on Twitter, they will get a notification about your follow. This will alert them to your account, and may drive traffic to your page. As long as your bio and account accurately represents your business and your content is enticing to them, you should be able to get a few new followers. In a few days, if you do not receive a follow from the accounts that you followed, you can go back in and unfollow them.</li>
</ol>
</li>
</ol>
');

/*Template id =23 Social Media Analyzer*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(29,'Providing/auditing Bio of Facebook',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Go through user’s website.</li>
<li>Understand the high-level keywords using Website Analysis tool.</li>
<li>Use the provided bio suggestions below.</li>
<li>Audit the bio for address, CTAs, and keywords, and compare the same with competitors.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thank you for your question regarding your Facebook bio.</p>
<p style="margin: 0 0 10px 0;">We have analyzed your account and we suggest the following.</p>
<ol>
<li>Share your value proposition
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>You are trying to engage prospects and influencers to gain followers, likes, and comments. Your bio should tell them what your company does. Using keywords from your website is the best way to accomplish this. You should include the following keywords:
<ol style="margin: 0 0 0 40px; padding: 0; list-style-type: lower-roman;color: #ff0000;">
<li>[[Keyword Set]]</li>
</ol>
</li>
</ol>
</li>
<li>Use Calls to Action
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>A call to action further engages your audience with special offers for a free trial, free taste, free download, etc. We suggest using the following calls to action:
<ol style="margin: 0 0 0 40px; padding: 0; list-style-type: lower-roman;color: #ff0000;">
<li>[[Call to Action 1]]</li>
<li>[[Call to Action 2]]</li>
</ol>
</li>
</ol>
</li>
<li>Include links
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>Include a link in your bio to either your homepage or a specific landing page with targeted messaging for your followers. However, keep the linking to a minimum.
<ol style="margin: 0 0 0 40px; padding: 0; list-style-type: lower-roman;color: #ff0000;">
<li>[[Audit users&rsquo;s Bio for appropriate links. Links can be to webpages, major campaigns etc..]]</li>
</ol>
</li>
</ol>
</li>
<li>Maximize the allowed length
<ol style="margin: 0 0 0 40px; padding: 0px; list-style-type: lower-alpha;">
<li>Use keywords and branded keywords frequently. Branded keywords may be something like &ldquo;official Facebook of [[Company]]&rdquo; or can be words like &ldquo;company product/brand&rdquo;
<ol style="margin: 0 0 0 40px; padding: 0; list-style-type: lower-roman;color: #ff0000;">
<li>[[Audit whether user is using the Bio length properly or not]]</li>
</ol>
</li>
</ol>
</li>
</ol>');


/*Template id =24 Top Ranked Websites by Keyword*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(30,'Auditing high ranked competitor webpages for relevent keywords',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>List of top ranking websites for the user&rsquo;s keyword and country
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Top Ranking website for keyword tool.</li>
<li>Use Google and Bing (If required -- when the keywords are generic, the top ten websites provided the tool may not be accurate. Search for competitors based on the industry vertical).</li>
</ul>
</li>
<li>Do a comparative study on the type of traffic coming to user&rsquo;s webpage and to the competition by searching webpages on similarweb.com.</li>
<li>Do on-page SEO analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Compare user&rsquo;s webpage against competition using Competitor Analysis Tool.</li>
<li>Also, use GTMetrix to get certain insights on the webpage.</li>
</ul>
</li>
<li>Do off-page SEO (majorly Link Building) analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Backlinks tool to get backlinks for both user&rsquo;s webpage and also to competition.</li>
<li>Analyze the data based on the pagerank and domain authority of backlinking websites and make relevant suggestions.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thanks for your question regarding your competitors&rsquo; website keywords.</p>
<p style="margin: 0 0 10px 0;">Based off of our analysis, your top competitors for the keyword [[Keyword]] are the following:</p>
<ul style="margin: 0 0 10px 40px; padding: 0; color: #ff0000;">
<li>[[Competitor-1]]</li>
<li>[[Competitor-2]]</li>
<li>[[Competitor-3]]&hellip;</li>
</ul>
<p style="margin: 0 0 10px 0;">This is how you rank against your competition.</p>
<p style="margin: 0 0 10px 0; color: #ff0000;">[[ Do comparative study for on the user&rsquo;s website and competitors using our Competitor Analysis Tool. Provide data in the following format]].</p>
<table class="addedTable" style="width: 800px;" border="2" cellspacing="0.5" cellpadding="0.5" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7208-f174-cf45-d5e0845b6f7c" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 1</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-0f72-33cf-37e3237604a1" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 2</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-2b0d-0273-e17b30f8641b" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 3</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-450c-3481-d73e3c016a66" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">User\'s\' Website</span></td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-625a-596b-7450d233c79f" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Page Size</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-7eff-c286-aa7bc07bf5ca" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Word count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-9676-9124-b70756bb1b6e" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Image count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-ad7a-895f-0d9934cb28d9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-cbeb-3d8a-8e2437d119fb" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Flash usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-f048-4ce2-5f94e712b4be" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Frames Usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-11d8-27f9-6bf4c3dc5429" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Text/Html Ratio</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-3039-8e71-67268154bee9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Headings (h1, h2, h3, h4, h5, h6)</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-537b-95d1-89594d6a10e8" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Favicon</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-6fdf-4db4-849c3fc0b313" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Language</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-86c5-5cf8-22f705301c63" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Load Time</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-a437-7d11-2b7f17551a3b" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">WWW Resolve</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-cc88-e090-20d5d1314279" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Title tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-f7d9-dbbe-8ad09651e019" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Description tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-37c9-9d9d-a1aa4f167720" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Keywords tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-5098-5dbc-1fc1806b1968" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-6765-ea95-a4c25ccbc8b0" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Body text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-91c7-cc5b-4f181f9478e6" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Alt tags</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-a976-6c11-f99b4e1e82f3" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p style="margin: 10px 0;">Based off of our analysis, we noticed that you have a great ranking in <span style="color: #ff0000;">[[anything that ranks in the number 1 spot if available]]</span> need to spend some time fixing <span style="color: #ff0000;">[[anything that ranks in the 3 or 4 spot]]</span></p>');

/*Template id =25 Weekly Keyword Rank Checker*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(10,'Auditing competitor webpages for relevent keywords',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>List of top ranking websites for the user&rsquo;s keyword and country
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Top Ranking website for keyword tool.</li>
<li>Use Google and Bing (If required -- when the keywords are generic, the top ten websites provided the tool may not be accurate. Search for competitors based on the industry vertical).</li>
</ul>
</li>
<li>Do a comparative study on the type of traffic coming to user&rsquo;s webpage and to the competition by searching webpages on similarweb.com.</li>
<li>Do on-page SEO analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Compare user&rsquo;s webpage against competition using Competitor Analysis Tool.</li>
<li>Also, use GTMetrix to get certain insights on the webpage.</li>
</ul>
</li>
<li>Do off-page SEO (majorly Link Building) analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Backlinks tool to get backlinks for both user&rsquo;s webpage and also to competition.</li>
<li>Analyze the data based on the pagerank and domain authority of backlinking websites and make relevant suggestions.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thanks for your question regarding your competitors&rsquo; website keywords.</p>
<p style="margin: 0 0 10px 0;">Based off of our analysis, your top competitors for the keyword [[Keyword]] are the following:</p>
<ul style="margin: 0 0 10px 40px; padding: 0; color: #ff0000;">
<li>[[Competitor-1]]</li>
<li>[[Competitor-2]]</li>
<li>[[Competitor-3]]&hellip;</li>
</ul>
<p style="margin: 0 0 10px 0;">This is how you rank against your competition.</p>
<p style="margin: 0 0 10px 0; color: #ff0000;">[[ Do comparative study for on the user&rsquo;s website and competitors using our Competitor Analysis Tool. Provide data in the following format]].</p>
<table class="addedTable" style="width: 800px;" border="2" cellspacing="0.5" cellpadding="0.5" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7208-f174-cf45-d5e0845b6f7c" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 1</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-0f72-33cf-37e3237604a1" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 2</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-2b0d-0273-e17b30f8641b" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 3</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-450c-3481-d73e3c016a66" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">User\'s\' Website</span></td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-625a-596b-7450d233c79f" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Page Size</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-7eff-c286-aa7bc07bf5ca" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Word count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-9676-9124-b70756bb1b6e" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Image count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-ad7a-895f-0d9934cb28d9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-cbeb-3d8a-8e2437d119fb" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Flash usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-f048-4ce2-5f94e712b4be" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Frames Usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-11d8-27f9-6bf4c3dc5429" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Text/Html Ratio</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-3039-8e71-67268154bee9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Headings (h1, h2, h3, h4, h5, h6)</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-537b-95d1-89594d6a10e8" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Favicon</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-6fdf-4db4-849c3fc0b313" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Language</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-86c5-5cf8-22f705301c63" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Load Time</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-a437-7d11-2b7f17551a3b" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">WWW Resolve</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-cc88-e090-20d5d1314279" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Title tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-f7d9-dbbe-8ad09651e019" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Description tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-37c9-9d9d-a1aa4f167720" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Keywords tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-5098-5dbc-1fc1806b1968" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-6765-ea95-a4c25ccbc8b0" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Body text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-91c7-cc5b-4f181f9478e6" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Alt tags</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-a976-6c11-f99b4e1e82f3" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p style="margin: 10px 0;">Based off of our analysis, we noticed that you have a great ranking in <span style="color: #ff0000;">[[anything that ranks in the number 1 spot if available]]</span> need to spend some time fixing <span style="color: #ff0000;">[[anything that ranks in the 3 or 4 spot]]</span></p>');



/*Template id =26 Competitor Analysis*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(7,'Auditing competitor webpages for relevent keywords',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>List of top ranking websites for the user&rsquo;s keyword and country
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Top Ranking website for keyword tool.</li>
<li>Use Google and Bing (If required -- when the keywords are generic, the top ten websites provided the tool may not be accurate. Search for competitors based on the industry vertical).</li>
</ul>
</li>
<li>Do a comparative study on the type of traffic coming to user&rsquo;s webpage and to the competition by searching webpages on similarweb.com.</li>
<li>Do on-page SEO analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Compare user&rsquo;s webpage against competition using Competitor Analysis Tool.</li>
<li>Also, use GTMetrix to get certain insights on the webpage.</li>
</ul>
</li>
<li>Do off-page SEO (majorly Link Building) analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Backlinks tool to get backlinks for both user&rsquo;s webpage and also to competition.</li>
<li>Analyze the data based on the pagerank and domain authority of backlinking websites and make relevant suggestions.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thanks for your question regarding your competitors&rsquo; website keywords.</p>
<p style="margin: 0 0 10px 0;">Based off of our analysis, your top competitors for the keyword [[Keyword]] are the following:</p>
<ul style="margin: 0 0 10px 40px; padding: 0; color: #ff0000;">
<li>[[Competitor-1]]</li>
<li>[[Competitor-2]]</li>
<li>[[Competitor-3]]&hellip;</li>
</ul>
<p style="margin: 0 0 10px 0;">This is how you rank against your competition.</p>
<p style="margin: 0 0 10px 0; color: #ff0000;">[[ Do comparative study for on the user&rsquo;s website and competitors using our Competitor Analysis Tool. Provide data in the following format]].</p>
<table class="addedTable" style="width: 800px;" border="2" cellspacing="0.5" cellpadding="0.5" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7208-f174-cf45-d5e0845b6f7c" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 1</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-0f72-33cf-37e3237604a1" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 2</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-2b0d-0273-e17b30f8641b" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 3</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-450c-3481-d73e3c016a66" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">User\'s\' Website</span></td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-625a-596b-7450d233c79f" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Page Size</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-7eff-c286-aa7bc07bf5ca" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Word count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-9676-9124-b70756bb1b6e" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Image count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-ad7a-895f-0d9934cb28d9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-cbeb-3d8a-8e2437d119fb" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Flash usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-f048-4ce2-5f94e712b4be" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Frames Usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-11d8-27f9-6bf4c3dc5429" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Text/Html Ratio</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-3039-8e71-67268154bee9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Headings (h1, h2, h3, h4, h5, h6)</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-537b-95d1-89594d6a10e8" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Favicon</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-6fdf-4db4-849c3fc0b313" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Language</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-86c5-5cf8-22f705301c63" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Load Time</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-a437-7d11-2b7f17551a3b" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">WWW Resolve</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-cc88-e090-20d5d1314279" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Title tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-f7d9-dbbe-8ad09651e019" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Description tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-37c9-9d9d-a1aa4f167720" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Keywords tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-5098-5dbc-1fc1806b1968" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-6765-ea95-a4c25ccbc8b0" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Body text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-91c7-cc5b-4f181f9478e6" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Alt tags</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-a976-6c11-f99b4e1e82f3" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p style="margin: 10px 0;">Based off of our analysis, we noticed that you have a great ranking in <span style="color: #ff0000;">[[anything that ranks in the number 1 spot if available]]</span> need to spend some time fixing <span style="color: #ff0000;">[[anything that ranks in the 3 or 4 spot]]</span></p>');


/*Template id =27 Competitor Webpage Monitor*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(9,'Auditing competitor webpages for their SEO activities',
'<ul style="margin: 0 0 0 40px; padding: 0;">
<li>List of top ranking websites for the user&rsquo;s keyword and country
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Top Ranking website for keyword tool.</li>
<li>Use Google and Bing (If required -- when the keywords are generic, the top ten websites provided the tool may not be accurate. Search for competitors based on the industry vertical).</li>
</ul>
</li>
<li>Do a comparative study on the type of traffic coming to user&rsquo;s webpage and to the competition by searching webpages on similarweb.com.</li>
<li>Do on-page SEO analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Compare user&rsquo;s webpage against competition using Competitor Analysis Tool.</li>
<li>Also, use GTMetrix to get certain insights on the webpage.</li>
</ul>
</li>
<li>Do off-page SEO (majorly Link Building) analysis:
<ul style="margin: 0 0 0 40px; padding: 0;list-style-type: circle;">
<li>Use the Backlinks tool to get backlinks for both user&rsquo;s webpage and also to competition.</li>
<li>Analyze the data based on the pagerank and domain authority of backlinking websites and make relevant suggestions.</li>
</ul>
</li>
</ul>',
'<p style="margin: 0 0 10px 0;">Thanks for your question regarding your competitors&rsquo; website keywords.</p>
<p style="margin: 0 0 10px 0;">Based off of our analysis, your top competitors for the keyword [[Keyword]] are the following:</p>
<ul style="margin: 0 0 10px 40px; padding: 0; color: #ff0000;">
<li>[[Competitor-1]]</li>
<li>[[Competitor-2]]</li>
<li>[[Competitor-3]]&hellip;</li>
</ul>
<p style="margin: 0 0 10px 0;">This is how you rank against your competition.</p>
<p style="margin: 0 0 10px 0; color: #ff0000;">[[ Do comparative study for on the user&rsquo;s website and competitors using our Competitor Analysis Tool. Provide data in the following format]].</p>
<table class="addedTable" style="width: 800px;" border="2" cellspacing="0.5" cellpadding="0.5" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7208-f174-cf45-d5e0845b6f7c" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 1</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-0f72-33cf-37e3237604a1" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 2</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-2b0d-0273-e17b30f8641b" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">Competitor - 3</span></td>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-450c-3481-d73e3c016a66" style="font-size: 13.3333px; font-family: Arial; color: #ff0000; vertical-align: baseline; background-color: transparent;">User\'s\' Website</span></td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-625a-596b-7450d233c79f" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Page Size</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-7eff-c286-aa7bc07bf5ca" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Word count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-9676-9124-b70756bb1b6e" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Image count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-ad7a-895f-0d9934cb28d9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-cbeb-3d8a-8e2437d119fb" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Flash usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-7209-f048-4ce2-5f94e712b4be" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Frames Usage</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-11d8-27f9-6bf4c3dc5429" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Text/Html Ratio</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-3039-8e71-67268154bee9" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Headings (h1, h2, h3, h4, h5, h6)</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-537b-95d1-89594d6a10e8" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Favicon</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-6fdf-4db4-849c3fc0b313" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Language</span>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-86c5-5cf8-22f705301c63" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Load Time</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-a437-7d11-2b7f17551a3b" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">WWW Resolve</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-cc88-e090-20d5d1314279" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Title tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720a-f7d9-dbbe-8ad09651e019" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Description tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-37c9-9d9d-a1aa4f167720" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Keywords tag</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-5098-5dbc-1fc1806b1968" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link count</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-6765-ea95-a4c25ccbc8b0" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Body text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-91c7-cc5b-4f181f9478e6" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Alt tags</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;<span id="docs-internal-guid-c33f0fdf-720b-a976-6c11-f99b4e1e82f3" style="font-size: 13.3333px; font-family: Arial; color: #000000; vertical-align: baseline; background-color: transparent;">Link text</span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p style="margin: 10px 0;">Based off of our analysis, we noticed that you have a great ranking in <span style="color: #ff0000;">[[anything that ranks in the number 1 spot if available]]</span> need to spend some time fixing <span style="color: #ff0000;">[[anything that ranks in the 3 or 4 spot]]</span></p>');



/*Template id =28 SEO Webpage Analysis*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(8,'Auditing on-page SEO factors for my website',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>
<p>On Page Factors to Analyze:</p>
</li>
<ul style="margin: 0 0 0 40px; padding: 0px; list-style-type: circle;">
<li>Titles.</li>
<li>Meta-Descriptions.</li>
<li>Headers.
</li>
</ul>
</ul>',
'<p style="margin: 0 0 10px 0;">Thank you for your question regarding your on-page SEO factors.</p>
<p style="margin: 0 0 10px 0;">We have analyzed the top 500 webpages under your domain. Attached to this message you will find a complete breakdown of individual sections. Here are some of our recommendations.</p>
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Titles
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You have set up titles on all of your pages.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The average title length on your page is [[longer than 60 characters] or [shorter than 50 characters] or [between 50 to 60 characters]]. The optimal length for titles is 50 to 60 characters.</li>
</ul>
</li>
<li>[or] You have not set up titles on all of your pages.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The optimal length for titles is 50 to 60 characters.</li>
</ul>
</li>
<li>Your titles are unique [or] Your titles are not unique.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The majority of your pages have unique titles [or] The majority of your pages do not have unique titles.</li>
</ul>
</li>
<li><span style="color: #ff0000;">[[#_of_non-unique titles]]</span> pages do not have unique titles. Update the titles to be descriptive of their respective pages.</li>
</ul>
</li>
<li>Meta-Descriptions
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You have non-relevant and duplicate meta-descriptions
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Update the meta-descriptions on the followingwebpages to increase their relevancy to the landing page
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[Webpage 1]]</li>
<li>[[Webpage 2]]</li>
<li>[[Webpage 3]]</li>
</ul>
</li>
<li>[or] Your meta-descriptions are relevant and do not seem to be duplicated. You do not have to make any changes to your meta-descriptions.</li>
</ul>
</li>
<li>You have pages that are missing meta descriptions.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Add meta descriptions to the following pages.
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[Webpage 1]]</li>
<li>[[Webpage 2]]</li>
<li>[[Webpage 3]]</li>
</ul>
</li>
</ul>
</li>
<li>[or] All of your pages contain meta-descriptions. You do not have to add any meta-descriptions.</li>
</ul>
</li>
<li>Header Tags
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The header tag <span style="color: #ff0000;">[[header_tag_text]]</span> is appearing frequently. We suggest updating the text if possible to avoid repetition.</li>
<li>[or] Your website has related and varied header tags. You do not need to make any changes.</li>
</ul>
</li>
<li>Website Structure
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website has [or] does not have category specific pages.</li>
<li>Your category pages have [or] do not have a focused structure.</li>
</ul>
</li>
<li>Canonicalization
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You need to resolve the following URLs into one URL structure.
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[URL group 1]]</li>
<li>[[URL group 2]]</li>
<li>[[URL group 3]]</li>
</ul>
</li>
<li>[or] You have a good selection of URLs and they do not need to be resolved or combined.</li>
</ul>
</li>
<li>URL Structure
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>It looks as though your URLs have a good structure and do not need to be changed.</li>
<li>[or] Your URLs do not have a proper structure. Please make the following changes:
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Do not use a string of numbers or symbols.</li>
<li>Use keywords to properly call out the name of pages.</li>
<li>Use a hyphen (-) instead of an underscore (_) to indicate spaces.</li>
<li>Limit the title to two or three key words.</li>
</ul>
</li>
</ul>
</li>
<li>Images
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Images should be under 100kb. You currently have (#_of_images) over 100kb. Try compressing photos if possible.</li>
<li>Make sure that all of your photos have an ALT tag assigned to them.</li>
</ul>
</li>
<li>Website Load Speed
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website page speed is [[speed]]</li>
<li>Your JavaScript or CSS may be slowing down parts of your website. Check these pages to make sure they are not slowing down your website.</li>
</ul>
</li>
<li>Sitemap
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website [has <em>or</em> does not have]] a well formatted sitemap.</li>
<li>Edit or build your sitemap on<a href="\&quot;http://www.lxrmarketplace.com/seo-sitemap-builder-tool.html\&quot;"> LXRMarketplace</a>.</li>
</ul>
</li>
<li>Mobile Optimization
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Yourwebpage is [or] is not mobile optimized
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Google ranks mobile optimized sites higher than those that are not optimized.</li>
<li>[If not] Optimize your website to increase your search engine ranking as well as your page visits and conversions.</li>
</ul>
</li>
</ul>
</li>
</ul>'
);

/*Template id =29 Keyword Combinator*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(2,'Audit keyword set for profitability',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>Get the column1, column2 and column3 values from user inputs.</li>
<li>Rerun the Keyword combinator tool.</li>
<li>Use Google AdWords keyword suggestions and reorder the keyword set on decreasing order of monthly searches and also in decreasing order of CPC.</li>
<li>If possible split the keyword set into four quadrants.
<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>High Search Volume &amp; Low CPC.</li>
<li>High Search Volume &amp; High CPC.</li>
<li>Low Search Volume &amp; Low CPC.</li>
<li>Low Search Volume &amp; High CPC.</li>
</ul>
</li>
<li>Suggested list should be in the above order as well.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">As per monthly searches and estimated CPC by Google, we rank your keywords into following four quadrants.</p>
<ul style="margin: 0 0 10px 40px; padding: 0px;">
<li>High Search Volume &amp; Low CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 1]]</li>
</ul>
</li>
<li>High Search Volume &amp; High CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 2]]</li>
</ul>
</li>
<li>Low Search Volume &amp; Low CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 4]]</li>
</ul>
</li>
<li>Low Search Volume &amp; High CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 3]]</li>
</ul>
</li>
</ul>
<p style="margin: 0 0 10px 0;">We suggest you to bid on same order as it is given above.</p>'
);

/*Template id =30 Keyword Performance Analyzer*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(4,'Audit non performing keywords for profitability',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>Get the column1, column2 and column3 values from user inputs.</li>
<li>Rerun the Keyword combinator tool.</li>
<li>Use Google AdWords keyword suggestions and reorder the keyword set on decreasing order of monthly searches and also in decreasing order of CPC.</li>
<li>If possible split the keyword set into four quadrants.
<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>High Search Volume &amp; Low CPC.</li>
<li>High Search Volume &amp; High CPC.</li>
<li>Low Search Volume &amp; Low CPC.</li>
<li>Low Search Volume &amp; High CPC.</li>
</ul>
</li>
<li>Suggested list should be in the above order as well.</li>
</ul>',
'<p style="margin: 0 0 10px 0;">As per monthly searches and estimated CPC by Google, we rank your keywords into following four quadrants.</p>
<ul style="margin: 0 0 10px 40px; padding: 0px;">
<li>High Search Volume &amp; Low CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 1]]</li>
</ul>
</li>
<li>High Search Volume &amp; High CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 2]]</li>
</ul>
</li>
<li>Low Search Volume &amp; Low CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 4]]</li>
</ul>
</li>
<li>Low Search Volume &amp; High CPC
<ul style="margin: 0 0 0 40px; padding: 0px;color: #ff0000;">
<li>[[Keyword List - 3]]</li>
</ul>
</li>
</ul>
<p style="margin: 0 0 10px 0;">We suggest you to bid on same order as it is given above.</p>'
);



/*Template id =31 Most Used Keyword Tool*/
insert into lxrm_ate_answer_templates(tool_id,question,steps,answer) 
values(34,'Auditing on-page SEO factors for my website',
'<ul style="margin: 0 0 0 40px; padding: 0px;">
<li>
<p>On Page Factors to Analyze:</p>
</li>
<ul style="margin: 0 0 0 40px; padding: 0px; list-style-type: circle;">
<li>Titles.</li>
<li>Meta-Descriptions.</li>
<li>Headers.
</li>
</ul>
</ul>',
'<p style="margin: 0 0 10px 0;">Thank you for your question regarding your on-page SEO factors.</p>
<p style="margin: 0 0 10px 0;">We have analyzed the top 500 webpages under your domain. Attached to this message you will find a complete breakdown of individual sections. Here are some of our recommendations.</p>
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Titles
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You have set up titles on all of your pages.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The average title length on your page is [[longer than 60 characters] or [shorter than 50 characters] or [between 50 to 60 characters]]. The optimal length for titles is 50 to 60 characters.</li>
</ul>
</li>
<li>[or] You have not set up titles on all of your pages.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The optimal length for titles is 50 to 60 characters.</li>
</ul>
</li>
<li>Your titles are unique [or] Your titles are not unique.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The majority of your pages have unique titles [or] The majority of your pages do not have unique titles.</li>
</ul>
</li>
<li><span style="color: #ff0000;">[[#_of_non-unique titles]]</span> pages do not have unique titles. Update the titles to be descriptive of their respective pages.</li>
</ul>
</li>
<li>Meta-Descriptions
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You have non-relevant and duplicate meta-descriptions
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Update the meta-descriptions on the following webpages to increase their relevancy to the landing page
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[Webpage 1]]</li>
<li>[[Webpage 2]]</li>
<li>[[Webpage 3]]</li>
</ul>
</li>
<li>[or] Your meta-descriptions are relevant and do not seem to be duplicated. You do not have to make any changes to your meta-descriptions.</li>
</ul>
</li>
<li>You have pages that are missing meta descriptions.
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Add meta descriptions to the following pages.
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[Webpage 1]]</li>
<li>[[Webpage 2]]</li>
<li>[[Webpage 3]]</li>
</ul>
</li>
</ul>
</li>
<li>[or] All of your pages contain meta-descriptions. You do not have to add any meta-descriptions.</li>
</ul>
</li>
<li>Header Tags
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>The header tag <span style="color: #ff0000;">[[header_tag_text]]</span> is appearing frequently. We suggest updating the text if possible to avoid repetition.</li>
<li>[or] Your website has related and varied header tags. You do not need to make any changes.</li>
</ul>
</li>
<li>Website Structure
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website has [or] does not have category specific pages.</li>
<li>Your category pages have [or] do not have a focused structure.</li>
</ul>
</li>
<li>Canonicalization
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>You need to resolve the following URLs into one URL structure.
<ul style="margin: 0 0 0 40px; padding: 0;color: #ff0000;">
<li>[[URL group 1]]</li>
<li>[[URL group 2]]</li>
<li>[[URL group 3]]</li>
</ul>
</li>
<li>[or] You have a good selection of URLs and they do not need to be resolved or combined.</li>
</ul>
</li>
<li>URL Structure
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>It looks as though your URLs have a good structure and do not need to be changed.</li>
<li>[or] Your URLs do not have a proper structure. Please make the following changes:
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Do not use a string of numbers or symbols.</li>
<li>Use keywords to properly call out the name of pages.</li>
<li>Use a hyphen (-) instead of an underscore (_) to indicate spaces.</li>
<li>Limit the title to two or three key words.</li>
</ul>
</li>
</ul>
</li>
<li>Images
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Images should be under 100kb. You currently have (#_of_images) over 100kb. Try compressing photos if possible.</li>
<li>Make sure that all of your photos have an ALT tag assigned to them.</li>
</ul>
</li>
<li>Website Load Speed
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website page speed is [[speed]]</li>
<li>Your JavaScript or CSS may be slowing down parts of your website. Check these pages to make sure they are not slowing down your website.</li>
</ul>
</li>
<li>Sitemap
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your website [has <em>or</em> does not have]] a well formatted sitemap.</li>
<li>Edit or build your sitemap on<a href="\&quot;http://www.lxrmarketplace.com/seo-sitemap-builder-tool.html\&quot;"> LXRMarketplace</a>.</li>
</ul>
</li>
<li>Mobile Optimization
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Your webpage is [or] is not mobile optimized
<ul style="margin: 0 0 0 40px; padding: 0;">
<li>Google ranks mobile optimized sites higher than those that are not optimized.</li>
<li>[If not] Optimize your website to increase your search engine ranking as well as your page visits and conversions.</li>
</ul>
</li>
</ul>
</li>
</ul>');



/********* Dec 09th 2016 Page Speed Insights **************/
 INSERT INTO tool (`id`, `name`, `description`, `img_path`, `is_free`, `tagline`, 
`cat_id`, `desc_link`, `tool_link`, `is_deleted`, `show_in_home`,`tool_date`) 
VALUES ('35', 'PageSpeed Insights', 'Quickly identify the page speed performance impact issues of a web page', 'PSI', '1', 
'Identify your web page performance.', '3','\page-speed-insights-tool?query=\'desc\'','\page-speed-insights-tool.html', '0', '1','2016-12-09');
insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,35,4.9,'','','2016-12-09 00:00:00');
/***Tool id 35 Page Speed related tools maping query ***/
insert into tools_grouping (tool_id, correlated_tool_id, tools_order) values (35,8,1),(35,31,2),(35,20,3),(35,6,4),(35,34,5);

/********* JAN 2th 2017 SEO Penalty Checker **************/
INSERT INTO `tool` (`name`,`description`,`img_path`,`is_free`,`tagline`,`cat_id`,`desc_link`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`) 
VALUES ('SEO Penalty Checker','It shows Google update had a positive or negative impact on your website.','SPC',1,
'Google Algorithm Update & Penalty Checker',3,'penalty-checker-tool?query=\'desc\'','penalty-checker-tool.html',0,1,'2017-12-24');
insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,36,5.0,'','','2017-12-09 00:00:00');
/***Tool id 35 Page Speed related tools maping query ***/
insert into tools_grouping (tool_id, correlated_tool_id, tools_order) values (36,8,1),(36,31,2),(36,20,3),(36,6,4),(36,34,5);



INSERT INTO google_updates_info (updated_date,update_type,description)

VALUES ('2013-03-14', 'Panda', 'Google announced a Panda update which would be the last update before Panda was integrated into the core algorithm. <a href="http://www.seroundtable.com/google-panda-25-16506.html" target="_blank">Google`s Final Manual Panda Refresh Here? #25</a>'),

('2013-05-22', 'Penguin', 'Google launched the 4th Penguin update. This update is more targeted towards to the page level rankings. <a href="http://searchengineland.com/penguin-4-with-penguin-2-0-generation-spam-fighting-is-now-live-160544" target="_blank">Penguin 4, With Penguin 2.0 Generation Spam-Fighting, Is Now Live</a>'),


('2013--07-18', 'Panda', 'Google launched a Panda update which might have softened previous Panda penalties. <a href="http://www.seroundtable.com/google-panda-update-17094.html" target="_blank">Confirmed: Google Panda Update: The "Softer" Panda Algorithm</a>'),

('2013-08-20', 'Hummingbird', 'Google announced that the "Hummingbird" update was rolled out. The update is made to core algorithm that affects semantic search and the Knowledge Graph. <a href="http://searchengineland.com/google-hummingbird-172816" target="_blank">All About The New Google "Hummingbird" Algorithm</a>'),

('2013-10-04', 'Penguin', 'Google launched Penguin 2.1 update. This update is majorly towards data refresh. <a href="http://searchengineland.com/penguin-2-1-and-5-live-173632" target="_blank">Penguin 5, With The Penguin 2.1 Spam-Filtering Algorithm, Is Now Live</a>'),

('2014-05-19', 'Panda', 'Google launched major update to Panda which has effects on both the algorithm update and a data refresh. <a href="http://searchengineland.com/google-begins-rolling-panda-4-0-now-192043" target="_blank">Google Begins Rolling Out Panda 4.0 Now</a>'),

('2014-06-12', 'Other', 'Google launched new update to its anti-spam algorithm. <a href="http://www.seroundtable.com/google-payday-loan-3-spam-18695.html" target="_blank">Google Spam Algorithm Version 3.0 Launches Today</a>'),

('2014-07-24', 'Pigeon', 'Google changed some local results and modified how they handle and interpret location cues. <a href="http://searchengineland.com/google-makes-significant-changes-local-search-ranking-algorithm-197778" target="_blank">Google "Pigeon" Updates Local Search Algorithm With Stronger Ties To Web Search Signal</a>'),

('2014-09-23', 'Panda', 'Google launched Panda update that had major impact on the algorithmic component. <a href="http://searchengineland.com/panda-update-rolling-204313" target="_blank">Panda 4.1 — Google’s 27th Panda Update — Is Rolling Out</a>'),

('2014-10-17', 'Penguin', 'Google launched a Penguin update which is more of data-only (not a new Penguin algorithm). <a href="http://www.hmtweb.com/marketing-blog/penguin-3-analysis-findings/" target="_blank">Penguin 3.0 Analysis – Penguin Tremors, Recoveries, Fresh Hits, and Crossing Algorithms.</a>'),

('2014-10-21', 'Other', 'Google launched another update to combat software and digital media piracy. <a href="http://blog.searchmetrics.com/us/2014/10/26/google-pirate-update-analysis-and-loser-list/" target="_blank">Google Pirate Update Analysis and Loser List</a>'),

('2014-12-10', 'Penguin', 'Google confirmed that it was moving towards continous updates to Penguin. <a href="http://searchengineland.com/google-says-penguin-shift-continuous-updates-210580" target="_blank">Google Says Penguin To Shift To "Continuous Updates"</a>'),

('2014-12-22', 'Pigeon', 'Google`s algorithm update "Pigeon" was expanded to the United Kingdom, Canada, and Australia. <a href="http://searchengineland.com/google-pigeon-update-rolls-uk-canada-australia-211576" target="_blank">Google Pigeon Update Rolls Out To UK, Canada & Australia</a>'),

('2015--04-22', 'Other', 'Google announced an algorithm update due to which mobile rankings would differ for mobile-friendly sites. <a href="http://googlewebmastercentral.blogspot.com/2015/02/finding-more-mobile-friendly-search.html" target="_blank">Finding more mobile-friendly search results</a>'),

('2015-05-03', 'Other', 'Google updated core algorithm impacting quality signals. Google hasn`t revealed any specifics about the nature of the changes made to quuality signals. <a href="http://searchengineland.com/the-quality-update-google-confirms-changing-how-quality-is-assessed-resulting-in-rankings-shake-up-221118" target="_blank">The Quality Update: Google Confirms Changing How Quality Is Assessed, Resulting In Rankings Shake-Up</a>'),

('2015-07-17', 'Panda', 'Google announced Panda 4.2 data refresh. <a href="http://www.thesempost.com/google-panda-update-everything-we-know-about-panda-4-2/" target="_blank">Google Panda Update: Everything We Know About Panda 4.2</a>'),

('2015-10-26', 'RankBrain', 'Google revealed that machine learning had been a part of its algorithm for months. These algorithm changes contribute to the 3rd most influential ranking factor. <a href="http://searchengineland.com/faq-all-about-the-new-google-rankbrain-algorithm-234440" target="_blank">All About The New Google RankBrain Algorithm</a>'),

('2016-02-23', 'Other', 'Google removed right-column ads and rolled our 4-ad top blocks. This change can have significant implications for CTR for both paid and organic results, especially on competitive keywords. <a href="http://www.thesempost.com/google-adwords-switching-to-4-ads-on-top-none-on-sidebar/" target="_blank">Google AdWords Switching to 4 Ads on Top, None on Sidebar</a>'),

('2016-05-12', 'Other', 'Google rolled out ranking signal boost to benefit mobile-friendly sites on mobile search. <a href="https://webmasters.googleblog.com/2016/03/continuing-to-make-web-more-mobile.html" target="_blank">Continuing to make the web more mobile friendly</a>'),

('2016-09-01', 'Other', 'While unconfirmed by Google, data suggests this update (or a simultaneous update) also heavily impacted organic results. <a href="http://searchengineland.com/everything-need-know-googles-possum-algorithm-update-258900" target="_blank">Everything you need to know about Google’s ‘Possum’ algorithm update</a>.'),

('2016-09-23', 'Penguin', 'Google announced a new Penguin update almost after 2 years. They suggested the new Penguin is now real-time and baked into the "core" algorithm. <a href="https://webmasters.googleblog.com/2016/09/penguin-is-now-part-of-our-core.html" target="_blank">Penguin is now part of our core algorithm</a>');

INSERT INTO google_updates_info (updated_date,update_type,description) 
VALUES ('2017-01-10', 'Other', 'Google has announced a new update in which pages where content is not easily accessible to a user on the transition from the mobile search results may not rank as high. <a href="https://webmasters.googleblog.com/2016/08/helping-users-easily-access-content-on.html" target="_blank">Intrusive Interstitial Penalty is now Live</a>');

INSERT INTO google_updates_info (updated_date,update_type,description)
VALUES ('2017-03-08', 'Fred', 'Google rolled out an update called Fred. This update would effect sites with Shallow Content, Advertising heavy and Affiliate heavy.');

/********* APR 26th 2017 Structure Data Generator **************/
INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`is_free`,`tagline`,`cat_id`,`desc_link`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`) 
VALUES (37,'Structured Data Generator','Provides a quick and easy way of generating different types of markups for your website.','SDG',1,
'Generate structured data for a website.',3,'structured-data-generator.html?query=\'desc\'','structured-data-generator.html',0,1,'2017-05-16');
insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,37,5.0,'','','2017-05-16 00:00:00');

-- 10th July, 2017 - updating the tool content(tagline and description nadroid and ios apps data).
UPDATE tool SET tagline = "Receive a quick overview on whether or not your links are working.", description = "Improve the overall effectiveness of your digital marketing campaigns.", tool_icon_color = "#89cf55" WHERE id = 1;
UPDATE tool SET tagline = "Generate keywords for your digital marketing campaigns.", description = "Easily upload to Google AdWords Editor and Bing Ads Editor.", tool_icon_color = "#9c89c7" WHERE id = 2;
UPDATE tool SET tagline = "Calculate the revenue of your digital marketing campaign.", description = "Save time and effort from complex calculations.", tool_icon_color = "#454f57", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.roi&feature=more_from_developer#?t=W251bGwsMSwyLDEwMiwiY29tLmx4cm1hcmtldHBsYWNlLnJvaSJd" WHERE id = 3;
UPDATE tool SET tagline = "Analyze the performance of your campaigns' keywords.", description = "Make time-effective campaign decisions.", tool_icon_color = "#db4321" WHERE id = 4;
UPDATE tool SET tagline = "Automatically generate sitemaps for your website.", description = "Download your sitemap or send via email.", tool_icon_color = "#e36a4f" WHERE id = 6;
UPDATE tool SET tagline = "Quickly analyze your website against your competitor's.", description = "Identify areas of improvement alongside our recommendations.", tool_icon_color = "#7587bf" WHERE id = 7;
UPDATE tool SET tagline = "Analyze your website's on-page SEO factors.", description = "Boost your on-page optimization with our recommendations.", tool_icon_color = "#706a73", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.seo&feature=search_result#?t=W251bGwsMSwyLDEsImNvbS5seHJtYXJrZXRwbGFjZS5zZW8iXQ" WHERE id = 8;
UPDATE tool SET tagline = "Track and monitor changes in competitor websites.", description = "Stay on top of your competitors with automatic email alerts.", tool_icon_color = "#1eb370" WHERE id = 9;
UPDATE tool SET tagline = "Track your keyword rankings in Google and Bing.", description = "Get weekly email alerts for your organic keyword positions.", tool_icon_color = "#ba6c98" WHERE id = 10;
UPDATE tool SET tagline = "Admin App for Android and iOS Phones, Tablets and iPads.", description = "Keep track of all vital sales and product metrics in real-time.", tool_icon_color = "#f07b22", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.dashly&hl=en", ios_app_url = "https://itunes.apple.com/us/app/dashly-for-magento/id972690172?mt=8" WHERE id = 18;
UPDATE tool SET tagline = "Examine all backlinks to your website.", description = "Analyze all backlink metrics to get higher organic rankings.", tool_icon_color = "#e38444", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.seobacklink", ios_app_url = "https://itunes.apple.com/us/app/seo-backlinks/id1005485030?ls=1&mt=8" WHERE id = 20;
UPDATE tool SET tagline = "Age and WHOIS lookup for any domain.", description = "Get a quick snapshot of all essential domain information.", tool_icon_color = "#885c94", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.domainagechecker", ios_app_url = "https://itunes.apple.com/us/app/domain-spy/id948230247?mt=8" WHERE id = 21;
UPDATE tool SET tagline = "Admin App for Android and iOS Phones, Tablets and iPads.", description = "Keep track of all vital sales and product metrics in real-time.", tool_icon_color = "#00abc9", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.bigcommerce&hl=en", ios_app_url = "https://itunes.apple.com/us/app/dashly-for-bigcommerce/id980585815?mt=8" WHERE id = 22;
UPDATE tool SET tagline = "Admin App for Android and iOS Phones, Tablets and iPads.", description = "Keep track of all vital sales and product metrics in real-time.", tool_icon_color = "#de0265", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.prestashop&hl=en", ios_app_url = "https://itunes.apple.com/us/app/dashly-for-prestashop/id1077227844?mt=8" WHERE id = 23;
UPDATE tool SET tagline = "Generate a robots.txt file for your website.", description = "Control how search engines crawl your website.", tool_icon_color = "#a18170" WHERE id = 24;
UPDATE tool SET tagline = "Validate a robots.txt file for your website.", description = "Identify and fix syntax and logical errors in robots.txt file.", tool_icon_color = "#5a8de0" WHERE id = 25;
UPDATE tool SET tagline = "Generate or edit meta tags for your website.", description = "Optimize your meta tags for better SEO.", tool_icon_color = "#88cf53" WHERE id = 27;
UPDATE tool SET tagline = "Find DNS records for a specified domain or hostname.", description = "Download a detailed report of all your DNS records information.", tool_icon_color = "#3a9ba8", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.dnschecker" WHERE id = 28;
UPDATE tool SET tagline = "Analyze social media channels for you and your competitors.", description = "Track your performance in up to 5 channels.", tool_icon_color = "#dc4220" WHERE id = 29;
UPDATE tool SET tagline = "Get a list of top ranked websites for any keyword in Google and Bing.", description = "Know the backlink count of each url and domain for every ranked website.", tool_icon_color = "#2c3427" WHERE id = 30;
UPDATE tool SET tagline = "Easily find broken links on your website.", description = "Fix your broken links and provide a better user experience.", tool_icon_color = "#f37a43" WHERE id = 31;
UPDATE tool SET tagline = "Admin App for Android and iOS Phones, Tablets and iPads.", description = "Keep track of all vital sales and product metrics in real-time.", tool_icon_color = "#1DB272", android_app_url = "https://play.google.com/store/apps/details?id=com.lxrmarketplace.woocommerce&hl=en", ios_app_url = "https://itunes.apple.com/us/app/dashly-for-woocommerce/id1046823908?mt=8" WHERE id = 33;
UPDATE tool SET tagline = "Find the most used keywords and HTML/Code ratio for your top 100 pages.", description = "Analyze and make changes to your keywords for better content optimization.", tool_icon_color = "#706a73" WHERE id = 34;
UPDATE tool SET tagline = "Measure a webpage's performance on mobile and desktop devices.", description = "Analyze the content of a web page & get suggestions to make the page faster.", tool_icon_color = "#4385f5" WHERE id = 35;
UPDATE tool SET tagline = "Check if your website is penalized by any Google algorithm updates.", description = "Take necessary steps to recover from penalty and regain your visibility.", tool_icon_color = "#AE5EC3" WHERE id = 36;
UPDATE tool SET tagline = "Get structured data markup for any website.", description = "Make your website more understandable to search engines and users.", tool_icon_color = "#2c3427" WHERE id = 37;


/********* SEP 13th 2017 User Persona Master Data **************/
INSERT INTO lxrm_user_persona_category (persona_category_name) VALUES ('Help us provide you a personalised experience');
INSERT INTO lxrm_user_persona_category (persona_category_name) VALUES ('How did you first hear about LXRMarketplace?');

INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('1', 'I am a Digital Marketing Professional');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('1', 'I am a Freelancer');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('1', 'I am a Business Owner');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('1', 'I am a Blogger');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('1', 'Other');

INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('2', 'Google (or any other search engine)');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('2', 'Facebook');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('2', 'Twitter');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('2', 'Referral');
INSERT INTO lxrm_user_persona_category_info (persona_category_id, persona_category_info_name) VALUES ('2', 'Other');

-- For Tool Advisor insertion in tool table for tracking purpose
insert into tool (id, name, description, img_path, tagline, cat_id, tool_link, is_deleted,show_in_home, tool_date) 
values (38, 'Tool Advisor', '', '', '', 4, '',0 , 0, NOW());


-- Updated IOS Mobile APP  URL's Oct 13,2017
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/seo-webpage-analysis/id1282971042?ls=1&mt=8' WHERE id=8;
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/domain-spy/id948230247?ls=1&mt=8' WHERE id=21;
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/dashly-for-magento/id972690172?ls=1&mt=8' WHERE id=18;
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/dashly-for-bigcommerce/id980585815?ls=1&mt=8' WHERE id=22;
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/dashly-for-prestashop/id1077227844?ls=1&mt=8' WHERE id=23;
UPDATE tool SET ios_app_url='https://itunes.apple.com/us/app/dashly-for-woocommerce/id1046823908?ls=1&mt=8' WHERE id=33;

-- Changed E-Commerce tool URLs (Nov 2nd, 2017)
UPDATE tool SET tool_link = 'dashly-for-magento.html' where id = 18;
UPDATE tool SET tool_link = 'dashly-for-bigcommerce.html' where id = 22;
UPDATE tool SET tool_link = 'dashly-for-prestaShop.html' where id = 23;
UPDATE tool SET tool_link = 'dashly-for-woocommerce.html' where id = 33;

UPDATE `tool` SET `cat_id`='4' WHERE `id`='0';
UPDATE `tool` SET `is_deleted`='1' WHERE `id`='32';



/********* May 30th 2018 Micro Influencer Generator **************/
INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`tagline`,`cat_id`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`,`tool_icon_color`) 
VALUES (40,'Micro Influencer Generator','Provides a quick and easy way to identify influencers from social channels.','MIG',
'Identify influencers data for your business.',3,'micro-influencer-generator.html',0,1,curdate(),'#FF7575');
insert into feedback(user_id,tool_id,rating,title,comments,feedback_date) values(2,40,5.0,'','',curdate());

INSERT INTO `oldmktplace`.`lxrm_signup_type` (`signup_type`, `signup_type_desc`) VALUES ('1', 'Facebook');
INSERT INTO `oldmktplace`.`lxrm_signup_type` (`signup_type`, `signup_type_desc`) VALUES ('2', 'Google');
INSERT INTO `oldmktplace`.`lxrm_signup_type` (`signup_type`, `signup_type_desc`) VALUES ('3', 'Manual');

/********* june 15th 2018 New tool-Woo Commerce Product feed **************/
INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`tagline`,`cat_id`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`,`tool_icon_color`) 
VALUES (39,'Woocommerce Product Feed','It generates the Product Feed in CSV and XML formats the way it suits the Shopping Engines such as Google Merchant Center in just a few clicks.','WPF',
'Product Feed Woocommerce.',6,'woocommerce-product-feed.html',0,1,curdate(),'#33799d');
/********* june 15th 2018 New tool-Feed Back **************/
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1,39,5.0,'Test','Useful Tool',curdate());


INSERT INTO `lxrm_platform` (`platform`, `platform_desc`) VALUES ('1', 'Android');
INSERT INTO `lxrm_platform` (`platform`, `platform_desc`) VALUES ('2', 'iOS');

/***Tool id 40 Micro Influencer related tools maping query ***/
insert into tools_grouping (tool_id, correlated_tool_id, tools_order) values (40,8,1),(40,20,2),(40,35,3),(40,29,4);

/*Adding new section for tools :Social Aug 10,2018*/
UPDATE tool SET `cat_id`='7' WHERE `id`='40';
UPDATE  tool SET `cat_id`='7' WHERE `id`='29';

INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`tagline`,`cat_id`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`,`tool_icon_color`) 
VALUES (41,'Amazon Product Listing Grader','Generates scores for both product urls or amazon product feed  parameters listed as per amazon standards.','APLG',
'Provides you with the improvement tips to increase the demand for your products on amazon.',7,'product-listing.html',0,1,curdate(),'#1E334E');
/********* june 15th 2018 New tool-Feed Back **************/
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1,41,5.0,'Test','Useful Tool',curdate());

/* --- Sep 13th, 2018 Google Algorithm Updates Information --- */
INSERT INTO google_updates_info (updated_date,update_type,description)
VALUES ('2017-04-25', 'Project Owl', '<a href="https://searchengineland.com/googles-project-owl-attack-fake-news-273700" target="_blank">Project Owl</a> created by Google is a change in their algorithm to fight Fake News and Problematic Content. This should allow Google to put actual facts regarding a topic above popular opinion. This update should not effect any eCommerce websites'),
('2017-10-17', 'Chrome HTTPS Warnings', 'Starting October 2017 Google has started showing <a href="https://searchengineland.com/google-emails-warnings-webmasters-chrome-will-mark-http-pages-forms-not-secure-280907" target="_blank">“Not secure”</a> warning in two additional situations: when users enter data on an HTTP page, and on all HTTP pages visited in Incognito mode. This might effect the number of Returning Visitors and also the Conversion Rates.'), ('2017-10-27', 'Featured Snippet Drop', 'This is an unconfirmed update.Over a period of a few days from October 27-31, there was a <a href="https://searchengineland.com/featured-snippet-bubble-busting-286107" target="_blank">substantial drop</a> in Featured Snippets. This co-occurred with a jump in Knowledge Panels, as Google seemed to add many panels for broad terms and objects ("travel", "toilet", "web design", etc.).'),
('2017-11-14', 'Unnamed Update', 'Algorithm trackers and webmaster chatter detected a <a href="https://www.seroundtable.com/google-algorithm-search-ranking-update-24774.html" target="_blank">high amount of flux, peaking</a> in the data around November 15 but google did not confirm an official update.'),
('2017-11-30', 'Snippet Length Increase', 'This is a confirmed update. After testing longer search snippets for over two years, Google increased them across a large number of results. This led us to adopt a <a href="https://searchengineland.com/google-officially-increases-length-snippets-search-results-287596" target="_blank">new Meta Description</a> limit upto 300 characters from the previous 155 (almost doubling).'),
('2017-12-14', '"Maccabees" Update', 'This is an unconfirmed update.This update is related to <a href="https://searchengineland.com/google-confirms-mid-december-search-ranking-algorithm-update-288682" target="_blank">keyword permutations and sites utilizing doorway pages</a>.'),
('2018-03-08', '"Brackets" Core Update', 'This is a confirmed but an unknown update. As per Google, there is no "fix" for pages that may perform less well or where the rankings have dropped, other than to remain focused on building great content. <a href="https://searchengineland.com/google-confirms-core-search-ranking-algorithm-update-293961" target="_blank">Click here to know more</a>.'),
('2018-03-14', 'Zero-result SERP Test', 'Google has come up with an experiment in which it shows zero results for specific search queries. If a user is looking for a specific answer, Google displays just the answer instead of showing 10 Organic results. <a href="http://www.thesempost.com/depth-look-googles-new-zero-result-search-results/" target="_blank">Google has closed this experiment after a week</a>.'),
('2018-03-26', 'Mobile-First Index Roll-out', 'As per Google, "Till now, <a href="https://searchengineland.com/google-begins-rolling-out-mobile-first-indexing-to-more-sites-295095" target="_blank">Google\'s crawling, indexing, and ranking systems</a> have typically used the desktop version of a page\'s content, which may cause issues for mobile searchers when that version is vastly different from the mobile version.  Mobile-first indexing means that Google will use the mobile version of the page for indexing and ranking, to better help the primarily mobile users find what they are looking for." Websites which are not mobile-friendly might see a drop in rankings.'),
('2018-04-17', 'Unnamed Core Update', 'This is a confirmed but an unamed update. It is a <a href="https://searchengineland.com/google-confirms-rolling-out-a-broad-core-search-algorithm-update-earlier-week-296600" target="_blank">broad core algorithm update</a>, as google routinely do throughout the year to improve the search results.'),
('2018-05-13', 'Snippet Length Drop', 'This is a confirmed update. After testing longer display snippets of up to 300+ characters for a few months(last dec), Google rolled back most snippets to the former limit (about 150-160 characters). If you went ahead and already lengthened your meta descriptions, there is no need to go back and shorten them now. <a href="https://searchengineland.com/google-confirms-it-shortened-search-results-snippets-after-expanding-them-last-december-298196" target="_blank">Google’s advice</a> is to not focus too much on these, as many of the snippets Google chooses are dynamic anyway and not pulled from your meta descriptions.'),
('2018-05-23', 'Unnamed Update', 'It is an unconfirmed and unnamed update by Google. Algorithm <a href="https://www.seroundtable.com/google-may-update-25790.html" target="_blank">tracking tools and webmaster chatter showed heavy activity</a>, but Google did not confirm an update.'),
('2018-06-14', 'Video Carousels ', 'This is a confirmed update. Google rolled out <a href="https://searchengineland.com/google-replacing-video-boxes-with-video-carousel-on-desktop-search-300491" target="_blank">video carousels in the desktop SERPs</a> (after having them live in the mobile SERPs for some time) causing a shake-up in results that were previously tracked as organic. At the same time, the number of SERPs with videos increased significantly.'),
('2018-07-09', 'Speed Update', 'This is an unconfirmed update. Google has begun rolling out the <a href="https://searchengineland.com/google-speed-update-is-now-being-released-to-all-users-301657" target="_blank">Google Speed Update</a> that they first announced in January 2018, making page speed a ranking factor for mobile results. And this only affected the slowest mobile sites, and there was no evidence of major mobile rankings shifts.'),
('2018-07-21', 'Unnamed Update', 'This is an unconfirmed and unamed update. <a href="https://www.seroundtable.com/google-search-algorithm-update-saturday-26087.html" target="_blank">Algorithm trackers and webmaster chatter signaled heavy rankings flux</a>, but Google did not confirm.'),
('2018-07-24', 'Chrome Security Warnings (Full Site)', 'This is a confirmed update. After warning users of unsecured (non-HTTPS) forms months earlier, Chrome 68 began marking all non-HTTPS sites as "not secure i.e. Instead of the small “i” icon for HTTP URLs, Chrome will add a “Not secure” label of text to that. It is strongly recommended to upgrade your website to <a href="https://searchengineland.com/tomorrow-chrome-starts-telling-users-http-sites-are-not-secure-302481" target="_blank">HTTPS URLs and be secure</a>, even if your site does not ask for payment information, logins or other private information'),
('2018-08-01', '"Medic" Core Update ', 'This is a confirmed update by Google. The roll-out finished around Wednesday 8th August, 2018. The <a href="https://searchengineland.com/google-confirms-broad-search-algorithm-update-is-rolling-out-302982" target="_blank">advice from Google</a> hasn\'t changed, in that there\'s nothing specific to optimise or re-optimise for. If you\'ve seen a dip coinciding with this update, and you haven\'t changed anything on your website, then it could be your competitors have done something new, or something they were already doing is now being given more credit.');


/* Oct 31,2018: Replacing woocommerce-product-feed.html to magento-product-feed.html and created the new file.
Enable the record id 39 (is_deleted to 0) 
Wref to Mail from: Updates to Magento Product Feed - Oct26,2018 */
UPDATE tool SET `name`='Magento Product Feed', `tagline`='Product Feed Magento.', `tool_link`='magento-product-feed.html', `is_deleted`='0', `tool_date`='2018-10-28',`img_path`='DFM' WHERE `id`='39';


/* --- Oct 30th, 2018 New Tool Hreflang Generator and Checker --- */
INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`tagline`,`cat_id`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`,`tool_icon_color`) 
VALUES (42, 'hreflang Language Generator & Checker','Check for and generate proper hreflang attributes for multi-language webpages.', 'HLGC',
'Makes your webpages easier to understand so search engines can serve the correct location and language version of your webpage to the user.', 3, 'hreflang-tool.html', 0, 1, curdate(), '#0051CB');
/********* june 15th 2018 New tool-Feed Back **************/
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1, 42, 5.0, 'Test', 'Useful Tool', curdate());


/* --- Nov 23th, 2018 New Tool Image Compression Tool --- */
INSERT INTO `tool` (`id`,`name`,`description`,`img_path`,`tagline`,`cat_id`,`tool_link`,`is_deleted`,`show_in_home`,`tool_date`,`tool_icon_color`) 
VALUES (43, 'Image Compression Tool','Can upload JPEG or JPG image files and download the compressed image file in the same file type originally uploaded.', 'ICT',
'Generates a compressed version of the original image file uploaded by the user.', 3, 'image-compression.html', 0, 1, curdate(), '#184F78');
/********* Nov 23th, 2018 New tool-Feed Back **************/
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1, 43, 5.0, 'Test', 'Useful Tool', curdate());

/* --- Sep 10th, 2019 AdSpy Tool --- */
INSERT INTO tool (id,name,description,img_path,tagline,cat_id,tool_link,is_deleted,show_in_home,tool_date,tool_icon_color) 
VALUES (44, 'AdSpy','Instantly uncover websites Visits, Ad Presence and Estimated Monthly Adwords Budget details through SimilarWeb and SpyFu APIs .', 'ADS',
'Instantly uncover websites Visits, Ad Presence and Estimated Monthly Adwords Budget.', 8, 'ad-spy-v2.html', 0, 0, curdate(), '#2c3427');

insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1, 44, 5.0, 'Test', 'Useful Tool', curdate());

/* --- Sep 10th, 2019 Preliminary Search Marketing Analysis Tool --- */
INSERT INTO tool (id,name,description,img_path,tagline,cat_id,tool_link,is_deleted,show_in_home,tool_date,tool_icon_color) 
VALUES (45, 'Preliminary Search Marketing Analysis','Identify the Channel Performance, Paid Search Performance, Search Traffic By Engine, Device Performance and SEO Performance Scores.', 'PSMA',
'Instantly uncover Website Domain traffic and SEO performance details through SimilarWeb and LXRSEO APIs.', 8, 'preliminary-search.html', 0, 0, curdate(), '#0051CB');
/* --- Sep 10th, 2019 Preliminary Search Marketing Analysis Tool --- */
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1, 45, 5.0, 'Test', 'Useful Tool', curdate());

/* --- Sep 10th, 2019 Domain Finder Tool --- */
INSERT INTO tool (id,name,description,img_path,tagline,cat_id,tool_link,is_deleted,show_in_home,tool_date,tool_icon_color) 
VALUES (46, 'Domain Finder','Instantly find the domain / URL for the list of company names.', 'DF',
'Instantly find the domain / URL for the list of company names', 8, 'domain-finder.html', 0, 0, curdate(), '#184F78');
/* --- Sep 10th, 2019 Domain Finder Tool --- */
insert into feedback (user_id, tool_id, rating, title, comments, feedback_date) 
values (1, 46, 5.0, 'Test', 'Useful Tool', curdate());


create table lxrm_gmail_api_keys (
	email_sno int primary key,
    email_id varchar(235) default null,
    access_token longtext default null,
    refresh_token longtext default null,
    last_updated_date timestamp
    );
INSERT INTO lxrm_gmail_api_keys (email_sno, email_id) VALUES ('1', 'support@lxrmarketplace.com');