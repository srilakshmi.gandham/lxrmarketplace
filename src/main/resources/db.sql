CREATE SCHEMA lxrmarketplace DEFAULT CHARACTER SET utf8 ;
CREATE USER lxrmarketplace IDENTIFIED BY 'lxrmarketplace';
GRANT ALL ON lxrmarketplace.* TO lxrmarketplace;

CREATE TABLE user (id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(100),
    activation_id BIGINT,
    is_active TINYINT(1)
);

--##################################### 16-NOV-2011 ######################################
ALTER TABLE user 
  ADD COLUMN is_admin TINYINT(1) NULL  AFTER is_active , 
  ADD COLUMN reg_date TIMESTAMP NULL  AFTER is_admin , 
  ADD COLUMN act_date TIMESTAMP NULL  AFTER reg_date ;
  
ALTER TABLE user 
  CHANGE COLUMN activation_id activation_id BIGINT(20) NULL DEFAULT NULL  AFTER is_admin ;

CREATE TABLE session(
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id BIGINT NOT NULL,
  start_time DATETIME,
  end_time DATETIME,
  duration DOUBLE,
  ip VARCHAR(25),
  os VARCHAR(50),
  browser VARCHAR(50),
  browser_version VARCHAR(10),
  INDEX user_id_index (user_id ASC) ,
  CONSTRAINT user_ref FOREIGN KEY (user_id) REFERENCES user (id )
);
  
CREATE  TABLE tool (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL ,
  description VARCHAR(255) NULL
);
                               
CREATE  TABLE tool_usage (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  session_id BIGINT NOT NULL ,
  tool_id BIGINT NOT NULL ,
  count INT NULL ,
  description VARCHAR(255) NULL ,
  INDEX session_id_index (session_id ASC) ,
  CONSTRAINT session_ref FOREIGN KEY (session_id) REFERENCES session (id),
  INDEX tool_id_index (tool_id ASC) ,
  CONSTRAINT tool_ref FOREIGN KEY (tool_id) REFERENCES tool (id)
);



--**************************************november 29 ******************************--

alter table user
   modify column is_admin TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
   modify column is_active  TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;

--************************************Table for cpc ***************************--
create table lxr_mkpl_cpc_index (id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
date date,
adwords_wtcpc DOUBLE(4,2),
bing_wtcpc DOUBLE(4,2),
CONSTRAINT MyUniqueKey UNIQUE (date));

--**********************************for Amazon price Analyzer and enhancements  ********************--
ALTER TABLE user 
  ADD COLUMN domain_name varchar(255);

create table task(
    task_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT(20),
    domain_name VARCHAR(255),
    created_time_stamp DATETIME,
    updated_time_stamp DATETIME,
    status VARCHAR(255),
    result_file VARCHAR(255)
);

create table product(
     product_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     user_id BIGINT(20),
     product_name VARCHAR(255),
     is_Deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0
);

create table result(
     result_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     user_id BIGINT(20),
     task_id BIGINT(20),
     product_id BIGINT(20),
     product_name VARCHAR(255),
     client_price DOUBLE(10,2),
     competitor_name VARCHAR(255),
     competitor_price DOUBLE(10,2),
     remark_id BIGINT(20),
     result_date DATETIME
);

create table user_options(
id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
user_id BIGINT(20),
tool_id BIGINT(20),
scheduler_isenabled  TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
scheduled_day  BIGINT(20),
user_domain  VARCHAR(255));

ALTER TABLE tool
 add column img_path VARCHAR(255);
 
alter table session  modify column duration double NOT NULL DEFAULT 0.00; 

--******************** March 5 : Supporting auto mail option *****************************--
CREATE TABLE mail_templates(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    temp_name VARCHAR(255) NOT NULL,
    status TINYINT(1) NOT NULL DEFAULT 0,
    mail_subject VARCHAR(1024) NOT NULL,
    mail_body mediumblob NOT NULL,
    category BIGINT(20),
    days_gap BIGINT(20),
    event BIGINT(20),
    start_date DATE,
    end_date DATE,
    time_of_day TIME
) DEFAULT CHARSET=utf8;

ALTER TABLE mail_templates ADD COLUMN is_deleted TINYINT(1) DEFAULT 0;

ALTER TABLE mail_templates 
    ADD COLUMN last_mail_sent DATETIME, 
    ADD COLUMN creation_date DATETIME, 
    ADD COLUMN updation_date DATETIME;
--****************March 15: Filtering Local Users ********************--
ALTER TABLE user
   ADD COLUMN is_local TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;  
   
--*************************Apr 23 :for NEW UI CHANGES *************************--   
ALTER TABLE tool
  ADD COLUMN is_free TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  ADD COLUMN tagline varchar(255) ,
  ADD COLUMN cat_id bigint(20) ;

  
  
ALTER TABLE tool
  Add column desc_link varchar(255) ,
  Add column tool_link varchar(255) ;
  
  
create table feedback(
id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
user_id BIGINT NOT NULL,
tool_id BIGINT NOT NULL ,
rating Double(10,1),
title VARCHAR(255),
comments  VARCHAR(255),
feedback_date DATETIME);

--*************************Apr 27 :User Authentication *************************--   

ALTER TABLE user ADD COLUMN deleted TINYINT(1) DEFAULT 0;   

--*******************Apr 30th for Unsubscription of mails *****************--
ALTER TABLE user
ADD COLUMN unsubscribe boolean not null default 0;

--*******************May 22th for Generalized report generation for AmazonPriceAnalyzer *****************--
CREATE TABLE amazonPA_reco(
id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
reco VARCHAR(255)
);

ALTER TABLE `lxrmarketplace`.`feedback` CHANGE COLUMN `comments` `comments` VARCHAR(1024) NULL DEFAULT NULL  ;

--********************* June 1st :email tracking *********************--

create table event_details(
  `event_detail_id`  BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   mail_template_id  bigint(20),
   sent_date datetime null
   );

create table user_event(
  `id`  BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   event_detail_id BIGINT NOT NULL,
    INDEX event_detail_id_index (event_detail_id ASC) ,
    FOREIGN KEY (event_detail_id) REFERENCES event_details (event_detail_id),
   user_id  bigint(20)
   );

create table event_track_details(
  `track_id`  BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   event_detail_id  bigint(20),
   user_id  bigint(20),
   opened_date  datetime null,
   visited_date  datetime null,
   no_of_visits  BIGINT  default  0
   );   
   
--*****To send ondemand mails to all guest users whose mailId's are present****--   
update user set is_active=1, unsubscribe=0 where email is NOT NULL;   

--*****To filter local users june 20,2012*****----
CREATE TABLE localuser_filter (
    filter_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    filter VARCHAR(255),
    filter_type int(10)
    );

--****To inser guest users mail  on click of getresult on july 20********---
CREATE TABLE guestuser_emails(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT,  
    session_id BIGINT,
    email_id VARCHAR(255)
 ); 
 
 -----****for video tutorial usage on Aug 14********------
 
CREATE TABLE video_usage(id  BIGINT  NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    session_id  BIGINT,
    video_id BIGINT,
    count INT NULL );
    
  -----****for stop words on oct 17********------   
    
   create table stop_words(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,stop_word varchar(255));
   
   ------****nov 26 added is_deleted col in tool table  ****-------------------
   ALTER  TABLE   tool   ADD  COLUMN  is_deleted TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;
   
   -------**** Jan 03 2013 for admin settings ***--------------
   ALTER TABLE `lxrmarketplace`.`localuser_filter` ADD COLUMN `restriction_type` INT(4) NULL ;
   ALTER TABLE `lxrmarketplace`.`localuser_filter` CHANGE COLUMN `filter_type` `filter_option` INT(4) NULL DEFAULT NULL  ;
   alter table localuser_filter  add column is_deleted boolean  not null  DEFAULT false;
   
   -------**** Feb 4 2013 for Competitor webpage Monitor tool ****--------

create table cmp_url_Info(info_id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,user_id  bigint not null ,cmp_url  varchar(2048),frequency int,start_date DATETIME,is_deleted boolean  not null  DEFAULT false);

create table  cmp_summary( info_id bigint not null ,CONSTRAINT info_ref  FOREIGN KEY (info_id) REFERENCES cmp_url_Info (info_id),date DATETIME,total_urls bigint,new_urls bigint,removed_urls bigint,total_words bigint,new_words bigint,removed_words bigint,text_changes MEDIUMTEXT,is_deleted boolean  not null  DEFAULT false);

create table cmp_links(link_id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY, links varchar(2048),link_desc varchar(255),anchor_text varchar(2048),is_deleted boolean  not null  DEFAULT false);

create table cmp_images(img_id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY, image_src varchar(2048),alt_text varchar(2048),img_path varchar(255),img_name varchar(2048),is_deleted boolean  not null  DEFAULT false,img_width bigint(20),img_height bigint(20));

CREATE TABLE `cmp_data` (
`info_id` bigint(20) NOT NULL,
`date` datetime DEFAULT NULL,
`content_type` int(11) DEFAULT NULL,
`content_id` bigint(20) NOT NULL,
KEY `info_ref` (`info_id`)
);

create table cmpweb_emails(info_id BIGINT NOT NULL,date  datetime,email  VARCHAR(255));

   
   -------**** Mar 28 2013 for Competitor webpage Monitor tool ****--------

alter table cmp_summary  add column remark  varchar(255) default null;


-------------******** May 16 Keyword rank checker tool *********-------------

create table rank_checker_user(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id bigint not null,CONSTRAINT rankuser_ref  FOREIGN KEY (user_id) REFERENCES  user(id),
    alert_status  boolean  not null  DEFAULT false,
    weekly_report  boolean  not null  DEFAULT false,
    geographic_loc varchar(255),language varchar(255),
    bingMarkVal varchar(255),
    start_date datetime,
    is_logged_in boolean not null
);

create table rank_checker_domain(
    dom_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    domain  varchar(2048) null, 
    rank_user_id bigint not null,
    CONSTRAINT rank_user_id_ref  FOREIGN KEY (rank_user_id) REFERENCES  rank_checker_user(id),
    is_userdomain  boolean  not null  DEFAULT false,
    added_date datetime,
    is_deleted boolean  not null  DEFAULT false
);

create table rank_checker_keyword(
    key_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    rank_user_id bigint not null,
    CONSTRAINT rank_ref  FOREIGN KEY (rank_user_id) REFERENCES  rank_checker_user(id),
    keyword varchar(2048),
    added_date datetime,
    is_deleted boolean  not null  DEFAULT false,
    alert_operator  bigint,
    alert_value bigint,
    google_se boolean not null DEFAULT false,
    bing_se boolean not null DEFAULT false
);

create table rank_checker_keyword_stats(
    stats_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    keyword_id bigint not null,
    CONSTRAINT keyword_ref  FOREIGN KEY (keyword_id) REFERENCES rank_checker_keyword(key_id),
    domain_id bigint not null,
    CONSTRAINT keyworddom_ref  FOREIGN KEY (domain_id) REFERENCES rank_checker_domain(dom_id),
    keyword_rank_g bigint,
    keyword_rank_b bigint,
    date datetime
);

ALTER  TABLE  rank_checker_keyword CHANGE COLUMN  alert_value alert_value  varchar(255);


   -------**** May 22 2013 for Paypal transactions ****--------

CREATE TABLE `credit_card_info` (
  `card_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `paypal_card_id` varchar(50) NOT NULL,
  `card_no` varchar(50) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `valid_untill` datetime DEFAULT NULL,
  `expire_month` int(11) DEFAULT NULL,
  `expire_year` int(11) DEFAULT NULL,
  `saving_time` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0', 
  `address_line_1` varchar(25) DEFAULT NULL,    
  `address_line_2` varchar(25) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`card_id`),
  KEY `user_id_index` (`user_id`),
  CONSTRAINT `user_ref1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE `paypal_transaction_stats` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `state` varchar(10) NOT NULL,
  `intent` tinytext,
  `tool_id` bigint(20) DEFAULT NULL,
  `currency` tinytext,
  `amount` double(10,2) DEFAULT NULL,
  `paypal_payment_id` varchar(100) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `lxr_card_id` bigint(20) DEFAULT NULL,
  `is_card` tinyint(1) NOT NULL,
  `payer_user_id` bigint(20) NOT NULL,
  `paypal_payer_id` varchar(100) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `payer_user_id_index` (`payer_user_id`),
  KEY `tool_id_index` (`tool_id`),
  KEY `lxr_card_id_index` (`lxr_card_id`),
  CONSTRAINT `lxr_card_ref1` FOREIGN KEY (`lxr_card_id`) REFERENCES `credit_card_info` (`card_id`),
  CONSTRAINT `payer_user_ref1` FOREIGN KEY (`payer_user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `tool_ref1` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`)
);

create table `seo_user_campaign_map`( 
  `map_id` bigint(20) NOT NULL  AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL, 
  `camp_id` bigint(20) NOT NULL, 
  `permission` boolean NOT NULL DEFAULT false,
   PRIMARY KEY (`map_id`),
   KEY `user_id_index` (`user_id`),
   CONSTRAINT `user_ref2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);
   
create table `seo_campaign`( 
  `camp_id` bigint(20) NOT NULL  AUTO_INCREMENT,
  `campaign_name` varchar(1024) DEFAULT NULL,
  `domain_name` varchar(1024) DEFAULT NULL,
  `analytics_refresh_token` varchar(100) DEFAULT NULL,
  `analytics_access_token` varchar(100) DEFAULT NULL,
  `analytics_profile_id`  varchar(100) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,geographic_loc varchar(255),language varchar(255),
  `bingMarkVal` varchar(255),
  `is_deleted` boolean default false,
   PRIMARY KEY (`camp_id`) 
);   


CREATE TABLE `seo_keywords` (
  `keyword_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `camp_id` bigint(20) NOT NULL,
  `keyword_text` varchar(2048) DEFAULT NULL,
  `added_date` datetime,
  `is_deleted` tinyint(1) DEFAULT '0',
    PRIMARY KEY (`keyword_id`),
    KEY `camp_id_index` (`camp_id`),
    CONSTRAINT `seo_campaign_ref2` FOREIGN KEY (`camp_id`) REFERENCES `seo_campaign` (`camp_id`)
);

CREATE TABLE `seo_keyword_stats` (
  `key_stats_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyword_id` bigint(20) NOT NULL,
  `domain_id` bigint(20) NOT NULL,
  `keyword_rank_g` bigint(20) DEFAULT NULL,
  `keyword_rank_b` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`key_stats_id`),
  KEY `seo_domain_ref` (`domain_id`),
  KEY `seo_keyword_ref` (`keyword_id`),
  CONSTRAINT `seo_domain_ref` FOREIGN KEY (`domain_id`) REFERENCES `seo_domains` (`domain_id`),
  CONSTRAINT `seo_keyword_ref` FOREIGN KEY (`keyword_id`) REFERENCES `seo_keywords` (`keyword_id`)
);

CREATE TABLE `seo_dashboard_stats` (
  `domain_stats_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `visits` bigint(20) DEFAULT 0,
  `revenue` double(20,2) DEFAULT 0,
  `backlinks` bigint(20) DEFAULT 0,
  `alexa_rank` bigint(20) DEFAULT 0,
  `page_rank` tinyint DEFAULT 0,
  `social_links` bigint(20) DEFAULT 0,
  `unique_anchor_text` bigint(20) DEFAULT 0,    
  PRIMARY KEY (`domain_stats_id`),
  KEY `seo_domain_ref2` (`domain_id`),
  CONSTRAINT `seo_domain_ref2` FOREIGN KEY (`domain_id`) REFERENCES `seo_domains` (`domain_id`)
);

CREATE TABLE `seo_web_stats` (
  `web_stats_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `domain_id` BIGINT(20) NOT NULL,
  `fetch_date` DATETIME DEFAULT NULL,
  `non_working_url_count` BIGINT(40) DEFAULT 0,
  `title_score`TINYINT DEFAULT 0,
  `meta_key_score` TINYINT DEFAULT 0,   
  `meta_desc_score` TINYINT DEFAULT 0,
  `image_score` TINYINT DEFAULT 0,
  `h_tag_score` TINYINT DEFAULT 0,  
  `word_score` TINYINT DEFAULT 0,
  `keyword_in_url_score` TINYINT DEFAULT 0,
  `home_loadtime_score` TINYINT DEFAULT 0,
  `home_content_size` DOUBLE(20,2) DEFAULT 0,   
  `is_www_resolved` TINYINT(1) DEFAULT 0,
  `has_error_page` TINYINT(1) DEFAULT 0,    
  `has_robot_txt` TINYINT(1) DEFAULT 0,
  `has_sitemap` TINYINT(1) DEFAULT 0,
  `home_code_text_ratio` DOUBLE(20,2) DEFAULT 0,    
  `mobile_compatibility` TINYINT(1) DEFAULT 0,
  `seo_friendly_url_score` TINYINT DEFAULT 0, 
  `backlink_count` BIGINT DEFAULT 0,
  `backlink_score` TINYINT DEFAULT 0,  
  `backlink_quality` INT DEFAULT 0,  
  `nofollow_pc` TINYINT DEFAULT 0, 
  `internal_link_score` TINYINT DEFAULT 0, 
  `backlink_ancr_text_pc` TINYINT DEFAULT 0, 
  `unique_backlink_ancr_text_pc` TINYINT DEFAULT 0, 
  `backlink_ancr_stopwords_pc` TINYINT DEFAULT 0, 
  `directory_listing_score` TINYINT DEFAULT 0,
  `google_plus_shares` BIGINT DEFAULT 0,
  `facebook_shares` BIGINT DEFAULT 0,
  `facebook_total` BIGINT DEFAULT 0,
  `pinterest_shares` BIGINT DEFAULT 0,
  `tweets` BIGINT DEFAULT 0,
  `linkedin_shares` BIGINT DEFAULT 0,
  `social_score` TINYINT DEFAULT 0, 
  `content_score` TINYINT DEFAULT 0, 
  `technical_score` TINYINT DEFAULT 0, 
  `link_score` TINYINT DEFAULT 0, 
  `site_grader_score` TINYINT DEFAULT 0,  
  PRIMARY KEY (`web_stats_id`),
  KEY `seo_domain_ref3` (`domain_id`),
  CONSTRAINT `seo_domain_ref3` FOREIGN KEY (`domain_id`) REFERENCES `seo_domains` (`domain_id`)
);

CREATE TABLE `seo_url_stats` (
  `url_stats_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) NOT NULL,
  `url` varchar(512) NOT NULL,
  `date` datetime DEFAULT NULL,
  `status_code` int(11) DEFAULT '0',
  `has_title` tinyint(1) DEFAULT '0',
  `has_metakey` tinyint(1) DEFAULT '0',
  `has_metadesc` tinyint(1) DEFAULT '0',
  `title` varchar(512) DEFAULT NULL,
  `metakey` varchar(1024) DEFAULT NULL,
  `metadesc` varchar(1024) DEFAULT NULL,
  `total_images_count` bigint(40) DEFAULT '0',
  `alttexted_image_count` bigint(20) DEFAULT '0',
  `h_tag_score` tinyint(4) DEFAULT '0',
  `h1_texts` varchar(1024) DEFAULT NULL,
  `iframe_count` bigint(20) DEFAULT '0',
  `internal_link_count` bigint(40) DEFAULT '0',
  `internal_link_score` tinyint(4) DEFAULT '0',
  `word_count` bigint(40) DEFAULT '0',
  `word_score` bigint(20) DEFAULT '0',
  `title_score` bigint(20) DEFAULT '0',
  `meta_key_score` bigint(20) DEFAULT '0',
  `meta_desc_score` bigint(20) DEFAULT '0',
  `keyword_in_url_score` bigint(20) DEFAULT '0',
  `seo_friendly_url_score` bigint(20) DEFAULT '0',
  PRIMARY KEY (`url_stats_id`),
  KEY `seo_domain_ref4` (`domain_id`),
  CONSTRAINT `seo_domain_ref4` FOREIGN KEY (`domain_id`) REFERENCES `seo_domains` (`domain_id`)
);

alter table seo_user_campaign_map add column is_guest_user TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;


CREATE TABLE `paid_tool_permission` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `tool_id` bigint(20) NOT NULL,
  `permission` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `user_id_index` (`user_id`)
); 

create table seo_domains_map(
  domain_map_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  domain_id bigint(20) not null,
  CONSTRAINT seo_domains_ref  FOREIGN KEY (domain_id) REFERENCES seo_domains(domain_id), 
  camp_id bigint(20) not null,
  CONSTRAINT seo_campaign_ref  FOREIGN KEY (camp_id) REFERENCES seo_campaign(camp_id),  
  domain varchar(2048),is_user_domain  boolean  not null  DEFAULT false,
  added_date datetime,is_deleted boolean  not null  DEFAULT false
); 
    
CREATE TABLE seo_domains (
  domain_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  domain varchar(2048) DEFAULT NULL,
  added_date datetime
);

ALTER TABLE seo_campaign 
  ADD COLUMN analytics_account_id varchar(255);
  
ALTER TABLE `lxrmarketplace`.`seo_url_stats` CHANGE COLUMN `url` `url` VARCHAR(1024) NOT NULL  , CHANGE COLUMN `title` `title` VARCHAR(1024) NULL DEFAULT NULL  , CHANGE COLUMN `metakey` `metakey` VARCHAR(4096) NULL DEFAULT NULL  , CHANGE COLUMN `metadesc` `metadesc` VARCHAR(4096) NULL DEFAULT NULL  ;
ALTER TABLE `lxrmarketplace`.`seo_web_stats` CHANGE COLUMN `home_code_text_ratio` `home_code_text_ratio` TINYINT(4) NULL DEFAULT '0'  ;

ALTER TABLE seo_campaign ADD COLUMN modified_date datetime DEFAULT NULL;

ALTER TABLE tool ADD COLUMN show_in_home TINYINT(1) DEFAULT 1 AFTER is_deleted;

create table seo_tasks(
    task_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(1024),
    short_desc VARCHAR(4096),
    long_desc VARCHAR(4096),
    category BIGINT(20),
    repetitive boolean  not null  DEFAULT false,
    priority BIGINT(100),
    task_type VARCHAR(250),
    tool_id BIGINT NOT NULL
);

CREATE TABLE seo_campaign_task_map (
  map_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  campaign_id bigint(20) NOT NULL,
  task_id bigint(20) NOT NULL,
  action bigint(20) NOT NULL,
  action_date datetime,
  CONSTRAINT seo_campaign_ref3  FOREIGN KEY (campaign_id) REFERENCES seo_campaign(camp_id),
  CONSTRAINT seo_tasks_ref1  FOREIGN KEY (task_id) REFERENCES seo_tasks(task_id)
);

CREATE TABLE `seo_scheduled_task` (
  `map_id` bigint(20) NOT NULL PRIMARY KEY,
  `scheduled_date` datetime DEFAULT NULL,
   CONSTRAINT `seo_map_ref1` FOREIGN KEY (`map_id`) REFERENCES `seo_campaign_task_map` (`map_id`)
);


alter table seo_campaign add column weekly_report boolean not null DEFAULT false,unsubscribe boolean not null DEFAULT false;

ALTER TABLE seo_campaign ADD COLUMN currency varchar(255);


-------------------------------------------- LXRSEO FEEDBACK 7-OCT-2013 ---------------------------------------------

CREATE TABLE seo_fb_questions(
    fb_q_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fb_question VARCHAR(1024) NOT NULL,
    fb_q_type BIGINT NOT NULL,
    is_active boolean  not null  DEFAULT true
);

CREATE TABLE seo_fb_received(
    fb_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fb_q_id bigint(20) NOT NULL,
    session_id bigint(20) NOT NULL,
    fb_time datetime DEFAULT NULL,
    screen_name VARCHAR(1024) NOT NULL,
    response VARCHAR(1024) NOT NULL,
    CONSTRAINT fb_q_id_ref1 FOREIGN KEY (fb_q_id) REFERENCES seo_fb_questions (fb_q_id)
);

ALTER TABLE seo_dashboard_stats ADD COLUMN campaign_id bigint(20) NOT NULL;
ALTER TABLE user ADD COLUMN signup_type TINYINT;

/*------------------------- PAGE TRACKING --------------------------------*/
CREATE TABLE `user_webpage_tracker` (
  `session_id` bigint(20) NOT NULL,
  `request_uri` varchar(1024) NOT NULL,
  `request_query` varchar(1024) DEFAULT NULL,
  `entry_time` datetime DEFAULT NULL,
  KEY `session_ref_page` (`session_id`),
  CONSTRAINT `session_ref_page` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`)
);

ALTER TABLE session CHANGE COLUMN `os` `os` VARCHAR(256) NULL DEFAULT NULL  , 
CHANGE COLUMN `browser` `browser` VARCHAR(256) NULL DEFAULT NULL  , 
CHANGE COLUMN `browser_version` `browser_version` VARCHAR(256) NULL DEFAULT NULL  ;

ALTER TABLE seo_dashboard_stats ADD COLUMN conversions bigint(20) NOT NULL;

ALTER TABLE paid_tool_permission ADD COLUMN free_trial TINYINT(1) NOT NULL DEFAULT 0 AFTER is_completed;
ALTER TABLE `lxrmarketplace`.`seo_campaign` CHANGE COLUMN `campaign_name` `campaign_name` VARCHAR(50) NULL DEFAULT NULL  ;
ALTER TABLE `lxrmarketplace`.`seo_campaign` CHANGE COLUMN `domain_name` `domain_name` VARCHAR(50) NULL DEFAULT NULL  ;



/*------------------------- Tool usage Tracking by IP --------------------------------*/

ALTER TABLE seo_campaign ADD COLUMN unsubscribe_date datetime DEFAULT NULL AFTER unsubscribe;
ALTER TABLE seo_campaign ADD COLUMN base_domain varchar(1024) NOT NULL ;
alter table seo_web_stats add column mobile_compatable_score  bigint(20) ;

/*----------For Auto mail changes ---------------*/

alter table mail_templates add column send_mail_option bigint(20), add column upload_file_name varchar(1024);

/*---------- For saving failed paypal transactions ----------*/
ALTER TABLE `paypal_transaction_stats` ADD COLUMN `failure_message` VARCHAR(1024) NULL  AFTER `transaction_id` ;

/* to support feature to show website related data fpr LXRSE Tasks */ 
ALTER TABLE `seo_tasks` ADD COLUMN `data_availability` TINYINT(1) NOT NULL DEFAULT 0  AFTER `tool_id` ;

/* Feb 19: to provide promotional feature for LXRSEO*/

ALTER TABLE `paid_tool_permission` ADD COLUMN `promotion_id` BIGINT NULL DEFAULT NULL  AFTER `free_trial` ;

CREATE TABLE promotion_partner(
    partner_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    partner_name VARCHAR(255)   
);

CREATE TABLE promotional_offers (
    promotion_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    partner_id BIGINT NOT NULL,
    tool_id BIGINT NOT NULL, 
    promotion_name VARCHAR(255),
    start_date datetime DEFAULT NULL,
    end_date datetime DEFAULT NULL,
    is_active TINYINT(1) DEFAULT false,
    price DOUBLE(10,2) DEFAULT 0.00,
    no_of_times MEDIUMINT DEFAULT 0,
    no_of_days MEDIUMINT DEFAULT 0,
    display_string VARCHAR(255),
    CONSTRAINT tool_ref2 FOREIGN KEY (tool_id) REFERENCES tool (id),
    CONSTRAINT `promotion_partner_ref2` FOREIGN KEY (`partner_id`) REFERENCES `promotion_partner` (`partner_id`)
);

CREATE TABLE user_partner_map(
    u_p_map_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    partner_id BIGINT NOT NULL,
    CONSTRAINT `user_ref3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
    CONSTRAINT `promotion_partner_ref1` FOREIGN KEY (`partner_id`) REFERENCES `promotion_partner` (`partner_id`)
);

/* Feb 19: to store raw details-------Phrase: NX-LOXERS */

CREATE TABLE `raw_details` (
  `card_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,  
  `encr_card_no` varchar(50) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `encr_cvv` varchar(50) DEFAULT NULL,  
  `exp_month` int(11) DEFAULT NULL,
  `exp_year` int(11) DEFAULT NULL, 
  `created_date` datetime DEFAULT NULL, 
  `country_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`card_id`)
);

ALTER TABLE `raw_details` CHANGE COLUMN `encr_card_no` `encr_card_no` VARCHAR(1024) NULL DEFAULT NULL  , 
CHANGE COLUMN `encr_cvv` `encr_cvv` VARCHAR(100) NULL DEFAULT NULL  ;

ALTER TABLE `raw_details` CHANGE COLUMN `encr_card_no` `number` VARCHAR(1024) NULL DEFAULT NULL  ;
ALTER TABLE `raw_details` CHANGE COLUMN `card_type` `type` VARCHAR(25) NULL DEFAULT NULL  ;
ALTER TABLE `raw_details` CHANGE COLUMN `encr_cvv` `code` VARCHAR(100) NULL DEFAULT NULL  ;
ALTER TABLE `raw_details` CHANGE COLUMN `exp_month` `month` INT(11) NULL DEFAULT NULL  ;
ALTER TABLE `raw_details` CHANGE COLUMN `exp_year` `year` INT(11) NULL DEFAULT NULL  ;

ALTER TABLE `raw_details` CHANGE COLUMN `month` `month` VARCHAR(100) NULL DEFAULT NULL  , CHANGE COLUMN `year` `year` VARCHAR(100) NULL DEFAULT NULL  ;


/*--------------For saving authorize.net info ----------------*/

alter table credit_card_info add column cust_profile_id varchar(255) NOT NULL after paypal_card_id;
alter table credit_card_info add column payment_profile_id varchar(255) NOT NULL after cust_profile_id;
alter table credit_card_info add column service_provider bigint(5) NOT NULL;

/*--------------create lxrm payemnt table on Feb 10----------------*/

CREATE TABLE `lxrm_credit_card_info` (
 `card_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `cust_profile_id` varchar(255) NOT NULL,
  `payment_profile_id` varchar(255) NOT NULL,
  `card_no` varchar(50) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `valid_untill` datetime DEFAULT NULL,
  `expire_month` int(11) DEFAULT NULL,
  `expire_year` int(11) DEFAULT NULL,
  `saving_time` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0', 
  `address_line_1` varchar(25) DEFAULT NULL,    
  `address_line_2` varchar(25) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`card_id`),
  KEY `user_id_index` (`user_id`),
  CONSTRAINT `user_tabref1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`));
  
  
CREATE TABLE `lxrm_payment_transaction_stats` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `state` varchar(10) NOT NULL,
  `intent` tinytext,
  `tool_id` bigint(20) DEFAULT NULL,
  `currency` tinytext,
  `amount` double(10,2) DEFAULT NULL,
  `authorization_code` varchar(100) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `lxr_card_id` bigint(20) DEFAULT NULL,
  `is_card` tinyint(1) NOT NULL,
  `payer_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `payer_user_id_index` (`payer_user_id`),
  KEY `tool_id_index` (`tool_id`),
  KEY `lxr_card_id_index` (`lxr_card_id`),
  CONSTRAINT `lxr_card_tabref1` FOREIGN KEY (`lxr_card_id`) REFERENCES `lxrm_credit_card_info` (`card_id`),
  CONSTRAINT `payer_user_tabref1` FOREIGN KEY (`payer_user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `tool_tabref1` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`)
);

create table download_report_details(r_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
report_name varchar(1000),tool_id bigint(20) NOT NULL,user_id bigint(20) NOT NULL,created_date DATETIME,
url varchar(2048) NOT NULL);

alter table lxrm_payment_transaction_stats add column failure_message varchar(2048) after payer_user_id;

create table tools_pricing (id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,tool_id BIGINT NOT NULL,display_string varchar(2048),
report_type varchar(2048),amount double NOT NULL,CONSTRAINT tool_ref_tab FOREIGN KEY (id) REFERENCES tool (id) );


alter table lxrm_credit_card_info add column paypal_card_id  varchar(50) NOT NULL after cust_profile_id;

alter table lxrm_payment_transaction_stats add column paypal_payer_id varchar(100) DEFAULT NULL after payer_user_id;

alter table lxrm_payment_transaction_stats add column paypal_payment_id varchar(100) DEFAULT NULL after amount;

/*----- TO STORE UNSUBSCRIPTION DETAILS ON MAR13 2014 ---------*/

alter table cmp_url_Info add column emial_unsubscription TINYINT(1) NULL,
		Add column tool_unsubscription  TINYINT(1) NULL,
		Add column emial_unsub_date  DATETIME,
		Add column tool_unsub_date  DATETIME,
		Add column remark VARCHAR(1024);
		
alter table rank_checker_user add column tool_unsubscription  TINYINT(1) NULL,
        Add column tool_unsub_date  DATETIME,
        Add column remark VARCHAR(1024);
        
alter table mail_templates add column from_address bigint(20);

/* ------ Changing Promotions from no of days to no of months ------ */
ALTER TABLE `promotional_offers` CHANGE COLUMN `no_of_days` `no_of_months` MEDIUMINT(9) NULL DEFAULT '0';

/* -------------- mail Templates ----------------*/
alter table mail_templates add column logo bigint default null,add column url varchar(2048) default null,
add column personal_address varchar(2048) default null;

create table unsubscribe_reasons( user_id bigint not null,CONSTRAINT unsub_reasons  FOREIGN KEY (user_id) REFERENCES  user(id),
unsub_reason varchar(1024),unsub_date DATETIME);

/*------------- Store failures in card provision in LXRSEO   ------------------*/
create table lxrseo_card_failure(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
user_id BIGINT(20) NOT NULL, 
fail_time DATETIME, 
card_type varchar(1000),
failure_message varchar(2048) NOT NULL,
CONSTRAINT `user_card_failure_ref` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`));


/*-------------- Apr24-2014 : Storing Report Download Stats   -------------------*/  
CREATE TABLE lxr_reports (
report_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
tool_id BIGINT,
report_name VARCHAR(1024) NULL,
CONSTRAINT tool_ref_reports FOREIGN KEY (tool_id) REFERENCES tool (id)
);

CREATE  TABLE lxr_report_downloads (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  session_id BIGINT NOT NULL ,
  report_id BIGINT NOT NULL ,
  count INT NULL ,
  INDEX session_id_index (session_id ASC) ,
  CONSTRAINT session_ref_down FOREIGN KEY (session_id) REFERENCES session (id),
  CONSTRAINT report_ref_down FOREIGN KEY (report_id) REFERENCES lxr_reports (report_id)
);


/**********------For Mobile Site Grader Data--------***********/

create table mobile_user(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),email VARCHAR(255),start_time DATETIME,deleted TINYINT(1)); 
    
create table mobile_campaign(`camp_id` bigint(20) NOT NULL  AUTO_INCREMENT,
  `campaign_name` varchar(1024) DEFAULT NULL,
  `domain_name` varchar(1024) DEFAULT NULL,`creation_date` datetime DEFAULT NULL,
	base_domain varchar(1024) DEFAULT NULL,modified_date datetime DEFAULT NULL,
   `is_deleted` boolean default false,
   PRIMARY KEY (`camp_id`) 
  );

create table mobile_user_campaign_map( `map_id` bigint(20) NOT NULL  AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL, 
  `camp_id` bigint(20) NOT NULL, 
   PRIMARY KEY (`map_id`),
   KEY `user_id_index` (`user_id`),
   CONSTRAINT `mobile_user_ref` FOREIGN KEY (`user_id`) REFERENCES mobile_user (`id`)
);

create table mobile_domains_map(domain_map_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  domain_id bigint(20) not null,
  CONSTRAINT seo_domains_tab_ref  FOREIGN KEY (domain_id) REFERENCES seo_domains(domain_id), 
  camp_id bigint(20) not null,
  CONSTRAINT mobile_campaign_ref  FOREIGN KEY (camp_id) REFERENCES mobile_campaign(camp_id),  
  domain varchar(2048),
  added_date datetime,is_deleted boolean  not null  DEFAULT false);
  
/**********------ For First Visit Tracking --------***********/  
CREATE TABLE urls(
	url_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	url VARCHAR(255) UNIQUE, 
	added_date datetime DEFAULT NULL
); 

CREATE TABLE first_visit_tracking(
	user_id bigint(20) NOT NULL, 
	url_id bigint(20) NOT NULL, 
	added_date datetime,
	CONSTRAINT first_visit_user_ref FOREIGN KEY (user_id) REFERENCES user (id),
	CONSTRAINT first_visit_url_ref FOREIGN KEY (url_id) REFERENCES urls (url_id)
);


alter table mail_templates add column send_mail_today tinyint(1) default 0;

CREATE TABLE admin_redirect (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        URL VARCHAR(255),
        redirect_URL VARCHAR(255),
	added_date datetime,
        status TINYINT(1)
);

/********------------July3 ,2014----------------**********************/

alter table session ADD INDEX ip_index (ip ASC);

create table lxrm_session_by_ip(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,raw_ip varchar(25),conv_ip int unsigned,tool_id bigint,count INT NULL);


create table ip_country(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,raw_ip varchar(25),country varchar(1024),region_name varchar(256),city_name varchar(256));


/***insert into lxrm_session_by_ip(raw_ip,conv_ip,tool_id,count) 
select s.ip,INET_ATON(s.ip),A.tool_id,sum(A.count) as count_val
from (select  session_id,tool_id,count from tool_usage )A JOIN
(select ip,id from session)s ON s.id=A.session_id group by s.ip,A.tool_id;**/

/**************--------- July14-2014 Increased size of some columns ------------*********/
ALTER TABLE `seo_campaign` CHANGE COLUMN `campaign_name` `campaign_name` VARCHAR(2048) NULL DEFAULT NULL  , CHANGE COLUMN `domain_name` `domain_name` VARCHAR(2048) NULL DEFAULT NULL  ;

ALTER TABLE `seo_url_stats` CHANGE COLUMN `url` `url` VARCHAR(4096) NOT NULL  , CHANGE COLUMN `title` `title` VARCHAR(4096) NULL DEFAULT NULL  , CHANGE COLUMN `h1_texts` `h1_texts` VARCHAR(4096) NULL DEFAULT NULL  ;

ALTER TABLE `seo_campaign` CHANGE COLUMN `base_domain` `base_domain` VARCHAR(2048);

/****************Logical mapping of LXRM tools sep 1 *******************/

create table lxrm_logically_mapped_tools(id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
tool_id BIGINT NOT NULL,mapped_id VARCHAR(255));

/**********--------------Stroing mobile user data ---------*********************/

alter table mobile_user add column mobile_tool_name varchar(256) not null;

/**********--------------For Stroing lxrseo user data ---------*********************/
Alter table user add column lxrseo_password varchar(50);

/**********--------------For Stroing mobile unsubscription data ---------*********************/
alter table mobile_user add column unsubscribe  TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
add column unsub_reason VARCHAR(1024),add column unsub_date DATETIME; 

/********------------JAN 21 ,2016----------------**********************/
CREATE TABLE seo_tip_user (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    activation_date TIMESTAMP NULL,
    unsubscribe _date TIMESTAMP NULL,
    unsubscribe TINYINT(1)
);

/********--Added for social media analyzer tool for pinterest channel  Feb 14 ,2016---**********************/
CREATE TABLE smt_pinterest(
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    media_user_id bigint(22) NOT NULL,
  smt_pin_audience bigint(24),/**--Pinterest followers---*/
  smt_pin_activity bigint(24),/**--Pinterest pins (Toatl posts) ---*/
  smt_pin_likes bigint(24),/**--Pinterest Likes  ---*/
  smt_pin_boards bigint(24),/**--Pinterest boards  ---*/
  smt_pin_engagement bigint(20),
  pin_post_audience_engaged double(32,2),
  pin_recent_post varchar(2048),
  pin_post_likes bigint(22),
  pin_post_comments bigint(22),
  pin_post_image_path  mediumtext,
  pin_post_date varchar(25),
  insert_date datetime  not null,
  CONSTRAINT `smt_pinterest_fk_1` FOREIGN KEY (`media_user_id`) REFERENCES `social_media_user` (`id`)

/*Mar 28,2016 -Social Media Analyzer [Enhancement to Social Media Analyzer tool in LXRMarketplace to accommodate the competitor data]*/
alter table lxrmarketplace.social_media_user add is_userdomain TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 after is_logged_in;
/*For existed user's only before adding is_userdomain coloum  */
Update  lxrmarketplace.social_media_user set is_userdomain=1;



ALTER TABLE smt_twitter DROP COLUMN smt_tw_engagement,DROP COLUMN tw_post_audience_engaged;
ALTER TABLE smt_facebook DROP COLUMN smt_fb_engagement,DROP COLUMN fb_post_audience_engaged;
ALTER TABLE smt_googleplus DROP COLUMN smt_gp_engagement,DROP COLUMN gp_post_audience_engaged;
ALTER TABLE smt_googleplus DROP COLUMN smt_gp_activity;
ALTER TABLE  smt_instagram DROP COLUMN smt_inst_engagement,DROP COLUMN inst_post_audience_engaged;
ALTER TABLE  smt_youtube DROP COLUMN smt_yt_engagement,DROP COLUMN yt_post_audience_engaged;
ALTER TABLE  smt_pinterest DROP COLUMN smt_pin_engagement,DROP COLUMN pin_post_audience_engaged;

CREATE TABLE smt_pinterest(
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    media_user_id bigint(20) NOT NULL,
  smt_pin_audience int(10),/**--Pinterest followers---*/
  smt_pin_activity int(10),/**--Pinterest pins (Total posts) ---*/
  smt_pin_likes int(10),/**--Pinterest Likes  ---*/
  smt_pin_boards int(10),/**--Pinterest boards  ---*/
smt_pin_following int(10),
  pin_recent_post varchar(2048),
 pin_post_repins  int(4),
  pin_post_likes int(4),
  pin_post_comments int(4),
  pin_post_image_path  mediumtext,
  pin_post_date varchar(25),
  insert_date datetime  not null,
  CONSTRAINT `smt_pinterest_fk_1` FOREIGN KEY (`media_user_id`) REFERENCES `social_media_user` (`id`)
);
 
alter table smt_twitter add smt_tw_following bigint(20) DEFAULT 0 after smt_tw_activity;
alter table smt_twitter add smt_tw_favourites bigint(20) DEFAULT 0 after smt_tw_following;
alter table smt_youtube add smt_yt_views bigint(20) DEFAULT 0 after smt_yt_activity;
alter table smt_instagram add smt_inst_following bigint(20) DEFAULT 0 after smt_inst_activity;

/*April 06,2016 for social media analyzer tool*/
alter table smt_twitter add tw_hash_tag varchar(2084) DEFAULT 'NoHashTag' after tw_post_date;

/* for Storing channel names/id of a url for each user*/
create table social_channel_user_names(
user_name_id int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
media_user_id bigint(20) NOT NULL,
twitter_name CHAR(60),
facebook_name CHAR(60),
instagram_name CHAR(60),
pinterest_name CHAR(60),
youtube_name CHAR(60),
goolgePlus_name CHAR(60),
insert_date date  not null,
CONSTRAINT `user_names_fk` FOREIGN KEY (`media_user_id`) REFERENCES `social_media_user` (`id`)
)

/*(Revamped)June 14,2016 for knowledgeBase(Orginal Date Jun 01,2016.) */
CREATE TABLE lxrm_knowledgebase_questions (
	question_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	question varchar(1024) default NULL,
	answer mediumblob default NULL  ,
	question_type SMALLINT default 2,
	active SMALLINT default 1,
	added_date datetime default NULL,
	last_updated datetime default NULL
);

CREATE TABLE lxrm_knowledgebase_questions_tools_map (
	tool_id bigint NOT NULL,
	question_id int NOT NULL,
	PRIMARY KEY (tool_id, question_id),
	CONSTRAINT `tool_ref3` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`),
	CONSTRAINT `lxrm_knowledgebase_questions_ref1` FOREIGN KEY (`question_id`) REFERENCES `lxrm_knowledgebase_questions` (`question_id`)
);

/*June 09,2016 for website tarcking of a user in lxrmarketplace */
create table lxrm_websites_tracking(
website_id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
website varchar(255),
lxrm_tool_id BIGINT NOT NULL,
lxrm_user_id BIGINT NOT NULL,
insert_date  datetime NOT NULL,
CONSTRAINT `lxrm_tool` FOREIGN KEY (`lxrm_tool_id`) REFERENCES `tool` (`id`),
CONSTRAINT `lxrm_user` FOREIGN KEY (`lxrm_user_id`) REFERENCES `user` (`id`)
);

/*May 26, 2016 for LXRMarketplace User's help*/
CREATE  TABLE lxrm_help (
    user_id BIGINT NOT NULL,
    email VARCHAR(255),
    page_url VARCHAR(255),
    help_text VARCHAR(512),
    date_time DATETIME,
    CONSTRAINT user_help_ref FOREIGN KEY (user_id) REFERENCES user (id)
);

/*June 05, 2016 for LXRMarketplace Related tools in tool description page*/
CREATE  TABLE tools_grouping (
    tool_id INT NOT NULL,
    correlated_tool_id INT NOT NULL,
    tools_order INT NOT NULL
);
alter table tool add column tool_date date;

/*June 07, 2016 for LXRMarketplace default tools sorting in homepage*/
CREATE  TABLE lxrm_tool_sorting (
    sorting_order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tool_id INT NOT NULL
);


/*Jul 25,2016 for LXRMarketplace admin can submit answer for user questions of ask the expert.*/
CREATE TABLE lxrm_ate_questions(
	question_id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tool_id bigint(20) NOT NULL,
	user_id bigint(20) NOT NULL,
	question varchar(2048) NOT NULL,
	answer mediumblob default NULL,
	domain varchar(2100) NOT NULL,
	added_date datetime NOT NULL,
	payment_transaction_id bigint(20) NOT NULL,
	view_status tinyint(1) NOT NULL DEFAULT '0',
	CONSTRAINT `tool_id-ref` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`),
	CONSTRAINT `user_id_ref` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
	CONSTRAINT `payment_id_ref` FOREIGN KEY (`payment_transaction_id`) REFERENCES `lxrm_payment_transaction_stats` (`payment_id`)
);

/*July 14, 2016 for trigger modify the primary key in lxrm_tool_sorting*/
alter table lxrm_tool_sorting modify column sorting_order_id mediumint not null;

--July 14, 2016 Trigger
-- when inserting the new tools tool table this trigger will fire to insert new sorting order in lxrm_tool_sorting
DELIMITER //
CREATE TRIGGER tool_sorting
AFTER INSERT
   ON tool FOR EACH ROW
begin
delete from lxrm_tool_sorting;
insert into lxrm_tool_sorting(sorting_order_id,tool_id)
(select @curRank := @curRank + 1 AS rank , id from tool,
(SELECT @curRank := 0) q where tool_date >= date_sub(current_date(),interval 6 month)
and is_deleted=0 and show_in_home=1 order by tool_date desc limit 20)
union
(select @curRank := @curRank + 1 AS rank, t.id
from tool t left join (select tool_id, count(distinct user_id) as 'usage'
from tool_usage, session,(SELECT @curRank := 0) q
 where tool_usage.session_id = session.id group by tool_id order by tool_id) as tu
on t.id = tu.tool_id where t.is_deleted = 0 and t.show_in_home = 1 and
t.tool_date<=date_sub(current_date(),interval 6 month) order by tu.usage desc);
end ; //

/*Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I]
with reference to mail from Pavan Sir Subj:LXRM Tool Changes.*/
rename table cmp_url_Info TO cmp_url_info;

/*Sep 2,2016 Modify table name from seo_tip_user to  lxrm_user_aquisition and add aquisition_type*/
RENAME TABLE  `seo_tip_user` TO  `lxrm_user_aquisition`;
ALTER TABLE lxrm_user_aquisition  ADD COLUMN aquisition_type varchar(24);

/* Creating lxrm_ate_experts table*/
CREATE TABLE lxrm_ate_experts (
    expert_id INT AUTO_INCREMENT PRIMARY KEY,
    first_name varchar(55), 
    last_name varchar(55),
    email varchar(75), 
    role char(50),
    is_deleted tinyint(1) NOT NULL DEFAULT '0'
);

/* Adding extra columns in lxrm_ate_questions*/
ALTER TABLE lxrm_ate_questions ADD COLUMN answer_date DATETIME,
            ADD COLUMN expert_id INT,
            ADD COLUMN reviewer_id INT NULL,
            ADD COLUMN updated_date DATETIME,
            ADD COLUMN is_published tinyint NOT NULL DEFAULT FALSE,
            ADD COLUMN is_feedback_mail_sent tinyint NOT NULL DEFAULT FALSE,
            ADD CONSTRAINT lxrm_ate_experts_ref FOREIGN KEY (expert_id) REFERENCES lxrm_ate_experts(expert_id);


/* Modifying column name added_date to created_date in lxrm_ate_questions*/
 ALTER TABLE lxrm_ate_questions CHANGE added_date created_date DATETIME;

/* Creating lxrm_ate_questions_feedback table*/
CREATE TABLE lxrm_ate_questions_feedback(
    id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    question_id bigint(20),
    rating SMALLINT,
    comment varchar(1024) DEFAULT NULL,
    created_date DATETIME,
    CONSTRAINT lxrm_ate_questions_ref FOREIGN KEY (question_id) REFERENCES lxrm_ate_questions(question_id)
);

/* Oct 6, 2016 creating lxrm_user_aquisition_popup table */
CREATE TABLE lxrm_user_aquisition_popup(
    popup_id int AUTO_INCREMENT PRIMARY KEY,
    popup_name varchar(24),
    is_active tinyint NOT NULL DEFAULT FALSE,
    created_date datetime,
    mail_attachment_name varchar(100)
);

/* Oct 13, 2016 creating lxrm_promos table and lxrm_user_promos_map*/
CREATE TABLE lxrm_promos(
    promo_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    promo_code VARCHAR(12),
    start_date date not null,
    end_date date not null,
    promo_name varchar(255),
    reusable_count SMALLINT
);
CREATE TABLE lxrm_user_promos_map (
    user_id bigint(20) NOT NULL,
    promo_id int,
    applied_count smallint,
    CONSTRAINT `user_id_ref1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
    CONSTRAINT `lxrm_promos_ref` FOREIGN  KEY (`promo_id`) REFERENCES `lxrm_promos`(`promo_id`)
);

ALTER TABLE lxrm_ate_questions DROP FOREIGN KEY payment_id_ref;

ALTER TABLE lxrm_ate_questions MODIFY COLUMN payment_transaction_id bigint(20);

/*
* Revamp of lxrm_ate_experts for admin to add new expert users  in dashboard.
* DOM: Oct 28,2016.Refer the product back log.
*/
update lxrm_ate_experts set first_name=concat(first_name,' ',last_name) where expert_id in(1,2,3,4);

alter table lxrm_ate_experts drop column last_name;

alter table lxrm_ate_experts change  first_name  name varchar(150);

alter table lxrm_ate_experts add column insert_date date NOT NULL after role;

/*Nov 9, 2016 - To store tool template answer related to ate questions.*/
CREATE TABLE lxrm_ate_answer_templates(
    template_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tool_id bigint NOT NULL,
    question varchar(3000) not NULL,
    steps BLOB NOT NULL,
    answer mediumblob NOT NULL,
    CONSTRAINT `tool_refTemplates` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`)
);

/*Nov 10, 2016 - Adding steps column in lxrm_ate_questions*/
ALTER TABLE lxrm_ate_questions ADD COLUMN steps BLOB default NULL AFTER question,
ADD COLUMN other_inputs varchar(1024) AFTER domain;

/*16th Nov, 2016 - Modifying column name insert_date to created_date in lxrm_websites_tracking*/
ALTER TABLE lxrm_websites_tracking CHANGE insert_date created_date DATETIME;

/*16th Nov, 2016 - Added columns in lxrm_websites_tracking table*/
ALTER TABLE lxrm_websites_tracking 
    ADD COLUMN user_input varchar(1024), 
    ADD COLUMN issues varchar(1024), 
    ADD COLUMN suggested_question varchar(512), 
    ADD COLUMN tool_url varchar(1024);
/*24th Nov, 2016 - Added FULLTEXT for question in lxrm_ate_answer_templates*/
ALTER TABLE lxrm_ate_answer_templates ADD FULLTEXT index_name (question);


/*20 january,2017 To Maintain History of Answer temple with Date and Time*/
CREATE TABLE lxrm_ate_answers_history(
    id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    question_id bigint(20) NOT NULL,
    answer mediumblob default NULL,
    created_date datetime NOT NULL, 
 CONSTRAINT `lxrm_ate_questions_ref1` FOREIGN KEY (`question_id`) REFERENCES `lxrm_ate_questions` (`question_id`)
);

/*20th Jan, 2017 - Adding device_category in lxrm_ate_questions */
ALTER TABLE lxrm_ate_questions ADD COLUMN device_category varchar(12) AFTER created_date;

/*31th Jan, 2017 - Adding google_updates_info table for penalty checker */
CREATE TABLE google_updates_info(
    update_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    update_type varchar(100) NOT NULL,
    updated_date date NOT NULL,
    description varchar(2048) NOT NULL
);
CREATE TABLE inboundlinks_domains(

domain_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
domain_name VARCHAR(255) NOT NULL,
fetched_backlinks int DEFAULT '0',
fetched_date date,
ahrefs_backlinks bigint  DEFAULT '0',
ref_pages bigint  DEFAULT '0',
nofollow_links bigint  DEFAULT '0',
dofollow_links bigint  DEFAULT '0',
canonical bigint  DEFAULT '0',
gov_links bigint  DEFAULT '0',
edu_links bigint  DEFAULT '0',
rss_links bigint  DEFAULT '0',
internal_links bigint DEFAULT '0',
external_links bigint  DEFAULT '0',
ref_domains bigint  DEFAULT '0',
google_Indexed_pages Bigint

);

CREATE TABLE inbound_links(

domain_id BIGINT NOT NULL,
source_url varchar(512) NOT NULL,
target_url varchar(512) NOT NULL,
ahrefs_rank double  DEFAULT '0.0',
domain_rating int  DEFAULT '0',
internal_links bigint  DEFAULT '0',
external_links bigint  DEFAULT '0',
title varchar(255) NOT NULL,
lastvisited date NOT NULL,
alt_text varchar(255) NOT NULL,
anchor_text varchar(255) NOT NULL,
anchor_type varchar(100) NOT NULL,
nofollow boolean  not null  DEFAULT false,

CONSTRAINT inboundlinks_domains_ref FOREIGN KEY (`domain_id`) REFERENCES `inboundlinks_domains` (`domain_id`)

);

/*Expert answer File Attachment Jul 19,2017*/
alter table lxrm_ate_questions  add column file_name varchar(256) null  after answer;

/*Removeing Likes column, Pinterest API not provideing like metric for pinterest channel of SMA tool. Aug 08, 2017*/
ALTER TABLE smt_pinterest drop column smt_pin_likes;

-- 10th July, 2017 - For cleanup the tool table.
UPDATE tool SET show_in_home = 0 WHERE id = 32;

ALTER TABLE tool 
  ADD COLUMN tool_icon_color varchar(10) NULL,
  ADD COLUMN is_android_app TINYINT(1) DEFAULT 0,
  ADD COLUMN android_app_url varchar(1024) NULL,
  ADD COLUMN is_ios_app TINYINT(1) DEFAULT 0,
  ADD COLUMN ios_app_url varchar(1024) NULL;
-- Removing unused colums in tools table
ALTER TABLE tool drop column is_free;
ALTER TABLE tool drop column desc_link;

ALTER TABLE tool drop column is_ios_app;
ALTER TABLE tool drop column is_android_app;

/*13th Sep, 2017 - Adding User persona tables */
create table lxrm_user_persona_category(
persona_category_id SMALLINT PRIMARY KEY AUTO_INCREMENT,
persona_category_name varchar(112) NOT NULL
);

create table lxrm_user_persona_category_info(
persona_category_info_id SMALLINT PRIMARY KEY AUTO_INCREMENT,
persona_category_id SMALLINT NOT NULL,
persona_category_info_name varchar(112) NOT NULL,
CONSTRAINT lxrm_user_persona_category_ref FOREIGN KEY (persona_category_id) REFERENCES lxrm_user_persona_category (persona_category_id)

);
create table lxrm_user_persona_info(
id BIGINT PRIMARY KEY AUTO_INCREMENT,
user_id BIGINT NOT NULL,
persona_category_info_id SMALLINT NOT NULL, 
CONSTRAINT lxrm_user_persona_category_info_ref1 FOREIGN KEY (persona_category_info_id) REFERENCES lxrm_user_persona_category_info (persona_category_info_id),
CONSTRAINT lxrm_persona_info_user FOREIGN KEY (user_id) REFERENCES user (id)
)

/*Dec 04, 2017: Not saving the user card deatils while payment.*/
Drop table lxrm_credit_card_info;
ALTER TABLE lxrm_payment_transaction_stats DROP FOREIGN KEY  lxr_card_tabref1, 
drop column lxr_card_id;

/*Dec 21, 2017: To store payment info for UDAAN app */
CREATE TABLE `lxrm_udaan_transaction_stats` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `state` varchar(10) NOT NULL,
  `intent` tinytext,
  `currency` tinytext,
  `amount` double(10,2) DEFAULT NULL,
  `authorization_code` varchar(100) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `is_card` tinyint(1) NOT NULL,
  PRIMARY KEY (`payment_id`)
);
/*Dec 06, 2017: Added new metric for backlinks html_pages pulling from AHREF API -- Inbound Link Checker tool*/
alter table inboundlinks_domains add column html_pages bigint DEFAULT '0' after ref_domains;
alter table  inbound_links modify column domain_rating double  DEFAULT '0.0';

/*Jan 17, 2018: To store referring domains that contain backlinks to the target. */
alter table  inboundlinks_domains add column refdomain_fetched_date date after fetched_date;
CREATE TABLE inbound_refdomains(
    domain_id BIGINT NOT NULL,
    refdomain varchar(512) NOT NULL,
    backlinks int  DEFAULT '0',
    refpages int  DEFAULT '0',
    first_seen varchar(212) NOT NULL,
    last_visited varchar(212) NOT NULL,
    domain_rating double  DEFAULT '0.0',
    CONSTRAINT inboundlinks_refdomains FOREIGN KEY (`domain_id`) REFERENCES `inboundlinks_domains` (`domain_id`)
);

/* June 5, 2018: Creating new mobile user table*/

CREATE TABLE lxrm_signup_type(
    signup_type TINYINT PRIMARY KEY,
    signup_type_desc VARCHAR(24)
);
CREATE TABLE lxrm_platform(
    platform TINYINT PRIMARY KEY,
    platform_desc VARCHAR(24)
);

CREATE TABLE lxrm_mobile_users(
    user_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) DEFAULT NULL,
    email VARCHAR(100) DEFAULT NULL,
    created_date TIMESTAMP NOT NULL,
    city VARCHAR(255),
    country VARCHAR(8),
    platform TINYINT DEFAULT NULL,
    signup_type TINYINT DEFAULT NULL,
    unsubscribe BOOLEAN DEFAULT 0,
    unsubscirbe_reasion VARCHAR(255),
    unsubscirbe_date TIMESTAMP NULL,
    CONSTRAINT lxrm_mobile_users_signup_type_ref FOREIGN KEY (signup_type) REFERENCES lxrm_signup_type (signup_type ),
    CONSTRAINT lxrm_mobile_user_platform_ref FOREIGN KEY (platform) REFERENCES lxrm_platform (platform )
);

CREATE TABLE lxrm_mobile_tool_usage(
    tool_usage_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created_date TIMESTAMP NOT NULL,
    user_id BIGINT NOT NULL,
    tool_id BIGINT NOT NULL,
    input VARCHAR(255),
    CONSTRAINT lxrm_mobile_tool_usage_user_id_ref FOREIGN KEY (user_id) REFERENCES lxrm_mobile_users (user_id ),
    CONSTRAINT lxrm_mobile_tool_usage_tool_ref FOREIGN KEY (tool_id) REFERENCES tool (id)
);

/*July 23,2018 GDPR changes*/
ALTER TABLE user ADD COLUMN privacy_policy_agreed_time TIMESTAMP NULL  AFTER act_date;

/*Oct 09,2018- To Track API errors*/
CREATE TABLE lxrm_api_errors (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    api_name VARCHAR(55) NOT NULL,
    issue_identified datetime NOT NULL,
    error_message VARCHAR(10024),
    tool_id BIGINT NOT NULL,
    CONSTRAINT `tool_id-ref2` FOREIGN KEY (`tool_id`) REFERENCES `tool` (`id`)
);

ALTER TABLE user ADD COLUMN is_encrypted TINYINT(1) DEFAULT 0 AFTER contact_no;

/*PreSales tools to track pre sales tools usage
created on Sep 07,2019*/
CREATE TABLE presales_tools_usage(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tool_id BIGINT NOT NULL,
    user_email VARCHAR(100) NOT NULL,
    created_date TIMESTAMP NOT NULL,
    number_of_requests INT NOT NULL
);
CREATE TABLE presales_tools_api_usage(
    id BIGINT NOT NULL,
    api_name VARCHAR(100) NOT NULL,
    response_count INT NOT NULL,
    CONSTRAINT presales_api_index_ref FOREIGN KEY (id) REFERENCES presales_tools_usage (id)
);


/*Oct 21, 2019
Migration from Paypal payment gateway to Brain Tree payment service
For mobile apps(Dashly) payer user id is empty
*/
alter table lxrm_payment_transaction_stats modify payer_user_id bigint(21);
alter table lxrm_payment_transaction_stats drop column authorization_code;
alter table lxrm_payment_transaction_stats drop column paypal_payer_id;
alter table lxrm_payment_transaction_stats add column invoice_file_path varchar(100) DEFAULT NULL after payer_user_id;
alter table lxrm_payment_transaction_stats add column payment_service varchar(20) DEFAULT NULL after invoice_file_path;


/*Nov 21, 
Changes to user table to capture social profile data*/
ALTER TABLE user ADD COLUMN is_social_signup VARCHAR(15) NULL DEFAULT null AFTER is_active;

CREATE TABLE user_social_profile(
user_social_id BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 user_id BIGINT(20) default NULL,
 profile_id VARCHAR(500) COLLATE 'utf8mb4_unicode_ci' NULL ,
 social_channel VARCHAR(25) NULL,
 access_token varchar(300) default NULL,
  captured_date TIMESTAMP NOT NULL,
  INDEX fk_user_social_profile_1_idx (user_id ASC),
 CONSTRAINT `fk_user_social_profile_1`
   FOREIGN KEY (`user_id`)
   REFERENCES `user` (`id`)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION
);
