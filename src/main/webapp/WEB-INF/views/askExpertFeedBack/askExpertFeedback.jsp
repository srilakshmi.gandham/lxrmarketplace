
<%-- 
    Document   : askExpertFeedback
    Created on : 7 Oct, 2016, 6:26:00 PM
    Author     : vemanna
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Ask The Expert Feedback | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/askTheExpertFeedback.css">
        <%
            String comment = "";
            if (session.getAttribute("comment") != null) {
                comment = (String) session.getAttribute("comment");
            }
        %>
        <script type="text/javascript">

            // This is by default selected rating
            var userRating = 5;
            var isAnotherUserLoggedIn = false;
            var comment = '<%=comment%>';
            $(document).ready(function () {

                document.getElementById("msg-notification-count").style.display = "none";
                document.getElementById("m-msg-notification-count").style.display = "none";


                //This values comes when user click rating button from mail.
                var exncriptedUserId = getUrlVars()["user-id"];
                var exncriptedQuestionId = getUrlVars()["question-id"];
                var userRatingFromMail = getUrlVars()["rating"];
                if (exncriptedUserId !== null && exncriptedUserId !== undefined && exncriptedQuestionId !== null
                        && exncriptedQuestionId !== undefined && userRatingFromMail !== null && userRatingFromMail !== undefined) {
                    decriptUserIdAndLogin(exncriptedUserId, exncriptedQuestionId, userRatingFromMail);
                }

                if (${userLoggedIn}) {
                    //This block executes when user click 'Help us to improve' from ATE answers page and
                    //User give rating from mail (Just updating users rating and again showing this page for providing comment)

                    //quesId: Question id value assign in ATE anaser page when click 'Help us to improve' and User give rating from mail
                    var quesId = getUrlVars()["question-id"];

                    //rating: Rating param value assign on User given rating from mail only
                    var rating = getUrlVars()["rating"];


                    //Setting question id
                    if (quesId !== null && quesId !== undefined) {
                        $('#questionId').val(quesId);
                    }
                    //Selected rating button
                    if (rating !== null && rating !== undefined) {
                        selectedRating(rating);
                    }

                    var errorMsg = "${errorMessage}";
                    if (errorMsg.trim() !== "") {
                        alert(errorMsg);
                    }

                    if (comment !== "" && comment !== null) {
                        $('#comment').val(comment);
                    }

                }

                $('#feedback-submit').click(function () {
                    if (isAnotherUserLoggedIn) {
                        alert("Please sign out and login with your mail id.");
                        return false;
                    } else {
                        insertATEFeedback();
                    }

                });


            });

            function insertATEFeedback() {

                var quesId = $('#questionId').val();
                var comment = $('#comment').val().trim();
                $('#rating').val(userRating);

                if (quesId !== null && quesId <= 0) {
                    alert("You are not selected any question.");
                    return false;
                }

                if (comment.length > 1000) {
                    alert("Comment limit is 1000 charecter only.");
                    $('#comment').focus();
                    return false;
                }

                var f = $("#askExpertFeedback");
                f.attr('action', "/ask-expert-feedback.html?submit=feedback");
                f.submit();

            }

            function selectedRating(ratingId) {
                $('#2').removeClass('active');
                $('#3').removeClass('active');
                $('#4').removeClass('active');
                $('#5').removeClass('active');

                $('#' + ratingId).addClass('active');
                userRating = ratingId;
            }

            function decriptUserIdAndLogin(exncriptedUserId, exncriptedQuestionId, userRatingFromMail) {
                var params = {userId: exncriptedUserId, questionId: exncriptedQuestionId, rating: userRatingFromMail};
                $.getJSON("/ask-expert-feedback.html?check=login", params,
                        function (data) {
                            if (data[0] === "success") {
                                parent.window.location = data[1];
                            } else if (data[0] === "error") {
                                parent.window.location = data[1];
                            } else {
                                alert(data[0]);
                                isAnotherUserLoggedIn = true;
                            }
                        }
                , "json");

            }

            function getUrlVars() {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                    vars[key] = value;
                });
                return vars;
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 visible-lg"></div>
                <div class="col-lg-8 text-center">
                    <springForm:form  method="POST" modelAttribute="askExpertFeedback" >
                        <p id="rate-text">How would you rate the answer provided by our experts?</p>
                        <div id="ratingBoxes">
                            <span id="2" onclick="selectedRating(this.id);">Poor</span>
                            <span id="3" onclick="selectedRating(this.id);">Fair</span>
                            <span id="4" onclick="selectedRating(this.id);">Good</span>
                            <span id="5" class="active" onclick="selectedRating(this.id);" >Excellent</span>
                        </div>
                        <div>
                            <springForm:hidden path="questionId" />
                            <springForm:hidden path="rating" />
                            <p id="comment-title">Comments:</p>
                            <springForm:textarea path="comment"  /><br>
                            <input type="submit" value="Submit" id="feedback-submit" class="btn"/>
                        </div>
                    </springForm:form >
                </div>
                <div class="col-lg-2 visible-lg"></div>
            </div>
        </div>
        <div style="height: 40px; clear: both"></div>
        <div class="visible-lg visible-md" style="height: 140px; clear: both"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
