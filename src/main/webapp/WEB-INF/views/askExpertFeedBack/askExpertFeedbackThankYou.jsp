
<%-- 
    Document   : askExpertFeedbackThankYou
    Created on : 9 Oct, 2016, 6:42:16 PM
    Author     : vemanna
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>AskTheExpert Feedback Thank you | LXRMarketplace</title>

        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/askTheExpertFeedback.css">

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container-fluid">
            <div class="row text-center">
                <h1 id="thnk-you">Thank you for your feedback!</h1>
                <h2 id="thnk-sharing"> Sharing your experience with us is extremely helpful. </h2>
            </div>
        </div>
        <div style="height: 40px; clear: both"></div>
        <div class="visible-lg visible-md" style="height: 300px; clear: both"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>

