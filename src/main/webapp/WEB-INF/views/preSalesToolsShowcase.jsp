<%-- 
    Document   : preSalesToolsShowcase
    Created on : 27 Feb, 2019, 4:01:39 PM
    Author     : NE16T1213/sagar.K
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Free PreSales Tools - LXRMarketplace Toolkit</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/emptyHeader.jsp"%>
        <div class="container-fluid lxr-fullbg">
            <div class="clearfixdiv" style="height: 45px !important;"></div>
            <div class="lxr-toolbox" id="lxr-presales-toolbox"> 
                <div id="preSales-tools">
                    <blockquote class="blockquote">Free PPC <span>Tools: </span></blockquote>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-3 lxr-tooladviser form-group">
                            <div class="lxr-tool-showcase col-xs-12" style="border: 1px #2c3427 solid;background: white;border-radius: 5px;padding: 0;">
                                <a href="ad-spy-v2.html">
                                    <div class="col-item lxr-items" style="background-color:#2c3427;">
                                        <div class="post-img-content">
                                            <span class="post-title">
                                                <span class="pull-left"><img src="../../resources/images/tool-icons/ADS.png"></span>
                                            </span>
                                        </div>
                                        <div class="price col-lg-12" style="padding-left: 8px;">
                                            <h4 class="align-text-bottom lxr-title" title="Ad Spy">Ad Spy</h4> 
                                        </div>
                                    </div>
                                    <div class="info">
                                        <ul class="fa-ul" style="height: 96px">
                                            <li><i class="fa-li fa fa-check" style="color:#2c3427"></i>Instantly uncover website's Visits, Ad Presence and Estimated Monthly Adwords Budget details through SimilarWeb and SpyFu API's .</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-3 lxr-tooladviser form-group">
                            <div class="lxr-tool-showcase col-xs-12" style="border: 1px #0051CB solid;background: white;border-radius: 5px;padding: 0;">
                                <a href="preliminary-search.html">
                                    <div class="col-item lxr-items" style="background-color:#0051CB;">
                                        <div class="post-img-content">
                                            <span class="post-title">
                                                <span class="pull-left"><img src="../../resources/images/tool-icons/PSMA.png"></span>
                                            </span>
                                        </div>
                                        <div class="price col-lg-12" style="padding-left: 8px;">
                                            <h4 class="align-text-bottom lxr-title" title="Preliminary Search Marketing Analysis">Preliminary Search Marketin..</h4>
                                        </div>
                                    </div>
                                    <div class="info">
                                        <ul class="fa-ul" style="height: 96px">
                                            <li><i class="fa-li fa fa-check" style="color:#0051CB"></i>Instantly uncover Website Domain traffic and SEO performance details through SimilarWeb and LXRSEO APIs.</li>
                                            <li><i class="fa-li fa fa-check" style="color:#0051CB"></i>Identify the Channel Performance, Paid Search Performance, Search Traffic By Engine, Device Performance Scores.</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-3 lxr-tooladviser form-group">
                            <div class="lxr-tool-showcase col-xs-12" style="border: 1px #184F78 solid;background: white;border-radius: 5px;padding: 0;">
                                <a href="domain-finder.html">
                                    <div class="col-item lxr-items" style="background-color:#184F78;">
                                        <div class="post-img-content">
                                            <span class="post-title">
                                                <span class="pull-left" style="padding: 8px;"><img src="../../resources/images/tool-icons/DF.png"></span>
                                            </span>
                                        </div>
                                        <div class="price col-lg-12" style="padding-left: 8px;">
                                            <h4 class="align-text-bottom lxr-title" title="Domain Finder">Domain Finder</h4>
                                        </div>
                                    </div>
                                    <div class="info">
                                        <ul class="fa-ul" style="height: 96px">
                                            <li><i class="fa-li fa fa-check" style="color:#184F78"></i>Instantly find the Domain / URL for the list of company names.</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv" style="height: 105px !important;"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>

