
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.WhoIsInfo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description"
              content="Try the Domain Age Checker at LXRMarketplace. Finds estimated age of the domains, displays information to monitor & compare competitors' domains. Try it now!">
        <meta name="keywords"
              content="Domain Age Checker, Whois Tool, Domain Age Tool">
        <title>Domain Age Checker and Whois Tool | LXRMarketplace.com</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/domainAgeChecker.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <%
            int toolDBId = 21;
            WhoIsInfo whoIsInfo = null;
            if (session.getAttribute("whoIsInfo") != null) {
                whoIsInfo = (WhoIsInfo) session.getAttribute("whoIsInfo");
                String status = "";
                if (whoIsInfo.getHttpStatus() == 200) {
                    status = "Working";
                } else if (whoIsInfo.getHttpStatus() == 301) {
                    status = "Permanent Redirection";
                } else if (whoIsInfo.getHttpStatus() >= 302 || whoIsInfo.getHttpStatus() < 400) {
                    status = "Temporary Redirection";
                } else if (whoIsInfo.getHttpStatus() >= 400 || whoIsInfo.getHttpStatus() <= 600) {
                    status = "Internal Server Error";
                }
            }
            String redirectedUrl = "";
            if (session.getAttribute("redirectedUrl") != null && !session.getAttribute("redirectedUrl").toString().trim().equals("")) {
                redirectedUrl = (String) session.getAttribute("redirectedUrl");
            }
            pageContext.setAttribute("whoIsInfo", whoIsInfo);
            pageContext.setAttribute("redirectedUrl", redirectedUrl);
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${getSuccess}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
//                toolLimitExceeded();
            <c:if test="${!notWorkingUrl.equals('')}">
                $("#notWorkingUrl").html("${notWorkingUrl}");
            </c:if>
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                          && (whoIsInfo != '' && whoIsInfo != null)}">
                $("#editImport").show();
                $("#domain").addClass("gray");
            </c:if>
                $('#getResult').on('click', function () {
                    resultLoginValue = true;
                    /*   Login POPUP on tool limit exceeded */
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if ($('#domain').val().trim() === "" || $('#domain').val() === null || $('#domain').val() === 0 || $('#domain').val() === "Enter Website URL") {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $('#domain').focus();
                        return false;
                    } else if (!urlValidation($('#domain').val().trim())) {
                        $("#notWorkingUrl").html("Please enter a valid URL");
                        $('#domain').focus();
                        return false;
                    }
                    var $this = $(this);
                    $this.button('loading');
                    document.getElementById("domain").readOnly = true;
                    var f = $("#domainAgeChecker");
                    f.attr('method', "POST");
                    f.attr('action', "domain-age-checker-tool.html?getResult=getResult");
                    f.submit();
                });
                $('#domain').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                $('#editImport, #domain').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#domain").removeClass("gray");
                });
                // Sendmail functionality
                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        sendReportToMail();
                        return false;
                    }
                });
                $('#email').click(function () {
                    $('#mail-sent-status').hide();
                    return false;
                });

            });

            function sendReportToMail() {
                $('#mail-sent-status').hide();
                var emailid = $.trim($("#email").val());
                var status = true;
                var statusMsg = "";
                if (emailid === "") {
                    statusMsg = "Please provide your email";
                    status = false;
                } else if (!isValidMail(emailid)) {
                    statusMsg = "Please enter a valid email address.";
                    status = false;
                }
                if (!status) {
                    $("#email").focus();
                    $('#mail-sent-status').show();
                    $("#mail-sent-status").html(statusMsg);
                    $('#mail-sent-status').css("color", "red");
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/domain-age-checker-tool.html?download=sendmail&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {

            }
            function getUserToolInputs(website, userInput) {
                $('#domain').val(website);
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }

        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <!--table part start-->
        <div class="container">
            <div class="row">
                <springForm:form commandName="domainAgeChecker">
                    <div class="col-lg-12">
                        <div class="form-group col-lg-10 col-md-9 col-sm-12">
                            <div class="icon-addon addon-lg">
                                <springForm:input placeholder="Enter your URL" path="domain" cssClass="form-control input-lg"/>
                                <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                            </div>
                            <span id="notWorkingUrl" class="lxr-danger text-left"></span>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn">
                                <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="getResult" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> ANALYZING">
                                    ANALYZE
                                </button>
                            </div>
                        </div>  
                    </div>
                </springForm:form>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <c:if test="${status.equals('success') || fn:contains(uri, 'backToSuccessPage=')&& (whoIsInfo != '' && whoIsInfo != null)}">
            <!--vertical tab system-->
            <div class="container-fluid">
                <div class="row lxrtool-tabs">
                    <div class="tabs">
                        <div class="tab">
                            <button class="tab-toggle active lxrtool-tabs-heading">
                                <span>Domain Registration Details</span>
                                <p>
                                    Domain registration is the process of registering a domain name, which identifies 
                                    one or more IP addresses with a name that is easier to remember and 
                                    use in URLs to identify particular Web pages.
                                </p>
                            </button>
                        </div>
                        <div class="content main-content-1 active">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1" style="padding: 0px">
                                <!--<div id="firstcontent-1">-->
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Domain Registration Details</span>
                                        <!--<p style="padding: 10px 0px;" id="issue-text-1">These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS.</strong></p>-->
                                        <!--<p>You can improve metrics by following these <strong  class="lxr-steps nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS.</strong></p>-->
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div id="result-content-1">
                                            <table class="table table-striped lxrtool-table">
                                                <thead>
                                                    <tr>
                                                        <td colspan="2"><strong>Domain Registration Details</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Registered Domain Name</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.registeredDomName != null}">
                                                                <td>${whoIsInfo.registeredDomName}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Estimated Domain Age</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.domainAge != null}">
                                                                <td>${whoIsInfo.domainAge}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Domain Created Date</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.createdDate != null}">
                                                                <td>${whoIsInfo.createdDate}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Domain Updated Date</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.updatedDate != null}">
                                                                <td>${whoIsInfo.updatedDate}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Domain expires Date</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.regExpiration != null}">
                                                                <td>${whoIsInfo.regExpiration}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>Domain Registrant Details</span>
                                <p>
                                    The person or business that registers a domain name is called the domain name registrant.
                                </p>
                            </button>
                        </div>
                        <div class="content main-content-2">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2" style="padding: 0px">
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Domain Registrant Details</span>
                                        <!--<p style="padding: 10px 0px;" id="issue-text-2">These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(2);">STEPS.</strong></p>-->
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div id="result-content-2">
                                            <table class="table table-striped lxrtool-table">
                                                <thead>
                                                    <tr>
                                                        <td colspan="2"><strong>Domain Registrant Details</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Domain Registrant Name</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.registrantName != null}">
                                                                <td>${whoIsInfo.registrantName}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Registrant Email</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.registrantEmail != null}">
                                                                <td>${whoIsInfo.registrantEmail}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Registrant Address</td>
                                                        <c:choose>
                                                            <c:when test="${(whoIsInfo.registrantStreet.equals('NA')) 
                                                                            && (whoIsInfo.registrantCity.equals('NA')) 
                                                                            && (whoIsInfo.registrantState.equals('NA')) 
                                                                            && (whoIsInfo.registrantPostalCode.equals('NA'))}">
                                                                    <td>NA</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="registrantAddress" value=""/>
                                                                <c:if test="${!whoIsInfo.registrantStreet.equals('NA')}">
                                                                    <c:set var="registrantAddress" value="${whoIsInfo.registrantStreet},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.registrantCity.equals('NA')}">
                                                                    <c:set var="registrantAddress" value="${registrantAddress} ${whoIsInfo.registrantCity},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.registrantState.equals('NA')}">
                                                                    <c:set var="registrantAddress" value="${registrantAddress} ${whoIsInfo.registrantState},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.registrantPostalCode.equals('NA')}">
                                                                    <c:set var="registrantAddress" value="${registrantAddress} ${whoIsInfo.registrantPostalCode}."/>
                                                                </c:if>
                                                                <td>${registrantAddress}</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Registrant Phone</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.registrantPhone != null}">
                                                                <td>${whoIsInfo.registrantPhone}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>Administrative Contact Details</span>
                                <p>
                                    In domain name system terminology, 
                                    the administrative contact is the individual who is authorized by the registrant 
                                    to interact with Network Solutions or domain name registrar to answer questions 
                                    about the domain name registration and registrant.
                                </p>
                            </button>
                        </div>
                        <div class="content main-content-3">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3" style="padding: 0px">
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Administrative Contact Details</span>
                                        <!--                                        <p style="padding: 10px 0px;" id="issue-text-3">These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(3);">STEPS.</strong></p>-->
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div id="result-content-3">
                                            <table class="table table-striped lxrtool-table">
                                                <thead>
                                                    <tr>
                                                        <td colspan="2"><strong>Administrative Contact Details</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Name</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.adminName != null}">
                                                                <td>${whoIsInfo.adminName}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.adminEmail != null}">
                                                                <td>${whoIsInfo.adminEmail}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Address</td>
                                                        <c:choose>
                                                            <c:when test="${(whoIsInfo.adminStreet.equals('NA')) 
                                                                            && (whoIsInfo.adminCity.equals('NA')) 
                                                                            && (whoIsInfo.adminState.equals('NA')) 
                                                                            && (whoIsInfo.adminPostalCode.equals('NA'))}">
                                                                    <td>NA</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="adminAddress" value=""/>
                                                                <c:if test="${!whoIsInfo.adminStreet.equals('NA')}">
                                                                    <c:set var="adminAddress" value="${whoIsInfo.adminStreet},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.adminCity.equals('NA')}">
                                                                    <c:set var="adminAddress" value="${adminAddress} ${whoIsInfo.adminCity},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.adminState.equals('NA')}">
                                                                    <c:set var="adminAddress" value="${adminAddress} ${whoIsInfo.adminState},"/>
                                                                </c:if>
                                                                <c:if test="${!whoIsInfo.adminPostalCode.equals('NA')}">
                                                                    <c:set var="adminAddress" value="${adminAddress} ${whoIsInfo.adminPostalCode}."/>
                                                                </c:if>
                                                                <td>${adminAddress}</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone</td>
                                                        <c:choose>
                                                            <c:when test="${whoIsInfo.adminPhone != null}">
                                                                <td>${whoIsInfo.adminPhone}</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>NA</td>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>Technical Contact Details</span>
                                <p>
                                    The Technical Contact will give you all the details about the person responsible 
                                    for maintaining the DNS nameservers associated with the domain name. 
                                    This will give you all the information about the domain owner.
                                </p>
                            </button>
                        </div>
                        <div class="content main-content-4">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-4" style="padding: 0px">
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Technical Contact Details</span>
                                        <!--                                        <p style="padding: 10px 0px;" id="issue-text-4">These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(4);">STEPS.</strong></p>-->
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div id="result-content-3">
                                            <div id="result-content-4">
                                                <table class="table table-striped lxrtool-table">
                                                    <thead>
                                                        <tr>
                                                            <td colspan="2"><strong>Technical Contact Details</strong></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Name</td>
                                                            <c:choose>
                                                                <c:when test="${whoIsInfo.techName != null}">
                                                                    <td>${whoIsInfo.techName}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td>NA</td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <c:choose>
                                                                <c:when test="${whoIsInfo.techEmail != null}">
                                                                    <td>${whoIsInfo.techEmail}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td>NA</td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                        <tr>
                                                            <td>Address</td>
                                                            <c:choose>
                                                                <c:when test="${(whoIsInfo.techStreet.equals('NA')) 
                                                                                && (whoIsInfo.techCity.equals('NA')) 
                                                                                && (whoIsInfo.techState.equals('NA')) 
                                                                                && (whoIsInfo.techPostalCode.equals('NA'))}">
                                                                        <td>NA</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:set var="techAddress" value=""/>
                                                                    <c:if test="${!whoIsInfo.techStreet.equals('NA')}">
                                                                        <c:set var="techAddress" value="${whoIsInfo.techStreet},"/>
                                                                    </c:if>
                                                                    <c:if test="${!whoIsInfo.techCity.equals('NA')}">
                                                                        <c:set var="techAddress" value="${techAddress} ${whoIsInfo.techCity},"/>
                                                                    </c:if>
                                                                    <c:if test="${!whoIsInfo.techState.equals('NA')}">
                                                                        <c:set var="techAddress" value="${techAddress} ${whoIsInfo.techState},"/>
                                                                    </c:if>
                                                                    <c:if test="${!whoIsInfo.techPostalCode.equals('NA')}">
                                                                        <c:set var="techAddress" value="${techAddress} ${whoIsInfo.techPostalCode}."/>
                                                                    </c:if>
                                                                    <td>${techAddress}</td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone</td>
                                                            <c:choose>
                                                                <c:when test="${whoIsInfo.techPhone != null}">
                                                                    <td>${whoIsInfo.techPhone}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td>NA</td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row download-report-row">
                    <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                    <div class="col-lg-9 col-sm-12 col-xs-12 download-report-div">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                            <p style="margin: 3% 0;">Download complete report here: </p>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding: 0;">
                            <div class="input-group" style="padding: 0;">
                                <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                </div>
                            </div>
                            <div id="mail-sent-status" class="emptyData"></div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <!--Recommended part start-->
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <!--Recommended part end-->
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>