
<%@page import="lxr.marketplace.crawler.LxrmUrlStats"%>
<%@page import="java.util.List"%>
<%@page import="lxr.marketplace.brokenlinkchecker.BrokenLinkCheckerResults"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Broken link Checker Tool | LXR Marketplace</title>
        <meta name="description"
              content="Visit LXRMarketplace for the Free Broken Link Checker. Quickly and easily review your website for any broken links. Try it out today!">
        <meta name="keywords"
              content=" Broken Link Checker Tool, Broken Link Checker Analyzer">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/brokenLinkChecker.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <%
            int toolDBId = 31;
            BrokenLinkCheckerResults brokenLinkCheckerResults = null;
            if (session.getAttribute("brokenLinkCheckerResults") != null) {
                brokenLinkCheckerResults = (BrokenLinkCheckerResults) session.getAttribute("brokenLinkCheckerResults");
                int totalUrlsCount = brokenLinkCheckerResults.getNotWorkingUrlCount() + brokenLinkCheckerResults.getPermanentRedirectingUrls()
                        + brokenLinkCheckerResults.getTemporaryRedirectingUrls() + brokenLinkCheckerResults.getTooManyRedirectionUrls();
                pageContext.setAttribute("totalUrlsCount", totalUrlsCount);
            }
            pageContext.setAttribute("brokenLinkCheckerResults", brokenLinkCheckerResults);
            String reqParam = request.getParameter("report");
            String downloadReportType = request.getParameter("report-type");
            pageContext.setAttribute("reqParam", reqParam);
            pageContext.setAttribute("downloadReportType", downloadReportType);
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${status}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat("${downloadReportType}");
                }
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                          && (brokenLinkCheckerResults != '' && brokenLinkCheckerResults != null)}">
                $("#editImport").show();
                $("#domain").addClass("gray");
            </c:if>
            <c:if test="${!notWorkingUrl.equals('')}">
                $("#notWorkingUrl").html("${notWorkingUrl}");
            </c:if>
            <%--<c:set var="allUrlsStatus" value="0"></c:set>--%>
            <c:choose>
                <c:when test="${brokenLinkCheckerResults.notWorkingUrlCount > 0}">
                showData('404', 'result-content-1');
                $(".lxrtool-tabsheading1").addClass("active");
                $(".main-content-1").addClass("active");
                </c:when>
                <c:when test="${brokenLinkCheckerResults.tooManyRedirectionUrls > 0}">
                showData('0', 'result-content-2');
                $(".lxrtool-tabsheading2").addClass("active");
                $(".main-content-2").addClass("active");
                </c:when>
                <c:when test="${brokenLinkCheckerResults.temporaryRedirectingUrls > 0}">
                showData('302', 'result-content-3');
                $(".lxrtool-tabsheading3").addClass("active");
                $(".main-content-3").addClass("active");
                </c:when>
                <c:when test="${brokenLinkCheckerResults.permanentRedirectingUrls > 0}">
                showData('301', 'result-content-4');
                $(".lxrtool-tabsheading4").addClass("active");
                $(".main-content-4").addClass("active");

                    <%--<c:otherwise>--%>
//                showData('all', 'result-content-5');
//                $(".lxrtool-tabsheading5").addClass("active");
//                $(".main-content-5").addClass("active");
                    <%--</c:otherwise>--%>
                </c:when>
            </c:choose>
                $(".active").css("display", "block");
            <%--<c:if test="${notWorkingUrl != null && notWorkingUrl != ''}">--%>
//                $("#notWorkingUrl").show();
//                $("#notWorkingUrl").html("${notWorkingUrl}");
            <%--</c:if>--%>
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();
                $("#getResult").bind('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var backLinkURL = "";
                    if ($('#domain').val().trim() === "" || $('#domain').val() === null || $('#domain').val() === 0 || $('#domain').val() === "Enter Website URL") {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $('#domain').focus();
                        return false;
                    } else if (!urlValidation($('#domain').val().trim())) {
                        $("#notWorkingUrl").html("Please enter a valid URL");
                        $('#domain').focus();
                        return false;
                    }
                    var $this = $(this);
                    $this.button('loading');
                    $(".srch-term").prop("disabled", true);
                    backLinkURL = $('#domain').val();
                    $("#notWorkingUrl").empty();
                    refreshPage();
                    $.ajax({
                        type: "POST",
                        url: "/broken-link-checker-tool.html?getResult=getResult",
                        processData: true,
                        data: {"backLinkURL": backLinkURL},
//                        data: {"backLinkURL": backLinkURL, "domain": $('#domain').val()},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function (xhr, textstatus) {
                            // xhr.responseText contains the response from the server
                            var allheaders = xhr.getAllResponseHeaders();
                            if (allheaders.indexOf("limitexceed") > 0) {
                                location.reload();
                            }
                            $this.button('reset');
                        }
                    });
                });
                $('#domain').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                // Sendmail functionality
                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        sendReportToMail();
                        return false;
                    }
                });
                $('#email').click(function () {
                    $('#mail-sent-status').hide();
                    return false;
                });

                $('#editImport, #domain').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#domain").removeClass("gray");
                });

                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }
            });
            function sucess() {
                getUpdate();
            }
            function refreshPage() {
                setTimeout('sucess()', 2000);
            }
            function getUpdate() {
                $.ajax({
                    type: "POST",
                    url: "/broken-link-checker-tool.html?getdata=getdata",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        var backLinkUrl = data[0];
                        if (backLinkUrl === "valid") {
                            var crawlStatus = data[3];
                            if (crawlStatus === "true") {
                                showSucessPage();
                            } else {
//                                blcTotUrlCnt = data[1];
                                $("#analyzingCount").show();
                                $("#analyzingCount").html(data[2] + " URL(s) analyzed.......");
                                refreshPage();
                            }
                        } else {
                            $("#analyzingCount").hide();
                            $("#notWorkingUrl").html(backLinkUrl);
                        }
                    }
                });
            }
            function showSucessPage() {
                var f = $("#brokenLinkCheckerTool");
                f.attr('action', "broken-link-checker-tool.html?backLink=backLink");
                f.submit();
            }

            function showData(statusCode, divId) {
                var statusMessage = "";
                var color = "";
                var resultInfo = "";
                var urlsCount = "";
                if (statusCode === "404") {
                    statusMessage = "Page Not Found";
                    color = "red";
                    urlsCount = "${brokenLinkCheckerResults.notWorkingUrlCount}";
                } else if (statusCode === "0") {
                    statusMessage = "Too Many Redirections";
                    color = "#EB5757";
                    urlsCount = "${brokenLinkCheckerResults.tooManyRedirectionUrls}";
                } else if (statusCode === "302") {
                    statusMessage = "Temporary Redirection";
                    color = "#F2C94C";
                    urlsCount = "${brokenLinkCheckerResults.temporaryRedirectingUrls}";
                } else if (statusCode === "301") {
                    statusMessage = "Permanent Redirection";
                    color = "green";
                    urlsCount = "${brokenLinkCheckerResults.permanentRedirectingUrls}";
//                } else if (statusCode === "200") {
//                    statusMessage = "Working";
//                    color = "green";
//                    urlsCount = "${brokenLinkCheckerResults.workingUrlCount}";
                } else {
//                    resultInfo += showData('404', '');
//                    resultInfo += showData('0', '');
//                    resultInfo += showData('302', '');
//                    resultInfo += showData('301', '');
//                    resultInfo += showData('200', '');
//                    color = "#2F80ED";
                }
            <c:if test="${brokenLinkCheckerResults != null}">
                if ((divId !== "" && ($("#" + divId).html() === "") && statusCode !== "all")
                        || (divId === "" && statusCode !== "all")) {
                    resultInfo += '<table class="table table-striped lxrtool-table">'
                            + '<thead>'
                            + '<tr>'
                            + '<th style="border-right: 10px white solid;width: 60%;">' + urlsCount + ' URL(s) with '
                            + '<span style="color: ' + color + ';">' + statusCode + '</span>'
                            + '<span>&nbsp;&nbsp;&nbsp;STATUS MESSAGE: </span> '
                            + '<span style="color: ' + color + ';">' + statusMessage + '</span>'
                            + '</th>'
                            + '<th>BASE URL</th>'
                            + '</tr>'
                            + '</thead>'
                            + '<tbody>';
                    var urlInfo = "";
                <c:forEach items="${brokenLinkCheckerResults.lxrmurlStatsList}" var="lxrmUrlStats">
                    if ("${lxrmUrlStats.statusCode}" === statusCode) {
                        urlInfo += '<tr>'
                                + '<td><a href="${lxrmUrlStats.url}" target="_blank">${lxrmUrlStats.url}</a></td>';
                        var baseUrl = "${lxrmUrlStats.baseUrl}";
                        if (baseUrl !== "") {
                            urlInfo += '<td><a href="${lxrmUrlStats.baseUrl}" target="_blank">${lxrmUrlStats.baseUrl}</a></td>';
                        } else {
                            urlInfo += '<td class="text-center">-</td>';
                        }
                        urlInfo += '</tr>';
                    }
                </c:forEach>
                    resultInfo += urlInfo;
                    resultInfo += '</tbody>'
                            + '</table>';
                    if (urlInfo === "") {
                        resultInfo = "";
                    }
                }
                if (divId !== "" && $("#" + divId).html() === "") {
                    $("#" + divId).html(resultInfo);
                }
            </c:if>
                return resultInfo;
            }
            // send mail and download report to user
            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    var f = $("#brokenLinkCheckerTool");
                    f.attr('method', "POST");
                    f.attr('action', "/broken-link-checker-tool.html?download=download&report-type=" + reportType);
                    f.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function sendReportToMail() {
                $('#mail-sent-status').hide();
                var emailid = $.trim($("#email").val());
                var status = true;
                var statusMsg = "";
                if (emailid === "") {
                    statusMsg = "Please provide your email";
                    status = false;
                } else if (!isValidMail(emailid)) {
                    statusMsg = "Please enter a valid email address.";
                    status = false;
                }
                if (!status) {
                    $("#email").focus();
                    $('#mail-sent-status').show();
                    $("#mail-sent-status").html(statusMsg);
                    $('#mail-sent-status').css("color", "red");
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/broken-link-checker-tool.html?download=sendmail&report-type=sendmail&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }
            //  Ask Expert Block
            function getToolRelatedIssues() {
                var url = $('#domain').val();
                $.ajax({
                    type: "POST",
                    url: "/broken-link-checker-tool.html?ate='ate'&domain=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                $('#domain').val(website);
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <!--<div class="hidden-xs clearfixdiv-common"></div>-->
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <!--table part start-->
        <div class="container">
            <!--<div class="">-->
            <springForm:form commandName="brokenLinkCheckerTool" method="POST">
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="domain" cssClass="form-control input-lg"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="notWorkingUrl" class="lxr-danger text-left" >${notWorkingUrl}</span>
                        <p id="analyzingCount" style="padding: 1%;text-align: center;color: #2F80ED;display: none;"></p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12" id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>  
                </div>
            </springForm:form>
            <!--</div>-->
        </div>
        <div class="clearfixdiv"></div>
        <!--   Success Page -->
        <c:if test="${status.equals('success') || fn:contains(uri, 'backToSuccessPage=') 
                      && (brokenLinkCheckerResults != '' && brokenLinkCheckerResults != null)}">
            <c:choose>
                <c:when test="${totalUrlsCount > 0}">
                    <!--vertical tab system-->
                    <div class="container-fluid">
                        <div class="row lxrtool-tabs">
                            <div class="tabs">
                                <!--1-->
                                <c:if test="${brokenLinkCheckerResults.notWorkingUrlCount > 0}">
                                    <%--<c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>--%>
                                    <div class="tab" onclick="showData('404', 'result-content-1');">
                                        <button class="tab-toggle active lxrtool-tabsheading1 lxrtool-tabs-heading">
                                            <span class="lxr-danger">${brokenLinkCheckerResults.notWorkingUrlCount} </span> <span class="lxr-danger">NONWORKING URLs</span>
                                            <p>
                                                The HTTP 404 Not Found Error means that the web page you were trying to reach could not be found on the server. It is a Client-side Error which means that either the page has been removed or moved.
                                            </p>
                                        </button>
                                    </div>
                                    <!--table-->
                                    <div class="content main-content-1 active">
                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lxr-panel-heading">
                                                    <span class="panel-title lxr-danger">NONWORKING URLs</span>
                                                    <p id="issue-text-1">
                                                        404 errors can hurt your website organic rankings because 
                                                        it delivers a bad User-experience to the visitor.
                                                        There are three <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">METHODS</strong> you can use to fix 404 not found errors for your website.
                                                    </p>
                                                </div>
                                                <div class="panel-body lxr-panel-body">
                                                    <div id="result-content-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--how to fix-->
                                        <div id="howtofix-1" style="display: none;">
                                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading lxr-panel-heading">
                                                        <span class="panel-title lxr-light-danger">How to fix broken links</span>
                                                        <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                                    </div>
                                                    <div class="panel-body lxr-panel-body">
                                                        <h5>Steps for fixing broken links:</h5>
                                                        <p>1. Permanent 301 should be placed, if a page is important.</p>
                                                        <p>2. Correct the Source Link.</p>
                                                        <p>3. Restore Deleted Pages or redirect them to related category pages.</p>

                                                        <div class="lxr-panel-footer">
                                                            <h3><spring:message code="ate.expert.label.2" /></h3>
                                                            <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <!--2-->

                                <c:if test="${brokenLinkCheckerResults.tooManyRedirectionUrls > 0}">
                                    <%--<c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>--%>
                                    <div class="tab" onclick="showData('0', 'result-content-2');">
                                        <button class="tab-toggle lxrtool-tabsheading2 lxrtool-tabs-heading">
                                            <span class="lxr-light-danger">${brokenLinkCheckerResults.tooManyRedirectionUrls} </span> <span class="lxr-light-danger">TOO MANY REDIRECTIONS</span>
                                            <p>
                                                If your website is redirecting more than 2 times called too many redirections.
                                            </p>
                                        </button>
                                    </div>
                                    <div class="content main-content-2">
                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lxr-panel-heading">
                                                    <span class="panel-title lxr-light-danger">Too Many Redirection URLs</span>
                                                    <p id="issue-text-2">
                                                        Too many redirections is a blackhat SEO strategy to rank keywords. 
                                                        Avoid too many redirections to save your site from the penalty. 
                                                        Follow the below <strong class="partially_notwork" onClick ="hideContentAndShowHowToFix(2);">STEPS</strong> to change too many redirections.
                                                    </p>
                                                </div>
                                                <div class="panel-body lxr-panel-body">
                                                    <div id="result-content-2"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--how to fix-->
                                        <div  id="howtofix-2" style="display: none;">
                                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading lxr-panel-heading">
                                                        <span class="panel-title lxr-light-danger">How to fix broken links</span>
                                                        <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                                    </div>
                                                    <div class="panel-body lxr-panel-body">
                                                        <h5>STEPS</h5>
                                                        <p> 
                                                            It depends on the website platform or content management system, 
                                                            but after identifying the redirects it is better to move those 
                                                            temporary or too many redirections to 1 permanent redirect or 301.
                                                        </p>
                                                        <div class="lxr-panel-footer">
                                                            <h3><spring:message code="ate.expert.label.2" /></h3>
                                                            <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <!--3-->
                                <c:if test="${brokenLinkCheckerResults.temporaryRedirectingUrls > 0}">
                                    <%--<c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>--%>
                                    <div class="tab" onclick="showData('302', 'result-content-3');">
                                        <button class="tab-toggle lxrtool-tabsheading3 lxrtool-tabs-heading">
                                            <span class="lxr-warning">${brokenLinkCheckerResults.temporaryRedirectingUrls} </span> <span class="lxr-warning">TEMPORARY REDIRECTIONS</span>
                                            <p>
                                                302 is a temporary redirect, if you want to move your website URL on a temporary base, you can use this.
                                            </p>
                                        </button>
                                    </div>
                                    <div class="content main-content-3">
                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3" style="padding: 0px;">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lxr-panel-heading">
                                                    <span class="panel-title lxr-warning">Temporary Redirections URLs</span>
                                                    <p id="issue-text-3">
                                                        It’s quite dangerous to use a 302 redirect by mistake. 
                                                        It will really hurt your site’s search engine visibility. 
                                                        Follow the <strong class="total_url" onClick ="hideContentAndShowHowToFix(3);">STEPS</strong> to change temporary redirection.
                                                    </p>
                                                </div>
                                                <div class="panel-body lxr-panel-body">
                                                    <div id="result-content-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--how to fix-->
                                        <div  id="howtofix-3" style="display: none;">
                                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading lxr-panel-heading">
                                                        <span class="panel-title lxr-light-danger">How to fix Temporary Redirections</span>
                                                        <span class="glyphicon glyphicon-remove pull-right" id="closebox3" onclick="hideHowToFixAndShowContent(3);"></span>
                                                    </div>
                                                    <div class="panel-body lxr-panel-body">
                                                        <h5>Steps for fixing temporary redirections:</h5>
                                                        <p> Note: This is only used for the temporary redirect in case of emergency or technical difficulties for the domain.</p>
                                                        <p> It depends on your content management system or the platform which you are using, usually, it is done through access file by using 302 redirects.</p>
                                                        <div class="lxr-panel-footer">
                                                            <h3><spring:message code="ate.expert.label.2" /></h3>
                                                            <button class="btn" onclick="submitATEQuery('3');">Get Help</button>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${brokenLinkCheckerResults.permanentRedirectingUrls > 0}">
                                    <%--<c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>--%>
                                    <!--4-->
                                    <div class="tab" onclick="showData('301', 'result-content-4');">
                                        <button class="tab-toggle lxrtool-tabsheading4 lxrtool-tabs-heading">
                                            <span class="lxr-success">${brokenLinkCheckerResults.permanentRedirectingUrls} </span> <span class="lxr-success">PERMANENT REDIRECTIONS</span>
                                            <p>
                                                A 301 redirect is a permanent redirect. In most instances, 
                                                the 301 redirect is the best method for implementing redirects on a website.
                                            </p>
                                        </button>
                                    </div>
                                    <div class="content main-content-4">
                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-4">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lxr-panel-heading">
                                                    <span class="panel-title lxr-success">Permanent Redirection URLs</span>
                                                    <p id="issue-text-4">
                                                        If we will not do permanent redirection, so we may lose our ranking in SERP. 
                                                        When you have two or more URLs which are temporary redirected, 
                                                        These are the <strong  class="total_url" onClick ="hideContentAndShowHowToFix(4);">STEPS</strong> to make permanent redirection.
                                                    </p>
                                                </div>
                                                <div class="panel-body lxr-panel-body">
                                                    <div id="result-content-4"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--how to fix-->
                                        <div id="howtofix-4" style="display: none;">
                                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading lxr-panel-heading">
                                                        <span class="panel-title lxr-light-danger">How to fix Temporary Redirections</span>
                                                        <span class="glyphicon glyphicon-remove pull-right" id="closebox4" onclick="hideHowToFixAndShowContent(4);"></span>
                                                    </div>
                                                    <div class="panel-body lxr-panel-body">
                                                        <h5>Steps for fixing temporary redirections:</h5>
                                                        <p> It depends on your content management system or the platform which you are using, usually, it is done through access file by using 301 redirects.</p>
                                                        <div class="lxr-panel-footer">
                                                            <h3><spring:message code="ate.expert.label.2" /></h3>
                                                            <button class="btn" onclick="submitATEQuery('4');">Get Help</button>
                                                        </div>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <%--<c:if test="${allUrlsStatus > 1 || allUrlsStatus == 0}">
                            <!--5-->
                            <div class="tab" onclick="showData('all', 'result-content-5');">
                                <button class="tab-toggle lxrtool-tabsheading5 lxrtool-tabs-heading">
                                    <span class="lxr-primary">${totalUrlsCount} </span> <span class="lxr-primary">TOTAL URLs</span>
                                    <p>It contains all status code URLs like Non-working URLs, Temporary Redirections URLs, Permanent Redirection URLs etc.</p>
                                </button>
                            </div>
                            <div class="content main-content-5">
                                <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-primary">TOTAL URLs</span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <div id="result-content-5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>--%>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row download-report-row">
                            <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                            <div class="col-md-9 col-sm-12 col-xs-12 download-report-div">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                                    <p style="margin: 3% 0;">Download complete report here: </p>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                        </div>
                                    </div>
                                    <div id="mail-sent-status" class="emptyData"></div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-2">
                                    <div class="dropdown">
                                        <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                        <div id="downloadAnalysisReport" class="dropdown-content">
                                            <p onclick="downloadFormat('pdf');">PDF</p>
                                            <p onclick="downloadFormat('xls');">XLS</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="container">
                        <div class="col-xs-12">
                            <p class="lxrm-bold" style="border: 1px solid #ddd;padding: 1%;text-align: center;">No records found</p>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </c:if>

        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
