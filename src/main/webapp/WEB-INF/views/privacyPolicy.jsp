<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Privacy Policy</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--        <meta name="google-site-verification"
                      content="feMMk_4sqErYGfNaVquEEDMtQ_Ud4uW3k-xFfCi_XoY" />-->
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/static-pages.css">
        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var downloadReportType = "";
            isHomePage = true;
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-justify" style=" color: #888; ">            
                    <div>
                        <h2 style="color: black;font-family: ProximaNova-Bold;">Privacy Policy</h2>
                    </div>
                    <h4 style="margin-bottom: 0px; color: #f96003;font-family: ProximaNova-Bold;">
                        LXRMarketplace Privacy Policy
                    </h4>
                    <p>
                        (Last updated: July 23, 2018)
                    </p>
                    <p>
                        NetElixir, Inc. has created this privacy statement in order to demonstrate our firm commitment to privacy. 
                        This privacy statement discloses the information collection and disclosure practices for our Web sites (the "Sites") and our software, offerings, information portals and registries, 
                        and related consulting services and advice, such as our "Ask the Expert" features (the "Tools"). 
                        By accessing the Sites or using the Tools, you consent to the collection and use of information as described herein. 
                        We reserve the right to change this privacy statement at any time. If we make any material changes to our privacy statement, we will post a new statement on the Sites and update the effective date set forth above. 
                        Therefore, we encourage you to check the date of our privacy statement whenever you visit the Sites to see if it has been updated since your last visit. 
                        If you object to any of the changes to our Privacy Policy, please discontinue your use of our Sites and Tools as your continued use shall constitute your consent to the changes.
                    </p>

                    <p><span  class="static-side-heading">Personal Information:</span> 
                        Our Sites and Tools provide advertisers, agencies and businesses with various services related to internet marketing, 
                        pay per click ad campaigns, search engine optimization and other marketing services and software. 
                        Our Sites provide both free and purchased or licensed Tools to help users improve their business and marketing experience.  
                        On some pages of our Sites or within our tools, you may be able to request information, subscribe to mailing lists, collaborate, 
                        provide feedback, submit information or register for events or Tools, or ask our experts for advice on how to improve your website or marketing campaigns.
                        In order for us to provide these services and insure authorized access, 
                        we may collect your e-mail address, user IDs, passwords, credit card and billing information, product serial numbers and control codes, 
                        account numbers, as well as contact information, including but not limited to your name, company, address, and phone number.
                    </p>

                    <p>
                        We may also use a third party to host our Sites, including our online data collection forms. 
                        This third party provider has access to the user information for purposes such as maintaining our Sites.
                    </p>


                    <p><span  class="static-side-heading">Aggregate Information:</span> 
                        Aggregate information is data and statistics we collect about a group or category of products, services or users, from which individual user identities have been removed. 
                        We collect aggregate information about you and your use of our services and combine it with information about how others use the same service. Such aggregate information includes, without limitation, site traffic, page views, and impressions. 
                        This information may be correlated to you for our internal use only. However, we may collect, compile, store, publish, promote, sell, or otherwise 
                        disclose or use any and all aggregate information (i.e., information that does not personally identify you).
                    </p>

                    <p><span class="static-side-heading">Log Files and IP Addresses:</span>
                        Every time you use or access our website or use our tools, such as requesting or downloading a file, NetElixir may store data about these events and your IP address and host name in a log file.  
                        We may use this information to analyze trends, administer or Sites, monitor usage of our Tools, and gather demographic information for other business purposes.  
                        When you access or leave our Sites using a hyperlink, we may receive the URL from prior or subsequently visited website.  
                        We may also receive the IP address of your computer or proxy server used to access our Sites, your operating system, the type of browser you used, 
                        the type of device you use, your mobile device carrier or your ISP.
                    </p>

                    <p><span  class="static-side-heading">Cookies and Web Beacons:</span>
                        We may use cookies to identify users and customize their experience on our site based on information they give us. Cookies are small pieces of information or files that are stored on a user's computer 
                        by the Web server through a user's browser for record-keeping purposes. A cookie itself is not designed or intended to read any 
                        information from your computer (other than the contents of the cookie); rather, it is an identifier used by the Web site that 
                        originally placed it on your hard drive. The actual contents of the cookie information can be retrieved by the same site's Web 
                        server in order to personalize, monitor, or regulate the use of the site. We do not use Web beacons or similar technology on our 
                        Sites. However, we may from time to time permit third parties to place Web beacons on our Sites to monitor the effectiveness of 
                        advertising and for other lawful purposes. A Web beacon, also known as a "Web bug," is a small, graphic image on a Web page, 
                        Web-based document or in an e-mail message that is designed to allow the site owner or a third party to monitor who is visiting 
                        a site. Web beacons collect non-personally identifying information such as the IP address of the computer that the Web beacon 
                        is sent to, the URL of the page the Web beacon comes from and the time it was viewed. We do not use (or permit third parties 
                        to use) Web beacons to collect personally identifying information about you.
                    </p>

                    <p><span  class="static-side-heading">Usage Information:</span> 
                        We may from time to time collect information regarding your usage of our
                        free software tools to help you better use the tools and gain insights into your marketing efforts.  
                        This information is used strictly to communicate with you and improve our tools.
                    </p>

                    <p><span  class="static-side-heading">Location Information:</span> 
                        When you use a location-enabled device, we may collect and process information about your actual location, 
                        like GPS signals sent by a mobile device.  We also use various tools to determine location, such as sensor data 
                        from your device that may, for example, provide information on nearby Wi-Fi access points and cell towers.
                    </p>


                    <h4>USE of PERSONAL INFORMATION</h4>
                    <p>
                        We use personal information internally to provide our Tools and other offerings to uses, communicate with and serve our users and to enhance and extend our 
                        relationship with our users. More specifically, we use personal information for the purposes for which it was collected 
                        (such as to register you for, provide you with, and bill you for our tools and services or to provide advice through features such as "Ask the Expert"), to anticipate and resolve problems 
                        with our Sites, to respond to customer support inquiries, and to send you periodic e-mails or newsletters, and for our own 
                        internal operations (including internal analysis as described below). You should review the following section to understand 
                        when we disclose personal information to third parties.
                    </p>

                    <h4>Disclosure of Personal Information</h4>
                    <p>
                        Except as set forth in this privacy statement, we do not disclose, sell, rent, loan, trade or lease 
                        any personally identifying information collected at our Sites to any third party. 
                        We will disclose personal information in the following circumstances:
                    </p>

                    <p><span  class="static-side-heading">Internal Analysis and Operations:</span> 
                        We may use and disclose information about you for our own internal statistical, design or operational purposes, such as to estimate our audience size, 
                        measure aggregate traffic patterns, and understand demographic, user interest, purchasing and other trends among our users 
                        and to improve our products. We may outsource these tasks and disclose personal information about you to third parties, 
                        provided the use and disclosure of your personal information by these third parties are restricted to performance of such tasks. 
                        We may also share personal information with third-party vendors who assist us in performing internal functions necessary to 
                        operate our business. We require our third party vendors to keep such information confidential and to only use the information 
                        for the purpose it was provided.
                    </p>

                    <p><span  class="static-side-heading">Transaction Processing:</span> 
                        We may disclose personal information to third parties as necessary to perform services or to process transactions for you, 
                        such as credit card processing. We or our service providers may contact you to complete the transactions you have requested 
                        by any methods available including via e-mail, telephone, regular mail or other means.
                    </p>

                    <p><span  class="static-side-heading">Protection of NetElixir and Others:</span> 
                        We may disclose personal information when we believe it is appropriate to comply with the law 
                        (e.g., a lawful subpoena, warrant or court order); to enforce or apply this privacy statement or 
                        our other policies or agreements; to protect our or our users' rights, property or safety; 
                        or to protect our users from fraudulent, abusive, or unlawful use of, our Sites.
                    </p>

                    <p><span class="static-side-heading">Business Transfers:</span> 
                        In addition, information about users of our Sites, including personally identifiable information provided to us by our users, 
                        may be disclosed as part of any merger, acquisition, debt financing, sale of company assets, as well as in the event of an 
                        insolvency, bankruptcy or receivership in which personally identifiable information could be transferred to third parties as one of the business assets of the company.
                    </p>
                    <h4 class="static-privacy-side-heading">Deletion of Personal Information</h4>
                    <p>
                        You can request to delete your Personally Information from our systems by sending 
                        an email request to <a href="mailto:support@lxrmarketplace.com">support@lxrmarketplace.com</a> 
                        from your registered email address. The information will be deleted from our systems 
                        within 5 business days.
                    </p>
                    <h4 class="static-privacy-side-heading">Links</h4>
                    <p>
                        Please be aware that we may provide links to third-party Web sites from our Sites as a service to our users and we are not responsible 
                        for the content or information collection practices of those sites. We have no ability to control the privacy and data 
                        collection practices of such sites and the privacy policies of such sites may differ from that of our sites. Therefore, 
                        we encourage you to review and understand the privacy policies of such sites before providing them with any information.
                    </p>

                    <h4  class="static-privacy-side-heading">Security</h4>
                    <p>
                        We use reasonable security measures to store and maintain personally identifying information to protect it from loss, misuse, alternation or destruction by any unauthorized party while 
                        it is under our control including password protection and SSL encryption for exchange of sensitive data between your 
                        web browser and our Sites or Tools.  We also periodically monitor our system for possible vulnerabilities and attacks.  
                        Please be aware that the Internet is not a 100% secure environment and we cannot ensure or warrant the security of 
                        information that you submit through our Sites or using our Tools will not be accessed, disclosed, altered or destroyed 
                        in breach of our security protections.  Note that it is your responsibility to protect the security and integrity of 
                        your account details, including user name and passwords.  Also note that emails, instant messaging and similar communications 
                        with NetElixir are not encrypted and you should exercise caution in communicating confidential information through these means.
                    </p>

                    <h4  class="static-privacy-side-heading">DATA PROTECTION</h4>
                    <p>
                        Our Sites and Tools are intended primarily for use in the United States.  Given the international scope of Internet access, 
                        your personal information may be visible to persons outside of your country of residence, including persons in countries that 
                        deem our policies deficient in ensuring an adequate level of protection for such information.  If you are unsure whether 
                        this privacy statement is in conflict with applicable local laws, you should not submit your information.  
                        Individuals outside of the United States of America that submit personal information through our Sites or Tools hereby 
                        consent to the general use of such information as provided in this Privacy Policy and to its transfer and storage in 
                        the United States of America notwithstanding your country of origin or residence.
                    </p>

                    <h4  class="static-privacy-side-heading">Children</h4>
                    <p>
                        We do not intend, and our Sites and Tools are not designed, to collect personal information from children under the age of 16. If you are under 16 years old, 
                        you should not register for our Tools or otherwise provide information on our Sites.  
                        By accessing our Sites or using our Tools, you are legally acknowledging that you are over the age of 18 or are 
                        an emancipated minor.
                    </p>

                    <h4  class="static-privacy-side-heading">CALIFORNIA PRIVACY and DO-NOT-TRACK LAW</h4>
                    <p>
                        California residents who provide personal information in obtaining products or services for personal, 
                        family or household use are entitled to request and obtain from us, once per the calendar year, information about the customer 
                        information we shared, if any, with other businesses for their own direct marketing uses. If applicable, this information 
                        would include the categories of customer information and the names and addresses of those businesses with which we shared 
                        customer information for the immediately prior calendar year. To obtain this information from us, please send us an email 
                        with "Request for California Privacy Information" as the subject of your message, and we will send you a reply e-mail 
                        containing the requested information. Not all information sharing is covered by the "Shine the Light" requirements 
                        and only information on covered sharing will be included in our response. 
                    </p>
                    <p>
                        Please note that NetElixir does not respond to "do not track" signals or other similar mechanisms intended to allow California 
                        residents to opt-out of Internet tracking. NetElixir also may track and/or disclose your online activities over time and across different websites to 
                        third parties when you use our services subject to our privacy policy.
                    </p>

                    <h4  class="static-privacy-side-heading">Choice and Access</h4>
                    <p>
                        NetElixir gives users the following options for accessing, changing and 
                        deleting personal information previously provided, or opting out of receiving communications from us:
                    </p>
                    <p>Email us at support@lxrmarketplace.com; or</p>
                    <p>Call us at the following telephone number: (609) 356-5112</p>
                    <p>
                        Please note that if you close your account, your personal information may remain in our archives and back up devices.  
                        Also, we may keep your information following the closure of your account to meet regulatory requirements, resolve disputes, 
                        prevent fraud and abuse or enforce this Privacy Policy or terms of use and to facilitate our ongoing operations.
                    </p>

                    <h4  class="static-privacy-side-heading">Questions and Concerns</h4>
                    <p>
                        If you have any questions about this privacy statement, the practices of our Sites, or your dealings with our Sites, 
                        please contact us at the following address or email us at:                                                    
                        <a href="mailto:support@lxrmarketplace.com">support@lxrmarketplace.com</a></p>
                    <p>NetElixir Customer Support</p>
                    <p>3 Independence Way, Suite #203,</p>
                    <p>Princeton, NJ 08540 USA,</p>
                    <p><a href="/privacy-policy-14sep2016.html" target="_blank">Archived Privacy policy: September 14, 2016.</p>

                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>