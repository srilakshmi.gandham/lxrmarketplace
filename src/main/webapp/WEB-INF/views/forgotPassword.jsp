<%-- 
    Document   : brokenLinkChecker
    Created on : Aug 1, 2017, 11:24:49 AM
    Author     : netsystem68
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Forgot Password | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <script type="text/javascriptx" src="/resources/js/commonFunctions.js"></script>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
        </script>
        <style>
            .main-center{
                margin-top: 30px;
                margin: 0 auto;
                max-width: 400px;
                padding: 10px 16px;
                background:#fff;
                color: black;
                text-shadow: none;
                border: 1px solid rgba(0,0,0,.15);
                border-radius: 4px;
                -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
                box-shadow: 0 6px 12px rgba(0,0,0,.175);
            }
            
            
            
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container" style="height: 545px">
            <div class="row main">
                <div class="main-login main-center" style="border-top: 2px #F47A44 solid;">
                    <form role="form" action="">
                        <div class="row setup-content">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <h3>Forgotten your password?</h3>
                                    <div class="form-group">
                                        <input type="email" required="required" class="form-control" name="userName" id="userName" placeholder="Enter your email address" />
                                    </div>
                                    <p style="display: none;text-align: center;" id="status-message"></p>
                                    <button class="btn btn-block nextBtn lxr-subscribe" type="button" id="submitBut" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Processing...">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.nextBtn').click(function () {

                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                            curInputs = curStep.find("input[type='email']"),
                            isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }
                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });
                $('#submitBut').click(function () {
                    $("#status-message").hide();
                    var statusMsg = "";
                    var status = true;
                    var email = $("#userName").val();
                    if (email === "") {
                        statusMsg = "Email cannot be left blank.";
                        $("#userName").focus();
                        status = false;
                    } else if (!isValidMail(email)) {
                        statusMsg = "Please enter a valid email address.";
                        $("#userName").focus();
                        status = false;
                    }
                    if (!status) {
                        $("#status-message").show();
                        $("#status-message").html(statusMsg);
                        $("#status-message").css("color", "red");
                        return false;
                    }
                    var $this = $(this);
                    $this.button('loading');
                    $('#userName').attr('readonly', true);
                    $.ajax({
                        method: "POST",
                        url: "forgot-password.html?sendmail='sendmail'&userName=" + $('#userName').val(),
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            $('#userName').attr('readonly', false);
                            $('#status-message').show();
                            $this.button('reset');
                            if (data[0] === "mailSent") {
                                $('#status-message').html("Password has been mailed successfully. Please check your mail account for password.");
                                $('#status-message').css('color', 'green');
                            } else if (data[0] === "socialChannelLogin") {
                                $('#status-message').html("You have registered using facebook or google, please login with your respective login credentials.");
                                $('#status-message').css('color', 'red');
                            } else if (data[0] === "forgotPasswordErrorMsg") {
                                $('#status-message').html("The entered email does not match with our records.");
                                $('#status-message').css('color', 'red');
                            } else if (data[0] === "contactSupportTeam") {
                                $('#status-message').html("You don't have permission to reset your password with this email. For more details please contact our support team at support@lxrmarketplace.com");
                                $('#status-message').css('color', 'red');
                            }
                        }
                    });
                });
                $('#userName').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#submitBut").click();
                        return false;
                    }
                });
            });
        </script>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>