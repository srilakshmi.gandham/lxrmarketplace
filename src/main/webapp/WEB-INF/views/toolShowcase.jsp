
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.web.context.WebApplicationContext, org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="java.util.List, java.util.ArrayList, java.util.Map, java.util.HashMap, lxr.marketplace.util.Tool, lxr.marketplace.util.ui.CommonUi"%>
<%

    CommonUi toolshowrating = new CommonUi(session);
    Map<Integer, Tool> toolsMap = null;
    if (session.getAttribute("sortedToolsBasedOnUser") != null) {
        toolsMap = (Map<Integer, Tool>) session.getAttribute("sortedToolsBasedOnUser");
    }
    long toolid = 0;
    List<Tool> totalTools = new ArrayList<>(toolsMap.values());
    List<Tool> seoTools = new ArrayList<>();
    List<Tool> semTools = new ArrayList<>();
    List<Tool> socialTools = new ArrayList<>();
    List<Tool> otherTools = new ArrayList<>();
    List<Tool> sectionWiseTools = new ArrayList<>();
    if (totalTools != null && totalTools.size() > 0) {
        for (Tool toolObj : totalTools) {
            if (toolObj.isShowInHome()) {
                if (toolObj.getCategoryid() == 1) { //Category Id 1 means SEM Tools
                    semTools.add(toolObj);
                } else if (toolObj.getCategoryid() == 3) { //Category Id 3 means SEO Tools
                    seoTools.add(toolObj);
                } else if (toolObj.getCategoryid() == 6) { //Category Id 6 means E-Commerce Tools
                    otherTools.add(toolObj);
                } else if (toolObj.getCategoryid() == 7) { //Category Id 7 means Social Tools
                    socialTools.add(toolObj);
                }
            }
        }
    }
    //  No. of tool sections in home page
    List<Integer> numberOfSections = new ArrayList<>();
    numberOfSections.add(1);
    numberOfSections.add(2);
    numberOfSections.add(3);
    numberOfSections.add(4);
    pageContext.setAttribute("numberOfSections", numberOfSections);
    pageContext.setAttribute("seoTools", seoTools);
    pageContext.setAttribute("semTools", semTools);
    pageContext.setAttribute("otherTools", otherTools);
    pageContext.setAttribute("socialTools", socialTools);
    pageContext.setAttribute("sectionWiseTools", sectionWiseTools);
%>
<script type="text/javascript">
    $(document).ready(function () {
        // The below variale is coming from lxrMarketplace.jsp page
        if (<%=topThreeTools == null%>) {
            showAllSectionTools(4, "");
        }
    <c:if test="${userLoggedIn}">
        $('#ebookName').val("${userH.getName()}");
        $('#ebookEmail').val("${userH.getUserName()}");
    </c:if>

    });
    function showAllSectionTools(limit, toolSection) {
        var toolsHtml = "";
        /* if topThreeToolIds are there in session then assign to recomendedToolIdsArray
         * recomendedToolIdsArray is for exclude top three tools from tool showcase in home page */
        var recomendedToolIdsArray = <%=topThreeToolIds%>;
        if (topThreeToolsIds !== null) {
            recomendedToolIdsArray = topThreeToolsIds;
        }
        console.log("recomendedToolIdsArray::: "+recomendedToolIdsArray);
    <c:forEach items="${numberOfSections}" var="sectionTools">
        <c:choose>
            <c:when test="${sectionTools == 1}">
                <c:set var="sectionWiseTools" value="${seoTools}"/>
                <c:set var="sectionName" value="seo"/>
                <c:set var="heading" value="Free SEO <span>Tools for Small Businesses</span>"/>
            </c:when>
            <c:when test="${sectionTools == 2}">
                <c:set var="sectionWiseTools" value="${socialTools}"/>
                <c:set var="sectionName" value="social"/>
                <c:set var="heading" value="Free Social <span>Tools for Small Businesses</span>"/>
            </c:when>
            <c:when test="${sectionTools == 3}">
                <c:set var="sectionWiseTools" value="${semTools}"/>
                <c:set var="sectionName" value="ppc"/>
                <c:set var="heading" value="Free SEM/PPC <span>Tools for Small Businesses</span>"/>
            </c:when>
            <c:when test="${sectionTools == 4}">
                <c:set var="sectionWiseTools" value="${otherTools}"/>
                <c:set var="sectionName" value="ecommerce"/>
                <c:set var="heading" value="E-commerce <span>Mobile Apps for Small Businesses</span>"/>
            </c:when>
        </c:choose>
        /* this condition is for to display only section wise tools when user clicks on seemore link */
        if (("${sectionName}" === toolSection && toolSection !== "") || toolSection === "") {
            if (${sectionTools} === 2) {
                toolsHtml += '<div id="ebookContainer" class="row"><form><div class="col-lg-4 "><div class="form-group"><img src="../../resources/images/ebook.svg" alt="top 10 seo ebook"><span><strong>SEO Ebook:</strong> Top 10 SEO Tactics That Work</span></div></div><div class="col-lg-8"><div class="form-group col-lg-3" ><input type="text" id="ebookName" class="form-control" placeholder="Please enter name"></div><div class="form-group col-lg-5"><input type="text" id="ebookEmail" class="form-control" placeholder="Please enter email"></div><div class="form-group col-lg-4" style="margin: 0px"><button type="button" id="sendFreeBook" class="form-control" onclick="return sendMyEbook(this)" data-loading-text="Sending..." ><i class="fa fa-paper-plane" aria-hidden="true"></i> Send my free ebook</button></div><div style="height:8px;clear:both;" class="visible-xs visible-sm visible-md"></div></div></form></div>';
            }

            toolsHtml += '<div id="${sectionName}-tools">';
            toolsHtml += '<blockquote class="blockquote">${heading}</blockquote>';
            toolsHtml += '<div class="row">';
            var toolsLimit = 1;
            var remainingToolsCount = 0;
        <c:forEach items="${sectionWiseTools}" var="tool">
            /* if limit is 4 then we have show four tools in the tools section along with 'seemore' button.
             * otherwise show list of all the tools in the tools section*/
            if (limit === 0 || (limit > 0 && toolsLimit <= limit)) {
                if (!recomendedToolIdsArray.includes(${tool.toolId})) {
//                if (jQuery.inArray( ${tool.toolId}, recomendedToolIdsArray ) !== -1) {
                    toolsHtml += '<div  class="col-xs-12 col-sm-6 col-lg-3 lxr-tooladviser form-group">'
                            + '<div class="lxr-tool-showcase col-xs-12" style="border: 1px ${tool.toolIconColor} solid;background: white;border-radius: 5px;padding: 0;">'
                            + '<a href="${tool.toolLink}">'
                            + '<div class="col-item lxr-items"  style="background-color:${tool.toolIconColor};">'
                            + '<div class="post-img-content">'
                            + '<span class="post-title">'
                            + '<span class="pull-left" style="padding: 8px;"><img src="../../resources/images/tool-icons/${tool.imgPath}.png"></span>';
            <c:if test="${tool.categoryid ne 6}">
                    toolsHtml += '<div class="pull-right" ><span style="padding: 8px 10px 0px 0px;" class="pull-right"><img src="../../resources/images/user.png"></span>';
                    if (${tool.toolUserInfo.toolUsageCount} >= 1000) {
                        toolsHtml += '<span style="font-size: 30px;" class="lead">' + kFormatter(${tool.toolUserInfo.toolUsageCount}) + '</span><span class="sma">k&nbsp;&nbsp;</span></div>';
                    } else {
                        toolsHtml += '<span style="font-size: 30px;" class="lead">${tool.toolUserInfo.toolUsageCount}</span><span class="sma">&nbsp;&nbsp;</span></div>';
                    }
            </c:if>
                    var toolName = "${tool.toolName}";
                    if ("${tool.toolName}".length > 28) {
                        toolName = toolName.substring(0, 26) + "..";
                    }
                    toolsHtml += '</span>'
                            + '</div>'
                            + '<div class="price col-lg-12" style="padding-left: 8px;">'
                            + '<h4 class="align-text-bottom lxr-title" title="${tool.toolName}">' + toolName + '</h4>'
                            + '<div id="stars" class="starrr align-text-bottom pull-left">${tool.toolUserInfo.avgRatingGraph}</div>'
                            + '<span class="pull-left">&nbsp;&nbsp;(${tool.toolUserInfo.toolRatedUsersCount})</span>'
                            + '</div>'
                            + '</div>'
                            + '<div class="info">'
                            + '<ul class="fa-ul" style="height: 96px">'
                            + "<li><i class='fa-li fa fa-check' style='color:${tool.toolIconColor}'></i>${tool.tagline}</li>"
                            + "<li><i class='fa-li fa fa-check' style='color:${tool.toolIconColor}'></i>${tool.toolDesc}</li>"
                            + '</ul>'
                            + '<div class="lxr-clearfix">';
            <c:if test="${tool.isIOSApp eq true || tool.isAndroidApp eq true}">
                    toolsHtml += '<span>Download App</span>';
            </c:if>
            <c:if test="${tool.isAndroidApp eq true}">
                    toolsHtml += '<a href="${tool.androidAppURL}" target="_blank">'
                            + '<img src="../../resources/images/android.png"></a>';
            </c:if>
            <c:if test="${tool.isIOSApp eq true}">
                    toolsHtml += '<a href="${tool.iosAppURL}" target="_blank" >'
                            + '<img src="../../resources/images/apple-xxl.png"></a>';
            </c:if>
                    toolsHtml += '</div>'
                            + '</div>'
                            + '</a></div>'
                            + '</div>';
                    if (limit > 0) {
                        toolsLimit++;
                    }
                }
            } else {
                remainingToolsCount++;
            }
        </c:forEach>
            if (remainingToolsCount > 0) {
                toolsHtml += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right"  style="clear: both;overflow: hidden;border: 0px red solid;">'
                        + "<div class='col-xs-7 col-sm-8 col-md-8 col-lg-9 pull-right' style='padding: 2%;background: #1E334E;color: #FFF;border-radius: 4px;font-size: 1.6rem;cursor:pointer;' onClick=\"showAllSectionTools(0, '${sectionName}')\">"
                        + '<span id="${sectionName}-seemore" class="fa fa-chevron-circle-down"></span>'
                        + '<span class="lxrm-bold" style="padding-left: 4px;">See More Similar Tools</span>'
                        + '</div>'
                        + '</div>';
            } else if (limit === 0) {
                toolsHtml += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right"  style="clear: both;overflow: hidden;border: 0px red solid;">'
                        + "<div class='col-xs-5 col-sm-6 col-md-6 col-lg-6 pull-right' style='padding: 2%;background: #1E334E;color: #FFF;border-radius: 4px;font-size: 1.6rem;cursor:pointer;' onClick=\"showAllSectionTools(4, '${sectionName}')\">"
                        + '<span id="${sectionName}-seeless" class="fa fa-chevron-circle-up"></span>'
                        + '<span class="lxrm-bold" style="padding-left: 4px;">See Less Tools</span>'
                        + '</div>'
                        + '</div>';
            }
        }
        toolsHtml += "</div>";
        toolsHtml += "</div>";
    </c:forEach>
        if (toolSection !== "") {
            $("#" + toolSection + "-tools").html("");
            $("#" + toolSection + "-tools").html(toolsHtml);
//            $('html, body').animate({scrollTop:$('#'+toolSection+"-tool").position().top}, 'slow');
        } else {
//            $("#lxr-toolbox").html("");
//            $("#lxr-toolbox").html(toolsHtml);
            $("#lxr-home-toolbox").empty();
            $("#lxr-home-toolbox").html(toolsHtml);
        }
//        console.log("toolsHtml:: "+toolsHtml);
    }

</script>

<div class="container-fluid lxr-fullbg">
    <!--<div class="lxr-toolbox" id="lxr-toolbox"> </div>-->
    <div class="lxr-toolbox" id="lxr-home-toolbox"> </div>
</div>
<script src=resources/js/ebook.js></script>