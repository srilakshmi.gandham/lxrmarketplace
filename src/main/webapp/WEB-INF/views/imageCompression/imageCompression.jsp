<%-- 
    Document   : faviconGenerator
    Created on : 15 Nov, 2018, 11:22:02 AM
    Author     : netelixir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Image Compression Tool</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>

        <!--        <h1>Hello World!</h1>-->        <!-- Angular JS -->
        <script type="text/javascript" src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>
        <script type="text/javascript" src="../resources/js/angular/angular.min.js"></script>
        <!--<script type="text/javascript"  src="/resources/js/app.js"></script>-->
        <!--<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>-->
        <!--<script src="jQuery-plugin-(.js"></script>-->

        <script type="text/javascript" src="/resources/js/tools/imageCompression/imageCompressionTool.js"></script> 
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/imageCompression.css">
        <!--<link rel="stylesheet/less" type="text/css" href="/resources/css/tools/newstyle.scss" />-->
        <%
            int toolDBId = 43;
        %>

        <script type="text/javascript">

            var reportDownload = 0;
            var atePopupStatus = true;
            var downloadReportType = "";
            var isBackToSuccess = false;
            var listOfFiles = null;
            $(document).ready(function () {
                $(window).on('popstate', function () {
                    location.reload(true);
                });
            });

            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/image-compression.html?ate=ate&domain=",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function submitPreviousRequest() {
                angular.element('#loading').triggerHandler('click');
            }
        </script>
    </head>
    <body  ng-app="myApp1"  ng-controller="myCtrl1 as ctrl" >
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div id="loadingDivForImageCompression" class="col-12 p-0" style="position: fixed;background: gray;width: 100%;height: 100%;top: 0;z-index: 1000;color: #FFF;left: 0;opacity: 0.8;text-align: center;">
            <span class="lxrm-bold" style="position: relative;top: 50%;font-size: 2rem;"><span class='fa fa-spinner fa-spin'></span> Loading...</span>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none" id="image-compression-blockqoute">
                <blockquote class="blockquote ">
                    <p>
                        The Image Compression Tool is used to compress the file size of an image by removing correlated pixels. The tool can upload files sizes that are greater than or equal to 15 KB. Image files that are already compressed should not be uploaded.
                    </p>
                </blockquote>
            </div>
            <form method="post" class="col-xs-12 col-lg middleContent mobile-padding-none" enctype="multipart/form-data" style="margin-top: 20px;" id="formData">
                <div class="row middleContent col-lg-12 col-xs-12  normal-styles" >
                    <!--<div class="row col-lg-6 col-xs-12  normal-styles"  >-->
                    <div class="col-xs-12" style="padding-top: 10px; text-align: center;">
                        <label style=" font-size: 2rem;" class="uploadLabel lxrm-bold">Upload your Image(s): 
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                               title="The Image Compression Tool allows up to 5 JPEG or JPG image files. The total size of the images should be between 15 KB and 1 GB.">
                                <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                            </a>
                        </label>

                        <label for="imageFile" id="browseFile" class="btn btn-primary lxr-subscribe" style="font-size: 18px;">Browse File</label>
                        <input type="file" multiple="multiple" id="imageFile" file-model="fileModel" name="imageFile" style="display: none;"  onchange="angular.element(this).scope().getFileDetails(this)" />
                        <p class="col-lg-9 col-xs-12 "style="float: right; padding-top: 10px;">(Upload files in jpg or jpeg format only.)</p>
                    </div>
                    <!--</div>-->
                    <div class="col-xs-12 " id="showFileName" style="padding-top: 20px;">
                        <div class="col-xs-12" ng-if="compressedFiles !== null && compressedFiles.length > 0">
                            <table class="table lxr-table table-bordered filenames col-xs-12 "  >
                                <thead style="background: white;color: #4F4F4F;">
                                    <tr>
                                        <th id='fileName'>File Name</th>
                                        <th id='size'>Image Size</th>
                                        <th id='isAccepted'>Accepted?</th>
                                        <th id='action'>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="imageDetails{{$index}}" ng-repeat="item in compressedFiles track by $index">
                                        <td>{{item.name}}</td>
                                        <td>{{formatSizeUnits($index)}}</td>
                                        <td class="text-center">
                                            <span  ng-switch="item.size < 15360 || !(item.name.endsWith('.jpeg') || item.name.endsWith('.jpg'))" >
                                                <i ng-switch-when="true" class="fa fa-times lxr-danger" style="font-size: 1.5em !important;"></i>
                                                <i class="fa fa-check lxr-success" style="font-size: 1.5em !important;"  ng-switch-default></i>
                                            </span>
                                        </td>
                                        <td id="removeId" class="text-center">
                                            <i class="fa fa-2x fa-trash lxr-danger" aria-hidden="true" style="cursor: pointer; font-size: 2rem;" ng-click="removeUnsupportedFile(item)"></i>
                                            <!--<button type="button" class=" btn top-line btn-lg buttonStyles button1" ng-click="removeUnsupportedFile(item)">Remove</button>-->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span class="col-xs-12 lxr-danger text-center" id="fileUpload1Error"></span>

                    </div>
                    <div class="col-xs-12" style="margin-top: 6px; text-align:  center;">
                        <button ng-click="getCompressedFile()" type="submit" class="btn top-line btn-lg buttonStyles lxr-search-btn"   id="loading" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> COMPRESSING..">
                            COMPRESS
                        </button>
                    </div>
                </div>
            </form>
            <div class="lxrm-box-shadow col-xs-12" id="result" style=" margin-top: 20px;">
                <div class="col-xs-12" style="margin-top: 20px; padding: 0px;">
                    <h4>Following below are the compressed images:</h4>
                </div>
                <div class="col-xs-12" ng-if="imageData !== null && imageData.length > 0" style="overflow-x: auto; padding: 0;">
                    <table class="table table-bordered lxr-table" >
                        <thead style="background: white; color: #4F4F4F;">
                            <tr>
                                <th style="min-width: 96px;">File Name</th>
                                <th style="min-width: 105px;">Original size</th>
                                <th style="min-width: 140px;">Compressed size</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tr  ng-repeat="result in imageData">
                            <td ng-switch="fileSizeEqual(result)===true">{{result.fileName}}<span ng-switch-when="true"><i class="fa fa-asterisk" style="color:red; position: absolute; font-size: 10px;"></i></span><span ng-switch-default></span></td>
                            <td>{{result.imageSize}}</td>
                            <td>{{result.compressedImageSize}}</td>
                            <td>
                                <div class="image-and-download-button">
                                    <img src="/download/{{result.fileName}}" alt="{{result.fileName}}" height="40" width="50"/>
                                    <span  class="fa fa-2x fa-download"  title="Download" ng-click="download(result.fileName)" style="padding-left: 10px; cursor: pointer;"></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12" style="padding: 0;padding-bottom: 10px;">
                    <span><i class="fa fa-asterisk" style="color:red"></i></span> - Indicates that the image has been already compressed, and cannot reduce the size.
                </div>
            </div>

            <form method="get" action="/image-compression.html" id="downloadForm">
                <input type="hidden" id="name" name="imageName" />
            </form>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
