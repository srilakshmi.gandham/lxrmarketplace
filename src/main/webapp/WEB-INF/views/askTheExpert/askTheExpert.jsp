
<%@page import="lxr.marketplace.user.Login"%>
<link rel="stylesheet" type="text/css" href="/resources/css/askTheExpert.css">

<script type="text/javascript">
    var catId = 0;
//    var catId = ${toolObject.categoryid};
    var tempCatId = "${toolObject.categoryid}";
    if (tempCatId !== "") {
        catId = parseInt(tempCatId);
    }
    var expertInfo = "";
    var emailId = "${userH.userName}";
    var question = "";
    var issues = "";
    var toolName = "${toolObject.toolName}";
    var otherInputs = "";
    $(document).ready(function () {
        var domainPresentArray = [4, 2, 30, 37, 40, 41];
        var askTHeExpertPopupArray = [6, 9, 36, 37, 27, 40, 41, 43, 35];
        if (catId !== 6 && jQuery.inArray(toolDBId, domainPresentArray) === -1) {
//        }
//        if (catId !== 6 && toolDBId !== 4 && toolDBId !== 2 && toolDBId !== 30 && toolDBId !== 37 && toolDBId !== 40 && toolDBId !== 41) {
            $('#ateDomain').show();
        }
        if ((!userLoggedIn) || ((userLoggedIn && loginLxrmUserEmail === null || loginLxrmUserEmail === undefined || loginLxrmUserEmail === ''))) {
            console.log("Default ATE load for guest user and ");
            $('#ateEmail').show();
        }
//        //  If user cookies are there then auto populate the email id in popup
//        if (userLoggedIn && emailId !== "" && emailId.indexOf("GuestUser") !== -1) {
//            emailId = emailId.slice(emailId.indexOf("<") + 1, emailId.lastIndexOf(">"));
//            $("#ateEmail").val(emailId);
//        }

        $("#chevron-up").hide();
        $('#ateWindow').click(function () {
            $('#ask-expert-form').slideToggle();
            if ($('#chevron-up').is(':visible')) {
                $("#envelope-o").show();
                $("#chevron-up").hide();
            } else {
                $("#envelope-o").hide();
                $("#chevron-up").show();
            }
        });

        //These tools are ajax get results so need hanlde separate
        if (jQuery.inArray(toolDBId, askTHeExpertPopupArray) === -1) {
//        if (toolDBId !== 6 && toolDBId !== 9 && toolDBId !== 36 && toolDBId !== 37 && toolDBId !== 40 && toolDBId !== 27 && toolDBId !== 41 && toolDBId !== 43) {
            getAskExpertPopup(false);
        }
        /* If user cookies are there then auto populate the email id in popup */
//        if (emailId !== "" && emailId !== 'Email') {
//            if (emailId.indexOf("GuestUser") >= 0) {
//                emailId = emailId.slice(emailId.indexOf("<") + 1, emailId.lastIndexOf(">"));
//                $("#ateEmail").val(emailId);
//            } else {
//                $("#ateEmail").val(emailId);
//            }
//        }
    });


    /*  To show tool related issue After doing the analysis of the tool  */
    function getAskExpertPopup(toolWiseAskExpert) {
        if ((parentUrl.indexOf("getResult=") !== -1) || (parentUrl.indexOf("backLink=") !== -1) || (parentUrl.indexOf("=getToolData") !== -1)
                || (parentUrl.indexOf("ViewResult=") !== -1) || ((parentUrl.indexOf("query=") !== -1)
                && ((parentUrl.indexOf("save") !== -1) || (parentUrl.indexOf("getResult") !== -1)))
                || (parentUrl.indexOf("update=") !== -1) || (parentUrl.indexOf("backToSuccessPage=") !== -1)
                || (toolWiseAskExpert)) {
            if (userLoggedIn) {
                $("#ateEmail").hide();
            }
            /*This case may raise
             If emai is not captured from social login channels API, when user login/signup using social channels.*/
            console.log("lxrmUserEmail:: " + loginLxrmUserEmail);
            if (loginLxrmUserEmail === null || loginLxrmUserEmail === undefined || loginLxrmUserEmail === '') {
                $("#ateEmail").show();
            }

            getToolRelatedIssues();
            //To display pop up after 3 seconds
            setTimeout(function () {
                var textareatext = question;
                $('#ateText').html(textareatext);
                var toolIssues = issues;
                if (toolIssues === undefined || toolIssues === null || toolIssues === "") {
                    $('#tool-issues').hide();
                    $("#ateText").show();
                    if (toolDBId === 40 || toolDBId === 41 || toolDBId === 43 || toolDBId === 35) {
                        $('#ateWindow').click();
                    }
                } else {
                    toolIssues = toolIssues.trim();
                    $("#tool-issues").html(toolIssues);
                    $("#tool-issues").show();
                    $("#ateText").hide();
                    $('#ateWindow').click();
                }
            }, 10000);
        }
        //Only for E-Commerce Tools
        else if (catId === 6) {
            $("#ateText").hide();
            $("#ateDomain").hide();
//            $("#ateEmail").hide();
//            $("#hr-line").show();

            var toolIssues = "";
            toolIssues += "Looking for help with your " + toolName + " store? ";
            toolIssues += "We're specialized in optimizing and implementing high quality solutions "
                    + "to your store hosted on " + toolName + " store?";
            toolIssues += "<ul>";
            toolIssues += "<li>Full End to End Ecommerce Implementations.</li>";
            toolIssues += "<li>Installation and Configuration of Extensions and Modules.</li>";
            toolIssues += "<li>Custom Development of Extensions and Modules.</li>";
            toolIssues += "<li>Custom Theme Development.</li>";
            toolIssues += "<li>Extend Current Ecommerce Platform to Configure Multiple Websites/Stores.</li>";
            toolIssues += "<li>Configure Blogs for Your Ecommerce Store.</li>";
            toolIssues += "<li>Update Your Payment Gateways and Shipping Methods Integration.</li>";
            toolIssues += "<li>Landing Page Design for Product or Promotional Activities.</li>";
            toolIssues += "<li>Integration of Your Store With Internal Systems (i.e. CRM / SAP).</li>";
            toolIssues += "</ul>";
            $("#tool-issues").html(toolIssues);
            $("#submit-expert-ecommerce").show();
            $("#submit-expert-issues").hide();
            $("#tool-issues").show();
            $("#ateText").hide();
            $('#ateWindow').click();
        }
    }

    function validateATEData() {
        var helpValidation = true;
        if ((!userLoggedIn) || ((userLoggedIn && loginLxrmUserEmail === null || loginLxrmUserEmail === undefined || loginLxrmUserEmail === ''))) {
            var email = $('#ateEmail').val().trim();
            console.log("email:: " + email);
            if (email !== "") {
                if (!isValidMail(email)) {
                    $("#ateEmail").addClass('inputErr');
                    helpValidation = false;
                } else {
                    $("#ateEmail").removeClass('inputErr');
                }
            } else {
                $("#ateEmail").addClass('inputErr');
                helpValidation = false;
            }
        }

        if ($('#ateDomain').css('display') !== 'none') {
            var helpUrl = $('#ateDomain').val().trim();
            if (helpUrl === "") {
                $("#ateDomain").addClass('inputErr');
                helpValidation = false;
            } else {
                $("#ateDomain").removeClass('inputErr');
            }
            if (helpUrl !== "") {
                var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                if (!re.test(helpUrl)) {
                    $("#ateDomain").addClass('inputErr');
                    helpValidation = false;
                }
            }
        }

        var userQuestion = $('#ateText').val().trim();
        if ($('#ateText').css('display') !== 'none') {
            if (userQuestion !== "") {
                $("#ateText").removeClass('inputErr');
                if (userQuestion.length > 2048) {
                    alert("Please ensure the text is below 2048 characters");
                    helpValidation = false;
                }
            } else {
                $("#ateText").addClass('inputErr');
                helpValidation = false;
            }
        }
        return helpValidation;
    }

    function doAsk() {
        $("#error-msg").hide();
        if (!validateATEData()) {
            $("#error-msg").show();
            return false;
        }
        if (checkDefaultLoginCookie() && sessionTimeOutVariable) {
            ateLoginValue = true;
            login(uname, pwd, true, true);
            return false;
        }
        var $this = $("#ask-the-expert-submit");
        $this.button('loading');

        var toolIssues = $("#tool-issues").html();
        var expertInformation = $("#expert-info").html();

        var toolId = $("<input>").attr("type", "hidden").attr("name", "toolId").val(toolDBId);
        var pageUrl = $("<input>").attr("type", "hidden").attr("name", "pageUrl").val(parentUrl);
        var toolResultIssues = $("<input>").attr("type", "hidden").attr("name", "issues").val(toolIssues);
        var expertInfo = $("<input>").attr("type", "hidden").attr("name", "expertInfo").val(expertInformation);
        var othrInputs = $("<input>").attr("type", "hidden").attr("name", "otherInputs").val(otherInputs);

        var ateForm = $("#ask-expert-form");
        $('#ask-expert-form').append($(toolId));
        $('#ask-expert-form').append($(pageUrl));
        $('#ask-expert-form').append($(toolResultIssues));
        $('#ask-expert-form').append($(expertInfo));
        $('#ask-expert-form').append($(othrInputs));

        ateForm.attr('method', "POST");
        ateForm.attr('action', "/lxrm-ask-question.html");
        ateForm.submit();
    }
    function submitATEQuery(id) {
        var toolId = $("<input>").attr("type", "hidden").attr("name", "toolId").val(toolDBId);
        var pageUrl = $("<input>").attr("type", "hidden").attr("name", "pageUrl").val(parentUrl);
        var expertInfo = $("<input>").attr("type", "hidden").attr("name", "expertInfo").val("expertInformation");
        var othrInputs = $("<input>").attr("type", "hidden").attr("name", "otherInputs").val("otherInputs");
        var ateForm = $("#ask-expert-form");
        $('#ask-expert-form').append($(toolId));
        $('#ask-expert-form').append($(pageUrl));
        $('#ask-expert-form').append($(expertInfo));
        $('#ask-expert-form').append($(othrInputs));
        ateForm.attr('method', "POST");
        ateForm.attr('action', "/lxrm-ask-question.html?toolhelp=true");
        ateForm.submit();
    }
</script>

<div class="row">	
    <div id='frm-wrap' class="col-md-push-9 col-md-3 col-xs-push-1 col-xs-10">
        <div id="ateWindow" class="row">
            <div>
                <span  class="col-md-2 col-xs-2"><i id="envelope-o" class="fa fa-envelope-o" aria-hidden="true"></i></span>
                <span class="col-md-8 col-xs-8 text-center">Ask The Expert</span>
                <span  class="col-md-2 col-xs-2"><i id="chevron-up" class="fa fa-chevron-down" aria-hidden="true"></i></span>
            </div>
        </div>
        <form name="myForm" id="ask-expert-form">
            <p id="expert-info">Our Google certified experts are ready to answer any questions you might have about your website or digital marketing campaign. How can we help you?</p>
            <div class="tool-issues-div" id="tool-issues" style="display: none;margin-bottom: 4%;"></div>
            <div id="ate-input-fileds">
                <div class="form-group ate-domain-div">
                    <input type="text" style="display: none;" id="ateDomain" name="domain"  class="form-control" value="${lxrmAskExpert.domain}" placeholder="Enter your URL">
                </div>
                <div class="form-group ate-email-div">
                    <input type="text" style="display: none;" id="ateEmail" name="email"  class="form-control" placeholder='Enter your Email'>
                </div>	
                <div class="form-group">
                    <textarea id="ateText" name="question" class="form-control" style="height: 100px" placeholder="What can we help you with?"></textarea>
                </div>
                <div class="form-group">
                    <div id="error-msg" class="error" style="display: none;">Please fill all the above details</div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="ask-the-expert-submit" class="btn btn-block nextBtn lxr-search-btn" style="margin-bottom: 0px" onclick="return doAsk(this)" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Send">Send</button>
            </div>			
        </form>
    </div>

</div>

