
<%-- 
    Document   : askExpertAnswers
    Created on : 9 Oct, 2017, 12:09:31 PM
    Author     : vemanna
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ask The Expert Messages | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/askTheExpertAnsers.css">

        <script type="text/javascript">
            $(document).ready(function () {
                $("#msg-notification-count").hide();
                $("#m-msg-notification-count").hide();
                if (${userLoggedIn}) {
                    getAnswersList();
                }
            });
            /*ATE questions and answers list*/
            function getAnswersList() {
                var userId = ${userH.id};
                var params = {userId: userId};
                jQuery.getJSON("/user-messages.html?ate=allAnswers", params,
                        function (data) {
                            if (data !== null) {
                                constructRecentAnswersList(data);
                                $("#messageAlert").text("");
                                $("#mobileNotification").text("");
                                return data[0].length;
                            }
                        });
            }

            function constructRecentAnswersList(data) {
                if (data.length > 0) {
                    var totExistingList = "";
                    for (var i = 0; i < data.length; i++) {
                        var convertedDate = generateDisplayDate(data[i].addedDate);
                        totExistingList += '<div class="recentAnsweredWrap">';
                        totExistingList += '<div class="question">' + data[i].userQuestion + '</div>';
                        totExistingList += '<div class="answer">' + data[i].adminAnswer + '</div>';
                        totExistingList += '<div class="row"><div class="col-md-4 mydate"><span>' + convertedDate + '</span></div>';
                        if (data[i].attachedFile !== null && data[i].attachedFile !== '') {
                            totExistingList += '<div class="col-md-4 full-right"><span>1 Attached File: <span><a href="/ate-view-attached-file.html?attachedFile='+ data[i].attachedFile +'" id="viewAttachedFile"><span id="fileDownload" class="fa fa-file-text" aria-hidden="true" title=' + data[i].attachedFile + '><span></a></div>';
                        }
                        var rating = data[i].rating;
                        var ratingClass;
                        var ratingMsg;
                        if (parseInt(rating) !== 0) {
                            if (parseInt(rating) === 2) {
                                ratingClass = "poor";
                                ratingMsg = 'Poor';
                            } else if (parseInt(rating) === 3) {
                                ratingClass = "fair";
                                ratingMsg = 'Fair';
                            } else if (parseInt(rating) === 4) {
                                ratingClass = "excellent";
                                ratingMsg = 'Good';
                            } else if (parseInt(rating) === 5) {
                                ratingClass = "excellent";
                                ratingMsg = 'Excellent';
                            }
                            totExistingList += "<div class='col-md-4 pull-right'><div class='pull-right'><span>Rated: </span> <span class=" + ratingClass + "> " + ratingMsg + "</span> <span class='editRating' title='Edit Rating' onClick=\"helpUsToImprove(" + data[i].questionId + "," + rating + ",'" + data[i].comment + "')\" > <i class='fa fa-pencil' aria-hidden='true'></i></span></div></div>";
                        } else {
                            ratingMsg = "-";
                            totExistingList += "<div class='col-md-4 pull-right'><input type='submit' class='btn submitFeedback pull-right' value= 'Help Us Improve' onClick=\"helpUsToImprove(" + data[i].questionId + "," + rating + ",'" + data[i].comment + "')\" /></div>";
                        }
                        totExistingList += '</div>';

                        totExistingList += '</div>';
                    }
                    $("#recentQuestions").html("" + totExistingList);
                    addParentDiv();
                } else {
                    $("#recentQuestions").html("");
                    $('#footer').addClass("footer-bootom");
                }
            }

            function addParentDiv() {
                $('.addedTable').wrap("<div class=\"tableParent\"></div>");
                $('.tableParent').each(function () {
                    var width = $(this).find('table').width();
                    if (width > 600) {
                        $(this).css('overflow-x', 'scroll');
                    } else if (width < 600) {
                        $(this).css('overflow-x', 'none');
                    }
                });
            }

            function generateDisplayDate(addedDate) {
                var monthNames = [
                    "January", "February", "March",
                    "April", "May", "June", "July",
                    "August", "September", "October",
                    "November", "December"
                ];
                var formatDate = new Date(addedDate);
                var day = formatDate.getDate();
                var monthIndex = formatDate.getMonth();
                var year = formatDate.getFullYear();
                var displayDate = monthNames[monthIndex] + ' ' + day + ', ' + year;
                return displayDate;
            }
            function helpUsToImprove(questionId, rating, comment) {
                var params = {questionId: questionId, rating: rating, comment: comment};
                jQuery.getJSON("/user-messages.html?edit=rating", params,
                        function (data) {
                            parent.window.location = data;
                        });
            }

        </script> 

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-1 visible-lg"></div>
                <div class="col-lg-10">
                    <h1>Messages</h1>
                    <div class="inner-content">
                        <div id="recentQuestions"></div>
                    </div>
                </div>
                <div class="col-lg-1 visible-lg"></div>
            </div>
        </div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
