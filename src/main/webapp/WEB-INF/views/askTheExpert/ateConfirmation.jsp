<%-- 
    Document   : askTheExpertConfirmation
    Created on : 5 Oct, 2017, 1:10:04 PM
    Author     : netelixir
--%>

<%@page import="lxr.marketplace.askexpert.LxrmAskExpert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ASK THE EXPERT CONFIRMATION | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/ateConfirmation.css">
        <%
            if (session.getAttribute("lxrmAskExpert") != null) {
                LxrmAskExpert lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
                pageContext.setAttribute("lxrmAskExpert", lxrmAskExpert);
            }
            String expertUserName = "";
            if (session.getAttribute("username") != null) {
                expertUserName = (String) session.getAttribute("username");
            }
            pageContext.setAttribute("expertUserName", expertUserName);
        %>

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container ask-expert-block">
            <h2>GET EXPERT HELP</h2>
            <div class="clearfixdiv"></div>
            <springForm:form modelAttribute="lxrmAskExpert" method="POST">
                <div class="col-xs-12" style="border-bottom: 1px solid #ddd;"> 
                    <div class="col-md-6 col-xs-12 ate-issues-section">
                        <div class="col-xs-12 ate-heading-div">
                            <div class="col-xs-12 col-sm-2">
                                <a href="https://www.google.co.in/partners/#a_profile;idtf=1430663498" target="_blank">
                                    <img src="/resources/images/ask-expert-profile.png" alt="Ask the Expert Profile">
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-10" style="padding: 0;">
                                Our Google certified experts are ready to answer any questions you might have 
                                about your website or digital marketing campaign. How can we help you?
                            </div>
                        </div>
                        <div class="col-xs-12 ate-tool-issues-div">
                            <div class="tool-issues-div" id="tool-issues">${lxrmAskExpert.issues}</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 ate-inputs-section">
                        <c:if test="${lxrmAskExpert.toolId > 0}">
                            <div class="col-xs-12 ate-margin-top">
                                <div class="col-xs-12 col-md-3 label-div"><p>PRODUCT </p></div>
                                <div class="col-xs-12 col-md-9"><springForm:input path="toolName" cssClass="form-control col-xs-12" readonly="true"/></div>
                            </div>
                        </c:if>
                        <c:if test="${!lxrmAskExpert.domain.equals('')}">
                            <div class="col-xs-12 ate-margin-top">
                                <div class="col-xs-12 col-md-3 label-div"><p>WEBSITE URL </p></div>
                                <div class="col-xs-12 col-md-9"><springForm:input path="domain" cssClass="form-control col-xs-12"/></div>
                            </div>
                        </c:if>
                        <div class="col-xs-12 ate-margin-top">
                            <div class="col-xs-12 col-md-3 label-div"><p>YOUR EMAIL </p></div>
                            <div class="col-xs-12 col-md-9"><springForm:input path="email" cssClass="form-control col-xs-12" readonly="true"/></div>
                        </div>
                        <div class="col-xs-12 ate-margin-top">
                            <div class="col-xs-12 col-md-3 label-div"><p>YOUR QUESTION </p></div>
                            <div class="col-xs-12 col-md-9">
                                <springForm:textarea path="question" cssClass="form-control col-xs-12" rows="5" placeholder="Please ask your question"/>
                                <div class="col-xs-12 ate-input-errors lxr-danger" style="padding: 1%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="margin-top: 1%;">
                    <div class="col-xs-12 col-md-8">
                        <p><strong>Note: </strong>We want to help you as quickly as possible, but we may need a little bit of time assessing 
                            your specific question. A member of our expert team will respond to you at 
                            <span class="lxrm-bold">${lxrmAskExpert.email}</span> within the next 48 hours.</p>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <button class="col-xs-12 col-md-5 col-md-offset-1" id="ask-our-expert-cancel">CANCEL</button>
                        <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                        <button class="col-xs-12 col-md-5 pull-right" id="ask-our-expert">ASK</button>
                    </div>
                </div>
            </springForm:form>
        </div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

                $('#ask-our-expert').on('click', function () {
                    var userQuestion = $('#question').val().trim();
                    var statusMsg = "";
                    if (userQuestion === "") {
                        statusMsg = "Please enter question.";
                        $('#question').focus();
                    }
                    if (userQuestion !== "") {
                        if (userQuestion.length > 2048) {
                            statusMsg = "Please ensure the text is below 2048 characters";
                            $('#question').focus();
                        }
                    }
                    if ($('#domain').val() !== undefined) {
                        var helpUrl = $('#domain').val().trim();
                        if (helpUrl === "" || helpUrl === null)
                        {
                            statusMsg = "Please enter website URL name.";
                            $('#domain').focus();
                        }
                        if (helpUrl !== "")
                        {
                            var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                            if (!re.test(helpUrl)) {
                                statusMsg = "Please enter valid website name.";
                                $('#domain').focus();
                            }
                        }
                    }
                    if (statusMsg !== "") {
                        $(".ate-input-errors").html(statusMsg);
                        return false;
                    }
                    var f = $("#lxrmAskExpert");
                    f.attr('method', "POST");
                    f.attr('action', "/lxrm-question-payment.html");
                    f.submit();
                });
                $('#ask-our-expert-cancel').on('click', function () {
                    var f = $("#lxrmAskExpert");
                    f.attr('method', "POST");
                    f.attr('action', "/ask-expert-cancel.html");
                    f.submit();
                });
                // For user's auto login
            <c:if test="${!userLoggedIn}">
                var uname = "${userH.userName}";
                var pwd = "${userH.password}";
                pwd = encodeURIComponent(pwd);
                if (!(parentUrl.indexOf("loginRefresh") !== -1)) {
                    if (uname !== null && uname !== "" && pwd !== null && pwd !== "") {
                        login(uname, pwd, false, false);
                    } else if (parentUrl.indexOf("toolhelp") !== -1) {
                        $("#loginPopupModal").modal('show');
                        return false;
                    }
                }
            </c:if>
            });
        </script>
    </body>
</html>
