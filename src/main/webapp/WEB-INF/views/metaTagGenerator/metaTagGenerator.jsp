
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="lxr.marketplace.metataggenerator.MetaTagGeneratorTool"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Meta Tag Generator Tool - SEO Tools - LXR Marketplace</title>
        <meta name="description" content=" Try the Meta Tag Generator Tool at LXRMarketplace. Generate your meta tags online instantly to boost your search engines rankings. Try it out today!">
        <meta name="keywords" content="meta tag generator, meta tags, create meta tags, meta tag creator, meta tag builder">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/metaTagGenerator.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <%
            int toolDBId = 27;
            MetaTagGeneratorTool metaTagGeneratorTool = null;
            if (session.getAttribute("metaTagGeneratorTool") != null) {
                System.out.println("in exis");
                metaTagGeneratorTool = (MetaTagGeneratorTool) session.getAttribute("metaTagGeneratorTool");
                pageContext.setAttribute("metaTagGeneratorTool", metaTagGeneratorTool);
            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var isGenerated=true;
            var titleWarning = "<i class='fa fa-exclamation-triangle fa-1x' style='color:#FDA100;' aria-hidden='true'></i> Search engines like Google will truncate title in search results that exceed a certain length - about 65 characters.";
            var titleBelowWarning = "<i class='fa fa-exclamation-triangle fa-1x' style='color:#FDA100;' aria-hidden='true'></i> Your Title is less than 40 characters. There is a scope to add more words.";
            var descriptionBelowWarning = "<i class='fa fa-exclamation-triangle fa-1x' style='color:#FDA100;' aria-hidden='true'></i> Your Description is less than 70 characters. You could add more words to make it meaningful and informative.";
            var descriptionWarning = "<i class='fa fa-exclamation-triangle fa-1x' style='color:#FDA100;' aria-hidden='true'></i> Search engines like Google will truncate description in search results that exceed a certain length - about 160 characters.";
            var keywordWarning = "<i class='fa fa-exclamation-triangle fa-1x' style='color:#FDA100;' aria-hidden='true'></i> Search engines like Google will truncate keywords in search results that exceed a certain length - about 10 keywords.";

            $(document).ready(function () {
                $("#importMeta").bind('click', function () {
                    fetchmetaContentFromURL();
                     isGenerated=false;
                    toolLimitExceeded();

                });
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage='))}">
                <c:if test="${metaTagGeneratorTool.url ne null && not empty metaTagGeneratorTool.url}">
                $("#editImport").show();
                $("#url").addClass("gray");
                </c:if>
                <c:if test="${metaTagGeneratorTool.metaTagContent ne null && not empty metaTagGeneratorTool.metaTagContent}">
                $("#toolResultWrapper").show();
                </c:if>

            </c:if>

            <c:if test="${fn:contains(uri, 'backToSuccessPage=')}">
                /*Calling ask our expert function */
                setTimeout(getAskExpertPopup(true), 2000);
            </c:if>

                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }

//                toolLimitExceeded();

                $("#getResult").bind('click', function (e) {
                    resultLoginValue = true;
                    isGenerated=true;
//                    toolLimitExceeded();
                    var isValidALL = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var showAlert = true;
                    if (parentUrl.indexOf("limit-exceeded") !== -1 && userLoggedIn === 0) {
                        showAlert = false;
                        return false;
                    }
                    if (showAlert) {
                        var htmlContent = $("#toolQueryMessage").text();
                        if (htmlContent.includes("This URL cannot be reviewed")) {
                            $('#url').val("");
                            $("#toolQueryMessage").empty();
                        }
                        $("#titleError,#toolQueryMessage, #descriptionError, #keywordError").empty();
                        $("#titleError, #descriptionError, #keywordError").show();
                        var titleContent;
                        var descriptionContent;
                        var keywordContent;
                        var authorContent;
                        var queryURL;
                        if ($('#title').val().trim() === "" || $('#title').val() === null || $('#title').val() === 0)
                        {
                            $("#titleError").html('Please enter the title.');
                            $("#titleError").addClass('error');
                            $("#titleError").removeClass('lxrm-warning');
                            isValidALL = false;
                        } else {
                            $("#titleError").empty();
                        }
                        if ($('#description').val().trim() === "" || $('#description').val() === null || $('#description').val() === 0)
                        {
                            $("#descriptionError").html('Please enter the description.');
                            $("#descriptionError").addClass('error');
                            $("#descriptionError").removeClass('lxrm-warning');
                            isValidALL = false;
                        } else {
                            $("#descriptionError").empty();
                        }
                        var $this = $("#getResult");
                        if (isValidALL) {
                            $this.button('loading');
                            titleContent = $('#title').val().trim();
                            descriptionContent = $('#description').val().trim();
                            keywordContent = $('#keyword').val().trim();
                            authorContent = $('#author').val().trim();
                            queryURL = $('#url').val().trim();

                            $.ajax({
                                type: "POST",
                                url: "/meta-tag-generator-tool.html?query='getResult'",
                                processData: true,
                                data: {'queryURL': queryURL, 'titleContent': titleContent, 'descriptionContent': descriptionContent, 'keywordContent': keywordContent, 'authorContent': authorContent},
                                dataType: "json",
                                success: function (data) {
                                    $("#toolResultWrapper").show();
                                    $("#metaTagContent").empty();
                                    $("#metaTagContent").val(data[0].metaTagContent);
                                    $("#url").val(data[0].url);
                                    $('html, body').animate({scrollTop: $('#toolResultWrapper').offset().top}, 'slow');
                                    /*Calling ask our expert function */
                                    setTimeout(getAskExpertPopup(true), 2000);

                                },
                                error: function (jqxhr, textStatus, error) {
                                    if (textStatus.indexOf('error') !== -1) {
                                        toolLimitExceededForAjax();
                                    }
                                },
                                complete: function (xhr, textstatus) {
                                    $this.button('reset');
                                }
                            });
                        } else {
                            $this.button('reset');
                            return false;
                        }
                    }
                });
                $("#resetMetaTags").bind('click', function (e) {
                    $('#title, #description ,#keyword, #url, #author').val("");
//                    $("#getResult").removeClass('lxr-search-btn');
//                    $("#getResult").addClass('lxrm-btn-inactive');
                    var f = $("#metaTagGeneratorTool");
                    f.attr('action', "meta-tag-generator-tool.html?query=clearAll");
                    f.submit();
                });
                $('#url').keypress(function (ev) {
                    if (ev.which === 13) {
                        fetchmetaContentFromURL();
                    }
                });
                $('#title').keypress(function (ev) {
                    performValidation('title');
                    if (ev.which === 13) {
                        fetchmetaContentFromURL();
                    }
                });
                /*To handle backspace event*/
                $('#title').keyup(function (ev) {
                    performValidation('title');
                });

                $('#keyword').keypress(function (ev) {
                    performValidation('keyword');
                });
                /*To handle backspace event*/
                $('#keyword').keyup(function (ev) {
                    performValidation('keyword');
                });

                $('#description').keypress(function (ev) {
                    performValidation('description');
                });
                /*To handle backspace event*/
                $('#description').keyup(function (ev) {
                    performValidation('description');
                });

                $('#url').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#importMeta").click();
                        return false;
                    }
                });
                $("#editImport, #url").click(function () {
                    $("#url").removeClass('gray');
                    $("#editImport").hide();
                });
            });
            function  fetchmetaContentFromURL() {
                resultLoginValue = true;
                $("#toolQueryMessage, #titleError, #descriptionError").empty();
                if ($('#url').val().trim() === "" || $('#url').val() === null || $('#url').val() === 0) {
                    $("#toolQueryMessage").html("Please enter your URL.");
                    $('#url').focus();
                    return false;
                } else if (!urlValidation($('#url').val().trim())) {
                    $("#toolQueryMessage").html("Please enter a valid URL.");
                    $('#url').focus();
                    return false;
                }
                var queryURL = $('#url').val().trim();
                var $this = $("#importMeta");
                $this.button('loading');
                $.ajax({
                    type: "POST",
                    url: "/meta-tag-generator-tool.html?query='importMeta'&metaURL=" + queryURL + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        $("#toolResultWrapper").hide();
                        $("#metaTagContent").empty();
                        if (data[0] !== null)
                        {
                            $this.button('reset');
                            if (data[0].description.includes("This URL cannot be reviewed")) {
                                $("#toolQueryMessage").empty();
                                $("#toolQueryMessage").html(data[0].description);
                                $("#title").val("");
                                $("#description").val("");
                                $("#keyword").val("");
                                $("#author").val("");
                                getAskExpertPopup(true);
                            } else {
                                $("#url").val(data[0].url);
                                $("#title").val(data[0].title);
                                setTimeout(performValidation('title'), 500);

                                $("#description").val(data[0].description);
                                setTimeout(performValidation('description'), 500);

                                $("#keyword").val(data[0].keyword);
                                setTimeout(performValidation('keyword'), 500);

                                $("#author").val(data[0].author);
                                $("#editImport").show();
                                $("#url").addClass("gray");
                            }
                        }
                    },
                    error: function (jqxhr, textStatus, error) {
                        if (textStatus.indexOf('error') !== -1) {
                            toolLimitExceededForAjax();
                        }
                    },
                    complete: function (xhr, textstatus) {
                        $this.button('reset');
                        // xhr.responseText contains the response from the server
                        var allheaders = xhr.getAllResponseHeaders();
                        if (allheaders.indexOf("limitexceed") > 0) {
                            location.reload();
                        }
                    }
                });
            }
            function performValidation(inputFieldName) {
                var charachterLength = $.trim($("#" + inputFieldName).val()).length;
                if (charachterLength > 0) {
                    switch (String(inputFieldName)) {
                        case "title":
                            if (charachterLength <= 40) {
                                $("#" + inputFieldName + "Error").empty();
                                $("#" + inputFieldName + "Error").show();
                                $("#" + inputFieldName + "Error").removeClass('error');
                                $("#" + inputFieldName + "Error").addClass('lxrm-warning');
                                $("#" + inputFieldName + "Error").html(titleBelowWarning);
                            } else if (charachterLength >= 66) {
                                $("#" + inputFieldName + "Error").empty();
                                $("#" + inputFieldName + "Error").show();
                                $("#" + inputFieldName + "Error").removeClass('error');
                                $("#" + inputFieldName + "Error").addClass('lxrm-warning');
                                $("#" + inputFieldName + "Error").html(titleWarning);
                            } else if (charachterLength >= 41 && charachterLength <= 65) {
                                $("#" + inputFieldName + "Error").empty();
                            }
                            break;
                        case "description":
                            if (charachterLength <= 70) {
                                $("#" + inputFieldName + "Error").empty();
                                $("#" + inputFieldName + "Error").show();
                                $("#" + inputFieldName + "Error").removeClass('error');
                                $("#" + inputFieldName + "Error").addClass('lxrm-warning');
                                $("#" + inputFieldName + "Error").html(descriptionBelowWarning);
                            } else if (charachterLength >= 161) {
                                $("#" + inputFieldName + "Error").empty();
                                $("#" + inputFieldName + "Error").show();
                                $("#" + inputFieldName + "Error").removeClass('error');
                                $("#" + inputFieldName + "Error").addClass('lxrm-warning');
                                $("#" + inputFieldName + "Error").html(descriptionWarning);
                            } else if (charachterLength >= 71 && charachterLength <= 160) {
                                $("#" + inputFieldName + "Error").empty();
                            }
                            break;
                        case "keyword":
                            var keywordsLength = $.trim($("#keyword").val());
                            if (keywordsLength.split(",").length >= 11) {
                                $("#" + inputFieldName + "Error").empty();
                                $("#" + inputFieldName + "Error").show();
                                $("#" + inputFieldName + "Error").removeClass('error');
                                $("#" + inputFieldName + "Error").addClass('lxrm-warning');
                                $("#" + inputFieldName + "Error").html(keywordWarning);
                            } else {
                                $("#" + inputFieldName + "Error").empty();
                            }
                            break;
                    }
                } else if (charachterLength === 0) {
                    $("#" + inputFieldName + "Error").empty();
                    $("#" + inputFieldName + "Error").removeClass('error');
                    $("#" + inputFieldName + "Error").removeClass('lxrm-warning');
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                var url = $("#url").val().trim();
                $.ajax({
                    type: "POST",
                    url: "/meta-tag-generator-tool.html?query='meatTagATE'&metaURL=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            function getUserToolInputs(website, userInput) {
                $('#url').val(website);
                $('#importMeta').click();
            }
            function submitPreviousRequest() {
                if(isGenerated===true){
                $("#getResult").click();
            }
            else{
                $("#importMeta").click();
            }
        }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <!--<div class="hidden-xs clearfixdiv-common"></div>-->
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="metaTagGeneratorTool" method="POST">
                <div class="col-xs-12">
                    <div class="col-lg-12">
                        <div class="form-group col-lg-9 col-md-9 col-sm-12 col-xs-12" style="padding-left: 0px">
                            <div class="icon-addon addon-lg">
                                <springForm:input placeholder="Enter your URL" path="url"  cssClass="form-control input-lg srch-term" />
                                <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                            </div>
                            <span class="lxr-danger text-left error" id="toolQueryMessage"></span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn">
                                <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="importMeta"  data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> IMPORTING META TAGS">
                                    IMPORT META TAGS
                                </button>
                            </div>
                        </div>   
                    </div>
                    <div class="clearfixdiv" style="height: 20px !important;"></div>
                    <div class="col-xs-12 toolInputWrapper">
                        <span id="toolInputTitle" class="lxrm-bold">Create your own meta tags:</span>
                    </div>
                    <div class="clearfixdiv hidden-xs" style="height: 15px !important;"></div>
                    <div class="col-xs-12 toolInputWrapper">
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <div class="visible-md visible-lg lxrm-empty-div"></div>
                            <p class="lxrm-bold">Title:</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10">
                            <div class="form-group">
                                <div class="icon-addon addon-lg">
                                    <springForm:textarea placeholder="Title upto 60 characters" path="title"  cssClass="form-control input-lg srch-term" rows="1" cssStyle="resize:none;"/>
                                </div>
                                <span class="text-left" id="titleError"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 toolInputWrapper">
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <div class="visible-md visible-lg lxrm-empty-div" style="height: 25px;"></div>
                            <p class="lxrm-bold">Description:</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10">
                            <div class="form-group">
                                <div class="icon-addon addon-lg">
                                    <springForm:textarea placeholder="Description 2-3 sentences and do not use line breaks"  path="description" rows="3" cssClass="form-control input-lg srch-term" cssStyle="resize:none;"/>
                                </div>
                                <span class="text-left" id="descriptionError"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 toolInputWrapper">
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <div class="visible-md visible-lg lxrm-empty-div"></div>
                            <p class="lxrm-bold">Keywords:</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10">
                            <div class="form-group">
                                <div class="icon-addon addon-lg">
                                    <springForm:input placeholder="Keywords separated by commas, up to 255 characters"  path="keyword" cssClass="form-control input-lg srch-term"/>
                                </div>
                                <span class="text-left"  id="keywordError"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 toolInputWrapper">
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <div class="visible-md visible-lg lxrm-empty-div"></div>
                            <p class="lxrm-bold">Author:</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-10">
                            <div class="form-group">
                                <div class="icon-addon addon-lg">
                                    <springForm:input placeholder="Author for page" path="author"  cssClass="form-control input-lg srch-term"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--</div>-->
                <div class="clearfixdiv" style="height: 20px;"></div>
                <div class="row" id="toolButtonsWrapper" align="center">
                    <div class="col-xs-12">
                        <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <!--<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">-->
                            <div class="input-group-btn">
                                <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetMetaTags">
                                    <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                </button>
                            </div>
                            <!--</div>--> 
                        </div>
                        <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                        <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="input-group-btn">
                                <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult"  data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING">
                                    &nbsp;GENERATE&nbsp;
                                </button>
                            </div>
                            <!--</div>--> 
                        </div>
                        <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="row" id="toolResultWrapper">
                    <div class="col-md-12  panel-content" style="padding: 0px">
                        <div class="panel-heading lxr-panel-heading">
                            <span class="toolLabel" style="color:#999">GENERATED</span>
                            <span class="toolLabel" style="color:#4F4F4F">META TAG CONTENT</span>
                            <p>Please copy the code below and paste in the head tag on your webpage.</p>
                        </div>
                        <div class="panel-body lxr-panel-body">
                            <div class="col-lg-12 col-xs-12" style="padding: 0;">
                                <springForm:textarea path="metaTagContent" rows="8" cssClass="form-control hiddenscrollbars"/>
                            </div>
                        </div>
                    </div>
                </div>
            </springForm:form>
        </div>
        <div class="clearfixdiv"></div>
        <!--Recommended part start-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>