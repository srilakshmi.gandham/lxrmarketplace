
<%@page import="lxr.marketplace.competitorwebpagemonitor.CmpImgInfo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="lxr.marketplace.competitorwebpagemonitor.CmpLinksInfo"%>
<%@page import="lxr.marketplace.competitorwebpagemonitor.CmpSummary"%>
<%@page import="lxr.marketplace.competitorwebpagemonitor.CompetitorWebpageMonitor"%>
<%@page import="lxr.marketplace.competitorwebpagemonitor.CompetitorWebpageMonitor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Free Competitor Web Page Monitor Tool | LXR Marketplace</title>
        <meta name="description"
              content="Try the Competitor Webpage Monitor Tool at LXRMarketplace. Keep track of any changes your competitor implements on their site with alerts. Save time today!">
        <meta name="keywords"
              content="Competitor Web Page Monitor, Competitor Web Page Monitor Tool">

        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/compWebpageMonitor.css">
        <script type="text/javascript" src="/resources/js/tools/compWebPageMonitor.js"></script>
        <!--<script src="/resources/css/tools/tooltabs.js"></script>-->
        <%
            int toolDBId = 9;
            CompetitorWebpageMonitor cmpSettingsData = null;
            if (session.getAttribute("cmpSettingsData") != null) {
                cmpSettingsData = (CompetitorWebpageMonitor) session.getAttribute("cmpSettingsData");
            }
            Map<Integer, List<CmpLinksInfo>> linkResults = null;
            CmpSummary cmpSummary = null;
            if (session.getAttribute("linkResults") != null) {
                linkResults = new HashMap<>();
                linkResults = (Map<Integer, List<CmpLinksInfo>>) session.getAttribute("linkResults");
                cmpSummary = null;
                if (session.getAttribute("CmpSummaryInfo") != null) {
                    cmpSummary = (CmpSummary) session.getAttribute("CmpSummaryInfo");
                }
            }
            List<CmpLinksInfo> newLinksListTemp = null;
            if (linkResults != null && linkResults.size() > 0) {
                newLinksListTemp = linkResults.get(2);
            }
            Calendar TimeStop = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(" dd MMM");
            String lastdataDay = sdf.format(TimeStop.getTime());
            if (session.getAttribute("lastdataDay") != null) {
                lastdataDay = sdf.format(session.getAttribute("lastdataDay"));
            }
            String freq = (String) session.getAttribute("frequency");
            if (freq == null || freq == "") {
                freq = "Days";
            }
            pageContext.setAttribute("linkResults", linkResults);
            pageContext.setAttribute("cmpSummary", cmpSummary);
            pageContext.setAttribute("frequency", freq);
        %>


    </head>
    <script>
        $(document).ready(function () {
            $("#getResult").bind('click', function (e) {
                if (!${userLoggedIn}) {
                    $("#loginPopupModal").modal('show');
                    return false;
                }
                var status = true;
                var competitorUrl = $('#competitorUrl').val().trim();
                var emailid = $("#email").val().trim();
                if (competitorUrl === "" || competitorUrl === null || competitorUrl === 0) {
                    document.getElementById('competitorUrlError').textContent = "Please enter your competitor website URL";
                    document.getElementById('competitorUrlError').style.display = 'block';
                    $('#competitorUrl').focus();
                    status = false;
                } else {
                    document.getElementById('competitorUrlError').style.display = 'none';
                }
               if (emailid === "" || emailid === null || emailid === 0) {
                    document.getElementById('emailError').textContent = "Please provide email id";
                    document.getElementById('emailError').style.display = 'block';
                    $('#email').focus();
                    status = false;
                } else if (!isValidMail(emailid)) {
                    document.getElementById('emailError').textContent = "Please enter a valid email address";
                    document.getElementById('emailError').style.display = 'block';
                    $('#email').focus();
                    status = false;
                } else {
                    document.getElementById('emailError').style.display = 'none';
                }

                if (status) {
                    var $this = $(this);
                    $this.button('loading');
                    var f = $("#compWebpageMonitorTool");
                    f.attr('action', "competitor-webpage-monitor-tool.html?getResult='getResult'");
                    f.submit();
                }
            });
        });
    </script>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="compWebpageMonitorTool" method="POST">
                <c:set var="settings" value="${settings}" />
                <div class="col-lg-12">

                    <div class="form-group col-lg-4 ">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your competitor website URL" path="competitorUrl" cssClass="form-control input-lg srch-term"/>
                            <i id="editImport1" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="competitorUrlError" class="error"></span>
                    </div>
                    <div class="form-group col-lg-4 ">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your email address" path="email" cssClass="form-control input-lg srch-term"/>
                            <i id="editImport2" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="emailError" class="error"></span>
                    </div>
                    <div class="form-group col-lg-2 ">
                        <div class="icon-addon addon-lg">
                            <springForm:select path="alertFrequency" cssClass="form-control input-lg srch-term">
                                <springForm:option value="1" label="Daily" />
                                <springForm:option value="2" label="Weekly" />
                            </springForm:select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-12 col-xs-12 text-center" style="padding-left: 0px;">
                        <div class="input-group-btn" >
                            <c:if test="${settings.equals('hide')}"> 
                                <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12" id="getResult"  data-loading-text="<i class='fa fa-refresh fa-spin'></i> ANALYZING">
                                    ANALYZE
                                </button>
                            </c:if>

                        </div>
                    </div>
                </div>
                <c:if test="${settings.equals('show')}"> 
                    <div class="col-lg-12 ">

                        <div class="form-group col-lg-3 ">
                            <springForm:checkbox path="emailUnsubscription" value="1" />  Unsubscribe from Emails
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                               title="You will not receive the updates through email.You can visit this page to know the results.">
                                <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="form-group col-lg-5 ">
                            <springForm:checkbox path="toolUnsubscription" value="2" /> Unsubscribe from Competitor Webpage Monitor Tool
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                               title="You will be unsubscribed from the tool and the data fetch will be stopped. You can resubscribe anytime.">
                                <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="form-group col-lg-2 ">
                            <button type="button" class="btn btn-block nextBtn lxr-search-btn" id="edit"  data-loading-text="<i class='fa fa-refresh fa-spin'></i> PROCESSING..">
                                SAVE
                            </button>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${fn:contains(Mailsent, 'unable') || fn:contains(Mailsent, 'not updated')}">
                            <div class="col-lg-12 lxr-danger" style="text-align: center;">${Mailsent}</div>
                        </c:when>
                        <c:otherwise>
                            <div class="col-lg-12 lxr-success" style="text-align: center;">${Mailsent}</div>
                        </c:otherwise>
                    </c:choose>
                    <hr class="col-lg-12">
                </c:if>

            </springForm:form>
        </div>

        <div class="container-fluid ">                                                                            
            <c:set var="result" value="${result}" />
            <c:if test="${result.equals('show')}"> 

                <div id="firstcontent-1" class="links-tab">
                    <div class="box-styles">
                        <h3 class="lxrm-bold"><i class="fa fa-link" aria-hidden="true"></i> Internal Links</h3>
                        <p>Internal Linking in SEO refers to linking the content on your pages with each other. 
                            This means, linking certain phrases or words within the body text of your pages to 
                            other pages, where relevant.</p>
                    </div>
                    <p style="padding: 10px 6px 0px 6px;">
                        Internal links in SEO are important to both visitors to your site and search engines. 
                        Both visitors and search engines will navigate your site using the same internal links. 
                        <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">OPTIMIZATION </strong>
                        of internal links is one of the factors to improve your rankings in search engines.
                    </p>
                    <div id="exTab1">
                        <ul class="nav nav-tabs tab-text-color" style="display: flex;">
                            <li class="active">
                                <a href="#1" data-toggle="tab">Change Summary</a>
                            </li>
                            <li>
                                <a href="#2" data-toggle="tab">New Links</a>
                            </li>
                            <li>
                                <a href="#3" data-toggle="tab">Removed Links</a>
                            </li>
                        </ul>
                        <div class="tab-content clearfix">

                            <div class="tab-pane active col-lg-12" id="1">
                                <div class="col-lg-4 col-xs-12" >
                                    <table class="col-xs-12">
                                        <thead><tr><td colspan="2"><h5>Change Summary&nbsp;-&nbsp; <%=cmpSummary.getResultDate()%></h5></td></tr></thead>
                                        <tbody>
                                            <% if (cmpSummary != null) {%>
                                            <tr>
                                                <td> Total Links </td>
                                                <td><%=cmpSummary.getTotalUrls()%> </td>
                                            </tr>
                                            <tr>
                                                <td>Links Added</td>
                                                <td> <%=cmpSummary.getNewUrls()%> </td>
                                            </tr>
                                            <tr>
                                                <td>Links Removed</td>
                                                <td><%=cmpSummary.getRemovedUrls()%></td>
                                            </tr>
                                            <% } else { %>
                                            <tr>
                                                <td>Total Links</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>Links Added</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>Links Removed</td>
                                                <td>0</td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-8 col-xs-12">
                                    <h5>Links Changes for last 7 <%=freq%></h5>
                                    <div id="linksgraph">
                                        <div id="linkchartdiv" class="table-responsive hiddenscrollbars"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="2" >

                                <ul class="set-scroll" style="list-style-type: none; line-height: 25px;">
                                    <%  if (linkResults != null && linkResults.size() > 0) {
                                            List<CmpLinksInfo> newLinksList = linkResults.get(2);
                                            if (newLinksList != null && newLinksList.size() > 0) {
                                                if (cmpSummary != null) {
                                                    if (cmpSummary.getNewUrls() != 0) {
                                                        for (CmpLinksInfo cmpLink : newLinksList) {%>
                                    <li>
                                        <span class="hypen">-</span>&nbsp;
                                        <a href="<%=cmpLink.getAncLink()%>" class="urlLinks"  target="_blank">
                                            <%=cmpLink.getAncLink()%>
                                        </a>
                                    </li>
                                    <% }
                                    } else {%>
                                    <li><p >No added internal links found.</p></li>
                                        <%}
                                        } else {%>
                                    <li><p >No added internal links found.</p></li>
                                        <%}
                                        } else {%>
                                    <li><p >No added internal links found.</p></li>
                                        <%}
                                        } else {%>
                                    <li><p >No added internal links found.</p></li>
                                        <%} %>
                                </ul>
                            </div>
                            <div class="tab-pane" id="3">

                                <ul class="set-scroll" style="list-style-type: none; line-height: 25px;">
                                    <%  if (linkResults != null && linkResults.size() > 0) {
                                            List<CmpLinksInfo> removedList = linkResults.get(3);
                                            if (removedList != null && removedList.size() > 0) {
                                                if (cmpSummary != null) {
                                                    if (cmpSummary.getRemovedUrls() != 0) {
                                                        for (CmpLinksInfo cmpLink : removedList) {%>
                                    <li>
                                        <span class="hypen">-</span>&nbsp;
                                        <a href="<%=cmpLink.getAncLink()%>" class="urlLinks" target="_blank">
                                            <%=cmpLink.getAncLink()%>
                                        </a>
                                    </li>
                                    <% }
                                    } else {%>
                                    <li><p>No removed internal links found.</p></li>
                                        <% }
                                        } else {%>
                                    <li><p>No removed internal links found.</p></li>
                                        <% }
                                        } else {%>
                                    <li id="noLinks"><p >No removed internal links found.</p></li>
                                        <% }
                                        } else {%>
                                    <li><p>No removed internal links found.</p></li>
                                        <% }%>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--how to fix-->
                <div id="howtofix-1" style="display: none;">
                    <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd;">
                        <div class="panel panel-default">
                            <div class="panel-heading lxr-panel-heading">
                                <span class="panel-title lxr-success">Below are the best practices to create Internal links</span>
                                <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                            </div>
                            <div class="panel-body lxr-panel-body">
                                <p>1. Use descriptive keywords in anchor text that give a sense of the topic.</p>
                                <p>2. The deeper your links go, the better you rank for those key terms or anchors.</p>
                                <p>3. Use links that are natural for the reader. Don't link just for the sake of ranking.</p>
                                <p>4. Use relevant links. Don't just add home page and contact page in the content.</p>
                                <p>5. Use a reasonable number of internal links.</p>
                                <p>6. Do not create massive blocks of site-wide footer links.</p>
                                <div class="lxr-panel-footer">
                                    <h3><spring:message code="ate.expert.label.2" /></h3>
                                    <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>

                <div class="clearfixdiv"></div>
                <div id="firstcontent-2" class="content-tab">
                    <div class="box-styles">
                        <h3 class="lxrm-bold"><i class="fa fa-file-text-o" aria-hidden="true"></i> Content</h3>
                        <p>By “content” we mean any information that lives on the web and can be consumed on the web. 
                            It can be pieces of information regarding your product or category on the page that can add 
                            value to both search engine results and users alike.</p>
                    </div>
                    <p style="padding: 10px 6px 0px 6px;">
                        Follow the 
                        <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(2);">STEPS </strong>
                        for best practices of creating content and benefits of the content. </p>

                    <div id="exTab2">	
                        <ul class="nav nav-tabs tab-text-color" style="display: flex;">
                            <li class="active">
                                <a href="#5" data-toggle="tab">Change Summary</a>
                            </li>
                            <li>
                                <a href="#6" data-toggle="tab">Text Changes</a>
                            </li>
                            <li>
                                <a href="#7" data-toggle="tab">Image Changes</a>
                            </li>
                        </ul>

                        <div class="tab-content clearfix">
                            <div class="tab-pane active col-lg-12" id="5">

                                <div class="col-lg-4 col-xs-12">
                                    <table class="col-xs-12">
                                        <thead><tr><td colspan="2"><h5>Change Summary&nbsp;-&nbsp; <%=cmpSummary.getResultDate()%></h5></td></tr></thead>
                                        <tbody>
                                            <% if (cmpSummary != null) {%>
                                            <tr>
                                                <td> Total Words </td>
                                                <td><%=cmpSummary.getTotalWords()%> </td>
                                            </tr>
                                            <tr>
                                                <td>Words Added</td>
                                                <td> <%=cmpSummary.getNewWords()%> </td>
                                            </tr>
                                            <tr>
                                                <td>Words Removed</td>
                                                <td><%=cmpSummary.getRemovedWords()%></td>
                                            </tr>
                                            <% } else { %>
                                            <tr>
                                                <td>Total Words</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>Words Added</td>
                                                <td>0</td>
                                            </tr>
                                            <tr>
                                                <td>Words Removed</td>
                                                <td>0</td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-8 col-xs-12">
                                    <h5>Content Changes for last 7 <%=freq%> </h5>
                                    <div id="contentgraph">
                                        <div id="contentchartdiv" class="table-responsive hiddenscrollbars"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="6">
                                <div id="textchanges" class="set-scroll"></div>
                            </div>
                            <div class="tab-pane" id="7">
                                <div class=" col-lg-12 col-xs-12 set-scroll">
                                    <p style="color: green;">Note :&nbsp;Only your site related images will be considered.</p>
                                    <table class="col-lg-8 col-xs-12">
                                        <thead>
                                            <tr>
                                                <th>Alt Text</th>
                                                <th>Image</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  boolean noImg = false, remImg = false;
                                                Map<Integer, List<CmpImgInfo>> imgList = new HashMap<Integer, List<CmpImgInfo>>();
                                                imgList = (Map<Integer, List<CmpImgInfo>>) session.getAttribute("imgResults");
                                                if (imgList != null && imgList.size() > 0) {
                                                    List<CmpImgInfo> imgResults = imgList.get(1);
                                                    List<CmpImgInfo> removedImgs = imgList.get(2);
                                                    if (imgResults != null && imgResults.size() > 0) {
                                                        for (CmpImgInfo cmpImg : imgResults) {%>
                                            <tr>
                                                <td style="word-wrap: break-word;">
                                                    <%if (cmpImg.getAltText() != null && !cmpImg.getAltText().trim().equals("")) {%>
                                                    <%=cmpImg.getAltText()%> <% }%>
                                                </td>
                                                <td>
                                                    <%  String finalImgPath = "";
                                                        if (cmpImg.getImgPath() != null) {
                                                            if (cmpImg.getImgPath().startsWith("/disk2/lxrmarketplace")) {
                                                                finalImgPath = cmpImg.getImgPath().replace("/disk2/lxrmarketplace", "");
                                                            }
                                                        }%> 
                                                    <a class="Regular" href="<%=cmpImg.getImgSrc()%>" target="_blank">
                                                        <img src="<%=finalImgPath%>" />
                                                    </a>
                                                </td>
                                                <td style="text-align: center;">
                                                    <i class="fa fa-plus" aria-hidden="true" style="color: green;font-size: 36px;"></i>
                                                </td>
                                            </tr>
                                            <%  }
                                                } else {
                                                    noImg = true;
                                                }
                                                if (removedImgs != null && removedImgs.size() > 0) {
                                                    for (CmpImgInfo cmpImg : removedImgs) {%>
                                            <tr>
                                                <td><%=cmpImg.getAltText()%></td>
                                                <%  String finalImgPath = "";
                                                    if (cmpImg.getImgPath() != null) {
                                                        if (cmpImg.getImgPath().startsWith("/disk2/lxrmarketplace")) {
                                                            finalImgPath = cmpImg.getImgPath().replace("/disk2/lxrmarketplace", "");
                                                        }
                                                    }
                                                %>
                                                <td>
                                                    <a class="" href="<%=cmpImg.getImgSrc()%>" target="_blank">
                                                        <img src="<%=finalImgPath%>" />
                                                    </a>
                                                </td>
                                                <td style="text-align: center;">
                                                    <i class="fa fa-minus" aria-hidden="true" style="color: red;font-size: 36px;"></i>
                                                </td>
                                            </tr>
                                            <%  }
                                                } else {
                                                    remImg = true;
                                                }
                                                if (remImg && noImg) {%>
                                            <tr>
                                                <td colspan="3" class="text-center">No image changes found.</td>
                                            </tr>
                                            <%  }
                                            } else {
                                            %>
                                            <tr>
                                                <td colspan="3" class="text-center">No image changes found.</td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--how to fix-->
                <div id="howtofix-2" style="display: none;">
                    <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd;">
                        <div class="panel panel-default">
                            <div class="panel-heading lxr-panel-heading">
                                <span class="panel-title lxr-success">Below are the steps for best practices to create content and benefits </span>
                                <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                            </div>
                            <div class="panel-body lxr-panel-body">

                                <p><strong>Below are some of the benefits</strong></p>
                                <p>1. Can improve your presence in search engines.</p>
                                <p>2. Content can help you achieve better rankings.</p>
                                <p>3. User engagement will increase on the website if your content is unique.</p>

                                <p>
                                    <strong>The content on your site should be organized in a logical way. 
                                        You can follow the below steps while creating text
                                    </strong>
                                </p>
                                <p>1. Write in a compelling way to your readers.</p>
                                <p>2. Content should be created in such a way that your user should not look for any alternatives.</p>
                                <p>3. Organize your text for bots (titles with keywords, relatively short paragraphs, fine hierarchy of headings).</p>
                                <p>4. Maximize targeted keywords (no keyword stuffing) using LSI keywords instead of exact match keywords.</p>
                                <p>5. Cluster keywords to rank your articles for a set of keywords for maximum exposure.</p>
                                <p>6. Implement Text Analysis of existing content (body and titles).</p>
                                <p>7. Avoid keyword cannibalization.</p>
                                <p>8. Do not copy content from any source.</p>


                                <div class="lxr-panel-footer">
                                    <h3><spring:message code="ate.expert.label.2" /></h3>
                                    <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </c:if>
        </div>
        <!--   Success Page END-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            var globalGoogleData = null;
            var googleCharts = "false";
            var result = "hide";
            var reportDownload = 0;
            var downloadReportType = "";
            var showLogin = null;
            var messageToUser = "${Mailsent}";
            if ('${login}' !== "") {
                showLogin = '${login}';
            }
            var listSize = 0;

            $(document).ready(function () {
                isPageRefresh = true;
                if (!${userLoggedIn}) {
                    $("#loginPopupModal").modal('show');
                    return false;
                } else if ($("#email").val() === null || $("#email").val() === "") {
                    $("#email").val("${userH.userName}");
                }

                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();
            <c:if test="${result.equals('show') || fn:contains(uri, 'editSettings=') || fn:contains(uri, 'backToSuccessPage=') || fn:contains(uri, 'backToSuccessPage=')}">
                getAskExpertPopup(true);
            </c:if>
                if ('${connectionerror}' !== '' && '${connectionerror}' !== null) {
                    $('#competitorUrlError').html('${connectionerror}');
                    $('#competitorUrlError').show();
                }
                if ('${notWorkingUrl}' !== '' && '${notWorkingUrl}' !== null) {
                    $('#competitorUrlError').html('${notWorkingUrl}');
                    $('#competitorUrlError').show();
                }
                if ('${UrlnotWorking}' !== '' && '${UrlnotWorking}' !== null) {
                    $('#competitorUrlError').html('${UrlnotWorking}');
                    $('#competitorUrlError').show();
                }

                googleCharts = '${googleCharts}';
                result = '${result}';
                if (googleCharts === "true") {
                    getGoogleCharts();
                }

                if (result === "show") {
                    appendTextChanges();
                    $('#competitorUrl,#email').addClass("gray");
                    $("#editImport1,#editImport2").show();
                }

                $('#competitorUrl').on('click', function () {
                    $("#editImport1").hide();
                    $("#competitorUrl").removeClass("gray");
                    $("#competitorUrl").focus();

                });

                $('#email').on('click', function () {
                    $("#editImport2").hide();
                    $("#email").removeClass("gray");
                    $("#email").focus();

                });


                $("#edit").bind('click', function (e) {

                    resultLoginValue = true;
                    /* Login POPUP on tool limit exceeded */
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var status = true;
                    var competitorUrl = $('#competitorUrl').val().trim();
                    var emailid = $("#email").val().trim();

                    if (competitorUrl === "" || competitorUrl === null || competitorUrl === 0) {
                        document.getElementById('competitorUrlError').textContent = "Please enter your competitor website URL";
                        document.getElementById('competitorUrlError').style.display = 'block';
                        $('#competitorUrl').focus();
                        status = false;
                    } else {
                        document.getElementById('competitorUrlError').style.display = 'none';
                    }

                    var emailid = $("#email").val().trim();
                    if (emailid === "" || emailid === null || emailid === 0) {
                        document.getElementById('emailError').textContent = "Please provide email address";
                        document.getElementById('emailError').style.display = 'block';
                        $('#email').focus();
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        document.getElementById('emailError').textContent = "Please enter a valid email address";
                        document.getElementById('emailError').style.display = 'block';
                        $('#email').focus();
                        status = false;
                    } else {
                        document.getElementById('emailError').style.display = 'none';
                    }

                    if (status) {
                        var $this = $(this);
                        $this.button('loading');

                        var f = $("#compWebpageMonitorTool");
                        f.attr('action', "competitor-webpage-monitor-tool.html?editSettings='editSettings'");
                        f.submit();
                        $('#edit').attr('disabled', true);
                        $('#competitorUrl').attr('disabled', true);
                    }


                });
            });
            function appendTextChanges() {
            <%String[] textDiff;
                String textChanges = "";
                if (session.getAttribute("textChanges") != null) {
                    textChanges = (String) session.getAttribute("textChanges");
                    out.println("cntchanges=\"" + textChanges + "\";");%>
                $("#textchanges").html(cntchanges);
                $("#textchanges").css("padding-left", "2%");
            <%} else {%>
                $("#textchanges").html("No content changes found.");
                $("#textchanges").css("padding", "2%");
                //$("#textchanges").css("font-weight", "bold");
            <%}%>
//                GetDivHeight('textchanges');
            }
            function getGoogleCharts() {
                var jqxhr = $.get("/competitor-webpage-monitor-tool.html?getGoogleChart='getGoogleChart'", function (data) {
                    var datetype = data[1];
                    if (datetype !== null) {
                        drawChart(data[0], datetype);
                    } else {
                        drawChart(data[0], 1);
                    }
                }, "json");
                jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                    $('<span">No data available</span>').appendTo('#linkchartdiv');
                    $('<span">No data available</span>').appendTo('#contentchartdiv');
                    $("#linkchartdiv").css("min-height", "200px");
                    $("#linkchartdiv").css("padding-left", "40%");
                    $("#linkchartdiv").css("padding-top", "15%;");
                    $("#contentchartdiv").css("min-height", "200px");
                    $("#contentchartdiv").css("padding-left", "40%");
                    $("#contentchartdiv").css("padding-top", "15%;");
                });
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                var url = $('#competitorUrl').val();
                if (!(messageToUser.indexOf("We are unable to fetch your data") >= 0)) {
                    $.ajax({
                        type: "POST",
                        url: "/competitor-webpage-monitor-tool.html?ate='compMonitorATE'&domain=" + url + "",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            if (data[0] !== null)
                            {
                                $("#tool-issues").html(data[0].issues);
                                $("#ateDomain").val(data[0].domain);
                                issues = data[0].issues;
                                question = data[0].question;
                                toolName = data[0].toolName;
                                otherInputs = data[0].otherInputs;
                            }
                        }
                    });
                }
            }

            function getUserToolInputs(website, userInput) {
                var userToolInputs = JSON.parse(userInput);
                $('#competitorUrl').val(website);
                $('#email').val(userToolInputs["2"]);
                $('#alertFrequency').val(userToolInputs["3"]);
                $('#emailUnsubscription1').val(userToolInputs["4"]);
                $('#toolUnsubscription1').val(userToolInputs["5"]);
                $('#getResult').click();
            }

            function hideHowToFixAndShowContent(id) {
                $('#howtofix-' + id).hide();
                $('#firstcontent-' + id).show(1000);
            }

            function hideContentAndShowHowToFix(id) {
                $('#firstcontent-' + id).hide();
                var contentheight = $('#howtofix-' + id).outerHeight();
                $('.content').css('background-color', 'white');
                $('#howtofix-' + id).show(1000);
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </body>
</html>