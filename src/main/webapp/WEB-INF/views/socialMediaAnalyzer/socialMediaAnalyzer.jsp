<%-- 
    Document   : SocialMediaAnalyzer
    Created on : Sep 09, 2017, 11:24:49 AM
    Author     : NE16T1213/ K.Sagar
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List,java.util.LinkedList"%>
<%@page import="lxr.marketplace.socialmedia.SocialCompDomains"%>
<%@page import="lxr.marketplace.socialmedia.SocialMedia"%>
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.PinterestMetrics"%>
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.YouTubeMetrics"%>
<%--<%@page import="lxr.marketplace.apiaccess.lxrmbeans.GoogleMetrics"%>--%>
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.FacebookMetrics"%>
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.TwitterMetrics"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Social Media Analyzer | LXR Marketplace</title>
        <meta name="description" content="Use LXR Marketplace's easy-to-use Social media Analyzer Tool. 
              Get a snapshot of your social media reach and engagement by each social media channel. Try it now!">
        <meta name="keywords" content="Social Media Tool, Social Media Checker, Social Media Analysis">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/socialmediaanalyzer.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <script type="text/javascript" src="/resources/js/tools/socialMediaAnalyzer.js" ></script>
        <%
            int toolDBId = 29;
            Map<String, Object> socialMediaObject = null;
            if (session.getAttribute("socialmediaObjects") != null) {
                socialMediaObject = (Map<String, Object>) session.getAttribute("socialmediaObjects");
                pageContext.setAttribute("socialMediaObject", socialMediaObject);
            }
            SocialMedia socialMediaTool = null;
            if (session.getAttribute("socialMediaTool") != null) {
                socialMediaTool = (SocialMedia) session.getAttribute("socialMediaTool");
            } else {
                socialMediaTool = new SocialMedia();
            }
            pageContext.setAttribute("socialMediaTool", socialMediaTool);
            Map<Integer, String> invalidURLS = null;
            if (session.getAttribute("toolInValidList") != null) {
                invalidURLS = new HashMap<>();
                invalidURLS = (Map<Integer, String>) session.getAttribute("toolInValidList");
            }
            pageContext.setAttribute("invalidURLS", invalidURLS);
        %>
        <script type="text/javascript">

            var callAjax = false;

            var settingsSucMsg = '${settingsSucMsg}';
            var isResultPage = false;
            var reportDownload = 0;
            var socialuserId = 0;
            var downloadReportType = "";
            var buttonType = 0;/* 1.Save and 2.Cancel */

            $(document).ready(function () {
                isPageRefresh = true;
                $("#save").click(function () {
                    buttonType = 1;
                    resultLoginValue = true;
                    processSocialAnalysis('save');
                });
                $("#getCompetitorResult").click(function () {
                    processSocialAnalysis('getCompetitorResult');
                });

                $("#getSMAReport").click(function () {
                    downloadSMAReport();
                });
                $('#userEmailId').keypress(function (ev) {
                    if (ev.which === 13) {
                        $('#mailReport').click();
                        return false;
                    }
                });
                $("#socialMediaTool :input[id='mediaUrl'], #socialMediaTool :input[id='competitorDomainOne'],#socialMediaTool :input[id='competitorDomainTwo'],#socialMediaTool :input[id='comp2'],#socialMediaTool :input[id='comp3']").keypress(
                        function (e) {
                            if (e.which === 13) {
                                return false;
                            }
                        });

                /*To auto populate for invalid URL(s) in input fields*/
                var existedURL;
            <c:if test="${pageScope.invalidURLS !=null && not empty pageScope.invalidURLS}">
                <c:choose>
                    <c:when test="${userLoggedIn && userIdInSocialUser != 0}" >
                        <c:forEach var="entry" items="${pageScope.invalidURLS}" varStatus="status">
                            <c:choose>
                                <c:when test="${entry.key == 2}" >
                existedURL = $("#comp2").val();
                if ((existedURL === null || existedURL === "")) {
                    $("#comp2").val("${entry.value}");
                }
                                </c:when>
                                <c:when test="${entry.key == 3}" >
                existedURL = $("#comp3").val();
                if ((existedURL === null || existedURL === "")) {
                    $("#comp3").val("${entry.value}");
                }
                                </c:when>
                            </c:choose>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="entry" items="${pageScope.invalidURLS}" varStatus="status">
                            <c:choose>
                                <c:when test="${entry.key == 2}" >
                $("#competitorDomainOne").val("${entry.value}");
                                </c:when>
                                <c:when test="${entry.key == 3}" >
                $("#competitorDomainTwo").val("${entry.value}");
                                </c:when>
                            </c:choose>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            <c:if test="${(getSuccess eq true) && empty uri}">
                $("#submitWrapper").css('padding-bottom', '0px');
                getAskExpertPopup(true);
            </c:if>
                /* To handle/raise auto download event, When user logged in from popup or header. */
                if (parentUrl.indexOf("report") !== -1) {
                    downloadSMAReport();
                }
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();
                $("#comptetiorUrls").val("");
                $("#comptetiorUrls").empty();

                /*Checking below conditions
                 * User login status
                 * User registered and active for tool.
                 * User unsubscrition from tool.*/
            <c:choose>
                <c:when test="${socialMediaTool.toolUnsubscription}">
                /*User unsubscribed from tool*/
                    <c:if test="${fn:length(socialMediaTool.compDomainSettingRows) ge 2}">
                /*manageInputFields('show');*/
                $(".regUserAccess").prop("readonly", true);
                $("#submitWrapper").css('padding-bottom', '0px');
                    </c:if>
                disableToolAccess();
                </c:when>
                <c:when test="${(userLoggedIn && userIdInSocialUser != 0 && !socialMediaTool.toolUnsubscription) || callAjax}">
                /*Checking for user login status for analyzing data*/
                getMetricAnalysisInJson();
                $(".analysisMessages").html("<p>${analysisMessage}</p>");
                /*Disabling input fields in result screen
                 disableInputFields();*/
                </c:when>
            </c:choose>



                /*Tool Tip*/
                $('[data-toggle="tooltip"]').tooltip();
                /*To Handle event of checkbox for tool unsubscription.*/
                $('#toolUnsubscription1').change(function () {
                    if (this.checked) {
                        var returnVal = confirm("Are you sure to unsubscribe from the tool?");
                        $(this).prop("checked", returnVal);
                    } else {
                        $(".regUserAccess").prop("readonly", false);
                    }
                });


                $('#userEmailId').click(function () {
                    $('#mailIdMessage').empty();
                    $('#mailIdMessage').hide();
                    return false;
                });
                /*Functions related to download, send report to mail*/
                $("#mailReport").click(function () {
                    var emailid = $.trim($("#userEmailId").val());
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').css("color", "red");
                    if (!(emailid)) {
                        $('#mail-sent-status').html("Please provide email id.");
                        $("#userEmailId").focus();
                        return false;
                    } else if (!isValidMail(emailid)) {
                        $('#mail-sent-status').html("Please enter a valid email address.");
                        $("#userEmailId").focus();
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/social-media-analyzer.html?download=sendmail&email=" + emailid,
                            processData: true,
                            data: {},
                            dataType: "json",
                            success: function (data) {
                            },
                            complete: function () {
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                            }
                        });
                    }
                });
                $("#cancel").bind('click', function (event) {
                    buttonType = 2;
                    resultLoginValue = true;
                    var form = $("#socialMediaTool");
                    form.attr('action', "/social-media-analyzer.html?cancel='cancel'");
                    form.submit();
                });
            });/*EOF of doc ready function*/
            function applyEdit(inputId, editID) {
                $("#" + inputId).addClass("gray");
                $("#" + editID).show();
            }
            /*common function to submit user query*/
            function processSocialAnalysis(buttonId) {
                if (!toolLimitExceeded()) {
                    return false;
                }
            <c:choose>
                <c:when test="${userLoggedIn && userIdInSocialUser !=0}">
                return processUserQuery('existingUser', buttonId);
                </c:when>
                <c:when test="${!userLoggedIn || (userLoggedIn && userIdInSocialUser == 0)}">
                return processUserQuery('guestUser', buttonId);
                </c:when>
            </c:choose>
            }
            function disableToolAccess() {
                /*Disabling all input fields and showing setting options to resubscribe to tool*/
                $("#socialMediaTool :input[type=text]").prop("readonly", true);
                $('input[name="toolOptions"]').attr('readonly', 'readonly');
                $('input[type=radio]').attr('readonly', true);
                $("#toolAccessSettings").show(1000);
            }
            /*Submiting request to download Social Media Analyzer report in pdf format */
            function downloadSMAReport() {
                if (userLoggedIn) {
                    var form = $("#socialMediaTool");
                    form.attr('method', "POST");
                    form.attr('action', "/social-media-analyzer.html?download=download");
                    form.submit();
                } else {
                    reportDownload = 1;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
            <c:if test="${(getSuccess eq true)}">
                var socialUrl = $("#mediaUrl").val();
                $.ajax({
                    type: "POST",
                    url: "/social-media-analyzer.html?socialMediaATE='socialMediaATE'&socialUrl=" + socialUrl + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            </c:if>
            }
            function focusInputfield(inputFieldName, editName) {
                $("#errorMessage").html("");
                $("#" + editName).hide();
                $("#" + inputFieldName).removeClass("gray");
                $("#" + inputFieldName).focus();
            }

            function disableInputFields() {
                $("#socialMediaTool :input[type=text]").each(function () {
                    var inputValue = $(this).val();
                    var siblingId;
                    if (inputValue !== "" && inputValue !== null) {
                        $(this).attr('readonly', true);
                        siblingId = $(this).siblings().attr('Id');
                        $("#" + siblingId + "").css("color", "#2f80ed");
                    } else if (inputValue === "" || inputValue === null) {
                        $(this).attr('readonly', false);
                    }
                });
            }
            function submitPreviousRequest() {
                if (buttonType == 1) {
                    $("#save").click();
                } else {
                    $("#cancel").click();
                }

            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <!--textboxes search part start-->
        <div class="container">
            <div class="clearfixdiv"></div>
            <springForm:form commandName="socialMediaTool" method="POST">
                <div id="toolInputWrapper" class="col-lg-12">
                    <c:set var="toolUserId" value="${userIdInSocialUser}"/>
                    <c:choose>
                        <c:when test="${!userLoggedIn || (userLoggedIn && toolUserId == 0)}">
                            <div id="userURLWrapper" class="form-group col-lg-4">
                                <div class="icon-addon addon-lg">
                                    <springForm:input placeholder="Enter your URL" path="mediaUrl" onclick="focusInputfield('mediaUrl', 'userDomainEdit')" value="${socialMediaTool.mediaUrl}" cssClass="form-control input-lg srch-term"/>
                                    <i id="userDomainEdit" onclick="focusInputfield('mediaUrl', 'userDomainEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                </div>
                                <span id="userDomainError" class="lxr-danger text-center" ></span>
                            </div>
                            <springForm:hidden path="comptetiorUrls"/>
                            <div class="form-group col-lg-3 lxr-hide-input">
                                <div class="icon-addon addon-lg">
                                    <input class="form-control input-lg" onclick="focusInputfield('competitorDomainOne', 'compOneURlEdit')" placeholder="Competitor #1 (optional)" id="competitorDomainOne"  type="text"/>
                                    <i id="compOneURlEdit" onclick="focusInputfield('competitorDomainOne', 'compOneURlEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                </div>
                                <span id="competitorOneError" class="lxr-danger text-center" ></span>
                            </div>
                            <div class="form-group col-lg-3 lxr-hide-input">
                                <div class="icon-addon addon-lg">
                                    <input class="form-control input-lg" onclick="focusInputfield('competitorDomainTwo', 'compTwoURlEdit')" placeholder="Competitor #2 (optional)" id="competitorDomainTwo"  type="text"/>
                                    <i id="compTwoURlEdit" onclick="focusInputfield('competitorDomainTwo', 'compTwoURlEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                </div>
                                <span id="competitorTwoError" class="lxr-danger text-center" ></span>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" id="resultbuttonWrapper" style="padding-left: 0px">
                                <div class="input-group-btn" id="getCompetitorResultWrapper">
                                    <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="getCompetitorResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                        ANALYZE
                                    </button>
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <p id="errorMessage" class="lxr-danger text-center" >${messageToUser}</p>
                            </div>
                        </c:when>
                        <c:when test="${userLoggedIn && toolUserId != 0 }">
                            <!--User should login and should have socialMediaId-->
                            <c:forEach items="${socialMediaTool.compDomainSettingRows}" varStatus="userDomName">
                                <spring:bind path="socialMediaTool.compDomainSettingRows[${userDomName.index}].userDomain">
                                    <c:set var="totDomCount" value="${status.value}" />
                                </spring:bind>
                                <c:if test='${totDomCount == true}'>
                                    <spring:bind path="socialMediaTool.compDomainSettingRows[${userDomName.index}].domainId">
                                        <input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
                                    </spring:bind>
                                    <spring:bind path="socialMediaTool.compDomainSettingRows[${userDomName.index}].domain">
                                        <div id="userURLWrapper" class="form-group col-lg-4">
                                            <div class="icon-addon addon-lg">
                                                <input id='mediaUrl' type="text" onclick="focusInputfield('mediaUrl', 'userDomainEdit')" class="form-control input-lg"  placeholder="Enter your URL" name="${status.expression}" value="${status.value}"/>
                                                <i id="userDomainEdit" onclick="focusInputfield('mediaUrl', 'userDomainEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                            </div>
                                            <span id="userDomainError" class="lxr-danger text-center" ></span>
                                        </div>
                                    </spring:bind>
                                </c:if>
                            </c:forEach>
                            <c:set var="competitorCount" value="1"/>
                            <c:forEach items="${socialMediaTool.compDomainSettingRows}" varStatus="socialCompDomain">
                                <spring:bind path="socialMediaTool.compDomainSettingRows[${socialCompDomain.index}].userDomain">
                                    <input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
                                    <c:choose>
                                        <c:when test='${status.value != true}'>
                                            <spring:bind path="socialMediaTool.compDomainSettingRows[${socialCompDomain.index}].domainId">
                                                <input type="hidden"  name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
                                            </spring:bind>
                                            <spring:bind path="socialMediaTool.compDomainSettingRows[${socialCompDomain.index}].domain">
                                                <c:choose>
                                                    <c:when test="${competitorCount == 2}">
                                                        <div class="form-group col-lg-4">
                                                            <div class="icon-addon addon-lg">
                                                                <input type="text" onclick="focusInputfield('comp${competitorCount}', 'compOneURlEdit')" class="form-control input-lg" placeholder="Competitor #${competitorCount-1} (optional)" id="comp${competitorCount}" name="${status.expression}" value="${status.value}" />
                                                                <i id="compOneURlEdit" onclick="focusInputfield('comp${competitorCount}', 'compOneURlEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                                            </div>
                                                            <span id="competitorOneError" class="lxr-danger text-center" ></span>
                                                        </div>
                                                    </c:when>
                                                    <c:when test="${competitorCount == 3}">
                                                        <div class="form-group col-lg-4">
                                                            <div class="icon-addon addon-lg">
                                                                <input type="text" onclick="focusInputfield('comp${competitorCount}', 'compTwoURlEdit')" class="form-control input-lg" placeholder="Competitor #${competitorCount-1} (optional)" id="comp${competitorCount}" name="${status.expression}" value="${status.value}" />
                                                                <i id="compTwoURlEdit" onclick="focusInputfield('comp${competitorCount}', 'compTwoURlEdit')" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                                                            </div>
                                                            <span id="competitorTwoError" class="lxr-danger text-center" ></span>
                                                        </div>
                                                    </c:when>        
                                                </c:choose>
                                            </spring:bind>
                                        </c:when>
                                    </c:choose>
                                </spring:bind>
                                <c:set var="competitorCount" value="${competitorCount+1}" scope="page"/>
                            </c:forEach>
                            <div class="form-group col-lg-12">
                                <p id="errorMessage" class="lxr-danger text-center" style="font-size:1.1em;" >${messageToUser}</p>
                            </div>
                        </c:when>
                    </c:choose>
                </div>
                <c:if test="${userLoggedIn && toolUserId !=0}">
                    <div id="toolAccessSettings" class="row col-lg-12 text-center ">
                        <span class="col-xs-12 col-sm-12 col-md-4"><springForm:checkbox path="weeklyReport" /> Send me Weekly PDF Report through email.</span>
                        <span class="col-xs-12 col-sm-12 col-md-4"><springForm:checkbox path="toolUnsubscription" /> Unsubscribe from Social Media Analyzer. 
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                               title="You will be unsubscribed from the tool and the data fetch will be stopped. You can re-subscribe anytime by unchecking">
                                <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;" aria-hidden="true"></i>
                            </a>
                        </span>
                        <div class="col-xs-12 col-md-4">
                            <button class="col-xs-12 col-md-5 col-md-offset-1" id="cancel">CANCEL</button>
                            <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                            <button type="button" class="col-xs-12 col-md-5 pull-right btn  top-line buttonStyles" id="save" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> SAVING..">SAVE</button>
                        </div>
                    </div>
                </c:if>
                <!--<div class="row error" id="queryURLMessage" ></div>-->
            </springForm:form>
        </div>
        <div class="clearfixdiv"></div>
        <!--vertical tab system-->
        <c:if test="${(getSuccess eq true)}"> 
            <div class="container-fluid">
                <div class="row lxrtool-tabs">
                    <div class="tabs">
                        <!--1-->
                        <div class="tab ">
                            <button class="tab-toggle active lxrtool-tabs-heading">
                                <div class="row" id="channelActivityWrapper">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <!--                                        <div class="pull-right lxr-arrows" id="googleActivityWrapper">
                                                                                    <div style="float: left">
                                                                                        <p><span id="googleplusmetricsTop" class="glyphicon glyphicon-triangle-top lxr-smaActivityTraingle"></span>
                                                                                        <p><span id="googleplusmetricsBottom" class="glyphicon glyphicon-triangle-bottom lxr-smaActivityTraingle"></span> </p>
                                                                                    </div>
                                                                                    <span>&nbsp;<i class="fa fa-google-plus-square" style="color:#ea4335;font-size: 35px;" aria-hidden="true"></i>&nbsp; </span>
                                                                                </div>-->
                                        <div class="pull-right lxr-arrows" id="youtubeActivityWrapper">
                                            <div style="float: left">
                                                <p><span id="youtubemetricsTop" class="glyphicon glyphicon-triangle-top lxr-smaActivityTraingle"></span>
                                                <p><span id="youtubemetricsBottom" class="glyphicon glyphicon-triangle-bottom lxr-smaActivityTraingle"></span> </p>
                                            </div>
                                            <span>&nbsp;<i class="fa fa-youtube-square" style="color:#bd081c;font-size: 35px;" aria-hidden="true"></i>&nbsp; </span>
                                        </div>
                                        <div class="pull-right lxr-arrows" id="pinterestActivityWrapper">
                                            <div style="float: left">
                                                <p><span id="pinterestmetricsTop" class="glyphicon glyphicon-triangle-top lxr-smaActivityTraingle"></span>
                                                <p><span id="pinterestmetricsBottom" class="glyphicon glyphicon-triangle-bottom lxr-smaActivityTraingle"></span> </p>
                                            </div>
                                            <span>&nbsp;<i class="fa fa-pinterest-square" style="color:#bd081c;font-size: 35px;" aria-hidden="true"></i>&nbsp; </span>
                                        </div>
                                        <div class="pull-right lxr-arrows" id="twitterActivityWrapper">
                                            <div style="float: left">
                                                <p><span id="twittermetricsTop" class="glyphicon glyphicon-triangle-top lxr-smaActivityTraingle"></span>
                                                <p><span id="twittermetricsBottom" class="glyphicon glyphicon-triangle-bottom lxr-smaActivityTraingle"></span>  </p>
                                            </div>

                                            <span>&nbsp;<i class="fa fa-twitter-square" style="color:#1c94e0;font-size: 35px;" aria-hidden="true"></i> &nbsp;</span>
                                        </div>
                                        <div class="pull-right lxr-arrows" id="facebookActivityWrapper">
                                            <div style="float: left">
                                                <p><span id="facebookmetricsTop" class="glyphicon glyphicon-triangle-top lxr-smaActivityTraingle"></span>
                                                <p><span id="facebookmetricsBottom" class="glyphicon glyphicon-triangle-bottom lxr-smaActivityTraingle"></span></p>
                                            </div>
                                            <span>&nbsp;<i class="fa fa-facebook-official" style="color:#3d5a99;font-size: 35px;" aria-hidden="true"></i> &nbsp;</span>
                                        </div>
                                    </div>
                                </div>
                                <span class="lxr-socialtabone-header">ACTIVITY</span>   
                                <p>There is lot's of activities in social media, Activities likes, Video Sharing, Photo Sharing, Create Business Pages, Content Sharing, Comments & Tweets.</p>
                            </button>
                        </div>
                        <!--table-->
                        <div class="content main-content-1 active">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1" style="padding: 0px">
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">ACTIVITY</span>
                                        <p><strong  class="lxr-steps nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">CHECK </strong>the Activity benefits for Social Media channels.</p>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row analysisMessages"></div>
                                        <div class="row analysisURLs"></div>
                                        <hr>
                                        <!--1 facebook channel wrapper starts-->
                                        <div class="col-xs-12 lxrmetric-title" id="facebookWrapper">
                                            <div class="channelTitle"><i class="fa fa-facebook-official" style="color:#3d5a99"></i> FACEBOOK </div>
                                            <div id="fbMetric">
                                                <div class="metricTitle">Likes</div>
                                                <div class="row lxr-socialmediar-row" id="faceBookLikesWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                            </div>
                                            <div class="lxr-socialmediar-row" id="faceBookNoData"></div>
                                        </div>
                                        <!--2 Twitter channel wrapper starts-->
                                        <div class="col-xs-12 lxrmetric-title" id="twitterWrapper">
                                            <div class="channelTitle"><i class="fa fa-twitter-square" style="color:#1c94e0"></i> TWITTER </div>
                                            <div id="twMetrics">
                                                <div class="metricTitle">Followers</div>
                                                <div class="row lxr-socialmediar-row" id="twitterFollowersWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                                <div class="metricTitle">Tweets</div>
                                                <div class="row lxr-socialmediar-row" id="twitterTweetsWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                                <div class="metricTitle">Likes</div>
                                                <div class="row lxr-socialmediar-row" id="twitterLikesWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                                <div class="metricTitle">Following</div>
                                                <div class="row lxr-socialmediar-row" id="twitterFollowingWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                            </div>
                                            <div class="lxr-socialmediar-row" id="twitterNoData"></div>
                                        </div>
                                        <!--4 YouTube channel wrapper starts-->
                                        <div class="col-xs-12 lxrmetric-title" id="youtubeWrapper">
                                            <div class="channelTitle"><i class="fa fa-youtube-square" style="color:#bd081c"></i> YOUTUBE </div>
                                            <div id="ytMetrics">
                                                <div class="metricTitle">Subscribers</div>
                                                <div class="row lxr-socialmediar-row" id="youtubeSubscribersWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                                <div class="metricTitle">Videos</div>
                                                <div class="row lxr-socialmediar-row" id="youtubeVideosWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                                <div class="metricTitle">Views</div>
                                                <div class="row lxr-socialmediar-row" id="youtubeViewsWrapper"></div>
                                                <div class="clearfixdiv"></div>
                                            </div>
                                            <div class="lxr-socialmediar-row" id="youtubeNoData"></div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!--how to fix-->
                            <div  id="howtofix-1" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 panel-content" style="padding: 0px">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-success">Below are the Activity benefits for Social Media channels.</span>
                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <p> Traffic Generation.</p>
                                            <p> Building Brand.</p>
                                            <p> Public Relations.</p>
                                            <p> More Credibility More customers.</p>
                                            <div class="lxr-panel-footer">
                                                <h3><spring:message code="ate.expert.label.1" /></h3>
                                                <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <!--2 tab start here-->
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>HASH TAGS </span>
                                <p>A hashtag is a keyword or phrase headed by the hash symbol (#), that people include in their social media posts.</p>
                            </button>
                        </div>
                        <div class="content main-content-2">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2" style="padding: 0px">
                                <div class="panel panel-default">
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">HASH TAGS</span>
                                        <p>Best way to use Hash Tags in Social Media by following these <strong  class="lxr-steps partially_notwork" onClick="hideContentAndShowHowToFix(2);">STEPS.</strong></p>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row analysisMessages"></div>
                                        <div class="row analysisURLs" ></div>
                                        <hr>
                                        <div class="channelTitle"><i class="fa fa-twitter-square" style="color:#1c94e0" aria-hidden="true"></i> TWITTER</div> 
                                        <!--Twitter Hash Tags-->
                                        <div class="row" id="twitterHashTagWrapper">
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!--how to fix-->
                            <div  id="howtofix-2" style="border: 0px red solid;display: none">
                                <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-success">Best way to use Hash Tags in Social Media  by these STEPS</span>
                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <p>1. Company Hashtag- Simple and worth recall. Use it on all social accounts. Monitor its use by the audience.</p>
                                            <p>2. Hashtag for a particular Campaign/Event- Use a relevant, catchy and shareable hashtag here in accordance with your campaign.</p>
                                            <p>3. Use already trending hashtags to company's advantage. Keep up with time and make sure your posts/tweets are sharp. </p>
                                            <div class="lxr-panel-footer">
                                                <h3><spring:message code="ate.expert.label.1" /></h3>
                                                <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>

                        <!--3-->
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>POSTS </span> 
                                <p>If you are sharing any thought or any picture on social media website so this activity called as post.</p>
                            </button>
                        </div>
                        <div class="content main-content-3">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3" style="padding: 0px">
                                <div class="panel panel-default">
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">POSTS</span>
                                        <p>Your recent/latest activity for each channel. Best way to Manage Social Media Posts by following these <strong  class="lxr-steps total_url" onClick ="hideContentAndShowHowToFix(3);">STEPS.</strong></p>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row analysisMessages"></div>
                                        <div class="row postMetricsURL">
                                            <!--<div class="col-xs-12 col-sm-12 col-md-12 lxr-socialmediar-col text-center splitWord"><i class="fa fa-briefcase" aria-hidden="true"></i> ${socialMediaTool.mediaUrl}</div>-->
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="channelTitle"><i class="fa fa-facebook-official" style="color:#3d5a99" aria-hidden="true"></i> FACEBOOK</div>
                                            <c:choose>
                                                <c:when test="${socialMediaObject.facebookmetrics.facebookAccountStatus && ((socialMediaObject.facebookmetrics.postImagePath ne null && not empty socialMediaObject.facebookmetrics.postImagePath) || (socialMediaObject.facebookmetrics.postdescription ne null && not empty socialMediaObject.facebookmetrics.postdescription))}">
                                                    <div class="row">
                                                        <!--<div class="well">-->
                                                        <div class="media">
                                                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-center" >
                                                                <c:if test="${(socialMediaObject.facebookmetrics.postImagePath ne null && not empty socialMediaObject.facebookmetrics.postImagePath)}">
                                                                    <img class="media-object" width="100" height="100" alt='FaceBook Recent Post Image' src="${socialMediaObject.facebookmetrics.postImagePath}"/>
                                                                </c:if>
                                                            </div>
                                                            <div class="col-xs-12 visible-xs" style="height: 10px;display:block;"></div>
                                                            <div class="col-xs-12 col-sm-10 col-md-10 col-md-10" >
                                                                <div class="media-body">
                                                                    <c:if test="${socialMediaObject.facebookmetrics.postCreatedTime ne null && not empty socialMediaObject.facebookmetrics.postCreatedTime}">
                                                                        <h4 class="media-heading"><c:out value="${socialMediaObject.facebookmetrics.postCreatedTime}" /></h4>
                                                                    </c:if>
                                                                    <p><c:out value="${socialMediaObject.facebookmetrics.postdescription}"/></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--</div>-->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 hidden-xs visible-sm visible-md visible-lg"></div>
                                                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <div style="overflow: auto;">
                                                                <table class="table table-striped lxrtool-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center">Likes</th>
                                                                            <th class="text-center">Comments</th>
                                                                            <th class="text-center">Shares</th>
                                                                            <th class="text-center">Engagement</th>
                                                                            <th class="text-center">Audience Engaged</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.facebookmetrics.postLikes}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.facebookmetrics.postcomments}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.facebookmetrics.postShares}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.facebookmetrics.engagement}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.facebookmetrics.audienceEngaged}" />%</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <!--<div class="media-body noPostData">Unable to fetch your recent post data.</div>-->
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPostData">Unable to fetch your recent post data.
                                                        <div class="clearfixdiv"></div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <!--2-->
                                        <div class="row">
                                            <div class="channelTitle"><i class="fa fa-twitter-square" style="color:#1c94e0" aria-hidden="true"></i> TWITTER</div> 
                                            <c:choose>
                                                <c:when test="${socialMediaObject.twittermetrics.twitterAccountStatus && ((socialMediaObject.twittermetrics.readPath ne null && not empty socialMediaObject.twittermetrics.readPath) || (socialMediaObject.twittermetrics.recentTweetText ne null && not empty socialMediaObject.twittermetrics.recentTweetText))}">
                                                    <div class="row">
                                                        <div class="media">
                                                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-center" >
                                                                <c:if test="${socialMediaObject.twittermetrics.readPath ne null && not empty socialMediaObject.twittermetrics.readPath}">
                                                                    <img class="media-object" width="100" height="100" alt='Twitter Recent Post Image' src="${socialMediaObject.twittermetrics.readPath}"/>
                                                                </c:if>
                                                            </div>
                                                            <div class="col-xs-12 visible-xs" style="height: 10px;display:block;"></div>
                                                            <div class="col-xs-12 col-sm-10 col-md-10 col-md-10" >
                                                                <div class="media-body">
                                                                    <c:if test="${socialMediaObject.twittermetrics.tweetDate ne null && not empty socialMediaObject.twittermetrics.tweetDate}">
                                                                        <h4 class="media-heading"><c:out value="${socialMediaObject.twittermetrics.tweetDate}" /></h4>
                                                                    </c:if>
                                                                    <p><c:out value="${socialMediaObject.twittermetrics.recentTweetText}"/></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 hidden-xs visible-sm visible-md visible-lg"></div>
                                                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <div style="overflow: auto;">
                                                                <table class="table table-striped lxrtool-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center">Retweets</th>
                                                                            <th class="text-center">Likes</th>
                                                                            <th class="text-center">Engagement</th>
                                                                            <th class="text-center">Audience Engaged</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.twittermetrics.recentTweetRetweets}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.twittermetrics.recentTweetFavourites}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.twittermetrics.engagement}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.twittermetrics.audienceEngaged}" />%</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPostData">Unable to fetch your recent post data.
                                                        <div class="clearfixdiv"></div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div> 
                                        <!--2-->
                                        <div class="row">
                                            <div class="channelTitle"><i class="fa fa-youtube-square" style="color:#bd081c" aria-hidden="true"></i> YOUTUBE</div> 
                                            <c:choose>
                                                <c:when test="${socialMediaObject.youtubemetrics.youtubeAccountStatus && ((socialMediaObject.youtubemetrics.youtubeUserLogPath ne null && not empty socialMediaObject.youtubemetrics.youtubeUserLogPath)|| (socialMediaObject.youtubemetrics.recentMediaTitle ne null && not empty socialMediaObject.youtubemetrics.recentMediaTitle))}">
                                                    <div class="row">
                                                        <div class="media">
                                                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-center" >
                                                                <c:if test="${socialMediaObject.youtubemetrics.youtubeUserLogPath ne null && not empty socialMediaObject.youtubemetrics.youtubeUserLogPath}">
                                                                    <img class="media-object" width="100" height="100" alt='YouTube Recent Post Image' src="${socialMediaObject.youtubemetrics.youtubeUserLogPath}"/>
                                                                </c:if>
                                                            </div>
                                                            <div class="col-xs-12 visible-xs" style="height: 10px;display:block;"></div>
                                                            <div class="col-xs-12 col-sm-10 col-md-10 col-md-10" >
                                                                <div class="media-body">
                                                                    <c:if test="${socialMediaObject.youtubemetrics.userPostDate ne null && not empty socialMediaObject.youtubemetrics.userPostDate}">
                                                                        <h4 class="media-heading"><c:out value="${socialMediaObject.youtubemetrics.userPostDate}" /></h4>
                                                                    </c:if>
                                                                    <p><c:out value="${socialMediaObject.youtubemetrics.recentMediaTitle}"/></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-2 col-md-2 col-lg-2 hidden-xs visible-sm visible-md visible-lg"></div>
                                                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                            <div style="overflow: auto;">
                                                                <table class="table table-striped lxrtool-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="text-center">Views</th>
                                                                            <th class="text-center">Likes</th>
                                                                            <th class="text-center">Comments</th>
                                                                            <th class="text-center">Engagement</th>
                                                                            <th class="text-center">Audience Engaged</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.youtubemetrics.recentMediaviews}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.youtubemetrics.recentMediaLikes}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.youtubemetrics.recentMediaComments}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.youtubemetrics.engagement}" /></td>
                                                                            <td class="text-center"><c:out value="${socialmediaObjects.youtubemetrics.audienceEngaged}" />%</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPostData">Unable to fetch your recent post data.
                                                        <div class="clearfixdiv"></div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!--how to fix-->
                            <div  id="howtofix-3" style="border: 0px red solid;display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-success">Best way to Manage Social Media Posts by following these STEPS.</span>
                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox3" onclick="hideHowToFixAndShowContent(3);"></span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <p>1. Post at least once in a day or two.</p>
                                            <p>2. Don't just post about offers and your products, post something that is informative to the fans.</p>
                                            <p>3. Make the page more vibrant by adding Reviews and Videos.</p>
                                            <p>4. Respond to the comments.</p>
                                            <p>5. Use images.</p>
                                            <p>6. Use hashtags.</p>
                                            <div class="lxr-panel-footer">
                                                <h3><spring:message code="ate.expert.label.1" /></h3>
                                                <button class="btn" onclick="submitATEQuery('3');">Get Help</button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row download-report-row">
                    <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                    <div class="col-md-9 col-sm-12 col-xs-12 download-report-div">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                            <p style="margin: 3% 0;">Download complete report here: </p></div>
                        <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter your email" name="EmailId" id="userEmailId" value="${userH.userName}">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" style="border: 1px #ccc solid !important;" id="mailReport"><i class="glyphicon glyphicon-send"></i></button>
                                </div>
                            </div>
                            <div id="mail-sent-status" style="font-family: ProximaNova-Regular;" class="emptyData"></div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <div class="dropdown">
                                <span id="getSMAReport" class="glyphicon glyphicon-download-alt"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <!--   Success Page END-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>



