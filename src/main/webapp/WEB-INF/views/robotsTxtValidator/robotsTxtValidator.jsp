<%-- 
    Document   : robotsTxtValidator
    Created on : 23 Oct, 2017, 1:05:14 PM
    Author     : NE16T1213-Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Map, java.util.List, java.util.HashMap, java.util.Set, lxr.marketplace.robotstxtvalidator.RobotsTxtValidatorResult"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Try the Robots.txt validator  at LXRMarketplace. Identify, analyze, and validate your existing Robots.txt file errors. Try it now!">
        <meta name="keywords" content=" Robots.txt Analyzer, Robots.txt Validation Tool, Robots.txt Validator, Robots.txt Checker">
        <title>Robots.txt Validator Tool Online - | LXRMarketplace.com</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/robotsTxtValidator.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <%
            int toolDBId = 25;
            Map<Integer, List<RobotsTxtValidatorResult>> validationMap = null;
            if (session.getAttribute("validationMap") != null) {
                validationMap = (Map<Integer, List<RobotsTxtValidatorResult>>) session.getAttribute("validationMap");
                pageContext.setAttribute("validationMap", validationMap);
            }
            boolean validationError = false;
            if (session.getAttribute("noError") != null) {
                validationError = (boolean) (Boolean) session.getAttribute("noError");
            }
            pageContext.setAttribute("validationError", validationError);
            int totalErrorCount = 0, totalWarningCount = 0;
            pageContext.setAttribute("totalErrorCount", totalErrorCount);
            pageContext.setAttribute("totalWarningCount", totalWarningCount);
            String queryURL = null;
            if (session.getAttribute("robotsValidtorURLATE") != null) {
                queryURL = (String) session.getAttribute("robotsValidtorURLATE");
            }
            pageContext.setAttribute("queryURL", queryURL);

        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var messageWrapperDiv, textAreaContent;
            var isImported = true;
            $(document).ready(function () {
                $("#import").bind('click', function () {
                    resultLoginValue = true;
                    isImported = true;
                    $('#toolQueryMessage, #robotQueryMessage').empty();
                    if ($("#domainUrl").val().trim() === "" || $("#domainUrl").val() === null || $("#domainUrl").val().trim() === 0) {
                        $('#toolQueryMessage').html("Please enter your URL.");
                        $('#domainUrl').focus();
                        return false;
                    } else if (!urlValidation($('#domainUrl').val().trim())) {
                        $("#toolQueryMessage").html("Please enter a valid URL.");
                        $('#domainUrl').focus();
                        return false;
                    }
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    $('#robotsContent').css('overflow', 'scroll');
                    var $this = $('#import');
                    $this.button('loading');
                    var form = $("#robotsTxtValidatorTool");
                    form.attr('method', "POST");
                    form.attr('action', "robots-txt-validator-tool.html?getResult=validateUrlCont");
                    form.submit();
                    $('#import, #validateRobotCtnt, #resetRobot').attr('disabled', true);
                    $('#domainUrl').attr('readonly', true);
                });
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
                messageWrapperDiv = document.getElementById("messageWrapper");
                textAreaContent = document.getElementById("robotsContent");
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();
            <c:if test="${(getSuccess.equals('success') || fn:contains(uri, 'backToSuccessPage='))}">
                <c:if test="${(queryURL !=null && !queryURL.equals(''))}">
                $("#editImport").show();
                $("#domainUrl").addClass("gray");
                </c:if>
            </c:if>
                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }

                $('[data-toggle="tooltip"]').tooltip();
                $('#domainUrl').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#import").click();
                        return false;
                    }
                });
                $("#validateRobotCtnt").bind('click', function () {
                    isImported = false;
                     resultLoginValue = true;
                    $('#toolQueryMessage, #robotQueryMessage').empty();
                    if ($("#robotsContent").val().trim() === "" || $("#robotsContent").val() === null || $("#robotsContent").val().trim() === 0) {
                        $('#robotQueryMessage').empty();
                        $('#robotQueryMessage').html("Please enter robots.txt file content");
                        $('#robotsContent').focus();
                        return false;
                    }
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var $this = $('#validateRobotCtnt');
                    $this.button('loading');
                    var form = $("#robotsTxtValidatorTool");
                    form.attr('method', "POST");
                    form.attr('action', "robots-txt-validator-tool.html?getResult=validateTxt");
                    form.submit();
                    $('#import, #validateRobotCtnt, #resetRobot').attr('disabled', true);
                    $('#domainUrl').attr('readonly', true);
                });
                $("#resetRobot").bind('click', function (e) {
                    e.preventDefault();
                    $('#domainUrl').val("");
                    $('#toolQueryMessage').empty();
                    var f = $("#robotsTxtValidatorTool");
                    f.attr('action', "robots-txt-validator-tool.html?clearAll='clearAll'");
                    f.submit();
                });
                $('#editImport, #domainUrl').on('click', function () {
                    $("#toolQueryMessage").html("");
                    $("#editImport").hide();
                    $("#domainUrl").removeClass("gray");
                });
            });
            function loadRobotsTxtData() {
                textAreaContent = document.getElementById("robotsContent");
                messageWrapperDiv = document.getElementById("messageWrapper");
                document.getElementById("robotsContent").wrap = "off";
                valideRoobotTxtContent();
                $('[data-toggle="tooltip"]').tooltip();
                textAreaContent.onscroll = function () {
                    messageWrapperDiv.style.top = -(textAreaContent.scrollTop) + "px";
                    return true;
                };
                textAreaContent.onkeyup = function () {
                    updateLineCount();
                    return true;
                };
            }

            function valideRoobotTxtContent() {
                messageWrapperDiv = document.getElementById("messageWrapper");
                textAreaContent = document.getElementById("robotsContent");
                var messageContent = "";
                var errMsgs = "";
                var warnMsgs = "";
                var isError = false;
                var isWarn = false;
                var imageSpanContent = "";
                var contnetLength = $("#robotsContent").val().split("\n").length;
                messageWrapperDiv.innerHTML = "";
                messageWrapperDiv.style.top = -(textAreaContent.scrollTop) + "px";
                for (var index = 1; index <= contnetLength; index++) {
                    /*Parent for message content*/
                    messageContent = "";
                    messageContent = "<div id=message" + index + ">";
                    imageSpanContent = "<span></span>";
                    errMsgs = "";
                    warnMsgs = "";
                    isError = false;
                    isWarn = false;
            <c:if test = "${validationMap != null && fn:length(validationMap) gt 0}">
                    /*Fetching number of directories*/
                <c:forEach var="directory" items="${validationMap}">
                    <c:set var="validationError" value="true"/>
                    var lineNumber = ${directory.key};
                    if (index === lineNumber) {
                    <c:set var="errStyCount" value="0"/>
                    <c:set var="warStyCount" value="0"/>
                    <c:if test = "${directory.value != null && fn:length(directory.value) gt 0}">
                        <c:forEach items="${directory.value}" var="directoryContent" varStatus="directoryList">
                            <c:if test = "${directoryContent.getErrorType() == 1}">
                        isError = true;
                                <c:set var="totalErrorCount" value="${totalErrorCount+1}"/>
                                <c:set var="errStyCount" value="${errStyCount+1}"/>
                        errMsgs = errMsgs + '<a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" data-selector="true" title="${directoryContent.getErrMessage()}"><i class="fa fa-exclamation-circle fa-1x lxrm-tool-tip" style="color:#ea4335;" aria-hidden="true"></i></a>';
                            </c:if>
                            <c:if test = "${directoryContent.getErrorType() == 2}">
                        isWarn = true;
                                <c:set var="totalWarningCount" value="${totalWarningCount+1}"/>
                                <c:set var="warStyCount" value="${warStyCount+1}"/>
                        warnMsgs = warnMsgs + '<a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left"  data-selector="true" title="${directoryContent.getErrMessage()}"><i class="fa fa-exclamation-triangle fa-1x lxrm-tool-tip" style="color:#FDA100;" aria-hidden="true"></i></a>';
                            </c:if>
                        </c:forEach>
                    </c:if>
                        if (isError && isWarn) {
                            imageSpanContent = "<span>" + errMsgs + "" + warnMsgs + "</span>";
                        } else if (isError) {
                            imageSpanContent = "<span>" + errMsgs + "</span>";
                        } else if (isWarn) {
                            imageSpanContent = "<span>" + warnMsgs + "</span>";
                        }
                    }
                </c:forEach>
            </c:if>
                    messageContent += imageSpanContent + "<span>" + index + ".</span></div>";
                    messageWrapperDiv.innerHTML = messageWrapperDiv.innerHTML + messageContent;
            <c:if test="${validationError eq true || totalErrorCount >= 1 || totalWarningCount >= 1}">
                    $("#validationResult").show();
            </c:if>
                }
            }
            function updateLineCount() {
                messageWrapperDiv = document.getElementById("messageWrapper");
                textAreaContent = document.getElementById("robotsContent");
                var contentLength = $("#robotsContent").val().split("\n").length;
                messageWrapperDiv.innerHTML = "";
                for (var index = 1; index <= contentLength; index++) {
                    messageWrapperDiv.innerHTML = messageWrapperDiv.innerHTML + "<div id=message" + index + " ><span></span><span>" + index + ".</span></div>";
                }
            }
            function getToolRelatedIssues() {
                var url = $("#domainUrl").val();
                console.log("Robots.txt validator url is " + url);
                $.ajax({
                    type: "POST",
                    url: "/robots-txt-validator-tool.html?robotsValidATE='robotsValidATE'&robotsValidURL=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {

                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                if (website !== "") {
                    $('#domainUrl').val(website);
                    $("#import").click();
                    $('#import').attr('disabled', false);
                } else {
                    if (userInput.trim() !== "" || userInput !== null || userInput.length !== 0) {
                        var userToolInputs = JSON.parse(userInput);
                        $('#robotsContent').val(userToolInputs["2"]);
                        $('#validateRobotCtnt').click();
                    }
                }
            }
            function submitPreviousRequest() {
                if (isImported === true)

                {
                    $("#import").click();
                } else {
                    $("#validateRobotCtnt").click();
                }
            }
        </script>
    </head>
    <body onload="loadRobotsTxtData();">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <springForm:form commandName="robotsTxtValidatorTool" method="POST">
            <div class="container">
                <div class="col-xs-12" id="toolInputWrapper">
                    <div class="form-group col-xs-12 col-sm-12 col-md-7 col-lg-7" >
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="domainUrl" cssClass="form-control input-lg srch-term"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span class="lxr-danger text-left error" id="toolQueryMessage">${notWorkingUrl}</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 text-center" style="padding: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="import"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> IMPORTING AND VALIDATING ROBOTS.TXT">
                                IMPORT AND VALIDATE ROBOTS.TXT
                            </button>
                        </div>
                    </div>   
                </div>
                <div class="col-xs-12 text-center">
                    <div class="clearfixdiv hidden-lg"></div>
                    <p class="lxrm-bold" style="font-size: 1.7em;">(OR)</p>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-xs-12 lxrm-tool-heading" >
                    <p class="lxrm-bold" style="font-size: 1.3em;">Paste/Edit your robots.txt and check for errors:</p>
                </div>
                <div class="col-xs-12 table-responsive">
                    <div class="syntaxHighlter">
                        <div id="messageWrapper" style="position: relative;"></div>
                    </div>
                    <div id="contentWrapper">
                        <springForm:textarea placeholder="Paste / Edit your robots.txt and check for errors" rows="15"  path="robotsContent" cssClass="form-control input-lg srch-term"/>
                        <span class="lxr-danger text-left error" id="robotQueryMessage"></span>
                    </div>
                </div>

                <div id="validationResult" class="col-xs-12 text-left" style="margin-top: 1%;" >
                    <span><i class="fa fa-exclamation-circle fa-1x" style="color:#ea4335;font-size: 1.4em;" aria-hidden="true"></i></span>
                    <span style="font-size: 1.3em;">${totalErrorCount} Error</span>
                    <span><i class="fa fa-exclamation-triangle fa-1x" style="color:#FDA100;font-size: 1.4em;" aria-hidden="true"></i></span>
                    <span style="font-size: 1.3em;">${totalWarningCount} Warning</span>
                </div>
            </div>
            <div class="container">
                <div class="col-lg-12" style="padding-top: 1%">
                    <div class="col-md-3 col-lg-4 visible-md visible-lg"></div>
                    <div class="col-xs-12  col-sm-6 col-md-3 col-lg-2 tool-buttons-wrapper">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetRobot">
                                &nbsp;&nbsp;&nbsp;<i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL&nbsp;&nbsp;&nbsp;
                            </button>
                        </div>
                    </div>
                    <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                    <div class="col-xs-12  col-sm-6 col-md-3 col-lg-2 tool-buttons-wrapper">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="validateRobotCtnt" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> VALIDATING ROBOTS.TXT">
                                VALIDATE ROBOTS.TXT
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                </div>
            </div>
        </springForm:form>

        <div class="clearfixdiv"></div>
        <!--   Success Page -->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
