
<%@page import="com.google.api.services.analytics.model.Profile"%>
<%@page import="com.google.api.services.analytics.model.Profiles"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SEO Penalty Checker | LXRMarketplace</title>
        <meta name="description" content="Try our SEO Penalty Checker tool now! Enter your URL into this">
        <meta name="keywords" content="SEO Penalty Checker Tool">
        <%--<%@ include file="/WEB-INF/views/commonImports.jsp"%>--%>
        <%@ include file="/WEB-INF/views/commonImportsWithDatePicker.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/penaltyChecker.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <%
            int toolDBId = 36;
            List<Profiles> analyticsProfilesList = (List<Profiles>) session.getAttribute("analyticsProfilesList");
            List<String> analyticsAccountNames = (List<String>) session.getAttribute("analyticsAccountNames");
            HashMap<String, String> analyticsWebPropertiesList = (HashMap<String, String>) session.getAttribute("analyticsWebPropertiesList");

            Object[] dailyPageReachData = (Object[]) session.getAttribute("penaltyDailyReachInfo");
            boolean isAnalytics = false;
            if (session.getAttribute("isAnalytics") != null) {
                isAnalytics = (Boolean) session.getAttribute("isAnalytics");
            }

            pageContext.setAttribute("analyticsProfilesList", analyticsProfilesList);
            pageContext.setAttribute("analyticsAccountNames", analyticsAccountNames);
            pageContext.setAttribute("webPropertiesList", analyticsWebPropertiesList);
            pageContext.setAttribute("isAnalytics", isAnalytics);
            pageContext.setAttribute("dailyPageReachData", dailyPageReachData);
        %>
        <script type="text/javascript">
//            google.charts.load('current', {'packages': ['corechart']});
            google.load("visualization", "1", {packages: ["corechart"]});
//            google.setOnLoadCallback(drawChart);
        </script>

        <script type="text/javascript">
            var hideUpdate = [];
            var mainData;
            var startDateValue = new Date();
            var endDateValue = new Date();
            var dataSeries = {};

            dataSeries[0] = {type: 'line', enableInteractivity: true, tooltip: true};
            dataSeries[1] = {type: 'line', enableInteractivity: true, tooltip: true};
            dataSeries[2] = {enableInteractivity: true, tooltip: false};
            dataSeries[3] = {enableInteractivity: true, tooltip: false};
            dataSeries[4] = {enableInteractivity: true, tooltip: false};
            dataSeries[5] = {enableInteractivity: true, tooltip: false};
            dataSeries[6] = {enableInteractivity: true, tooltip: false};
            dataSeries[7] = {enableInteractivity: true, tooltip: false};
            dataSeries[8] = {enableInteractivity: true, tooltip: false};
            dataSeries[9] = {enableInteractivity: true, tooltip: false};
            dataSeries[10] = {enableInteractivity: true, tooltip: false};
            dataSeries[11] = {enableInteractivity: true, tooltip: false};
            dataSeries[12] = {enableInteractivity: true, tooltip: false};
            dataSeries[13] = {enableInteractivity: true, tooltip: false};
            dataSeries[14] = {enableInteractivity: true, tooltip: false};
            dataSeries[15] = {enableInteractivity: true, tooltip: false};
            dataSeries[16] = {enableInteractivity: true, tooltip: false};
            dataSeries[17] = {enableInteractivity: true, tooltip: false};
            dataSeries[18] = {enableInteractivity: true, tooltip: false};
            dataSeries[19] = {enableInteractivity: true, tooltip: false};
            dataSeries[20] = {enableInteractivity: true, tooltip: false};
            dataSeries[21] = {enableInteractivity: true, tooltip: false};
            dataSeries[22] = {enableInteractivity: true, tooltip: false};
            dataSeries[23] = {enableInteractivity: true, tooltip: false};
            dataSeries[24] = {enableInteractivity: true, tooltip: false};
            dataSeries[25] = {enableInteractivity: true, tooltip: false};
            var options = {
                title: '',
                legend: {
                    position: 'bottom',
                    textStyle: {fontSize: 10}
                },
                tooltip: {
                    textStyle: {
                        fontSize: 12
                    }
                },
                pointSize: 4,
                titleTextStyle: {fontSize: 14},
                colors: ['#497B88', '#a52714', '#039dec', '#3366cc', '#097138', '#ff6060', '#3da7ff', '#f49ff4', '#ff9900'],
                lineWidth: 2.5,
                width: '1200',
                height: '400',
                backgroundColor: '#FFF',
                chartArea: {
                    width: '90%'
                },
                animation: {
                    duration: 1000,
                    easing: 'linear',
                    startup: true
                },
                hAxis: {
                    gridlines: {color: '#E8E8E8', width: "5%"},
                    viewWindow: {min: startDateValue, max: endDateValue},
                    textStyle: {fontSize: 14}
                },
                vAxis: {
                    gridlines: {color: '#E8E8E8'},
                    textStyle: {fontSize: 14},
                    "minValue": 0
                },
                bar: {
                    groupWidth: "95%"
                },
                explorer: {
                    actions: ['dragToZoom', 'rightClickToReset'],
                    axis: 'horizontal',
                    keepInBounds: true,
                    maxZoomIn: 4.0
                },
                seriesType: 'bars',
                series: dataSeries
            };
            var reportDownload = 0;
            var downloadReportType = "";
            var isAnalyticsRequest = true;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
//                toolLimitExceeded();
                // onload after result or get back from payment page to tool page
            <c:if test="${fn:contains(uri, 'backToSuccessPage=')}">
                if (profileDataInfo !== "" && ${isAnalytics}) {
                    $("#non-analytics-section").hide();
                    $("#radioNo").removeClass("disabled");
                    $("#analytics-section").show();
                    $('.profile-info').show();
                    $('input:radio[name=isAnalyticsAccount]')[0].checked = true;
                    $('#penaltyCheckerFilter').show();
                } else {
                    $("#editImport").show();
                    $("#domain").addClass("gray");
                    $("#non-analytics-section").show();
                    $("#radioNo").addClass("disabled");
                    $("#radioYes").removeClass("active");
                    $("#analytics-section").hide();
                    $('.profile-info').hide();
                    $('input:radio[name=isAnalyticsAccount]')[1].checked = true;
                    $('#penaltyCheckerFilter').hide();
                }
                <c:if test="${dailyPageReachData[4] != null && dailyPageReachData[4] != ''}">

                $("#editImport").show();
                $("#domain").addClass("gray");
                $("#domain").val("${dailyPageReachData[1]}");
//                $("#profile-loading").show();
                loadingPage(true);
                $.ajax({
                    type: "POST",
                    url: "/penalty-checker-tool.html?backToSuccessPage=backToSuccessPage",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        $(".chart-actions").show();
                        mainData = data;
                        bulidResultChartAndTable(true);
                        $('html, body').animate({scrollTop: $('#result-section').offset().top}, 'slow');
                    },
                    error: function () {
                        $("#no-analytics").show();
                        $("#result-section").hide();
                        $("#no-analytics").html("<p class='text-center'>Unable to fetch the data for your site please try again.</p>");
                        $(".chart-actions").hide();
                        $("#websiteTrafficChart").html("<p class='text-center'>We are unable to process your request. Please try again later.</p>");
                    }, complete: function (jqXHR, textStatus) {
//                        $("#profile-loading").hide();
                        loadingPage(false);
                    }
                });
                </c:if>
            </c:if>
                // onload after result or get back from payment page to tool page
//                if (parentUrl.indexOf("backToSuccessPage=") !== -1) {
//                    $("#editImport").show();
//                    $("#domain").addClass("gray");
//                }
                $('#connectAnalytics').on('click', function () {
                    isAnalyticsRequest = true;
                    resultLoginValue = true;
                    /*   Login POPUP on tool limit exceeded */
                    if (!toolLimitExceeded()) {
                        return false;
                    } else {
                        window.open('${authorizationUrl}', '_self');
                    }
                });
                $('#getResult').on('click', function () {
                    isAnalyticsRequest = false;
                    resultLoginValue = true;
                    /*   Login POPUP on tool limit exceeded */
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if ($('#domain').val().trim() === "" || $('#domain').val() === null || $('#domain').val() === 0) {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $('#domain').focus();
                        return false;
                    }
                    $("#notWorkingUrl").html("");
                    var domain = $('#domain').val().trim();
                    $("#domain").css("disable", true);
                    $("#result-section").hide();
                    var $this = $(this);
                    $this.button('loading');
                    $.ajax({
                        type: "GET",
                        url: "/penalty-checker-tool.html?fetchData=traffic-history",
                        processData: true,
                        data: {"domain": domain},
                        dataType: "json",
                        success: function (data) {
                            $(".chart-actions").show();
                            $("#domain").css("disable", false);
                            $("#editImport").show();
                            $("#domain").addClass("gray");
                            $("#google-updates-table-div").show();
                            $("#review-of-table").show();
                            $("#penaltyCheckerFilter").hide();
                            if (data[0].indexOf("not working") !== -1 || data[0].indexOf("not exist") !== -1) {
                                $("#notWorkingUrl").html(data[0]);
                            } else if (data[0].length > 0) {
                                mainData = data;
                                bulidResultChartAndTable(true);
                                $('html, body').animate({scrollTop: $('#result-section').offset().top}, 'slow');
                            } else {
                                $("#no-analytics").show();
                                $("#result-section").hide();
                                $("#no-analytics").html("<p class='text-center'>Unable to fetch the data for your site please try again.</p>");
                            }
                        }, error: function (jqxhr, textStatus, error) {
                            $("#result-section").hide();
                            if (textStatus.indexOf('error') !== -1) {
                                toolLimitExceededForAjax();
                            } else {
                                $("#no-analytics").show();
                                $("#no-analytics").html("<p class='text-center'>Unable to fetch the data for your site please try again.</p>");
                            }
//                            $("#google-updates-table-div").hide();
//                            $("#review-of-table").hide();
//                            $(".chart-actions").hide();
//                            $("#websiteTrafficChart").html("<p class='text-center'>We are unable to process your request. Please try again later.</p>");
                        }, complete: function (jqXHR, textStatus) {
                            $this.button('reset');
                        }
                    });
                });
                /***   Google Anlytics Profile data starts   **/
                var profileDataInfo = getProfiledataFromSession();

                //  after getting the analytics data 
                if (parentUrl.indexOf("gAnalytics") !== -1) {
                    $('#connectAnalytics').show();
                    $('.form-buttons').hide();
                    if (profileDataInfo !== "") {
                        $('.profile-info').show();
                    } else {
                        $('.profile-info').hide();
                        $('#no-analytics').html("<p class='text-center lxr-danger'>User doesn't have any Google Analytics account</p>");
                    }
                }
                $("#profileData").html(profileDataInfo);
                //   For downloading the google chart
                $("#deviceDownload").on("click", function () {
                    var uri = document.getElementById("chartImg").src;
                    var d = new Date().toISOString().slice(0, 19).replace(/-/g, "");
                    filename = "seo-penalty-checker-" + d + ".png";
                    downloadImage(uri, filename);
                });
                // TODO
                /***   Google Anlytics Profile data end   **/


                // For zoom, reset and download options
                $('#previousButton').click(function () {
                    var pStartDate = new Date(Date.parse(mainData[0][0].date));
                    var pEndDateValue = new Date(Date.parse(mainData[0][mainData[0].length - 1].date));
                    if (startDateValue > pStartDate) {
                        startDateValue.setMonth(startDateValue.getMonth() - 2);
                        endDateValue.setMonth(endDateValue.getMonth() - 2);
                        if (startDateValue < pStartDate) {
                            startDateValue = pStartDate;
                        } else {
                            $(".chart-actions .fa-chevron-left").removeClass("lxr-diable-icons");
                            $(".chart-actions .fa-chevron-right").addClass("lxr-diable-icons");
                        }
                        options.hAxis.viewWindow.min = startDateValue;
                        options.hAxis.viewWindow.max = endDateValue;
                        drawChart();
                    }
//                    if (startDateValue > pStartDate) {
//                        $(".chart-actions .fa-chevron-right").removeClass("lxr-diable-icons");
//                    } else {
//                        $(".chart-actions .fa-chevron-left").addClass("lxr-diable-icons");
//                        $(".chart-actions .fa-chevron-right").removeClass("lxr-diable-icons");
//                        var timeDiff = Math.abs(endDateValue.getTime() - startDateValue.getTime());
//                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//                        if (diffDays < 130) {
//                            $(".chart-actions .fa-search-plus").addClass("lxr-diable-icons");
//                        } else {
//                            $(".chart-actions .fa-search-plus").removeClass("lxr-diable-icons");
//                        }
//                        timeDiff = Math.abs(pEndDateValue.getTime() - endDateValue.getTime());
//                        diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//                        if (endDateValue >= pEndDateValue) {
//                            $(".chart-actions .fa-search-minus").addClass("lxr-diable-icons");
//                        } else {
//                            $(".chart-actions .fa-search-minus").removeClass("lxr-diable-icons");
//                        }
//                    }
                });
                $('#nextButton').click(function () {
//                    var pStartDate = new Date(Date.parse(mainData[0][0].date));
                    var pEndDateValue = new Date(Date.parse(mainData[0][mainData[0].length - 1].date));
//                    console.log("nextButton");
//                    console.log(pEndDateValue);
                    if (endDateValue < pEndDateValue) {
                        startDateValue.setMonth(startDateValue.getMonth() + 2);
                        endDateValue.setMonth(endDateValue.getMonth() + 2);
                        if (endDateValue > pEndDateValue) {
                            endDateValue = pEndDateValue;
                        }
                        options.hAxis.viewWindow.min = startDateValue;
                        options.hAxis.viewWindow.max = endDateValue;
                        drawChart();
                    }
//                    if (endDateValue < pEndDateValue) {
//                        $(".chart-actions .fa-chevron-left").removeClass("lxr-diable-icons");
//                    } else {
//                        $(".chart-actions .fa-chevron-right").addClass("lxr-diable-icons");
//                        var timeDiff = Math.abs(endDateValue.getTime() - startDateValue.getTime());
//                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//                        if (diffDays < 130) {
//                            $(".chart-actions .fa-search-plus").addClass("lxr-diable-icons");
//                        } else {
//                            $(".chart-actions .fa-search-plus").removeClass("lxr-diable-icons");
//                        }
//                        timeDiff = Math.abs(pEndDateValue.getTime() - endDateValue.getTime());
//                        diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//                        if (endDateValue >= pEndDateValue) {
//                            $(".chart-actions .fa-search-minus").addClass("lxr-diable-icons");
//                        } else {
//                            $(".chart-actions .fa-search-minus").removeClass("lxr-diable-icons");
//                        }
//                    }
                });
                $('#zoomInButton').click(function () {
                    var timeDiff = Math.abs(endDateValue.getTime() - startDateValue.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    if (diffDays >= 130) {
                        endDateValue.setMonth(endDateValue.getMonth() - 5);
//                        console.log("zoomInButton");
//                        console.log(endDateValue);
                        options.hAxis.viewWindow.min = startDateValue;
                        options.hAxis.viewWindow.max = endDateValue;
                        drawChart();
                    }
                    timeDiff = Math.abs(endDateValue.getTime() - startDateValue.getTime());
                    diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    if (diffDays >= 130) {
//                        alert("if");
//                        $(".chart-actions .fa-chevron-right, .chart-actions .fa-refresh, .chart-actions .fa-search-minus").removeClass("lxr-diable-icons");
                    } else {
//                        alert("Else");
//                        $(".chart-actions .fa-search-plus").addClass("lxr-diable-icons");
//                        $(".chart-actions .fa-search-minus").addClass("lxr-diable-icons");
                    }
                });
                $('#zoomOutButton').click(function () {
//                    var pStartDate = new Date(Date.parse(mainData[0][0].date));
                    var pEndDateValue = new Date(Date.parse(mainData[0][mainData[0].length - 1].date));
                    var timeDiff = Math.abs(pEndDateValue.getTime() - endDateValue.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    if (endDateValue < pEndDateValue) {
                        if (diffDays < 150) {
                            endDateValue = pEndDateValue;
                        } else {
                            endDateValue.setMonth(endDateValue.getMonth() + 5);
                        }
                        options.hAxis.viewWindow.min = startDateValue;
                        options.hAxis.viewWindow.max = endDateValue;
                        drawChart();
                    }
                    if (endDateValue < pEndDateValue) {
//                        $(".chart-actions .fa-chevron-right, .chart-actions .fa-refresh, .chart-actions .fa-search-plus").removeClass("lxr-diable-icons");
                    } else {
//                        $(".chart-actions .fa-search-minus").addClass("lxr-diable-icons");
                    }
                });
                $('#resetButton').click(function () {
                    var pStartDate = new Date(Date.parse(mainData[0][0].date));
                    var pEndDateValue = new Date(Date.parse(mainData[0][mainData[0].length - 1].date));
                    if (startDateValue < pStartDate || startDateValue > pStartDate
                            || endDateValue > pEndDateValue || endDateValue < pEndDateValue) {
                        startDateValue = pStartDate;
                        endDateValue = pEndDateValue;
                        options.hAxis.viewWindow.min = startDateValue;
                        options.hAxis.viewWindow.max = endDateValue;
                        drawChart();
//                        $(".chart-actions .fa-chevron-right, .chart-actions .fa-chevron-left, .chart-actions .fa-refresh, .chart-actions .fa-search-minus").addClass("lxr-diable-icons");
//                        $(".chart-actions .fa-search-plus").removeClass("lxr-diable-icons");
                    }
                });
                $('input[type=radio][name=isAnalyticsAccount]').change(function () {
                    if (this.value === "1") {
                        $("#non-analytics-section").hide();
                        $("#radioNo").removeClass("disabled");
                        $("#radioNo").removeClass("active");
                        $("#radioYes").addClass("active");
                        $("#analytics-section").show();
                        if ($("#profileData").html() !== undefined || $("profileData").html() !== "") {
                            $(".profile-info").show();
                        }
                    } else if (this.value === "0") {
                        $("#non-analytics-section").show();
                        $("#radioNo").addClass("disabled");
                        $("#radioNo").addClass("active");
                        $("#radioYes").removeClass("active");
                        $("#analytics-section").hide();
                        $(".profile-info").hide();
                        var content = $("#notWorkingUrl").text();
                        if ((content !== null && content !== "") && (content.indexOf("reviewed") !== -1 || content.indexOf("enter your URL") !== -1)) {
                            $("#notWorkingUrl").empty();
                            $('#domain').val("");
                            $('#domain').removeClass('gray');
                            $("#editImport").hide();
                        }
                    }
                    $("#no-analytics").hide();
                });
                $('#domain').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                $('#editImport, #domain').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#domain").removeClass("gray");
                });
                var isAnalyticsAccount = $('input[name=isAnalyticsAccount]:checked').val();
                if (profileDataInfo !== "" && isAnalyticsAccount === "1") {
                    $('.profile-info').show();
                }
//                $("#websiteTrafficChart").hover(function () {
//                    setGraphRectWidth();
//                });
            });
            function bulidResultChartAndTable(isOnload) {
                var data = mainData;
                if (data[0] !== null && data[0].indexOf("working") !== -1) {
                    $("#notWorking").html(data[0]);
                    $("#notWorking").show();
                } else {
                    $("#result-section").show();
                    $("#website").html(data[1]);
                    $("#domain").val(data[1]);
                    $("#sDate").html(data[2]);
                    $("#eDate").html(data[3]);
                    $("#startDate").val(data[9].startDate);
                    $("#endDate").val(data[9].endDate);
                    if (data[9].device !== null && data[9].startDate !== "") {
                        var device = data[9].device;
                        $("#deviceGroup label").addClass("not-active");
                        if (device === "mobile") {
                            $("#deviceGroup label:nth-child(" + 1 + ")").removeClass("not-active");
                            $('input:radio[name=device]')[0].checked = true;
                        } else if (device === "tablet") {
                            $("#deviceGroup label:nth-child(" + 2 + ")").removeClass("not-active");
                            $('input:radio[name=device]')[1].checked = true;
                        } else if (device === "desktop") {
                            $("#deviceGroup label:nth-child(" + 3 + ")").removeClass("not-active");
                            $('input:radio[name=device]')[2].checked = true;
                        } else {
                            $("#deviceGroup label:nth-child(" + 4 + ")").removeClass("not-active");
                            $('input:radio[name=device]')[3].checked = true;
                        }
                    }
                    if (data[9].analyticsFetchType !== null && data[9].analyticsFetchType !== "") {
                        var analyticsFetchType = data[9].analyticsFetchType;
                        $("#dayGroup label").addClass("not-active");
                        if (analyticsFetchType === "date") {
                            $("#dayGroup label:nth-child(" + 1 + ")").removeClass("not-active");
                            $('input:radio[name=analyticsFetchType]')[0].checked = true;
                        } else {
                            $("#dayGroup label:nth-child(" + 2 + ")").removeClass("not-active");
                            $('input:radio[name=analyticsFetchType]')[1].checked = true;
                        }
                    }
                    $("#profileId").val(data[9].profileId);
                    $("#domainFilter").val(data[9].domain);
                    $("#organicTraffic").html(data[4]);
                    startDateValue = new Date(Date.parse(data[0][0].date));
                    endDateValue = new Date(Date.parse(data[0][data[0].length - 1].date));
                    options.hAxis.viewWindow.min = startDateValue;
                    options.hAxis.viewWindow.max = endDateValue;
                    if (isOnload) {
                        drawChart('websiteTrafficChart', data);
                    } else {
                        google.setOnLoadCallback(drawChart);
                    }
                    if (data[1] !== null && data[1] !== "") {
                        var googleUpdateData = data[5];
                        var resultTable = "<thead><tr><th id='updateDateCol'>Updated Date</th><th id='trafficCol'>Organic Traffic</th>"
                                + "<th id='updateTypeCol'>Update Type</th><th id='updateCol'>Update</th></tr></thead><tbody>";
                        if (googleUpdateData.length > 0) {
                            for (var i = 0; i < googleUpdateData.length; i++) {
//                                var updateDate = dateFormat(new Date(googleUpdateData[i].updatedDate));
//                                resultTable += "<tr><td>" + updateDate.getDate() + "-" + (updateDate.getMonth() + 1) + "-" + updateDate.getFullYear()
                                resultTable += "<tr><td style='width:10%;'>" + googleUpdateData[i].updatedDate + "</td>"
                                        + "<td style='text-align:right;padding-right: 2%;width:10%;'>" + googleUpdateData[i].organicTraffic + "</td>"
                                        + "<td style='width:10%;'><sapn onclick='hideThisUpdate(" + googleUpdateData[i].updateId + ");'>" + googleUpdateData[i].updateType + "</sapn></td>"
                                        + "<td style='width:70%;'>" + googleUpdateData[i].description + "</td></tr>";
                            }
                        } else {
                            resultTable += "<tr><td style='text-align:center;' colspan='4'>No google update falls on selected date range</td></tr>";
                        }
                        resultTable += "</tbody>";
                        $("#resultDivTable").html(resultTable);
                    }
                    getAskExpertPopup(true);
                }
            }
            function dateFormat(date) {
                var today = date;
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return new Date(yyyy + "/" + mm + "/" + dd + " 00:00:00");
            }
            function getWeekNumber(d) {
                // Copy date so don't modify original
                d = new Date(+d);
                d.setHours(0, 0, 0, 0);
                // Set to nearest Thursday: current date + 4 - current day number
                // Make Sunday's day number 7
                d.setDate(d.getDate() + 4 - (d.getDay() || 7));
                // Get first day of year
                var yearStart = new Date(d.getFullYear(), 0, 1);
                // Calculate full weeks to nearest Thursday
                var weekNumber = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
                // Return array of year and week number
                return weekNumber;
            }
            function drawChart() {
                var data = new google.visualization.DataTable();
                var successData = mainData;
                var pageViewAndReachData = successData[0];
                var newUsersData = successData[8];
                var googleUpdateData = successData[5];
                var maxValue = successData[7];
                var isAnalytics = true;
                if (newUsersData === null || newUsersData === "" || newUsersData.length <= 0) {
                    isAnalytics = false;
                    dataSeries[1] = {type: 'bar', enableInteractivity: true, tooltip: false};
                    options.series = dataSeries;
                } else {
                    dataSeries[1] = {type: 'line', enableInteractivity: true, tooltip: true};
                    options.series = dataSeries;
                }
                if (pageViewAndReachData.length === 0) {
                    $("#websiteTrafficChart").empty();
                    $('<p class="no-chart-data">No data available.</p>').appendTo("#websiteTrafficChart");
                    $("#deviceDownload").hide();
                } else {
                    data.addColumn('date', 'Month');
                    data.addColumn('number', 'Visits');
                    if (isAnalytics) {
                        data.addColumn('number', 'New Users');
                    }
                    var updatesList = successData[6];
                    if (updatesList.length > 0) {
                        for (var i = 0; i < updatesList.length; i++) {
                            data.addColumn('number', updatesList[i]);
                            data.addColumn({type: 'string', role: 'tooltip', p: {"html": true}});
                        }
                    }

                    var dataObjArray = [];
                    if (pageViewAndReachData !== null) {
                        var googleDataIndex = 0;
                        for (var i = 0; i < pageViewAndReachData.length; i++) {
                            var dataSetArray = [];
                            var alexaTrafficDate = "";
                            var googleUpdatedDate = "";
                            alexaTrafficDate = dateFormat(new Date(Date.parse(pageViewAndReachData[i].date)));
                            if (googleDataIndex < googleUpdateData.length) {
                                googleUpdatedDate = dateFormat(new Date(Date.parse(googleUpdateData[googleDataIndex].updatedDate)));
                            }
                            if (new Date(alexaTrafficDate).getTime() > new Date(googleUpdatedDate).getTime()) {
                                dataSetArray.push(new Date(Date.parse(pageViewAndReachData[i].date)));
                                dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                                if (isAnalytics) {
                                    dataSetArray.push(parseInt(newUsersData[i].newUsers));
                                }
                                i--;
                            } else if (new Date(alexaTrafficDate).getTime() === new Date(googleUpdatedDate).getTime()) {
                                dataSetArray.push(new Date(Date.parse(googleUpdateData[googleDataIndex].updatedDate)));
                                dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                                if (isAnalytics) {
                                    dataSetArray.push(parseInt(newUsersData[i].newUsers));
                                }
                            } else {
                                dataSetArray.push(new Date(Date.parse(pageViewAndReachData[i].date)));
                                dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                                if (isAnalytics) {
                                    dataSetArray.push(parseInt(newUsersData[i].newUsers));
                                }
                            }
                            if ((googleDataIndex < googleUpdateData.length) && (googleUpdateData.length > 0)
                                    && (new Date(alexaTrafficDate).getTime() >= new Date(googleUpdatedDate).getTime()))
                            {
                                for (var j = 0; j < updatesList.length; j++) {
                                    if (updatesList[j] === googleUpdateData[googleDataIndex].updateType) {
                                        dataSetArray.push(maxValue);
                                        dataSetArray.push(googleUpdateData[googleDataIndex].toolTip);
                                    } else {
                                        dataSetArray.push(0);
                                        dataSetArray.push("");
                                    }
                                }
                                googleDataIndex++;
                            } else {
                                for (var j = 0; j < updatesList.length; j++) {
                                    dataSetArray.push(0);
                                    dataSetArray.push("");
                                }
                            }
                            dataObjArray.push(dataSetArray);
                        }
                        data.addRows(dataObjArray);
                    }

//                    var chart = new google.visualization.ComboChart(document.getElementById("websiteTrafficChart"));
                    var chart = new google.visualization.ComboChart(document.getElementById('websiteTrafficChart'));
//                    chart.draw(data, options);
                    var deviceDownload = document.getElementById('deviceDownload');
                    // Disabling the button while the chart is drawing.
                    $("#previousButton").disabled = true;
                    $("#nextButton").disabled = true;
                    $("#zoomButton").disabled = true;
                    // Wait for the chart to finish drawing before calling the getImageURI() method.
                    google.visualization.events.addListener(chart, 'ready', function () {
                        deviceDownload.innerHTML = '<img style="display:none" id="chartImg" src="' + chart.getImageURI() + '">';
                        $("#previousButton").disabled = options.hAxis.viewWindow.min <= startDateValue;
                        $("#nextButton").disabled = options.hAxis.viewWindow.max >= endDateValue;
                        $("#zoomButton").disabled = false;
                    });

                    google.visualization.events.addListener(chart, 'select', selectHandler, showHideSeries);
                    function showHideSeries() {
                        var sel = chart.getChart().getSelection();
                        // if selection length is 0, we deselected an element
                        if (sel.length > 0) {
                            // if row is undefined, we clicked on the legend
                            if (sel[0].row == null) {
                                var col = sel[0].column;
                                if (typeof (columns[col]) == 'number') {
                                    var src = columns[col];

                                    // hide the data series
                                    columns[col] = {
                                        label: data.getColumnLabel(src),
                                        type: data.getColumnType(src),
                                        sourceColumn: src,
                                        calc: function () {
                                            return null;
                                        }
                                    };

                                    // grey out the legend entry
                                    series[columnsMap[src]].color = '#CCCCCC';
                                } else {
                                    var src = columns[col].sourceColumn;
                                    // show the data series
                                    columns[col] = src;
                                    series[columnsMap[src]].color = null;
                                }
                                var view = chart.getView() || {};
                                view.columns = columns;
                                chart.setView(view);
                                chart.draw();
                            }
                        }
                    }
                    function selectHandler() {
                        var selection = chart.getSelection();
                        var popupText = "";
                        if (selection[0] !== undefined && selection[0].row !== undefined && selection[0].row !== null) {
                            for (var i = 0; i < selection.length; i++) {
                                var item = selection[i];
                                var date = new Date(data.getValue(selection[0].row, 0));
                                var weekNumber = getWeekNumber(date);
                                // if column is not line chart then will show popup for update information
                                if (!(item.column === 1 || (isAnalytics && item.column === 2))) {
                                    for (var k = 0; k < googleUpdateData.length; k++) {
                                        var updateDate = new Date(Date.parse(googleUpdateData[k].updatedDate));
                                        var updateWeek = getWeekNumber(updateDate);
                                        if (updateWeek === weekNumber && date.getFullYear() === new Date(updateDate).getFullYear()) {
                                            popupText += "<p><span class='lxrm-bold'>Google Update</span>: " + googleUpdateData[k].updateType + "</p>";
                                            popupText += "<p><span class='lxrm-bold'>Updated Date</span>: " + googleUpdateData[k].updatedDate + "</p>";
                                            popupText += "<p><span class='lxrm-bold'>Description</span>: " + googleUpdateData[k].description + "</p>";
                                        }
                                    }
                                    if (popupText === "") {
                                        popupText = data.getValue(selection[0].row, 3);
                                    }
                                }
                            }
                            if (popupText !== "") {
                                $('#tooltip-content').html(popupText);
                                $("#myModal").modal('show');
                            }
                        }
                    }
                    google.visualization.events.addListener(chart, 'onmouseover', onMouseHoverOnBarChart);
                    google.visualization.events.addListener(chart, 'onmouseout', OnMouseOutOnBarChart);
                    chart.draw(data, options);
                    function onMouseHoverOnBarChart() {
                        $('#websiteTrafficChart').css('cursor', 'pointer');
                        setGraphRectWidth();
                    }
                    function OnMouseOutOnBarChart() {
                        $('#websiteTrafficChart').css('cursor', 'default');
                        setGraphRectWidth();
                    }
                    setTimeout(function () {
                        setGraphRectWidth();
                    }, 2000);
                }
            }
            function setGraphRectWidth() {
                $("#websiteTrafficChart svg>g>g>g:nth-child(2)>rect").attr("width", "4");
                $("#websiteTrafficChart svg>g>g>g:nth-child(2)>rect").attr("stroke-width", "8");
            }
            //  To get the profile data from session
            function getProfiledataFromSession() {
                var profileDataInfo = "";
            <c:choose>
                <c:when test="${analyticsProfilesList != null && fn:length(analyticsProfilesList) gt 0}">

                profileDataInfo = '<div class="col-xs-12 col-sm-6 col-md-4 profile-sub-div" style="max-height: 500px !important;overflow: auto;">';
                profileDataInfo += '<h4 class="lxrm-bold">Select Analytics Account: </h4>';

                    <c:forEach items="${analyticsProfilesList}" var="profiles" varStatus="indexValue">
                        <c:set var="accountName" value="${analyticsAccountNames[indexValue.index]}"></c:set>
                profileDataInfo += '<div class="panel panel-default">';
                profileDataInfo += '<div class="panel-heading"><h4 class="panel-title">';
                profileDataInfo += "<a class='accordion-toggle' aria-expanded='false' data-toggle='collapse' data-parent='#accordion' href='#collapseParent${indexValue.index}'><span class='ficon pull-right'></span>${accountName}</a>";
                profileDataInfo += '</h4></div>';
                profileDataInfo += '<div id="collapseParent${indexValue.index}" class="panel-collapse collapse">';
                profileDataInfo += '<div class="panel-body">';
                        <c:forEach var="webProperty" items="${webPropertiesList}">
                var temp = '';
                            <c:forEach items="${profiles.items}" var="profile">
                                <c:if test = "${webProperty.key == profile.webPropertyId}">
                var profileName = "${profile.name}";
                temp += "<p><span onclick='getProfileData(${profile.id})' style='cursor:pointer;color:#2f80ed;'>" + profileName + "</span></p>";
                                </c:if>
                            </c:forEach>
                if (temp !== '') {
                    var webPropertyName = "${webProperty.value}";
                    profileDataInfo += '<div class="panel panel-default">';
                    profileDataInfo += '<div class="panel-heading"><h4 class="panel-title">';
                    profileDataInfo += "<a class='accordion-toggle' aria-expanded='false' data-toggle='collapse' data-parent='#accordion' href='#collapse${webProperty.key}'><span class='ficon pull-right'></span>" + webPropertyName + "</a>";
                    profileDataInfo += '</h4></div>';
                    profileDataInfo += '<div id="collapse${webProperty.key}" class="panel-collapse collapse">';
                    profileDataInfo += '<div class="panel-body">';
                    profileDataInfo += '<div class="">' + temp + '</div>';
                    profileDataInfo += '</div></div></div>';
                    temp = "";
                }
                        </c:forEach>
                profileDataInfo += '</div></div>';
                    </c:forEach>
                profileDataInfo += '</div>';
                profileDataInfo += '</div>';

                profileDataInfo += '<div id="profile-loading" class="col-xs-12 text-center" style="padding-top:2%;font-size:18px;display:none;"><span><i class="fa fa-refresh fa-spin fa-fw"></i> ANALYZING..</span></div>';

                </c:when>
                <c:otherwise>
                profileDataInfo = "";
                </c:otherwise>
            </c:choose>

                return profileDataInfo;
            }
            function getProfileDataOld(profileId) {
                $("#profileId").val(profileId);
                $("#no-analytics").hide();
//                toolLimitExceeded();
                $("#profile-loading").show();
                $.ajax({
                    type: "POST",
                    url: "/penalty-checker-tool.html",
                    processData: true,
                    data: $("#penaltyCheckerFilter").serialize(),
                    dataType: "json",
                    success: function (data) {
                        if (data[0].length > 0) {
                            mainData = data;
                            bulidResultChartAndTable(true);
                            $("#profile-loading").hide();
                            $('html, body').animate({scrollTop: $('#result-section').offset().top}, 'slow');
                        } else {
                            $("#resultDiv").hide();
                            $("#no-analytics").show();
                            $("#no-analytics").html("<p class='text-center'>Selected profile doesn't have analytics data.</p>");
                        }
                    }, error: function (jqxhr, textStatus, error) {
                        $("#result-section").hide();
                        if (textStatus.indexOf('error') !== -1) {
                            toolLimitExceededForAjax();
                        } else {
                            $("#no-analytics").show();
                            $("#no-analytics").html("<p class='text-center'>Unable to fetch the data for your site please try again.</p>");
                        }
//                            $("#google-updates-table-div").hide();
//                            $("#review-of-table").hide();
//                            $(".chart-actions").hide();
//                            $("#websiteTrafficChart").html("<p class='text-center'>We are unable to process your request. Please try again later.</p>");
                    },
                    complete: function (xhr, textstatus) {
                        $("#profile-loading").hide();
                    }
                });
            }
            function getProfileData(profileId) {
                $("#profileId").val(profileId);
                $("#no-analytics").hide();
                toolLimitExceeded();
//                $("#profile-loading").show();
                if (toolLimitExceeded()) {
                    loadingPage(true);
                    $.ajax({
                        type: "POST",
                        url: "/penalty-checker-tool.html",
                        processData: true,
                        data: $("#penaltyCheckerFilter").serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data[0].length > 0) {
                                mainData = data;
                                bulidResultChartAndTable(true);
//                            var lablesAndDataSet = prepareDataSets(mainData[0], mainData[8], mainData[5], mainData[7]);
//                            drawChartJs(lablesAndDataSet);
//                            $("#profile-loading").hide();
                                $("#penaltyCheckerFilter").show();
                                $('html, body').animate({scrollTop: $('#result-section').offset().top}, 'slow');
                            } else {
                                $("#resultDiv").hide();
                                $("#no-analytics").show();
                                $("#no-analytics").html("<p class='text-center'>Selected profile doesn't have analytics data.</p>");
                            }
                        }, error: function (jqxhr, textStatus, error) {
                            $("#result-section").hide();
                            if (textStatus.indexOf('error') !== -1) {
                                toolLimitExceededForAjax();
                            } else {
                                $("#no-analytics").show();
                                $("#no-analytics").html("<p class='text-center'>Unable to fetch the data for your site please try again.</p>");
                            }
//                            $("#google-updates-table-div").hide();
//                            $("#review-of-table").hide();
//                            $(".chart-actions").hide();
//                            $("#websiteTrafficChart").html("<p class='text-center'>We are unable to process your request. Please try again later.</p>");
                        },
                        complete: function (xhr, textstatus) {
                            loadingPage(false);
//                        $("body").removeAttr("overflow-y");
                        }
                    });
                }
            }
            function submitSortingFunction(param) {
                var $this = $(param);
                $this.button('loading');
                var profileId = $("#profileId").val();
                var d1 = new Date($("#startDate").val());
                var d2 = new Date($("#endDate").val());
                if (d1 > d2) {
                    alert("Start date should be less than end date");
                    return false;
                }
                getProfileData(profileId);
            }
            function loadingPage(status) {
                if (status) {
                    $("#loadingDiv").show();
                    $("body").css("overflow-y", "hidden");
                } else {
                    $("#loadingDiv").hide();
                    $("body").css("overflow-y", "auto");
                }
            }
            // for downloading chart image
            function downloadImage(uri, filename) {
                var link = document.createElement('a');
                if (typeof link.download === 'string') {
                    document.body.appendChild(link); // Firefox requires the link to be in the body
                    link.download = filename;
                    link.href = uri;
                    link.click();
                    document.body.removeChild(link); // remove the link when done
                } else {
                    location.replace(uri);
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                var url;
                var analyticsAccount = $('input[name="isAnalyticsAccount"]:checked').val();
                if (analyticsAccount === "1") {
                    url = $("#website").html();
                } else {
                    url = $('#domain').val();
                }
                $.ajax({
                    type: "POST",
                    url: "/penalty-checker-tool.html?ate=penaltyCheckerATE&domain=" + url,
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            /*If user comes from mail link we are auto populating the inputs with their data 
             * and gives result for that request*/
            function getUserToolInputs(website, userInput) {
                var userToolInputs = JSON.parse(userInput);
                $('#domain').val(website);
                var isAnalyticsAccount = userToolInputs["2"];
                if (isAnalyticsAccount) {
                    $("#non-analytics-section").hide();
                    $("#radioNo").removeClass("disabled");
                    $("#analytics-section").show();
                    if ($("#profileData").html() !== "") {
                        $('.profile-info').show();
                    }
                    $("#analytics-error").html("Please connect with your analytics account.");
                } else {
                    $("#non-analytics-section").show();
                    $("#radioNo").addClass("disabled");
                    $("#analytics-section").hide();
                    $("#analytics-error").hide();
                    $('.profile-info').hide();
                    $('#getResult').click();
                }
                if (this.value === "1") {
                    $("#non-analytics-section").hide();
                    $("#radioNo").removeClass("disabled");
                    $("#analytics-section").show();
                } else if (this.value === "0") {
                    $("#non-analytics-section").show();
                    $("#radioNo").addClass("disabled");
                    $("#analytics-section").hide();
                }
            }
            function submitPreviousRequest() {
                if (isAnalyticsRequest) {
                    $("#connectAnalytics").click();
                } else {
                    $("#getResult").click();
                }
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                // Input radio-group visual controls
                $('.radio-group label').on('click', function () {
                    $(this).removeClass('not-active').siblings().addClass('not-active');
                });

                $('#startDate.lxr-date-picker').datetimepicker({
                    format: 'YYYY-MM-DD',
                    maxDate: new Date()
                });
                $('#endDate.lxr-date-picker').datetimepicker({
                    format: 'YYYY-MM-DD',
//                    minDate: function () {
//                        return $('#startDate').val();
//                    },
                    maxDate: new Date()
                });
            });
        </script>
        <style type="text/css">
            .penalty-checker-filter-section label.not-active {
                color: #828282 !important;
                background-color: #eeeeef !important;
                border-color: #9e9e9e !important;
            }
            .penalty-checker-filter-section label:hover,
            .penalty-checker-filter-section label{border-color: #828282 !important;background-color: #9e9e9e !important;color: #FFF !important;}
            .radio-group label {
                overflow: hidden;
            }
            .radio-group input {
                /* This is on purpose for accessibility. Using display: hidden is evil.
                This makes things keyboard friendly right out tha box! */
                height: 1px;
                width: 1px;
                position: absolute;
                top: -20px;
            }
            .radio-group .not-active {
                color: #3276b1;
                background-color: #fff;
            }
            .pc-input-group-border{border-color: #828282 !important;}
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <!--<div id="chart_div"></div>-->
        <div class="container">
            <!--            <div class="row">
                            <div class="col-xs-5 col-md-3 col-lg-1 vcenter">
                                <div style="height:10em;border:1px solid #000">Big</div>
                            </div>
                            <div class="col-xs-5 col-md-7 col-lg-9 vcenter">
                                <div style="height:3em;border:1px solid #F00">Small</div>
                            </div>
                        </div>-->
            <div id="loadingDiv" class="col-12 p-0" style="display: none;position: fixed;background: gray;width: 100%;height: 100%;top: 0;z-index: 1000;color: #FFF;left: 0;opacity: 0.8;text-align: center;">
                <span class="lxrm-bold" style="position: relative;top: 50%;font-size: 2rem;"><span class='fa fa-spinner fa-spin'></span> Loading...</span>
            </div>
            <!--<canvas id="mixed-chart" width="500" height="250"></canvas>-->
            <springForm:form commandName="penaltyChecker" method="POST">
                <div class="col-xs-12 select-analytics-section text-center">
                    <span class="compareWrapper" style="font-size: 18px;">Do You Have Analytics Account?</span>
                    <!--                    <div class="btn-group btn-group-toggle button-toggle-div" data-toggle="buttons" style="margin: 0 auto;">
                                            <span id="jsonld" class="btn active lxr-switch" role="button">JSON-LD
                                                <input type="radio" name="formatType" value="json-ld" id='yes'>
                                            </span>
                                            <span id="microdata" class="btn lxr-switch in-active" role="button">Microdata
                                                <input type="radio" name="formatType" value="microdata" id='no'>
                                            </span>
                                        </div>-->
                    <!--<div class="btn-group btn-group-toggle" data-toggle="buttons">-->
                    <!--                        <span id="radioYes" class="btn active lxr-switch" role="button" title="Yes">Yes
                                                <input type="radio" name="isAnalyticsAccount" value="1" id='yes'>
                                            </span>
                                            <span id="radioNo" class="btn lxr-switch" role="button" title="No">No
                                                <input type="radio" name="isAnalyticsAccount" value="0" id='no'>
                                            </span>-->
                    <div class="col-xs-12 text-center">
                        <label class="radio-inline lxrm-bold"><input type="radio" name="isAnalyticsAccount" value="1" id='yes' checked>Yes</label>
                        <label class="radio-inline lxrm-bold"><input type="radio" name="isAnalyticsAccount" value="0" id='no'>No</label>
                    </div>
                </div>
                <div style="clear: both;height: 10px;" class="visible-xs visible-sm visible-md"></div>
                <div class="col-lg-12" id="non-analytics-section" style="display: none;padding-top: 2%;">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="domain" cssClass="form-control input-lg"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil"></i>
                        </div>
                        <span id="notWorkingUrl" class="lxr-danger text-left"></span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12" id="analytics-section" style="padding-top: 2%;">
                    <button type="button" 
                            id="connectAnalytics" value="LOG IN TO ANALYTICS" title="LOG IN TO ANALYTICS" 
                            class="btn lxr-search-btn top-line btn-lg buttonStyles">
                        LOG IN TO ANALYTICS</button>
                </div>
                <div class="profile-info" style="width: 100%;float: left;margin-top: 1%;display: none;">
                    <div>
                        <div id="profileData"></div>
                    </div>
                </div>
                <div id="no-analytics" class="col-xs-12 text-center lxr-danger" style="padding-top: 2%;"></div>
                <div class="col-xs-12 text-center lxr-danger">
                    <p id="analytics-error"></p>
                </div>
            </springForm:form>
        </div>
        <div class="container-fluid" id="result-section" style="display: none;">
            <div class="col-xs-12 col-sm-8 col-md-6 lxrm-padding-none">
                <h4>
                    <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                    <span class="lxrm-bold">Review of: </span><span id="website">${penaltyChecker.domain}</span>
                </h4>
                <table class="table lxr-table lxr-border" id="review-of-table">
                    <tbody>
                        <tr>
                            <td class="lxrm-bold">Start Date</td>
                            <td class="lxrm-bold">End Date</td>
                            <td class="lxrm-bold">Total Organic Traffic</td>
                        </tr>
                        <tr>
                            <td><span id="sDate">${penaltyChecker.startDate}</span></td>
                            <td><span id="eDate">${penaltyChecker.endDate}</span></td>
                            <td><span id="organicTraffic"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-xs-12 lxrm-padding-none">
                <form class="col-xs-12 lxrm-padding-none" name="penaltyCheckerFilter" id="penaltyCheckerFilter" style="margin-bottom: 10px;">
                    <div class="row  penalty-checker-filter-section">
                        <input type="hidden" name="profileId" id="profileId">
                        <input type="hidden" name="domain" id="domainFilter">
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <div class="input-group lxrm-bold" style="margin-top: 10px;width: 100%;">
                                    <span class="input-group-addon pc-input-group-border"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    <input type="text" id="startDate" name="startDate" class="form-control pc-input-group-border lxr-date-picker" placeholder="From Date" aria-describedby="basic-addon2">
                                    <span class="input-group-addon pc-input-group-border" id="basic-addon2">To</span>
                                    <input type="text" id="endDate" name="endDate" class="form-control pc-input-group-border lxr-date-picker" placeholder="To Date" aria-describedby="basic-addon2">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-2">
                                <div class="input-group" style="margin-top: 10px;width: 100%;">
                                    <div class="btn-group radio-group lxrm-bold" id="dayGroup" style="width: 100%;">
                                        <label class="btn btn-primary not-active" style="width: 50%;">
                                            <!--<i class="fa fa-calendar-day" aria-hidden="true"></i>--> 
                                            Day <input type="radio" value="date" name="analyticsFetchType"></label>
                                        <label class="btn btn-primary" style="width: 50%;">
                                            <!--<i class="fa fa-calendar-week" aria-hidden="true"></i>--> 
                                            Week <input type="radio" value="week" name="analyticsFetchType" checked></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-4">
                                <div class="input-group" style="margin-top: 10px;width: 100%;">
                                    <div class="btn-group radio-group lxrm-bold" id="deviceGroup" style="width: 100%;">
                                        <label class="btn btn-primary not-active" style="width: 27%;"><i class="fa fa-mobile" aria-hidden="true"></i> Mobile <input type="radio" value="mobile" name="device"></label>
                                        <label class="btn btn-primary not-active" style="width: 27%;"><i class="fa fa-tablet" aria-hidden="true"></i> Tablet <input type="radio" value="tablet" name="device"></label>
                                        <label class="btn btn-primary not-active" style="width: 27%;"><i class="fa fa-desktop" aria-hidden="true"></i> Desktop <input type="radio" value="desktop" name="device"></label>
                                        <label class="btn btn-primary" style="width: 19%;">All <input type="radio" value="" name="device" checked></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-2" style="margin-top: 10px;">
                                <div class="input-group lxrm-bold" style="width: 100%;">
                                    <button type="button" id="filterSubmit" value="SUBMIT" title="SUBMIT" class="btn lxr-search-btn buttonStyles" style="width: 100%;" onclick="submitSortingFunction()">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 mobile-padding-none" style="border: 1px solid #ddd;box-shadow: 3px 3px 3px 2px #ddd;">
                <!--            <div class="col-xs-12 lxrm-padding-none">
                                <h4 class="lxrm-bold"><span style="color: green;">
                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    </span> Organic Traffic and Google Algorithm Updates
                                </h4>
                            </div>-->
                <!--<h3 class="lxrm-bold">Organic Traffic and Google Algorithm Updates: </h3>-->
                <div class="chart-info">
                    <div class="chart-actions">
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 pull-right mobile-padding-none" style="padding-top: 10px;">
                            <span><i class="fa fa-refresh" aria-hidden="true" id="resetButton" title="Reset"></i></span>
                            <span><i class="fa fa-chevron-left" aria-hidden="true" id="previousButton" title="Previous"></i></span>
                            <span><i class="fa fa-chevron-right" aria-hidden="true" id="nextButton" title="Next"></i></span>
                            <span><i class="fa fa-search-minus" aria-hidden="true" id="zoomOutButton" title="Zoom Out"></i></span>
                            <span><i class="fa fa-search-plus" aria-hidden="true" id="zoomInButton" title="Zoom In"></i></span>
                            <span><i class="fa fa-download" aria-hidden="true" id="deviceDownload" title="Download"></i></span>
                        </div>
                    </div>
                    <!--                    <div class="chart-actions">
                                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 pull-right mobile-padding-none" style="padding-top: 10px;">
                                                <span><i class="fa fa-refresh lxr-diable-icons" aria-hidden="true" id="resetButton" title="Reset"></i></span>
                                                <span><i class="fa fa-chevron-left lxr-diable-icons" aria-hidden="true" id="previousButton" title="Previous"></i></span>
                                                <span><i class="fa fa-chevron-right lxr-diable-icons" aria-hidden="true" id="nextButton" title="Next"></i></span>
                                                <span><i class="fa fa-search-minus lxr-diable-icons" aria-hidden="true" id="zoomOutButton" title="Zoom Out"></i></span>
                                                <span><i class="fa fa-search-plus" aria-hidden="true" id="zoomInButton" title="Zoom In"></i></span>
                                                <span><i class="fa fa-download" aria-hidden="true" id="deviceDownload" title="Download"></i></span>
                                            </div>
                                        </div>-->
                    <div class="col-xs-12" style="overflow-x: auto;overflow-y: hidden;">
                        <div class="graph">
                            <div id="websiteTrafficChart" class="chart-box"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 lxrm-padding-none" style="margin-top: 2%;" id="google-updates-table-div">
                <div class="col-xs-12 lxrm-padding-none">
                    <h4 class="lxrm-bold"><span style="color: green;">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </span> Google Algorithm Updates
                    </h4>
                </div>
                <table style="border: 1px solid #ddd;" id="resultDivTable" class="table  lxr-table"></table>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div id="tooltip-content"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--</div>-->
        </div>
        <!--Recommended part start-->
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    </body>
</html>