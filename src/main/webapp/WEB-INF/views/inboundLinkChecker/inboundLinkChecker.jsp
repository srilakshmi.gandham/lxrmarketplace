
<%@page import="lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink"%>
<%@page import="lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description"
              content="Try the Free Inbound Link Checker Link at LXRMarketplace. Review URLs, analyze backlinks listed for your site & compare with competitors. Try it today!.">
        <meta name="keywords"
              content="Backlink Checker, Inbound Link Checker, Free Back Link Analyzer,seo inbound links checker, seo back links analysis tool, seo competitor back link analysis tool">
        <title>Free Backlink Checker Tool - SEO Inbound Link Analyzer</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/inboundLinkChecker.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <%
            int toolDBId = 20;
            Object userLoggedInObject = session.getAttribute("userLoggedIn");
            boolean isUserLoggedIn = false;
            if (userLoggedInObject != null) {
                isUserLoggedIn = true;
            }
            /* paidDownloadedReportDetailsList is for previous downloads, 
                if user have downloaded any report in tool.
                Only for logged in users.
             */
            if (isUserLoggedIn) {
                if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                    List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                    pageContext.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
                }
            }
            if (session.getAttribute("blcBacklinks") != null) {
                ArrayList<AhrefBacklink> backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                pageContext.setAttribute("blcBacklinks", backLinks);

            }
            Map<String, String> backlinksPrice = null;
            if (session.getAttribute("backlinksPrice") != null) {
                backlinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                pageContext.setAttribute("backlinksPrice", backlinksPrice);
            }
            String downloadType = "";
            if (session.getAttribute("downloadType") != null) {
                downloadType = (String) session.getAttribute("downloadType");
            }
            pageContext.removeAttribute("selectedPrice");
            pageContext.removeAttribute("selectedBackLinkCount");

            String reqParam = request.getParameter("report");
            String selectedPrice = request.getParameter("price");
            String selectedBackLinkCount = request.getParameter("count");

            pageContext.setAttribute("downloadType", downloadType);
            pageContext.setAttribute("reqParam", reqParam);
            pageContext.setAttribute("selectedPrice", selectedPrice);
            pageContext.setAttribute("selectedBackLinkCount", selectedBackLinkCount);

        %>
        <script type="text/javascript">
            <c:set var="status" value="${getSuccess}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            var reportDownload = 0;
            var downloadReportType = "";
            var price = 0;
            var count = 0;
            var priceMap;
            $(document).ready(function () {
            <c:if test="${(status.equals('success')) && (blcBacklinks != '' && blcBacklinks ne null)}">
                priceMap = ${priceJSON};
            </c:if>
                $("#getResult").on('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if ($('#url').val().trim() === "" || $('#url').val() === null || $('#url').val() === 0) {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $('#url').focus();
                        return false;
                    }
                    $("#notWorkingUrl").html("");

                    var $this = $(this);
                    $this.button('loading');
                    var f = $("#inboundLinkCheckerTool");
                    f.attr('action', "seo-inbound-link-checker-tool.html?getResult='getResult'");
                    f.submit();

                });
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report") !== -1) {
                    downloadBackLinks();
                }
//                toolLimitExceeded();
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage='))  
                          && (blcBacklinks != '' && blcBacklinks ne null)}">
                $("#editImport").show();
                $("#url").addClass("gray");
                priceMap = ${priceJSON};
            </c:if>

                $('#editImport, #url').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#url").removeClass("gray");
                });
                $('#url').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }
            });

            function giveDownloadReport(fileName) {
                var f = $("#inboundLinkCheckerTool");
                f.attr('method', "POST");
                f.attr('action', "seo-inbound-link-checker-tool.html?prvDownloadFile=" + fileName);
                f.submit();
                return false;
            }

            function downloadBackLinks() {
                var backlinks = "${downloadType}";
                var pricePlan = "${selectedPrice}";
                var numberOfBackLinks = "";
                if (backlinks !== "") {
                    numberOfBackLinks = "${reqParam}";
                } else if (pricePlan > 0) {
                    numberOfBackLinks = "${selectedBackLinkCount}";
                } else {
                    numberOfBackLinks = $("#numberOfBackLinks").val();
                }
                count = 0;
                count = numberOfBackLinks;
                price = 0;
                price = priceMap[count];
                if (userLoggedIn) {
                    var f = $("#inboundLinkCheckerTool");
                    f.attr('method', 'POST');
                    f.attr('action', 'seo-inbound-link-checker-tool.html?download=download&price=' + price + '&count=' + count);
                    f.submit();
                } else {
                    reportDownload = 1;
                    count = 0;
                    count = numberOfBackLinks.trim();
                    price = 0;
                    price = priceMap[count];
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
                var url = $('#url').val();
                $.ajax({
                    type: "POST",
                    url: "/seo-inbound-link-checker-tool.html?ate='askexpert'&domain=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function showHowToFixContent(id) {
                $(".backlink-urls-info").hide();
                $(".how-to-fix-" + id).show(1000);
            }
            function hideHowToFixContent(id) {
                $(".backlink-urls-info").show(1000);
                $(".how-to-fix-" + id).hide();
            }
            function getUserToolInputs(website, userInput) {
                $('#url').val(website);
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <div class="container">
            <!--<div class="row">-->
                <springForm:form commandName="inboundLinkCheckerTool" method="POST">
                    <div class="col-lg-12">
                        <!--<div class="form-group col-lg-10 col-sm-10" >-->
                        <div class="form-group col-lg-10 col-md-9 col-sm-12">
                            <div class="icon-addon addon-lg">
                                <springForm:input placeholder="Enter your URL" path="url" cssClass="form-control input-lg"/>
                                <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                            </div>
                            <span id="notWorkingUrl" class="lxr-danger text-left">${notWorkingUrl}</span>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn">
                                <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                    ANALYZE
                                </button>
                            </div>
                        </div>
                    </div>
                </springForm:form>
            <!--</div>-->
            <div class="clearfixdiv"></div>
        </div>
        <!--Recently Downloaded Reports:Ends -->
        <!--   Success Page -->
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                      && (blcBacklinks != '' && blcBacklinks ne null)}">
              <div class="container">
                  <div class="row">
                      <div class="col-xs-12 backlink-info" style="margin:0 auto;">
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.ahrefsbackLinks}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-9 stats-header">
                                  <p>Number of Backlinks</p>
                              </div>
                          </div>
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.refdomains}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-9 stats-header">
                                  <p>Referring Domains</p>
                              </div>
                          </div>
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.noFollowLinks}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-9 stats-header">
                                  <p>Total No Follow Links</p>
                              </div>
                          </div>
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.govLinks}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-9 stats-header">
                                  <p>Governmental Links</p>
                              </div>
                          </div>
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.eduLinks}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-9 stats-header">
                                  <p>Educational Links</p>
                              </div>
                          </div>
                          <div class="col-lg-2 col-sm-4 col-xs-6 text-center">
                              <div class="col-xs-12 stats-numeric">
                                  <h3>${ahrefBackLinkDomainInfo.googleIndexedPages >= 0 ? ahrefBackLinkDomainInfo.googleIndexedPages : '-'}</h3>
                              </div>
                              <div class="col-xs-12 col-lg-10 stats-header">
                                  <p>Indexed Pages in Google</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row">
                      <div class="clearfixdiv"></div>
                      <div class="col-xs-12 backlink-urls-info" style="overflow: auto;">
                          <table class="table table-striped col-xs-12">
                              <thead>
                                  <tr>
                                      <th class="col-xs-5">REFERRING URL</th>
                                      <th class="col-xs-1">REFERRING URL RATING</th>
                                      <th class="col-xs-4">ANCHOR TEXT</th>
                                      <th class="col-xs-2">LAST CHECKED</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <c:choose>
                                      <c:when test="${ahrefBackLinkDomainInfo.ahrefsbackLinks ne 0}">
                                          <c:set var="isDofollow" value="true"/>
                                          <c:set var="isNofollow" value="true"/>
                                          <c:forEach items="${blcBacklinks}" var="ahrefBacklink" varStatus="i">
                                              <c:if test="${ahrefBacklink.noFollow}">
                                                  <c:if test="${isNofollow}">
                                                      <c:set var="isNofollow" value="false"/>
                                                  <thead>
                                                      <tr>
                                                          <th colspan="4" style="background: #FFF;border-top: none; border-bottom: 1px solid #ddd;">
                                                              <span class="lxr-danger">NO FOLLOW URL(s)</span>
                                                              <div>
                                                                  <p class="sub-header">
                                                                      Nofollow provides a way for Webmasters to instruct search engines to 
                                                                      <span class="lxr-danger" style="cursor: pointer;" onclick="showHowToFixContent(1);">NOT FOLLOW</span>
                                                                      a specific link. They also do not pass any link value.
                                                                  </p>
                                                              </div>
                                                          </th>

                                                      </tr>
                                                  </thead>
                                              </c:if>
                                              <tr>
                                                  <td><a href="${ahrefBacklink.sourceURL}" target="_blank">${ahrefBacklink.sourceURL}</a></td>
                                                  <td class="text-right" style="padding-right: 2%;">${ahrefBacklink.rating}</td>
                                                  <td>
                                                      <c:choose>
                                                          <c:when test="${ahrefBacklink.anchorText eq ''}">
                                                              <c:out value="-"/>
                                                          </c:when>
                                                          <c:when test="${fn:length(ahrefBacklink.anchorText) gt 50}">
                                                              <c:out value="${fn:substring(ahrefBacklink.anchorText, 0, 50)}.."/>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <c:out value="${ahrefBacklink.anchorText}"/>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </td>
                                                  <td>${ahrefBacklink.lastVisitedDateString}</td>
                                              </tr>
                                          </c:if>
                                      </c:forEach>
                                      <c:forEach items="${blcBacklinks}" var="ahrefBacklink">
                                          <c:if test="${!ahrefBacklink.noFollow}">
                                              <c:if test="${isDofollow}">
                                                  <c:set var="isDofollow" value="false"/>
                                                  <thead>
                                                      <tr>
                                                          <th colspan="4" style="color: green;background: #FFF;border-top: none;border-bottom: 1px solid #ddd;">
                                                              <span class="lxr-success">DO FOLLOW URLs</span>
                                                              <div>
                                                                  <p class="sub-header">
                                                                      Dofollow links allow all search engines to 
                                                                      <span class="lxr-success" style="cursor: pointer;" onclick="showHowToFixContent(2);">FOLLOW</span> 
                                                                      them and pass the link authority from one page to another.
                                                                  </p>
                                                              </div>
                                                          </th>

                                                      </tr>
                                                  </thead>
                                              </c:if>
                                              <tr>
                                                  <td><a href="${ahrefBacklink.sourceURL}" target="_blank">${ahrefBacklink.sourceURL}</a></td>
                                                  <td class="text-right" style="padding-right: 2%;">${ahrefBacklink.rating}</td>
                                                  <td>
                                                      <c:choose>
                                                          <c:when test="${ahrefBacklink.anchorText eq ''}">
                                                              <c:out value="-"/>
                                                          </c:when>
                                                          <c:when test="${fn:length(ahrefBacklink.anchorText) gt 50}">
                                                              <c:out value="${fn:substring(ahrefBacklink.anchorText, 0, 50)}.."/>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <c:out value="${ahrefBacklink.anchorText}"/>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </td>
                                                  <td>${ahrefBacklink.lastVisitedDateString}</td>
                                              </tr>
                                          </c:if>
                                      </c:forEach>
                                  </c:when>
                                  <c:otherwise>
                                      <tr> <td colspan="4" style="text-align: center;background: #FFF;border-bottom: 1px solid #ddd;font-family: ProximaNova-Bold;color: #333;"> No backlinks found for your website. </td> </tr>
                                  </c:otherwise>
                              </c:choose>
                              </tbody>
                          </table>
                      </div>
                      <div class="how-to-fix-1" style="display: none;">
                          <div class="col-xs-12 panel-content">
                              <div class="panel panel-default">
                                  <div class="panel-heading lxr-panel-heading">
                                      <span class="panel-title lxr-light-danger">Nofollow link is harmful ?</span>
                                      <span class="glyphicon glyphicon-remove pull-right" id="closebox3" onclick="hideHowToFixContent(1);"></span>
                                  </div>
                                  <div class="panel-body lxr-panel-body">
                                      <p>
                                          <span class="lxrm-bold">For example:</span>

                                      <xmp><a href="http://www.example.com/" rel="nofollow">Link Text</a></xmp>
                                      </p>
                                      <p>
                                          Nofollow links won't hurt you unless you're spamming at a huge scale. 
                                          But, these links are not calculated as a backlink and don't help a page's placement in the SERP.
                                      </p>
                                      <!--                                      <p>1. Try to use the simple, amazing, attractive and readable URL that is easily recognized by the readers.</p>
                                                                            <p>2. Use the URL which matched your business keyword. </p>
                                                                            <p>3. Don’t use long tail keyword which is difficult to read by the customers.</p>-->
                                      <div class="lxr-panel-footer">
                                          <h3><spring:message code="ate.expert.label.2" /></h3>
                                          <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="how-to-fix-2" style="display: none;">
                          <div class="col-xs-12 panel-content">
                              <div class="panel panel-default">
                                  <div class="panel-heading lxr-panel-heading">
                                      <span class="panel-title lxr-light-danger">Advantages / disadvantages of do follow links?</span>
                                      <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixContent(2);"></span>
                                  </div>
                                  <div class="panel-body lxr-panel-body">
                                      <p>
                                          <span class="lxrm-bold">For example: </span><xmp><a href="http://www.example.com/">Link Text</a></xmp> By default every link is a do follow link.
                                      </p>
                                      <p>
                                          A dofollow backlink is a real backlink. 
                                          Yes, dofollow backlinks have both advantages and disadvantages. 
                                          If you earn backlinks from high authority and relevant sites, 
                                          your rankings and authority increases and benefits your site in various ways.
                                          Yet, low quality and irrelevant backlinks can attract penalties.
                                      </p>
                                      <!--                                      <p>A dofollow backlink is a real backlink. Yes, Dofollow backlink has both Advantages and disadvantages. 
                                                                                If you create the backlink on high authority, relevant site, you will get enough benefits. 
                                                                                But low quality, the irrelevant backlink is not good at all for your site.
                                                                            </p>-->
                                      <div class="lxr-panel-footer">
                                          <h3><spring:message code="ate.expert.label.2" /></h3>
                                          <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <c:if test="${ahrefBackLinkDomainInfo.ahrefsbackLinks > 0 }">
                  <div class="container-fluid">
                      <div class="row download-report-row">
                          <div class="col-xs-12 download-report-div" style="width:98%;margin: 0 1%;border: 1px #ccc solid;">
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0.5% 0 0 0;">
                                  <p>Download report here: </p>
                              </div>
                              <div class="col-lg-5 col-md-5 col-sm-10 col-xs-10" style="padding: 0;">
                                  <select id="numberOfBackLinks" class="form-control col-xs-12">
                                      <c:forEach var="entry" items="${backlinksPrice}"> 
                                          <fmt:parseNumber var="linkCount" value="${entry.key}" integerOnly="true"/>
                                          <c:choose>
                                              <c:when test="${linkCount != ahrefBackLinkDomainInfo.ahrefsbackLinks}">
                                                  <c:choose>
                                                      <c:when test="${linkCount <= 100}">
                                                          <option value="${linkCount}">Get upto ${linkCount} Backlinks for FREE</option>
                                                      </c:when>
                                                      <c:otherwise>
                                                          <option value="${linkCount}">Get upto ${linkCount} Backlinks for $${entry.value}</option>
                                                      </c:otherwise>
                                                  </c:choose>
                                              </c:when>
                                              <c:when test="${linkCount == ahrefBackLinkDomainInfo.ahrefsbackLinks}">
                                                  <option value="${linkCount}">Get ALL Backlinks for $${entry.value}</option>
                                              </c:when>
                                          </c:choose>
                                      </c:forEach>
                                  </select>
                              </div>
                              <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                                  <div class="dropdown">
                                      <span id="downloadReport" onclick="downloadBackLinks();" class="glyphicon glyphicon-download-alt"></span>
                                  </div>
                              </div>
                              <c:if test="${ahrefBackLinkDomainInfo.ahrefsbackLinks > 100000}">
                                  <div class="col-xs-12" style="border-top: 1px solid #ddd;margin-top:1%;padding:1% 0 0 0; ">
                                      <p><i class="fa fa-hand-o-right" aria-hidden="true" style="color:#009A2D"></i>&nbsp;&nbsp;To get a complete set of backlinks (${ahrefBackLinkDomainInfo.ahrefsbackLinks}), please contact our support team: <a href="mailto:support@lxrmarketplace.com">support@lxrmarketplace.com</a>.</p>
                                  </div>
                              </c:if>
                          </div>
                      </div>  
                  </div>
              </c:if>
        </c:if>
        <div class="container">
            <div class="clearfixdiv"></div>
            <!--Recently Downloaded Reports:Begin -->
            <c:if test = "${paidDownloadedReportDetailsList != null && paidDownloadedReportDetailsList != '' && fn:length(paidDownloadedReportDetailsList) gt 0}">
                <c:if test="${fn:length(paidDownloadedReportDetailsList) gt 0}">
                    <div class="row">
                        <div class="col-xs-12 prev-dwnld-div">
                            <h4 class='prvs-reports'>Previous Downloaded Reports:</h4>
                            <div style="padding: 0;overflow: auto;max-height: 250px;border: 1px solid #ddd;">
                                <table class="table lxr-table" style="border: 1px solid #ddd;margin-bottom: 0;">
                                    <thead style="background: white;color: #4F4F4F;">
                                        <tr>
                                            <th class="text-left">Created Time</th>
                                            <th class="text-left">URL</th>
                                            <th class="text-left">Report Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${paidDownloadedReportDetailsList}" var="paidReports">
                                            <tr>
                                                <td class="text-left">${paidReports.createdTime}</td>
                                                <td class="text-left">${paidReports.url}</td>
                                                <td class="text-left">
                                                    <span onclick="giveDownloadReport('${paidReports.reportName}');" style="cursor: pointer;color: #2f80ed !important;">${paidReports.reportName}</span>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </c:if>
            </c:if>
            <c:set var="fileMessage" value="${fileExistence}" />
            <c:if test = "${fileMessage eq 'Sorry the requested file is no longer existed.'}">  
                <div class="row">
                    <div id="reportNotification" class="col-xs-12 col-lg-8 notworking prev-dwnld-div">
                        <p class="text-center lxr-danger">Sorry the requested file has no longer existed.</p>
                    </div>
                </div>
            </c:if>
            <div class="clearfixdiv"></div>
        </div>
        <!--   Success Page End-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>