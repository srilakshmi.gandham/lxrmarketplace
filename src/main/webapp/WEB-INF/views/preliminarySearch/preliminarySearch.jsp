<%-- 
    Document   : Preliminary Search Marketing Analysis(PSMA)
    Created on : 17 Aug, 2018, 12:07:52 PM
    Author     : sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google-signin-scope" content="profile email">
        <meta name='google-signin-client_id'  content='587399183753-k9c1ldj0kg3k21kiesjedpo78o7jf7k5.apps.googleusercontent.com'/>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <title>Preliminary Search Marketing Analysis</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <!-- Angular JS -->
        <script type="text/javascript" src="/resources/js/angular/angular.min.js"></script>
        <script src="/resources/js/tools/preliminarySearch/preliminarySearchController.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/preliminarySearch.css">

        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var logoPresent = false;
            var downloadReportType = "";
            var userEmail = "";
            var isGmailLoginSuccess = false;
            $(document).ready(function () {
                $("#logoNameWrapper").empty();
                $("#browseWrapper").click(function () {
                    var file = $('#deckLogo');
                    file.trigger('click');
                });
                $("#deckLogo").change(function () {
                    $('#logoError').empty();
                    $('#logoError').hide();
                    $("#logoNameWrapper").empty();
                    var logoName = $("#deckLogo").val();
                    logoName = logoName.replace("C:\\fakepath\\", "");
                    if (validateDeckLogo()) {
                        $("#logoImageWrapper").hide();
                        if (logoName.length > 9) {
                            logoName = logoName.substr(0, 5) + "..." + logoName.substring(logoName.length - 6, logoName.length);
                        }
                        $("#logoNameWrapper").show();
                        $("#logoNameWrapper").html(logoName);
                    } else {
                        return false;
                    }
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
            function downloadPreliminarySearchReport() {
                var downloadForm = null;
                downloadForm = $("#preliminaryForm");
                downloadForm.attr('method', "POST");
                downloadForm.attr('action', "/preliminary-search.html?request=download");
                downloadForm.submit();
            }
            function validateDeckLogo() {
                if (($("#deckLogo"))[0].files.length > 0) {
                    var file = $("#deckLogo")[0].files[0];
                    var fileType = file["type"];
                    var ValidImageTypes = ["image/gif", "image/jpeg", "image/jpg", "image/png", "image/tiff"];
                    if ($.inArray(fileType, ValidImageTypes) < 0) {
                        alert("Please upload a valid image format.");
                        return false;
                    } else {
                        if ($("#deckLogo")[0].files[0] !== undefined && $("#deckLogo")[0].files[0] !== '' && $("#deckLogo")[0].files[0] !== null) {
                            return validateDeckLogoSize();
                        }
                    }
                }
            }
            function validateDeckLogoSize() {
                var fileUploadLimit = 7000000;
                var fileValidation = true;
                var uploadFile = $("#deckLogo")[0].files[0];
                if (uploadFile.size >= fileUploadLimit) {
                    alert("Uploaded Logo is too large");
                    fileValidation = false;
                }
                return fileValidation;
            }
        </script>
        <style>
            #apiUpdatedMessage{
                display: none;
            }
            .toolInfoWrapper{
                font-size: 15px !important;
                padding: 1% 0;
            }
        </style>
    </head>
    <body ng-app="lxrmarketplacePreSales" ng-controller="PreliminarySearchController as ctrl" id="psmaForm">
        <%@ include file="/WEB-INF/views/emptyHeader.jsp"%>
        <div class="container-fluid">
            <nav style="text-align: right;padding: 3px;font-size: 12px;color: #828282 !important">
                <a class="breadcrumb-item" href="/">Home ></a>
                <span class="breadcrums Regular color2"><a class="breadcrumb-item" href="presales-tools.html">PreSales ></a></span>
                <span itemprop="name"><span class="breadcrumb-item">Preliminary Search Marketing Analysis</span></span>
            </nav>
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="col-xs-2 col-sm-1 col-md-1" style="padding: 0px">
                        <span class="badge lxrsrt-toolicon pull-left" style="background-color: #FFD837;padding: 1px;"> <img src="/resources/images/tool-icons/PSMA.png" alt="ADS.png"></span>
                    </div>
                    <div class="col-xs-10 col-sm-11 col-md-11">
                        <h2 id="toolName" style="font-family: ProximaNova-Bold;">
                            Preliminary Search Marketing Analysis 
                        </h2>
                        <span class="label label-default" style="padding: 5px;background-color: #F58120;">
                            5.0
                            <span class="glyphicon glyphicon-star"></span> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container" style="min-height: 650px;">
            <div class="row">
                <div class="col-xs-12 mobile-padding-none">
                    <form name="preliminarySearchForm" class="form-horizontal" id="preliminaryForm" enctype="multipart/form-data">
                        <div class="col-xs-12 mobile-padding-none">
                            <div class="col-xs-12">
                                <blockquote class="blockquote">
                                    <p>Instantly uncover Website Domain traffic and SEO performance details through SimilarWeb and LXRSEO APIs.</p>
                                    <p>Identify the Channel Performance, Paid Search Performance, Search Traffic By Engine, Device Performance and SEO Performance Scores.</p>
                                </blockquote>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="clearfixdiv" style="height: 20px !important;"></div>
                            <div class="form-group ">
                                <label class="col-lg-5 col-md-5 control-label text-right">User Domain:<span class="lxr-danger">*</span></label>
                                <div class="col-xs-6 text-left">
                                    <div class="col-lg-8">
                                        <input type="text" id="userDomain" ng-model="userDomain"  name="searchQuery" class="form-control input-lg" placeHolder="Enter user domain"/>
                                        <span id="userDomainError" class="error"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-5 col-md-5 control-label text-right">Competitor Domain(s):</label>
                                <div class="col-xs-6 text-left">
                                    <div class="col-lg-8">
                                        <textarea class="form-control lxr-no-resize competitor-wrapper" ng-model="competitorDomains" name="competitorDomains" id="competitorDomains" rows="4" maxlength="1024" placeHolder="Enter your competitor's domains one per line (maximum 4 allowed)"></textarea>
                                        <span id="competitorError" class="error"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-5 col-md-5 control-label text-right">Logo:<span class="lxr-danger">*</span></label>
                                <div class="col-xs-7 text-left pl-3">
                                    <span>
                                        <button class="lxr-bold btn btn-default logoUpload" id="browseWrapper" type="button"><span class="pp-usage-color">B</span>rowse</button>
                                    </span>
                                    <div id="logoNameWrapper"></div>
                                    <input type="file" file-model="deckLogo" name="deckLogo" id="deckLogo" placeholder="Upload Logo" accept="image/*" data-type='image' style="display: none;"> 
                                    <span id="logoError" class="error"/>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <span>
                                    <label class="col-lg-5 col-md-5 control-label text-right">
                                        Email
                                        <a data-toggle="tooltip" data-placement="right" title="The report will be mailed to your email address. Alternately, you can download the report after analysis.">
                                            <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;"  aria-hidden="true"></i>
                                        </a>
                                        :<span class="lxr-danger">*</span>
                                    </label>
                                </span>
                                <div class="col-xs-6 text-left">
                                    <div class="col-xs-8">
                                        <input type="text" id="userOffline" ng-model="userOfflineEmail"  name="userOfflineEmail" class="form-control input-lg" placeHolder="Enter your email address"/>
                                                                                <input type="text" id="userOffline" ng-model="userOfflineEmail"  name="userOfflineEmail" class="form-control input-lg" placeHolder="Enter your email address" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required/>
                                        <span id="userOffilineEmailError" class="error"/>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </form>
                    <div class="col-xs-12">
                        <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="input-group-btn">
                                <button type="button" ng-click="resetPreliminarySearch()" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetPSMA">
                                    <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                </button>
                            </div>
                            <!--</div>--> 
                        </div>
                        <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="input-group-btn">
                                <button type="button" ng-click="processPreliminarySearch()" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING...">
                                    &nbsp;ANALYZE&nbsp;
                                </button>
                            </div>
                            <!--</div>--> 
                        </div>
                        <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                    </div>
                    <div class="col-xs-12 text-center" style="margin: 1.5% 0;">
                        <div id="apiInfoMessage" class="toolInfoWrapper">Available API Hits <span class="lxrm-bold">: ${apiBalanceCount}</span> for this  month (Auto renew's 1st of every month).</div>
                        <div id="apiUpdatedMessage" class="toolInfoWrapper">Available API Hits <span class="lxrm-bold">: {{ctrl.updatedBalanceCount}}</span> for this  month (Auto renew's 1st of every month).</div>
                        <div id="notWorkingUrl" class="lxr-danger text-center error"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12" style="margin: 1.5% 0;">
                            <p class="lxrm-bold  text-center" id="messageToUser" style="margin:0%;font-size: 16px;"></p>
                        </div>
                        <div class="col-xs-12 text-center"  id="downloadWrapper">
                            <div id="downloadSearchReport" onclick="downloadPreliminarySearchReport();" title="Download complete report here" style="margin: 0;">
                                <button>Download Report <i class="glyphicon glyphicon-download-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="googleLoginPopupModal" class="modal fade">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="top: 190px;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title lxrm-bold">Login with NETELIXIR Email</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12" style="padding-left: 3rem; text-align: center;" id="loginBodyWrapper" style="display:none;">
                                    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style='margin-left: 20%;'></div>
                                    <div class="clearfixdiv" style="height: 13px !important;"></div>
                                </div>
                                <div class="col-xs-12" id="errorBodyWrapper" style="display:none;">
                                    <div class="col-xs-12 text-center">
                                        <p id="loginErrorMessage" class="text-center" style="color: red;font-size: 14px;"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv" style="height: 105px !important;"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
                    var googleAuth = sessionStorage.getItem('GAuthToken');
                    if (googleAuth === null) {
                        gmailLoginSuccess();
                        $("#googleLoginPopupModal").modal('show');
                        var errorMessage = sessionStorage.getItem('PSMAError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                        setTimeout(function () {
                            localStorage.setItem("PSMAError", '');
                            sessionStorage.setItem("PSMAError", '');
                        }, 10);
                        //                        $('#googleLoginPopupModal').modal({
                        //                            backdrop: 'static',
                        //                            keyboard: false
                        //                        });
                        function onSignIn(googleUser) {
                            // Useful data for your client-side scripts:
                            var profile = googleUser.getBasicProfile();
                            console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                            console.log('Full Name: ' + profile.getName());
                            console.log('Given Name: ' + profile.getGivenName());
                            console.log('Family Name: ' + profile.getFamilyName());
                            console.log("Image URL: " + profile.getImageUrl());
                            console.log("Email: " + profile.getEmail());
                            userEmail = profile.getEmail();
                            if (userEmail.indexOf('@netelixir.com') >= 0) {
                                isGmailLoginSuccess = true;
                                $("#googleLoginPopupModal").modal('hide');
                                gmailLoginSuccess();
                                sessionStorage.setItem("userOAuthEmail", userEmail);
                                var id_token = googleUser.getAuthResponse().id_token;
                                //console.log("ID Token: " + id_token);
                                sessionStorage.setItem("GAuthToken", id_token);
                                angular.element(document.getElementById('psmaForm')).scope().setEmailValue();
                                $("#userOffline").val(userEmail);
                            } else {
                                isGmailLoginSuccess = false;
                                sessionStorage.setItem("PSMAError", "Please sign in with @netelixir.com mail to use Pre-Sales Tools");
                                signOutfromGoogle();
                            }
                        }
                        ;
                    } else if (googleAuth !== null) {
                        $("#googleLoginPopupModal").modal('hide');
                        isGmailLoginSuccess = true;
                    }
                    function gmailLoginSuccess() {
                        $("#loginBodyWrapper").show();
                        $("#errorBodyWrapper").hide();
                    }
                    function signOutfromGoogle() {
                        var auth2 = gapi.auth2.getAuthInstance();
                        auth2.signOut().then(function () {
                            console.log('User signed out.');
                            localStorage.setItem("GAuthToken", '');
                            $('<iframe style="display: none" src="https://accounts.google.com/Logout"></iframe>').appendTo(document.body);
                            //                            location.reload(true);
                        });
                        var errorMessage = sessionStorage.getItem('PSMAError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                        setTimeout(function () {
                            sessionStorage.setItem("PSMAError", '');
                        }, 10);
                    }
                    $(document).ready(function () {
                        var userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
                        if (userOAuthEmail !== null) {
                            $("#userOffline").val(userOAuthEmail);
                            angular.element(document.getElementById('psmaForm')).scope().setEmailValue();
                        }
                    });
        </script>
    </body>
</html>
