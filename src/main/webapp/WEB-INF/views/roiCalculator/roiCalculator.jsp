
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Free ROI Calculator - Return on Investment Tool | LXR
            Marketplace</title>
        <meta name="description"
              content="Free website Return on Investment (ROI) calculator for online marketing campaigns. Determine your ROI from orders, AOV, CPC, and clicks.">
        <meta name="keywords"
              content="ROI Calculator, Return on Investment Calculator">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <%
            int toolDBId = 3;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var reportStatus = true;
            var downloadReportType = "";
            $(document).ready(function () {

//                toolLimitExceeded();
                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();

                $('#getResultSimple').on('click', function () {
                    resultLoginValue = true;

                    toolLimitExceeded();
                    reportStatus = true;
                    var revenue = $("#revenue").val().trim();
                    var cost = $("#cost").val().trim();
                    var status = true;

                    var validRevenue = validateNumberORFloat(revenue);
                    if (revenue === "" || revenue === null) {
                        document.getElementById('revenueError').textContent = "Please enter revenue";
                        document.getElementById('revenueError').style.display = 'block';
                        status = false;
                    } else if (revenue < 0.01) {
                        document.getElementById('revenueError').textContent = "Revenu should not be 0";
                        document.getElementById('revenueError').style.display = 'block';
                        status = false;
                    } else if (revenue !== "" && validRevenue === null) {
                        document.getElementById('revenueError').textContent = "Please enter a valid revenue";
                        document.getElementById('revenueError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('revenueError').style.display = 'none';
                    }

                    var validCost = validateNumberORFloat(cost);
                    if (cost === "" || cost === null) {
                        document.getElementById('costError').textContent = "Please enter cost";
                        document.getElementById('costError').style.display = 'block';
                        status = false;
                    } else if (cost < 0.01) {
                        document.getElementById('costError').textContent = "Cost should not be 0";
                        document.getElementById('costError').style.display = 'block';
                        status = false;
                    } else if (cost !== "" && validCost === null) {
                        document.getElementById('costError').textContent = "Please enter a valid cost";
                        document.getElementById('costError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('costError').style.display = 'none';
                    }
                    if (status) {
                        var revenue = $("#revenue").val();
                        var cost = $("#cost").val();
                        var output1 = (revenue / cost) * 100;
                        var roi = ((revenue - cost) / cost) * 100;
                        $("#output1Simple").val(output1.toFixed(2) + " %");
                        $("#roiOutputSimple").val(roi.toFixed(2) + " %");

                        $.post('usersession.html?action=roiCalculator');

                        getAskExpertPopup(true);
                    }
                });

                $("#getResultAdvanced").on('click', function () {
                    resultLoginValue = true;
                    toolLimitExceeded();
                    reportStatus = false;
                    var status = true;
                    var orders = $("#orders").val().trim();
                    var aov = $("#aov").val().trim();
                    var cpc = $("#cpc").val().trim();
                    var clicks = $("#clicks").val().trim();
                    var validOrders = validateNumberORFloat(orders);
                    if (orders === "" || orders === null) {
                        document.getElementById('ordersError').textContent = "Please enter number of orders";
                        document.getElementById('ordersError').style.display = 'block';
                        status = false;
                    } else if (orders < 0.01) {
                        document.getElementById('ordersError').textContent = "Orders should not be 0";
                        document.getElementById('ordersError').style.display = 'block';
                        status = false;
                    } else if (orders !== "" && validOrders === null) {
                        document.getElementById('ordersError').textContent = "Please enter a valid number of orders";
                        document.getElementById('ordersError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('ordersError').style.display = 'none';
                    }

                    var validAOV = validateNumberORFloat(aov);
                    if (aov === "" || aov === null) {
                        document.getElementById('aovError').textContent = "Please enter AOV";
                        document.getElementById('aovError').style.display = 'block';
                        status = false;
                    } else if (aov < 0.01) {
                        document.getElementById('aovError').textContent = "AOV should not be 0";
                        document.getElementById('aovError').style.display = 'block';
                        status = false;
                    } else if (aov !== "" && validAOV === null) {
                        document.getElementById('aovError').textContent = "Please enter a valid AOV";
                        document.getElementById('aovError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('aovError').style.display = 'none';
                    }

                    var validCPC = validateNumberORFloat(cpc);
                    if (cpc === "" || cpc === null) {
                        document.getElementById('cpcError').textContent = "Please enter CPC";
                        document.getElementById('cpcError').style.display = 'block';
                        status = false;
                    } else if (cpc < 0.01) {
                        document.getElementById('cpcError').textContent = "CPC should not be 0";
                        document.getElementById('cpcError').style.display = 'block';
                        status = false;
                    } else if (cpc !== "" && validCPC === null) {
                        document.getElementById('cpcError').textContent = "Please enter a valid CPC";
                        document.getElementById('cpcError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('cpcError').style.display = 'none';
                    }

                    var validClicks = validateNumberORFloat(clicks);
                    if (clicks === "" || clicks === null) {
                        document.getElementById('clicksError').textContent = "Please enter clicks";
                        document.getElementById('clicksError').style.display = 'block';
                        status = false;
                    } else if (clicks < 0.01) {
                        document.getElementById('clicksError').textContent = "Clicks should not be 0";
                        document.getElementById('clicksError').style.display = 'block';
                        status = false;
                    } else if (cpc !== "" && validClicks === null) {
                        document.getElementById('clicksError').textContent = "Please enter a valid number of clicks";
                        document.getElementById('clicksError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('clicksError').style.display = 'none';
                    }

                    if (status) {
                        var revAdv = orders * aov;
                        var costAdv = cpc * clicks;
                        var output1Adv = (revAdv / costAdv) * 100;
                        var roiAdv = ((revAdv - costAdv) / costAdv) * 100;
                        $("#output1Advance").val(output1Adv.toFixed(2) + " %");
                        $("#roiOutputAdvance").val(roiAdv.toFixed(2) + " %");
                        $.post('usersession.html?action=roiCalculator');
                        getAskExpertPopup(true);
                    }
                });

            });
            function submitPreviousRequest() {
                if (reportStatus === true) {
                    $("#getResultSimple").click();
                } else {
                    alert();
                    $("#getResultAdvanced").click();
                }
            }
            function getToolRelatedIssues() {

            }
        </script>
        <style>
            .tab-text-color li a{
                color: #337ab7 !important;
                font-size: 16px;
            }
            .tab-text-color>li.active>a, .tab-text-color>li.active>a:focus, .tab-text-color>li.active>a:hover {
                color: #555 !important;
            }

            #exTab1 .tab-content {
                padding : 10px;
                border-bottom: 1px solid #ddd !important;
                border-left: 1px solid #ddd !important;
                border-right: 1px solid #ddd !important;
            }
            .input-group.input-group-unstyled input.form-control {
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
            }
            .input-group-unstyled .input-group-addon {
                border-radius: 4px;
                border: 0px;
                background-color: transparent;
            }
            span i{
                cursor: pointer;
                color: #C4C4C4;
                font-size: 18px !important;
            }
            /*            #clearSimple{
                            background: #fff;
                             height: 45px; 
                            vertical-align: middle;
                            color: #EB5757 !important;
                            border: 1px solid #EB5757 !important;
                            text-align: center;
                            border-radius: 4px !important;
                        }*/
        </style>

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container-fluid">
            <div id="exTab1">	
                <ul class="nav nav-tabs tab-text-color">
                    <li class="active">
                        <a href="#2" data-toggle="tab">Basic</a>
                    </li>
                    <li>
                        <a href="#3" data-toggle="tab">Advanced</a>
                    </li>
                </ul>
                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="2">
                        <form class="form-horizontal" >
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">Revenue</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="revenue" name="revenue">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Total Revenue">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="revenueError"/>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">Cost</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="cost" name="cost">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Total Cost">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="costError"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12  col-sm-4 col-md-3 col-lg-2 text-center">
                                <button type="button" id="getResultSimple" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12">CALCULATE</button>
                                <!--<button type="button" id="clearSimple" class="btn btn-lg buttonStyles col-lg-12">CLEAR</button>-->
                            </div>

                            <div class="col-xs-12  col-sm-4 col-md-3 col-lg-3">

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">RoAS(%)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="output1Simple" name="output1Simple">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Gross Sales Revenue / Advertising Spend">
                                                    <i class="fa fa-info-circle" aria-hidden="true" ></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">ROI(%)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="roiOutputSimple" name="roiOutputSimple">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Gross Margin / Advertising Spend">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="visible-lg visible-md col-lg-4 col-md-3"></div>

                        </form>
                    </div>

                    <div class="tab-pane" id="3" >
                        <form class="form-horizontal" >
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">Orders</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="orders" name="orders"/>
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Number of sales (or lead) conversions">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="ordersError"/>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">AOV($)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="aov" name="aov">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Average Order Value">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="aovError"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">CPC($)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="cpc" name="cpc">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Cost Per Click">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="cpcError"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">Clicks</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="clicks" name="clicks">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Number of Clicks">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>                                                
                                                </a>
                                            </span>
                                        </div>
                                        <span class="error" id="clicksError"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12  col-sm-4 col-md-3 col-lg-2 text-center">
                                <button type="button" id="getResultAdvanced" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12">CALCULATE</button>
                            </div>

                            <div class="col-xs-12  col-sm-4 col-md-3 col-lg-3">

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">RoAS(%)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="output1Advance" name="output1Advance">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Gross Sales Revenue / Advertising Spend">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="text">ROI(%)</label>
                                    <div class="col-sm-9">
                                        <div class="input-group input-group-unstyled">
                                            <input type="text" class="form-control" id="roiOutputAdvance" name="roiOutputAdvance">
                                            <span class="input-group-addon">
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Gross Margin / Advertising Spend">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="visible-lg visible-md col-lg-4 col-md-3"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>