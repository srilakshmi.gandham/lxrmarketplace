<%-- 
    Document   : AdSpy
    Created on : 11 Apr, 2018, 4:27:58 PM
    Author     : Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name='google-signin-client_id'  content='587399183753-k9c1ldj0kg3k21kiesjedpo78o7jf7k5.apps.googleusercontent.com'/>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <title>Ad Spy</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <script type="text/javascript" src="/resources/js/angular/angular.min.js"></script>
        <script src="/resources/js/tools/adSpyV2/adSpyV2.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/adSpy.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var downloadReportType = "";
            var userEmail = "";
            var isGmailLoginSuccess = false;
            $(document).on('change', ':file', function () {
            var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready(function () {
            $('#fileUploadWrapper').click(function () {
            var manuallyContent = $.trim($("#queryDomains").val());
            manuallyContent = manuallyContent.replace(/&nbsp;/gi, " ");
            manuallyContent = manuallyContent.replace(/<br>/gi, " ");
            manuallyContent = manuallyContent.trim();
            if (manuallyContent !== undefined && manuallyContent !== null && manuallyContent !== "") {
            var response = confirm('Manually entered company website URLs needs to be cleared before uploading the file.\nDo you want to clear now ?');
            if (response === true) {
            $('#queryDomains').val('');
            $("#inputUrlError").empty();
            angular.element(document.getElementById('AdSpyV2JS')).scope().resetManaully();
            } else {
            if (($.trim($("#queryDomains").val()) && ($.trim($("#filePath").val()) &&
                    ($("#domainsList")[0].files[0] !== undefined
                            && $("#domainsList")[0].files[0] !== ''
                            && $("#domainsList")[0].files[0] !== null)))) {
            $("#domainsList").val('');
            $("#filePath").val('');
            $("#domainsList, #filePath").empty();
            }
            return false;
            }
            }
            });
            $(':file').on('fileselect', function (event, numFiles, label) {
            var input = $(".file-upload-wrapper").find('input[type=text]'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
            input.val(log);
            }
            });
            });
            function getLines(id) {
            var queryURLSCount = 0;
            var lines = $.trim($('#' + id).val()).split("\n");
            for (var i = 0; i < lines.length; i++) {
            if (lines[i].length > 0)
                    queryURLSCount++;
            }
            return queryURLSCount;
            }
        </script>
    </head>
    <body ng-app="adSpyV2PreSales" ng-controller="AdSpyV2Controller as ctrl" id="AdSpyV2JS">
        <%@ include file="/WEB-INF/views/emptyHeader.jsp"%>
        <div class="container-fluid">
            <nav style="text-align: right;padding: 3px;font-size: 12px;color: #828282 !important">
                <a class="breadcrumb-item" href="/">Home ></a>
                <span class="breadcrums Regular color2"><a class="breadcrumb-item" href="presales-tools.html">PreSales ></a></span>
                <span itemprop="name"><span class="breadcrumb-item">Ad Spy</span></span>
            </nav>
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="col-xs-2 col-sm-1 col-md-1" style="padding: 0px">
                        <span class="badge lxrsrt-toolicon pull-left" style="background-color: #00DAAE;padding: 1px;"> <img src="/resources/images/tool-icons/ADS.png" alt="ADS.png"></span>
                    </div>
                    <div class="col-xs-10 col-sm-11 col-md-11">
                        <h2 id="toolName" style="font-family: ProximaNova-Bold;">
                            Ad Spy
                        </h2>
                        <span class="label label-default" style="padding: 5px;background-color: #F58120;">
                            5.0
                            <span class="glyphicon glyphicon-star"></span> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <form name="adSpyForm" class="form-horizontal" id="adSpyV2Form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12 mobile-padding-none">
                        <div class="col-xs-12"> 
                            <blockquote class="blockquote">
                                <p>Instantly uncover website's Visits, Ad Presence and Estimated Monthly Adwords Budget details through SimilarWeb and SpyFu API's .</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 20px !important;"></div>
                    <!--<div class="col-xs-12">-->
                    <div class="col-xs-12 tool-input-wrapper">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Enter the list of company website URLs :
                                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                           title="Enter company website URLs in the text box below. Enter one company website URL per line (example.com).">
                                            <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                        </a>
                                    </label>
                                    <textarea class="form-control lxr-no-resize col-xs-9" ng-model="queryDomains" name="queryDomains" id="queryDomains" rows="5" placeHolder="Enter the company website URLs one per line (example.com)."></textarea>
                                </div>
                                <p id="inputUrlError" class="error"></p>
                            </div>
                            <div style="height:10px;clear: both;" class="visible-xs visible-sm"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="keywordsFile" class="uploadLabel empty-left-padding"><span style="color:#4CAF50;font-family: 'ProximaNova-Bold';">(OR)</span> Upload your company website URL(S) list (.xls /.xlsx formats) : </label>
                                    <div class="col-xs-12 empty-left-padding ">
                                        <div class="col-xs-10 empty-left-padding file-upload-wrapper">
                                            <div class="input-group">
                                                <input type="text" readonly="readonly" id="filePath" class="form-control" placeholder="Uploaded file name" style="height: 3.5rem;">
                                                <span id="clearFilePath" style="cursor: pointer;" class="input-group-addon"><i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 empty-left-padding" id="fileUploadWrapper" style="padding-right: 0;">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary file-action-wrapper" style="width: 100%;">
                                                    Browse<input type="file" file-model="domainsList" name="domainsList" id="domainsList" style="display: none;"> 
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <p class="columnInfo">Mandatory Column: Company URL</p>
                                </div>
                                <div class="clearfixdiv" style="height: 10px !important"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <label ng-repeat="metric in metrics" style="margin-right: 10px;">
                                <input type="checkbox" name="selectedOptions" value="{{metric}}" ng-checked="selection.indexOf(metric) > - 1" ng-click="toggleSelection(metric)"> {{metric}}
                                <!--<input type="checkbox" name="selectedOptions" value="{{metric}}" ng-checked="selection.indexOf(metric) > - 1"> {{metric}}-->
                            </label>
                        </div>
                        <div class="col-xs-12">
                            <p id="metricSelectError" class="error"></p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 tool-input-wrapper">
                            <div style="height:10px;clear: both;"></div>
                            <div class="col-xs-12">
                                <div class="col-xs-4 visible-md visible-lg"></div>
                                <div class="col-xs-12 col-sm-6 col-md-2">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="resetAdSpyForm()" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetAdSpy">
                                            <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                                <div class="col-xs-12 col-sm-6 col-md-2">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="processAdSpyQuery()" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                            &nbsp;ANALYZE&nbsp;
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-4 visible-md visible-lg"></div>
                            </div>
                            <div class="col-xs-12 text-center">
                                <p id="toolInfo" class="lxrm-bold toolInfoWrapper"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p id="apiInfoMessage" class="toolInfoWrapper">Available API Hits <span class="lxrm-bold">: ${apiBalanceCount}</span> for this  month (Auto renew's 1st of every month).</p>
                        <p id="apiUpdatedMessage" class="toolInfoWrapper">Available API Hits <span class="lxrm-bold">: {{ctrl.updatedAPIBalanceCount}}</span> for this  month (Auto renew's 1st of every month).</p>
                        <p id="toolInfoMessage" class="toolInfoWrapper lxrm-bold"></p>
                    </div>
                </div>
            </form>
            <div class="clearfixdiv"></div>
            <div class="row" id="adSpyResultWrapper">
                <!--<div class="col-xs-12"  ng-if="ctrl.adSpyProcessedList.length > 0 && ctrl.isResponseSuccess">-->
                <div class="col-xs-12">
                    <div class="col-xs-12 panel-default">
                        <div class="panel-heading lxr-panel-heading row" ng-if="((ctrl.adPresence && ctrl.adwordsBudget) || (ctrl.adPresence && !ctrl.adwordsBudget))">
                            <div class="col-xs-6"><span>Ad Presence Analysis</span></div>
                            <div class="col-xs-6 text-right">Date Range for the data: &nbsp;<span>{{ctrl.fromDate}} - {{ctrl.toDate}}</span></div>
                        </div> 
                        <div class="panel-heading lxr-panel-heading row" ng-if="ctrl.adwordsBudget && !ctrl.adPresence">
                            <div class="col-xs-12"><span>Ad Presence Analysis</span></div>
                        </div> 
                        <div class="panel-body lxr-panel-body" ng-if="ctrl.adwordsBudget || ctrl.adPresence">
                            <div class="col-xs-12 table-parent-div text-nowrap" ng-if="ctrl.adPresence && ctrl.adwordsBudget">
                                <table class="table" id="adSpyV2AnalysisAdPresenceAndBudget" ng-if="ctrl.adPresence && ctrl.adwordsBudget">
                                    <thead style="background: #22A6B3 !important;">
                                        <tr>
                                            <th class="text-center">S.No.</th>
                                            <th class="">Company</th>
                                            <th class="text-center">Ad Presence</th>
                                            <th class="text-center">Total Visits (Past 12 Months)</th>
                                            <th class="text-center">Average Monthly Visits</th>
                                            <th class="text-center">Paid Traffic</th>
                                            <th class="text-center">Organic Traffic</th>
                                            <th class="text-center">Paid Visits</th>
                                            <th class="text-center">Organic Visits</th>
                                            <th class="text-center">Last Month's Estimated Adwords Budget</th>
                                            <th class="text-center">Approx Monthly Adwords Budget (Last Month's Est Adwords Budget * 6)</th>
                                        </tr>
                                    </thead>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length > 0">
                                        <tr ng-repeat="adSpyV2 in ctrl.adSpyProcessedList track by $index">
                                            <td class="text-right">{{$index + 1}}.</td>
                                            <td class="text-left">{{adSpyV2.companyName}}</td>
                                            <td class="text-center">{{adSpyV2.addPresence ? 'YES' :'NO'}}</td>
                                            <td class="text-right">{{adSpyV2.trafficTotalVisits| number}}</td>
                                            <td class="text-right">{{adSpyV2.trafficMonthlyVisits| number}}</td>
                                            <td class="text-center">{{((adSpyV2.paidTraffic > 0) ? ((adSpyV2.paidTraffic > 0 && adSpyV2.paidTraffic < 1)?"< 1":(adSpyV2.paidTraffic | number : 2)):adSpyV2.paidTraffic)}} %</td>
                                            <td class="text-center">{{((adSpyV2.organicTraffic > 0) ? ((adSpyV2.organicTraffic > 0 && adSpyV2.organicTraffic < 1)?"< 1":(adSpyV2.organicTraffic | number : 2)):adSpyV2.organicTraffic)}} %</td>
                                            <td class="text-right">{{adSpyV2.paidVisits| number}}</td>
                                            <td class="text-right">{{adSpyV2.organicVisits| number}}</td>
                                            <td class="text-right">$ {{adSpyV2.monthlyAdwordsBudget| number}}</td>
                                            <td class="text-right">$ {{adSpyV2.approxMonthlyAdwordsBudget| number}}</td>
                                        </tr>
                                    </tbody>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length == 0">
                                        <tr><td class="text-right" colspan="11"><span style="">No Records Found</span></td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12 table-parent-div text-nowrap" ng-if="ctrl.adPresence && !ctrl.adwordsBudget">
                                <table class="table" id="adSpyV2AnalysisAdPresence" ng-if="ctrl.adPresence && !ctrl.adwordsBudget">
                                    <thead style="background: #22A6B3 !important;">
                                        <tr>
                                            <th class="text-center">S.No.</th>
                                            <th class="">Company</th>
                                            <th class="text-center">Ad Presence</th>
                                            <th class="text-center">Total Visits (Past 12 Months)</th>
                                            <th class="text-center">Average Monthly Visits</th>
                                            <th class="text-center">Paid Traffic</th>
                                            <th class="text-center">Organic Traffic</th>
                                            <th class="text-center">Paid Visits</th>
                                            <th class="text-center">Organic Visits</th>
                                        </tr>
                                    </thead>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length > 0">
                                        <tr ng-repeat="adSpyV2 in ctrl.adSpyProcessedList track by $index">
                                            <td class="text-right">{{$index + 1}}.</td>
                                            <td class="text-left">{{adSpyV2.companyName}}</td>
                                            <td class="text-center">{{adSpyV2.addPresence ? 'YES' :'NO'}}</td>
                                            <td class="text-right">{{adSpyV2.trafficTotalVisits| number}}</td>
                                            <td class="text-right">{{adSpyV2.trafficMonthlyVisits| number}}</td>
                                            <td class="text-center">{{((adSpyV2.paidTraffic > 0) ? ((adSpyV2.paidTraffic > 0 && adSpyV2.paidTraffic < 1)?"< 1":(adSpyV2.paidTraffic | number : 2)):adSpyV2.paidTraffic)}} %</td>
                                            <td class="text-center">{{((adSpyV2.organicTraffic > 0) ? ((adSpyV2.organicTraffic > 0 && adSpyV2.organicTraffic < 1)?"< 1":(adSpyV2.organicTraffic | number : 2)):adSpyV2.organicTraffic)}} %</td>
                                            <td class="text-right">{{adSpyV2.paidVisits| number}}</td>
                                            <td class="text-right">{{adSpyV2.organicVisits| number}}</td>
                                        </tr>
                                    </tbody>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length == 0">
                                        <tr><td class="text-right" colspan="11"><span style="">No Records Found</span></td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12 table-parent-div text-nowrap" ng-if="ctrl.adwordsBudget && !ctrl.adPresence">
                                <table class="table" id="adSpyV2AnalysisAdBudget" ng-if="ctrl.adwordsBudget && !ctrl.adPresence">
                                    <thead style="background: #22A6B3 !important;">
                                        <tr>
                                            <th class="text-center">S.No.</th>
                                            <th class="">Company</th>
                                            <th class="text-center">Last Month's Estimated Adwords Budget</th>
                                            <th class="text-center">Approx Monthly Adwords Budget (Last Month's Est Adwords Budget * 6)</th>
                                        </tr>
                                    </thead>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length > 0">
                                        <tr ng-repeat="adSpyV2 in ctrl.adSpyProcessedList track by $index">
                                            <td class="text-right">{{$index + 1}}.</td>
                                            <td class="text-left">{{adSpyV2.companyName}}</td>
                                            <td class="text-right">$ {{adSpyV2.monthlyAdwordsBudget| number}}</td>
                                            <td class="text-right">$ {{adSpyV2.approxMonthlyAdwordsBudget| number}}</td>
                                        </tr>
                                    </tbody>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length == 0">
                                        <tr><td class="text-right" colspan="11"><span style="">No Records Found</span></td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfixdiv" style="height: 25px !important;"></div>
                        </div>
                        <div class="panel-footer lxr-panel-tool-footer" style="height: 6.5rem;" ng-if="ctrl.adwordsBudget || ctrl.adPresence">
                            <div class="row" ng-if="ctrl.adSpyProcessedList != null && ctrl.adSpyProcessedList.length > 0">
                                <div class="col-xs-3 visible-md visible-lg"></div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                    <input type="text" class="form-control emailWrapper" placeholder="Enter your email address" name="email" id="email">
                                    <div id="mail-sent-status" class="emptyData"></div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-2" style="padding-top: 0.5%;" ng-init="clearAnalaysisCount()">
                                    <div id="mailAdSpy"  ng-click="sendAnalysisReport()" style="font-size: 20px;cursor: pointer;padding-left: 0;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-send sendReport"></div>
                                    <div id="downloadAdSpyReport" ng-click="downloadAnalysisReport()" style="font-size: 20px;cursor: pointer;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt"></div>
                                    <div class="col-xs-12 col-sm-5 col-lg-4 visible-md visible-lg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 185px !important;"></div>
                </div>
            </div>
        </div>
        <div id="googleLoginPopupModal" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="top: 190px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title lxrm-bold">Login with NETELIXIR Email</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="padding-left: 3rem; text-align: center;" id="loginBodyWrapper" style="display:none;">
                                <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style='margin-left: 20%;'></div>
                                <div class="clearfixdiv" style="height: 13px !important;"></div>
                            </div>
                            <div class="col-xs-12" id="errorBodyWrapper" style="display:none;">
                                <div class="col-xs-12 text-center">
                                    <p id="loginErrorMessage" class="text-center" style="color: red;font-size: 14px;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" role="dialog" id="confirmationPopupModal"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content" style="top: 100px;">
                    <div class="modal-header">
                        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                        <h4 class="modal-title lxrm-bold">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p id="confirmationMessageWrraper"></p>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-12 mtb">
                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="offlineEmail" type="text" class="form-control" name="offlineEmail" placeholder="Enter your email address">
                                    </div>
                                    <p id="offlineError" class="mt"></p>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="cancelInput()" class="btn top-line btn-lg buttonStyles col-xs-12"  id="cancelInput">
                                            Cancel this input
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="acceptConditions()" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="limitAccepted">
                                            Continue with offline Result 
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv" style="height: 105px !important;"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
            var gauth = sessionStorage.getItem('GAuthToken');
            if (gauth === null) {
                    gmailLoginSuccess();
                      $("#googleLoginPopupModal").modal('show');
                        var errorMessage = sessionStorage.getItem('ADSPYError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                    setTimeout(function () {
                            sessionStorage.setItem("ADSPYError", '');
                            }, 10);
                    function onSignIn(googleUser) {
                            // Useful data for your client-side scripts:
                            var profile = googleUser.getBasicProfile();
                            console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                            console.log('Full Name: ' + profile.getName());
                            console.log('Given Name: ' + profile.getGivenName());
                            console.log('Family Name: ' + profile.getFamilyName());
                            console.log("Image URL: " + profile.getImageUrl());
                            console.log("Email: " + profile.getEmail());
                            userEmail = profile.getEmail();
                            if (userEmail.indexOf('@netelixir.com') >= 0) {
                                    sessionStorage.setItem("userOAuthEmail", userEmail);
                                    isGmailLoginSuccess = true;
                                    $("#googleLoginPopupModal").modal('hide');
                                    gmailLoginSuccess();
                                    $("#email").val(userEmail);
                                    var id_token = googleUser.getAuthResponse().id_token;
                                    sessionStorage.setItem("GAuthToken", id_token);
                            } else {
                                isGmailLoginSuccess = false;
                                sessionStorage.setItem("ADSPYError", "Please sign in with @netelixir.com email to use Pre-Sales Tools");
                                signOutfromGoogle();
                            }
                    };
            } else if (gauth !== null) {
                    $("#googleLoginPopupModal").modal('hide');
                    var userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
                    if (userOAuthEmail !== null) {
                    $("#email").val(userOAuthEmail);
                    }
                    isGmailLoginSuccess = true;
            }
            function gmailLoginSuccess() {
                    $("#loginBodyWrapper").show();
                    $("#errorBodyWrapper").hide();
            }
            function signOutfromGoogle() {
                    var auth2 = gapi.auth2.getAuthInstance();
                    auth2.signOut().then(function () {
                    console.log('User signed out.');
                    sessionStorage.setItem("GAuthToken", '');
                    $('<iframe style="display: none" src="https://accounts.google.com/Logout"></iframe>').appendTo(document.body);
        //                    location.reload(true);
                    });
                    var errorMessage = sessionStorage.getItem('ADSPYError');
                    if (errorMessage !== null) {
                        $("#loginErrorMessage").empty();
                        $("#errorBodyWrapper").show();
                        $("#loginErrorMessage").html(errorMessage);
                    }
                    setTimeout(function () {
                        sessionStorage.setItem("ADSPYError", '');
                    }, 10);
            }
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    </body>
</html>

