<%-- 
    Document   : AdSpy
    Created on : 11 Apr, 2018, 4:27:58 PM
    Author     : Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name='google-signin-client_id'  content='587399183753-k9c1ldj0kg3k21kiesjedpo78o7jf7k5.apps.googleusercontent.com'/>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <title>Ad Spy - V2</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/adSpy.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var downloadReportType = "";
            var userEmail = "";
            var isGmailLoginSuccess = false;
            $(document).on('change', ':file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready(function () {
                $('#fileUploadWrapper').click(function () {
                    var manuallyContent = $.trim($("#queryKeywords").val());
                    manuallyContent = manuallyContent.replace(/&nbsp;/gi, " ");
                    manuallyContent = manuallyContent.replace(/<br>/gi, " ");
                    manuallyContent = manuallyContent.trim();
//                    alert("manuallyContent: "+manuallyContent.length);
                    if (manuallyContent !== "") {
                        var response = confirm('Manually entered company website URL (S) needs to be cleared before uploading the file.\nDo you want to clear now ?');
                        if (response === true) {
                            $('#queryKeywords').val("");
                            $("#inputUrlError").empty();
                        } else {
                            if (($.trim($("#queryKeywords").val()) && ($.trim($("#filePath").val()) && 
                                ($("#keywordsFile")[0].files[0] !== undefined 
                                && $("#keywordsFile")[0].files[0] !== '' 
                                && $("#keywordsFile")[0].files[0] !== null)))) {
                                $("#keywordsFile").val('');
                                $("#filePath").val('');
                                $("#keywordsFile, #filePath").empty();
                            }
                            return false;
                        }
                    }
                });
                $('#getResult').click(function () {
                    if (isGmailLoginSuccess) {
                        processAdSpy();
                    } else {
                        gmailLoginSuccess();
                        $("#googleLoginPopupModal").modal('show');
                        var errorMessage = sessionStorage.getItem('ADSPYError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                        $("#loginErrorMessage").empty();
                        $("#errorBodyWrapper").hide();
                    }
                });
                $('#resetAdSpy').click(function () {
                    $("#queryKeywords").val('');
                    $("#keywordsFile").val('');
                    $("#queryKeywords, #keywordsFile, #filePath").empty();
                    var form = $("#addSpy");
                    form.attr('method', 'POST');
                    form.attr('action', "/ad-spy-v2.html");
                    form.submit();
                });
                $(':file').on('fileselect', function (event, numFiles, label) {
                    var input = $(".file-upload-wrapper").find('input[type=text]'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;
                    if (input.length) {
                        input.val(log);
                    }
                });
                $('#clearFilePath').click(function () {
                    $("#keywordsFile").val('');
                    $("#filePath").val('');
                    $("#keywordsFile, #filePath").empty();
                });
            <c:if test="${success}">
                var input = $(".file-upload-wrapper").find('input[type=text]');
                input.val('${fileName}');
            </c:if>
                $("#downloadAdSpyReport").bind('click', function (e) {
                    downloadAdSpyReport();
                });
                $("#mailAdSpy").bind('click', function () {
                    $('#mail-sent-status').empty();
                    var emailid = $.trim($("#email").val());
                    var status = true;
                    var statusMsg = "";
                    if (emailid === "") {
                        statusMsg = "Please enter your email id";
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        statusMsg = "Please enter a valid email address.";
                        status = false;
                    }
                    $('#mail-sent-status').show();
                    if (!status) {
                        $("#email").focus();
                        $("#mail-sent-status").html(statusMsg);
                        $('#mail-sent-status').css("color", "red");
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/ad-spy-v2.html?download=sendmail&email=" + emailid,
                            processData: true, data: {},
                            dataType: "json",
                            success: function (data) {
                            }, complete: function () {
                                $('#mail-sent-status').empty();
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                            }
                        });
                    }
                });
            });
            function getLines(id) {
                var queryURLSCount = 0;
                var lines = $.trim($('#' + id).val()).split("\n");
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].length > 0)
                        queryURLSCount++;
                }
                return queryURLSCount;
            }
            function processAdSpy() {
                var isError = false;
                $("#inputUrlError").empty();
                if (!($.trim($("#queryKeywords").val()) || ($("#keywordsFile")[0].files[0] !== undefined && $("#keywordsFile")[0].files[0] !== '' && $("#keywordsFile")[0].files[0] !== null))) {
                    $("#inputUrlError").html("Please enter company website URL (S) or upload file to analyze.");
                    $("#queryKeywords").focus();
                    isError = true;
                    return;
                }
                if (($.trim($("#queryKeywords").val()) && ($("#keywordsFile")[0].files[0] !== undefined && $("#keywordsFile")[0].files[0] !== '' && $("#keywordsFile")[0].files[0] !== null))) {
                    $("#inputUrlError").html("Please choose either manual list or upload file to analyze.");
                    $("#queryKeywords").focus();
                    isError = true;
                    return;
                }
                if (!isError) {
                    var queryURLS = $.trim($("#queryKeywords").val());
                    if (queryURLS !== "") {
                        var urlsCount = getLines('queryKeywords');
                        if (urlsCount > 50) {
                            var response = confirm('Maximum of 50 company website URL (S) can be analyzed at a time. Do you want to continue.');
                            if (response === true) {
                                $("#queryKeywords").focus();
                            } else {
                                $("#inputUrlError").html("Please enter maximum up to 50 company website URL(S).");
                                isError = true;
                            }
                        }
                    }
                }
                if (!isError) {
                    $("#getResult").button('loading');
                    $("#toolInfo").show();
                    var form = $("#addSpy");
                    form.attr('action', "/ad-spy-v2.html?request=analyze");
                    form.submit();
                }
            }
            function downloadAdSpyReport() {
                var form = $("#addSpy");
                form.attr('method', "POST");
                form.attr('action', "/ad-spy-v2.html?download=download");
                form.submit();
            }
        </script>
        <style>
            #resetAdSpy {
                background: #fff;
                height: 45px;
                vertical-align: middle;
                color: #EB5757 !important;
                border: 1px solid #EB5757 !important;
                text-align: center;
                border-radius: 4px !important;
            }
            #adSpyAnalysis_filter{
                margin-right: 3.5% !important;
            }
        </style>
    </head>

    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container-fluid">
            <nav style="text-align: right;padding: 3px;font-size: 12px;color: #828282 !important">
                <a class="breadcrumb-item" href="/">Home ></a>
                <span itemprop="name"><span class="breadcrumb-item">Ad Spy - V2</span></span>
            </nav>
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="col-xs-2 col-sm-1 col-md-1" style="padding: 0px">
                        <span class="badge lxrsrt-toolicon pull-left" style="background-color: #FFF"> <img src="/resources/images/tool-icons/ADS.png" alt="ADS.png"></span>
                    </div>
                    <div class="col-xs-10 col-sm-11 col-md-11">
                        <h2 id="toolName" style="font-family: ProximaNova-Bold;">
                            Ad Spy - V2
                        </h2>
                        <span class="label label-default" style="padding: 5px;background-color: #F58120;">
                            5.0
                            <span class="glyphicon glyphicon-star"></span> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="addSpy" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12 mobile-padding-none">
                        <div class="col-xs-12">
                            <blockquote class="blockquote">
                                <p>Instantly uncover website's Visits, Ad Presence and Est. Monthly Adwords Budget details through SimilarWeb and SpyFu Interface's .</p>
                                <p>This tool checks the company website URL entered and returns the metrics of the website using the metrics extracted from SimilarWeb and SpyFu.</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 20px !important;"></div>
                    <!--<div class="col-xs-12">-->
                    <div class="col-xs-12 tool-input-wrapper">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Enter the list of company website URL (S) :
                                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                           title="Enter company website URL (S) in the text box below. Enter one company website URL per line (example.com).">
                                            <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                        </a>
                                    </label>
                                    <springForm:textarea path="queryKeywords" id="queryKeywords" cssClass="form-control col-xs-9" rows="5" placeHolder="Enter the company website URL (S) one per line (example.com)." style="resize:none;"/>
                                </div>
                                <p id="inputUrlError" class="error"></p>
                            </div>
                            <div style="height:10px;clear: both;" class="visible-xs visible-sm"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="keywordsFile" class="uploadLabel empty-left-padding"><span style="color:#4CAF50;font-family: 'ProximaNova-Bold';">(OR)</span> Upload your company website URL (S) list (xls format) : </label>
                                    <div class="col-xs-12 col-lg-12 empty-left-padding ">
                                        <!--                                    <div class="col-xs-10 empty-left-padding file-upload-wrapper">
                                                                                <input type="text" id="filePath" class="form-control" readonly style="height: 3.5rem;">
                                                                            </div>-->
                                        <div class="col-xs-10 empty-left-padding file-upload-wrapper">
                                            <div class="input-group">
                                                <input type="text" readonly="readonly" id="filePath" class="form-control" placeholder="Uploaded file name" style="height: 3.5rem;">
                                                <span id="clearFilePath" style="cursor: pointer;" class="input-group-addon"><i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 empty-left-padding" id="fileUploadWrapper" style="padding-right: 0;">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary file-action-wrapper" style="width: 100%;">
                                                    Browse<input type="file" name="keywordsFile" id="keywordsFile" style="display: none;" multiple>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfixdiv" style="height: 10px !important"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <div class="col-xs-12 tool-input-wrapper">
                                <div style="height:10px;clear: both;"></div>
                                <div class="col-xs-12">
                                    <div class="col-xs-4 visible-md visible-lg"></div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetAdSpy">
                                                <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                            </button>
                                        </div>
                                    </div>
                                    <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                                &nbsp;ANALYZE&nbsp;
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 visible-md visible-lg"></div>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <p id="toolInfo" class="lxrm-bold" style="display: none;margin-top:2%;font-size: 15px !important;">Analyinzg the website URL (S), these may take a few minutes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </springForm:form>
            <div class="clearfixdiv"></div>
            <div class="row">
                <div class="col-xs-12">
                    <c:choose>
                        <c:when test="${success}">
                            <div class="col-xs-12 panel-default">
                                <div class="panel-heading lxr-panel-heading row" style="padding-top: 2%;">
                                    <div class="col-xs-6"><span>Ad Presence Analysis</span></div>
                                    <div class="col-xs-6 text-right">Date Range for the data: &nbsp;<span>${startDate} - ${endDate}</span></div>
                                </div> 
                                <div class="panel-body lxr-panel-body">
                                    <div class="col-xs-12 table-parent-div text-nowrap">
                                        <table class="table" id="adSpyV2Analysis">
                                            <thead style="background: #22A6B3 !important;">
                                                <tr>
                                                    <th class="text-center">S.No.</th>
                                                    <th class="">Company</th>
                                                    <th class="text-center">Ad Presence</th>
                                                    <th class="text-center">Total Visits (12 Months)</th>
                                                    <th class="text-center">Average Monthly Visits</th>
                                                    <th class="text-center">Paid Traffic</th>
                                                    <th class="text-center">Organic Traffic</th>
                                                    <th class="text-center">Paid Visits</th>
                                                    <th class="text-center">Organic Visits</th>
                                                    <th class="text-center">Est. Monthly Adwords Budget</th>
                                                    <th class="text-center">Approx. Monthly Adwords Budget</th>
                                                </tr>
                                            </thead>
                                            <tbody style="word-break: break-all;">
                                                <c:if test="${resultAdSpyList == null || empty resultAdSpyList}">
                                                    <!--<tr><td class="text-center lxrm-bold col-xs-12" colspan="11">No Records Found</td></tr>-->
                                                    <tr><td class="text-center lxrm-bold" colspan="11">No Records Found</td></tr>
                                                </c:if>
                                                <c:set var="count" scope = "page" value = "1"/>
                                                <c:forEach var="adSpyObj" items="${resultAdSpyList}" varStatus="urlCount">
                                                    <tr>
                                                        <td class="text-right">${count}</td>
                                                        <td class="text-left">${adSpyObj.companyName}</td>
                                                        <td class="text-center">${adSpyObj.addPresence ? 'YES':'NO'}</td>
                                                        <td class="text-right">
                                                            <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.trafficTotalVisits}" />
                                                        </td>
                                                        <td class="text-right">
                                                            <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.trafficMonthlyVisits}" />
                                                        </td>
                                                        <td class="text-right">
                                                            <c:choose>
                                                                <c:when test="${adSpyObj.paidTraffic > 0 && adSpyObj.paidTraffic < 1}">
                                                                    < 1 %
                                                                </c:when>
                                                                <c:when test="${adSpyObj.paidTraffic > 1}">
                                                                    <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.paidTraffic}" /> %
                                                                </c:when>
                                                                <c:otherwise>
                                                                    ${adSpyObj.paidTraffic} %
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td class="text-right">
                                                            <c:choose>
                                                                <c:when test="${adSpyObj.organicTraffic > 0 && adSpyObj.organicTraffic < 1}"> 
                                                                    < 1 %
                                                                </c:when>
                                                                <c:when test="${adSpyObj.organicTraffic > 1}">
                                                                    <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.organicTraffic}" /> %
                                                                </c:when>
                                                                <c:otherwise>
                                                                    ${adSpyObj.organicTraffic} %
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td class="text-right">
                                                            <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.paidVisits}" />
                                                        </td>
                                                        <td class="text-right">
                                                            <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.organicVisits}" />
                                                        </td>
                                                        <td class="text-right">
                                                            $ <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.monthlyAdwordsBudget}" />
                                                        </td>
                                                        <td class="text-right">
                                                            $ <fmt:formatNumber maxFractionDigits="2" value="${adSpyObj.approxMonthlyAdwordsBudget}" />
                                                        </td>
                                                    </tr>
                                                    <c:set var="count" value = "${count+1}" scope = "page"/>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfixdiv" style="height: 25px !important;"></div>
                                </div>
                                <div class="panel-footer lxr-panel-tool-footer" style="height: 6.5rem;">
                                    <c:if test="${resultAdSpyList !=null && not empty resultAdSpyList}">
                                        <div class="row">
                                            <div class="col-xs-3 visible-md visible-lg"></div>
                                            <div class="col-xs-12 col-sm-4 col-lg-3">
                                                <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-lg-4">
                                                <input type="text" class="form-control emailWrapper" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                                <div id="mail-sent-status" class="emptyData"></div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-lg-2" style="padding-top: 0.5%;">
                                                <div id="mailAdSpy" style="font-size: 20px;cursor: pointer;padding-left: 0;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-send sendReport"></div>
                                                <div id="downloadAdSpyReport" style="font-size: 20px;cursor: pointer;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt"></div>
                                                <div class="col-xs-12 col-sm-5 col-lg-4 visible-md visible-lg"></div>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${APILimit != null}">
                                    <div class="clearfixdiv" style="height: 185px !important;">
                                        <p class="lxrm-bold apiError d-flex-justify-center">
                                            <span class="lxr-danger" style="font-size:20px;"><i class="fa fa-times-circle"></i></span>
                                            <span class="mt" style="font-size:15px;">&nbsp;${APILimit}</span>
                                        </p>
                                    </div>
                                </c:when>
                                <c:when test="${ToolQueryLimit != null}">
                                    <div class="clearfixdiv" style="height: 185px !important;">
                                        <p class="lxrm-bold apiError d-flex-justify-center" style="font-size:15px;">
                                            <span class="lxr-danger" style="font-size:20px;"><i class="fa fa-times-circle"></i></span>
                                            <span class="mt" style="font-size:15px;">&nbsp;${ToolQueryLimit}</span>
                                        </p>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="clearfixdiv" style="height: 185px !important;"></div>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <div id="googleLoginPopupModal" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="top: 190px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title lxrm-bold">Login with NETELIXIR Email</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="padding-left: 3rem; text-align: center;" id="loginBodyWrapper" style="display:none;">
                                <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style='margin-left: 20%;'></div>
                                <div class="clearfixdiv" style="height: 13px !important;"></div>
                            </div>
                            <div class="col-xs-12" id="errorBodyWrapper" style="display:none;">
                                <div class="col-xs-12 text-center">
                                    <p id="loginErrorMessage" class="text-center" style="color: red;font-size: 14px;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv" style="height: 105px !important;"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
            var gauth = sessionStorage.getItem('GAuthToken');
            if (gauth === null) {
                gmailLoginSuccess();
                $("#googleLoginPopupModal").modal('show');
                var errorMessage = sessionStorage.getItem('ADSPYError');
                if (errorMessage !== null) {
                    $("#loginErrorMessage").empty();
                    $("#errorBodyWrapper").show();
                    $("#loginErrorMessage").html(errorMessage);
                }
                setTimeout(function () {
                    sessionStorage.setItem("ADSPYError", '');
                }, 10);
//                $('#googleLoginPopupModal').modal({
//                    backdrop: 'static',
//                    keyboard: false
//                });
                function onSignIn(googleUser) {
                    // Useful data for your client-side scripts:
                    var profile = googleUser.getBasicProfile();
                    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                    console.log('Full Name: ' + profile.getName());
                    console.log('Given Name: ' + profile.getGivenName());
                    console.log('Family Name: ' + profile.getFamilyName());
                    console.log("Image URL: " + profile.getImageUrl());
                    console.log("Email: " + profile.getEmail());
                    userEmail = profile.getEmail();
                    if (userEmail.indexOf('@netelixir.com') >= 0) {
                        var id_token = googleUser.getAuthResponse().id_token;
//                        console.log("ID Token: " + id_token);
                        sessionStorage.setItem("GAuthToken", id_token);
                        sessionStorage.setItem("userOAuthEmail", userEmail);
//                                location.reload(true);
                        isGmailLoginSuccess = true;
                        $("#googleLoginPopupModal").modal('hide');
                        gmailLoginSuccess();
                        $("#email").val(userEmail);
                    } else {
                        isGmailLoginSuccess = false;
                        sessionStorage.setItem("ADSPYError", "Please sign in with @netelixir.com email to use Pre-Sales Tools");
                        signOutfromGoogle();
                    }
                }
                ;
            } else if (gauth !== null) {
                $("#googleLoginPopupModal").modal('hide');
                var userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
                if (userOAuthEmail !== null) {
                    $("#email").val(userOAuthEmail);
                }
                isGmailLoginSuccess = true;
            }
            $(document).ready(function () {
            <c:if test="${success}">
                $('#adSpyV2Analysis').DataTable({
                    scrollX: true,
                    fixedHeader: true,
                    orderCellsTop: true,
                    "lengthMenu": [[10, 25, -1], [10, 25, "All"]],
                });
            </c:if>
            });
            function gmailLoginSuccess() {
                $("#loginBodyWrapper").show();
                $("#errorBodyWrapper").hide();
            }
            function signOutfromGoogle() {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                    console.log('User signed out.');
                    sessionStorage.setItem("GAuthToken", '');
                    $('<iframe style="display: none" src="https://accounts.google.com/Logout"></iframe>').appendTo(document.body);
//                    location.reload(true);
                });
                var errorMessage = sessionStorage.getItem('ADSPYError');
                if (errorMessage !== null) {
                    $("#loginErrorMessage").empty();
                    $("#errorBodyWrapper").show();
                    $("#loginErrorMessage").html(errorMessage);
                }
                setTimeout(function () {
                    sessionStorage.setItem("ADSPYError", '');
                }, 10);
            }
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    </body>
    <tbody style="word-break: break-all;">
                                        <tr ng-repeat="adSpyV2 in ctrl.adSpyProcessedList track by $index">
                                            <td class="text-right">{{$index + 1}}.</td>
                                            <td class="text-left">{{adSpyV2.companyName}}</td>
                                            <td class="text-center">{{adSpyV2.addPresence ? 'YES' :'NO'}}</td>
                                            <td class="text-right">{{adSpyV2.trafficTotalVisits}}</td>
                                            <td class="text-right">{{adSpyV2.trafficMonthlyVisits}}></td>
                                            <td class="text-center">{{adSpyV2.paidTraffic}}</td>
                                            <td class="text-center">{{adSpyV2.organicTraffic}}</td>
                                            <td class="text-right">{{adSpyV2.paidVisits}}</td>
                                            <td class="text-right">{{adSpyV2.organicVisits}}</td>
                                            <td class="text-right">{{adSpyV2.monthlyAdwordsBudget}}</td>
                                            <td class="text-right">{{adSpyV2.approxMonthlyAdwordsBudget}}</td>
<!--                                            <td class="text-right">{{$index + 1}}.</td>
                                            <td class="text-left">{{adSpyV2.companyName}}</td>
                                            <td class="text-center">{{adSpyV2.addPresence ? 'YES' :'NO'}}</td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.trafficTotalVisits}" /></td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.trafficMonthlyVisits}" /></td>
                                            <td class="text-center">{{((adSpyV2.paidTraffic != 0) ? ((adSpyV2.paidTraffic > 0 && adSpyV2.paidTraffic < 1)?'<1%':<fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.paidTraffic}" />) :adSpyV2.paidTraffic)}}</td>
                                            <td class="text-center">{{((adSpyV2.organicTraffic != 0) ? ((adSpyV2.organicTraffic > 0 && adSpyV2.organicTraffic < 1)?'<1%':<fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.organicTraffic}" />) : adSpyV2.organicTraffic)}}</td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.paidVisits}" /></td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.organicVisits}" /></td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.monthlyAdwordsBudget}" /></td>
                                            <td class="text-right"><fmt:formatNumber maxFractionDigits="2" value="${adSpyV2.approxMonthlyAdwordsBudget}" /></td>-->
                                        </tr>
                                    </tbody>
                                    
                                    //                    var userEmail = "";
//                    if (sessionStorage.getItem("userOAuthEmail") !== null) {
//                        userEmail = sessionStorage.getItem('userOAuthEmail');
//                    }
</html>


