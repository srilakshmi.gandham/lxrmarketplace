<%-- 
    Document   : AdSpy
    Created on : 11 Apr, 2018, 4:27:58 PM
    Author     : Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ad Spy</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/adSpy.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).on('change', ':file', function () {
                if (($.trim($("#queryKeywords").val()) !== "")) {
                    var response = confirm('Manually entered company names needs to be cleared before uploading the file.\nDo you want to clear now ?');
                    if (response === true) {
                        $('#queryKeywords').val("");
                    } else {
                        $("#keywordsFile").val('');
                        $("#keywordsFile").empty();
                        return false;
                    }
                }
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready(function () {
                $('#getResult').click(function () {
                    processAdSpy();
                });
                $('#resetAdSpy').click(function () {
                    $("#queryKeywords").val('');
                    $("#keywordsFile").val('');
                    $("#queryKeywords").empty();
                    $("#keywordsFile").empty();

                    var form = $("#addSpy");
                    form.attr('method', 'POST');
                    form.attr('action', "/ad-spy.html");
                    form.submit();
                });
                $(':file').on('fileselect', function (event, numFiles, label) {
                    if (($.trim($("#queryKeywords").val()) !== "")) {
                        var response = confirm('Manually entered company(s) needs to be cleared before uploading the file.\nDo you want to clear now ?');
                        if (response === true) {
                            $('#queryKeywords').val("");
                        } else {
                            $("#keywordsFile").val('');
                            $("#keywordsFile").empty();
                            return false;
                        }
                    }
                    var input = $(".file-upload-wrapper").find('input[type=text]'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;
                    if (input.length) {
                        input.val(log);
                    }
                });
            <c:if test="${success}">
                var input = $(".file-upload-wrapper").find('input[type=text]');
                    input.val('${fileName}');
            </c:if>
                $("#downloadAdSpyReport").bind('click', function (e) {
                    downloadAdSpyReport();
                });
                $("#mailAdSpy").bind('click', function () {
                    $('#mail-sent-status').empty();
                    var emailid = $.trim($("#email").val());
                    var status = true;
                    var statusMsg = "";
                    if (emailid === "") {
                        statusMsg = "Please enter your email id";
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        statusMsg = "Please enter a valid email address.";
                        status = false;
                    }
                    $('#mail-sent-status').show();
                    if (!status) {
                        $("#email").focus();
                        $("#mail-sent-status").html(statusMsg);
                        $('#mail-sent-status').css("color", "red");
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/ad-spy.html?download=sendmail&email=" + emailid,
                            processData: true, data: {},
                            dataType: "json",
                            success: function (data) {
                            }, complete: function () {
                                $('#mail-sent-status').empty();
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                            }
                        });
                    }
                });
            });
            function processAdSpy() {
                var isError = false;
                $("#inputUrlError").empty();
                if (!($.trim($("#queryKeywords").val()) || ($("#keywordsFile")[0].files[0] !== undefined && $("#keywordsFile")[0].files[0] !== '' && $("#keywordsFile")[0].files[0] !== null))) {
                    $("#inputUrlError").html("Please enter company names or upload file to analyze.");
                    $("#queryKeywords").focus();
                    isError = true;
                    return;
                }
                if (($.trim($("#queryKeywords").val()) && ($("#keywordsFile")[0].files[0] !== undefined && $("#keywordsFile")[0].files[0] !== '' && $("#keywordsFile")[0].files[0] !== null))) {
                    $("#inputUrlError").html("Please choose only one manaually list or upload file to analyze.");
                    $("#queryKeywords").focus();
                    isError = true;
                    return;
                }
                if (!isError) {
                    $("#getResult").button('loading');
                    var form = $("#addSpy");
                    form.attr('action', "/ad-spy.html?request=analyze");
                    form.submit();
                }
            }
            function downloadAdSpyReport() {
                var form = $("#addSpy");
                form.attr('method', "POST");
                form.attr('action', "/ad-spy.html?download=download");
                form.submit();
            }
        </script>
        <style>
            #resetAdSpy {
                background: #fff;
                height: 45px;
                vertical-align: middle;
                color: #EB5757 !important;
                border: 1px solid #EB5757 !important;
                text-align: center;
                border-radius: 4px !important;
            }
            #adSpyAnalysis_filter{
                margin-right: 3.5% !important;
            }
        </style>
    </head>

    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container-fluid">
            <nav style="text-align: right;padding: 3px;font-size: 12px;color: #828282 !important">
                <a class="breadcrumb-item" href="/">Home ></a>
                <span itemprop="name"><span class="breadcrumb-item">Ad Spy</span></span>
            </nav>
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="col-xs-2 col-sm-1 col-md-1" style="padding: 0px">
                        <span class="badge lxrsrt-toolicon pull-left" style="background-color: #FFF"> <img src="/resources/images/tool-icons/ADS.png" alt="ADS.png"></span>
                    </div>
                    <div class="col-xs-10 col-sm-11 col-md-11">
                        <h2 id="toolName" style="font-family: ProximaNova-Bold;">
                            Ad Spy
                        </h2>
                        <span class="label label-default" style="padding: 5px;background-color: #F58120;">
                            5.0
                            <span class="glyphicon glyphicon-star"></span> 
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="addSpy" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12 mobile-padding-none">
                        <div class="col-xs-12">
                            <blockquote class="blockquote">
                                <p>Instantly Uncover Any Company's Ad Presence.</p>
                                <p>This tool checks the company name entered has any Ad Presence on the Google Search Engine.</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 20px !important;"></div>
                    <!--<div class="col-xs-12">-->
                    <div class="col-xs-12 tool-input-wrapper">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Enter the list of company names:
                                    <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                       title="Enter company names in the text box below. Enter one company name per line.">
                                        <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                    </a>
                                </label>
                                <springForm:textarea path="queryKeywords" id="queryKeywords" cssClass="form-control col-xs-9" rows="5" placeHolder="Enter the company names one per line." style="resize:none;"/>
                            </div>
                            <p id="inputUrlError" class="error"></p>
                        </div>
                        <div style="height:10px;clear: both;" class="visible-xs visible-sm"></div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="keywordsFile" class="uploadLabel empty-left-padding"><span style="color:#4CAF50;font-family: 'ProximaNova-Bold';">(OR)</span> Upload your company name list (xls format): </label>
                                <div class="col-xs-12 col-lg-12 empty-left-padding ">
                                    <div class="col-xs-10 empty-left-padding file-upload-wrapper">
                                        <input type="text" class="form-control" readonly style="height: 3.5rem;">
                                    </div>
                                    <div class="col-xs-2 empty-left-padding" style="padding-right: 0;">
                                        <label class="input-group-btn">
                                            <span class="btn btn-primary file-action-wrapper" style="width: 100%;">
                                                Browse<input type="file" name="keywordsFile" id="keywordsFile" style="display: none;" multiple>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfixdiv" style="height: 10px !important"></div>
                            <div class="form-group">
                                <label class="control-label col-xs-12" style="padding-left: 0;font-size: 15px;">Geo Location to be checked:
                                    <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                       title="The existence of Ads for the given company names is checked in this selected Geographical Location.">
                                        <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                    </a>
                                </label>
                                <div class="col-xs-12 col-lg-12" style="padding-left: 0">
                                    <div class="input-group date lxr-date-picker-24hours">
                                        <span class="input-group-addon" style="background: #fff;">
                                            <span class="glyphicon glyphicon-map-marker" style="color:#2F80ED;"></span>
                                        </span>
                                        <springForm:select path="geoLocation" class="form-control col-xs-12 selectWrapper">
                                            <springForm:options items="${geoLocations}" class="form-control"/>
                                        </springForm:select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</div>-->
                    </div>
                    <div class="clearfixdiv"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <div class="col-xs-12 tool-input-wrapper">
                                <div style="height:10px;clear: both;"></div>
                                <div class="col-xs-12">
                                    <div class="col-xs-4 visible-md visible-lg"></div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetAdSpy">
                                                <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                            </button>
                                        </div>
                                    </div>
                                    <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                                &nbsp;ANALYZE&nbsp;
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 visible-md visible-lg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </springForm:form>
            <div class="clearfixdiv"></div>
            <div class="row">
                <div class="col-xs-12">
                    <c:choose>
                        <c:when test="${success}">
                            <div class="col-xs-12 panel-default">
                                <div class="panel-heading lxr-panel-heading" style="padding-top: 2%;">
                                    <p style="font-size: 16px;">Ad Presence Analysis</p>
                                </div>
                                <div class="panel-body lxr-panel-body">
                                    <div class="col-xs-12 table-parent-div">
                                        <table class="table" id="adSpyAnalysis">
                                            <thead style="background: #22A6B3 !important;">
                                                <tr>
                                                    <th class="col-xs-1">S.No</th>
                                                    <th class="col-xs-3">Company</th>
                                                    <th class="col-xs-2">Ad-Presence</th>
                                                    <th class="col-xs-6">Company URL</th>
                                                </tr>
                                            </thead>
                                            <tbody style="word-break: break-all;">
                                                <c:if test="${companyAdSpyList.size() == 0}">
                                                    <tr><td class="text-center lxrm-bold col-xs-12" colspan="4">No Records Found</td></tr>
                                                </c:if>
                                                <c:set var="count" scope = "page" value = "1"/>
                                                <c:forEach var="adSpyObj" items="${companyAdSpyList}" varStatus="urlCount">
                                                    <tr>
                                                        <td class="" style="width: 8%;">${count}</td>
                                                        <td style="width: 30%;">${adSpyObj.companyName}</td>
                                                        <td class="" style="width: 15%;">${adSpyObj.addPresence ? 'YES':'NO'}</td>
                                                        <td class="${(adSpyObj.url ne null || not empty adSpyObj.url)? "text-left":"text-center"}" style="width: 47%;">${(adSpyObj.url ne null || not empty adSpyObj.url)? adSpyObj.url:"-"}</td>
                                                    </tr>
                                                    <c:set var="count" value = "${count+1}" scope = "page"/>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfixdiv" style="height: 25px !important;"></div>
                                </div>
                                <div class="panel-footer lxr-panel-tool-footer" style="height: 6.5rem;">
                                    <c:if test="${companyAdSpyList !=null && not empty companyAdSpyList}">
                                        <div class="row">
                                            <div class="col-xs-3 visible-md visible-lg"></div>
                                            <div class="col-xs-12 col-sm-4 col-lg-3">
                                                <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-lg-4">
                                                <input type="text" class="form-control emailWrapper" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                                <div id="mail-sent-status" class="emptyData"></div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-lg-2" style="padding-top: 0.5%;">
                                                <div id="mailAdSpy" style="font-size: 20px;cursor: pointer;padding-left: 0;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-send sendReport"></div>
                                                <div id="downloadAdSpyReport" style="font-size: 20px;cursor: pointer;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt"></div>
                                                <div class="col-xs-12 col-sm-5 col-lg-4 visible-md visible-lg"></div>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="clearfixdiv" style="height: 185px !important;"></div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script>
            $(document).ready(function () {
            <c:if test="${success}">
                $('#adSpyAnalysis').DataTable();
//                $('#adSpyAnalysis').dataTable({
//                    "dom": ' <"search"f><"top"l>rt<"bottom"ip><"clear">'
//                    });
            </c:if>
            });
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    </body>

</html>
