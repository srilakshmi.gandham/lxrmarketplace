
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Map,java.util.HashMap,java.util.List,java.util.ArrayList"%>
<%@ page import="lxr.marketplace.util.Tool"%>
<%
    Map<Integer, List<Integer>> relatedTools = new HashMap<>();
    ServletContext toolsContext = request.getServletContext();
    if (toolsContext.getAttribute("relatedTools") != null) {
        relatedTools = (Map<Integer, List<Integer>>) toolsContext.getAttribute("relatedTools");
    }
    List<Integer> relatedToolIds = new ArrayList<>();
    relatedToolIds = relatedTools.get(toolDBId);
    List<Tool> relatedToolsList = new ArrayList<>();
    if (relatedToolIds != null) {
        for (Integer relatedToolId : relatedToolIds) {
            /* totalTools varaible is configured in header.jsp */
            relatedToolsList.add(totalTools.get(relatedToolId));
        }
    }
    pageContext.setAttribute("relatedToolsList", relatedToolsList);
%>
<script type="text/javascript">
    $(document).ready(function () {
        getRelatedTools(4);
    });
    function getRelatedTools(limit) {
        var toolsLimit = 1;
        var remainingToolsCount = 0;
        var relatedTools = "";
    <c:forEach items="${relatedToolsList}" var="relatedTool">
        if (limit === 0 || (limit > 0 && toolsLimit <= limit)) {
            relatedTools += '<div  class="col-xs-12 col-sm-6 col-lg-3 lxr-tooladviser form-group">'
                    + '<div class="lxr-tool-showcase" style="border: 1px ${relatedTool.toolIconColor} solid;background: white;border-radius: 5px;">'
                    + '<a href="${relatedTool.toolLink}">'
                    + '<div class="col-item lxr-items"  style="background-color:${relatedTool.toolIconColor};">'
                    + '<div class="post-img-content">'
                    + '<span class="post-title">'
                    + '<span class="pull-left" style="padding: 8px;"><img src="../../resources/images/tool-icons/${relatedTool.imgPath}.png"></span>';
        <c:if test="${relatedTool.categoryid ne 6}">
            relatedTools += '<div class="pull-right" ><span style="padding: 8px 10px 0px 0px;" class="pull-right"><img src="../../resources/images/user.png"></span>'
            if (${relatedTool.toolUserInfo.toolUsageCount} >= 1000) {
                relatedTools += '<span style="font-size: 30px;" class="lead">' + kFormatter(${relatedTool.toolUserInfo.toolUsageCount}) + '</span><span class="sma">k&nbsp;&nbsp;</span></div>';
            } else {
                relatedTools += '<span style="font-size: 30px;" class="lead">${relatedTool.toolUserInfo.toolUsageCount}</span><span class="sma">&nbsp;&nbsp;</span></div>';
            }
        </c:if>
            var toolName = "${relatedTool.toolName}";
            if (toolName.length > 28) {
                toolName = toolName.substring(0, 26) + "..";
            }
            relatedTools += '</span>'
                    + '</div>'
                    + '<div class="price col-lg-12" style="padding-left: 8px;">'
                    + '<h4 class="align-text-bottom lxr-title" title="${relatedTool.toolName}">' + toolName + '</h4>'
                    + '<div id="stars" class="starrr align-text-bottom pull-left">${relatedTool.toolUserInfo.avgRatingGraph}</div>'
                    + '<span class="pull-left">&nbsp;&nbsp;(${relatedTool.toolUserInfo.toolRatedUsersCount})</span>'
                    + '</div>'
                    + '</div>'
                    + '<div class="info">'
                    + '<ul class="fa-ul" style="height: 96px">'
                    + "<li ><i class='fa-li fa fa-check' style='color:${relatedTool.toolIconColor}'></i>${relatedTool.tagline}</li>"
                    + "<li><i class='fa-li fa fa-check' style='color:${relatedTool.toolIconColor}'></i>${relatedTool.toolDesc}</li>"
                    + '</ul>'
                    + '<div class="lxr-clearfix">';
        <c:if test="${relatedTool.isIOSApp eq true || relatedTool.isAndroidApp eq true}">
            relatedTools += '<span>Download App</span>';
        </c:if>
        <c:if test="${relatedTool.isAndroidApp eq true}">
            relatedTools += '<a href="${relatedTool.androidAppURL}" target="_blank">'
                    + '<img src="../../resources/images/android.png"></a>';
        </c:if>
        <c:if test="${relatedTool.isIOSApp eq true}">
            relatedTools += '<a href="${relatedTool.iosAppURL}" target="_blank" >'
                    + '<img src="../../resources/images/apple-xxl.png"></a>';
        </c:if>
            relatedTools += '</div>'
                    + '</div>'
                    + '</a></div>'
                    + '</div>';
            if (limit > 0) {
                toolsLimit++;
            }
        } else {
            remainingToolsCount++;
        }
    </c:forEach>

        if (remainingToolsCount > 0) {
            relatedTools += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right"  style="clear: both;overflow: hidden;border: 0px red solid;">'
                    + "<div class='col-xs-7 col-sm-8 col-md-8 col-lg-9 pull-right' style='padding: 2%;background: #1E334E;color: #FFF;border-radius: 4px;font-size: 1.6rem;cursor:pointer;' onClick=\"getRelatedTools(0)\">"
                    + '<span id="${sectionName}-seemore" class="fa fa-chevron-circle-down"></span>'
                    + '<span class="lxrm-bold" style="padding-left: 4px;">See More Similar Tools</span>'
                    + '</div>'
                    + '</div>';
        } else if (limit === 0) {
            relatedTools += '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right"  style="clear: both;overflow: hidden;border: 0px red solid;">'
                    + "<div class='col-xs-5 col-sm-6 col-md-6 col-lg-6 pull-right' style='padding: 2%;background: #1E334E;color: #FFF;border-radius: 4px;font-size: 1.6rem;cursor:pointer;' onClick=\"getRelatedTools(4)\">"
                    + '<span id="${sectionName}-seeless" class="fa fa-chevron-circle-up"></span>'
                    + '<span class="lxrm-bold" style="padding-left: 4px;">See Less Tools</span>'
                    + '</div>'
                    + '</div>';
        }
        $("#recomendedTools").html(relatedTools);
    }
</script>
<c:if test="${fn:length(relatedToolsList) gt 0}">
    <div class="container-fluid lxrcommon-recomended">
        <blockquote class="blockquote">RECOMMENDED <span>Tools for Businesses</span></blockquote>
        <div class="row" id="recomendedTools"></div>
    </div>
</c:if>