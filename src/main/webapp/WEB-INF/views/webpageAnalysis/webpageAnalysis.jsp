
<%@page import="java.util.Iterator"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page import="lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails, lxr.marketplace.spiderview.SpiderViewResult"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Website SEO Analysis – Web Page Analyzer Tool | LXRMarketplace</title>
        <meta name="description" content="Try the Free Website Analysis Tool at LXRMarketplace. Review on-page factors & get best recommendations to improve your SEO rankings. Find out more today!.">
        <meta name="keywords" content="Website SEO Analysis, Web Page Analyzer,website analyzer tool">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/webpageAnalysis.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>

        <%
            int toolDBId = 8;
            Object userLoggedInObject = session.getAttribute("userLoggedIn");
            boolean isUserLoggedIn = false;
            if (userLoggedInObject != null) {
                isUserLoggedIn = true;
            }
            /* paidDownloadedReportDetailsList is for previous downloads, 
                if user have downloaded any report in tool.
                Only for logged in users.
             */
            if (isUserLoggedIn) {
                if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                    List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                    pageContext.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
                }
            }
            SpiderViewResult spiderViewResult = null;
            if (session.getAttribute("spiderviewresults") != null) {
                spiderViewResult = (SpiderViewResult) session.getAttribute("spiderviewresults");
                pageContext.setAttribute("spiderViewResult", spiderViewResult);
                long wordCount = spiderViewResult.getTotalWordCount();
                if (wordCount > 0) {
                    DecimalFormat df = new DecimalFormat();
                    df.getDecimalFormatSymbols().setGroupingSeparator(',');
                    df.setGroupingSize(3);
                    String totalWordCount = df.format(wordCount);
                    pageContext.setAttribute("wordCount", wordCount);
                    pageContext.setAttribute("totalWordCount", totalWordCount);
                }
                double textRatioCount = spiderViewResult.getTextRatio();
                if (textRatioCount > 0.0) {
                    pageContext.setAttribute("textRatioCount", textRatioCount);
                }
            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${getSuccess}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {

                $("#getResult").bind('click', function () {
                    resultLoginValue = true;
                    /* Login POPUP on tool limit exceeded */
                    $("#notWorkingUrl").html("");
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if ($('#url').val().trim() === "" || $('#url').val() === null || $('#url').val() === 0) {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $('#url').focus();
                        return false;
                    }

                    var $this = $(this);
                    $this.button('loading');

                    var f = $("#spiderViewTool");
                    f.attr('action', "seo-webpage-analysis-tool.html?getResult='getResult'");
                    f.submit();
                });

                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }

                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();

                //Auto download when user logged in after
                if (parentUrl.indexOf("report") !== -1) {
                    downloadFormat();
                }
//                toolLimitExceeded();
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                          && (spiderViewResult != '' && spiderViewResult ne null)}">
                $("#editImport").show();
                $("#url").addClass("gray");
//                $('html, body').animate({scrollTop: $('#result-div').offset().top - 50}, 'slow');
            </c:if>


                $('#url').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                $('#editImport, #url').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#url").removeClass("gray");
                });
            });
            // Report downloading...
            function downloadFormat() {
                if (userLoggedIn) {
                    var f = $("#spiderViewTool");
                    f.attr('method', "post");
                    f.attr('action', "/seo-webpage-analysis-tool.html?download='SWAreport'");
                    f.submit();
                } else {
                    reportDownload = 1;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            // Previous downloaded file downloading...
            function giveDownloadReport(fileName) {
                var f = $("#spiderViewTool");
                f.attr('method', "POST");
                f.attr('action', "seo-webpage-analysis-tool.html?prvDownloadFile=" + fileName);
                f.submit();
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
                var url = $('#url').val();
                $.ajax({
                    type: "POST",
                    url: "/seo-webpage-analysis-tool.html?ate='wepageAnalysisATE'&domain=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                $('#url').val(website);
                $("#getResult").click();
                return false;
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }

        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="spiderViewTool" method="POST">
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter a web page URL" path="url" cssClass="form-control input-lg"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="notWorkingUrl" class="lxr-danger text-left" >${notWorkingUrl}</span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12" id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </div>
            </springForm:form>

            <div class="clearfixdiv"></div>
        </div>
        <!--Recently Downloaded Reports:Ends -->
        <!--   Success Page -->
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                      && (spiderViewResult != '' && spiderViewResult ne null)}">
              <div class="container-fluid" id="result-div">
                  <div class="row lxrtool-tabs">
                      <div class="tabs">
                          <div class="tab">
                              <button class="tab-toggle active lxrtool-tabs-heading">
                                  <span>ON PAGE FACTORS</span>
                                  <p>On-page optimization is the first part to make website crawler friendly 
                                      whether it is connected to code optimization or meta titles, 
                                      meta descriptions, alt tags, page content etc.
                                  </p>
                              </button>
                          </div>
                          <div class="content active">
                              <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <span class="panel-title">ON PAGE FACTORS</span>
                                          <p id="issue-text-1">
                                              On-page SEO is the process of optimizing each and every web page of your site in order to rank higher 
                                              in the Search Engine Results Pages (SERPS), 
                                              follow the <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS</strong> to improve on page factors.
                                          </p>
                                      </div>
                                      <div class="panel-body lxr-panel-body">
                                          <div id="result-content-1">
                                              <!-- Title Tag Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Page Title</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="Search engines consider the title element to be the most important place to identify keywords and associate the page with a topic and/or set of terms. Pages which use targeted keyword(s) at the front of the title tag benefit greatly in the rankings.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <c:choose>
                                                          <c:when test="${fn:length(spiderViewResult.titleTag) gt 0}">
                                                              <c:set var="titleStopWords" value=""/>
                                                              <c:forEach items="${spiderViewResult.titleTag}" var="title" varStatus="i">
                                                                  <c:choose>
                                                                      <c:when test="${i.index == 0}">
                                                                          <p><span>"${title}"</span></p>
                                                                      </c:when>
                                                                      <c:when test="${i.index == 1}">
                                                                          <p><spring:message code="message.title" arguments="${title}" />
                                                                              <span class="lxrm-bold">
                                                                                  <c:if test="${title >= 41 && title <= 65}">
                                                                                      good.
                                                                                  </c:if>
                                                                                  <c:if test="${title <= 40 || title >= 66}">
                                                                                      not good.
                                                                                  </c:if>
                                                                              </span></p>
                                                                          </c:when>
                                                                          <c:when test="${i.index == 2}">
                                                                          <p><spring:message code="message.titlerelevancy" arguments="${title}" />
                                                                              <span class="lxrm-bold">
                                                                                  <c:choose>
                                                                                      <c:when test="${Double.parseDouble(title) == 100}">perfect.</c:when>
                                                                                      <c:when test="${Double.parseDouble(title) >= 60}">good.</c:when>
                                                                                      <c:when test="${Double.parseDouble(title) < 60}">not good.</c:when>
                                                                                  </c:choose>
                                                                              </span><p>
                                                                          </c:when>
                                                                          <c:otherwise>
                                                                              <c:choose>
                                                                                  <c:when test="${titleStopWords == ''}">
                                                                                      <c:set var="titleStopWords" value="${title}"/>
                                                                                  </c:when>
                                                                                  <c:otherwise>
                                                                                      <c:set var="titleStopWords" value="${titleStopWords}, ${title}"/>
                                                                                  </c:otherwise>
                                                                              </c:choose>
                                                                          </c:otherwise>
                                                                      </c:choose>
                                                                  </c:forEach>

                                                                  <c:if test="${fn:trim(titleStopWords) != '' && titleStopWords ne null}">
                                                                  <p><spring:message code="message.titlestopwords" /></p>
                                                                  <p><span><span class="lxrm-bold">Stop Words: </span>${titleStopWords}</span></p>
                                                              </c:if>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <p>No title tag found</p>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </div>

                                              </div>
                                              <!-- Title Tag End -->
                                              <div class="clearfixdiv"></div>
                                              <!-- Meta Tag Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Meta Description</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="Using keywords in the meta description tag gives it a much better chance to be used by the engine as the page's snippet. It also creates prominence and visibility for searchers, as the engines will make query terms in the search results snippets bold.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <c:choose>
                                                          <c:when test="${fn:length(spiderViewResult.metaDescriptionTag) gt 0}">
                                                              <c:set var="metaTagStopWords" value=""/>
                                                              <c:forEach items="${spiderViewResult.metaDescriptionTag}" var="description" varStatus="i">
                                                                  <c:choose>
                                                                      <c:when test="${i.index == 0}">
                                                                          <p><span>"${description}"</span></p>
                                                                      </c:when>
                                                                      <c:when test="${i.index == 1}">
                                                                          <p><spring:message code="message.description" arguments="${description}" />
                                                                              <span class="lxrm-bold">
                                                                                  <c:if test="${description >= 71 && description <= 160}">good.</c:if>
                                                                                  <c:if test="${description <= 70 || description >= 161}">not good.</c:if>
                                                                                  </span>
                                                                              </p>
                                                                      </c:when>
                                                                      <c:when test="${i.index == 2}">
                                                                          <p>
                                                                              <spring:message code="message.descriptionrelevancy" arguments="${description}" />
                                                                              <span class="lxrm-bold">
                                                                                  <c:choose>
                                                                                      <c:when test="${Double.parseDouble(description) == 100}">perfect.</c:when>
                                                                                      <c:when test="${Double.parseDouble(description) >= 60}">good.</c:when>
                                                                                      <c:when test="${Double.parseDouble(description) < 60}">not good.</c:when>
                                                                                  </c:choose>
                                                                              </span>
                                                                          <p>
                                                                          </c:when>
                                                                          <c:otherwise>
                                                                              <c:choose>
                                                                                  <c:when test="${metaTagStopWords == ''}">
                                                                                      <c:set var="metaTagStopWords" value="${description}"/>
                                                                                  </c:when>
                                                                                  <c:otherwise>
                                                                                      <c:set var="metaTagStopWords" value="${metaTagStopWords}, ${description}"/>
                                                                                  </c:otherwise>
                                                                              </c:choose>
                                                                          </c:otherwise>
                                                                      </c:choose>
                                                                  </c:forEach>
                                                                  <c:if test="${metaTagStopWords != '' && metaTagStopWords ne null}">
                                                                  <p><spring:message code="message.descriptionstopwords" /></p>
                                                                  <p><span><span class="lxrm-bold">Stop Words: </span>${metaTagStopWords}</span></p>
                                                              </c:if>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <p>No title tag found</p>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </div>
                                              </div>

                                              <!-- Meta Tag End -->
                                              <div class="clearfixdiv"></div>
                                              <!-- H-Tags Start-->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Heading Tags</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="Best practices for both SEO and accessibility require only a single H1 tag. The H1 is meant to be the page's headline, and thus, multiple H1s are confusing. Consider employing H2, H3 or CSS styles to achieve the same results with text visualization.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <c:if test="${fn:length(spiderViewResult.headingAndPhraseElements) gt 0}">
                                                          <table class="table lxr-table headings-table">
                                                              <tbody>
                                                                  <c:set var="moreInfoStatus" value="false"/>
                                                                  <c:forEach items="${spiderViewResult.headingAndPhraseElements}" var="elements">
                                                                      <c:set var="keyname" value=""/>
                                                                      <c:choose>
                                                                          <c:when test="${elements.key == 7}">
                                                                              <c:set var="keyname" value="Strong - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 8}">
                                                                              <c:set var="keyname" value="b - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 9}">
                                                                              <c:set var="keyname" value="em - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 10}">
                                                                              <c:set var="keyname" value="i - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 11}">
                                                                              <c:set var="keyname" value="acronym - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 12}">
                                                                              <c:set var="keyname" value="dfn - tag(s)"/>
                                                                          </c:when>
                                                                          <c:when test="${elements.key == 13}">
                                                                              <c:set var="keyname" value="abbr - tag(s)"/>
                                                                          </c:when>
                                                                          <c:otherwise>
                                                                              <c:set var="keyname" value="h${elements.key} - tag(s)"/>
                                                                          </c:otherwise>
                                                                      </c:choose>
                                                                      <%--<c:if test="${fn:length(elements.value) gt 0}">--%>
                                                                      <c:set var="hAndPText" value=""/>
                                                                      <c:forEach items="${elements.value}" var="hAndPElement">
                                                                          <c:if test="${hAndPElement.text() ne ''}">
                                                                              <c:set var="hAndPText" value="${hAndPText}<p>${hAndPElement.text()}</p>"/>
                                                                          </c:if>
                                                                      </c:forEach>
                                                                      <c:if test="${fn:length(hAndPText) gt 100}">
                                                                          <c:set var="hAndPText" value="${fn:substring(hAndPText, 0, 99)}..."/>
                                                                          <c:set var="moreInfoStatus" value="true"/>
                                                                      </c:if>
                                                                      <tr>
                                                                          <td class="text-left">${keyname}</td>
                                                                          <c:choose>
                                                                              <c:when test="${hAndPText ne ''}">
                                                                                  <td class="text-left"> ${hAndPText} </td>
                                                                              </c:when>
                                                                              <c:otherwise>
                                                                                  <td class="text-left"> Not Found </td>
                                                                              </c:otherwise>
                                                                          </c:choose>
                                                                      </tr>
                                                                      <%--</c:if>--%>
                                                                  </c:forEach>
                                                              </tbody>
                                                          </table>
                                                          <c:if test="${moreInfoStatus}">
                                                              <p class="text-left lxr-note"><span class="lxrm-bold">Note:</span> For complete list please download the report.</p>
                                                          </c:if>
                                                      </c:if>
                                                  </div>
                                              </div>
                                              <!-- H-Tags End-->
                                              <div class="clearfixdiv"></div>
                                              <!-- Words Count Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Sufficient Word Count</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="Search engines seek pages that contain text content that fulfills the goals of their visitors. The 50 word limit, while somewhat arbitrary, is in a reasonable range for what the engines appears to fulfill 'minimum' unique content in most cases.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <c:choose>
                                                          <c:when test="${wordCount gt 0}">
                                                              <p><span class="lxrm-bold">Word Count:</span> ${totalWordCount}</p>
                                                              <p>
                                                                  <spring:message code="message.seowordcount" arguments="${wordCount}" />
                                                                  <span class="lxrm-bold">
                                                                      <c:if test="${wordCount > 50}"> good.</c:if>
                                                                      <c:if test="${wordCount <= 50}"> not good.</c:if>
                                                                      </span>
                                                                  </p>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <p>Warning: No text found</p>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </div>
                                              </div>

                                              <!-- Words Count Ends -->
                                              <div class="clearfixdiv"></div>
                                              <!-- Text/HTML Ratio Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Text/HTML Ratio</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="The Code to Text ratio represents the percentage of actual text in a web page.<ul><li>0-10 %-There is a scope for adding more text content or revamping your code</li><li>11-24 %-Text content is moderate, there is a room to improve by adding more text</li><li>25+ %-Text content is Good, don't overdo it</li></ul>">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <c:choose>
                                                          <c:when test="${textRatioCount gt 0}">
                                                              <p><span class="lxrm-bold">Text/Html Ratio:</span> ${textRatioCount}</p>
                                                              <p>
                                                                  <span>
                                                                      <c:if test="${textRatioCount  >= 0.0 && textRatioCount <= 10.0}"><spring:message code="message.seotextratiobad" /></c:if>
                                                                      <c:if test="${textRatioCount >= 11.0 && textRatioCount <= 24.0}"><spring:message code="message.seotextratiomod" /></c:if>
                                                                      <c:if test="${textRatioCount >= 25}"><spring:message code="message.seotextratiogood" /></c:if>
                                                                      </span>
                                                                  </p>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <p><span class="lxrm-bold">Warning:</span> No Text/Html Ratio is not Found</p>
                                                          </c:otherwise>
                                                      </c:choose>
                                                  </div>
                                              </div>
                                              <!-- Text/HTML Ratio Ends -->
                                              <div class="clearfixdiv"></div>
                                              <!-- Keywords in Image Alt Attributes Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Keywords in Image Alt Attributes</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="The purpose of the alt attribute is to provide alternative text descriptions of your images. Keyword usage in the alt attribute of an image has recently been correlated with good rankings. It also helps considerably with image search.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <table class="table lxr-table">

                                                          <c:choose>
                                                              <c:when test="${fn:length(spiderViewResult.keywordsFoundInAltAttributes) > 0}">
                                                                  <thead style="background: white;color: #4F4F4F;">
                                                                      <tr>
                                                                          <th class="text-left lxrm-bold">Keyword</th>
                                                                          <th class="text-right lxrm-bold">Count</th>
                                                                      </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      <c:forEach var="KeywordsFound" items="${spiderViewResult.keywordsFoundInAltAttributes}" varStatus="limit">
                                                                          <c:if test="${limit.index < 5}">
                                                                              <tr>
                                                                                  <td class="text-left">${KeywordsFound.key}</td>
                                                                                  <td class="text-right">${KeywordsFound.value}</td>
                                                                              </tr>
                                                                          </c:if>
                                                                      </c:forEach>
                                                                  </tbody>
                                                              </c:when>
                                                              <c:otherwise>
                                                                  <tr>
                                                                      <td colspan="2" class="text-center"><span class="lxrm-bold">Warning:</span> No Keywords Found in Image Alt Attributes</td>
                                                                  </tr>
                                                              </c:otherwise>
                                                          </c:choose>
                                                      </table>
                                                      <c:if test="${fn:length(spiderViewResult.keywordsFoundInAltAttributes) > 10}">
                                                          <p class="text-left lxr-note"><span class="lxrm-bold">Note:</span> For complete list please download the report.</p>
                                                      </c:if>
                                                  </div>
                                              </div>

                                              <!-- Keywords in Image Alt Attributes Ends -->
                                              <div class="clearfixdiv"></div>
                                              <!-- Missing Image Alt Attributes Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>Missing Image Alt Attributes</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="The purpose of the alt attribute is to provide alternative text descriptions of your images. Keyword usage in the alt attribute of an image has recently been correlated with good rankings. It also helps considerably with image search.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-12 section-content text-center">
                                                      <table class="table lxr-table">
                                                          <c:choose>
                                                              <c:when test="${fn:length(spiderViewResult.missingImageAltAttributes) > 0}">
                                                                  <thead style="background: white;color: #4F4F4F;">
                                                                      <tr>
                                                                          <th class="text-left lxrm-bold">URL</th>
                                                                          <th class="text-right lxrm-bold">Count</th>
                                                                      </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      <c:forEach var="missingImgAltTags" items="${spiderViewResult.missingImageAltAttributes}" varStatus="limit">
                                                                          <c:if test="${limit.index < 5}">
                                                                              <tr>
                                                                                  <td class="text-left">${missingImgAltTags.key}</td>
                                                                                  <td class="text-right">${missingImgAltTags.value}</td>
                                                                              </tr>
                                                                          </c:if>
                                                                      </c:forEach>
                                                                  </tbody>
                                                              </c:when>
                                                              <c:otherwise>
                                                                  <tr>
                                                                      <td colspan="2" class="text-center">No Missing Image Alt Attributes</td>
                                                                  </tr>
                                                              </c:otherwise>
                                                          </c:choose>
                                                      </table>
                                                      <c:if test="${fn:length(spiderViewResult.missingImageAltAttributes) > 5}">
                                                          <p class="text-left lxr-note"><span class="lxrm-bold">Note:</span> For complete list please download the report.</p>
                                                      </c:if>
                                                  </div>
                                              </div>
                                              <!-- Missing Image Alt Attributes Ends -->
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!--how to fix-->
                              <div id="howtofix-1" style="display: none;">
                                  <div class="col-lg-12 col-sm-12 col-xs-12 panel-content">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-light-danger">Below are the steps to fix on page issues on the website</span>
                                              <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p>1. Write Meta tags such as Title, Meta Description and Keywords related to your category or product pages.</p>
                                              <p>2. Need to Optimize Page Loading Speed.</p>
                                              <p>3. Usage of Keywords in the cotent.</p>
                                              <p>4. Write H1 to H3 Tag.</p>
                                              <p>5. Write good content for your webpages.</p>
                                              <p>6. Need to Take On Page SEO Friendly Url.</p>
                                              <p>7. Image Optimization, Adding alt tags and renaming your images according to the content can help you be found on google image.</p>
                                              <div class="lxr-panel-footer">
                                                  <h3><spring:message code="ate.expert.label.2" /></h3>
                                                  <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                              </div>
                                          </div>
                                      </div>  
                                  </div>
                              </div>
                          </div>

                          <div class="tab">
                              <button class="tab-toggle lxrtool-tabs-heading">
                                  <span>RELEVANT KEYWORDS</span>
                                  <p>
                                      A keyword is a word or phrase that is a topic of significance that helps visitors 
                                      and search engines understand the purpose of your page.
                                  </p>
                              </button>
                          </div>
                          <div class="content">
                              <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <span class="panel-title">RELEVANT KEYWORDS / KEYPHRASES</span>
                                          <p id="issue-text-2">
                                              If we do not use Relevant keywords, we can face some issues 
                                              like decrease conversion, rankings drop, we won't get better quality web traffic, 
                                              cannot identify user behavior and much more. 
                                              Follow the <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(2);">STEPS</strong> for better Relevant keywords.
                                          </p>
                                      </div>
                                      <div class="panel-body lxr-panel-body">
                                          <div id="result-content-2">
                                              <!-- Keyword Phrases Starts -->
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <c:if test="${fn:length(spiderViewResult.keywordPhrses) > 0}">
                                                      <c:forEach items="${spiderViewResult.keywordPhrses}" var="keywordsAndKeyphrases" varStatus="listIndex">
                                                          <div class="col-xs-12 section-heading lxrm-padding-none">
                                                              <div class="col-xs-10 lxrm-padding-none">
                                                                  <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                                  </span>
                                                                  <c:if test="${listIndex.index==0}">
                                                                      <span>Single Keywords</span>
                                                                  </c:if>
                                                                  <c:if test="${listIndex.index==1}">
                                                                      <span>Two Word Keyphrases</span>
                                                                  </c:if>
                                                                  <c:if test="${listIndex.index==2}">
                                                                      <span>Three Word Keyphrases</span>
                                                                  </c:if>
                                                                  <c:if test="${listIndex.index==3}">
                                                                      <span>Four Word Keyphrases</span>
                                                                  </c:if>
                                                              </div>
                                                          </div>
                                                          <div class="col-xs-12 section-content text-center">
                                                              <c:choose>
                                                                  <c:when test="${listIndex.index == 0}">
                                                                      <!-- Single phrase Keywords Starts-->
                                                                      <table class="table lxr-table">
                                                                          <c:choose>
                                                                              <c:when test="${fn:length(keywordsAndKeyphrases) > 0}">
                                                                                  <thead style="background: white;color: #4F4F4F;">
                                                                                      <tr>
                                                                                          <th class="text-left lxrm-bold">Keyword</th>
                                                                                          <th class="text-right lxrm-bold">Count</th>
                                                                                      </tr>
                                                                                  </thead>
                                                                                  <tbody>
                                                                                      <c:forEach items="${keywordsAndKeyphrases}" var="keywordPhrases" varStatus="mapIndex">
                                                                                          <c:if test="${mapIndex.index < 5}">
                                                                                              <tr>
                                                                                                  <td class="text-left">${keywordPhrases.key}</td>
                                                                                                  <td class="text-right">${keywordPhrases.value.count}</td>
                                                                                              </tr>
                                                                                          </c:if>
                                                                                      </c:forEach>
                                                                                  </tbody>
                                                                              </c:when>
                                                                              <c:otherwise>
                                                                                  <tr>
                                                                                      <td colspan="2" class="text-center"><span class="lxrm-bold">Warning: </span>No meaningful Single Keywords found.</td>
                                                                                  </tr>
                                                                              </c:otherwise>
                                                                          </c:choose>
                                                                      </table>
                                                                      <!-- Single phrase Keywords Ends-->
                                                                  </c:when>
                                                                  <c:otherwise>
                                                                      <table class="table lxr-table">
                                                                          <c:choose>
                                                                              <c:when test="${fn:length(keywordsAndKeyphrases) > 0}">
                                                                                  <thead style="background: white;color: #4F4F4F;">
                                                                                      <tr>
                                                                                          <th class="text-left lxrm-bold" style="width: 60%;">Keyword</th>
                                                                                          <th class="text-left lxrm-bold" style="width: 20%;">Rating</th>
                                                                                          <th class="text-right lxrm-bold" style="width: 20%;">Count</th>
                                                                                      </tr>
                                                                                  </thead>
                                                                                  <tbody>
                                                                                      <c:forEach items="${keywordsAndKeyphrases}" var="keywordPhrases" varStatus="mapIndex">
                                                                                          <c:if test="${mapIndex.index < 5}">
                                                                                              <tr>
                                                                                                  <td class="text-left">${keywordPhrases.key}</td>
                                                                                                  <td class="text-left">${keywordPhrases.value.rating}</td>
                                                                                                  <td class="text-right">${keywordPhrases.value.count}</td>
                                                                                              </tr>
                                                                                          </c:if>
                                                                                      </c:forEach>
                                                                                  </tbody>
                                                                              </c:when>
                                                                              <c:otherwise>
                                                                                  <tr>
                                                                                      <td colspan="2" class="text-center"><span class="lxrm-bold">Warning: </span>No meaningful Keywords found.</td>
                                                                                  </tr>
                                                                              </c:otherwise>
                                                                          </c:choose>
                                                                      </table>
                                                                  </c:otherwise>
                                                              </c:choose>
                                                          </div>
                                                      </c:forEach>

                                                      <c:if test="${fn:length(spiderViewResult.keywordPhrses[0]) > 5 || fn:length(spiderViewResult.keywordPhrses[2]) > 5 
                                                                    || fn:length(spiderViewResult.keywordPhrses[2]) > 5 || fn:length(spiderViewResult.keywordPhrses[3]) > 5}">
                                                            <div class="col-xs-12 section-content">
                                                                <p class="text-left lxr-note"><span class="lxrm-bold">Note:</span> For complete list please download the report.</p>
                                                            </div>

                                                      </c:if>
                                                  </c:if>

                                              </div>
                                              <!-- Keyword Phrases End -->
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!--how to fix-->
                              <div id="howtofix-2" style="display: none;">
                                  <div class="col-lg-12 col-sm-12 col-xs-12 panel-content">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-light-danger">Use following steps to create relevant keywords</span>
                                              <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p>1. Choose the keyword from your website content which is related to your business and service, make sure to keep the keyword density (ratio between the total content word count and keyword count) level as required.</p>
                                              <p>2. Use Google AdWords keyword planner to choose the popular and common searchable keyword, you must think as customer point of view.</p>
                                              <div class="lxr-panel-footer">
                                                  <h3><spring:message code="ate.expert.label.2" /></h3>
                                                  <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                              </div>
                                          </div>
                                      </div>  
                                  </div>
                              </div>
                          </div>

                          <div class="tab">
                              <button class="tab-toggle lxrtool-tabs-heading">
                                  <span>SEARCH ENGINE RESULTS</span>
                                  <p>
                                      Whenever you search for something and the top 10 results you get as the result is known as SERP.
                                  </p>
                              </button>
                          </div>
                          <div class="content">
                              <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <span class="panel-title">SEARCH ENGINE RESULTS</span>
                                          <!--<p id="issue-text-3">These links are dead and need to be handled before they do damage. You can fix this by following these. <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(3);">STEPS.</strong></p>-->
                                      </div>
                                      <div class="panel-body lxr-panel-body">
                                          <div id="result-content-3">
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <p>
                                                      <span style="color: green;">
                                                          <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                      This is an example of how your site might appear as a listing within search results.</p>
                                                  <div class="search-results-content">
                                                      <c:choose>
                                                          <c:when test="${spiderViewResult.pageDisplayedInSerachResults[0] != null 
                                                                          && spiderViewResult.pageDisplayedInSerachResults[0] != ''}">
                                                                  <a style="color: #2F80ED !important;font-size: 22px;" href="${spiderViewResult.pageDisplayedInSerachResults[2]}"
                                                                     target="_blank">${spiderViewResult.pageDisplayedInSerachResults[0]}</a>
                                                          </c:when>
                                                          <c:otherwise>
                                                              <p>
                                                                  <a href="${spiderViewResult.pageDisplayedInSerachResults[2]}"
                                                                     target="_blank">No Title Found.</a>
                                                              </p>
                                                          </c:otherwise>
                                                      </c:choose>
                                                      <p>
                                                          <a style="color:#2F80ED !important;" href="${spiderViewResult.pageDisplayedInSerachResults[2]}"
                                                             target="_blank">${spiderViewResult.pageDisplayedInSerachResults[2]}</a>
                                                      </p>
                                                      <c:if test="${spiderViewResult.pageDisplayedInSerachResults[1] != null 
                                                                    && spiderViewResult.pageDisplayedInSerachResults[1] != ''}">
                                                            <p>
                                                                <a href="${spiderViewResult.pageDisplayedInSerachResults[2]}"
                                                                   target="_blank">${spiderViewResult.pageDisplayedInSerachResults[1]}</a>
                                                            </p>
                                                      </c:if>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="tab">
                              <button class="tab-toggle lxrtool-tabs-heading">
                                  <span>URLS FOUND IN THE PAGE</span>
                                  <p>
                                      URL is an acronym for Uniform Resource Locator. A URL is the address of a specific Web site or file on the Internet.
                                  </p>
                              </button>
                          </div>
                          <div class="content">
                              <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-4">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <span class="panel-title">URLS FOUND IN THE PAGE</span>
                                          <p id="issue-text-4">
                                              URL is a significant aspect of your website. 
                                              Aside from enabling your visitors to view your content, this can also make or break your website, 
                                              follow the <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(4);">STEPS</strong> to create SEO friendly URLs. 

                                          </p>
                                      </div>
                                      <div class="panel-body lxr-panel-body">
                                          <div id="result-content-4">
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <p>This section lists the URLs found within the page 
                                                      ${spiderViewResult.pageDisplayedInSerachResults[2]}, and the number of 
                                                      times each was found. Google used to index only about 100 
                                                      kilobytes of a page. When we thought about how many links a 
                                                      page might reasonably have and still be under 100K, it 
                                                      seemed about right to recommend 100 links.
                                                  </p>
                                                  <%  Map<Integer, Map<String, Integer>> totLinks = null;
                                                      int uniquelinks = 0;
                                                      if (spiderViewResult != null) {
                                                          totLinks = spiderViewResult.getAnchorLinks();
                                                          if (totLinks.size() > 0) {
                                                              uniquelinks = totLinks.get(2).size() + totLinks.get(3).size() + totLinks.get(4).size();
                                                          }
                                                      }%>
                                                  <c:choose>
                                                      <c:when test="${fn:length(spiderViewResult.anchorLinks) > 0}">
                                                          <c:forEach items="${spiderViewResult.anchorLinks}" var="anchorLink" varStatus="i">
                                                              <c:if test="${i.index == 0}">
                                                                  Found ${anchorLink.value['totalNoOfLinks']} URLs of which <%=uniquelinks%> are unique
                                                              </c:if>
                                                              <c:if test="${i.index > 0}">
                                                                  <c:set var="linkName" value=""/>
                                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                                      <div class="col-xs-10 lxrm-padding-none">
                                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                                              <c:if test="${i.index == 1}">
                                                                                  <c:set var="linkName" value="Internal"/>
                                                                              </c:if>
                                                                              <c:if test="${i.index == 2}">
                                                                                  <c:set var="linkName" value="External"/>
                                                                              </c:if>
                                                                              <c:if test="${i.index == 3}">
                                                                                  <c:set var="linkName" value="Social"/>
                                                                              </c:if>
                                                                          <span>${linkName}</span>
                                                                      </div>
                                                                  </div>
                                                                  <c:choose>
                                                                      <c:when test="${fn:length(anchorLink.value) > 0}">
                                                                          <ul class="brk extern">
                                                                              <c:forEach items="${anchorLink.value}" var="links" varStatus="j">
                                                                                  <c:if test="${j.index < 5}">
                                                                                      <li>
                                                                                          <span>&#45;</span> 
                                                                                          <a href="${links.key}" target="_blank" title="open in new window">${links.key}</a>
                                                                                          <span> - ${links.value}</span>
                                                                                      </li>
                                                                                  </c:if>
                                                                              </c:forEach>
                                                                          </ul>
                                                                      </c:when>
                                                                      <c:otherwise>
                                                                          <table class="table lxr-table"><tr><td class="text-center">${linkName} links not found.<td></tr></table>
                                                                      </c:otherwise>
                                                                  </c:choose>
                                                              </c:if>
                                                          </c:forEach>
                                                      </c:when>
                                                      <c:otherwise>
                                                          <p class="text-center">No links found.</p>
                                                      </c:otherwise>
                                                  </c:choose>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!--how to fix-->
                              <div id="howtofix-4" style="display: none;">
                                  <div class="col-lg-12 col-sm-12 col-xs-12 panel-content">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-light-danger">Use following steps to create SEO friendly URLs </span>
                                              <span class="glyphicon glyphicon-remove pull-right" id="closebox3" onclick="hideHowToFixAndShowContent(4);"></span>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p>1. Try to use the simple, amazing, attractive and readable URL that is easily recognized by the readers.</p>
                                              <p>2. Use the URL which matched your business keyword. </p>
                                              <p>3. Don’t use long tail keyword which is difficult to read by the customers.</p>
                                              <div class="lxr-panel-footer">
                                                  <h3><spring:message code="ate.expert.label.2" /></h3>
                                                  <button class="btn" onclick="submitATEQuery('4');">Get Help</button>
                                              </div>
                                          </div>
                                      </div>  
                                  </div>
                              </div>
                          </div>

                          <div class="tab">
                              <button class="tab-toggle lxrtool-tabs-heading">
                                  <span>HTTP HEADERS CHECK</span>
                                  <p>To ensure that search engines understand your website structure correctly 
                                      you should control each page's HTTP headers. 
                                      Your server should handle all requests properly - by this, you will both achieve 
                                      a better crawl rate and higher rankings.
                                  </p>
                              </button>
                          </div>
                          <div class="content">
                              <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-5">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <span class="panel-title">HTTP HEADERS CHECK</span>
                                      </div>
                                      <div class="panel-body lxr-panel-body">
                                          <div id="result-content-5">
                                              <div class="col-xs-12 lxrm-padding-none">
                                                  <div class="col-xs-12 section-heading lxrm-padding-none">
                                                      <div class="col-xs-10 lxrm-padding-none">
                                                          <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                                          <span>HTTP Headers Check</span>
                                                      </div>
                                                      <div class="col-xs-2 text-right">
                                                          <span>
                                                              <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                                                 title="To ensure that search engines understand your web site structure correctly you should control each page's HTTP headers. Your server should handle all requests properly - by this you will both achieve a better crawl rate and higher rankings.">
                                                                  <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                              </a>
                                                          </span>
                                                      </div>
                                                  </div>
                                                  <table class="table lxr-table">
                                                      <tbody>
                                                          <c:choose>
                                                              <c:when test="${fn:length(spiderViewResult.httpHeaders) > 0}">
                                                                  <c:forEach var="httpHeaders" items="${spiderViewResult.httpHeaders}" varStatus="limit">
                                                                      <tr>
                                                                          <td class="lxrm-bold" style="width: 30%;">${httpHeaders.key}</td>
                                                                          <td>${httpHeaders.value}</td>
                                                                      </tr>
                                                                  </c:forEach>
                                                              </c:when>
                                                              <c:otherwise>
                                                                  <tr>
                                                                      <td colspan="2" class="text-center">No Headers Found.</td>
                                                                  </tr>
                                                              </c:otherwise>
                                                          </c:choose>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row download-report-row">
                      <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                      <div class="col-lg-10 col-sm-12 col-xs-12 download-report-div">
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none">
                              <p style="margin: 5% 0;">Download complete report here: </p>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding: 0;">
                              <div class="dropdown">
                                  <div id="downloadReport" onclick="downloadFormat();" title="GENERATE PDF REPORT FOR $1.99" style="margin: 0;">
                                      <button class="col-xs-12 col-lg-8">GENERATE PDF REPORT FOR $1.99  <i class="glyphicon glyphicon-download-alt"></i></button>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
        </c:if>
        <div class="container">
            <div class="clearfixdiv"></div>
            <!--Recently Downloaded Reports:Begin -->
            <c:if test = "${paidDownloadedReportDetailsList != null && paidDownloadedReportDetailsList != '' && fn:length(paidDownloadedReportDetailsList) gt 0}">
                <c:if test="${fn:length(paidDownloadedReportDetailsList) gt 0}">
                    <div class="col-xs-12 prev-dwnld-div">
                        <h4 class='prvs-reports'>Previous Downloaded Reports:</h4>
                        <div style="padding: 0;overflow: auto;max-height: 250px;border: 1px solid #ddd;">
                            <table class="table lxr-table" style="border: 1px solid #ddd;margin-bottom: 0;">
                                <thead style="background: white;color: #4F4F4F;">
                                    <tr>
                                        <th class="text-left">Created Time</th>
                                        <th class="text-left">URL</th>
                                        <th class="text-left">Report Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${paidDownloadedReportDetailsList}" var="paidReports">
                                        <tr>
                                            <td class="text-left">${paidReports.createdTime}</td>
                                            <td class="text-left">${paidReports.url}</td>
                                            <td class="text-left">
                                                <span onclick="giveDownloadReport('${paidReports.reportName}');" style="cursor: pointer;color: #2f80ed !important;">${paidReports.reportName}</span>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </c:if>
            </c:if>
            <c:set var="fileMessage" value="${fileExistence}" />
            <c:if test = "${fileMessage eq 'Sorry the requested file is no longer existed.'}">  
                <div class="row">
                    <div id="reportNotification" class="col-xs-12 col-lg-12 notworking prev-dwnld-div">
                        <p class="text-center lxr-danger">Sorry the requested file has no longer existed.</p>
                    </div>
                </div>
            </c:if>
        </div>
        <!--   Success Page END-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
