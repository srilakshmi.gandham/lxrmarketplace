
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1" id="cookiePopup" style="display: none;padding: 1rem;z-index: 1000;position: fixed;bottom: 0;background: #000;color: #FFF;border-radius: 6px;font-size: 14px;">
            <!--<div class="row">-->
            <div class="col-xs-12 col-md-10">
                We use cookies to understand how you use our site and provide the best browsing experience possible. 
                This includes analysing our traffic, personalizing content and advertising. 
                By continuing to use our site, you accept our use of cookies, 
                <a href="/privacy-policy.html" target="_blank">Privacy Policy</a> and 
                <a href="/terms-of-service.html" target="_blank">Terms of Service</a>.
            </div>
            <div class="col-xs-12 col-md-2" style="padding-top: 10px;">
                <div class="col-xs-8">
                    <button class="btn lxrm-bold" style="color: #000;width: 100%;" onclick="setCookie();">Accept</button>
                </div>
                <div class="col-xs-4">
                    <span class="fa fa-times fa-2x" style="cursor: pointer;" onclick="closeCookiePopup();"></span>
                </div>
            </div>
            <!--</div>-->
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        checkCookie();
    });
    function closeCookiePopup() {
        $("#cookiePopup").hide();
    }
    function setCookie() {
        document.cookie = "LXRMCookiePopup" + "=cookiesAccepted;path=/";
        $("#cookiePopup").hide();
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function checkCookie() {
        var user = getCookie("LXRMCookiePopup");
        if (user !== "") {
            $("#cookiePopup").fadeOut();
        } else {
            $("#cookiePopup").fadeIn(2000);
        }
    }
</script>