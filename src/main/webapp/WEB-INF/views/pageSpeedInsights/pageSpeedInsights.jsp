<%-- 
    Document   : PageSpeed Insights
    Created on : Sep 09, 2017, 11:24:49 AM
    Author     : NE16T1213
--%>

<%--<%@page import="java.util.LinkedList, java.util.List, lxr.marketplace.pagespeedinsights.PageSpeedInsights"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="lxrmarketplacePSI">
    <head>
        <title> PageSpeed Insights | LXR Marketplace Tool | Download Report</title>
        <meta name="description" content="Try the Free PageSpeed Insights Tool at LXRMarketplace.">
        <meta name="keywords" content=" PageSpeed Insights">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="/resources/css/tools/tooltabs.js"></script>-->
        <!-- Angular JS -->
        <script type="text/javascript" src="/resources/js/angular/angular.min.js"></script>
        <script type="text/javascript"  src="/resources/js/app.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js" ></script>

        <!--Tool angularJS controller-->
        <script src="/resources/js/tools/pageSpeedInsights/pageSpeedInsights.js"></script>


        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/pageSpeedInsights.css">

        <!--Scripts related to score graph-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!--Scripts/Styles related to critical request chain-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>



        <%
            int toolDBId = 35;
            String reqParam = null;
            if (request.getParameter("report") != null) {
                reqParam = request.getParameter("report");
                pageContext.setAttribute("reqParam", reqParam);
            }
            String downloadReportType = null;
            if (request.getParameter("report-type") != null) {
                downloadReportType = request.getParameter("report-type");
                pageContext.setAttribute("downloadReportType", downloadReportType);
            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var atePopupStatus = true;
            var isBackToSuccess = false;
            var isLimitExceedLogin = false;
            var GoogleAPIKibiBytes = 1024;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                setTimeout(function () {
                    $('#psiLoadingDiv').hide();
                }, 300);
                /*Enabling tool tip*/
                $("#psiResultWrapper").hide();
                $('[data-toggle="tooltip"]').tooltip();
                $(window).on('popstate', function () {
                    location.reload(true);
                });
                /*  auto download when user logged from success page*/
            <c:choose>
                <c:when test="${(fn:contains(uri, 'loginRefresh') && fn:contains(uri, 'report'))}">
                isBackToSuccess = true;
                setTimeout(function () {
                    angular.element('#downloadPSIReportId').triggerHandler('click');
                }, 1000);
                </c:when>
                <c:when test="${(fn:contains(uri, 'loginRefresh') && fn:contains(uri, 'limit-exceeded=1'))}">
                isLimitExceedLogin = true;
                isBackToSuccess = false;
                </c:when>
                <c:when test="${(fn:contains(uri, 'backToSuccessPage'))}">
                isBackToSuccess = true;
                </c:when>
                <c:when test="${(fn:contains(uri, 'loginRefresh'))}">
                isBackToSuccess = false;
                isLimitExceedLogin = true;
                </c:when>
            </c:choose>

                $('#userPSIEmail').keypress(function (ev) {
                    if (ev.which === 13) {
                        $('#sendPSIReportToMail').click();
                        return false;
                    }
                });
                $('#userPSIEmail').click(function () {
                    $('#mailSentStatus').empty();
                    $('#mailSentStatus').hide();
                    return false;
                });
                /*Related reset input fields*/
                $('#editSearch, #queryWebPageURL').on('click', function () {
                    document.getElementById("queryWebPageURL").readOnly = false;
                    $("#queryWebPageURL").focus();
                    $("#analyze-spinner").removeClass("fa-spin");
                    $("#mailIdMessage, #searchQueryError").empty();
                    $('#editSearch').hide();
                    $("#queryWebPageURL").removeClass('gray');
                });
                $('#queryWebPageURL').keypress(function (ev) {
                    if (ev.which === 13) {
                        angular.element('#analyzePageSpeedInsights').triggerHandler('click');
                        return false;
                    }
                });
                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}");
                }
                /*creating jstree instances.*/
                $(function () {
                    $('#jstree_demo_div').jstree();
                });
            }); /*EOF of on ready*/
            function requestPSIReportToDownload(psiResultList) {
                console.log("requestPSIReportToDownload::");
                var downloadForm = $("#pageSpeedInsightsForm");
                if ($('#pageSpeedInsightsResult').length) {
                    $("#pageSpeedInsightsResult").remove();
                }
                var pageSpeedInsightsJSON = JSON.stringify(psiResultList);
                var pageSpeedInsightsResult = $("<input>").attr("type", "hidden").attr("id", "pageSpeedInsightsResult").attr("name", "pageSpeedInsightsResult").val(pageSpeedInsightsJSON);
                downloadForm.append($(pageSpeedInsightsResult));
                downloadForm.attr('method', "POST");
                downloadForm.attr('action', "/page-speed-insights-tool.html?request=download");
                downloadForm.submit();
            }
            /*Functions related to download, send report to mail*/
            function requestPSIReportToEmail(psiResultList) {
                console.log("requestPSIReportToEmail:: " + psiResultList.length);
                var emailid = $.trim($("#userPSIEmail").val());
                $('#mailSentStatus').show();
                $('#downloadPSIReportId').disabled;
                if (!(emailid)) {
                    $('#mailSentStatus').html("Please provide email address.");
                    $('#mailSentStatus').css("color", "red");
                    $("#userPSIEmail").focus();
                    return false;
                } else if (!isValidMail(emailid)) {
                    $('#mailSentStatus').html("Please enter a valid email address.");
                    $('#mailSentStatus').css("color", "red");
                    $("#userPSIEmail").focus();
                    return false;
                } else {
                    disableAllBeforeSendMail();
                    $('#mailSentStatus').html("sending...");
                    $('#mailSentStatus').css("color", "green");
                    var userEmail = $("#userPSIEmail").val().trim();
                    var oMyForm = new FormData();
                    oMyForm.append("pageSpeedInsightsResult", JSON.stringify(psiResultList));
                    oMyForm.append("userEmail", userEmail);
                    $.ajax({
                        type: "POST",
                        url: "/page-speed-insights-tool.html?request=sendMail&email=" + userEmail,
                        data: oMyForm,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function (data) {
                            $('#mailSentStatus').empty();
                            if (data[0]) {
                                $('#mailSentStatus').html("Mail sent successfully.");
                                $('#mailSentStatus').css("color", "green");
                            } else {
                                $('#mailSentStatus').html("Sending mail failed.");
                                $('#mailSentStatus').css("color", "red");
                            }
                        },
                        error: function () {
                            console.log("Error in sending tool analysis report");
                        },
                        complete: function () {
                            enableAllAfterSendMail();
                        }
                    });
                }
            }
            /*Request to get Ask The Expert related questions.*/
            function getToolRelatedIssues() {
                var pageSpeedAteURL = $('#queryWebPageURL').val();
                $.ajax({
                    type: "POST",
                    url: "/page-speed-insights-tool.html?requestMethod=pageSpeedATE&userATEDomain=" + pageSpeedAteURL + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website) {
                setTimeout(function () {
                    angular.element(document.getElementById("pageSpeedAngularWrapper")).scope().setToolAdvisiorURL(website);
                    $("#queryWebPageURL").val(website);
                }, 10);
            }

            function enableAllAfterSendMail() {
                console.log("In enableAllAfterSendMail");
                $('#queryWebPageURL').attr('disabled', false);
                $('#userPSIEmail').attr('disabled', false);
                $('#queryWebPageURL').attr('readonly', false);
                $('#userPSIEmail').attr('readonly', false);
                $('#downloadPSIReportId').attr('disabled', false);
                $('#sendPSIReportToMail').attr('disabled', false);
                $('#analyzePageSpeedInsights').attr('disabled', false);
            }

            function disableAllBeforeSendMail() {
                console.log("In disableAllBeforeSendMail");
                $('#queryWebPageURL').attr('disabled', true);
                $('#userPSIEmail').attr('disabled', true);
                $('#queryWebPageURL').attr('readonly', true);
                $('#userPSIEmail').attr('readonly', true);
                $('#downloadPSIReportId').attr('disabled', true);
                $('#sendPSIReportToMail').attr('disabled', true);
                $('#analyzePageSpeedInsights').attr('disabled', true);
            }

            function submitPreviousRequest() {
                angular.element('#analyzePageSpeedInsights').triggerHandler('click');
            }

        </script>
    </head>
    <body ng-controller="PageSpeedInsightsController as psiCTRL" id="pageSpeedAngularWrapper"  data-spy="scroll" data-target="#head">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div id="psiLoadingDiv" class="col-12 p-0" style="position: fixed;background: gray;width: 100%;height: 100%;top: 0;z-index: 1000;color: #FFF;left: 0;opacity: 0.8;text-align: center;">
            <span class="lxrm-bold" style="position: relative;top: 50%;font-size: 2rem;"><span class='fa fa-spinner fa-spin'></span> Loading...</span>
        </div>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <!--textboxes search part start-->
        <div class="container">
            <form name="pageSpeedInsightsForm" id="pageSpeedInsightsForm">
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-xs-12">
                        <div class="icon-addon addon-lg">
                            <input type="text" id="queryWebPageURL" ng-model="queryWebPageURL"  name="queryWebPageURL" class="form-control input-lg" placeHolder="Enter a web page URL"/>
                            <i id="editSearch" class="glyphicon glyphicon-pencil lxrtool-pencil" style="display: none;"></i>
                        </div>
                        <span id="searchQueryError" class="lxr-danger text-left"></span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="submit" ng-click="fetchPageSpeedInsights()" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12" id="analyzePageSpeedInsights" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING...">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="clearfixdiv"></div>
        <div class="clearfixdiv"  ng-init="processPSIBackToSuccessPage()"></div>
        <div class="container lxrm-shadow" id="psiResultWrapper" ng-if="psiCTRL.isDataPulled">
            <div id="resultRow" class="row" ng-switch="((psiCTRL.mobilePageSpeedInsight.statusCode === 200 || psiCTRL.deskTopPageSpeedInsight.statusCode === 200) && psiCTRL.isDataPulled)">
                <div class="col-xs-12" ng-switch-when="true">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#mobileDeviceWrapper" data-toggle="tab" class="lxrm-bold lxrm-sub-header-1"><i class="fa fa-mobile" aria-hidden="true"></i> MOBILE</a>
                                </li>
                                <li>
                                    <a href="#deskTopDeviceWrapper" data-toggle="tab" class="lxrm-bold lxrm-sub-header-1"><i class="fa fa-laptop" aria-hidden="true"></i> DESKTOP</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="mobileDeviceWrapper">
                                    <!--start of mobileDeviceWrapper parent row-->
                                    <div class="row lxrm-padding-none" ng-switch="psiCTRL.mobilePageSpeedInsight.statusCode === 200">
                                        <div class="col-xs-12" ng-switch-when="true">
                                            <div class="row">
                                                <div class="col-xs-12 analysisTime">
                                                    <p style="padding: 1rem 2rem;">
                                                        <span class="tool-info">Analysis time</span>
                                                        <span class="lxrm-bold metric-title lxrm-sub-header-3 text-right"> : {{psiCTRL.mobilePageSpeedInsight.analysisUTCTimestamp}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row border-bottom">
                                                <!--Device score-->
                                                <div class="col-xs-12 lxrm-bg-white lxrm-rounded-lg scoreWrapper" ng-init="generateScoreGraphForDevices('mobile')">
                                                    <!-- Progress bar 1 -->
                                                    <div id="mobileDeviceScore" class="progress lxrm-mx-auto">
                                                        <span class="progress-left">
                                                            <span class="progress-bar border-primary" id='mobileProgressColorLeft'></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar border-primary" id='mobileProgressColorRight'></span>
                                                        </span>
                                                        <div class="progress-value lxrm-w-100 lxrm-h-100 rounded-circle lxrm-d-flex lxrm-align-items-center lxrm-justify-content-center">
                                                            <div class="h2 font-weight-bold" id='mobileProgressValue'></div>
                                                        </div>
                                                    </div>
                                                    <!-- END of  Progress bar 1-->
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-xs-0 col-md-2"></div>
                                                    <div class="col-xs-12 col-md-8">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="psiReviewURL lxrm-sub-header-2 lxrm-bold metric-title"><a href="{{psiCTRL.mobilePageSpeedInsight.finalUrl}}" target="_blank">{{psiCTRL.mobilePageSpeedInsight.finalUrl}}</a></p>
                                                            </div>
                                                            <div class="col-xs-12 scoreGuideWrapper">
                                                                <div class="row text-center">
                                                                    <div class="col-xs-0 col-md-3"></div> 
                                                                    <div class="col-xs-12 col-md-6 lightHouseScoreGuide metric-title tool-info lxrm-ptb-Half">
                                                                        <div class="col-xs-3">
                                                                            <span class="slow"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span>&nbsp;0&nbsp;-&nbsp;49&nbsp;</span>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <span class="average"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span>&nbsp;50&nbsp;-&nbsp;89&nbsp;</span>
                                                                        </div>
                                                                        <div class="col-xs-4 text-left lxrm-pl-none">
                                                                            <span class="fast"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span class="metric-title">&nbsp;90&nbsp;-&nbsp;100&nbsp;</span>
                                                                            <span> 
                                                                                &nbsp;
                                                                                <a href="https://developers.google.com/web/tools/lighthouse/v3/scoring" target="_blank" title="Lighthouse Scoring Guide">
                                                                                    <i class="fa fa-info-circle fa-2" aria-hidden="true" style="color:#333;"></i>
                                                                                </a>
                                                                                &nbsp;
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-0 col-md-3"></div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-0 col-md-2"></div>
                                                </div>
                                                <!--EOF of mobileDeviceWrapper parent row-->
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-0 col-md-1"></div>
                                                <div class="col-xs-12 col-md-8">
                                                    <!--Field Data Wrapper-->
                                                    <div class="row lxrm-p-1">
                                                        <div class="col-xs-12" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div ng-switch-when="true">
                                                                <span class="lxrm-bold lxrm-sub-header-2 metric-title">Field Data &nbsp; </span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.FIELD_DATA_PREFIX" /></span>
                                                                <span class="{{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPColor}} lxrm-bold tool-info">{{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataCategory| capitalize}}</span>
                                                                <span class="lxrm-regular tool-info"><spring:message code="PageSpeedInsights.FIELD_DATA_POSTFIX" /></span>
                                                            </div>
                                                            <div ng-switch-default>
                                                                <span class="lxrm-bold lxrm-sub-header-3 metric-title">Field Data &nbsp;</span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.NO_DATA_MSG" /> page.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 lxrm-p-1" ng-if="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPColor}}" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-8 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstContentfulPaint" /> (FCP)
                                                                            </span>

                                                                            <span class="col-xs-2 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPPercentile| millSecondsToSecond}} s
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="mobileFieldDataFCP" ng-init="constructFCPGraph('mobileFieldDataFCP', psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFCPdistributions)"></div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFIDColor}}" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFIDColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-7 lxrm-bold lxrm-sub-header-3  metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstInputDelay" /> (FID)
                                                                            </span>
                                                                            <!--                                                                            <span class="col-xs-1"></span>-->
                                                                            <span class="col-xs-3 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFIDColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFIDPercentile}} ms
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="mobileFieldDataFID" ng-init="constructFIDGraph('mobileFieldDataFID', psiCTRL.mobilePageSpeedInsight.realWorldFieldData.fieldDataFIDdistributions)"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Field Data Wrapper-->
                                                    <!--Orgin Summary Wrapper-->
                                                    <div class="row lxrm-p-1 orginSummaryDataWrapper">
                                                        <div class="col-xs-12" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div ng-switch-when="true">
                                                                <span class="lxrm-bold lxrm-sub-header-2 metric-title"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY" /> &nbsp; </span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.ORIGIN_SUMMARY_PREFIX" /></span>
                                                                <span class="{{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPColor}} lxrm-bold tool-info">{{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryCategory| capitalize}}</span>
                                                                <span class="lxrm-regular tool-info"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY_POSTFIX" /></span>
                                                            </div>
                                                            <div ng-switch-default>
                                                                <span class="lxrm-bold tool-info metric-title"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY" /> &nbsp;</span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.NO_DATA_MSG" /> origin.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 lxrm-p-1" ng-if="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPColor}}" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-8 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstContentfulPaint" /> (FCP)
                                                                            </span>
                                                                            <!--                                                                            <span class="col-xs-1"></span>-->
                                                                            <span class="col-xs-2 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPPercentile| millSecondsToSecond}} s
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="mobileOrginSummaryFCP" ng-init="constructFCPGraph('mobileOrginSummaryFCP', psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFCPdistributions)"></div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFIDColor}}" ng-switch="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFIDColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-7 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstInputDelay" /> (FID)
                                                                            </span>
                                                                            <!--                                                                            <span class="col-xs-1"></span>-->
                                                                            <span class="col-xs-3 {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFIDColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFIDPercentile}} ms
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="mobileOrginSummaryFID" ng-init="constructFIDGraph('mobileOrginSummaryFID', psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryFIDdistributions)"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12" ng-if="psiCTRL.mobilePageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata"/>'"  ng-init="hideDivData('.orginSummaryDataWrapper')"></div>
                                                        <!--EOF Orgin Summary Wrapper-->
                                                    </div>
                                                    <!--Lab Data-->
                                                    <div class="row lxrm-p-1">
                                                        <!--labdataWrapper-->
                                                        <div class="col-xs-12 labdataWrapper ">
                                                            <div class="row">
                                                                <div class="col-xs-12 lab-info">
                                                                    <div class="col-xs-4 col-md-6 text-left lxrm-bold metric-title lxrm-sub-header-2 lxrm-padding-none">Lab Data</div>
                                                                    <div class="col-xs-8 col-md-6 text-right lxrm-padding-none" style="padding:0em;">
                                                                        <div id="mobileLabDataOptionsWrapper">
                                                                            <label class="btn btn-default btn-on btn-xs active" id="mobileLabDataLessLabel">
                                                                                <input type="radio" name="mobileLabData" checked="checked" value="mobileLabDataLess"> View Less
                                                                            </label>
                                                                            <label class="btn btn-default btn-off btn-xs" id="mobileLabDataMoreLabel">
                                                                                <input type="radio" name="mobileLabData"  value="mobileLabDataMore"> View More
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--EOf of labdata title-->
                                                                <!--labdata metrics info-->
                                                                <div class="col-xs-12 result-data" id="mobileLabDataWrapper">
                                                                    <!--labdata metrics left-->
                                                                    <div class="col-xs-12 col-md-6 lxrm-padding-none labDataLeftCell">
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-contentful-paint" ng-init="constructLabDataMetric('first-contentful-paint', 'divider', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="speed-index" ng-init="constructLabDataMetric('speed-index', 'divider', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider-bottom lxrm-padding-none" id="interactive" ng-init="constructLabDataMetric('interactive', 'divider-bottom', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                    </div>
                                                                    <!--Eof labdata metrics left-->
                                                                    <!--labdata metrics right-->
                                                                    <div class="col-xs-12 col-md-6 lxrm-padding-none labDataRightCell">
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-meaningful-paint" ng-init="constructLabDataMetric('first-meaningful-paint', 'divider', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-cpu-idle" ng-init="constructLabDataMetric('first-cpu-idle', 'divider', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider-bottom lxrm-padding-none" id="max-potential-fid" ng-init="constructLabDataMetric('max-potential-fid', 'divider-bottom', 'mobile')"></div>
                                                                        <!--Metric end--->
                                                                    </div>
                                                                    <!--EOF labdata metrics right-->
                                                                </div>
                                                                <div class="col-xs-12" ng-init="activeMobileLabDataOptions()"></div>
                                                            </div>
                                                            <!--EOF labdataWrapper-->
                                                        </div>
                                                        <!--EOF labdata section-->
                                                    </div>
                                                    <!--EOF of Lab Data-->
                                                    <!--Image thumb nails screen shot loading-->
                                                    <div class="row lxrm-p-1">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="mobileScreenShotThumbnails" ng-init="constructScreenShots('mobile', 'screenshot-thumbnails', 'mobileScreenShotThumbnails')"></div>
                                                    </div>
                                                    <!--Final screen shot of mobile device will display only for small devices-->
                                                    <div class="row lxrm-p-1 lxrm-visible-xs lxrm-visible-sm" style="justify-content: center;text-align: center;">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="mobileFinalScreenShotSmallDevices" ng-init="constructScreenShots('mobile', 'final-screenshot', 'mobileFinalScreenShotSmallDevices')"></div>
                                                    </div>
                                                    <!--Audit Rules categories-->
                                                    <!--Opportunities Audit rules categories-->
                                                    <div class="row lxrm-padding-none auditCategorieWrapper" ng-if="psiCTRL.mobileOpportunitiesList !== null && psiCTRL.mobileOpportunitiesList.length > 0">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <p>
                                                                        <span class="lxrm-bold metric-title lxrm-sub-header-2"><spring:message code="PageSpeedInsights.Opportunities" />&nbsp;&nbsp;</span>
                                                                        <span class="tool-info" ng-if="psiCTRL.mobilePageSpeedInsight.loadOpportunitiesDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <span id="mobileOpportunityTag">{{getPreviewContent(psiCTRL.mobilePageSpeedInsight.loadOpportunitiesDesc, 'mobileOpportunityTag', -1)}}</span>
                                                                        </span>
                                                                        <span class="tool-info" ng-if="!psiCTRL.mobilePageSpeedInsight.loadOpportunitiesDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <spring:message code="PageSpeedInsights.Opportunities.Def"/>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="col-xs-6 metric-title tool-info lxrm-padding-none lxrm-bold">Opportunity</div>
                                                                    <div class="col-xs-6 metric-title tool-info text-right lxrm-padding-none lxrm-bold">Estimated Savings</div>
                                                                </div>
                                                                <div class="col-xs-12 lxrm-padding-none">
                                                                    <div class="accordion" id="mobileOpportunityWrapper">
                                                                        <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.mobilePageSpeedInsight.auditRules track by $index">-->
                                                                        <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.mobileOpportunitiesList| orderBy: '-overallSavingsMs' track by $index">
                                                                            <div class="card-header" id="mobileOpportunityRule{{$index}}">
                                                                                <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12 ruleButton" data-toggle="collapse" data-target="#mobileOpportunityRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                        <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="col-xs-9 col-md-8 lxrm-bold lxrm-sub-header-3  lxrm-pl-none lxrm-sm-pl metric-title text-left">
                                                                                        {{deviceAuditRule.title}}
                                                                                    </span>
                                                                                    <span class="col-xs-1 col-md-2 {{deviceAuditRule.scoreColor}} lxrm-bold tool-info lxrm-padding-none text-right" data-placement="bottom" data-toggle="tooltip" title="{{deviceAuditRule.displayValue}}">
                                                                                        {{deviceAuditRule.overallSavingsMs| millSecondsToSecond}} s
                                                                                    </span>
                                                                                    <span class="col-xs-1 text-right angleUpDown" style="float:right;">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                                <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#mobileOpportunityRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1"></span>
                                                                                    <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                        {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div id="mobileOpportunityRuleBody{{$index}}"  class="collapse" aria-labelledby="mobileOpportunityRule{{$index}}" data-parent="#mobileOpportunityWrapper">
                                                                                <div class="card-body">
                                                                                    <div class="col-xs-12 lxrm-padding-none">
                                                                                        <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-description psi-rule-warning">
                                                                                            <ul class="lxrm-bold"style="padding: 0;margin: 0">Warnings:</ul>
                                                                                            <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.description" id="mobileOpportunityAuditRuleDesc{{$index}}" class="col-xs-12 tool-info psi-description">
                                                                                            {{getPreviewContent(deviceAuditRule.descriptionHTML, 'mobileOpportunityAuditRuleDesc', $index)}}
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info psi-description">
                                                                                            <div class="row">
                                                                                                <div class="col-xs-12 image-txt-container">
                                                                                                    <img src="/images/wordpress.png"/>
                                                                                                    <span class="lxrm-p-1" id="mobileOpportunityAuditRuleStackDesc{{$index}}">
                                                                                                        {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'mobileOpportunityAuditRuleStackDesc', $index)}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="mobileOpportunityAuditRule{{$index}}" ng-init="constructAuditRuleTable('mobileOpportunityAuditRule', deviceAuditRule, $index)"></div>
                                                                                        <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="mobileOpportunityAuditCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('mobileOpportunityAuditCriticalRule', deviceAuditRule, $index)"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Opportunities Audit rules categories-->
                                                    <!--Diagnostics Audit rules categories-->
                                                    <div class="row lxrm-padding-none auditCategorieWrapper" ng-if="psiCTRL.mobileDiagnosticsList !== null && psiCTRL.mobileDiagnosticsList.length > 0">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <p>
                                                                        <span class="lxrm-bold metric-title lxrm-sub-header-2"><spring:message code="PageSpeedInsights.Diagnostics" />&nbsp;&nbsp;</span>
                                                                        <span class="tool-info" ng-if="psiCTRL.mobilePageSpeedInsight.diagnosticsDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <span id="mobileDiagnosticsDescTag">{{getPreviewContent(psiCTRL.mobilePageSpeedInsight.diagnosticsDesc, 'mobileDiagnosticsDescTag', -1)}}</span>
                                                                        </span>
                                                                        <span class="tool-info" ng-if="!psiCTRL.mobilePageSpeedInsight.diagnosticsDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <spring:message code="PageSpeedInsights.Diagnostics.Def"/>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-12 lxrm-padding-none">
                                                                    <div class="accordion" id="mobileDiagnosticsWrapper">
                                                                        <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.mobilePageSpeedInsight.auditRules track by $index">-->
                                                                        <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.mobileDiagnosticsList| orderBy: '-rulePriority' track by $index">
                                                                            <div class="card-header" id="mobileDiagnosticsRule{{$index}}">
                                                                                <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#mobileDiagnosticsRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                        <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="col-xs-10 lxrm-padding-none lxrm-sm-pl lxrm-bold lxrm-pl-none lxrm-sub-header-3 text-left">
                                                                                        <span class="lxrm-pl-none metric-title">
                                                                                            {{deviceAuditRule.title}}
                                                                                        </span>
                                                                                        <span ng-if="deviceAuditRule.displayValue" class="{{deviceAuditRule.scoreColor}} lxrm-pl-none">
                                                                                            &nbsp;&nbsp;-&nbsp;&nbsp;{{deviceAuditRule.displayValue}}
                                                                                        </span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown text-right">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                                <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#mobileDiagnosticsRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1"></span>
                                                                                    <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                        {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown text-right">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div id="mobileDiagnosticsRuleBody{{$index}}"  class="collapse" aria-labelledby="mobileDiagnosticsRule{{$index}}" data-parent="#mobileDiagnosticsWrapper">
                                                                                <div class="card-body">
                                                                                    <div class="col-xs-12 lxrm-padding-none">
                                                                                        <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-description psi-rule-warning">
                                                                                            <ul class="lxrm-bold lxrm-p-0" style="margin: 0">Warnings:</ul>
                                                                                            <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.description" class="col-xs-12 tool-info psi-description" id="mobileDiagnosticsAuditRuleDesc{{$index}}">
                                                                                            {{getPreviewContent(deviceAuditRule.descriptionHTML, 'mobileDiagnosticsAuditRuleDesc', $index)}}
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info psi-description">
                                                                                            <div class="row">
                                                                                                <div class="col-xs-12 image-txt-container">
                                                                                                    <img src="/images/wordpress.png"/>
                                                                                                    <span class="lxrm-p-1" id="mobileDiagnosticsAuditRuleStackDef{{$index}}">
                                                                                                        {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'mobileDiagnosticsAuditRuleStackDef', $index)}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="mobileDiagnosticsAuditRule{{$index}}" ng-init="constructAuditRuleTable('mobileDiagnosticsAuditRule', deviceAuditRule, $index)"></div>
                                                                                        <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="mobileDiagnosticsAuditCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('mobileDiagnosticsAuditCriticalRule', deviceAuditRule, $index)"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Diagnostics Audit rules categories-->
                                                    <!--PassedAudits Audit title wrapper section-->
                                                    <div class="row" ng-if="psiCTRL.mobilePassedAuditsList !== null && psiCTRL.mobilePassedAuditsList.length > 0">
                                                        <div class="accordion" id="mobilePassedAuditCompleteWrapper">
                                                            <div class="card">
                                                                <div class="card-header" id="mobilePassedAuditTitleWrapper">
                                                                    <button type="button" class="btn btn-link col-xs-12" style="text-decoration: none;text-align: left;" data-toggle="collapse" data-target="#mobilePassedAuditsRulesWrapper"> 
                                                                        <span class="col-sm-11 col-xs-10 text-left metric-title lxrm-sub-header-2 lxrm-bold lxrm-pl-none"><spring:message code="PageSpeedInsights.Passed" /> &nbsp;&nbsp; ({{psiCTRL.mobilePassedAuditsList.length}}) </span>
                                                                        <span class="col-sm-1 col-xs-2 angleUpDown text-right" style="padding:0.3em">
                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                        </span>
                                                                    </button>    
                                                                </div>
                                                                <div id="mobilePassedAuditsRulesWrapper" class="collapse" aria-labelledby="mobilePassedAuditTitleWrapper" data-parent="#mobilePassedAuditCompleteWrapper">
                                                                    <div class="card-body col-xs-12">
                                                                        <div class="col-xs-12 lxrm-padding-none auditCategorieWrapper">
                                                                            <div class="col-xs-12 lxrm-m-0">
                                                                                <div class="row ">
                                                                                    <div class="col-xs-12 lxrm-p-0">
                                                                                        <div class="accordion" id="mobilePassedAuditsWrapper">
                                                                                            <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.mobilePageSpeedInsight.auditRules track by $index">-->
                                                                                            <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.mobilePassedAuditsList| orderBy: '-score' track by $index">
                                                                                                <div class="card-header" id="mobilePassedAuditsRule{{$index}}">
                                                                                                    <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#mobilePassedAuditsRuleBody{{$index}}"> 
                                                                                                        <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                                            <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                        <span class="col-xs-10 lxrm-padding-none lxrm-bold lxrm-sub-header-3 text-left lxrm-sm-pl">
                                                                                                            <span class="lxrm-pl-none metric-title">
                                                                                                                {{deviceAuditRule.title}}
                                                                                                            </span>
                                                                                                            <span ng-if="deviceAuditRule.displayValue" class="{{deviceAuditRule.scoreColor}} lxrm-pl-none">
                                                                                                                &nbsp;&nbsp;-&nbsp;&nbsp;{{deviceAuditRule.displayValue}}
                                                                                                            </span>
                                                                                                        </span>
                                                                                                        <span class="col-xs-1 angleUpDown text-right" >
                                                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                    </button>
                                                                                                    <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#mobilePassedAuditsRuleBody{{$index}}"> 
                                                                                                        <span class="col-xs-1"></span>
                                                                                                        <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                                            {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                                        </span>
                                                                                                        <span class="col-xs-1 angleUpDown text-right"  >
                                                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div id="mobilePassedAuditsRuleBody{{$index}}"  class="collapse" aria-labelledby="mobilePassedAuditsRule{{$index}}" data-parent="#mobilePassedAuditsWrapper">
                                                                                                    <div class="card-body">
                                                                                                        <div class="col-xs-12 lxrm-padding-none">
                                                                                                            <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-rule-warning">
                                                                                                                <ul class="lxrm-bold"style="padding: 0;margin: 0">Warnings:</ul>
                                                                                                                <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                                            </div>
                                                                                                            <div ng-if="deviceAuditRule.description" class="col-xs-12 tool-info psi-description" id="mobilePassedRuleDesc{{$index}}">
                                                                                                                {{getPreviewContent(deviceAuditRule.descriptionHTML, 'mobilePassedRuleDesc', $index)}}
                                                                                                            </div>
                                                                                                            <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-xs-12 image-txt-container">
                                                                                                                        <img src="/images/wordpress.png"/>
                                                                                                                        <span class="lxrm-p-1" id="mobilePassedRuleStackDesc{{$index}}">
                                                                                                                            {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'mobilePassedRuleStackDesc', $index)}}
                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="mobilePassedRule{{$index}}" ng-init="constructAuditRuleTable('mobilePassedRule', deviceAuditRule, $index)"></div>
                                                                                                            <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="mobilePassedCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('mobilePassedCriticalRule', deviceAuditRule, $index)"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--PassedAudits Audit rules categories-->
                                                        </div>
                                                    </div>
                                                    <!--EOF PassedAudits Audit rules categories-->
                                                </div><!--EOF tab middle md-12-->
                                                <div class="col-xs-0 col-md-3">
                                                    <!--Final screen shot of deskTop device will display only for above medium devices-->
                                                    <div class="row lxrm-p-1 lxrm-hidden-xs lxrm-hidden-sm" style="justify-content: center;text-align: center;">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="mobileFinalScreenShot" ng-init="constructScreenShots('mobile', 'final-screenshot', 'mobileFinalScreenShot')"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12" id="mobilDeviceErrorMessage" ng-switch-default>
                                            <p class="lxrm-bold lxrm-sub-header-2 text-center lxrm-p-1" id="mobileDeviceError"> 
                                                {{getPreviewContent(psiCTRL.mobilePageSpeedInsight.finalUrl, 'mobileDeviceError', -1)}}
                                            </p>
                                        </div>
                                    </div>
                                    <!--EOF of mobileDeviceWrapper -->
                                </div>
                                <div class="tab-pane" id="deskTopDeviceWrapper">
                                    <!--start of  deskTop DeviceWrapper parent row-->
                                    <div class="row lxrm-padding-none" ng-switch="psiCTRL.deskTopPageSpeedInsight.statusCode === 200">
                                        <div class="col-xs-12" ng-switch-when="true">
                                            <div class="row">
                                                <div class="col-xs-12 analysisTime">
                                                    <p style="padding: 1rem 2rem;">
                                                        <span class="tool-info">Analysis time</span>
                                                        <span class="lxrm-bold metric-title lxrm-sub-header-3 text-right"> : {{psiCTRL.deskTopPageSpeedInsight.analysisUTCTimestamp}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row border-bottom">
                                                <!--Device score-->
                                                <div class="col-xs-12 lxrm-bg-white lxrm-rounded-lg scoreWrapper" ng-init="generateScoreGraphForDevices('deskTop')">
                                                    <!-- Progress bar 1 -->
                                                    <div id="deskTopDeviceScore" class="progress lxrm-mx-auto">
                                                        <span class="progress-left">
                                                            <span class="progress-bar border-primary" id='deskTopProgressColorLeft'></span>
                                                        </span>
                                                        <span class="progress-right">
                                                            <span class="progress-bar border-primary" id='deskTopProgressColorRight'></span>
                                                        </span>
                                                        <div class="progress-value lxrm-w-100 lxrm-h-100 rounded-circle lxrm-d-flex lxrm-align-items-center lxrm-justify-content-center">
                                                            <div class="h2 font-weight-bold" id='deskTopProgressValue'></div>
                                                        </div>
                                                    </div>
                                                    <!-- END of  Progress bar 1-->
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-xs-0 col-md-2"></div>
                                                    <div class="col-xs-12 col-md-8">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p class="psiReviewURL lxrm-sub-header-2 lxrm-bold metric-title"><a href="{{psiCTRL.deskTopPageSpeedInsight.finalUrl}}" target="_blank">{{psiCTRL.deskTopPageSpeedInsight.finalUrl}}</a></p>
                                                            </div>
                                                            <div class="col-xs-12 scoreGuideWrapper">
                                                                <div class="row text-center">
                                                                    <div class="col-xs-0 col-md-3"></div> 
                                                                    <div class="col-xs-12 col-md-6 lightHouseScoreGuide metric-title tool-info lxrm-ptb-Half">
                                                                        <div class="col-xs-3">
                                                                            <span class="slow"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span>&nbsp;0&nbsp;-&nbsp;49&nbsp;</span>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <span class="average"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span>&nbsp;50&nbsp;-&nbsp;89&nbsp;</span>
                                                                        </div>
                                                                        <div class="col-xs-4 text-left lxrm-pl-none">
                                                                            <span class="fast"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                                            <span class="metric-title">&nbsp;90&nbsp;-&nbsp;100&nbsp;</span>
                                                                            <span> 
                                                                                &nbsp;
                                                                                <a href="https://developers.google.com/web/tools/lighthouse/v3/scoring" target="_blank" title="Lighthouse Scoring Guide">
                                                                                    <i class="fa fa-info-circle fa-2" aria-hidden="true" style="color:#333;"></i>
                                                                                </a>
                                                                                &nbsp;
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-0 col-md-3"></div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-0 col-md-2"></div>
                                                </div>
                                                <!--EOF of mobileDeviceWrapper parent row-->
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-0 col-md-1"></div>
                                                <div class="col-xs-12 col-md-8">
                                                    <!--Field Data Wrapper-->
                                                    <div class="row lxrm-p-1">
                                                        <div class="col-xs-12" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div ng-switch-when="true">
                                                                <span class="lxrm-bold lxrm-sub-header-2 metric-title">Field Data &nbsp; </span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.FIELD_DATA_PREFIX" /></span>
                                                                <span class="{{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPColor}} lxrm-bold tool-info">{{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataCategory| capitalize}}</span>
                                                                <span class="lxrm-regular tool-info"><spring:message code="PageSpeedInsights.FIELD_DATA_POSTFIX" /></span>
                                                            </div>
                                                            <div ng-switch-default>
                                                                <span class="lxrm-bold lxrm-sub-header-3 metric-title">Field Data &nbsp;</span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.NO_DATA_MSG" /> page.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 lxrm-p-1" ng-if="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPColor}}" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-8 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstContentfulPaint" /> (FCP)
                                                                            </span>
                                                                            <span class="col-xs-2 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPPercentile| millSecondsToSecond}} s
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="deskTopFieldDataFCP" ng-init="constructFCPGraph('deskTopFieldDataFCP', psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFCPdistributions)"></div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFIDColor}}" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFIDColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-7 lxrm-bold lxrm-sub-header-3  metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstInputDelay" /> (FID)
                                                                            </span>
                                                                            <span class="col-xs-3 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFIDColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFIDPercentile}} ms
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="deskTopFieldDataFID" ng-init="constructFIDGraph('deskTopFieldDataFID', psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.fieldDataFIDdistributions)"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Field Data Wrapper-->
                                                    <!--Orgin Summary Wrapper-->
                                                    <div class="row lxrm-p-1 orginSummaryDataWrapper">
                                                        <div class="col-xs-12" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div ng-switch-when="true">
                                                                <span class="lxrm-bold lxrm-sub-header-2 metric-title"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY" /> &nbsp; </span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.ORIGIN_SUMMARY_PREFIX" /></span>
                                                                <span class="{{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPColor}} lxrm-bold tool-info">{{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryCategory| capitalize}}</span>
                                                                <span class="lxrm-regular tool-info"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY_POSTFIX" /></span>
                                                            </div>
                                                            <div ng-switch-default>
                                                                <span class="lxrm-bold tool-info metric-title"><spring:message code="PageSpeedInsights.ORIGIN_SUMMARY" /> &nbsp;</span>
                                                                <span class="lxrm-regular tool-info">&nbsp;-&nbsp;<spring:message code="PageSpeedInsights.NO_DATA_MSG" /> origin.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 lxrm-p-1" ng-if="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata" />'">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPColor}}" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-8 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstContentfulPaint" /> (FCP)
                                                                            </span>
                                                                            <span class="col-xs-2 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPPercentile| millSecondsToSecond}} s
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom lxrm-plr-1 graphWrapper" id="deskTopOrginSummaryFCP" ng-init="constructFCPGraph('deskTopOrginSummaryFCP', psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFCPdistributions)"></div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="col-xs-12 border-top lxrm-p-1">
                                                                        <div class="row">
                                                                            <span class="col-xs-1 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFIDColor}}" ng-switch="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFIDColor">
                                                                                <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span class="col-xs-7 lxrm-bold lxrm-sub-header-3 metric-title">
                                                                                <spring:message code="PageSpeedInsights.firstInputDelay" /> (FID)
                                                                            </span>
                                                                            <span class="col-xs-3 {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFIDColor}} lxrm-bold tool-info lxrm-padding-none text-right">
                                                                                {{psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFIDPercentile}} ms
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 border-bottom  lxrm-plr-1 graphWrapper" id="deskTopOrginSummaryFID" ng-init="constructFIDGraph('deskTopOrginSummaryFID', psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryFIDdistributions)"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12" ng-if="psiCTRL.deskTopPageSpeedInsight.realWorldFieldData.originSummaryCategory !== '<spring:message code="PageSpeedInsights.Nodata"/>'"  ng-init="hideDivData('.orginSummaryDataWrapper')"></div>
                                                        <!--EOF Orgin Summary Wrapper-->
                                                    </div>
                                                    <!--Lab Data-->
                                                    <div class="row lxrm-p-1">
                                                        <!--labdataWrapper-->
                                                        <div class="col-xs-12 labdataWrapper ">
                                                            <div class="row">
                                                                <div class="col-xs-12 lab-info">
                                                                    <div class="col-xs-4 col-md-6 text-left lxrm-bold metric-title lxrm-sub-header-2 lxrm-padding-none">Lab Data</div>
                                                                    <div class="col-xs-8 col-md-6 text-right lxrm-padding-none" style="padding:0em;">
                                                                        <div id="deskTopLabDataOptionsWrapper">
                                                                            <label class="btn btn-default btn-on btn-xs active" id="deskTopLabDataLessLabel">
                                                                                <input type="radio" name="deskTopLabData" checked="checked" value="deskTopLabDataLess"> View Less
                                                                            </label>
                                                                            <label class="btn btn-default btn-off btn-xs" id="deskTopLabDataMoreLabel">
                                                                                <input type="radio" name="deskTopLabData"  value="deskTopLabDataMore"> View More
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--EOf of labdata title-->
                                                                <!--labdata metrics info-->
                                                                <div class="col-xs-12 result-data" id="deskTopLabDataWrapper">
                                                                    <!--labdata metrics left-->
                                                                    <div class="col-xs-12 col-md-6 lxrm-padding-none labDataLeftCell">
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-contentful-paint-deskTop" ng-init="constructLabDataMetric('first-contentful-paint', 'divider', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="speed-index-deskTop" ng-init="constructLabDataMetric('speed-index', 'divider', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider-bottom lxrm-padding-none" id="interactive-deskTop" ng-init="constructLabDataMetric('interactive', 'divider-bottom', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                    </div>
                                                                    <!--Eof labdata metrics left-->
                                                                    <!--labdata metrics right-->
                                                                    <div class="col-xs-12 col-md-6 lxrm-padding-none labDataRightCell">
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-meaningful-paint-deskTop" ng-init="constructLabDataMetric('first-meaningful-paint', 'divider', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider lxrm-padding-none" id="first-cpu-idle-deskTop" ng-init="constructLabDataMetric('first-cpu-idle', 'divider', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                        <!--Metric start--->
                                                                        <div class="col-xs-12 divider-bottom lxrm-padding-none" id="max-potential-fid-deskTop" ng-init="constructLabDataMetric('max-potential-fid', 'divider-bottom', 'deskTop')"></div>
                                                                        <!--Metric end--->
                                                                    </div>
                                                                    <!--EOF labdata metrics right-->
                                                                </div>
                                                                <div class="col-xs-12" ng-init="activeDeskTopLabDataOptions()"></div>
                                                            </div>
                                                            <!--EOF labdataWrapper-->
                                                        </div>
                                                        <!--EOF labdata section-->
                                                    </div>
                                                    <!--EOF of Lab Data-->
                                                    <!--Image thumb nails screen shot loading-->
                                                    <div class="row lxrm-p-1">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="deskTopScreenShotThumbnails" ng-init="constructScreenShots('deskTop', 'screenshot-thumbnails', 'deskTopScreenShotThumbnails')"></div>
                                                    </div>
                                                    <!--Final screen shot of deskTop device will display only for small devices-->
                                                    <div class="row lxrm-p-1 lxrm-visible-xs lxrm-visible-sm" style="justify-content: center;text-align: center;">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="deskTopFinalScreenShotSmallDevices" ng-init="constructScreenShots('deskTop', 'final-screenshot', 'deskTopFinalScreenShotSmallDevices')"></div>
                                                    </div>
                                                    <!--Audit Rules categories-->
                                                    <!--Opportunities Audit rules categories-->
                                                    <div class="row lxrm-padding-none auditCategorieWrapper" ng-if="psiCTRL.deskTopOpportunitiesList !== null && psiCTRL.deskTopOpportunitiesList.length > 0">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <p>
                                                                        <span class="lxrm-bold metric-title lxrm-sub-header-2"><spring:message code="PageSpeedInsights.Opportunities" />&nbsp;&nbsp;</span>
                                                                        <span class="tool-info" ng-if="psiCTRL.deskTopPageSpeedInsight.loadOpportunitiesDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <span id="deskTopOpportunitiesDescTag">{{getPreviewContent(psiCTRL.deskTopPageSpeedInsight.loadOpportunitiesDesc, 'deskTopOpportunitiesDescTag', -1)}}</span>
                                                                        </span>
                                                                        <span class="tool-info" ng-if="!psiCTRL.deskTopPageSpeedInsight.loadOpportunitiesDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <spring:message code="PageSpeedInsights.Opportunities.Def"/>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="col-xs-6 metric-title tool-info lxrm-padding-none lxrm-bold">Opportunity</div>
                                                                    <div class="col-xs-6 metric-title tool-info text-right lxrm-padding-none lxrm-bold">Estimated Savings</div>
                                                                </div>
                                                                <div class="col-xs-12 lxrm-padding-none">
                                                                    <div class="accordion" id="deskTopOpportunityWrapper">
                                                                        <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.deskTopPageSpeedInsight.auditRules track by $index">-->
                                                                        <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.deskTopOpportunitiesList| orderBy: '-overallSavingsMs' track by $index">
                                                                            <div class="card-header" id="deskTopOpportunityRule{{$index}}">
                                                                                <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12 ruleButton" data-toggle="collapse" data-target="#deskTopOpportunityRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                        <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="col-xs-9 col-md-8 lxrm-bold lxrm-sub-header-3 text-left lxrm-sm-pl lxrm-pl-none metric-title">
                                                                                        {{deviceAuditRule.title}}
                                                                                    </span>
                                                                                    <span class="col-xs-1 col-md-2 {{deviceAuditRule.scoreColor}} lxrm-bold tool-info lxrm-padding-none text-right" data-placement="bottom" data-toggle="tooltip" title="{{deviceAuditRule.displayValue}} ({{deviceAuditRule.overallSavingsMs}})">
                                                                                        {{deviceAuditRule.overallSavingsMs| millSecondsToSecond}} s
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                                <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#deskTopOpportunityRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1"></span>
                                                                                    <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                        {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div id="deskTopOpportunityRuleBody{{$index}}"  class="collapse" aria-labelledby="deskTopOpportunityRule{{$index}}" data-parent="#deskTopOpportunityWrapper">
                                                                                <div class="card-body">
                                                                                    <div class="col-xs-12 lxrm-padding-none">
                                                                                        <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-description psi-rule-warning">
                                                                                            <ul class="lxrm-bold lxrm-p-0" style="margin: 0">Warnings:</ul>
                                                                                            <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.description" class="col-xs-12 tool-info psi-description" id="deskTopOpportunityAuditRuleDesc{{$index}}">
                                                                                            {{getPreviewContent(deviceAuditRule.descriptionHTML, 'deskTopOpportunityAuditRuleDesc', $index)}}
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info psi-description">
                                                                                            <div class="row">
                                                                                                <div class="col-xs-12 image-txt-container">
                                                                                                    <img src="/images/wordpress.png"/>
                                                                                                    <span class="lxrm-p-1" id="deskTopOpportunityAuditRuleStackDesc{{$index}}">
                                                                                                        {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'deskTopOpportunityAuditRuleStackDesc', $index)}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="deskTopOpportunityAuditRule{{$index}}" ng-init="constructAuditRuleTable('deskTopOpportunityAuditRule', deviceAuditRule, $index)"></div>
                                                                                        <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="deskTopOpportunityAuditCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('deskTopOpportunityAuditCriticalRule', deviceAuditRule, $index)"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Opportunities Audit rules categories-->
                                                    <!--Diagnostics Audit rules categories-->
                                                    <div class="row lxrm-padding-none auditCategorieWrapper" ng-if="psiCTRL.deskTopDiagnosticsList !== null && psiCTRL.deskTopDiagnosticsList.length > 0">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <p>
                                                                        <span class="lxrm-bold metric-title lxrm-sub-header-2"><spring:message code="PageSpeedInsights.Diagnostics" />&nbsp;&nbsp;</span>
                                                                        <span class="tool-info" ng-if="psiCTRL.deskTopPageSpeedInsight.diagnosticsDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <span id="deskTopDiagnosticsDescTag">{{getPreviewContent(psiCTRL.deskTopPageSpeedInsight.diagnosticsDesc, 'deskTopDiagnosticsDescTag', -1)}}</span>
                                                                        </span>
                                                                        <span class="tool-info" ng-if="!psiCTRL.deskTopPageSpeedInsight.diagnosticsDesc">
                                                                            <span class="lxrm-bold">-</span>&nbsp;&nbsp;
                                                                            <spring:message code="PageSpeedInsights.Diagnostics.Def"/>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-12 lxrm-padding-none">
                                                                    <div class="accordion" id="deskTopDiagnosticsWrapper">
                                                                        <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.deskTopPageSpeedInsight.auditRules track by $index">-->
                                                                        <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.deskTopDiagnosticsList| orderBy: '-rulePriority' track by $index">
                                                                            <div class="card-header" id="deskTopDiagnosticsRule{{$index}}">
                                                                                <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#deskTopDiagnosticsRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                        <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                        <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="col-xs-10 lxrm-padding-none lxrm-bold lxrm-pl-none lxrm-sm-pl lxrm-sub-header-3 text-left">
                                                                                        <span class="lxrm-pl-none metric-title">
                                                                                            {{deviceAuditRule.title}}
                                                                                        </span>
                                                                                        <span ng-if="deviceAuditRule.displayValue" class="{{deviceAuditRule.scoreColor}} lxrm-pl-none">
                                                                                            &nbsp;&nbsp;-&nbsp;&nbsp;{{deviceAuditRule.displayValue}}
                                                                                        </span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                                <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#deskTopDiagnosticsRuleBody{{$index}}"> 
                                                                                    <span class="col-xs-1"></span>
                                                                                    <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                        {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                    </span>
                                                                                    <span class="col-xs-1 angleUpDown">
                                                                                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                            <div id="deskTopDiagnosticsRuleBody{{$index}}"  class="collapse" aria-labelledby="deskTopDiagnosticsRule{{$index}}" data-parent="#deskTopDiagnosticsWrapper">
                                                                                <div class="card-body">
                                                                                    <div class="col-xs-12 lxrm-padding-none">
                                                                                        <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-description psi-rule-warning">
                                                                                            <ul class="lxrm-bold"style="padding: 0;margin: 0">Warnings:</ul>
                                                                                            <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.description" class="col-xs-12 tool-info psi-description" id="deskTopDiagnosticsAuditRuleDesc{{$index}}">
                                                                                            {{getPreviewContent(deviceAuditRule.descriptionHTML, 'deskTopDiagnosticsAuditRuleDesc', $index)}}
                                                                                        </div>
                                                                                        <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info psi-description">
                                                                                            <div class="row">
                                                                                                <div class="col-xs-12 image-txt-container">
                                                                                                    <img src="/images/wordpress.png"/>
                                                                                                    <span class="lxrm-p-1" id="deskTopDiagnosticsAuditRuleStackDesc{{$index}}">
                                                                                                        {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'deskTopDiagnosticsAuditRuleStackDesc', $index)}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="deskTopDiagnosticsAuditRule{{$index}}" ng-init="constructAuditRuleTable('deskTopDiagnosticsAuditRule', deviceAuditRule, $index)"></div>
                                                                                        <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="deskTopDiagnosticsAuditCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('deskTopDiagnosticsAuditCriticalRule', deviceAuditRule, $index)"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--EOF Diagnostics Audit rules categories-->
                                                    <!--PassedAudits Audit title wrapper section-->
                                                    <div class="row" ng-if="psiCTRL.deskTopPassedAuditsList !== null && psiCTRL.deskTopPassedAuditsList.length > 0">
                                                        <div class="accordion" id="deskTopPassedAuditCompleteWrapper">
                                                            <div class="card">
                                                                <div class="card-header" id="deskTopPassedAuditTitleWrapper">
                                                                    <button type="button" class="btn btn-link col-xs-12" style="text-decoration: none;text-align: left;" data-toggle="collapse" data-target="#deskTopPassedAuditsRulesWrapper"> 
                                                                        <span class="col-sm-11 col-xs-10 text-left metric-title lxrm-sub-header-2 lxrm-bold lxrm-pl-none"><spring:message code="PageSpeedInsights.Passed" /> &nbsp;&nbsp; ({{psiCTRL.deskTopPassedAuditsList.length}}) </span>
                                                                        <span class="col-sm-1 col-xs-2 angleUpDown text-right">
                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                        </span>
                                                                    </button>    
                                                                </div>
                                                                <div id="deskTopPassedAuditsRulesWrapper" class="collapse" aria-labelledby="deskTopPassedAuditTitleWrapper" data-parent="#deskTopPassedAuditCompleteWrapper">
                                                                    <div class="card-body col-xs-12">
                                                                        <div class="col-xs-12 lxrm-padding-none auditCategorieWrapper">
                                                                            <div class="col-xs-12 lxrm-m-0">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12 lxrm-p-0">
                                                                                        <div class="accordion" id="deskTopPassedAuditsWrapper">
                                                                                            <!--<div class="card" ng-repeat="deviceAuditRule in psiCTRL.deskTopPageSpeedInsight.auditRules track by $index">-->
                                                                                            <div class="card cardWrapper" ng-repeat="deviceAuditRule in psiCTRL.deskTopPassedAuditsList| orderBy: '-score' track by $index">
                                                                                                <div class="card-header" id="deskTopPassedAuditsRule{{$index}}">
                                                                                                    <button ng-if="!deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#deskTopPassedAuditsRuleBody{{$index}}"> 
                                                                                                        <span class="col-xs-1 text-left {{deviceAuditRule.scoreColor}}" ng-switch="deviceAuditRule.scoreColor">
                                                                                                            <i ng-switch-when="fast" class="fa fa-circle" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="average" class="fa fa-stop" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="slow" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                                                                            <i ng-switch-when="none" class="fa fa-circle" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                        <span class="col-xs-10 lxrm-padding-none lxrm-bold lxrm-sub-header-3 text-left lxrm-sm-pl">
                                                                                                            <span class="lxrm-pl-none metric-title" style="float: left;">
                                                                                                                {{deviceAuditRule.title}}
                                                                                                            </span>
                                                                                                            <span ng-if="deviceAuditRule.displayValue" class="{{deviceAuditRule.scoreColor}} lxrm-pl-none" style="float: left;">
                                                                                                                &nbsp;&nbsp;-&nbsp;&nbsp;{{deviceAuditRule.displayValue}}
                                                                                                            </span>
                                                                                                        </span>
                                                                                                        <span class="col-xs-1 angleUpDown">
                                                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                    </button>
                                                                                                    <button ng-if="deviceAuditRule.errorMessage"  type="button" class="lxrm-padding-none btn btn-link col-xs-12" data-toggle="collapse" data-target="#deskTopPassedAuditsRuleBody{{$index}}"> 
                                                                                                        <span class="col-xs-1"></span>
                                                                                                        <span class="col-xs-10 lxrm-bold lxrm-sub-header-3 lxrm-pl-none metric-title  text-left">
                                                                                                            {{deviceAuditRule.title}} &nbsp;<span class="slow lxrm-bold"><spring:message code="PageSpeedInsights.ERROR"/></span>
                                                                                                        </span>
                                                                                                        <span class="col-xs-1 angleUpDown">
                                                                                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                                                        </span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div id="deskTopPassedAuditsRuleBody{{$index}}"  class="collapse" aria-labelledby="deskTopPassedAuditsRule{{$index}}" data-parent="#deskTopPassedAuditsWrapper">
                                                                                                    <div class="card-body">
                                                                                                        <div class="col-xs-12 lxrm-padding-none">
                                                                                                            <div ng-if="deviceAuditRule.warnings !== null && deviceAuditRule.warnings.length > 0" class="col-xs-12 tool-info average psi-description psi-rule-warning">
                                                                                                                <ul class="lxrm-bold"style="padding: 0;margin: 0">Warnings:</ul>
                                                                                                                <li class="lxrm-bold warning-li" ng-repeat="warning in deviceAuditRule.warnings track by $index">{{warning}}</li>
                                                                                                            </div>
                                                                                                            <div ng-if="deviceAuditRule.description" class="col-xs-12 tool-info psi-description" id="deskTopPassedRuleDesc{{$index}}">
                                                                                                                {{getPreviewContent(deviceAuditRule.descriptionHTML, 'deskTopPassedRuleDesc', $index)}}
                                                                                                            </div>
                                                                                                            <div ng-if="deviceAuditRule.stackPackInfoHTML" class="col-xs-12 tool-info psi-description">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-xs-12 image-txt-container">
                                                                                                                        <img src="/images/wordpress.png"/>
                                                                                                                        <span class="lxrm-p-1" id="desktopPassedRuleStackDesc{{$index}}">
                                                                                                                            {{getPreviewContent(deviceAuditRule.stackPackInfoHTML, 'desktopPassedRuleStackDesc', $index)}}
                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12 table-responsive" ng-if="deviceAuditRule.tableHeadersRow.length > 0" id="deskTopPassedRule{{$index}}" ng-init="constructAuditRuleTable('deskTopPassedRule', deviceAuditRule, $index)"></div>
                                                                                                            <div class="col-xs-12 lxrm-pl-none" ng-if="deviceAuditRule.criticalRequestChain.length > 0" id="deskTopPassedCriticalRule{{$index}}" ng-init="constructAuditRuleCriticalChainList('deskTopPassedCriticalRule', deviceAuditRule, $index)"></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--PassedAudits Audit rules categories-->
                                                        </div>
                                                    </div>
                                                    <!--EOF PassedAudits Audit rules categories-->
                                                </div><!--EOF tab middle md-12-->
                                                <div class="col-xs-0 col-md-3">
                                                    <!--Final screen shot of deskTop device will display only for above medium devices-->
                                                    <div class="row lxrm-p-1 lxrm-hidden-xs lxrm-hidden-sm" style="justify-content: center;text-align: center;">
                                                        <div class="col-xs-12 lxrm-thumbnail" id="deskTopFinalScreenShot" ng-init="constructScreenShots('deskTop', 'final-screenshot', 'deskTopFinalScreenShot')"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12" id="mobilDeviceErrorMessage" ng-switch-default>
                                            <p class="lxrm-bold lxrm-sub-header-2 text-center lxrm-p-1" id="deskTopDeviceError">
                                                {{getPreviewContent(psiCTRL.deskTopPageSpeedInsight.finalUrl, 'deskTopDeviceError', -1)}}
                                            </p>
                                        </div>
                                    </div>
                                    <!--EOF of deskTopDeviceWrapper -->
                                </div>
                                <!--EOF tab-content-->
                            </div>
                            <!--EOF tabbable-line-->
                        </div>
                        <!--EOF tabbable-panel-->
                    </div>
                </div>
                <!--Display error message from API-->
                <div class="col-xs-12" ng-switch-default>
                    <p class="lxrm-bold metric-title lxrm-sub-header-2 text-center" id="lightHouseErrorMessage" style="padding: 3rem 1.5rem;margin: 0 !important;">
                        {{getPreviewContent(psiCTRL.deskTopPageSpeedInsight.finalUrl, 'lightHouseErrorMessage', -1)}}
                    </p>
                </div>
                <div class="col-xs-12" ng-if="((psiCTRL.mobilePageSpeedInsight.statusCode === 200 || psiCTRL.deskTopPageSpeedInsight.statusCode === 200) && psiCTRL.isDataPulled)" ng-init="activeAngleUpDown()">
                    <div class="col-xs-12 clearfixdiv"></div>
                    <div class="row download-report-row d-flex-justify-center">
                        <div class="col-xs-0 col-md-1"></div>
                        <div class="col-xs-12 col-md-8 download-report-div  lxrm-p-1">
                            <div class="col-md-4 col-xs-12 lxrm-padding-none tool-info" style="padding: 0;">
                                <p style="margin: 3% 0;">Download complete report here: </p>
                            </div>
                            <div class="col-md-7 col-xs-10" style="padding: 0;">
                                <div class="input-group">
                                    <input type="text" class="form-control" title="Enter your email id" placeholder="Enter your email address" name="userPSIEmail" id="userPSIEmail" value="${userH.userName}">
                                    <div class="input-group-btn">
                                        <button id="sendPSIReportToMail" class="btn btn-default" ng-click="sendReportToMail();" style="border: 1px #ccc solid !important;"><i class="glyphicon glyphicon-send"></i></button>
                                    </div>
                                </div>
                                <div id="mailSentStatus" class="emptyData tool-info"></div>
                            </div>
                            <div class="col-md-1 col-sm-2 col-xs-2">
                                <div id="downloadPSIReportId" ng-click="downloadPSIReport()"  ng-disabled="downLoadDisabled"  style="font-size: 20px;cursor: pointer;padding-top: 0.1em;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt mt-1"></div>
                            </div>
                        </div>
                        <div class="col-xs-0 col-md-3 visible-md hidden-sm"></div>
                    </div>
                    <!--Eof of  resultRow-->
                </div>
                <div class="col-xs-12 clearfixdiv" ng-if="psiCTRL.mobilePageSpeedInsight.statusCode === 200 && psiCTRL.deskTopPageSpeedInsight.statusCode === 200"></div>
                <!--Eof of  resultWrapper-->
            </div>
        </div>

        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <div class="clearfixdiv"></div>
        <%@include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <!--<div class="clearfixdiv"></div>-->
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>