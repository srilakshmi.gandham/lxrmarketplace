<%-- 
    Document   : payment
    Created on : 6 Oct, 2017, 10:43:15 AM
    Author     : netelixir
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/payment.css">
        <%
            Object[] vals = (Object[]) session.getAttribute("valsObjArray");
            pageContext.setAttribute("vals", vals);
            if (session.getAttribute("geoLocation") != null) {
                HashMap<String, String> geoLocation = (HashMap<String, String>) session.getAttribute("geoLocation");
                pageContext.setAttribute("geoLocation", geoLocation);
            }
        %>
        <script type="text/javascript">
            $(document).ready(function (event) {
                $("#submit").on('click', function () {
                    $("#paymentErrorMessage").empty();
                    var status = validateData();
                    if (!status) {
                        return false;
                    }
                    var $this = $(this);
                    $this.button('loading');
                    var f = $("#marketplaceCreditCard");
                    f.attr('action', "/lxrm-payment.html?submit='submit'");
                    f.submit();
                });

                $("#cancel").on('click', function () {
                    var f = $("#marketplaceCreditCard");
                    f.attr('action', "/lxrm-payment.html?cancel=cancel");
                    f.submit();
                });
                $('#promo').keypress(function (ev) {
                    if (ev.which === 13) {
                        return false;
                    }
                });
            });
            function checkPromoCode(param) {
                $(".card-error-msg").hide();
                $("#expireMonth").removeClass("inputErr");
                $("#expireYear").removeClass("inputErr");
                var promoCode = $('#promo').val();
                if (promoCode.trim() !== null && promoCode.trim().length > 0) {
                    var $this = $(param);
                    $this.button('loading');
                    var params = {promo: promoCode};
                    $.getJSON("/lxrm-payment.html?checkPromo='promocode'", params,
                            function (data) {
                                $this.button('reset');
                                if (data[0] === 1) {
                                    $('#promoMsg').text(data[1]);
                                    $('#promoMsg').css('color', 'green');
                                    $('#promoMsg').removeClass("lxr-danger");
                                    document.getElementById("promo-apply").style.display = 'block';
                                    document.getElementById("promo-check").style.display = 'none';
                                    $("#termsofservice").css("margin-bottom", "14%");
                                } else {
                                    $('#promoMsg').text(data[1]);
                                    $('#promoMsg').addClass("lxr-danger");
                                    document.getElementById("promo-apply").style.display = 'none';
                                    document.getElementById("promo-check").style.display = 'block';
                                }
                            }
                    , "json");
                } else {
                    $('#promoMsg').text("Please enter promo code.");
                    $('#promoMsg').addClass("lxr-danger");
                }
                return false;
            }
            function applyPromoCode(param) {
                var $this = $(param);
                $this.button('loading');
                var f = $("#marketplaceCreditCard");
                f.attr('action', "/lxrm-payment.html?applyPromo='applyPromo'");
                f.submit();
            }
            function ValidateCard(e) {
                var t = parseInt(e.substring(e.length - 1, e.length));
                var n = e.substring(0, e.length - 1);
                if (Calculate(n) === parseInt(t)) {
                    return true;
                }
                return false;
            }
            function validateData() {
                $("#paymentErrorMessage").empty();
                $(".inputErr").removeClass("inputErr");
                $(".card-error-msg").empty();
                var validationSuccess = true;
                var firstName = $("#firstName").val();
                var cardNo = $("#cardNo").val();
                var expireMonth = $("#expireMonth").val();
                var expireYear = $("#expireYear").val();
                var cvv = $("#cvv").val();
//                var postalCode = $("#postalCode").val();
                var bilName = $("#billingName").val();
                var bilEmail = $("#billingEmail").val();

                console.log("bilEmail.trim() " + bilEmail.trim() + ", inds:: " + bilEmail.trim().indexOf('@'));
                console.log("firstName:: " + firstName.trim() + ", bilName:: " + bilName.trim());
                console.log("expireMonth:: " + expireMonth+ ", expireYear::  " + expireYear+", cvv:: "+cvv);

                if (firstName.trim() === "") {
//                    $("#firstName").addClass("inputErr");
                    $("#firstName").val(firstName.trim());
                    $("#errorFirstName").html("Please enter name on Credit / Debit card");
                    document.getElementById('errorFirstName').style.display = 'block';
                    validationSuccess = false;
                } else {
                    document.getElementById('errorFirstName').style.display = 'none';
                }

                if (!ValidateCard(cardNo) || cardNo.trim() === "") {
                    if (cardNo.trim() === null || cardNo.trim() === "" || cardNo.trim() === " ") {
                        $("#errorCardNo").html("Please enter Credit / Debit card number");
                        document.getElementById('errorCardNo').style.display = 'block';
                        validationSuccess = false;
                    } else {
                        $("#crdtNo").empty();
                        $("#errorCardNo").html("Please enter a valid Credit / Debit card number");
                        document.getElementById('errorCardNo').style.display = 'block';
                        validationSuccess = false;
                    }
                } else {
//                    document.getElementById('crdtNo').style.display = 'block';
                    $("#type").val(getCardType(cardNo));
                    document.getElementById('errorCardNo').style.display = 'none';
                }
                var eM = parseInt(expireMonth, 10);
                if (isNaN(eM) || eM < 1 || eM > 12) {
                    $("#expireMonth").addClass("inputErr");
                    validationSuccess = false;
                }
                var eY = parseInt(expireYear, 10);
                if (isNaN(eY) || eY < 2019 || eY > 10000) {
                    $("#expireYear").addClass("inputErr");
                    validationSuccess = false;
                }
                var validCardDate = getPaymentCardValidation();
                console.log("validCardDate " + validCardDate);
                if (!validCardDate) {
                    validationSuccess = false;
                    $("#cardExpiryError").show();
                    $("#cardExpiryError").empty();
                    $("#cardExpiryError").html("Please enter a valid expiry date");
                    $("#expireMonth").addClass("inputErr");
                    $("#expireYear").addClass("inputErr");
                } else {
                    $("#cardExpiryError").empty();
                    $("#cardExpiryError").hide();
                }
                var cvvInt = parseInt(cvv, 10);
                if (cvv === "") {
//                    $("#cvv").addClass("inputErr");
                    $("#errorCVV").html("Please enter CVV");
                    document.getElementById('errorCVV').style.display = 'block';
                    validationSuccess = false;
                } else if (isNaN(cvvInt) || cvvInt > 10000) {
//                    $("#cvv").addClass("inputErr");
                    $("#errorCVV").html("Please enter a valid CVV");
                    document.getElementById('errorCVV').style.display = 'block';
                    validationSuccess = false;
                } else {
                    document.getElementById('errorCVV').style.display = 'none';
                }


                if (bilName.trim() === "") {
                    $("#errorBillingName").html("Please enter name for billing");
                    document.getElementById('errorBillingName').style.display = 'block';
                    validationSuccess = false;
                } else {
                    document.getElementById('errorBillingName').style.display = 'none';
                }

                if (bilEmail.trim() === "") {
                    $("#errorBillingEmail").html("Please enter email to send invoice");
                    document.getElementById('errorBillingEmail').style.display = 'block';
                    validationSuccess = false;
                } else if (!isValidMail(bilEmail.trim())) {
                    $("#errorBillingEmail").html("Please enter a valid email");
                    document.getElementById('errorBillingEmail').style.display = 'block';
                    validationSuccess = false;
                } else {
                    document.getElementById('errorBillingEmail').style.display = 'none';
                }

                return validationSuccess;
            }

            function getCardType(creditCardNo) {
                var type = "unknown";
                if (/^5[1-5][0-9]{14}$/.test(creditCardNo)) {
                    type = "mastercard";
                } else if (/^4[0-9]{12}(?:[0-9]{3})?$/.test(creditCardNo)) {
                    type = "visa";
                } else if (/^3[47][0-9]{13}$/.test(creditCardNo)) {
                    type = "amex";
                } else if (/^(?:2131|1800|35\d{3})\d{11}$/.test(creditCardNo)) {
                    type = "jcb";
                } else if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(creditCardNo)) {
                    type = "dinnersclub";
                } else if (/^6(?:011|5[0-9]{2})[0-9]{12}$/.test(creditCardNo)) {
                    type = "discover";
                }
                return type;
            }

            function Calculate(e) {
                var t = 0;
                for (i = 0; i < e.length; i++) {
                    t += parseInt(e.substring(i, i + 1));
                }
                var n = new Array(0, 1, 2, 3, 4, -4, -3, -2, -1, 0);
                for (i = e.length - 1; i >= 0; i -= 2) {
                    var r = parseInt(e.substring(i, i + 1));
                    var s = n[r];
                    t += s;
                }
                var o = t % 10;
                o = 10 - o;
                if (o === 10) {
                    o = 0;
                }
                return o;
            }

            function getPaymentCardValidation() {
                var isCardValid = false;
                var today = new Date();
                var expireMonth = parseInt($("#expireMonth").val());
                var expireYear = parseInt($("#expireYear").val());
                if ((expireMonth === 0 || expireYear === 0) || (expireYear < today.getFullYear() ||
                        expireMonth > 12 || (expireMonth < (today.getMonth() + 1) && expireYear === today.getFullYear()))) {
                    isCardValid = false;
                } else {
                    isCardValid = true;
                }
                console.log("isCardValid:: " + isCardValid);
                return isCardValid;
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container payment-main-div">
            <form method="POST" id="marketplaceCreditCard">
                <div class="col-xs-12">
                    <h2>PAY USING CREDIT / DEBIT CARD</h2>

                    <%--<c:if test="${vals[1] == 0}">--%>
                    <div class="col-xs-12 col-md-8">
                        <c:if test="${vals != null && fn:length(vals) gt 0}">
                            <c:choose>
                                <c:when test="${vals[1] == 0}">
                                    <h3>GET EXPERT HELP at $${vals[5]}</h3>
                                    <div class="col-xs-12 promo-section">
                                        <div class="col-xs-12 col-md-3"><p>HAVE PROMO CODE? </p></div>
                                        <div class="col-xs-12 col-md-6 padding-default">
                                            <input type="text" id="promo" class="form-control col-xs-12">
                                            <span class="col-xs-12" id="promoMsg" style="padding: 1% 0 0 0;"></span>
                                        </div>
                                        <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                                        <div class="col-xs-12 col-md-3 padding-default">
                                            <button type="button" class="col-xs-12 promo-button" id="promo-check" onclick="checkPromoCode(this);" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Verifying...">APPLY</button>
                                            <button type="submit" class="col-xs-12 promo-button" id="promo-apply" style="display: none;" onclick="applyPromoCode(this);" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Processing...">SUBMIT</button>
                                        </div>
                                    </div>
                                </c:when>
                                <c:otherwise><h3>${vals[7]} Tool Price at $${vals[5]}</h3></c:otherwise>
                            </c:choose>
                        </c:if>

                    </div>
                    <%--</c:if>--%>
                    <div class="col-xs-12 col-md-4 pull-right payment-gateway-section">
                        <div class="images-section">
                            <div class="paypalImg">
                                <img src="/resources/images/payment/lxrm-paypal-cards.png"
                                     alt="paypal cards" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="payment-details" style="padding: 2% 0;margin: 2% 0;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;">
                    <c:if test="${!errorMessage.equals('')}">
                        <div class="col-xs-12 text-center lxr-danger" id="paymentErrorMessage" style="margin-bottom: 2%;">${errorMessage}</div>
                    </c:if>
                    <div class="col-xs-12 col-md-6 input-div-section">
                        <div class="col-xs-12 input-div">
                            <div class="col-xs-12"><p>NAME ON CARD</p></div>
                            <div class="col-xs-12">
                                <input type="text" id="firstName" name="firstName" class="form-control col-xs-12" placeholder="FULL NAME" />
                                <span id="errorFirstName" class="lxr-danger card-error-msg" style="display: none;"></span>
                            </div>
                        </div>
                        <input type="hidden" id="type" name="type" />
                        <div class="col-xs-12 input-div">
                            <div class="col-xs-12"><p>CREDIT / DEBIT CARD NUMBER</p></div>
                            <div class="col-xs-12">
                                <input type="text" id="cardNo" name="cardNo" size="19" maxlength="19" autocomplete="off" class="form-control col-xs-12" placeholder="CARD NUMBER"/>
                                <span id="errorCardNo" class="lxr-danger card-error-msg" style="display: none;"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 input-div">
                            <div class="col-xs-8"><p>EXPIRY DATE</p></div>
                            <div class="col-xs-4" style="padding-left: 15px;"><p>CVV</p></div>
                            <div class="col-xs-8">
                                <div class="col-xs-5">
                                    <select id="expireMonth" name="expireMonth" class="form-control">
                                        <option value='01'>01</option>
                                        <option value='02'>02</option>
                                        <option value='03'>03</option>
                                        <option value='04'>04</option>
                                        <option value='05'>05</option>
                                        <option value='06'>06</option>
                                        <option value='07'>07</option>
                                        <option value='08'>08</option>
                                        <option value='09'>09</option>
                                        <option value='10'>10</option>
                                        <option value='11'>11</option>
                                        <option value='12'>12</option>
                                    </select> 
                                </div>
                                <div class="col-xs-7" style="padding-left: 15px;">
                                    <select id="expireYear" name="expireYear" class="form-control">
                                        <option value='2020'>2020</option>
                                        <option value='2021'>2021</option>
                                        <option value='2022'>2022</option>
                                        <option value='2023'>2023</option>
                                        <option value='2024'>2024</option>
                                        <option value='2025'>2025</option>
                                        <option value='2026'>2026</option>
                                        <option value='2021'>2027</option>
                                        <option value='2022'>2028</option>
                                        <option value='2023'>2029</option>
                                        <option value='2024'>2030</option>
                                        <option value='2025'>2031</option>
                                        <option value='2026'>2032</option>
                                        <option value='2021'>2033</option>
                                        <option value='2022'>2034</option>
                                        <option value='2023'>2035</option>
                                        <option value='2024'>2036</option>
                                        <option value='2025'>2037</option>
                                        <option value='2026'>2038</option>
                                        <option value='2021'>2039</option>
                                        <option value='2022'>2040</option>
                                        <option value='2023'>2041</option>
                                        <option value='2024'>2042</option>
                                        <option value='2025'>2043</option>
                                        <option value='2026'>2044</option>
                                        <option value='2021'>2045</option>
                                        <option value='2022'>2046</option>
                                        <option value='2023'>2047</option>
                                        <option value='2024'>2048</option>
                                        <option value='2025'>2049</option>
                                        <option value='2026'>2050</option>
                                    </select>
                                </div>
                                <p class="col-xs-12 lxr-danger card-error-msg" id="cardExpiryError" style="display: none;padding-left: 0;"></p>
                            </div>
                            <div class="col-xs-4" style="padding-left: 15px;">
                                <input type="password" id="cvv" name="cvv" size="4" maxlength="4" value="" class="form-control col-xs-12" placeholder="CVV"/>
                                <span id="errorCVV" class="lxr-danger card-error-msg" style="display: none;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 input-div-section">
                        <div class="col-xs-12 input-div">
                            <div class="col-xs-12"><p>BILLING NAME</p></div>
                            <div class="col-xs-12">
                                <input type="text" id="billingName" name="billingName" class="form-control col-xs-12" value="${userH.getName()}" placeholder="BILLING NAME"/>
                                <span id="errorBillingName" class="lxr-danger card-error-msg" style="display: none;"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 input-div">
                            <div class="col-xs-12"><p>BILLING EMAIL</p></div>
                            <div class="col-xs-12">
                                <input type="text" id="billingEmail" name="billingEmail" class="form-control col-xs-12" value="${userH.userName}" placeholder="EMAIL TO SEND INVOICE"/>
                                <span id="errorBillingEmail" class="lxr-danger card-error-msg" style="display: none;"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <c:if test="${vals != null && fn:length(vals) gt 0}">
                        <div class="col-xs-12 col-md-8">
                            <p>Your payment data is encrypted and secured. All amounts shown are in US Dollars.</p>
                            <c:choose>
                                <c:when test="${vals[1] == 0}">
                                    <p>By using LXRMarketplace services, you agree to the <a href="/terms-of-service.html" target="_blank;">Terms of Service</a></p>
                                </c:when>
                                <c:otherwise>
                                    <p> By downloading your report, you agree to the <a href="/terms-of-service.html" target="_blank;">Terms of Service</a>.</p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:if>
                    <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                    <div class="col-xs-12 col-md-4">
                        <button type="submit" class="col-xs-12 col-md-5 col-md-offset-1" id="cancel">CANCEL</button>
                        <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                        <button type="submit" class="col-xs-12 col-md-5 pull-right" id="submit" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Processing...">SUBMIT</button>
                        <!--<button type="submit" class="col-xs-12 col-md-5 pull-right" id="submit">SUBMIT</button>-->
                    </div>
                </div>
            </form>
        </div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>