<%-- 
    Document   : paymentThanks
    Created on : 9 Oct, 2017, 2:27:35 PM
    Author     : netelixir
--%>

<%@page import="lxr.marketplace.payment.PaymentModel"%>
<%@page import="lxr.marketplace.user.Login"%>
<%@page import="lxr.marketplace.util.Tool"%>
<%@page import="lxr.marketplace.payment.PaymentModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LXRMarketplace.com</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>


        <%
            Tool toolObject = null;
            boolean isFreePromoCodeApplied = false;
            Login userPayment = (Login) session.getAttribute("user");
            Object ul = session.getAttribute("userLoggedIn");
            PaymentModel paymentModel = null;
            paymentModel = (PaymentModel) session.getAttribute("paymentModel");
            boolean userLoggedIn = false;
            if (ul != null) {
                userLoggedIn = (Boolean) ul;
            }
            boolean redirectionStatus = false;
            if (session.getAttribute("relatedToolObj") != null) {
                redirectionStatus = true;
                toolObject = (Tool) session.getAttribute("relatedToolObj");
            } else if (session.getAttribute("toolObj") != null) {
                toolObject = (Tool) session.getAttribute("toolObj");
            }
            pageContext.setAttribute("toolObject", toolObject);
            if (session.getAttribute("freePromoCode") != null) {
                isFreePromoCodeApplied = (boolean) session.getAttribute("freePromoCode");
                pageContext.setAttribute("isFreePromoCodeApplied", isFreePromoCodeApplied);
            }
            Object[] vals = (Object[]) session.getAttribute("valsObjArray");
            pageContext.setAttribute("vals", vals);
            pageContext.setAttribute("redirectionStatus", redirectionStatus);
            pageContext.setAttribute("userPayment", userPayment);
//            pageContext.setAttribute("paymentModel", paymentModel);
        %>
        <%
            Tool successTool = (Tool) session.getAttribute("relatedToolObj");
            int toolIds = (Integer) vals[1];
            if (toolIds != 8 && toolIds != 20) {
                if (successTool != null && successTool.getToolId() != 38) {%>
        <meta http-equiv="refresh" content="30;url=<%=successTool.getToolLink()%>">
        <%} else { %>
        <meta http-equiv="refresh" content="30;url=/lxrmarketplace.html">
        <% }
            }%>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#ate-free-promocde-success-msg').hide();
                $("#sendmail").on('click', function () {
                    $("#msent").html("");
//                    var $this = $(this);
//                    $this.button('loading');
                    var sendPath = "/" + "${toolObject.toolLink}" + "?downloadFile='sendmail'";
                    window.location = sendPath;
//                    $this.button('reset');
                    return false;
                });
                $("#download").on('click', function () {
                    $("#msent").html("");
//                    var $this = $(this);
//                    $this.button('loading');
                    var downloadPath = "/" + "${toolObject.toolLink}" + "?downloadFile='downloadFile'";
                    window.location = downloadPath;
//                    $this.button('reset');
                    return false;
                });
                if (parentUrl.indexOf("ms") !== -1) {
                    $("#msent").html("Mail Sent Successfully");
                }
                console.log("emil:: " + ("${paymentModel.billingEmailId}"));
                console.log("name:: " + ("${paymentModel.billingUserName}"));
                console.log("vals[1]:: " + ("${vals[1]}"));
            <c:choose>
                <c:when test="${vals[1] ne null && vals[1] == 0}">
                    <c:choose>
                        <c:when test="${isFreePromoCodeApplied}">
                $('#ate-free-promocde-success-msg').show();
                $('#ate-payment-success-msg').hide();
                        </c:when>
                        <c:otherwise>
                $('#ate-free-promocde-success-msg').hide();
                $('#ate-payment-success-msg').show();
                        </c:otherwise>
                    </c:choose>
                $('#download-report-msg').hide();
                $('#reportBut').hide();
                $('#default-payment-msg').hide();
                $('#ask-expert-payment-msg').show();
                // For inserting user's question information
                    <c:if test="${redirectionStatus}">
                jQuery.post("/lxrm-ate-user-info.html",
                        function (data) {
                        });
                    </c:if>
                </c:when>
                <c:when test="${vals[1] ne null && (vals[1]  == 8 || vals[1]  == 20)}">
                console.log("In final when");
                /*For Payment success related to tool id 8/12 user is provided with 
                 * send mail and download tool report option.
                 * Page redirects to tool result page after time out.*/
                $('#ate-free-promocde-success-msg').hide();
                $('#ask-expert-payment-msg').hide();
                </c:when>
                <c:otherwise>
                window.location = "/lxrmarketplace.html";
                </c:otherwise>
            </c:choose>
            <%--<c:choose>
                <c:when test="${toolObject ne null}">
                    <c:if test="${mailSent ne null && mailSent != ''}">
                $("#msent").html("Mail Sent Successfully");
                    </c:if>
                </c:when>
                <c:otherwise>
                window.location = "/lxrmarketplace.html";
                </c:otherwise>
            </c:choose>--%>
            <c:if test="${vals[1] eq null || fn:length(vals) <= 0}">
                window.location = "/lxrmarketplace.html";
            </c:if>

            });
            function updateTask() {
                setTimeout('updateTime()', 1000);
            }
            function updateTime() {
                var sec = document.getElementById("seconds").innerHTML;
                var diff = sec - 1;
                if (diff > 0) {
                    document.getElementById("seconds").innerHTML = diff;
                }
                updateTask();
            }
        </script>
        <style>
            .main-center{
                margin-top: 30px;
                margin: 0 auto;
                max-width: 500px;
                padding: 10px 16px;
                background:#fff;
                color: black;
                text-shadow: none;
                border: 1px solid rgba(0,0,0,.15);
                border-radius: 4px;
                -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
                box-shadow: 0 6px 12px rgba(0,0,0,.175);
            }
            #reportBut button {
                background-color: #2f80ed;
                border-radius: 2px !important;
                border: 1px rgb(88, 142, 192) solid !important;
                padding:3%;
                color:white;
            }
            /*Only for mobile*/
            @media screen and (max-width: 992px) {
                .padding-none{
                    padding: 0;
                }
            }
        </style>
    </head>
    <body onload="updateTask();">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="clearfixdiv visible-lg"></div>
        <div class="container">
            <div class="row">
                <div class="main-center">
                    <form method="POST">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="row">
                                        <h2 style="color: green;">Your Payment Was Successful</h2>
                                    </div>
                                    <div id="ate-payment-success-msg">
                                        <p>Success! Thanks for your payment. An invoice has been sent to <span id="invoiceEmail" class="lxrm-bold">${paymentModel.billingEmailId}</span>.</p>
                                        <c:if  test="${vals[1] ne null && vals[1] == 0}">
                                            <p>
                                                Your question has been sent to our experts. An appropriate answer to your question will be sent to your email address <strong><%=userPayment.getUserName()%></strong>.    
                                            </p>
                                        </c:if>
                                        <p>If you have any questions, please contact our customer support at 
                                            <span>
                                                <a href="mailto:support@lxrmarketplace.com?subject=LXRMarketplace Customer Support"> support@lxrmarketplace.com</a>
                                            </span>
                                        </p>
                                    </div>
                                    <div id="ate-free-promocde-success-msg" style="display: none;">
                                        <p>
                                            Success! Your question has been sent to our experts. An appropriate answer to your question will be sent to your email address <strong><%=userPayment.getUserName()%></strong>.    
                                        </p>
                                        <p>If you have any questions, please contact our customer support at 
                                            <span>
                                                <a href="mailto:support@lxrmarketplace.com?subject=LXRMarketplace Customer Support"> support@lxrmarketplace.com</a>
                                            </span>
                                        </p>
                                    </div>

                                    <hr>
                                    <div class="col-xs-12" style="padding: 0;">
                                        <div class="tool-report-section">
                                            <p id="download-report-msg">Please download your report by clicking the links below :</p>
                                            <div class="col-xs-12" id="reportBut" style="padding: 0;">
                                                <div class="col-xs-12 col-md-6 padding-none">
                                                    <button class="col-xs-12" type="submit" id="download" title="DOWNLOAD REPORT" value="DOWNLOAD REPORT" 
                                                            data-loading-text="<span id='downloadSpin' class='fa fa-refresh fa-spin fa-fw'></span> Downloading..">
                                                        DOWNLOAD REPORT
                                                    </button>
                                                </div>
                                                <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                                                <div class="col-xs-12 col-md-6 padding-none">
                                                    <button class="col-xs-12" type="submit" id="sendmail" title="SEND REPORT" value="SEND REPORT" 
                                                            data-loading-text="<span id='sendMailSpin' class='fa fa-refresh fa-spin fa-fw'></span> Sending..">
                                                        SEND REPORT
                                                    </button>
                                                </div>
                                            </div>
                                            <div id="msent" style="padding: 1% 0;color: green;" class="col-xs-12 text-center"></div>
                                            <c:if test="${toolObject ne null || toolObject ne ''}">
                                                <p id="default-payment-msg" class="col-xs-12" style="padding: 2% 0;">
                                                    You can also access the current and previous reports from the 
                                                    <a href="/${toolObject.toolLink}">${toolObject.toolName}</a> page.
                                                </p>
                                                <p id="ask-expert-payment-msg" class="col-xs-12" style="padding: 2% 0;display: none;">
                                                    You will be redirected to previous tool page in 
                                                    <span id="seconds">30</span> seconds or Please 
                                                    <a href="/${toolObject.toolLink}"  id="redirect" style="text-decoration: none">
                                                        <span>Click here</span>
                                                    </a>
                                                    to go back to previous tool ${toolObject.toolName} page.
                                                </p>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
