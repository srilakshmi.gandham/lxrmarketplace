<!--
   Document   : Competitor Analysis
   Created on : Aug 1, 2017, 11:24:49 AM
   Author     : NE16T1213
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Free Competitor Analysis Tool - Competitor Website Analyzer</title>
        <meta name="description" content="Try our SEO Competitor Analysis tool now! Enter your competitor's URL into this , 
              and any targeted search phrase and the tool will provide a free analysis.">
        <meta name="keywords" content=" Competitor Analysis Tool,  Competitor Website Analyzer">
        <%@ page import="lxr.marketplace.competitoranalysis.CompetitorAnalysisResult, 
                 lxr.marketplace.competitoranalysis.CompetitorAnalysisTool"%>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/competitoras.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>

        <%
            int toolDBId = 7;
            CompetitorAnalysisTool competitorAnalysisTool = null;
            CompetitorAnalysisResult userAnalysis = null;
            List<CompetitorAnalysisResult> comptetiorAnalysisResultsList = null;
            boolean secondCompetitor = false;
            if (session.getAttribute("competitorAnalysistool") != null) {
                competitorAnalysisTool = (CompetitorAnalysisTool) session.getAttribute("competitorAnalysistool");
                pageContext.setAttribute("competitorAnalysisTool", competitorAnalysisTool);

                if (session.getAttribute("yoursite") != null) {
                    userAnalysis = (CompetitorAnalysisResult) session.getAttribute("yoursite");
                }
                pageContext.setAttribute("userAnalysis", userAnalysis);

                if (session.getAttribute("compsite") != null) {
                    comptetiorAnalysisResultsList = (List<CompetitorAnalysisResult>) session.getAttribute("compsite");
                    if (comptetiorAnalysisResultsList.size() > 0) {
                        pageContext.setAttribute("comptetiorAnalysis", comptetiorAnalysisResultsList);
                    }
                    if (comptetiorAnalysisResultsList.size() > 1) {
                        secondCompetitor = true;
                        pageContext.setAttribute("secondCompetitor", secondCompetitor);
                    }
                }
            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

                //Auto Download after user login success through header
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat('pdf');
                }
//                toolLimitExceeded();

                $("#errorMessage, #mailIdMessage").empty();

                $("#getResult").on('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    $("#errorMessage, #userDomainError,#keywordError,#compDomainError").empty();
                    var isValidAll = true;
                    if ($('#yourUrl').val().trim() === "" || $('#yourUrl').val() === null) {
                        $("#userDomainError").html("Please enter your URL.");
                        isValidAll = false;
                    }
                    if ($('#domainUrlList0').val().trim() === "" || $('#domainUrlList0').val() === null) {
                        $("#compDomainError").html("Please enter your competitor URL.");
                        isValidAll = false;
                    }
                    if ($('#searchPhrase').val().trim() === "" || $('#searchPhrase').val() === null) {
                        $("#keywordError").html("Enter search phrase.");
                        isValidAll = false;
                    }
                    if (isValidAll) {
                        var $this = $(this);
                        $this.button('loading');
                        var form = $("#competitorAnalysisTool");
                        form.attr('method', "POST");
                        form.attr('action', "seo-competitor-analysis-tool.html?getResult='getResult'");
                        form.submit();
                    }

                });

                /*Auto populating values navigation from  payment cancel page
                 if (parentUrl.indexOf("backToSuccessPage=") !== -1) {
                 autoPoPulateValues();
                 }*/
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) && (userAnalysis != null && comptetiorAnalysis != null)}">
                autoPoPulateValues();
                $('#yourUrl,#domainUrlList0,#domainUrlList1,#searchPhrase').addClass("gray");
//                $("#editImport1").show();
//                $("#editImport2").show();
//                $("#editImport3").show();
//                $("#editImport4").show();
            </c:if>
            <c:if test="${(notWorkingUrl != null)}">
                $("#errorMessage").show();
                $("#errorMessage").html('${notWorkingUrl}');
//                $("#yourUrl").val('');
//                $("#competitorUrl").val('');
//                $("#searchPhrase").val('');
//                autoPoPulateValues();
            </c:if>
                $('#userEmailId').keypress(function (ev) {
                    if (ev.which === 13) {
                        $('#mailReport').click();
                        return false;
                    }
                });
                $('#userEmailId').click(function () {
                    $('#mailIdMessage').empty();
                    $('#mailIdMessage').hide();
                    return false;
                });
                /*Functions related to download, send report to mail*/
                $("#mailReport").click(function () {
                    var emailid = $.trim($("#userEmailId").val());
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').css("color", "red");
                    if (!(emailid)) {
                        $('#mail-sent-status').html("Please enter your email id");
                        $("#userEmailId").focus();
                        return false;
                    } else if (!isValidMail(emailid)) {
                        $('#mail-sent-status').html("Please enter a valid email id");
                        $("#userEmailId").focus();
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/seo-competitor-analysis-tool.html?download='sendmail'&report-type='sendmail'&email=" + emailid,
                            processData: true,
                            data: {},
                            dataType: "json",
                            success: function (data) {
                            },
                            complete: function () {
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                            }
                        });
                    }
                });

                $('#yourUrl').on('click', function () {
                    $("#errorMessage").html("");
//                    $("#editImport1").hide();
                    $("#yourUrl").removeClass("gray");
                    $("#yourUrl").focus();

                });

                $('#domainUrlList0').on('click', function () {
                    $("#errorMessage").html("");
//                    $("#editImport2").hide();
                    $("#domainUrlList0").removeClass("gray");
                    $("#domainUrlList0").focus();
                });

                $('#domainUrlList1').on('click', function () {
                    $("#errorMessage").html("");
//                    $("#editImport3").hide();
                    $("#domainUrlList1").removeClass("gray");
                    $("#domainUrlList1").focus();
                });

                $('#searchPhrase').on('click', function () {
                    $("#errorMessage").html("");
//                    $("#editImport4").hide();
                    $("#searchPhrase").removeClass("gray");
                    $('#searchPhrase').focus();
                });

            });

            function autoPoPulateValues() {
                $("#yourUrl").val('${fn:trim(competitorAnalysisTool.yourUrl)}');
                $("#domainUrlList0").val('${fn:trim(competitorAnalysisTool.domainUrlList[0])}');
            <c:if test="${secondCompetitor}">
                $("#domainUrlList1").val('${fn:trim(competitorAnalysisTool.domainUrlList[1])}');
            </c:if>
                $("#searchPhrase").val('${fn:trim(competitorAnalysisTool.searchPhrase)}');
            }
            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    var form = $("#competitorAnalysisTool");
                    form.attr('method', "POST");
                    form.attr('action', "/seo-competitor-analysis-tool.html?download=download&report-type=" + reportType);
                    form.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {

                var url = $('#yourUrl').val();
                var competitorUrl = $('#competitorUrl').val();
                var domainUrlList0 = $("#domainUrlList0").val();
                var domainUrlList1 = $("#domainUrlList1").val();
                var searchPhrase = $('#searchPhrase').val();
                $.ajax({
                    type: "POST",
                    url: "/seo-competitor-analysis-tool.html?ate='competitorAnalysisATE'&domain=" + url + "&competitorUrl=" + competitorUrl + "&searchPhrase=" + searchPhrase + "&domainUrlList0=" + domainUrlList0 + "&domainUrlList1=" + domainUrlList1,
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
        <style type="text/css">
            .text-message-border-0 td{border-top: 0 !important;}
            .text-message-border-0 th{border-bottom: 0 !important;}
            .text-message-padding td{padding-top: 14px !important;}
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="competitorAnalysisTool" method="POST" cssClass="col-12" style="display: flex;">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-4" style="margin-left: auto;margin-right: auto;">
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="yourUrl"  type="text" cssClass="form-control input-lg srch-term"/>
                            <!--<span id="editImport1" class="glyphicon glyphicon-pencil lxrtool-pencil" ></span>-->
                        </div>
                        <span id="userDomainError" class="lxr-danger text-left" ></span>
                    </div>
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Competitor URL #1"  type="text" path="domainUrlList[0]" cssClass="form-control input-lg srch-term"/>
                            <!--<span id="editImport2" class="glyphicon glyphicon-pencil lxrtool-pencil" ></span>-->
                        </div>
                        <span id="compDomainError" class="lxr-danger text-left" ></span>
                    </div>
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Competitor URL #2 (Optional)"  type="text" path="domainUrlList[1]" cssClass="form-control input-lg srch-term"/>
                            <!--<span id="editImport3" class="fa fa-pencil lxrtool-pencil" ></span>-->
                        </div>
                        <!--<span id="compDomainError" class="lxr-danger text-left" ></span>-->
                    </div>
                    <div class="form-group">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Search Phrase" type="text" path="searchPhrase" cssClass="form-control input-lg srch-term"/>
                            <!--<span id="editImport4" class="glyphicon glyphicon-pencil lxrtool-pencil" ></span>-->
                        </div>
                        <span id="keywordError" class="lxr-danger text-left" ></span>
                    </div>
                    <div class="form-group text-center">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12" id="getResult"  data-loading-text="<i class='fa fa-refresh fa-spin'></i> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </div>
            </springForm:form>
            <div class="col-lg-12 text-center">
                <span id="errorMessage" class="lxr-danger text-center" >${notWorkingUrl}</span>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) && (userAnalysis != null && comptetiorAnalysis != null)}">
            <!--vertical tab system-->
            <div class="container-fluid">
                <div class="row lxrtool-tabs" >
                    <div class="tabs">
                        <!--1-->
                        <div class="tab ">
                            <button class="tab-toggle active lxrtool-tabs-heading">
                                <span>USABILITY</span>
                                <p>Usability is a mixture of how the interface, messaging, and intuitiveness of a functional process combine to create an experience that makes sense to users. </p>
                            </button>
                        </div>
                        <!--table-->
                        <div class="content active">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1" style="padding: 0px">
                                <div class="panel panel-default" >
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Usability Metrics Need Improvement Vs Competitors</span>
                                        <p>9 Important factors which reveal your issues comparing your website to your competitors, Follow the <strong  class="lxr-steps nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS</strong> to improve your visibility</p>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row">
                                            <div class="col-xs-12" style="padding: 0;">
                                                <table class="table">
                                                    <thead>
                                                        <tr class="text-message-border-0">
                                                            <th style="width: 16%;"></th>
                                                            <th><h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${userAnalysis.url}</h5></th>
                                                            <th><h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[0].url}</h5></th>
                                                                        <c:if test="${secondCompetitor}">
                                                                <th><h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[1].url}</h5></th>
                                                                        </c:if>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="text-message-border-0 lxrm-bold">
                                                            <td>PAGE SIZE</td>
                                                            <td>${userAnalysis.pageSize} Kb</td>
                                                            <td>${comptetiorAnalysis[0].pageSize} Kb</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].pageSize} Kb</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0 text-recommendation">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.pagesize" /></td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>WORD COUNT</td>
                                                            <td>${userAnalysis.totalWordCount}</td>
                                                            <td>${comptetiorAnalysis[0].totalWordCount}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].totalWordCount}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.wordcount" /></td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Image Count</td>
                                                            <td>${userAnalysis.imageCount}</td>
                                                            <td>${comptetiorAnalysis[0].imageCount}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].imageCount}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}">
                                                                <c:set var="alttext" value="${userAnalysis.containAlttext}"/>
                                                                <c:choose>
                                                                    <c:when test="${alttext > 0 &&  alttext == userAnalysis.imageCount}">
                                                                        <spring:message code="message.positive" />
                                                                    </c:when>
                                                                    <c:when test="${alttext > 0 &&  alttext != userAnalysis.imageCount}">
                                                                        <spring:message code="message.negative" arguments="${alttext}"/>
                                                                    </c:when>
                                                                    <c:when test="${alttext == 0 &&  userAnalysis.imageCount != 0}">
                                                                        <spring:message code="message.positive" />
                                                                    </c:when>
                                                                </c:choose>
                                                                <spring:message code="message.imagecount" />
                                                            </td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Link Count</td>
                                                            <td>${userAnalysis.linkCount}</td>
                                                            <td>${comptetiorAnalysis[0].linkCount}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].linkCount}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.linkcount" /></td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Flash</td>
                                                            <td>${userAnalysis.flash}</td>
                                                            <td>${comptetiorAnalysis[0].flash}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].flash}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}">
                                                                <c:choose>
                                                                    <c:when test="${fn:trim(userAnalysis.flash) == 'Flash is used'}">
                                                                        <spring:message code="message.negativeflash" />
                                                                    </c:when>
                                                                    <c:when test="${fn:trim(userAnalysis.flash) == 'Flash is not used'}">
                                                                        <spring:message code="message.positiveflash" />
                                                                    </c:when>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Frames</td>
                                                            <td>${userAnalysis.framesCount}</td>
                                                            <td>${comptetiorAnalysis[0].framesCount}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].framesCount}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}">
                                                                <c:choose>
                                                                    <c:when test="${fn:trim(userAnalysis.framesCount) == 'Frames are used'}">
                                                                        <spring:message code="message.negativeframes" />
                                                                    </c:when>
                                                                    <c:when test="${fn:trim(userAnalysis.framesCount) == 'Frames are not used'}">
                                                                        <spring:message code="message.positiveframes" />
                                                                    </c:when>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Text / Html Ratio</td>
                                                            <td>${userAnalysis.textbyhtmlratio}</td>
                                                            <td>${comptetiorAnalysis[0].textbyhtmlratio}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].textbyhtmlratio}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}">
                                                                <spring:message code="message.texthtmlratio" />
                                                                <br>
                                                                <spring:message code="message.texthtmlrecom1" />
                                                                <br>
                                                                <spring:message code="message.texthtmlrecom2" />
                                                                <br>
                                                                <spring:message code="message.texthtmlrecom3" />
                                                            </td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Favicon</td>
                                                            <td>${userAnalysis.favicon}</td>
                                                            <td>${comptetiorAnalysis[0].favicon}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].favicon}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.favicon" /></td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Language</td>
                                                            <td>${userAnalysis.getLanguage()}</td>
                                                            <td>${comptetiorAnalysis[0].getLanguage()}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].getLanguage()}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}">
                                                                <c:choose>
                                                                    <c:when test="${fn:trim(userAnalysis.getLanguage()) == 'Language missing'}">
                                                                        <spring:message code="message.negativelanguage" />
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <spring:message code="message.positivelanguage" />
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>Load Time</td>
                                                            <td>${userAnalysis.loadtime}</td>
                                                            <td>${comptetiorAnalysis[0].loadtime}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].loadtime}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.loadtime" /></td>
                                                        </tr>
                                                        <tr class="text-message-padding lxrm-bold">
                                                            <td>WWW Resolve</td>
                                                            <td>${(userAnalysis.wwwresolve != null || userAnalysis.wwwresolve ne '') ? userAnalysis.wwwresolve : '-'}</td>
                                                            <td>${comptetiorAnalysis[0].wwwresolve !=null ? comptetiorAnalysis[0].wwwresolve : '-'}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td>${comptetiorAnalysis[1].wwwresolve !=null ? comptetiorAnalysis[0].wwwresolve : '-'}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr class="text-message-border-0">
                                                            <td colspan="${secondCompetitor ? '4' : '3'}"><spring:message code="message.wwwresolve" /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!--how to fix-->
                            <div id="howtofix-1" style="display: none;">
                                <div class="col-lg-12 col-sm-12 col-xs-12 panel-content">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-light-danger">Below are the steps to fix on page issues on the website</span>
                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <p>1. Avoid publishing too much irrelevant content.</p> 
                                            <p>2. Speed up your page load times so users don’t abandon your site out of frustration, visit our SEO Page Speed insights to optimize your page speed.</p> 
                                            <p>3. Improve your content and Optimize it with the relevant content by comparing your competitor.</p> 
                                            <p>4. Text to HTML ratio could be increased to improve your visibility.</p> 
                                            <p>5. Proper redirects to be made for better indexing.</p> 
                                            <div class="lxr-panel-footer">
                                                <h3><spring:message code="ate.expert.label.2" /></h3>
                                                <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <!--2 tab start here-->
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>HEADINGS</span>
                                <p>Heading tags are HTML tags that can help emphasize important topics and keywords within a page. H1 headings help indicate the important topics of your page to search engines. </p>
                            </button>
                        </div>
                        <div class="content main-content-2">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2" style="padding: 0px">
                                <div class="panel panel-default">
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Heading Tags Need Improvement Vs Competitors</span>
                                        <p>Heading defines most important text, So Please fix these, Followed by below  <strong  class="lxr-steps partially_notwork" onClick ="hideContentAndShowHowToFix(2);">STEPS.</strong></p>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <h5>Headings (H1,H2...H6)</h5>
                                            </div>
                                            <!--1-->
                                            <div class="col-xs-12 ${secondCompetitor ? ' col-sm-4' : 'col-sm-6'}">
                                                <h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${userAnalysis.url}</h5>
                                                <c:if test="${userAnalysis.totalHeadingsList ne null && not empty userAnalysis.totalHeadingsList}">
                                                    <c:forEach var="headingsMap" items="${userAnalysis.totalHeadingsList}">
                                                        <div class="btn-group-vertical">
                                                            <div class="lxr-badge-vertical-up">H${headingsMap.key}</div>
                                                            <div class="lxr-badge-vertical-down">${headingsMap.value.size()}</div>
                                                        </div>
                                                    </c:forEach>
                                                </c:if>
                                                <div class="row" style="margin: 15px 0px 0px 0px">
                                                    <c:if test="${userAnalysis.totalHeadingsList ne null && not empty userAnalysis.totalHeadingsList}">
                                                        <c:forEach var="headingsMap" items="${userAnalysis.totalHeadingsList}">
                                                            <c:if test="${headingsMap.value.size() > 0}">
                                                                <c:forEach items="${headingsMap.value}" var="element">
                                                                    <strong>H[${headingsMap.key}] </strong>${element}<br>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <!--2-->
                                            <div class="col-xs-12 ${secondCompetitor ? ' col-sm-4' : 'col-sm-6'}">
                                                <h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[0].url}</h5>
                                                <c:if test="${comptetiorAnalysis[0].totalHeadingsList ne null && not empty comptetiorAnalysis[0].totalHeadingsList}">
                                                    <c:forEach var="headingsMap" items="${comptetiorAnalysis[0].totalHeadingsList}">
                                                        <div class="btn-group-vertical">
                                                            <div class="lxr-badge-vertical-up">H${headingsMap.key}</div>
                                                            <div class="lxr-badge-vertical-down">${headingsMap.value.size()}</div>
                                                        </div>
                                                    </c:forEach>
                                                </c:if>
                                                <div class="row" style="margin: 15px 0px 0px 0px">
                                                    <c:if test="${comptetiorAnalysis[0].totalHeadingsList ne null && not empty comptetiorAnalysis[0].totalHeadingsList}">
                                                        <c:forEach var="headingsMap" items="${comptetiorAnalysis[0].totalHeadingsList}">
                                                            <c:if test="${headingsMap.value.size() > 0}">
                                                                <c:forEach items="${headingsMap.value}" var="element">
                                                                    <strong>H[${headingsMap.key}] </strong>${element}<br>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <c:if test="${secondCompetitor}">
                                                <div class="col-xs-12 col-sm-4">
                                                    <h5><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[1].url}</h5>
                                                    <c:if test="${comptetiorAnalysis[1].totalHeadingsList ne null && not empty comptetiorAnalysis[1].totalHeadingsList}">
                                                        <c:forEach var="headingsMap" items="${comptetiorAnalysis[0].totalHeadingsList}">
                                                            <div class="btn-group-vertical">
                                                                <div class="lxr-badge-vertical-up">H${headingsMap.key}</div>
                                                                <div class="lxr-badge-vertical-down">${headingsMap.value.size()}</div>
                                                            </div>
                                                        </c:forEach>
                                                    </c:if>
                                                    <div class="row" style="margin: 15px 0px 0px 0px">
                                                        <c:if test="${comptetiorAnalysis[1].totalHeadingsList ne null && not empty comptetiorAnalysis[1].totalHeadingsList}">
                                                            <c:forEach var="headingsMap" items="${comptetiorAnalysis[1].totalHeadingsList}">
                                                                <c:if test="${headingsMap.value.size() > 0}">
                                                                    <c:forEach items="${headingsMap.value}" var="element">
                                                                        <strong>H[${headingsMap.key}] </strong>${element}<br>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </c:if>
                                            <p><spring:message code="message.headings"/></p>
                                        </div>
                                    </div>
                                </div>  
                            </div>

                            <!--how to fix-->
                            <div  id="howtofix-2" style="border: 0px red solid;display: none">
                                <div class="col-md-12 col-sm-12 col-xs-12 panel-content" style="padding: 0px">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lxr-panel-heading">
                                            <span class="panel-title lxr-light-danger">How to fix heading tags</span>
                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                        </div>
                                        <div class="panel-body lxr-panel-body">
                                            <p> Never ignore it because Search spiders pay attention to the words used in the h1 tag as it should contain a basic description of the page content, just as the page title does.But other header tags from H2 to H6 are also important if you have lengthy content depending on the requirement you can assign these tags.</p>
                                            <p> The H1 tag should contain your targeted keywords, ones that closely relate to the page title and are relevant to your content.</p>
                                            <p> Example of how to use your header tags. See below:</p>
                                            <p><xmp><h1>Important heading goes here</h1></xmp></p>
                                            <div class="lxr-panel-footer">
                                                <h3><spring:message code="ate.expert.label.2" /></h3>
                                                <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>

                        <!--3-->
                        <div class="tab">
                            <button class="tab-toggle lxrtool-tabs-heading">
                                <span>SEARCH PHRASE DENSITY</span> 
                                <p>Phrase Search is a type of search that allows users to search for documents containing an exact sentence or phrase rather than comparing a set of keywords in random order</p>
                            </button>
                        </div>
                        <div class="content main-content-3">
                            <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3" style="padding: 0px">
                                <div class="panel panel-default">
                                    <div class="panel-heading lxr-panel-heading">
                                        <span class="panel-title">Search phrase density Need Improvement Vs Competitors</span>
                                    </div>
                                    <div class="panel-body lxr-panel-body">
                                        <div class="row">
                                            Competitive Keyword Density for &nbsp;<strong><c:out value="'${competitorAnalysisTool.searchPhrase}'"/></strong>
                                        </div>
                                        <!--2-->
                                        <div class="row">
                                            <!--1-->
                                            <div class="col-xs-12">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Keyword Density in</th>
                                                            <th class="text-center"><i class="fa fa-briefcase" aria-hidden="true"></i> ${userAnalysis.url}</th>
                                                            <th class="text-center"><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[0].url}</th>
                                                                <c:if test="${secondCompetitor}">
                                                                <th class="text-center"><i class="fa fa-briefcase" aria-hidden="true"></i> ${comptetiorAnalysis[1].url}</th>
                                                                </c:if>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th>Title Tag </th>
                                                            <td class="text-center">${userAnalysis.titleTag}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].titleTag}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].titleTag}</td>
                                                            </c:if>

                                                        </tr>
                                                        <tr>
                                                            <td>Description Tag</td>
                                                            <td class="text-center">${userAnalysis.descriptionTag}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].descriptionTag}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].descriptionTag}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr>
                                                            <td>Keyword Tag</td>
                                                            <td class="text-center">${userAnalysis.keywordsTag}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].keywordsTag}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].keywordsTag}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr>
                                                            <td>Body Text</td>
                                                            <td class="text-center">${userAnalysis.bodytext}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].bodytext}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].bodytext}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr>
                                                            <td>Alt Tag</td>
                                                            <td class="text-center">${userAnalysis.altTags}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].altTags}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].altTags}</td>
                                                            </c:if>
                                                        </tr>
                                                        <tr>
                                                            <td>Link Text</td>
                                                            <td class="text-center">${userAnalysis.linkText}</td>
                                                            <td class="text-center">${comptetiorAnalysis[0].linkText}</td>
                                                            <c:if test="${secondCompetitor}">
                                                                <td class="text-center">${comptetiorAnalysis[1].linkText}</td>
                                                            </c:if>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p>  <spring:message code="message.keyworddensity" /></p>
                                        </div>
                                    </div>
                                </div>  
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row download-report-row">
                    <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                    <div class="col-md-9 col-sm-12 col-xs-12 download-report-div">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                            <p style="margin: 3% 0;">Download complete report here: </p></div>
                        <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter your email id" name="EmailId" id="userEmailId" value="${userH.userName}">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" style="border: 1px #ccc solid !important;" id="mailReport"><i class="glyphicon glyphicon-send"></i></button>
                                </div>
                            </div>
                            <div id="mail-sent-status" style="font-family: ProximaNova-Regular;" class="emptyData"></div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <div class="dropdown">
                                <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                <div id="downloadAnalysisReport" class="dropdown-content">
                                    <p onclick="downloadFormat('pdf');">PDF</p>
                                    <p onclick="downloadFormat('xls');">XLS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <!--vertical tab system-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
