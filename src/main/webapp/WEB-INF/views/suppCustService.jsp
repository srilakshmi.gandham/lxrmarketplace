
<!DOCTYPE html>
<%@ page import="lxr.marketplace.user.Login,java.util.* ,lxr.marketplace.amazonpricetracker.AmazonPriceTrackerTool ,lxr.marketplace.util.Common,java.util.List,java.util.ArrayList,lxr.marketplace.util.Tool,lxr.marketplace.util.ToolService ,org.springframework.web.context.WebApplicationContext ,org.springframework.web.context.support.WebApplicationContextUtils , lxr.marketplace.feedback.Feedback, lxr.marketplace.user.Signup"%>

<html>
    <head>
        <title>Search Engine Marketing Company | Support Contact Us | LXRMarketplace.com</title>

        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/styles.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/contactus.css">
        <%
            long toolDBId = -1l;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            isHomePage = true;
            $(document).ready(function () {
                $('#supt li').click(function () {

                    if (this.id === "FAQs") {
                        window.location = "/support.html";
                    } else if (this.id === "Testimonials") {
                        window.location = "/supptestmnls.html";
                    }
                });
            <c:set var="clearAll" value="${clearAll}" />
            <c:if test="${(clearAll.equals('clearAll'))}">
                $('html, body').animate({scrollTop: $('#feedback').offset().top}, 'slow');
            </c:if>
            });
        </script>

    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container">
            <div class="row">
                <div class="contact-us-wrapper col-md-12" style="color: #888;">
                    <h2 style="font-family: ProximaNova-Bold;color: black;">Contact Us</h2>
                    <p>
                        <strong>Support & Feedback: </strong>
                        <a href="mailto:support@lxrmarketplace.com">support@lxrmarketplace.com</a>
                    </p>
                    <div class="address-wrapper row">
                        <div class="col-md-4">
                            <h4 class="Bold" style="color: #f96003;">Global Headquarters</h4>
                            <p class="Bold">NetElixir Inc</p>
                            <p>3 Independence Way</p>
                            <p>Suite #203</p>
                            <p>Princeton,NJ 08540, USA.</p>
                        </div>
                        <div class="col-md-4">
                            <h4 class="Bold" style="color: #f96003;">Europe Office</h4>
                            <p class="Bold">NetElixir Ltd</p>
                            <p>727-729 High Road,</p>
                            <p>London,</p>
                            <p>N12 0BP</p>
                        </div>
                        <div class="col-md-4">
                            <h4 class="Bold" style="color: #f96003;">Asia Pacific Office</h4>
                            <p class="Bold">NetElixir Digital Solutions</p>
                            <p>C/O Workafella, #102 Western Aqua Building,</p>
                            <p>12th Floor, Whitefields, Hitech City Road,</p>
                            <p>Kondapur, Hyderabad 500081, India</p>
                        </div>
                    </div>
                    <%@ include file="/WEB-INF/views/feedback.jsp"%>
                </div>
            </div>

        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
