<%-- 
    Document   : microInfluencer
    Created on : 29 Jun, 2018, 8:56:51 PM
    Author     : sagar
--%>

<%@page import="java.util.List, lxr.marketplace.microInfluencer.MicroInfluencer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="lxrmarketplace">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Micro Influencer Generator | LXRMarketplace</title>
        <meta name="description" content="Try our Micro Influencer Generator tool now to generate list of micro-influencers.">
        <meta name="keywords" content="Micro Influencer Generator Tool">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <script type="text/javascript" src="/resources/css/tools/tooltabs.js"></script>
        <!-- Angular JS -->
        <script type="text/javascript" src="/resources/js/angular/angular.min.js"></script>
        <script type="text/javascript"  src="/resources/js/app.js"></script>
        <!--Tool angularJS controller-->
        <script src="/resources/js/tools/microInfluencer/microInfluencerController.js"></script>

        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/microInfluencer.css">

        <!--ProgressBar JS & Styles-->
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common-material.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.material.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.material.mobile.min.css" />
        <script src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>

        <!--Js & CSS related to range selecter-->
        <link rel="stylesheet" type="text/css" href="/resources/css/rzslider.min.css">
        <script type="text/javascript"  src="/resources/js/angular/rz-slider/ui-bootstrap-tpls.js"></script>
        <script type="text/javascript"  src="/resources/js/angular/rz-slider/rzslider.js"></script>
        <script type="text/javascript"  src="/resources/js/angular/pagination/ui-bootstrap-tpls-0.10.0.js"></script>

        <%
            int toolDBId = 40;
            String reqParam = null;
            if (request.getParameter("report") != null) {
                reqParam = request.getParameter("report");
                pageContext.setAttribute("reqParam", reqParam);
            }
            String downloadReportType = null;
            if (request.getParameter("report-type") != null) {
                downloadReportType = request.getParameter("report-type");
                pageContext.setAttribute("downloadReportType", downloadReportType);
            }

        %>
        <script type="text/javascript">
            var atePopupStatus = true;
            var reportDownload = 0;
            var downloadReportType = "";
            var isBackToSuccess = false;
            var selectedMinValue = 0;
            var selectedMaxValue = 0;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                $("#resultWrapper").hide();
                /*  auto download when user logged from success page*/
            <c:choose>
                <c:when test="${(fn:contains(uri, 'loginRefresh') && fn:contains(uri, 'report'))}">
                isBackToSuccess = true;
                if (sessionStorage.getItem("selectedMinValue") != null) {
                    selectedMinValue = sessionStorage.getItem("selectedMinValue");
                }
                if (sessionStorage.getItem("selectedMaxValue") != null) {
                    selectedMaxValue = sessionStorage.getItem("selectedMaxValue");
                }
                downloadInfluencerReport();
                </c:when>
                <c:when test="${(fn:contains(uri, 'loginRefresh') && fn:contains(uri, 'limit-exceeded=1'))}">
                isBackToSuccess = false;
                </c:when>
                <c:when test="${(fn:contains(uri, 'backToSuccessPage') || fn:contains(uri, 'loginRefresh'))}">
                isBackToSuccess = true;
                </c:when>
            </c:choose>
//                toolLimitExceeded();
                // Sendmail functionality
                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        sendReportToMail();
                        return false;
                    }
                });
                $('#email').click(function () {
                    $('#mail-sent-status').hide();
                    return false;
                });
                $('#editSearch, #searchQuery').on('click', function () {
//                    $("#searchQueryError").empty();
                    $("#editSearch").hide();
                    $("#searchQuery").removeClass("gray");
                });
            });
            function sendReportToMail() {
                if (selectedMaxValue !== -1 && selectedMinValue !== -1) {
                    $('#mail-sent-status').empty();
                    var emailid = $.trim($("#email").val());
                    var status = true;
                    var statusMsg = "";
                    if (emailid === "") {
                        statusMsg = "Please enter your email id";
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        statusMsg = "Please enter a valid email address.";
                        status = false;
                    }
                    $('#mail-sent-status').show();
                    if (!status) {
                        $("#email").focus();
                        $("#mail-sent-status").html(statusMsg);
                        $('#mail-sent-status').css("color", "red");
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
//                        var oMyForm = new FormData();
//                        oMyForm.append("minFollowers", selectedMinValue);
//                        oMyForm.append("maxFollowers", selectedMaxValue);
//                        oMyForm.append("email",emailid);
                        $.ajax({
                            type: "POST",
                            url: '/micro-influencer-generator.html?request=sendMail&email=' + emailid + '&minFollowers=' + selectedMinValue + '&maxFollowers=' + selectedMaxValue,
                            processData: true,
                            contentType: false,
                            data: {},
                            dataType: "json",
                            statusCode: {
                                400: function (response) {
                                    toolLimitExceededForAjax();
                                },
                                404: function (response) {
                                    toolLimitExceededForAjax();
                                }
                            },
                            success: function (data) {
                                if (data) {
                                    $('#mail-sent-status').empty();
                                    $('#mail-sent-status').show();
                                    $('#mail-sent-status').html("Mail sent successfully");
                                    $('#mail-sent-status').css("color", "green");
                                } else {
                                    $("#mail-sent-status").html("Sending mail is failed.");
                                    $('#mail-sent-status').css("color", "red");
                                }
                            }, error: function (jqXHR, textStatus, errorThrown) {
                                console.log("Error: " + jqXHR.status + ",textStatus: " + textStatus + ", errorThrown: " + errorThrown);
//                            location.reload();
                            }
                        });
                    }
                } else {
                    alert("MicroInfluencers profiles are not selected.");
                }
            }
            // send mail and download report to user
            function downloadInfluencerReport() {
                if (selectedMaxValue !== -1 && selectedMinValue !== -1) {
                    if (userLoggedIn) {
                        var downloadForm = null;
                        var minValue = null;
                        var maxValue = null;
                        $("#influencerForm").find("input:hidden").remove();
                        minValue = $("<input>").attr("type", "hidden").attr("name", "minFollowers").val(selectedMinValue);
                        maxValue = $("<input>").attr("type", "hidden").attr("name", "maxFollowers").val(selectedMaxValue);
                        downloadForm = $("#influencerForm");
                        downloadForm.append($(maxValue));
                        downloadForm.append($(minValue));
                        downloadForm.attr('method', "POST");
                        downloadForm.attr('action', "/micro-influencer-generator.html?request=download");
                        downloadForm.submit();
                    } else {
                        reportDownload = 1;
//                    downloadReportType = reportType;
                        $("#loginPopupModal").modal('show');
                        return false;
                    }
                } else {
                    alert("MicroInfluencers profiles are not selected.");
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/micro-influencer-generator.html?ate=ate&domain=",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            function submitPreviousRequest() {
                angular.element('#searchInfluencers').triggerHandler('click');
            }

        </script>
    </head>
    <body ng-controller="MicroInfluencerController as ctrl" id="body-content">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs" style="height: 20px;clear: both"></div>
        <div class="container">
            <form name="microInfluencerGeneratorForm" id="influencerForm">
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <input type="text" id="searchQuery" ng-model="searchQuery"  name="searchQuery" class="form-control input-lg" placeHolder="Search for keywords or #hashtags or @handle for lookalikes" title="Search for keywords or #hashtags or @handle for lookalikes."/>
                            <i id="editSearch" class="glyphicon glyphicon-pencil lxrtool-pencil" style="display: none;"></i>
                        </div>
                        <span id="searchQueryError" class="lxr-danger text-left" >{{queryRequired}}</span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="submit" ng-click="fetchMicroInfluencers()" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12" id="searchInfluencers" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> SEARCHING...">
                                ANALYZE
                            </button>
                        </div>
                    </div>  
                </div>
            </form>
        </div>
        <div class="clearfixdiv"  ng-init="processBackToSuccessPage()"></div>
        <div class="container" id="resultWrapper" style="display: none;">
            <div class="clearfixdiv"></div>
            <!--<div class="row">-->
            <div class="col-xs-12">
                <div class="col-xs-12 panel-default">
                    <div class="panel-heading lxr-panel-heading row" ng-switch="ctrl.isThreadCompleted">
                        <div ng-switch-when="true" ng-if="ctrl.apiResponseList.length > 0 && ctrl.isThreadCompleted" class="col-xs-12 d-sm-flex align-items-center">
                            <!--<div class="{{ctrl.apiResponseList.length != 1? col-xs-12  col-md-3: col-xs-12 }}">-->
                            <div class="col-xs-12  col-md-4">
                                <p><span class="lxrm-bold" id="influencers-count">{{ctrl.influencerCount}}</span> Micro influencers found on twitter</p>
                                <input type="hidden" ng-model="followersRangeSlider.minValue"/>
                            </div>
                            <div class="col-xs-12 col-md-2 textRight" ng-if="ctrl.apiResponseList.length > 1 && ctrl.isThreadCompleted">
                                <p class="mt" >Followers range:</p>
                                <input type="hidden" ng-model="followersRangeSlider.maxValue"/>
                            </div>
                            <div class="col-xs-12 col-md-6" ng-if="ctrl.apiResponseList.length > 1 && ctrl.isThreadCompleted">
                                <rzslider rz-slider-model="followersRangeSlider.minValue" rz-slider-high="followersRangeSlider.maxValue" rz-slider-options="followersRangeSlider.options"></rzslider>
                            </div>
                        </div>
                        <div ng-switch-when="true" ng-if="ctrl.apiResponseList.length === 0 && ctrl.isThreadCompleted" class="mh"></div>
                        <div ng-switch-default class="col-xs-12" style="display: flex;align-items: center;">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="demo-section k-content" ng-if="ctrl.apiResponseList.length !== 0">
                                    <h5>{{status}}</h5>
                                    <div kendo-progress-bar="progressBar" ng-model="progress" k-options="progressBarOptions" style="width: 75%;height: 3vh;"></div>
                                </div>
                                <div class="" ng-if="ctrl.apiResponseList.length === 0">
                                    <h5>Looking for micro influencers....</h5>
                                    <div class="animated-background height width3 mb"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 textRight" ng-if="ctrl.apiResponseList.length !== 0">
                                <p style="margin-top:1.2rem !important;">
                                    <span style="font-family: ProximaNova-Regular !important;font-size: 14px !important;">Processed:</span>
                                    <span style="font-family: ProximaNova-Bold !important;font-size: 20px !important;">{{ctrl.apiResponseList.length}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body lxr-panel-body">
                        <div ng-switch="ctrl.isThreadCompleted || ctrl.apiResponseList.length > 0">
                            <div class="col-xs-12 table-parent-div table-responsive" ng-switch-when="true" ng-switch="ctrl.apiResponseList.length > 0">
                                <div id="resultTableWrapper" ng-switch-when="true" ng-switch="ctrl.dataTableMatrixList !== null && ctrl.dataTableMatrixList.length > 0">
                                    <table ng-switch-when="true" class="table" ng-table="microInfluencersResult" id="microInfluencersList" cellpadding="0" cellspacing="0">
                                        <thead><tr><th></th><th></th><th></th><th></th></tr></thead>
                                        <tbody style="word-break: break-all;">
                                            <tr ng-repeat='item in filtered = ctrl.dataTableMatrixList| startFrom:(currentPage - 1) * entryLimit | limitTo:entryLimit'>
                                                <td ng-repeat="influencer in item" class="col-xs-3 cell-width"  style="cursor: pointer;" data-toggle="modal" data-target="#viewInfluencer" ng-click="viewCompleteProfile(influencer)">
                                                    <div>
                                                        <div title="View {{influencer.name}}" class="thumbnail shadow">
                                                            <img title="{{influencer.name}}" src="{{influencer.profileImageURL !== null ? influencer.profileImageURL:'/images/profile_image.jpg'}}" alt="{{influencer.screenName}}" class="img-circle mt-1" width="45" height="45"/>
                                                            <div class="caption text-center">
                                                                <h4 title="View {{influencer.name}}" class="lxrm-bold text-truncate">{{influencer.name}}</h4>
                                                                <p title="View {{influencer.name}}" class="screenName text-truncate"><span class="lxrm-bold">@</span>{{influencer.screenName}}</p>
                                                            </div>
                                                            <div class="d-flex">
                                                                <p class="text-center">
                                                                    <span>{{influencer.followers}}</span><br/> Followers
                                                                </p>
                                                                <p class="text-center">
                                                                    <span>{{influencer.posts}}</span><br/> Tweets
                                                                </p>
                                                                <p class="text-center">
                                                                    <span>{{influencer.following}}</span><br/> Following
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td ng-if="getRemainingColumns(items.length) > 0" class="cell-width"></td>
                                                <td ng-if="getRemainingColumns(items.length) > 1" class="cell-width"></td>
                                                <td ng-if="getRemainingColumns(items.length) > 2" class="cell-width"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-switch-default class="text-center lxrm-bold nodata-bg mt-3 p-2">
                                        <!--<p class="d-flex-justify-center" style="font-size: 1.4rem;height: 45%;">Micro influencers not available in selected followers range.</p>-->
                                        <p class="lxrm-bold noResults">Micro influencers not available in selected followers range.</p>
                                    </div>
                                    <pagination class="pull-right" page='currentPage' max-size='noOfPages' total-items='totalItems' items-per-page='entryLimit'></pagination>
                                </div>
                                <div ng-switch-default class="text-center lxrm-bold nodata-bg mt-3 p-2 align-items-center" >
                                    <p class="lxrm-bold noResults" ng-if="ctrl.noData">The keyword "{{ctrl.noResultsQuery}}" you entered did not bring up any results.</p>
                                </div>
                            </div>
                            <div ng-switch-default class="">
                                <div class="col-xs-12 table-parent-div table-responsive">
                                    <table class="table" cellpadding="0" cellspacing="0">
                                        <thead></thead>
                                        <tbody>
                                            <c:forEach begin="1" end="3">
                                                <tr>
                                                    <c:forEach begin="1" end="4">
                                                        <td>
                                                            <div>
                                                                <div class="thumbnail">
                                                                    <div class="d-flex-justify-center">
                                                                        <div class="animated-background width0"></div>
                                                                    </div><br/>
                                                                    <div class="d-flex-justify-center">
                                                                        <div class="animated-background height width1 mb"></div>
                                                                    </div>
                                                                    <div class="d-flex-justify-center">
                                                                        <div class="animated-background width2 mb"></div><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </c:forEach>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>  
                        </div>
                        <div id="viewInfluencer" class="modal fade" role="dialog" style="top:12%;">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <div>
                                            <a href="https://twitter.com/{{ctrl.viewProfile.screenName}}" target="_blank">
                                                <span class="channelTitle"><i class="fa fa-twitter-square" style="color:#1c94e0" aria-hidden="true"></i> TWITTER</span>
                                                <span>@{{ctrl.viewProfile.screenName}}</span>
                                            </a>
                                        </div> 
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <p>
                                                            <a href="https://twitter.com/{{ctrl.viewProfile.screenName}}" target="_blank"><img title="{{ctrl.viewProfile.name}}" src="{{ctrl.viewProfile.profileImageURL}}" alt="{{ctrl.viewProfile.screenName}}" class="media-object" class="img-circle" width="45" height="45"></a>
                                                        </p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="row pl-0">
                                                            <div class="col-xs-12  pl-0">
                                                                <div class="col-xs-12  col-sm-6 col-md-6">
                                                                    <h4 class="media-heading lxrm-bold">{{ctrl.viewProfile.name}}</h4>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-6 textRight" ng-if="(ctrl.viewProfile.location !== null && ctrl.viewProfile.location !== '' && ctrl.viewProfile.location !== 'null')">
                                                                    <span class="glyphicon glyphicon-map-marker" style="color:#2F80ED;"></span>
                                                                    <span>{{ctrl.viewProfile.location}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" ng-if="(ctrl.viewProfile.description !== null && ctrl.viewProfile.description !== '' && ctrl.viewProfile.description !== 'null')">
                                                                <p>{{ctrl.viewProfile.description}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="col-sm-1 col-md-1 hidden-xs"></div>
                                                <div class="col-xs-12 col-sm-11 col-md-11 table-parent-div table-responsive">
                                                    <table class="table">
                                                        <thead style="border-bottom:1px solid #ddd;">
                                                            <tr>
                                                                <th class="text-center">Followers</th>
                                                                <th class="text-center">Tweets</th>
                                                                <th class="text-center">Following</th>
                                                                <th class="text-center">Likes</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-center">{{ctrl.viewProfile.followers}}</td>
                                                                <td class="text-center">{{ctrl.viewProfile.posts}}</td>
                                                                <td class="text-center">{{ctrl.viewProfile.following}}</td>
                                                                <td class="text-center">{{ctrl.viewProfile.likes}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12  pl-0">
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                    <p>
                                                        <span>UserSince:</span>
                                                        <span class="lxrm-bold">{{ctrl.viewProfile.userSince}}</span>
                                                    </p>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 textRight" ng-if="(ctrl.viewProfile.website !== null && ctrl.viewProfile.website !== '' && ctrl.viewProfile.website !== 'null')">
                                                    <p>
                                                        <span>Domain:</span>
                                                        <span><a href="{{ctrl.viewProfile.website}}" target="_blank">{{ctrl.viewProfile.website}}</a></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer lxr-panel-tool-footer" ng-switch="(ctrl.isThreadCompleted && ctrl.apiResponseList.length !== 0)">
                        <div class="row download-report-row d-flex-justify-center" ng-switch-when="true" >
                            <div class="col-lg-3 visible-lg visible-md"></div>
                            <div class="col-md-9 col-xs-12 download-report-div">
                                <div class="col-md-4 col-xs-12 lxrm-padding-none" style="padding: 0;">
                                    <p style="margin: 3% 0;">Download complete report here: </p>
                                </div>
                                <div class="col-md-7 col-xs-10" style="padding: 0;">
                                    <div class="input-group">
                                        <input type="text" class="form-control" title="Enter your email id" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                        <div class="input-group-btn">
                                            <button id="mailReport" class="btn btn-default" onclick="sendReportToMail();" style="border: 1px #ccc solid !important;"><i class="glyphicon glyphicon-send"></i></button>
                                        </div>
                                    </div>
                                    <div id="mail-sent-status" class="emptyData mb-1"></div>
                                </div>
                                <div class="col-md-1 col-sm-2 col-xs-2">
                                    <div id="downloadInfluencerReport" onclick="downloadInfluencerReport()" style="font-size: 20px;cursor: pointer;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt mt-1"></div>
                                </div>
                            </div>
                        </div>
                        <div ng-switch-default class="mh"></div>
                    </div>
                </div>
            </div>
            <!--</div>-->
        </div><!--EOF of tool page parent div-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
