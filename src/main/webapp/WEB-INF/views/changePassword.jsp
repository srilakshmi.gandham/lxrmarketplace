<%-- 
    Document   : brokenLinkChecker
    Created on : Aug 1, 2017, 11:24:49 AM
    Author     : netsystem68
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Password | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <style>
            .main-center{
                margin-top: 30px;
                margin: 0 auto;
                max-width: 400px;
                padding: 10px 16px;
                background:#fff;
                color: black;
                text-shadow: none;
                border: 1px solid rgba(0,0,0,.15);
                border-radius: 4px;
                -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
                box-shadow: 0 6px 12px rgba(0,0,0,.175);

            }
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container" style="height: 545px">
            <div class="row main">
                <div class="main-login main-center" style="border-top: 2px #F47A44 solid;">
                    <form role="form">
                        <div class="row setup-content">
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <h3>Change Password?</h3>
                                    <div class="form-group">
                                        <input maxlength="100" type="password" required="required" class="form-control" name="oldPassword" id="oldPassword"  placeholder="Enter Old Password"/>
                                        <span id="oldPasswordError" class="error"/>
                                    </div>
                                    <div class="form-group">
                                        <input maxlength="100" type="password" required="required" class="form-control" name="newPassword" id="newPassword"  placeholder="Enter New Password"/>
                                        <span id="passwordError" class="error"/>
                                    </div>
                                    <div class="form-group">
                                        <input maxlength="100" type="password" required="required" class="form-control"  name="confirmPassword" id="confirmPassword"  placeholder="Confirm New Password" />
                                        <span id="confirmPasswordError" class="error"/>
                                    </div>
                                    <p style="display: none;text-align: center;" id="status-message"></p>
                                    <button class="btn btn-block nextBtn lxr-subscribe" type="button" onClick="changePassword(this);" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Processing...">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var userPassword = "${userH.getPassword()}";
            $(document).ready(function () {
                $('.nextBtn').click(function () {
                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                            curInputs = curStep.find("input[type='password']"),
                            isValid = true;
                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }
                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });
            });
            function changePassword(param) {
                $("#status-message").hide();
                var oldPassword = $('#oldPassword').val();
                var newPassword = $('#newPassword').val();
                var confirmPassword = $('#confirmPassword').val();
                var isValidAll = true;
                if (!${userLoggedIn}) {
                    $("#status-message").html("Please login with your existing username");
                    return false;
                }
                if (oldPassword === "" || oldPassword === null) {
                    document.getElementById('oldPasswordError').textContent = "Please enter old password";
                    document.getElementById('oldPasswordError').style.display = 'block';
                    isValidAll = false;
                } else if (oldPassword !== userPassword) {
                    document.getElementById('oldPasswordError').textContent = "Please enter a correct old password.";
                    document.getElementById('oldPasswordError').style.display = 'block';
                    isValidAll = false;
                } else {
                    document.getElementById('oldPasswordError').style.display = 'none';
                }

                if (newPassword === "" || newPassword === null) {
                    document.getElementById('passwordError').textContent = "Please enter password";
                    document.getElementById('passwordError').style.display = 'block';
                    isValidAll = false;
                } else if (newPassword.length < 6) {
                    document.getElementById('passwordError').textContent = "Your password must be at least 6 characters long";
                    document.getElementById('passwordError').style.display = 'block';
                    isValidAll = false;
                } else if (newPassword !== "" && newPassword !== null && /\s/g.test(newPassword)) {
                    document.getElementById('passwordError').textContent = "Space is not allowed for password.";
                    document.getElementById('passwordError').style.display = 'block';
                    isValidAll = false;
                } else if (oldPassword === newPassword) {
                    document.getElementById('passwordError').textContent = "Do not use the same old password.";
                    document.getElementById('passwordError').style.display = 'block';
                    isValidAll = false;
                } else {
                    document.getElementById('passwordError').style.display = 'none';
                }

                if (confirmPassword === "" || confirmPassword === null) {
                    document.getElementById('confirmPasswordError').textContent = "Please enter confirm password";
                    document.getElementById('confirmPasswordError').style.display = 'block';
                    isValidAll = false;
                } else if (newPassword.length > 0 && confirmPassword.length > 0 && newPassword !== confirmPassword) {
                    document.getElementById('confirmPasswordError').textContent = "Password and Confirm Password should be same";
                    document.getElementById('confirmPasswordError').style.display = 'block';
                    isValidAll = false;
                } else {
                    document.getElementById('confirmPasswordError').style.display = 'none';
                }
                if (!isValidAll) {
                    return false;
                } else if (isValidAll) {
                    var $this = $(param);
                    $this.button('loading');
                    $.ajax({
                        method: "POST",
                        url: "/change-password.html?changePassword='changePassword'&newPassword=" + $('#newPassword').val() + "",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            userPassword = newPassword;
                            if (data[0] === true) {
                                $this.button('reset');
                                $("#oldPassword").val("");
                                $("#newPassword").val("");
                                $("#confirmPassword").val("");
                                $("#status-message").show();
                                $("#status-message").html("Password has been updated successfully.");
                                $("#status-message").css("color", "green");
                            }
                        }
                    });
                }
            }

        </script>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
