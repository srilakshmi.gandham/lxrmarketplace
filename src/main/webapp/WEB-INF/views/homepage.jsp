<%-- 
    Document   : homepage
    Created on : Jul 4, 2017, 5:04:15 PM
    Author     : LXR MARKETPLACE TEAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LXR MARKETPLACE</title>
        <link rel="stylesheet" type="text/css" href="../../resources/css/styles.css">
        <link rel="stylesheet" type="text/css" href="../../resources/css/simple-sidebar.css">
        <link rel="stylesheet" href="../../fonts/FontAwesome.otf">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--Jquery File-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!--Bootstrap Files-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="../../resources/js/rating.js" crossorigin="anonymous"></script>

        <script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
    </head>

    <body>

        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="top: 110px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Login to continue</h4>
                    </div>
                    <div class="modal-body">
                       
                        <form>
                           <div class="form-group">
                        <label class="sr-only" for="username">Email address</label>
                        <input type="email" class="form-control" id="username" name="email" placeholder="Email address" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="logInPassword">Password</label>
                        <input type="password" class="form-control"  id="logInPassword" name="pwd" placeholder="Password" required>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="isLoginRemember" checked> Remember me
                        </label>
                        <label class="pull-right">
                            <a href="">Forgot Password</a>
                        </label>
                    </div>
                       
                    <div><p class="input-errors" style="display: none;color: red;"></p></div>
                    <div class="form-group">
                        <button type="button" id="sign" class="btn btn-block lxr-subscribe">Sign in</button>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-block lxr-register" onclick="signupLxrm()">Register</button>
                    </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
<!--proximanova-->