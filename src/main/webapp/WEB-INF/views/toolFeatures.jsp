

<div class="row">
    <div class="col-xs-11 col-sm-11 col-md-6 col-lg-6 lxrtool-centerdiv" style="border: 0px green solid;">
        <div class="clearfixdiv"></div>
        <div class="row">
            <blockquote>FEATURES</blockquote>
            <div class="col-xs-12 col-sm-12 col-md-4  lxr-toolslist" >
                <div class="well row">
                    <i class="fa fa-check col-xs-4 col-md-12" aria-hidden="true"></i>
                    <div class="col-xs-8 col-md-12" style="padding: 0;"><spring:message code="${toolObject.toolId}.1"/></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4  lxr-toolslist">
                <div class="well row">
                    <i class="fa fa-exclamation-triangle col-xs-4 col-md-12 " aria-hidden="true"></i>
                    <div class="col-xs-8 col-md-12" style="padding: 0;"><spring:message code="${toolObject.toolId}.2"/></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4  lxr-toolslist">
                <div class="well row">
                    <i class="fa fa-share-alt col-xs-4 col-md-12" aria-hidden="true"></i>
                    <div class="col-xs-8 col-md-12" style="padding: 0;"><spring:message code="${toolObject.toolId}.3"/></div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
