
<%@page import="lxr.marketplace.keywordcombination.domain.Keyword"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="lxr.marketplace.keywordcombination.domain.KeywordCombination"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PPC Keyword Combiner Tool - Keyword Combinator | PPC Keyword Combinator | LXRMarketplace</title>
        <meta name="description" content="Visit LXRMarketplace for the PPC Keyword Combiner. Generate long tail keywords for campaigns.  Download as an AdWords Editor, AdCenter Desktop compatible report.">
        <meta name="keywords" content="PPC Keyword Combiner Tool, PPC Keyword Combinator, keyword combinator">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/keywordCombinator.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <!--<script src="/resources/css/tools/tooltabs.js"></script>-->
        <%
            int toolDBId = 2;
            ArrayList<Keyword> keywordList = (ArrayList<Keyword>) session.getAttribute("keywordList");
            KeywordCombination keycomb = (KeywordCombination) session.getAttribute("keywordCombination");
            if (session.getAttribute("keyComb") != null) {
                KeywordCombination keywordCombination = (KeywordCombination) session.getAttribute("keyComb");
                // For three columns
                Pattern p = Pattern.compile("\n");
                String colOneStr = "", colTwoStr = "", colThreeStr = "";

                String colOneList[] = p.split(keywordCombination.getColOne());
                for (int i = 0; i < colOneList.length; i++) {
                    colOneStr += colOneList[i].trim() + ",";
                }
                String colTwoList[] = p.split(keywordCombination.getColTwo());
                for (int i = 0; i < colTwoList.length; i++) {
                    colTwoStr += colTwoList[i].trim() + ",";
                }
                String colThreeList[] = p.split(keywordCombination.getColThree());
                for (int i = 0; i < colThreeList.length; i++) {
                    colThreeStr += colThreeList[i].trim() + ",";
                }
            }
            String value = "";
            if (keywordList != null) {
                for (int i = 0; i < keywordList.size(); i++) {
                    Keyword keyword = (Keyword) keywordList.get(i);
                    value += keyword.getKeyword() + ", ";
                    value += keyword.getMatchType();
                    if (!(keyword.getSearchEng().trim().equals(""))) {
                        value += ", " + keyword.getStatus() + ", ";
                        value += Double.toString(keyword.getCpc()) + ", ";
                        value += keyword.getUrl();
                    }
                    value += "\n";
                }

            }
            String reqParam = request.getParameter("report");
            String downloadReportType = request.getParameter("report-type");
            pageContext.setAttribute("reqParam", reqParam);
            pageContext.setAttribute("downloadReportType", downloadReportType);

            pageContext.setAttribute("keywordCombination", keycomb);
            pageContext.setAttribute("resultText", value);
        %>
        <script type="text/javascript">
            <c:set var="status" value="${getSuccess}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {
//                toolLimitExceeded();
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat("${downloadReportType}");
                }
                $('#powerPost1').change(function () {
                    if ($(this).is(':checked')) {
                        $("#search-engine-inputs").show(1000);
                        $('#searchEngine0').prop("checked", true);
                    } else {
                        $("#search-engine-inputs").hide(1000);
                        $('input[name="searchEngine"]').prop("checked", false);
                        resetAdvancedInputs("powerPost");
                    }
                });
            <c:choose>
                <c:when test="${keywordCombination.advanced ne '' && keywordCombination.advanced ne null}">
                $("#advanced-label-plus").hide();
                $("#advanced-label-minus").show();
                $("#advanced-inputs-section").show();
                if (${keywordCombination.powerPost}) {
                    $("#search-engine-inputs").show();
                    var searchEngineValue = "${keywordCombination.searchEngine}";
                    $('#powerPost1').prop('checked', true);
                    if (searchEngineValue.indexOf("Google") !== -1) {
                        $('input:radio[name=searchEngine]')[0].checked = true;
                    } else {
                        $('input:radio[name=searchEngine]')[1].checked = true;
                    }
                }
                </c:when>
                <c:otherwise>
                $("#advanced-label-plus").show();
                $("#advanced-label-minus").hide();
                $("#advanced-inputs-section").hide();
                </c:otherwise>
            </c:choose>
                $("#getResult").bind('click', function (event) {
                    resultLoginValue = true;
                    var alertMsg = "";
                    $(".error-info").html("");
                    if (!($.trim($("#colOne").val()))) {
                        alertMsg = "Please enter Words/Phrases in First Column!\n";
                        $(".colOneError").html(alertMsg);
                    }
                    var matchTYpeValue = $("input[name='matchType']:checked").val();
                    if (matchTYpeValue === undefined || matchTYpeValue === "") {
                        alertMsg = "Please select at least one match type";
                        $(".matchTypeError").html(alertMsg);
                    }
                    var advancedVal = $("#advanced").val();
                    if (advancedVal !== "") {
                        if ($("#powerPost1").is(':checked')) {
                            if ($("input[name='searchEngine']:checked").val()) {
                                var seEng = $("input[name='searchEngine']:checked").val();
                                var maxBid = $("#maxBid").val();
                                if (seEng.indexOf('Google') > 0) {
                                    if (maxBid < 0.01) {
                                        alertMsg = "As Google format is selected : \n";
                                        alertMsg += "Bid Amount should be minimum 0.01!\n";
                                        $(".maxBidError").html(alertMsg);
                                    }
                                } else if (seEng.indexOf('Bing') > 0) {
                                    if (maxBid < 0.05) {
                                        alertMsg = "As Bing format is selected : \n";
                                        alertMsg += "Bid Amount should be minimum 0.05!\n";
                                        $(".maxBidError").html(alertMsg);
                                    }
                                }
                                if (maxBid !== "") {
                                    var valid = validateNumberORFloat(maxBid);
                                    if (valid === null) {
                                        alertMsg = "Please Enter a valid max bid";
                                        $(".maxBidError").html(alertMsg);
                                    }
                                }
                                if (!($.trim($("#url").val()))) {
                                    alertMsg = "Please enter Destination URL!";
                                    $(".urlError").html(alertMsg);
                                } else if (!urlValidation($('#url').val().trim())) {
                                    alertMsg = "Please enter a valid website URL";
                                    $(".urlError").html(alertMsg);
                                }
                            }
                        }
                    }
                    if (getLines('colOne') > 15) {
                        alertMsg = "Only 15 keywords are accepted.";
                        $(".colOneError").html(alertMsg);
//                        var data = getTruncData('colOne');
//                        $("#colOne").val(data);
                    }
                    if (getLines('colTwo') > 15) {
                        alertMsg = "Only 15 keywords are accepted.";
                        $(".colTwoError").html(alertMsg);
//                        var data = getTruncData('colTwo');
//                        $("#colTwo").val(data);
                    }
                    if (getLines('colThree') > 15) {
                        alertMsg = "Only 15 keywords are accepted.";
                        $(".colThreeError").html(alertMsg);
//                        var data = getTruncData('colThree');
//                        $("#colThree").val(data);
                    }
                    if (alertMsg !== "") {
                        return false;
                    } else {
                        var $this = $(this);
                        $this.button('loading');
                        var f = $("#keywordCombination");
                        f.attr('action', "ppc-keyword-combination-tool.html?ViewResult='ViewResult'");
                        f.submit();
                    }
                });

                $("#clearAll").on('click', function () {
                    parent.window.location = "/ppc-keyword-combination-tool.html";
                });
                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();
            });
            function sendReportToMail() {
                $('#mail-sent-status').hide();
                var emailid = $.trim($("#email").val());
                var status = true;
                var statusMsg = "";
                if (emailid === "") {
                    statusMsg = "Please provide your email";
                    status = false;
                } else if (!isValidMail(emailid)) {
                    statusMsg = "Please enter a valid email address.";
                    status = false;
                }
                if (!status) {
                    $("#email").focus();
                    $('#mail-sent-status').show();
                    $("#mail-sent-status").html(statusMsg);
                    $('#mail-sent-status').css("color", "red");
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/ppc-keyword-combination-tool.html?download='sendmail'&report-type='sendmail'&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }

            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    var f = $("#keywordCombination");
                    f.attr('method', "POST");
                    f.attr('action', "/ppc-keyword-combination-tool.html?download=download&report-type=" + reportType);
                    f.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function getLines(id) {
                return $('#' + id).val().split("\n").length;
            }
            function getTruncData(id) {
                var data = $('#' + id).val();
                var len = data.split("\n").length;
                var trunData = "";
                if (len > 15) {
                    var eachRow = data.split("\n");
                    for (var i = 0; i < 15; i++) {
                        if (i === 14) {
                            trunData += eachRow[i];
                            break;
                        } else {
                            trunData += eachRow[i] + "\n";
                        }
                    }
                }
                return trunData;
            }
            function hideAdvancedOptions() {
                $('input[name="powerPost"]').prop("checked", false);
                resetAdvancedInputs("advanced");
                $("#advanced-label-plus").show();
                $("#advanced-label-minus").hide();
                $("#advanced-inputs-section").hide(1000);
                $("#advanced").val("");
                $("#maxBid").val("0.01");
                
                /*Hideing search engine inputs*/
                $("#search-engine-inputs").hide(1000);
                $('input[name="searchEngine"]').prop("checked", false);
                resetAdvancedInputs("powerPost");
            }
            function showAdvancedOptions() {
                $("#advanced-label-plus").hide();
                $("#advanced-label-minus").show();
                $("#advanced-inputs-section").show(1000);
                $("#advanced").val("true");

            }
            function resetAdvancedInputs(section) {
                if (section === "advanced") {
                    $("#prefix").val("");
                    $("#suffix").val("");
                }
                $("#url").val("");
                $("#maxBid").val(0.01);
            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/ppc-keyword-combination-tool.html?ate='keyWordCombinatorATE'",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                var userToolInputs = JSON.parse(userInput);
                $('#colOne').val(userToolInputs["2"]);
                $('#colTwo').val(userToolInputs["3"]);
                $('#colThree').val(userToolInputs["4"]);
                $('#prefix').val(userToolInputs["5"]);
                $('#suffix').val(userToolInputs["6"]);
                if (userToolInputs["7"] !== "" && userToolInputs["7"] !== null) {
                    $("#powerPost").prop("checked", true);
                    if (userToolInputs["7"].indexOf("google")) {
                        $("#searchEngine1").prop("checked", true);
                    } else {
                        $("#searchEngine2").prop("checked", true);
                    }
                    $('#maxBid').val(userToolInputs["8"]);
                    $('#url').val(userToolInputs["9"]);
                }
                var i = 10;
                if (userToolInputs.hasOwnProperty("" + i)) {
                    $("#matchType1").prop("checked", true);
                    i++;
                }
                if (userToolInputs.hasOwnProperty("" + i)) {
                    $("#matchType2").prop("checked", true);
                    i++;
                }
                if (userToolInputs.hasOwnProperty("" + i)) {
                    $("#matchType3").prop("checked", true);
                }
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container keyword-combinator-inputs">
            <div class="col-xs-12 mobile-padding-none">
                <blockquote class="blockquote">
                    <p>
                        The Keyword Combinator is designed to help you generate long tail keywords for your ad campaigns. 
                        This tool generates all possible keyword combinations using your keyword input 
                        and is reported in an easily downloadable format.
                    </p>
                </blockquote>
            </div>
            <div class="clearfixdiv"></div>
            <springForm:form modelAttribute="keywordCombination" method="POST">
                <div class="col-xs-12" style="margin-bottom: 2%;">
                    <h4 class="lxrm-bold" style="margin-bottom: 0;">
                        GENERATES AN EXHAUSTIVE LIST OF KEYWORD COMBINATIONS USING ALL COLUMNS AND ROWS
                    </h4>
                    <span>Please enter maximum 15 words per column</span>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="colOne">COLUMN 1 (REQUIRED):
                                <span>
                                    <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                       title="<img src='../resources/images/keycomb-help.png' alt='keycomhelp image'/>">
                                        <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                    </a>
                                </span>

                            </label>
                            <springForm:textarea path="colOne" placeholder="Description" cssClass="form-control" rows="5" style="resize:none;"/>
                            <span class="colOneError lxr-danger error-info"></span>
                        </div>
                        <div class="form-group col-md-4">   
                            <label for="colTwo">COLUMN 2:</label>
                            <springForm:textarea path="colTwo" placeholder="Description" cssClass="form-control" rows="5" style="resize:none;"/>
                            <span class="colTwoError lxr-danger error-info"></span>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="colThree">COLUMN 3:</label>
                            <springForm:textarea path="colThree" placeholder="Description" cssClass="form-control" rows="5" style="resize:none;"/>
                            <span class="colThreeError lxr-danger error-info"></span>
                        </div>
                        <div class="col-xs-12 padding-none">
                            <div class="col-xs-12 col-md-2" style="padding-right: 0;">
                                <label class="lxrm-bold">MATCH TYPE:
                                    <span>
                                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                           title="Match types tell you how search terms that triggered your ads on search engine are related to actual keywords in your account.">
                                            <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                        </a>
                                    </span> &nbsp;&nbsp;&nbsp;&nbsp;</label>
                            </div>
                            <div class="col-xs-12 col-md-10">
                                <label class="checkbox-inline">
                                    <springForm:checkbox path="matchType" value="Broad" />Broad
                                </label>
                                <label class="checkbox-inline">
                                    <springForm:checkbox path="matchType" value="Phrase" />Phrase
                                </label>
                                <label class="checkbox-inline">
                                    <springForm:checkbox path="matchType" value="Exact"/>Exact
                                </label>
                            </div>
                            <div class="col-xs-12">
                                <span class="matchTypeError lxr-danger error-info"></span>
                            </div>

                        </div>
                        <div class="col-xs-12" style="height: 10px;"></div>
                        <div class="col-md-12" style="margin-top: 1%;">
                            <springForm:hidden path="advanced"/>
                            <div class="col-xs-12 padding-none" style="margin-bottom: 2%;">
                                <span class="lxrm-bold" id="advanced-label-plus" onclick="showAdvancedOptions();"><i class="fa fa-plus-square-o"></i>&nbsp;&nbsp; ADVANCED</span>
                                <span class="lxrm-bold" id="advanced-label-minus" onclick="hideAdvancedOptions();" style="display: none;"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp; ADVANCED</span>
                            </div>
                            <div class="col-xs-12 padding-none" id="advanced-inputs-section" style="display: none;">
                                <div class="form-group">
                                    <label class="col-md-2 control-label padding-none">Prefix:</label>
                                    <div class="col-md-10 padding-none input-margin-bottom">
                                        <springForm:input path="prefix" cssClass="form-control" maxlength="100" placeholder="prefix" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label  padding-none">Suffix:</label>
                                    <div class="col-md-10 padding-none input-margin-bottom">
                                        <springForm:input path="suffix" cssClass="form-control" maxlength="100"  placeholder="suffix" />
                                    </div>
                                </div>
                                <div class="col-xs-12 padding-none">
                                    <div class="checkbox">
                                        <label><springForm:checkbox path="powerPost" />AdWords/Bing Ads Power Posting
                                            <span>
                                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                                   title="Generates keywords in AdWords Editor or  Bing Ads Desktop formats." />
                                                <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12 padding-none" id="search-engine-inputs" style="display: none;">
                                    <div class="radio col-md-12 padding-none">
                                        <c:set var="engineName" value=""/>
                                        <c:forEach items="${searchEngineList}" var="searchEngine" varStatus="i">
                                            <c:if test="${i.index == 0}">
                                                <c:set var="engineName" value="Show Results in Google AdWords Format"/>
                                            </c:if>
                                            <c:if test="${i.index == 1}">
                                                <c:set var="engineName" value="Show Results in  Bing Ads Format"/>
                                            </c:if>
                                            <c:choose>
                                                <c:when test="${keywordCombination.searchEngine eq engineName}">
                                                    <div class="col-xs-12 col-sm-6 col-md-4 padding-none">
                                                        <label>
                                                            <input id="searchEngine${i.index}" name="searchEngine" type="radio" value="${searchEngine}"/>${engineName}
                                                        </label>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="col-xs-12 col-sm-6 col-md-4 padding-none">
                                                        <label>
                                                            <input id="searchEngine${i.index}" name="searchEngine" type="radio" value="${searchEngine}"/>${engineName}
                                                        </label>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </div>
                                    <div style="height: 8px;clear: both;" class="visible-xs visible-sm"></div>
                                    <div class="form-group input-margin-top">
                                        <label class="col-md-2 control-label padding-none">Max Bid:</label>
                                        <div class="col-md-10 padding-none input-margin-bottom">
                                            <springForm:input path="maxBid" cssClass="form-control number-only" />
                                            <span class="maxBidError lxr-danger error-info"></span>
                                        </div>
                                    </div>
                                    <div class="form-group input-margin-top">
                                        <label class="col-md-2 control-label padding-none">Destination URL:</label>
                                        <div class="col-md-10 padding-none input-margin-bottom">
                                            <springForm:input path="url" cssClass="form-control" placeholder="Website's URL" />
                                            <span class="urlError lxr-danger error-info"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv"></div>
                    <div class="row" id="toolButtonsWrapper" align="center">
<!--                        <div class="col-xs-12">
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles col-xs-12 col-sm-12 col-md-8 col-lg-12"  id="clearAll">
                                        <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                    </button>
                                </div>
                            </div>
                            <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles lxr-search-btn col-xs-12 col-sm-12 col-md-8 col-lg-12"  id="getResult" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING">
                                        &nbsp;GENERATE&nbsp;
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        </div>-->
                        <div class="col-xs-12 mobile-padding-none">
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 text-center">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles col-xs-12"  id="clearAll">
                                        <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles lxr-search-btn col-xs-12"  id="getResult" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING">
                                        &nbsp;GENERATE&nbsp;
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        </div>
                    </div>
                </div>
            </springForm:form>
        </div>
        <div class="clearfixdiv"></div>
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) && (keywordCombination ne '' && keywordCombination ne null)}">
            <div class="container" id="result-div">
                <div class="col-md-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd !important;" id="firstcontent-1">
                    <div class="col-xs-12 lxr-panel-heading1" style="padding: 6px 12px;">
                        <span class="panel-title lxrm-bold">GENERATED ALL POSSIBLE KEYWORD COMBINATIONS</span>
                        <p id="issue-text-1">
                            Generates all possible keyword combinations using your own keyword input and is reported in an easily downloadable format.
                        </p>
                    </div>
                    <div class="col-xs-12" style="padding: 1%;">
                        <textarea class="col-xs-12 lxrsrt-codesnippet1" id="jsondata" rows="20" style="resize:none;border: none;">${resultText}</textarea>
                    </div>
                    <div class="col-xs-12 lxr-panel-footer1 text-left" style="padding: 1%;border-top: 1px solid #ddd;">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                            <div style="clear: both;height: 8px;" class="visible-lg visible-md"></div>
                            <span class="lxrm-bold">Get generated code:</span>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                </div>
                            </div>
                            <div id="mail-sent-status" class="emptyData"></div>
                        </div>
                        <div class="col-md-1 col-sm-2 col-xs-2">
                            <div class="dropdown">
                                <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                <div id="downloadAnalysisReport" class="dropdown-content">
                                    <p class="lxrm-bold" onclick="downloadFormat('xls');">XLS</p>
                                    <p class="lxrm-bold" onclick="downloadFormat('csv');">CSV</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--how to fix-->
                <!--                <div id="howtofix-1" style="display: none;">
                                    <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading lxr-panel-heading">
                                                <span class="panel-title lxr-light-danger">Below are the steps to fix on page issues on the website</span>
                                                <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                            </div>
                                            <div class="panel-body lxr-panel-body">
                                                <p class="json-ld-step">
                                                    JavaScript Object Notation for Linked Data (JSON-LD) is 
                                                    a WCW3 standard website method of structured markup implementation using the JSON notation. 
                                                    Major search engines such as Google, Bing, and Yandex can understand 
                                                    JSON-LD for pages on your site.
                                                </p>
                                                <p class="microdata-step" style="display: none;">
                                                    Microdata is "hidden HTML code" that can be inserted in web pages. 
                                                    Microdata is also commonly referred to as structured data or Schema Markup Code.
                                                </p>
                                                <div class="lxr-panel-footer">
                                                    <h3><spring:message code="ate.expert.label.2" /></h3>
                                                    <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>-->
            </div>
            <div class="clearfixdiv"></div>

        </c:if>

        <!--Recommended part start-->
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>