
<%-- 
    Document   : SiteMap Builder
    Created on : Sep 09, 2017, 11:24:49 AM
    Author     : NE16T1213
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Calendar, lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails, lxr.marketplace.sitemapbuilder.SiteMapBuilderTool"%>
<!DOCTYPE html> 
<html>
    <head>
        <title>Sitemap Generator| Sitemap Builder| LXR Marketplace</title>
        <meta name="description"
              content="Try the Free Sitemap Generator at LXRMarketplace. This SEO tool generates detailed sitemaps for any website. Try it today!">
        <meta name="keywords" content="sitemap generator">
        <%@ include file="/WEB-INF/views/commonImportsWithDatePicker.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/sitemapBuildr.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <%
            int toolDBId = 6;
            Object userLoggedInObject = session.getAttribute("userLoggedIn");
            boolean isUserLoggedIn = false;
            if (userLoggedInObject != null) {
                isUserLoggedIn = true;
            }
            /* paidDownloadedReportDetailsList is for previous downloads, 
                if user have downloaded any report in tool.
                Only for logged in users.
             */
            if (isUserLoggedIn) {
                if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                    List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                    pageContext.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
                }
            }
        %>
        <script type="text/javascript">
            <%  out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");%>
            var reportDownload = 0;
            var downloadReportType = "";
            var timeToStopConditionMet = false;
            var isProcesscompleted = false;
            var enableFields = false;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
            <c:if test="${notWorkingUrl !=null && !notWorkingUrl.equals('')}">
                $("#userMessage").empty();
                $("#userMessage").show();
                $("#userMessage").html('${notWorkingUrl}');
            </c:if>
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success'))}">
                $("#editImport, #editEmail").show();
                $("#inputUrl, #email").addClass("gray");
                $('html, body').animate({scrollTop: $('.toolResultWrapper').offset().top}, 'slow');
            </c:if>
//                toolLimitExceeded();
                /*Date picker*/
                var Digital = new Date();
                var dd = Digital.getDate();
                var mm = Digital.getMonth() + 1;
                var yyyy = Digital.getFullYear();
                var hours = Digital.getHours();
                var minutes = Digital.getMinutes();
                var seconds = Digital.getSeconds();
                var totdt = dd + "/" + mm + "/" + yyyy + " " + hours + ":" + minutes + ":" + seconds;
                $("#getResult").bind('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    $("#queryMessage").empty();
                    var status = true;
                    var inputUrl = $("#inputUrl").val().trim();
                    var emailid = $("#email").val().trim();

                    if (inputUrl === "" || inputUrl === null || inputUrl === 0) {
                        document.getElementById('inputUrlError').textContent = "Please enter your URL";
                        document.getElementById('inputUrlError').style.display = 'block';
                        $('#inputUrl').focus();
                        status = false;
                    } else {
                        document.getElementById('inputUrlError').style.display = 'none';
                    }

                    if (emailid === "" || emailid === null || emailid === 0) {
                        document.getElementById('emailError').textContent = "Please enter your email";
                        document.getElementById('emailError').style.display = 'block';
                        $('#email').focus();
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        document.getElementById('emailError').textContent = "Please enter a valid email address";
                        document.getElementById('emailError').style.display = 'block';
                        $('#email').focus();
                        status = false;
                    } else {
                        document.getElementById('emailError').style.display = 'none';
                    }
                    if (status) {
                        $("#reportNotification").html("");
                        $('#getResult, #resetSiteMap').attr('disabled', true);
                        $('#inputUrl, #frequency, #email, #download').attr('readonly', true);
                        $(".siteMapOptions").attr('readonly', true);
                        $('#reviewURL, #reportNotification').html("");
                        var $this = $(this);
                        $this.button('loading');
                        var form = $("#siteMapBuilderTool");
                        form.attr('action', "seo-sitemap-builder-tool.html?getResult=getResult");
                        form.submit();
                    }
                });
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report") !== -1) {
                    downloadFormat();
                    refreshPage();
                }
                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }

                $('#customeDatePicker').datetimepicker({
                    format: 'DD/MM/YYYY hh:mm:ss',
                    maxDate: new Date(serverTime)
                });
                $("#siteMapDate").val(totdt);
                $('#resetSiteMap').bind('click', function () {
                    $('#inputUrl').val("");
                    $('#example6').val(totdt);
                    $('#frequency').val(1);
                    $('#sepriority').val(1.0);
                    $('#reviewURL,#reportNotification').html("");
                    $('#lastmod').val(2);
                    $('#userMessage').html("");
                    var form = $("#siteMapBuilderTool");
                    form.attr('action', "seo-sitemap-builder-tool.html?clearAll='clearAll'");
                    form.submit();
                });
                $("#download").bind('click', function () {
                    downloadFormat();
                });
                $('#editImport, #editEmail').attr('disabled', true);
                $('#inputUrl').on('click', function () {
            <c:if test="${status.equals('success')}">
                    if (isProcesscompleted || enableFields) {
                        $("#userMessage").html("");
                        $("#editImport").hide();
                        $("#inputUrl").removeClass("gray");
                        $('#getResult, #resetSiteMap').attr('disabled', false);
                    }
            </c:if>
                });
                $('#email, #editEmail').on('click', function () {
            <c:if test="${status.equals('success')}">
                    if (isProcesscompleted || enableFields) {
                        $("#userMessage").html("");
                        $("#editEmail").hide();
                        $("#email").removeClass("gray");
                        $('#getResult, #resetSiteMap').attr('disabled', false);
                    }
            </c:if>
                });
                var existedEmail = $("#email").val().trim();
                if (existedEmail === null || existedEmail === undefined || existedEmail === '') {
                    $("#email").val(loginLxrmUserEmail);
                }
            });/*End of on ready*/
            function refreshPage() {
                if (!parentUrl.indexOf("report") !== -1) {
            <c:if test="${status.equals('success')}">
                    $('#getResult, #resetSiteMap').attr('disabled', true);
                    enableFields = true;
            </c:if>
            <c:choose>
                <c:when test="${sessionScope.loginrefresh == null}">
                    setTimeout('sucess()', 2000);
                </c:when>
                <c:otherwise>
                    <c:if test="${sessionScope.ld != null}">
                    $("#linkDepthWrapper").html("Link Depth:<span class='lxrm-bold'> ${sessionScope.ld}</span>");
                    </c:if>
                    <c:if test="${sessionScope.ps != null}">
                    $("#pagesScannedWrapper").html("Pages Scanned:<span class='lxrm-bold'> ${sessionScope.ps}</span>");
                    </c:if>
                    <c:if test="${sessionScope.pl != null}">
                    $("#pageLeftWrapper").html("Pages Left:<span class='lxrm-bold'> ${sessionScope.pl}</span>");
                    </c:if>
                    <c:if test="${sessionScope.tp != null}">
                        $("#totalPagesWrapper").html("Total Pages:<span class='lxrm-bold'> ${sessionScope.tp}</span>");
                    </c:if>
                    <c:if test="${sessionScope.siteMapThreadTerminated != null}">
                    timeToStopConditionMet = <%=((Boolean) session.getAttribute("siteMapThreadTerminated")).booleanValue()%>;
                    </c:if>
                    <c:if test="${sessionScope.siteMapThreadTerminated != null}">
                    isProcesscompleted = <%=((Boolean) session.getAttribute("siteMapProcessCompleted")).booleanValue()%>;
                    if (isProcesscompleted) {
                        $('#downloadWrapper, #download').show();
                        $('#download').attr('disabled', false);
                        $('#getResult, #resetSiteMap').attr('disabled', false);
                        $(".siteMapOptions").attr('disabled', false);
                    }
                    </c:if>
                    $("#inputUrl, #frequency, #email").attr('readonly', false);
                    $(".siteMapOptions").attr('disabled', false);
                    $('#getResult, #resetSiteMap').attr('disabled', false);
                    $('#loadImage').hide();
                    enableFields = true;
                    /*Added based on Tester feedback*/
                    $('#downloadWrapper, #download').show();
                    if ((parentUrl.indexOf("report") !== -1 && parentUrl.indexOf("loginRefresh") !== -1)) {
                        $('#downloadWrapper, #download').show();
                        $(".siteMapOptions").attr('disabled', true);
                    }
                </c:otherwise>
            </c:choose>
                }
            }
            function sucess() {
                if (!timeToStopConditionMet || !isProcesscompleted) {
                    if (timeToStopConditionMet)
                    {
                        $('.toolResultWrapper #loadImage').hide();
                        
                        $('.toolResultWrapper #loadImage').css('display','none');
                        $('#siteMapStatus').show();
                        $('#editImport, #editEmail').attr('disabled', true);
                    }
                    getUpdate();
            <c:if test="${status.equals('success')&& !isProcesscompleted }">
                    $('#getResult, #resetSiteMap').attr('disabled', true);
                   
            </c:if>
                    $('#loadImage').show();
                    $('#download, #editImport,  #editEmail').attr('disabled', true);
                    $('#inputUrl').attr('readonly', true);
                    $(".siteMapOptions").attr('disabled', true);
                    $('#getResult, #resetSiteMap').attr('disabled', true);
                    /*Added based on Tester feedback*/
                    $('#downloadWrapper, #download, #successMessageWrapper').hide();

                } else if (isProcesscompleted) {
                    $('#loadImage').hide();
                    $('#siteMapStatus').hide();
                    $('#inputUrl, #frequency, #email').attr('readonly', false);
                    $('#editImport, #editEmail').attr('disabled', false);
                    $(".siteMapOptions").attr('disabled', false);
                    $('#getResult, #resetSiteMap, #download').attr('disabled', false);
                    /*Added based on Testing Team feedback*/
                    $('#successMessageWrapper, #downloadWrapper, #download').show();
                    /*Calling dynamically ask our expert function*/
                    /*Passing empty value for variable toolWiseAskExpert of this function*/
                    getAskExpertPopup(false);
                    return false;
                }
            }
            function getUpdate() {
                $.ajax({
                    type: "POST",
                    url: "/seo-sitemap-builder-tool.html?getdata=getdata",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data !== null && data.length !== 0) {
                            timeToStopConditionMet = data[0];
                            isProcesscompleted = data[1];
                            $("#linkDepthWrapper").html("Link Depth: <span class='lxrm-bold'> " + data[2] + "</span>")
                            $("#pagesScannedWrapper").html("Pages Scanned: <span class='lxrm-bold'> " + data[3] + "</span>")
                            $("#pageLeftWrapper").html("Pages Left: <span class='lxrm-bold'> " + data[4] + "</span>")
                            $("#totalPagesWrapper").html("Total Pages: <span class='lxrm-bold'> " + data[5] + "</span>")
                            if (data[2] === 0 && data[3] === 0 && data[4] === 0 && data[5] === 0) {
                                $('#resetSiteMap').click();
                            } else {
                                refreshPage();
                            }
                        } else if (data === null || data.length === 0) {
                            $('#resetSiteMap').click();
                        }
                    },
                    error: function () {
                        $('#getResult, #resetSiteMap, .siteMapOptions').attr('disabled', false);
                        $('#loadImage, #download, #downloadWrapper').hide();
                        $('#inputUrl').attr('readonly', false);
                    }
                });
            }

            function getToolRelatedIssues() {
                var url = $("#inputUrl").val();
                $.ajax({
                    type: "POST",
                    url: "/seo-sitemap-builder-tool.html?siteMapATE='siteMapATE'&siteMapURL=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                            /*Bottom Analysis*/
                            $("#bottom_help_domain").val(data[0].domain);
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                $('#inputUrl').val(website);
                if (userInput !== "") {
                    var userToolInputs = JSON.parse(userInput);
                    $('#frequency').val(userToolInputs["2"]);
                    if (userToolInputs["3"] === 1) {
                        $("#lastmod1").prop("checked", true);
                    } else if (userToolInputs["3"] === 2) {
                        $("#lastmod2").prop("checked", true);
                    } else {
                        $("#customdate").prop("checked", true);
                        $('#example6').val(userToolInputs["4"]);
                    }
                    if (userToolInputs["5"] === 1.0) {
                        $("#sepriority1").prop("checked", true);
                    } else {
                        $("#sepriority2").prop("checked", true);
                    }
                    $('#email').val(userToolInputs["6"]);
                }
                $('#getResult').click();
            }
            // send mail and download report to user
            function downloadFormat() {
                if (userLoggedIn) {
                    var f = $("#siteMapBuilderTool");
                    f.attr('method', "POST");
                    f.attr('action', "/seo-sitemap-builder-tool.html?download='download'");
                    f.submit();
                } else {
                    reportDownload = 1;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function giveDownloadReport(fileName) {
                var form = $("#siteMapBuilderTool");
                form.attr('method', "POST");
                form.attr('action', "/seo-sitemap-builder-tool.html?prvDownloadFile=" + fileName);
                form.submit();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>

    </head>
    <body onload="refreshPage()">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container-fluid">
            <div class="inputurl">
                <springForm:form class="form-horizontal" commandName="siteMapBuilderTool" method="POST">

                    <div class="form-group">
                        <label class="col-lg-5 col-md-5 control-label text-right">Enter Domain:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:input path="inputUrl" cssClass="form-control " placeholder="Enter your URL"  />
                                <span id="inputUrlError" class="error" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Change Frequency:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:select path="frequency" cssClass="form-control siteMapOptions">
                                    <springForm:option value="1" label="None" />
                                    <springForm:option value="2" label="Always" />
                                    <springForm:option value="3" label="Hourly" />
                                    <springForm:option value="4" label="Daily" />
                                    <springForm:option value="5" label="Weekly" />
                                    <springForm:option value="6" label="Monthly" />
                                    <springForm:option value="7" label="Never" />
                                </springForm:select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Last Modification:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-12">
                                <springForm:radiobutton path="lastmod" class="siteMapOptions" value="1" /> None
                            </div>
                            <div class="col-lg-12">
                                <springForm:radiobutton path="lastmod" class="siteMapOptions" value="2" /> Use Server's Response
                            </div>
                            <div class='col-lg-6 col-md-8'>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon lxrm-radio-group"><springForm:radiobutton path="lastmod" class="siteMapOptions" value="3" id="customdate" /> Custom Date:</span>
                                    <div class='input-group date' id='customeDatePicker'>
                                        <springForm:input  id="siteMapDate"  class="form-control" path="userdate"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Priority:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:radiobutton path="sepriority" class="siteMapOptions" value="1.0" /> None<br/>
                                <springForm:radiobutton path="sepriority" class="siteMapOptions" value="2.0" /> Automatically Calculated Priority
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-5 col-md-5 control-label text-right">Your Email:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:input path="email" cssClass="form-control" placeHolder="Enter your email" />
                                <span id="emailError" class="error" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            Note: Sitemap.zip will be mailed to your email address
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="queryMessage" class="col-lg-12 text-center error">
                            ${notWorkingUrl}
                        </div>
                    </div>
                </springForm:form>
            </div>
            <div class="row" align="center">
                <div class="col-xs-12">
                    <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12"  id="resetSiteMap">
                                <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                            </button>
                        </div>
                    </div>
                    <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12 lxr-search-btn"  id="getResult" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING..">
                                &nbsp;GENERATE&nbsp;
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                </div>
            </div>
            <div class="clearfixdiv"></div>
            <c:if test="${status.equals('success')}">
                <div class="row toolResultWrapper">
                    <div class="col-xs-12">
                        <p>
                            <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                            <span class="lxrm-bold">Review of: </span>
                            <span>${sessionScope.SiteMapUrl}</span>
                        </p>    
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center" id="linkDepthWrapper">Link Depth:<span class="lxrm-bold"></span></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center" id="pagesScannedWrapper">Pages Scanned:<span class="lxrm-bold"></span></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center" id="pageLeftWrapper">Pages Left:<span class="lxrm-bold"></span></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center" id="totalPagesWrapper">Total Pages:<span class="lxrm-bold"></span></div>
                    </div>
                    <div class="col-xs-12 text-center" id="loadImage" >
                        <div class="col-xs-12" style="font-size:1.9em"><i class="fa fa-refresh fa-spin" aria-hidden="true"></i></div>
                        <div id="siteMapStatus" class="col-xs-12 green">Verifying scanned pages</div>
                        <div id="loadImage" class="col-xs-12">Scanning the website pages</div>
                    </div>
                    <div class="col-xs-12 text-center" id="downloadWrapper">
                        <div id="successMessageWrapper" class="col-xs-12 green text-center">Your sitemap is ready. Please click on download.</div>
                        <div class="col-sm-4 col-md-5 col-lg-5 hidden-xs"></div>
                        <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn">
                                <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 col-sm-12 col-md-8 col-lg-12 lxr-search-btn"  id="download">
                                    DOWNLOAD
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-5 hidden-xs"></div>
                    </div>         
                </div>
            </c:if>
        </div>
        
        <div class='container'>
            <c:if test="${status.equals('success')}">
            <div class="row">
                <div class="col-xs-12 note" style="font-size: 1.1em; margin-top: 1em;display:block">Note:The process may take upto 15-30 minutes depending  on the number of pages in the website. We recommend to  return back to download the files.</div>
            </div>
            </c:if>
        </div>
        <div class="container">
            <!--Recently Downloaded Reports:Begin -->
            <c:if test = "${paidDownloadedReportDetailsList != null && paidDownloadedReportDetailsList != '' && fn:length(paidDownloadedReportDetailsList) gt 0}">
                <c:if test="${fn:length(paidDownloadedReportDetailsList) gt 0}">
                    <div class="row">
                    <div class="col-xs-12 prev-dwnld-div">
                        <h4 class='prvs-reports'>Previous Downloaded Reports:</h4>
                        <div style="padding: 0;overflow: auto;max-height: 250px;">
                            <table class="table lxr-table" style="border: 1px solid #ddd;">
                                <thead style="background: white;color: #4F4F4F;">
                                    <tr>
                                        <th class="text-left">Created Time</th>
                                        <th class="text-left">Domain Name</th>
                                        <th class="text-left">Report Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${paidDownloadedReportDetailsList}" var="paidReports">
                                        <tr>
                                            <td class="text-left">${paidReports.createdTime}</td>
                                            <td class="text-left">${paidReports.url}</td>
                                            <td class="text-left" onclick="giveDownloadReport('${paidReports.reportName}');" style="cursor: pointer; color: #2f80ed !important;">${paidReports.reportName}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </c:if>
            </c:if>
            <c:if test = "${fileExistence eq 'Sorry the requested file is no longer existed.'}">  
                <div class="row">
                    <div id="reportNotification" class="col-xs-12 col-lg-12 notworking prev-dwnld-div">
                        <p class="text-center lxr-danger">${fileExistence}</p>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
