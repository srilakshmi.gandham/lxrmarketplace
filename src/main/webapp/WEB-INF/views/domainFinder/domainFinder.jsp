<%-- 
    Document   : domainFinder
    Created on : 30 Jan, 2019, 1:18:55 PM
    Author     : NE16T1213/Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google-signin-scope" content="profile email">
        <meta name='google-signin-client_id'  content='587399183753-k9c1ldj0kg3k21kiesjedpo78o7jf7k5.apps.googleusercontent.com'/>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <title> Domain Finder</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <script type="text/javascript" src="/resources/js/angular/angular.min.js"></script>
        <script src="/resources/js/tools/domainFinder/domainFinder.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/domainFinder.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var downloadReportType = "";
            var userEmail = "";
            var isGmailLoginSuccess = false;
            $(document).on('change', ':file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(document).ready(function () {
                $('#feedUploadWrapper').click(function () {
                    var manuallyContent = $.trim($("#queryCompanyNames").val());
                    manuallyContent = manuallyContent.replace(/&nbsp;/gi, " ");
                    manuallyContent = manuallyContent.replace(/<br>/gi, " ");
                    manuallyContent = manuallyContent.trim();
                    if (manuallyContent !== undefined && manuallyContent !== null && manuallyContent !== "") {
                        var response = confirm('Manually entered company names needs to be cleared before uploading the file.\nDo you want to clear now ?');
                        if (response === true) {
                            $('#queryCompanyNames').val('');
                            $('#queryCompanyNames').empty();
                            $("#inputQueryError").empty();
                            angular.element(document.getElementById('domainFinderJS')).scope().resetManaullyQuery();
                        } else {
                            if (($.trim($("#queryCompanyNames").val()) && ($.trim($("#feedPath").val()) &&
                                    ($("#keywordsFeedList")[0].files[0] !== undefined
                                            && $("#keywordsFeedList")[0].files[0] !== ''
                                            && $("#keywordsFeedList")[0].files[0] !== null)))) {
                                $("#keywordsFeedList").val('');
                                $("#feedPath").val('');
                                $("#keywordsFeedList, #feedPath").empty();
                            }
                            return false;
                        }
                    }
                });
                $(':file').on('fileselect', function (event, numFiles, label) {
                    var input = $(".file-upload-wrapper").find('input[type=text]'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;
                    if (input.length) {
                        input.val(log);
                    }
                });
            });
            function getLines(id) {
                var queryURLSCount = 0;
                var lines = $.trim($('#' + id).val()).split("\n");
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].length > 0)
                        queryURLSCount++;
                }
                return queryURLSCount;
            }
        </script>   
    </head>
    <body ng-app="domainFinderPreSales" ng-controller="DomainFinderController as ctrl" id="domainFinderJS">
        <%@ include file="/WEB-INF/views/emptyHeader.jsp"%>
        <div class="container-fluid">
            <nav style="text-align: right;padding: 3px;font-size: 12px;color: #828282 !important">
                <a class="breadcrumb-item" href="/">Home ></a>
                <span class="breadcrums Regular color2"><a class="breadcrumb-item" href="presales-tools.html">PreSales ></a></span>
                <span itemprop="name"><span class="breadcrumb-item">Domain Finder</span></span>
            </nav>
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="col-xs-2 col-sm-1 col-md-1" style="padding: 0px">
                        <span class="badge lxrsrt-toolicon pull-left" style="background-color: #184F78"> <img src="/resources/images/tool-icons/DF.png" alt="DF.png"></span>
                    </div>
                    <div class="col-xs-10 col-sm-11 col-md-11">
                        <h2 id="toolName" style="font-family: ProximaNova-Bold;">
                            Domain Finder
                        </h2>
                        <span class="label label-default" style="padding: 5px;background-color: #F58120;">
                            5.0
                            <span class="glyphicon glyphicon-star"></span> 
                        </span>
                    </div>
                </div>
            </div>
        </div><!--EOF of container-fluid-->
        <div class="clearfixdiv"></div>
        <div class="container">
            <form name="domainFinderForm" class="form-horizontal" id="domainFinderForm" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12 mobile-padding-none">
                        <div class="col-xs-12">
                            <blockquote class="blockquote">
                                <p>Instantly find the domain / URL for the list of company names.</p>
                                <p>Google API is used to transform this detail from company name to domain / URL. Google has a throttle limit and overall number of queries which is less than 1K. If the service fails to identify the detail, please retry later or the next day.</p>
                                <p>Please upload a maximum of 1000 company names.</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 20px !important;"></div>
                    <div class="col-xs-12 tool-input-wrapper">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="queryCompanyNames" class="control-label">Enter the list of company name(s):
                                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                           title="Enter company name(s) in the text box below. Enter one company name per line (i.e: netelixir).">
                                            <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                        </a>
                                    </label>
                                    <textarea class="form-control lxr-no-resize col-xs-9" ng-model="queryCompanyNames" name="queryCompanyNames" id="queryCompanyNames" rows="5" placeHolder="Enter the brand keyword, one per line (i.e: netelixir)."></textarea>
                                </div>
                                <p id="inputQueryError" class="error"></p>
                            </div>
                            <div style="height:10px;clear: both;" class="visible-xs visible-sm"></div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="uploadLabel empty-left-padding"><span style="color:#4CAF50;font-family: 'ProximaNova-Bold';">(OR)</span> Upload your company name(s) list (.xls /.xlsx formats) : </label>
                                    <div class="col-xs-12 empty-left-padding ">
                                        <div class="col-xs-10 empty-left-padding file-upload-wrapper">
                                            <div class="input-group">
                                                <input type="text" readonly="readonly" id="feedPath" class="form-control" placeholder="Uploaded file name" style="height: 3.5rem;">
                                                <span id="clearFeedPath" style="cursor: pointer;" class="input-group-addon"><i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 empty-left-padding" id="feedUploadWrapper" style="padding-right: 0;">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary file-action-wrapper" style="width: 100%;">
                                                    Browse<input type="file" file-model="keywordsFeedList" name="keywordsFeedList" id="keywordsFeedList" style="display: none;"> 
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <p class="columnInfo">Mandatory Column: Company Name</p>
                                </div>
                                <div class="clearfixdiv" style="height: 10px !important"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 tool-input-wrapper">
                            <div style="height:10px;clear: both;"></div>
                            <div class="col-xs-12">
                                <div class="col-xs-4 visible-md visible-lg"></div>
                                <div class="col-xs-12 col-sm-6 col-md-2">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="resetDomainForm()" class="btn top-line btn-lg buttonStyles col-xs-12"  id="resetDomainFinder">
                                            <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                                <div class="col-xs-12 col-sm-6 col-md-2">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="processDomainFinderQuery()" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="getResult" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                            &nbsp;ANALYZE&nbsp;
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-4 visible-md visible-lg"></div>
                            </div>
                            <div class="col-xs-12 text-center">
                                <p id="toolMsgInfo" class="lxrm-bold toolInfoWrapper"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p id="errorMessageInfo" class="toolInfoWrapper lxrm-bold"></p>
                    </div>
                </div>
            </form>
            <div class="clearfixdiv"></div>
            <div class="row" id="domainFinderResultWrapper">
                <div class="col-xs-12">
                    <div class="col-xs-12 panel-default">
                        <div class="panel-heading lxr-panel-heading row" style="height: 6.5rem;padding-top: 2rem;">
                            <div class="col-xs-12"><span>Company Name mapped to Domain / URL</span></div>
                        </div>
                        <div class="panel-body lxr-panel-body" style="width: 100%;">
                            <div class="col-xs-12 table-parent-div text-nowrap" style="width: 100%;">
                                <table class="table" id="domainFinderAnalysis" style="word-break: break-all;width: 100%;">
                                    <thead style="background: #22A6B3 !important;">
                                        <tr>
                                            <th class="text-center">S.No.</th>
                                            <th>Company Name</th>
                                            <th>Domain / URL</th>
                                        </tr>
                                    </thead> 
                                    <tbody style="word-break: break-all;" ng-if="ctrl.domainFinderResultList !== null && ctrl.domainFinderResultList.length > 0">
                                        <tr ng-repeat="domainFinder in ctrl.domainFinderResultList track by $index">
                                            <td class="text-center">{{$index + 1}}.</td>
                                            <td>{{domainFinder.brandKeyword}}</td>
                                            <td ng-switch="domainFinder.domainURL !== null" class="text-left">
                                                <p ng-switch-when="true" ng-switch='domainFinder.domainURL !== "Google fetch limit exceeded"'> 
                                                    <a ng-switch-when="true" href="{{domainFinder.domainURL}}" target="_blank">{{domainFinder.domainURL}}</a>
                                                    <span ng-switch-default class="error"> Google fetch limit exceeded </span>
                                                </p>
                                                <p ng-switch-default class="error">
                                                    No Matching Domain / URL Found
                                                </p>
                                            </td>                                           
                                        </tr>
                                    </tbody>
                                    <tbody style="word-break: break-all;" ng-if="ctrl.domainFinderResultList === null || ctrl.domainFinderResultList.length === 0">
                                        <tr><td class="text-center lxrm-bold error" style="font-size: 16px;" colspan="3">No records found</td></tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="clearfixdiv" style="height: 5px !important;"></div>
                        </div>
                        <div class="panel-footer lxr-panel-tool-footer" style="height: 6.5rem;">
                            <div class="row" ng-if="ctrl.domainFinderResultList !== null && ctrl.domainFinderResultList.length > 0">
                                <div class="col-xs-3 visible-md visible-lg"></div>
                                <div class="col-xs-12 col-sm-4 col-lg-3">
                                    <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                    <input type="text" class="form-control emailWrapper" placeholder="Enter your email address" name="userEmail" id="userEmail">
                                    <div id="mailSentStatus" class="emptyData"></div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-2" style="padding-top: 0.5%;" ng-init="clearAnalayzedCount()">
                                    <div id="mailDomainFinder"  ng-click="sendDomainFinderReport()" style="font-size: 20px;cursor: pointer;padding-left: 0;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-send sendReport"></div>
                                    <div id="downloadDomainFinderReport" ng-click="downloadDomainFinderReport()" style="font-size: 20px;cursor: pointer;" class="col-xs-12 col-sm-5 col-lg-4 glyphicon glyphicon-download-alt"></div>
                                    <div class="col-xs-12 col-sm-5 col-lg-4 visible-md visible-lg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfixdiv" style="height: 185px !important;"></div>
                </div>
            </div>
        </div>
        <div id="googleLoginPopupModal" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="top: 190px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title lxrm-bold">Login with NETELIXIR Email</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="padding-left: 3rem; text-align: center;" id="loginBodyWrapper" style="display:none;">
                                <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style='margin-left: 20%;'></div>
                                <div class="clearfixdiv" style="height: 13px !important;"></div>
                            </div>
                            <div class="col-xs-12" id="errorBodyWrapper" style="display:none;">
                                <div class="col-xs-12 text-center">
                                    <p id="loginErrorMessage" class="text-center" style="color: red;font-size: 14px;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" role="dialog" id="confirmationLimitPopupModal"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content" style="top: 100px;">
                    <div class="modal-header">
                        <h4 class="modal-title lxrm-bold">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p id="confirmationMessageWrraper"></p>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-12 mtb">
                                    <div class="input-group col-xs-12">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="offlineEmail" type="text" class="form-control" name="offlineEmail" placeholder="Enter your email address">
                                    </div>
                                    <p id="offlineError" class="mt"></p>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="cancelInput()" class="btn top-line btn-lg buttonStyles col-xs-12"  id="cancelInput">
                                            Cancel this input
                                        </button>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group-btn">
                                        <button type="button" ng-click="acceptConditions()" class="btn top-line btn-lg buttonStyles col-xs-12 lxr-search-btn"  id="limitAccepted">
                                            Continue with offline Result 
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv" style="height: 105px !important;"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
        <script type="text/javascript">
                    var gauth = sessionStorage.getItem('GAuthToken');
                    if (gauth === null) {
                        gmailLoginSuccess();
                        $("#googleLoginPopupModal").modal('show');
                        var errorMessage = sessionStorage.getItem('DomainFinderError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                        setTimeout(function () {
                            sessionStorage.setItem("DomainFinderError", '');
                        }, 10);
                        function onSignIn(googleUser) {
                            // Useful data for your client-side scripts:
                            var profile = googleUser.getBasicProfile();
                            console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                            console.log('Full Name: ' + profile.getName());
                            console.log('Given Name: ' + profile.getGivenName());
                            console.log('Family Name: ' + profile.getFamilyName());
                            console.log("Image URL: " + profile.getImageUrl());
                            console.log("Email: " + profile.getEmail());
                            userEmail = profile.getEmail();
                            if (userEmail.indexOf('@netelixir.com') >= 0) {
                                sessionStorage.setItem("userOAuthEmail", userEmail);
                                isGmailLoginSuccess = true;
                                $("#googleLoginPopupModal").modal('hide');
                                gmailLoginSuccess();
                                $("#userEmail").val(userEmail);
                                var id_token = googleUser.getAuthResponse().id_token;
//                        console.log("ID Token: " + id_token);
                                sessionStorage.setItem("GAuthToken", id_token);
                            } else {
                                isGmailLoginSuccess = false;
                                sessionStorage.setItem("DomainFinderError", "Please sign in with @netelixir.com email to use Pre-Sales Tools");
                                signOutfromGoogle();
                            }
                        }
                        ;
                    } else if (gauth !== null) {
                        $("#googleLoginPopupModal").modal('hide');
                        var userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
                        if (userOAuthEmail !== null) {
                            $("#userEmail").val(userOAuthEmail);
                        }
                        isGmailLoginSuccess = true;
                    }
                    function gmailLoginSuccess() {
                        $("#loginBodyWrapper").show();
                        $("#errorBodyWrapper").hide();
                    }
                    function signOutfromGoogle() {
                        var auth2 = gapi.auth2.getAuthInstance();
                        auth2.signOut().then(function () {
                            console.log('User signed out.');
                            sessionStorage.setItem("GAuthToken", '');
                            $('<iframe style="display: none" src="https://accounts.google.com/Logout"></iframe>').appendTo(document.body);
//                    location.reload(true);
                        });
                        var errorMessage = sessionStorage.getItem('DomainFinderError');
                        if (errorMessage !== null) {
                            $("#loginErrorMessage").empty();
                            $("#errorBodyWrapper").show();
                            $("#loginErrorMessage").html(errorMessage);
                        }
                        setTimeout(function () {
                            sessionStorage.setItem("DomainFinderError", '');
                        }, 10);
                    }
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    </body>
</html>
