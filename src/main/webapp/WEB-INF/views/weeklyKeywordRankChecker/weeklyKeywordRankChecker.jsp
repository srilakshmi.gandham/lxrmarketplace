
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="lxr.marketplace.keywordrankchecker.KeywordRankChecker"%>
<%@page import="lxr.marketplace.keywordrankchecker.KeywordRankCheckerStats"%>
<%@page import="lxr.marketplace.keywordrankchecker.CompDomainSettings"%>
<%@page import="lxr.marketplace.keywordrankchecker.KeywordSettings"%>
<!DOCTYPE html>

<html>
    <head>
        <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <title>Keyword Rank Checker Tool – SEO Rank Tracker | LXR Marketplace</title>
        <meta name="description" content="Try the Free Weekly Keyword Ranker Tool at LXRMarketplace. Track SERP’s ranking positions. Monitor & analyze daily website rankings. Find out more!.">
        <meta name="keywords" content="Keyword Rank Checker, SEO Rank Tracker Tools, Rank Checker Tools">

        <%@ include file="/WEB-INF/views/commonImportsWithDatePicker.jsp"%>
        <link rel="stylesheet" type="text/css" href="../../../resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/tools/weeklyKeywordRankChecker.css">
        <script type="text/javascript" src="/resources/js/tools/weeklyKeywordRankChecekr.js"></script>


        <%
            int toolDBId = 10;
            long totalReviewdUserCount1 = 0;
            KeywordRankChecker kwdSettings = null;
            if (session.getAttribute("totalReviewedUsers") != null) {
                totalReviewdUserCount1 = (long) (Long) session.getAttribute("totalReviewedUsers");
            }
            String reqParam = request.getParameter("query");

            String rating = request.getParameter("rating");

            List<KeywordSettings> keyRows = (List<KeywordSettings>) session.getAttribute("sortedKeywordRows");
            List<CompDomainSettings> compRows = (List<CompDomainSettings>) session.getAttribute("compDomainRows");
            Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = (Map<Long, List<KeywordRankCheckerStats>>) session.getAttribute("keywordStatsMap");
            if (session.getAttribute("userSettingData") != null) {
                kwdSettings = (KeywordRankChecker) session.getAttribute("userSettingData");
            }

            pageContext.setAttribute("compDomainRows", session.getAttribute("compDomainRows"));
            pageContext.setAttribute("keywordStatsMap", session.getAttribute("keywordStatsMap"));
            pageContext.setAttribute("sortedKeywordRows", session.getAttribute("sortedKeywordRows"));
        %>
        <script type="text/javascript" src="/scripts/Charts.js"></script>
        <script type="text/javascript" src="/scripts/lxrm-commons.js"></script>

        <script type="text/javascript">
            <%
                out.println("var reqParam=" + reqParam + ";");
                out.println("var rating=" + rating + ";");
                long userDomId = 0, keywordId = 0;
                int flagCount = 0;
                if (keyRows != null && keyRows.size() > 0) {
                    keywordId = keyRows.get(0).getKeywordId();
                }

                out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                out.println("var selectedFirstKeyId=" + keywordId + ";");
                int REG_USER_KEYWORD_LIMIT = 5;
                int REG_USER_DOMAIN_LIMIT = 4;

            %>

            var userLoggedIn = '${userLoggedIn}';
            var var1 = "${geoLocation}";
            var lang = "${loadLanguages}";
            var toolId = 10;
            var compDomNote = "Enter your competitor's domains one per line (maximum 3 allowed)";
            var keywordNote = "Enter your keywords one per line (maximum 5 allowed)";
            var yourDomain = "Enter your domain";
            var parentUrl = top.location.href;
            /* my code */
            var fullDate = new Date(serverTime);
            var fd;
            var td;
            var option;
            var dayInMilSec = 24 * 60 * 60 * 1000;
            var keyIsNotEmpty = false;
            var notValidAlertVal = false;
            var fid;
            var shownKeywordArray = new Array();
            var hiddenKeywordArray = new Array();
            var hiddenDomArray = new Array();
            var shownDomArray = new Array();
            var chartGraph = "false";
            chartGraph = '${googleGraph}';
            var reportDownload = 0;
            var downloadReportType = "";
            var expertButtonLabel = "Get Keyword Audit For Your Website";
            <%KeywordRankChecker keywordRankChecker = (KeywordRankChecker) session.getAttribute("keywordRankChecker");%>

            $(function () {
                $('.datepicker').datetimepicker({
                    format: 'DD-MM-YYYY',
                    maxDate: new Date(serverTime)
                });

                $("#toDate").on("dp.change", function (e) {
                    if ($('#dateRangeOption').val() === "-1") {
                        gocall();
                    }
                });
                $("#fromDate").on("dp.change", function (e) {
                    if ($('#dateRangeOption').val() === "-1") {
                        gocall();
                    }
                });
            });

            function gocall() {
                fd = $("#fromDate").val().split("-");
                td = $("#toDate").val().split("-");

                fd = new Date(fd[2], fd[1] - 1, fd[0]);
                td = new Date(td[2], td[1] - 1, td[0]);

                if (fd > td) {
                    alert("Start Date is less than End Date, \n please select a valid date range");
                    $('#toDate').data("DateTimePicker").date(new Date(fullDate));
                    $("#fromDate").focus();
                    return false;
                } else {
                    loadGraphData();
                }
            }

            $(document).ready(function () {
                isPageRefresh = true;
                //Setting default values
            <c:if test="${userLoggedIn eq true}" >
                if (chartGraph === "true") {
                    $('#dateRangeOption').val("3");
                    td = new Date(fullDate);
                    fd = new Date(fullDate - new Date(28 * dayInMilSec));

                    $('#fromDate').data("DateTimePicker").date(new Date(fd));
                    $('#toDate').data("DateTimePicker").date(new Date(td));

                    $("#statsChart").show();

                    loadGraphData();
                    var parentUrl = top.location.href;
                    if ((parentUrl.indexOf("getResult=") === -1)) {
                        setTimeout(getAskExpertPopup(true), 1000);
                    }

                }
                showKeywordChart(selectedFirstKeyId);
                $("#settingsLinkDiv").show();
                $("#selectDateRange").show();

            </c:if>

                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();
                if ('${notWorkingUrl}' != null) {
                    $("#new-user-notwrk").show();
                }
//                toolLimitExceeded();


            <c:set var="showSuccess" value="${showSuccess}" />
            <c:set var="clear" value="${clear}" />
            <c:set var="notWorking" value='${notWorkingUrl}' />
            <c:set var="compNotWorking" value='${compNotWorkingUrl}' />

            <%if (kwdSettings != null) {
                    if (kwdSettings.isToolUnsubscription()) {%>
                $("#resultDiv").hide();
            <%}
                }%>




            <c:if test="${userLoggedIn == false}" >
                $("#dateandChart").css("display", "none");
                $(".daily-table").css("margin-top", "3%");
            </c:if>

                $('#dateRangeOption').change(function () {
                    option = $('#dateRangeOption').val();
                    if (option === "-1") {
                        $('#cDateRange').show();
                    } else {
                        $('#cDateRange').hide();
                        if (option === "3") {
                            fd = new Date(fullDate - new Date(28 * dayInMilSec));
                        } else if (option === "4") {
                            fd = new Date(new Date().getFullYear(), 0, 1);
                        } else if (option === "5") {
                            fd = new Date(fullDate - new Date(90 * dayInMilSec));
                        }

                        td = new Date(fullDate);
                        $('#fromDate').data("DateTimePicker").date(new Date(fd));
                        $('#toDate').data("DateTimePicker").date(new Date(td));

                        loadGraphData();
                    }
                });

                //Keywords tabke
                keywordsTable();

            <%long negCount = 0;
                if (keywordRankChecker != null) {
                    long keyCount = keywordRankChecker.getKeyCount();
                    if (keyRows != null && keyRows.size() > 0) {
                        for (KeywordSettings keyRow : keyRows) {
                            out.println("var positiveKeyId=\"" + keyRow.getKeywordId() + "\";");%>
                shownKeywordArray.push(positiveKeyId);
            <%}
                }
                if (keyCount < REG_USER_KEYWORD_LIMIT) {
                    negCount = REG_USER_KEYWORD_LIMIT - keyCount;%>
            <%long j = 1;
                for (int i = 1; i <= negCount; i++) {
                    j--;
                    out.println("var val=\"" + j + "\";");%>
                hiddenKeywordArray.push(val);
            <%}
                    }
                }%>
            <%long negDomCount = 0;
                if (keywordRankChecker != null) {
                    long domainCount = keywordRankChecker.getDomainCount();
                    if (compRows != null && compRows.size() > 0) {
                        for (CompDomainSettings cmpDom : compRows) {
                            out.println("var positiveDomId=\"" + cmpDom.getDomainId() + "\";");%>
                shownDomArray.push(positiveDomId);
            <%}
                }
                if (domainCount < REG_USER_DOMAIN_LIMIT) {
                    negDomCount = REG_USER_DOMAIN_LIMIT - domainCount;%>
            <%long x = 1;
                for (int i = 1; i <= negDomCount; i++) {
                    x--;
                    out.println("var domVal=\"" + x + "\";");%>
                hiddenDomArray.push(domVal);
            <%}
                    }
                }%>

                $("#alertVal").keypress('click', function (e) {
                    return isNumberKey(e);
                });

                $("#getResult").bind('click', function (e) {
//                     resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }

                    var status = true;
                    var keywords = $('#keywords').val().trim();
                    var userDomain = $('#userdomain').val().trim();
                    var compDomain = $("#compDomains").val().trim();

                    if (keywords === "" || keywords === null) {
                        document.getElementById('keywordsError').textContent = "Please enter keyword(s)";
                        document.getElementById('keywordsError').style.display = 'block';
                        $('#keywords').focus();
                        status = false;
                    } else if (keywords.indexOf(',') !== -1) {
                        document.getElementById('keywordsError').textContent = "Keywords are to be entered one per line and commas are not allowed";
                        document.getElementById('keywordsError').style.display = 'block';
                        $('#keywords').focus();
                        status = false;
                    } else {
                        document.getElementById('keywordsError').style.display = 'none';
                    }

                    if (userDomain === "" || userDomain === null) {
                        document.getElementById('userdomainError').textContent = "Please enter your domain";
                        document.getElementById('userdomainError').style.display = 'block';
                        $('#userdomain').focus();
                        status = false;
                    } else {
                        document.getElementById('userdomainError').style.display = 'none';
                    }

                    if (compDomain === compDomNote) {
                        $("#compDomains").val("");
                    } else {
                        document.getElementById('compDomainError').style.display = 'none';
                    }

                    if (compDomain.indexOf(',') !== -1) {
                        document.getElementById('compDomainError').textContent = "Competitor domains to be entered one per line and commas are not allowed.";
                        document.getElementById('compDomainError').style.display = 'block';
                        $('#compDomains').focus();
                        status = false;
                    } else {
                        document.getElementById('compDomainError').style.display = 'none';
                    }

                    if (status) {
                        var $this = $(this);
                        $this.button('loading');

                        var f = $("#keywordRankCheckerTool");
                        f.attr('action', "/weekly-keyword-rank-checker-tool.html?getResult='getResult'");
                        f.submit();
                    }

                });

                $("#save").bind('click', function (e) {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if (shownKeywordArray !== null && shownKeywordArray.length > 0) {
                        var notValidAlertVal = true, keyIsNotEmpty = true;
                        for (var i = 0; i < shownKeywordArray.length; i++) {
                            var state = document.getElementById("k" + shownKeywordArray[i]);
                            if (state.style.display !== 'none') {
                                var tempInputArray = state.getElementsByTagName('input');
                                if (tempInputArray[0].value.trim() === null || tempInputArray[0].value.trim() === "") {
                                    keyIsNotEmpty = false;
                                    tempInputArray[0].focus();
                                    alert("Keyword can not be empty, please delete the row instead");
                                    break;
                                } else {
                                    var selectArray = state.getElementsByTagName('select');
                                    if (selectArray[0].value !== "0") {
                                        var parseVal = parseInt(tempInputArray[1].value.trim());
                                        if (isNaN(parseVal) || parseVal <= 0 || parseVal > 100) {
                                            notValidAlertVal = false;
                                            alert("Enter Alert Value between 1 & 100");
                                            break;
                                        }
                                    } else if (tempInputArray[1].value.trim() !== null && tempInputArray[1].value.trim() !== "") {
                                        tempInputArray[1].value = "";
                                    }
                                }
                            }
                        }
                        if (!notValidAlertVal || !keyIsNotEmpty) {
                            return false;
                        } else {
                            var $this = $(this);
                            $this.button('loading');
                            var f = $("#keywordRankCheckerTool");
                            f.attr('action', "weekly-keyword-rank-checker-tool.html?saveSettingsData='saveSettingsData'");
                            $('#save').attr('disabled', true);
                            $('#cancel').attr('disabled', true);
                            f.submit();
                        }
                    }
                });

                $("#cancel").bind('click', function (e) {
                    var $this = $(this);
                    $this.button('loading');
                    var f = $("#keywordRankCheckerTool");
                    f.attr('action', "weekly-keyword-rank-checker-tool.html?cancelSettingsData='cancelSettingsData'");
                    $('#cancel').attr('disabled', true);
                    $('#save').attr('disabled', true);
                    f.submit();
                });
                if ('${compNotWorkingUrl}' !== "" && '${invalidDomainId}' === 0) {
                    if ('${invalidUrl}' !== "") {
                        var inValidURL = '${invalidUrl}';
                        showCompDomainRow(inValidURL);
                    } else {
                        showCompDomainRow('ADD');
                    }
                }

            });
            function keywordsTable() {
                var rows = "";
                //for user domain
            <c:set var="totDomCount" value="0"/>
            <c:forEach var="totalDom" items="${pageScope.compDomainRows}">
                <c:if test="${totalDom.domain != '' && totalDom.domain != null}">
                    <c:set var="totDomCount" value="${totDomCount+1}"/>
                </c:if>
            </c:forEach>
            <c:forEach var="domain" items="${pageScope.compDomainRows}">
                <c:if test="${domain.domain != '' && domain.domain != null}">
                    <c:set var="countVal" value="${countVal+1}"/>
                    <c:if test="${domain.userDomain == true}">
                        <c:set var="someID" value="${domain.domainId}" />
                rows += '<tr>'
                rows += '<th class="ud-header" colspan="2" rowspan="2">' + 'Keywords' + '</th>';
                rows += '<th class="ud-header" colspan="2">${domain.domain}</th>';
                        <c:if test="${totDomCount gt 1 }">
                rows += '<th class="ud-header cmpd-header" colspan="2">';
                        </c:if>
                rows += '</tr>';
                rows += '<tr>'
                        <c:forEach  begin="1" end="${totDomCount}" varStatus="val">

                rows += '<th class="google-rank" ><img src="/resources/images/google-favicon.png" alt="Google" /><span>Google Rank</span></th>';
                rows += '<th class="bing-rank"><img src="/resources/images/bing-favicon.png" alt="Bing" /><span>Bing Rank</span></th>';
                        </c:forEach>
                rows += '</tr>';
                        <c:set var="firstKeyId" value="${0}"/>
                        <c:forEach var="keywords" items="${pageScope.keywordStatsMap[someID]}" varStatus="status">
                            <c:if test="${firstKeyId == 0}">
                                <c:set var="firstKeywordId" value="${keywords.keywordId}"/>
                                <c:set var="firstKeyId" value="${1}"/>
                            </c:if>
                rows += '<tr>'
                            <c:if test="${userLoggedIn == true}" >
                rows += '<td>' + '<input type="radio" name="radVal" onclick="showKeywordChart(${keywords.keywordId},this)" value="${keywords.keywordId}" style="cursor:pointer;"' +
                                <c:if test="${keywords.keywordId == firstKeywordId}">
                'checked="checked"' + </c:if>
                '/>' + '</td>';
                rows += '<td>' + '<c:out value="${pageScope.sortedKeywordRows[status.index].keyword}" />' + '</td>';
                            </c:if>
                            <c:if test="${userLoggedIn == false}" >
                rows += '<td colspan="2">' + '<c:out value="${pageScope.sortedKeywordRows[status.index].keyword}" />' + '</td>';
                            </c:if>
                rows += '<td>'
                            <c:choose>
                                <c:when test="${keywords.keywordRankInG == 0}">
                + '<c:out value="–" />'
                                </c:when>
                                <c:when test="${keywords.keywordRankInG > 0}">
                + '<c:out value="${keywords.keywordRankInG}" />'
                                </c:when>
                                <c:when test="${keywords.keywordRankInG == -1}">
                + '<c:out value="> 100" />'
                                </c:when>
                            </c:choose>
                            <c:if test="${userLoggedIn == true}" >
                                <c:choose>
                                    <c:when test="${keywords.progressInG == 1}">
                + '<span class="greenUp">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInG == 0}">
                + '<span class="blueRight">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInG == -1}">
                + '<span class="redDown">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInG == 5 || keywords.progressInG == 10}">
                + '<span>' + '</span>'
                                    </c:when>
                                </c:choose>
                            </c:if>
                + '</td>';
                rows += '<td class="${keywords.keywordId}">'
                            <c:choose>
                                <c:when test="${keywords.keywordRankInB == 0}">
                + '<c:out value="–" />'
                                </c:when>
                                <c:when test="${keywords.keywordRankInB > 0}">
                + '<c:out value="${keywords.keywordRankInB}" />'
                                </c:when>
                                <c:when test="${keywords.keywordRankInB == -1}">
                + '<c:out value="> 100" />'
                                </c:when>
                            </c:choose>
                            <c:if test="${userLoggedIn == true}" >
                                <c:choose>
                                    <c:when test="${keywords.progressInB == 1}">
                + '<span class="greenUp">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInB == 0}">
                + '<span class="blueRight">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInB == -1}">
                + '<span class="redDown">' + '</span>'
                                    </c:when>
                                    <c:when test="${keywords.progressInB == 5 || keywords.progressInB == 10}">
                + '<span>' + '</span>'
                                    </c:when>
                                </c:choose>
                            </c:if>
                + '</td>';
                            <%--<c:if test="${totDomCount gt 1}">--%>
//                rows += '<td class="comp-rank"></td>';
                            <%--</c:if>--%>
                rows += '</tr>'
                        </c:forEach>
                    </c:if>
                </c:if>
            </c:forEach>


                $(".result-table").append(rows);
                //for competitor domain
            <c:set var="count" value="${0}"/>
            <c:set var="compdVal" value="${0}"/>
            <c:forEach var="domain" items="${pageScope.compDomainRows}">
                <c:if test="${domain.domain != '' && domain.domain != null}">
                    <c:set var="compdVal" value="${compdVal+1}" />
                    <c:if test="${compdVal gt 1}">
                        <c:if test="${domain.userDomain == false }">
                            <c:set var="count" value="${count+1}"/>
                            <c:set var="cmpDomId" value="${domain.domainId}" />

                            <c:if test="${compdVal == 2 }">
                $(".cmpd-header").addClass("cmpd-header${count}");
                $(".cmpd-header").append('${domain.domain}</th>');
                            </c:if>

                            <c:if test="${compdVal gt 2 }">
                $(".cmpd-header${count-1}").after('<th class="ud-header cmpd-header${count}" colspan="2">${domain.domain}</th>');
                            </c:if>


                            <c:set var="i" value="0"/>
                            <c:forEach var="keywords" items="${pageScope.keywordStatsMap[cmpDomId]}">



//                alert('${domain.domain}' + " - ${keywords.keywordId} - " + '${i}+" - "+${count}');
                                <c:if test="${compdVal == 2 }">
                $(".${keywords.keywordId}").after('<td class="g${keywords.keywordId}${count}${i}">'
                                    <c:choose>
                                        <c:when test="${keywords.keywordRankInG == 0}">
                + '<c:out value="–" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInG > 0}">
                + '<c:out value="${keywords.keywordRankInG}" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInG == -1}">
                + '<c:out value="> 100" />'
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${userLoggedIn == true}" >
                                        <c:choose>
                                            <c:when test="${keywords.progressInG == 1}">
                + '<span class="greenUp">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == 0}">
                + '<span class="blueRight">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == -1}">
                + '<span class="redDown">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == 5 || keywords.progressInG == 10}">
                + '<span>' + '</span>'
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                + '</td>'
                        );
                        $(".g${keywords.keywordId}${count}${i}").after('<td class="b${keywords.keywordId}${count}${i}">'

                                    <c:choose>
                                        <c:when test="${keywords.keywordRankInB == 0}">
                + '<c:out value="–" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInB > 0}">
                + '<c:out value="${keywords.keywordRankInB}" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInB == -1}">
                + '<c:out value="> 100" />'
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${userLoggedIn == true}" >
                                        <c:choose>
                                            <c:when test="${keywords.progressInB == 1}">
                + '<span class="greenUp">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == 0}">
                + '<span class="blueRight">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == -1}">
                + '<span class="redDown">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == 5 || keywords.progressInB == 10}">
                + '<span>' + '</span>'
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                + '</td>'
                        );
                                </c:if>

                                <c:if test="${compdVal gt 2 }">
                $(".b${keywords.keywordId}${count-1}${i}").after('<td class="g${keywords.keywordId}${count}${i}">'

                                    <c:choose>
                                        <c:when test="${keywords.keywordRankInG == 0}">
                + '<c:out value="–" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInG > 0}">
                + '<c:out value="${keywords.keywordRankInG}" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInG == -1}">
                + '<c:out value="> 100" />'
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${userLoggedIn == true}" >
                                        <c:choose>
                                            <c:when test="${keywords.progressInG == 1}">
                + '<span class="greenUp">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == 0}">
                + '<span class="blueRight">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == -1}">
                + '<span class="redDown">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInG == 5 || keywords.progressInG == 10}">
                + '<span>' + '</span>'
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                + '</td>'
                        );
                        $(".g${keywords.keywordId}${count}${i}").after('<td class="b${keywords.keywordId}${count}${i}">'

                                    <c:choose>
                                        <c:when test="${keywords.keywordRankInB == 0}">
                + '<c:out value="–" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInB > 0}">
                + '<c:out value="${keywords.keywordRankInB}" />'
                                        </c:when>
                                        <c:when test="${keywords.keywordRankInB == -1}">
                + '<c:out value="> 100" />'
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${userLoggedIn == true}" >
                                        <c:choose>
                                            <c:when test="${keywords.progressInB == 1}">
                + '<span class="greenUp">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == 0}">
                + '<span class="blueRight">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == -1}">
                + '<span class="redDown">' + '</span>'
                                            </c:when>
                                            <c:when test="${keywords.progressInB == 5 || keywords.progressInB == 10}">
                + '<span>' + '</span>'
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                + '</td>'
                        );
                                </c:if>
                                <c:set var="i" value="${i+1}"/>
                            </c:forEach>
                        </c:if>
                    </c:if>
                </c:if>
            </c:forEach>

            }
            //Ask Expert Block
            function getToolRelatedIssues() {
                var keywordDomain = "";
                keywordDomain = $("#domainDiv input").val();
                if (keywordDomain === undefined) {
                    keywordDomain = $("#userdomain").val();
                }
                $.ajax({
                    type: "POST",
                    url: "/weekly-keyword-rank-checker-tool.html?rankCheckerATE='rankCheckerATE'&keywordDomain=" + keywordDomain + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                var f = $("#keywordRankCheckerTool");
                f.attr('action', "weekly-keyword-rank-checker-tool.html?saveSettingsData='saveSettingsData'");
                $('#save').attr('disabled', true);
                $('#cancel').attr('disabled', true);
                f.submit();
            }
            function showCompDomainRow1() {
                if (hiddenDomArray.length > 0) {
                    var state = document.getElementById("d" + hiddenDomArray[0]);
                    if (state.style.display === 'none') {
                        document.getElementById("d" + hiddenDomArray[0]).style.display = 'block';
                        shownDomArray.push(hiddenDomArray[0]);
                    }
                    hiddenDomArray = hiddenDomArray.slice(1, hiddenDomArray.length);
                }
            }
            function submitPreviousRequest() {
                $.ajax({
                    type: "GET",
                    url: "/weekly-keyword-rank-checker-tool.html",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                    },
                    error: function () {
                    }, complete: function (jqXHR, textStatus) {
                    }
                });
                $("#save").click();
            }

        </script>


    </head>
    <body>

        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form class="form-horizontal" commandName="keywordRankCheckerTool" method="POST">
                <c:set var="showSettings" value="${showSettings}"/>
                <c:if test="${showSettings.equals('fail')}"> 
                    <div class="form-group">
                        <label class="col-lg-5 col-md-5 control-label text-right">Enter Keyword(s):</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:textarea path="keywords" cssClass="form-control input-lg srch-term" placeHolder="Enter your keywords one per line (maximum 5 allowed)"  rows="5"/>
                                <span id="keywordsError" class="error"/>
                            </div>
                        </div>

                    </div>

                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Geographical Location:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:select path="location" cssClass="form-control input-lg srch-term">
                                    <springForm:options items="${geoLocation}" />
                                </springForm:select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Language:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:select path="language" cssClass="form-control input-lg srch-term">
                                    <springForm:options items="${loadLanguages}" />
                                </springForm:select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-lg-5 col-md-5 control-label text-right">Your Domain:</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:input path="userdomain" cssClass="form-control input-lg srch-term" placeHolder="Enter your domain" />
                                <span id="userdomainError" class="error"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-5 col-md-5 control-label text-right">Competitor Domain(s):</label>
                        <div class="col-lg-7 col-md-7 text-left">
                            <div class="col-lg-6 col-md-8">
                                <springForm:textarea path="compDomains" cssClass="form-control input-lg srch-term" placeHolder="Enter your competitor's domains one per line (maximum 3 allowed)" rows="3"/>
                                <span id="compDomainError" class="error"/>
                            </div>
                            <div class="col-lg-12" style="margin-top: 2%;">
                                <div class="col-xs-12 col-sm-12 col-md-6" style="padding: 0;">
                                    <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles"  id="getResult"  data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GETTING KEYWORD RANKS..">
                                        GET KEYWORD RANKS
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="new-user-notwrk" class="Regular notworking" style="display: none;">
                        <p class="lxr-danger text-center">${invalidUrl} ${notWorkingUrl}</p>
                    </div>
                </c:if>

                <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
                <c:if test="${showSettings.equals('success') || (fn:contains(uri, 'user-id=') && fn:contains(uri, 'tracking-id='))}"> 
                    <div id="settingsdiv">

                        <div class="form-group ">
                            <label class="col-lg-3 col-md-3 control-label text-right">Keywords:</label>
                            <div class="col-lg-9 col-md-9 text-left">
                                <div class="col-lg-12" style="margin-top: 8px;">
                                    Add keywords and set conditions i.e Alert me when my <strong>Keyword's rank</strong> is <, =, > <strong>RANK</strong>
                                </div>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label class="col-lg-3 col-md-3 control-label text-right"></label>

                            <div class="col-lg-9 col-md-9 text-left">
                                <div class="col-lg-8 col-md-10">
                                    <div class="table-responsive">
                                        <table >
                                            <thead>
                                                <tr>
                                                    <th class="keywords-header ">Keyword(s)</th>
                                                    <th class="keywords-header ">Condition</th>
                                                    <th class="keywords-header ">Alert Value</th>
                                                    <th class="keywords-header ">Google</th>
                                                    <th class="keywords-header ">Bing</th>
                                                    <th class="keywords-header ">Remove</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${keywordRankCheckerTool.keywordSettingRows}" varStatus="settingRow">
                                                    <c:set var="keyStyle" value="" />
                                                    <c:set var="keywdId" value="${keywordRankCheckerTool.keywordSettingRows[settingRow.index].keywordId}" />
                                                    <c:choose>
                                                        <c:when test='${keywordRankCheckerTool.keywordSettingRows[settingRow.index].keywordId > 0}'><c:set var="keyStyle" value="" /></c:when>
                                                        <c:when test='${keywordRankCheckerTool.keywordSettingRows[settingRow.index].keywordId <= 0}'><c:set var="keyStyle" value="display:none;" /> </c:when>
                                                    </c:choose>


                                                    <tr id="k${keywdId}" style="${keyStyle}">
                                                        <td class="keywords-list"><springForm:input path="keywordSettingRows[${settingRow.index}].keyword" value="${status.value}" class="form-control dom-input" /></td>

                                                        <td class="keywords-list"><springForm:select path="keywordSettingRows[${settingRow.index}].alertOperator" class="form-control dom-input">
                                                                <springForm:option value="0">Select</springForm:option>
                                                                <springForm:option value="1">=</springForm:option>
                                                                <springForm:option value="2">&gt;</springForm:option>
                                                                <springForm:option value="3">&lt;</springForm:option>
                                                            </springForm:select></td>
                                                        <td class="keywords-list"><springForm:input path="keywordSettingRows[${settingRow.index}].alertValue" class="form-control dom-input"/></td>
                                                        <td class="keywords-list text-center"><springForm:checkbox path="keywordSettingRows[${settingRow.index}].googleCheckbox"  /></td>
                                                        <td class="keywords-list text-center"> <springForm:checkbox path="keywordSettingRows[${settingRow.index}].bingCheckbox"/></td>
                                                        <td class="keywords-list text-center"> <div onclick="deleteKeywordRow('${keywdId}');"><i class="fa fa-times" aria-hidden="true" style="cursor: pointer;"></i></div></td>

                                                    </tr>

                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-md-3"></div>
                            <div class="col-lg-9 col-md-9 text-right">
                                <div class="col-lg-8 col-md-10">
                                    <div class="add-button pull-right" onclick="showkeywordRow();">
                                        <i class="fa fa-plus" style="margin-top: 8px;" title="Added keywords" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-lg-3 col-md-3 control-label text-right">Your Domain:</label>
                            <div class="col-lg-9 col-md-9 text-left">
                                <div class="col-lg-8 col-md-10" id="domainDiv">
                                    <c:forEach items="${keywordRankCheckerTool.compDomainSettingRows}" varStatus="userDomName">
                                        <spring:bind path="keywordRankCheckerTool.compDomainSettingRows[${userDomName.index}].userDomain">
                                            <c:set var="totDomCount" value="${status.value}" />
                                        </spring:bind>
                                        <c:if test='${totDomCount == true}'>
                                            <springForm:input path="compDomainSettingRows[${userDomName.index}].domain" class="form-control dom-input cursor-ev" readonly="true" />
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 col-md-3 control-label text-right">Competitor Domain(s):</label>
                            <div class="col-lg-9 col-md-9 text-left">
                                <div class="col-lg-8 col-md-10">
                                    <c:set var="compDomainsFound" value="false" />
                                    <c:forEach items="${keywordRankCheckerTool.compDomainSettingRows}" varStatus="compDomainRow">
                                        <c:choose>
                                            <c:when test='${!keywordRankCheckerTool.compDomainSettingRows[compDomainRow.index].userDomain}'>
                                                <c:set var="compDomain" value="" />
                                                <c:set var="domainId" value="${keywordRankCheckerTool.compDomainSettingRows[compDomainRow.index].domainId}" />
                                                <c:choose>
                                                    <c:when test='${keywordRankCheckerTool.compDomainSettingRows[compDomainRow.index].domainId <= 0}'><c:set var="compDomain" value="display:none;margin-top: 10px;" /></c:when>
                                                    <c:when test='${keywordRankCheckerTool.compDomainSettingRows[compDomainRow.index].domainId > 0}'><c:set var="compDomain" value="display:block;margin-top: 10px;" /> 
                                                        <c:set var="compDomainsFound" value="true" />
                                                    </c:when>
                                                </c:choose>
                                                <div id="d${domainId}" style="${compDomain}">
                                                    <springForm:input path="compDomainSettingRows[${compDomainRow.index}].domain" class="form-control dom-input" />
                                                    <i class="fa fa-times comp-dom-remove" onclick="deleteCompDomainRow('${domainId}');" style="cursor: pointer;"></i>
                                                </div>

                                            </c:when>
                                        </c:choose>
                                    </c:forEach>
                                    <c:if test="${compDomainsFound eq false}">
                                        <input value="No competitor domains" class="form-control dom-input cursor-ev no-comp-val" readonly="readonly" />
                                    </c:if>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-lg-3 col-md-3"></div>
                            <div class="col-lg-9 col-md-9 text-right">
                                <div class="col-lg-8 col-md-10">
                                    <div class="add-button pull-right" onclick="showCompDomainRow('ADD');">
                                        <i class="fa fa-plus " style="margin-top: 8px" title="Added competitor domains" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div> 


                        <div id="settings" class="row col-lg-12 text-center ">
                            <span class="col-xs-12 col-md-3 col-lg-3"><springForm:checkbox path="alertStatus" /> Send me Keyword Alerts through email.</span>
                            <span class="col-xs-12 col-md-4 col-lg-3"><springForm:checkbox path="weeklyReport" /> Send me Weekly PDF Report through email.</span>
                            <span class="col-xs-12 col-md-5 col-lg-4"><springForm:checkbox path="toolUnsubscription" /> Unsubscribe from Weekly Keyword Rank Checker. 
                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                                   title="You will be unsubscribed from the tool and the data fetch will be stopped. You can resubscribe anytime">
                                    <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                </a>
                            </span>
                            <span class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                                <button type="button" id="cancel" class="btn lxr-search-btn top-line buttonStyles col-lg-6" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span>CANCEL..">
                                    CANCEL
                                </button>
                                <button type="button" id="save" class="btn lxr-search-btn top-line buttonStyles col-lg-5 col-xs-offset-1" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span>SAVE..">
                                    SAVE
                                </button>
                            </span>

                        </div>

                        <div id="settingsSucMsg" class="row col-lg-12 text-center" style="color: red; padding: 5px;">
                            <span><c:out value="${sucMsg}" /></span>	
                        </div>

                        <div id="notwrk" class="row col-lg-12 text-center">
                            <span class="error">${compNotWorkingUrl}</span>
                        </div>
                        <hr class="col-lg-12">
                    </div>

                </c:if>
            </springForm:form>
        </div>
        <div class="container-fluid">
            <c:set var="showSuccess" value="${showSuccess}" />
            <c:if test="${showSuccess.equals('success')}"> 
                <div id="showResult" class="col-lg-12">
                    <div class="daily-table table-responsive">
                        <table class="result-table col-lg-12 col-sm-12 col-xs-12"></table>
                    </div> 
                    <div class="clearfixdiv"></div>
                    <div id="resultDiv" >
                        <div id="dateandChart">
                            <div id="statsChart">

                                <div id="daterange" style="padding:8px; text-align: right;">
                                    <div id="commonDiv">
                                        <div id="selectDateRange" style="display: none;">
                                            <select id="dateRangeOption">
                                                <option value="3">Last 4 Weeks</option>
                                                <option value="4">This Year</option>
                                                <option value="-1">Custom Date</option>
                                            </select>
                                        </div>
                                        <div id="cDateRange" class="row " style="margin-top: 10px; display: none;">

                                            <div class="col-lg-4 pull-right" >
                                                <div class='col-xs-6' style="padding-right: 0px;">
                                                    <input type="text" id="fromDate" class="form-control datepicker"/>
                                                </div>
                                                <div class='col-xs-6' style="padding-right: 0px;">
                                                    <input type="text" id="toDate" class="form-control datepicker" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="statschartdiv" class="table-responsive" style="padding-bottom: 16px;text-align: center;"></div>
                            </div>
                        </div>


                        <div class="result-desc " style="margin-top: 2%;">
                            <div class='result-desc-para'>
                                <span><b>&ndash;</b>&nbsp;:&nbsp;No data available</span>
                            </div>
                            <div class='result-desc-para'>
                                <span><b>&gt; 100</b>&nbsp;:&nbsp;Keyword rank outside top 100 positions</span>
                            </div>
                        </div>
                        <div id="descrption">
                            <p><strong>Note:</strong> This rank is the result of the API calls provided by Google and Bing called at a specific instance of time every
                                day. Actual rank in the search engines might vary slightly.</p>
                        </div>

                        <c:if test="${userLoggedIn == false}" >
                            <div id="signupLink">
                                Register now to get automated weekly reports sent to your
                                mailbox, set custom alerts and generate keyword ranking trend
                                charts.<span onclick="signupPopUp('emailSignUP');" id="signup" style="margin-left: 10px; cursor: pointer;"> 
                                    <a style="color: #2F80ED !important;"><u>Sign Up Now!</u></a></span>
                            </div>
                        </c:if>

                    </div>

                    <div id="curve_chart"></div>
                </div> 
            </c:if>                                                         




        </div>


        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>

    </body>

</html>

