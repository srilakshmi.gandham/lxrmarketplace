
<%@page import="lxr.marketplace.user.Login"%>
<%
    ServletContext homeUserServletContext = request.getServletContext();
    int homeUserAquisitionPopupId = (Integer) homeUserServletContext.getAttribute("userAquisitionPopupId");
    HttpSession homeUserAquisitionsession = request.getSession();
    Object ulTip = homeUserAquisitionsession.getAttribute("userLoggedIn");
    Login homeUserAquisitionUser = (Login) homeUserAquisitionsession.getAttribute("user");
    boolean homeUserLoggedIn = false;
    if (ulTip != null) {
        homeUserLoggedIn = (Boolean) ulTip;
    }
%>
<script type="text/javascript">
    $(document).ready(function () {
        var homeUserAquisitionEmailId = "<%=homeUserAquisitionUser.getUserName()%>";
        var homeUserAquisitionPopupId = "<%=homeUserAquisitionPopupId%>";
        if (homeUserAquisitionEmailId !== "" && homeUserAquisitionEmailId !== 'Email') {
            if (homeUserAquisitionEmailId.indexOf("GuestUser") >= 0) {
                homeUserAquisitionEmailId = omeUserAquisitionEmailId.slice(homeUserAquisitionEmailId.indexOf("<") + 1, homeUserAquisitionEmailId.lastIndexOf(">"));
                $("#email").val(homeUserAquisitionEmailId);
            } else {
                $("#email").val(homeUserAquisitionEmailId);
            }
        }
        if (<%=(homeUserLoggedIn)%>) {
            var loginName = "<%=(homeUserAquisitionUser.getName())%>";
            var loginEmailID = " <%=(homeUserAquisitionUser.getUserName())%>";
            $("#name").attr("value", loginName);
            $("#email").attr("value", loginEmailID);
        }
        $("#user-aquisition-button").bind('click', function (e) {
            var name = $("#name").val().trim();
            var email = $("#email").val().trim();
            $("#ebook-success-msg").hide();
            if (name === "") {
                $("#name").addClass('inputErr');
                $("#ebook-error-msg").show();
                $("#ebook-error-msg").html("Please enter your name.");
                $("#name").focus();
                return false;
            }
            $("#name").removeClass('inputErr');
            if (email === "") {
                $("#email").addClass('inputErr');
                $("#ebook-error-msg").show();
                $("#ebook-error-msg").html("Please enter your email.");
                $("#email").focus();
                return false;
            }
            if (!isValidMail(email)) {
                $("#email").addClass('inputErr');
                $("#ebook-error-msg").show();
                $("#ebook-error-msg").html("Please enter a valid email.");
                $("#email").focus();
                return false;
            }
            $("#name").removeClass('inputErr');
            $("#email").removeClass('inputErr');
            $("#smallLoadImage").show();
            $("#ebook-success-msg").hide();
            $("#ebook-error-msg").hide();
            $("#user-aquisition-button").attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "/lxrm-send-user-request.html?emailId=" + email + "&name=" + name + "&userAquisitionPopupId=" + homeUserAquisitionPopupId,
                processData: true,
                data: {},
                dataType: "json",
                success: function (data) {
                    if (data[0] !== null)
                    {
                        $("#smallLoadImage").hide();
                        $("#ebook-success-msg").show();
                        $("#ebook-success-msg").html("Please check your mailbox to download the Ebook.");
                        $("#user-aquisition-button").attr('disabled', false);
                        $("#name").html("");
                        $("#email").html("");
                    }
                }
            });
        });
    });

    function isValidMail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
        return filter.test(email);
    }
</script>
<style>
#ebook-success-msg{margin-top: 1%;color: #00ff00;}
#ebook-error-msg{margin-top: 1%;color: red;}
.inputErr {border: 1px solid #cc4444 !important;}
</style>
<div class="row lxr-ebook" style="padding: 10px;background: #4F4F4F;">
    <div class="col-md-2">
        <img class="img-responsive" src="../../resources/images/ebook-banner.png" alt="ebook">
    </div>
    <div class="col-md-10">
        <div class="row lxr-ebook-sub" >
            <h6>FREE EBOOK: TOP 10 SEO Tactics That Work </h6>
            <p>Here are the top tactics that you can use to succeed in your SEO quest. Subscribe and download the free Ebook now! </p>
        </div>
        <div class="hidden-xs" style="height: 60px"></div>
        <div class="row" style="border: 0px red solid;">
            <form>
                <div class="form-group col-md-4" style="padding: 0px">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter your name">
                </div>
                <div class="form-group col-md-4 lxr-e1">
                    <input type="email" id="email" name="email" class="form-control"  placeholder="Enter your email">
                </div>
                <div class="form-group col-md-4 lxr-e1">
                    <button type="button" class="btn lxr-subscribe btn-block" id='user-aquisition-button'>GET IT NOW!</button>
                </div>
                <div id="ebook-error-msg" style="display: none;"></div>
                <div id="ebook-success-msg" style="display: none;"></div>
                <div id="smallLoadImage" style="display: none;">
                    <img src="../images/tools/processing.gif" alt="processing..."
                         title="processing" style="margin-left: 17px;" />
                </div>
            </form>
        </div>

    </div>
</div>

<div class="container-fluid hidden-xs" style="border: 0px red solid;height: 100px;clear: both"></div>
