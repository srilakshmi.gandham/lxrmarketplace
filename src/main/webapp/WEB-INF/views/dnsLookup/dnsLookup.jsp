
<%@page import="lxr.marketplace.dnslookup.DnsLookupResult"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Domain Name Server (DNS) Lookup Tool - LXR Marketplace</title>
        <meta name="description"
              content="Try the Free DNS (Domain Name Server) Lookup Tool at LXRMarketplace. Search a server name & get information for domain names or hostnames online. Visit now!">
        <meta name="keywords"
              content="Domain Name Servers, DNS Lookup, Hostname, DNS Record, Free DNS Lookup">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/dnsLookup.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <%
            int toolDBId = 28;
            DnsLookupResult dnsResult = null;
            if (session.getAttribute("dnsResult") != null) {
                dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
            }
            pageContext.setAttribute("dnsResult", dnsResult);
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${status}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report") !== -1) {
                    downloadFormat();
                }
                //  Success Page
                $("#getResult").bind('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var domainName = $("#url").val().trim();
                    if ($('#url').val() === " " || $('#url').val().trim() === ""
                            || $('#url').val() === null || $('#url').val() === "Enter Website URL") {
                        $("#notWorkingUrl").html("Please enter your URL");
                        $("#url").focus();
                        return false;
                    }
                    if (!checkDomain(domainName)) {
                        $("#notWorkingUrl").html("Please check the URL");
                        $("#url").focus();
                        return false;
                    }
                    $("#notWorkingUrl").html("");
                    var $this = $(this);
                    $this.button('loading');
                    var f = $("#dnsLookupTool");
                    f.attr('action', "dns-lookup-tool.html?getResult=getResult");
                    f.submit();
                });

                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        sendReportToMail();
                        return false;
                    }
                });
                $('#email').click(function () {
                    $('#aftermailsent').hide();
                    return false;
                });
//                toolLimitExceeded();
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                          && (dnsResult ne null && dnsResult != '')}">
                $("#editImport").show();
                $("#url").addClass("gray");
            </c:if>
                $('#url').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });
                $('#editImport, #url').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#url").removeClass("gray");
                });
            });
            function downloadFormat() {
                if (${userLoggedIn}) {
                    var f = $("#dnsLookupTool");
                    f.attr('method', "POST");
                    f.attr('action', "dns-lookup-tool.html?download=download");
                    f.submit();
                } else {
                    reportDownload = 1;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function sendReportToMail() {
                $('#mail-sent-status').hide();
                var emailid = $.trim($("#email").val());
                var status = true;
                var statusMsg = "";
                if (emailid === "") {
                    statusMsg = "Please provide your email";
                    status = false;
                } else if (!isValidMail(emailid)) {
                    statusMsg = "Please enter a valid email address.";
                    status = false;
                }
                if (!status) {
                    $("#email").focus();
                    $('#mail-sent-status').show();
                    $("#mail-sent-status").html(statusMsg);
                    $('#mail-sent-status').css("color", "red");
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/dns-lookup-tool.html?download=sendmail&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }

            //Ask Expert Block
            function getToolRelatedIssues() {

            }

            function getUserToolInputs(website, userInput) {
                $('#url').val(website);
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }

        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form commandName="dnsLookupTool" method="POST">
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="url" cssClass="form-control input-lg"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="notWorkingUrl" class="lxr-danger text-left" >${notWorkingUrl}</span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12" id="getResult"  data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>  
                </div>
            </springForm:form>

        </div>
        <!--<div class="row">-->
        <div class="clearfixdiv"></div>
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                      && (dnsResult != '' && dnsResult ne null)}">
              <div class="container-fluid" style="overflow-x: auto;">
                  <div class="col-xs-12 lxrm-padding-none">
                      <h4><span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span> <span> Review of </span> <span style="text-decoration:underline;">${dnsLookupTool.url}</span></h4>
                  </div>
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <!--<div class="col-xs-12 section-heading lxrm-padding-none">-->
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> A Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Address record:</span> Returns a 32-bit IPv4 address, most commonly used to map hostnames to an IP address of the host.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.aRecords)>0}">
                                  <thead>
                                      <tr>
                                          <th>Name</th>
                                          <th>Address</th>
                                          <th>Type</th>
                                          <th>Class</th>
                                          <th>TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="aRecord" items="${dnsResult.aRecords}">
                                          <tr>
                                              <%--<c:set var="aRecordName" value="${fn:replace(aRecord.name, '\\\.\$', '')}" />--%>
                                              <td>
                                                  ${aRecord.name}
                                              </td>
                                              <c:set var="aRecordAddr" value="${fn:split(aRecord.address, '/')}" />
                                              <td>${aRecordAddr[1]}</td>
                                              <td>${aRecord.type}</td>
                                              <td>${aRecord.DClass}</td>
                                              <td>${aRecord.TTL}s</td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.aRecords)==0}">
                                  <tr>
                                      <td>No record found for A Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>

                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> C Name Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Canonical name record:</span> Alias of one name to another, the DNS lookup will continue by retrying the lookup with the new name.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.cNameRecords) > 0}">
                                  <tbody>
                                      <c:forEach var="cRecord" items="${dnsResult.cNameRecords}">
                                          <tr>
                                              <td>CName</td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(cRecord.alias, '\\\.\\$', '')}" />--%>
                                                  ${cRecord.alias}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>Name</td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(cRecord.name,'\\\.\$', '')}" />--%>
                                                  ${cRecord.name}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>Type</td>
                                              <td>${cRecord.type}</td>
                                          </tr>
                                          <tr>
                                              <td>Class</td>
                                              <td>
                                                  ${cRecord.DClass}</td>
                                          </tr>
                                          <tr>
                                              <td title="Time to live">TTL</td>
                                              <td>${cRecord.TTL}s</td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.cNameRecords)==0}">
                                  <tr>
                                      <td style="text-align: center">No record found for CNAME Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>

                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">

                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> D Name Records <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Delegation Name:</span> Alias for a name and all its subnames, unlike CNAME, which is an alias for only the exact name.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <tbody>
                                  <c:if test="${fn:length(dnsResult.dRecords)==0}">
                                      <tr>
                                          <td class="text-center">No record found for DName Records</td>
                                      </tr>
                                  </c:if>
                                  <c:forEach var="dRecord" items="${dnsResult.dRecords}">
                                      <tr>
                                          <td>DName</td>
                                          <td>
                                              <%--<c:out value="${f:replaceEndDot(dRecord.target,'\\\.\$', '')}" />--%>
                                              ${dRecord.target}
                                          </td>
                                      </tr>
                                      <tr>
                                          <td title="Time to live">TTL</td>
                                          <td>${dRecord.TTL}s</td>
                                      </tr>
                                  </c:forEach>
                              </tbody>
                          </table>
                      </div>

                  </div>
                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> MX Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Mail exchange record:</span> Maps a domain name to a list of message transfer agents for that domain.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.mxRecords)>0}">
                                  <thead>
                                      <tr>
                                          <th>Host</th>
                                          <th>Preference Exchange</th>
                                          <th>Name</th>
                                          <th>Type</th>
                                          <th>Class</th>
                                          <th title="Time to live">TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="mxRecord" items="${dnsResult.mxRecords}">
                                          <tr>
                                              <td>
                                                  <%--<fmt:formatNumber value="${mxRecord.target}" pattern="^[.$]+$" var="recordTarget" />--%> 
                                                  <%--<c:out value="${fn:replace(mxRecord.target, '^[.$]+$', '')}" />--%>
                                                  ${mxRecord.target}
                                              </td>
                                              <td>${mxRecord.priority}</td>
                                              <td>
                                                  <%--<c:out value="${fn:replaceEndDot(mxRecord.name,'\.$', '')}" />--%>
                                                  ${mxRecord.name}
                                              </td>
                                              <td>${mxRecord.type}</td>
                                              <td>${mxRecord.DClass}</td>
                                              <td>${mxRecord.TTL}s</td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.mxRecords)==0}">
                                  <tr>
                                      <td colspan="6" class="text-center">No record found for MX Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>
                  </div>
                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> NS Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Name server record:</span> Delegates a DNS zone to use the given authoritative name servers.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.nsRecords) > 0}">
                                  <thead>
                                      <tr>
                                          <th>Nsd name</th>
                                          <th>Name</th>
                                          <th>Type</th>
                                          <th>Class</th>
                                          <th title="Time to live">TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="nsRecord" items="${dnsResult.nsRecords}">
                                          <tr>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(nsRecord.target,'\\\.\$', '')}" />--%>
                                                  ${nsRecord.target}
                                              </td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(nsRecord.name,'\\\.\$', '')}" />--%>
                                                  ${nsRecord.name}
                                              </td>
                                              <td>${nsRecord.type}</td>
                                              <td>${nsRecord.DClass}</td>
                                              <td>${nsRecord.TTL}s</td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.nsRecords)==0}">
                                  <tr>
                                      <td class="text-center">No record found for NS Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>
                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> TXT Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Text record:</span> Originally for arbitrary human-readable text in a DNS record.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.txtRecords)>0}">
                                  <thead>
                                      <tr>
                                          <th>Text</th>
                                          <th title="Time to live">TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="txtRecord" items="${dnsResult.txtRecords}">
                                          <tr>
                                              <td>${dnsResult.text}</td>
                                              <td>${txtRecord.TTL}s</td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.txtRecords)==0}">
                                  <tr>
                                      <td colspan="2" class="text-center">No record found for TXT Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>
                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> SOA Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>Start of [a zone of] authority record:</span> Specifies authoritative information about a DNS zone, including the primary name server, the email of the domain administrator, the domain serial number, and several timers relating to refreshing the zone.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.soaRecords)>0}">
                                  <thead>
                                      <tr>
                                          <th>Text</th>
                                          <th title="Time to live">TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="soaRecord" items="${dnsResult.soaRecords}">
                                          <tr>
                                              <td>Host</td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(soaRecord.host,'\\\.\$', '')}" />--%>
                                                  ${soaRecord.host}
                                              </td>

                                          </tr>
                                          <tr>
                                              <td>Admin</td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(soaRecord.admin,'\\\.\$', '')}" />--%>
                                                  ${soaRecord.admin}
                                              </td>

                                          </tr>
                                          <tr>
                                              <td><P>Serial</td>
                                              <td><c:out value="${soaRecord.serial}" /></td>
                                          </tr>
                                          <tr>
                                              <td>Refresh</td>
                                              <td><c:out value="${soaRecord.refresh}s" /></td>
                                          </tr>
                                          <tr>
                                              <td>Retry</td>
                                              <td><c:out value="${soaRecord.retry}s" /></td>

                                          </tr>
                                          <tr>
                                              <td>Expire</td>
                                              <td><c:out value="${soaRecord.expire}s" /></td>
                                          </tr>
                                          <tr>
                                              <td>Minimum</td>
                                              <td><c:out value="${soaRecord.minimum}s" /></td>
                                          </tr>
                                          <tr>
                                              <td>Name</td>
                                              <td>
                                                  <%--<c:out value="${f:replaceEndDot(soaRecord.name,'\\\.\$', '')}" />--%>
                                                  ${soaRecord.name}
                                              </td>

                                          </tr>
                                          <tr>
                                              <td>Type</td>
                                              <td><c:out value="${soaRecord.type}" /></td>
                                          </tr>
                                          <tr>
                                              <td>Class</td>
                                              <td><c:out value="${soaRecord.DClass}" /></td>
                                          </tr>
                                          <tr>
                                              <td title="Time to live"><p>TTL</td>
                                              <td><c:out value="${soaRecord.TTL}s" /></td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.soaRecords)==0}">
                                  <tr>
                                      <td class="text-center">No record found for SOA Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>
                  <!--<div class="col-xs-12">-->
                  <div class="col-xs-12 section-heading lxrm-padding-none">
                      <div class="col-xs-12 lxrm-padding-none">
                          <h4><span style="color: green;">
                                  <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                              </span> AAAA Records
                              <span class="pull-right" style="padding-right: 1%;">
                                  <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="left" 
                                     title="<span class='lxrm-bold'>IPv6 address record:</span> Returns a 128-bit IPv6 address, most commonly used to map hostnames to an IP address of the host.">
                                      <i class="fa fa-info-circle fa-1x" aria-hidden="true"></i>
                                  </a>
                              </span>
                          </h4>
                      </div>
                      <!--</div>-->
                      <div class="col-xs-12 lxrm-padding-none" style="overflow: auto;">
                          <table class="table">
                              <c:if test="${fn:length(dnsResult.aaaaRecords)>0}">
                                  <thead>
                                      <tr>
                                          <th>IP</th>
                                          <th>TTL</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach var="a4Record" items="${dnsResult.aaaaRecords}">
                                          <tr>
                                              <td>
                                                  <c:set var="a4RecordAddr" value="${fn:split(a4Record.address, '/')}" />
                                                  <c:out value="${a4RecordAddr[1]}" />
                                              </td>
                                              <td>
                                                  ${a4Record.TTL}s
                                              </td>
                                          </tr>
                                      </c:forEach>
                                  </tbody>
                              </c:if>
                              <c:if test="${fn:length(dnsResult.aaaaRecords)==0}">
                                  <tr>
                                      <td colspan="2" class="text-center">No record found for AAAA Records</td>
                                  </tr>
                              </c:if>
                          </table>
                      </div>

                  </div>
              </div>
              <div class="container-fluid">
                  <div class="col-xs-12 download-report-row lxrm-padding-none">
                      <!--<div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>-->
                      <div class="col-md-12 col-sm-12 col-xs-12 download-report-div">
                          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                              <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10" style="padding: 0;">
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                  <div class="input-group-btn">
                                      <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                  </div>
                              </div>
                              <div id="mail-sent-status" class="emptyData"></div>
                          </div>
                          <div class="col-md-1 col-sm-2 col-xs-2">
                              <div class="dropdown" onclick="downloadFormat();">
                                  <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                  <!--<div id="downloadAnalysisReport" class="dropdown-content" onclick="downloadFormat();"></div>-->
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
        </c:if>

        <!--   Success Page END-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>