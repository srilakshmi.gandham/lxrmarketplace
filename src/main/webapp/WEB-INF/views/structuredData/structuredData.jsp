<%-- 
    Document   : strecturedata
    Created on : Jul 26, 2017, 6:01:19 PM
    Author     : netsystem68
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Structured Data Generator | LXRMarketplace</title>
        <meta name="description" content="Try our Structured Data Generator tool now to generate Structured Data! Enter your info into this">
        <meta name="keywords" content="Structured Data Generator Tool, Structure Data Generator Tool">
        <%@ include file="/WEB-INF/views/commonImportsWithDatePicker.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/structuredData.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/js/tools/structuredData.js" type="text/javascript"></script>
        <script src="/resources/css/tools/tooltabs.js"></script>
        <%
            int toolDBId = 37;
            Map<String, String> reqTypeParamsMap = null;
            if (session.getAttribute("reqTypeParamsMap") != null) {
                reqTypeParamsMap = (Map<String, String>) session.getAttribute("reqTypeParamsMap");
                String formatType = reqTypeParamsMap.get("formatType");
                String markupType = reqTypeParamsMap.get("markupType");
                pageContext.setAttribute("reqTypeParamsMap", reqTypeParamsMap);
                pageContext.setAttribute("formatType", formatType);
                pageContext.setAttribute("markupType", markupType);
            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var atePopupStatus = true;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
//                toolLimitExceeded();
                //Tool tip
                $('[data-toggle="popover"]').popover();
            <c:if test="${(fn:contains(uri, 'backToSuccessPage=')) && reqTypeParamsMap != '' && reqTypeParamsMap ne null}">
                addInputsForPreviousResults();
            </c:if>
                $('input[type=radio][name=formatType]').change(function () {
                    if (this.value === "json-ld") {
                        $("#microdata").addClass("in-active");
                        $("#jsonld").removeClass("in-active");
                        $(".json-ld-step").show();
                        $(".microdata-step").hide();
                    } else if (this.value === "microdata") {
                        $("#jsonld").addClass("in-active");
                        $("#microdata").removeClass("in-active");
                        $(".json-ld-step").hide();
                        $(".microdata-step").show();
                    }
                    $("#getResult").click();
                });
                $("#clearAll").on('click', function () {
                    parent.window.location = "/structured-data-generator.html";
                });
                $('input[type=radio][name=markupType]').change(function () {
                    atePopupStatus = true;
                    $("div").removeClass("radioBackgroung");
                    $(this).parent().parent().addClass("radioBackgroung");
                    var markupType = this.value;
                    $(".sdgt-tool").html("");
                    $(".sdgt-tool").hide();
                    var inputs = generateInputTagsForMarkups(markupType);
                    $("#markuptype-inputs").html(inputs);
                    $("#result-div").hide();
                    $("#toolButtonsWrapper").show();
                    $(function () {
                        $('.lxr-date-picker').datetimepicker({
                            format: 'LT'
                        });
                    });
                    $(function () {
                        $('.lxr-date-picker-24hours').datetimepicker({
                            format: 'HH:mm'
                        });
                    });
                    $(function () {
                        $('#datePublished, #dateModified, #uploadDate, #expires, #startDate, #endDate, #datePosted, #validThrough, #priceValidUntil').datetimepicker({
                            format: 'YYYY-MM-DD'
//                            minDate: new Date()
                        });
                        $('#datePublished, #dateModified, #uploadDate, #expires, #startDate, #endDate, #datePosted, #validThrough, #priceValidUntil').val("");
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                });
            });
            //  To show the previous results of the user, when user clicks on calcel button from ask the expert and payment page
            function addInputsForPreviousResults() {
                $("#formatType").val("${formatType}");
                $("#markupType").val("${markupType}");
                var markupType = "${markupType}";
                var inputs = generateInputTagsForMarkups(markupType);
                $("#markuptype-inputs").html(inputs);
                if (inputs !== "") {
                    $("#toolButtonsWrapper").show();
                }
                $("#result-div").hide();
                var $radios = $('input:radio[name=markupType]');
                if ($radios.is(':checked') === false) {
                    $radios.filter('[value=' + markupType + ']').prop('checked', true);
                }
                $(function () {
                    $('.lxr-date-picker').datetimepicker({
                        format: 'LT'
                    });
                });
                $(function () {
                    $('.lxr-date-picker-24hours').datetimepicker({
                        format: 'HH:mm'
                    });
                });
                $(function () {
                    $('#datePublished, #dateModified, #uploadDate, #expires, #startDate, #endDate, #datePosted, #validThrough, #priceValidUntil').datetimepicker({
                        format: 'YYYY-MM-DD'
//                        minDate: new Date()
                    });
//                    $('#datePublished, #dateModified, #uploadDate, #expires, #startDate, #endDate, #datePosted, #validThrough, #priceValidUntil').val("");
                });
                $('[data-toggle="tooltip"]').tooltip();
            <c:forEach var="reqTypeParams" items="${reqTypeParamsMap}">
                /*This choose function is for, if input have new line characters 
                 * then we are trimming that input and inserting in the respestive field*/
                <c:choose>
                    <c:when test="${reqTypeParams.key.equals('@id')}">
                        $("#mainEntityOfPage").val("${reqTypeParamsMap['@id']}");
                    </c:when>
                    <c:when test="${reqTypeParams.key.equals('description') || reqTypeParams.key.equals('sameAs') 
                                    || reqTypeParams.key.equals('recipeIngredient') 
                                    || reqTypeParams.key.equals('recipeInstructions')}">
                        <%String inputName = "";%>
                        <c:choose>
                            <c:when test="${reqTypeParams.key.equals('sameAs')}">
                                <%inputName = "sameAs";%>
                            </c:when>
                            <c:when test="${reqTypeParams.key.equals('recipeIngredient')}">
                                <%inputName = "recipeIngredient";%>
                            </c:when>
                            <c:when test="${reqTypeParams.key.equals('recipeInstructions')}">
                                <%inputName = "recipeInstructions";%>
                            </c:when>
                            <c:otherwise>
                                <%inputName = "description";%>
                            </c:otherwise>
                        </c:choose>
                        <%
                            String inputText = reqTypeParamsMap.get(inputName);
                            inputText = inputText.replaceAll("\n", "&#13;&#10;").replaceAll("\r", "");
                        %>
                var inputText = "<%=inputText%>";
                $("#${reqTypeParams.key}").html("" + inputText + "");
                    </c:when>
                    <c:otherwise>
                $("#${reqTypeParams.key}").val("${reqTypeParams.value}");
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${markupType.equals('event')}">
                $("#lname").val("${reqTypeParamsMap['location.name']}");
                </c:when>
                <c:when test="${markupType.equals('organization')}">
                $("#logoUrl").val("${reqTypeParamsMap['logo']}");
                </c:when>
                <c:when test="${markupType.equals('article')}">
                $("#imageUrl").val("${reqTypeParamsMap['image.url']}");
                $("#imageWidth").val("${reqTypeParamsMap['image.width']}");
                $("#imageHeight").val("${reqTypeParamsMap['image.height']}");
                $("#pName").val("${reqTypeParamsMap['publisher.name']}");
                $("#logourl").val("${reqTypeParamsMap['logo.url']}");
                $("#logoWidth").val("${reqTypeParamsMap['logo.width']}");
                $("#logoHeight").val("${reqTypeParamsMap['logo.height']}");
                $("#aName").val("${reqTypeParamsMap['author.name']}");
                </c:when>
                <c:when test="${markupType.equals('recipe')}">
                $("#aName").val("${reqTypeParamsMap['author.name']}");
                $("#calories").val("${reqTypeParamsMap['nutrition.calories']}");
                $("#fatContent").val("${reqTypeParamsMap['nutrition.fatContent']}");
                $("#ratingValue").val("${reqTypeParamsMap['aggregateRating.ratingValue']}");
                $("#reviewCount").val("${reqTypeParamsMap['aggregateRating.reviewCount']}");
                </c:when>
                <c:when test="${markupType.equals('product')}">
                $("#brand").val("${reqTypeParamsMap['brand.name']}");
                $("#sname").val("${reqTypeParamsMap['seller.name']}");
                </c:when>
                <c:when test="${markupType.equals('localBusiness')}">
                $("#logourl").val("${reqTypeParamsMap['logo.url']}");
                    <c:set var="weekdaysString" value='${["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]}' scope="application" />
                    <c:forEach var="weekday" items="${weekdaysString}">
                        <c:set var="keyName" value="openingHours${weekday}"/>
                        <c:if test="${reqTypeParamsMap.containsKey(keyName)}">
                $("#openingHours" + "${weekday}").prop('checked', true);
                        </c:if>
                    </c:forEach>
                $("#monFrom").val("${reqTypeParamsMap['opensMo']}");
                $("#monTo").val("${reqTypeParamsMap['closeMo']}");
                $("#tueFrom").val("${reqTypeParamsMap['opensTu']}");
                $("#tueTo").val("${reqTypeParamsMap['closeTu']}");
                $("#wedFrom").val("${reqTypeParamsMap['opensWe']}");
                $("#wedTo").val("${reqTypeParamsMap['closeWe']}");
                $("#thuFrom").val("${reqTypeParamsMap['opensTh']}");
                $("#thuTo").val("${reqTypeParamsMap['closeTh']}");
                $("#friFrom").val("${reqTypeParamsMap['opensFr']}");
                $("#friTo").val("${reqTypeParamsMap['closeFr']}");
                $("#satFrom").val("${reqTypeParamsMap['opensSa']}");
                $("#satTo").val("${reqTypeParamsMap['closeSa']}");
                $("#sunFrom").val("${reqTypeParamsMap['opensSu']}");
                $("#sunTo").val("${reqTypeParamsMap['closeSu']}");
                </c:when>
                <c:when test="${markupType.equals('breadcrumbs')}">
                var breadcrumbLevel = "${reqTypeParamsMap['level']}";
                if (breadcrumbLevel === "3") {
                    var inputs = addInputsforBreadcrumbs();
                    $("#markuptype-inputs").html(inputs);
                    $("#item2").val("${reqTypeParamsMap['item2']}");
                    $("#name3").val("${reqTypeParamsMap['name3']}");
                }
                $("#level").val("${reqTypeParamsMap['level']}");
                $("#name0").val("${reqTypeParamsMap['name0']}");
                $("#item0").val("${reqTypeParamsMap['item0']}");
                $("#name1").val("${reqTypeParamsMap['name1']}");
                $("#item1").val("${reqTypeParamsMap['item1']}");
                $("#name2").val("${reqTypeParamsMap['name2']}");
                </c:when>
                <c:when test="${markupType.equals('jobPosting')}">
                $("#iValue").val("${reqTypeParamsMap['identifier.value']}");
                $("#hName").val("${reqTypeParamsMap['hiringOrganization.name']}");
                $("#hValue").val("${reqTypeParamsMap['hiringOrganization.sameAs']}");
                    <c:choose>
                        <c:when test="${!reqTypeParamsMap['baseSalary.value'].equals('') && reqTypeParamsMap['baseSalary.value'] != null}">
                $("#bsValue").val("${reqTypeParamsMap['baseSalary.value']}");
                        </c:when>
                        <c:otherwise>
                $("#bsMinValue").val("${reqTypeParamsMap['baseSalary.minValue']}");
                $("#bsMaxValue").val("${reqTypeParamsMap['baseSalary.maxValue']}");
                        </c:otherwise>
                    </c:choose>
                </c:when>
            </c:choose>
                $("#getResult").click();
//                generateStructureDataScript();
            }
            function submitPreviousRequest(){
                $("#getResult").click();
            }
        </script>
    </head>

    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <form id="structured-data-form">
                <input type="hidden" name="formatType" id="formatType"/>
                <div class="col-xs-12 mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            Structured data is on-page markup that enables search engines to decipher the information currently on your business’s webpage better while improving your business’s search results listing.
                        </p>
                    </blockquote>
                </div>
                <div class="col-xs-12 input-section mobile-padding-none">
                    <h4 class="lxrm-bold">SELECT THE TYPE OF MARKUP YOU WANT TO CREATE</h4>
                    <div class="col-xs-12 mobile-padding-none">
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="article">Article</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="event">Event</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="jobPosting">Job Posting</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="localBusiness">Local Business</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="organization">Organization</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="product">Product</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="recipe">Recipe</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="video">Video</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="website">Website</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="aggregateRating">Aggregate Rating</label></div>
                        <div class="radio col-lg-3 col-md-3 col-sm-4 col-xs-6"><label><input type="radio" name="markupType" value="breadcrumbs">Breadcrumbs</label></div>
                    </div>
                    <div class="clearfixdiv"></div>
                    <div id="markuptype-inputs"></div>
                    <div class="row" id="toolButtonsWrapper" align="center" style="display: none;">
                        <div class="col-xs-12">
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12"  id="clearAll">
                                        <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles lxr-search-btn col-xs-12 col-sm-12 col-md-12 col-lg-12" id="getResult" onclick="generateStructureDataScript(this);" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING">
                                        &nbsp;GENERATE&nbsp;
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        </div>
                    </div>
                </div>
            </form>
            <!--   Success Page -->

            <div class="clearfixdiv"></div>
            <!--code snippet start-->
            <div class="container-fluid" id="result-div" style="display: none;">
                <div class="col-md-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd !important;" id="firstcontent-1">
                    <div class="col-xs-12 lxr-panel-heading1" style="padding: 0;">
                        <div class="col-lg-9">
                            <span class="panel-title lxrm-bold">GENERATED STRUCTURED DATA</span>
                            <p id="issue-text-1">
                                Schema <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">HELPS</strong> 
                                to increase CTR of a webpage, helping SEO indirectly. 
                                Currently, Schema is not a direct ranking signal, but may be in the future.
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <div class="col-lg-12" style="padding: 0;display: flex;">
                                <div style="height: 20px;clear: both;" class="visible-lg visible-md"></div>
                                <div class="btn-group btn-group-toggle button-toggle-div" data-toggle="buttons" style="margin: 0 auto;">
                                    <span id="jsonld" class="btn active lxr-switch" role="button">JSON-LD
                                        <input type="radio" name="formatType" value="json-ld" id='yes'>
                                    </span>
                                    <span id="microdata" class="btn lxr-switch in-active" role="button">Microdata
                                        <input type="radio" name="formatType" value="microdata" id='no'>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="padding: 1%;">
                        <textarea readonly class="col-xs-12 lxrsrt-codesnippet1" id="jsondata" style="resize:none;"></textarea>
                    </div>
                    <div class="col-xs-12 lxr-panel-footer1 text-left" style="padding: 1%;border-top: 1px solid #ddd;">
                        <div class="text-center" style="margin: 1% 0;">
                            <div class="copy-text text-center col-xs-12 lxr-success"></div>
                            <button type="button" onclick="selectElementContents('jsondata')" class="lxrsrt-copybutton" >
                                <i class="fa fa-files-o" aria-hidden="true"></i> COPY THIS CODE</button>
                        </div>
                    </div>
                </div>
                <!--how to fix-->
                <div id="howtofix-1" style="display: none;">
                    <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd;">
                        <div class="panel panel-default">
                            <div class="panel-heading lxr-panel-heading">
                                <span class="panel-title lxr-light-danger">Below are the steps to fix on page issues on the website</span>
                                <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                            </div>
                            <div class="panel-body lxr-panel-body">
                                <p class="json-ld-step">
                                    <!--                                    <span class="lxrm-bold">JSON-LD: </span>-->
                                    JavaScript Object Notation for Linked Data (JSON-LD) is 
                                    a WCW3 standard website method of structured markup implementation using the JSON notation. 
                                    Major search engines such as Google, Bing, and Yandex can understand 
                                    JSON-LD for pages on your site.
                                </p>
                                <p class="microdata-step" style="display: none;">
                                    <!--<span class="lxrm-bold">Microdata: </span>-->
                                    Microdata is "hidden HTML code" that can be inserted in web pages. 
                                    Microdata is also commonly referred to as structured data or Schema Markup Code.
                                </p>
                                <div class="lxr-panel-footer">
                                    <h3><spring:message code="ate.expert.label.2" /></h3>
                                    <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12" style="padding: 10px;"><span class="lxrm-bold">Note: </span>Test implementation with <a onclick="showGoogleTestingTool();" style="cursor: pointer;">Google’s Structured Data Testing Tool.</a></div>
                <div class="clearfixdiv"></div>
                <div class="sdgt-tool" style="display: none;"></div>
                <div class="clearfixdiv"></div>
            </div>

        </div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>