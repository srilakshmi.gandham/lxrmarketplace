
<script type="text/javascript">
    var maxCount = 2;
    var countfile = 0;
    var limitMessage = 0;
    $(document).ready(function (event) {
        $('#name').val("");
        $('#email').val("");
        $('#phoneNo').val("");
        $('#subject').val("");
        $('#mailBody').val("");
        var divId = document.getElementById('browse1');
    <%boolean messageSent = false;
        HttpSession session1 = request.getSession();
        Object msg = session1.getAttribute("messageSent");
        if (msg != null) {
            messageSent = (Boolean) msg;
        }%>
        if (<%=messageSent%>) {
            $('#afterFeedBackmailsent').show();
        }
        $("#browsedisable").click(function () {
            alert("You cannot attach more than 1 file in a mail, you can add ZIP file as attachment.");
            return false;
        });
//        $('#feedbackSubmit').on('click', function () {
//            submitclick(this);
//        });

        $('#feedbackClear').on('click', function () {
            cleardata();
        });


        $('#name').click(function () {
            $('#afterFeedBackmailsent').hide();
            $('#nameErrorMsg').html("");
        });
        $('#email').click(function () {
            $('#afterFeedBackmailsent').hide();
            $('#emailErrorMsg').html("");
        });
        $('#phoneNo').click(function () {
            $('#afterFeedBackmailsent').hide();
            $('#phoneErrorMsg').html("");
        });
        $('#subject').click(function () {
            $('#afterFeedBackmailsent').hide();
            $('#subjectErrorMsg').html("");
        });
        $('#mailBody').click(function () {
            $('#afterFeedBackmailsent').hide();
            $('#mailBodyErrorMsg').html("");
        });

    });

    function cleardata() {
        $("#name").val("");
        $("#email").val("");
        $("#phoneNo").val("");
        $("#file").val("");
        $("#mailBody").val("");
        $("#subject").val("");
        var f = $("#feedback");
        f.attr('action', "/suppcustservice.html?feedbackClear='feedbackClear'");
        f.submit();
    }
    function submitclick(param) {
        $('#afterFeedBackmailsent').hide();
        var status = true;
        if ($("#name").val() === "" || $("#name").val() === null) {
            $("#nameErrorMsg").html("Name cannot be left blank.");
            status = false;
        } else {
            $("#nameErrorMsg").html("");
        }
        var email = $("#email").val();
        if (email === "" || email === null) {
            $("#emailErrorMsg").html("Email cannot be left blank.");
            status = false;
        } else if (!isValidMail(email)) {
            $("#emailErrorMsg").html("Please enter a valid email address.");
            status = false;
        } else {
            $("#emailErrorMsg").html("");
        }
        
        if ($('#phoneNo').val() !== "" && !ValidateForm()) {
            $("#phoneErrorMsg").html("Please Enter a Valid Phone Number.");
            status = false;
        } else {
            $("#phoneErrorMsg").html("");
        }
        if ($('#subject').val() === "" && $('#mailBody').val() === "") {
            $("#subjectErrorMsg").html("Please enter either subject or mail body.");
            status = false;
        } else {
            $("#subjectErrorMsg").html("");
        }
        if (status) {
            var $this = $(param);
            $this.button('loading');
            $('#feedbackSubmit').prop("disabled", true);
            $.ajax({
                method: "POST",
                url: "suppcustservice.html?feedbackSubmit='feedbackSubmit'&name="
                        + $('#name').val() + "&email="
                        + $('#email').val() + "&phoneNo=" + $('#phoneNo').val()
                        + "&subject=" + $('#subject').val() + "&mailBody=" + $('#mailBody').val()
                        + "&attachments=" + $('#attachments').val(),
                processData: true,
                data: {},
                dataType: "json",
                success: function (data) {
                }, complete: function (jqXHR, textStatus) {
                    $('#afterFeedBackmailsent').show();
                    $('#name').val("");
                    $('#email').val("");
                    $('#phoneNo').val("");
                    $('#subject').val("");
                    $('#mailBody').val("");
                    $this.button('reset');
                }
            });
        } else {
            return false;
        }
    }
</script>
<springForm:form commandName="feedback" method="POST">
    <div style="height: 10px;clear: both;"></div>
    <p>
        At LXRMarketplace, we are passionate about adding value to our
        user's life, fanatical about our products  and dedicated to provide
        exemplary Customer Service! Whether you have queries about our
        applications or any new ideas, we want to talk with YOU.
    </p>
    <div class="row">
        <div class="col-lg-8 form-group padding-none">
            <div class="col-lg-2">
                <p class="lxrm-bold">Name<span class="lxr-danger">*</span></p>
            </div>
            <div class="col-lg-8">
                <springForm:input path="name" maxlength="50" cssClass="form-control" />
                <span id="nameErrorMsg" class="lxr-danger contact-error-text"></span>
            </div>
        </div>
        <div class="">
            <div class="col-lg-8 form-group padding-none">
                <div class="col-lg-2">
                    <p class="lxrm-bold">Email Id<span class="lxr-danger">*</span></p>
                </div>
                <div class="col-lg-8">
                    <springForm:input path="email" maxlength="50" cssClass="form-control"/>
                    <span id="emailErrorMsg" class="lxr-danger contact-error-text"></span>
                </div>
            </div>
        </div>
        <div class="col-lg-8 form-group padding-none">
            <div class="col-lg-2">
                <p class="lxrm-bold">Phone</p>
            </div>
            <div class="col-lg-8">
                <springForm:input path="phoneNo" maxlength="50" cssClass="form-control"/>
                <span id="phoneErrorMsg" class="lxr-danger contact-error-text"></span> 
            </div>
        </div>
        <div class="col-lg-8 form-group padding-none">
            <div class="col-lg-2">
                <p class="lxrm-bold">Subject<span class="lxr-danger">*</span></p>
            </div>
            <div class="col-lg-8">
                <springForm:input path="subject" maxlength="250" size="61" cssClass="form-control"/> 
                <span id="subjectErrorMsg" class="lxr-danger contact-error-text"></span> 
            </div>
        </div>
        <div class="col-lg-8 form-group padding-none">
            <div class="col-lg-2">
                <p class="lxrm-bold">Mail Body</p>
            </div>
            <div class="col-lg-8">
                <springForm:textarea cols="70" rows="15" path="mailBody" cssClass="form-control"></springForm:textarea>
                <span id="mailBodyErrorMsg" class="lxr-danger contact-error-text"></span> 
                <div id="afterFeedBackmailsent" class="text-center">Mail Sent Successfully</div>
                <div class="col-xs-12 pull-right" style="padding: 0;padding-top: 1.2em;margin-bottom: 2%;">
                    <button  type="button" class="col-xs-12 col-lg-4 col-lg-offset-3 btn " id="feedbackClear">
                        CANCEL
                    </button>
                    <div class="visible-xs visible-sm" style="height: 10px;clear: both;"></div>
                    <button type="button" class="col-xs-12 col-lg-4 pull-right btn lxr-subscribe" id="feedbackSubmit" onclick="submitclick(this);" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> SENDING">
                        SUBMIT
                    </button>
                </div>
            </div>
        </div>
    </div>
</springForm:form>
