
<%@page import="lxr.marketplace.keywordanalyzer.KeywordAnalyzerTool"%>
<%@page import="org.springframework.jdbc.support.rowset.SqlRowSetMetaData"%>
<%@page import="org.springframework.jdbc.support.rowset.SqlRowSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PPC Keyword Performance Analyzer Tool | LXR Marketplace</title>
        <meta name="description"
              content="Try the PPC Keyword Performance Analyzer at LXRMarketplace. Get reports of your top & lowest performing keywords based on your metric selection. Try it now!">
        <meta name="keywords"
              content="Keyword Performance Analyzer Tool, PPC Keyword Analyzer, ppc analyzer">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/keyAnalyzer.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <%
            int toolDBId = 4;

            String jsonValue = session.getAttribute("json").toString();
            jsonValue = jsonValue.substring(1, jsonValue.length() - 1);
            jsonValue = jsonValue.replace("\"", "");
            jsonValue = jsonValue.replace(",", ", ");
            pageContext.setAttribute("jsonValue", jsonValue);

            String reqParam = request.getParameter("report");
            String downloadReportType = request.getParameter("report-type");
            pageContext.setAttribute("reqParam", reqParam);
            pageContext.setAttribute("downloadReportType", downloadReportType);
            KeywordAnalyzerTool keywordAnalyzerResult = null;
            if (session.getAttribute("keywordAnalyzerTool") != null) {
                keywordAnalyzerResult = (KeywordAnalyzerTool) session.getAttribute("keywordAnalyzerTool");
                pageContext.setAttribute("keywordAnalyzerResult", keywordAnalyzerResult);
            }
        %>
        <script type="text/javascript">
            var callAjax = false;
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${getSuccess}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />

            //Tool tip
            $('[data-toggle="tooltip"]').tooltip();
            $(document).ready(function () {
//                toolLimitExceeded();
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat("${downloadReportType}");
                }

            <c:set var="getSuccess" value="${getSuccess}" />
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) && (keywordAnalyzerResult != null)}">
                showchart();
//                $("#uploadFile").val("${keywordAnalyzerTool.uploadFile}");
            </c:if>

                $("#getResult").bind('click', function (e) {
                    resultLoginValue = true;
                    toolLimitExceeded();
                    var fileNameExist = $("#fileName").html();
                    var fileuploaded = $("#uploadFile").val();
                    var keymetric = $("#keyMetric").val();
                    var status = true;
                    if (fileNameExist === '') {
                        if (fileuploaded === null || fileuploaded === "") {
                            document.getElementById('fileUploadError').textContent = "Please upload a file";
                            document.getElementById('fileUploadError').style.display = 'block';
                            status = false;
                        } else if (!(fileuploaded.endsWith(".csv") || fileuploaded.endsWith(".tsv"))) {
                            document.getElementById('fileUploadError').textContent = "Please upload a csv or tsv file";
                            document.getElementById('fileUploadError').style.display = 'block';
                            status = false;
                        } else {
                            document.getElementById('fileUploadError').style.display = 'none';
                        }
                    }
//                    This condition is for second time file is not getting bind with spring forms, 
//                    But we are getting that file in controller so for that we need this validation for that related scenario
                    else if (!(fileNameExist.endsWith(".csv") || fileNameExist.endsWith(".tsv"))) {
                        document.getElementById('fileUploadError').textContent = "Please upload a csv or tsv file";
                        document.getElementById('fileUploadError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('fileUploadError').style.display = 'none';
                    }

                    if (keymetric === null || keymetric === "" || parseInt(keymetric) === 0) {
                        document.getElementById('keyMetricError').textContent = "Please select key metric";
                        document.getElementById('keyMetricError').style.display = 'block';
                        status = false;
                    } else {
                        document.getElementById('keyMetricError').style.display = 'none';
                    }

                    if (status) {
                        $("#fileUploadError").removeClass("lxr-success");
                        $("#fileUploadError").html("");
                        var $this = $(this);
                        $this.button('loading');

                        var f = $("#keywordAnalyzerTool");
                        f.attr('method', "POST");
                        f.attr('action', "ppc-keyword-performance-analyzer-tool.html?getResult='getResult'");
                        f.submit();
                    } else {
                        return false;
                    }
                });
                $("#uploadFile").change(function (e) {
                    $("#fileUploadError").removeClass("lxr-success");
                    $("#fileUploadError").html("");
                    var fileName = $("#uploadFile").val();
                    fileName = fileName.replace("C:\\fakepath\\", "");
                    if (fileName.length > 50) {
                        fileName = fileName.substr(0, 5) + "..." + fileName.substring(fileName.length - 6, fileName.length);
                    }
                    $("#fileName").html(fileName);
                    if (fileName !== "") {
                        if (!(fileName.endsWith(".csv") || fileName.endsWith(".tsv"))) {
                            document.getElementById('fileUploadError').textContent = "Please upload a csv or tsv file";
                            document.getElementById('fileUploadError').style.display = 'block';
                            status = false;
                        } else {
                            $("#fileUploadError").html("The file has been uploaded successfully");
                            $("#fileUploadError").addClass("lxr-success");
                            //                        document.getElementById('fileUploadError').style.display = 'none';
                        }
                    }
                });
                getValidation();
            });
            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    if (reportType === undefined) {
                        reportType = 'pdf';
                    }
                    var f = $("#keywordAnalyzerTool");
                    f.attr('method', "POST");
                    f.attr('action', "/ppc-keyword-performance-analyzer-tool.html?download=download&report-type=" + reportType);
                    f.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function sendReportToMail() {
                $('#mail-sent-status').hide();
                var emailid = $.trim($("#email").val());
                var status = true;
                var statusMsg = "";
                if (emailid === "") {
                    statusMsg = "Please provide your email";
                    status = false;
                } else if (!isValidMail(emailid)) {
                    statusMsg = "Please enter a valid email address.";
                    status = false;
                }
                if (!status) {
                    $("#email").focus();
                    $('#mail-sent-status').show();
                    $("#mail-sent-status").html(statusMsg);
                    $('#mail-sent-status').css("color", "red");
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/ppc-keyword-performance-analyzer-tool.html?download='sendmail'&report-type='sendmail'&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }
            function showchart() {
                if (callAjax)
                {
                    if (gogoleChartData === null) {
                        getGoogleCharts();
                    } else {
                        if ((gogoleChartData[0].length === 0) && (gogoleChartData[1].length === 0) && (gogoleChartData[2].length === 0)) {
                            $("#UnavailableData").empty();
                            $("#UnavailableData").css("margin-left", 0 + '%');
                            $("#UnavailableData").css("width", 689 + 'px');
                            $('<p style="text-align:center">No data available</p>').appendTo('#UnavailableData');
                        } else {
                            drawKeyWordChart();
                            drawConversionRevenueChart();
                            drawCostChart();
                            unavailableData();
                        }
                    }
                } else {
                    getGoogleCharts();
                }
            }
            function getGoogleCharts() {
                $.get("/ppc-keyword-performance-analyzer-tool.html?getGoogleData='getGoogleData'", function (data) {
                    gogoleChartData = data;
                    callAjax = true;
                    if ((gogoleChartData === null) || (gogoleChartData[0].length === 0) && (gogoleChartData[1].length === 0) && (gogoleChartData[2].length === 0)) {
                        $("#UnavailableData").empty();
                        $("#UnavailableData").css("margin-left", 0 + '%');
                        $("#UnavailableData").css("width", 689 + 'px');
                        $('<p style="text-align:center">No data available</p>').appendTo('#UnavailableData');
                    } else {
                        drawKeyWordChart();
                        drawConversionRevenueChart();
                        drawCostChart();
                        unavailableData();
                    }
                }, "json");
            }

            var graphWidth = 650;
            var grapHeight = 300;

            function drawKeyWordChart() {
                $("#keyWordsChart_div").empty();
                if (gogoleChartData[0].length !== 0) {
                    var keyWordsData = new google.visualization.DataTable();
                    keyWordsData.addColumn('string', 'Metrics');
                    keyWordsData.addColumn('number', 'Score');
                    keyWordsData.addRows(gogoleChartData[0]);

                    var keyWordOptions = {
                        title: 'Keywords',
                        is3D: true,
                        legend: 'right',
                        pieSliceText: 'value',
                        sliceVisibilityThreshold: 0,
                        tooltip: {showColorCode: true},
                        'width': graphWidth,
                        'height': grapHeight
                    };
                    var keywordChart = new google.visualization.PieChart(document.getElementById('keyWordsChart_div'));
                    keywordChart.draw(keyWordsData, keyWordOptions);
                }

            }
            function drawConversionRevenueChart() {
                $("#RevenueChart_div").empty();
                if (gogoleChartData[1].length !== 0) {
                    var chartTitle = "";
                    chartTitle = $("select option:selected").text();
                    var ConversionRevenueData = new google.visualization.DataTable();
                    ConversionRevenueData.addColumn('string', 'Metrics');
                    ConversionRevenueData.addColumn('number', 'Score');

                    ConversionRevenueData.addRows(gogoleChartData[1]);

                    var ConversionRevenueOptions = {
                        title: chartTitle,
                        is3D: true,
                        pieSliceText: 'value',
                        sliceVisibilityThreshold: 0,
                        legend: {position: 'right', alignment: 'start', maxLines: 1},
                        tooltip: {showColorCode: true},
                        'width': graphWidth,
                        'height': grapHeight
                    };

                    var conversionsRevenuechart = new google.visualization.PieChart(document.getElementById('RevenueChart_div'));
                    conversionsRevenuechart.draw(ConversionRevenueData, ConversionRevenueOptions);
                }

            }
            function drawCostChart() {
                $("#CostChart_div").empty();
                if (gogoleChartData[2].length !== 0) {
                    var CostsData = new google.visualization.DataTable();
                    CostsData.addColumn('string', 'Metrics');
                    CostsData.addColumn('number', 'Score');

                    CostsData.addRows(gogoleChartData[2]);

                    var CostsOptions = {
                        title: 'Cost',
                        is3D: true,
                        pieSliceText: 'value',
                        sliceVisibilityThreshold: 0,
                        legend: {position: 'right', alignment: 'start', maxLines: 1},
                        tooltip: {showColorCode: true},
                        'width': graphWidth,
                        'height': grapHeight
//                        chartArea: {'width': "200px", 'height': "150px"}
                    };
                    var costChart = new google.visualization.PieChart(document.getElementById('CostChart_div'));
                    costChart.draw(CostsData, CostsOptions);
                }
            }
            function  unavailableData() {
                if ((gogoleChartData[0].length === 0) && (gogoleChartData[1].length === 0)) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct keywords and Conversion/Revenue graph data</p>').appendTo('#UnavailableData');
                } else if ((gogoleChartData[1].length === 0) && (gogoleChartData[2].length === 0)) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct Conversion/Revenue and Cost graph data </p>').appendTo('#UnavailableData');
                } else if ((gogoleChartData[0].length === 0) && (gogoleChartData[2].length === 0)) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct keywords and Cost graph data</p>').appendTo('#UnavailableData');
                } else if (gogoleChartData[0].length === 0) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct keywords graph data</p>').appendTo('#UnavailableData');
                } else if (gogoleChartData[1].length === 0) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct Conversion/Revenue graph data </p>').appendTo('#UnavailableData');
                } else if (gogoleChartData[2].length === 0) {
                    $("#UnavailableData").empty();
                    $('<p class="NoData">No data available to construct Cost graph data </p>').appendTo('#UnavailableData');
                }
            }
            function getValidation() {
                var jsonValue = "${jsonValue}";
                if (jsonValue === null || jsonValue === '"empty\"') {
                    alert("The uploaded file is empty.");
                    $("#fileName").empty();
                    clearForm();
                    return false;
                } else if (jsonValue === '"nodata\"' || jsonValue === 'nodata') {
                    alert("The uploaded file has no data");
                    $("#fileName").empty();
                    clearForm();
                    return false;
                } else if (jsonValue === '"nocolumns\"' || jsonValue === 'nocolumns') {
                    alert("Mandatory columns are missing: \n\nKeyword \nImpressions \nClicks  \nCost / Spend \nConversions / Revenue.");
                    $("#fileName").empty();
                    clearForm();
                    return false;
                } else if (jsonValue === '"wrongkeymetric\"' || jsonValue === 'wrongkeymetric') {
                    alert("Please select Key Metric");
//                    return false;
                } else if (jsonValue !== '' && jsonValue !== 'allcolumns') {
                    alert("Mandatory column missing: " + jsonValue);
                    $("#fileName").empty();
                    clearForm();
                    return false;
//                    alert("Mandatory columns are missing: \n\nKeyword \nImpressions \nClicks  \nCost / Spend \nConversions / Revenue.");
                }
            }
            function clearForm() {
                $("#keyMetric").val(0);
                $("#fileName").empty();
                $("#uploadFile").val('');
                $("#fileUploadError").removeClass("lxr-success");
                $("#fileUploadError").html("");
                var f = $("#keywordAnalyzerTool");
                f.attr('method', "POST");
                f.attr('action', "/ppc-keyword-performance-analyzer-tool.html?clearAll='clearAll'");
                f.submit();
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/ppc-keyword-performance-analyzer-tool.html?ate='keyWordAnalyzerATE'",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            $("#bottom_help_domain").val(data[0].domain);
                        }
                    }
                });
            }

            function submitPreviousRequest() {
                $("#getResult").click();
            }

        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <blockquote class="blockquote">
                    <p>
                        The Keyword Performance Analyzer is a tool designed to relay information about which keywords are bringing in the most success for your campaigns.
                    </p>
                </blockquote>
            </div>
            <div class="clearfixdiv"></div>
            <springForm:form commandName="keywordAnalyzerTool" method="POST" enctype="multipart/form-data">
                <div class="col-xs-12">
                    <div class="row col-xs-12 col-sm-8 col-md-6 normal-styles">
                        <b>Step 1:</b> Select the AdWords /AdCenter Keyword Report to Upload
                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                           title="Based on the search engine, the keyword performance of which you want to analyze, choose respective search engine’s keyword report from your desktop/laptop. For more information on the required metrics for the keywords report to be uploaded, you can download a sample template below.">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </a>

                        <div>
                            <a id="template" href="ppc-keyword-performance-analyzer-tool.html?sampleTemp='sampleTemp'">Click here</a>
                            to download Sample Template
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-6 normal-styles">
                        <div >
                            <label for="uploadFile" class="btn btn-primary lxr-subscribe">Browse File</label>
                            <input type="file" id="uploadFile" name="uploadFile" />
                        </div>
                        <span id="fileName">${keywordAnalyzerTool.upFileName}</span>
                        <div>(Only in csv or tsv format)</div>
                        <div><span id="fileUploadError" class="error"></span></div>
                    </div>
                </div>
                <div class="col-xs-12 normal-styles">
                    <div class="row col-xs-12 ">
                        <b>Step 2: </b> Select the Key Metric
                        <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="bottom" 
                           title="Based on your primary goal of analysis you can choose the required metric of either, number of Conversions/Orders or Revenue, here. Based on the chosen metric, the classification into the buckets of 80% and 20% contributing keywords will be done.">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </a>
                        <springForm:select path="keyMetric"  >
                            <springForm:option value="0" label="-- Select --" />
                            <springForm:option value="1" label="Conversions" />
                            <springForm:option value="2" label="Revenue" />
                        </springForm:select>
                        <div>(Based on the selected Key Metric, the 80/20 analysis will be done)</div>
                        <span id="keyMetricError" class="error"></span>
                    </div>
                </div>


                <div class="col-xs-12 text-center">
                    <button type="submit" id="getResult" style="width: 180px;" class="btn top-line btn-lg buttonStyles lxr-search-btn " data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">ANALYZE</button>
                </div>

            </springForm:form>
        </div>
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) && (keywordAnalyzerResult != null)}">
            <%  int keymetric = 0;
                if (session.getAttribute("keyMetric") != null) {
                    keymetric = Integer.parseInt(session.getAttribute("keyMetric").toString());
                }
            %>	
            <div class="container-fluid">
                <div class="visible-xs" style="margin-top: 10px;"></div>
                <div id="exTab1">	
                    <ul class="nav nav-tabs tab-text-color">
                        <li class="active">
                            <a href="#2" data-toggle="tab">Table</a>
                        </li>
                        <li>
                            <a href="#3" data-toggle="tab">Pie Chart</a>
                        </li>
                    </ul>
                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="2">
                            <div class="table-responsive" >
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Keyword Category</th>
                                            <th>Keywords</th>

                                            <%if (keymetric == 1) { %>
                                            <th>Conversions</th>
                                                <%} else if (keymetric == 2) { %>
                                            <th >Revenue</th>
                                                <%}%>

                                            <th>Cost</th>
                                            <th>Recommendations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList arr = new ArrayList<>();
                                            if (session.getAttribute("al") != null) {
                                                arr = (ArrayList) session.getAttribute("al");
                                            }
                                            if (arr.isEmpty() == false) {
                                                for (int i = 0; i < arr.size(); i++) {
                                                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);
                                                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();%>
                                        <tr>
                                            <%if (i == 0) {
                                                    if (keymetric == 1) {%>
                                            <td style="background-color: #A0D565;" class="lxrm-bold">Top Converting Keywords</td>
                                            <%} else if (keymetric == 2) {%>
                                            <td style="background-color: #A0D565;" class="lxrm-bold">Top Revenue Generating Keywords</td>
                                            <%}
                                                }
                                                if (i == 1) {
                                                    if (keymetric == 1) {%>
                                            <td style="background-color: #FFFFCC;" class="lxrm-bold">Other Converting Keywords</td>
                                            <%} else if (keymetric == 2) {%>
                                            <td style="background-color: #FFFFCC;" class="lxrm-bold">Other Revenue Generating Keywords</td>
                                            <%}
                                                }
                                                if (i == 2) {
                                                    if (keymetric == 1) {%>
                                            <td style="background-color: #FF643F;" class="lxrm-bold">Keywords with Clicks but no Conversions</td>
                                            <%} else if (keymetric == 2) {%>
                                            <td style="background-color: #FF643F;" class="lxrm-bold">Keywords with Clicks but no Revenue</td>
                                            <%}
                                                }
                                                if (i == 3) {%>
                                            <td style="background-color: #DDD9C3;" class="lxrm-bold">Keywords with Impressions but no Clicks</td>
                                            <%}
                                                if (i == 4) {%>
                                            <td style="background-color: #D8D8D8;" class="lxrm-bold">Keywords with no Impressions</td>
                                            <%}
                                                sqlRowSet.first();
                                                for (int j = 1; j <= sqlRowSetMetDat.getColumnCount(); j++) {%>
                                            <%if (i % 2 == 0) { %>
                                            <td class="text-center">
                                                <%if ((sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString())) == null) {%>
                                                - <%} else {%> <%=sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString())%>
                                                <%}%>
                                            </td>
                                            <%} else{%>
                                            <td class="text-center">

                                                <%if ((sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString())) == null) {%>
                                                - <%} else {
                                                %> <%=sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString())%>
                                                <%}%>
                                            </td>
                                            <%}
                                                }%>
                                            <%if (i == 0) {
                                                    for (int k = 0; k <= 0; k++) {
                                                        SqlRowSet sqlRowSet0 = (SqlRowSet) arr.get(k);
                                                        SqlRowSetMetaData sqlRowSetMetDat0 = sqlRowSet.getMetaData();
                                                        sqlRowSet.first();
                                                        for (int p = 1; p <= 1; p++) {
                                                            if (k == 0) {%>
                                            <td>
                                                <%if ((sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(p).toString())) == null || (sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(p).toString().trim())).equals("0")) {%>
                                                - <%} else {%>
                                                <ol class="recList">
                                                    <li><spring:message code="message.recomendation1" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation12" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation13" /></li>
                                                </ol>
                                                <%}%>
                                            </td>
                                            <%}
                                                        }
                                                    }
                                                }
                                                if (i == 1) {
                                                    for (int k1 = 1; k1 <= 1; k1++) {
                                                        SqlRowSet sqlRowSet1 = (SqlRowSet) arr.get(k1);
                                                        SqlRowSetMetaData sqlRowSetMetDat1 = sqlRowSet.getMetaData();
                                                        sqlRowSet1.first();
                                                        for (int p1 = 1; p1 <= 1; p1++) {
                                                            if (k1 == 1) {%>
                                            <td>
                                                <%if ((sqlRowSet1.getString(sqlRowSetMetDat1.getColumnName(p1).toString())) == null || (sqlRowSet1.getString(sqlRowSetMetDat1.getColumnName(p1).toString().trim())).equals("0")) {%>
                                                - <%} else{%><ol class="recList">
                                                    <li><spring:message code="message.recomendation2" /></li>
                                                        <%if (keymetric == 1) { %>
                                                    <li><spring:message
                                                            code="message.recomendation22" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation23" /></li>
                                                        <%} else if (keymetric == 2) { %>
                                                    <li><spring:message
                                                            code="message.recomendation22(1)" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation23(1)" /></li>
                                                        <%} %>

                                                </ol>
                                                <%}%>
                                            </td>
                                            <%}
                                                        }
                                                    }
                                                }
                                                if (i == 2) {
                                                    for (int k2 = 2; k2 <= 2; k2++) {
                                                        SqlRowSet sqlRowSet2 = (SqlRowSet) arr.get(k2);
                                                        SqlRowSetMetaData sqlRowSetMetDat2 = sqlRowSet.getMetaData();
                                                        sqlRowSet2.first();
                                                        for (int p2 = 1; p2 <= 1; p2++) {
                                                            if (k2 == 2) {%>
                                            <td>
                                                <%if ((sqlRowSet2.getString(sqlRowSetMetDat2.getColumnName(p2).toString())) == null || (sqlRowSet2.getString(sqlRowSetMetDat2.getColumnName(p2).toString().trim())).equals("0")) {%>
                                                - <%} else{%><ol class="recList">
                                                    <li><spring:message code="message.recomendation3" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation32" /></li>
                                                        <%if (keymetric == 1) { %>
                                                    <li><spring:message
                                                            code="message.recomendation33" /></li>
                                                        <%} else if (keymetric == 2) { %>
                                                    <li><spring:message
                                                            code="message.recomendation33(1)" /></li>
                                                        <%}%>
                                                    <li><spring:message
                                                            code="message.recomendation34" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation35" /></li>
                                                </ol>
                                                <%}%>
                                            </td>
                                            <%}
                                                        }
                                                    }
                                                }
                                                if (i == 3) {
                                                    for (int k3 = 3; k3 <= 3; k3++) {
                                                        SqlRowSet sqlRowSet3 = (SqlRowSet) arr.get(k3);
                                                        SqlRowSetMetaData sqlRowSetMetDat3 = sqlRowSet.getMetaData();
                                                        sqlRowSet3.first();
                                                        for (int p3 = 1; p3 <= 1; p3++) {
                                                            if (k3 == 3) {%>
                                            <td>
                                                <%if ((sqlRowSet3.getString(sqlRowSetMetDat3.getColumnName(p3).toString())) == null || (sqlRowSet3.getString(sqlRowSetMetDat3.getColumnName(p3).toString().trim())).equals("0")) {%>
                                                - <%} else {%><ol class="recList">
                                                    <li><spring:message code="message.recomendation4" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation42" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation43" /></li>
                                                    <li><spring:message
                                                            code="message.recomendation44" /></li>
                                                </ol>
                                            </td>
                                            <%}
                                                            }
                                                        }
                                                    }
                                                }
                                                if (i == 4) {
                                                    for (int k4 = 4; k4 <= 4; k4++) {
                                                        SqlRowSet sqlRowSet4 = (SqlRowSet) arr.get(k4);
                                                        SqlRowSetMetaData sqlRowSetMetDat4 = sqlRowSet.getMetaData();
                                                        sqlRowSet4.first();
                                                        for (int p4 = 1; p4 <= 1; p4++) {
                                                            if (k4 == 4) {%>
                                            <td class="keyanatable Regular" >
                                                <%if ((sqlRowSet4.getString(sqlRowSetMetDat4.getColumnName(p4).toString())) == null || (sqlRowSet4.getString(sqlRowSetMetDat4.getColumnName(p4).toString().trim())).equals("0")) {%>
                                                - <%} else {%>
                                                <ol class="recList">
                                                    <li><spring:message code="message.recomendation5" /></li>
                                                    <li><spring:message code="message.recomendation52" /></li>
                                                </ol>
                                                <%}%>
                                            </td>
                                            <%}%>
                                            <%}
                                                            }
                                                        }
                                                    }
                                                }%>
                                        </tr>
                                        <tr>
                                            <td class="lxrm-bold" >Total</td>
                                            <%if ((session.getAttribute("totkeys")) != null) {
                                                    int totkeys = Integer.parseInt(session.getAttribute("totkeys").toString());%>
                                            <td class="lxrm-bold text-center" style="text-align: center"><%=totkeys%></td>
                                            <%} %>
                                            <%
                                                int keyMetric = Integer.parseInt(session.getAttribute("keyMetric").toString());%>
                                            <%if (keyMetric == 1) {
                                                    if ((session.getAttribute("totconv")) != null) {
                                                        int totconv = Integer.parseInt(session.getAttribute("totconv").toString());
                                            %><td class="lxrm-bold text-center" style="text-align: center"><%=totconv%></td>
                                            <%}
                                                }
                                                if (keyMetric == 2) {
                                                    if ((session.getAttribute("totconv")) != null) {
                                                        float totrev = Float.parseFloat(session.getAttribute("totrev").toString());
                                            %><td class="lxrm-bold text-center" style="text-align: center"><%=totrev%></td>
                                            <%}
                                                }%>
                                            <%if ((session.getAttribute("totcost")) != null) {
                                                    double totcost = Double.parseDouble(session.getAttribute("totcost").toString());%>
                                            <td class="lxrm-bold text-center" style="text-align: center"><%=totcost%></td>
                                            <%}%>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane " id="3">
                            <div class="table-responsive">
                                <div id="keyWordsChart_div" class="col-xs-12 col-lg-6 "></div>
                                <div id="RevenueChart_div" class="col-xs-12  col-lg-6"></div>
                                <div id="CostChart_div" class="col-xs-12  col-lg-6 "></div>
                            </div>
                            <div id="UnavailableData"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="download-report-row col-xs-12" style="border: 1px solid #ddd;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 1%;">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none">
                                <p style="margin: 3% 0;"  class="lxrm-bold">Download complete report here: </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10">
                                <!--<div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">-->
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                    </div>
                                </div>
                                <div id="mail-sent-status" class="emptyData"></div>
                                <!--</div>-->
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="dropdown">
                                    <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                    <div id="downloadAnalysisReport" class="dropdown-content">
                                        <p class="lxrm-bold" onclick="downloadFormat('pdf');">PDF</p>
                                        <p class="lxrm-bold" onclick="downloadFormat('xls');">XLS</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>