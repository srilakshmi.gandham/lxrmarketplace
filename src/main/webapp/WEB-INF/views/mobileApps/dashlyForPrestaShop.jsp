
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PrestaShop Mobile Dashboard</title>
        <meta name="description"
      content=" Visit the PrestaShop Mobile Dashboard for the free PrestaShop store & get live access to informations such as Sales, Revenue, Invoice & Shipment. Try now!">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/ecommerce.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <%
            int toolDBId = 23;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

            });
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
         <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <div class="col-xs-12 blockquote-section mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            PrestaShop Mobile Dashboard is an Admin App for Android and iOS Phones, Tablets and iPads. 
                            This app allows you to connect to your PrestaShop store on mobile phones and tablets and provides you with live access to all vital store information such as Sales, Revenue, Invoice, and Shipment Information.
                        </p>
                        <p>
                            You can also connect with any number of PrestaShop stores from your app. 
                            Best of all there is no need for any coding. 
                            Anyone can simply install this app by creating a WebService in your PrestaShop store. 
                            We also provide a helpful guide with screenshot to show how a username, API Path, and API Key can be created.
                        </p>
                    </blockquote>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none features-section">
                    <blockquote><p class="lxrm-bold">Key Features & Benefits</p></blockquote>
                    <div class="col-xs-12">
                        <ul>
                            <li>
                                Receive automatic notifications on your mobile phone or tablet, if a purchase is made or certain products are currently out of stock. 
                                The notification feature is especially useful during the holiday season to ensure that all your popular products are consistently in stock.
                            </li>
                            <li>
                                Keep track of all vital sales and product metrics in a simple, yet intuitive dashboard in real-time wherever you are.
                            </li>
                            <li>
                                Get detailed information about each aspect of your e-commerce business by tapping the relevant widget. 
                                For example, if you want to know your revenue trend, just tap on the revenue widget, and you'll find out the month till date revenue chart. 
                            </li>
                            <li>
                                Take a closer look at each order to know who placed the order, what items were purchased within two taps, and more.
                            </li>
                            <li>
                                PrestaShop Mobile Dashboard provides a quick overview of the orders that are pending and which need your immediate attention.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none how-to-use-section">
                    <blockquote><p class="lxrm-bold">HOW TO USE?</p></blockquote>
                    <div class="col-xs-12">
                        <p class="lxrm-bold">Step 1: Download the Android app from Google Play Store and iOS app from App Store.</p>
                        <p class="lxrm-bold">Step 2: Login to your Prestashop admin.</p>
                        <p class="lxrm-bold">Step 3: Go To Admin Menu and select Advanced Parameters and from their select Web Service from drop down.</p>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p>1. In Prestashop Admin Menu, click on 'Advanced Parameters'. Select 'Web service' from the dropdown.</p>
                            <img src="/resources/images/prestaShop/step-1.png" alt=""/>
                            <p class="top-bottom-margin">2. Click 'Add New Webservice Key'.</p>
                            <img src="/resources/images/prestaShop/step-2.png" alt=""/>
                            <p class="top-bottom-margin">3. Click 'Generate' to create API Key automatically or enter one manually.</p>
                            <img src="/resources/images/prestaShop/step-3.png" alt=""/>
                            <p class="top-bottom-margin">4. Under Permission, select the first checkbox under View (Get) column to enable all the options and Save.</p>
                            <img src="/resources/images/prestaShop/step-4.png" alt=""/>
                            <p class="top-bottom-margin">5. Under Configuration, select Yes for the option 'Enable Prestashop' webservice and save.</p>
                            <img style="width:99%;" src="/resources/images/prestaShop/step-5.png" alt=""/>
                        </div>
                        <p class="lxrm-bold">Step 4: Open the App and enter your Store URL, Store Name and API Key along with store details.</p>
                        <div class="col-xs-12 padding-none">
                            <img src="/resources/images/prestaShop/step-6.png" alt=""/>
                        </div>
                        <div class="col-xs-12 padding-none">
                            <p class="lxrm-bold top-bottom-margin">Step 5: After entering the information Test the connection and if it is successful, you will now access the Dashboard.</p>
                        </div>
                        <div class="col-xs-12 padding-none" style="margin-top: 1%;">
                            <div class="col-xs-12 padding-none htu-steps">
                                <div class="col-xs-12 question-content padding-none lxrm-bold">
                                    What does the Dashboard contain?
                                </div>
                                <div class="col-xs-12 ans-content padding-none">
                                    The Dashboard consists of several widgets which provide you the latest snapshots of your store's order information in an easy to read manner. 
                                    You can tap on individual widgets to get more data. 
                                    It also contains the number of orders that are pending invoices or shipments. 
                                    Description of each widget is given below.
                                </div>
                                <img src="/resources/images/prestaShop/step-7.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Today's Orders Widget: </span>
                                    Displays total orders for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. Here you can view all the orders placed on the site along with their statuses. 
                                    You can also select different date ranges by tapping the calendar icon. 
                                    Filter the order by selecting the search icon. Tapping on the order will lead to the order details page.
                                </p>
                                <p>
                                    You can filter out the orders through the status of the order i.e. Completed, Shipped etc.
                                </p>
                                <img src="/resources/images/prestaShop/step-8.png" alt=""/>
                                <p class="top-bottom-margin">In the order details page you can view the Product and Customer details.</p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Today's Revenue Widget:</span>
                                    Displays total revenue for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view the Month Till Date Revenue trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and Lifetime store revenue and Last Month Revenue and Current Month Revenue. 
                                </p>
                                <img src="/resources/images/prestaShop/step-9.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Top Selling Widget:</span>
                                    Displays the current day’s Top Selling Product price.
                                </p>
                                <p>
                                    Tapping on this widget displays the list of Top selling products and the number of orders received for each one. 
                                    You can use the calendar and search buttons to further refine the list. 
                                    Tapping the product, leads to the product details page where you can find all product attributes.
                                </p>
                                <img src="/resources/images/prestaShop/step-10.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Last Transaction Widget:</span>
                                   Displays the timestamp of the most recent order.
                                </p>
                                <p>
                                    Tapping this widget leads to the list of Last 10 transactions on the website.
                                </p>
                                <img src="/resources/images/prestaShop/step-11.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Average Order Value Widget:</span>
                                   Displays the Average Order Value of the products sold today.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. Here you can view the Month AOV trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and Lifetime store AOV and Last Month AOV and Current Month AOV. 
                                </p>
                                <img src="/resources/images/prestaShop/step-12.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Invoice Pending Widget:</span>
                                   Displays the list of orders that are pending invoice.
                                </p>
                                <p>
                                    Tapping on it, leads to Invoice Pending List, which shows the list of all orders for whom the invoices are pending.
                                </p>
                                <img src="/resources/images/prestaShop/step-13.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Shipment Pending Widget:</span>
                                   Displays the list of orders that are pending shipment.
                                </p>
                                <p>
                                    Tapping on it, leads to Shipment Pending List, which shows the list of all orders for whom the shipments are pending.
                                </p>
                                <img src="/resources/images/prestaShop/step-14.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Settings:</span>
                                   You can change the settings of your store by tapping on the Settings (Gear) icon.
                                </p>
                                <p>Tapping it leads to the Settings Page</p>
                                <p>
                                    <span class="lxrm-bold">Sales Notification: </span>
                                   You can turn on/off the sales notification alerts received on your mobile. You can also set the frequency at which you want to receive the alerts from the Frequency drop down.
                                </p>
                                <p>
                                    <span class="lxrm-bold">Out of Stock Notification: </span>
                                   You can turn on/off the out of stock notification alerts received on your mobile. You can also set the frequency at which you want to receive the alerts from the Frequency Dropdown.
                                </p>
                                <img src="/resources/images/prestaShop/step-15.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Add another Store:</span>
                                   You can add multiple Prestashop stores to your Dashly App using this option. Select Add Store option from the drop down icon next to your store name.
                                </p>
                                <img src="/resources/images/prestaShop/step-16.png" alt=""/>
                                <p class="top-bottom-margin">
                                   Enter your store details and click Test to add the store to your app
                                </p>
                                <img src="/resources/images/prestaShop/step-17.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Recommended part start-->
        <div class="clearfixdiv"></div>
         <div class="container">
            <%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>