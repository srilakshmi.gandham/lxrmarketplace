<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Magento Product Feed</title>
        <meta name="description"
              content=" Visit the Bigcommerce Mobile Dashboard for the paid Bigcommerce store & keep track of all the vital sales & product metrics. Try now!">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/ecommerce.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <%
            int toolDBId = 39;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

            });
        </script>
        <style>
            .col-xs-12 img{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <div class="col-xs-12 blockquote-section mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            LXR Product Feed Manager extension is designed for automatically generating the products list
                            in a format that can be directly uploaded to shopping sites such as Google Shopping. It generates
                            the Product Catalog Feed in csv, xml files from your Magento Ecommerce Store.
                        </p>
                    </blockquote>
                    <p class="padding-none" style="padding-top: 12px;"><span class="lxrm-bold">Note:</span> This extension does not create any Google AdWords / Google Merchant Center Account. It helps in generating a feed and push it to the Google Merchant Center Account which is linked to the Google AdWords. 
                        To upload the product feed, you need to <a href="https://www.google.com/retail/solutions/shopping-campaigns/#?modal_active=none" target="_blank">sign up here:</a> Google Merchant Center.</p>
                </div>

                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none features-section">
                    <blockquote><p class="lxrm-bold">Key Features & Benefits</p></blockquote>
                    <div class="col-xs-12">
                        <ul>
                            <li>
                                The generated feed file can be directly uploaded to Google Merchant Centre to make your
                                products available on Google Shopping.
                            </li>
                            <li>
                                Automated Product Feeds can be scheduled from your Magento Store to Google Merchant
                                Center.
                            </li>
                            <li>
                                You have a choice of filtering / selecting the products to be sent to the Feed based on different
                                parameters explained below.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none how-to-use-section">
                    <blockquote><p class="lxrm-bold">How to set up a shopping feed for Google Merchant Center?</p></blockquote>
                    <div class="col-xs-12">
                        <p>Click the LXR Feed button from the left side menu.</p>
                        <img style="width:100%; " src="/resources/images/magentoProductFeed/magento-plugin-screen1_1_1.png" alt=""/>
                        <p>Click on <span class="lxrm-bold">Add New Feed</span> button to create a new product feed.</p>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold top-bottom-margin">Step 1: Shopping Feed information setup</p>
                            <p>Select ‘Google Shopping’ from the feed type dropdown.  [This Magento Extension will be
                                extended to other shopping feeds in future and hence the dropdown].</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen2_1_1.png" alt=""/>
                            <p style="margin-top: 2%;">Click <span class="lxrm-bold">Save Feed</span> button to continue.</p>
                        </div>
                        <div>
                            <p class=" lxrm-bold">Step 2: Basic product filter information setup</p>
                            <p>Product Feed Information: Provide Feed name and Feed File Name to generate a feed template.</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen3_1_1.png" alt=""/>
                        </div>
                        <div class="col-xs-12 padding-none htu-steps top-bottom-margin">
                            <p class="lxrm-bold">Product Name Filters:</p>
                            <p><span class="lxrm-bold">1.&nbsp;Product name Begins with:</span> Filter products based on the product name begins with </p>
                            <p>Enter the Starting letter of the product names to be filtered separated by commas.</p>
                            <p><span class="lxrm-bold">2.&nbsp;Product name Contains with:</span> Filter products based on the product name contain within particular product title.</p>
                            <p>Enter the letters contains in the product name separated by commas.</p>
                            <img src="/resources/images/magentoProductFeed/product_filter_1_1.png" alt=""/> 
                        </div>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold ">Price Filter:</p>
                            <p><span class="lxrm-bold">1. Price Type:</span> Filter products based on price type (Sale Price, Actual Price, etc.). Select any price type from the dropdown provided.</p>
                            <p><span class="lxrm-bold">2. Price Range Option:</span> Filter products based on the price range of the products. For example Less than, Greater than, equal to, etc. Select a price range less than or greater than or between from the drop down.</p>
                            <p><span class="lxrm-bold">3. Price Range Value:</span> Filter products based on price range value of the products. These
                                values will be filtered depending on the Price Range.</p>
                            <p>Enter a price range value to filter the products.</p>
                            <p>These price range values will be filtered based on the range and value given in the text
                                box.</p>
                            <p>If Price range is selected <span class="lxrm-bold">‘Price Range Between’</span> then give values as ‘100-20000’.</p>
                            <p>If Price range is selected <span class="lxrm-bold">‘ALL’</span> then no need to give any price values.</p>
                            <img src="/resources/images/magentoProductFeed/price_fitler_1_1.png" alt=""/> 
                        </div>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold ">Stock Filters:</p>
                            <p><span class="lxrm-bold">1. Availability:</span> Filter products based on the Stock Availability of the products. For example
                                In Stock, Out of Stock, etc. Ideally include only the products that are In Stock and
                                exclude products that are not in stock.</p>
                            <p>Select Availability from the dropdown provided.</p> 
                            <p><span class="lxrm-bold">2. Stock Availability Range Option:</span> Filter products based on the stock availability of the
                                products. For example Less Than, Greater than, a particular count.</p>
                            <p>Select a stock range less than or greater than or all from the drop down.</p>
                            <p><span class="lxrm-bold">3. Stock Availability Range Value:</span> Filter products based on stock range of the products.
                            <p>These values will be filtered depending on the Stock Range.</p>
                            <p>Enter a Stock availability range value to filter the products.</p>
                            <p>If Stock availability range is selected as ‘All’, then stock availability range will be ignored
                                and all products will be selected.</p>
                            <img src="/resources/images/magentoProductFeed/stock_filter_1_1.png" alt=""/> 
                        </div>
                        <div>
                            <p class=" lxrm-bold">Step 3. Product Category filter information setup</p>
                            <p>Select at least one category to filter products.</p>
                            <p>Select <span class="lxrm-bold">All Categories</span> if you would like to apply the below filter on all categories.</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen-4_1_1.png" alt=""/>
                            <div class="clearfixdiv"></div>
                        </div>
                        <div> 
                            <p class=" lxrm-bold ">Step 4: Basic Product Feed information setup</p>
                            <p>Google Merchant Center will require some mandatory fields for product feed setup. The
                                requirements depend on your store location and the type of products.</p>
                            <p><span class="lxrm-bold">GTIN</span> (A Global Trade Item Number) and <span class="lxrm-bold">MPN</span> (Manufacturer Part Number)
                                are important fields
                                that need to be part of the product feed. Understand the definitions of these data fields and map
                                the related product attribute for these fields from the dropdown. [Dropdown lists all the product
                                attributes from your product catalog].</p>
                            <p><span class="lxrm-bold">Google Product Category</span> field is an attribute to indicate the category of your item based on the
                                Google product taxonomy.Click <a href="https://support.google.com/merchants/answer/6324436?hl=en" target="_blank"> here</a> for more details.</p>
                            <p>
                                You can verify the  <a href="https://www.google.com/basepages/producttype/taxonomy.en-US.txt">list</a> of google product categories based on item/category.
                            </p>
                            <p>If Google Product Category attribute is not defined in your website as per the Google Merchant
                                Centre, then the default product categories will be considered while generating the feed.</p>
                            <p><span class="lxrm-bold" style="color:#F58120;">Note:</span>
                                If the store product categories are not according to Google Product Categories then your
                                Feed may disapproved. Then need to create a custom attribute for Google Product Category and
                                need to provide the related category name to all products.</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen5_1_1.png" alt=""/> 
                        </div>
                        <div>
                            <div class="clearfixdiv" style="height:15px !important;"></div>
                            <p class=" lxrm-bold ">Step 5. Scheduler setup</p>
                            <p>Helps to schedule the automated product feed generation from your store to Google Merchant Center. </p>
                            <p><span class="lxrm-bold">Select Day(s): </span>Select the days to fetch the product data from the store.</p>
                            <p><span class="lxrm-bold">Select Hour(s): </span>Select a particular timeslot from the dropdown, to update/fetch the product feed
                                data for the above selected days.</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen6_1_1.png" alt=""/> 
                            <p class="top-bottom-margin" >Click <span class="lxrm-bold">Save Feed</span> Button.</p>
                            <p class="top-bottom-margin" >It will return back to plugin home page.</p>
                            <p class="top-bottom-margin" >Once the above setup is complete, click on the generated feed name, it will open the feed with
                                all the filters already selected.</p>
                            <p class="top-bottom-margin" >Click <span class="lxrm-bold">Generate CSV</span> button to generate the CSV Product Feed with the selected filters.</p>
                            <p class="top-bottom-margin" >Click <span class="lxrm-bold">Generate XML</span> button to generate the XML Product Feed with the selected filters.</p>
                            <p class="top-bottom-margin" >Click <span class="lxrm-bold">Preview Sample Feed</span> option to view sample feed for ten products in XML format.</p>
                            <p class="top-bottom-margin" >
                                We can able to view or download the Generated (CSV/XML) product feed at plugin main page
                                with desired location URL.
                            </p>
                            <img style="width:100%;" src="/resources/images/magentoProductFeed/magento-plugin-screen31_1_1.png" alt=""/> 
                        </div>
                        <div>
                            <div class="clearfixdiv" style="height:15px !important;"></div>
                            <p class=" lxrm-bold">Merchant Center</p>
                            <p>Merchant Center is a tool that helps you upload your store, brand and product data and make it
                                available to Google Shopping and other Google services. To advertise your products on Google,
                                you’ll need a Merchant Center account.</p>
                            <p class="lxrm-bold">How to Create a Merchant Center Account?</p>
                            <p> <a href="https://support.google.com/merchants/answer/188924?hl=en"><span style="text-decoration: underline;">https://support.google.com/merchants/answer/188924?hl=en</span></a></p>
                            <p class="lxrm-bold">How to setup product feed to Merchant Center?</p>
                            <img src="/resources/images/magentoProductFeed/magento-plugin-screen1_1_1.png" alt=""/> 
                            <p class="top-bottom-margin" >Upload either the generated CSV or XML URL to merchant center.</p>
                        </div>
                        <div>  
                            <p><span class="lxrm-bold" style="color:#F58120;">Note:</span></p>
                            <p class="lxrm-bold">How do Scheduler Tasks Work:</p>
                            <p>cron.php is launched regularly from your server side (every 5 minutes most of the time)</p>
                            <p>The cron task for Product Feed is targeted and evaluates if a product feed data needs to be
                                updated based on the scheduler day and timestamp stored in the database and on the schedule
                                table for this specific profile.</p>
                            <p>If the above condition matches, then the product feed data will be re-generated.</p>
                            <p>Be sure that you have correctly configured a scheduled task from the server side (cpanel)
                                targeting the cron.php file in your Woocommerce installation.</p>
                            <img src="/resources/images/magentoProductFeed/SchedulerTaskNew.png" alt="" style="width:50%;"/>          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>