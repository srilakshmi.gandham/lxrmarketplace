
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashly For Magento</title>
        <meta name="description"
              content=" Visit the Magento Mobile Dashboard for the free Magento store & get detailed information about each aspect of your ecommerce business. Find out more!">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/ecommerce.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <%
            int toolDBId = 18;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

            });
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <div class="col-xs-12 blockquote-section mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            Dashly is a real-time Magento Mobile Dashboard and Admin App for both Android and IOS Phones, Tablets and iPads.
                            It lets you connect to your Magento Store on mobile phones and tablets and get live access to all vital store information such as Sales, Revenue, Invoice, and Shipment information. 
                        </p>
                        <p>
                            You can also connect with any number of Magento stores from your app. 
                            Best of all, there is no need for any type of coding. 
                            Anyone can simply install this app by creating an API user in your Magento store. 
                            We also provide a helpful guide with screenshots on how an API User and other roles can be created.
                        </p>
                    </blockquote>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none features-section">
                    <blockquote><p class="lxrm-bold">Key Features & Benefits</p></blockquote>
                    <div class="col-xs-12">
                        <ul>
                            <li>
                                Keep track of all vital sales and product metrics in a simple, yet intuitive dashboard in real-time wherever you are. 

                            </li>
                            <li>
                                Get detailed information about each aspect of your e-commerce business by tapping the relevant widget. For example, if you want to know your revenue trend, just tap on the revenue widget, and you'll see the month till date revenue chart.
                            </li>
                            <li>
                                Take a closer look at each order to know who placed the order, what items were purchased within two taps, and so on.
                            </li>
                            <li>
                                Quick overview of all pending orders that need your attention. As an option, you can simply generate an invoice or process a shipment, or even hold the order, directly from your mobile.
                            </li>
                            <li>
                                Receive automatic notifications on your mobile phone or tablet, if a purchase is made or certain products are currently out of stock. The notification feature is especially useful during the holiday season to ensure that all your popular products are consistently in stock.
                            </li>
                            <li>
                                Ability to add multiple Magento stores targeting different geographies.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none how-to-use-section">
                    <blockquote><p class="lxrm-bold">HOW TO USE?</p></blockquote>
                    <div class="col-xs-12">
                        <p class="lxrm-bold">Step 1: Enable Soap.</p>
                        <div class="col-xs-12 padding-none" style="margin-top: 1%;">
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>1. Locate php.ini in your apache bin folder, i.e Apache/bin/php.ini</p>
                                <p>2. Remove the ; from the beginning of extension=php_soap.dll</p>
                                <p>3. Restart your Apache server</p>
                                <img src="/resources/images/magento/step-1.png" alt=""/>
                                <p class="top-bottom-margin">4. Look up your phpinfo(); again and check if you see a similar picture to the one above</p>
                            </div>
                        </div>
                        <p class="lxrm-bold">Step 2: Create Role for the web services.</p>
                        <div class="col-xs-12 padding-none" style="margin-top: 1%;">
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>A. Go to the Web Service Roles screen from the System menu.</p>
                                <img src="/resources/images/magento/step-2.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>B. Add a new Role.</p>
                                <img src="/resources/images/magento/step-3.png" alt=""/>
                                <p class="top-bottom-margin">*If you already have a suitable role, feel free to use that instead of creating a new one.</p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>C. You might like to call it dashly but you can call it whatever you like.</p>
                                <img src="/resources/images/magento/step-4.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p class="top-bottom-margin">D. Choose selected resources as shown.</p>
                                <img src="/resources/images/magento/step-5.png" alt=""/>
                            </div>
                        </div>
                        <p class="lxrm-bold">Step 3: Create a user for the webservices</p>
                        <div class="col-xs-12 padding-none" style="margin-top: 1%;">
                            <div class="col-xs-12 padding-none htu-steps">
                                <p class="top-bottom-margin">A. Go to the Web Service Users screen from the System menu.</p>
                                <img src="/resources/images/magento/step-6.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p class="top-bottom-margin">B. Add a new User.</p>
                                <img src="/resources/images/magento/step-7.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p class="top-bottom-margin">C. Fill in the details, choose any username and key you like, the key should be secure, and unique.</p>
                                <img src="/resources/images/magento/step-8.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p class="top-bottom-margin">D. Choose the role for this new user, or an existing role if you did not need to create one.</p>
                                <img src="/resources/images/magento/step-9.png" alt=""/>
                                <p class="top-bottom-margin">* Then Save your new Web Service user and you’re done!</p>
                            </div>
                        </div>
                        <p class="lxrm-bold">Step 4: Open the App and enter your API username and Key along with store details.</p>
                        <div class="col-xs-12 padding-none htu-steps">
                            <img src="/resources/images/magento/step-10.png" alt=""/>
                        </div>
                        <p class="lxrm-bold">Step 5: After entering the information Test the connection and if it is successful, you will now access the Dashboard.</p>
                        <div class="col-xs-12 padding-none htu-steps">
                            <div class="col-xs-12 question-content padding-none lxrm-bold">
                                What does the Dashboard contain?
                            </div>
                            <div class="col-xs-12 ans-content padding-none">
                                The Dashboard consists of several widgets which provide you the latest snapshots of your store's order information in an easy to read manner. You can tap on individual widgets to get more data. 
                                It also contains the number of orders that are pending invoices or shipments. 
                                Description of each widget is given below.
                            </div>
                            <img src="/resources/images/magento/step-11.png" alt=""/>
                            <p class="top-bottom-margin"></p>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold top-bottom-margin">Today's Orders Widget:</span>
                                    Displays total orders for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view all the orders placed on the site along with their statuses.
                                    You can also select different date ranges by tapping the calendar icon. 
                                    Filter the order by selecting the search icon. Tapping on the order will lead to the order details page.
                                </p>
                                <p>You can filter out the orders through the status of the order ie. Completed, Shipped etc.</p>
                                <img src="/resources/images/magento/step-12.png" alt=""/>
                                <img src="/resources/images/magento/step-13.png" alt=""/>
                                <p class="top-bottom-margin">
                                    In the order details page you can view the Product and Customer details. If the order is pending then you can perform operations such as Hold the Order, Cancel it, Create Invoice or Create shipment if the invoice is completed.
                                </p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Today's Revenue Widget:</span>
                                    Displays total revenue for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view the Month Till Date Revenue trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and Lifetime store revenue and Last Month Revenue and Current Month Revenue.
                                </p>
                                <p>You can filter out the orders through the status of the order ie. Completed, Shipped etc.</p>
                                <img src="/resources/images/magento/step-14.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Top Selling Widget:</span>
                                    Displays the current day’s Top Selling Product and its price.
                                </p>
                                <p>
                                    Tapping on this widget displays the list of Top selling products and the number of orders received for each one. 
                                    You can use the calendar and search buttons to further refine the list. 
                                    Tapping the product, leads to the product details page where you can find all product attributes.
                                </p>
                                <img src="/resources/images/magento/step-15.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Last Transaction Widget:</span>
                                    Displays the timestamp of the most recent order.
                                </p>
                                <p>Tapping this widget leads to the list of Last 10 transactions on the website.</p>
                                <img src="/resources/images/magento/step-16.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Average Order Value Widget:</span>
                                    Displays the Average Order Value of the products sold today.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view the Month AOV trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and Lifetime store AOV and Last Month AOV and Current Month AOV. 
                                </p>
                                <img src="/resources/images/magento/step-17.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Invoice Pending Widget: </span>
                                    Displays the list of orders that are pending invoice.
                                </p>
                                <p>
                                    Tapping on it, leads to Invoice Pending List, which shows the list of all orders for whom the invoices are pending. 
                                    You can select single or multiple orders and create invoices for them by tapping the Create Invoice button. 
                                    You also can create shipment for the same orders. Orders can be placed on hold or cancelled as well.
                                </p>
                                <img src="/resources/images/magento/step-18.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Shipment Pending Widget:</span>
                                    Displays the list of orders that are pending shipment.
                                </p>
                                <p>
                                    Tapping on it, leads to Shipment Pending List, which shows the list of all orders for whom the shipments are pending. 
                                    You can select single or multiple orders and create shipments for them by tapping the Create Shipment button.
                                </p>
                                <img src="/resources/images/magento/step-19.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Settings:</span>
                                    You can change the settings of your store by tapping on the Settings (Gear) icon.
                                </p>
                                <p>Tapping it leads to the Settings Page</p>
                                <p>
                                    <span class="lxrm-bold">Sales Notification:</span>
                                    You can turn on/off the sales notification alerts received on your mobile. 
                                    You can also set the frequency at which you want to receive the alerts from the Frequency Drop Down
                                </p>
                                <p>
                                    <span class="lxrm-bold">Out of Stock Notification:</span>
                                    You can turn on/off the out of stock notification alerts received on your mobile. 
                                    You can also set the frequency at which you want to receive the alerts from the Frequency Drop Down.
                                </p>
                                <img src="/resources/images/magento/step-20.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Add another Store:</span>
                                    You can add multiple Magento stores to your Dashly App using this option. 
                                    Select Add Store option from the drop down icon next to your store name.
                                </p>
                                <img src="/resources/images/magento/step-21.png" alt=""/>
                                <p class="top-bottom-margin">Enter your store details and click Test to add the store to your app</p>
                                <img src="/resources/images/magento/step-22.png" alt=""/>
                            </div>
                        </div>
                    </div><!--col-xs-12-->
                </div>
            </div>
        </div>
        <!--Recommended part start-->
        <div class="clearfixdiv"></div>
         <div class="container">
            <%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>