
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bigcommerce Mobile Dashboard</title>
        <meta name="description"
              content=" Visit the Bigcommerce Mobile Dashboard for the paid Bigcommerce store & keep track of all the vital sales & product metrics. Try now!">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/ecommerce.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <%
            int toolDBId = 22;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

            });
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <div class="col-xs-12 blockquote-section mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            Bigcommerce Mobile Dashboard is an Admin App for Android and iOS Phones, Tablets and iPads. 
                            This app allows you to connect to your Bigcommerce store on both mobile phones and tablets. 
                            You'll be able to get live access to vital store information 
                            such as Sales, Revenue, Invoice, and Shipment Information.
                        </p>
                        <p>
                            Best of all, there's no need for any coding. 
                            You can simply install the app by creating a Legacy API Account in your Bigcommerce store. 
                            We also provide a helpful guide with screenshot to show how a username, 
                            API Path, and API Key can be created.
                        </p>
                    </blockquote>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none features-section">
                    <blockquote><p class="lxrm-bold">Key Features & Benefits</p></blockquote>
                    <div class="col-xs-12">
                        <ul>
                            <li>
                                Receive automatic notifications on your mobile phone or tablet, 
                                if a purchase is made or certain products are currently out of stock. 
                                The notification feature is especially useful during the holiday season to ensure that 
                                all your popular products are consistently in stock. 

                            </li>
                            <li>
                                Keep track of all vital sales and product metrics in a simple, yet intuitive dashboard in real-time wherever you are.
                            </li>
                            <li>
                                Get detailed information about each aspect of your e-commerce business by tapping the relevant widget. 
                                For example, if you want to know your revenue trend, just tap on the revenue widget, and 
                                you'll find out the month till date revenue chart.
                            </li>
                            <li>
                                Take a closer look at each order to know who placed the order, what items were purchase within two taps, and more.
                            </li>
                            <li>
                                Bigcommerce Mobile Dashboard provides a quick overview of the orders that are pending and 
                                which need your immediate attention.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none how-to-use-section">
                    <!--<div class="clearfixdiv"></div>-->
                    <blockquote><p class="lxrm-bold">HOW TO USE?</p></blockquote>
                    <div class="col-xs-12">
                        <p class="lxrm-bold">Step 1: Download the Android app from Google Play Store and iOS app from App Store.</p>
                        <p class="lxrm-bold">Step 2: Login into your bigcommerce admin.</p>
                        <p class="lxrm-bold">Step 3: Create Legacy API Accounts. To create a Legacy API accounts, follow the steps below.</p>

                        <div class="col-xs-12 padding-none" style="margin-top: 1%;">
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>1. In Bigcommerce Admin menu, click on 'Setup & Tools'. Select 'Legacy API accounts' from the dropdown.</p>
                                <img src="/resources/images/bigCommerce/step-1.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>2. Click 'Create a Legacy API Account'.</p>
                                <img src="/resources/images/bigCommerce/step-2.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>3. Enter Username, you can generate new API Token also and finally click 'save' button.</p>
                                <img src="/resources/images/bigCommerce/step-3.png" alt=""/>
                                <p>
                                    <span class="lxrm-bold">Note: </span>
                                    Finally go back to the app and enter Username, API Path and API Key as what you created in Store. 
                                    Store name is optional and select your store currency.
                                </p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>4. Open the App and enter your API username, API Path and API Token along with store details.</p>
                                <img src="/resources/images/bigCommerce/step-4.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>5. After entering the information Test the connection and if it is successful, you will now access the Dashboard.</p>
                                <div class="col-xs-12 question-content padding-none lxrm-bold">
                                    What does the Dashboard contain?
                                </div>
                                <div class="col-xs-12 ans-content padding-none">
                                    The Dashboard consists of several widgets which provide you the latest snapshots of 
                                    your store’s order information in an easy to read manner. 
                                    You can tap on individual widgets to get more data. It also contains the number of orders 
                                    that are pending invoices or shipments. Description of each widget is given below.
                                </div>
                                <img src="/resources/images/bigCommerce/step-5.png" alt=""/>


                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Today's Orders Widget: </span>
                                    Displays total orders for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view all the orders placed on the site along with their statuses. 
                                    You can also select different date ranges by tapping the calendar icon. 
                                    Filter the order by selecting the search icon. 
                                    Tapping on the order will lead to the order details page.
                                </p>
                                <p>You can filter out the orders through the status of the order ie. Completed, Shipped etc.</p>
                                <img src="/resources/images/bigCommerce/step-6.png" alt=""/>
                                <p class="top-bottom-margin">
                                    In the order details page you can view the Product and Customer details.
                                </p>
                                <img src="/resources/images/bigCommerce/step-7.png" alt="" />
                                
                            </div>

                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Today's Revenue Widget: </span>
                                    Displays total revenue for the day.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view the Month Till Date Revenue trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and 
                                    Lifetime store revenue and Last Month Revenue and Current Month Revenue.
                                </p>
                                <img src="/resources/images/bigCommerce/step-8.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Top Selling Widget: </span>
                                    Displays the current day’s Top Selling Product price.
                                </p>
                                <p>
                                    Tapping on this widget displays the list of Top selling products and 
                                    the number of orders received for each one. 
                                    You can use the calendar and search buttons to further refine the list. 
                                    Tapping the product, leads to the product details page where you can find 
                                    all product attributes.
                                </p>
                                <img src="/resources/images/bigCommerce/step-9.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Last Transaction Widget: </span>
                                    Displays the timestamp of the most recent order.
                                </p>
                                <p>Tapping this widget leads to the list of Last 10 transactions on the website.</p>
                                <img src="/resources/images/bigCommerce/step-10.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Average Order Value Widget: </span>
                                    Displays the Average Order Value of the products sold today.
                                </p>
                                <p>
                                    Tapping on this widget leads to the Order Details page. 
                                    Here you can view the Month AOV trend. 
                                    Also provided are metrics such as revenue for Last 7 days, Last 30 days and 
                                    Lifetime store AOV and Last Month AOV and Current Month AOV.
                                </p>
                                <img src="/resources/images/bigCommerce/step-11.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Invoice Pending Widget: </span>
                                    Displays the list of orders that are pending invoice.
                                </p>
                                <p>Tapping on it, leads to Invoice Pending List, which shows the list of 
                                    all orders for whom the invoices are pending.
                                </p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Shipment Pending Widget: </span>
                                    Displays the list of orders that are pending shipment.
                                </p>
                                <p>Tapping on it, leads to Shipment Pending List, which shows the list of 
                                    all orders for whom the shipments are pending.
                                </p>
                                <img src="/resources/images/bigCommerce/step-12.png" alt=""/>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Settings: </span>
                                    You can change the settings of your store by tapping on the Settings (Gear) icon.
                                </p>
                                <img src="/resources/images/bigCommerce/step-13.png" alt=""/>
                                <p>Tapping it leads to the Settings Page.</p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Sales Notification: </span>
                                    You can turn on/off the sales notification alerts received on your mobile. 
                                    You can also set the frequency at which you want to receive 
                                    the alerts from the Frequency Dropdown.
                                </p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Out of Stock Notification: </span>
                                    You can turn on/off the out of stock notification alerts received on your mobile. 
                                    You can also set the frequency at which you want to receive 
                                    the alerts from the Frequency Drop Down.
                                </p>
                            </div>
                            <div class="col-xs-12 padding-none htu-steps">
                                <p>
                                    <span class="lxrm-bold">Add another Store: </span>
                                    You can add multiple Bigcommerce stores to your Dashly App using this option. 
                                    Select Add Store option from the drop down icon next to your store name.
                                </p>
                                <img src="/resources/images/bigCommerce/step-14.png" alt=""/>
                                <p class="top-bottom-margin">
                                    Enter your store details and click Test to add the store to your app.
                                </p>
                                <img src="/resources/images/bigCommerce/step-15.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Recommended part start-->
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>