<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> WooCommerce Product Feed</title>
        <meta name="description"
              content=" Visit the Bigcommerce Mobile Dashboard for the paid Bigcommerce store & keep track of all the vital sales & product metrics. Try now!">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/ecommerce.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <%
            int toolDBId = 33;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            $(document).ready(function () {

            });
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-xs-12 mobile-padding-none">
                <div class="col-xs-12 blockquote-section mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            It generates the Product Feed in CSV and XML formats the way it suits the Shopping Engines such as Google Merchant Center in just a few clicks. Shopping Engines accept the product feeds in a specific format and it should be the updated file. This extension helps you to generate the feed with a one-time configuration. You can set up the specific product filters which you would like to sell on these engines and configure the feed at scheduled intervals to the Shopping Engines.
                        </p>
                    </blockquote>
                    <p class="padding-none" style="padding-top: 12px;"><span class="lxrm-bold">Note:</span> This extension does not create any Google AdWords / Google Merchant Center Account. It helps in generating a feed and push it to the Google Merchant Center Account which is linked to the Google AdWords. 
                        To upload the product feed, you need to <a href="https://www.google.com/retail/solutions/shopping-campaigns/#?modal_active=none">sign up here:</a> Google Merchant Center.</p>
                </div>

                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none features-section">
                    <blockquote><p class="lxrm-bold">Key Features & Benefits</p></blockquote>
                    <div class="col-xs-12">
                        <ul>
                            <li>
                                LXR Product Feed Manager extension is designed for automatically generating the products list
                                from your WooCommerce Store in a format that can be directly uploaded to shopping sites such
                                as ‘Google Shopping’. It generates the Product Catalog Feed in csv, xml files from your
                                WooCommerce based Store.
                            </li>
                            <li>
                                The generated feed file can be directly uploaded to ‘Google Merchant Centre’ to make your
                                products available on ‘Google Shopping’.
                            </li>
                            <li>
                                One can Schedule Automated Product Feeds from the WooCommerce Store to ‘Google Merchant
                                Center’ to have a fresh product feed.
                            </li>
                            <li>
                                You have a choice of filtering / selecting the products to be sent to the Feed based on different
                                parameters explained below.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12 mobile-padding-none how-to-use-section">
                    <blockquote><p class="lxrm-bold">How to set up a shopping feed for Google Merchant Center?</p></blockquote>
                    <div class="col-xs-12">
                        <p>Click the LXR Feed button from the left side menu.</p>
                        <p>Click on LXR Feed button to create a new product feed.</p>
                        <img style="width:100%; " src="/resources/images/wooCommerceProductFeed/Create.png" alt=""/>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold top-bottom-margin">Step 1: Shopping Feed information setup</p>
                            <p>1. Product Feed Information: Provide Feed File Name to create a Feed template.</p>
                            <p>2. Select ‘Feed Format’ - either CSV or XML.</p>
                            <img src="/resources/images/wooCommerceProductFeed/feed-format.png" alt=""/>
                            <p style="margin-top: 2%;">3. Select ‘Google Shopping’ from the template dropdown. [This WooCommerce Extension
                                will be extended to other shopping feeds in future and hence the dropdown]. </p>
                            <img src="/resources/images/wooCommerceProductFeed/feed-template.png" alt=""/> 
                        </div>
                        <div>
                            <p class=" lxrm-bold">Step 2: Product Filter Options</p>
                            You can filter the products to be included in the Google Product Feed using multiple options (one
                            or more) - Product Category, Product Name beginning with or containing specific letters, pricing
                            range and stock quantity range.
                        </div>
                        <div class="col-xs-12 padding-none htu-steps top-bottom-margin">
                            <p class="lxrm-bold">Product Category filter information setup</p>
                            <p>1. Select one or more product categories (at least one category) to filter products.</p>
                            <p>2. Select All Categories if you would like to apply the below filter on all categories.</p>
                            <img src="/resources/images/wooCommerceProductFeed/product-filter.png" alt=""/> 
                        </div>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold ">Product Name Filters:</p>
                            <p><span class="lxrm-bold">1. Product name Begins with:</span> Filter products based on the product name begins with particular letter.</p>
                            <p>Enter the Starting letter or few letters(s) of the product names to be filtered separated by commas.</p>
                            <p>Ex: Enter “Men” to filter all products like Men’s Shoes, Men’s Shirts etc.</p> 
                            <p><span class="lxrm-bold">2. Product name Contains with:</span> Filter products based on the product name contains Sequence of letters within its name.</p>
                            <p>Enter the letters contains in the product name separated by commas.</p>
                            <img src="/resources/images/wooCommerceProductFeed/product-filter-contains.png" alt=""/> 
                        </div>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold ">Price Filters:</p>
                            <p><span class="lxrm-bold">1. Price Type:</span> Filter products based on price type (Sale Price, Actual Price, etc.).
                                Select any price type from the dropdown provided.</p>
                            <p><span class="lxrm-bold">2. Price Range Option:</span> Filter products based on the price range of the products. For example
                                Less than, Greater than, Equal to, etc. Select a price range less than or greater than or
                                between from the drop down.</p>
                            <p><span class="lxrm-bold">3. Price Range Value:</span> Filter products based on price range value of the products.
                            <p>These values will be filtered depending on the Price Range.</p>
                            <p>Enter a price range value to filter the products.</p>
                            <p>These price range values will be filtered based on the range and value given in the text box.</p>
                            <p>If Price range is selected ‘Price Range Between’ then give values as ‘100-250’. If
                                Select the Price Range as ‘All’ to include all products ignoring the pricing filter.</p>
                            <img  src="/resources/images/wooCommerceProductFeed/price-filter.png" alt=""/>
                        </div>
                        <div class="col-xs-12 padding-none htu-steps">
                            <p class="lxrm-bold ">Stock Filters:</p>
                            <p><span class="lxrm-bold">1. Availability:</span> Filter products based on the Stock Availability of the products. For example
                                In Stock, Out of Stock, etc. Ideally include only the products that are In Stock and
                                exclude products that are not in stock.</p>
                            Select Availability from the dropdown provided.</p> 
                            <p><span class="lxrm-bold">2. Stock Availability Range Option:</span> Filter products based on the stock availability of the
                                products. For example Less Than, Greater than, a particular count.</p>
                            <p>Select a stock range less than or greater than or all from the drop down.</p>
                            <p><span class="lxrm-bold">3. Stock Availability Range Value:</span> Filter products based on stock range of the products.
                            <p>These values will be filtered depending on the Stock Range.</p>
                            <p>Enter a Stock availability range value to filter the products.</p>
                            <p>If Stock availability range is selected as ‘All’, then stock availability range will be ignored
                                and all products will be selected.</p>
                            <img src="/resources/images/wooCommerceProductFeed/stock-filter.png" alt=""/> 
                            <p class="top-bottom-margin" >Click Save Feed button to continue.</p>
                        </div>
                        <div> 
                            <p class=" lxrm-bold ">Step 3: Basic Product Feed information setup</p>
                            <p>‘Google Merchant Center’ will require some mandatory fields for product feed setup. The
                                requirements depend on your store location and the type of products.</p>
                            <p>GTIN (A Global Trade Item Number) and MPN (Manufacturer Part Number) are important fields
                                that need to be part of the Product Feed. Our plugin added these fields to the products as a
                                custom attributes along with the plugin installation.</p>
                            <p>You will have to populate the corresponding data to the products that needs to be included in
                                the Product Feed. The Product Feed will be rejected by the Google Merchant Center without
                                these data fields.</p>
                        </div>
                        <div>
                            <p class=" lxrm-bold ">Step 4: Scheduler setup</p>
                            <p>Helps to schedule the automated product feed generation from your store to Google Merchant
                                Center. If this is marked as “Yes”, Automated Feed will be generated on a daily basis as per your
                                WooCommerce Schedulers Configuration.</p>
                            <p>[Please refer below on how to enable Cron’s for your WooCommerce Store. By default Cron’s
                                might have disabled in your store. It is mandatory to enable the Cron’s for this setup to generate
                                feed daily.]</p>
                            <p>Mark this as “No”, if you would like to send adhoc feed.</p>
                            <img src="/resources/images/wooCommerceProductFeed/cron.png" alt=""/> 

                            <p class="top-bottom-margin" >Click Save Feed Button. It will return to feed list page.</p></div>
                        <div>
                            <p class="lxrm-bold">Step 5: Generate Feeds</p>
                            <p>It will navigate to the feedlist page to see the list of feeds created.</p>
                            <p>Click Generate CSV / Generate XML button to generate the CSV/XML Product Feed with the
                                selected filters.</p>
                            <img style="width:100%;" src="/resources/images/wooCommerceProductFeed/generated-feed-files.png" alt=""/> 
                        </div>
                        <p class="top-bottom-margin">Click Edit Feed link to edit the saved feed and update the filters.</p>
                        <p>After updating the filters, click Generate CSV / Generate XML of a particular feed to regenerate the
                            feed with updated filters.</p>
                        <p>After Clicking on Generate CSV / Generate XML button it will navigate to the generated feeds
                            page, click on feed url to download the Feed URL or else use the FEED URL link to add in Googl
                            Merchant Center.</p>
                        <img src="/resources/images/wooCommerceProductFeed/feed-list-generated.png" alt=""/> 
                        <p class="top-bottom-margin">Click on Delete Feed icon to delete the generated feed.</p>
                        <p class=" lxrm-bold">Merchant Center</p>
                        <p>Merchant Center is a tool that helps you upload your store, brand and product data and make it
                            available to Google Shopping and other Google services. To advertise your products on Google,
                            you’ll need a Merchant Center account.</p>
                        <p class="lxrm-bold">How to Create a Merchant Center Account?</p>
                        <p> <a href="https://support.google.com/merchants/answer/188924?hl=en"><span style="text-decoration: underline;">https://support.google.com/merchants/answer/188924?hl=en</span></a></p>
                        <p class="lxrm-bold">How to setup product feed to Merchant Center?</p>
                        <img src="/resources/images/wooCommerceProductFeed/feed-list-generated.png" alt=""/> 
                        <p class="top-bottom-margin" >Upload either the generated CSV or XML URL to merchant center.</p>
                        <div>  
                           
                            <p class="lxrm-bold">How do Scheduler Tasks Work:</p>
                            <p>cron.php is launched regularly from your server side (every 5 minutes most of the time)</p>
                            <p>The cron task for Product Feed is targeted and evaluates if a product feed data needs to be
                                updated based on the scheduler day and timestamp stored in the database and on the schedule
                                table for this specific profile.</p>
                            <p>If the above condition matches, then the product feed data will be re-generated.</p>
                            <p>Be sure that you have correctly configured a scheduled task from the server side (cpanel)
                                targeting the cron.php file in your Woocommerce installation.</p>
                        </div>
                        <img src="/resources/images/wooCommerceProductFeed/2.jpg" alt=""/>          
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@ include file="/WEB-INF/views/askTheExpert/askTheExpert.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>