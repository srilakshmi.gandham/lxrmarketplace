
<footer id="footer">
    <div class="container-fluid" id="footer-start">
        <div class="row ">
            <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12" style="display: flex;align-items: center;margin-top: 1.5rem;font-size: 15px;">
                <p><a href="https://www.netelixir.com/" style="color: black !important;" target="_blank">&copy; 2011&nbsp;-&nbsp;<span id="copyRightsToYear"></span> NetElixir Inc. All Rights Reserved.</a></p>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6" >
                <div id="footer-items" >
                    <ul class="footer-ul lxr-footer-items-ul">
                        <li><a href="/suppcustservice.html"> Contact Us</a></li>
                        <li><a href="https://lxrmarketplace.com/blog/" target="_blank"> BLOG</a></li>
                        <li><a href="/privacy-policy.html" target="_blank"> Privacy Policy</a></li>
                        <li><a href="/terms-of-service.html" target="_blank"> Terms of Service</a></li>
                        <li><a href="/sitemap.html" target="_blank"> SiteMap</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                <ul class="footer-social lxr-social-icons col-centered text-center" >
                    <li>
                        <a href="https://www.facebook.com/Lxrmarketplace-1657035624516281/" target="_blank">
                            <img src="../../resources/images/marketplace-fb.png" alt=""/>
                        </a>
                    </li>
<!--                Removeing Google Plus as consumer version of Google+ is shutting down in Feb 04,2019
                    <li><a href="//plus.google.com/107793863097965760189?prsrc=3" target="_blank">
                            <img src="../../resources/images/marketplace-gmail.png" alt=""/>
                        </a>
                    </li>-->
                    <li><a href="http://twitter.com/LXRMarketplace" target="_blank">
                            <img src="../../resources/images/marketplace-twitter.png" alt=""/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

</div>
</div>
<%@include file="/WEB-INF/views/cookieInfoPopup.jsp" %>

<script>
    $(document).ready(function () {
        var dteNow = new Date();
        var intYear = dteNow.getFullYear();
        $("#copyRightsToYear").html(intYear);
    });
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
