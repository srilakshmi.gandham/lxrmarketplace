<%-- 
    Document   : mostUsedKeywords
    Created on : 14 Oct, 2017, 10:30:28 AM
    Author     : NE16T1213-Eswar
--%>

<%@page import="lxr.marketplace.crawler.LxrmUrlStats,java.util.List,java.util.ArrayList"%>
<%@page import="lxr.marketplace.mostusedkeywords.MostUsedKeyWordsResult"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Most Used Keywords Tool | LXR Marketplace</title>
        <meta name="description"
              content="Try the Free Most Used Keywords Tool at LXRMarketplace. Track & analyze text-to-code ratio, the total number of keywords, and the frequently used keywords!">
        <meta name="keywords"
              content=" Most Used Keywords Checker, Text-To-Code Ratio">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/mostUsedKeywords.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <%
            int toolDBId = 34;
            List<MostUsedKeyWordsResult> mostUsedKeywordsResult = null;
            if (session.getAttribute("mostUsedKeywordResult") != null) {
                mostUsedKeywordsResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
                pageContext.setAttribute("mostUsedKeywords", mostUsedKeywordsResult);
            }
            String queryURL = null;
            if (session.getAttribute("validMostUsedKeywordsUrl") != null) {
                queryURL = (String) session.getAttribute("validMostUsedKeywordsUrl");
                pageContext.setAttribute("validMostUsedKeywordsUrl", queryURL);
            }
            int keywordsLinkDepth = 0;
            if (session.getAttribute("keywordsLinkDepth") != null) {
                keywordsLinkDepth = (int) session.getAttribute("keywordsLinkDepth");
                pageContext.setAttribute("keywordsLinkDepth", keywordsLinkDepth);
            }
            
            String reqParam = request.getParameter("report");
            String downloadReportType = request.getParameter("report-type");
            pageContext.setAttribute("reqParam", reqParam);
            pageContext.setAttribute("downloadReportType", downloadReportType);
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat("${downloadReportType}");
                }
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();
                $("#userDomain").val('');
                $("#editImport").hide();
                $("#userDomain").removeClass("gray");
            <c:if test="${(keywordSuccess eq true || fn:contains(uri, 'backToSuccessPage=')) 
                          && (mostUsedKeywords != null && not empty mostUsedKeywords)}">
                $("#editImport").show();
                $("#userDomain").addClass("gray");
            </c:if>
                $("#userDomain").val("${pageScope.validMostUsedKeywordsUrl}");
                $("#getResult").bind('click', function () {
                    resultLoginValue = true;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    var toolQueryURL = "";
                    if ($('#userDomain').val().trim() === "" || $('#userDomain').val() === null || $('#userDomain').val() === 0 || $('#userDomain').val() === "Enter Website URL") {
                        $("#notWorkingUrl").html("Please enter your URL.");
                        $('#userDomain').focus();
                        return false;
                    } else if (!urlValidation($('#userDomain').val().trim())) {
                        $("#notWorkingUrl").html("Please enter a valid domain URL.");
                        $('#userDomain').focus();
                        return false;
                    }
                    $("#notWorkingUrl").empty();
                    var $this = $(this);
                    $this.button('loading');
                    toolQueryURL = $('#userDomain').val();
                    refreshToolPage();
                    $.ajax({
                        type: "POST",
                        url: "/most-used-keywords-tool.html?requestMethod=getResult",
                        processData: true,
                        data: {"toolQueryURL": toolQueryURL},
                        dataType: "json",
                        success: function (data) {},
                        complete: function (xhr, textstatus) {
                            // xhr.responseText contains the response from the server
                            var allheaders = xhr.getAllResponseHeaders();
                            if (allheaders.indexOf("limitexceed") > 0) {
                                location.reload();
                            }
                        }
                    });
                });
                $('#userDomain').keypress(function (ev) {
                    if (ev.which === 13) {
                        $('#getResult').click();
                        return false;
                    }
                });
                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        $('#sendMail').click();
                        return false;
                    }
                });
                /*Request to sendmail for tool report*/
                $("#sendMail").bind('click', function () {
                    $('#mail-sent-status').empty();
                    var emailid = $.trim($("#email").val());
                    var status = true;
                    var statusMsg = "";
                    if (emailid === "") {
                        statusMsg = "Please enter your email id";
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        statusMsg = "Please enter valid email address.";
                        status = false;
                    }
                    $('#mail-sent-status').show();
                    if (!status) {
                        $("#email").focus();
                        $("#mail-sent-status").html(statusMsg);
                        $('#mail-sent-status').css("color", "red");
                        return false;
                    } else {
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/most-used-keywords-tool.html?download=sendmail&email=" + emailid,
                            processData: true, data: {},
                            dataType: "json",
                            success: function (data) {
                            }, complete: function () {
                                $('#mail-sent-status').empty();
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                            }
                        });
                    }
                });
                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }
                $('#editImport, #userDomain').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#userDomain").removeClass("gray");
                });
            });/*End of on ready*/

            /*To show status of URL's analysed*/
            function refreshToolPage() {
                /*Request to get count of URL's are analyzed.*/
                setTimeout('getAnalyzedCount()', 2000);
            }/*EOF of refreshKeywordsPage*/
            function getAnalyzedCount() {
                $.ajax({
                    type: "POST",
                    url: "/most-used-keywords-tool.html?requestMethod=getAnalyizedURLs",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] === "valid") {
                            var crawlStatus = data[2];
                            if (crawlStatus) {
                                showToolResultPage();
                            } else {
                                $("#analyzingCount").show();
                                $("#analyzingCount").html(data[1] + " URL(s) analyzed.......");
                                refreshToolPage();
                            }
                        } else {
                            inavalidQueryURL();
                        }
                    }
                });
            }
            /*Request to get most used keywords tool result. */
            function showToolResultPage() {
//                alert("showToolResultPage");
                var form = $("#mostUsedKeywords");
                form.attr('action', "/most-used-keywords-tool.html?requestMethod=getToolData");
                form.submit();
            }
            /*Submiting form to get invalid url status. */
            function inavalidQueryURL() {
                var form = $("#mostUsedKeywords");
                form.attr('action', "/most-used-keywords-tool.html?requestMethod=invalidQueryURL");
                form.submit();
            }
            /*Request to get Ask The Expert related questions.*/
            function getToolRelatedIssues() {
                var keywordsAteURL = $('#userDomain').val().trim();
                $.ajax({
                    type: "POST",
                    url: "/most-used-keywords-tool.html?requestMethod=keyordsATE&userATEDomain=" + keywordsAteURL + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null) {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                $('#userDomain').val(website);
                $('#getResult').click();
            }

            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    var form = $("#mostUsedKeywords");
                    form.attr('method', "POST");
                    form.attr('action', "/most-used-keywords-tool.html?download=download&report-type=" + reportType);
                    form.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <div class="container">
            <springForm:form commandName="mostUsedKeywords" method="POST">
                <div class="col-xs-12 mobile-padding-none">
                    <div class="col-xs-12">
                        <blockquote class="blockquote">
                            <p>
                                The Most Used Keywords tool is designed to review your domain's two most important SEO on-page factors. It analyzes the top 100 URLs of the domain provided by the user and gives the text-to-code ratio, the total number of keywords, and the most repeated keywords for each crawled web page. It'll also filter out common stop words (to, the, which, etc.) if they aren't part of any keyword phrase.
                            </p>
                        </blockquote>
                    </div>
                </div>
                <div class="clearfixdiv" style="height: 10px;"></div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-md-9 col-sm-12">
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="userDomain" cssClass="form-control input-lg srch-term"/>
                            <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span id="notWorkingUrl" class="lxr-danger text-left" >${mostKeywordsInvalidUrl}</span>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="getResult"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                    <div class="form-group col-lg-10 col-lg-offset-0 col-sm-offset-1" style="height: 20px;clear: both;max-height: 20px;">
                        <p id="analyzingCount" style="display: none;padding: 1%;text-align: center;color: #2F80ED;"></p>
                    </div>
                </div>
            </springForm:form>
        </div>
        <c:if test="${(keywordSuccess eq true || fn:contains(uri, 'backToSuccessPage='))
                      && (mostUsedKeywords != null && not empty pageScope.mostUsedKeywords)}">
            <c:set var="usedKeywordsSize" value="${pageScope.mostUsedKeywords.size()}"/>
            <div class="container-fluid">
                <c:choose>
                    <c:when test="${pageScope.mostUsedKeywords != null && not empty pageScope.mostUsedKeywords}">
                        <div class="col-lg-12 lxrm-section-heading">
                            <p>
                                <span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                <span class="lxrm-bold">Review of: </span>
                                <span>${pageScope.validMostUsedKeywordsUrl}</span>
                            </p>    
                        </div>
                        <div  class="col-lg-12" style="font-size: 1.2em;padding: 0px;">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                <div class="row col-xs-12 col-sm-6 col-md-12">Link Depth:&nbsp;&nbsp;<span class="lxrm-bold lxrm-number-value">${pageScope.keywordsLinkDepth}</span></div>
                                <div class="row col-xs-12 col-sm-6 col-md-12">Pages Scanned:&nbsp;&nbsp;<span class="lxrm-bold lxrm-number-value">${pageScope.mostUsedKeywords.size()}</div>
                            </div>
                            <div class="visible-xs " style="height: 20px;clear: both"></div>
                            <div class="row col-xs-12 col-sm-6 col-md-4 col-lg-4 text-right">
                                <span class="col-md-10 col-xs-9 lxrm-text-align">Average No.of Keywords:</span><span class="col-md-2 col-xs-2 text-left lxrm-bold lxrm-number-value">${dashBoardView.averageKeywords}</span>
                                <span class="col-md-10 col-xs-9 lxrm-text-align">Min:</span><span class="col-md-2 col-xs-3 lxrm-bold text-left lxrm-number-value">${dashBoardView.minimumKeywords}</span>
                                <span class="col-md-10 col-xs-9 lxrm-text-align">Max:</span><span class="col-md-2 col-xs-3 lxrm-bold text-left lxrm-number-value">${dashBoardView.maximiumKeywords}</span>
                            </div>
                            <div class="visible-xs " style="height: 20px;clear: both"></div>
                            <div class="row col-xs-12 col-sm-6 col-md-4 col-lg-5 text-right">
                                <span class="col-md-10 col-xs-9 col-lg-10 lxrm-text-align">Average Text/Code Ratio:</span><span class="col-md-2 col-xs-2 col-lg-2 text-left lxrm-bold lxrm-number-value">${dashBoardView.averageTextCodeRatio}%</span>
                                <span class="col-md-10 col-xs-9 col-lg-10 lxrm-text-align">Min:</span><span class="col-md-2 col-xs-3 col-lg-2 text-left lxrm-bold lxrm-number-value" >${dashBoardView.minimumTextCodeRatio}%</span>
                                <span class="col-md-10 col-xs-9 col-lg-10 lxrm-text-align">Max:</span><span class="col-md-2 col-xs-3 col-lg-2 text-left lxrm-bold lxrm-number-value">${dashBoardView.maximiumTextCodeRatio}%</span>
                            </div>
                        </div>
                    </c:when>
                    <c:when test="${pageScope.mostUsedKeywords !=null && empty pageScope.mostUsedKeywords}">
                        <div class="col-lg-12">
                            <p>Link Depth:&nbsp;<span class="lxrm-bold">${pageScope.keywordsLinkDepth}</span></p>
                            <p>Pages Scanned:&nbsp;<span class="lxrm-bold">${pageScope.mostUsedKeywords.size()}</span></p>
                        </div>
                    </c:when>
                </c:choose>
                <div class="visible-xs" style="height: 20px;clear: both"></div>
                <c:if test="${pageScope.mostUsedKeywords !=null && empty pageScope.mostUsedKeywords}">
                    <br>
                    <div class="row">
                        <div class="col-lg-12 lxrm-section-heading" style="padding: 0px;">
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    MOST USED KEYWORDS
                                </div>
                            </div>
                        </div>
                        <div class="clearfixdiv" style="height: 25px;"></div>
                        <div class="col-lg-1 visible-md visible-lg"></div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <c:forEach var="domainKeywords" items="${dashBoardView.topKeywordsofDomain}" varStatus="keyword">
                                <c:if test="${dashBoardView.topKeywordsofDomain.size() >=5 && keyword.index <= 4 }">
                                    <div class="col-lg-12 lxrm-keyword-div">
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 lxrm-bold">${domainKeywords.key}</div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right ">${domainKeywords.value}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <c:forEach var="domainKeywords" items="${dashBoardView.topKeywordsofDomain}" varStatus="keyword">
                                <c:if test="${(dashBoardView.topKeywordsofDomain.size() >=6) && (keyword.index >= 5 && keyword.index <= dashBoardView.topKeywordsofDomain.size()) }">
                                    <div class="col-lg-12  lxrm-keyword-div">
                                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 lxrm-bold">${domainKeywords.key}</div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right ">${domainKeywords.value}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="col-lg-1 visible-md visible-lg"></div>
                    </div>
                </c:if>
                <div class="clearfixdiv" style="height: 25px;"></div>

                <div class="row">
                    <div class="col-xs-12 col-lg-12 lxrm-section-heading ">
                        KEYWORD ANALYSIS FOR ${pageScope.mostUsedKeywords.size()} PAGES
                    </div>
                    <br>
                    <div class="col-md-12 col-lg-12" style="overflow: auto;">
                        <table class="table table-striped col-lg-12" id="keywordAnalysis" style="word-break: break-word;">
                            <thead>
                                <tr>
                                    <th class="text-left lxrm-bold col-xs-4">URL</th>
                                    <th class="text-left lxrm-bold col-xs-2">Text/Code Ratio</th>
                                    <th class="text-left lxrm-bold col-xs-2">Total Keywords</th>
                                    <th class="text-left lxrm-bold col-xs-4">Most Used Keywords</th>
                                </tr>
                            </thead>
                            <tbody style="word-break: break-all;">
                                <c:if test="${pageScope.mostUsedKeywords.size() == 0}">
                                    <tr><td colspan="4">No Records Found</td></tr>
                                </c:if>
                                <c:forEach var="keywordsMetric" items="${pageScope.mostUsedKeywords}" varStatus="urlCount">
                                    <c:if test="${keywordsMetric.subDomain !=null && not empty keywordsMetric.subDomain}">
                                        <tr>
                                            <td class="text-left" style="width: 35%;">
                                                <a href="${keywordsMetric.subDomain}" target="_blank">${keywordsMetric.subDomain}</a>
                                            </td>
                                            <td class="text-center" style="width: 15%;">
                                                <c:if test="${keywordsMetric.textCodeRatio !=null}">
                                                    ${keywordsMetric.textCodeRatio}<b>%</b>
                                                </c:if>
                                            </td>
                                            <td class="text-center" style="width: 15%;">
                                                <c:if test="${keywordsMetric.keywordsCount !=null}">
                                                    ${keywordsMetric.keywordsCount}
                                                </c:if>
                                            </td>
                                            <td style="width: 35%;">
                                                <c:if test="${keywordsMetric.topKeyWords !=null}">
                                                    <c:choose>
                                                        <c:when test="${empty keywordsMetric.topKeyWords}">
                                                            <div class="col-lg-12 text-center">No keywords available.</div>
                                                        </c:when>
                                                        <c:otherwise>
                                                        <c:set var="keywordIndex" value="1" />
                                                            <c:forEach var="keywordsPhares" items="${keywordsMetric.topKeyWords}"  varStatus="keyword">
                                                                <c:if test="${keywordsPhares!=null}">
                                                                    <c:if test="${keyword.index %2 == 0}">
                                                                        <div class="col-lg-12" style="padding: 0;">
                                                                    </c:if>
                                                                    <c:choose>
                                                                            <c:when test="${keywordIndex != keywordsMetric.topKeyWords.size()}">
                                                                                ${keywordsPhares},
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                ${keywordsPhares}.
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    <c:if test="${keyword.index %2 == 1}">
                                                                        </div>
                                                                    </c:if>
                                                                    <c:set var="keywordIndex" value="${keywordIndex + 1}"/>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfixdiv"></div>
            <div class="container-fluid">
                <div class="col-xs-12 download-report-row lxrm-padding-none" style="padding:0%;">
                    <div class="col-md-12 col-sm-12 col-xs-12 download-report-div" style="margin:0% !important;">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                            <p style="margin: 3% 0;" class="lxrm-bold">Download complete report here: </p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10" style="padding: 0;">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" style="border: 1px #ccc solid !important;" id="sendMail"><i class="glyphicon glyphicon-send"></i></button>
                                </div>
                            </div>
                            <div id="mail-sent-status" class="emptyData"></div>
                        </div>
                        <div class="col-md-1 col-sm-2 col-xs-2">
                            <div class="dropdown">
                                <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                <div id="downloadAnalysisReport" class="dropdown-content">
                                    <p onclick="downloadFormat('pdf');">PDF</p>
                                    <!--<p onclick="downloadFormat('xls');">XLS</p>-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </c:if>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
