
<%@page import="lxr.marketplace.toprankedwebsitesbykeyword.TopRankedWebsitesbyKeywordTool"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Top Ranked Websites by Keyword Tool | LXR Marketplace</title>
        <meta name="description"
              content="Use LXRMarketplace easy-to-use Top Ranked Websites by Keyword tool to find out the top ranked sites in Google and Bing for any keyword!">
        <meta name="keywords"
              content="Top Ranked Websites by Keyword Tool, Top Ranked Websites by Keyword Analyzer">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/topRankedWebsiteByKeyword.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js" type="text/javascript"></script>
        <%
            int toolDBId = 30;

            if (session.getAttribute("googleResults") != null) {
                pageContext.setAttribute("googleResults", session.getAttribute("googleResults"));
            } else {
                pageContext.setAttribute("googleResults", null);
            }
            if (session.getAttribute("bingResults") != null) {
                pageContext.setAttribute("bingResults", session.getAttribute("bingResults"));
            } else {
                pageContext.setAttribute("bingResults", null);
            }
            TopRankedWebsitesbyKeywordTool topRankedWebsitesbyKeywordTool = null;
            String keyword = "";
            if (session.getAttribute("topRankedWebsitesbyKeywordTool") != null) {
                topRankedWebsitesbyKeywordTool = (TopRankedWebsitesbyKeywordTool) session.getAttribute("topRankedWebsitesbyKeywordTool");
                keyword = topRankedWebsitesbyKeywordTool.getSearchKeyword();
            }
            pageContext.setAttribute("topRankedWebsitesbyKeywordTool", topRankedWebsitesbyKeywordTool);
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            <c:set var="status" value="${status}" />
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report") !== -1) {
                    downloadReport();
                }
//                toolLimitExceeded();
                // onload after result or get back from payment page to tool page
            <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage=')) 
                          && (googleResults != '' && googleResults != null) 
                          && (bingResults != '' && bingResults != null)}">
                $("#editImport").show();
                $("#searchKeyword").addClass("gray");
            </c:if>
                $('#getResult').on('click', function () {
                     resultLoginValue = true;
                    /*   Login POPUP on tool limit exceeded */
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    if ($('#searchKeyword').val().trim() === "" || $('#searchKeyword').val() === null || $('#searchKeyword').val() === 0) {
                        $("#notWorkingUrl").html("Please enter search query.");
                        $('#searchKeyword').focus();
                        return false;
                    }
                    $("#notWorkingUrl").html("");
                    var $this = $(this);
                    $this.button('loading');
//                    document.getElementById("searchKeyword").readOnly = true;
//                    document.getElementById("location").readOnly = true;
//                    document.getElementById("language").readOnly = true;
                    var f = $("#topRankedWebsitesbyKeywordTool");
                    f.attr('action', "top-ranked-websites-bykeyword-tool.html?getResult=getResult");
                    f.submit();
                });

//                $('#edit-textbox').on('click', function () {
////                    $("#getResult").show();
////                    $("#edit-textbox").hide();
////                    $("#analyze-spinner").removeClass("fa-spin");
//                    $("#edit-textbox").css("color", "#E0E0E0");
//                    $(".srch-term").prop("disabled", false);
//                });
                $('#editImport, #searchKeyword').on('click', function () {
                    $("#notWorkingUrl").html("");
                    $("#editImport").hide();
                    $("#searchKeyword").removeClass("gray");
                });
                $('#searchKeyword').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#getResult").click();
                        return false;
                    }
                });

                // Sendmail functionality
                $('#email').keypress(function (ev) {
                    if (ev.which === 13) {
                        sendReportToMail();
                        return false;
                    }
                });
                $('#email').click(function () {
                    $('#mail-sent-status').hide();
                    return false;
                });
            });
            //Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/top-ranked-websites-bykeyword-tool.html?ate='topRankedKeyWordATE'",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            // send mail and download report to user
            function downloadReport() {
                if (userLoggedIn) {
                    var f = $("#topRankedWebsitesbyKeywordTool");
                    f.attr('method', "POST");
                    f.attr('action', "top-ranked-websites-bykeyword-tool.html?download=download");
                    f.submit();
                } else {
                    reportDownload = 1;
//                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }

            function sendReportToMail() {
                var emailid = $.trim($("#email").val());
                $('#mail-sent-status').hide();
                if (!(emailid)) {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("Please provide email id");
                    $('#mail-sent-status').css("color", "red");
                    $("#email").focus();
                    return false;
                } else if (!isValidMail(emailid)) {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("Please enter a valid email address");
                    $('#mail-sent-status').css("color", "red");
                    $("#email").focus();
                    return false;
                } else {
                    $('#mail-sent-status').show();
                    $('#mail-sent-status').html("sending...");
                    $('#mail-sent-status').css("color", "green");
                    $.ajax({
                        type: "POST",
                        url: "/top-ranked-websites-bykeyword-tool.html?download=sendmail&email=" + emailid,
                        processData: true, data: {},
                        dataType: "json",
                        success: function (data) {
                        },
                        complete: function () {
                            $('#mail-sent-status').show();
                            $('#mail-sent-status').html("Mail sent successfully");
                            $('#mail-sent-status').css("color", "green");
                        }
                    });
                }
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/top-ranked-websites-bykeyword-tool.html?ate='topRankedKeyWordATE'",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                var userToolInputs = JSON.parse(userInput);
                $('#searchKeyword').val(userToolInputs["2"]);
                $('#location').val(userToolInputs["3"]);
                $('#language').val(userToolInputs["4"]);
                $('#getResult').click();
            }
            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <!--Head Start-->
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="row">
                <springForm:form commandName="topRankedWebsitesbyKeywordTool" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group col-lg-4 ">
                            <div class="icon-addon addon-lg">
                                <springForm:input path="searchKeyword" cssClass="form-control input-lg" placeholder="Enter a search query" />
                                <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                            </div>
                            <span id="notWorkingUrl" class="lxr-danger text-left"></span>
                        </div>
                        <div class="form-group col-lg-3">
                            <div class="icon-addon addon-lg">
                                <springForm:select path="location" cssClass="form-control selectpicker" style="height: 47px;">
                                    <springForm:options items="${geoLocation}" />
                                </springForm:select>
                            </div>
                        </div>
                        <div class="form-group col-lg-3 ">
                            <div class="icon-addon addon-lg">
                                <springForm:select path="language" cssClass="form-control selectpicker" style="height: 47px;border-radius: 4px;">
                                    <springForm:options items="${loadLanguages}" />
                                </springForm:select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn" >
                                <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12" id="getResult"  data-loading-text="<i class='fa fa-refresh fa-spin'></i> ANALYZING">
                                    ANALYZE
                                </button>
                            </div>
                        </div>
                    </div>
                </springForm:form>
            </div>
        </div>

        <div class="clearfixdiv"></div>
        <!--   Success Page -->
        <c:if test="${(status.equals('success') || fn:contains(uri, 'backToSuccessPage='))
                      && (googleResults != '' && googleResults != null) 
                      && (bingResults != '' && bingResults != null)}">
              <!--vertical tab system-->
              <div class="container-fluid">
                  <div class="row lxrtool-tabs">
                      <div class="tabs">
                          <!--1-->
                          <div class="tab">
                              <button class="tab-toggle active lxrtool-tabs-heading">
                                  <span>Google</span>
                                  <p>It gives you the top 10 ranked websites for your search query in Google.</p>
                              </button>
                          </div>
                          <!--table-->
                          <div class="content active">
                              <!--<p style="padding: 10px 0px;"><span id="issue-text-1">These links are dead and need to be handeld before they do damage.</span> You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS.</strong></p>-->
                              <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <c:if test="${fn:length(googleResults) gt 0}">
                                              <span class="panel-title">Top 10 Ranked Websites in Google</span>
                                          </c:if>
                                          <!--<p id="issue-text-1">These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">STEPS.</strong></p>-->
                                      </div>
                                  </div>
                                  <div class="panel-body lxr-panel-body">
                                      <div id="result-content-1">
                                          <table class="table table-striped lxrtool-table">
                                              <thead>
                                                  <tr>
                                                      <th style="width: 30%;">Domain</th>
                                                      <th style="width: 30%;">URL</th>
                                                      <th style="width: 15%;">URL Backlink Count</th>
                                                      <th style="width: 15%;">Domain Backlink Count</th>
                                                      <th style="width: 10%;">Rank</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <c:choose>
                                                      <c:when test="${fn:length(googleResults) gt 0}">
                                                          <c:forEach items="${googleResults}" var="keywordResults">
                                                              <tr>
                                                                  <td>${keywordResults.domainName}</td>
                                                                  <td><a href="${keywordResults.resultURL}" target="_blank">${keywordResults.resultURL}</a></td>
                                                                  <td class="text-right">${keywordResults.urlBacklinkCount}</td>
                                                                  <td class="text-right">${keywordResults.domainBacklinkCount}</td>
                                                                  <td class="text-right">${keywordResults.position}</td>
                                                              </tr>
                                                          </c:forEach>
                                                      </c:when>
                                                      <c:otherwise>
                                                          <tr>
                                                              <td colspan="5" style="text-align: center;">No results found</td>
                                                          </tr>
                                                      </c:otherwise>
                                                  </c:choose>
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                              <!--</div>-->
                          </div>

                          <!--2-->
                          <div class="tab">
                              <button class="tab-toggle lxrtool-tabs-heading">
                                  <span>Bing</span>
                                  <p>It gives you the top 10 ranked websites for your search query in Bing.</p>
                              </button>
                          </div>
                          <div class="content">
                              <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2">
                                  <div class="panel panel-default">
                                      <div class="panel-heading lxr-panel-heading">
                                          <c:if test="${fn:length(bingResults) gt 0}">
                                              <span class="panel-title">Top 10 Ranked Websites in Bing</span>
                                          </c:if>
                                          <!--<p id="issue-text-1">2-These links are dead and need to be handled before they do damage. You can fix this by following these <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(2);">STEPS.</strong></p>-->
                                      </div>
                                  </div>
                                  <div class="panel-body lxr-panel-body">
                                      <div id="result-content-1">
                                          <table class="table table-striped lxrtool-table">
                                              <thead>
                                                  <tr>
                                                      <th style="width: 30%;">Domain</th>
                                                      <th style="width: 30%;">URL</th>
                                                      <th style="width: 15%;">URL Backlink Count</th>
                                                      <th style="width: 15%;">Domain Backlink Count</th>
                                                      <th style="width: 10%;">Rank</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <c:choose>
                                                      <c:when test="${fn:length(bingResults) gt 0}">
                                                          <c:forEach items="${bingResults}" var="keywordResults">
                                                              <tr>
                                                                  <td>${keywordResults.domainName}</td>
                                                                  <td><a href="${keywordResults.resultURL}" target="_blank">${keywordResults.resultURL}</a></td>
                                                                  <td class="text-right">${keywordResults.urlBacklinkCount}</td>
                                                                  <td class="text-right">${keywordResults.domainBacklinkCount}</td>
                                                                  <td class="text-right">${keywordResults.position}</td>
                                                              </tr>
                                                          </c:forEach>
                                                      </c:when>
                                                      <c:otherwise>
                                                          <tr>
                                                              <td colspan="5" style="text-align: center;">No results found</td>
                                                          </tr>
                                                      </c:otherwise>
                                                  </c:choose>
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                              <!--</div>-->
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row download-report-row">
                      <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                      <div class="col-md-9 col-sm-12 col-xs-12 download-report-div">
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                              <p style="margin: 3% 0;">Download complete report here: </p>
                          </div>
                          <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                              <!--<div class="col-md-6 col-sm-10 col-xs-10">-->
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                  <div class="input-group-btn">
                                      <button class="btn btn-default" style="border: 1px #ccc solid !important;" onclick="sendReportToMail();"><i class="glyphicon glyphicon-send"></i></button>
                                  </div>
                              </div>
                              <div id="mail-sent-status" class="emptyData"></div>
                          </div>
                          <div class="col-md-1 col-sm-2 col-xs-2">
                              <div class="dropdown">
                                  <span id="downloadReport" class="glyphicon glyphicon-download-alt"  onclick="downloadReport();"></span>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
        </c:if>

        <div class="clearfixdiv"></div>
        <!--Recommended part start-->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <!--Recommended part end-->
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>