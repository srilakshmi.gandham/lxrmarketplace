<%-- 
    Document   : emailVerification
    Created on : 23 Dec, 2019, 7:32:42 PM
    Author     : vidyasagar.korada
--%>
<!DOCTYPE html>
<%@ page import="lxr.marketplace.user.Signup"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Email Verification  | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/signup.css">
        <script type="text/javascript">
            $(document).ready(function () {
            <c:if test="${not empty veificationEror}">
                $("#messageToUser").show();
                $("#messageToUser").empty();
                $("#messageToUser").removeClass('lxr-success');
                $("#messageToUser").addClass('lxr-danger');
                $("#messageToUser").html("${veificationEror}");
            </c:if>
                $("#submitVerificationCode").bind('click', function () {
                    if (validateForm("submit")) {
                        var $this = $(this);
                        $this.button('loading');
                        var form = $("#emailVerificationForm");
                        form.attr('method', "POST");
                        form.attr('action', "validate-verification-code.html");
                        form.submit();
                    }
                });
                $("#resendVerificationCode").bind('click', function () {
                    if (validateForm("resend")) {
                        var $this = $(this);
                        $this.button('loading');
                        generateEmailVerifyCode();
                    }
                });
                $("#backToSignUp").bind('click', function () {
                    var $this = $(this);
                    $this.button('loading');
                    var form = $("#emailVerificationForm");
                    form.attr('method', "POST");
                    form.attr('action', "back-to-signup.html");
                    form.submit();
                });
            });
            function validateForm(type) {
                var userName = $("#userNameToBeVerified").val().trim();
                var userEmail = $("#userEmailToBeVerified").val().trim();
                var userVerificationCode = $("#emailValidationCode").val();
                var status = true;
                if (userName === "") {
                    $("input[id='userNameToBeVerified']").addClass("error-input-field");
                    $("#userNameError").empty();
                    $("#userNameError").html("Please enter name.");
                    $("#userNameError").show();
                    status = false;
                } else {
                    $("#userNameError").empty();
                    $("#userNameError").hide();
                    $("input[id='userNameToBeVerified']").removeClass("error-input-field");
                }
                if (userEmail === "") {
                    $("input[id='userEmailToBeVerified']").addClass("error-input-field");
                    $("#userEmailError").empty();
                    $("#userEmailError").html("Please enter email address.");
                    $("#userEmailError").show();
                    status = false;
                } else if (!isValidMail(userEmail)) {
                    $("input[id='userEmailToBeVerified']").addClass("error-input-field");
                    $("#userEmailError").empty();
                    $("#userEmailError").html("Please enter a valid email address.");
                    $("#userEmailError").show();
                    status = false;
                } else {
                    $("#userEmailError").hide();
                    $("input[id='userEmailToBeVerified']").removeClass("error-input-field");
                }
                if (type === "submit") {
                    if (userVerificationCode === "") {
                        $("input[id='emailValidationCode']").addClass("error-input-field");
                        $("#verificationCodeError").empty();
                        $("#verificationCodeError").html("Please enter verification code.");
                        $("#verificationCodeError").show();
                        status = false;
                    } else if (userVerificationCode.length !== 6) {
                        $("input[id='emailValidationCode']").addClass("error-input-field");
                        $("#verificationCodeError").empty();
                        $("#verificationCodeError").html("Please enter a valid verification code.");
                        $("#verificationCodeError").show();
                        status = false;
                    } else {
                        $("#verificationCodeError").hide();
                        $("input[id='emailValidationCode']").removeClass("error-input-field");
                    }
                } else {
                    $("#verificationCodeError").hide();
                    $("input[id='emailValidationCode']").removeClass("error-input-field");
                }
                return status;
            }

            function generateEmailVerifyCode() {
                var emailToBeVerify = $("#userEmailToBeVerified").val().trim();
                var userName = $("#userNameToBeVerified").val().trim();
                console.log("IN generateEmailVerifyCode " + emailToBeVerify);
                var emailRequestForm = getEmailValidationForm(userName, emailToBeVerify);
                /*Call to service to get code*/
                $.ajax({
                    type: "POST",
                    url: '/generate-verification-code.html?request=generateCode',
                    data: emailRequestForm,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (response) {
                        console.log("For Code generation and response data:: " + JSON.stringify(response));
                        if (response[0] !== null) {
                            $("#messageToUser").show();
                            $("#messageToUser").empty();
                            if (response[0]) {
                                $("#messageToUser").removeClass('lxr-danger');
                                $("#messageToUser").addClass('lxr-success');
                            }else{
                                $("#messageToUser").removeClass('lxr-success');
                                $("#messageToUser").addClass('lxr-danger');
                            }
                            if (response[1] !== null) {
                                $("#messageToUser").html(response[1]);
                            }
                        } else {
                            $("#messageToUser").show();
                            $("#messageToUser").empty();
                            $("#messageToUser").addClass('lxr-danger');
                            $("#messageToUser").html("Failed to send new verification code to your email address");
                        }
                        $("#resendVerificationCode").button('reset');
                    }, error: function (e) {
                        $("#resendVerificationCode").button('reset');
                        console.log("Error in generateEmailVerifyCode");
                        $("#messageToUser").show();
                        $("#messageToUser").empty();
                        $("#messageToUser").addClass('lxr-danger');
                        $("#messageToUser").html("Failed to send new verification code to your email address");
                    },
                });
            }
            function getEmailValidationForm(userName, userEmail) {
                var emailValidateForm = new FormData();
                emailValidateForm.append("userName", userName);
                emailValidateForm.append("userEmail", userEmail);
                console.log("getEmailValidationForm " + emailValidateForm);
                return emailValidateForm;
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <!--Section #1-->
                <div class="col-xs-12 col-md-5">
                    <div class="row">
                        <!--LXR logo & Improve your website with our helpful free tools-->
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 lxrformlogo">
                                    <a href="/"><img  src="../../resources/images/lxr-logo.png" alt="LXRMarketplace"></a> 
                                </div>
                                <div class="col-xs-12 signUpTitle">
                                    <h3 class="lxrm-bold">Enter the code sent to your email address</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-lg-9" style="text-align: center">
                                    <img  src="/resources/images/emailVerification.png" alt="LXRMarketplace Email Verification"   >
                                </div>
                                <div class="col-xs-12 col-lg-9">
                                    <form class="" id="emailVerificationForm" method="POST">
                                        <div class="form-group">
                                            <input id="userNameToBeVerified" name="userNameToBeVerified" placeholder="Name" type="text" class="form-control input-without-boxshadow" value="${userNameToBeVerified}" maxlength="100" readonly style="cursor:none;">
                                            <label for="userNameToBeVerified" class="form-control-placeholder">Name</label>
                                            <div id="userNameError" class="lxr-danger" style="display: none;"></div>      
                                        </div>
                                        <div class="form-group">
                                            <input id="userEmailToBeVerified" name="userEmailToBeVerified" placeholder="Email Address" type="text" class="form-control input-without-boxshadow" value="${userEmailToBeVerified}" maxlength="100" readonly style="cursor:none;">
                                            <label for="userEmailToBeVerified" class="form-control-placeholder">Email Address</label>
                                            <div id="userEmailError" class="lxr-danger" style="display: none;"></div>      
                                        </div>
                                        <div class="form-group">
                                            <input id="emailValidationCode" name="emailValidationCode" placeholder="6 digit verification code" type="text" class="form-control input-without-boxshadow" value="" maxlength="100">
                                            <label for="emailValidationCode" class="form-control-placeholder">6 digit verification code</label>
                                            <div id="verificationCodeError" class="lxr-danger" style="display: none;"></div>      
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" class="btn top-line buttonStyles lxrm-bold verificationBtn" id="backToSignUp" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> Processing..">Back</button>
                                            <button type="button" class="btn top-line buttonStyles lxrm-bold verificationBtn" id="submitVerificationCode" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> Validating..">Submit</button>
                                            <button type="button" class="btn top-line buttonStyles lxrm-bold verificationBtn" id="resendVerificationCode" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> Sending..">Resend</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-12 col-lg-9 lxrm-bold" id="messageToUser" style="padding: 3%;"></div>
                                <div class="col-xs-12 col-lg-9 lxrm-bold">
                                    <br>
                                    <p class="text-center">After verifying the code, Your LXRMarketplace account will be created successfully</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- EOF Section #1-->
                </div>
                <!--Section #2 Page image-->
                <div class="col-md-7 visible-md visible-lg">
                    <br>
                    <br>
                    <div class="">
                        <img  src="../../resources/images/lxrmSignUpLogo.png" alt="LXRMarketplace" width="100%"  >
                    </div>
                    <!--EOF Section #2 Page image--> 
                </div>
            </div>            
            <!--EOF container-fluid--> 
        </div>
    </body>
</html>