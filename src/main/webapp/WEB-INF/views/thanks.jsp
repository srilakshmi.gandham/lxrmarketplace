
<%-- 
    Document   : signup
    Created on : Aug 1, 2017, 11:24:49 AM
    Author     : Vemanna
--%>

<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Thank you | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="../../resources/css/signup.css">
        <%
            String successpage = (String) session.getAttribute("successpage");
            session.removeAttribute("successpage");
            if (successpage != null) {
                pageContext.setAttribute("successpage", successpage);
            } else {
                pageContext.setAttribute("successpage", "/lxrmarketplace.html");
            }
            if (session.getAttribute("sendMailId") != null) {
                pageContext.setAttribute("email", (String) session.getAttribute("sendMailId"));
            }
        %>
        <meta http-equiv="refresh" content="30;url=${successpage}">

        <script>
            $(document).ready(function () {
                $('#loginPage').hide();
                var redirect = document.getElementById("redirect");
                redirect.href = "${successpage}";

            });
            function updateTask() {
                setTimeout('updateTime()', 1000);
            }
            function updateTime() {
                var sec = document.getElementById("seconds").innerHTML;
                var diff = sec - 1;
                if (diff > 0) {
                    document.getElementById("seconds").innerHTML = diff;
                }
                updateTask();
            }
        </script>
    </head>
    <body onload="updateTask()">
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="row main">
                <div class="main-login main-center">

                    <form method="POST" action="/help-us.html" >
                        <div class="row setup-content" id="step-2">
                            <div class="col-xs-12">
                                <div class="col-md-11 col-md-offset-1 lxr-thankyoupage">
                                    <div class="row lxrformlogo">
                                        <a href="/"><img  src="../../resources/images/lxr-logo.png" alt="Web site logo"></a> 
                                    </div>
                                    <span id="thnks">Thank You for signing up!</span>
                                    <div id="signupComplete">
                                        <p class="Regular color2">
                                            <i class="fa fa-check-square-o" aria-hidden="true" style="color: green; font-size: 24px; margin-right: 6px"></i>
                                            Your Login Credentials have been mailed to 
                                            <span class="Bold">${email}</span>
                                        </p>
                                        <hr>
                                        <p class="notepara Regular color2">
                                            <span class="Bold">Note:</span> In most of the cases you should receive the
                                            verification mail within minutes, if not <span class="Bold">please check your
                                                Spam/Junk/Bulk Mail </span> folders for the mail. In some cases depending
                                            on your email system it could take longer, however if you don't get
                                            an email from us within half an hour, please write to us at <span class="Bold">support@lxrmarketplace.com</span>
                                            <br>
                                            <br>
                                        <p class="italicpara Regular color2">
                                            You will be redirected to lxrmarketplace.com in 
                                            <span id="seconds" class="Bold">30 </span> seconds or you can 
                                            <a href="/" id="redirect" style="text-decoration: none">
                                                <span class="Bold">Click here</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
