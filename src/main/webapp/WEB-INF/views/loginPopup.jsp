
<script type="text/javascript">
    $(document).ready(function () {
        $("#pop-username").keypress(function (e) {
            if (e.keyCode === 13) {
                $("#pop-password").focus();
            }
        });
        $("#pop-password").keypress(function (e) {
            if (e.keyCode === 13) {
                $("#login").click();
            }
        });
        $('#loginPopUPRegisterHere').click(function () {
            signupPopUp('emailSignUP');
        });
    });
    function popupLogin() {
        var username = $.trim($("#pop-username").val());
        var password = $.trim($("#pop-password").val());
        var isLoginRemember = "false";
        if ($("#isLoginRemember").prop("checked") === true) {
            isLoginRemember = "true";
        }
        $('#login-err-msg').hide();
        if (username === "" || username === "E Mail") {
            $('#login-err-msg').html("Please enter your email address");
            $('#login-err-msg').show();
            $("#pop-username").focus();
            return false;
        } else {
            if (!isValidMail(username)) {
                $('#login-err-msg').html("Please enter a valid email address.");
                $('#login-err-msg').show();
                $("#pop-username").focus();
                return false;
            }
        }
        if (!$("#pop-password").val()) {
            $('#login-err-msg').html("Please enter your password");
            $('#login-err-msg').show();
            $("#pop-password").focus();
            return false;
        }
        /*Decode the special characters in password field.*/
        password = encodeURIComponent(password);
        login(username, password, isLoginRemember, true);
    }
</script>
<style>
    .btn-google {
        color: #fff;
        background-color: #5087EC;
        border-color: #5087EC;
        font-size: 16px
    }
    .btn-facebook {
        color: #fff;
        background-color: #3b5998;
        border-color: rgba(0,0,0,0.2);
        padding-left: 23% !important;
        font-size: 16px
    }
    .btn-social {
        position: relative;
        padding-left: 44px;
        white-space: nowrap;
        overflow: hidden;
        border-radius: 0px;
        width: 100%;
        font-size: 16px;
        height: 47px;
        padding-top: 14px;
        font-weight: bold;
    }
    .btn-social:hover {
        color: #eee;
    }
    .btn-social :first-child {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        width: 45px;
        padding: 12px;
        font-size: 1.6em;
        text-align: center;
        background-color: white;
        font-weight:bold;
        color: #3b5998;
    }
</style>

<div id="loginPopupModal" class="modal fade">
    <div id="loginPopupModalChild" class="modal-dialog modal-sm" style="width: 340px;margin: 0 auto;" >
        <div id="loginPopupModalGrandChild" class="modal-content" style="top: 20px;padding: 1rem;border-radius: 3px;">
            <div class="modal-header" style="border-bottom: none;">
                <!--<span> <img  src="../../resources/images/lxr-logo.png" alt="LXRMarketplace"></span>-->
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <p class="lxrm-bold" style="font-size: 20px">Login to continue</p>
                <p style="font-size: 12px">Don't have an account?
                    <!--<a href="/signup.html" class="lxrm-bold">Register here.</a>-->
                    <span class="lxrm-bold" style="color: #23527c;cursor: pointer;" id="loginPopUPRegisterHere" >Register here.</span>
                </p>
            </div>
            <div class="modal-body" style="padding-top:3%;padding-bottom:1%;">
                <div class="social lxrm-bold">
                    <a class="btn btn-lg btn-social btn-facebook" style=";" onclick="navigateFaceBookLoginScreen();">
                        <i class="fa fa-facebook fa-fw"></i> Continue with Facebook
                    </a>
                </div>
                <br>
                <div class="social lxrm-bold" style=" margin-bottom: 15px;">
                    <a class="btn btn-lg btn-social btn-google" style="font-size: 16px;" onclick="navigateGoogleLoginScreen();">
                        <img  src="/resources/images/google_signup.png" >Continue with Google
                    </a>
                </div> 
                <div class="form-group text-center">
                    <p class="lxrm-bold"> (OR) </p>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="pop-username">Email address</label>
                    <input type="email" class="form-control" id="pop-username" name="email" placeholder="Email" required 
                           style="width:100% !important;height: 47px;border-radius: 0px;">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="pop-password">Password</label>
                    <input type="password" class="form-control" id="pop-password" name="pwd" placeholder="Password" required 
                           style="width:100% !important;height: 47px;border-radius: 0px;">
                </div>
                <div class="form-group">
                    <button type="button" onclick="popupLogin();" id="login" class="btn btn-block lxr-subscribe" 
                            style="height: 47px;border-radius: 0px;margin-top: 13px;font-weight: bold">Sign in</button>
                </div>
                <div style="padding:0px">
                    <p id="login-err-msg" style="display: none;color: red;"></p>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="isLoginRemember" checked> Remember me
                    </label>
                    <label class="pull-right">
                        <a href="/forgot-password.html">Forgot Password?</a>
                    </label>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <span style="color: black;font-size: 12px;"> By signing in, I agree to the 
                        <a href="/privacy-policy.html" target="_blank" style="cursor:pointer">Privacy Policy</a> and  
                        <a href="/terms-of-service.html" target="_blank" style="cursor:pointer">Terms of Service</a>.
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
