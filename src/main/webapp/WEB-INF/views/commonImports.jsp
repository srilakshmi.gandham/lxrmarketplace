
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="hJPH7XX6JUMR5V_sGmO8u72_sK8rjsgeVdVUf7dM5RQ" />
<meta name="msvalidate.01" content="833D56911F5E7DD281053D9699EE959D" />

<link rel="stylesheet" type="text/css" href="/resources/css/styles.css">
<link rel="stylesheet" type="text/css" href="/resources/css/simple-sidebar.css">
<!--<link rel="stylesheet" href="/resources/fonts/FontAwesome.otf">-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!--jQuery File-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--CORE JS-->
<script src="/resources/js/core.js"></script>
<!--Bootstrap Files-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<!--Tag lib for Spring and JSTL-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript" src="/scripts/ui/jquery.raty.min.js"></script>
<script type="text/javascript" src="/scripts/ui/jquery.raty.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="/scripts/Charts.js"></script>

<!-- GOOGLE Tag Manager -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-PM76C6"
        height="0" width="0" style="display: none; visibility: hidden"></iframe>
</noscript>

<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PM76C6');</script>

<!--Added on  July 3rd, 2017 Crazy Egg tracking script suggested by product manager.-->
<script type="text/javascript">
    setTimeout(function () {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0060/1444.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true;
        a.type = "text/javascript";
        b.parentNode.insertBefore(a, b)
    }, 1);
</script>

<meta property="og:image" content="https://lxrmarketplace.com/resources/images/lxr-logo.png" />