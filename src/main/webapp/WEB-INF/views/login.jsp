
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
    var isLoggedInUserId = 0;
    var isUserLogin = ${userLoggedIn};
    var uname = "";
    var pwd = "";
    // is page refresh is true i.e to refresh the page after auto login because for this tool user have historic data
    var isPageRefresh = false;

    $(document).ready(function () {
    var intials = $('#firstname').text().charAt(0).toUpperCase();
    var profileImage = $('#profileImage').text(intials);
    
        $("#emailLoginSwitchOption").click(function () {
            $('#loginSocialSection').hide();
            $('#loginEmailSection').show();
        });
        $("#socialLoginSwitchOption").click(function () {
            $('#loginEmailSection').hide();
            $('#loginSocialSection').show();
        });
        setHeaderMenu(isUserLogin);
    <c:if test="${(userH != null || userH != '') && userH.getId() > 0}">
        isLoggedInUserId = ${userH.getId()};
        uname = "${userH.getUserName()}";
        pwd = "${userH.getPassword()}";
    </c:if>
        if (!isUserLogin && isLoggedInUserId > 0)
        {
            if (checkDefaultLoginCookie()) {
                defaultLoginCheck();
            } else {
                toolLimitExceeded();
            }
        } else if (isLoggedInUserId == 0 && !isUserLogin && parentUrl.indexOf("limit-exceeded") !== -1) {
            toolLimitExceeded();
        }
        /*if user comes from mail link we should get the user analysis data of that link.
         if registered user then we should do auto login and then process that data
         otherwise simply process that data*/
        var numberOfParameters = (parentUrl.match(/&/g) || []).length;
        if ((parentUrl.indexOf("user-id") !== -1 && parentUrl.indexOf("tracking-id") !== -1
                && numberOfParameters < 2 && parentUrl.indexOf("/#home") === -1)
                || (parentUrl.indexOf("question-id") !== -1 && parentUrl.indexOf("#tool") !== -1)) {
            var userId = getUrlVars()["user-id"];
            var websiteTrackingId = getUrlVars()["tracking-id"];
            var questionId = getUrlVars()["question-id"];
            var urlRequest = "";
            if (parentUrl.indexOf("question-id") !== -1) {
                urlRequest = "/user-analysis-data.html?question-id=" + questionId;
            } else {
                urlRequest = "/user-analysis-data.html?user-id=" + userId + "&tracking-id=" + websiteTrackingId;
            }
            $.ajax({
                type: "GET",
                url: urlRequest,
                processData: true,
                data: {},
                dataType: "json",
                success: function (data) {
                    if (data !== null) {
                        var website = data[1];
                        var userInput = data[2];
                        var userName = data[3];
                        var password = data[4];
                        var name = data[5];
                        //  For auto login
                        if (!isUserLogin && password !== "" && password !== null) {
                            $.ajax({
                                type: "POST",
                                url: "/login.html?popuplogin='popuplogin'&username=" + userName + "&password=" + password,
                                processData: true,
                                data: {},
                                dataType: "json",
                                success: function (data) {
                                    setHeaderMenu(true);
                                    getUserToolInputs(website, userInput);
                                }
                            });
                        } else {
                            getUserToolInputs(website, userInput);
                        }
                    } else {
                        alert("Not found any user with this id. Please try again.");
                    }
                }
            });
        }

        $('#lxrmUserLogin').click(function () {
            loginSubmit();
        });
        $('#loginRegisterHere').click(function () {
                signupPopUp('emailSignUP');
        });

        //After entering password and clicking enter button, this invokes
        $('#loginHeaderPassword').keypress(function (ev) {
            if (ev.which === 13) {
                loginSubmit();
                return false;
            }
        });
        //  For closing the login popup div if user clicks out side of the login popup
        $(document).click(function (e) {
            // check that your clicked
            // element has no id=userLogin and is not child of userLogin
            if (e.target.id !== 'userLogin' && !$('#userLogin').find(e.target).length) {
                $("#userLogin").hide();
            }
        });
    });

    function toolLimitExceeded() {
        var status = true;
        if (checkDefaultLoginCookie() && (sessionTimeOutVariable || !userLoggedIn)) {
            login(uname, pwd, true, true);
            status = false;
        } else if ((parentUrl.indexOf("limit-exceeded") !== -1 && !userLoggedIn) || sessionTimeOutVariable) {
            $("#loginPopupModal").modal('show');
            status = false;
        }
        return status;
    }

    function checkDefaultLoginCookie() {
        var defaultLoginStatus = false;
        var username = getCookie("isLoggedIn");
        if (username !== "") {
            // alert("Welcome again " + username);
            defaultLoginStatus = true;
        }
        return defaultLoginStatus;
    }

    function defaultLoginCheck() {
        $.ajax({
            type: "POST",
            url: "/default-login.html?userId=" + isLoggedInUserId,
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                if (data[0] === "success")
                {
                    defaultLogin();
                } else {
                    toolLimitExceeded();
                }
            }
        });
    }

    function defaultLogin() {
//        pwd = encodeURIComponent(pwd);
        if (isPageRefresh) {
            login(uname, pwd, true, true);
        } else {
            $.ajax({
                type: "POST",
                url: "/login.html?popuplogin='popuplogin'&username=" + uname + "&password=" + pwd + "",
                processData: true,
                data: {},
                dataType: "json",
                success: function (data) {
                    userLoggedIn = true;
                    setHeaderMenu(true);
                    getNotificationCount();
//                loadingAutologin(false);
                }
            });
        }
    }

    function loginSubmit() {
        $(".input-errors").hide();
        $(".input-errors").html("");
        var username = $.trim($("#loginEmailAddress").val());
        var password = $.trim($("#loginHeaderPassword").val());
        console.log("username::" + username + ", password::" + password + ",  passid:: " + ($("#loginHeaderPassword").val()));
        var isLoginRemember = "false";
        if ($("#isLoginRememberHeader").prop("checked") === true) {
            isLoginRemember = "true";
        }
//        password = encodeURIComponent(password);
        if (username === "" || username === "E Mail") {
            $(".input-errors").show();
            $(".input-errors").html("Please enter your email address.");
            $("#username").focus();
            return false;
        } else {
            if (!isValidMail(username)) {
                $(".input-errors").show();
                $(".input-errors").html("Please enter a valid email address.");
                $("#username").focus();
                return false;
            }
        }
        if (!$("#loginHeaderPassword").val()) {
            $(".input-errors").show();
            $(".input-errors").html("Please enter your password.");
            $("#loginHeaderPassword").focus();
            return false;
        }
        //loginFromHeader(username, password, isLoginRemember);
        login(username, password, isLoginRemember, false);
    }
    function setHeaderMenu(isUserLogin) {
        /* Hide and shows menu items based on used logged in and the role of the user */
        if (isUserLogin) {
            $(".user-signout-section").show();
            $("#m-user-signout-section").show();
            $("#user-login-section").hide();
            $("#m-user-login-section").hide();
        } else {
            $(".user-signout-section").hide();
            $("#m-user-signout-section").hide();
            $("#user-login-section").show();
            $("#m-user-login-section").show();
        }
    }
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }
    //Recent Questions Notifications
    function getNotificationCount() {
        var params = {userId: isLoggedInUserId};
        jQuery.getJSON("/user-messages.html?check = unViewdAnswers", params,
                function (data) {
                    showNotification(data[0], data[1]);
                });
    }
    function showNotification(totalAnswersCount, totalUnViewedAnswersCount) {
        if (totalAnswersCount > 0) {
            $("#user-messages").show();
            $("#m-user-messages").show();
            if (totalUnViewedAnswersCount > 0) {
                $("#msg-notification-count").html(totalUnViewedAnswersCount);
                $("#m-msg-notification-count").html(totalUnViewedAnswersCount);
            } else {
                $("#msg-notification-count").hide();
                $("#m-msg-notification-count").hide();
            }
        }
    }

    function loadingAutologin(status) {
        if (status) {
            $("#loadingAutologin").show();
            $("body").css("overflow-y", "hidden");
        } else {
            $("#loadingAutologin").hide();
            $("body").css("overflow-y", "auto");
        }
    }
    $('#admin-menu').click(function () {
        var role = "${role}";
        parentUrl = window.location.protocol + "//" + window.location.host;
        if (role === 'reviewer' || role === 'expert') {
            parent.window.location = parentUrl + "/ate-question.html";
        } else {
            parent.window.location = parentUrl + "/dashboard.html";
        }
    });
</script>
<style>
    #profileImage {
        width: 50px;
        height: 50px;
        border-radius: 50%;
        background: #F58120;
        font-size: 2em;
        color: #fff;
        text-align: center;
        line-height: 54px;
}
    .btn-google {
        color: #fff;
        background-color: #5087EC;
        border-color: #5087EC;
    }
    .btn-facebook {
        color: #fff;
        background-color: #3b5998;
        border-color: rgba(0,0,0,0.2);
    }
    .btn-social {
        position: relative;
        padding-left: 44px;
        white-space: nowrap;
        overflow: hidden;
        border-radius: 0px;
        width: 90%;
        font-size: 16px;
        height: 47px;
        padding-top: 14px;
        font-weight: bold;
    }
    .btn-social:hover {
        color: #eee;
    }
    .btn-social :first-child {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        width: 45px;
        padding: 12px;
        font-size: 1.6em;
        text-align: center;
        background-color: white;
        font-weight:bold;
        color: #3b5998;
    }
</style>

<div id="loadingAutologin" class="col-12 p-0" style="display: none;position: fixed;background: gray;width: 100%;height: 100%;top: 0;z-index: 1000;color: #FFF;left: 0;opacity: 0.8;text-align: center;">
    <span class="lxrm-bold" style="position: relative;top: 50%;font-size: 2rem;"><span class='fa fa-spinner fa-spin'></span> Loading...</span>
</div>
<ul id="userLogin" class="dropdown-menu pull-right" style="top:66px;display: none;overflow: inherit;" >
    <li id="user-login-section" style="height:439px;padding-left:38px;max-width:424px;">
        <div class="row">
            <div class="col-xs-12">
                <br>
                <p class="lxrm-bold" style="font-size: 20px">Login to continue</p>
                <p style="font-size: 12px">Don't have an account?
                    <span class="lxrm-bold" style="color: #23527c;cursor: pointer;" id="loginRegisterHere" >Register here.</span>
                </p>
                <div  id="loginSocialSection"><br><br><br>
                    <div class="social lxrm-bold" >
                        <a class="btn btn-lg btn-social btn-facebook" onclick="navigateFaceBookLoginScreen();" style="width:90%;padding-right: 2.5rem;">
                            <i class="fa fa-facebook fa-fw"></i> Continue with Facebook
                        </a>
                    </div>
                    <br>
                    <div class="social lxrm-bold" style=" margin-bottom: 15px;">
                        <a class="btn btn-lg btn-social btn-google" onclick="navigateGoogleLoginScreen();" style="width:90%">
                            <img  src="/resources/images/google_signup.png" >Continue with Google
                        </a>
                    </div>
                    <div>
                        <span id="emailLoginSwitchOption" class="lxrm-bold" style="cursor: pointer;color:#5087EC;">Continue with Email</span>
                    </div>
                </div><br><br>
                <div class="form" role="form" accept-charset="UTF-8" id="loginEmailSection" style="display:none">
                    <div class="form-group">
                        <input type="email" class="form-control" id="loginEmailAddress" name="loginEmailAddress" placeholder="Email" required style="border-radius: 0px;max-width:90%;height: 50px;">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="loginHeaderPassword" name="loginHeaderPassword" placeholder="Password" required style="border-radius: 0px;max-width:90%;height: 50px;">
                    </div>
                    <div class="form-group">
                        <div><p class="input-errors error" style="display: none;"></p></div>
                    </div>
                    <div class="form-group">
                        <button type="button" id="lxrmUserLogin" class="btn btn-block lxr-subscribe" style="border-radius: 0px;max-width:90%;height: 50px;font-weight: bold">Sign in</button>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="isLoginRemember" checked> Remember me
                        </label>
                        <label class="pull-right" style="padding-right: 32px;">
                            <a href="/forgot-password.html">Forgot Password?</a>
                        </label>
                    </div>
                    <div>
                        <span class="lxrm-bold" id="socialLoginSwitchOption" style="color:#5087EC;cursor: pointer;">Continue with Social</span>
                    </div>
                </div>
                <div class="col-xs-12 Privacy" style="font-size: 12px;padding-left: 0px"><br><br>
                    <span> By signing in, I agree to the 
                        <a href="/privacy-policy.html" target="_blank"  style="cursor: pointer;">Privacy Policy</a> and 
                        <a href="/terms-of-service.html" target="_blank"  style="cursor: pointer;">Terms of Service</a>.
                    </span>
                </div>
            </div>
        </div>
    </li>
    <li class="user-signout-section" style="padding: 15px 15px 15px 33px;width: 360px;">
        <div class="row">
            <div class="media">
                <div class="media-left">
<!--                    <img class="img-responsive userStatsImages" src="/resources/images/userSignInProfile.png" alt="${userH.getName()}">-->
                    <div id="profileImage"></div>
                </div>
                <div class="media-body">
                    <p>
                        <span class="lxrm-bold" id="firstname" style="font-size: 16px;">${userH.getName()}</span><br/>
                        <c:if test="${userH.getUserName() != null && !userH.getUserName().isEmpty()}">
                            <span>${userH.getUserName()}</span>
                        </c:if>
                    </p>
                </div>
                <p class="col-xs-10" style="border-bottom:1px solid #eee;padding-top: 1em;"></p>
            </div>
            <c:if test="${userH.getUserUsageStats().getCountofToolsUsed() > 0}">
                <div class="media">
                    <div class="media-left">
                        <img src="../../resources/images/loginUserUsedTools.png" class="img-responsive userStatsImages" alt="Tools Used" />
                    </div>
                    <div class="media-body">
                        <p>
                            <span class="lxrm-bold">${userH.getUserUsageStats().getCountofToolsUsed()}</span>&nbsp;<span>Tools Used</span>
                        </p>
                    </div>
                </div>
            </c:if>
            <c:if test="${userH.getUserUsageStats().getCountOfWebSitesAnalysis() > 0}">
                <div class="media">
                    <div class="media-left">
                        <img src="../../resources/images/loginUserWebsitesAnalysis.png" class="img-responsive userStatsImages" alt="Websites Analyzed" />
                    </div>
                    <div class="media-body">
                        <p>
                            <span class="lxrm-bold">${userH.getUserUsageStats().getCountOfWebSitesAnalysis()}</span>&nbsp;<span>Websites Analyzed</span>
                        </p>
                    </div>
                    <p class="col-xs-10" style="border-bottom:1px solid #eee;padding-top: 1em;"></p>
                </div>
            </c:if>
            <div class="media">
                <div class="media-body">
                    <button  class="btn lxrm-bold userSignOutBtn col-xs-10 lxrm-bold" style="border-radius:0px;height: 40px;" onclick="signOut();">Sign Out</button>
                </div>
            </div>
            <c:if test="${!userH.isSocialLogin()}">
                <div class="media">
                    <div class="media-left">
                        <a href="/change-password.html" class="lxr-changepsd" style="color :#333333 !important;"><span><i class="fa fa-unlock-alt" aria-hidden="true"></i></span> Change Password</a>
                    </div>
                </div>
            </c:if>
        </div>
    </li>
</ul>