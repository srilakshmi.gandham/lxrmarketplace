<%-- 
    Document   : hreflang
    Created on : 29 Oct, 2018, 4:44:54 PM
    Author     : anilkumar
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>hreflang Language Generator & Checker | LXRMarketplace</title>
        <meta name="description" content="Try our hreflang Language Generator & Checker tool now to generate hreflang link attrubute for your webpages! Enter your info into this">
        <meta name="keywords" content="hreflang Language Generator & Checker">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <!--<link rel="stylesheet" type="text/css" href="/resources/css/tools/structuredData.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
        <%
            int toolDBId = 42;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var atePopupStatus = true;
            var isCheckedRequest = true;
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
//                toolLimitExceeded();
                console.log("Is Href Checked: " +${hrefLang.checked});
                radioButtonChange(${hrefLang.checked});
                //Tool tip
                $('[data-toggle="popover"]').popover();
                $("#clearAll").on('click', function () {
                    parent.window.location = "/hreflang-tool.html";
                });
                $("#microdata").click(function () {
                    activeHrefGenerateSection();
                });
                $("#jsonld").click(function () {
                    activeHrefValidateSection();
                });
//                $('input:radio').change(function () {
//                    var isCheckSection = $("#hrefOptions input[type='radio']:checked").val();
//                    alert("Value of Changed Radio is : " + isCheckSection);
//                    console.log("isCheckSection:" + isCheckSection);
//                    radioButtonChange(isCheckSection);
//                });
//                $('input[type=radio][name=checked]').change(function () {
//                    console.log("")
//                    var isCheckSection = (this.value === 'true');
//                    alert("isCheckSection:" + isCheckSection);
//                    radioButtonChange(isCheckSection);
//                });
                /*Dynamically auto populating the ATE.*/
            <c:if test="${showATEPpopup}">
                getAskExpertPopup(true);
            </c:if>
            });
            function hrelangGenerateSection() {
                $('html, body').animate({scrollTop: $('#hrefLangForm').offset().top}, 'slow');
                $("#checked1").prop("checked", true).trigger("click");
                radioButtonChange(false);
            }
            //  To show the previous results of the user, when user clicks on calcel button from ask the expert and payment page
            function radioButtonChange(isCheckSection) {
                console.log("In radioButtonChange:" + isCheckSection);
                if (isCheckSection) {
                    console.log("if");
                    activeHrefValidateSection()
                } else {
                    console.log("else");
                    activeHrefGenerateSection()
                }
            }
            function activeHrefGenerateSection() {
                $("#checkSection").hide();
                $("#generateSection").show();
                $("#checked2").parent("span").removeClass("active");
                $("#checked2").parent("span").addClass("in-active");
                $("#checked1").parent("span").removeClass("in-active");
                $("#checked1").parent("span").addClass("active");
            }
            function activeHrefValidateSection() {
                $("#generateSection").hide();
                $("#checkSection").show();
                $("#checked2").parent("span").removeClass("in-active");
                $("#checked2").parent("span").addClass("active");
                $("#checked1").parent("span").removeClass("active");
                $("#checked1").parent("span").addClass("in-active");
            }
            function copyToClipboard() {
                /* Get the text field */
                var copyText = document.getElementById("generatedHrefLangContent");

                /* Select the text field */
                copyText.select();

                /* Copy the text inside the text field */
                document.execCommand("copy");

                /* Alert the copied text */
                $(".copy-text").show();
                $(".copy-text").text("Copied...");
                $(".copy-text").fadeOut(5000);
            }

            $(document).ready(function () {
                var inputIndex = 1;
            <c:if test="${fn:length(hrefLang.hrefLangPageList) > 0 && !hrefLang.checked}">
                inputIndex = ${fn:length(hrefLang.hrefLangPageList)};
                $('html, body').animate({scrollTop: $('#resultSection').offset().top}, 'slow');
            </c:if>
                $('#hrefLangForm')
                        // Add button click handler
                        .on('click', '.addButton', function () {
                            var $template = $('#optionTemplate'),
                                    $clone = $template
                                    .clone()
                                    .removeClass('hide')
                                    .removeAttr('id')
                                    .insertBefore($template);
                            $clone.find('[name=pageUrl]').attr('name', 'hrefLangPageList[' + inputIndex + '].pageUrl')
                                    .attr('id', 'hrefLangPageList' + inputIndex + '.pageUrl')
                                    .attr('class', 'form-control pageurl-input index_' + inputIndex);
                            $clone.find('[name=language]').attr('name', 'hrefLangPageList[' + inputIndex + '].language')
                                    .attr('id', 'hrefLangPageList' + inputIndex + '.language')
                                    .attr('class', 'form-control language-input language-input-index-' + inputIndex);
                            $clone.find('[class=error-msg-language]').attr('class', 'lxr-danger error-url-input error-msg-language' + inputIndex);
                            $clone.find('[name=country]').attr('name', 'hrefLangPageList[' + inputIndex + '].country')
                                    .attr('id', 'hrefLangPageList' + inputIndex + '.country')
                                    .attr('class', 'form-control location-input location-input-index-' + inputIndex);
                            $clone.find('[class=error-msg-location]').attr('class', 'lxr-danger error-url-input error-msg-location' + inputIndex);
                            $clone.find('div[class=error-url-input]').attr('class', 'lxr-danger error-url-input error-msg-div' + inputIndex);
                            inputIndex++;
                        })
                        // Remove button click handler
                        .on('click', '.removeButton', function () {
                            var removeButtonLength = $(".removeButton").length;
                            if (removeButtonLength > 2) {
                                var $row = $(this).parents('.form-group'),
                                        $option = $row.find('[name="option[]"]');
                                // Remove element containing the option
                                $row.remove();
                            } else {
                                alert("Atleast one row should be there.");
                            }
                        });

                $('#hrefLangForm').validate({// initialize the plugin
                    submitHandler: function (form) { // for demo
                        resultLoginValue = true;
                        if (!toolLimitExceeded()) {
                            return false;
                        }
                        var isCheck = ($("input[name='checked']:checked").val() === 'true');
                        var formSubmitStatus = true;
                        $("#notWorkingUrl").html("");
                        if (isCheck) {
                            if ($("#websiteURL").val().trim() === "") {
                                $("#notWorkingUrl").html("Please enter website URL");
                                formSubmitStatus = false;
                            } else if (!urlValidation($("#websiteURL").val().trim())) {
                                $("#notWorkingUrl").html("Please enter a valid website URL");
                                formSubmitStatus = false;
                            }
                        } else {
                            formSubmitStatus = validateInputUrls();
                        }
                        if (formSubmitStatus) {
                            if (isCheck) {
                                $("#checkGetResult").button('loading');
                            } else {
                                $("#generateGetResult").parent(".input-group-btn").parent().css("padding", "0");
                                $("#generateGetResult").button('loading');
                            }
                            form.submit();
                        }
                    }
                });

            });
            function validateInputUrls() {
                $(".error-url-input").hide();
                var inputStatus = true;
                $('.pageurl-input').each(function () {
                    var urlErrorMsg = "";
                    var inputValue = this.value.trim();
                    if (inputValue === "") {
                        urlErrorMsg = "Please enter webpage URL";
                    } else if (!urlValidation(inputValue)) {
                        urlErrorMsg = "Please enter a valid webpage URL";
                    }
                    if (urlErrorMsg !== "") {
                        var classNamesArray = this.className.split(" ");
                        for (var i = 0; i < classNamesArray.length; i++) {
                            if (classNamesArray[i].indexOf("index_") !== -1) {
                                $(".error-msg-div" + classNamesArray[i].split("_")[1]).show();
                                $(".error-msg-div" + classNamesArray[i].split("_")[1]).html(urlErrorMsg);
                            }
                        }
                        inputStatus = false;
                    }
                });
                $('.language-input').each(function () {
                    var inputValue = this.value.trim();
                    if (inputValue === "" || inputValue === "default") {
                        var languageErrorMsg = "";
                        var index = "";
                        var classNamesArray = this.className.split(" ");
                        for (var i = 0; i < classNamesArray.length; i++) {
                            if (classNamesArray[i].indexOf("index-") !== -1) {
                                index = classNamesArray[i].split("-")[3];
                            }
                        }
                        var locationValue = $(".location-input-index-" + index).val();
                        if (inputValue === "") {
                            languageErrorMsg = "Please select language";
                        } else if (inputValue === "default" && locationValue !== "" && locationValue !== "default") {
                            languageErrorMsg = "Please select a valid language";
                        }
                        if (languageErrorMsg !== "") {
                            $(".error-msg-language" + index).show();
                            $(".error-msg-language" + index).html(languageErrorMsg);
                            inputStatus = false;
                        }
                    }
                });

                $('.location-input').each(function () {
                    var inputValue = this.value.trim();
                    if (inputValue === "" || inputValue === "default") {
                        var locationErrorMsg = "";
                        var index = "";
                        var classNamesArray = this.className.split(" ");
                        for (var i = 0; i < classNamesArray.length; i++) {
                            if (classNamesArray[i].indexOf("index-") !== -1) {
                                index = classNamesArray[i].split("-")[3];
                            }
                        }
                        var languageValue = $(".language-input-index-" + index).val();
                        if (inputValue === "") {
                            locationErrorMsg = "Please select location";
                        } else if (inputValue === "default" && languageValue !== "" && languageValue !== "default") {
                            locationErrorMsg = "Please select a valid location";
                        }
                        if (locationErrorMsg !== "") {
                            $(".error-msg-location" + index).show();
                            $(".error-msg-location" + index).html(locationErrorMsg);
                            inputStatus = false;
                        }
                    }
                });
                return inputStatus;
            }

            function urlValidation(url) {
                var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                if (!re.test(url)) {
                    return false;
                } else {
                    return true;
                }
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
                var queryURL = "";
                var isCheck = ($("input[name='checked']:checked").val() === 'true');
                if (isCheck) {
                    queryURL = $('#websiteURL').val();
                }
                $.ajax({
                    type: "POST",
                    url: "/hreflang-tool.html?ate=ate&domain=" + queryURL + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            function submitPreviousRequest() {
                $("#generateGetResult").click();
            }
        </script>
        <style type="text/css">
            .button-toggle-div span{border-color: white !important;border-radius: 15px;}
            active.panel{border: 0;-webkit-box-shadow: 0 1px 1px white;}
            .button-toggle-div .btn.active, button-toggle-div .btn:active {
                background: #27AE60;
                /*border-radius: 8px;*/
                color: white;
                cursor: text;
            }
            .button-toggle-div .btn.in-active{background: #ddd;}
            .lxrsrt-codesnippet1{padding: 2%;height: 300px;overflow-y: auto;border: none;background: #F2F2F2 !important;}
            .lxrsrt-copybutton{
                color: white;
                background: #2f80ed !important;
                cursor: pointer;
                border: 1px solid #2f80ed;
                padding: 1% 2%;
                border-radius: 18px;
            }
            .removeButton, .addButton{cursor: pointer;}
            .error-url-input{display: none;}
            blockquote p{font-family: ProximaNova-Regular !important;}
            #hrefLangForm blockquote{
                padding: 10px 0 10px 10px!important;
                font-family: ProximaNova-Regular !important;
                font-size: 14px;
                margin: 10px 0 10px !important;
                box-shadow: 3px 3px 3px 2px #ddd;
                background-color: white !important;
                border-color: #ddd;
            }
            #hrefLangForm blockquote p{
                font-size: 14px;font-family: ProximaNova-Regular !important;color: black;
            }
            #getResult {
                font-family: ProximaNova-Regular;
                /*font-size: 18px !important;*/
                /*height: 45px;*/
                border-radius: 4px !important;
            }
            #clearAll{
                background: #fff;
                /* height: 45px; */
                vertical-align: middle;
                color: #EB5757 !important;
                border: 1px solid #EB5757 !important;
                text-align: center;
                border-radius: 4px !important;
            }
            .padding-none{padding: 0;}
            .labe-margin{margin-top: 10px;margin-bottom: 0;}
            /*            .btn.btn-radio { background-color:#FFF;border-color:#CCC;color:#333; }
                        .btn.btn-radio:hover { background-color:#EBEBEB;border-color:#ADADAD;color:#333; }
                        .btn.btn-radio.active { background-color:#9BB4C9;border-color:#285E8E;color:#000; }
                        .btn.btn-radio .badge { background-color:#333;color:#FFF; }*/
        </style>
    </head>

    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <springForm:form modelAttribute="hrefLang" id="hrefLangForm" action="/hreflang-tool.html" method="POST">
                <div class="col-xs-12 mobile-padding-none">
                    <blockquote class="blockquote">
                        <p>
                            The hreflang attribute notifies search engines which country and language a particular page is serving. 
                            As a result, search engines will display the respective page to a user, based on their search location.
                        </p>
                    </blockquote>
                </div>

                <div class="clearfixdiv" style="height: 50px;"></div>
                <div class="col-lg-12">
                    <div class="col-lg-12 padding-none" style="display: flex;margin-left: auto;margin-right: auto;margin-bottom: 10px;">
                        <div style="height: 20px;clear: both;" class="visible-lg visible-md"></div>
                        <div id="hrefOptions" class="btn-group btn-group-toggle button-toggle-div lxrm-bold" data-toggle="buttons" style="margin: 0 auto;">
                            <span id="microdata" class="btn lxr-switch active" role="button">Generate
                                <springForm:radiobutton path="checked" value="false"  />
                            </span>
                            <span id="jsonld" class="btn lxr-switch in-active" role="button">Check
                                <springForm:radiobutton path="checked" value="true" />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 padding-none" style="display: ${hrefLang.checked ? 'none' : 'block'}" id="generateSection">
                    <div class="col-xs-12 padding-none visible-lg visible-md">
                        <div class="col-md-4 lxrm-bold">Page URL</div>
                        <div class="col-md-4 lxrm-bold">Language</div>
                        <div class="col-md-4 lxrm-bold">Location</div>
                    </div>
                    <c:choose>
                        <c:when test="${fn:length(hrefLang.hrefLangPageList) > 0}">
                            <c:forEach items="${hrefLang.hrefLangPageList}" var="hrefLangPage" varStatus="i">
                                <div class="col-xs-12 form-group" style="padding: 0;">
                                    <div class="col-md-4">
                                        <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Page URL</label>
                                        <springForm:input path="hrefLangPageList[${i.index}].pageUrl" cssClass="form-control pageurl-input index_${i.index}" placeholder="Please enter page url" />
                                        <div class="lxr-danger error-url-input error-msg-div${i.index}"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Language</label>
                                        <springForm:select path="hrefLangPageList[${i.index}].language" cssClass="form-control language-input language-input-index-${i.index}" placeholder="Please enter language" >
                                            <springForm:option value="">-- select --</springForm:option>
                                            <springForm:option value="default">Default</springForm:option>
                                            <springForm:options items="${languages}" />
                                        </springForm:select>
                                        <div class="lxr-danger error-url-input error-msg-language${i.index}"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Location</label>
                                        <div class="input-group">
                                            <springForm:select path="hrefLangPageList[${i.index}].country" cssClass="form-control location-input location-input-index-${i.index}" placeholder="Please enter country">
                                                <springForm:option value="">-- select --</springForm:option>
                                                <springForm:option value="default">Default</springForm:option>
                                                <springForm:options items="${locations}" />
                                            </springForm:select>
                                            <span class="input-group-addon removeButton"><i class="fa fa-minus"></i></span>
                                        </div>
                                        <div class="lxr-danger error-url-input error-msg-location${i.index}"></div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <div class="col-xs-12 form-group padding-none">
                                <div class="col-md-4">
                                    <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Page URL</label>
                                    <springForm:input path="hrefLangPageList[0].pageUrl" cssClass="form-control pageurl-input index_0" placeholder="Please enter page url" />
                                    <div class="lxr-danger error-url-input error-msg-div0"></div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Language</label>
                                    <springForm:select path="hrefLangPageList[0].language" cssClass="form-control language-input language-input-index-0">
                                        <springForm:option value="">-- select --</springForm:option>
                                        <springForm:option value="default">Default</springForm:option>
                                        <springForm:options items="${languages}" />
                                    </springForm:select>
                                    <div class="lxr-danger error-url-input error-msg-language0"></div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Location</label>
                                    <div class="input-group">
                                        <springForm:select path="hrefLangPageList[0].country" cssClass="form-control location-input location-input-index-0">
                                            <springForm:option value="">-- select --</springForm:option>
                                            <springForm:option value="default">Default</springForm:option>
                                            <springForm:options items="${locations}" />
                                        </springForm:select>
                                        <span class="input-group-addon removeButton"><i class="fa fa-minus"></i></span>
                                    </div>
                                    <div class="lxr-danger error-url-input error-msg-location0"></div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <div class="col-xs-12 form-group hide" id="optionTemplate" style="padding: 0;">
                        <div class="col-md-4">
                            <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Page URL</label>
                            <input name="pageUrl" id="pageUrl" placeholder="Please enter page url" />
                            <div class="error-url-input"></div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Language</label>
                            <select name="language" id="language" class="form-control">
                                <option value="">-- select --</option>
                                <option value="default">Default</option>
                                <c:forEach items="${languages}" var="entry">
                                    <option value="${entry.key}">${entry.value}</option>
                                </c:forEach>
                            </select>
                            <div class="error-msg-language"></div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-xs-12 padding-none labe-margin visible-sm visible-xs">Location</label>
                            <div class="input-group">
                                <select name="country" id="country" class="form-control">
                                    <option value="">-- select --</option>
                                    <option value="default">Default</option>
                                    <c:forEach items="${locations}" var="entry">
                                        <option value="${entry.key}">${entry.value}</option>
                                    </c:forEach>
                                </select>
                                <span class="input-group-addon removeButton"><i class="fa fa-minus"></i></span>
                            </div>
                            <div class="error-msg-location"></div>
                        </div>
                    </div>
                    <!-- The option field template containing an option field and a Remove button -->
                    <div class="col-xs-12 form-group">
                        <button type="button" class="btn lxr-search-btn addButton pull-right"><i class="fa fa-plus"></i> Add More</button>
                    </div>
                    <div class="col-xs-12 visible-xs" style="height: 10px;"></div>
                    <div class="col-xs-12 form-group">
                        <div class="checkbox">
                            <label><springForm:checkbox path="xml" />Generate XML Sitemap Tags</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--                        <div class="col-sm-6 col-sm-offset-5">
                                                    <button type="submit" class="btn lxr-search-btn top-line btn-lg buttonStyles" id="generateGetResult"
                                                            data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING" style="padding: 10px 30px;">
                                                        ANALYZE
                                                    </button>
                                                </div>-->
                        <div class="col-xs-12" style="padding: 0;">
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12"  id="clearAll">
                                        <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <!--<div class="col-xs-12 visible-xs" style="height:8px;"></div>-->
                            <!--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 lxrtool-centerdiv">-->
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-lg buttonStyles lxr-search-btn col-xs-12 col-sm-12 col-md-12 col-lg-12" id="generateGetResult" data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATING">
                                        &nbsp;GENERATE&nbsp;
                                    </button>
                                </div>
                                <!--</div>--> 
                            </div>
                            <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="padding: 0;display: ${hrefLang.checked ? 'block' : 'none'};" id="checkSection">
                    <div class="col-xs-12">
                        <div class="form-group col-lg-10 col-md-9 col-sm-12">
                            <div class="icon-addon addon-lg">
                                <springForm:input placeholder="Enter your website URL" path="websiteURL" cssClass="form-control input-lg"/>
                                <i id="editImport" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                            </div>
                            <span id="notWorkingUrl" class="lxr-danger text-left">${notWorkingUrl}</span>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center" style="padding-left: 0px">
                            <div class="input-group-btn">
                                <button type="submit" id="checkGetResult" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                                    ANALYZE
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </springForm:form>
            <!--   Success Page -->
            <div class="clearfixdiv"></div>
            <c:choose>
                <c:when test="${hrefLang.checked}">
                    <div class="col-xs-12">
                        <div class="col-xs-12" style="margin-bottom: 4px;">
                            <h4 class="lxrm-bold">hreflang Language Attributes for Webpage </h4>
                            <!--                            <table class="table lxr-table col-12 padding-none" style="border: 1px solid #ddd;margin-bottom: 0;">
                                                            <tbody class="col-12 padding-none">
                                                                <tr class="col-12 padding-none">
                                                                    <td class="col-xs-6 padding-none"><span class="lxrm-bold">URL:</span> ${hrefLang.websiteURL}</td>
                                                                    <td class="col-xs-3 padding-none"><span class="lxrm-bold">Canonical Tag:</span> <i class="fa ${hrefLang.canonical ? 'fa-check lxr-success' : 'fa-times lxr-danger'}"></i></td>
                                                                    <td class="col-xs-3 padding-none">#<span class="lxrm-bold">${fn:length(hrefLangPageList) > 0 && fn:length(hrefLangPageList) < 9 ? '0' : ''}${fn:length(hrefLangPageList)} hreflang tags</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>-->
                            <div class="col-xs-12 padding-none" style="border: 1px solid #ccc;padding: 10px;">
                                <div class="col-xs-12 col-md-6"><span class="lxrm-bold">URL:</span> ${hrefLang.websiteURL}</div>
                                <div class="col-xs-12 col-sm-6 col-md-3"><span class="lxrm-bold">Canonical Tag:</span> <i class="fa ${hrefLang.canonical ? 'fa-check lxr-success' : 'fa-times lxr-danger'}"></i></div>
                                <div class="col-xs-12 col-sm-6 col-md-3">#<span class="lxrm-bold">
                                        ${fn:length(hrefLangPageList) > 0 && fn:length(hrefLangPageList) < 9 ? '0' : ''}${fn:length(hrefLangPageList)} hreflang tags</span>
                                </div>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${hrefLangPageList != null && fn:length(hrefLangPageList) > 0}">
                                <div class="col-xs-12 table-responsive" style="margin-top: 2rem;">
                                    <table class="table lxr-table" style="border: 1px solid #ddd;margin-bottom: 0;">
                                        <thead>
                                            <tr>
                                                <th>Alternate URL</th>
                                                <th>hreflang Value</th>
                                                <th>Language</th>
                                                <th>Region</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${hrefLangPageList}" var="hrefLangPage" varStatus="i">
                                                <tr>
                                                    <td>${hrefLangPageList[i.index].pageUrl}</td>
                                                    <c:choose>
                                                        <c:when test="${hrefLangPageList[i.index].language == 'default'}">
                                                            <td>X-default</td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td>${hrefLangPageList[i.index].language}-${hrefLangPageList[i.index].country}</td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <td>${hrefLangPageList[i.index].language}</td>
                                                    <td>${hrefLangPageList[i.index].country}</td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-xs-12" style="margin-top: 16px;">
                                    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-sm-offset-1 col-md-offset-2 col-lg-offset-3" style="box-shadow: 0px 1px 3px 2px #ddd;padding: 1rem;">
                                        <p class="lxrm-bold lxr-danger">We were unable to find a hreflang-markup as a HTML link-element within this website's header!</p>
                                        <p>Does your website providing same webpages in different languages? If so, add hreflng link attribute to your webpage. 
                                            Our hreflang generator will help you to create hreflang link attribute for your webpages.</p>
                                        <p><span style="cursor: pointer;color: #337ab7;" onclick="hrelangGenerateSection();">Click here</span> to generate hreflang link attrubute for your webpages</p>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:when>
                <c:when test="${!hrefLang.checked && resultContent != null}">
                    <div class="container-fluid">
                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd !important;" id="resultSection">
                            <div class="col-xs-12 lxr-panel-heading1" style="padding: 0;">
                                <div class="col-lg-12" style="padding-top: 8px;">
                                    <span class="panel-title lxrm-bold">GENERATED Hreflang Tags</span>
                                    <!--                                <p id="issue-text-1">
                                                                        Schema <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">HELPS</strong> 
                                                                        to increase CTR of a webpage, helping SEO indirectly. 
                                                                        Currently, Schema is not a direct ranking signal, but may be in the future.
                                                                    </p>-->
                                </div>
                            </div>
                            <div class="col-xs-12" style="padding: 1%;">
                                <textarea readonly class="col-xs-12 lxrsrt-codesnippet1" id="generatedHrefLangContent" style="resize:none;">${resultContent}</textarea>
                            </div>
                            <div class="col-xs-12 lxr-panel-footer1 text-left" style="padding: 1%;border-top: 1px solid #ddd;">
                                <div class="text-center" style="margin: 1% 0;">
                                    <div class="copy-text text-center col-xs-12 lxr-success"></div>
                                    <button type="button" onclick="copyToClipboard()" class="lxrsrt-copybutton" >
                                        <i class="fa fa-files-o" aria-hidden="true"></i> COPY THIS CODE</button>
                                </div>
                            </div>
                        </div>
                        <!--how to fix-->
                        <!--                    <div id="howtofix-1" style="display: none;">
                                                <div class="col-lg-12 col-sm-12 col-xs-12 panel-content" style="border: 1px solid #ddd;">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading lxr-panel-heading">
                                                            <span class="panel-title lxr-light-danger">Below are the steps to fix on page issues on the website</span>
                                                            <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                                        </div>
                                                        <div class="panel-body lxr-panel-body">
                                                            <p class="json-ld-step">
                                                                JavaScript Object Notation for Linked Data (JSON-LD) is 
                                                                a WCW3 standard website method of structured markup implementation using the JSON notation. 
                                                                Major search engines such as Google, Bing, and Yandex can understand 
                                                                JSON-LD for pages on your site.
                                                            </p>
                                                            <p class="microdata-step" style="display: none;">
                                                                Microdata is "hidden HTML code" that can be inserted in web pages. 
                                                                Microdata is also commonly referred to as structured data or Schema Markup Code.
                                                            </p>
                                                            <div class="lxr-panel-footer">
                                                                <h3>
                        <%--<spring:message code="ate.expert.label.2" />--%>
                        </h3>
                                                                <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                        <div class="clearfixdiv"></div>
                    </div>
                </c:when>
            </c:choose>
        </div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>