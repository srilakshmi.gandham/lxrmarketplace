
<!--<script type="text/javascript" src="/scripts/ui/jquery.raty.min.js"></script>
<script type="text/javascript" src="/scripts/ui/jquery.raty.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="/scripts/Charts.js"></script>-->
<script>
    google.load('visualization', '1.0', {'packages': ['corechart']});
    var titleLimit = 255;
    var commentLimit = 1024;
    var readmoreCount = 0;
    $(document).ready(function () {
        $("#ratingError").empty();

        getToolReview(1);
        getToolRatingChatData();
        getDefaultRating();
        $("#submitFeedback").click(function () {
            $("#ratingError").show();
            $("#ratingError").empty();
            $("#ratingError").css('color', 'red');
            if (userLoggedIn) {
                if ($('#ratingFeedback').val() === "") {
                    $("#ratingError").show();
                    $("#ratingError").html('Please provide rating');
                    $("#ratingError").css('color', 'red');
                    return false;
                }
                var contentLength = $.trim($('#titleFeedback').val()).length;
                if (contentLength > titleLimit) {
                    $("#ratingError").empty();
                    $("#ratingError").html("Please ensure title is below 255 characters");
                    return false;
                }
                var validTitle = $('#titleFeedback').val().trim();
                validTitle = removeHTMLText(validTitle);
                contentLength = $.trim($('#commentFeedback').val()).length;
                if (contentLength > commentLimit) {
                    $("#ratingError").empty();
                    $("#ratingError").html("Please ensure comment is below 1024 characters");
                    return false;
                }
                var validComment = $('#commentFeedback').val().trim();
                validComment = removeHTMLText(validComment);
                var feedParams = {rating: $('#ratingFeedback').val(), title: validTitle, toolId: toolDBId, comments: validComment};
                $.ajax({
                    method: "POST",
                    url: "/starrating.html?feedbackdata=submit",
                    processData: true,
                    data: feedParams,
                    dataType: "json",
                    success: function (data) {
                        var avgRating = Math.round(parseFloat(data[0]) * 100.0) / 100.0;
                        $('#toolAverageRating').html(avgRating);
                        $('#toolRatingStarGraph').html("");
                        $("#ratingError").show();
                        $("#ratingError").empty();
                        $("#ratingError").css('color', '#27AE60');
                        $("#ratingError").html('Thanks for your feedback.');
                        $("#toolRatingStarGraph").html(constructRatingGraph(avgRating, "averageRating"));
                    },
                    complete: function (xhr, textstatus) {
                        readmoreCount = 1;
                        $('#toolRatingInput').raty('cancel');
                        $('#toolRatingInput').empty();
                        $('#ratingFeedback').val("");
                        getDefaultRating();
                        getUpdatedReviews();
                        $('#titleFeedback').val("");
                        $('#commentFeedback').val("");
                    }
                });

            } else {
                $("#loginPopupModal").modal('show');
            }
        });
//         $("#readMoreReviews").click(function(){getCompleteToolReviews(2);});
    });
    function removeHTMLText(htmlCode) {
        var htmlFreeCode = htmlCode.replace(/&nbsp;/gi, " ");
        htmlFreeCode = htmlFreeCode.replace(/\n/gi, " ");
        htmlFreeCode = htmlFreeCode.replace(/<br>/gi, " ");
        htmlFreeCode = htmlFreeCode.replace(/<\/?(\w+)\s*[\w\W]*?>/g, " ");
        return htmlFreeCode.trim();
    }

    function getToolRatingChatData() {
        $.ajax({
            type: "POST",
            url: "/userreviewdisplay.html?toolId=" + toolDBId,
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                getToolRatingChart(data[1]);
                $("#toolNumberOfRating").text(data[0]);
            }
        });
    }
    /*Constructing tool rating chart(GoogleAPI) */
    function getToolRatingChart(chartData) {
        var data = google.visualization.arrayToDataTable([
            ["Rating", "value", {role: "style"}],
            ["5 star", chartData[4], "#9fc05a"],
            ["4 star", chartData[3], "#add633"],
            ["3 star", chartData[2], "#ffd834"],
            ["2 star", chartData[1], "color: #ffb234"],
            ["1 star", chartData[0], "color: #ff8b5a"]
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"},
            2]);

        var options = {
            'width': 300,
            'height': 200,
            chartArea: {width: '50%', height: '65%'},
            bar: {groupWidth: "35%"},
            legend: {position: "none"},
            hAxis: {gridlines: {color: 'transparent'}, textPosition: 'none'},
            vAxis: {textStyle: {color: '#828282',
                    fontName: 'Arial',
                    bold: true,
                    fontSize: '15'}},
            animation: {duration: 1000, easing: 'out', startup: true},
        };
        var chart = new google.visualization.BarChart(document.getElementById("toolRatingGraph"));
        chart.draw(view, options);
    }

    /*Setting the default value for rating*/
    function getDefaultRating() {
        $('#toolRatingInput').raty({
            toolRatingInput: true,
            start: 5.0,
            click: function (score, evt) {
                $("#ratingFeedback").val(score);
            }
        });
    }
    /*Functions related to user reviews starts*/
    /*To get user reviews Call this on page load.*/
    function getToolReview(reviewType) {
        $.ajax({
            type: "POST",
            url: "/userreviewdisplay.html?userreview=" + reviewType + "&toolId=" + toolDBId,
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                generateToolReviews(data);
            }
        });
        return false;
    }
    /*To get complete tool reviews.*/
    function getCompleteToolReviews() {
        readmoreCount = 1;
        getToolReview(2);
        getToolRatingChatData();
        $('#readMoreReviews').hide();
        return false;
    }
    ;
    /*To get tool reviews after user submit ratings.*/
    function getUpdatedReviews() {
        getToolRatingChatData(2);
        if (readmoreCount === 1) {
            getToolReview(2);
        } else {
            getToolReview(1);
        }
    }
    /*Constructing dynamic user reviews */
    function generateToolReviews(data) {
        var reviews = '';
        if (data[0].length !== 0) {
            reviews += '<div class="col-md-12">';
            var i = 0;
            for (i = 0; i < data[0].length; i++) {
                var username = data[0][i].username;
                var feedbackdate = data[0][i].feedbackdate;
                var title = data[0][i].title;
                var comments = "";
                if (data[0][i].comments !== null) {
                    comments = data[0][i].comments;
                    comments = comments.replace(/\n/gi, '<br/>');
                }
                if (data[0].length === 4 && readmoreCount === 0) {
                    if (i === 3)
                    {
                        break;
                    }
                }
                /*Parent div for each review section.*/
                reviews += '<div class="row" style="padding: 1%;">';
                reviews += '<div class="col-md-6">';
                /*Feedback Title*/
                reviews += '<h5 style="font-family: ProximaNova-Bold;">' + title + '</h5>';
                /*Feedback comment*/
                reviews += '<p style="font-family: ProximaNova-Regular;">' + comments + '</p>';
                /*EOF of parent div for title and comment*/
                reviews += '</div>';
                /*Parent div for user name, feedback date, stars and user rating  with right align*/
                reviews += '<div class="col-md-6 text-right" >';
                /*User name and feedback date*/
                reviews += '<div style="font-family: ProximaNova-Regular;"><span><strong>' + username + '</strong></span><span>, &nbsp;' + feedbackdate + '</span></div>';
                /*rating in star format*/
                reviews += constructRatingGraph(data[0][i].rating, "userRevies");
                /*EOF of parent div for parent div for user name, feedback date, stars and user rating*/
                reviews += '</div>';
                /*EOF of parent div compelete single review*/
                reviews += '</div>';
                reviews += '<hr style="margin:0 1% !important;">';
            }
            if (readmoreCount === 0 && data[0].length === 4 && data[0].length !== 0) {
                reviews += '<div class="text-center" style="margin:1%;"><button type="button" id="readReviews" onclick="getCompleteToolReviews(2)" class="btn lxr-subscribe">Read All Reviews</button></div>';
            }
            reviews += '</div>';
            $('#toolReviewsSection').html(reviews);
        } else {
            reviews += '<div class="col-md-12 text-center" style="margin:2% 0 0 0;font-size:1em;">No reviews found</div>';
            $('#toolReviewsSection').html(reviews);
        }
    }
    function constructRatingGraph(ratingValue, graphPlacement) {
        var ratingGraph = '';
        var rating = ratingValue;
        var halfrating = rating % 1;
        var empty = 5 - rating;
        /*rating in star format*/
        if (graphPlacement === "userRevies") {
            ratingGraph += "<div id=\"star\" class=\"reviewstar pull-right\">";
        } else {
            ratingGraph += "<div id=\"star\" class=\"reviewstar\">";
        }
        for (var r = 1; r <= rating; r++) {
            ratingGraph += "<span><img src=\"resources/images/stars/full-star-black.png\"></span>";
        }
        if (halfrating > 0.0 && halfrating < 0.25) {
            ratingGraph += "<span><img src=\"resources/images/stars/one-third-star-black.png\"></span>";
        } else if (halfrating > 0.25 && halfrating < 0.75) {
            ratingGraph += "<span><img src=\"resources/images/stars/half-star-black.png\"></span>";
        } else if (halfrating > 0.75 && halfrating < 1) {
            ratingGraph += "<span><img src=\"resources/images/stars/two-third-star-black.png\"></span>";
        }
        for (var e = 1; e <= empty; e++) {
            ratingGraph += "<span><img src=\"resources/images/stars/empty-star-black.png\"></span>";
        }
        ratingGraph += '</div>';
        return ratingGraph;
    }
</script>
<div class="container-fluid">
    <!--<div class="col-xs-11 col-sm-11 col-md-11 col-lg-12 lxrtool-centerdiv">-->
    <blockquote> RATE IT !</blockquote>
    <div class="lxr-srtrating">
        <!--1-->
        <div class="row">
            <input id="ratingFeedback" type="hidden" value="" />
            <!--Rating Input Wrapper coloum -->
            <div class="col-md-6">
                <!--Rating Star Input-->
                <div class="form-group col-md-12" style="margin-top: 8px">
                    <span class="ratingHeader">Rating:</span>
                    <span id="toolRatingInput"></span>
                </div>
                <div class="form-group col-md-12 titleInputWrapper">
                    <input type="text" id="titleFeedback" type="text" class="form-control lxrsrt-rating" placeholder="Title">
                </div>
                <div class="form-group col-md-12 commentInputWrapper">
                    <textarea id="commentFeedback" class="form-control lxrsrt-rating" rows="4" placeholder="Comment"></textarea>
                </div>
                <div class="form-group col-md-12 submitInputWrapper">
                    <span id="ratingError"></span><span><button type="button" id="submitFeedback" class="btn lxr-subscribe   pull-right"><i class="fa fa-upload" aria-hidden="true"></i> SUBMIT</button>&nbsp;&nbsp;</span>
                </div>
            </div>
            <!--Rating Analyses Wrapper coloum -->
            <div class=" col-md-6">
                <div class=" col-md-6 " style="padding-top: 3.5rem;">
                    <div class="lxrsrt-avgrating ">
                        <p class="ratingTitle">AVERAGE RATING</p>
                        <p id="toolAverageRating" >${toolObject.getToolUserInfo().getAvgRating()}</p>
                        <div id="toolRatingStarGraph"></div>
                        <p id="toolNumberOfRating">${toolObject.getToolUserInfo().getToolRatedUsersCount()}</p>
                    </div>
                </div>
                <div class=" col-md-6">
                    <div id="toolRatingGraph" ></div>
                </div>
            </div>
        </div>
        <!--2-->
        <div class="row">
            <div class="col-md-12">
                <div class="reviewTitle">USER REVIEWS</div>
                <div id="toolReviewsSection" class="row"></div>
                <div class="clearfixdiv"></div> 
            </div>
        </div>
    </div>
    <!--</div>-->
</div>