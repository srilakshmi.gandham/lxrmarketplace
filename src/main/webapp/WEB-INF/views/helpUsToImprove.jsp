
<%-- 
    Document   : signup
    Created on : Aug 1, 2017, 11:24:49 AM
    Author     : Sagar
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Help us to improve | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="../../resources/css/signup.css">
        <script>
            function submitHelpUs(param) {
                $('#personapError').html("");
                var userDesignations = $('#userDesignation').val();
                var knowAboutLXRM = $('#knowAboutLXRM').val();
                if (userDesignations === undefined) {
                    $('#personapError').html("Please select your designation");
                    return false;
                }
                if (knowAboutLXRM === "") {
                    $('#personapError').html("Please select how did you first hear about LXRMarketplace");
                    return false;
                }
                var $this = $(param);
                $this.button('loading');
                $.ajax({
                    type: "POST",
                    url: "/help-us.html",
                    processData: true,
                    data: {howToKnowAboutLXRM: knowAboutLXRM, userDesignations: userDesignations},
                    dataType: "json",
                    success: function (data) {
                        $this.button('reset');
                        if (data === 'success') {
                            skipHelpUs();
                        } else {
                            $('#personapError').html("Sorry! Please do login or signup for submitting answer");
                        }
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <!--Section #1-->
                <div class="col-xs-12 col-md-5">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 lxrformlogo">
                                    <a href="/"><img  src="../../resources/images/lxr-logo.png" alt="LXRMarketplace"></a> 
                                </div>
                                <div class="col-xs-12 signUpTitle" style="padding-left:15px" >
                                    <span> <h2 style="font-weight:bold;padding-left:6px">Thank You For Registration !</h2></span>
                                    <c:if test="${isUserEmailAvailable}">
                                        <span  style="font-size: 12px;padding-left:6px">Your  Login Credentials have been mailed to </span><br>
                                        <span  style="font-size: 12px;padding-left:6px;color: #2f80ed;">${loginUserEmail}</span><br>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <!--Help us Provide You a personalized experience-->
                        <div class="col-xs-12"style="padding-left:6px" >
                            <div class="row" id=" personalized experience">
                                <div class="col-xs-12 " >
                                    <div class="col-lg-12 " style="font-weight: inherit;padding-left:16px" >                                               
                                        <h3 style="font-weight: inherit"> Help us Provide You a personalized experience</h3>
                                    </div>
                                    <div class="col-xs-12 ">
                                        <p class="text-center" style="border-bottom: 1px solid #7F8386;"></p>
                                    </div>
                                    <div class="col-xs-12 ">
                                        <div class="form-group"> <span id="personapError" class="error"></span></div>
                                    </div>
                                    <form class="col-xs-12" style="padding-left: 19px;">
                                        <div class="form-group">
                                            <label for="userDesignation" style="color: #2f80ed;">I am:</label>
                                            <select class="form-control" id="userDesignation" name="userDesignation" style="border-radius: 0; width: 300px;height: 50px;">
                                                <option value="" label="--- select ----" >- - - select - - -</option>
                                                <c:forEach items="${userDesignations}" var="item" varStatus="count"> 
                                                    <option value="${item.getKey()}" label="${item.getValue()}">${item.getValue()}</option>
                                                </c:forEach>
                                            </select>      
                                        </div>
                                        <div class=" form-group">
                                            <label for="knowAboutLXRM" style="color: #2f80ed;">How did you first hear about LXRMarketplace:</label>
                                            <select class="form-control" id="knowAboutLXRM" style="border-radius: 0; width: 300px;height: 50px;">
                                                <option value="" label="--- select ----" >- - - select - - -</option>
                                                <c:forEach items="${howToKnowAboutLXRM}" var="item" varStatus="count"> 
                                                    <option value="${item.getKey()}" name="howToKnowAboutLXRM">${item.getValue()}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block nextBtn lxr-search-btn" style="border-radius: 0px; width: 300px;height: 50px;font-weight:bold;" type="button" onclick="return submitHelpUs(this)" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Wait a moment...">Go to LXRMarketplace</button>
                                        </div>
                                        <div class="skipHelpUs" onclick="skipHelpUs()">Skip</div>
                                        <div class=" col-xs-12 Note" style="padding-left: 2px;margin-top: 5rem;font-size: 12px;word-break: break-word; ">
                                             <span> <b>Note:</b> In most of the cases you should receive the verification mails within minutes.If not please check your Spam/Junk/Bulk mail folders for the mail.In some cases depending on your email system it could take longer,however if you don"t get an email from us within half an hour,please write to us at 
                                                    <a href="/terms-of-service.html" target="_blank">support@lxrmarketplace.com</a>.
                                             </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 visible-md visible-lg">
                    <div class="">
                        <img  src="../../resources/images/lxrmSignUpLogo.png" alt="LXRMarketplace" width="100%" >
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        function skipHelpUs() {
            parent.window.location = window.location.protocol + "//" + window.location.host + "/";
        }
    </script>
</html>
