<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Engine Marketing Services | SEM Consulting |
            LXRMarketplace.com</title>
        <meta name="description"
              content="NetElixir, the force behind the LXRMarketplace, seeks to democratize online search by helping small businesses compete through knowledge, free tools & advice">
        <meta name="keywords" content="search engine marketing services">
        <%--<%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>--%>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/static-pages.css">
        <% long toolDBId = -1l;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-justify" style=" color: #888; ">
                    <h2 class="static-main-heading">About Us</h2>
                    <h4 style="color: black;font-family: ProximaNova-Bold;">
                        Search Engine Marketing Services
                    </h4>
                    <p>
                        Over the past 10 years, NetElixir has managed online
                        marketing campaigns for 200+ online retailers in 9 countries.
                        This has given us first-hand insight into what works and what
                        doesn't. As small retailers are losing faith in the power of
                        online marketing due to rising costs and diminishing returns
                        there becomes a tangible need for a solution. The big challenge
                        for small retailers is that they cannot afford expensive tools or
                        outsourced agency management.
                    </p>
                    <p>
                        As a result, in 2011 NetElixir launched LXRMarketplace, a
                        free web-based application marketplace for small retailers. The
                        mission of LXRMarketplace is to simplify lives of small retail
                        marketers by providing them with free and low priced web
                        applications for improving their online marketing management
                        efficiency as well as campaign performance.
                    </p>
                    <p>
                        We at NetElixir have a grand vision of democratizing the
                        online retail marketing industry. As the e-commerce industry
                        becomes more polarized, we believe in helping small businesses
                        compete through knowledge, free tools, and advice.
                    </p>
                    <p>
                        We intend to launch more and more free online SEO
                        and SEM web apps for small business owners. At this current time,
                        users have access to:
                    </p>
                    <ul>
                        <p class="static-sub-heading">SEO Tools for Small Business</p>


                        <li><a href="/structured-data-generator.html">Structured Data Generator</a> - Provides a quick and easy way of generating different types of markups for your website.</li>
                        <li><a href="/penalty-checker-tool.html">SEO Penalty Checker</a> - It shows Google update had a positive or negative impact on your website.</li>
                        <li><a href="/page-speed-insights-tool.html">PageSpeed Insights</a> - Quickly identify the page speed performance impact issues of a web page.</li>
                        <li><a href="/most-used-keywords-tool.html">Most Used Keywords</a> - Check and analyse most used keywords present on a website.</li>
                        <li><a href="/broken-link-checker-tool.html" target="_blank">Broken Link Checker</a> - Quickly identify and analyze broken hyperlinks for any website.</li>
                        <li><a href="/top-ranked-websites-bykeyword-tool.html" target="_blank">Top Ranked Websites by Keyword</a> - Find out who your top competitors are for any keyword!</li>
                        <li><a href="/social-media-analyzer.html" target="_blank">Social Media Analyzer</a> - Get an easy-to-understand report of your social media reach and engagement by each of your social media channels!</li>		
                        <li><a href="/dns-lookup-tool.html" target="_blank">DNS Lookup</a> - A quick and easy way to fetch DNS records.</li>
                        <li><a href="/meta-tag-generator-tool.html" target="_blank">Meta Tag
                                Generator</a> - A quick and easy way to generate an edited/new meta tags for your webpage.</li>
                        <li><a href="/robots-txt-validator-tool.html" target="_blank">Robots.txt
                                Validator</a> - A quick and easy way to analyze the syntax errors
                            of a robots.txt file for your website!</li>
                        <li><a href="/robots-txt-generator-tool.html" target="_blank">Robots.txt
                                Generator</a> - By using the quick and efficient robots.txt
                            generator, you can easily generate an edited or new robots.txt
                            file for your website.</li>
                        <li><a href="/domain-age-checker-tool.html" target="_blank">Domain Age
                                Checker</a> - Generates a complete report to help you identify the
                            Website Domain Age, Registration Information, Administrative
                            and Technical contact details.</li>
                        <li><a href="/seo-inbound-link-checker-tool.html" target="_blank">Inbound
                                Link Checker</a> - See all the inbound links (backlinks) of your
                            site or your competitor’s site. Check the number and quality of
                            the links pointing back to your website.</li>
                        <li><a href="/weekly-keyword-rank-checker-tool.html" target="_blank">Weekly
                                Keyword Rank Checker</a> - Monitor and analyze weekly website
                            rankings in Google and Bing and compare with competitors.</li>
                        <li><a href="/competitor-webpage-monitor-tool.html" target="_blank">Competitor
                                Webpage Monitor</a> - Track and monitor your competitor's webpages
                            easily and automatically by setting up email alerts 
                            with this tool.</li>
                        <li><a href="/seo-webpage-analysis-tool.html" target="_blank">SEO Webpage
                                Analysis</a> - Analyze various on-page SEO factors on your website
                            to judge how well it is SEO optimized and whether the SEO
                            elements are in place or not.</li>
                        <li><a href="/seo-competitor-analysis-tool.html" target="_blank">Competitor
                                Analysis</a> - Quickly analyze how search-friendly your website is
                            compared to your competitor's websites.</li>
                        <li><a href="/seo-sitemap-builder-tool.html" target="_blank">Sitemap Builder</a>
                            - Automatically generate sitemaps for your website whenever you
                            make changes or update the content on your site.</li>
                        <!--         <li><a href="/amazonPriceAnalyzer.html">Amazon Price Analyzer</a> - Quickly compare your product prices to those of your competitors to effectively compete in the common marketplace.</li> -->
                        <!--<p class="smallheading Bold"> SEM Tools for Small Business</p>-->

                        <p class="static-sub-heading">SEM/PPC Tools for Small Business</p>


                        <li><a href="/ppc-keyword-performance-analyzer-tool.html" target="_blank">Keyword
                                Performance Analyzer</a> - Analyze the keywords of your current
                            marketing campaigns to easily separate the top performers from
                            the non-performers.</li>
                        <li><a href="/return-on-investment-calculator-tool.html" target="_blank">ROI Calculator </a>-
                            Calculate how much money your online marketing campaign is
                            making for you, in seconds!</li>
                        <li><a href="/ppc-keyword-combination-tool.html" target="_blank">Keyword
                                Combinator </a>- Generate thousands of keywords in Adwords editor
                            and AdCenter desktop compatible formats.</li>
                        <li><a href="/url-auditor-tool.html" target="_blank">URL Auditor</a> -
                            Analyze your marketing campaigns' URLs to quickly find and fix
                            those that are broken.</li>


                        <!--                        <p class="smallheading Bold">E-commerce Mobile Apps for Small
                                                    Business
                                                </p>-->
                        <p class="static-sub-heading">E-commerce Mobile Apps for Small
                            Business</p>

                        <li><a href="/dashly-for-woocommerce.html">Dashly For Woocommerce</a>
                            - Dashly for Woocommerce is a mobile dashboard and admin Android application for Ecommerce Store implemented on WooCommerce.</li>
                        <li><a href="/dashly-for-magento.html" target="_blank">Dashly For Magento</a>
                            - Dashly is a Mobile Dashboard and Admin Android application
                            for Magento Ecommerce Store.</li>
                        <li><a href="/dashly-for-bigcommerce.html" target="_blank">Dashly For
                                Bigcommerce</a> - Bigcommerce Mobile Dashboard is a Mobile
                            Dashboard and Admin Android application for Bigcommerce
                            Ecommerce Store.</li>
                        <li><a href="/dashly-for-prestaShop.html" target="_blank">Dashly For
                                PrestaShop</a> - PrestaShop Mobile Dashboard is a Mobile Dashboard
                            and Admin Android application for PrestaShop Ecommerce Store.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>