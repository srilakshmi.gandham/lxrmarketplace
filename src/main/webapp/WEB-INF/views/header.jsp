
<%@page import="lxr.marketplace.user.Login, lxr.marketplace.admin.expertuser.ExpertUser"%>
<%
    Login userH = (Login) session.getAttribute("user");
    Object ulH = session.getAttribute("userLoggedIn");
    boolean userLoggedInH = false;
    String role = "";
    boolean isUserAdmin = false;
    boolean isLocalUser = false;
    if (ulH != null) {
        userLoggedInH = (Boolean) ulH;
        if (userH != null) {
            isUserAdmin = userH.isAdmin();
            isLocalUser = userH.isIslocal();
            if (userH.getUserName().contains("GuestUser")) {
                userH.setUserName(userH.getUserName().substring(userH.getUserName().indexOf("<") + 1, userH.getUserName().lastIndexOf(">")));
            }
            if (userH.getUserName().equalsIgnoreCase("Email")) {
                userH.setUserName("");
            }
        }
    }
    int totalAnswersCount = 0;
    int totalUnViewedAnswersCount = 0;
    boolean isHavingMsgs = true;
    if (session.getAttribute("totalAnswersCount") != null && session.getAttribute("totalUnViewedAnswersCount") != null) {
        totalAnswersCount = (int) session.getAttribute("totalAnswersCount");
        totalUnViewedAnswersCount = (int) session.getAttribute("totalUnViewedAnswersCount");
        isHavingMsgs = false;
    }
    if (userLoggedInH) {
        if (session.getAttribute("expertUser") != null) {
            role = ((ExpertUser) session.getAttribute("expertUser")).getRole();
        }
    }
    String facebookLoginOAuthURL = null;
    String googleLoginOAuthURL = null;
    if (session.getAttribute("facebookLoginOAuthURL") != null) {
        facebookLoginOAuthURL = (String) session.getAttribute("facebookLoginOAuthURL");
    }
    if (session.getAttribute("googleLoginOAuthURL") != null) {
        googleLoginOAuthURL = (String) session.getAttribute("googleLoginOAuthURL");
    }

    pageContext.setAttribute("userH", userH, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("userLoggedIn", userLoggedInH, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("isUserAdmin", isUserAdmin, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("isLocalUser", isLocalUser, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("totalAnswersCount", totalAnswersCount, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("totalUnViewedAnswersCount", totalUnViewedAnswersCount, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("isHavingMsgs", isHavingMsgs, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("facebookLoginOAuthURL", facebookLoginOAuthURL, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("googleLoginOAuthURL", googleLoginOAuthURL, PageContext.SESSION_SCOPE);
    pageContext.setAttribute("role", role, PageContext.SESSION_SCOPE);
//    System.out.println("User email:: "+userH.getUserName().length());
%>
<%@ include file="/WEB-INF/views/login.jsp"%>
<script type="text/javascript">
    var userLoggedIn = ${userLoggedIn};
    var isLocalUser = ${isLocalUser};
    var loginLxrmUserEmail = "${userH.userName}";
    var headerLoginuserName = "${userH.getName()}";
    $(document).ready(function () {
//        $("#loginUserNameWrapper").empty();
        $(".sidebar-nav li a").click(function (event) {
            $('.navbar-toggle').click();
        });
        if (${userLoggedIn}) {
            $("#happyCustomerImage").hide();
            $("#headerprofileimage").show();
            $("#headerprofileimage").html(headerLoginuserName.charAt(0).toUpperCase());
        } else {
            $("#happyCustomerImage").show();
        }

        //Login toogle
        $(".lxr-useri-con").click(function () {
            $("#userLogin").toggle();
        });
        //To display the Notification
        $("#user-messages, #m-user-messages").click(function () {
            naviagtionToMessages(${userLoggedIn}, ${isUserAdmin});
        });
        if (${userLoggedIn}) {
            if (${totalAnswersCount} > 0 && !${isHavingMsgs}) {
                showNotification(${totalAnswersCount}, ${totalUnViewedAnswersCount});
            } else {
                getNotificationCount();
            }
            setAdminMenu(${userLoggedIn}, ${isUserAdmin});
        }


//        Based on the admin role we should navigate to the admin panel tabs
        $('#admin-menu, #m-admin-menu').click(function () {
            var role = "<%=role%>";
            parentUrl = window.location.protocol + "//" + window.location.host;
            if (role === 'reviewer' || role === 'expert') {
                parent.window.location = parentUrl + "/ate-question.html";
            } else {
                parent.window.location = parentUrl + "/dashboard.html";
            }
        });

        //Functio for ios devices to clos tooltip popup
        $('body div').click(function () {
            var toolTipHtml = $('div.tooltip[role=tooltip]').html();
            if (toolTipHtml !== null && toolTipHtml !== undefined && toolTipHtml !== "") {
                if ($('div.tooltip[role=tooltip]').is(':hidden')) {
                    $('div.tooltip[role=tooltip]').show();
                } else {
                    $('div.tooltip[role=tooltip]').hide();
                }
            }
        });
    });

    /* user's messages */
    function naviagtionToMessages(userLoggedIn, isUserAdmin) {
        if (userLoggedIn && !isUserAdmin) {
            parentUrl = window.location.protocol + "//" + window.location.host;
            parent.window.location = parentUrl + "/user-messages.html";
        }
    }
//    Navigate to signup page
    function signupLxrm() {
        parent.window.location = "/signup.html";
    }

//    User signout function
    function signOut() {
        $.ajax({
            type: "POST",
            url: "login.html?signout='signout'",
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                if (data[0] !== null) {
                    parent.window.location = data[0];
                }
                sessionStorage.removeItem('name');
                sessionStorage.removeItem('email');
            }
        });
    }
    // If user is admin then show Admin option in header menu
    function setAdminMenu(userLoggedIn, isUserAdmin) {
        if (userLoggedIn && isUserAdmin) {
            $('#admin-menu').show();
            $('#m-admin-menu').show();
        }
    }
    function navigate(section) {
        if (isHomePage) {
            $('html, body').animate({scrollTop: $('#' + section + '-tools').offset().top}, 'slow');
        } else {
            parent.window.location = "/#" + section + "-tools";
        }
    }

    function navigateFaceBookLoginScreen() {
        signupPopUp('social');
        setTimeout(function () {
            var fbOAuthURL = "${facebookLoginOAuthURL}";
            console.log("facebookLoginOAuthURL:: " + fbOAuthURL);
            if (fbOAuthURL.length > 0) {
                window.open(fbOAuthURL, "_self", "facebookLoginOAuthURL::");
            }
        }, 500);
        return false;
    }
    function navigateGoogleLoginScreen() {
        signupPopUp('social');
        setTimeout(function () {
            var gooogleOAuthURL = "${googleLoginOAuthURL}";
            console.log("googleLoginOAuthURL ::" + gooogleOAuthURL);
            if (gooogleOAuthURL.length > 0) {
                window.open(gooogleOAuthURL, "_self", "googleLoginOAuthURL");
            }
        }, 500);
        return false;
    }


</script>
<style>
    #headerprofileimage{
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background: #F58120;
        font-size: 1.2em;
        color: #fff;
        text-align: center;
        line-height: 33px;
        display: none;
    }
</style>
<nav class="navbar navbar-default" role="navigation">
    <div id="wrapper">
        <!-- Side bar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav" id="lxrmNavigationMobileWrapper">
                <li  id="lxrmMobileNavigationHome" class="active"> <a href="/">Home</a> </li>
                <li  id="lxrmMobileNavigationSeo"> <a onclick="navigate('seo');">SEO</a> </li>
                <li  id="lxrmMobileNavigationSocial"> <a onclick="navigate('social');">SOCIAL</a> </li>
                <li  id="lxrmMobileNavigationPpc"> <a onclick="navigate('ppc');">PPC</a> </li>
                <li  id="lxrmMobileNavigationEcommerce"> <a onclick="navigate('ecommerce');">E-COMMERCE</a> </li>
                <li> <a href="https://lxrmarketplace.com/blog/" target="_blank">BLOG</a> </li>
                <li id="lxrmNavigationAboutUs"> <a href="/about-us.html" target="_blank">ABOUT US</a> </li>
                <li id="lxrmNavigationAdminPannel"> <a id="m-admin-menu" style="display: none;">Admin Panel</a> </li>
                <li id="m-user-messages" class="dropdown" style="display: none;">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        Messages<span id="m-msg-notification-count" class="lxr-badges1"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div id="page-content-wrapper" style="padding: 0px;">
            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" >
                    <button type="button" class="navbar-toggle pull-left" id="menu-toggle" style="border:none;background: none;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="lxr-navbar-brand" href="/"><img class="img-responsive center-block"  src="../../resources/images/lxr-logo.png" alt="lxrmarketplace.com"></a>
                    <!--User Profile in Mobile-->
                    <ul class="visible-xs pull-right" style="margin: 8px 6px 0px 0px;">
                        <li class="dropdown ">
                            <a data-toggle="dropdown" class="dropdown-toggle lxr-useri-con" href="#">
                                <span><img src="../../resources/images/happy-customers.png" class="img-responsive" alt="User profile" width="32" height="35"/></span>
                            </a>
                        </li>
                    </ul>
                    <!--User Profile in Mobile end-->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="box-shadow: 0px 2px 0px 0px #f1f1f1;">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="/">HOME</a></li>
                        <li><a onclick="navigate('seo');">SEO</a></li>
                        <li> <a onclick="navigate('social');">SOCIAL</a> </li>
                        <li><a onclick="navigate('ppc');">PPC</a></li>
                        <li><a onclick="navigate('ecommerce');">E-COMMERCE</a></li>
                        <li><a href="https://lxrmarketplace.com/blog/" target="_blank">BLOG</a></li>
                        <li><a href="/about-us.html" target="_blank">ABOUT US</a></li>
                        <li id="admin-menu" style="display: none;"><a href="#">ADMIN PANEL</a></li>
                        <li id="user-messages" class="dropdown" style="display: none;">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="glyphicon glyphicon-envelope lxr-notify-icon"></span><span id="msg-notification-count" class="badge lxr-badges"></span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle lxr-useri-con" href="#">
                                <span id="happyCustomerImage"><img src="../../resources/images/happy-customers.png" class="img-responsive" alt="LXRMarketplace SignIn " width="32" height="35"/></span>
                                <div id="headerprofileimage"></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</nav>
