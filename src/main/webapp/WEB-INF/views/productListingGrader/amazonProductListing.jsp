<%-- 
    Document   : amazonProductListing
    Created on : 23 Aug, 2018, 3:29:16 PM
    Author     : netelixir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Amazon Product Listing Grader</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <script type="text/javascript" src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>
        <script type="text/javascript" src="../resources/js/angular/angular.min.js"></script>
        <script type="text/javascript" src="/resources/js/tools/amazonProductListingGrader/amazonProductListing.js"></script> 
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/amazon.css">
        <%
            int toolDBId = 41;
        %>

        <script type="text/javascript">
            var reportDownload = 0;
            var atePopupStatus = true;
            var downloadReportType = "";
            var isBackToSuccess = false;
            var isPreUpload = false;

            $(document).ready(function () {
                $(window).on('popstate', function () {
                    location.reload(true);
                });

                $('.switch-input').on('change', function () {
                    isChecked = $(this).is(':checked');
                    var $switchLabel = $('.switch-label');
                    if (isChecked) {
                        $switchLabel.attr('data-on');
                        $("#upload").show();
                        $("#urls").hide();
                    } else {
                        $switchLabel.attr('data-off');
                        $("#upload").hide();
                        $("#urls").show();
                    }
                });
//             
                $("#fileUpload").change(function () {
                    $("#fileName").empty();
                    var fileNameOriginal = $("#fileUpload").val();
                    fileNameOriginal = fileNameOriginal.replace("C:\\fakepath\\", "");
                    $("#fileName").hide();
                    if (fileNameOriginal.length !== 0) {
                        if (fileNameOriginal.length > 9) {
                            fileNameOriginal = fileNameOriginal.substr(0, 9) + "..." + fileNameOriginal.substring(fileNameOriginal.length - 6, fileNameOriginal.length);
                        }
                        $("#fileName").show();
                        $("#fileName").html(fileNameOriginal);
                        $('#fileLabel').show();
                    } else {
                        $('#fileLabel').hide();
                    }

                });


                $("#resultInfo a[href^='#']").click(function (e) {
                    e.preventDefault();

                    var position = $($(this).attr("href")).offset().top;

                    $("body, html").animate({
                        scrollTop: position
                    } /* speed */);
                });

//                toolLimitExceeded();
            });

//Ask Expert Block
            function getToolRelatedIssues() {
                $.ajax({
                    type: "POST",
                    url: "/product-listing.html?ate=ate&domain=",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {
                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }
            function sliderFunV2()
            {
                var SETTINGS = {
                    navBarTravelling: false,
                    navBarTravelDirection: "",
                    navBarTravelDistance: 150
                }
                document.documentElement.classList.remove("no-js");
                document.documentElement.classList.add("js");

                // Out advancer buttons
                var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
                var pnAdvancerRight = document.getElementById("pnAdvancerRight");

                var pnProductNav = document.getElementById("pnProductNav");
                var pnProductNavContents = document.getElementById("pnProductNavContents");

                pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));

// Handle the scroll of the horizontal container
                var last_known_scroll_position = 0;
                var ticking = false;

                function doSomething(scroll_pos) {
                    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
                }

                pnProductNav.addEventListener("scroll", function () {
                    last_known_scroll_position = window.scrollY;
                    if (!ticking) {
                        window.requestAnimationFrame(function () {
                            doSomething(last_known_scroll_position);
                            ticking = false;
                        });
                    }
                    ticking = true;
                });


                $('#pnAdvancerLeft').on('click', function () {
                    // If in the middle of a move return
                    if (SETTINGS.navBarTravelling === true) {
                        return;
                    }
                    // If we have content overflowing both sides or on the left
                    if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
                        // Find how far this panel has been scrolled
                        var availableScrollLeft = pnProductNav.scrollLeft;
                        // If the space available is less than two lots of our desired distance, just move the whole amount
                        // otherwise, move by the amount in the settings
                        if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
                            pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
                        } else {
                            pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
                        }
                        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
                        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
                        // Update our settings
                        SETTINGS.navBarTravelDirection = "left";
                        SETTINGS.navBarTravelling = true;
                    }
                    // Now update the attribute in the DOM
                    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
                });

                $('#pnAdvancerRight').on('click', function () {
                    // If in the middle of a move return
                    if (SETTINGS.navBarTravelling === true) {
                        return;
                    }
                    // If we have content overflowing both sides or on the right
                    if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
                        // Get the right edge of the container and content
                        var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
                        var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
                        // Now we know how much space we have available to scroll
                        var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
                        // If the space available is less than two lots of our desired distance, just move the whole amount
                        // otherwise, move by the amount in the settings
                        if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
                            pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
                        } else {
                            pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
                        }
                        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
                        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
                        // Update our settings
                        SETTINGS.navBarTravelDirection = "right";
                        SETTINGS.navBarTravelling = true;
                    }
                    // Now update the attribute in the DOM
                    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
                });

                pnProductNavContents.addEventListener(
                        "transitionend",
                        function () {
                            // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
                            var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
                            var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
                            // If there is no transition we want to default to 0 and not null
                            var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
                            pnProductNavContents.style.transform = "none";
                            pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
                            // Now lets set the scroll position
                            if (SETTINGS.navBarTravelDirection === "left") {
                                pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
                            } else {
                                pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
                            }
                            SETTINGS.navBarTravelling = false;
                        },
                        false
                        );

// Handle setting the currently active link
                $('#pnProductNavContents').on('click', function (e) {
                    var links = [].slice.call(document.querySelectorAll(".pn-ProductNav_Link"));
                    links.forEach(function (item) {
                        item.setAttribute("aria-selected", "false");
                    })
                    e.target.setAttribute("aria-selected", "true");
                })

                function determineOverflow(content, container) {
                    var containerMetrics = container.getBoundingClientRect();
                    var containerMetricsRight = Math.floor(containerMetrics.right);
                    var containerMetricsLeft = Math.floor(containerMetrics.left);
                    var contentMetrics = content.getBoundingClientRect();
                    var contentMetricsRight = Math.floor(contentMetrics.right);
                    var contentMetricsLeft = Math.floor(contentMetrics.left);
                    if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
                        return "both";
                    } else if (contentMetricsLeft < containerMetricsLeft) {
                        return "left";
                    } else if (contentMetricsRight > containerMetricsRight) {
                        return "right";
                    } else {
                        return "none";
                    }
                }

            }

            function submitPreviousRequest() {
                if (isPreUpload === false) {
                    angular.element('#loading').triggerHandler('click');
                } else {
                    angular.element('#loading1').triggerHandler('click');
                }
            }

        </script>
    </head>

    <!--<div id="loader"  style="text-align: center; font-size: 30px;"><span class="fa fa-spinner fa-spin" ></span>Loading....</div>-->
    <body  ng-app="myApp"  ng-controller="myCtrl as ctrl" data-spy="scroll" data-target="#head">
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div id="loadingDiv" class="col-12 p-0" style="position: fixed;background: gray;width: 100%;height: 100%;top: 0;z-index: 1000;color: #FFF;left: 0;opacity: 0.8;text-align: center;">
            <span class="lxrm-bold" style="position: relative;top: 50%;font-size: 2rem;"><span class='fa fa-spinner fa-spin'></span> Loading...</span>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container"  id="myDiv" >
            <div class="col-md-12" style="text-align: center;"><h1 class=" font-size-content ">Find out how well listed your products are on<br> <span style="color:#F58120; ">Amazon.com</span></h1></div>
            <div  class="col-md-12" style="text-align: center; margin-top: 20px;">
                <label class="switch" style="margin-left: auto; margin-right: auto;">
                    <input class="switch-input"id="switchButton" type="checkbox" unchecked name="switchUpload">
                    <span class="switch-label"   data-on="Pre Upload" data-off="Post Upload"></span> 
                    <span class="switch-handle"></span> 
                </label>
            </div>
            <form  class="col-xs-12 form-page middleContent mobile-padding-none" id="urls" style="padding-top: 20px;">
                <div class="col-lg-6 col-xs-12 middleContent padding-zero">
                    <div class="padding-zero col-xs-12" style="padding-bottom: 10px;">
                        <textarea placeholder="Enter your amazon product URL(s) one per line (maximum 100 allowed)" rows="7" name="manuallyURLS" id="manuallyURLS" class="form-control input-lg col-xs-9" ng-model="manuallyURLS" style="resize:none; margin-bottom: inherit;"></textarea>
                        <span id="searchQueryError" class="lxr-danger text-left"  >
                        </span>
                    </div>
                    <div class="col-xs-12">
                        <button type="button" ng-click="submitUrl()" class=" btn lxr-search-btn top-line btn-lg buttonStyles middleButton"id="loading" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING">
                            ANALYZE
                        </button>
                    </div>

                </div>
            </form>

            <form method="post" class="col-xs-12 middleContent mobile-padding-none" enctype="multipart/form-data" style=" margin-top: 20px; display: none; text-align: center;" id="upload">
                <div class="row col-lg-8 col-xs-12 middleContent normal-styles"  >
                    <div class="col-xs-12 ">
                        <div class="col-xs-12">
                            <a href="/download-sample-file.html" style="font-size: 1.5rem;">Click here to download a sample template file</a>
                        </div>
                        <div class="col-xs-12" style="padding-top: 10px;">
                            <label style=" font-size: 2rem;" class="uploadLabel lxrm-bold">Upload your File: 
                                <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                                   title="Upload xls or xlsx file of amazon product feed to know the score for your parameters such as title, description, images count and features.">
                                    <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                                </a>
                            </label>
                            <label for="fileUpload" id="browseFile" class="btn btn-primary lxr-subscribe" style="font-size: 18px;">Browse File</label>
                            <input class="file " type="file" data-rule-required="true" id="fileUpload" file-model="fileModel" name="fileUpload" style="display:none; " >
                            <p class="col-lg-8 col-xs-12 "style="float: right; padding-top: 10px;">(Only one file in xls or xlsx format)</p>
                        </div>
                        <div class="col-xs-12" id="showFileName">
                            <span  style="font-size: 1.5rem" ><span id="fileLabel" class="lxrm-bold" style="display: none;"> FileName: </span><span id="fileName" style="padding-top: 10px;"></span></span>
                            <span class="col-xs-12 lxr-danger text-center" id="fileUpload1Error"></span>

                        </div>
                        <div class="col-xs-12" style="margin-top: 6px;">
                            <button ng-click="continueFileUpload()" type="button" class="btn top-line btn-lg buttonStyles lxr-search-btn"  id="loading1" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div  class="result col-xs-12"  id="resultInfo">
                <div class="col-xs-12" id="errorUrlsDiv" style="margin-top: 20px;"  ng-if="amazonProductInfoList !== null && amazonProductInfoList.length >= 1">
                    <h4 class="col-xs-12 lxrm-bold" >Following are the URLs which cannot be analyzed or reviewed :</h4>
                    <div class="col-xs-12" id="errorUrls" ng-repeat="amazonProductErrorInfo in amazonProductInfoList track by $index">
                        <div class="col-xs-12">{{$index + 1}}. {{amazonProductErrorInfo}}</div>
                    </div>
                </div>
                <div class="col-xs-12" id="repeatedUrlsDiv" style="margin-top: 40px;"  ng-if="repeatedUrls !== null && getLength(repeatedUrls) > 0">
                    <h4 class="col-xs-12 lxrm-bold" ng-switch="amazonProductAnalysis.preUpload === true"><span ng-switch-when="true">Following are the Product id's which are repeated more than one time :</span><span ng-switch-default>Following are the URLs which are repeated more than one time :</span></h4>
                    <div class="col-xs-12" id="repeatedUrls" >
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th ng-switch="amazonProductAnalysis.preUpload === true">
                                        <span ng-switch-when="true">PRODUCT ID</span><span ng-switch-default="">URL</span>    
                                    </th>
                                    <th>Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(key, value) in repeatedUrls track by $index">
                                    <td>{{key}}</td>
                                    <td>{{value}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--<div class="col-xs-12">{{$index + 1}}. {{amazonProductrepeatedInfo}}</div>-->
                    </div>
                </div>
                <div class="col-lg-12 lxrm-bold"  style="padding-left: 0; margin-top: 20px;" ><h2>OVERVIEW</h2></div>
                <div class=" col-12 col-sm-12 col-lg-12 " ng-if="amazonProductListing !== null && amazonProductListing.length > 0">
                    <div class="pn-ProductNav_Wrapper" id="slider">
                        <nav id="pnProductNav" class="pn-ProductNav">
                            <div id="pnProductNavContents" class="pn-ProductNav_Contents" style="display: flex;">
                                <a href="#" class="pn-ProductNav_Link "aria-selected="{{$index == 0 ? 'true' : 'false'}}" ng-repeat="amazonProductListings in amazonProductListing track by $index" style= "padding-right: 12px;"> 
                                    <button style="color: black; padding-top: 10px; padding-bottom: 10px;" title="{{amazonProductAnalysis.preUpload == true ? amazonProductListings.skuId : amazonProductListings.url}}" type="button" ng-click="indexBasedData($index)" class="lxrm-bold w-100 btn lxr-bg-black px-4 {{$index == 0 ? 'button-clicked' : ''}}" id="indexedButton{{$index}}">PRODUCT &#35;{{$index + 1}}</button>
                                </a>
                            </div>
                        </nav>
                        <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                            <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/></svg>
                        </button>
                        <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                            <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/></svg>
                        </button>
                    </div>
                </div>
                <div class="padding-box-content  col-xs-12 lxrm-box-shadow " style="    margin-top: 15px;">
                    <div  class=" col-lg-6 col-xs-12 col-sm-6 col-md-6">
                        <div class="col-lg-8 col-xs-12" style="text-align: center;">
                            <div id="container" class="col-xs-12" style="line-height: 0;"></div>
                            <h4 class="col-xs-12" >LISTING SCORE</h4>
                            <h1  class="col-xs-12 more-font-size" style="line-height: 0.5; ">{{amazonProductInfo.totalScore}}/100</h1>

                            <h2 class="Bold col-xs-12" style="color: {{getColorByScore(amazonProductInfo.totalScore)}};">{{getMessageByScore(amazonProductInfo.totalScore)}}</h2>


                            <h4 style="word-break: break-all;padding: 0;" class="col-xs-12"><span ng-switch="amazonProductAnalysis.preUpload === true">
                                    <span ng-switch-when="true"style="color: #337ab7;">PRODUCT ID: <span ng-switch="amazonProductInfo.skuId !== null"><span ng-switch-when="true"></span>{{amazonProductInfo.skuId}}<span ng-switch-default class="lxr-danger">Product Id Not Found</span></span></span>
                                    <a href="{{amazonProductInfo.url}}" ng-switch-default target="_blank">{{amazonProductInfo.url.split("&")[0]}}</a>
                                </span></h4>

                        </div>
                    </div>
                    <div class=" col-lg-4 col-xs-12 col-sm-4 col-md-4" style="float: right;">
                        <div id="head" ng-if="amazonProductInfo.length !== 0">
                            <h5 class="Bold line-gap"><a class="scroll" href="#titleId" style="color: black;">Product Title </a><span style="float: right;">{{amazonProductInfo.titleScore}}/100</span></h5>
                            <div class="progress  progress-radius-bg ">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.titleScore}}%;background: {{getColorByScore(amazonProductInfo.titleScore)}};">
                                </div>
                            </div>
                            <h5 class="Bold line-gap"><a class="scroll" href="#descriptionId" style="color: black;">Product Descripition</a><span style="float: right;">{{amazonProductInfo.descriptionScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.descriptionScore}}%;background: {{getColorByScore(amazonProductInfo.descriptionScore)}};">
                                </div>
                            </div>
                            <h5 class="Bold line-gap"><a class="scroll" href="#featuresId" style="color: black;">Product Features</a><span style="float: right;">{{amazonProductInfo.featuresScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{amazonProductInfo.featuresScore}}%;background: {{getColorByScore(amazonProductInfo.featuresScore)}};">
                                </div>
                            </div>
                            <h5 class="Bold line-gap"><a class="scroll" href="#imagesId" style="color: black;">Product Images</a><span style="float: right;">{{amazonProductInfo.imageCountScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{amazonProductInfo.imageCountScore}}%;background: {{getColorByScore(amazonProductInfo.imageCountScore)}};">
                                </div>
                            </div>
                            <h5 class="Bold line-gap" id="ratingLabel"><a class="scroll" href="#ratingId" style="color: black;">Product Ratings</a><span style="float: right;">{{amazonProductInfo.ratingScore}}/100</span></h5>
                            <div class="progress progress-radius-bg" id="ratingProgress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{amazonProductInfo.ratingScore}}%;background: {{getColorByScore(amazonProductInfo.ratingScore)}};">
                                </div>
                            </div>
                            <h5 class="Bold line-gap" id="reviewLabel"><a class="scroll" href="#reviewsId" style="color: black;">Product Reviews</a><span style="float: right;">{{amazonProductInfo.reviewsScore}}/100</span></h5>
                            <div class="progress progress-radius-bg" id="reviewProgress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{amazonProductInfo.reviewsScore}}%;background: {{getColorByScore(amazonProductInfo.reviewsScore)}};">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-xs-12" style="margin-top: 2%;padding: 0;">
                    <div class="Bold"><h2>DETAILS & RECOMMENDATIONS</h2></div>
                    <!--<div>-->
                    <div class="  col-xs-12 mobile-padding-none lxrm-box-shadow " id="titleId" style="padding-top: 20px;">
                        <div  class=" col-lg-4  col-xs-12 col-sm-5 col-md-5" >
                            <h5 class="Bold" >Product Title<span style="float: right;">{{amazonProductInfo.titleScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.titleScore}}%;background: {{getColorByScore(amazonProductInfo.titleScore)}};">
                                </div>
                            </div>
                        </div>
                        <div class="  col-xs-12">
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered " style="min-width: 283px !important;">
                                    <thead> <tr><th class="col-xs-2">Title Length</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-xs-2" ng-switch="amazonProductInfo.title.length > 0">
                                                <span ng-switch-when="true">{{amazonProductInfo.title.length}}</span>
                                                <span ng-switch-default>0</span>
                                            </td>
                                            <td class="col-xs-2">190-210</td>
                                            <td class="col-xs-8 col-lg-8" >
                                                <span ng-switch="amazonProductInfo.title.length >= 190 && amazonProductInfo.title.length <= 210">
                                                    <span ng-switch-when="true">You have an optimized Title Length. 
                                                        Try to include more product related keywords that you are targeting and 
                                                        you will see more qualified traffic to your listing! 
                                                    </span>
                                                    <span ng-switch-default ng-switch="amazonProductInfo.title.length > 210">
                                                        <span ng-switch-when="true">Make your Title Length optimized. 
                                                            Try to include only product related keywords that you are targeting and 
                                                            you will see more qualified traffic to your listing!
                                                        </span>
                                                        <span ng-switch-default>You’re just getting started. 
                                                            You can add more details to the Title to increase it to optimized length. 
                                                            Try to include more product related keywords that you are targeting and 
                                                            you will see more qualified traffic to your listing!
                                                        </span>
                                                    </span>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div  class=" col-xs-12 mobile-padding-none  padding-parameter lxrm-box-shadow " id="descriptionId">
                        <div  class=" col-lg-4 col-xs-12 col-sm-5 col-md-5">
                            <h5 class="Bold">Product Description <span style="float: right;">{{amazonProductInfo.descriptionScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.descriptionScore}}%;background: {{getColorByScore(amazonProductInfo.descriptionScore)}};">

                                </div>
                            </div>
                        </div>
                        <div class=" col-xs-12">
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered " style="min-width: 283px !important;" >
                                    <thead> <tr><th class="col-xs-2">Description Length</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody>
                                        <tr>
                                            <td ng-switch="amazonProductInfo.description.length > 0">
                                                <span ng-switch-when="true">{{amazonProductInfo.description.length}}
                                                </span>
                                                <span ng-switch-default>0</span>
                                            </td>
                                            <td>1900-2100</td>
                                            <td ng-switch="amazonProductInfo.description.length >= 1900 && amazonProductInfo.description.length <= 2100">
                                                <span ng-switch-when="true">You have optimized Description length. Improve your searchability in the Amazon by including more product related keywords. 
                                                    This will help your customers understand about the product. You may see increased impressions and conversion rate 
                                                    if you let Amazon and your customers know more about your product.</span>
                                                <span ng-switch-default ng-switch="amazonProductInfo.description.length > 2100">
                                                    <span ng-switch-when="true">Make your Description length optimized.
                                                        Improve your searchability in the Amazon by including only product related keywords. 
                                                        This will help your customers understand about the product. You may see increased impressions and conversion rate 
                                                        if you let Amazon and your customers know more about your product </span>
                                                    <span ng-switch-default>
                                                        You’re just getting started. Improve your searchability in the Amazon
                                                        by including more text in Product Description as per recommended length. Additionally, 
                                                        this will help your customers understand about the product. 
                                                        You may see increased impressions and conversion rate if you let
                                                        Amazon and your customers know more about your product.</span>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div  class=" col-xs-12 mobile-padding-none padding-parameter lxrm-box-shadow " id="featuresId">
                        <div  class=" col-lg-4 col-xs-12 col-sm-5 col-md-5 ">
                            <h5 class="Bold">Product Features<span style="float: right;">{{amazonProductInfo.featuresScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.featuresScore}}%;background: {{getColorByScore(amazonProductInfo.featuresScore)}};">
                                </div>
                            </div>
                        </div>
                        <div class="  col-xs-12 "> 
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered" style="min-width: 283px !important;" >
                                    <thead> <tr><th class="col-xs-2 ">Total Features</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody><tr><td>{{amazonProductInfo.features}}</td><td>5 & above</td><td ng-switch="amazonProductInfo.features > 5"><span ng-switch-when="true">You have optimized number of features. Try to include better keywords that are likely to bring organic search results.</span><span ng-switch-default>Include as much detail as possible in separate bullet points that provide clarity about the Product features. Make sure to include your main keywords to improve your chances of ranking for the relevant search queries.</span></td></tr></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div  class=" col-xs-12 mobile-padding-none  padding-parameter lxrm-box-shadow " id="imagesId">
                        <div  class=" col-lg-4 col-xs-12 col-sm-5 col-md-5 ">
                            <h5 class="Bold">Product Images<span style="float: right;">{{amazonProductInfo.imageCountScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.imageCountScore}}%;background: {{getColorByScore(amazonProductInfo.imageCountScore)}};">
                                </div>
                            </div>
                        </div>
                        <div class=" col-xs-12">
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered" style="min-width: 283px !important;" >
                                    <thead> <tr><th class="col-xs-2">Total Images / Videos</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody><tr><td ng-switch="amazonProductInfo.videoCount > 0"><span ng-switch-when="true">{{amazonProductInfo.imagesCount}}+Video(s)</span><span ng-switch-default>{{amazonProductInfo.imagesCount}}</span></td><td>8+video(s)</td><td ng-switch="amazonProductInfo.imagesCount > 8 && amazonProductInfo.videoCount != 0"><span ng-switch-when="true">You have good number of images and product video.</span><span ng-switch-default>Take the  full advantage of one of the most important aspects of your product listing. You can add more images of your product from different views and possibly a video that helps customers understand the product better.</span></td></tr></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div  class=" col-xs-12 mobile-padding-none  padding-parameter lxrm-box-shadow " id="ratingId">
                        <div  class=" col-lg-4 col-xs-12 col-sm-5 col-md-5">
                            <h5 class="Bold">Product Rating<span style="float: right;">{{amazonProductInfo.ratingScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.ratingScore}}%;background: {{getColorByScore(amazonProductInfo.ratingScore)}};">
                                </div>
                            </div>
                        </div>
                        <div class="  col-xs-12">
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered" style="min-width: 283px !important;" >
                                    <thead> <tr><th class="col-xs-2">Current Ratings</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody><tr><td>{{amazonProductInfo.rating}}</td><td>4.5 & above</td><td ng-switch="amazonProductInfo.rating >= 4.5"><span ng-switch-when="true">Ratings can add confidence about the product to the potential customers. Encourage your customers to provide their ratings.</span><span ng-switch-default>Ratings can add confidence about the product to the potential customers. Encourage your customers to provide their ratings. If possible get the negative ratings removed.</span></td></tr></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 mobile-padding-none padding-parameter lxrm-box-shadow " id="reviewsId" >
                        <div  class=" col-lg-4 col-xs-12 col-sm-5 col-md-5">
                            <h5 class="Bold">Product Reviews<span style="float: right;">{{amazonProductInfo.reviewsScore}}/100</span></h5>
                            <div class="progress progress-radius-bg">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: {{amazonProductInfo.reviewsScore}}%; background: {{getColorByScore(amazonProductInfo.reviewsScore)}};">
                                </div>
                            </div>
                        </div>
                        <div class=" col-xs-12">
                            <div class="col-xs-12 " style="overflow-x: auto; padding: 0;">
                                <table class="table table-bordered " style="min-width: 283px !important;" >
                                    <thead> <tr><th class="col-xs-2">Total Reviews</th><th class="col-xs-2">Recommended</th><th class="col-xs-8 col-lg-8">Improvements Tips</th></tr></thead>
                                    <tbody><tr><td>{{amazonProductInfo.reviews}}</td><td>300 & above</td><td ng-switch="amazonProductInfo.reviews >= 300"><span ng-switch-when="true"> Reviews will improve your organic search rankings. Encourage your customers to provide reviews. Address all the negative reviews and complaints with proper response.</span><span ng-switch-default>Reviews will improve your organic search rankings. Encourage your customers to provide reviews. Address all the negative reviews and complaints with proper response.</span></td></tr></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--</div>-->
                </div>
            </div>
            <div class="col-xs-12 middleButton" style="float: right; text-align: center; margin-top: 80px;">
                <div id="img1" style="margin-bottom: 20px;"  class="col-sm-4"><img style="margin-bottom: 10px;" class="col-sm-12 lxrm-box-shadow" src="resources/images/amazonProductListing/amazonProductListing1.png" height="220px" width="300"><br><span><h4 class="lxrm-bold" >Validate First Time Feed</h4></span></div>
                <div id="img2" style="margin-bottom: 20px;"  class="col-sm-4"><img style="margin-bottom: 10px;" class="col-sm-12 lxrm-box-shadow" src="resources/images/amazonProductListing/amazonProductListing2.png" height="220px" width="300"><br><span><h4 class="lxrm-bold">Optimize For New Products</h4></span></div>
                <div id="img3" style="margin-bottom: 20px;" class="col-sm-4"><img style="margin-bottom: 10px;" class="col-sm-12 lxrm-box-shadow" src="resources/images/amazonProductListing/amazonProductListing3.png" height="220px" width="300"><br><span><h4 class="lxrm-bold">Monitor Impact Of Reviews & Ratings</h4></span></div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
