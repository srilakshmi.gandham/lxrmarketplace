
<%-- 
    Document   : urlAuditor
    Created on : 27 Oct, 2017, 11:10:40 AM
    Author     : NE16T1213-Sagar
--%>

<%@page import="java.util.List, java.util.ArrayList,lxr.marketplace.urlvalidator.domain.UrlValidator,lxr.marketplace.urlvalidator.domain.UrlStatus"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>URL Auditor Tool - Free Broken Link Checker | PPC Tool - URL Auditor Tool | LXR
            Marketplace</title>
        <meta name="description"
              content="Try the Free URL Auditor Tool at LXR Marketplace! Use this PPC tool to analyze your URLs &  determine the working condition of your landing pages. Try it now!">
        <meta name="keywords" content="ppc tools">
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/urlAuditor.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/css/tools/tooltabs.js"></script>
        <script src="/resources/js/tools/urlAuditor.js"></script>
        <% int toolDBId = 1;
            UrlValidator urlValidator = null;

            if (session.getAttribute("urlValidator") != null) {
                urlValidator = (UrlValidator) session.getAttribute("urlValidator");
            } else {
                urlValidator = new UrlValidator();
            }
            pageContext.setAttribute("urlValidator", urlValidator);

            ArrayList<UrlStatus> urlStatusDashBoardList = null;

            if (session.getAttribute("urlAuditiorStatusList") != null) {
                urlStatusDashBoardList = (ArrayList<UrlStatus>) session.getAttribute("urlAuditiorStatusList");
            } else {
                urlStatusDashBoardList = new ArrayList<>();
            }

            pageContext.setAttribute("urlStatusDashBoardList", urlStatusDashBoardList);
            String fileName = null;
            if (session.getAttribute("urlAuditiorStatusList") != null) {

            }
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var downloadReportType = "";
            var displayIndex = 0;
            displayIndex = "${selCol}";
            var selectedColName = null;
            selectedColName = "${selectedColName}";
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />

            function createUploader() {
                var oMyForm = new FormData();
                oMyForm.append("uploadedFile", uploadedFile.files[0]);
                $.ajax({
                    type: "POST",
                    url: '/url-auditor-tool.html?uploadFile=uploadFile',
                    data: oMyForm,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (responseJSON) {
                        $("#loadImage").hide();
                        $("#manualUrl, #url, #keyword").prop("readonly", false);
                        document.getElementById("uploadedFile").disabled = false;
                        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", false);
                        if (responseJSON === null || responseJSON === "" || responseJSON === "[object Object]") {
                            if (!toolLimitExceeded()) {
                                return false;
                            }
                            $(".qq-upload-list").html("");
                            return false;
                        } else {
                            $('#destUrlCol').find('option').remove().end();
                            $('.qq-upload-list').html("");
                            $('#selColumn').html("");
                            if (responseJSON.length !== 0) {
                                var columnList = responseJSON.toString();
                                var columns = columnList.split(",");
                                $('#selColumn').html("");
                                $("#fileUploadPopupModal").modal('show');
                                $("#destUrlCol").show();
                                $.each(columns, function (val1, text) {
                                    if (text !== null) {
                                        selectedURL = text;
                                    }
                                    $('#destUrlCol').append($('<option></option>').val(val1).html(text));
                                    displayIndex = 0;
                                    selectedColName = null;
                                });
                                return;
                            } else if (responseJSON.length === 0) {
                                alert("Please upload URL(s) list");
                            }
                        }/*EOF of Else*/
                    }, error: function (jqxhr, textStatus, error) {
                        $("#loadImage").hide();
                        if (textStatus.indexOf('error') !== -1) {
                            toolLimitExceededForAjax();
                        }
                    }, complete: function () {
                        $("#manualUrl, #url, #keyword").prop("readonly", false);
                        document.getElementById("uploadedFile").disabled = false;
                        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", false);
                    },
                    messages: {
                        typeError: "Please Upload .csv, .tsv, .xls, or .xlsx file.",
                        sizeError: "Exceeding File Size Limit",
                        minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                        emptyError: "{file} is empty, please select files again without it.",
                        onLeave: "The file are being uploaded, if you leave now the upload will be cancelled."
                    },
                    showMessage: function (message) {
                        alert(message);
                    }
                });
            }
            function printSelectedColumn(req) {
                if (req !== 2) {
                    $('#destUrlCol').find('option').remove().end();
            <%ArrayList<String> columnList = (ArrayList<String>) session.getAttribute("columnList");
                if (columnList != null) {
                    for (int i = 0; i < columnList.size(); i++) {
                        out.println("$('#destUrlCol').append( $('<option></option>').val('" + i + "').html('" + columnList.get(i) + "') )");
                    }
                }
                int previousSelected = 0;
                if (session.getAttribute("selCol") != null) {
                    previousSelected = (Integer) session.getAttribute("selCol");
                }
            %>
                    $('#destUrlCol').val(<%= previousSelected%>);
                }
                if (req === 2) {
                    displayIndex = 0;
                    selectedColName = null;
                }
                if ($("#destUrlCol option").text()) {
                    var selectCol = $("#destUrlCol option:selected");
                    var colName = null;
                    colName = selectCol.text();

                    if ((selectedColName !== "") && (selectedColName !== null)) {
                        colName = selectedColName;
                    }

                    var ind = selectCol.index();
                    var column = "Column ";
                    if (displayIndex !== 0) {
                        ind = 0;
                        ind = parseInt(displayIndex);
                    }
                    if (ind > 26) {
                        ind -= 26;
                        column = "Column A";
                    }
                    var col = column + String.fromCharCode(ind + 65) + " : ";
                    var finaltext = col + colName;
                    $("#selColumn").html(finaltext);
                    if (req !== 2) {
            <% if (urlValidator != null) {
                    fileName = urlValidator.getSelFile();
                    if (fileName != null && !(fileName.trim().equals(""))) {
                        int ind = fileName.lastIndexOf(".");
                        String ext = fileName.substring(ind, fileName.length());
                        if (fileName.length() > 20) {
                            fileName = fileName.substring(0, 13) + ".." + ext;
                        }
                        String finalStr = "\"<li><span class='qq-upload-size' style='display: inline;'> " + fileName + "</span></li>\"";
                        out.println("$('.qq-upload-list').html(" + finalStr + ")");
                    }
            %>
            <%}%>
                    }
                }
            }
            $(document).ready(function () {
                //Tool tip
                $('[data-toggle="tooltip"]').tooltip();
                /*  auto download when user logged in after   */
                if (parentUrl.indexOf("report-type") !== -1 && parentUrl.indexOf("report") !== -1) {
                    downloadFormat("${downloadReportType}");
                }
            <c:if test="${result ne 'success' || not fn:contains(uri, 'backToSuccessPage=')}">
                $('#headerRow').attr('checked', false);
            </c:if>
            <c:set var="allUrlsStatus" value="0"></c:set>
            <c:if test="${(result.equals('show') || fn:contains(uri, 'backToSuccessPage=') || (fn:length(urlStatusDashBoardList) > 0)) 
                          && (urlStatusDashBoardList != null && urlStatusDashBoardList != '')}">
                <c:choose>
                    <c:when test="${urlValidator.notWorkingURLCount > 0}">
                showData('404', 'result-content-1', 'result-heading-1');
                $(".lxrtool-tabsheading1").addClass("active");
                $(".main-content-1").addClass("active");
                    </c:when>
                    <c:when test="${urlValidator.temporaryRedirectingCount > 0}">
                showData('302', 'result-content-2', 'result-heading-2');
                $(".lxrtool-tabsheading2").addClass("active");
                $(".main-content-2").addClass("active");
                    </c:when>
                    <c:when test="${urlValidator.permanentRedirectingCount > 0}">
                showData('301', 'result-content-3', 'result-heading-3');
                $(".lxrtool-tabsheading3").addClass("active");
                $(".main-content-3").addClass("active");
                    </c:when>
                    <c:when test="${urlValidator.workingURLCount > 0}">
                showData('200', 'result-content-4', 'result-heading-4');
                $(".lxrtool-tabsheading4").addClass("active");
                $(".main-content-4").addClass("active");
                    </c:when>
                    <c:when test="${urlValidator.noURLStatusCount > 0}">
                showData('0', 'result-content-5', 'result-heading-5');
                $(".lxrtool-tabsheading5").addClass("active");
                $(".main-content-5").addClass("active");
                    </c:when>
                    <c:otherwise>
                        <c:if test="${fn:length(urlStatusDashBoardList) > 0}">
                showData('all', 'result-content-6', 'result-heading-6');
                $(".lxrtool-tabsheading6").addClass("active");
                $(".main-content-6").addClass("active");
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:if>
                $(".active").css("display", "block");
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();
                $('#manualUrl').keyup(function () {
                    resetToolInputFields();
                });
                $("#manualUrl").bind('input paste', function (e) {
                    if (e.keyCode === undefined) {
                        resetToolInputFields();
                    }
                });
                if (!($.trim($("#manualUrl").val()))) {
                    printSelectedColumn(1);
                }
//                createUploader();
                $("#uploadedFile").change(function () {
                    if (($.trim($("#manualUrl").val()))) {
                        var response = confirm('Manually entered URL(s) needs to be cleared before uploading the file.\nDo you want to clear now ?');
                        if (response === true) {
                            $('#manualUrl').val("");
                        } else {
                            $("#uploadedFile").val('');
                            $("#fileName, #selColumn").empty();
                            return false;
                        }
                    }
                    var fileName = $("#uploadedFile").val();
                    if (fileName === null || fileName === "") {
                        document.getElementById('fileUploadError').textContent = "Please upload a file.";
                        document.getElementById('fileUploadError').style.display = 'block';
                        removeFileContent();
                        return false;
                    } else if (!(fileName.endsWith(".csv") || fileName.endsWith(".tsv") || fileName.endsWith(".xls") || fileName.endsWith(".xlsx"))) {
                        document.getElementById('fileUploadError').textContent = "Please upload a csv or tsv or xls or xlsx file.";
                        document.getElementById('fileUploadError').style.display = 'block';
                        removeFileContent();
                        return false;
                    } else {
                        document.getElementById('fileUploadError').style.display = 'none';
                    }
                    fileName = fileName.replace("C:\\fakepath\\", "");
                    if (fileName.length > 50) {
                        fileName = fileName.substr(0, 5) + "..." + fileName.substring(fileName.length - 6, fileName.length);
                    }
                    $("#fileName").html(fileName);
                    $("#loadImage").show();

                    $("#manualUrl, #url, #keyword").prop("readonly", true);
                    document.getElementById("uploadedFile").disabled = true;
                    $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", "disabled");
                    createUploader();
                });
                $('#getResult').click(function () {
                    resultLoginValue = true;
                    getResultFun();
                });
                $('#resetURL').click(function () {
                    resetToolInputs();
                });
                $('#submitColumn').click(function () {
                    printSelectedColumn(2);
                    $("#fileUploadPopupModal").modal('hide');
                    $('#resetURL').attr('disabled', false);
                });
                $("#selColumn").click(function () {
                    var content = $('#selColumn').text();
                    if (content !== null && content !== "") {
                        $("#fileUploadPopupModal").modal('show');
                    }
                    $('#resetURL').attr('disabled', false);
                });

                $("#fileName").click(function () {
                    var content = $('#selColumn').text();
                    if (content === null || content === "") {
                        $("#fileUploadPopupModal").modal('show');
                    }
                    $('#resetURL').attr('disabled', false);
                });

                $("#keyword").keypress(function (e) {
                    return enterKey(e);
                });
                $("#sendReport").bind('click', function (event) {
                    $('#mail-sent-status').hide();
                    var emailid = $.trim($("#email").val());
                    var status = true;
                    var statusMsg = "";
                    if (emailid === "") {
                        statusMsg = "Please enter your email id";
                        status = false;
                    } else if (!isValidMail(emailid)) {
                        statusMsg = "Please enter a valid email address.";
                        status = false;
                    }
                    if (!status) {
                        $("#email").focus();
                        $('#mail-sent-status').show();
                        $("#mail-sent-status").html(statusMsg);
                        $('#mail-sent-status').css("color", "red");
                        return false;
                    } else {
                        $('#mail-sent-status').show();
                        $('#mail-sent-status').html("sending...");
                        $('#mail-sent-status').css("color", "green");
                        $.ajax({
                            type: "POST",
                            url: "/url-auditor-tool.html?download='sendmail'&email=" + emailid,
                            processData: true, data: {},
                            dataType: "json",
                            success: function (data) {
                            }, complete: function () {
                                $('#mail-sent-status').show();
                                $('#mail-sent-status').html("Mail sent successfully");
                                $('#mail-sent-status').css("color", "green");
                                $('#mail-sent-status').show();
                            }
                        });
                    }
                });
                $(".sendmailbox").bind('click', function (event) {
                    event.preventDefault();
                    $('#sendmail').bind(event);
                });
                $('#submitColumn').bind('click', function () {
                    $("#fileUploadPopupModal").modal('hide');
                    $('#downloadButton').attr('disabled', false);
                    $('#sendmail').attr('disabled', false);
                    $('.sendmailbox').attr('disabled', false);
                    return false;
                });
                $("#closeFile").bind('click', function (event) {
                    $("#fileUploadPopupModal").modal('hide');
                    event.preventDefault();
                    var existedString = $("#selColumn").html();
//                    if ($("#selColumn").is(':empty')) {
                    if (existedString.contains("Column")) {
                        $("#uploadedFile").val("");
                        $('#keyword').val("");
                        $('#keyword').empty();
                        $('#destUrlCol').empty().append('whatever');
                    }
                    $('#resetURL').attr('disabled', false);
                });
            }); /*EOF on ready*/
            function showData(statusCode, divId, pId) {
                var statusMessage = "";
                var color = "";
                var resultInfo = "";
                var urlsCount = "";
                if (statusCode === "404") {
                    statusMessage = "Page Not Found";
                    color = "red";
                    urlsCount = "${urlValidator.notWorkingURLCount}";
                } else if (statusCode === "302") {
                    statusMessage = "Temporary Redirection(s)";
                    color = "#F2C94C";
                    urlsCount = "${urlValidator.temporaryRedirectingCount}";
                } else if (statusCode === "301") {
                    statusMessage = "Permanent Redirection(s)";
                    color = "green";
                    urlsCount = "${urlValidator.permanentRedirectingCount}";
                } else if (statusCode === "200") {
                    statusMessage = "Working";
                    color = "green";
                    urlsCount = "${urlValidator.workingURLCount}";
                } else if (statusCode === "0") {
                    statusMessage = "No Status URL(s)";
                    color = "#EB5757";
                    urlsCount = "${urlValidator.noURLStatusCount}";
                } else {
                    resultInfo += showData('404', '', '');
                    resultInfo += showData('302', '', '');
                    resultInfo += showData('301', '', '');
                    resultInfo += showData('200', '', '');
                    resultInfo += showData('0', '', '');
                    color = "#2F80ED";
                }
                var tabDefination = urlsCount + ' URL(s) with '
                        + '<span style="color: ' + color + ';">' + statusCode + '</span>'
                        + '<span>&nbsp;&nbsp;&nbsp;STATUS MESSAGE: </span> '
                        + '<span style="color: ' + color + ';">' + statusMessage + '</span>';
            <c:if test="${urlStatusDashBoardList != null}">
                if ((divId !== "" && ($("#" + divId).html() === "") && statusCode !== "all")
                        || (divId === "" && statusCode !== "all")) {
                    resultInfo += commonTableHeader();
                    var urlInfo = "";
                <c:set var="parentCount" value="${1}"/>
                <c:forEach items="${urlStatusDashBoardList}" var="lxrmUrlAuditor">
                    /*Handling status code 200,301,302 || 400 and above i.e 503*/
                    if (("${lxrmUrlAuditor.statusCode}" === statusCode) || (statusCode === "404" && (${lxrmUrlAuditor.statusCode} >= 400 && ${lxrmUrlAuditor.statusCode} <= 600))) {
                        urlInfo += '<tr>';
                        urlInfo += '<td><a href="${lxrmUrlAuditor.inputUrl}" target="_blank">${lxrmUrlAuditor.inputUrl}</a></td>';
                        urlInfo += '<td>';
                    <c:choose>
                        <c:when test="${lxrmUrlAuditor.chainedRedirectsMap ne null && not empty lxrmUrlAuditor.chainedRedirectsMap}">
                        urlInfo += '<span>${lxrmUrlAuditor.chainedRedirectsMap.size()}</span>';
                        urlInfo += '<span id="urlParent${parentCount}" onclick="showSubTd(${parentCount})"> &nbsp;&nbsp;<i class="fa fa-plus-square expand" aria-hidden="true"></i></span>';
                        </c:when>
                        <c:when test="${empty lxrmUrlAuditor.chainedRedirectsMap}">
                        urlInfo += '<span>${lxrmUrlAuditor.chainedRedirectsMap.size()}</span>';
                        </c:when>
                    </c:choose>
                        urlInfo += '</td>';
                    <c:choose>
                        <c:when test="${lxrmUrlAuditor.statusCode < 400 && not fn:containsIgnoreCase(lxrmUrlAuditor.destinationURL, 'Connection')}">
                        urlInfo += '<td><a href="${lxrmUrlAuditor.destinationURL}" target="_blank">${lxrmUrlAuditor.destinationURL}</a></td>';
                        </c:when>
                        <c:when test="${(lxrmUrlAuditor.statusCode >= 400 && lxrmUrlAuditor.statusCode <= 600 )|| lxrmUrlAuditor.statusCode == 0}">
                        urlInfo += '<td style="text-align:center;"><span><strong>-</strong></span></td>';
                        </c:when>
                        <c:when test="${fn:containsIgnoreCase(lxrmUrlAuditor.destinationURL, 'Connection')}">
                        urlInfo += '<td>${lxrmUrlAuditor.destinationURL}</td>';
                        </c:when>
                    </c:choose>
                        urlInfo += '<td>${lxrmUrlAuditor.finalStatusCode}</td>';
                        urlInfo += '<td>${lxrmUrlAuditor.keywordCount}</td>';
                        urlInfo += '</tr>';
                    <c:if test="${lxrmUrlAuditor.chainedRedirectsMap ne null && not empty lxrmUrlAuditor.chainedRedirectsMap}">
                        <c:forEach var="chainedRedirectsMapEntry" items="${lxrmUrlAuditor.chainedRedirectsMap}">
                        urlInfo += '<tr class="urlChild${parentCount} chainedRedirections">';
                        urlInfo += '<td><a href="${chainedRedirectsMapEntry.key}" target="_blank">${chainedRedirectsMapEntry.key}</a></td>';
                        urlInfo += '<td colspan="5" style="text-align: center;">${chainedRedirectsMapEntry.value}</td>';
                        urlInfo += '</tr>';
                        </c:forEach>
                    </c:if>
                    <c:set var="parentCount" value="${parentCount + 1}" scope="page"/>
                    }
                </c:forEach>
                    resultInfo += urlInfo;
                    resultInfo += '</tbody>'
                            + '</table>';
                    if (urlInfo === "") {
                        resultInfo = "";
                    }
                }
                if (divId !== "" && $("#" + divId).html() === "") {
                    $("#" + divId).html(resultInfo);
                    $("#" + pId).html(tabDefination);
                }
            </c:if>
                return resultInfo;
            }
            function getCompleteAudit() {
                var completeAudit = "";
                completeAudit += commonTableHeader();
            <c:if test="${urlValidator.notWorkingURLCount ge 1}">
                completeAudit += getStatusCodeRows('404');
            </c:if>
            <c:if test="${urlValidator.temporaryRedirectingCount ge 1}">
                completeAudit += getStatusCodeRows('302');
            </c:if>
            <c:if test="${urlValidator.permanentRedirectingCount ge 1}">
                completeAudit += getStatusCodeRows('301');
            </c:if>
            <c:if test="${urlValidator.workingURLCount ge 1}">
                completeAudit += getStatusCodeRows('200');
            </c:if>
            <c:if test="${urlValidator.noURLStatusCount ge 1}">
                completeAudit += getStatusCodeRows('0');
            </c:if>
                completeAudit += '</tbody></table>';
                $("#result-content-6").empty();
                $("#result-content-6").html(completeAudit);
            }

            function getStatusCodeRows(statusCode) {
                var statusMessage = "";
                var color = "";
                var urlsCount = "";
                if (statusCode === "404") {
                    statusMessage = "Page Not Found";
                    color = "red";
                    urlsCount = "${urlValidator.notWorkingURLCount}";
                } else if (statusCode === "302") {
                    statusMessage = "Temporary Redirection(s)";
                    color = "#F2C94C";
                    urlsCount = "${urlValidator.temporaryRedirectingCount}";
                } else if (statusCode === "301") {
                    statusMessage = "Permanent Redirection(s)";
                    color = "green";
                    urlsCount = "${urlValidator.permanentRedirectingCount}";
                } else if (statusCode === "200") {
                    statusMessage = "Working";
                    color = "green";
                    urlsCount = "${urlValidator.workingURLCount}";
                } else if (statusCode === "0") {
                    statusMessage = "No Status URL(s)";
                    color = "#EB5757";
                    urlsCount = "${urlValidator.noURLStatusCount}";
                }
                var rowDefination = '<span style="color: ' + color + ';font-family: ProximaNova-Bold;">' + urlsCount + '</span> URL(s) with '
                        + '<span style="color: ' + color + ';font-family: ProximaNova-Bold;">' + statusCode + '</span>'
                        + '<span>&nbsp;&nbsp;&nbsp;STATUS MESSAGE: </span> '
                        + '<span style="color: ' + color + ';font-family: ProximaNova-Bold;">' + statusMessage + '</span>';
            <c:if test="${urlStatusDashBoardList != null}">
                var urlInfo = "";
                urlInfo += '<tr><td colspan="5" >' + rowDefination + '</td></tr>';
                <c:set var="parentCount" value="${1}"/>
                <c:forEach items="${urlStatusDashBoardList}" var="lxrmUrlAuditor">
                if (("${lxrmUrlAuditor.statusCode}" === statusCode) || (statusCode === "404" && (${lxrmUrlAuditor.statusCode} >= 400 && ${lxrmUrlAuditor.statusCode} <= 600))) {
                    urlInfo += '<tr>';
                    urlInfo += '<td><a href="${lxrmUrlAuditor.inputUrl}" target="_blank">${lxrmUrlAuditor.inputUrl}</a></td>';
                    urlInfo += '<td>';
                    <c:choose>
                        <c:when test="${lxrmUrlAuditor.chainedRedirectsMap ne null && not empty lxrmUrlAuditor.chainedRedirectsMap}">
                    urlInfo += '<span>${lxrmUrlAuditor.chainedRedirectsMap.size()}</span>';
                    urlInfo += '<span id="urlParent${parentCount}" onclick="showSubTd(${parentCount})"> &nbsp;&nbsp;<i class="fa fa-plus-square expand" aria-hidden="true"></i></span>';
                        </c:when>
                        <c:when test="${empty lxrmUrlAuditor.chainedRedirectsMap}">
                    urlInfo += '<span>${lxrmUrlAuditor.chainedRedirectsMap.size()}</span>';
                        </c:when>
                    </c:choose>
                    urlInfo += '</td>';
                    <c:choose>
                        <c:when test="${lxrmUrlAuditor.statusCode < 400 && not fn:containsIgnoreCase(lxrmUrlAuditor.destinationURL, 'Connection')}">
                    urlInfo += '<td><a href="${lxrmUrlAuditor.destinationURL}" target="_blank">${lxrmUrlAuditor.destinationURL}</a></td>';
                        </c:when>
                        <c:when test="${(lxrmUrlAuditor.statusCode >= 400 && lxrmUrlAuditor.statusCode <= 600 )|| lxrmUrlAuditor.statusCode == 0}">
                    urlInfo += '<td style="text-align:center;"><span><strong>-</strong></span></td>';
                        </c:when>
                        <c:when test="${fn:containsIgnoreCase(lxrmUrlAuditor.destinationURL, 'Connection')}">
                    urlInfo += '<td>${lxrmUrlAuditor.destinationURL}</td>';
                        </c:when>
                    </c:choose>
                    urlInfo += '<td>${lxrmUrlAuditor.finalStatusCode}</td>';
                    urlInfo += '<td>${lxrmUrlAuditor.keywordCount}</td>';
                    urlInfo += '</tr>';
                    <c:if test="${lxrmUrlAuditor.chainedRedirectsMap ne null && not empty lxrmUrlAuditor.chainedRedirectsMap}">
                        <c:forEach var="chainedRedirectsMapEntry" items="${lxrmUrlAuditor.chainedRedirectsMap}">
                    urlInfo += '<tr class="urlChild${parentCount} chainedRedirections">';
                    urlInfo += '<td><a href="${chainedRedirectsMapEntry.key}" target="_blank">${chainedRedirectsMapEntry.key}</a></td>';
                    urlInfo += '<td colspan="5" style="text-align: center;">${chainedRedirectsMapEntry.value}</td>';
                    urlInfo += '</tr>';
                        </c:forEach>
                    </c:if>
                    <c:set var="parentCount" value="${parentCount + 1}" scope="page"/>
                }
                </c:forEach>
            </c:if>
                return urlInfo;
            }
            function commonTableHeader() {
                var toolHeader = '<table class="table table-striped lxrtool-table" style="word-wrap: break-word;;">'
                        + '<thead>'
                        + '<tr>'
                        + '<th class="col-xs-3" style="border-right: 1.5px white solid;">BASE URL</th>'
                        + '<th class="col-xs-2" style="border-right: 1.5px white solid;"># of Redirects</th>'
                        + '<th class="col-xs-3" style="border-right: 1.5px white solid;">FINAL DESTINATION </th>'
                        + '<th class="col-xs-2" style="border-right: 1.5px white solid;">Final HTTP Status</th>'
                        + '<th class="col-xs-2" style="border-right: 1.5px white solid;">Keyword Count </th>'
                        + '</tr>'
                        + '</thead>'
                        + '<tbody>';
                return toolHeader;
            }
            function showSubTd(childID) {
                $('.urlChild' + childID + '').toggle("slow", "linear");
                console.log("#urlParent" + childID);
                $('#urlParent' + childID + '').find('i').toggleClass('fa-plus-square fa-minus-square');
            }
            function resetToolInputs() {
                $('#manualUrl').val("");
                $('#keyword').val("");
                $('#selColumn').html("");
                $('.qq-upload-list').html("");
                $("#uploadedFile").val("");
                $('#headerRow').attr('checked', false);
            <% if (urlValidator != null) {
                    out.println("$('.qq-upload-list').html('')");
                }%>
                var form = $("#urlValidator");
                form.attr('action', "/url-auditor-tool.html?clearAll='clearAll'&fileField='testFile'");
                form.submit();
            }
            function downloadFormat(reportType) {
                if (userLoggedIn) {
                    var form = $("#urlValidator");
                    form.attr('method', "POST");
                    form.attr('action', "/url-auditor-tool.html?download=" + reportType);
                    form.submit();
                } else {
                    reportDownload = 1;
                    downloadReportType = reportType;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            //Ask Expert Block
            function getToolRelatedIssues() {}
            function getUserToolInputs(website, userInput) {
                $('#manualUrl').val(website);
                $('#getResult').click();
            }
            function valiDateFileSize() {
                var fileValidation = true;
                var uploadFile = document.getElementById("uploadedFile").files[0];
                if (uploadFile.size >= 10000000) {
                    alert("Uploaded file is too large");
                    fileValidation = false;
                }
                return fileValidation;
            }

            function submitPreviousRequest() {
                $("#getResult").click();
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <div class="container">
            <springForm:form commandName="urlValidator" method="POST" enctype="multipart/form-data">
                <input type="hidden" id="fileField" name="fileField" value="" />
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <label class="control-label">Enter your URL(s):
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                               title="Enter up to 100 URLs manually in the text box below. Enter one URL per line.">
                                <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                            </a>
                        </label>
                        <springForm:textarea path="manaullyURL" id="manualUrl" cssClass="form-control col-xs-9" rows="7" placeHolder="Enter your URL(s) one per line (maximum 100 allowed)" style="resize:none;"/>
                    </div>
                    <p id="toolInfoMessageWrapper" class="lxrm-bold" style="padding: 3% 0 0 0"> LXRMarketplace User-Agent (search engine bot) is used to check HTTP status codes  </p>
                    <p id="inputUrlError" class="error"></p>
                </div>
                <div class="clearfixdiv visible-xs visible-sm"></div>
                <div class="col-xs-12 col-sm-12 col-md-1 text-center lxrm-bold" style="font-size: 1.6em;">(OR)</div>
                <div class="clearfixdiv visible-xs visible-sm"></div>
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="row col-lg-12  normal-styles">
                        <label style="padding-left: 0;" class="uploadLabel">Upload your URL(s): 
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                               title="Upload list of 100 URLs you want to test in bulk. You can also directly upload Google Adwords or MSN Adcenter Keyword Report and get results in same format.">
                                <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-10 col-md-11" style="padding-left: 0;">
                        <div class="col-xs-12" style="padding-left: 0;">
                            <label for="uploadedFile" id="browseFile" class="btn btn-primary lxr-subscribe">Browse File</label>
                            <span id="loadImage" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-refresh fa-spin" aria-hidden="true"></i></span>
                            <input type="file" id="uploadedFile" name="uploadedFile" style="display: none;">
                        </div>
                        <div class="col-xs-12" style="padding-left: 0;">
                            <p id="fileName">${urlValidator.selFile}</p>
                            <p>(Only in csv or tsv or xls or xlsx format)</p>
                            <p id="selColumn"></p>
                            <p id="fileUploadError" class="error">Please upload a csv or tsv or xls or xlsx file.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="control-label col-xm-12 col-sm-12 col-md-3" style="padding-left: 0;font-size: 18px;">Search Term (Optional):
                            <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" 
                               title="Checks if the term is present on the link/page and also gives the number of times it appears. The result appears in the final output report that you download.">
                                <i class="fa fa-info-circle fa-1x lxrm-tool-help" aria-hidden="true"></i>
                            </a>
                        </label>
                        <div class="col-xs-12 col-sm-12 col-md-3" style="padding-left: 0">
                            <springForm:input path="keyword" cssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <!--<div class="row" align="center">-->
                <div class="col-xs-12">
                    <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12"  id="resetURL">
                                <i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL
                            </button>
                        </div>
                    </div>
                    <div class="clearfixdiv visible-xs" style="height:8px;"></div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 text-center" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn top-line btn-lg buttonStyles col-xs-12 col-sm-12 col-md-12 col-lg-12 lxr-search-btn"  id="getResult" data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> ANALYZING..">
                                &nbsp;ANALYZE&nbsp;
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 visible-md visible-lg"></div>
                </div>
                <div id="fileUploadPopupModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content" style="top: 110px;">
                            <div class="modal-header">
                                <button type="button" id="closeFile" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Match Column To Fields</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center lxrm-regular" style="width:100%;font-size: 1.2em;"> Source Column </p>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label text-center" style="line-height: 36px;">Destination URL:</label>
                                    <div class="col-lg-8  text-left">
                                        <div class="col-lg-12">
                                            <springForm:select id="destUrlCol" path="destUrlCol" cssClass="form-control input-lg srch-term">
                                                <springForm:options items="${columnList}" />
                                            </springForm:select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfixdiv" style="height: 10px;"></div>
                                <p class="lxrm-regular" style="width:100%;font-size: 1.2em;"><span style="color: green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span> Indicate which file column should go in the destination URL field </p>
                                <div class="checkbox">
                                    <label style="font-size: 1.2em;">
                                        <springForm:checkbox id="headerRow" path="headerRow" /> Check if first row in file is header
                                    </label>
                                </div>
                            </div>
                            <div class="modal-footer" style="padding: 6px;">
                                <button type="button" class="btn top-line btn-lg buttonStyles lxr-search-btn" data-dismiss="modal" id="submitColumn">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--</div>-->
            </springForm:form>
        </div>
        <div class="clearfixdiv"></div>
        <c:if test="${(result.equals('show') || fn:contains(uri, 'backToSuccessPage=')) 
                      && (urlStatusDashBoardList != null && urlStatusDashBoardList != '')}">
              <div class="container-fluid">
                  <div class="row lxrtool-tabs">
                      <div class="tabs">
                          <c:if test="${urlValidator.notWorkingURLCount > 0}">
                              <c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>
                                  <div class="tab" onclick="showData('404', 'result-content-1', 'result-heading-1');">
                                      <button class="tab-toggle active lxrtool-tabsheading1 lxrtool-tabs-heading">
                                          <span class="lxr-danger">${urlValidator.notWorkingURLCount} </span> <span class="lxr-danger">NONWORKING URL(s)</span>
                                      <p>
                                          The HTTP 404 Not Found Error means that the web page you were trying to reach could not be found on the server. It is a Client-side Error which means that either the page has been removed or moved.
                                      </p>
                                  </button>
                              </div>
                              <!--table-->
                              <div class="content main-content-1 active">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-1">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-danger">NONWORKING URL(s)</span>
                                              <p id="issue-text-1">
                                                  404 errors can hurt your website organic rankings because 
                                                  it delivers a bad User-experience to the visitor.
                                                  There are three <strong class="nonworkingurl" onClick ="hideContentAndShowHowToFix(1);">METHODS</strong> you can use to fix 404 not found errors for your website.
                                              </p>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p id="result-heading-1"></p>
                                              <div id="result-content-1"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--how to fix-->
                                  <div id="howtofix-1" style="display: none;">
                                      <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                          <div class="panel panel-default">
                                              <div class="panel-heading lxr-panel-heading">
                                                  <span class="panel-title lxr-light-danger">How to fix broken links</span>
                                                  <span class="glyphicon glyphicon-remove pull-right" id="closebox" onclick="hideHowToFixAndShowContent(1);"></span>
                                              </div>
                                              <div class="panel-body lxr-panel-body">
                                                  <h5>Steps for fixing broken links:</h5>
                                                  <p>1. Permanent 301 should be placed, if a page is important.</p>
                                                  <p>2. Correct the Source Link.</p>
                                                  <p>3. Restore Deleted Pages or redirect them to related category pages.</p>

                                                  <div class="lxr-panel-footer">
                                                      <h3><spring:message code="ate.expert.label.2" /></h3>
                                                      <button class="btn" onclick="submitATEQuery('1');">Get Help</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </c:if>
                          <c:if test="${urlValidator.temporaryRedirectingCount > 0}">
                              <c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>
                                  <div class="tab" onclick="showData('302', 'result-content-2', 'result-heading-2');">
                                      <button class="tab-toggle lxrtool-tabsheading2 lxrtool-tabs-heading">
                                          <span class="lxr-warning">${urlValidator.temporaryRedirectingCount} </span> <span class="lxr-warning">TEMPORARY REDIRECTION URL(s)</span>
                                      <p>
                                          302 is a temporary redirect, if you want to move your website URL on a temporary base, you can use this.
                                      </p>
                                  </button>
                              </div>
                              <div class="content main-content-2">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-2" style="padding: 0px;">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-warning">TEMPORARY REDIRECTION URL(s)</span>
                                              <p id="issue-text-2">
                                                  It’s quite dangerous to use a 302 redirect by mistake. 
                                                  It will really hurt your site’s search engine visibility. 
                                                  Follow the <strong class="total_url" onClick ="hideContentAndShowHowToFix(2);">STEPS</strong> to change temporary redirection.
                                              </p>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p id="result-heading-2"></p>
                                              <div id="result-content-2"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--how to fix-->
                                  <div  id="howtofix-2" style="display: none;">
                                      <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                          <div class="panel panel-default">
                                              <div class="panel-heading lxr-panel-heading">
                                                  <span class="panel-title lxr-light-danger">How to fix Temporary Redirections</span>
                                                  <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                              </div>
                                              <div class="panel-body lxr-panel-body">
                                                  <h5>Steps for fixing temporary redirections:</h5>
                                                  <p> Note: This is only used for the temporary redirect in case of emergency or technical difficulties for the domain.</p>
                                                  <p> It depends on your content management system or the platform which you are using, usually, it is done through access file by using 302 redirects.</p>
                                                  <div class="lxr-panel-footer">
                                                      <h3><spring:message code="ate.expert.label.2" /></h3>
                                                      <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                                                  </div>
                                              </div>
                                          </div>  
                                      </div>
                                  </div>
                              </div>
                          </c:if>
                          <c:if test="${urlValidator.permanentRedirectingCount > 0}">
                              <c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>
                                  <!--3-->
                                  <div class="tab" onclick="showData('301', 'result-content-3', 'result-heading-3');">
                                      <button class="tab-toggle lxrtool-tabsheading3 lxrtool-tabs-heading">
                                          <span class="lxr-success">${urlValidator.permanentRedirectingCount} </span> <span class="lxr-success">PERMANENT REDIRECTION URL(s)</span>
                                      <p>
                                          A 301 redirect is a permanent redirect. In most instances, 
                                          the 301 redirect is the best method for implementing redirects on a website.
                                      </p>
                                  </button>
                              </div>
                              <div class="content main-content-3">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-3">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-success">PERMANENT REDIRECTION URL(s)</span>
                                              <p id="issue-text-3">
                                                  If we will not do permanent redirection, so we may lose our ranking in SERP. 
                                                  When you have two or more URLs which are temporary redirected, 
                                                  These are the <strong  class="total_url" onClick ="hideContentAndShowHowToFix(3);">STEPS</strong> to make permanent redirection.
                                              </p>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p id="result-heading-3"></p>
                                              <div id="result-content-3"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--how to fix-->
                                  <div id="howtofix-3" style="display: none;">
                                      <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                          <div class="panel panel-default">
                                              <div class="panel-heading lxr-panel-heading">
                                                  <span class="panel-title lxr-light-danger">How to fix Temporary Redirections</span>
                                                  <span class="glyphicon glyphicon-remove pull-right" id="closebox3" onclick="hideHowToFixAndShowContent(3);"></span>
                                              </div>
                                              <div class="panel-body lxr-panel-body">
                                                  <h5>Steps for fixing temporary redirections:</h5>
                                                  <p> It depends on your content management system or the platform which you are using, usually, it is done through access file by using 301 redirects.</p>
                                                  <div class="lxr-panel-footer">
                                                      <h3><spring:message code="ate.expert.label.2" /></h3>
                                                      <button class="btn" onclick="submitATEQuery('3');">Get Help</button>
                                                  </div>
                                              </div>
                                          </div>  
                                      </div>
                                  </div>
                              </div>
                          </c:if>
                          <c:if test="${urlValidator.workingURLCount > 0}">
                              <c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>
                                  <!--4-->
                                  <div class="tab" onclick="showData('200', 'result-content-4', 'result-heading-4');">
                                      <button class="tab-toggle lxrtool-tabsheading4 lxrtool-tabs-heading">
                                          <span class="lxr-success">${urlValidator.workingURLCount} </span> <span class="lxr-success">WORKING URL(s)</span>
                                      <p>It contains working URL(s) with status code 200.</p>
                                  </button>
                              </div>
                              <div class="content main-content-4">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-4">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-success">WORKING URL(s)</span>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p id="result-heading-4"></p>
                                              <div id="result-content-4"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--how to fix-->
                                  <!--                                  <div id="howtofix-5" style="display: none;">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading lxr-panel-heading">
                                                                                    <span class="panel-title lxr-light-danger">How to fix Temporary Redirections</span>
                                                                                    <span class="glyphicon glyphicon-remove pull-right" id="closebox4" onclick="hideHowToFixAndShowContent(5);"></span>
                                                                                </div>
                                                                                <div class="panel-body lxr-panel-body">
                                                                                    <h5>Steps for fixing temporary redirections:</h5>
                                                                                    <p> It depends on your content management system or the platform which you are using, usually, it is done through .htaccess file by using 301 redirects.</p>
                                                                                    <div class="lxr-panel-footer">
                                                                                        <h3>
                                  <%--<spring:message code="ate.expert.label.2" />--%>
</h3>
                                                    <button class="btn" onclick="submitATEQuery('5');">Get Help</button>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>-->
                              </div>
                          </c:if>
                          <c:if test="${urlValidator.noURLStatusCount > 0}">
                              <c:set var="allUrlsStatus" value="${allUrlsStatus+1}"></c:set>
                                  <div class="tab" onclick="showData('0', 'result-content-5', 'result-heading-5');">
                                      <button class="tab-toggle lxrtool-tabsheading5 lxrtool-tabs-heading">
                                          <span class="lxr-light-danger">${urlValidator.noURLStatusCount} </span> <span class="lxr-light-danger">NO STATUS URL(s)</span>
                                      <p>
                                          Unable to find status code of a URL.
                                      </p>
                                  </button>
                              </div>
                              <div class="content main-content-5">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-5">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-light-danger">NO STATUS URL(s)</span>
                                              <!--                                              <p id="issue-text-5">
                                                                                                Too many redirections is a blackhat SEO strategy to rank keywords. 
                                                                                                Avoid too many redirections to save your site from the penalty. 
                                                                                                Follow the below <strong class="partially_notwork" onClick ="hideContentAndShowHowToFix(5);">STEPS</strong> to change too many redirections.
                                                                                            </p>-->
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <p id="result-heading-5"></p>
                                              <div id="result-content-5"></div>
                                          </div>
                                      </div>
                                  </div>

                                  <!--                                  how to fix
                                                                    <div  id="howtofix-2" style="display: none;">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12 panel-content">
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading lxr-panel-heading">
                                                                                    <span class="panel-title lxr-light-danger">How to fix broken links</span>
                                                                                    <span class="glyphicon glyphicon-remove pull-right" id="closebox2" onclick="hideHowToFixAndShowContent(2);"></span>
                                                                                </div>
                                                                                <div class="panel-body lxr-panel-body">
                                                                                    <h5>STEPS</h5>
                                                                                    <p> 
                                                                                        It depends on the website platform or content management system, 
                                                                                        but after identifying the redirects it is better to move those 
                                                                                        temporary or too many redirections to 1 permanent redirect or 301.
                                                                                    </p>
                                                                                    <div class="lxr-panel-footer">
                                                                                        <h3>
                                  <%--<spring:message code="ate.expert.label.2" />--%>
                                  </h3>
                                <button class="btn" onclick="submitATEQuery('2');">Get Help</button>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>-->
                              </div>
                          </c:if>
                          <c:if test="${allUrlsStatus > 1 || allUrlsStatus == 0}">
                              <!--5-->
                              <div class="tab" onclick="getCompleteAudit();">
                                  <button class="tab-toggle lxrtool-tabsheading6 lxrtool-tabs-heading">
                                      <span class="lxr-primary">${urlValidator.totalURLCount} </span> <span class="lxr-primary">TOTAL URL(s)</span>
                                      <p>It contains all status code URL(s) like Non-working URL(s), Temporary Redirections URL(s), Permanent Redirection URL(s) etc.</p>
                                  </button>
                              </div>
                              <div class="content main-content-6">
                                  <div class="col-md-12 col-sm-12 col-xs-12 panel-content" id="firstcontent-6">
                                      <div class="panel panel-default">
                                          <div class="panel-heading lxr-panel-heading">
                                              <span class="panel-title lxr-primary">TOTAL URL(s)</span>
                                          </div>
                                          <div class="panel-body lxr-panel-body">
                                              <div id="result-content-6"></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </c:if>
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row download-report-row">
                      <div class="col-lg-3 visible-lg visible-md" style="width: 30%;float: left;"></div>
                      <div class="col-md-9 col-sm-12 col-xs-12 download-report-div">
                          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 lxrm-padding-none" style="padding: 0;">
                              <p style="margin: 3% 0;">Download complete report here: </p>
                          </div>
                          <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10" style="padding: 0;">
                              <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Enter your email id" name="email" id="email" value="${userH.userName}">
                                  <div class="input-group-btn">
                                      <button class="btn btn-default" style="border: 1px #ccc solid !important;" id="sendReport"><i class="glyphicon glyphicon-send"></i></button>
                                  </div>
                              </div>
                              <div id="mail-sent-status" class="emptyData"></div>
                          </div>
                          <div class="col-md-1 col-sm-2 col-xs-2">
                              <div class="dropdown">
                                  <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                  <div id="downloadAnalysisReport" class="dropdown-content">
                                      <p onclick="downloadFormat('xls');" class="reportFormat">Excel (xls)</p>
                                      <p onclick="downloadFormat('xlsx');" class="reportFormat">Excel (xlsx)</p>
                                      <p onclick="downloadFormat('csv');" class="reportFormat">Text (csv)</p>
                                      <p onclick="downloadFormat('tsv');" class="reportFormat">Text (tsv)</p>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
        </c:if>
        <div class="clearfixdiv"></div>
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
