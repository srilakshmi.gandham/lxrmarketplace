<%-- 
    Document   : RobotsTxt Generator
    Created on : Sep 26, 2017, 4:44:49 PM
    Author     : NE16T1213-Sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Map, java.util.HashMap, java.util.Set"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description"
              content="Try the Robots.txt Generator at LXRMarketplace. Helps search engines crawl and indexing your website properly & create robots.txt file online. Try it today!">
        <meta name="keywords"
              content="Robots.txt Generator, Create Robots.txt, Robots.txt File, Robots Exclusion Protocol">
        <title>Robots.txt Generator | LXRMarketplace.com</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/robotsTxtGenerator.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <script src="/resources/js/tools/robotTextGenerator.js"></script>
        <!--<script src="/resources/css/tools/tooltabs.js"></script>-->
        <%
            int toolDBId = 24;
            Map<String, String> robotNamesMap = new HashMap<>();
            Set<String> robotKeySet = null;
        %>
        <script type="text/javascript">
            var reportDownload = 0;
            var isImportedForGenerator = true;
            var downloadReportType = "";
            var indexValStr = "";
            var robotsArray = new Array();
            var showExistedResult = false;
            showExistedResult =${showExistedResult};
            <c:set var="uri" value="${requestScope['javax.servlet.forward.query_string']}" />
            $(document).ready(function () {
                /*Functions Handling button click events*/
                $("#importRobots").bind('click', function () {
                    resultLoginValue = true;
                    isImportedForGenerator = true;
                    $("#toolQueryMessage").empty();
                    $("#toolQueryMessage").show();
                    var domain = $('#robotsTxtUrl').val();
                    if (domain === null || domain.trim() === "" || domain.trim().length === 0) {
                        $("#toolQueryMessage").html("Please enter your URL.");
                        $('#robotsTxtUrl').focus();
                        return false;
                    } else if (!urlValidation($('#robotsTxtUrl').val().trim())) {
                        $("#toolQueryMessage").html("Please enter a valid URL.");
                        $('#robotsTxtUrl').focus();
                        return false;
                    }
                    importRobotsRequest();
                });
                $("#messageWrapper, #invalidSiteMap").empty();
                $('[data-toggle="tooltip"]').tooltip();
                /*  Auto download when user logged*/
                if (parentUrl.indexOf("report") !== -1) {
                    downloadFormat();
                }
                /*Functions related to Page onLoad*/
                if (showExistedResult) {
                    /*Ajax Call to construct existed data for user who navigating from cancel page of ask our expert.*/
                    $('#robotsTxtUrl').val("${robotsURLATE}");
                    prepareRobots();
                    getRobotsTextData('showExistedResult', robotsArray);
                }
                /*   Login POPUP on tool limit exceeded    */
//                toolLimitExceeded();

                //Directly comes from tool advisor then prepopulating the data by default
                var toolAdvisor = getUrlVars()["tool"];
                if (toolAdvisor !== null && toolAdvisor === 'advisor') {
                    getUserToolInputs("${toolAdvisorDomain}", "");
                }

                enableButtons();

                $('#robotsTxtUrl').keypress(function (ev) {
                    if (ev.which === 13) {
                        $("#importRobots").click();
                        return false;
                    }
                });


                $("#getResult").bind('click', function (e) {
                    resultLoginValue = true;
                    isImportedForGenerator = false;
                    if (!toolLimitExceeded()) {
                        return false;
                    }
                    $("#toolQueryMessage, #userAgentError, #invalidSiteMap, #directiveError").empty();
                    $("#toolQueryMessage, #userAgentError, #directiveError").hide();
                    e.preventDefault();
                    var directorysArray = new Array();
                    var siteMap = "";
                    var finalSiteMap = "";
                    var showAlert = true;
                    var importDirectoryParent = document.getElementById("importDirectivesWrapper");
                    var manuallyDirectoryParent = document.getElementById("manuallyDirectivesWrapper");
                    var importDirectoryChildern = importDirectoryParent.getElementsByTagName('div');
                    var manuallyDirectoryChildern = manuallyDirectoryParent.getElementsByTagName('div');
                    if (importDirectoryChildern.length <= 0 && manuallyDirectoryChildern.length <= 0) {
                        $("#messageWrapper").html("Please add directives");
                        return false;
                    }
                    if (parentUrl.indexOf("limit-exceeded") !== -1 && userLoggedIn === 0) {
                        showAlert = false;
                    }
                    siteMap = $.trim($("#sitemapUrl").val());
                    if (showAlert) {
                        if (siteMap !== null && siteMap !== "") {
                            var sitemapArr = siteMap.split("\n");
                            for (var i = 0; i < sitemapArr.length; i++) {
                                var sitemapVal = $.trim(sitemapArr[i]);
                                if (i === 0) {
                                    finalSiteMap = finalSiteMap + sitemapVal;
                                } else {
                                    finalSiteMap = finalSiteMap + "," + sitemapVal;
                                }
                                if (sitemapVal.match('^(http://|https://)') === null) {
                                    $("#invalidSiteMap").html("Please enter proper sitemap URL");
                                    $("#sitemapUrl").focus();
                                    return false;
                                }
                            }
                        }
                    }

                    prepareJSONObjectofDirectoryList(directorysArray, 'import');
                    prepareJSONObjectofDirectoryList(directorysArray, 'manually');
                    disableButtons();
                    $.ajax({
                        type: "POST",
                        url: "/robots-txt-generator-tool.html?getResult=getResult",
                        processData: true,
                        data: {'siteMap': finalSiteMap, 'totRobotsArr': JSON.stringify(directorysArray)},
                        dataType: "json",
                        success: function (data) {
                            $("#robotTxtContent").val(data[0]);
                            $("#toolResultWrapper").show();
                            if ($.trim($("#robotTxtContent").val()) !== "") {
                                $('html, body').animate({scrollTop: $('#toolResultWrapper').position().top}, 'slow');
                            }
                            enableButtons();
                            /*Calling ask our expert function */
                            setTimeout(getAskExpertPopup(true), 2000);

                        }, error: function (jqxhr, textStatus, error) {
                            if (textStatus.indexOf('error') !== -1) {
                                toolLimitExceededForAjax();
                            }
                        },
                        complete: function (xhr, textstatus) {
                            enableButtons();
                        }
                    });
                });

                $("#clearDirectory").bind('click', function (e) {
                    e.preventDefault();
                    $('#domain').val("");
                    $("#toolResultWrapper").hide();
                    var form = $("#robotsTxtGeneratorTool");
                    form.attr('action', "robots-txt-generator-tool.html?clearAll='clearAll'");
                    form.submit();
                });
                $("#downloadReport").bind('click', function (e) {
                    downloadFormat();
                });
                $('#userAgentName').change(function () {
                    $("#messageWrapper").empty();
                    if ($(this).val() === "Specify") {
                        $("#specialUserAgentName").val("");
                        $("#specialAgentWrapper").show();
                    } else {
                        $("#specialUserAgentName").val("");
                        $("#specialAgentWrapper").hide();
                    }
                });
                /*To add user agent and directive fields manually. */
                $("#addDirective").click(function () {
                    $("#directiveError, #userAgentError").show();
                    $("#directiveError, #userAgentError, #messageWrapper").html('');
                    if ($.trim($("#userAgentName").val()) === "Specify") {
                        if ($.trim($("#specialUserAgentName").val()).length === 0) {
                            $("#userAgentError").html("Please specify user-agent name");
                            $("#addButtonWrapper").css('padding-bottom', '4%');
                            return false;
                        } else {
                            $("#addButtonWrapper").css('padding-bottom', '0%');
                        }
                    }
                    if ($("#directoryOrFilePath").val().search("/") !== 0) {
                        $("#directiveError").html("The url must begin with a / ");
                        $("#addButtonWrapper").css('padding-bottom', '4%');
                        return false;
                    } else {
                        $("#addButtonWrapper").css('padding-bottom', '0%');
                    }
                    $("#directiveError, #userAgentError").hide();
                    generateNewDirectory('manuallyDirectivesWrapper');
                });

                $("#robotsTxtUrl").focus(function () {
                    $("#robotsTxtUrl").removeClass('gray');
                    $("#edit-textbox").hide();
                });
                $("#edit-textbox, #robotsTxtUrl").click(function () {
                    $("#robotsTxtUrl").removeClass('gray');
                    $("#edit-textbox").hide();
                    $("#messageWrapper").empty();
                });
                $("#sitemapUrl").click(function () {
                    $("#messageWrapper, #invalidSiteMap").empty();
                });
            });

            function downloadFormat() {
                if (userLoggedIn) {
                    disableButtons();
                    var form = $("#robotsTxtGeneratorTool");
                    form.attr('method', "POST");
                    form.attr('action', "/robots-txt-generator-tool.html?download='download'");
                    form.submit();
                    enableButtons();
                } else {
                    reportDownload = 1;
                    $("#loginPopupModal").modal('show');
                    return false;
                }
            }
            function importRobotsRequest() {
                prepareRobots();
                getRobotsTextData('import', robotsArray);
            }
            function prepareRobots() {
                robotsArray = [];
            <%  if (request.getAttribute("robotNames") != null) {
                    robotNamesMap = (Map<String, String>) request.getAttribute("robotNames");
                    robotKeySet = robotNamesMap.keySet();
                    for (String robotVal : robotKeySet) {
                        out.println("var robotVal=\"" + robotVal + "\";");%>
                robotsArray.push(robotVal);
            <% }
                }%>
            }

            //Ask Expert Block
            function getToolRelatedIssues() {
//                var innerContent = $("#toolQueryMessage").text();
//                var url="";
//                if (innerContent.indexOf("This URL cannot be reviewed") !== -1) {
//                    url = $("#robotsTxtUrl").val();
//                }
                var url = $("#robotsTxtUrl").val();
                $.ajax({
                    type: "POST",
                    url: "/robots-txt-generator-tool.html?robotsATE='robotsATE'&robotsURL=" + url + "",
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function (data) {

                        if (data[0] !== null)
                        {
                            $("#tool-issues").html(data[0].issues);
                            $("#ateDomain").val(data[0].domain);
                            issues = data[0].issues;
                            question = data[0].question;
                            toolName = data[0].toolName;
                            otherInputs = data[0].otherInputs;
                        }
                    }
                });
            }

            function getUserToolInputs(website, userInput) {
                if (userInput !== "") {
                    var userToolInputs = JSON.parse(userInput);
                    $('#addDirectiveType').val(userToolInputs["2"]);
                    $('#userAgentName').val(userToolInputs["3"]);
                    $('#directoryOrFilePath').val(userToolInputs["4"]);
                    $('#sitemapUrl').val(userToolInputs["5"]);
                    $('#robotTxtContent').val(userToolInputs["6"]);
                } else {
                    $("#robotsTxtUrl").val(website);
                    $('#importRobots').click();
                }
            }
            function submitPreviousRequest() {
                if (isImportedForGenerator === true) {
                    $("#importRobots").click();
                } else {
                    $("#getResult").click();
                }
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <!--<div class="hidden-xs clearfixdiv-common"></div>-->
        <div class="clearfixdiv"></div>
        <div class="visible-xs " style="height: 20px;clear: both"></div>
        <!--table part start-->
        <springForm:form commandName="robotsTxtGeneratorTool" method="POST"> 
            <div class="container">
                <div class="col-lg-12" id="toolInputWrapper" >
                    <div class="form-group col-xs-12 col-sm-12 col-md-9 col-lg-9" >
                        <div class="icon-addon addon-lg">
                            <springForm:input placeholder="Enter your URL" path="robotsTxtUrl" cssClass="form-control input-lg srch-term"/>
                            <i id="edit-textbox" class="glyphicon glyphicon-pencil lxrtool-pencil" ></i>
                        </div>
                        <span class="lxr-danger text-left error" id="toolQueryMessage"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  text-center">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-lg-12 col-md-12"  id="importRobots"  data-loading-text="<span id='importSpin' class='fa fa-refresh fa-spin fa-fw'></span> IMPORTING ROBOTS.TXT">
                                IMPORT ROBOTS.TXT
                            </button>
                        </div>
                    </div>
                </div>
                <div class="clearfixdiv visible-xs visible-sm" ></div>
                <div class="col-lg-12 justify-content-md-center justify-content-center" id="toolOptionsWrapper">
                    <p class="directiveLabel toolFontStyle">ADD DIRECTIVE</p>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 2px" >
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="input-group col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                <span class = "input-group-addon toolAddon">
                                    <a class="toolHelpInfo" data-toggle="tooltip" data-html="true" data-placement="right" title="Allow: Allow crawling of a particular path.<br>Disallow: Disallow crawling of a particular path.">
                                        <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;" aria-hidden="true"></i>
                                    </a>
                                </span>
                                <springForm:select path="addDirectiveType" cssClass="form-control selectPicker">
                                    <springForm:option value="1">Allow</springForm:option>
                                    <springForm:option value="2">Disallow</springForm:option>
                                </springForm:select>
                            </div>
                        </div>
                        <div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <div class="input-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <span class = "input-group-addon toolAddon">
                                    <a data-toggle="tooltip" data-placement="right" title="A user-agent is a specific search engine robot. The Web Robots Database lists many common bots. You can set an entry to apply to a specific bot (by listing the name) or you can set it to apply to all bots (by listing All).">
                                        <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;"  aria-hidden="true"></i>
                                    </a>
                                </span>
                                <springForm:select path="userAgentName" cssClass="form-control selectPicker">
                                    <springForm:options items="${robotNames}" />
                                </springForm:select>
                            </div>
                        </div>
                        <div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>
                        <div class="col-md-3" id="specialAgentWrapper">
                            <div class="input-group col-md-12">
                                <springForm:input path="specialUserAgentName" placeholder="Specify UserAgent" cssClass="form-control input-lg"/>
                                <p class="error text-center" id="userAgentError"></p>
                            </div>
                            <div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group col-md-12">
                                <div class="input-group input-group-lg">
                                    <span class = "input-group-addon toolAddon">
                                        <a data-toggle="tooltip" data-placement="right" title="Directories or single pages or a pattern to exclude.">
                                            <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;"  aria-hidden="true"></i>
                                        </a>
                                    </span>
                                    <springForm:input path="directoryOrFilePath" cssClass="form-control input-lg" value="/"/>
                                </div>
                                <p class="error text-center" id="directiveError"></p>
                                <div class="input-group-btn" id="addButtonWrapper" style="left:8px;">
                                    <button class="btn btn-lg buttonStyles" id="addDirective" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfixdiv" id="messageWrapper" style="color: red;padding: 1% 0;text-align: center;"></div>
                <div class="row text-center directivesWrapper" style="overflow-x: hidden;overflow-y: auto;">
                    <!--<div class="row text-center directivesWrapper" style="overflow-x: auto;overflow-y: hidden;">-->
                    <div class="row">
                        <div class="col-md-1 visible-md visible-lg"></div>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10" id="importDirectivesWrapper"></div>
                        <div class="col-md-1 visible-md visible-lg"></div>
                    </div><!--EOF importDirectivesWrapper-->
                    <div class="row">
                        <div class="col-md-1 visible-md visible-lg"></div>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10" id="manuallyDirectivesWrapper"></div>
                        <div class="col-md-1 visible-md visible-lg"></div>
                    </div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="sitemapUrl">
                                XML SITEMAP URL'S<a href="#" data-toggle="tooltip" data-placement="right" title="A sitemap is a file where you can list the web pages of your site to tell Google and other search engines about the organization of your site content. For ex: http://www.xyz.com/sitemap.xml">
                                    <i class="fa fa-info-circle fa-1x" style="color: #C4C4C4;"  aria-hidden="true"></i>
                                </a>
                            </label>
                            <div class="icon-addon addon-lg">
                                <springForm:textarea path="sitemapUrl" id="sitemapUrl" rows="3" cssClass="form-control hiddenscrollbars" placeholder="Enter your sitemap URL(s) as one per line"/>
                            </div>
                            <span id="invalidSiteMap" class="error text-left" ></span>
                        </div>
                    </div>
                </div><!--EOF siteMapWrapper-->
                <div class="col-xs-12">
                    <div class="col-sm-1 col-md-3 col-lg-4 hidden-xs"></div>
                    <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
                        <div class="input-group-btn">
                            <button class="btn buttonStyles col-xs-12" id="clearDirectory" type="button"><i class="fa fa-times" style="font-size: 1.2em;margin-right:2%; " aria-hidden="true"></i>CLEAR ALL</button>
                        </div>
                    </div>
                    <div class="visible-xs" style="height: 10px;clear: both"></div>
                    <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg buttonStyles col-xs-12"  id="getResult"  data-loading-text="<span class='fa fa-refresh fa-spin fa-fw'></span> GENERATEING ROBOTS.TXT">
                                GENERATE ROBOTS.TXT
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-1 col-md-3 col-lg-4 hidden-xs"></div>
                </div>
                <div class="clearfixdiv"></div>
                <div class="row" id="toolResultWrapper">
                    <div class="col-md-12  panel-content" style="padding: 0px">
                        <div class="panel-heading lxr-panel-heading">
                            <span class="toolLabel" style="color:#999">GENERATED</span>
                            <span class="toolLabel" style="color:#4F4F4F">ROBOTS.txt</span>
                            <p>Instruct search engines robots on how to crawl.  <a href="/robots-txt-generator-help.html" target="_blank" style="color: #2f80ed !important;">LEARN MORE</a></p>
                        </div>
                        <div class="panel-body lxr-panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-xs-12" style="padding: 0;">
                                    <springForm:textarea path="robotTxtContent" rows="10" cssClass="form-control hiddenscrollbars" readonly="true"/>
                                </div>
                            </springForm:form>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0;margin:0.5% 0 0 0;">
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 lxr-download-label" style="padding: 0;margin: 1% 0 0 0;font-family:'ProximaNova-Bold' ">Get generated code:</div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="text-align:right;">
                                    <!--<span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>-->                                       
                                    <span id="downloadReport" class="glyphicon glyphicon-download-alt"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <!--   Success Page -->
        <div class="container">
            <%@include file="/WEB-INF/views/toolFeatures.jsp"%>
        </div>
        <%@ include file="/WEB-INF/views/loginPopup.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/toolRatings.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/recomendedTools.jsp"%>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>