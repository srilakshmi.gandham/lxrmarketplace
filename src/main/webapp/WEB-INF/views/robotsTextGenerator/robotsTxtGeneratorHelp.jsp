<%-- 
    Document   : robotsTxtGeneratorHelp
    Created on : 13 Oct, 2017, 3:16:29 PM
    Author     : sagar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="keywords" content="Robots.txt Generator, Create Robots.txt, Robots.txt File, Robots Exclusion Protocol">
        <title>Robots.txt Generator | LXRMarketplace.com</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolsHeader.css">
        <!--        <link rel="stylesheet" type="text/css" href="/resources/css/tools/rateit.css">
                <link rel="stylesheet" type="text/css" href="/resources/css/tools/tooltabs.css">-->
        <link rel="stylesheet" type="text/css" href="/resources/css/tools/toolfeatures.css">
        <% int toolDBId = 24;%>
        <style>
            blockquote {
                padding: 3px 0 3px 15px!important;
                margin: 20px 0 20px !important;
                font-size: 20px !important;
                border-left: 5px solid #F58120 !important;
                color: #F58120 !important;
                font-family: ProximaNova-Bold !important;
            }
            .lxrtool-centerdiv li{
                list-style-type: disc !important;
            }
            .lxrm-help-content{
                background: transparent !important;
                padding: 10px 15px;
                border: 1px solid #DDD;
                border-radius: 3px;
            }
        </style>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <%@ include file="/WEB-INF/views/toolHeaderContent.jsp"%>
        <div class="clearfixdiv"></div>
        <div class="container">
            <div class="col-lg-12 lxrtool-centerdiv" style="border: 0px green solid;">
                <blockquote>GUIDE</blockquote>
                <div class="col-lg-12 lxrm-help-content"  style="font-family: ProximaNova-Regular">
                    <p>Robots.txt file is a text file for restricting bots (robots and search engine crawlers) from a website or certain pages on the website.</p>
                    <p>Did you know that text files are very powerful and a single text file could destroy your entire website? That error is damaging and can happen pretty easily.</p>
                    <p>You can specify in your Robots.txt file that you don't want the search engines to be able to access your thank you page, payment gateway page, add to cart, etc.<p>
                    <p>You can add your sitemap in Robots.txt file. This tells search engines to crawl your sitemap and index the URLs in the sitemap itself.<p>

                    <p>This is the default format for Robots.txt with Sitemap<p>
                    <div style="padding-left: 10px;">
                        <div>User-agent: *</div>
                        <div>Disallow: /404/</div>
                        <div>Disallow: /checkout/</div>
                        <div>Disallow: /wishlist/</div>
                        <div>Sitemap: https://www.domain.com/sitemap.xml</div>
                    </div>

                    <p style="margin-top: 6px;">This is the default format for Robots.txt with multiple Sitemaps<p>

                    <div style="padding-left: 10px;">
                        <div>User-agent: *</div>
                        <div>Disallow: /404/</div>
                        <div>Disallow: /checkout/</div>
                        <div>Disallow: /wishlist/</div>
                        <div>Sitemap: https://www.domain.com/sitemap.xml</div>
                        <div>Sitemap2: https://www.domain.com/sitemap.xml</div>
                    </div>

                    <p style="margin-top: 10px">Don't worry. We won't let you or your website suffer. The best thing to do to avoid this issue is to use our FREE robots.txt generator. Our tool was completely designed to generate the proper robots.txt file for your website.</p>






                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>
