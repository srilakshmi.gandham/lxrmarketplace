<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Terms of Servicesss</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="/resources/css/static-pages.css">
        <script type="text/javascript">
            var toolDBId = 0;
            var reportDownload = 0;
            var downloadReportType = "";
            isHomePage = true;
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/views/header.jsp"%>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-justify" style=" color: #888; ">
                    <h2 class="static-main-heading">Terms of Service</h2>
                    <h4 class="static-sub-heading">
                        Netelixir LXRMarketplace Terms and Conditions of Use
                    </h4>
                    <p>
                        (Last updated: September 14, 2016)
                    </p>

                    <p>
                        NetElixir, Inc.'s software and internet marketing tools and related consulting services and advice, such as our "Ask the Expert" features (the "Service") 
                        and its affiliate internet websites (the "Sites") are accessible worldwide to anyone with Internet access. 
                        Access to and use of the Service and the Sites are subject to these terms and conditions of use and all applicable laws and regulations, including laws and regulations governing copyright and trademark.
                        <strong>BY ACCESSING AND USING THE SERVICE AND THE SITES, YOU ACCEPT, WITHOUT LIMITATION OR QUALIFICATION, THESE TERMS AND CONDITIONS OF USE</strong>.
                        NetElixir and its affiliates reserve the right to change these terms and conditions at any time. The changes will appear on this screen. By using the Service and the Sites, you agree in advance to accept any changes.
                    </p>


                    <p>
                        The materials offered, used and displayed as part of the Service and on the Sites, including but not limited to text, software, user tools, photographs, graphics, illustrations and artwork, video, music and sound, 
                        and names, logos, trademarks and service marks, are the property of NetElixir, Inc. 
                        or its affiliates or licensors and are protected by copyright, trademark and other laws. 
                        Any such content may be used solely for your personal, non-commercial use. You agree not to modify, reproduce, retransmit, distribute, disseminate, sell, publish, 
                        broadcast or circulate any such material without the written permission of NetElixir, Inc. or the appropriate affiliate.
                    </p>

                    <p>
                        The Service and the Sites contain resources, tools, information, facts and opinions from NetElixir and various individuals and organizations. 
                        THE SERVICE AND THE SITES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF TITLE OR IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, 
                        OTHER THAN THOSE WARRANTIES WHICH ARE IMPOSED BY AND INCAPABLE OF EXCLUSION, RESTRICTION OR MODIFICATION UNDER THE LAWS APPLICABLE TO THIS AGREEMENT.
                        NEITHER NETELIXIR, INC. NOR ITS AFFILIATES ENDORSE OR ARE RESPONSIBLE FOR THE ACCURACY OR RELIABILITY OF ANY TOOLS OR INFORMATION PROVIDED AS PART OF THE SERVICE OR THE SITES, INCLUDING ADVICE PROVIDED THROUGH OUR "ASK THE EXPERT" 
                        OR SIMILAR FEATURES OFFERED THROUGH THE SERVICE.
                    </p>

                    <p>
                        Your use of the Service and the Sites is at your own risk. NEITHER NETELIXIR, INC. NOR ANY OF ITS SUBSIDIARIES, DIVISIONS, AFFILIATES, AGENTS, REPRESENTATIVES OR LICENSORS SHALL BE LIABLE TO YOU OR ANYONE ELSE FOR ANY LOSS OR INJURY OR ANY DIRECT,
                        INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, PUNITIVE OR SIMILAR DAMAGES ARISING OUT OF YOUR ACCESS OR USE OF, OR YOUR INABILITY TO ACCESS OR USE, THE SERVICE AND THE SITES AND THE INFORMATION OR TOOLS AVAILABLE ON OR PROVIDED AS PART OF THE 
                        SERVICE (INCLUDING INFORMATION OR ADVICE PROVIDED THROUGH OUR "ASK THE EXPERT" OR SIMILAR FEATURES) AND THE SITES OR ARISING OUT OF ANY ACTION TAKEN IN RESPONSE TO OR AS A RESULT OF ANY INFORMATION AVAILABLE ON OR ADVICE PROVIDED THROUGH THE SERVICE
                        AND THE SITES. YOU HEREBY WAIVE ANY AND ALL CLAIMS AGAINST NETELIXIR, INC. AND ITS SUBSIDIARIES, DIVISIONS, AFFILIATES, AGENTS, REPRESENTATIVES AND LICENSORS ARISING OUT OF YOUR USE OF THE SERVICE AND THE SITES AND THE INFORMATION AVAILABLE THEREON.
                    </p>

                    <p>
                        NetElixir, Inc. and its affiliates do not review or monitor any web sites linked to the Service and the Sites and are not responsible for the content of any such linked web sites. Your use of such web sites is at your own risk. You acknowledge that the 
                        use of any advice, opinion, recommendation, statement or other information displayed, link to, or distributed through the Service and the Sites is at your own risk. Prior to purchasing any third party products or services described on the Service and the Sites,
                        you are advised to verify pricing, product quality and other information necessary to make an informed purchase. Neither NetElixir, Inc., nor any of its subsidiaries, divisions, affiliates, agents, representatives or licensors shall have any liability arising 
                        from your purchases of third party products or services based upon the information provided on the Service and the Sites.
                    </p>
                    <p>This User Agreement and any disputes arising out of or related to the Service and the Sites shall be governed by, and construed and enforced in accordance with, the laws of the State of New Jersey.</p>
                </div>
            </div>
        </div>
        <div class="clearfixdiv"></div>
        <%@ include file="/WEB-INF/views/footer.jsp"%>
    </body>
</html>