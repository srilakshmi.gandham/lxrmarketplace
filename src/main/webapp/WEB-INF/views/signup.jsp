<%-- 
    Document        : signup
    Created/ Updated on      : Nov 27, 2019, 11:24:49 AM
    Author          : Sagar
--%>

<!DOCTYPE html>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="java.util.Collections, java.util.Comparator, java.util.List,java.util.HashMap,java.util.Map"%>
<%@ page import="lxr.marketplace.user.Signup"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Signup | LXRMarketplace</title>
        <%@ include file="/WEB-INF/views/commonImports.jsp"%>
        <link rel="stylesheet" type="text/css" href="../../resources/css/signup.css">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <%  WebApplicationContext lxrmUserStasticsContenxt = WebApplicationContextUtils.getWebApplicationContext(application);
            Map<String, Long> userStatsticsMap = null;
            Signup signUPObj = null;
             if (session.getAttribute("signUpObjToBeVerified") != null) {
               signUPObj = (Signup)session.getAttribute("signUpObjToBeVerified");
               pageContext.setAttribute("signUPObj", signUPObj);
            }
            if (lxrmUserStasticsContenxt.getServletContext().getAttribute("lxrmUserStatstics") != null) {
                userStatsticsMap = (Map<String, Long>) lxrmUserStasticsContenxt.getServletContext().getAttribute("lxrmUserStatstics");
                pageContext.setAttribute("totalLXRMUsers", userStatsticsMap.get("totalLXRMUsers"));
                pageContext.setAttribute("totalTools", userStatsticsMap.get("totalTools"));
                pageContext.setAttribute("totalWebsitesAnalyzed", userStatsticsMap.get("totalWebsitesAnalyzed"));
            }
        %>
        <script>
            var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Registering...';
            var reportDownload = 0;
            var isHomePage = true;
            var downloadReportType = "";
            $(document).ready(function () {
                $("#lxrmSignUpEmailCode").hide();
                console.log("signUpError::" + ${signUpError});
                /*Defaul on page load*/
            <c:if test="${signUpError}" >
                setTimeout(function () {
                    $('#socialSectionWrapper').hide();
                    $('#emailSectionWrapper').show();
                }, 10);
                <c:if test="${not empty signUPObj}">
                    <c:if test="${not empty signUPObj.name}">
                $("#name").val("${signUPObj.name}");
                    </c:if>
                    <c:if test="${not empty signUPObj.email}">
                $("#email").val("${signUPObj.email}");
                    </c:if>
                </c:if>
            </c:if>

                $("#continueWithEmail").click(function () {
                    $('#socialSectionWrapper').hide();
                    $('#emailSectionWrapper').show();
                    resetErrorField();
                    resetBindingErrorField();
                    resetValueField();
                });
                $("#continueWithSocial").click(function () {
                    $('#emailSectionWrapper').hide();
                    $('#socialSectionWrapper').show();
                });

                $("#lxrmSignUp").click(function () {
                    disableSubmitLoadButtons();
                    var emailValidationStatus = false;
                    emailValidationStatus = signupValidation();
                    if (emailValidationStatus) {
                        var form = $("#signup");
                        form.attr('method', "POST");
                        form.attr('action', "/signup.html");
                        form.submit();
                    } else {
                        enableSubmitLoadButtons();
                    }
                });
            });

            function getFaceBookLoginScreen() {
                signupPopUp('social');
                setTimeout(function () {
                    var fbOAuthURL = "${facebookLoginOAuthURL}";
                    console.log("facebookLoginOAuthURL:: " + fbOAuthURL);
                    if (fbOAuthURL.length > 0) {
                        window.open(fbOAuthURL, "_self", "facebookLoginOAuthURL::");
                    }
                }, 500);
                return false;
            }
            function getGoogleLoginScreen() {
                signupPopUp('social');
                setTimeout(function () {
                    var gooogleOAuthURL = "${googleLoginOAuthURL}";
                    console.log("googleLoginOAuthURL ::" + gooogleOAuthURL);
                    if (gooogleOAuthURL.length > 0) {
                        window.open(gooogleOAuthURL, "_self", "googleLoginOAuthURL");
                    }
                }, 500);
                return false;
            }
            function signupValidation() {
                var name = $("#name").val().trim();
                var email = $("#email").val().trim();
                var password = $("#password").val();
                var confirmPassword = $("#repassword").val();
                var domainName = $("#domainName").val();
                var status = true;
                if (name === "") {
                    $("#nameError").empty();
                    $("#nameError").html("Please enter name.");
                    $("#nameError").show();
                    $("input[id='name']").addClass("error-input-field");
                    status = false;
                } else {
                    $("#nameError").hide();
                    $("input[id='name']").removeClass("error-input-field");
                }
                if (email === "") {
                    $("#emailError").empty();
                    $("#emailError").html("Please enter email.");
                    $("#emailError").show();
                    $("input[id='email']").addClass("error-input-field");
                    status = false;
                } else if (!isValidMail(email)) {
                    $("#emailError").empty();
                    $("#emailError").html("Please enter a valid email.");
                    $("#emailError").show();
                    $("input[id='email']").addClass("error-input-field");
                    status = false;
                } else {
                    $("#emailError").hide();
                    $("input[id='email']").removeClass("error-input-field");
                }
                if (domainName === "") {
                    $("#domainNameError").empty();
                    $("#domainNameError").hide();
//                    status = false;
                } else if (!isValidDomain(domainName)) {
                    $("#domainNameError").empty();
                    $("#domainNameError").html("Please enter a valid Webiste URL.");
                    $("#domainNameError").show();
                    $("input[id='domainName']").addClass("error-input-field");
                    status = false;
                } else {
                    $("#domainNameError").hide();
                    $("input[id='domainName']").removeClass("error-input-field");
                }
               
                if (password === "") {
                    $("#passwordError").empty();
                    $("#passwordError").html("Please enter password.");
                    $("#passwordError").show();
                    $("input[id='password']").addClass("error-input-field");
                    status = false;
                } else if (password.length < 6) {
                    $("#passwordError").empty();
                    $("#passwordError").html("Your password must be at least 6 characters long.");
                    $("#passwordError").show();
                    $("input[id='password']").addClass("error-input-field");
                    status = false;
                } else if (password !== "" && password !== null && /\s/g.test(password)) {
                    $("#passwordError").empty();
                    $("#passwordError").html("Space is not allowed for password.");
                    $("#passwordError").show();
                    $("input[id='password']").addClass("error-input-field");
                    status = false;
                } else {
                    $("#passwordError").hide();
                    $("input[id='password']").removeClass("error-input-field");
                }
                if (confirmPassword === "") {
                    $("#confirmPasswordError").empty();
                    $("#confirmPasswordError").html("Please enter confirm password.");
                    $("#confirmPasswordError").show();
                    $("input[id='repassword']").addClass("error-input-field");
                    status = false;
                } else if (password !== confirmPassword) {
                    $("#confirmPasswordError").empty();
                    $("#confirmPasswordError").html("Password and Confirm Password should be same.");
                    $("#confirmPasswordError").show();
                    $("input[id='password']").addClass("error-input-field");
                    $("input[id='repassword']").addClass("error-input-field");
                    status = false;
                } else {
                    $("#confirmPasswordError").hide();
                    $("input[id='repassword']").removeClass("error-input-field");

                }
//                console.log(document.getElementById('termsOfService').checked);
                if (!document.getElementById('termsAndConditions1').checked) {
                    $("#termsAndConditionsError").empty();
                    $("#termsAndConditionsError").html("Please accept Terms of Service and Privacy Policy.");
                    $("#termsAndConditionsError").show();
                    status = false;
                } else {
                    $("#termsAndConditionsError").empty();
                    $("#termsAndConditionsError").hide();
                }
                return status;
            }/*EOF of signup form validation*/
        </script>
        <style>
            @media screen and (min-width: 480px) {
                .btn-social {width:50%;}

            }
            @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
                .btn-social {width:100%;}
            }
            @media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (-webkit-min-device-pixel-ratio: 2)  and (orientation: portrait)  {
                .btn-social {width:90%;}
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <!--Section #1-->
                <div class="col-xs-12 col-md-5">
                    <div class="row">
                        <!--LXR logo & Improve your website with our helpful free tools-->
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 lxrformlogo">
                                    <a href="/"><img  src="../../resources/images/lxr-logo.png" alt="LXRMarketplace"></a> 
                                </div>
                                <div class="col-xs-12 signUpTitle">
                                    <h1 class="lxrm-bold">Improve your website with our helpful free tools</h1>
                                </div>
                            </div>
                        </div>
                        <!--Sign up page details-->
                        <div class="col-xs-12">
                            <!--Social signup section-->
                            <div class="row" id="socialSectionWrapper">
                                <div class="col-xs-12" id="lxrmStatisticsInfo">
                                    <div class="row">
                                        <div class="col-xs-12 lxr-analytics"style="margin-top:8px" >
                                            <div class="col-lg-12 lxr-analytics-sub" >
                                                <span class="image"><img src="../../resources/images/loginUserHappyCustomers.png" class="img-responsive" alt="Happy Customer" /></span>
                                                <span class="lxrm-bold"id="happy-customers" style="padding-left:18px"> ${totalLXRMUsers}</span>
                                                <span id="Websites-Analyzed">Happy Customers</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 lxr-analytics"style="margin-top:37px" >
                                            <div class="col-lg-12 lxr-analytics-sub">
                                                <span class="image"style=""><img src="../../resources/images/loginUserUsedTools.png" class="img-responsive" alt="Happy Customer" /></span>
                                                <span class="lxrm-bold" id="Free-Tools" style="padding-left:19px"> ${totalTools} </span>
                                                <span id="Websites-Analyzed">Free Tools</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 lxr-analytics" style="margin-top:37px">
                                            <div class="col-lg-8 lxr-analytics-sub" >
                                                <span class="image" ><img src="../../resources/images/loginUserWebsitesAnalysis.png" class="img-responsive" alt="Happy Customer" /></span>
                                                <span class="lxrm-bold" id="happy-customers" style="padding-left:18px"> ${totalWebsitesAnalyzed} </span>
                                                <span id="Websites-Analyzed">Websites Analyzed</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 "style="padding-top: 32px">
                                            <p class="text-center" style="border-bottom: 1px solid #7F8386"></p>
                                        </div>
                                    </div>
                                </div><!--EOF lxrmStatisticsInfo wrapper-->
                                <div class="col-xs-12" id="socialSignUpWrapper">
                                    <div class="row">
                                        <div class="social lxrm-bold col-xs-12" style="padding-top: 0.5rem;padding-left: 33px;">
                                            <a class="col-xs-12 col-md-8 col-sm-6 btn btn-lg btn-social btn-facebook" onclick="getFaceBookLoginScreen();">
                                                <i class="fa fa-facebook fa-fw"></i> Continue with Facebook
                                            </a>
                                        </div>
                                        <br>
                                        <div class="social lxrm-bold col-xs-12" style="margin-bottom: 15px;padding-left: 33px;padding-top: 1rem;">
                                            <a class="col-xs-12 col-md-8 col-sm-6 btn btn-lg btn-social btn-google" onclick="getGoogleLoginScreen();">
                                                <img  src="/resources/images/google_signup.png" >Continue with Google
                                            </a>
                                        </div>
                                        <div class="col-xs-12" style="color:#5087EC;padding-left: 33px;">
                                            <p><span class="lxrm-bold signUpSectionOptions" id="continueWithEmail" style="cursor: pointer;">Continue with Email</span></p>
                                        </div>
                                        <div class="col-xs-12" style="padding-left: 33px;padding-top: 1rem;">
                                            <div class="lxr-danger pb-2 bindingError">${socialSignupError}</div>
                                        </div>
                                        <br>
                                        <div class=" Privacy" style="margin-bottom: 15px;font-size: 12px ;padding-left: 33px;">
                                            <span style="color: black"> By signing in, I agree to the 
                                                <a href="/privacy-policy.html" target="_blank" style="cursor: pointer;">Privacy Policy</a> and 
                                                <a href="/terms-of-service.html" target="_blank" style="cursor: pointer;">Terms of Service</a>.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div><!--EOF of social section wrapper-->
                            <!--Email signup section-->
                            <div class="row" id="emailSectionWrapper" style="display: none;">
                                <div class="col-xs-12 col-sm-10 col-md-8 lxr-box-shadow backgroundColorForSignup" >
                                    <springForm:form  class="form-horizontal" method="POST" commandName="signup">
                                        <div class="text-center lxr-danger pb-2 bindingError">${error}</div>
                                        <div class="text-center lxr-danger pb-2 bindingError">${varyfiedResponse}</div>
                                        <div class="col-xs-12"style="margin-top: 10px;">
                                            <div class="form-group">
                                                <springForm:input path ="name" id="name" class="form-control input-without-boxshadow" type="text" maxlength="100" placeholder="Name" />
                                                <springForm:label class="form-control-placeholder" path="name">Name</springForm:label>
                                                <springForm:errors path="name" cssClass="error"/>
                                                <div id="nameError" class="lxr-danger" style="display: none;"></div>      
                                            </div>
                                            <div class="form-group">
                                                <springForm:input path ="email" id="email" class="form-control input-without-boxshadow" type="email" maxlength="100" placeholder="Email Address" />
                                                <springForm:label class="form-control-placeholder" path="email">Email Address</springForm:label>
                                                <springForm:errors path="email" cssClass="error"/>
                                                <div id="emailError" class="lxr-danger" style="display: none;"></div>
                                            </div>
                                            <div class="form-group">
                                                <springForm:input path="password" id="password"  class="form-control input-without-boxshadow" type="password" maxlength="50" placeholder="Password" />
                                                <springForm:label class="form-control-placeholder" path="password">Password</springForm:label>
                                                <springForm:errors path="password" cssClass="error" />
                                                <div id="passwordError" class="lxr-danger" style="display: none;"></div>
                                            </div>
                                            <div class="form-group">
                                                <springForm:input path="repassword" class="form-control input-without-boxshadow" type="password" maxlength="50" placeholder="Confirm Password"  />
                                                <springForm:label class="form-control-placeholder" path="repassword">Confirm Password</springForm:label>
                                                <springForm:errors path="repassword" cssClass="error"/>
                                                <div id="confirmPasswordError" class="lxr-danger" style="display: none;"></div>
                                            </div>
                                            <div class="form-group">
                                                <springForm:input path ="domainName" class="form-control input-without-boxshadow" type="text" id="domainName" maxlength="100" placeholder="Website URL(Optional)" />
                                                <springForm:label class="form-control-placeholder" path="domainName" id="domainName">Website URL(Optional)</springForm:label>
                                                <springForm:errors path="domainName" cssClass="error"/>
                                                <div id="domainNameError" class="lxr-danger" style="display: none;"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check p-0">
                                                        <label class="form-check-label p-0"style="font-weight:100">
                                                        <springForm:checkbox path="termsAndConditions" />
                                                        <span style="font-size: 12px;font-weight:0;cursor: pointer;" class="underline-for-terms"> By signing in,I agree to the 
                                                            <a href="/terms-of-service.html" target="_blank" style="text-decoration: underline;">Privacy Policy &nbsp;</a>and 
                                                            <a href="/privacy-policy.html" target="_blank" style="text-decoration: underline;">&nbsp;Terms of Service</a>.
                                                        </span>
                                                    </label>
                                                    <div> <springForm:errors path="termsAndConditions" cssClass="error"/></div>
                                                    <div id="termsAndConditionsError" class="lxr-danger" style="display: none;"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="g-recaptcha px-3" data-sitekey="6LcW8eYSAAAAAPe-p2DsSesB4uFd7IuyOMHpIoWZ"></div>
                                            </div>
                                            <div class="form-group col-12 mb-0 py-5 border-top d-flex justify-content-center button-top-margin" id="signUpSubmitBtnWrapper">
                                                <button type="button" name="LXRMarketplace SignUp" class="btn btn-md btn-block continue-button lxrm-bold signupButttons" id="lxrmSignUp">Register</button>
                                            </div>
                                            <div class="form-group col-12 mb-0 py-5 border-top d-flex justify-content-center button-top-margin" id="signUpLoadBtnWrapper" style="display: none;">
                                                <button class="btn btn-md btn-block continue-button lxrm-bold signupButttons"><i class="fa fa-refresh fa-spin"></i> &nbsp;Registering...</button>
                                            </div>
                                        </div>
                                    </springForm:form>
                                </div>
                                <div class="col-xs-12">
                                    <span class="lxrm-bold signUpSectionOptions" id="continueWithSocial" style="color: #5087EC;cursor: pointer;">
                                        Continue with Social
                                    </span>
                                </div>
                            </div>
                            <!--EOF email signup section-->
                        </div>
                    </div>
                    <!-- EOF Section #1-->
                </div>
                <!--Section #2 Page image-->
                <div class="col-md-7 visible-md visible-lg">
                    <div class="">
                        <img  src="../../resources/images/lxrmSignUpLogo.png" alt="LXRMarketplace" width="95%"  >
                    </div>
                </div>
            </div>
            <!--EOF container-fluid--> 
        </div>
    </body>
    <script>
        function resetErrorField() {
            console.log("resetErrorField");
            $("input[id='name']").removeClass("error-input-field");
            $("input[id='email']").removeClass("error-input-field");
            $("input[id='password']").removeClass("error-input-field");
            $("input[id='repassword']").removeClass("error-input-field");
            $("input[id='domainName']").removeClass("error-input-field");
            $("#nameError").empty();
            $("#emailError").empty();
            $("#passwordError").empty();
            $("#confirmPasswordError").empty();
            $("#domainNameError").empty();
            $("#termsAndConditionsError").empty();
        }

        function resetBindingErrorField() {
            console.log("resetBindingErrorField");
            $(".error").empty();
            $("#name.errors").empty();
            $("#email.errors").empty();
            $("#password.errors").empty();
            $("#repassword.errors").empty();
            $("#domainName.errors").empty();
            $("#termsAndConditions.errors").empty();
            $(".bindingError").empty();
        }

        function resetValueField() {
            console.log("resetValueField");
            $("input[id='name']").val("");
            $("input[id='email']").val("");
            $("input[id='password']").val("");
            $("input[id='repassword']").val("");
            $("input[id='domainName']").val("");
            $.each($("input[name='termsAndConditions']:checked"), function () {
                $(this).prop("checked", false);
            });
//            enableSubmitLoadButtons();
        }
        function enableSubmitLoadButtons() {
            console.log("enableSubmitLoadButtons");
            $("#signUpSubmitBtnWrapper").css("display", "block");
            $("#signUpLoadBtnWrapper").css("display", "none");
        }
        function disableSubmitLoadButtons() {
            console.log("disableSubmitLoadButtons");
            $("#signUpSubmitBtnWrapper").css("display", "none");
            $("#signUpLoadBtnWrapper").css("display", "block");
        }
       

    </script>
</html>
