
<%@ page import="lxr.marketplace.user.Login"%>
<%@ page import="lxr.marketplace.util.Common,java.util.Collections, java.util.Comparator, java.util.List,java.util.HashMap,java.util.ArrayList,lxr.marketplace.util.ui.CommonUi,java.util.Map,lxr.marketplace.util.Tool,lxr.marketplace.util.ToolService"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%  WebApplicationContext lxrmUserStasticsContenxt = WebApplicationContextUtils.getWebApplicationContext(application);
    Map<String, Long> userStatsticsMap = null;
    if (lxrmUserStasticsContenxt.getServletContext().getAttribute("lxrmUserStatstics") != null) {
        userStatsticsMap = (Map<String, Long>) lxrmUserStasticsContenxt.getServletContext().getAttribute("lxrmUserStatstics");
        pageContext.setAttribute("totalLXRMUsers", userStatsticsMap.get("totalLXRMUsers"));
        pageContext.setAttribute("totalTools", userStatsticsMap.get("totalTools"));
        pageContext.setAttribute("totalWebsitesAnalyzed", userStatsticsMap.get("totalWebsitesAnalyzed"));
    }
    String topThreeTools = null;
    // The below varibale using in the toolAdvisor.jsp
    List<Integer> topThreeToolIds = new ArrayList();
    if (session.getAttribute("topThreeTools") != null) {
        topThreeTools = (String) session.getAttribute("topThreeTools");
        pageContext.setAttribute("topThreeTools", topThreeTools);
    }
    if (session.getAttribute("topThreeToolIds") != null) {
        topThreeToolIds = (List<Integer>) session.getAttribute("topThreeToolIds");
    }
    String toolAdvisorDomain = "";
    if (session.getAttribute("toolAdvisorDomain") != null) {
        toolAdvisorDomain = (String) session.getAttribute("toolAdvisorDomain");
    }
    pageContext.setAttribute("toolAdvisorDomain", toolAdvisorDomain);
%>
<script type="text/javascript">
    $(document).ready(function () {



        //Showing top three tools
        if (<%=topThreeTools != null%>) {
            var topThreeToolsFromSession = <%=topThreeTools%>;
            showAdviorTools(topThreeToolsFromSession[1]);
        }
        animationCounter(1, ${totalLXRMUsers}, 100, 20000, 5000, "happy-customers");
        animationCounter(1, ${totalTools}, 100, 20000, 1, "free-tools-count");
        animationCounter(1, ${totalWebsitesAnalyzed}, 100, 20000, 10000, "analysed-websites");


        $('#getTopThreeTools').on('click', function () {

            $('#lxr-alert').removeClass("lxr-danger text-success");
            $('#lxr-alert').html("");

            if (!toolLimitExceeded()) {
                return false;
            }
            var domain = $('#domain').val();
            if (domain === null || domain.trim() === "" || domain.trim().length === 0) {
                $('#lxr-alert').show();
                $('#lxr-alert').html("Please enter your URL to analyze for best recommendation tools.");
                $('#lxr-alert').addClass("lxr-danger");
                $('#domain').focus();
                return false;
            }
            var $this = $(this);
            $this.button('loading');
            $('#domain').prop("disabled", true);
            var params = {domain: domain};
            jQuery.getJSON("/tool-advisor.json", params,
                    function (data) {
                        if (data[0] === 1) {
                            $('#domain').val(data[3]);
                            $('#lxr-alert').addClass("text-success");
                            $('#lxr-alert').html("<strong>Success!</strong> Below are the Recommended tools for your website.");
                            topThreeToolsIds = data[2];
                            showAdviorTools(data[1]);

                        } else {
                            $('#lxr-alert').addClass("lxr-danger");
                            $('#lxr-alert').html('<spring:message code="URL.error" />');
                            $(".tool-advisor-tool-box").hide();
                        }


                    })
                    .done(function () {
//                        console.log("second success");
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var allheaders = jqxhr.getAllResponseHeaders();
                        if (!userLoggedIn || (textStatus === 'error' || allheaders.indexOf("limitexceed") > 0)) {
                            $("#loginPopupModal").modal('show');
                            return false;
                        }
                    })
                    .always(function () {
                        console.log("Alyways:");
                        $('#domain').prop("disabled", false);
                        $this.button('reset');
                    });
        });

        $('#domain').keypress(function (ev) {
            if (ev.which === 13) {
                $("#getTopThreeTools").click();
                return false;
            }
        });
    });

    // For constructing emeded tools
    function showAdviorTools(advisorToolsList) {
        var recomendedToolsInfo = "<h5 class='hidden-xs'></h5>";
        for (var i = 0; i < advisorToolsList.length; i++) {
            recomendedToolsInfo += '<div  class="col-xs-12 col-sm-6 col-lg-4 lxr-tooladviser form-group">'
                    + '<div class="lxr-tool-showcase" style="border: 1px ' + advisorToolsList[i].toolIconColor + ' solid;background: white;border-radius: 5px">'
                    + '<a id="' + advisorToolsList[i].toolId + '"href="' + advisorToolsList[i].toolLink + '?tool=advisor">'
                    + '<div class="col-item lxr-items"  style="background-color:' + advisorToolsList[i].toolIconColor + ';">'
                    + '<div class="post-img-content">'
                    + '<span class="post-title">'
                    + '<span class="pull-left" style="padding: 8px;"><img src="../../resources/images/tool-icons/' + advisorToolsList[i].imgPath + '.png"></span>';
            if (advisorToolsList[i].categoryid !== 6) {
                recomendedToolsInfo += '<div class="pull-right" ><span style="padding: 8px 10px 0px 0px;" class="pull-right"><img src="../../resources/images/user.png"></span>';
                if (advisorToolsList[i].toolUserInfo.toolUsageCount >= 1000) {
                    recomendedToolsInfo += '<span style="font-size: 30px;" class="lead">' + kFormatter(advisorToolsList[i].toolUserInfo.toolUsageCount) + '</span><span class="sma">k&nbsp;&nbsp;</span></div>';
                } else {
                    recomendedToolsInfo += '<span style="font-size: 30px;" class="lead">' + advisorToolsList[i].toolUserInfo.toolUsageCount + '</span><span class="sma">&nbsp;&nbsp;</span></div>';
                }
            }
            var toolName = advisorToolsList[i].toolName;
            if (advisorToolsList[i].toolName.length > 28) {
                toolName = toolName.substring(0, 26) + "..";
            }
            recomendedToolsInfo += '</span>'
                    + '</div>'
                    + '<div class="price col-lg-12" style="padding-left: 8px;">'
                    + '<h4 class="align-text-bottom lxr-title">' + toolName + '</h4>'
                    + '<div id="stars" class="starrr align-text-bottom pull-left">' + advisorToolsList[i].toolUserInfo.avgRatingGraph + '</div>'
                    + '<span class="pull-left">&nbsp;&nbsp;(' + advisorToolsList[i].toolUserInfo.toolRatedUsersCount + ')</span>'
                    + '</div>'
                    + '</div>'
                    + '<div class="info">'
                    + '<ul class="fa-ul" style="height: 96px">';
            if (advisorToolsList[i].toolUserInfo.toolIssues !== null && advisorToolsList[i].toolUserInfo.toolIssues.trim() !== "") {
                recomendedToolsInfo += advisorToolsList[i].toolUserInfo.toolIssues;
            } else {
                recomendedToolsInfo += "<li class='not-warning'><i class='fa-li fa fa-check' style='color:" + advisorToolsList[i].toolIconColor + "'></i>" + advisorToolsList[i].tagline + "</li>"
                        + "<li class='not-warning'><i class='fa-li fa fa-check' style='color:" + advisorToolsList[i].toolIconColor + "'></i>" + advisorToolsList[i].toolDesc + "</li>";
            }
            recomendedToolsInfo += '</ul>'
                    + '<div class="lxr-clearfix">';
            if (advisorToolsList[i].isAndroidApp || advisorToolsList[i].isIOSApp) {
                recomendedToolsInfo += '<span>Download App</span>';
            }
            if (advisorToolsList[i].isAndroidApp) {
                recomendedToolsInfo += '<a href="' + advisorToolsList[i].androidAppURL + '" target="_blank">'
                        + '<img src="../../resources/images/android.png"></a>';
            }
            if (advisorToolsList[i].isIOSApp) {
                recomendedToolsInfo += '<a href="' + advisorToolsList[i].iosAppURL + '" target="_blank" >'
                        + '<img src="../../resources/images/apple-xxl.png"></a>';
            }
            recomendedToolsInfo += '</div>'
                    + '</div>'
                    + '</a></div>'
                    + '</div>';
        }
        $("#topThreeTools").html("");
        $("#topThreeTools").html(recomendedToolsInfo);
        $(".tool-advisor-tool-box").show();
        showAllSectionTools(4, "");
        // adding warning icon for tool recomendations text
        $("#topThreeTools .fa-ul i").addClass("fa-li fa fa-exclamation");
        $("#topThreeTools .not-warning i").removeClass("fa-exclamation");
        $(".fa-exclamation").css("color", "red");

    }
    // Number Spinner function
    function animationCounter(startNum, endNum, delay, duration, step, divId) {
        var myinterval = setInterval(function () {
            startNum = startNum + step;
            if (startNum < endNum) {
                $("#" + divId).html("" + startNum);
            } else {
                $("#" + divId).html("" + endNum);
                clearInterval(myinterval);
            }
        }, delay);
    }
</script>
<div class="container-fluid lxr-bgimg"> 
    <div class="container">
        <div class="row  " >
            <div class="jumbotron text-center lxr-jumbo-search" style="padding-top: 10px !important;">
                <h1>IMPROVE YOUR WEBSITE WITH OUR HELPFUL FREE TOOLS</h1> 
                <form class="form-inline" style="clear: both;">
                    <div class="input-group col-lg-5 col-md-7 col-sm-7 col-xs-9" style="display: inline-block; padding-right: 0px;">
                        <input type="text" class="form-control lxr-search input-lg" placeholder="www.mybusiness.com" required="" id = "domain" value="${toolAdvisorDomain}">
                    </div>
                    <div class="visible-xs visible-sm" style="height: 10px"></div>

                    <div class="input-group col-lg-2" style="padding-left: 0px">
                        <div class="input-group-btn">
                            <button type="button" class="btn lxr-search-btn top-line btn-lg col-lg-10"  id="getTopThreeTools"  data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> ANALYZING">
                                ANALYZE
                            </button>
                        </div>
                    </div>
                </form>
                <div class="row" style="margin-top: 10px;">
                    <div id="lxr-alert"></div>
                </div>
            </div>
        </div>
    </div>
    <!--analytics info start-->
    <div class="container-fluid text-center lxr-analyticsmain  clearfix" >
        <div class="row slideanim slide" id="lxranalyticsdetails">
            <div class="col-xs-4 lxr-analytics" >
                <div class="col-lg-8 col-lg-offset-6 lxr-analytics-sub" >
                    <span> <div style="height: 18px;clear: both"></div><img src="../../resources/images/happy-customers.png" class="img-responsive" alt="Happy Customer" /></span>
                    <h4 id="happy-customers">${totalLXRMUsers}</h4>
                    <p style="padding-right: 8px;">Happy Customers</p>
                </div>
            </div>
            <div class="col-xs-3 lxr-analytics" >
                <div class="col-lg-8 col-lg-offset-3 lxr-analytics-sub">
                    <span> <div style="height: 18px;clear: both"></div><img src="../../resources/images/free-tools.png"  alt="Happy Customer"/></span>
                    <h4 id="free-tools-count" style="text-align: center;">${totalTools}+ 

                    </h4>
                    <p style="text-align: right;padding-right: 31px;">Free Tools</p>
                </div>
            </div>
            <div class="col-xs-4 lxr-analytics" >
                <div class="col-lg-8 lxr-analytics-sub" >
                    <span> <div style="height: 18px;clear: both"></div><img src="../../resources/images/website-analyzer.png" class="img-responsive" alt="Happy Customer"/></span>
                    <h4 id="analysed-websites">${totalWebsitesAnalyzed} </h4>
                    <p style="padding-right: 6px;">Websites Analyzed</p>
                </div>
            </div>
        </div>

        <div class="row slideanim slide" id="lxranalyticsdetails-1" style="display: none">
            <div class="carousel slide slider-moves" data-ride="carousel" id="quote-carousel">
                <!--Carousel Slides / Quotes--> 
                <div class="carousel-inner" style="color: #000 !important;">
                    <!--Quote 1--> 
                    <div class="item active">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p><a href="/seo-webpage-analysis-tool.html">SEO
                                            Webpage Analysis Tool </a> is Good for those New to SEO | Great
                                        Tool. Really goes into depth. Great for those new to SEO and
                                        looking to improve their site.
                                    </p>
                                    <small>Victoria</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>

                    <div class="item" >
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p>
                                        <a href="/weekly-keyword-rank-checker-tool.html">Keyword Rank Checker</a> 
                                        is a fantastic tool. This is such a great little tool -- I get the PDF
                                        report in my inbox each week. Super helpful way to keep tabs on
                                        critical keywords.
                                    </p>
                                    <small>Sarah Bergman/Earplugstore.com</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>

                    <!--Quote 2--> 
                    <div class="item" >
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p>
                                        The <a href="/url-auditor-tool.html">URL
                                            Auditor</a> is GREAT! It was sent to me by email just as I was
                                        having problems with unrecognized URL's. It took some work to fix
                                        my database but it really came in handy.
                                    </p>
                                    <small>Jim Arthur/Midwest Shooting Sports &amp; Supply LLC</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>

                    <!--Quote 3--> 
                    <div class="item" >
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p>
                                        <a href="/ppc-keyword-combination-tool.html">Keyword
                                            Combinator</a> is a great app. I have always struggled with coming up
                                        with a good combination of keywords. This a great tool. 
                                    </p>
                                    <small>Karen McHugh/Sleepinggiantstudio.ie</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <div class="item" >
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p>
                                        <a href="/seo-webpage-analysis-tool.html">SEO
                                            Webpage Analysis Tool</a> is Very Informative. I use this tool to
                                        create a quick and easy snapshot of the websites for meetings
                                        with potential new clients. It gives us plenty to talk about.
                                    </p>
                                    <small>Jared Matthew Sewell</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <div class="item" >
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p>
                                        We use NetElixir's LXRMarketplace free
                                        tools for our website for its precise and vast selection. 
                                        <a href="/ppc-keyword-combination-tool.html">PPC Keyword Combinator tool</a>
                                        is one of our most used tools. 
                                    </p>
                                    <small>Sakshi Gupta/Carrygreen.com</small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                </div>
                <!--Bottom Carousel Indicators--> 
                <ol class="carousel-indicators">
                    <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#quote-carousel" data-slide-to="1"></li>
                    <li data-target="#quote-carousel" data-slide-to="2"></li>
                    <li data-target="#quote-carousel" data-slide-to="3"></li>
                    <li data-target="#quote-carousel" data-slide-to="4"></li>
                    <li data-target="#quote-carousel" data-slide-to="5"></li>

                </ol>

            </div>
        </div>       
    </div>

</div>
<!--analytics info ends-->

<!--Recomended tools starts-->
<div class="lxr-toolbox tool-advisor-tool-box" style="display: none;">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-3 visible-xs" >
            <blockquote class="blockquote" style="margin-left: 16px !important;font-family: ProximaNova-Bold !important;">
                Recommended <span>Tools</span>
            </blockquote>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3 lxr-recomended-tools hidden-xs" >
            <h5></h5>
            <blockquote class="lxr-tool-bk">
                <span>Recommended  </span>
                <span style="color: #F58120">Tools</span>
            </blockquote>
        </div>
        <div class="col-lg-9" id="topThreeTools"></div>
    </div>
</div>