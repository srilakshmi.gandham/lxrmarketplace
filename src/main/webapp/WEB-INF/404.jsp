<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--<%@ include file="/WEB-INF/view/include.jsp"%>--%>
<html>
<head>
<title>404 Page not Found - LXRMarketPlace.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="/WEB-INF/view/commonImport.jsp"%>
 <link rel="stylesheet"
	href="/css/0/themes/ui-lightness/jquery.ui.all.css">
<script type="text/javascript"
	src="/scripts/ui/jquery-ui-1.8.18.custom.min.js"></script>
<link rel="stylesheet" media="all" type="text/css"
	href="/scripts/jquery/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" media="all" type="text/css" href="/css/tools.css" />
<script type="text/javascript" src="/scripts/lxrm-commons.js"></script>
<script type="text/javascript" src="/js/backtotop.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="/css/pdf.css" />
<link rel="stylesheet" type="text/css" href="css/downloadpopup.css">
<script type="text/javascript" src="/js/submenuscroll.js"></script>
<% long toolDBId= -1l; %>
<script type="text/javascript">
    $(document).ready(function () {
                        $('#sign-in').hide();
                        $('.signup').hide();
                        
        });
</script>

<style type="text/css">
.errdiv {
	font-weight: bold;
	text-align: center;
	color: #505050;
	padding: 140px;
}
.inner-content {
float: left;
width: 100%;
padding: 2%;
background: rgb(255, 255, 255) none repeat scroll 0% 0%;
border-width: 5px 1px 1px;
border-style: solid;
border-color: #F47A44 #CCC #CCC;
margin-top:5.5%;
}
</style>
</head>
<body class="cbp-spmenu-push">
	<!-- Responsive Wrapper Starts -->
    <div class="responsive-wrapper">
	<%@ include file="/WEB-INF/view/header.jsp"%>
        <!-- Middle Content Starts -->
	<div class="middle-wrapper">
            <div class="col-wrapper">
		<div class="right-col" style="float: right"><!--id="bcont" -->
			<div id="bc-sub" class="portlet shadow">
                            <div class="contactdiv-wrap" itemscope itemtype="http://schema.org/Product">
                                <div class="content">
                                    <section class="tool">
                                        <div class="inner-content">
                                            <a style="text-decoration: none" href="/lxrmarketplace.html"
					target="_blank">
                                                <div class="errdiv">
                                                        <img src="../images/error.png" alt="404 Error" />
                                                        <div
                                                                style="font-size: 28px; padding-bottom: 17px; padding-top: 10px;">
                                                                Error</div>
                                                        <div style="font-size: 31px; padding-bottom: 15px; color: #f60;">404</div>
                                                        <div style="font-size: 16px; padding-bottom: 15px;">Page Not
                                                                Found</div>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                           
                            </div>
			</div>
		</div><!-- right-col ends-->
                <div class="left-col"><!--id="scont"  -->
			<%--<%@ include file="/WEB-INF/view/otherTools.jsp"%>--%>
		</div><!-- left-col ends-->
	<!--    <div id="videodiv"  title="Seo webpage analysis Video Tutorial"> -->
	<!--    </div> -->
            </div><!-- col wrapper Ends -->
            <%@ include file="/WEB-INF/view/footer.jsp"%>
            <p id="back-to-top"><a href="#top"><span></span></a></p>
        </div><!-- Middle Content Ends -->
    </div><!-- Responsive Wrapper Ends -->
    <%@ include file="/WEB-INF/view/commonBottomImport.jsp"%>
</body>
</html>
