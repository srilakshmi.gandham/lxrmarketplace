<script type="text/javascript" src="/scripts/fileuploader.js"></script>
<script type="text/javascript">
         var maxCount = 2;
         var countfile = 0;
         var limitMessage=0;
         function createUploader(event, divId){
             	var uploader = new qq.FileUploader({
        	    	element: divId,
               	action : "suppcustservice.html?uploadFile='uploadFile'",
               	multiple: false,
               	sizeLimit: 2097152,
               	onSubmit: function(id, fileName){
                                    			countfile++;
                                    	if(countfile == 1){
               					$('#file').hide();
               					$('#filedisable').css('display', 'block');
               					}
               				if(countfile >=maxCount){
               					alert("You cannot attach more than 1 file in a mail");
               					return false;
               				}
        			 	},
        			 	onProgress: function(id, fileName, loaded, total){
        			 		 		//$('.uploadfile').attr('disabled', true);
        			 		
        			 		
        			 	},
        		onComplete: function(id, fileName, responseJSON){
        			//$('.uploadfile').attr('disabled', false);
        					$('.qq-upload-list').html("");
        					$('#upFileName').val(fileName);
        					if(responseJSON.length == 1){
        						countfile--;
        						if(countfile <= 1){
                                                            //$('#file').css('display', 'inline-block');
        							$('#file').css('display', 'block');
        		   					$('#filedisable').hide();
        		   					}
        						alert(responseJSON[0]);
        					}else{
        						addRowForattachedFile(fileName);
        						}
        		       	},             
        		messages: {
        		            typeError: "Please Upload .csv or .tsv file.",
        		            //sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
        		            sizeError: "Exceeding File Size Limit",
        		            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
        		            emptyError: "{file} is empty, please select files again without it.",
        		            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
        		        },  
        		showMessage: function(message){
        		            alert(message);
        		        } 
              });   
         }
         function addRowForattachedFile(fileName){
        		var atchmnts = document.getElementById("attachments");
        		var changedFile = fileName.replace(new RegExp("'", 'g'),"\\\\\\'");
        		var atchmnts = document.getElementById("attachments");
        	    var lastRow = atchmnts.rows.length;
        		var row = atchmnts.insertRow(lastRow);
        		var cellLeft = row.insertCell(0);
        		var textNode = document.createTextNode(fileName);
        		cellLeft.appendChild(textNode);
        		var cellRight = row.insertCell(1);
        	    var remov = document.createElement('IMG');
        	    remov.setAttribute("src", "../images/tools/remove.png");
        	    remov.setAttribute("width", "80%");
        	   remov.onclick = function(){ var file = changedFile;  removeFile(file);};
        	    cellRight.appendChild(remov);
        		}
         function showPreviousAttachments(){
           	 $.ajaxSetup({
        		 cache: "false"
        	 });
        	 $.ajax({
        	        type: "POST",  
        	        url: "/suppcustservice.html?getAllFiles='getAllFiles'",
        	        processData: true,
        	    	data: {},
        	    	dataType: "json",
        	    	success: function (data) {
                            	countfile = data.length;
        	    		if(countfile == 1){
        	    			$('#file').hide();
                                        //$('#filedisable').css('display', 'inline-block');
           					$('#filedisable').css('display', 'block');
           					}
        	    		for(var i =0; i < countfile; i++){    			
        	    			addRowForattachedFile(data[i]);
        	    		}
        	    	}
        	    });
         }
         function removeFile(rmvFileName){
	 		 $.ajax({
	        type: "GET",  
	        url: "/suppcustservice.html?removeFile="+rmvFileName,
	        processData: true,
	    	data: {},
	    	dataType: "json",
	    	success: function (data) {
	    		var atchmnts = document.getElementById("attachments");
	    		while (atchmnts.lastChild) {
	    		  atchmnts.removeChild(atchmnts.lastChild);
	    		}
	    		countfile = data.length;
	    		if(countfile< 5){
                                        //$('#file').css('display', 'inline-block');
   					$('#file').css('display', 'block');
   					$('#filedisable').hide();
   					}
	    		for(var i =0; i < countfile; i++){    			
	    			addRowForattachedFile(data[i]);
	    			
	    		}
	    		//setDashboard();
	    	}
	    });
 }
        
             $(document).ready(function(event) {
                 
                $('#name').val("");
                $('#email').val("");
                $('#phoneNo').val("");
                $('#subject').val("");
                $('#mailBody').val("");
                
    <c:set var="feedBackMail" value="${feedBackMail}" />
              <c:if test="${(feedBackMail.equals('sent'))}">
              var feedBackMail="${feedBackMail}";
                            $('#afterFeedBackmailsent').show();
                            $('html, body').animate({scrollTop: $('#tm').offset().top},'slow');
            </c:if>
            	 var divId = document.getElementById('browse1');
            	 <%boolean messageSent = false;
            	 HttpSession session1 = request.getSession();
            	 Object msg = session1.getAttribute("messageSent");
            	 if(msg!= null){
            	 messageSent=(Boolean)msg;}%>
            	 if(<%=messageSent%>){
            		 $('#afterFeedBackmailsent').show();
            	 }
    			     createUploader(event, divId);
//    				 showPreviousAttachments();
    			   /* $("#phoneNo").keypress('click', function(e) {
    			    return isNumberKey(e); 
    	                
    	             });*/
    $("#browsedisable").click(function(){
        alert("You cannot attach more than 1 file in a mail, you can add ZIP file as attachment.");
        return false;
    });
    $('#feedbackSubmit').click(function (event) {
      submitclick(event);  
    });
    
    $('#feedbackClear').click(function (event) {
      cleardata(event); 
    });
    
    
    $('#name').click(function(){
        $('#afterFeedBackmailsent').hide();
    });
    $('#email').click(function(){
        $('#afterFeedBackmailsent').hide();
    });
    $('#phoneNo').click(function(){
        $('#afterFeedBackmailsent').hide();
    });
    $('#subject').click(function(){
        $('#afterFeedBackmailsent').hide();
    });
    $('#mailBody').click(function(){
        $('#afterFeedBackmailsent').hide();
    });
});
	function cleardata(event) {
		//alert("tets");
		//event.stopPropagation();
		event.preventDefault();
		$("#name").val("");
		$("#email").val("");
		$("#phoneNo").val("");
		$('.qq-upload-list').html("");
		$("#uploadFileName").val("");
		$("#file").val("");
		$("#mailBody").val("");
		$("#subject").val("");
		var f = $("#feedback");
		f.attr('action', "/suppcustservice.html?feedbackClear='feedbackClear'");
		f.submit();
	}
	function submitclick(event) {
             	//event.stopPropagation();
		event.preventDefault();
		if ($("#name").val() == "" || $("#name").val() == null) {
			 $('#afterFeedBackmailsent').hide();
			alert("Name cannot be left blank.");
			$("#name").focus();
			return false;
		}
		if ($("#email").val() == "" || $("#email").val() == null) {
			 $('#afterFeedBackmailsent').hide();
			alert("Email cannot be left blank.");
			$("#email").focus();
			return false;
		}
		
		var email = $("#email").val();
	if (!isValidMail(email)) {
		 $('#afterFeedBackmailsent').hide();
			alert("Please enter a valid email address.");
			$("#email").focus();
			return false;
		}
		if (!$('#phoneNo').val() == "" ){
			if(!ValidateForm()) {
			return false;
			}}
		if( $('#subject').val() == "" && $('#mailBody').val() == "" ) {
			 $('#afterFeedBackmailsent').hide();
			alert("Please enter either subject or mail body.");
			return false;
                    }else{
                        $('#loadImage').show();
                        $('#feedbackSubmit').prop("disabled", true);
                        $.ajax({
                                method: "POST",
                                url : "suppcustservice.html?feedbackSubmit='feedbackSubmit'&name="
						+ $('#name').val() + "&email="
						+ $('#email').val() + "&phoneNo=" + $('#phoneNo').val()
                                                + "&subject=" + $('#subject').val()+ "&mailBody=" + $('#mailBody').val()
                                                +"&attachments=" + $('#attachments').val(),
                                processData: true,
                                data: {},
                                dataType: "json",
                                success: function(data){
                                    $('#feedbackSubmit').prop("disabled", false);
                                    $('#loadImage').hide();
                                    $('#afterFeedBackmailsent').show();
                                    $('#name').val("");
                                    $('#email').val("");
                                    $('#phoneNo').val("");
                                    $('#subject').val("");
                                    $('#mailBody').val("");
                                    $('#attachments').hide();
                                }

                        });
			}
	}
</script>
<form:form commandName="feedback" method="POST">
	<div id="tm" class="portlet vn">
            <p class="fordesc Regular color2">At LXRMarketplace, we are passionate about adding value to our
			user's life, fanatical about our products  and dedicated to provide
			exemplary Customer Service! Whether you have queries about our
			applications or any new ideas, we want to talk with YOU.</p>
		<br>
		<table class="feedbackForm color2">
                    <tr>
                        <td class="Bold col1"><p>Name <span>*</span></p>
                        </td>
                        <td class="Regular col2">
                            <label>
                                <form:input path="name" maxlength="50" /> 
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="Bold col1"><p>Email Id <span>*</span></p></td>
                        <td class="Regular col2">
                            <label> 
                                <form:input path="email" maxlength="50" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="Bold col1"><p>Phone </p></td>
                        <td class="Regular col2">
                            <label> 
                                <form:input path="phoneNo" maxlength="50" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="Bold col1"><p>Subject</p> </td>
                        <td class="fullwidth Regular col2">
                            <label> 
                                <form:input path="subject" maxlength="250" size="61" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="fullwidth">
                            <div id="file">
                                <div id="attfile" class="Bold col1">
                                    <p>Attach a file:</p>
				</div>
                                <div class="smallBrowseButton chfile" id="browse1" alt=" " title="CHOOSE A FILE TO ATTACH"></div>
                            </div>
                            <div id="filedisable" width="100%" style="display: none;">
                                <div id="attfile" class="Bold col1">
                                    <p>Attach a file: </p>
                                </div>
                                <div id="browsedisable" class="smallBrowseButtonDisabled chfile" alt=" " title="CHOOSE A FILE TO ATTACH"></div>
                            </div>
                            <div class="attachments-wrap">
                                <table id="attachments">
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="Bold col1">Mail Body</td>
                        <td class="textareabox Regular col2">
                            <form:textarea cols="70" rows="15" path="mailBody"></form:textarea>
                        </td>
                    </tr>
                    <tr class="inputs">
                        <td>
                        </td>
                        <td>
                            <div id="afterFeedBackmailsent" class="Bold">Mail Sent Successfully</div>
                                <div id="loadImage" style="display: none;float: left">
                                    <img src="../images/tools/processing.gif" alt="Processing" title="Processing" />
                                </div>
                            <div id="buttons">
                                <input type="button" id="feedbackSubmit" class="submit Bold" title="SUBMIT" value="SUBMIT" /> 
                                <input type="reset" id="feedbackClear" class="clear Bold" title="CLEAR" value="CLEAR" />
                            </div>
                        </td>
                    </tr>
		</table>
		
	</div>
</form:form>
