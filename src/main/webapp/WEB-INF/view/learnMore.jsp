
<%-- 
    Document   : learnMore
    Created on : 8 Jul, 2016, 2:16:27 PM
    Author     : vemanna
--%>
<%@ page import="java.util.Map,java.util.HashMap"%>
<link rel="stylesheet" media="all" type="text/css" href="/css/learnmore.css" />
<%
    Map<Long, Integer> toolQuestionsCount = new HashMap<>();
    int numberofToolQuestions = 0;
    if (session.getAttribute("toolQuestionsCount") != null) {
        toolQuestionsCount = (Map<Long, Integer>) session.getAttribute("toolQuestionsCount");
        if (toolQuestionsCount.containsKey(toolDBId)) {
            numberofToolQuestions = toolQuestionsCount.get(toolDBId);
        }
    }
%>
<script>
    <%
        out.println("var numberofToolQuestions=" + numberofToolQuestions + ";");
        out.println("var toolDBId=" + toolDBId + ";");
    %>

    $(document).ready(function () {
        if (numberofToolQuestions > 2) {
            $("#learnMoreMenu").show();
            if (parentUrl.indexOf("#learn-more") !== -1) {
                getToolRelatedQuestion();
            }
        }

    });
    function getToolRelatedQuestion()
    {
        $('#overview').hide();
        $('#tool').hide();
        $('#rateit').hide();
        $('#learnMore-Content').show();
        $('#learn-more').show();
        $('#loadLearnMore').show();
        
        $.ajax({
            type: "POST",
            url: "/related-tool-qusetions.html?toolId=" + toolDBId,
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                getToolQuestions(data);
                $('#loadLearnMore').hide();
            }
        });

    }
    function getToolQuestions(data)
    {
        var toolQuestions = "<div id='questions-table'>";
        for (var i = 0; i < data.length; i++)
        {
            toolQuestions += "<div class='question'><p>" + data[i].question + "</p></div>"
                    + "<div class='answer' style='word-wrap: break-word;'><p>" + data[i].answer + "</p></div>";
        }
        toolQuestions += "</div>";

        $("#learnMore-Content").html(toolQuestions);
    }
    function getOverview()
    {
        showMenus();
    }
    function getToolPage()
    {
        showMenus();
    }
    function getHowtouse()
    {
        showMenus();
    }
    function getrateit()
    {
        showMenus();
    }
    function showMenus()
    {
        $('#overview').show();
        $('#tool').show();
        $('#rateit').show();
        $('#learn-more').hide();
    }
</script>
<!-- Learn more section Starts -->
<section id="learn-more" class="learnMoreTab" style="display: none;">
    <h1 class="Bold color1" style="text-align: left;">Learn more</h1>
    <div id="toolDivId" class="inner-content">
        
    <div id="loadLearnMore" style="display: none;">
                    <img src="../images/tools/processing.gif" alt="Processing"
                            title="Processing" />
    </div>
        <p id="learnMore-Content"></p>
    </div>
</section>
<!-- Learn more section ends -->