
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Login | LXRMarketplace</title>
        <script type="text/javascript">
            $(document).ready(function () {
                
                var isLoggedInUserId = 0;
                var isUserLogin = <%=userLoggedInH%>;
                var uname = "";
                var pwd = "";
                <%if(userH != null){%>
                    isLoggedInUserId = <%=userH.getId()%>;
                    uname = "<%=userH.getUserName()%>";
                    pwd = "<%=userH.getPassword()%>";
                <%}%>
                if(!isUserLogin && isLoggedInUserId > 0)
                {
                    $.ajax({
                        type: "POST",
                        url: "/default-login.html?userId="+isLoggedInUserId,
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            if(data[0] === "success")
                            {
                                defaultLogin();
                            }
                        }
                    });
                }
                /*if user comes from mail link we should get the user analysis data of that link.
                  if registered user then we should do auto login and then process that data
                  otherwise simply process that data*/
                var numberOfParameters = (parentUrl.match(/&/g) || []).length;
                if((parentUrl.indexOf("user-id") !== -1 && parentUrl.indexOf("tracking-id") !== -1 
                        && numberOfParameters < 2 && parentUrl.indexOf("/#home") === -1) 
                        || (parentUrl.indexOf("question-id") !== -1 && parentUrl.indexOf("#tool") !== -1))
                {
                    var userId = getUrlVars()["user-id"];
                    var websiteTrackingId = getUrlVars()["tracking-id"];
                    var questionId = getUrlVars()["question-id"];
                    var urlRequest = "";
                    if(parentUrl.indexOf("question-id")  !== -1){
                        urlRequest = "/user-analysis-data.html?question-id="+questionId;
                    }else {
                        urlRequest = "/user-analysis-data.html?user-id="+userId+"&tracking-id="+websiteTrackingId;
                    }
                    $.ajax({
                        type: "GET",
                        url: urlRequest,
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            if(data !== null){
                                var website = data[1];
                                var userInput = data[2];
                                var userName = data[3];
                                var password = data[4];
                                var name = data[5];
//                                For auto login
                                if(!isUserLogin && password !== "" && password !== null){
                                    $.ajax({
                                        type: "POST",
                                        url: "/login.html?popuplogin='popuplogin'&username=" + userName + "&password=" + password,
                                        processData: true,
                                        data: {},
                                        dataType: "json",
                                        success: function (data) {
                                            changeHeaders("subHeader1", name);
                                            changeH("mobile", name);
                                            getUserToolInputs(website, userInput);
                                        }
                                    });
                                } else{
                                    getUserToolInputs(website, userInput);
                                }
                            }else{
                                alert("Not found any user with this id. Please try again.");
                            }
                        }
                    });
                }
                
                var isIE = (navigator.userAgent.indexOf("MSIE") !== -1);
                if(isIE){
                    $('[placeholder]').focus(function() {
                        var input = $(this);
                        if (input.val() === input.attr('placeholder')) {
                          input.val('');
                          input.removeClass('placeholder');
                        }
                      }).blur(function() {
                        var input = $(this);
                        if (input.val() === '' || input.val() === input.attr('placeholder')) {
                          input.addClass('placeholder');
                          input.val(input.attr('placeholder'));
                        }
                    }).blur();
                }
        

                $("#username").keypress(function (e) {
                    var error = document.getElementById('error');
                    error.innerHTML = "";
                    $("#errorLogin").text("");
                    if (e.keyCode === 13) {
                        $("#logInPassword").focus();
                    }
                });

                $("#logInPassword").keypress(function (e) {
                    var error = document.getElementById('error');
                    error.innerHTML = "";
                    $("#errorLogin").text("");
                    if (e.keyCode === 13) {
                            $("#sign").click();
                    }
                });
                parentUrl = top.location.href;

                $('#sign').click(function () {
                    var error = document.getElementById('error');
                    error.innerHTML = "";
                    $("#errorLogin").text("");
                    loginSubmit();
                });
                
                function defaultLogin(){
                    pwd = encodeURIComponent(pwd);
                    $.ajax({
                        type: "POST",
                        url: "/login.html?popuplogin='popuplogin'&username=" + uname + "&password=" + pwd +"",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            changeHeaders("subHeader1", "<%=uName%>");
                            changeH("mobile", "<%=uName%>");
                            getNotificationCount();
                        }
                    });                    
                }
                
                function loginSubmit() {
                    var username = $.trim($("#username").val());
                    var password = $.trim($("#logInPassword").val());
                    var isLoginRemember = "false";
                    if($("#isLoginRemember").prop("checked") === true){
                        isLoginRemember = "true";
                    }
                    password = encodeURIComponent(password);
                    if (username === "" || username === "Email") {
                        alert("Please enter your email address.");
                        $("#username").focus();
                        return false;
                    } else {
                        if (!isValidMail(username)) {
                            alert("Please enter a valid email address.");
                            $("#username").focus();
                            return false;
                        }
                    }
                    if (!$("#logInPassword").val()) {
                        alert("Please enter your password.");
                        $("#logInPassword").focus();
                        return false;
                    }

            <%String queryStringL = (String) request.getParameter("getResult");
                    String queryString2L = (String) request.getParameter("backLink");
                    String parUrlL = "";
                    if ((queryStringL != null && !queryStringL.trim().equals("")) || (queryString2L != null && !queryString2L.trim().equals(""))) {
                parUrlL = "sucPage";
                    }%>
                    $.ajax({
                        type: "POST",
                        url: "/login.html?popuplogin='popuplogin'&username=" + username + "&password=" + password +"&isLoginRemember=" + isLoginRemember + "&page=<%=parUrlL%>",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            var error = document.getElementById('error');
                            error.innerHTML = "";
                            $("#errorLogin").text("");
                            if (data[0] === "success") {
                                if ((parentUrl.indexOf("getResult") !== -1) || (parentUrl.indexOf("backLink") !== -1) || (parentUrl.indexOf("Invalid") !== -1)) {
                                    parent.window.location = parentUrl + "&loginRefresh=1";
                                }else {
//                                    if((parentUrl.indexOf("/lxr-adwords-plugin.html?signUp=pluginSignUp") !== -1) ||
//                                            (parentUrl.indexOf("/lxr-plugin-payment.html?submit=newCard") !== -1)){
//                                        parent.window.location = parentUrl.split("?")[0];
//                                    }else 
                                        if((parentUrl.indexOf("/#seo-heading") !== -1)|| (parentUrl.indexOf("/#ppc-tools") !== -1)||
                                            (parentUrl.indexOf("/#ecommerce-tools") !== -1)|| (parentUrl.indexOf("/#home") !== -1)){
                                         parent.window.location = parentUrl.split("#")[0];
                                    }else{
                                         parent.window.location = parentUrl;
                                    }
                                }

                            }else if (data[0] === "dashboard"){
                                parent.window.location = data[1];
                            } else {
                                error.innerHTML = data[0];
                                $("#errorLogin").text(data[0]);

                            }
                        }
                    });
                }

                function isValidMail(email) {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
                    return filter.test(email);
                }

                 $('#username').click(function(){
                   var error = document.getElementById('error');
                    error.innerHTML = "";
                    $("#errorLogin").text("");
               });
               $('#logInPassword').click(function(){
                   var error = document.getElementById('error');
                    error.innerHTML = "";
                    $("#errorLogin").text("");
               });
            });
//            To get the query string parameters by name
            function getParameterByName(name, url) {
                if (!url) {
                  url = window.location.href;
                }
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }
            
            function getUrlVars() {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                    vars[key] = value;
                });
                return vars;
            }
            
        </script>
    </head>
    <body>
        <div class='d-wrapper'>
        <input type="text" id="username" name="email" value="" placeholder='Email'>
        <input type="password" id="logInPassword" name="pwd" value="" placeholder='Password'>
        <input type="text" name="fake_pass" id="fake_pass" value="Enter Password" style="display:none"/>
        <div id="errorLogin" class="inValid Regular color2" style=""></div>
        <div class="isLoginRemember">
            <input type="checkbox" id="isLoginRemember" checked><span>Remember Me</span>
        </div>
        <div id="forgot-login-Password">
            <span id="loginPassword" class="Regular color2" ><a href="/forgotpassword.html" target="_parent" style="color:#FFF">Forgot password? </a></span>
        </div>
        <input type='button' name='signin' id='sign'/>
        </div>
        <div class='m-wrapper' style='display:none;'>
        <input type="text" id="username" name="email" value="" placeholder='Email'>
        <input type="password" id="logInPassword" name="pwd" value="" placeholder='Password'>
        <div id="error" class="inValid Regular color2" style=""></div>
        <div id="forgot-login-Password">
            <span id="loginPassword" class="Regular color2" ><a href="/forgotpassword.html" target="_parent" style="color:#FFF">Forgot password? </a></span>
        </div>

        <input type='button' name='signin' id='sign'/>
        </div>
    </body>
</html>

