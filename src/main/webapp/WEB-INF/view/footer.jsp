
<%@page import="lxr.marketplace.user.Login"%>
<!-- Footer Starts -->
<%
    HttpSession sessionF = request.getSession();
    Login user1 = (Login) sessionF.getAttribute("user");
    Object ul1 = sessionF.getAttribute("userLoggedIn");
    boolean userLoggedIn1 = false;
    if (ul1 != null) {
        userLoggedIn1 = (Boolean) ul1;
    }
%>
<script>
    <%out.println("var userLoggedIn1=" + userLoggedIn1 + ";");%>
    $(document).ready(function () {
        var userName = "";
        var emailId = "";
        if (userLoggedIn1) {
            <%if(user1 != null){%>
                var userName = "<%=user1.getName()%>";
                var emailId = "<%=user1.getUserName()%>";
                sessionStorage.setItem('name', userName);
                sessionStorage.setItem('email', emailId);
            <%}%>
        }
        var d = new Date();
        $("#copyright-text").html("&copy NetElixir 2011 - "+ d.getFullYear()+". All Rights Reserved.");
    });

</script>
<div class="footer">
    <div class="copyright">
        <p><a href="http://www.netelixir.com/" target="_blank" id="copyright-text"></a></p>
    </div>
    <div class="links">
        <ul>
            <li><a href="/suppcustservice.html">Contact Us</a></li>							
            <li><a href="/privacyPolicy.jsp" target="_blank" >Privacy Policy</a></li>
            <li><a href="/termsofService.jsp" target="_blank" >Terms of Service</a></li>
            <li class="link-img">
                <a href="https://www.facebook.com/Lxrmarketplace-1657035624516281/" target="_blank">
                    <img src="../images/footer/facebook.png" alt="Facebook"/>
                </a>
            </li>
<!--          Removeing Google Plus as consumer version of Google+ is shutting down in Feb 04,2019 
<li class="link-img">
                <a id="googlePage" href="//plus.google.com/107793863097965760189?prsrc=3"
                   rel="publisher" target="_blank" style="text-decoration: none;">
                    <img src="../images/footer/google.png" alt="Google"/>
                </a>
            </li>-->
            <li class="link-img">
                <a href="http://twitter.com/LXRMarketplace" target="_blank">
                    <img src="../images/footer/twitter.png" alt="Twitter"/>
                </a>
            </li>
        </ul>
    </div>
</div><!-- Footer Ends -->
<script type='text/javascript'>
    var url = window.top.location.href;
    if (url.indexOf("/LXRSEO/") !== -1) {
        $('#googlePage').attr('href', '//plus.google.com/103693162109032910556?prsrc=3');
    } else {
        $('#userGuide').hide();
    }
</script>
<!-- end of footer div -->
