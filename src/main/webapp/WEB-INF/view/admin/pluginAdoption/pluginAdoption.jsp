<%-- 
    Document   : lxrPluginReport
    Created on : 8 Mar, 2017, 11:39:13 AM
    Author     : NE16T1213
--%>


<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,java.util.Calendar,java.util.Map,java.util.TreeSet,java.util.Set,lxr.marketplace.user.LoginService"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@page import="org.springframework.web.context.WebApplicationContext"%>
    <%@page
        import="org.springframework.web.context.support.WebApplicationContextUtils"%>
        <%   HttpSession sessionR = request.getSession();
            Login user = (Login) sessionR.getAttribute("user");
            Object ul = sessionR.getAttribute("userLoggedIn");
            boolean userLoggedIn = false;
            ExpertUser expertUser = null;
            if (ul != null) {
                userLoggedIn = (Boolean) ul;
            }
            if (userLoggedIn) {
                if (sessionR.getAttribute("expertUser") != null) {
                    expertUser = (ExpertUser) sessionR.getAttribute("expertUser");
                    pageContext.setAttribute("expertUser", expertUser);
                }
            }
        %>
        <html>
            <head>
                <title>LXRMarketplace | LXRPlugin Report</title>
                <meta name="keywords" content="LXRPlugin Report" />
                <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

                <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
                <%@ include file="/WEB-INF/view/commonImport.jsp"%>
                <%@ include file="/WEB-INF/view/header.jsp"%>

                <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.datepicker.js"></script>
                <script type="text/javascript" src="/scripts/tools/pluginAdoption.js"></script>
                <link rel="stylesheet" href="/css/lxrm-core.css">
                <link rel="stylesheet" href="/css/admin.css">
                <link rel="stylesheet" href="/css/pluginAdoption.css">
                <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">


                <script type="text/javascript">

                    <%  out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                        Calendar caldar = Calendar.getInstance();
                        caldar.set(Calendar.DAY_OF_MONTH, 1);
                        out.println("var serverFirstDay=" + caldar.getTimeInMillis() + ";");
                        out.println("var userLoggedIn=" + userLoggedIn + ";");
                    %>
                    var selectedAccountsArray = [];
                    $(document).ready(function () {

                        $('#selectPluginReports').prop('selectedIndex', 0);
                        var role = "";
                    <c:if test="${not empty expertUser}" >
                        var name = "${expertUser.name}"
                        role = "${expertUser.role}"
                        if (name.length > 15) {
                            name = name.substring(0, 15);
                        }
                        $('#uName').text(name + " (" + role + ")");
                    </c:if>
                        if (role === 'expert') {
                            $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        } else if (role === 'reviewer') {
                            $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        }
                        $('#templates').hide();
                        var logo = document.getElementById("logo");
                        logo.href = "/dashboard.html";
                        $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    <%if (user != null) {
                            out.println("$(\"#loginSuccess\").show();");
                        } else {
                            out.println("$(\"    #loginSuccess\").hide();");
                        }%>

                        fetchAccounts();

                        $("#selectPluginReports").change(function () {
                            var pluginType = $("#selectPluginReports").val();
                            if (pluginType === "custom") {
                                $("#startPluginDate").val("");
                                $("#endPluginDate").val("");
                                $(".customDateWrapper").show();
                            } else {
                                $(".customDateWrapper").hide();
                            }
                        });
                        $("#downloadPluginReport").bind('click', function () {
                            var reportType = $("#selectPluginReports").val();
                            var startAdoptionDate = $("#startPluginDate").datepicker("getDate");
                            var endAdoptionDate = $("#endPluginDate").datepicker("getDate");
                            if (reportType === "select") {
                                alert("Please select  date range for report.");
                                return false;
                            } else if (reportType === "custom") {
                                if ($("#startPluginDate").val().trim() === "") {
                                    alert("Please select the Start Date");
                                    $("#startPluginDate").focus();
                                    return false;
                                } else if ($("#endPluginDate").val().trim() === "") {
                                    alert("Please select the End Date");
                                    $("#endPluginDate").focus();
                                    return false;
                                }

                                if (startAdoptionDate > endAdoptionDate) {
                                    alert("Start Date is less than End Date, \n please select a valid date range");
                                    $("#startPluginDate").focus();
                                    return false;
                                }
                            }
                            startAdoptionDate = $("#startPluginDate").val();
                            endAdoptionDate = $("#endPluginDate").val();

                            $("#pluginDownloadForm").closest('form').find("input[type=hidden]").val("");
                            $("#pluginDownloadForm").closest('form').find("input[type=hidden]").remove();
                            document.getElementById("pluginDownloadForm").reset();


                            var pluginForm = $("#pluginDownloadForm");
                            var downloadType = $("<input>").attr("type", "hidden").attr("name", "pluginReportPeriod").attr("id", "accountPeriod").val(reportType);
                            var userStartDate = $("<input>").attr("type", "hidden").attr("name", "startPluginDate").attr("id", "reportStart").val(startAdoptionDate);
                            var userEndDate = $("<input>").attr("type", "hidden").attr("name", "endPluginDate").attr("id", "reportEnd").val(endAdoptionDate);

                            pluginForm.append($(downloadType));
                            pluginForm.append($(userStartDate));
                            pluginForm.append($(userEndDate));


                            $('#selectPluginReports').prop('selectedIndex', 0);
                            $(".customDateWrapper").hide();
                            pluginForm.attr('method', "POST");
                            pluginForm.attr('action', "/plugin-adoption.html?action=download");
                            pluginForm.submit();

                        });

                        $("#addTeamEmail").bind('click', function () {
                            $('#processing').hide();
                            generateTeamEmail();
                            
                        });

                        $("#addPluginAccount").bind('click', function () {
                            $('#processing').hide();
                            resetAccountForm();
                            $("#adoptionPopUp").show();
                            $("#accountAddPopUp").show();
                            $("#accountTitle").html("Add New Account");
                            $("#accountSubmit").show();
                            $("#accountUpdate").hide();
                        });

                        $("#accountCancel").bind('click', function () {
                            $('#processing').hide();
                            $("#adoptionPopUp").hide();
                            $("#accountAddPopUp").hide();
                            $("#accountUpdate").hide();
                            resetAccountForm();
                        });
                        $("#accountSubmit").bind('click', function () {
                            $("#accountUpdate").hide();
                            accountSubmit();
                        });

                        $("#accountUpdate").bind('click', function () {
                            updateAccountDetails();
                        });

                        $("#accountState").bind('click', function () {
                            updateAccountState();
                        });

                        $("#downloadPluginReport").bind('click', function () {
                        });
                        $("#deletePluginAccount").bind('click', function () {
                            deletePluginAccounts();
                        });

                        // Showing add account popup if there is any error
                        var error = getUrlVars()["action"];
                        if (error !== undefined && error === 'save') {
                            $("#adoptionPopUp").show();
                            $("#accountAddPopUp").show();
                        }
                    });/*End on Ready*/
                    $(function () {
                        $("#startPluginDate, #endPluginDate").datepicker({
                            showOn: "both",
                            buttonImage: "../css/0/images/calendar.jpg",
                            buttonImageOnly: true,
                            dateFormat: 'yy-mm-dd',
                            maxDate: new Date(serverTime),
                            changeMonth: true,
                            changeYear: true,
                            constrainInput: true
                        });
                    });

                    $(function () {
                        $("#tabs-icons").tabs();
                    });
                    function getUrlVars() {
                        var vars = {};
                        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                            vars[key] = value;
                        });
                        return vars;
                    }
                    function naviToOtherTabs(menuOption) {
                        var menuParams = {menuOption: menuOption};
                        $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                            if (data !== null) {
                                parent.window.location = data;
                            }
                        });
                    }

                    function deletePluginAccounts() {
                        if (selectedAccountsArray.length === 0) {
                            $(".accountSelectWrapper :checked").each(function () {
                                selectedAccountsArray.push($(this).val());
                            });
                        }

                        var accountSelected = false;

                        if (selectedAccountsArray.length > 0) {
                            accountSelected = true;
                        }
                        if (accountSelected === false) {
                            alert("Please select account to delete.");
                            selectedAccountsArray = [];
                            selectedAccountsArray.length = 0;
                            $(".accountSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                            return false;
                        } else {
                            var conformation = true;
                            conformation = confirm("Do you want to delete the selected Account(s)?");
                            if (conformation === false) {
                                selectedAccountsArray = [];
                                selectedAccountsArray.length = 0;
                                $(".accountSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                                return false;
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "/plugin-adoption.html?action=delete",
                                    processData: true,
                                    data: {'accountsDeleteArray': selectedAccountsArray},
                                    dataType: "json",
                                    traditional: true,
                                    success: function (data) {
                                        alert(data);
                                        $(".accountSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                                        fetchAccounts();
                                        selectedAccountsArray = [];
                                        selectedAccountsArray.length = 0;
                                    },
                                });
                            }
                        }
                    }
                </script>
            </head>
            <body>
                <div class="responsive-wrapper">
                    <div class="middle-wrapper">
                        <div id="content">
                            <div id="tabs-icons" style='padding:0;'>
                                <ul>
                                    <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                          class="ui-state-default ui-corner-top " 
                                                          onmouseover="this.style.cursor = 'pointer'" 
                                                          onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                    <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'"
                                                        onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                    <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                         onclick="naviToOtherTabs('autoMail')" 
                                                         class="ui-state-default ui-corner-top"
                                                         onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                    <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                        onclick="naviToOtherTabs('settings')" 
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                    <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                              onclick="naviToOtherTabs('knowledgeBase')" 
                                                              class="ui-state-default ui-corner-top"
                                                              onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                    <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                              onclick="naviToOtherTabs('expertAnswer')" 
                                                              class="ui-state-default ui-corner-top"
                                                              onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                    <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                           onclick="naviToOtherTabs('expertUser')"
                                                           class="ui-state-default ui-corner-top" 
                                                           onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                    <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top" 
                                                           onclick="naviToOtherTabs('ateReports')" 
                                                           onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                    <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" 
                                                               onmouseover="this.style.cursor = 'pointer'">LXRPlugin</a></li>
                                </ul>
                            </div>
                            <div id="lxrPluginResultDiv" >
                                <div class="downloadReportsWrapper">
                                    <form id="pluginDownloadForm">
                                        <div class="reportSelectionWrapper">
                                            <div class="text-label">Download Report: </div>
                                            <div class="input-field">
                                                <select id="selectPluginReports">
                                                    <option value="select">Select</option>
                                                    <option value="lastMonth">Last Month</option>
                                                    <option value="thisMonth">This Month</option>
                                                    <option value="custom">Custom</option>
                                                </select>
                                            </div>
                                            <div class="customDateWrapper">
                                                <div id="startDateSelection" class="lxrPlugin-custom-report">
                                                    <div class="text-label">Start Date: </div>
                                                    <div class="input-field">
                                                        <input id="startPluginDate" readonly size="12" />
                                                    </div>
                                                </div>
                                                <div id="endDateSelection" class="lxrPlugin-custom-report">
                                                    <div class="text-label">End Date: </div>
                                                    <div class="input-field">
                                                        <input id="endPluginDate" readonly size="12" />
                                                    </div>
                                                </div>
                                            </div><!--End of customDateWrapper-->
                                        </div><!--End of reportSelectionWrapper-->
                                        <div class="reportDownloadWrapper">
                                            <div class="input-field input-button">
                                                <input type="submit" value="Download Report" id="downloadPluginReport"  title="DOWNLOAD REPORT">
                                            </div>
                                        </div>
                                    </form>
                                </div><!--End of downloadReportsWrapper-->
                                <div class="lxrPluginAccountsWrapper">
                                    <div class="pluginAccountsOptions">
                                        <div id="accountCountWarpper"></div>
                                        <div class="buttonWrapper">
                                        <button type="button" id="addPluginAccount" class="pluginReportButton">ADD</button>
                                        <button type="button" id="deletePluginAccount" class="pluginReportButton">DELETE</button>
                                        </div>
                                    </div><!--End of pluginAccountsOptions-->    
                                    <div class="pluginAccountsResultWrapper">
                                        <table id="pluginAdoptionAccounts">
                                        </table>
                                        <div id="emptyAccountsData"><p>No accounts available</p></div>
                                    </div>
                                    <div id="adoptionPopUp" style="display: none">
                                        <form:form commandName="pluginAdoptionDTO" method="POST" >
                                            <div id="accountAddPopUp" style="display: none">
                                                <div class="popUpHeader">
                                                    <div id="accountTitle"></div>
                                                    <div id="accountStatusWrapper">
                                                        <button type="button" id="accountState" class="pluginReportButton"></button>
                                                        <input type="hidden" id="accountStatus"/>
                                                    </div>
                                                </div>
                                                <div id="errorMessage"></div>
                                                <div id="successMessage"></div>
                                                <div class="accountsWrapper">
                                                    <form:hidden path="accountId" />
                                                    <div class="pluginAccount">
                                                        <div class="text-label">
                                                            <label>Account Name:</label>
                                                            <p class="mandatory">*</p>
                                                        </div>
                                                        <div class="input-field">
                                                            <form:input path="accountName" cssClass="accountInput" maxlength="50" />
                                                        </div>
                                                    </div>
                                                    <div class="pluginAccount">
                                                        <div class="text-label">
                                                            <label>Account Id:</label>
                                                            <p class="mandatory">*</p>
                                                        </div>
                                                        <div class="input-field">
                                                            <form:input path="customerId" cssClass="accountInput"  maxlength="20" />
                                                        </div>
                                                    </div>
                                                    <div class="pluginAccount">
                                                        <div class="text-label">
                                                            <label>Manager Email:</label>
                                                            <p class="mandatory">*</p>
                                                        </div>
                                                        <div class="input-field">
                                                            <form:input path="managerEmail" cssClass="accountInput" maxlength="50" />
                                                        </div>
                                                    </div>
                                                    <div class="pluginAccount">
                                                        <div class="text-label">
                                                            <label>Team Member Email:</label>
                                                        </div>
                                                        <div id="teamMemberWrapper">
                                                            <div id="teamMemmberDiv-0" class="teamEmailInput">
                                                                <input name="pluginAdoptionTeamDTO[0].teamMemberEmail" type="text"  class="teamMemberEmail" id="teamEmailId-0"/>
                                                            </div>
                                                            <div id="emailDyanmicWrapper" class="teamEmailInput"> </div>
                                                        </div>
                                                    </div>
                                                    <div class="emailWrapper" style="width:78%;text-align: right;">
                                                        <button type="button" id="addTeamEmail" class="pluginReportButton">ADD EMAIL</button>
                                                    </div>
                                                    <div class="pluginAccount" style="text-align: center;">
                                                        <button type="button" id="accountSubmit" class="pluginReportButton">SUBMIT</button>
                                                        <button type="button" id="accountUpdate" class="pluginReportButton">UPDATE</button>
                                                        <button type="button" id="accountCancel" class="pluginReportButton">CANCEL</button>
                                                    </div>
                                                    <div id="processing">
                                                        <img src="/images/orange-loader.gif" alt="Processing" title="Processing" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form:form>
                                    </div>
                                </div><!--End of lxrPluginAccountsWrapper-->
                            </div>

                        </div><!--End of content-->
                    </div><!--End of middle-wrapper-->
                </div><!--End of responsive-wrapper-->
                <%@ include file="/WEB-INF/view/footer.jsp"%>
            </body>
        </html>
