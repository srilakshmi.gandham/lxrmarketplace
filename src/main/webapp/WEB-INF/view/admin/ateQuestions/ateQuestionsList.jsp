<%-- 
    Document   : ateQuestionList
    Created on : 4 Nov, 2016, 11:09:22 AM
    Author     : vemanna
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/view/include.jsp"%>

<%@ page import="lxr.marketplace.user.Login,java.util.Calendar"%>
<%    HttpSession expertSession = request.getSession();
    Login user = (Login) expertSession.getAttribute("user");
    Object ul = expertSession.getAttribute("userLoggedIn");
    boolean userLoggedIn = false;
    ExpertUser expertUser = null;
    if (ul != null) {
        userLoggedIn = (Boolean) ul;
    }
    if (userLoggedIn) {
        if (expertSession.getAttribute("expertUser") != null) {
            expertUser = (ExpertUser) expertSession.getAttribute("expertUser");
            pageContext.setAttribute("expertUser", expertUser);
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>LXRMarketplace | Expert Answers</title>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
        <%@ include file="/WEB-INF/view/commonImport.jsp"%>
        <%@ include file="/WEB-INF/view/header.jsp"%>

        <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
        <script src="js/classie.js"></script>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script type="text/javascript" src="/scripts/tools/ateQuestionsList.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript" src="/scripts/Charts.js"></script>
        <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
        <link rel="stylesheet" href="/css/lxrm-core.css">
        <link rel="stylesheet" href="/css/admin.css">
        <link rel="stylesheet" href="/css/expertAnswer.css">               
        <script type="text/javascript">
            <%
                out.println("var userLoggedIn=" + userLoggedIn + ";");

            %>
            var tempParentURL;
            $(document).ready(function () {
                //Calling with default values
                var role = "";
            <c:if test="${not empty expertUser}" >
                var name = "${expertUser.name}"
                role = "${expertUser.role}"
                if (name.length > 15) {
                    name = name.substring(0, 15);
                }
                $('#uName').text(name + " (" + role + ")");
            </c:if>

                if (role === 'expert') {
                    $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                } else if (role === 'reviewer') {
                    $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                }

                constructFilteredQuery(1, "");
                tempParentURL = window.location.protocol + "//" + window.location.host;
                //Setting  high level
                $("#unAssignedQues").text("${unAssignedQuestions}");
                $("#assignedQues").text("${assignedQuestions}");

                $("#expertAnswerFilter").on('change', function () {
                    var selectedOption = $("#expertAnswerFilter option:selected").val();
                    $("#serachCriteria").removeClass('inputErr');
                    $("#alertError").hide();
                    $("#serachCriteria").val("");
                    if ((selectedOption === "1") || (selectedOption === "2") ||
                            (selectedOption === "3") || (selectedOption === "4") || (selectedOption === "8") || (selectedOption === "10")) {
                        constructFilteredQuery(selectedOption, "");
                        closeCriteria();
                        $("#criteriaInputWrapper").hide();
                    } else if ((selectedOption === "5") || (selectedOption === "6") ||
                            (selectedOption === "7") || (selectedOption === "9")) {
                        $("#criteriaInputWrapper").show();
                    } else {
                        closeCriteria();
                    }
                });

                $("#searchQuestion").click(function () {
                    validateSearchCriteria();
                });

                $('#serachCriteria').keypress(function (e) {
                    if ((e.which === 13)) {
                        validateSearchCriteria();
                        $("#alertError").hide();
                    }
                });
                $("#closeFilter").click(function () {
                    closeCriteria();
                    $("#alertError").hide();
                    $("#serachCriteria").removeClass('inputErr');
                });

                var logo = document.getElementById("logo");
                logo.href = "/dashboard.html";
                $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,#about,#blog,.settings").hide();
                $("#tabs-icons").tabs();
            <%   if (user != null) {
                    out.println("$(\"#loginSuccess\").show();");
                } else {
                    out.println("$(\"#loginSuccess\").hide();");
                }
            %>
            });

            function naviToOtherTabs(menuOption) {
                var menuParams = {menuOption: menuOption};
                $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                    if (data !== null) {
                        parent.window.location = data;
                    }
                });
            }

        </script>
    </head>
    <body>
        <div class="responsive-wrapper">

            <div class="middle-wrapper">
                <div id="content">
                    <div id="expertAnswersWrapper">
                        <div id="tabs-icons">
                            <ul>
                                <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                      class="ui-state-default ui-corner-top " 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                     onclick="naviToOtherTabs('autoMail')" 
                                                     class="ui-state-default ui-corner-top"
                                                     onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                    onclick="naviToOtherTabs('settings')" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                          onclick="naviToOtherTabs('knowledgeBase')" 
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top ui-state-active"
                                                          onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                       onclick="naviToOtherTabs('expertUser')" 
                                                       class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                       onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top 
                                                       ui-tabs-selected" 
                                                       onclick="naviToOtherTabs('ateReports')" 
                                                       onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                            </ul>
                            <div class="expertAnswersBlock" >
                                <div class="adminOperationsWrapper">
                                    <div class="userQuestionStatus">
                                        <div class="questionsWrapper">
                                            <label>Total Unassigned Questions: </label>
                                            <label id="unAssignedQues"></label>
                                        </div>
                                        <div class="questionsWrapper">
                                            <label>Your assigned Questions: </label>
                                            <label id="assignedQues"></label>
                                        </div>
                                    </div>
                                    <div class="adminFilter">
                                        <div class="filterWrapper">
                                            <label>
                                                Selection Criteria:
                                            </label>
                                            <div class="criteriaSelection">
                                                <select  id="expertAnswerFilter">
                                                    <option value="4">All Questions</option>
                                                    <option value="10">All Unanswered Questions [In Review]</option>
                                                    <option value="1" selected>Unassigned & My Open Questions</option>
                                                    <option value="3">Unassigned Questions</option>
                                                    <option value="2">My Open Questions</option>
                                                    <option value="8">My Answered Questions</option>
                                                    <option value="5">Search by Email</option>
                                                    <option value="6">Search by Question</option>
                                                    <option value="7">Search by Tool</option>
                                                    <option value="9">Search by Status [Unassigned,Assigned,In Review,Answered]</option>

                                                </select>
                                            </div>
                                            <!--End of criteria-options-wrapper div--> 
                                            <div id="criteriaInputWrapper">
                                                <input type="text" id="serachCriteria">
                                                <input type="submit" id="searchQuestion" class="submitStyle" value="Search">
                                                <input type="submit" id="closeFilter" class="submitStyle" value="Clear">
                                                <p id="alertError"> Please enter your search query.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="expertAnswersResult">
                                    <table id="userATEQuestion"></table>
                                    <div id="emptyQuestionData">No Data Available.</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <%@ include file="/WEB-INF/view/footer.jsp"%>
    </body>
</html>
