
<%-- 
    Document   : ateQuestionEdit
    Created on : 7 Nov, 2016, 2:03:45 PM
    Author     : vemanna
--%>

<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page import="lxr.marketplace.user.Login,java.util.Calendar"%>
<%    HttpSession expertSession = request.getSession();
    Login user = (Login) expertSession.getAttribute("user");
    Object ul = expertSession.getAttribute("userLoggedIn");
    boolean userLoggedIn = false;
    ExpertUser expertUser = null;
    if (ul != null) {
        userLoggedIn = (Boolean) ul;
    }
    if (userLoggedIn) {
        if (expertSession.getAttribute("expertUser") != null) {
            expertUser = (ExpertUser) expertSession.getAttribute("expertUser");
            pageContext.setAttribute("expertUser", expertUser);
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>LXRMarketplace | Expert Answer Edit</title>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
        <%@ include file="/WEB-INF/view/commonImport.jsp"%>
        <%@ include file="/WEB-INF/view/header.jsp"%>

        <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
        <script src="js/classie.js"></script>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

        <script type="text/javascript" src="/scripts/tools/knowledgeBase_text_editor.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/knowledgeBase_text_editor.css">


        <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript" src="/scripts/Charts.js"></script>

        <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
        <link rel="stylesheet" href="/css/lxrm-core.css">
        <link rel="stylesheet" href="/css/admin.css">
        <link rel="stylesheet" href="/css/ateQuestionEdit.css">  

        <script type="text/javascript">
            <%
                out.println("var userLoggedIn=" + userLoggedIn + ";");
            %>
            var answerTemplateModified = false;
            var role = "";
            var fileUploadLimit = 7000000;
            $(document).ready(function () {

            <c:if test="${not empty expertUser}" >
                var name = "${expertUser.name}"
                role = "${expertUser.role}"
                if (name.length > 15) {
                    name = name.substring(0, 15);
                }
                $('#uName').text(name + " (" + role + ")");
            </c:if>

                if (role === 'expert') {
                    $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                } else if (role === 'reviewer') {
                    $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                }

                //fetching question id from url
                var questionId = getUrlVars()["question-id"];
                if (questionId !== null && questionId !== undefined) {
                    populateData(questionId);
                } else {

                }

                $("#answerTemplate").Editor();
                var logo = document.getElementById("logo");
                logo.href = "/dashboard.html";
                $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#about,#blog").hide();
                $("#tabs-icons").tabs();
            <%   if (user != null) {
                    out.println("$(\"#loginSuccess\").show();");
                } else {
                    out.println("$(\"#loginSuccess\").hide();");
                }
            %>


                $("#cancelPopUp, #cancelSubmission").click(function () {
                    document.getElementById("popupQ").style.display = "none";
                    document.getElementById("editConfirmationPopUp").style.display = "none";
                });

                $('#submitAnswer').click(function () {
                    if (!answerTemplateModified) {
                        var selectedVal = $('input[name=sendAns]:checked').val();
                        if (selectedVal === 'yes') {
                            sendAnswerToUser();
                        } else if (selectedVal === 'no') {
                            document.getElementById("editConfirmationPopUp").style.display = "none";
                            document.getElementById("popupQ").style.display = "none";
                        } else {
                            $('#errorMsg').text("Please select yes button to submit.");
                        }
                    } else {
                        alert("You have made changes either to the Answer or to the File uploading. Please make sure to Save your changes before sending an email.");
                    }
                });


                //Notifying events if user changed answers template or upload file
                $('#attachedFiles').change(function () {
                    answerTemplateModified = true;
                });

                $('.Editor-editor').keyup(function (event) {
                    if (event.which !== 13) {
                        answerTemplateModified = true;
                    }
                });

                $("#clearFile").click(function () {
                    answerTemplateModified = true;
                    var questionId = $('#quesId').val();
                    var params = {questionId: questionId};
                    $.post("/ate-edit-question.html?delete=file", params,
                            function (data) {
                                if (data) {
                                    $("#fileDownloadWrapper").hide();
                                    $("#attachedFileName").empty();
                                    $("#attachedFiles").show();
                                    $("#attachedFiles").val('');
                                }
                            });
                });

                $("#downloadFile").click(function () {
                    var f = $("#expertQuestionEditDTO");
                    f.attr('method', "POST");
                    f.attr('action', "/ate-edit-question.html?download=attachment");
                    f.submit();
                });
            });

            function populateData(questionId) {
                var params = {questionId: questionId};
                jQuery.getJSON("/ate-edit-question.html?fetch=ate-answer-template", params,
                        function (data) {
                            if (data !== null) {
                                setData(data);
                            }
                        });
            }
            function setData(data) {

                $('#mailId').text(data.expertQuestionsDTO.userEmail);
                $('#userName').text(data.expertQuestionsDTO.userName);
                $('#toolName').text(data.expertQuestionsDTO.toolName);

                $('#question').text(data.expertQuestionsDTO.question);
                $('#stepsOPS').html(data.steps);
                $("#answerTemplate").Editor("setText", data.answerTemplate);
                var assigneeId = data.assigneeId;
                var reviewerId = data.reviewerId;
                $('#reviewerId').val(reviewerId);
                $('#previousId').val(data.previousId);
                $('#quesId').val(data.expertQuestionsDTO.questionId);
                var queStatus = data.expertQuestionsDTO.status;
                $('#quesStatus').val(quesStatus);
                if (data.attachedFile !== null) {
                    $("#fileDownloadWrapper").show();
                    $("#attachedFileName").html(data.attachedFile);
                    $("#attachedFiles").hide();
                }


                // Showing users inputs based on tool 
                var webpage = data.webpage;
                var toolId = data.toolId;
                var othrInputs = data.otherInputs;

                var jsonOtherInputsData = null;
                if (othrInputs !== null && othrInputs !== "") {
                    jsonOtherInputsData = JSON.parse(othrInputs);
                }

                var a = document.getElementById('clickHere');
                if (webpage === "" && othrInputs === "") {
                    a.href = '/' + data.toolLink;
                } else {
                    a.href = '/' + data.toolLink + "?question-id=" + data.expertQuestionsDTO.questionId + "#tool";
                }

                if (webpage !== null && webpage !== "") {
                    $('#webpageName').text(data.webpage);
                } else {
                    $('#webpageWrap').hide();
                }

                if (othrInputs !== null && othrInputs !== "") {
                    var othrText = "";
                    if (parseInt(toolId) === 30) {
                        //Top Ranked Websites by Keyword
                        othrText = '<p> <b>Search Phase:</b> ' + jsonOtherInputsData["2"] + '</p><p> <b>Location:</b> ' + jsonOtherInputsData["3"] + '</p><p><b>Language:</b> ' + jsonOtherInputsData["4"] + '</p>';
                    } else if (parseInt(toolId) === 29) {
                        //Social Media Analyzer
                        if (jsonOtherInputsData["domain5"] !== undefined && jsonOtherInputsData["domain6"] !== undefined) {
                            othrText = '<p> <b>Competitor Domain(s):</b> ' + jsonOtherInputsData["domain5"] + ', ' + jsonOtherInputsData["domain6"];
                        } else if (jsonOtherInputsData["domain5"] !== undefined) {
                            othrText = '<p> <b>Competitor Domain(s):</b> ' + jsonOtherInputsData["domain5"];
                        }
                    } else if (parseInt(toolId) === 10) {
                        //Weekly Keyword Rank Checker
                        var k1 = "", k2 = "", k3 = "", k4 = "", k5 = "";
                        if (jsonOtherInputsData["kName9"] !== undefined) {
                            k1 = '<p> <b>Keyword(s):</b> ' + jsonOtherInputsData["kName9"] + '</p>';
                        }
                        if (jsonOtherInputsData["kName10"] !== undefined) {
                            k2 = '<p style =\'margin-left: 7.5%;\'>' + jsonOtherInputsData["kName10"] + '</p>';
                        }
                        if (jsonOtherInputsData["kName11"] !== undefined) {
                            k3 = '<p style =\'margin-left: 7.5%;\'>' + jsonOtherInputsData["kName11"] + '</p>';
                        }
                        if (jsonOtherInputsData["kName12"] !== undefined) {
                            k4 = '<p style =\'margin-left: 7.5%;\'>' + jsonOtherInputsData["kName12"] + '</p>';
                        }
                        if (jsonOtherInputsData["kName13"] !== undefined) {
                            k5 = '<p style =\'margin-left: 7.5%;\'>' + jsonOtherInputsData["kName13"] + '</p>';
                        }

                        var c1 = "", c2 = "", c3 = "";
                        if (jsonOtherInputsData["cName6"] !== undefined) {
                            c1 = '<p> <b>Competitor Domain(s):</b> ' + jsonOtherInputsData["cName6"] + '</p>';
                        }
                        if (jsonOtherInputsData["cName7"] !== undefined) {
                            c2 = '<p style =\'margin-left: 14%;\'>' + jsonOtherInputsData["cName7"] + '</p>';
                        }
                        if (jsonOtherInputsData["cName8"] !== undefined) {
                            c3 = '<p style =\'margin-left: 14%;\'>' + jsonOtherInputsData["cName8"] + '</p>';
                        }

                        othrText = k1 + k2 + k3 + k4 + k5 + c1 + c2 + c3;
                    } else if (parseInt(toolId) === 7) {
                        //Competitor Analysis
                        if (jsonOtherInputsData["2"] !== undefined && jsonOtherInputsData["3"] !== undefined) {
                            othrText = '<p> <b>Competitors URL:</b> ' + jsonOtherInputsData["2"] + '</p><p> <b>Search Phrase:</b> ' + jsonOtherInputsData["3"] + '</p>';
                        } else if (jsonOtherInputsData["2"] !== undefined && jsonOtherInputsData["3"] === undefined) {
                            othrText = '<p> <b>Competitors URL:</b> ' + jsonOtherInputsData["2"] + '</p>';
                        } else if (jsonOtherInputsData["2"] === undefined && jsonOtherInputsData["3"] !== undefined) {
                            othrText = '<p> <b>Search Phrase:</b> ' + jsonOtherInputsData["3"] + '</p>';
                        }
                    } else if (parseInt(toolId) === 2) {
                        //Keyword Combinator
                        var c1 = "", c2 = "", c3 = "", m1 = "", m2 = "", m3 = "", prefix = "", suffix = "", posting = "", radioVal = "", maxBid = "", url = "";
                        c1 = '<p><b>Column 1 :</b> ' + jsonOtherInputsData["2"] + "</p>";
                        if (jsonOtherInputsData["3"]) {
                            c2 = '<p><b>Column 2 :</b> ' + jsonOtherInputsData["3"] + "</p>";
                        }
                        if (jsonOtherInputsData["4"]) {
                            c3 = '<p><b>Column 3 :</b> ' + jsonOtherInputsData["4"] + "</p>";
                        }

                        m1 = '<p><b>Match Type:</b> ' + jsonOtherInputsData["10"];
                        if (jsonOtherInputsData["11"] !== undefined) {
                            m2 = ', ' + jsonOtherInputsData["11"];
                        }
                        if (jsonOtherInputsData["12"] !== undefined) {
                            m3 = ', ' + jsonOtherInputsData["12"];
                        }

                        if (jsonOtherInputsData["12"] !== undefined) {
                            m3 = ', ' + jsonOtherInputsData["12"];
                        }
                        if (jsonOtherInputsData["12"] !== undefined) {
                            m3 = ', ' + jsonOtherInputsData["12"];
                        }

                        if (jsonOtherInputsData["5"] !== "") {
                            prefix = '<p><b>Prefix:</b> ' + jsonOtherInputsData["5"] + "</p>";
                        }
                        if (jsonOtherInputsData["6"] !== "") {
                            suffix = '<p><b>Suffix:</b> ' + jsonOtherInputsData["6"] + "</p>";
                        }
                        if (jsonOtherInputsData["7"] !== "") {
                            posting = '<p><b>AdWords/Bing Ads Power Posting</b> </p><p style =\'margin-left: 6%;\'>' + jsonOtherInputsData["7"] + '</p>';
                        }

                        if (jsonOtherInputsData["8"] !== "") {
                            maxBid = '<p><b>Max bid: </b>' + jsonOtherInputsData["8"] + '</p>';
                        }

                        if (jsonOtherInputsData["9"] !== "") {
                            maxBid = '<p><b>Destination URL: </b>' + jsonOtherInputsData["9"] + '</p>';
                        }
                        othrText = c1 + c2 + c3 + m1 + m2 + m3 + "</p>" + prefix + suffix + posting + radioVal + maxBid + url;
                    }

                    $('#otherInputsWrap').html(othrText);
                }

                if (parseInt(assigneeId) === 0) {
                    assigneeId = -1;
                }

                if (queStatus === 'published') {
                    $('#assigneeId').val(reviewerId);
                    $('#send,#send1').css('background-color', '#95BCF2');
                } else if (queStatus === 'inreview') {
                    $('#assigneeId').val(reviewerId);
                    $('#review,#review1').css('background-color', '#95BCF2');
                    if (role === 'expert') {
                        $("#send,#send1").attr("disabled", "disabled").css('color', 'gray').off('click');
                        document.getElementById('send').style.pointerEvents = 'none';
                        document.getElementById('send1').style.pointerEvents = 'none';
                    }
                } else if (queStatus === 'assigned') {
                    $('#assigneeId').val(assigneeId);
                    $('#assign,#assign1').css('background-color', '#95BCF2');
                    $("#send,#send1").attr("disabled", "disabled").css('color', 'gray').off('click');
                    document.getElementById('send').style.pointerEvents = 'none';
                    document.getElementById('send1').style.pointerEvents = 'none';
                } else if (queStatus === 'unassigned') {
                    $('#assigneeId').val(assigneeId);
                    $("#review,#review1,#send,#send1").attr("disabled", "disabled").css('color', 'gray').off('click');
                    document.getElementById('review').style.pointerEvents = 'none';
                    document.getElementById('review1').style.pointerEvents = 'none';
                    document.getElementById('send').style.pointerEvents = 'none';
                    document.getElementById('send1').style.pointerEvents = 'none';
                }

            }

            function clickSave() {

                var assigneeId = $('#assigneeId').val();
                var previousId = $('#previousId').val();
                if (parseInt(assigneeId) === parseInt(previousId) || answerTemplateModified) {
                    var quesId = $('#quesId').val();
                    var stepsTemplate = $('#stepsOPS').html();
                    var answerTemplate = $("#answerTemplate").Editor("getText");
                    var oMyForm = new FormData();
                    oMyForm.append("quesId", quesId);
                    oMyForm.append("steps", stepsTemplate);
                    oMyForm.append("answer", answerTemplate);
                    if (attachedFiles.files[0] !== undefined && attachedFiles.files[0] !== '' && attachedFiles.files[0] !== null) {
                        if (valiDateFileSize()) {
                            oMyForm.append("attachedFile", attachedFiles.files[0]);
                        } else {
                            $("#attachedFiles").show();
                            $("#attachedFiles").val('');
                            return  false;
                        }
                    } else {
                        oMyForm.append("attachedFile", new File([0], "emptyFile"));
                    }

                    $('.processing').show();
                    $.ajax({
                        type: "POST",
                        url: '/ate-edit-question.html?click=save',
                        data: oMyForm,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function (data) {
                            $('.processing').hide();
                            answerTemplateModified = false;
                            alert(data[0]);
                            if (data[1] !== " ") {
                                $("#fileDownloadWrapper").show();
                                $("#attachedFileName").html(data[1]);
                                $("#attachedFiles").hide();
                            }
                        }
                    });
                } else {
                    alert('To change Assignee, please use the Assign button.');
                }
            }
            function clickAssign() {
                if (!answerTemplateModified) {
                    var assigneeId = $('#assigneeId').val();
                    if (parseInt(assigneeId) !== -1) {
                        var previousId = $('#previousId').val();
                        var quesId = $('#quesId').val();
                        var params = {quesId: quesId, assignee: assigneeId, previousId: previousId};
                        $('.processing').show();
                        $.post("/ate-edit-question.html?click=assignee", params,
                                function (data) {
                                    $('.processing').hide();
                                    if (data[1] !== null && parseInt(data[1]) === 1) {
                                        $('#previousId').val(data[2]);
                                        $("#review,#review1").removeAttr("disabled").css('color', 'black').click(function () { });
                                        document.getElementById('review').style.pointerEvents = 'auto';
                                        document.getElementById('review1').style.pointerEvents = 'auto';
                                        $('#assign,#assign1').css('background-color', '#95BCF2');
                                        $('#review,#review1').css('background-color', '');
                                        $('#send,#send1').css('background-color', '');
                                    }
                                    alert(data[0]);
                                });
                    } else {
                        alert("Please select an expert.");
                    }
                } else {
                    alert("You have made changes either to the Answer or to the File uploading. Please make sure to Save your changes before to change the Assignee.");
                }
            }

            function clickReview() {
                if (!answerTemplateModified) {
                    var reviewerId = $('#assigneeId').val();
                    var previousId = $('#reviewerId').val();
                    var quesId = $('#quesId').val();

                    var params = {quesId: quesId, reviewer: reviewerId, previousId: previousId};
                    $('.processing').show();
                    $.post("/ate-edit-question.html?click=review", params,
                            function (data) {
                                $('.processing').hide();
                                if (data[1] !== null && parseInt(data[1]) === 1) {
                                    $('#previousId').val(data[2]);
                                    if (role !== 'expert') {
                                        $("#send,#send1").removeAttr("disabled").css('color', 'black').click(function () { });
                                        document.getElementById('send').style.pointerEvents = 'auto';
                                        document.getElementById('send1').style.pointerEvents = 'auto';
                                    }
                                    $('#review,#review1').css('background-color', '#95BCF2');
                                    $('#assign,#assign1').css('background-color', '');
                                }
                                alert(data[0]);
                            });
                } else {
                    alert("You have made changes either to the Answer or to the File uploading. Please make sure to Save your changes before to change the Reviewer.");
                }

            }
            function clickSendAnswer() {
                if (role !== 'expert') {
                    var answerTemplate = $("#answerTemplate").Editor("getText");
                    var validateAnswer = answerTemplate.replace(/&nbsp;/gi, " ").replace(/<br>/gi, " ");
                    var htmlTextFilter = $("<div />").html(validateAnswer).text();
                    if (answerTemplate !== null && answerTemplate.trim().length > 0 && answerTemplate !== '<br>'
                            && validateAnswer.trim().length > 0 && htmlTextFilter.trim().length > 0) {

                        var assigneeId = $('#assigneeId').val();
                        var previousId = $('#previousId').val();
                        if (answerTemplateModified || parseInt(assigneeId) !== parseInt(previousId)) {
                            alert("You have made changes either to the Assignee, Answer or File uploading. Please make sure to Save your changes and verify the answer by sending a test mail to yourself.");
                        } else {
                            document.getElementById("popupQ").style.display = "block";
                            document.getElementById("editConfirmationPopUp").style.display = "block";

                        }

                    } else {
                        alert("Answer is empty. Please enter an answer and try again.");
                    }
                }else{
                    alert("You do not have permission to send an answer. Please send your answer to Reviewer.");
                }
            }

            function sendAnswerToUser() {
                document.getElementById("popupQ").style.display = "none";
                document.getElementById("editConfirmationPopUp").style.display = "none";
                $('.processing').show();
                $.post("/ate-edit-question.html?click=send-user",
                        function (data) {
                            $('.processing').hide();
                            if (data === 'success') {
                                $('#send,#send1').css('background-color', '#95BCF2');
                                $('#review,#review1,#assign,#assign1').css('background-color', '');
                                alert("Mail send to user successfully.");
                            } else {
                                alert("Mail not sent to user, please try again later.");
                            }


                        });
            }

            function sendTestMail() {
                if (!answerTemplateModified) {
                    var answerTemplate = $("#answerTemplate").Editor("getText");
                    var validateAnswer = answerTemplate.replace(/&nbsp;/gi, "").replace(/<br>/gi, " ");
                    var htmlTextFilter = $("<div />").html(validateAnswer).text();
                    if (answerTemplate !== null && answerTemplate.trim().length > 0 && answerTemplate !== '<br>'
                            && validateAnswer.trim().length > 0 && htmlTextFilter.trim().length > 0) {

                        $("#testMailWrapper").attr("disabled", "disabled").off('click');
                        $('.processing').show();
                        $.post("/ate-edit-question.html?click=send-mail-admin").done(function (userData) {
                            $('.processing').hide();
                            if (userData === "success") {
                                alert("Mail sent successfully to your registered mail Id.");
                            } else {
                                alert("Failed to send mail.");
                            }
                            $("#testMailWrapper").attr("disabled", false).on('click');
                        });

                    } else {
                        alert("Answer is empty. Please enter an answer and try again.");
                    }
                } else {
                    alert("You have made changes either to the Answer or to the File uploading. Please make sure to Save your changes before sending an email.");
                }
            }
            function goHome() {
                var assigneeId = $('#assigneeId').val();
                var previousId = $('#previousId').val();
                if (answerTemplateModified || parseInt(assigneeId) !== parseInt(previousId)) {
                    document.getElementById("popupQ").style.display = "block";
                    document.getElementById("backPopUp").style.display = "block";
                } else {
                    clickGoBack();
                }
            }
            function navgToExpertAnswers() {
                var assigneeId = $('#assigneeId').val();
                var previousId = $('#previousId').val();
                if (answerTemplateModified || parseInt(assigneeId) !== parseInt(previousId)) {
                    document.getElementById("popupQ").style.display = "block";
                    document.getElementById("backPopUp").style.display = "block";
                } else {
                    clickGoBack();
                }
            }
            function clickCancelBackPopUp() {
                document.getElementById("popupQ").style.display = "none";
                document.getElementById("backPopUp").style.display = "none";

            }
            function clickGoBack() {
                parent.window.location = "/ate-question.html";
            }
            function naviToOtherTabs(menuOption) {
                var assigneeId = $('#assigneeId').val();
                var previousId = $('#previousId').val();
                if (answerTemplateModified || parseInt(assigneeId) !== parseInt(previousId)) {
                    document.getElementById("popupQ").style.display = "block";
                    document.getElementById("backPopUp").style.display = "block";
                } else {
                    var menuParams = {menuOption: menuOption};
                    $.post("/ate-edit-question.html?action=menu", menuParams).done(function (data) {
                        if (data !== null) {
                            parent.window.location = data;
                        }
                    });
                }

            }

            function getUrlVars() {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                    vars[key] = value;
                });
                return vars;
            }

            function valiDateFileSize() {
                var fileValidation = true;
                var uploadFile = document.getElementById("attachedFiles").files[0];
                if (uploadFile.size >= fileUploadLimit) {
                    alert("Uploaded file is too large");
                    fileValidation = false;
                }
                return fileValidation;
            }

        </script>
    </head>
    <body>
        <div class="responsive-wrapper">
            <div class="middle-wrapper">
                <div id="content">
                    <div id="editQuestionWrapper">
                        <div id="tabs-icons">
                            <ul>
                                <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                      class="ui-state-default ui-corner-top " 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                     onclick="naviToOtherTabs('autoMail')" 
                                                     class="ui-state-default ui-corner-top"
                                                     onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                    onclick="naviToOtherTabs('settings')" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                          onclick="naviToOtherTabs('knowledgeBase')" 
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                          onclick="naviToOtherTabs('expertAnswer')" 
                                                          class="ui-state-default ui-corner-top ui-state-active"
                                                          onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                       onclick="naviToOtherTabs('expertUser')"
                                                       class="ui-state-default ui-corner-top ui-tabs-selected"
                                                       onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top 
                                                       ui-tabs-selected" 
                                                       onclick="naviToOtherTabs('ateReports')" 
                                                       onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                            </ul>
                            <div class="editQuestionBlock">
                                <form:form commandName="expertQuestionEditDTO" method="POST" enctype="multipart/form-data">

                                    <form:hidden path="previousId" />
                                    <form:hidden path="reviewerId" />
                                    <form:hidden path="expertQuestionsDTO.questionId" id="quesId"/>
                                    <form:hidden path="expertQuestionsDTO.status" id="quesStatus"/>
                                    <form:hidden path="steps" />
                                    <div class="editOperationWrapper">

                                        <div class="selectOperation">
                                            <p><span>Assignee: </span>
                                                <span> 
                                                    <form:select path="assigneeId">
                                                        <form:option value="-1">Select</form:option>
                                                        <form:options items="${experts}"  />
                                                    </form:select>
                                                </span>

                                            </p>
                                        </div>

                                        <div class="saveOperationWrapper">
                                            <div class="navigationImagesWrapper">
                                                <span id="home" onclick="goHome()">
                                                    <img id="navigationToQuestions" src="/images/admin/home-Icon.png" alt="Back To Question"/>
                                                </span>
                                                <span id="testMailWrapper" onclick="sendTestMail()">
                                                    <img id="viewInMail" src="/images/admin/view-In-Mail.png" alt="View In Mail"/>
                                                </span>
                                            </div>
                                            <div class="saveQuestionWrapper">
                                                <div class="processing">Processing.....</div>
                                                <div class="operationDivWrapper"><ul class="operationList">
                                                        <li id="save" class="operationDiv " onclick="clickSave();">Save</li>
                                                        <li id="assign" class="operationDiv " onclick="clickAssign();">Assign</li>
                                                        <li id="review" class="operationDiv " onclick="clickReview();">Review</li>
                                                        <li id="send" class="operationDiv " onclick="clickSendAnswer();">Send Answer</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="questionInputAnswerWrapper">
                                        <div class="userInputWrapper">
                                            <fieldset>
                                                <legend>User Inputs:</legend>
                                                <div class="userFieldsetInner">
                                                    <p><span class="Bold">Mail ID:</span> <span id="mailId"></span></p>
                                                    <p><span class="Bold">Tool:</span> <span id="toolName"></span></p>
                                                    <p><span class="Bold">Tool Inputs:</span></p>
                                                    <p class="webPageWrapper" id="webpageWrap"><span class="Bold">Webpage:</span> <span id="webpageName"></span></p>
                                                    <div class="webPageWrapper" id="otherInputsWrap"></div>
                                                    <p><span class="Bold" id="compWebpageHeader"></span> <span id="compWebpageName"></span></p>
                                                    <p><span class="Bold">Tool Output:</span> <a href="" target="_blank" id="clickHere">Click Here</a></p>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="expertAnswerWrapper">
                                            <fieldset>
                                                <legend>Answer:</legend>
                                                <div>
                                                    <p><span class="Bold">Question:</span> <span id="question"></span></p>
                                                    <p><span class="Bold">Steps for OPS:</span></p>
                                                    <p><span id="stepsOPS" ></span></p>
                                                </div>
                                                <div >
                                                    <form:textarea path="answerTemplate" />
                                                </div>
                                                <div style="margin-top: 1.5%">
                                                    <input type="file"  id="attachedFiles" name="attachedFile" />
                                                    <div id="fileDownloadWrapper">
                                                        <span id="attachedFileName"></span>
                                                        <span id="clearFile"><i class="fa fa-times fa-2 deleteDirectory" aria-hidden="true" style="color:red;"></i></span>
                                                        <span id="downloadFile"><i class="fa fa-download" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="bottomSaveWarpper">
                                            <div class="processing" style="text-align: right;">Processing.....</div>
                                            <div class="operationList">
                                                <div  id="save1" class="operationDiv" onclick="clickSave();">Save</div>
                                                <div  id="assign1" class="operationDiv" onclick="clickAssign();">Assign</div>
                                                <div  id="review1" class="operationDiv" onclick="clickReview();">Review</div>
                                                <div  id="send1" class="operationDiv" onclick="clickSendAnswer();">Send Answer</div>
                                            </div>
                                        </div>
                                    </div>

                                </form:form>
                            </div>
                        </div>
                    </div>

                </div><!-- content-->
            </div><!-- middle-wrapper-->

        </div><!-- responsive-wrapper-->
        <div id="popupQ" style="display: none">
            <div id="editConfirmationPopUp" style="display: none">
                <div id="popUpHeader">
                    <p>
                        <span class="popUpLabel">Send Answer</span>
                        <span class="closeImageWrapper" id="cancelSubmission">
                            <img src="../images/admin/pop-close.png" alt="Cancel Submission"/>
                        </span>
                    </p>
                </div>
                <div id="popUpBodyContent">
                    <div class="pop-content-box"> 
                        <div class="popUpActionsWrapper">
                            <p>Are you sure want to  send the answer to end user You may not able to send further changes to the answer.</p>
                            <div class="pop-radio">
                                <label><input type="radio" name="sendAns" value="no">No, Go back and review.</label> 
                                <label><input type="radio" name="sendAns" value="yes">Yes, I have verified the answer and the test mail format.</label>
                            </div>
                        </div>
                    </div>
                </div><br/><br/>
                <div class="pop-btns">
                    <div class="p-btns">
                        <div id="cancelPopUp" >CANCEL</div>
                        <div id="submitAnswer" >SUBMIT</div>
                    </div>
                </div>
                <div id="errorMsg"></div>
            </div>

            <div id="backPopUp" style="display: none">
                <div id="popUpBodyContent">
                    <p>By going back, changes made to the answer/assignee will be lost. Please make sure to save before going back.</p>
                </div>
                <div class="pop-btns">
                    <div class="p-btns">
                        <div id="goBack" onclick="clickGoBack()">Yes</div>
                        <div id="cancelBackPopUp" onclick="clickCancelBackPopUp()">No</div>
                    </div>
                </div>
            </div>

        </div>
        <%@ include file="/WEB-INF/view/footer.jsp"%>
    </body>
</html>