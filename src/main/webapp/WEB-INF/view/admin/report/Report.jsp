<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,java.util.Calendar,java.util.Map,java.util.TreeSet,java.util.Set,lxr.marketplace.user.LoginService"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%@page import="org.springframework.web.context.WebApplicationContext"%>
    <%@page
        import="org.springframework.web.context.support.WebApplicationContextUtils"%>

        <%    HttpSession sessionR = request.getSession();
            Login user = (Login) sessionR.getAttribute("user");
            Object ul = sessionR.getAttribute("userLoggedIn");
            boolean userLoggedIn = false;
            ExpertUser expertUser = null;
            if (ul != null) {
                userLoggedIn = (Boolean) ul;
            }
            if (userLoggedIn) {
                if (sessionR.getAttribute("expertUser") != null) {
                    expertUser = (ExpertUser) sessionR.getAttribute("expertUser");
                    pageContext.setAttribute("expertUser", expertUser);
                }
            }
        %>
        <html>
            <head>
                <title>LXRMarketplace | Reports</title>
                <meta name="keywords" content="Admin Reports" />
                <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
                <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
                <%@ include file="/WEB-INF/view/commonImport.jsp"%>
                <%@ include file="/WEB-INF/view/header.jsp"%>

                <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.datepicker.js"></script>
                <script type="text/javascript" src="/scripts/Charts.js"></script>
                <link rel="stylesheet" href="css/lxrm-core.css">
                <link rel="stylesheet" href="css/admin.css">
                <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">

                <script type="text/javascript">

                    <%	out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                        Calendar cal1st = Calendar.getInstance();
                        cal1st.set(Calendar.DAY_OF_MONTH, 1);
                        out.println("var serverFirstDay=" + cal1st.getTimeInMillis() + ";");
                        out.println("var userLoggedIn=" + userLoggedIn + ";");
                    %>
                    $(document).ready(function () {
                    <c:if test="${not empty expertUser}" >
                        var name = "${expertUser.name}"
                        var role = "${expertUser.role}"
                        if (name.length > 15) {
                            name = name.substring(0, 15);
                        }
                        $('#uName').text(name + " (" + role + ")");
                    </c:if>

                        if (role === 'expert') {
                            $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        } else if (role === 'reviewer') {
                            $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        }

                        //getTemplatesData();
                        $('#templates').hide();
                        var logo = document.getElementById("logo");
                        logo.href = "/dashboard.html";
                        $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    <%if (user != null) {
                            out.println("$(\"#loginSuccess\").show();");
                        } else {
                            out.println("$(\"#loginSuccess\").hide();");
                        }%>

                        if ($('#reportType').val() === 1) {
                            $('#videocheckbox').show();
                        }
                        if ($('#reportType').val() === 3) {
                            $('#tilldate').show();
                            $("#dateRangeOption").append('<option id="tilldate" value="6" label="Till Date">Till Date</option>');
                        }
                        var fullDate = new Date(serverTime);
                        var fd;
                        var td;
                        var option;
                        if ($('#dateRangeOption').val() === -1) {
                            $('#cDateRange').show();
                        }
                        $('#reportType').change(function () {
                            if ($('#reportType').val() === 1) {
                                $('#videocheckbox').show();
                            }
                            if ($('#reportType').val() !== 1) {
                                $('#videocheckbox').hide();
                            }
                            if ($('#reportType').val() !== 4) {
                                $('#dateSelection').show();
                                $('#templates').hide();
                            }
                            if ($('#reportType').val() === 3) {
                                $('#videocheckbox').hide();
                                $('#tilldate').show();
                                $("#dateRangeOption").append('<option id="tilldate" value="6" label="Till Date">Till Date</option>');
                            } else if ($('#reportType').val() === 4) {
                                $('#videocheckbox').hide();
                                getTemplatesData();

                            }
                            if ($('#reportType').val() === 5) {
                                $('#videocheckbox').hide();
                                $('#tilldate').show();
                                $("#dateRangeOption")
                                        .append(
                                                '<option id="tilldate" value="6" label="Till Date">Till Date</option>');
                            } else {
                                $('#dateSelection').show();
                                if ($('#dateRangeOption').val() === 6) {
                                    $('#dateRangeOption').val(
                                            "0");
                                    $('#tilldate').hide();
                                    $("#dateRangeOption option[value='6']")
                                            .remove();
                                } else {
                                    $('#tilldate').hide();
                                    $("#dateRangeOption option[value='6']")
                                            .remove();
                                }
                            }
                        });
                        $('#dateRangeOption').change(function () {
                            option = $('#dateRangeOption').val();
                            var dayInMilSec = 24 * 60 * 60 * 1000;
                            if (option === "-1") {
                                $('#cDateRange').show();
                                if ($('#fromDate').datepicker("getDate") === null) {
                                    $('#fromDate').datepicker("setDate", new Date(fullDate));
                                }
                                if ($('#toDate').datepicker("getDate") === null) {
                                    $('#toDate').datepicker("setDate", new Date(fullDate));
                                }
                            } else {
                                $('#cDateRange').hide();
                                if (option === "1") {
                                    fd = new Date(fullDate);
                                    td = new Date(fullDate);

                                } else if (option !== "0") {
                                    if (option === "2") {
                                        fd = new Date(fullDate - new Date(1 * dayInMilSec));
                                    } else if (option === "3") {
                                        fd = new Date(fullDate - new Date(7 * dayInMilSec));
                                    } else if (option === "4") {
                                        fd = new Date(fullDate - new Date(30 * dayInMilSec));
                                    } else if (option === "5") {
                                        fd = new Date(fullDate - new Date(90 * dayInMilSec));
                                    } else if (option === "6" && ($('#reportType').val() === 3)) {
                                        fd = new Date(2011, 10 - 1, 01);
                                    } else if (option === "7") {
                                        fd = new Date(serverFirstDay);
                                    }
                                    td = new Date(fullDate - new Date(1 * dayInMilSec));
                                }
                                $('#fromDate').datepicker("setDate", fd);
                                $('#toDate').datepicker("setDate", td);
                            }
                        });
                        $("#download").bind('click', function () {
                            option = $("#dateRangeOption").val();
                            fd = $("#fromDate").datepicker("getDate");
                            td = $("#toDate").datepicker("getDate");
                            var rt = $("#reportType").val();
                            if (rt === "0") {
                                alert("Please select the Report Type");
                                $("#reportType").focus();
                                return false;
                            } else if (option === "0") {
                                alert("Please select the Date Range");
                                return false;
                            } else if (option === 0 && rt !== 4) {
                                alert("Please select the Date Range");
                                $("#dateRangeOption").focus();
                                return false;
                            } else if (option === -1 && fd === null) {
                                alert("Please select the Start Date");
                                $("#fromDate").focus();
                                return false;
                            } else if (option === -1 && td === null) {
                                alert("Please select the End Date");
                                $("#toDate").focus();
                                return false;
                            } else if (option === -1 && (fd > td)) {
                                alert("Start Date is less than End Date, \n please select a valid date range");
                                $("#fromDate").focus();
                                return false;
                            }
                            var f = $("#report");
                            f.attr('method', 'POST');
                            if ($('#reportType').val() === 4) {
                                var eventId = $('#mailtemplates').val();
                                f.attr('action', "report.html?download=download&eventDetaiId=" + eventId);
                            } else {
                                f.attr('action', "report.html?download=download");
                            }
                            f.submit();
                        });
                    });
                    $(function () {
                        $("#fromDate, #toDate").datepicker({
                            showOn: "both",
                            buttonImage: "../css/0/images/calendar.jpg",
                            buttonImageOnly: true,
                            dateFormat: 'dd-M-yy',
                            maxDate: new Date(serverTime),
                            changeMonth: true,
                            changeYear: true,
                            constrainInput: true
                        });
                    });

                    function navgToDashBoard() {
                        var f = $("#report");
                        f.attr('action', "report.html?dashboard='dashboard'");
                        f.submit();

                    }
                    function navgToAutoMail() {
                        var f = $("#report");
                        f.attr('action', "report.html?automail='automail'");
                        f.submit();
                    }

                    function navgToSettings() {
                        var f = $("#report");
                        f.attr('action', "report.html?settings='settings'");
                        f.submit();
                    }

                    function navgToKnowledgeBase() {
                        var f = $("#report");
                        f.attr('action', "report.html?knowledgeBase='knowledgeBase'");
                        f.submit();
                    }

                    function navgToExpertAnswers() {
                        var f = $("#report");
                        f.attr('action', "report.html?expertAnswer='expertAnswer'");
                        f.submit();
                    }
                    function navgToExpertUser() {
                        var f = $("#report");
                        f.attr('action', "report.html?expertUsers='expertUsers'");
                        f.submit();
                    }
                    function navgToATEReports() {
                        var f = $("#report");
                        f.attr('action', "report.html?ateReports='ateReports'");
                        f.submit();
                    }
                    function navgToPlugin() {
                        var f = $("#report");
                        f.attr('action', "report.html?pluginAdoption='pluginAdoption'");
                        f.submit();
                    }

                    function naviToOtherTabs(menuOption) {
                        var menuParams = {menuOption: menuOption};
                        $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                            if (data !== null) {
                                parent.window.location = data;
                            }
                        });
                    }
                    <%--
                    function getTemplatesData() {
                    <% Map<Long, String> templates = (Map<Long, String>) sessionR.getAttribute("templates");
                        Map<Long, String> templates1 = (Map<Long, String>) sessionR.getAttribute("templates1");
                        Set<Long> tempKeys_unsorted = templates.keySet();
                        Set<Long> tempKeys = new TreeSet<>(tempKeys_unsorted);
                        Set<Long> tempKeys1_unsorted = templates1.keySet();
                        Set<Long> tempKeys1 = new TreeSet<>(tempKeys1_unsorted);%>
                    <%if (templates.size() == 0 && templates1.size() == 0) {%>
                        alert("You donot have any templates for this report");
                        $("#reportType").val(0);
                        return flase;
                    <%} else {%>
                        var create = "";
                    <%for (Long key : tempKeys) {%>
                        var dummy = cre;
                        ate;
                        create = "<option value=\"<%=key%>\"> <%=templates.get(key)%></option>";
                        create += dummy;
                    <%}%>
                    <%for (Long key1 : tempKeys1) {%>
                        var dummy1 = cr;
                        eate;
                        create = "<option value=\"<%=key1%>\"> <%=templates1.get(key1)%></option>";
                        create += dummy1;
                    <%}%>
                        //create += "</select>";
                        $('#mailtemplates').append(create);
                        $('#templates').show();
                        $('#dateSelection').hide();
                    <%}%>
                    }
                    --%>
                    $(function () {
                        $("#tabs-icons").tabs();
                    });
                </script>
                <style type="text/css">
                    #resultTable {
                        display: block;
                        border: 1px solid silver;
                        height: 408px;
                        padding: 50px;
                        line-height: 34px;
                        background-color: #FFFFFF;
                        padding-left: 280px;
                        margin-bottom: 4%;
                    }

                    #resultTable h2 {
                        display: inline;
                    }

                    .rtxtdiv {
                        width: 300px;
                        display: inline;
                        font-weight: bold;
                    }

                    .totdivwidth {
                        width: 500px;
                    }

                    .txtfield {
                        display: inline;
                        width: 290px;
                        float: right;
                    }
                </style>
            </head>
            <body>
                <div class="responsive-wrapper">

                    <div class="middle-wrapper">
                        <div class="" id="content">
                            <form:form commandName="report" method="POST">
                                <div id="tabs-icons" style='padding:0;'>
                                    <ul>
                                        <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                              class="ui-state-default ui-corner-top " 
                                                              onmouseover="this.style.cursor = 'pointer'" 
                                                              onclick="navgToDashBoard()">Dashboard</a></li>
                                        <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                            class="ui-state-default ui-corner-top ui-state-active"
                                                            onmouseover="this.style.cursor = 'pointer'"
                                                            onclick="navgToReports();" >Reports</a></li>
                                        <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                             class="ui-state-default ui-corner-top"
                                                             onmouseover="this.style.cursor = 'pointer'"
                                                             onclick="navgToAutoMail()" >Automail</a></li>
                                        <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                            class="ui-state-default ui-corner-top"
                                                            onmouseover="this.style.cursor = 'pointer'"
                                                            onclick="navgToSettings()" >Settings</a></li>
                                        <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  class="ui-state-default ui-corner-top"
                                                                  onmouseover="this.style.cursor = 'pointer'"
                                                                  onclick="navgToKnowledgeBase()" >Knowledge Base</a></li>
                                        <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                                  onmouseover="this.style.cursor = 'pointer'" 
                                                                  onclick="navgToExpertAnswers()">Expert Answer</a></li>
                                        <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top"
                                                               onmouseover="this.style.cursor = 'pointer'"
                                                               onclick="navgToExpertUser()" >Expert User</a></li>
                                        <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top"
                                                               onmouseover="this.style.cursor = 'pointer'"
                                                               onclick="navgToATEReports()" >ATE Reports</a></li>
                                        <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                                   class="ui-state-default ui-corner-top"
                                                                   onmouseover="this.style.cursor = 'pointer'"
                                                                   onclick="navgToPlugin()" >LXRPlugin</a></li>
                                    </ul>
                                </div>
                                <div id="resultTable">
                                    <div class="totdivwidth">
                                        <div class="rtxtdiv">Report Type :</div>
                                        <div class="txtfield">
                                            <select id="reportType"  name="reportType">
                                                <option value="0" label="-- Select --" />
                                                <optgroup label="LXRMarketplace">
                                                    <option value="1" label="Tool Usage Report" />
                                                    <option value="2" label="User Behaviour Report" />
                                                    <option value="3" label="Users Report" />
                                                    <option value="4" label="Email Performance Report" />
                                                    <option value="9" label="LXRMarketplace Sales Report" />
                                                    <option value="14" label="SEO Users Tip Report" />
                                                    <option value="15" label="LXRM Ask the Expert Payment Report" />
                                                    <option value="16" label="LXRM User Analysis Report" />
                                                    <option value="17" label="LXRM Subscriptions Report" />
                                                </optgroup>
                                                <optgroup label="LXRPlugin">
                                                    <option value="11" label="LXRPlugin Users Report" />
                                                    <option value="12" label="LXRPlugin Payment Report" />
                                                    <option value="13" label="LXRPlugin User Behaviour Report" />
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="templates" class="totdivwidth" style="display: block;">
                                        <div class="rtxtdiv">Mail Templates:</div>
                                        <div class="txtfield">
                                            <select id="mailtemplates"
                                                    style="word-wrap: break-word; width: 350px;">
                                            </select>
                                        </div>
                                    </div>
                                    <div id="dateSelection" class="totdivwidth">
                                        <div class="rtxtdiv">Date Range :</div>
                                        <div class="txtfield">
                                            <form:select path="dateRangeOption" cssStyle="width:115px;">
                                                <form:option value="0" label="-- Select --" />
                                                <form:option value="1" label="Today" />
                                                <form:option value="2" label="Yesterday" />
                                                <form:option value="3" label="Last 7 Days" />
                                                <form:option value="4" label="Last 30 Days" />
                                                <form:option value="7" label="This Month(till date)" />
                                                <form:option value="5" label="Last 90 Days" />
                                                <form:option value="-1" label="Custom" />
                                            </form:select>
                                        </div>
                                        <div id="cDateRange" style="display: none;">
                                            <table>
                                                <tr>
                                                    <td id="date">From: <form:input readonly="true"
                                                                path="fromDate" size="12" />
                                                    </td>
                                                    <td id="date">To: <form:input readonly="true" path="toDate"
                                                                size="12" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="totdivwidth">
                                        <div class="rtxtdiv">File Format:</div>
                                        <div class="txtfield">
                                            <form:select path="fileType" cssStyle="width:115px">
                                                <form:option value="2" label="  xls  " />
                                                <form:option value="1" label="  xlsx  " />
                                            </form:select>
                                        </div>
                                    </div>
                                    <div class="totdivwidth">
                                        <div class="rtxtdiv">Include Local Users:</div>
                                        <div class="txtfield" style="padding-top: 2px;">
                                            <form:checkbox path="includeLocal" />
                                        </div>
                                    </div>
                                    <div id="videocheckbox" class="totdivwidth" style="display: none;">
                                        <div class="rtxtdiv"
                                             style="width: 210px; padding-right: 1px; display: inline-block; margin-right: 0px;float: left;">Include
                                            Video Tutorial Usage:</div>
                                        <div style="width: 20px; display: inline-block; margin-left: 0px;float:left;margin-top:0%;" class="report-check">
                                            <form:checkbox path="includeVideo" />
                                        </div>
                                    </div>
                                    <br> <input type="image" id="download"
                                                src="../css/0/images/download.gif" name="image" alt="Download"
                                                title="download" />
                                </div>
                            </form:form>
                        </div>

                    </div>
                </div>
                <%@ include file="/WEB-INF/view/footer.jsp"%>
            </body>
        </html>
