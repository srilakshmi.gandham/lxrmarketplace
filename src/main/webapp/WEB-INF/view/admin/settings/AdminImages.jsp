<!DOCTYPE html>
<%try {%>
<%@ page
    import="lxr.marketplace.user.Login,lxr.marketplace.admin.settings.Settings,java.util.List "%>
    <%@page import="net.sf.json.JSONString"%>
    <%@page import="org.apache.jasper.tagplugins.jstl.core.Param"%>
    <%@page import="javax.mail.Session"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <jsp:useBean id="settings"
                 class="lxr.marketplace.admin.settings.Settings" />
    <%
        HttpSession sessionA = request.getSession();
        Login user = (Login) sessionA.getAttribute("user");
        Object ul = sessionA.getAttribute("userLoggedIn");
        boolean userLoggedIn = false;
        ExpertUser expertUser = null;
        if (ul != null) {
            userLoggedIn = (Boolean) ul;
        }
        if (userLoggedIn) {
            if (sessionA.getAttribute("expertUser") != null) {
                expertUser = (ExpertUser) sessionA.getAttribute("expertUser");
                pageContext.setAttribute("expertUser", expertUser);
            }
        }
    %>
    <html>
        <head>
            <title>LXRMarketplace | Settings</title>
            <meta name="keywords" content="Admin Reports" />
            <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
            <%@ include file="/WEB-INF/view/commonImport.jsp"%>
            <%@ include file="/WEB-INF/view/include.jsp"%>
            <%@ include file="/WEB-INF/view/header.jsp"%>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
            <script type="text/javascript" src="scripts/fileuploader.js"></script>
            <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" href="css/lxrm-core.css">
            <link rel="stylesheet" href="css/admin.css">
            <script type="text/javascript">

                var countfile = 0;
                $(document).ready(function (event) {
                <%
                    out.println("var userLoggedIn=" + userLoggedIn + ";");
                %>
                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    var role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>
                    var atchmnts = document.getElementById("attachments");
                    var logo = document.getElementById("logo");
                    logo.href = "/dashboard.html";
                    $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    $("#tabs-icons").tabs();
                    $('#sett li').click(function () {
                        if (this.id == "toolConfig") {
                            window.location = "/toolconfig.html";
                        } else if (this.id == "filt") {
                            window.location = "/settings.html";
                        } else if (this.id == "urlRedirection") {
                            window.location = "/admin-url-redirection.html";
                        }
                    });
                <%                                            if (user != null) {
                        out.println("$(\"#loginSuccess\").show();");
                    } else {
                        out.println("$(\"#loginSuccess\").hide();");
                    }
                %>
                    var divId = document.getElementById('browse1');
                    createUploader(event, divId);
                    var fileuploaded = $(".qq-upload-list").text();
                    GetHeight();
                });



                function GetHeight() {
                    var firstDiv = document.getElementById("scroller");
                    var firstheight = firstDiv.offsetHeight;
                    var secHgt = 300;
                    if (firstheight < 300) {
                        firstDiv.style.height = firstheight + "px";
                    } else if (firstheight >= 300) {
                        firstDiv.style.height = secHgt + "px";
                    }
                }

                var maxCount = 1;
                function createUploader(event, divId) {
                    var uploader = new qq.FileUploader({
                        element: divId,
                        action: "adminimages.html?uploadFile='uploadFile'",
                        multiple: false,
                        sizeLimit: 100000000,
                        allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
                        onSubmit: function (id, fileName) {
                            countfile++;
                            if (countfile == 1) {
                                $('#file').hide();
                                $('#filedisable').css('display', 'inline-block');
                                $("#clear").attr('disabled', true);
                            }
                            if (countfile > maxCount) {
                                alert("You cannot attach more than 1 file");
                                return false;
                            }
                        },
                        onProgress: function (id, fileName, loaded, total) {
                            $('.uploadfile').attr('disabled', true);
                            $("#clear").attr('disabled', true);

                        },
                        onComplete: function (id, fileName, responseJSON) {
                            $('.uploadfile').attr('disabled', false);
                            $('.qq-upload-list').html("");
                            $('#upFileName').val(fileName);
                            $("#clear").attr('disabled', false);
                            if (responseJSON.length == 1) {
                                countfile--;
                                if (countfile < 1) {
                                    $('#file').css('display', 'inline-block');
                                    $('#filedisable').hide();
                                }
                                alert(responseJSON[0]);
                            } else {
                                addRowForattachedFile(fileName);
                                //setDashboard();
                            }
                        },
                        messages: {
                            typeError: "Please upload png / gif / jpg / jpeg images only.",
                            //sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
                            sizeError: "Exceeding File Size Limit",
                            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                            emptyError: "{file} is empty, please select files again without it.",
                            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."
                        },
                        showMessage: function (message) {
                            alert(message);
                        }
                    });
                }

                function getAllFiles() {
                    var f = $("#adminimages");
                    f.attr('action', "/adminimages.html?getAllFiles='getAllFiles'");
                    f.submit();
                }

                function removeFile(fName) {
                    if (fName != null) {
                        var f = $("#adminimages");
                        f.attr('action', "/adminimages.html?removeFile=" + fName);
                        f.submit();
                    }

                }

                function addRowForattachedFile(fileName) {
                    var atchmnts = document.getElementById("attachments");
                    var changedFile = fileName.replace(new RegExp("'", 'g'), "\\\\\\'");
                    var atchmnts = document.getElementById("attachments");
                    var lastRow = atchmnts.rows.length;
                    var row = atchmnts.insertRow(lastRow);
                    var cellLeft = row.insertCell(0);
                    var textNode = document.createTextNode(fileName);
                    cellLeft.appendChild(textNode);
                    var cellRight = row.insertCell(1);
                    getAllFiles();
                }

                function navgToReports() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?report='report'");
                    f.submit();
                }

                function navgToAutoMail() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?automail='automail'");
                    f.submit();
                }

                function navgToDashBoard() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?dashboard='dashboard'");
                    f.submit();
                }
                function navgToKnowledgeBase() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?knowledgeBase='knowledgeBase'");
                    f.submit();
                }

                function navgToExpertAnswers() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?expertAnswer='expertAnswer'");
                    f.submit();
                }

                function showExpertUsers() {
                    var f = $("#adminimages");
                    f.attr('action', "adminimages.html?expertUsers='expertUsers'");
                    f.submit();
                }


            </script>
            <style type="text/css">
                .sucMsg {
                    color: green;
                    display: none;
                    font-size: 13.4px;
                    padding-bottom: 5px;
                    padding-left: 70px;
                }

                #scroller {
                    overflow-x: hidden;
                    overflow-y: scroll;
                }

                .forscroller {
                    margin: 0 0 15px 35px;
                    width: 635px;
                    float: center;
                }

                #resultsDiv {
                    padding: 5px;
                    line-height: 24px;
                    background-color: #FFFFFF;
                }

                #resultsDiv table {
                    border: 1px solid #E9E9E9;
                    border-collapse: collapse;
                    width: 100%;
                    cellpadding: 4px;
                    cellspacing: 4px;
                }

                #resultsDiv  table td {
                    border: 1px solid #E9E9E9;
                    padding: 8px;
                    text-align: center;
                }

                #resultsDiv  table th {
                    border: 1px solid #E9E9E9;
                    padding: 8px;
                    text-align: center;
                }
            </style>
        </head>
        <body>
            <div class="responsive-wrapper">
                <div class="middle-wrapper">
                    <div class="" id="content">
                        <div id="tabs-icons">
                            <ul>
                                <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top " 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="navgToDashBoard()">Dashboard</a></li>
                                <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="navgToReports();" >Reports</a></li>
                                <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                     class="ui-state-default ui-corner-top"
                                                     onmouseover="this.style.cursor = 'pointer'"
                                                     onclick="navgToAutoMail()" >Automail</a></li>
                                <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                    class="ui-state-default ui-corner-top ui-state-active"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="navgToSettings()" >Settings</a></li>
                                <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'"
                                                          onclick="navgToKnowledgeBase()" >Knowledge Base</a></li>
                                <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                          onmouseover="this.style.cursor = 'pointer'" 
                                                          onclick="navgToExpertAnswers()">Expert Answer</a></li>
                                <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top"
                                                       onmouseover="this.style.cursor = 'pointer'"
                                                       onclick="navgToExpertUser()" >Expert User</a></li>
                            </ul>
                        </div>
                        <div id="scont" style="float: left; padding-top: 10px;">
                            <div id="profil" class="shadow portlet vn">
                                <h2>Settings</h2>
                                <ul id="sett">
                                    <li id="filt">Filter</li>
                                    <li id="toolConfig">Tool Config</li>
                                    <li id="adminImages" class="selected">Admin Images</li>
                                    <li id="urlRedirection">URL Redirection</li>
                                </ul>
                            </div>
                        </div>
                        <div id="bcont" style="float: right; padding-top: 10px; margin-bottom: 20px; height: 442px;">
                            <div id="bc-sub" class="portlet shadow">
                                <form:form commandName="adminimages" method="POST">
                                    <div style="padding-top: 20px;">
                                        <table style="width: 100%">
                                            <tr>
                                                <td width="23%" rowspan="3"><form:hidden path="upFileName" />
                                                    <div id="file" width="100%"
                                                         style="display: inline-block; padding-left: 40px;">
                                                        <div
                                                            style="vertical-align: middle; padding-right: 17px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline;">
                                                            <b style="">Upload&nbsp;an&nbsp;Image:&nbsp; </b>
                                                        </div>
                                                        <div class="smallBrowseButton" id="browse1"
                                                             style="display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;"
                                                             title="Choose a File to Attach"></div>
                                                        <div class="note"
                                                             style="padding: 0px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;">(Please
                                                            upload either png / jpg / jpeg / gif images only)</div>
                                                        <br>
                                                        <br>
                                                    </div>
                                                    <div id="filedisable" width="100%"
                                                         style="display: none; padding-left: 40px;">
                                                        <div
                                                            style="vertical-align: middle; padding-right: 17px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline;">
                                                            <b style="">Attach&nbsp;a&nbsp;file:&nbsp; </b>
                                                        </div>
                                                        <div id="browsedisable"
                                                             style="display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;"
                                                             class="smallBrowseButtonDisabled"
                                                             title="Choose a File to Attach"></div>
                                                        <div class="note"
                                                             style="padding: 0px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;">(Please
                                                            upload png / jpg / jpeg / gif images only)</div>
                                                        <br>
                                                        <br>
                                                    </div>
                                                    <div>
                                                        <table id="attachments">
                                                        </table>
                                                    </div></td>
                                            </tr>
                                        </table>
                                        <br>
                                        <!-- 			<div style="padding-left:50px;"> -->
                                        <!-- 			 <img style="width:65px; height: 39px; padding-top: 3px; padding-bottom: 1px;" src = "../css/0/images/save.png" onclick="saveFile();" onmouseover="this.style.cursor='pointer'"/> -->
                                        <!-- 			 <img  style="width:60px; height: 26px;padding-bottom: 8px;" src = "../css/0/images/cancel.gif" onclick="cancelFile();" onmouseover="this.style.cursor='pointer'"  onclick="cancelFile()"/>  -->
                                        <!-- 			 <input type="image"  id="clear" src="../css/0/images/clear.gif" name="image" alt="Clear" style="width: 60px; height: 26px;padding-bottom: 8px;  cursor: pointer;"/> -->
                                        <!--             </div> -->

                                        <div id="resultsDiv">
                                            <div style="padding-top: 32px;">
                                                <div id="scroller" class="forscroller">
                                                    <table align="center" width="100%">
                                                        <tr style="color: #f60;">
                                                            <th width="60%">Image Path</th>
                                                            <th width="28%">Image</th>
                                                            <th width="12%">Delete</th>
                                                        </tr>

                                                        <%String folderPath = "/admin_images/";
                                                            String domain = (String) sessionA.getAttribute("domain");
                                                            String totalFiles[] = (String[]) sessionA.getAttribute("attachedFiles");
                                                            if (totalFiles != null && totalFiles.length > 0) {
                                                                for (String file : totalFiles) {%>

                                                        <%if (domain != null) {%>
                                                        <tr>
                                                            <td width="70%"
                                                                style="word-wrap: break-word; text-align: left; color: blue;"><a
                                                                    href="<%=domain + folderPath + file%>" target="_blank"><%=domain + folderPath + file%></a></td>
                                                                <%--         	 <a href="<%=cmpImg.getImgSrc()%>" target="_blank"><img src="<%=finalImgPath%>" /></a> --%>
                                                            <td width="30%"><a href="<%=domain + folderPath + file%>"
                                                                               target="_blank"><img src="<%=folderPath + file%>"
                                                                                     title="<%=file%>"
                                                                                     style="cursor: pointer; height: 40px; width: 50px;" /></a></td>
                                                            <td style="cursor: pointer;"><img
                                                                    src="../css/0/images/remove.png" alt="Remove" title="Remove"
                                                                    onclick="removeFile('<%=file%>');"
                                                                    style="height: 12px; width: 12px;" /></td>
                                                        </tr>
                                                        <%}
                                                            }
                                                        } else {%>
                                                        <tr>
                                                            <td colspan="3">No uploaded files.</td>
                                                        </tr>
                                                        <% }%>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>

                    <%@ include file="/WEB-INF/view/footer.jsp"%>
                </div>
            </div>
        </body>
    </html>
    <%} catch (Exception e) {
            e.printStackTrace();
        }%>