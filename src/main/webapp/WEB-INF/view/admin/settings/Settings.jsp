<!DOCTYPE html>
<%try {%>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,lxr.marketplace.admin.settings.Settings,java.util.List "%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <jsp:useBean id="settings"
                 class="lxr.marketplace.admin.settings.Settings" />
    <%    HttpSession sessionS = request.getSession();
        Login user = (Login) sessionS.getAttribute("user");
        List<Settings> totRecords = (List<Settings>) sessionS.getAttribute("totRecords");
        Object ul = sessionS.getAttribute("userLoggedIn");
        boolean userLoggedIn = false;
        ExpertUser expertUser = null;
        if (ul != null) {
            userLoggedIn = (Boolean) ul;
        }
        if (userLoggedIn) {
            if (sessionS.getAttribute("expertUser") != null) {
                expertUser = (ExpertUser) sessionS.getAttribute("expertUser");
                pageContext.setAttribute("expertUser", expertUser);
            }
        }
    %>

    <html>
        <head>
            <title>LXRMarketplace | Settings</title>
            <meta name="keywords" content="Admin Reports" />
            <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
            <%@ include file="/WEB-INF/view/commonImport.jsp"%>
            <%@ include file="/WEB-INF/view/header.jsp"%>

            <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
            <script type="text/javascript" src="/scripts/Charts.js"></script>
            <link rel="stylesheet"
                  href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" href="css/lxrm-core.css">
            <link rel="stylesheet" href="css/admin.css">

            <script type="text/javascript">
                <%
                    out.println("var userLoggedIn=" + userLoggedIn + ";");
                %>
                var isEdit = true;
                var isSaved = true;
                var tempFiltName;
                $(document).ready(function (event) {
                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    var role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>
                    var logo = document.getElementById("logo");
                    logo.href = "/dashboard.html";
                    $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    $("#tabs-icons").tabs();
                    $('#sett li').click(function () {
                        if (this.id === "toolConfig") {
                            window.location = "/toolconfig.html";
                        } else if (this.id === "adminImages") {
                            window.location = "/adminimages.html";
                        } else if (this.id === "urlRedirection") {
                            window.location = "/admin-url-redirection.html";
                        }
                    });
                <%
                    if (user != null) {
                        out.println("$(\"#loginSuccess\").show();");
                    } else {
                        out.println("$(\"#loginSuccess\").hide();");
                    }
                %>
                });
                function GetHeight() {
                    var firstDiv = document.getElementById("scroller");
                    var firstheight = firstDiv.offsetHeight;
                    var secHgt = 250;
                    if (firstheight < 250) {
                        firstDiv.style.height = firstheight + "px";
                    } else if (firstheight >= 250) {
                        firstDiv.style.height = secHgt + "px";
                    }
                }
                function showdiv() {
                    var state = document.getElementById("showFilter");
                    if (state.style.display === 'none') {
                        document.getElementById("showFilter").style.display = 'block';
                    }
                }
                function filterDetails(id, fName, filterOption, filterType) {
                    showdiv();
                    isEdit = true;
                    $("#msg").hide();
                    $('#afterReload').hide();
                    tempFiltName = $(fName).text();
                    $("#filterId").val(id);
                    $("#selectStyle").val(filterType);
                    $("#filterOption").val(filterOption);
                    if (filterOption === 1) {
                        $('#totdivwidth1').hide();
                        $('#totdivwidth').show();
                        $('#ipAddress').hide();
                        $('#ipAddress').val(trim($(fName).text()));
                        $('#ip').show();
                        $('#ip').val(trim($(fName).text()));
                        $('#dom').hide();
                        $('#dom').val("");
                    }
                    if (filterOption === 2) {
                        $('#totdivwidth').hide();
                        $('#totdivwidth1').show();
                        $('#domainName').hide();
                        $('#dom').show();
                        $('#domainName').val(trim($(fName).text()));
                        $('#dom').val(trim($(fName).text()));
                        $('#ip').hide();
                        $('#ip').val("");
                    }
                }
                function reload() {
                    $.ajax({
                        type: "POST",
                        url: "/settings.html?reload='reload'",
                        success: function (data) {
                            $('#afterReload').show();
                        }
                    });
                }
                function showNew() {
                    showdiv();
                    isEdit = false;
                    $("#msg").show();
                    $('#totdivwidth1').show();
                    $('#totdivwidth').show();
                    $('#ipAddress').show();
                    $('#domainName').show();
                    $('#ipAddress').val("");
                    $('#domainName').val("");
                    $("#selectStyle").val(1);
                    $('#ip').hide();
                    $('#ip').val("");
                    $('#dom').hide();
                    $('#dom').val("");
                    $('#afterReload').hide();
                }
                function saveFilter() {
                    $('#afterReload').hide();
                    if ($('#ipAddress').val() !== "" && $('#ipAddress').val() !== null) {
                        var tempAddress = $('#ipAddress').val();
                        if (!validateComa(tempAddress)) {
                            alert("Input value contains special charcter please enter right one");
                            return false;
                        }
                        var adrs = tempAddress.split("\n");
                        for (var x = 0; x < adrs.length; x++) {
                            if (!isValidIPAddress(adrs[x])) {
                                $("#ipAddress").focus();
                                alert("Please enter valid IP Address(s)");
                                return false;
                            }
                        }
                    }
                    if ($('#domainName').val() !== "" && $('#domainName').val() !== null) {
                        if (!validateComa($('#domainName').val())) {
                            alert("Input value contains special charcter please enter right one");
                            return false;
                        }
                    }
                    var f = $("#settings");
                    if (($('#ipAddress').val() === "" && $('#domainName').val() === "")
                            || ($('#ipAddress').val() === null && $('#domainName').val() === null)) {
                        alert("Please enter either IP Address or DomainName");
                        $("#ipAddress").focus();
                        return false;
                    }
                    if ($("#selectStyle").val() === 1) {
                        alert("Please select Filter Type");
                        $("#selectStyle").focus();
                        return false;
                    }
                    if (isEdit) {
                        $('#ipAddress').hide();
                        $('#domainName').hide();
                        $('#ip').show();
                        $('#dom').show();
                        f.attr('action', "settings.html?saveFilter=update");
                    } else {
                        var filtNames = $("#ipAddress").val().split("\n");
                        var filtNamesDom = $("#domainName").val().split("\n");
                        if (filtNames !== "") {
                            for (var i = 0; i < filtNames.length; i++) {
                                if (!checkFilterName(filtNames[i].trim())) {
                                    alert("IP Address already exists");
                                    $("#ipAddress").focus();
                                    return false;
                                }
                            }
                        }
                        if (filtNamesDom !== "") {
                            for (var i = 0; i < filtNamesDom.length; i++) {
                                if (!checkFilterDomainName(filtNamesDom[i].trim())) {
                                    alert("Domain Name already exists");
                                    $("#domainName").focus();
                                    return false;
                                }
                            }
                            for (var i = 0; i < filtNamesDom.length; i++) {
                                if (!checkFilterName(filtNamesDom[i].trim())) {
                                    alert("Domain Name already exists");
                                    $("#domainName").focus();
                                    return false;
                                }
                            }
                        }
                        f.attr('action', "settings.html?saveFilter=savenew");
                    }
                    f.submit();
                }
                function validateComa(filterVal) {
                    if (filterVal.indexOf(',') > -1) {
                        return false;
                    } else {
                        return true;
                    }
                }
                function cancelFilter() {
                    $('#afterReload').hide();
                    var r = true;
                    if ($('#ipAddress').val() !== "" || $('#domainName').val() !== "") {
                        r = confirm("Do you want to proceed without saving the Filter details? ");
                        if (r === false) {
                            window.location.hash = '#showFilter';
                            return false;
                        }
                    }
                    if (r) {
                        var state = document.getElementById("showFilter");
                        if (state.style.display === 'block') {
                            document.getElementById("showFilter").style.display = 'none';
                            window.location.hash = '#showFilter';
                        }
                    }
                }
                function deleteFilter(event) {
                    $('#afterReload').hide();
                    var isDelete = false;
                    var dletedVal = document.getElementsByName("deleted");
                    for (var i = 0; i < dletedVal.length; i++) {
                        if (dletedVal[i].checked)
                            isDelete = true;
                    }
                    if (isDelete) {
                        var f = $("#settings");
                        f.attr('action', "settings.html?delete='delete'");
                        f.attr('method', "POST");
                        f.submit();

                    } else {
                        alert("Please select the filter(s) to delete");
                        return false;
                    }
                }
                function checkFilterName(filterName) {
                <% if (totRecords != null) {
                        for (Settings s : totRecords) {
                            out.println("filtname =\"" + s.getFilterName().trim() + "\";");
                %>
                    if (filtname === filterName)
                        return false;
                <%}
                    }%>
                    return true;
                }
                function checkFilterDomainName(domainName) {
                <% if (totRecords != null) {
                        for (Settings s : totRecords) {
                            out.println("domname =\"" + s.getFilterNameDomain().trim() + "\";");
                %>
                    if (domname === domainName)
                        return false;
                <%}
                    }%>
                    return true;
                }


                function naviToOtherTabs(menuOption) {
                    var menuParams = {menuOption: menuOption};
                    $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                        if (data !== null) {
                            parent.window.location = data;
                        }
                    });
                }

                function isValidIPAddress(ipaddr) {
                    var re = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
                    if (re.test(ipaddr)) {
                        var parts = ipaddr.split(".");
                        if (parseInt(parseFloat(parts[0])) === 0) {
                            return false;
                        }
                        for (var i = 0; i < parts.length; i++) {
                            if (parseInt(parseFloat(parts[i])) > 255) {
                                return false;
                            }
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
            <style type="text/css">
                #scroller {
                    overflow-x: hidden;
                    overflow-y: scroll;
                }

                .forscroller {
                    margin: 0 0 15px 35px;
                    width: 635px;
                    float: center;
                }

                #resultsDiv {
                    padding: 5px;
                    line-height: 24px;
                    background-color: #FFFFFF;
                }

                #resultsDiv table {
                    border: 1px solid #E9E9E9;
                    border-collapse: collapse;
                    width: 100%;
                    cellpadding: 4px;
                    cellspacing: 4px;
                }

                #resultsDiv  table td {
                    border: 1px solid #E9E9E9;
                    padding: 8px;
                    text-align: center;
                }

                #resultsDiv  table th {
                    border: 1px solid #E9E9E9;
                    padding: 8px;
                    text-align: center;
                }

                #totdivwidth,#totdivwidth1 {
                    width: 100%;
                    padding-bottom: 21px;
                }

                #txtname,#txtname1 {
                    width: 22%;
                    float: left;
                    margin-top: 18px;
                }

                textarea {
                    resize: none;
                    border: 1px solid silver;
                    text-align: left;
                    width: 350px;
                    height: 50px;
                    margin: 0px;
                }

                #selectStyle {
                    width: 170px;
                }

                .txtfieldstyle {
                    display: none;
                    margin-top: 25px;
                    border: 1px solid silver;
                    height: 19px;
                    width: 280px;
                }

                #afterReload {
                    display: none;
                    float: left;
                    padding-top: 7px;
                    padding-right: 10px;
                    color: #095B42;
                }

                #msg {
                    padding-bottom: 25px;
                    color: #095B42;
                    font-weight: bold;
                    font-size: 12px;
                    display: none;
                }


            </style>
        </head>
        <body onload="GetHeight();">
            <div class="responsive-wrapper">

                <div class="middle-wrapper">
                    <div class="" id="content">
                        <div id="tabs-icons">
                            <ul>
                                <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top " 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                     class="ui-state-default ui-corner-top"
                                                     onmouseover="this.style.cursor = 'pointer'"
                                                     onclick="naviToOtherTabs('autoMail')" >Automail</a></li>
                                <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                    class="ui-state-default ui-corner-top ui-state-active"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="naviToOtherTabs('settings')" >Settings</a></li>
                                <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'"
                                                          onclick="naviToOtherTabs('knowledgeBase')" >Knowledge Base</a></li>
                                <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                          onmouseover="this.style.cursor = 'pointer'" 
                                                          onclick="naviToOtherTabs('expertAnswer')">Expert Answer</a></li>
                                <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top"
                                                       onmouseover="this.style.cursor = 'pointer'"
                                                       onclick="naviToOtherTabs('expertUser')">Expert User</a></li>
                                <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top"
                                                       onmouseover="this.style.cursor = 'pointer'"
                                                       onclick="naviToOtherTabs('ateReports')" >ATE Reports</a></li>
                                <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                            </ul>
                        </div>
                        <div id="scont" style="float: left; padding-top: 10px;">
                            <div id="profil" class="shadow portlet vn">
                                <h2>Settings</h2>
                                <ul id="sett">
                                    <li id="filt" class="selected">Filter</li>
                                    <li id="toolConfig">Tool Config</li>
                                    <li id="adminImages">Admin Images</li>
                                    <li id="urlRedirection">URL Redirection</li>
                                </ul>
                            </div>
                        </div>
                        <div id="bcont" style="float: right; padding-top: 10px; margin-bottom: 20px;">
                            <div id="bc-sub" class="portlet shadow">
                                <form:form commandName="settings" method="POST">
                                    <div id="resultsDiv">
                                        <div style="float: right; padding-right: 65px; padding-top: 15px;">
                                            <div id="afterReload">Reloaded Successfully.</div>
                                            <img style="width: 85px; height: 40px; cursor: pointer;"
                                                 src="../css/0/images/reload.png" alt="Reload"
                                                 onclick="reload();"
                                                 title="To update the filters information in application context" />
                                        </div>
                                        <div style="padding-top: 70px;">
                                            <div id="scroller" class="forscroller">
                                                <table align="center">
                                                    <tr style="color: #f60;">
                                                        <th>IP Address&nbsp;/&nbsp;Domain Name</th>
                                                        <th>Filter Type</th>
                                                        <th>Delete</th>
                                                    </tr>

                                                    <%if (totRecords != null && !totRecords.isEmpty()) {
                                                            for (Settings st : totRecords) {%>
                                                    <tr>
                                                        <td style="color: blue;"><span id="filtName"
                                                                                       onclick="filterDetails(<%=st.getFilterId()%>, this,<%=st.getFilterOption()%>,<%=st.getRestrictionType()%>);"
                                                                                       style="cursor: pointer;"><%=st.getFilterName().trim()%><%=st.getFilterNameDomain().trim()%></span>
                                                        </td>
                                                        <%String restrictionType = "";
                                                            if (st.getRestrictionType() == 2) {
                                                                restrictionType = "Mark As Local";
                                                            } else if (st.getRestrictionType() == 3) {
                                                                restrictionType = "Discard Request";
                                                            }%>
                                                        <td><%=restrictionType%></td>
                                                        <td><form:checkbox id="deleted" path="deleted"
                                                            value="<%=st.getFilterId()%>" /></td>
                                                    </tr>
                                                    <%}
                                                    } else {%>
                                                    <tr>
                                                        <td colspan="3">No data available.</td>
                                                    </tr>
                                                    <%}%>
                                                </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right; padding-right: 28px;">
                                            <img style="width: 80px; height: 40px; cursor: pointer;"
                                                 src="../css/0/images/new.png" alt="New" onClick="showNew()" /> <img
                                                 style="width: 65px; height: 40px; cursor: pointer;"
                                                 src="../css/0/images/delete.png" name="image" alt="Delete"
                                                 onClick="deleteFilter(event)" />
                                        </div>
                                        <div id="showFilter" style="display: none; padding: 50px;">
                                            <div>
                                                <p id="msg">Note : Please enter either IP Address or Domain
                                                    Name as new line seperated values</p>
                                                <div id="totdivwidth">
                                                    <div id="txtname">
                                                        <h2>IP Address :</h2>
                                                    </div>
                                                    <form:textarea path="filterName" id="ipAddress" />
                                                    <input class="txtfieldstyle" id="ip" type="text"
                                                           name="ipaddress" readonly />
                                                </div>
                                                <div id="totdivwidth1">
                                                    <div id="txtname1">
                                                        <h2>Domain Name :</h2>
                                                    </div>
                                                    <form:textarea path="filterNameDomain" id="domainName" />
                                                    <input class="txtfieldstyle" id="dom" type="text" name="domain"
                                                           readonly />
                                                </div>
                                                <form:hidden id="filterId" path="filterId" />
                                                <form:hidden id="filterOption" path="filterOption" />
                                                <div class="totdivwidth">
                                                    <div style="width: 22%; float: left;">
                                                        <h2>Filter Type :</h2>
                                                    </div>
                                                    <form:select path="restrictionType" id="selectStyle">
                                                        <form:option value="1" label="Select..." />
                                                        <form:option value="2" label="Mark As Local" />
                                                        <form:option value="3" label="Discard Request" />
                                                    </form:select>
                                                </div>
                                            </div>
                                            <div style="padding-top: 15px;">
                                                <img
                                                    style="width: 60px; height: 39px; padding-top: 3px; padding-bottom: 1px;"
                                                    src="../css/0/images/save.png" onclick="saveFilter();"
                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                    style="width: 60px; height: 26px; padding-bottom: 8px;"
                                                    src="../css/0/images/cancel.gif" onclick="cancelFilter();"
                                                    onmouseover="this.style.cursor = 'pointer'" />
                                            </div>
                                        </div>
                                    </div>

                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%@ include file="/WEB-INF/view/footer.jsp"%>
        </body>
    </html>
    <%} catch (Exception e) {
            e.printStackTrace();
        }%>
