<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page import="lxr.marketplace.user.Login,java.util.List "%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<jsp:useBean id="settings"
             class="lxr.marketplace.admin.settings.Settings" />
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%    HttpSession sessionU = request.getSession();
    Login user = (Login) sessionU.getAttribute("user");
    Object ul = sessionU.getAttribute("userLoggedIn");
    boolean userLoggedIn = false;
    ExpertUser expertUser = null;
    if (ul != null) {
        userLoggedIn = (Boolean) ul;
    }
    if (userLoggedIn) {
        if (sessionU.getAttribute("expertUser") != null) {
            expertUser = (ExpertUser) sessionU.getAttribute("expertUser");
            pageContext.setAttribute("expertUser", expertUser);
        }
    }
%>
<html>
    <head>
        <title>LXRMarketplace | Settings</title>
        <meta name="keywords" content="Admin Reports" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
        <%@ include file="/WEB-INF/view/commonImport.jsp"%>
        <%@ include file="/WEB-INF/view/header.jsp"%>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript" src="/scripts/Charts.js"></script>
        <link rel="stylesheet"
              href="/css/0/themes/ui-lightness/jquery.ui.all.css">
        <link rel="stylesheet" href="css/lxrm-core.css">
        <link rel="stylesheet" href="css/admin.css">
        <script type="text/javascript">
            $(document).ready(function (event) {
            <%
                out.println("var userLoggedIn=" + userLoggedIn + ";");
            %>
                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    var role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>
            <%if (request.getAttribute("messageFailure") != null) {
                    out.println("var messageF = '" + request.getAttribute("messageFailure") + "';");%>
                $('#message-div').addClass("error-div");
                $('#message-div').text(messageF);
            <%} else if (request.getAttribute("messageSuccess") != null) {
                out.println("var messageS = '" + request.getAttribute("messageSuccess") + "';");%>
                $('#message-div').addClass("success-div");
                $('#message-div').text(messageS);
            <%}%>
                var logo = document.getElementById("logo");
                logo.href = "/dashboard.html";
                $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                $("#tabs-icons").tabs();
                $('#sett li').click(function () {
                    if (this.id == "filt") {
                        window.location = "/settings.html";
                    } else if (this.id == "adminImages") {
                        window.location = "/adminimages.html";
                    } else if (this.id == "toolConfig") {
                        window.location = "/toolconfig.html";
                    }
                });
            <%
                if (user != null) {
                    out.println("$(\"#loginSuccess\").show();");
                } else {
                    out.println("$(\"#loginSuccess\").hide();");
                }
            %>
                $("#addUrl").bind('click', function (e) {
                    e.preventDefault();
                    var newUrl = $("#newUrl").val();
                    var newReUrl = $("#newReUrl").val();
                    if (newUrl == "") {
                        alert("Please provide url");
                        $("#newUrl").focus();
                        return false;
                    } else if (newReUrl == "") {
                        alert("Please provide redirection url");
                        $("#newReUrl").focus();
                        return false;
                    } else {
                        var f = $("#urlredirectionform");
                        f.attr('action', "admin-url-redirection.html?action=addNew");
                        f.submit();
                    }
                });
            });
            function navgToReports() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?report='report'");
                f.submit();
            }

            function navgToAutoMail() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?automail='automail'");
                f.submit();
            }
            function navgToDashBoard() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?dashboard='dashboard'");
                f.submit();
            }
            function navgToKnowledgeBase() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?knowledgeBase='knowledgeBase'");
                f.submit();
            }

            function navgToExpertAnswers() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?expertAnswer='expertAnswer'");
                f.submit();
            }

            function showExpertUsers() {
                var f = $("#urlredirectionform");
                f.attr('action', "admin-url-redirection.html?expertUsers='expertUsers'");
                f.submit();
            }

            function changeStatus(element, urlId) {
                $.ajax({
                    type: 'POST',
                    url: "/admin-url-redirection.html?action=changeStatus&urlId=" + urlId,
                    processData: true,
                    data: {},
                    dataType: "json",
                    success: function () {
                        var classes = $(element).attr('class');
                        if (classes.indexOf('re-status-true') > 0) {
                            $(element).removeClass('re-status-true');
                            $(element).addClass('re-status-false')
                        } else {
                            $(element).removeClass('re-status-false');
                            $(element).addClass('re-status-true')
                        }
                    }
                });
            }
        </script>
        <style type="text/css">
            .table {
                display: table;
                width: 96%;
                margin: auto;
            }

            .t-head {
                width: 100%;
                display: table-row;
            }

            .t-row {
                width: 100%;
                display: table-row;
            }

            .t-head div:nth-child(1) {
                width: 45%;
                border-top: 1px solid #ccc;
                font-weight: bold;
                color: #ff6600;
            }

            .t-head div:nth-child(2) {
                width: 45%;
                border-top: 1px solid #ccc;
                font-weight: bold;
                color: #ff6600;
            }

            .t-head div:nth-child(3) {
                width: 10%;
                border-right: 1px solid #ccc;
                border-top: 1px solid #ccc;
                font-weight: bold;
                color: #ff6600;
            }

            .t-row div:nth-child(1) {
                width: 45%;
            }

            .t-row div:nth-child(2) {
                width: 45%;
            }

            .t-row div:nth-child(3) {
                width: 10%;
                border-right: 1px solid #ccc;
            }

            .t-cell {
                display: table-cell;
                border-left: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                padding: 4px;
            }

            .text-input {
                height: 20px;
                width: 290px;
            }

            .shade {
                background-color: #eee;
                text-align: center;
            }

            .button {
                font-family: 'Montserrat-Bold', sans-serif;
                background: url("/css/0/images/SEONinja/middlebutton.png") 0 center
                    repeat-x scroll transparent;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
                border: 0;
                height: 30px;
                width: 50px;
                color: white;
                text-transform: uppercase;
                font-size: 14px;
                font-weight: bold;
                cursor: pointer;
            }

            .re-status-true {
                background: url('/css/0/images/Icon_Slider_On.png') 12px center
                    no-repeat;
                color: transparent;
                background-size: 40px auto;
                cursor: pointer;
            }

            .re-status-false {
                background: url('/css/0/images/Icon_Slider_Off.png') 12px center
                    no-repeat;
                color: transparent;
                background-size: 40px auto;
                cursor: pointer;
            }

            .error-div,.success-div {
                width: 90%;
                margin: 10px 0 10px 14px;
            }
        </style>
    </head>
    <body>
        <div class="responsive-wrapper">

            <div class="middle-wrapper">
                <div class="" id="content">
                    <div id="tabs-icons">
                        <ul>
                            <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                  class="ui-state-default ui-corner-top " 
                                                  onmouseover="this.style.cursor = 'pointer'" 
                                                  onclick="navgToDashBoard()">Dashboard</a></li>
                            <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                class="ui-state-default ui-corner-top"
                                                onmouseover="this.style.cursor = 'pointer'"
                                                onclick="navgToReports();" >Reports</a></li>
                            <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                 class="ui-state-default ui-corner-top"
                                                 onmouseover="this.style.cursor = 'pointer'"
                                                 onclick="navgToAutoMail()" >Automail</a></li>
                            <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                class="ui-state-default ui-corner-top ui-state-active"
                                                onmouseover="this.style.cursor = 'pointer'"
                                                onclick="navgToSettings()" >Settings</a></li>
                            <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top"
                                                      onmouseover="this.style.cursor = 'pointer'"
                                                      onclick="navgToKnowledgeBase()" >Knowledge Base</a></li>
                            <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="navgToExpertAnswers()">Expert Answer</a></li>
                            <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                   class="ui-state-default ui-corner-top"
                                                   onmouseover="this.style.cursor = 'pointer'"
                                                   onclick="navgToExpertUser()" >Expert User</a></li>
                        </ul>
                    </div>
                    <div id="scont" style="float: left; padding-top: 10px;">
                        <div id="profil" class="shadow portlet vn">
                            <h2>Settings</h2>
                            <ul id="sett">
                                <li id="filt">Filter</li>
                                <li id="toolConfig">Tool Config</li>
                                <li id="adminImages">Admin Images</li>
                                <li id="urlRedirection" class="selected">URL Redirection</li>
                            </ul>
                        </div>
                    </div>
                    <div id="bcont" style="float: right; padding-top: 10px;height: 442px;">
                        <div id="bc-sub" class="portlet shadow">
                            <form:form commandName="urlredirectionform" method="POST">
                                <div id="message-div"></div>
                                <div class="table">
                                    <div class="t-head">
                                        <div class="t-cell">URL</div>
                                        <div class="t-cell">Redirection URL</div>
                                        <div class="t-cell">Status</div>
                                    </div>
                                    <div class="t-row">
                                        <div class="t-cell shade">
                                            <form:input path='newUrl' cssClass="text-input" />
                                        </div>
                                        <div class="t-cell shade">
                                            <form:input path='newReUrl' cssClass="text-input" />
                                        </div>
                                        <div class="t-cell shade">
                                            <input id="addUrl" type="button" class="button" value="Add">
                                        </div>
                                    </div>
                                    <c:forEach items="${urlredirectionform.redirections}"
                                               varStatus="redirectRow">
                                        <div class="t-row">
                                            <div class="t-cell">
                                                <spring:bind
                                                    path="urlredirectionform.redirections[${redirectRow.index}].url">
                                                    <c:out value="${status.value}" />
                                                </spring:bind>
                                            </div>
                                            <div class="t-cell">
                                                <spring:bind
                                                    path="urlredirectionform.redirections[${redirectRow.index}].redirectionURL">
                                                    <c:out value="${status.value}" />
                                                </spring:bind>
                                            </div>
                                            <spring:bind
                                                path="urlredirectionform.redirections[${redirectRow.index}].id">
                                                <div
                                                    onClick="changeStatus(this, <c:out value="${status.value}" />);"
                                                </spring:bind>
                                                <spring:bind
                                                    path="urlredirectionform.redirections[${redirectRow.index}].status"> 
                                                    class='t-cell re-status-<c:out value="${status.value}" />'> 

                                                </div>
                                            </spring:bind>
                                        </div>
                                    </c:forEach>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
                <%@ include file="/WEB-INF/view/footer.jsp"%>
            </div>
        </div>
    </body>
</html>