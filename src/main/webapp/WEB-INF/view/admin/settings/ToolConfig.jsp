<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page import="lxr.marketplace.user.Login,java.util.List "%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<jsp:useBean id="settings" class="lxr.marketplace.admin.settings.Settings" />
<%    HttpSession sessionT = request.getSession();
    Login user = (Login) sessionT.getAttribute("user");
    Object ul = sessionT.getAttribute("userLoggedIn");
    boolean userLoggedIn = false;
    ExpertUser expertUser = null;
    if (ul != null) {
        userLoggedIn = (Boolean) ul;
    }
    if (userLoggedIn) {
        if (sessionT.getAttribute("expertUser") != null) {
            expertUser = (ExpertUser) sessionT.getAttribute("expertUser");
            pageContext.setAttribute("expertUser", expertUser);
        }
    }
%>
<html>
    <head>
        <title>LXRMarketplace | Settings</title>
        <meta name="keywords" content="Admin Reports" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
        <%@ include file="/WEB-INF/view/commonImport.jsp"%>
        <%@ include file="/WEB-INF/view/header.jsp"%>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript" src="/scripts/Charts.js"></script>
        <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
        <link rel="stylesheet" href="css/lxrm-core.css">
        <link rel="stylesheet" href="css/admin.css">
        <script type="text/javascript">
            $(document).ready(function (event) {
            <%
                out.println("var userLoggedIn=" + userLoggedIn + ";");
            %>
            <c:if test="${not empty expertUser}" >
                var name = "${expertUser.name}"
                var role = "${expertUser.role}"
                if (name.length > 15) {
                    name = name.substring(0, 15);
                }
                $('#uName').text(name + " (" + role + ")");
            </c:if>
                var logo = document.getElementById("logo");
                logo.href = "/dashboard.html";
                $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                $("#tabs-icons").tabs();
                $('#sett li').click(function () {
                    if (this.id == "filt") {
                        window.location = "/settings.html";
                    } else if (this.id == "adminImages") {
                        window.location = "/adminimages.html";
                    } else if (this.id == "urlRedirection") {
                        window.location = "/admin-url-redirection.html";
                    }
                });
            <%
                if (user != null) {
                    out.println("$(\"#loginSuccess\").show();");
                } else {
                    out.println("$(\"#loginSuccess\").hide();");
                }
            %>
            });
            function navgToReports() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?report='report'");
                f.submit();
            }

            function navgToAutoMail() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?automail='automail'");
                f.submit();
            }
            function navgToDashBoard() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?dashboard='dashboard'");
                f.submit();
            }
            function navgToKnowledgeBase() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?knowledgeBase='knowledgeBase'");
                f.submit();
            }

            function navgToExpertAnswers() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?expertAnswer='expertAnswer'");
                f.submit();
            }

            function showExpertUsers() {
                var f = $("#toolconfig");
                f.attr('action', "toolconfig.html?expertUsers='expertUsers'");
                f.submit();
            }

            function reload() {
                $.ajax({
                    type: "POST",
                    url: "/toolconfig.html?reload='reload'",
                    success: function (data) {
                        $('#afterReload').show();
                    }
                });
            }
        </script>
        <style type="text/css">
            #resultsDiv {
                padding: 5px;
                /* 	line-height: 24px; */
                background-color: #FFFFFF;
                text-align: center;
            }
            #bcont{height:429px;}
        </style>
    </head>
    <body>
        <div class="responsive-wrapper">

            <div class="middle-wrapper">
                <div class="" id="content">
                    <div id="tabs-icons">
                        <ul>
                            <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                  class="ui-state-default ui-corner-top " 
                                                  onmouseover="this.style.cursor = 'pointer'" 
                                                  onclick="navgToDashBoard()">Dashboard</a></li>
                            <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                class="ui-state-default ui-corner-top"
                                                onmouseover="this.style.cursor = 'pointer'"
                                                onclick="navgToReports();" >Reports</a></li>
                            <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                 class="ui-state-default ui-corner-top"
                                                 onmouseover="this.style.cursor = 'pointer'"
                                                 onclick="navgToAutoMail()" >Automail</a></li>
                            <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                class="ui-state-default ui-corner-top ui-state-active"
                                                onmouseover="this.style.cursor = 'pointer'"
                                                onclick="navgToSettings()" >Settings</a></li>
                            <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top"
                                                      onmouseover="this.style.cursor = 'pointer'"
                                                      onclick="navgToKnowledgeBase()" >Knowledge Base</a></li>
                            <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                      class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="navgToExpertAnswers()">Expert Answer</a></li>
                            <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                   class="ui-state-default ui-corner-top"
                                                   onmouseover="this.style.cursor = 'pointer'"
                                                   onclick="navgToExpertUser()" >Expert User</a></li>
                        </ul>
                    </div>
                    <div id="scont" style="float: left; padding-top: 10px;">
                        <div id="profil" class="shadow portlet vn">
                            <h2>Settings</h2>
                            <ul id="sett">
                                <li id="filt">Filter</li>
                                <li id="toolConfig" class="selected">Tool Config</li>
                                <li id="adminImages">Admin Images</li>
                                <li id="urlRedirection">URL Redirection</li>
                            </ul>
                        </div>
                    </div>
                    <div id="bcont" style="float: right; padding-top: 10px; margin-bottom: 20px;">
                        <div id="bc-sub" class="portlet shadow">
                            <form:form commandName="toolconfig" method="POST">
                                <div id="resultsDiv">
                                    <div id="afterReload" style="display: none; color: #095B42;">Reloaded
                                        Successfully.</div>
                                    <img
                                        style="width: 85px; height: 40px; cursor: pointer; padding: 5px;"
                                        src="../css/0/images/reload.png" alt="Reload" onclick="reload();"
                                        title="To update the tools information in application context" />
                                </div>
                            </form:form>
                        </div>
                    </div>

                </div>
            </div>
            <%@ include file="/WEB-INF/view/footer.jsp"%>
    </body>
</html>