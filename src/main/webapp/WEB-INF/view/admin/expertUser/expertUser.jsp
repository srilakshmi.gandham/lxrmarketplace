<%-- 
    Document   : expertUser.
    Created on : 28 Oct, 2016.
    Author     : NE16T1213.
--%>
<%@ include file="/WEB-INF/view/include.jsp"%>

<%@ page import="lxr.marketplace.user.Login,java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%    HttpSession expertSession = request.getSession();
    Login user = (Login) expertSession.getAttribute("user");
    Object ul = expertSession.getAttribute("userLoggedIn");
    boolean userLoggedIn = false;
    ExpertUser expertUser = null;
    if (ul != null) {
        userLoggedIn = (Boolean) ul;
    }
    String adminMessage = null;
    if (expertSession.getAttribute("adminMessage") != null) {
        adminMessage = (String) expertSession.getAttribute("adminMessage");
    }
    if (userLoggedIn) {
        if (expertSession.getAttribute("expertUser") != null) {
            expertUser = (ExpertUser) expertSession.getAttribute("expertUser");
            pageContext.setAttribute("expertUser", expertUser);
        }
    }


%>
<html>
    <head>
        <title>LXRMarketplace | Expert User</title>
        <meta name="keywords" content="Expert User" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
        <%@ include file="/WEB-INF/view/commonImport.jsp"%>
        <%@ include file="/WEB-INF/view/header.jsp"%>

        <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="/scripts/tools/expertUser.js"></script>
        <script src="js/classie.js"></script>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript" src="/scripts/Charts.js"></script>

        <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
        <link rel="stylesheet" href="/css/lxrm-core.css">
        <link rel="stylesheet" href="/css/admin.css">
        <link rel="stylesheet" href="/css/expertUser.css">    
        <script type="text/javascript">
            <%
                out.println("var userLoggedIn=" + userLoggedIn + ";");
            %>
            var selectedExpertUsersArray = [];
            $(document).ready(function () {
                var role = "";
            <c:if test="${not empty expertUser}" >
                var name = "${expertUser.name}"
                role = "${expertUser.role}"
                if (name.length > 15) {
                    name = name.substring(0, 15);
                }
                $('#uName').text(name + " (" + role + ")");
            </c:if>
                if (role === 'expert') {
                    $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                } else if (role === 'reviewer') {
                    $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                }
                /* Request to get list of expert users.*/
                fetchExpertUsersData();
                /*Display popup to add expert user.*/
                $("#addExpertUser").click(function () {
                    $('.popUpWrapper').html("Add New User:");
                    $("#dnld-ok-out-box").show();
                    $("#destOkUrlPopUp").show();
                    $('#downloadpopup').show();
                    $('#expertIdFeed').val(0);
                    $('#updatingFeed').val(false);
                    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                    selectedExpertUsersArray = [];
                    selectedExpertUsersArray.length = 0;
                    $("#userSubmit,#userCancel").attr('disabled', false);
                });

                /*Display popup to edit expert user.*/
                $("#updateExpertUser").click(function () {
                    if ($("#emptyUsersData").css('display') === "none") {
                        $('.error').hide();
                        modifyExpertUserData();
                    } else if ($("#emptyUsersData").css('display') === "block") {
                        alert("Add Expert Users.");
                    }
                });
                /*Action to handle Add/Update Expert User data.*/
                $("#userSubmit").click(function () {
                    var submitStatus = $("#updatingFeed").val();
                    $("#userExisted").empty();
                    $("#userExisted").hide();
                    $("#userSubmit,#userCancel").attr('disabled', true);
                    if (submitStatus === "true") {
                        return validateUserFeed(true);
                    } else if (submitStatus === "false") {
                        return  validateUserFeed(false);
                    }
                });
                /*Close the popup,reset the input fields.*/
                $("#userCancel").click(function () {
                    $("#dnld-ok-out-box").hide();
                    $("#destOkUrlPopUp").hide();
                    $('#downloadpopup').hide();
                    resetUserFields();
                    $("#userSubmit,#userCancel").attr('disabled', false);
                });
                /*Message for user operation.*/
            <% if (adminMessage != null) {%>
                $("#userOpertionMessage").html("<%=adminMessage%>");
                $("#userOpertionMessage").show();
                $("#userOpertionMessage").fadeOut(30000, "swing");
            <%}
                expertSession.removeAttribute("adminMessage");
            %>
                var logo = document.getElementById("logo");
                logo.href = "/dashboard.html";
                $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                $("#tabs-icons").tabs();
            <%   if (user != null) {
                    out.println("$(\"#loginSuccess\").show();");
                } else {
                    out.println("$(\"#loginSuccess\").hide();");
                }
            %>
            });


        </script>
    </head>
    <body>
        <div class="responsive-wrapper">

            <div class="middle-wrapper">
                <div id="content">
                    <div id="expert-user-wrapper">
                        <div id="tabs-icons">
                            <ul>
                                <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                      class="ui-state-default ui-corner-top " 
                                                      onmouseover="this.style.cursor = 'pointer'" 
                                                      onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'"
                                                    onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                     onclick="naviToOtherTabs('autoMail')" 
                                                     class="ui-state-default ui-corner-top"
                                                     onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                    onclick="naviToOtherTabs('settings')" 
                                                    class="ui-state-default ui-corner-top"
                                                    onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                          onclick="naviToOtherTabs('knowledgeBase')" 
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                          onclick="naviToOtherTabs('expertAnswer')" 
                                                          class="ui-state-default ui-corner-top"
                                                          onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top 
                                                       ui-tabs-selected ui-state-active" 
                                                       onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                       class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                       onclick="naviToOtherTabs('ateReports')" 
                                                       onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                            </ul>

                            <div id="expertUserResultDiv" >
                                <div class="userOptionsWrapper">
                                    <div id="optionsDiv">
                                        <button id="addExpertUser">ADD</button>
                                        <button id="updateExpertUser">UPDATE</button>
                                    </div>
                                </div>
                                <div class="resultWrapper">
                                    <div id="userOpertionMessage"></div>
                                    <div id="expertUserResultBlock"></div>
                                    <table id="expertUserResultTable">
                                    </table>
                                    <div id="emptyUsersData"><p>No data available</p></div>
                                    <!-- Pop Up Wrapper Code-->
                                    <div id="dnld-ok-out-box">
                                        <div id="destOkUrlPopUp">
                                            <div id="downloadpopup" class="popup popupPortlet ui-corner-all">
                                                <div id="downloadpopupmsg">
                                                    <div class='content'>
                                                        <div class="expertUserBlockWrapper">
                                                            <form id="expertUser">
                                                                <div class="popupTable">
                                                                    <div class="popUpWrapper color2 Bold"></div>
                                                                    <input type="hidden" id="expertIdFeed"/>
                                                                    <input type="hidden" id="updatingFeed"/>
                                                                    <div id="nameForm">
                                                                        <label>Name:</label>
                                                                        <div>
                                                                            <input type="text" id="nameFeed"/>
                                                                            <span id="nameError" class="error"/>
                                                                        </div>
                                                                    </div>   <!--EOF formIndividualWrap -->
                                                                    <div id="emailForm">
                                                                        <label>Email:</label>
                                                                        <div>
                                                                            <input type="text" id="emailFeed"/>
                                                                            <span id="emailError" class="error"/>
                                                                        </div>
                                                                    </div>   
                                                                    <div id="roleForm">
                                                                        <label>Role:</label>
                                                                        <div>
                                                                            <select  id="roleFeed">
                                                                                <option value="">Select</option>
                                                                                <option value="admin">Admin</option>
                                                                                <option value="expert">Expert</option>
                                                                                <option value="reviewer">Reviewer</option>
                                                                            </select>
                                                                            <span id="roleError" class="error"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="userExisted"></div>
                                                            </form> 
                                                            <div class="submit-wrap">
                                                                <button type="button" id="userSubmit">Submit</button>
                                                                <button type="button" id="userCancel">Cancel</button>
                                                            </div>
                                                        </div><!-- End of popupTable div-->   

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> 
        <%@ include file="/WEB-INF/view/footer.jsp"%>
    </body>
</html>
