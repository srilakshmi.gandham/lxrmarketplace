
<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,java.util.*"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%    HttpSession sessionK = request.getSession();
        Login user = (Login) sessionK.getAttribute("user");

        Object ul = sessionK.getAttribute("userLoggedIn");
        boolean userLoggedIn = false;
        ExpertUser expertUser = null;
        if (ul != null) {
            userLoggedIn = (Boolean) ul;
        }
        if (userLoggedIn) {
            if (sessionK.getAttribute("expertUser") != null) {
                expertUser = (ExpertUser) sessionK.getAttribute("expertUser");
                pageContext.setAttribute("expertUser", expertUser);
            }
        }

        String questionStatus = "";
        boolean qstatus = false;
        if (sessionK.getAttribute("questionStatus") != null) {
            questionStatus = (String) sessionK.getAttribute("questionStatus");
            qstatus = true;
            sessionK.removeAttribute("questionStatus");
        }
    %>
    <html>
        <head>
            <title>LXRMarketplace | Knowledge Base</title>
            <meta name="keywords" content="Knowledge Base" />
            <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

            <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
            <%@ include file="/WEB-INF/view/commonImport.jsp"%>
            <%@ include file="/WEB-INF/view/header.jsp"%>

            <script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
            <script src="js/classie.js"></script>
            <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
            <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

            <script type="text/javascript" src="/scripts/tools/knowledgeBase_text_editor.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

            <!--            <script type="text/javascript" src="/scripts/tools/knowledgebase_bootstrap_min.js"></script>-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="/css/knowledgeBase_text_editor.css">
            <!--            <link rel="stylesheet" href="/css/0/knowledgebase/font_awesome_min.css">
                        <link rel="stylesheet" href="/css/0/knowledgebase/bootstrap_min.css">-->
            <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
            <script type="text/javascript" src="/scripts/Charts.js"></script>

            <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" href="/css/lxrm-core.css">
            <link rel="stylesheet" href="/css/admin.css">
            <link rel="stylesheet" href="/css/knowledgeBase.css">

            <script type="text/javascript">
                <%
                    out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                    out.println("var userLoggedIn=" + userLoggedIn + ";");
                %>
                var selectedKnowledgeBaseArray = [];

                $(document).ready(function () {

                    var role = "";
                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>
                    if (role === 'expert') {
                        $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                    } else if (role === 'reviewer') {
                        $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                    }

                    var logo = document.getElementById("logo");
                    logo.href = "/dashboard.html";

                    $("#tabs-icons").tabs();
                    $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                <%
                    if (user != null) {
                        out.println("$(\"#loginSuccess\").show();");
                    } else {
                        out.println("$(\"#loginSuccess\").hide();");
                    }
                %>

                    $("#answer").Editor();
                <c:set var="buildQuestionsRows" value="${buildQuestionsRows}"/>
                <c:if test="${buildQuestionsRows eq true}">
                    getQuestionList();
                </c:if>
                    var qStatus = <%=qstatus%>;
                    var statusMessage = "<%=questionStatus%>";
                    if (!statusMessage.indexOf("failed", 1) > -1 && qStatus)
                    {
                        $("#statusMessage").show();
                        $("#statusMessage P").html(statusMessage);
                        $("#statusMessage").fadeOut(8000, "swing");
                        $("#statusMessage").css({"color": "green"});
                    } else if (statusMessage.indexOf("failed", 1) > -1 && qStatus)
                    {
                        $("#statusMessage").show();
                        $("#statusMessage P").html(statusMessage);
                        $("#statusMessage").fadeOut(8000, "swing");
                        $("#statusMessage").css({"color": "red"});
                    } else {
                        $("#statusMessage").hide();
                    }

                    $("#submitQuestion").click(function () {

                        var userQuestion = $("#question").val();
                        var userAnswer = $("#answer").Editor("getText");
                        var validateAnswer = $('.Editor-editor').html();
                        var tempValidateAnswer = "";
                        //alert("userAnswer "+userAnswer+"validateAnswer"+validateAnswer);
                        /*Tool Validation*/
                        var toolIds = 0;
                        toolIds = $("#multipleTooldID").val();
                        if (toolIds === null) {
                            alert("Please select tool.");
                            return false;
                        }
                        /*Question Validation*/
                        if ((userQuestion.trim() === null) || (userQuestion.trim() === "")
                                || (userQuestion.trim() === " ")) {
                            alert("Please enter question.");
                            $("#question").focus();
                            return false;
                        } else {
                            var text_length = userQuestion.length;
                            if (text_length > 1024) {
                                alert("Please ensure the question is below 1024 characters");
                                $("#question").focus();
                                return false;
                            }
                        }
                        /*Answer Validation*/

                        var htmlTextFilter = $("<div />").html(userAnswer).text();
                        if ((htmlTextFilter.trim() === null) || (htmlTextFilter.trim() === "") ||
                                (htmlTextFilter.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        }

                        var htTxtFltrValidate = $("<div />").html(validateAnswer).text();
                        if ((htTxtFltrValidate.trim() === null) || (htTxtFltrValidate.trim() === "") ||
                                (htTxtFltrValidate.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        }

                        if ((userAnswer.trim() === null) || (userAnswer.trim() === "") ||
                                (userAnswer.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        } else {

                            var unFilteredText = validateAnswer;
                            validateAnswer = validateAnswer.replace(/&nbsp;/gi, " ");
                            validateAnswer = validateAnswer.replace(/<br>/gi, " ");
                            tempValidateAnswer = validateAnswer;
                            //alert("tempValidateAnswer"+tempValidateAnswer);
                            if (tempValidateAnswer.match(/^\s+$/) === null) {
                                $("#answer").val(unFilteredText.trim());
                                var f = $("#knowledgeBase");
                                f.attr('action', "/knowledge-base.html?addQuestion=addQuestion");
                                f.attr('method', 'POST');
                                f.submit();
                                $("#selectALL").removeAttr('checked');
                                $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                                $('#userSelectedActive').prop('selectedIndex', 0);
                                $("#userSelectedType").prop('selectedIndex', 0);
                                $('#user_options').prop('selectedIndex', 0);
                                $("#userSelectedToolID").prop('selectedIndex', 0);
                            } else {
                                alert("Please enter answer.");
                                $(".Editor-editor").val('');
                                $(".Editor-editor").empty();
                                $(".Editor-editor").focus();
                                return false;
                            }
                        }
                    });/*End of submit*/

                    $("#modifyQuestion").click(function () {

                        var modifyContQuestion = $("#question").val();
                        var modifyContAnswer = $("#answer").Editor("getText");
                        var updateValidate = $('.Editor-editor').html();
                        var tempValidateAnswer = "";
                        /*Tool Validation*/
                        var updateToolId = 0;
                        updateToolId = $("#multipleTooldID").val();
                        if (updateToolId === null) {
                            alert("Please select tool.");
                            return false;
                        }
                        /*Question Validation*/
                        if ((modifyContQuestion.trim() === null) || (modifyContQuestion.trim() === "")
                                || (modifyContQuestion.trim() === " ")) {
                            alert("Please enter question.");
                            $("#question").focus();
                            return false;
                        } else {
                            var modify_text_length = modifyContQuestion.length;
                            if (modify_text_length > 1024) {
                                alert("Please ensure the question is below 1024 characters");
                                $("#question").focus();
                                return false;
                            }
                        }
                        /*Tool Validation*/

                        var htmlTextFilter = $("<div />").html(modifyContAnswer).text();
                        console.log("htmlTextFilter of modifyContAnswer " + htmlTextFilter);
                        if ((htmlTextFilter.trim() === null) || (htmlTextFilter.trim() === "") ||
                                (htmlTextFilter.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        }

                        var htTxtFltrValidate = $("<div />").html(updateValidate).text();
                        console.log("htTxtFltrValidate of updateValidate " + htTxtFltrValidate);
                        if ((htTxtFltrValidate.trim() === null) || (htTxtFltrValidate.trim() === "") ||
                                (htTxtFltrValidate.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        }


                        if ((modifyContAnswer.trim() === null) || (modifyContAnswer.trim() === "")
                                || (modifyContAnswer.trim() === " ")) {
                            alert("Please enter answer.");
                            $(".Editor-editor").focus();
                            return false;
                        } else {
                            var updateUnFilteredText = updateValidate;
                            updateValidate = updateValidate.replace(/&nbsp;/gi, " ");
                            updateValidate = updateValidate.replace(/<br>/gi, " ");
                            tempValidateAnswer = updateValidate;
                            if (tempValidateAnswer.match(/^\s+$/) === null) {
                                updateQuestion(updateUnFilteredText.trim());
                            } else {
                                alert("Please enter answer.");
                                $(".Editor-editor").val('');
                                $(".Editor-editor").empty();
                                $(".Editor-editor").focus();
                                return false;
                            }
                        }
                    });
                    $("#userSelectedToolID").change(function () {
                        $('#userSelectedActive').prop('selectedIndex', 0);
                        $("#userSelectedType").prop('selectedIndex', 0);
                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                        var f = $("#knowledgeBase");
                        f.attr('action', "/knowledge-base.html?applyToolFilter=applyToolFilter");
                        f.attr('method', 'POST');
                        f.submit();

                    });
                    /*To Display PopUp*/
                    $("#user_options").on('change', function () {
                        var selectOPt = $("#user_options option:selected").val();
                        if (selectOPt === "addOption") {
                            $("#dnld-ok-out-box").show();
                            $("#destOkUrlPopUp").show();
                            $('#dnld-pop-out-box').show();
                            $('#downloadpopup').show();

                            var addToolId = $("#userSelectedToolID").val();
                            $("#multipleTooldID").val(addToolId);

                            $("#question").val('');
                            $("#answer").Editor("setText", '');
                            $("#answer").val('');
                            $(".Editor-editor").val('');
                            $(".Editor-editor").empty();

                            $("#questionType2").prop("checked", true);
                            $("#activeType1").prop("checked", true);

                            $("#modifyDiv").hide();
                            $("#submitDiv").show();
                            $("#questionID").val(0);

                        } else if (selectOPt === "modifyOption") {
                            modifyQuestionTemplate();
                        } else if (selectOPt === "deleteOption") {
                            deleteQuestions();
                        }
                    });
                    function selectFilterCases() {
                        var toolId = $("#userSelectedToolID").val();
                        var questionType = $("#userSelectedType").val();
                        var activeStatus = $("#userSelectedActive").val();
                        /*alert("toolId "+toolId+"questionType"+questionType);*/
                        console.log("activeStatus is " + activeStatus + "questionType is" + questionType);
                        var i = 0;
                        var j = 0;
                        var m = 0;
                        var n = 0;
                        switch (questionType) {
                            case "1":
                                i = 1;
                                j = 0;
                                break;
                            case "2":
                                i = 2;
                                j = 0;
                                break;
                            default:
                                i = 1;
                                j = 2;
                                break;
                        }

                        switch (activeStatus) {
                            case "1":
                                m = 1;
                                n = 0;
                                break;
                            case "2":
                                m = 2;
                                n = 0;
                                break;
                            default:
                                m = 1;
                                n = 2;
                                break;
                        }
                        console.log("i--" + i + " j--" + j + " m--" + m + " n--" + n);
                        var params = {paidType: i, unpaidType: j, activeType: m, inactiveType: n, toolId: toolId};
                        jQuery.getJSON("/knowledge-base.html?applyTypeFilter=applyTypeFilter", params,
                                function (data) {
                                    buildSortedData(data);
                                });
                    }
                    $("#userSelectedType").change(function () {
                        selectFilterCases();
                    });

                    $("#userSelectedActive").change(function () {
                        selectFilterCases();
                    });

                    $("#cancelIndetailPopup").click(function () {
                        closeInDetial();
                    });
                    $("#cancelQuestion").click(function () {
                        closePopUP();
                    });
                    $("#closePopup").click(function () {
                        closePopUP();
                    });
                });/*End of doc on ready*/
                function showQuestionPopUP() {
                    $("#dnld-ok-out-box").show();
                    $("#destOkUrlPopUp").show();
                    $('#dnld-pop-out-box').show();
                    $('#downloadpopup').show();
                }
                function deleteQuestions() {
                    if (selectedKnowledgeBaseArray.length === 0) {
                        $(".userSelectWrapper :checked").each(function () {
                            selectedKnowledgeBaseArray.push($(this).val());
                        });
                    }
                    var questionsSelected = false;

                    if (selectedKnowledgeBaseArray.length > 0) {
                        questionsSelected = true;
                    }
                    if (questionsSelected === false) {
                        alert("Please select question to delete.");
                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                        return false;
                    } else {
                        var conformation = true;
                        conformation = confirm("Do you want to delete the selected Question?");
                        if (conformation === false) {
                            $('#user_options').prop('selectedIndex', 0);
                            selectedKnowledgeBaseArray = [];
                            selectedKnowledgeBaseArray.length = 0;
                            $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                            $("#selectALL").removeAttr('checked');
                            return false;
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "/knowledge-base.html?deleteQuestion=deleteQuestion",
                                processData: true,
                                data: {'selQuestion': selectedKnowledgeBaseArray},
                                dataType: "json",
                                traditional: true,
                                success: function (data) {
                                    buildSortedData(data);
                                    /*selectedKnowledgeBaseArray = [];
                                     selectedKnowledgeBaseArray.length=0;*/
                                    $('#userSelectedActive').prop('selectedIndex', 0);
                                    $("#userSelectedType").prop('selectedIndex', 0);
                                    $('#user_options').prop('selectedIndex', 0);
                                    $("#userSelectedToolID").prop('selectedIndex', 0)
                                    $("#selectALL").removeAttr('checked');
                                    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');

                                }
                            });
                        }
                    }
                }
                function modifyQuestionTemplate() {
                    if (selectedKnowledgeBaseArray.length === 0) {
                        $(".userSelectWrapper :checked").each(function () {
                            selectedKnowledgeBaseArray.push($(this).val());
                        });
                    }
                    console.log("In mdfy selectedKnowledgeBaseArray is " + selectedKnowledgeBaseArray + " and length" + selectedKnowledgeBaseArray.length);
                    if (selectedKnowledgeBaseArray.length === 0) {
                        alert("Please select question to modify.");
                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                        return false;
                    } else if (selectedKnowledgeBaseArray.length >= 2) {
                        alert("Please select single question to modify.");
                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                        return false;
                    } else if (selectedKnowledgeBaseArray.length === 1) {
                        var finalKnowledgeBaseID = selectedKnowledgeBaseArray[0];
                        var finalQuestionId = getQuestionIdfromSelectedRow(finalKnowledgeBaseID);
                        if (finalQuestionId !== 0) {
                            fetchContentfromController(finalQuestionId);
                        } else if (finalQuestionId === 0) {
                            alert("Please select single question to modify.");
                            $('#user_options').prop('selectedIndex', 0);
                            selectedKnowledgeBaseArray = [];
                            selectedKnowledgeBaseArray.length = 0;
                            $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                            $("#selectALL").removeAttr('checked');
                            return false;
                        }
                    }
                }

                function showAnswerContentInPopUp(id) {
                    fetchContentfromController(id);
                }
                /*Common function to view question content below the following events
                 * 1)User selects option to modify from select bar.
                 * 2)User clicks more hyperlink 
                 * Both events allows user to modify the question content.*/
                function fetchContentfromController(selectedQuestionID) {
                    if (selectedQuestionID !== 0) {
                        var params = {selectedQuestionID: selectedQuestionID};
                        jQuery.getJSON("/knowledge-base.html?modifyQuestion=modifyQuestion", params,
                                function (data) {
                                    var selectedToolID = [];
                                    selectedKnowledgeBaseArray = [];
                                    selectedKnowledgeBaseArray.length = 0;
                                    var selecteQuestionID = data[0];
                                    var selectedQuestion = data[1];
                                    var selectedAnswer = data[2];
                                    selectedToolID = data[3];
                                    var selectedType = data[4];
                                    var selectedActive = data[5];

                                    $("#multipleTooldID").prop("selectedIndex", -1);
                                    makeDefaultSelection(selectedToolID);

                                    $("#questionID").val(selecteQuestionID);
                                    $("#question").val(selectedQuestion);
                                    //alert("selectedAnswer"+selectedAnswer);
                                    $("#answer").Editor("setText", selectedAnswer);
                                    if (selectedType === 1) {
                                        $("#questionType1").prop("checked", true);
                                        $("#questionType2").prop("checked", false);
                                    } else if (selectedType === 2) {
                                        $("#questionType1").prop("checked", false);
                                        $("#questionType2").prop("checked", true);
                                    }

                                    if (selectedActive === 1) {
                                        $("#activeType1").prop("checked", true);
                                        $("#activeType2").prop("checked", false);
                                    } else if (selectedActive === 2) {
                                        $("#activeType1").prop("checked", false);
                                        $("#activeType2").prop("checked", true);
                                    }

                                    $("#modifyDiv").show();
                                    $("#submitDiv").hide();
                                    showQuestionPopUP();

                                    $("#selectALL").removeAttr('checked');
                                    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                                });
                    } else if (selectedQuestionID === 0) {
                        alert("Please select single question to modify.");
                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                        return false;
                    }
                }/*End of fetchContentFromController Function*/
                function makeDefaultSelection(selectedToolID) {
                    var j = 0;
                    var select = document.getElementById('multipleTooldID');
                    console.log("selectedToolID" + selectedToolID);
                    for (var j = 0; j <= selectedToolID.length - 1; j++) {
                        for (var i = 0, l = select.options.length, o; i < l; i++)
                        {
                            o = select.options[i];
                            var temp = parseInt(o.value)
                            if (selectedToolID[j] === temp)
                            {
                                o.selected = true;
                            }
                        }
                    }
                }

                function updateQuestion(tempValidateAnswer) {
                    //$("#answer").val(modifyContAnswer);
                    $("#answer").val(tempValidateAnswer.trim());
                    $('#userSelectedActive').prop('selectedIndex', 0);
                    $("#userSelectedType").prop('selectedIndex', 0);
                    $('#user_options').prop('selectedIndex', 0);
                    $("#userSelectedToolID").prop('selectedIndex', 0);
                    var f = $("#knowledgeBase");
                    f.attr('action', "/knowledge-base.html?updateKnowledgeCont=updateKnowledgeCont");
                    f.attr('method', 'POST');
                    f.submit();
                    $("#selectALL").removeAttr('checked');
                    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                }

                function sortingValues(param) {
                    var name = param.getAttribute("id");
                    var eClass = param.getAttribute("class");
                    if (eClass.indexOf('sortedUp') !== -1) {
                        $(".sortable").removeClass('sortedDown');
                        $(".sortable").removeClass('sortedUp');
                        $("#" + name).addClass('sortedDown');
                        $(".sortable").removeClass('sortedUp');
                        sortField(name, 'down');

                    } else {
                        $(".sortable").removeClass('sortedDown');
                        $(".sortable").removeClass('sortedUp');
                        $("#" + name).addClass('sortedUp');
                        $(".sortable").removeClass('sortedDown');
                        sortField(name, 'up');
                    }
                }

                function sortField(field, way) {
                    var params = {sort: field, way: way};
                    jQuery.getJSON("/knowledge-base.html?filter = filterData", params,
                            function (data) {
                                buildSortedData(data);
                            });

                }
                function getQuestionList() {
                    //var params={type: add};
                    jQuery.getJSON("/knowledge-base.html?questionList=questionList",
                            function (data) {
                                buildSortedData(data);
                            });
                }

                function storeQuestionArray(id) {
                    var status = document.getElementById("knowledgeQuestion" + id + "");
                    if (status !== null) {
                        var isSelected = status.checked;
                        if (isSelected) {
                            var index = selectedKnowledgeBaseArray.indexOf(id);
                            if (index === -1) {
                                selectedKnowledgeBaseArray.push(id);
                            }
                        } else {
                            var unCheckedindex = selectedKnowledgeBaseArray.indexOf(id);
                            if (unCheckedindex > -1) {
                                selectedKnowledgeBaseArray.splice(unCheckedindex, 1);
                            }
                        }
                    }
                    console.log("Final selectedKnowledgeBaseArray conatins " + selectedKnowledgeBaseArray + " and length" + storeQuestionArray.length);
                }

                function getQuestionIdfromSelectedRow(knowledgeID) {
                    var question_Status = document.getElementById("knowledgeQuestion" + knowledgeID + "");
                    if (question_Status !== null) {
                        var selectedType = question_Status.checked;
                        if (selectedType) {
                            var questionID = question_Status.value;
                            return questionID;
                        } else {
                            return 0;
                        }
                    }
                }
                function buildSortedData(data) {
                    var tableRows = "<tr>" + $('#questionsTable tr:nth-child(1)').html() + "</tr>";
                    //alert("length is "+data[0].length);
                    if (data[0].length > 0) {
                        $("#emptyData").hide();
                        for (var i = 0; i < data[0].length; i++) {

                            var tableRow = "<tr>";
                            tableRow += '<td class="userSelectWrapper">' + '<input type="checkbox" id="knowledgeQuestion' + data[0][i].questionID + '" onclick="storeQuestionArray(' + data[0][i].questionID + ',this)" name="selected_question" value=' + data[0][i].questionID + '>' + '</td>';
                            tableRow += "<td class='questionResult'><div id='resultQuestion" + data[0][i].questionID + "'>" + data[0][i].question + "</div></td>";
                            tableRow += '<td class="answerResult"><div>' + data[0][i].shortAnswer + '</div><div class="showMoreWrap"><a id="inDetailQuestion' + data[0][i].questionID + '" onclick="showAnswerContentInPopUp(' + data[0][i].questionID + ');" href="javascript:void(0);">More</a></div></td>';
                            if (data[0][i].questionType === 1) {
                                tableRow += "<td class='questinTypeResult'>" + "Paid" + "</td>";
                            } else if (data[0][i].questionType === 2) {
                                tableRow += "<td class='questinTypeResult'>" + "UnPaid" + "</td>";
                            } else {
                                tableRow += "<td class='questinTypeResult'>" + "UnPaid" + "</td>";
                            }
                            if (data[0][i].activeType === 1) {
                                tableRow += "<td class='questinActiveResult'>" + "Active" + "</td>";
                            } else if (data[0][i].activeType === 2) {
                                tableRow += "<td class='questinActiveResult'>" + "Inactive" + "</td>";
                            } else {
                                tableRow += "<td class='questinActiveResult'>" + "Inactive" + "</td>";
                            }
                            if (data[0][i].last_Updated === null) {
                                tableRow += "<td class='lastUpdateResult'><label>-</label></td>";
                            } else if (data[0][i].last_Updated !== null) {
                                tableRow += "<td class='lastUpdateResult'>" + data[0][i].last_Updated + "</td></tr>";
                            } else {
                                tableRow += "<td class='lastUpdateResult'>" + data[0][i].last_Updated + "</td></tr>";
                            }

                            tableRows += tableRow;
                        }

                        $('#user_options').prop('selectedIndex', 0);
                        selectedKnowledgeBaseArray = [];
                        selectedKnowledgeBaseArray.length = 0;
                        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                        $("#selectALL").removeAttr('checked');
                    } else {
                        $("#emptyData").show();
                    }
                    $("#questionsTable").html(tableRows);

                }

                /*To check/Uncheck all checkBoxs*/
                function selectALLQuestion() {
                    $("#selectALL").change(function () {
                        $("input:checkbox").prop('checked', $(this).prop("checked"));
                    });
                }
                function closePopUP() {
                    $("#dnld-ok-out-box").hide();
                    $("#destOkUrlPopUp").hide();
                    $('#dnld-pop-out-box').hide();
                    $('#downloadpopup').hide();
                    $('#user_options').prop('selectedIndex', 0);
                    selectedKnowledgeBaseArray = [];
                    selectedKnowledgeBaseArray.length = 0;
                    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
                    $("#selectALL").removeAttr('checked');
                }
                function naviToOtherTabs(menuOption) {
                    var menuParams = {menuOption: menuOption};
                    $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                        if (data !== null) {
                            parent.window.location = data;
                        }
                    });
                }
            </script>
        </head>
        <body onload="">
            <div class="responsive-wrapper">

                <div class="middle-wrapper">
                    <div class="" id="content">
                        <div id="knowledge-wrap">
                            <form:form commandName="knowledgeBase" method="POST">

                                <div id="tabs-icons">
                                    <ul>
                                        <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                              class="ui-state-default ui-corner-top " 
                                                              onmouseover="this.style.cursor = 'pointer'" 
                                                              onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                        <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                            class="ui-state-default ui-corner-top"
                                                            onmouseover="this.style.cursor = 'pointer'"
                                                            onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                        <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                             onclick="naviToOtherTabs('autoMail')" 
                                                             class="ui-state-default ui-corner-top"
                                                             onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                        <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                            onclick="naviToOtherTabs('settings')" 
                                                            class="ui-state-default ui-corner-top"
                                                            onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                        <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  onclick="naviToOtherTabs('knowledgeBase')" 
                                                                  class="ui-state-default ui-corner-top ui-state-active"
                                                                  onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                        <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  onclick="naviToOtherTabs('expertAnswer')" 
                                                                  class="ui-state-default ui-corner-top"
                                                                  onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                        <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                               onclick="naviToOtherTabs('expertUser')"
                                                               class="ui-state-default ui-corner-top ui-tabs-selected " 
                                                               onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                        <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top" 
                                                               onclick="naviToOtherTabs('ateReports')" 
                                                               onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                        <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                                   class="ui-state-default ui-corner-top"
                                                                   onmouseover="this.style.cursor = 'pointer'"
                                                                   onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>

                                    </ul>
                                    <div id="resultTable" >
                                        <div class="knowledgeContnet">
                                            <div class="userSelections">
                                                <div class="filter_Operations">
                                                    <h1>Filtering Options:</h1>    
                                                </div><!-- End of filter_Operations div -->
                                                <div class="questions_Operations">
                                                    <div class="userOptions">
                                                        <div class="toolsSelection">
                                                            <span class="tools_List">
                                                                <label>Tools:</label>
                                                            </span>
                                                            <span class="tools_Result">
                                                                <form:select path="userSelectedToolID">
                                                                    <form:option value="0" label="All"/>
                                                                    <form:options items="${lxrMarketplaceToolsList}" itemValue="toolId" itemLabel="toolName" />
                                                                </form:select>
                                                            </span>
                                                        </div>
                                                        <div class="typeSelection">
                                                            <span class="tools_List">
                                                                <label>Paid/UnPaid:</label>
                                                            </span>
                                                            <span class="tools_Result">
                                                                <select  id="userSelectedType">
                                                                    <option value="0">All</option>
                                                                    <option value="1">Paid</option>
                                                                    <option value="2">Unpaid</option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                        <div class="activeSelection">
                                                            <span class="tools_List">
                                                                Active/Inactive:
                                                            </span>
                                                            <span class="tools_Result">
                                                                <select  id="userSelectedActive">
                                                                    <option value="0">All</option>
                                                                    <option value="1">Active</option>
                                                                    <option value="2">Inactive</option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="actionOperations">
                                                        <span class="tools_List">
                                                            Actions:
                                                        </span>
                                                        <span class="selectTask">
                                                            <select id="user_options">
                                                                <option value="select">--Select--</option>
                                                                <option value="addOption">Add</option>
                                                                <option value="modifyOption">Modify</option>
                                                                <option value="deleteOption">Delete</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div><!-- End of questions_Operations div -->
                                                <div id="dnld-ok-out-box" style="display: none;">
                                                    <div id="destOkUrlPopUp" style="display: none;">
                                                        <div id="downloadpopup" class="popup popupPortlet ui-corner-all">
                                                            <div id="downloadpopupmsg">
                                                                <div class='content'>
                                                                    <!--                                                                    <div class="close_wrapper">                                                                    
                                                                                                                                                    <div class="">
                                                                                                                                                        <img src="../images/close-icon.png" id="closePopup" alt="popupclose" >
                                                                                                                                                    </div>
                                                                                                                                        </div>-->
                                                                    <div class="popUpWrapper color2 Bold">Add/Modify Question:</div>
                                                                    <div>
                                                                        <div class="popupTable">
                                                                            <div class="table-optins" style="width: 100%;">
                                                                                <div class="select-tools">
                                                                                    <div class="ques-content">
                                                                                        <div class="question_Type">
                                                                                            Tools:
                                                                                        </div>
                                                                                        <div id="multiple_Tools_Selection">
                                                                                            <form:select  path="multipleTooldID"  multiple= "true" >
                                                                                                <form:options items="${lxrMarketplaceToolsList}" itemValue="toolId" itemLabel="toolName" />
                                                                                            </form:select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ques-content">
                                                                                    <div class="question_Type type_popUp_Wrapper">
                                                                                        Paid/UnPaid:
                                                                                    </div>
                                                                                    <div class="paid_Radio">
                                                                                        <form:radiobutton path="questionType" value="1" label="Paid" />
                                                                                        <form:radiobutton path="questionType" value="2" label="UnPaid" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ques-content">
                                                                                    <div class="question_Type" id="statusType">
                                                                                        Active/Inactive:
                                                                                    </div>
                                                                                    <div class="active_Radio_Wrapper paid_Radio">
                                                                                        <form:radiobutton path="activeType" value="1" label="Active" />
                                                                                        <form:radiobutton path="activeType" value="2" label="Inactive"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="width: 100%;float:left;">
                                                                                <form:hidden path="questionID" />
                                                                                <span class="knowledge_Question">
                                                                                    Question:
                                                                                </span>
                                                                                <span class="question_Result">
                                                                                    <form:textarea path="question" />
                                                                                </span>
                                                                            </div>
                                                                            <div style="width: 100%;float:left;">
                                                                                <span class="knowledge_Answer">
                                                                                    Answer:   
                                                                                </span>
                                                                                <span class="answer_Result">
                                                                                    <form:textarea path="answer"/>    
                                                                                </span>
                                                                            </div>
                                                                            <div class="submit-button">
                                                                                <div id="button-wrap">
                                                                                    <div class="submitWrap">
                                                                                        <div id="modifyDiv">
                                                                                            <input type="submit" id="modifyQuestion" value="SUBMIT" >
                                                                                        </div>               
                                                                                        <div id="submitDiv">
                                                                                            <input type="submit" id="submitQuestion" value="SUBMIT" >
                                                                                        </div>    
                                                                                        <div id="cancelDiv">
                                                                                            <input type="button" id="cancelQuestion" value="CANCEL" >
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div><!-- End of user content div -->
                                                            </div><!-- End of user downloadpopupmsg div -->
                                                        </div><!-- End of user downloadpopup div -->
                                                    </div><!-- End of user destOkUrlPopUp div -->
                                                </div><!-- End of user dnld-ok-out-box div -->
                                            </div><!-- End of user selections div -->
                                            <div class="knowledgeBaseResultContnet">

                                                <div class="knowledgeBaseResult"> 
                                                    <table id="questionsTable">
                                                        <thead>
                                                            <tr>
                                                                <th><span class="selectALLWrap"> <input type='checkbox' id='selectALL' onclick='selectALLQuestion()'></span><span>Select</span></th>
                                                                <th class="sortable" id='sortQuestion' onclick='sortingValues(this);'>Question</th>
                                                                <th class="sortable" id='sortAnswer' onclick='sortingValues(this);'>Suggested Answer</th>
                                                                <th class="sortable" id='sortType' onclick='sortingValues(this);'>Paid/Unpaid</th>
                                                                <th class="sortable" id='sortActive' onclick='sortingValues(this);'>Active/Inactive</th>
                                                                <th class="sortable" id='sortLastUpdate' onclick='sortingValues(this);'>Last Updated</th>
                                                            </tr>
                                                        </thead>
                                                        <c:set var="knowledgeQuestionsList" value="${knowledgeBaseList}"/>

                                                        <tbody style="font-size: 12px;font-family: 'HelveticaNeue-Medium';">
                                                            <c:forEach items="${knowledgeBaseList}" var="knowledgeQuestionsIte">
                                                                <tr>
                                                                    <td><div class="checkBoxSel"><form:checkbox path="selected_question" value="${knowledgeQuestionsIte.questionID}"/></div></td>
                                                                    <td>${knowledgeQuestionsIte.question}</td>
                                                                    <td>${knowledgeQuestionsIte.answer}</td>
                                                                    <c:choose>
                                                                        <c:when test="${knowledgeQuestionsIte.questionType  eq true}">
                                                                            <td>Paid</td>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <td>UnPaid</td> 
                                                                        </c:otherwise>        
                                                                    </c:choose>
                                                                    <c:choose>
                                                                        <c:when test="${knowledgeQuestionsIte.last_Updated != null}">
                                                                            <td>${knowledgeQuestionsIte.last_Updated}</td>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <td><label>-</label></td> 
                                                                        </c:otherwise>        
                                                                    </c:choose>

                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                    <div id="emptyData"><p>No data available</p></div>
                                                    <div id="statusMessage"><p></p></div>
                                                </div><!-- End of knowledgeBaseResult div -->
                                            </div><!-- End of knowledgeContnet div -->
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>

                    </div>
                </div>
                <%@ include file="/WEB-INF/view/footer.jsp"%>
        </body>
    </html>
