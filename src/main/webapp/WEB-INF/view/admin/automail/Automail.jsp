<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page import="lxr.marketplace.admin.automail.Automail"%>
<%@ page
    import="lxr.marketplace.user.Login, java.util.Calendar"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    <html>
        <head>
            <title>LXRMarketplace | Automail</title>
            <meta name="keywords" content="Admin Reports" />
            <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
            <META HTTP-EQUIV="Expires" CONTENT="-1">
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
            <%@ include file="/WEB-INF/view/include.jsp"%>
            <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
            <%@ include file="/WEB-INF/view/commonImport.jsp"%>
            <%@ include file="/WEB-INF/view/header.jsp"%>
            <link rel="stylesheet"
                  href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" media="all" type="text/css"
                  href="/scripts/jquery/jquery-ui-1.8.16.custom.css" />
            <!-- link rel="stylesheet" type="text/css" href="/css/0/core.css"-->
            <link rel="stylesheet" type="text/css" href="/css/0/jquery.cleditor.css">
            <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" href="css/lxrm-core.css">
            <link rel="stylesheet" href="css/admin.css">
            <script type="text/javascript"
            src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
            <script type="text/javascript" src="/scripts/jquery/jquery-1.6.2.js"></script>
            <!--   text editor -->
            <script type="text/javascript" src="/scripts/ui/jquery.cleditor.min.js"></script>
            <script type="text/javascript"
            src="/scripts/ui/jquery.cleditor.advancedtable.min.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
            <script type="text/javascript" src="scripts/fileuploader.js"></script>
            <!--  used for datepicker  -->
            <script type="text/javascript" src="/scripts/core.js"></script>
            <%---  imports used for datepiker --%>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.datepicker.js"></script>
            <%--  imports used for timepicker  --%>
            <script type="text/javascript"
            src="/scripts/ui/jquery-ui-1.8.18.custom.min.js"></script>
            <script type="text/javascript"
            src="/scripts/ui/jquery-ui-timepicker-addon.js"></script>
            <script type="text/javascript"
            src="/scripts/ui/jquery-ui-sliderAccess.js"></script>
            <%  HttpSession sessionA = request.getSession();
                Login user = (Login) sessionA.getAttribute("user");
                String existingTemplate = (String) sessionA.getAttribute("existingTemplate");
                Object ul = sessionA.getAttribute("userLoggedIn");
                boolean userLoggedIn = false;
                ExpertUser expertUser = null;
                if (ul != null) {
                    userLoggedIn = (Boolean) ul;
                }
                if (userLoggedIn) {
                    if (sessionA.getAttribute("expertUser") != null) {
                        expertUser = (ExpertUser) sessionA.getAttribute("expertUser");
                        pageContext.setAttribute("expertUser", expertUser);
                    }
                }
            %>


            <script type="text/javascript">
                <%
                    out.println("var userLoggedIn=" + userLoggedIn + ";");
                %>
                var isEdit = true;
                var isSaved = true;
                var countfile = 0;
                var openingPreviousTemplate = false;
                $(document).ready(function (event) {
                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    var role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>


                    var sendOnToday = document.getElementsByName('emailTemplate.sendOnToday');
                    var categories = document.getElementsByName('emailTemplate.category');
                    if (categories[3].checked) {
                        $("#hideOption").hide();
                        $("#browseBtn").hide();
                        $("#upNote").hide();
                    } else {
                        $("#hideOption").show();
                        $("#browseBtn").show();
                        $("#upNote").show();
                    }

                    if (sendOnToday[0].checked) {
                        document.getElementById('daygap').disabled = "disabled";
                        $("#daygap").val(1);
                        $("#fromDate").val($.trim($("#toDate").val()));
                    } else {
                        document.getElementById('daygap').disabled = "";
                    }
                    $("#testmailpopup").hide();
                    $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    $("#tabs-icons").tabs();
                    $('.example-container > pre').each(function (i) {
                        eval($(this).text());
                    });
                    $("#input").cleditor({width: 850, height: 450, useCSS: true})[0].focus();
                    $('#submitColumn').bind('click', function () {
                        $("#testmailpopup").hide();
                        $("#testmail").val("");
                        return false;
                    });
                    var sendMailOptions = document.getElementsByName('emailTemplate.sendMailOption');

                    if (sendMailOptions[2].checked) {
                        $("#browseBtn").show();
                        $("#upNote").show();
                    } else {
                        $("#browseBtn").hide();
                        $("#upNote").hide();
                    }
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    var logo = document.getElementById("logo");
                    logo.href = "/dashboard.html";

                    $("a.clickable").click(function (event) {
                        event.preventDefault();
                        var f = $("#automail");
                        f.attr('action', "automail.html?showtemplate='id'");
                        f.submit();
                    });
                    var a = document.getElementsByName('emailTemplate.category');
                    if ($("#tempName").val() !== "") {
                        var test = "${testmailsent}";
                        if (test !== "") {
                            if (test === "existing") {
                                isEdit = true;
                                openingPreviousTemplate = true;
                            } else {
                                isEdit = false;
                            }
                        } else {
                            openingPreviousTemplate = true;
                        }
                        showdiv();	// to show particular template in edit mode 
                        isSaved = false;
                        for (var i = 0; i < a.length; i++) {
                            if (a[i].checked) {
                                category("catno" + (i + 1));
                            }
                        }
                        var a = document.getElementsByName("timeOfDay");
                        if (a[0].value !== "") {
                            var b = (a[0].value).split(":");
                            $('#example3').timepicker({
                                stepMinute: 30,
                                hour: b[0],
                                minute: b[1]
                            });
                            $('#example3').val(b[0] + ":" + b[1]);//alert( $('#example3').val() +""+b[0]+" "+b[1]);
                        } else {
                            $('#example3').timepicker({
                                stepMinute: 30,
                                hour: '00',
                                minute: '00'
                            });
                            $('#example3').val("00:00");
                            // alert( $('#example3').val() +""+b[0]+" "+b[1]);
                        }
                        var divId = document.getElementById('browse1');
                        uploadListOfUsersFile(event);
                        showPreviousUserAttachments();
                        createUploader(event, divId);
                        showPreviousAttachments();

                        // 			    printPrevFileName();
                    } else if (a[0].checked) {
                        showdiv();
                        isSaved = false;
                        isEdit = false;
                        var fd = $("#fromDate");
                        var td = $("#toDate");
                        var categories = document.getElementsByName('emailTemplate.category');
                        categories[0].disabled = false;
                        categories[1].disabled = false;
                        categories[2].disabled = false;
                        categories[3].disabled = false;
                        var divId = document.getElementById('browse1');
                        category("catno1");
                        $('#example3').timepicker({
                            stepMinute: 30,
                            hour: '00',
                            minute: '00'
                        });
                        $('#example3').val("00:00");
                        fd.val("");
                        td.val("");
                        createUploader(event, divId);
                        uploadListOfUsersFile(event);
                    }

                <%  if (existingTemplate != null) { %>
                    $('#existingTemplate').show();
                    var categories = document.getElementsByName('emailTemplate.category');
                    isEdit = false;
                    if (!isSaved) {
                        categories[0].disabled = false;
                        categories[1].disabled = false;
                        categories[2].disabled = false;
                        categories[3].disabled = false;
                    }
                    var a = document.getElementsByName("timeOfDay");
                    if (a[0].value === "undefined" || a[0].value === "") {
                        $('#example3').val("00:00");
                    } else {
                        var b = (a[0].value).split(":");
                        $('#example3').timepicker({
                            stepMinute: 30,
                            hour: b[0],
                            minute: b[1]
                        });
                        $('#example3').val(b[0] + ":" + b[1]);
                    }
                <%}
                    if (user != null) {%>
                    $("#loginSuccess").show();
                    //azaxCalls();
                <% } else { %>
                    $("#loginSuccess").hide();
                <% } %>
                    $("#daygap").keypress('click', function (e) {
                        return isNumberKey(e);
                    });

                });
                <%
                    out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                %>
                $(function () {
                    $("#fromDate, #toDate").datepicker({
                        showOn: "both",
                        buttonImage: "../css/0/images/calendar.jpg",
                        buttonImageOnly: true,
                        minDate: new Date(serverTime),
                        dateFormat: 'dd-M-yy',
                        changeMonth: true,
                        changeYear: true,
                        constrainInput: true
                    });
                });

                function showUploadBut(val) {
                    if (val === 3) {
                        $("#browseBtn").show();
                        $("#upNote").show();
                        $("#userListFile").show();
                        $("#attachedFiles").show();
                    } else {
                        $("#userListFile").hide();
                        $("#attachedFiles").hide();
                        $("#browseBtn").hide();
                        $("#upNote").hide();
                    }
                }

                function naviToOtherTabs(menuOption) {
                    var menuParams = {menuOption: menuOption};
                    $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                        if (data !== null) {
                            parent.window.location = data;
                        }
                    });
                }

                function saveTemplate() {
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    // 		alert("check val "+$("#sendOnTdy").is(':checked'));
                    if (!validateFields()) {
                        return false;
                    }
                    var f = $("#automail");
                    if (isEdit) {
                        f.attr('action', "automail.html?saveTemplate=update&tod=" + $('#example3').val());
                    } else {
                        f.attr('action', "automail.html?saveTemplate=savenew&tod=" + $('#example3').val());
                    }
                    f.submit();
                }

                function saveAndSendmail() {
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    $("#SuccessMsg4").html("");

                    if (!validateFields()) {
                        return false;
                    }
                    var r = true;
                    r = confirm("Are you sure you want to send this mail now? ");
                    if (r === false) {
                        return false;
                    } else {
                        var f = $("#automail");
                        if (isEdit) {
                            f.attr('action', "automail.html?saveTemplate=update&sendMail=true&tod=" + $('#example3').val());
                            var sendMailOptions = document.getElementsByName('emailTemplate.sendMailOption');
                            if (!sendMailOptions[2].checked) {
                                $("#userListFile").val("");
                                document.getElementById("attachedFiles").innerHTML = "";
                            }
                        } else {
                            // 				$("#SuccessMsg4").html("");
                            f.attr('action', "automail.html?saveTemplate=savenew&sendMail=true&tod=" + $('#example3').val());
                        }
                        f.submit();
                    }
                }
                function sendTestmail() {
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    if (!validateFields()) {
                        return false;
                    }
                    $("#testmailpopup").show();
                }
                function Sendmail() {
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    var email = $.trim($("#testmail").val());
                    if (email === "") {
                        alert("Please enter mail Id");
                        return false;
                    }
                    if (!isValidMail(email)) {
                        alert("Please enter a valid email address.");
                        $("#testmail").val("");
                        $("#testmail").focus();
                        return false;
                    }
                    var existing;
                    if (openingPreviousTemplate) {
                        existing = "existing";
                    } else {
                        existing = "";
                    }
                    var f = $("#automail");
                    f.attr('action', "automail.html?sendTestmail=sendTestmail&tod=" + $('#example3').val() + "&existing=" + existing);
                    f.submit();
                }
                function validateFields() {
                    $("#SuccessMsg1").html("");
                    $("#SuccessMsg2").html("");
                    $("#SuccessMsg3").html("");
                    var tn = $.trim($("#tempName").val());
                    var e = $.trim($("#ent").val());
                    var dg = $.trim($("#daygap").val());
                    var t = $.trim($("#example3").val());
                    var fd = $.trim($("#fromDate").val());
                    var td = $.trim($("#toDate").val());
                    var s = $.trim($("#subj").val());
                    var b = $.trim($("#input").val());
                    var uploadedFile = $.trim($("#userListFile").val());
                    var logo = $("#logoSel").val();
                    var url = $.trim($("#logoUrl").val());
                    var personalAddress = $.trim($("#personalAddr").val());
                    // 		 alert(body);
                    // 		 var x=document.getElementsByTagName("iframe");
                    // 		 alert(x.value);
                    // 		 var b=$("#wysiwyg").val();
                    option = $("#sel").val();

                    var fromAddressSel = $("#fromAddressSel").val();

                    var categories = document.getElementsByName('emailTemplate.category');
                    var sendMailOptions = document.getElementsByName('emailTemplate.sendMailOption');
                    var sendOnToday = document.getElementsByName('emailTemplate.sendOnToday');
                    var sendMailVal = false;
                    if (sendMailOptions[0].checked || sendMailOptions[1].checked || sendMailOptions[2].checked) {
                        sendMailVal = true;
                    }
                    if (categories[0].checked)
                    {
                        if (tn === "") {
                            alert("Please Enter Template Name.");
                            $("#tempName").focus();
                            return false;
                        } else if (option === 0) {
                            alert("Please select any event.");
                            $("#sel").focus();
                            return false;
                        } else if (s === "") {
                            alert("Please Enter Subject. ");
                            $("#subj").focus();
                            return false;
                        } else if (logo === 0) {
                            alert("Please select Logo option");
                            $("#logoSel").focus();
                            return false;
                        } else if ((logo === 1 || logo === 2) && (url === null || url === "")) {
                            alert("Please enter URL");
                            $("#logoUrl").focus();
                            return false;
                        } else if (fromAddressSel === 0) {
                            alert("Please select from address.");
                            $("#fromAddressSel").focus();
                            return false;
                        } else if (b === "") {
                            alert("Please Enter Body content.");
                            $("#wysiwyg").focus();
                            return false;
                        }
                    } else if (categories[2].checked) {
                        $("#hideOption").show();
                        $("#browseBtn").show();
                        $("#upNote").show();
                        if (tn === "") {
                            alert("Please Enter Template Name.");
                            $("#tempName").focus();
                            return false;
                        } else if (s === "") {
                            alert("Please Enter Subject.");
                            $("#subj").focus();
                            return false;
                        } else if (logo === 0) {
                            alert("Please select Logo option");
                            $("#logoSel").focus();
                            return false;
                        } else if ((logo === 1 || logo === 2) && (url === null || url === "")) {
                            alert("Please enter URL");
                            $("#logoUrl").focus();
                            return false;
                        } else if (fromAddressSel === 0) {
                            alert("Please select from address.");
                            $("#fromAddressSel").focus();
                            return false;
                        } else if (b === "") {
                            alert("Please Enter Body content.");
                            $("#wysiwyg").focus();
                            return false;
                        }

                    } else if (categories[3].checked)
                    {
                        $("#hideOption").hide();
                        $("#browseBtn").hide();
                        $("#upNote").hide();
                        if (sendOnToday[0].checked) {
                            $("#toDate").val(fd);
                            td = $.trim($("#toDate").val());
                        }
                        if (tn === "") {
                            alert("Please Enter Template Name.");
                            $("#tempName").focus();
                            return false;
                        } else if (dg === "") {
                            alert("Please Enter Days gap value.");
                            $("#daygap").focus();
                            return false;
                        } else if ($("#daygap").val() < 1)
                        {
                            alert("Input values should not be less than 1.");
                            $('#daygap').focus();
                            return false;
                        } else if (t === 0 || t === "") {
                            alert("Please select Time of a day value.");
                            $("#example3").focus();
                            return false;
                        } else if (fd === "") {
                            alert("Please select Start Date value.");
                            $("#fromDate").focus();
                            return false;
                        } else if (td === "") {
                            alert("Please select End Date value.");
                            $("#toDate").focus();
                            return false;
                        } else if (s === "") {
                            alert("Please Enter Subject.");
                            $("#subj").focus();
                            return false;
                        } else if (logo === 0) {
                            alert("Please select Logo option");
                            $("#logoSel").focus();
                            return false;
                        } else if ((logo === 1 || logo === 2) && (url === null || url === "")) {
                            alert("Please enter URL");
                            $("#logoUrl").focus();
                            return false;
                        } else if (fromAddressSel === 0) {
                            alert("Please select from address.");
                            $("#fromAddressSel").focus();
                            return false;
                        } else if (b === "") {
                            alert("Please Enter Body content.");
                            $("#wysiwyg").focus();
                            return false;
                        }
                        fd = $("#fromDate").datepicker("getDate");
                        td = $("#toDate").datepicker("getDate");

                        if (fd === null) {
                            alert("Please select the Start Date.");
                            $("#fromDate").focus();
                            return false;
                        } else if (td === null) {
                            alert("Please select the End Date.");
                            $("#toDate").focus();
                            return false;
                        } else if (fd > td) {
                            alert("Start Date is less than End Date, \n please select a valid date range");
                            $("#fromDate").focus();
                            return false;
                        }
                    } else if (categories[1].checked)
                    {
                        if (tn === "") {
                            alert("Please Enter Template Name.");
                            $("#tempName").focus();
                            return false;
                        } else if (option === 0) {
                            alert("Please select any event.");
                            $("#sel").focus();
                            return false;
                        } else if (dg === "") {
                            alert("Please Enter Days gap value.");
                            $("#daygap").focus();
                            return false;
                        } else if ($("#daygap").val() < 1)
                        {
                            alert("Input values should not be less than 1.");
                            $('#daygap').focus();
                            return false;
                        } else if (t === 0 || t === "") {
                            alert("Please select Time of a day value.");
                            $("#example3").focus();
                            return false;
                        } else if (s === "") {
                            alert("Please Enter Subject.");
                            $("#subj").focus();
                            return false;
                        } else if (logo === 0) {
                            alert("Please select Logo option");
                            $("#logoSel").focus();
                            return false;
                        } else if ((logo === 1 || logo === 2) && (url === null || url === "")) {
                            alert("Please enter URL");
                            $("#logoUrl").focus();
                            return false;
                        } else if (fromAddressSel === 0) {
                            alert("Please select from address.");
                            $("#fromAddressSel").focus();
                            return false;
                        } else if (b === "") {
                            alert("Please Enter Body content.");
                            $("#wysiwyg").focus();
                            return false;
                        }
                    }
                    if (!sendMailVal) {
                        alert("Please select send mail option");
                        return false;
                    }
                    if (sendMailOptions[2].checked) {
                        if ($.trim($("#userListFile").val()) === null || $.trim($("#userListFile").val()) === "") {
                            alert("please upload a file");
                            return false;
                        }
                    }
                    if (personalAddress !== null && personalAddress !== "") {
                        if (personalAddress.indexOf("<user>") < 0) {
                            alert("Personal Address not contain <user> String");
                            return false;
                        }
                    }
                    return true;
                }

                function showdiv() {
                    var state = document.getElementById("templateView");
                    if (state.style.display === 'none') {
                        document.getElementById("templateView").style.display = 'block';
                        window.location.hash = '#templateView';
                        disabRemField();

                    }
                }
                function apply() {
                    var tmp = document.getElementById("formrowTab");
                    var trs = tmp.getElementsByTagName('tr');
                    var trlength = trs.length - 1;
                    var cond = false;
                    for (var i = 0; i < trlength; i++) {
                        var statusElement = document.getElementsByName("formRows[" + i + "].status");
                        var Status = statusElement[0].checked;
                        if (Status) {
                            cond = true;
                        }
                    }
                    if (cond) {
                        var sendConfirm = true;
                        sendConfirm = confirm("If any on demand template(s) selected they will be sent immediately ");
                        if (sendConfirm) {
                            var f = $("#automail");
                            f.attr('action', "automail.html?apply='apply'");
                            f.submit();
                        } else {
                            return false;
                        }
                    }
                }
                function deleteTemplates(event) {
                    event.preventDefault();
                    var tmp = document.getElementById("formrowTab");
                    var trs = tmp.getElementsByTagName('tr');
                    var trlength = trs.length - 1;
                    var temp = false;
                    for (var i = 0; i < trlength; i++) {
                        //alert("form rows "+i);
                        var delElement = document.getElementsByName("formRows[" + i + "].deleted");
                        var delStat = delElement[0].checked;
                        if (delStat) {
                            temp = true;

                        }
                    }

                    if (temp === false) {
                        alert("Please select Template[s] to delete.");
                        return false;
                    } else {
                        var deleteConfirm = true;
                        deleteConfirm = confirm("Do you want to delete the checked templates? ");
                        if (deleteConfirm === false) {
                            return false;
                        } else {
                            var f = $("#automail");
                            f.attr('action', "automail.html?delete='delete'");
                            f.attr('method', "POST");
                            f.submit();
                        }
                    }
                }



                function isNumberKey(e) {
                    var browserName = navigator.appName;
                    if (browserName === "Microsoft Internet Explorer") {
                        isIE = true;
                    } else {
                        isIE = false;
                    }
                    keyEntry = !(isIE) ? e.which : event.keyCode;
                    if ((keyEntry > 47 && keyEntry < 58) || keyEntry === 8 || keyEntry === 0 || (e.ctrlKey && keyEntry === 118)) {
                        /*if($(".nonZero").val().length<9){
                         return true;	
                         }else{
                         if(keyEntry==8||keyEntry==0) {
                         return true;
                         }else{
                         return false;	
                         }	    		
                         }*/
                        return true;
                    } else {
                        if (browserName === "Microsoft Internet Explorer") {
                            event.returnValue = false;
                        } else {
                            return false;
                        }
                    }
                    return true;
                }

                function category(catId)
                {
                    var daysgap = document.getElementsByName('emailTemplate.daysGap');
                    if (catId === 'catno1')
                    {
                        $('#inst').show();
                        $('#timedays').hide();
                        $('#stdt').hide();
                        $('#sendbutt').hide();
                        $('#savebutt').show();
                        $('#Activation').hide();
                        $("#sel option[value='1']").remove();
                        daysgap[0].value = 0;
                        $("#schCheckBox").hide();

                    } else if (catId === 'catno3')
                    {
                        $("#hideOption").show();
                        $("#browseBtn").show();
                        $("#upNote").show();
                        $('#inst').hide();
                        $('#timedays').hide();
                        $('#stdt').hide();
                        $('#sendbutt').show();
                        $('#savebutt').hide();
                        $("#sel option[value='1']").remove();
                        daysgap[0].value = 0;
                        $("#schCheckBox").hide();
                    } else if (catId === 'catno4')
                    {
                        $("#hideOption").hide();
                        $("#browseBtn").hide();
                        $("#upNote").hide();
                        $('#inst').hide();
                        $('#timedays').show();
                        $('#stdt').show();
                        $('#sendbutt').hide();
                        $('#savebutt').show();
                        $("#sel option[value='1']").remove();
                        // alert(daysgap[0].value);
                        if (daysgap[0].value === 0) {
                            daysgap[0].value = 1;
                            $("#schCheckBox").show();
                        }
                    } else if (catId === 'catno2')
                    {
                        $('#inst').show();
                        $('#Activation').show();
                        if ($("#sel").val() == 1) {
                            $("#sel").append('<option id="Activation" value="1" label="Activation" selected>Activation</option>');
                        } else {
                            $("#sel").append('<option id="Activation" value="1" label="Activation" >Activation</option>');
                        }
                        $('#timedays').show();
                        $('#stdt').hide();
                        $('#sendbutt').hide();
                        $('#savebutt').show();
                        //alert(daysgap[0].value);
                        if (daysgap[0].value === 0) {
                            daysgap[0].value = 1;
                            $("#schCheckBox").hide();
                        }
                    }

                    // setDashboard();	  
                }
                function showTemplateWithId(id, event) {
                    event.preventDefault();
                    isEdit = true;
                    var r = true;
                    if (!isSaved) {
                        r = confirm("Do you want to proceed without saving the Template details? ");
                        if (r === false) {
                            window.location.hash = '#templateView';
                            return false;
                        }
                    }
                    if (r) {
                        var f = $("#automail");
                        f.attr('action', "automail.html?showtemplate=" + id);
                        f.submit();
                    }
                }
                function disabRemField() {
                    var sendOnToday = document.getElementsByName('emailTemplate.sendOnToday');
                    if (sendOnToday[0].checked) {
                        document.getElementById('daygap').disabled = "disabled";
                        $("#toDt").hide();
                        $("#daygap").val(1);
                    } else {
                        document.getElementById('daygap').disabled = "";
                        $("#toDt").show();
                    }
                }

                function showNew() {
                    isEdit = false;
                    var r = true;
                    if (!isSaved) {
                        r = confirm("Do you want to proceed without saving the Template details? ");
                        if (r === false) {
                            window.location.hash = '#templateView';
                            return false;
                        }
                    }
                    if (r) {
                        var f = $("#automail");
                        f.attr('action', "automail.html?showNewTemplate=true");
                        f.submit();
                    }
                }
                function cancelTemplate() {
                    var r = true;
                    if (!isSaved) {
                        r = confirm("Do you want to proceed without saving the Template details? ");
                        if (r === false) {
                            window.location.hash = '#templateView';
                            return false;
                        }
                    }
                    if (r) {
                        var f = $("#automail");
                        f.attr('action', "automail.html?cancel=cancel");
                        f.submit();
                    }
                }
                function removeFile(rmvFileName) {
                    $.ajax({
                        type: "GET",
                        url: "/automail.html?removeFile=" + rmvFileName,
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            var atchmnts = document.getElementById("attachments");
                            while (atchmnts.lastChild) {
                                atchmnts.removeChild(atchmnts.lastChild);
                            }
                            countfile = data.length;
                            if (countfile < 5) {
                                $('#file').css('display', 'inline-block');
                                $('#filedisable').hide();
                            }
                            for (var i = 0; i < countfile; i++) {
                                addRowForattachedFile(data[i]);

                            }
                            //setDashboard();
                        }
                    });
                }

                var maxCount = 5;
                function createUploader(event, divId) {
                    var uploader = new qq.FileUploader({
                        element: divId,
                        action: "automail.html?uploadFile='uploadFile'",
                        multiple: false,
                        sizeLimit: 2097152,
                        onSubmit: function (id, fileName) {
                            countfile++;
                            if (countfile === 5) {
                                $('#file').hide();
                                $('#filedisable').css('display', 'inline-block');
                            }
                            if (countfile > maxCount) {
                                alert("You cannot attach more than 5 files in a mail");
                                return false;
                            }
                        },
                        onProgress: function (id, fileName, loaded, total) {
                            $('.uploadfile').attr('disabled', true);


                        },
                        onComplete: function (id, fileName, responseJSON) {
                            $('.uploadfile').attr('disabled', false);
                            $('#browse1 .qq-upload-list').html("");
                            $('#upFileName').val(fileName);
                            if (responseJSON.length === 1) {
                                countfile--;
                                if (countfile < 5) {
                                    $('#file').css('display', 'inline-block');
                                    $('#filedisable').hide();
                                }
                            } else {
                                addRowForattachedFile(fileName);
                                //setDashboard();
                            }
                        },
                        messages: {
                            typeError: "Please Upload .csv or .tsv file.",
                            //sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
                            sizeError: "Exceeding File Size Limit",
                            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                            emptyError: "{file} is empty, please select files again without it.",
                            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."
                        },
                        showMessage: function (message) {
                            alert(message);
                        }
                    });
                }

                function uploadListOfUsersFile(event) {
                    var uploader = new qq.FileUploader({
                        element: document.getElementById('browseBtn'),
                        action: "automail.html?uploadUserListFile='uploadUserListFile'",
                        multiple: false,
                        sizeLimit: 2097152,
                        allowedExtensions: ['xls'],
                        onSubmit: function (id, fileName) {
                            $('#browseBtn .qq-upload-list').html("");
                            $('#userListFile').val(fileName);
                        },
                        onComplete: function (id, fileName, responseJSON) {
                            $('#browseBtn .qq-upload-list').html("");
                            $('#userListFile').val(fileName);
                            showUserattachedFile(fileName);
                            showUserDetails();
                        },
                        messages: {
                            typeError: "Please Upload xls file.",
                            sizeError: "Exceeding File Size Limit",
                            minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                            emptyError: "{file} is empty, please select files again without it.",
                            onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."
                        },
                        showMessage: function (message) {
                            alert(message);
                        }
                    });

                }

                function printPrevFileName() {
                    var prevFileName = $('#userListFile').val();
                    if (prevFileName !== "" && prevFileName != null) {
                        $('#browseBtn .qq-upload-list').html("" + prevFileName);
                    }
                }

                function addRowForattachedFile(fileName) {
                    var atchmnts = document.getElementById("attachments");
                    var changedFile = fileName.replace(new RegExp("'", 'g'), "\\\\\\'");
                    var atchmnts = document.getElementById("attachments");
                    var lastRow = atchmnts.rows.length;
                    var row = atchmnts.insertRow(lastRow);
                    var cellLeft = row.insertCell(0);
                    var textNode = document.createTextNode(fileName);
                    cellLeft.appendChild(textNode);
                    var cellRight = row.insertCell(1);
                    var remov = document.createElement('input');
                    remov.setAttribute('type', 'button');
                    remov.setAttribute('name', 'Remove');
                    remov.setAttribute('value', 'Remove');
                    remov.setAttribute('color', '#FFF');
                    remov.setAttribute('class', 'remove');
                    remov.onclick = function () {
                        var file = changedFile;
                        removeFile(file);
                    };
                    cellRight.appendChild(remov);
                }

                function showUserattachedFile(fileName) {
                    var atchmnts = document.getElementById("attachedFiles");
                    var atchmnts = document.getElementById("attachedFiles");
                    document.getElementById("attachedFiles").innerHTML = fileName;
                }

                function showUserDetails() {
                    $.post("/automail.html?userDetailsSucessMsg='userDetailsSucessMsg'", function (data, status) {
                        showUserDetailsMsg(data, status);
                    }, "json");
                }

                function showUserDetailsMsg(dataObj, status) {
                    if (dataObj != null && dataObj.length > 0) {
                        $("#SuccessMsg1").html("Total Users in uploaded file: " + dataObj[0]);
                        $("#SuccessMsg2").html("Users Present in our database: " + dataObj[1]);
                        $("#SuccessMsg3").html("Users Not Present in our database: " + dataObj[2]);
                        $("#SuccessMsg4").html("Unsubscribed users count: " + dataObj[3]);
                    } else {
                        $("#SuccessMsg1").html("Total Users in uploaded file: " + 0);
                        $("#SuccessMsg2").html("Users Present in our database: " + 0);
                        $("#SuccessMsg3").html("Users Not Present in our database: " + 0);
                        $("#SuccessMsg4").html("Unsubscribed users count: " + 0);
                    }
                }

                function showPreviousAttachments() {
                    $.ajaxSetup({
                        cache: "false"
                    });
                    $.ajax({
                        type: "POST",
                        url: "/automail.html?getAllFiles='getAllFiles'",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            countfile = data.length;
                            if (countfile === 5) {
                                $('#file').hide();
                                $('#filedisable').css('display', 'inline-block');
                            }
                            for (var i = 0; i < countfile; i++) {
                                addRowForattachedFile(data[i]);
                            }
                        }
                    });
                }

                function showPreviousUserAttachments() {
                    $.ajaxSetup({
                        cache: "false"
                    });
                    $.ajax({
                        type: "POST",
                        url: "/automail.html?getUserFile='getUserFile'",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            showUserattachedFile(data);
                        }
                    });
                }

            </script>

            <style type="text/css">
                #content{margin-bottom: 5px;}
                #resultTable {
                    display: block;
                    border-top: 1px solid silver;
                    min-height: 400px;
                    padding: 5px;
                    line-height: 24px;
                    background-color: #FFFFFF;
                    width: 100%;
                }
                /* css for timepicker */
                .ui-timepicker-div .ui-widget-header {
                    margin-bottom: 8px;
                }

                .ui-timepicker-div dl {
                    text-align: left;
                }

                .ui-timepicker-div dl dt {
                    height: 25px;
                    margin-bottom: -25px;
                }

                .ui-timepicker-div dl dd {
                    margin: 0 10px 10px 65px;
                }

                .ui-timepicker-div td {
                    font-size: 80%;
                }

                .ui-tpicker-grid-label {
                    background: none;
                    border: none;
                    margin: 0;
                    padding: 0;
                }

                .qq-upload-list {
                    margin: 6px 0 25px 0;
                    padding: 0;
                    list-style: none;
                    width: 750px;
                    text-align: left;
                    color: #000;
                    word-wrap: break-word;
                    white-space: pre-wrap; /* css-3 */
                    white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
                    white-space: -pre-wrap; /* Opera 4-6 */
                    white-space: -o-pre-wrap; /* Opera 7 */
                    word-wrap: break-word; /* Internet Explorer 5.5+ */
                    padding-top: 10px;
                }

                .qq-upload-button {
                    height: 20px;
                }

                .qq-uploader {
                    position: relative;
                }

                .qq-upload-cancel {
                    display: none;
                }

                .remove {
                    color: white;
                    background-color: #F56302;
                    border: 0px;
                    font-size: 11px;
                    font-weight: bold;
                    moz-border-radius: 15px;
                    webkit-border-radius: 15px;
                    border-radius: 5px;
                    border-style: solid;
                    height: 20px;
                    width: 60px;
                }

                #browseBtn {
                    background-image: url(css/0/images/upload.png);
                    background-repeat: no-repeat;
                    width: 77px;
                    height: 35px;
                    cursor: pointer;
                    z-index: 1;
                    position: absolute;
                    display: none;
                }

                #mailsent {
                    color: green;
                    font-size: 14px;
                    text-align: center;
                    font-weight: bold;
                }

                #resultTable h2 {
                    display: inline;
                    margin: 30px 15px 0px 15px;
                }

                #scroller {
                    overflow-x: hidden;
                    overflow-y: scroll;
                }

                #profileScroller {
                    overflow-x: hidden;
                    overflow-y: scroll;
                }

                .hideScroller {
                    overflow-x: hidden;
                    overflow-y: hidden !important;
                }

                .forscroller {
                    float: center;
                    height: 410px;
                }

            </style>

        </head>

        <body>
            <div class="responsive-wrapper">

                <div class="middle-wrapper">
                    <div id="content">
                        <form:form commandName="automail" method="POST">
                            <div id="tabs-icons" style='background:none;'>
                                <ul>
                                    <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top " 
                                                          onmouseover="this.style.cursor = 'pointer'" 
                                                          onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                    <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'"
                                                        onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                    <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                         class="ui-state-default ui-corner-top ui-state-active"
                                                         onmouseover="this.style.cursor = 'pointer'"
                                                         onclick="naviToOtherTabs('autoMail')" >Automail</a></li>
                                    <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'"
                                                        onclick="naviToOtherTabs('settings')" >Settings</a></li>
                                    <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                              class="ui-state-default ui-corner-top"
                                                              onmouseover="this.style.cursor = 'pointer'"
                                                              onclick="naviToOtherTabs('knowledgeBase')" >Knowledge Base</a></li>
                                    <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                              class="ui-state-default ui-corner-top" 
                                                              onmouseover="this.style.cursor = 'pointer'" 
                                                              onclick="naviToOtherTabs('expertAnswer')">Expert Answer</a></li>
                                    <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('expertUser')" >Expert User</a></li>
                                    <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('ateReports')" >ATE Reports</a></li>
                                    <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top"
                                                               onmouseover="this.style.cursor = 'pointer'"
                                                               onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                                </ul>

                                <div style="clear: both;"></div>
                                <div style='border-bottom: 1px solid silver;border-left: 1px solid silver;border-right: 1px solid silver;'>
                                    <table id="resultTable" width="100%">
                                        <tr>
                                            <td valign="top">
                                                <div style="margin-left: 30px; margin-top: 20px;">
                                                    <div style="" id="scroller" class="forscroller">
                                                        <table id="formrowTab" BORDER="0" CELLPADDING="8"
                                                               CELLSPACING="1" width="100%" align="center">
                                                            <tr class="aptresultheading gradient"
                                                                style="font-size: 1.3em; color: #fff;">
                                                                <th class="aptresultheading gradient" width="30%">Template&nbsp;Name</th>
                                                                <th class="aptresultheading gradient" width="10%">Category</th>
                                                                <th class="aptresultheading gradient" width="25%">Subject</th>
                                                                <th class="aptresultheading gradient" width="13%">Last&nbsp;Mail&nbsp;Sent</th>
                                                                <th class="aptresultheading gradient" width="12%">Is&nbsp;Enabled</th>
                                                                <th class="aptresultheading gradient" width="10%">Delete</th>
                                                            </tr>
                                                            <c:forEach items="${automail.formRows}"
                                                                       varStatus="automailRow">
                                                                <tr
                                                                    style="background-color: #EEEEEE; width: 100%; position: relative;">
                                                                    <td width="30%"><spring:bind
                                                                            path="automail.formRows[${automailRow.index}].tempId">
                                                                            <a href="automail.html?showtemplate=${status.value}"
                                                                               onclick="showTemplateWithId(${status.value}, event)"
                                                                               style="text-decoration: none;">
                                                                            </spring:bind> <spring:bind
                                                                                path="automail.formRows[${automailRow.index}].templateName">
                                                                                <c:out value="${status.value}" />
                                                                            </spring:bind></a></td>
                                                                    <td width="10%"><spring:bind
                                                                            path="automail.formRows[${automailRow.index}].category">
                                                                            <c:if test="${status.value == 1}">Event</c:if>
                                                                            <c:if test="${status.value == 2}">On Demand</c:if>
                                                                            <c:if test="${status.value == 3}">Scheduled</c:if>
                                                                            <c:if test="${status.value == 4}">Once Per Event</c:if>
                                                                        </spring:bind></td>
                                                                    <td width="25%"
                                                                        <spring:bind path="automail.formRows[${automailRow.index}].mailSubject">title="<c:out value="${status.value}"/>"</spring:bind>
                                                                        onmouseover="this.style.cursor = 'pointer'"><spring:bind
                                                                            path="automail.formRows[${automailRow.index}].mailSubject">
                                                                            <c:out value="${status.value}" />
                                                                        </spring:bind></td>
                                                                    <td width="12%"><spring:bind
                                                                            path="automail.formRows[${automailRow.index}].lastMailSent">
                                                                    <center>
                                                                        <c:if test='${status.value != null}'>
                                                                            <c:out value="${status.value}" />
                                                                        </c:if>
                                                                    </center>
                                                                </spring:bind></td>
                                                                <td width="14%"><spring:bind
                                                                        path="automail.formRows[${automailRow.index}].status">
                                                                    <center>
                                                                        <input type="hidden" align="right"
                                                                               name="_<c:out value="${status.expression}"/>">
                                                                        <input type="checkbox" align="right"
                                                                               name="<c:out value="${status.expression}"/>"
                                                                               value="true"
                                                                               <c:if test='${status.value == true}'>checked</c:if> />
                                                                        </center>
                                                                </spring:bind></td>
                                                                <td width="12%"><spring:bind
                                                                        path="automail.formRows[${automailRow.index}].deleted">
                                                                    <center>
                                                                        <input type="hidden"
                                                                               name="_<c:out value="${status.expression}"/>">
                                                                        <input type="checkbox"
                                                                               name="<c:out value="${status.expression}"/>"
                                                                               value="true"
                                                                               <c:if test='${status.value == true}'>checked</c:if> />
                                                                        </center>
                                                                </spring:bind></td>
                                                                </tr>
                                                            </c:forEach>
                                                        </table>
                                                    </div>
                                                    <div align="center" style="padding-left: 675px;">
                                                        <input style="width: 85px; height: 40px;" type="image"
                                                               src="../css/0/images/add_new.png" name="image" alt="Add New"
                                                               onClick="showNew()" /> <input
                                                               style="width: 65px; height: 40px;" type="image"
                                                               src="../css/0/images/apply.png" name="image" alt="Apply"
                                                               onClick="apply()" /> <input
                                                               style="width: 65px; height: 40px;" type="image"
                                                               src="../css/0/images/delete.png" name="image" alt="Delete"
                                                               onClick="deleteTemplates(event)" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div id="templateView" style="display: none; margin-left: 30px;">
                                                    <br>
                                                    <br>
                                                    <table bgcolor="#EEEEEE" border="0" id="templateTable"
                                                           width="95%" align="center" CELLPADDING="10" CELLSPACING="1">
                                                        <tr style="width: 100%;">
                                                            <td>
                                                                <div style="width: 100%;">
                                                                    <span id="disp1" style="width: 40%;"><b>Category:</b></span>
                                                                    <span style="width: 60%; padding-right: 15px;"></span> <span>
                                                                        <spring:bind path="automail.emailTemplate.category">
                                                                            <input type="radio" disabled="disabled"
                                                                                   onclick="category('catno1')"
                                                                                   name="<c:out value="${status.expression}"/>" " value="1"
                                                                                   <c:if test='${status.value == 1}'>checked</c:if> />Event 
                                                                                   <input type="radio"
                                                                                          disabled="disabled" onclick="category('catno2')"
                                                                                          name="<c:out value="${status.expression}"/>" " value="4"
                                                                                   <c:if test='${status.value == 4}'>checked</c:if> />Once&nbsp;Per&nbsp;Event
                                                                                   <input type="radio"
                                                                                          disabled="disabled" onclick="category('catno3')"
                                                                                          name="<c:out value="${status.expression}"/>" " value="2"
                                                                                   <c:if test='${status.value == 2}'>checked</c:if> />On&nbsp;Demand
                                                                                   <input type="radio"
                                                                                          disabled="disabled" onclick="category('catno4')"
                                                                                          name="<c:out value="${status.expression}"/>" " value="3"
                                                                                   <c:if test='${status.value == 3}'>checked</c:if> />Scheduled
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-bottom-style: none; width: 100%;">
                                                            <td style="padding-bottom: 1px; padding-left: 10px;">
                                                                <div style="width: 100%;">
                                                                    <span style="width: 20%; padding-right: 10px;"><b>Template&nbsp;Name:</b></span>
                                                                    <span style="width: 40%;"> <spring:bind
                                                                            path="automail.emailTemplate.templateName">
                                                                            <input id="tempName" size="28"
                                                                                   name="<c:out value="${status.expression}"/>"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind>
                                                                    </span> <span id="existingTemplate"
                                                                                  style="display: none; padding: 15px; width: 40%;">
                                                                        <% if (existingTemplate != null) { %> <b style="color: red;">There
                                                                            is a Template associated with this Name.</b> <% }%>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><span id="schCheckBox"> <spring:bind
                                                                        path="automail.emailTemplate.sendOnToday">
                                                                        <input type="hidden"
                                                                               name="_<c:out value="${status.expression}"/>">
                                                                        <input type="checkbox"
                                                                               name="<c:out value="${status.expression}"/>"
                                                                               onclick="disabRemField();" value="true"
                                                                               <c:if test='${status.value == true}'>checked</c:if> />
                                                                               <span style="font-weight: bold;">&nbsp;Schedule for
                                                                                   once</span>
                                                                        </spring:bind>
                                                                </span></td>
                                                        </tr>
                                                        <tr id="inst"
                                                            style="border-top-style: none; border-bottom-style: none; display: none; width: 100%;">
                                                            <td
                                                                style="padding-bottom: 5px; padding-top: 0px; padding-left: 10px;">
                                                                <br style="height: 1px;">
                                                                <div>
                                                                    <span style="width: 40%; padding-right: 41px;"><b>Event
                                                                            Type:</b></span> <span style="width: 40%;"> <spring:bind
                                                                                path="automail.emailTemplate.event">
                                                                            <select id="sel"
                                                                                    name="<c:out value="${status.expression}"/>">
                                                                                <option value="0"
                                                                                        <c:if test='${status.value == 0}'>selected</c:if>>Select....</option>
                                                                                        <option style="display: none;" value="1"
                                                                                        <c:if test='${status.value == 1}'>selected</c:if>>Activation</option>
                                                                                        <option value="2"
                                                                                        <c:if test='${status.value == 2}'>selected</c:if>>First
                                                                                            Login</option>
                                                                                        <option value="3"
                                                                                        <c:if test='${status.value == 3}'>selected</c:if>>First
                                                                                            tool usage</option>
                                                                                </select>
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="timedays"
                                                            style="border-top-style: none; border-bottom-style: none; width: 100px; display: none;">
                                                            <td
                                                                style="padding-bottom: 5px; padding-top: 5px; padding-left: 10px;">
                                                                <div>
                                                                    <span
                                                                        style="padding-right: 52px; padding-top: 0px; padding-bottom: 0px;"><b>Days&nbsp;Gap:</b></span>
                                                                    <span> <spring:bind
                                                                            path="automail.emailTemplate.daysGap">
                                                                            <input type="text" id="daygap"
                                                                                   class="inputBox1 rightText"
                                                                                   name="<c:out value="${status.expression}"/>"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind>
                                                                    </span> <span style="width: 20%; padding-right: 103px;"></span> <span
                                                                        style="width: 10%; padding-right: 11px; padding-top: 5px; padding-bottom: 5px;"><b>Time&nbsp;of&nbsp;a&nbsp;Day:</b></span>
                                                                    <span class="example-container"> <spring:bind
                                                                            path="automail.emailTemplate.timeOfDay">
                                                                            <input type="hidden" name="timeOfDay"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind> <input type="text" size="16"
                                                                               style="background-color: #FFFFFF;" name="example3"
                                                                               id="example3" readonly="true" /> <pre
                                                                               style="display: none;">
                                                                            $('#example3').timepicker({
                                                                            stepMinute: 30
                                                                            });
                                                                        </pre> <span class="note"
                                                                                     style="text-align: center;"> (Please provide in GMT
                                                                            24 hours format)</span> <span style="font-size: 12px;"><a
                                                                                href="http://www.worldtimebuddy.com/gmt-to-est-converter"
                                                                                target="_blank">Time Conversion</a></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="stdt"
                                                            style="border-top-style: none; border-bottom-style: none; width: 100px; display: none;">
                                                            <td
                                                                style="padding-bottom: 5px; padding-top: 5px; padding-left: 10px;">
                                                                <div>
                                                                    <span id="date" style="width: 40%; padding-right: 49px;"><b>Start&nbsp;Date:</b></span>
                                                                    <span><form:input readonly="true"
                                                                                cssStyle="background-color:#FFFFFF;" path="fromDate"
                                                                                size="16" /></span> <span
                                                                        style="width: 20%; padding-right: 60px;"></span> <span
                                                                        id="toDt"> <span id="date"
                                                                                     style="width: 40%; padding-right: 11px;"><b>End&nbsp;Date:</b></span>
                                                                        <span><form:input path="toDate" readonly="true"
                                                                                    cssStyle="background-color:#FFFFFF;" size="16" /></span>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 30px;"></tr>
                                                        <tr style="border-top-style: none; border-bottom-style: none;">
                                                            <td
                                                                style="padding-bottom: 8px; padding-top: 10px; padding-left: 10px;">
                                                                <div>
                                                                    <span style="width: 55%; padding-right: 61px;"><b>Subject:</b></span>
                                                                    <span> <spring:bind
                                                                            path="automail.emailTemplate.mailSubject">
                                                                            <input id="subj" style="width: 63%;"
                                                                                   name="<c:out value="${status.expression}"/>"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr style="border-top-style: none; border-bottom-style: none;">
                                                            <td
                                                                style="padding-bottom: 5px; padding-top: 0px; padding-left: 10px;">
                                                                <br style="height: 1px;">
                                                                <div>
                                                                    <span style="width: 40%; padding-right: 78px;"><b>Logo:</b></span>
                                                                    <span style="width: 40%;"> <spring:bind
                                                                            path="automail.emailTemplate.logo">
                                                                            <select id="logoSel"
                                                                                    name="<c:out value="${status.expression}"/>">
                                                                                <option value="0"
                                                                                        <c:if test='${status.value == 0}'>selected</c:if>>Select....</option>
                                                                                        <option value="1"
                                                                                        <c:if test='${status.value == 1}'>selected</c:if>>LXRMarketplace</option>
                                                                                        <option value="2"
                                                                                        <c:if test='${status.value == 2}'>selected</c:if>>LXRSEO</option>
                                                                                        <option value="3"
                                                                                        <c:if test='${status.value == 3}'>selected</c:if>>None</option>
                                                                                </select>
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr style="border-top-style: none; border-bottom-style: none;">
                                                            <td
                                                                style="padding-bottom: 8px; padding-top: 10px; padding-left: 10px;">
                                                                <div>
                                                                    <span style="width: 55%; padding-right: 84px;"><b>URL:</b></span>
                                                                    <span> <spring:bind
                                                                            path="automail.emailTemplate.url">
                                                                            <input id="logoUrl" style="width: 50%;"
                                                                                   name="<c:out value="${status.expression}"/>"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr style="border-top-style: none; border-bottom-style: none;">
                                                            <td
                                                                style="padding-bottom: 8px; padding-top: 10px; padding-left: 10px;">
                                                                <div>
                                                                    <span style="width: 55%;"><b>Personal Address:</b></span> <span>
                                                                        <spring:bind path="automail.emailTemplate.personalAddress">
                                                                            <input id="personalAddr" style="width: 50%;"
                                                                                   name="<c:out value="${status.expression}"/>"
                                                                                   value="<c:out value="${status.value}"/>" />
                                                                        </spring:bind>
                                                                    </span> <span class="note">&nbsp;(Ex: Hi &lt;user&gt;)</span>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <div id="file" width="100%" style="display: inline-block;">
                                                                    <div
                                                                        style="vertical-align: middle; padding-right: 17px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline;">
                                                                        <b style="">Attach&nbsp;a&nbsp;file:&nbsp; </b>
                                                                    </div>
                                                                    <div class="smallBrowseButton" id="browse1"
                                                                         style="display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;"
                                                                         alt=" " title="Choose a File to Attach"></div>
                                                                    <div class="note"
                                                                         style="padding: 0px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;">(You
                                                                        can upload max 5 files, Each of max size 2MB)</div>
                                                                    <br>
                                                                    <br>
                                                                    <br>
                                                                </div>
                                                                <div id="filedisable" width="100%" style="display: none;">
                                                                    <div
                                                                        style="vertical-align: middle; padding-right: 17px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline;">
                                                                        <b style="">Attach&nbsp;a&nbsp;file:&nbsp; </b>
                                                                    </div>
                                                                    <div id="browsedisable"
                                                                         style="display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;"
                                                                         class="smallBrowseButtonDisabled" alt=" "
                                                                         title="Choose a File to Attach"></div>
                                                                    <div class="note"
                                                                         style="padding: 0px; display: inline-block; display: inline-table; display: -moz-inline-stack; display: inline-block; zoom: 1; *display: inline; vertical-align: middle;">(You
                                                                        can upload max 5 files, Each of max size 2MB)</div>
                                                                    <br>
                                                                    <br>
                                                                    <br>
                                                                </div>
                                                                <div>
                                                                    <table id="attachments">
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <span style="width: 40%; padding-right: 27px;"><b>From
                                                                            Address:</b></span> <span style="width: 40%;"> <spring:bind
                                                                                path="automail.emailTemplate.fromAddress">
                                                                            <select id="fromAddressSel"
                                                                                    name="<c:out value="${status.expression}"/>">
                                                                                <option value="0"
                                                                                        <c:if test='${status.value == 0}'>selected</c:if>>Select....</option>
                                                                                        <option value="1"
                                                                                        <c:if test='${status.value == 1}'>selected</c:if>>LXRMarketplace
                                                                                            Team &lt;info@netelixir.com&gt;</option>
                                                                                        <option value="2"
                                                                                        <c:if test='${status.value == 2}'>selected</c:if>>LXRMarketplace
                                                                                            Support &lt;support@lxrmarketplace.com &gt;</option>
                                                                                        <option value="3"
                                                                                        <c:if test='${status.value == 3}'>selected</c:if>>LXRSEO
                                                                                            Team &lt;info@lxrseo.com&gt;</option>
                                                                                        <option value="4"
                                                                                        <c:if test='${status.value == 4}'>selected</c:if>>LXRSEO
                                                                                            Support &lt;support@lxrseo.com&gt;</option>
                                                                                </select>
                                                                        </spring:bind>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><div id="mailsent">
                                                                    <c:out value="${mailsent}" />
                                                                </div>
                                                            <td>
                                                        </tr>
                                                        <tr
                                                            style="border-top-style: none; border-bottom-style: none; padding-bottom: 0px;">
                                                            <td style="padding-bottom: 0px;">
                                                                <div
                                                                    style="width: 99%; padding-left: 8px; padding-right: 8px;">
                                                                    <spring:bind path="automail.emailTemplate.mailBody">
                                                                        <textarea id="input" name="${status.expression}" rows="25"
                                                                                  cols="23">
                                                                            <c:out value="${status.value}" />
                                                                        </textarea>

                                                                    </spring:bind>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><spring:bind
                                                                    path="automail.emailTemplate.sendMailOption">
                                                                    <input type="radio" onclick="showUploadBut('1')"
                                                                           name="<c:out value="${status.expression}"/>" " value="1"
                                                                           <c:if test='${status.value == 1}'>checked</c:if> />Send to All Users <br />
                                                                           <input type="radio" onclick="showUploadBut('2')"
                                                                                  name="<c:out value="${status.expression}"/>" " value="2"
                                                                           <c:if test='${status.value == 2}'>checked</c:if> />Send to All Users excluding Local Users<br />
                                                                           <span id="hideOption"> <input type="radio"
                                                                                                         onclick="showUploadBut('3')"
                                                                                                         name="<c:out value="${status.expression}"/>" " value="3"
                                                                               <c:if test='${status.value == 3}'>checked</c:if> />Send to
                                                                               Selected List
                                                                           </span>
                                                                </spring:bind></td>
                                                        </tr>
                                                        <tr>
                                                            <%--                            		<form:hidden path="automail.emailTemplate.userListFile" /> --%>
                                                            <spring:bind path="automail.emailTemplate.userListFile">
                                                            <input type="hidden" id="userListFile"
                                                                   name="<c:out value="${status.expression}"/>"
                                                                   value="<c:out value="${status.value}"/>" />
                                                        </spring:bind>
                                                        <td>
                                                            <div id="userDetailsMsg"
                                                                 style="color: green; font-weight: bold;">
                                                                <div id="SuccessMsg1"></div>
                                                                <div id="SuccessMsg2"></div>
                                                                <div id="SuccessMsg3"></div>
                                                                <div id="SuccessMsg4"></div>
                                                            </div>
                                                            <div class="browseBtn" id="browseBtn"></div>
                                                            <div id="upNote" class="note"
                                                                 style="padding-left: 80px; display: none;">(Please
                                                                upload xls file. Keep the header as 'userid', 'email',
                                                                'name'. Userid column can be blank. )</div>
                                                            <div class="browseBtnDisabled" id="browseBtnDisabled"
                                                                 style="display: none;"></div>
                                                            <br />
                                                            <div>
                                                                <div id="attachedFiles"></div>
                                                            </div>
                                                        </td>
                                                        </tr>

                                                        <tr id="sendbutt"
                                                            style="border-top-style: none; display: none;">
                                                            <td style="padding-left: 570px;"><img
                                                                    style="width: 60px; height: 26px; padding-bottom: 8px"
                                                                    src="../css/0/images/cancel.gif" onclick="cancelTemplate();"
                                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                                    style="width: 65px; height: 39px; padding-top: 3px; padding-bottom: 1px;"
                                                                    src="../css/0/images/save.png" onclick="saveTemplate();"
                                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                                    style="width: 75px; height: 37px; padding-top: 3px; padding-bottom: 3px;"
                                                                    src="../css/0/images/sendmail.png"
                                                                    onclick="saveAndSendmail();"
                                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                                    style="width: 80px; height: 37px; padding-top: 3px; padding-bottom: 3px;"
                                                                    src="../css/0/images/test_mail.gif" onclick="sendTestmail()"
                                                                    onmouseover="this.style.cursor = 'pointer'" /></td>
                                                        </tr>
                                                        <tr id="savebutt"
                                                            style="border-top-style: none; padding: 0px;">
                                                            <td style="padding-left: 650px;"><img
                                                                    style="width: 60px; height: 26px; padding-bottom: 8px;"
                                                                    src="../css/0/images/cancel.gif" onclick="cancelTemplate();"
                                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                                    style="width: 60px; height: 39px; padding-top: 3px; padding-bottom: 1px;"
                                                                    src="../css/0/images/save.png" onclick="saveTemplate();"
                                                                    onmouseover="this.style.cursor = 'pointer'" /> <img
                                                                    style="width: 80px; height: 37px; padding-top: 3px; padding-bottom: 3px;"
                                                                    src="../css/0/images/test_mail.gif" onclick="sendTestmail()"
                                                                    onmouseover="this.style.cursor = 'pointer'" /></td>
                                                        </tr>

                                                    </table>
                                                    <br>
                                                    <br>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                </div>

                            </div>
                            <div id="testmailpopup" class="popup popupPortlet ui-corner-all"
                                 align="center"
                                 style="position: fixed; width: 320px; height: 90px; top: 150px; left: 36%; border-radius: 8px; background-color: #F7F7F7; border: 3px solid gray; padding-top: 30px;">
                                <span class="closesymbol"
                                      style="padding-right: 4px; padding-top: 3px;"><input
                                        style="height: 10px; width: 10px;" type="image" id="submitColumn"
                                        src="../css/0/images/popupclose.png" alt="popupclose"></span>
                                    <%-- 					     <div id="mailsent"><c:out value="${mailsent}" /></div> --%>
                                <b>Enter Your Mail Id<b> &nbsp;&nbsp;<form:input
                                            path="testmail" /><br>
                                        <br> <img
                                            style="width: 75px; height: 38px; padding-top: 3px; padding-bottom: 1px;"
                                            src="../css/0/images/sendmail.png" onclick="Sendmail();"
                                            onmouseover="this.style.cursor = 'pointer'" />
                                        </div>

                                    </form:form>
                                    </div>

                                    </div>

                                    </div>
                                    <%@ include file="/WEB-INF/view/footer.jsp"%>
                                    </body>
                                    </html>