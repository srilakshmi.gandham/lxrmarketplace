<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,java.util.Calendar,java.util.Map,java.util.TreeSet,java.util.Set,lxr.marketplace.user.LoginService"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%@page import="org.springframework.web.context.WebApplicationContext"%>
    <%@page
        import="org.springframework.web.context.support.WebApplicationContextUtils"%>

        <%    HttpSession sessionR = request.getSession();
            Login user = (Login) sessionR.getAttribute("user");
            Object ul = sessionR.getAttribute("userLoggedIn");
            boolean userLoggedIn = false;
            ExpertUser expertUser = null;
            if (ul != null) {
                userLoggedIn = (Boolean) ul;
            }
            if (userLoggedIn) {
                if (sessionR.getAttribute("expertUser") != null) {
                    expertUser = (ExpertUser) sessionR.getAttribute("expertUser");
                    pageContext.setAttribute("expertUser", expertUser);
                }
            }
        %>
        <html>
            <head>
                <title>LXRMarketplace | Reports</title>
                <meta name="keywords" content="Admin Reports" />
                <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
                <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
                <%@ include file="/WEB-INF/view/commonImport.jsp"%>
                <%@ include file="/WEB-INF/view/header.jsp"%>

                <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
                <script type="text/javascript" src="/scripts/ui/jquery.ui.datepicker.js"></script>
                <link rel="stylesheet" href="css/lxrm-core.css">
                <link rel="stylesheet" href="css/admin.css">
                <link rel="stylesheet" href="css/ate-reports.css">
                <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">

                <script type="text/javascript">

                    <%	out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                        Calendar cal1st = Calendar.getInstance();
                        cal1st.set(Calendar.DAY_OF_MONTH, 1);
                        out.println("var serverFirstDay=" + cal1st.getTimeInMillis() + ";");
                        out.println("var userLoggedIn=" + userLoggedIn + ";");
                    %>
                    $(document).ready(function () {
                        var role = "";
                    <c:if test="${not empty expertUser}" >
                        var name = "${expertUser.name}"
                        role = "${expertUser.role}"
                        if (name.length > 15) {
                            name = name.substring(0, 15);
                        }
                        $('#uName').text(name + " (" + role + ")");
                    </c:if>
                        if (role === 'expert') {
                            $('#dashBoard, #reports, #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        } else if (role === 'reviewer') {
                            $('#dashBoard,  #autoMail, #setting, #expertuser, #pluginAdoption').hide();
                        }

                        //getTemplatesData();
                        $('#templates').hide();
                        var logo = document.getElementById("logo");
                        logo.href = "/dashboard.html";
                        $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    <%if (user != null) {
                            out.println("$(\"#loginSuccess\").show();");
                        } else {
                            out.println("$(\"#loginSuccess\").hide();");
                        }%>

                        if ($('#reportType').val() === 1) {
                            $('#videocheckbox').show();
                        }
                        if ($('#reportType').val() === 3) {
                            $('#tilldate').show();
                            $("#dateRangeOption").append('<option id="tilldate" value="6" label="Till Date">Till Date</option>');
                        }
                        var startDate;
                        var endDate;
                        var option;
                        if ($('#dateRangeOption').val() === -1) {
                            $('#cDateRange').show();
                        }
                        $("#run-report").bind('click', function () {
                            option = $("#dateRangeOption").val();
                            startDate = $("#startDate").datepicker("getDate");
                            endDate = $("#endDate").datepicker("getDate");
                            var reportName = $("#reportName").val();
                            if (reportName <= 0) {
                                alert("Please select the Report Name");
                                $("#reportName").focus();
                                return false;
                            } else if ($("#startDate").val().trim() === "") {
                                alert("Please select the Start Date");
                                $("#startDate").focus();
                                return false;
                            } else if ($("#endDate").val().trim() === "") {
                                alert("Please select the End Date");
                                $("#endDate").focus();
                                return false;
                            } else if ($("#expertName").val() < 0) {
                                alert("Please select the Expert Name");
                                $("#expertName").focus();
                                return false;
                            } else if (startDate > endDate) {
                                alert("Start Date is less than End Date, \n please select a valid date range");
                                $("#startDate").focus();
                                return false;
                            } else {
                                var f = $("#ateReport");
                                f.attr('method', 'POST');
                                f.attr('action', "ate-reports.html?report=download");
                                f.submit();
                            }
                        });
                    });
                    $(function () {
                        $("#startDate, #endDate").datepicker({
                            showOn: "both",
                            buttonImage: "../css/0/images/calendar.jpg",
                            buttonImageOnly: true,
                            dateFormat: 'yy-mm-dd',
                            maxDate: new Date(serverTime),
                            changeMonth: true,
                            changeYear: true,
                            constrainInput: true
                        });
                    });

                    $(function () {
                        $("#tabs-icons").tabs();
                    });
                    function naviToOtherTabs(menuOption) {
                        var menuParams = {menuOption: menuOption};
                        $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                            if (data !== null) {
                                parent.window.location = data;
                            }
                        });
                    }
                </script>
            </head>
            <body>
                <div class="responsive-wrapper">

                    <div class="middle-wrapper">
                        <div class="" id="content">
                            <form:form commandName="ateReport" method="POST">
                                <div id="tabs-icons" style='padding:0;'>
                                    <ul>
                                        <li id="dashBoard"><a id ="dashBoard" href="#tabs-icons-1" name="tabs-icons" 
                                                              class="ui-state-default ui-corner-top " 
                                                              onmouseover="this.style.cursor = 'pointer'" 
                                                              onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                        <li id="reports"><a href="#tabs-icons-2" name="tabs-icons" 
                                                            class="ui-state-default ui-corner-top"
                                                            onmouseover="this.style.cursor = 'pointer'"
                                                            onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                        <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                             onclick="naviToOtherTabs('autoMail')" 
                                                             class="ui-state-default ui-corner-top"
                                                             onmouseover="this.style.cursor = 'pointer'">Automail</a></li>
                                        <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                            onclick="naviToOtherTabs('settings')" 
                                                            class="ui-state-default ui-corner-top"
                                                            onmouseover="this.style.cursor = 'pointer'">Settings</a></li>
                                        <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  onclick="naviToOtherTabs('knowledgeBase')" 
                                                                  class="ui-state-default ui-corner-top"
                                                                  onmouseover="this.style.cursor = 'pointer'">Knowledge Base</a></li>
                                        <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                                  onclick="naviToOtherTabs('expertAnswer')" 
                                                                  class="ui-state-default ui-corner-top"
                                                                  onmouseover="this.style.cursor = 'pointer'">Expert Answer</a></li>
                                        <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                               onclick="naviToOtherTabs('expertUser')" 
                                                               onmouseover="this.style.cursor = 'pointer'">Expert User</a></li>
                                        <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top 
                                                               ui-tabs-selected ui-state-active" 
                                                               onmouseover="this.style.cursor = 'pointer'">ATE Reports</a></li>
                                        <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                                   class="ui-state-default ui-corner-top"
                                                                   onmouseover="this.style.cursor = 'pointer'"
                                                                   onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                                    </ul>
                                </div>
                                <div id="resultTable">
                                    <div class="expert-report-info">
                                        <div class="text-label">Report Name: </div>
                                        <div class="input-field">
                                            <form:select path="reportName">
                                                <form:option value="0" label="-- Select --" />
                                                <form:option value="1" label="ATE Experts Reports" />
                                            </form:select>
                                        </div>
                                    </div>
                                    <div id="startDateSelection" class="expert-report-info">
                                        <div class="text-label">Start Date: </div>
                                        <div class="input-field">
                                            <form:input readonly="true" path="startDate" size="12" />
                                        </div>
                                    </div>
                                    <div id="endDateSelection" class="expert-report-info">
                                        <div class="text-label">End Date: </div>
                                        <div class="input-field">
                                            <form:input readonly="true" path="endDate" size="12" />
                                        </div>
                                    </div>
                                    <div class="expert-report-info">
                                        <div class="text-label">Expert Name: </div>
                                        <div class="input-field">
                                            <form:select path="expertName">
                                                <form:option value="-1">Select</form:option>
                                                <form:option value="0">All</form:option>
                                                <form:options items="${experts}"  />
                                            </form:select>
                                        </div>
                                    </div>
                                    <div class="expert-report-info">
                                        <div class="text-label"></div>
                                        <div class="input-field input-button">
                                            <input type="submit" value="Run Report" id="run-report"  title="RUN REPORT">
                                            <input type="reset" id="clear" value="Cancel" title="CANCEL" onclick="clearForm()">
                                        </div>
                                    </div>
                                </div>
                            </form:form>
                        </div>

                    </div>
                </div>
                <%@ include file="/WEB-INF/view/footer.jsp"%>
            </body>
        </html>
