
<!DOCTYPE html>
<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page
    import="lxr.marketplace.user.Login,java.util.Calendar"%>
    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>
    <%    HttpSession sessionD = request.getSession();
        Login user = (Login) sessionD.getAttribute("user");
        Object ul = sessionD.getAttribute("userLoggedIn");
        boolean userLoggedIn = false;
        ExpertUser expertUser = null;
        if (ul != null) {
            userLoggedIn = (Boolean) ul;
        }
        if (userLoggedIn) {
            if (sessionD.getAttribute("expertUser") != null) {
                expertUser = (ExpertUser) sessionD.getAttribute("expertUser");
                pageContext.setAttribute("expertUser", expertUser);
            }
        }
    %>
    <html>
        <head>
            <title>LXRMarketplace | Dashboard</title>
            <meta name="keywords" content="Admin Reports" />
            <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

            <%@ include file="/WEB-INF/view/CommonHeadPart.jsp"%>
            <%@ include file="/WEB-INF/view/commonImport.jsp"%>
            <%@ include file="/WEB-INF/view/header.jsp"%>

            <script type="text/javascript" src="/scripts/ui/jquery.ui.datepicker.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.widget.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/scripts/ui/jquery.ui.tabs.js"></script>
            <script type="text/javascript" src="/scripts/Charts.js"></script>
            <link rel="stylesheet" href="/css/0/themes/ui-lightness/jquery.ui.all.css">
            <link rel="stylesheet" href="css/lxrm-core.css">
            <link rel="stylesheet" href="css/admin.css">
            <script type="text/javascript">

                <%
                    out.println("var serverTime=" + Calendar.getInstance().getTimeInMillis() + ";");
                    out.println("var userLoggedIn=" + userLoggedIn + ";");
                %>

                $(function () {
                    $("#fromDate, #toDate").datepicker({
                        showOn: "both",
                        buttonImage: "../css/0/images/calendar.jpg",
                        buttonImageOnly: true,
                        dateFormat: 'dd-M-yy',
                        maxDate: new Date(serverTime),
                        changeMonth: true,
                        changeYear: true,
                        constrainInput: true
                    });
                });

                $(document).ready(function () {

                <c:if test="${not empty expertUser}" >
                    var name = "${expertUser.name}"
                    var role = "${expertUser.role}"
                    if (name.length > 15) {
                        name = name.substring(0, 15);
                    }
                    $('#uName').text(name + " (" + role + ")");
                </c:if>

                    var logo = document.getElementById("logo");
                    logo.href = "/dashboard.html";

                    $("#seo,#ppc,#ecommerce,#adminPanel,#alertSymbol,.settings,#new-tool a,#about,#blog").hide();
                    $("#tabs-icons").tabs();
                <%                    if (user != null) {
                        out.println("$(\"#loginSuccess\").show();");
                    } else {
                        out.println("$(\"#loginSuccess\").hide();");
                    }
                %>
                    var fullDate = new Date(serverTime);
                    var fd;
                    var td;
                    var option;
                    var dayInMilSec = 24 * 60 * 60 * 1000;

                    $('#dateRangeOption').val("4");
                    td = new Date(fullDate);
                    fd = new Date(fullDate - new Date(30 * dayInMilSec));
                    $('#fromDate').datepicker("setDate", fd);
                    $('#toDate').datepicker("setDate", td);
                    if ($('#dateRangeOption').val() == -1) {
                        $('#cDateRange').show();
                    }


                    azaxCalls();

                    $('#includeLocal').change(function () {
                        azaxCalls();
                    });

                    $('#dateRangeOption').change(function () {
                        option = $('#dateRangeOption').val();
                        if (option == "-1") {
                            $('#cDateRange').show();
                            if ($('#fromDate').datepicker(
                                    "getDate") == null) {
                                $('#fromDate').datepicker(
                                        "setDate",
                                        new Date(fullDate));
                            }
                            if ($('#toDate').datepicker(
                                    "getDate") == null) {
                                $('#toDate').datepicker(
                                        "setDate",
                                        new Date(fullDate));
                            }
                        } else {
                            $('#cDateRange').hide();
                            if (option == "1") {
                                fd = new Date(fullDate);
                                td = new Date(fullDate);
                            } else {
                                if (option == "2") {
                                    fd = new Date(
                                            fullDate
                                            - new Date(
                                                    1 * dayInMilSec));
                                } else if (option == "3") {
                                    fd = new Date(
                                            fullDate
                                            - new Date(
                                                    7 * dayInMilSec));
                                } else if (option == "4") {
                                    fd = new Date(
                                            fullDate
                                            - new Date(
                                                    30 * dayInMilSec));
                                } else if (option == "5") {
                                    fd = new Date(
                                            fullDate
                                            - new Date(
                                                    90 * dayInMilSec));
                                } else if (option == "6") {
                                    fd = new Date(fullDate - new Date(60 * dayInMilSec));

                                    // 						fd = new Date(2011,
                                    // 								10 - 1, 01);
                                }
                                td = new Date(
                                        fullDate
                                        - new Date(
                                                1 * dayInMilSec));
                            }
                            $('#fromDate').datepicker(
                                    "setDate", fd);
                            $('#toDate').datepicker(
                                    "setDate", td);
                            azaxCalls();
                        }
                    });
                });

                function naviToOtherTabs(menuOption) {
                    var menuParams = {menuOption: menuOption};
                    $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
                        if (data !== null) {
                            parent.window.location = data;
                        }
                    });
                }
                
                function loadCharts1(divName, xmlData) {
                    var chart = new FusionCharts(
                            "/FusionCharts/FCF_Pie2D.swf?ChartNoDataText=No data available.",
                            "chart3Id", "240", "140", "1", "0");
                    chart.addParam("WMode", "Transparent");
                    chart.setDataXML(xmlData);
                    chart.render(divName);
                }

                function loadCharts(divName, xmlData) {
                    var chart = new FusionCharts(
                            "/FusionCharts/Column2D.swf?ChartNoDataText=No data available",
                            "chart3Id", "440", "220", "0", "0");
                    chart.addParam("WMode", "Transparent");
                    chart.setDataXML(xmlData);
                    chart.render(divName);
                }

                function fusionlineChart(divName, xmlData) {
                    var chart1 = new FusionCharts(
                            "FusionCharts/FCF_MSLine.swf?ChartNoDataText=No data available.",
                            "chart3Id", "440", "250");
                    chart1.addParam("WMode", "Transparent");
                    if (xmlData != "") {
                        chart1.setDataXML(xmlData);
                    } else {
                        chart1.setDataXML(xmlData);
                    }
                    chart1.render(divName);
                }

                function gocall() {
                    fd = $("#fromDate").datepicker("getDate");
                    td = $("#toDate").datepicker("getDate");
                    if (fd > td) {
                        alert("Start Date is less than End Date, \n please select a valid date range");
                        $("#fromDate").focus();
                        return false;
                    } else {
                        azaxCalls();
                    }
                }

                function azaxCalls() {
                    var local = false;
                    if ($('#includeLocal').attr('checked')) {
                        local = true;
                    } else {
                        local = false;
                    }
                    fd = $("#fromDate").datepicker("getDate");
                    td = $("#toDate").datepicker("getDate");

                    var startDate, endDate;
                    if (fd != null)
                        startDate = fd.getFullYear() + "-" + (fd.getMonth() + 1) + "-"
                                + fd.getDate() + " 00:00:00";
                    if (td != null)
                        endDate = td.getFullYear() + "-" + (td.getMonth() + 1) + "-"
                                + td.getDate() + " 23:59:59";
                    for (var i = 1; i < 9; i++) {
                        $("#table" + i + " tr:nth-child(1) td:nth-child(1)").addClass("data-loading");
                    }
                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=1&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data1: {},
                        dataType: "json",
                        success : function (data1) {
                            $("#table1 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            var per1 = (data1[0][2] != 0) ? data1[0][0]
                                    / data1[0][2] * 100 : 0;
                            var per2 = (data1[0][2] != 0) ? data1[0][1]
                                    / data1[0][2] * 100 : 0;
                            var div = document.getElementById('newVisitPer');
                            var spans = div.getElementsByTagName('span');
                            spans[0].innerHTML = per1.toFixed(1) + "% New Visits";
                            spans[1].innerHTML = data1[0][0] + " Visits";

                            var div = document.getElementById('retVisitPer');
                            var spans = div.getElementsByTagName('span');
                            spans[0].innerHTML = per2.toFixed(1)
                                    + "% Returning Visits";
                            spans[1].innerHTML = data1[0][1] + " Visits";

                            var totalVisits = document
                                    .getElementById('totalVisitor');
                            style = "height:14px;"
                            totalVisits.innerHTML = "Total number of visits: "
                                    + data1[0][2];
                            var noToolVisits = document
                                    .getElementById('noToolUseVisits');
                            style = "height:14px;"
                            noToolVisits.innerHTML = "No tool usage visits: "
                                    + data1[0][3];
                            loadCharts1('newVisitors', data1[1]);
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=2&startDate=" + startDate + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data2: {},
                        dataType: "json",
                        success: function (data2) {
                            $("#table2 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            $("#table2 tr:nth-child(1) td:first h1").text('Total Visitors: ' + data2[0][0]);
                            $("#table2 tr:nth-child(1) td:nth-child(2) h1").text('Total Registered Users: ' + data2[0][1]);
                            $("#table2 tr:nth-child(3) td:nth-child(2)").text(data2[0][2]);
                            for (var i = 0; i < 2; i++) {
                                $("#table2 tr:nth-child(" + (i + 4) + ") td:nth-child(2)").text(data2[0][i + 3]);
                                var per = 0;
                                if (data2[0][1] > 0) {
                                    per = (data2[0][i + 3] * 100 / data2[0][2]).toFixed(0);
                                }
                                $("#table2 tr:nth-child(" + (i + 4) + ") td:nth-child(3)").html("<div style=\"position: auto; width: " + per * 1.4 + "px; height: 15px; background-color:#4F81BD;\"></div>");
                                $("#table2 tr:nth-child(" + (i + 4) + ") td:nth-child(4)").html("<b>" + per + "</b>%");
                            }
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=3&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data3: {},
                        dataType: "json",
                        success: function (data3) {
                            $("#table3 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            //var gaolval = document.getElementById("GoalId");
                            //gaolval.innerHTML = data3[0] + " %";
                            fusionlineChart('goalConversion', data3[1]);
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=4&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data1: {},
                        dataType: "json",
                        success: function (data4) {
                            $("#table4 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            loadCharts('numoftimestoolused', data4[1]);
                            var mostToolId = document.getElementById('mostUsedTool');
                            mostToolId.innerHTML = "Most used tool: " + data4[2];
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: "/dashboard.html?block=5&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data5: {},
                        dataType: "json",
                        success: function (data5) {
                            $("#table5 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            tableDataFiller("table5", data5[0]);
                            var mostToolId = document.getElementById('mostNoUsed');
                            mostToolId.innerHTML = "Most number of tools used: " + data5[1];
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=6&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data6: {},
                        dataType: "json",
                        success: function (data6) {
                            $("#table6 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            tableDataFiller("table6", data6[0]);
                            var max = document.getElementById('mostTimeSpent');
                            max.innerHTML = "Most time spent: " + data6[1];
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=7&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data7: {},
                        dataType: "json",
                        success: function (data7) {
                            $("#table7 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            tableDataFiller("table7", data7[0]);
                            var max = document.getElementById('mostSessions');
                            max.innerHTML = "Most number of sessions: " + data7[1];
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=8&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data8: {},
                        dataType: "json",
                        success: function (data8) {
                            $("#table8 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            tableDataFiller("table8", data8[0]);
                            var max = document.getElementById('prevVisit');
                            max.innerHTML = "For most visits the previous visit happened: "
                                    + data8[1];
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=9&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data9: {},
                        dataType: "json",
                        success: function (data9) {
                            $("#table9 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            fusionlineChart('monthlyGoalConversion', data9[1]);
                        }
                    });


                    $.ajax({
                        type: "POST",
                        url: "/dashboard.html?block=10&startDate=" + startDate
                                + "&endDate=" + endDate + "&local=" + local,
                        processData: true,
                        data10: {},
                        dataType: "json",
                        success: function (data10) {
                            $("#table10 tr:nth-child(1) td:nth-child(1)").removeClass("data-loading");
                            var rows = "";
                            rows += " <tr><td colspan=\"4\"><span class=\"tableheader\"> Top used tools within Date Range</span><br>";
                            getMostUsedToolsTab("table10", data10[0], data10[1], rows);
                        }

                    });

                    //setDashboard();
                }

                function getMostUsedToolsTab(table10, tabDat, mostUsedTool, rows) {
                    total = 0.0;
                    if (tabDat != null && tabDat.length != null) {

                        for (var i = 0; i < tabDat.length; i++) {
                            total = total + parseInt(tabDat[i][1]);
                        }
                        rows += "<h1 id=\"mostSessions\" style=\"font-size: 10px\">Most Used Tool:&nbsp;" + mostUsedTool + "</h1></td></tr>" +
                                "<tr class=\"header\"><td align=\"left\" width=\"35%\">Tool Name</td> <td width=\"23%\">No. of times used </td><td width=\"42%\" colspan=\"2\">Percentage</td></tr>";
                        for (var i = 0; i < tabDat.length; i++) {
                            //             		alert("....."+tabDat[i][0]);
                            rows += "<tr><td>" + tabDat[i][0] + "</td><td>" + tabDat[i][1] + "</td>";
                            if (total != 0) {
                                var per = (parseInt(tabDat[i][1]) / total) * 100;
                                rows += "<td><div style=\"position: auto; width: " + per * 1.4 + "px; height: 15px; background-color:#4F81BD;\"></div></td>" +
                                        "<td><b>" + per.toFixed(0) + "</b> %</td>";
                            }
                            rows += "</tr>";
                        }
                    } else {
                        rows += "<tr><td colspan=\"4\">No Data Found</td></tr>";
                    }

                    document.getElementById(table10).innerHTML = rows;
                }

                function tableDataFiller(tableId, tableData) {
                    var values = tableData.toString().split(",");
                    var total = 0;
                    for (var i = 0; i < values.length; i++) {
                        total = total + parseInt(values[i]);
                    }
                    var table = document.getElementById(tableId);
                    var trs = table.getElementsByTagName('tr');
                    for (var i = 2; i < trs.length; i++) {
                        var tds = trs[i].getElementsByTagName('td');
                        tds[1].innerHTML = values[i - 2];
                        if (total != 0) {
                            var per = (parseInt(values[i - 2]) / total) * 100;
                            tds[2].innerHTML = "<div style=\"position: auto; width: " + per * 1.4 + "px; height: 15px; background-color:#4F81BD;\"></div>";
                            tds[3].innerHTML = "<b>" + per.toFixed(0) + "</b> %";
                        } else {
                            tds[2].innerHTML = "";
                            tds[3].innerHTML = "";
                        }
                    }
                }

                function tableHgt() {
                    var rgt = document.getElementById("righttab");
                    var lft = document.getElementById("lefttab");
                    if (rgt.offsetHeight > lft.offsetHeight) {
                        lft.style.height = rgt.offsetHeight + "px";
                    } else {
                        rgt.style.height = lft.offsetHeight + "px";
                    }
                }
            </script>

            <style type="text/css">
                #resultTable {
                    display: block;
                    border: 1px solid silver;
                    padding: 5px;
                    line-height: 24px;
                    background-color: #FFFFFF;
                }

                #resultTable h2 {
                    display: inline;
                }
                /*-------------------------*/
                .newandreturn {
                    height: 10px;
                    width: 10px;
                }

                .dashboardTables {
                    border: 1px solid #999999;
                    font-size: 11px;
                    line-height: 18px;
                }

                .dashboardTables tr td {
                    border: 0px;
                    border-left: 1px solid;
                    border-top: 1px solid;
                    border-color: #eeeeee;
                }

                .dashboardTables tr td.middleCol {
                    background-color: #e2e2e2;
                    text-align: center;
                    font-size: 11px;
                    font-weight: bold;
                }

                .dashboardTables tr.header {
                    background-color: #B6DDE8;
                    text-align: center;
                    font-weight: bold;
                }

                .dashboardTables tr:first-child td:first-child span {
                    font-size: 16px;
                    color: #111111;
                }

                .dashboardTables#table1,.dashboardTables#table3,.dashboardTables#table4
                {
                    border-bottom-style: solid;
                    border-left-style: none;
                    border-top-style: none;
                    border-right-style: none;
                    border-width: 1px;
                    line-height: 18px;
                    border-color: #999999;
                }

                .dashboardTables#table1 tr td,.dashboardTables#table3 tr td,.dashboardTables#table4 tr td
                {
                    border-left-style: solid;
                    border-top-style: solid;
                    border-right-style: solid;
                    border-width: 1px;
                    border-color: #999999;
                }

                .heading2 {
                    font-size: 11px;
                    font-weight: bold;
                    color: #F96003;
                    /*padding-left: 15px;*/
                }

                .brn {
                    border-right: 0px none;
                    border-style-right: none;
                    width: 34%;
                }

                .bln {
                    border-left: 0px none;
                    border-style-left: none;
                    width: 8%;
                    text-align: right;
                    padding-right: 4px;
                }

                #noToolUseVisits {
                    font-size: 10px;
                    padding-top: 24px;
                    text-align: right;
                    padding-right: 6px;
                }

                .data-loading {
                    background-image: url('/css/0/images/orange-loader .gif');
                    background-repeat: no-repeat;
                    background-position: calc(225px) center;
                }
                /*-----------------------*/
            </style>


        </head>
        <body onload="">
            <div class="responsive-wrapper">
                <div class="middle-wrapper">
                    <div class="" id="content">
                        <form:form commandName="dashboard" method="POST">
                            <div id="tabs-icons">
                                <ul>
                                    <li id="dashBoard"><a href="#tabs-icons-1" name="tabs-icons"
                                                          class="ui-state-default ui-corner-top ui-state-active" 
                                                          onmouseover="this.style.cursor = 'pointer'" 
                                                          onclick="naviToOtherTabs('dashBoard')">Dashboard</a></li>
                                    <li id="reports"><a href="#tabs-icons-2" name="tabs-icons"
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'"
                                                        onclick="naviToOtherTabs('reports');" >Reports</a></li>
                                    <li id="autoMail"><a href="#tabs-icons-3" name="tabs-icons"
                                                         class="ui-state-default ui-corner-top"
                                                         onmouseover="this.style.cursor = 'pointer'"
                                                         onclick="naviToOtherTabs('autoMail')" >Automail</a></li>
                                    <li id="setting"><a href="#tabs-icons-4" name="tabs-icons"
                                                        class="ui-state-default ui-corner-top"
                                                        onmouseover="this.style.cursor = 'pointer'"
                                                        onclick="naviToOtherTabs('settings')" >Settings</a></li>
                                    <li id="knowledgeBase"><a href="#tabs-icons-4" name="tabs-icons"
                                                              class="ui-state-default ui-corner-top"
                                                              onmouseover="this.style.cursor = 'pointer'"
                                                              onclick="naviToOtherTabs('knowledgeBase')" >Knowledge Base</a></li>
                                    <li id="expertNaswers"><a href="#tabs-icons-4" name="tabs-icons"
                                                              class="ui-state-default ui-corner-top ui-tabs-selected" 
                                                              onmouseover="this.style.cursor = 'pointer'" 
                                                              onclick="naviToOtherTabs('expertAnswer')">Expert Answer</a></li>
                                    <li id="expertuser"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('expertUser')" >Expert User</a></li>
                                    <li id="atereports"><a href="#tabs-icons-4" name="tabs-icons"
                                                           class="ui-state-default ui-corner-top"
                                                           onmouseover="this.style.cursor = 'pointer'"
                                                           onclick="naviToOtherTabs('ateReports')" >ATE Reports</a></li>
                                    <li id="pluginAdoption"><a href="#tabs-icons-4" name="tabs-icons"
                                                               class="ui-state-default ui-corner-top"
                                                               onmouseover="this.style.cursor = 'pointer'"
                                                               onclick="naviToOtherTabs('pluginAdoption')" >LXRPlugin</a></li>
                                </ul>
                                <div id="resultTable">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                        <tr height=40px>
                                            <td align="left" width="50%"><span class="subheader">Dashboard
                                                    Overview</span></td>
                                            <td align="right" width="50%"><span class="heading2">Select
                                                    Date Range </span><select id="dateRangeOption" Style="width: 150px; font-size: 12px;">

                                                    <option value="1">Today</option>
                                                    <option value="2">Yesterday</option>
                                                    <option value="3">Last 7 Days</option>
                                                    <option value="4">Last 30 Days</option>
                                                    <option value="6">Lst 60 Days</option>
                                                    <option value="5">Last 90 Days</option>
                                                    <!--                    <option value="6">Till Date</option> -->
                                                    <option value="-1">Custom</option>
                                                </select> <input type="checkbox" id="includeLocal" /><span
                                                    class="heading2"> Include Local Users</span></td>
                                        </tr>
                                        <tr height=35px>
                                            <td></td>
                                            <td align="right">
                                                <div id="cDateRange" style="display: none;">
                                                    <table>
                                                        <tr>
                                                            <td id="date">From: <input id="fromDate" readonly="true"
                                                                                       size="12" /></td>
                                                            <td id="date">To: <input id="toDate" readonly="true"
                                                                                     size="12" /></td>
                                                            <td><input id="go" type="button" value="Go" size="4"
                                                                       onclick="gocall();" /></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td>
                                                <table width="100%" id="lefttab">
                                                    <tr>
                                                        <td style="padding-top: 0px; padding-bottom: 10px">
                                                            <table id="table1" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td style="border-right: 0px;"><span
                                                                            class="tableheader"> % of New Visits / Total Visits
                                                                        </span><img class="helpImg" alt="Help"
                                                                                    title="New Visit : First visit to LXRMarketplace from a new user. &#013;Returning Visit : If a new user(in selected date range) visits again or a previous user visits in selected date range."
                                                                                    src="../css/0/images/help-icon2.png">
                                                                        <h1 id="totalVisitor"
                                                                            style="font-size: 10px; padding-left: 3px;">Total
                                                                            number of visits:</h1></td>
                                                                    <td style="border-left: 0px;"><h1 id="noToolUseVisits">No
                                                                            tool usage visits:</h1></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="40%" style="border-right: none;" class="brn1"><div
                                                                            id="newVisitors"></div></td>
                                                                    <td width="60%" style="border-left: none;" class="bln1"><div
                                                                            id="newVisitPer">
                                                                            <img src="../css/0/images/newvisit.jpeg"
                                                                                 class="newandreturn" alt="" title="" /> <span
                                                                                 class="subheader"></span><br>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;<span></span>
                                                                        </div> <br>
                                                                        <div id="retVisitPer">
                                                                            <img src="../css/0/images/returnvisit.jpeg"
                                                                                 class="newandreturn" alt="" title="" /> <span
                                                                                 class="subheader"></span><br>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;<span></span>
                                                                        </div></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;">
                                                            <table id="table2" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td colspan="2" class="data-loading"><span
                                                                            class="tableheader"> User Behavior</span>
                                                                        <h1 style="font-size: 10px"></h1></td>
                                                                    <td colspan="2" style="border: 0px;">
                                                                        <h1
                                                                            style="font-size: 10px; padding-top: 25px; text-align: right;"></h1>
                                                                    </td>
                                                                </tr>
                                                                <tr class="header">
                                                                    <td align="left" width="34%">Users Type</td>
                                                                    <td width="23%">No. of Users</td>
                                                                    <td width="42%" colspan="2">Percentage</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Users <img style="vertical-align: middle;"
                                                                                         src="../css/0/images/help-icon2.png"
                                                                                         title="Number of visitors who used one or more tools in the selected date range."
                                                                                         alt="Help" class="helpImg">
                                                                    </td>
                                                                    <td class="middleCol" align="center"></td>
                                                                    <td class="brn" width="35%"></td>
                                                                    <td class="bln" width="7%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>New Users <img style="vertical-align: middle;"
                                                                                       src="../css/0/images/help-icon2.png"
                                                                                       title="Number of users who have used at least one tool for the very first time in the selected date range."
                                                                                       alt="Help" class="helpImg">
                                                                    </td>
                                                                    <td class="middleCol" align="center"></td>
                                                                    <td class="brn" width="35%"></td>
                                                                    <td class="bln" width="7%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Repeating Users <img
                                                                            style="vertical-align: middle;"
                                                                            src="../css/0/images/help-icon2.png"
                                                                            title="Number of users who have used at least one tool in selected date range and also have toolusage prior to the selected date range."
                                                                            alt="Help" class="helpImg">
                                                                    </td>
                                                                    <td class="middleCol" align="center"></td>
                                                                    <td class="brn" width="35%"></td>
                                                                    <td class="bln" width="7%"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="padding-bottom: 10px;">
                                                            <table id="table3" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td><span class="tableheader"
                                                                              style="line-height: 18px;"> Goal Conversion Rate <img
                                                                                class="helpImg" alt="Help"
                                                                                title="Goal Conversion Rate is the percentage of Signups which resulted in the Activation of their account."
                                                                                src="../css/0/images/help-icon2.png"
                                                                                style="vertical-align: middle;">
                                                                        </span><br> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><div style="padding-top: 10px;"
                                                                             id="goalConversion"></div></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table id="table4" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td><span class="tableheader"> % of Users used
                                                                            the tool <img class="helpImg" alt="Help"
                                                                                          title="Provides the Percentage of distinct users who used the tool among all the users who logged in in selected duration."
                                                                                          src="../css/0/images/help-icon2.png"
                                                                                          style="vertical-align: middle;">
                                                                        </span><br>
                                                                        <h1 id="mostUsedTool" style="font-size: 10px">Most
                                                                            used tool:</h1></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%"><div id="numoftimestoolused"></div></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-bottom: 10px;">
                                                            <table id="table9" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td><span class="tableheader"
                                                                              style="line-height: 18px;"> Monthly Users <img
                                                                                class="helpImg" alt="Help"
                                                                                title="Monthly user Signups vs Activations"
                                                                                src="../css/0/images/help-icon2.png"
                                                                                style="vertical-align: middle;">
                                                                        </span><br></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><div style="padding-top: 10px;"
                                                                             id="monthlyGoalConversion"></div></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                            <td>
                                                <table width="100%" id="righttab">
                                                    <tr>
                                                        <td style="padding-top: 0px;">
                                                            <table id="table5" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td colspan="4"><span class="tableheader">
                                                                            Number of Tools Used</span><br>
                                                                        <h1 id="mostNoUsed" style="font-size: 10px">Most
                                                                            number of tools used:</h1></td>
                                                                </tr>
                                                                <tr class="header">
                                                                    <td align="left" width="28%">No. of Tools Used</td>
                                                                    <td width="30%">No. of Users</td>
                                                                    <td width="42%" colspan="2">Percentage</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>1 Tool</td>
                                                                    <td class="middleCol" align="center"></td>
                                                                    <td class="brn" width="35%"></td>
                                                                    <td class="bln" width="7%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2 Tools</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3 Tools</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4 - 6 Tools</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>6+ Tools</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Visitors</td>
                                                                    <td class="middleCol" align="center"></td>
                                                                    <td class="brn" width="35%"></td>
                                                                    <td class="bln" width="7%"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 10px;">
                                                            <table id="table6" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="3" width="100%">
                                                                <tr>
                                                                    <td colspan="4"><span class="tableheader"> Time
                                                                            Spent per Session </span><br>
                                                                        <h1 id="mostTimeSpent" style="font-size: 10px">Most
                                                                            time spent:</h1></td>
                                                                </tr>
                                                                <tr class="header">
                                                                    <td align="left" width="28%">Session Duration</td>
                                                                    <td width="30%">Number of sessions with this duration</td>
                                                                    <td width="42%" colspan="2">Percentage</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>0-60 seconds</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>61-300 seconds</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>301-600 seconds</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>601-1200 seconds</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>1200+ seconds</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 10px;">
                                                            <table id="table7" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td colspan="4"><span class="tableheader">
                                                                            Number of Sessions within Date Range</span><br>
                                                                        <h1 id="mostSessions" style="font-size: 10px">Most
                                                                            No. of Sessions:</h1></td>
                                                                </tr>
                                                                <tr class="header">
                                                                    <td align="left" width="28%">No. of Sessions</td>
                                                                    <td width="30%">No. of users with this session blocks</td>
                                                                    <td width="42%" colspan="2">Percentage</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>1 Session</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2-3 Sessions</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4-6 Sessions</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>7-10 Sessions</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>10+ Sessions</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 10px; padding-bottom: 0px">
                                                            <table id="table8" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td colspan="4"><span class="tableheader">Time
                                                                            Duration between Last Visit &amp; Now</span><br>
                                                                        <h1 id="prevVisit" style="font-size: 10px">For most
                                                                            visits the previous visit happened:</h1></td>
                                                                </tr>
                                                                <tr class="header">
                                                                    <td align="left" width="28%">Day since last visit</td>
                                                                    <td width="30%">Total visits</td>
                                                                    <td width="42%" colspan="2">Percentage</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Less than 1 day</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>1 - 7 days</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>7 - 15 days</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>15 - 30 days</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>More than 30 days</td>
                                                                    <td class="middleCol"></td>
                                                                    <td class="brn"></td>
                                                                    <td class="bln"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 10px;">
                                                            <table id="table10" class="dashboardTables" cellspacing="0"
                                                                   cellpadding="2" width="100%">
                                                                <tr>
                                                                    <td class="data-loading"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </form:form>
                    </div>

                </div>
            </div>
            <%@ include file="/WEB-INF/view/footer.jsp"%>
        </body>
    </html>