<%@ include file="/WEB-INF/view/include.jsp"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/css/0/core.css" />
<%--<script type="text/javascript" src="/scripts/jquery-1.6.2.min.js"></script>  --%>
<script type="text/javascript" src="/scripts/ajax.js"></script>
<script src="/scripts/jquery/jquery-1.6.2.js"></script>
<script type="text/javascript" src="/scripts/core.js"></script>
<script type="text/javascript" src="/scripts/Charts.js"></script>

<link rel="stylesheet" href="/scripts/jquery/jquery.ui.all.css">
<script src="/scripts/jquery/jquery.ui.core.js"></script>
<script src="/scripts/jquery/jquery.ui.widget.js"></script>
<script src="/scripts/jquery/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="/scripts/jquery/demos.css">

<script>
	$(function() {
		$( "#fromDate" ).datepicker({ minDate: -10, maxDate: "+1M +10D" });		
		$( "#toDate" ).datepicker({ minDate: -10, maxDate: "+1M +10D" });
		
		$( "#fromDate" ).datepicker( "option", "minDate", new Date(2011, 11 - 1, 25) );  
		$( "#fromDate" ).datepicker( "option", "maxDate", new Date(2011, 12 - 1, 25) );
		
		$( "#toDate" ).datepicker( "option", "minDate", new Date(2011, 11 - 1, 25) );  
		$( "#toDate" ).datepicker( "option", "maxDate", new Date(2011, 12 - 1, 25) );
		
		
	});
	</script>
<script type="text/javascript">

$(document).ready(function() {
	
	
	$("#home").bind('click', function() {
		 var f = $("#cpc");
		   f.attr('action',  "lxrmarketplace.html");
		   f.submit();
       signupPage.src = "/feedback.html"
   });
	$("#back").bind('click', function() {
	     var f = $("#cpc");
		  // f.attr('action',  "cpc.html?loadChartsDesc=loadChartsDesc");
		   f.attr('action',  "cpcDescription.html?loadChartsDesc=loadChartsDesc");
		  
		   f.submit();
   });
	
	$("#getResult").bind('click', function() {
		//alert("bind");
		
		updateData('loadCPCChart', $('#fromDate').val(), $('#toDate').val(),'');
  });
	
});

function chartDisplay(){
<%			HttpSession session = request.getSession(false);
			StringBuilder xml = (StringBuilder) session
					.getAttribute("cpcCompXmlData");
			if (xml != null) {
				out.println("xmlCpcData=\"" + xml.toString() + "\";");
			}%>
		loadCharts('CompChart', xmlCpcData);
	}

	function loadCharts(divName, xmlData) {
		//alert(xmlData);
		var chart = new FusionCharts(
				"FusionCharts/FCF_MSLine.swf?ChartNoDataText=No data available for this "
						+ "" + " account.", "chart3Id", "690", "400");
		chart.addParam("WMode", "Transparent");
		chart.setDataXML(xmlData);
		chart.render(divName);
	}
	
	function callAjax() {
		updateData('loadCPCChart', $('#fromDate').val(), $('#toDate').val(),'');
}

</script>

<title>Holiday Retail CPC Index | CPC Index | LXRMarketplace.com</title>
<meta name="description"
	content="The Holiday Retail CPC Index will help you make the best decisions regarding your paid search advertising." />

<meta name="keywords"
	content="Avg CPC, CPC Index, Holiday Retail CPC Index, Google CPC, Bing CPC, Holiday CPC Index" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26359564-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta name="google-site-verification"
	content="feMMk_4sqErYGfNaVquEEDMtQ_Ud4uW3k-xFfCi_XoY" />
<!--<meta name="msvalidate.01" content="48A7DE0876C5AC6C15A29761DD44137F" />-->
</head>
<body
	onload="resizeIframeMain('mainDiv', 1),resizeDiv(),chartDisplay(),loadSelTool(),setHeight();">
	<form:form commandName="cpc">
		<div id="PageCnt" width="100%" cellspacing="0" cellpadding="0">
			<div>
				<%@ include file="/WEB-INF/view/Header.jsp"%>
			</div>
			<div id="BodyCnt" style="width: 960px; margin: 0 auto">
				<div id="BCnt"
					style="width: 960px; margin: 0 auto; width: 955px; height: 100%;">
					<div id="mainDiv"
						style="font-size: 11px; width: 715px; position: absolute; background-color: #fff;">
						<div class="portlet">
							<table cellpadding="0" cellspacing="0"
								style="width: 715px; height: 44px; line-height: 44px;">
								<tr>
									<td>
										<h1 style="padding-left: 20px;">
											HOLIDAY RETAIL SEM INDEX <img
												style="vertical-align: middle; left: 158px"
												src="../css/0/images/help_icon.gif" class="helpImg"
												alt="Help"
												title="How much will paid search advertising cost you this holiday season compared to 2010? NetElixir’s Holiday Retail CPC Index is a daily tracking widget of average cost per click of both Google & Bing for 11 different retail categories. The widget compares daily CPC’s to last year’s daily CPC’s from November 25-December 25. Use the Holiday Retail CPC Index daily as a guide to make the best decisions for your paid search advertising campaigns. Get the most bang for your buck this Holiday Season!." />
										</h1>
									</td>
									<td class="linkText">
										<h1>
											<%--	<a target="_blank" href="Keyword_combinator.pdf"
											style="margin-right: 5px; color: #F96003">Quick Start
											Guide</a>| --%>

											<span id="home" style="margin-right: 5px">Home</span>| <span
												id="back">Back</span>
										</h1>
									</td>
								</tr>
							</table>
							<div class="portlet-content shadow"
								style="width: 705px; padding: 0 0 0 10px; margin: 0">


								<div class="demo">
									<p style="vertical-align: middle;">
										From Date: <input class="inputBox" type="text" id="fromDate"
											name="fromDate"
											value=<%out.println(session.getAttribute("fromDate"));%>>
										To Date: <input class="inputBox" type="text" id="toDate"
											name="toDate"
											value=<%out.println(session.getAttribute("toDate"));%>>&nbsp;&nbsp;
										<img style="vertical-align: middle; cursor: pointer;"
											id="getResult" alt="Get Result" name="image"
											src="../css/0/images/getresult.png"></img>

										<%-- <input style="vertical-align: middle;" id="getResult" type="image" alt="Get Result" name="image" src="../css/0/images/getresult_gif.png" > --%>
										<%-- <input type="button" value="go" onclick="callAjax()"> --%>
									</p>
								</div>

								<div id="CompChart" style="padding: 0px 0px 0px 0px;"></div>
							</div>
						</div>
					</div>

					<div id="rightcont"
						style="display: inline-block; top: 275px; background-color: #FFFFFF; width: 235px; float: right;">
						<iframe name="signupPage" id="signupPage" frameborder="0"
							src="/feedback.html" scrolling="no" width="100%" height="100%"
							tabindex="-1">I frame is not supporting</iframe>
						<div style="background-color: #FFFFFF">
							<%--   <%@ include file="/WEB-INF/view/OtherTools.jsp" %> --%>
							<jsp:include page="/WEB-INF/view/OtherTools.jsp">
								<jsp:param name="tool" value="cpcIndex.html" />
							</jsp:include>
						</div>
					</div>
				</div>

			</div>
			<div id="fCnt"
				style="margin-top: 15px; width: 100%; vertical-align: middle; background-color: #000000; position: absolute;">
				<table width="100%">
					<tr class="copyrightText">
						<td align="right" width="30%">&copy; Copyright NetElixir 2011
							| <a href="PrivacyPolicy.jsp" target="_blank">Privacy Policy</a>
							| <a href="TermsofService.jsp" target="_blank">Terms of
								Service</a> | <a href="http://netelixirblog.wordpress.com/"
							target="_blank">Blog</a>
						</td>
						<td align="center" width="10%">Follow us on <%--   <a href="http://www.facebook.com/netelixir" target="_blank"> --%>
							<a
							href="http://www.facebook.com/pages/Lxrmarketplace/150549935043293"
							target="_blank"> <img src="../css/0/images/facebook.png"
								alt="" width="16" height="16" hspace="5" />
						</a> <a href="http://twitter.com/#!/LXRMarketplace" target="_blank">
								<img src="../css/0/images/twitter.png" alt="" width="16"
								height="16" hspace="5" />
						</a>
						</td>
						<td align="center" width="36%">Powered by: <a
							href="http://www.netelixir.com" target="_blank">NetElixir,
								Inc.</a>
						</td>
					</tr>
				</table>
				<%-- <div
                style="width: 100%; border: 0px; float: left; padding-left: 10px; background-color: #000000">

                <span
                    style="width: 550px; height: 31px; line-height: 31px; float: left; margin: 0 auto;"
                    class="copyrightText">
                </span> <span style="width: 137px;  line-height: 31px;" class="copyrightText">  </span> <span style="width: 173px; padding-left: 80px;  line-height: 31px;"
                    class="copyrightText">Go to <a
                    href="http://www.netelixir.com" target="_blank">www.netelixir.com</a>
                </span>
            </div>
          --%>
			</div>

		</div>
	</form:form>
</body>
</html>