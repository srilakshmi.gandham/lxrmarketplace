<%@ include file="/WEB-INF/view/include.jsp"%>
<%@ page import="lxr.marketplace.user.Login"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CPC Index | Holiday Retail CPC Index | LXRMarketplace.com</title>
<meta name="description"
	content="LXRMarketplace’s Holiday Retail CPC Index – a daily tracker from Nov. 25-Dec. 25 of this year’s Google & Bing average CPC’s compared to 2010 for 11 different retail categories!." />

<meta name="keywords"
	content="CPC Index, Holiday Retail CPC Index, Google CPC, Bing CPC, Avg CPC, Holiday CPC Index" />

<link rel="stylesheet" type="text/css" href="/css/0/core.css" />
<script type="text/javascript" src="../scripts/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/scripts/core.js"></script>
<script type="text/javascript">
        $(document).ready(function(){   
             document.getElementById("otherfreetools").style.height = 206 +"px";
             //alert(document.getElementById("rightcont").offsetHeight);
            <%HttpSession session = request.getSession();
            Login user = (Login) session.getAttribute("user");%>
         /*   $("#slider").easySlider({
                auto: false, 
                continuous: false
            }); */
            resizeIframeMain("descCont", 1);  
            var signupPage = parent.document.getElementById("signupPage");
            var iframe = parent.document.getElementById("landingPage");
            
            $("#cpcFreeAccess, #cpcImg").bind('click', function(e) {               
            	var iframe = parent.document.getElementById('loginPage');
                var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
				var login = innerDoc.getElementById('loginDiv').style.display;
                if(login=='none'){
		   var f = $("#signup");
		   f.attr('action',  "cpc.html?loadCharts=loadCharts");
		   f.submit();
                }else{
                   alert("Please sign in if already a member.\n\nNew users please register.");
                   e.preventDefault();
            }
            });   
            
            $("#home").bind('click', function() {
		       var f = $("#signup");
		   f.attr('action',  "lxrmarketplace.html");
		   f.submit();
                <%if (user != null) {%>
                     signupPage.src = "/feedback.html";
                <%} %>
            });
            
            $("#back").bind('click', function() {
		       var f = $("#signup");
		   f.attr('action',  "lxrmarketplace.html");
		   f.submit();
                <%if (user != null) {%>
	                signupPage.src = "/feedback.html"
	            <%} %>
            });
        }); 
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26359564-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta name="google-site-verification"
	content="feMMk_4sqErYGfNaVquEEDMtQ_Ud4uW3k-xFfCi_XoY" />
<!--<meta name="msvalidate.01" content="48A7DE0876C5AC6C15A29761DD44137F" />-->

</head>
<body onload="resizeDiv(),loadSelTool(),setHeight();">
	<form:form commandName="signup" method="POST">
		<input id="toolDesc" type="hidden" value="cpcDescriptionIndex.html" />
		<div id="PageCnt" width="100%" cellspacing="0" cellpadding="0">
			<div>
				<%@ include file="/WEB-INF/view/Header.jsp"%>
			</div>
			<div id="BodyCnt" style="margin: 0 auto;">
				<div id="BCnt" style="margin: 0 auto; width: 955px; height: 100%;">
					<div id="descCont"
						style="width: 715px; margin: 0 auto; position: absolute; background-color: #FFFFFF;">

						<table style="width: 715px; height: 44px; line-height: 44px;"
							cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">
									<h1 style="padding-left: 20px;">HOLIDAY RETAIL SEM INDEX</h1>
								</td>
								<td align="right">
									<h1>
										<span class="linkText"
											style="position: relative; float: right;"> <span
											id="home" style="margin-right: 5px">Home</span>| <span
											id="back">Back</span>
										</span>
									</h1>
								</td>
							</tr>
						</table>
						<div class="shadow">
							<div style="display: inline; margin: 0; padding: 0">
								<div class="separotor"
									style="width: 240px; display: inline; margin: 0; padding: 0">
									<div class="separotor" style="width: 240px; padding: 10px;">
										<p style="margin: 0 0 0 10px">
											<img id="cpcImg"
												src="../css/0/images/HolidayIndexWidget180x143static.gif"
												style="cursor: pointer; width: 75px; height: 75px"
												alt="HOLIDAY RETAIL SEM INDEX"
												title="HOLIDAY RETAIL SEM INDEX" />
										</p>
										<h2>Description :</h2>
										<p class="content"
											style="padding-top: 3px; margin-top: 0px; text-align: justify;">
											How much will paid search advertising cost you this holiday
											season compared to 2010? NetElixir’s Holiday Retail CPC Index
											is a daily tracking widget of average cost per click of both
											Google & Bing for 11 different retail categories. The widget
											compares daily CPC’s to last year’s daily CPC’s from November
											25-December 25. Use the Holiday Retail CPC Index daily as a
											guide to make the best decisions for your paid search
											advertising campaigns. Get the most bang for your buck this
											Holiday Season!</p>

										<%-- 	<h1><i> Happy Holidays! Happy Selling!</i></h1> --%>
										<img alt="Happy Selling" src="../css/0/images/Happy.PNG" />
										<%-- <p class="heading2" style="padding-bottom: 0px; margin-bottom: 0px;">Key Features :</p>
										<h2>Key Features :</h2>
										<p class="content" style="padding-top: 3px; margin-top: 0px;">
											The ROI Calculator is customized to two versions - a regular
											version calculating revenue & cost, and a more advanced
											version determining ROI from orders, AOV, CPC & clicks. Both
											are quick, simple, effective & free!</p>
										<%-- <p class="heading2" style="padding-bottom: 0px; margin-bottom: 0px;">Benefits :</p>
										<h2>Benefits :</h2>
										<p class="content" style="padding-top: 3px; margin-top: 0px;">An
											easy way to determine your marketing campaign's profitability
											without sacrificing time and energy into complex
											calculations.</p> --%>
										<p style="margin-left: 10px">
											<a id="cpcFreeAccess" href="#"
												style="text-decoration: none; font-size: 12px"> <b>Access
													Live Holiday CPC Index Tracker>></b></a>
											<%-- 	<input type="image" id="cpcFreeAccess"
												src="../css/0/images/free-access-bu1.jpg" name="image2"
												alt="Free Access" title="Free Access" /> --%>
										</p>
									</div>
								</div>
								<div
									style="display: inline-block; position: absolute; left: 265px; top: 120px;">
									<div id="slider">
										<ul>
											<li><img style="border: 1px solid silver"
												src="../css/0/images/screenshots/CPCSShot.png"
												alt="ROI Calcolator" title="HOLIDAY RETAIL SEM INDEX" /></li>
											<%-- <li><img style="border: 1px solid silver"
												src="../css/0/images/screenshots/rsz_roi_2.bmp"
												alt="ROI Calcolator" title="ROI Calcolator" /></li> --%>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="rightcont"
						style="display: inline-block; top: 275px; background-color: #FFFFFF; width: 235px; float: right;">
						<%if(user != null) {%>
						<iframe name="signupPage" id="signupPage" frameborder="0"
							src="/feedback.html" scrolling="no" width="100%" height="100%"
							tabindex="-1">I frame is not supporting</iframe>
						<%}else{%>
						<iframe name="signupPage" id="signupPage" frameborder="0"
							src="/signup.html" scrolling="no" width="100%" height="100%"
							tabindex="-1">I frame is not supporting</iframe>
						<%}%>
						<div id="otherfreetools" style="background-color: #FFFFFF">
							<jsp:include page="/WEB-INF/view/OtherTools.jsp">
								<jsp:param name="tool" value="cpcDescriptionIndex.html" />
							</jsp:include>
						</div>
					</div>
				</div>
			</div>
			<div>
				<%@ include file="/WEB-INF/view/Footer.jsp"%>
			</div>
		</div>
	</form:form>

</body>
</html>