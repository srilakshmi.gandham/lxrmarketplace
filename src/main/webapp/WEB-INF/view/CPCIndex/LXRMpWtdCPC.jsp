<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/css/0/core.css" />
<%--<script type="text/javascript" src="/scripts/jquery-1.6.2.min.js"></script>  --%>
<script type="text/javascript" src="/scripts/ajax.js"></script>
<script src="/scripts/jquery/jquery-1.6.2.js"></script>
<script type="text/javascript" src="/scripts/core.js"></script>
<script type="text/javascript" src="/scripts/Charts.js"></script>
<script type="text/javascript" src="/scripts/autoNumeric.js"></script>

<link rel="stylesheet" href="/scripts/jquery/jquery.ui.all.css">
<script src="/scripts/jquery/jquery.ui.core.js"></script>
<script src="/scripts/jquery/jquery.ui.widget.js"></script>
<script src="/scripts/jquery/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="/scripts/jquery/demos.css">

<script>
	$(function() {
		$( "#fromDate" ).datepicker({ minDate: -10, maxDate: "+1M +10D" });
		$( "#fromDate" ).datepicker( "option", "minDate", new Date(2011, 11 - 1, 25) );  
		$( "#fromDate" ).datepicker( "option", "maxDate", new Date(2011, 12 - 1, 25) );		
	});
	</script>

<script type="text/javascript">
$(document).ready(function() {
		$("#getResult").bind('click', function() {
		insertCpcData('insertData',$('#fromDate').val(), $('#Adwords').val(), $('#Bing').val(),$('#code').val());
  });
		
	jQuery(function($) {
			$('#Bing,#Adwords').autoNumeric({
				aSep : '',
				mRound: 'U',
				vMax: '99.99',
				aPad: false,
				aDec: '.',
				altDec: '.'
			});
		});
});
</script>
<title>Insert title here</title>
</head>
<body>
	<div class="demo">
		<p style="vertical-align: middle;">
		<table>
			<tr>
				<td>Date:</td>
				<td><input class="inputBox" type="text" id="fromDate"
					name="fromDate"></td>
			</tr>
			<tr>
				<td>Adwords_wt_cpc :</td>
				<td><input class="inputBox" type="text" id="Adwords"
					name="Adwords"></td>
			</tr>
			<tr>
				<td>Bing_wt_cpc :</td>
				<td><input class="inputBox" type="text" id="Bing" name="Bing">
				</td>
			</tr>
			<tr>
				<td>Security Code :</td>
				<td><input class="inputBox" type="password" id="code"
					name="code"></td>
			</tr>
			<tr>
				<td><img style="vertical-align: middle; cursor: pointer;"
					id="getResult" alt="Get Result" name="image"
					src="../css/0/images/submit.gif"></img></td>
			</tr>
		</table>
		<div id="errorDiv"></div>
	</div>
</body>
</html>