
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Login | LXRMarketplace</title>
        <script type="text/javascript">

            $(document).ready(function () {
                var isLoggedInUserId = 0;
                var lg = <%=userLoggedInH%>;
                var uname = "";
                var pwd = "";
                <%if(userH != null){%>
                    isLoggedInUserId = <%=userH.getId()%>;
                    uname = "<%=userH.getUserName()%>";
                    pwd = "<%=userH.getPassword()%>";
                <%}%>
                if(!lg && isLoggedInUserId > 0)
                {
                    $.ajax({
                        type: "POST",
                        url: "/default-login.html?userId="+isLoggedInUserId,
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            if(data[0] === "success")
                            {
                                defaultLogin();
                            }
                        }
                    });
                }
                
                function defaultLogin(){
                    pwd = encodeURIComponent(pwd);
                    $.ajax({
                        type: "POST",
                        url: "/login.html?popuplogin='popuplogin'&username=" + uname + "&password=" + pwd +"",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            changeHeaders("subHeader1", "<%=uName%>");
                            changeH("mobile", "<%=uName%>");
                            getNotificationCount();
                        }
                    });                    
                }

                $("#mUsername").keypress(function (e) {
                    if (e.keyCode === 13) {
                        $("#password").focus();
                    }
                });

                $("#mLogInPassword").keypress(function (e) {
                    if (e.keyCode === 13) {
                        login();
                    }
                });
                parentUrl = top.location.href;

                $('#mSign').click(function () {
                    loginSubmit();
                });
                function loginSubmit() {

                    var username = $.trim($("#mUsername").val());
                    var password = $.trim($("#mLogInPassword").val());
                    var isLoginRemember = "false";
                    if($("#isLoginRemember").prop("checked") === true){
                        isLoginRemember = "true";
                    }
                    if (username === "" || username === "Email") {
                        alert("Please enter your email address.");
                        $("#mUsername").focus();
                        return false;
                    } else {mobileUser.html
                        if (!isValidMail(username)) {
                            alert("Please enter a valid email address.");
                            $("#mUsername").focus();
                            return false;
                        }
                    }
                    if (!$("#mLogInPassword").val()) {
                        $("#mLogInPassword").focus();
                        return false;
                    }

            <%String mQueryStringL = (String) request.getParameter("getResult");
                    String mQueryString2L = (String) request.getParameter("backLink");
                    String mParUrlL = "";
                    if ((mQueryStringL != null && !mQueryStringL.trim().equals("")) || (mQueryString2L != null && !mQueryString2L.trim().equals(""))) {
                mParUrlL = "sucPage";
                    }%>
                    $.ajax({
                        type: "POST",
                        url: "/login.html?popuplogin='popuplogin'&username=" + username + "&password=" + password +"&isLoginRemember=" + isLoginRemember + "&page=<%=mParUrlL%>",
                        processData: true,
                        data: {},
                        dataType: "json",
                        success: function (data) {
                            var error = document.getElementById('error');
                            error.innerHTML = "";
                            if (data[0] === "success") {
                                if (parentUrl.indexOf("getResult") !== -1) {
                                    parent.window.location = parentUrl + "&loginRefresh=1";
                                } else {
                                    parent.window.location = parentUrl;
                                }

                            }else if (data[0] === "dashboard"){
                                parent.window.location = data[1];
                            } else {
                                error.innerHTML = data[0];

                            }
                        }
                    });
                }

                function isValidMail(email) {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
                    return filter.test(email);
                }
            });
                
        </script>

    </head>
    <body>
        <div class='m-wrapper' style='display:none;'>
            <p class="m-close"><img src='images/close.png' alt='close'/></p>
        <input type="text" id="mUsername" name="email" value="" placeholder='Email'>
        <input type="password" id="mLogInPassword" name="pwd" value="" placeholder='Password'>
        <div id="error" class="inValid Regular color2" style=""></div>
        <div class="isLoginRemember">
            <input type="checkbox" id="isLoginRemember" checked><span class="Regular color2" style="color:#FFF">Remember me</span>
        </div>
        <div id="mforgot-login-Password">
            <span id="mLogInPassword" class="Regular color2" ><a href="/forgotpassword.html" target="_parent" style="color:#FFF">Forgot password? </a></span>
        </div>

        <input type='button' name='signin' id='mSign'/>
        </div>
    </body>
</html>

