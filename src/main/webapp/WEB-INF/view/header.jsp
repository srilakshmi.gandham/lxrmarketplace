
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="lxr.marketplace.admin.expertuser.ExpertUser"%>
<%@page import="lxr.marketplace.user.Login"%>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta name="google-site-verification" content="hJPH7XX6JUMR5V_sGmO8u72_sK8rjsgeVdVUf7dM5RQ" />

<script>

    $(document).ready(function () {

    <%
        HttpSession sessionH = request.getSession();
        Login userH = (Login) sessionH.getAttribute("user");
        Object ulH = sessionH.getAttribute("userLoggedIn");
        boolean userLoggedInH = false;
        String uName = "", role = "";

        long messageUserID = 0l;
        boolean isUserAdmin = false;

        if (ulH != null) {
            userLoggedInH = (Boolean) ulH;
            if (userH != null) {
                uName = userH.getName();
                messageUserID = userH.getId();
                isUserAdmin = userH.isAdmin();
            }
        }
        if (userLoggedInH) {
            if (sessionH.getAttribute("expertUser") != null) {
                role = ((ExpertUser) sessionH.getAttribute("expertUser")).getRole();
            }
    %>

        changeHeaders("subHeader1", "<%=uName%>");
        changeH("mobile", "<%=uName%>");
        setAdminMenu(<%=userLoggedInH%>, <%=isUserAdmin%>);

        //Notification displaying
    <%if (sessionH.getAttribute("totalAnswersCount") != null && sessionH.getAttribute("totalUnViewedAnswersCount") != null) {
            int totalAnswersCount = (int) sessionH.getAttribute("totalAnswersCount");
            int totalUnViewedAnswersCount = (int) sessionH.getAttribute("totalUnViewedAnswersCount");
    %>
        showNotification("<%=totalAnswersCount%>", "<%=totalUnViewedAnswersCount%>");
    <%} else {%>
        getNotificationCount();
    <%}%>

    <%} else {%>
        changeHeaders("subHeader0", "");
        changeH("mobile-menunav", "");
    <%}%>

        $('#settings-div').hide();
        $('.settings').click(function () {
            $('#settings').css('width', '20%');
            $('#settings-div').show();
        });
        $(".settings").mouseleave(function () {
            $('#settings-li').css('width', '10%');
            $('#settings-div').hide();
        });

        $(document).mouseup(function (e) {
            var container = $("#login-div");
            var container1 = $("#sign-in");
            if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0 && !container1.is(e.target) // if the target of the click isn't the container...
                    && container1.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }

        });
        if ((window.location.href.indexOf("home") > -1) || (window.location.href.indexOf("#pluginHome") > -1)) {
            click();
        }
        $('.empty').css('width', '0');
        $('#sign-in').click(function () {
            click();
        });
        function click() {
            $('#login-div').toggle();
            $('#mlogin-div').hide();
            $('.m-wrapper').hide();
        }
        $('#m-login').click(function () {
            $('#mlogin-div').toggle();
            $('.m-wrapper').show();
            $('#mobile-menunav').addClass('test');
            $('#login-div').hide();
            $('.d-wrapper').hide();
        });
        $('.ma').click(function () {
            $('body').removeClass('cbp-spmenu-push-toright');
            $('nav').removeClass('cbp-spmenu-close cbp-spmenu-open');

        });

        $(".mob-menu-icon").click(function () {

        });

        $('.m-close').click(function () {
            $('#mlogin-div').hide();
            $('#mobile-menunav').removeClass('test');
        });

        $("#notificationList, #messageAlert, #notificationWrapper, #mobileNotification").click(function () {
            naviagtionToMessages(<%=userLoggedInH%>, <%=isUserAdmin%>);
        });

        $('#admin, #adminMob').click(function () {
            var role = "<%=role%>";
            parentUrl = window.location.protocol + "//" + window.location.host;
            if (role === 'reviewer' || role === 'expert') {
                parent.window.location = parentUrl + "/ate-question.html";
            } else {
                parent.window.location = parentUrl + "/dashboard.html";
            }

        });


    });

    function naviagtionToMessages(userLoggedInH, isUserAdmin) {
        if (userLoggedInH && !isUserAdmin) {
            parentUrl = window.location.protocol + "//" + window.location.host;
            parent.window.location = parentUrl + "/user-messages.html";
        }
    }
    function signOut() {
        parentUrl = top.location.href;
        $.ajax({
            type: "POST",
            url: "login.html?signout='signout'",
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                if (data[0] !== null) {
                    parent.window.location = data[0];
                } else {
                    parent.window.location = parentUrl;
                }
                sessionStorage.removeItem('name');
                sessionStorage.removeItem('email');
            }
        });
    }
    function changeHeaders(pageVal, userName) {
        if ((pageVal === 'subHeader1' && userName !== null)) {
            $('#uName').text(userName);
            $('#mob-uName').text(userName);
            document.getElementById('subHeader1').style.display = "block";
            document.getElementById('subHeader0').style.display = "none";
        } else {
            document.getElementById('subHeader1').style.display = "none";
            document.getElementById('subHeader0').style.display = "block";
        }
    }
    function changeH(pageVal, userName) {
        if (pageVal === 'mobile' && userName !== null) {
            $('#mob-uName').text(userName);
            document.getElementById('mobile').style.display = "block";
            document.getElementById('mobile-menunav').style.display = "none";
        } else {
            document.getElementById('mobile').style.display = "none";
            document.getElementById('mobile-menunav').style.display = "block";
        }
    }
    function signupLxrm() {
        parent.window.location = "/signup.html";

    }
    //Recent Questions
    function getNotificationCount() {
        var params = {userId: <%=messageUserID%>};
        jQuery.getJSON("/user-messages.html?check = unViewdAnswers", params,
                function (data) {
                    showNotification(data[0], data[1]);
                });
    }
    function showNotification(totalAnswersCount, totalUnViewedAnswersCount) {
        if (totalAnswersCount > 0) {
            $("#notificationList").show();
            $("#messageAlert").show();
            $("#mobileNotification").show();
            $("#alertSymbol").show();
            if (totalUnViewedAnswersCount > 0) {
                $("#messageAlert").text(totalUnViewedAnswersCount);
                $("#mobileNotification").text(totalUnViewedAnswersCount);
            }
        }
    }
    function setAdminMenu(userLoggedInH, isUserAdmin) {
        if (userLoggedInH && isUserAdmin) {
            $('#admin,#adminMob').show();
        }
    }
</script>
<!-- Mobile Menu Starts -->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <div id='mlogin-div' style='display:none;'>

        <div id='mlogin-form'>
            <%@ include file="/WEB-INF/view/mobileLogin.jsp"%>
        </div>
    </div>

    <div id='mobile-menunav'>
        <a href="/aboutUs.jsp" target="_blank">About Us</a>
        <a href="https://lxrmarketplace.com/blog/" target="_blank">Blog</a>
        <a href="/#seo-heading" class="ma">SEO</a>
        <a href="/#ppc-tools" class="ma">PPC</a>
        <a href="/#ecommerce-tools" class="ma">E-Commerce</a>
        <a class="mob-login" id='m-login' style='cursor:pointer;' title="SIGN IN">Sign In</a>
        <a class="mob-signup" onclick="signupLxrm()" title="FREE SIGN UP NOW">Free Sign Up Now</a>
    </div>
    <form id="navigationMobileMessages">  
        <div id='mobile'>
            <a href="/aboutUs.jsp" target="_blank">About Us</a>
            <a href="https://lxrmarketplace.com/blog/" target="_blank">Blog</a>
            <a href="/#seo-heading" class="ma">SEO</a>
            <a href="/#ppc-tools" class="ma">PPC</a>
            <a href="/#ecommerce-tools" class="ma">E-Commerce</a>
            <li id="adminMob" style="cursor: pointer;display: none"><a id="adminPanelMob">Admin Panel</a></li>
            <a class="username" style="color:#2c3427;" id="mob-uName"></a>
            <a style='cursor:pointer;' id="notificationWrapper"><div  id="mobileNotification"></div><img id="lXRMMobileNotification" src='/images/LXRMMobileNotification.png'></a>
            <a class="m-settings" style='cursor:pointer;' href="/change-password.html">Change Password</a>
            <a style="cursor: pointer;color:#5a8de0;" onClick="signOut();">Sign Out</a>
        </div>
    </form>
</nav> <!-- Mobile Menu Ends -->
<div class="container" id="main-div-wrap">
    <div class="main">
        <!-- Mobile Menu Button Stars -->
        <section class="buttonset">
            <img id="showLeftPush" class='mob-menu-icon' src="images/MM.png" alt="Menu"/>
        </section>
        <!-- Mobile Menu Button Ends -->
        <!-- Desktop Menu Start -->
        <div class="logo" id="logo">
            <a href="/"><img src='/resources/images/lxr-logo.png'></a>
        </div>

        <div class="navbar" id="subHeader0" style="display:none">
            <ul class="nav navbar-nav">
                <li><a href="/aboutUs.jsp" target="_blank">About Us</a></li>
                <li><a href="https://lxrmarketplace.com/blog/" target="_blank">Blog</a></li>
                <li><a href="/#seo-heading">SEO</a></li>
                <li><a href="/#ppc-tools">PPC</a></li>
                <li><a href="/#ecommerce-tools">E-Commerce</a></li>
                <li title="SIGN IN" class="login" id='sign-in'><a style='cursor:pointer;'>Sign In</a></li>
                <li title="FREE SIGN UP NOW" style="cursor: pointer;" class="signup" onclick="signupLxrm()"><a>Free Sign Up Now</a></li>
            </ul>
            <div id='login-div' class='login-divt' style='display: none;'>
                <div id='login-form'>
                    <div class='form-wrapper'>
                        <%@ include file="/WEB-INF/view/login.jsp"%>
                    </div>
                </div>
            </div>
        </div>

        <form id="navigationMessages">
            <div class="navbar" id="subHeader1" style="display:none">
                <ul class="nav navbar-nav">
                    <li id="about"><a href="/aboutUs.jsp" target="_blank">About Us</a></li>
                    <li id="blog"><a href="https://lxrmarketplace.com/blog/" target="_blank">Blog</a></li>
                    <li id="seo"><a href="/#seo-heading">SEO</a></li>
                    <li id="ppc"><a href="/#ppc-tools">PPC</a></li>
                    <li id="ecommerce"><a href="/#ecommerce-tools">E-Commerce</a></li>
                    <li id="admin" style="cursor: pointer;display: none"><a id="adminPanel">Admin Panel</a></li>
                    <li><a class="username" style="color:#2c3427;" id="uName"></a></li>
                    <li id="notificationList"><a><div id="messageAlert" class="notificationSymbol"></div><img src='/images/LXRMNotification.png' id="alertSymbol"></a></li>
                    <li class="settings">
                        <span class="header-settings">
                            <img class="simg" src="images/header/setting-black.png" alt="settings-img">
                        </span>
                        <div style="z-index: 9999;position: absolute;">
                            <span class="header-settings1" id='settings-div' style="display:none;width: 150px;">
                                <ul>
                                    <li><a style='cursor:pointer;' href="/change-password.html">Change Password</a></li>
                                </ul>
                            </span>
                        </div>
                    </li>
                    <li><a style="cursor: pointer;color:#5a8de0;padding-right: 0px;" onClick="signOut();">Sign Out</a></li>
                </ul>

            </div>
        </form>    
        <!-- Desktop Menu Ends -->
    </div> <!-- main Ends -->
