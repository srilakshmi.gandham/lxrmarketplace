function getElementLeft(Elem) {
    if (document.getElementById) {
        var elem = document.getElementById(Elem);
    } else if (document.all) {
        var elem = document.all[Elem];
    }
    if (elem != null) {
        var xPos = elem.offsetLeft;
        var check = xPos;
        tempEl = elem.offsetParent;
        while (tempEl != null) {
            xPos += tempEl.offsetLeft;
            tempEl = tempEl.offsetParent;
        }
        return xPos;
    }
}

function getElementTop(Elem) {
    if (document.getElementById) {
        var elem = document.getElementById(Elem);
    } else if (document.all) {
        var elem = document.all[Elem];
    }
    if (elem != null) {
        var yPos = elem.offsetTop;
        var check = yPos;
        tempEl = elem.offsetParent;
        while (tempEl != null) {
            yPos += tempEl.offsetTop;
            tempEl = tempEl.offsetParent;
        }
        return yPos;
    }
}

function LTrim(value) {
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}
// Removes ending whitespaces
function RTrim(value) {
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");
}
// Removes leading and ending whitespaces
function trim(value) {
    return LTrim(RTrim(value));
}




function whichBrs() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("opera") !== -1)
        return 'Opera';
    if (agt.indexOf("staroffice") !== -1)
        return 'Star Office';
    if (agt.indexOf("webtv") !== -1)
        return 'WebTV';
    if (agt.indexOf("beonex") !== -1)
        return 'Beonex';
    if (agt.indexOf("chimera") !== -1)
        return 'Chimera';
    if (agt.indexOf("netpositive") !== -1)
        return 'NetPositive';
    if (agt.indexOf("phoenix") !== -1)
        return 'Phoenix';
    if (agt.indexOf("firefox") !== -1)
        return 'Firefox';
    if (agt.indexOf("safari") !== -1)
        return 'Safari';
    if (agt.indexOf("skipstone") !== -1)
        return 'SkipStone';
    if (agt.indexOf("msie") !== -1)
        return 'Internet Explorer';
    if (agt.indexOf("netscape") !== -1)
        return 'Netscape';
    if (agt.indexOf("mozilla/5.0") !== -1)
        return 'Mozilla';
    if (agt.indexOf('\/') !== -1) {
        if (agt.substr(0, agt.indexOf('\/')) !== 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('\/'));
        } else
            return 'Netscape';
    } else if (agt.indexOf(' ') !== -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else
        return navigator.userAgent;
}

function showstars() {
    var state = document.getElementById("starrt");
    if (state.style.display === 'block') {
        document.getElementById("starrt").style.display = 'none';
    } else {
        document.getElementById("starrt").style.display = 'block';
    }
}

function isValidMail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
    return filter.test(email);
}

function isValidDomain(domainName) {
//    var domainfilter = /^((http|https|ftp):\/\/)?(www.)?([a-zA-Z0-9]+.)*[a-zA-Z0-9]+.(com|net|org|co(.[a-z0-9]+)*)$/;
    var domainfilter = /^((http|https):\/\/)?((?!-)[a-zA-Z0-9_][-a-zA-Z0-9]{0,62}(?<!-)\.)+([a-zA-Z]{2,6})?([\/a-zA-Z]{2,4})$/;
    return domainfilter.test(domainName);
}

function isNumberKey(e) {
    var browserName = navigator.appName;
    if (browserName === "Microsoft Internet Explorer") {
        isIE = true;
    } else {
        isIE = false;
    }
    keyEntry = !(isIE) ? e.which : event.keyCode;
    if ((keyEntry > 47 && keyEntry < 58) || keyEntry == 8 || keyEntry == 0 || (e.ctrlKey && keyEntry == 118)) {
        /*if($(".nonZero").val().length<9){
         return true;	
         }else{
         if(keyEntry==8||keyEntry==0) {
         return true;
         }else{
         return false;	
         }	    		
         }*/
        return true;
    } else {
        if (browserName === "Microsoft Internet Explorer") {
            event.returnValue = false;
        } else {
            return false;
        }
    }
    return true;
}
var digits = "0123456789";
var phoneNumberDelimiters = "()-.)(";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
function isInteger(s)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9")))
            return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{
    //alert(s);
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c !== " ")
            returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) === -1)
            returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone) {//alert(strPhone);
    var bracket = 3;
    strPhone = trim(strPhone);
    if (strPhone.indexOf("+") > 1)
        return false;
    if (strPhone.indexOf("-") !== -1)
        bracket = bracket + 1;
    if (strPhone.indexOf("(") !== -1 && strPhone.indexOf("(") > bracket)
        return false;
    var brchr = strPhone.indexOf("(");
    if (strPhone.indexOf("(") !== -1 && strPhone.charAt(brchr + 2) !== ")")
        return false;
    if (strPhone.indexOf("(") === -1 && strPhone.indexOf(")") !== -1)
        return false;
    s = stripCharsInBag(strPhone, validWorldPhoneChars);
    return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function ValidateForm() {
    var Phone = document.getElementById('phoneNo');
    if (checkInternationalPhone(Phone.value) === false) {
        alert("Please Enter a Valid Phone Number")
        Phone.value = "";
        Phone.focus();
        return false;
    }
    return true;
}


// if user tries to signup from download popup in Tool's success screens
function signup() {
    if (reportDownload === 1) {
        parentUrl += "&report=1";
    }
    if (downloadReportType !== null && downloadReportType !== "undefined" && downloadReportType !== "") {
        parentUrl += "&report-type=" + downloadReportType;
        /*Case: To forward opted price by user from login to inbound link checker tool controller*/
        if (toolDBId === 20 && selectedPrice > 0) {
                 parentUrl += "&selected-price=" + selectedPrice;
        }
    }

    $.ajax({
        type: "POST",
        url: "/save-success-page.json?successpage=" + parentUrl,
        processData: true,
        data1: {},
        dataType: "json",
        success: function (data) {
            alert(data);
            parent.window.location = data;
        }, error: function (e) {
            try {
                var statusCode = e.status;
                if (statusCode === 200) {
                    parent.window.location = "/signup.html";
                }
            } catch (err) {
            }
        }
    });


}

function selectOptCols() {
    $(function () {
        $("#videodiv").dialog({
            iframe: false,
            closeOnEscape: true,
            close: function (event, ui) {
                $("#videodiv").html(""); // stops video/audio on dialog close
            },
            modal: true,
            width: '590',
            position: 'center'
        });

        $("#videodiv").dialog({
            closeOnEscape: true
        });
    });
}


/* Functions For Video Tracking */
function videotrack() {
    $.ajax({
        type: "POST",
        url: "/videotracking.html?toolvideotrack=" + toolId,
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
        }
    });
}

function checkDomain(nname) {
    var arr = new Array(
            '.com', '.net', '.org', '.biz', '.coop', '.info', '.museum', '.name',
            '.pro', '.edu', '.gov', '.int', '.mil', '.ac', '.ad', '.ae', '.af', '.ag',
            '.ai', '.al', '.am', '.an', '.ao', '.aq', '.ar', '.as', '.at', '.au', '.aw',
            '.az', '.ba', '.bb', '.bd', '.be', '.bf', '.bg', '.bh', '.bi', '.bj', '.bm',
            '.bn', '.bo', '.br', '.bs', '.bt', '.bv', '.bw', '.by', '.bz', '.ca', '.cc',
            '.cd', '.cf', '.cg', '.ch', '.ci', '.ck', '.cl', '.cm', '.cn', '.co', '.cr',
            '.cu', '.cv', '.cx', '.cy', '.cz', '.de', '.dj', '.dk', '.dm', '.do', '.dz',
            '.ec', '.ee', '.eg', '.eh', '.er', '.es', '.et', '.fi', '.fj', '.fk', '.fm',
            '.fo', '.fr', '.ga', '.gd', '.ge', '.gf', '.gg', '.gh', '.gi', '.gl', '.gm',
            '.gn', '.gp', '.gq', '.gr', '.gs', '.gt', '.gu', '.gv', '.gy', '.hk', '.hm',
            '.hn', '.hr', '.ht', '.hu', '.id', '.ie', '.il', '.im', '.in', '.io', '.iq',
            '.ir', '.is', '.it', '.je', '.jm', '.jo', '.jp', '.ke', '.kg', '.kh', '.ki',
            '.km', '.kn', '.kp', '.kr', '.kw', '.ky', '.kz', '.la', '.lb', '.lc', '.li',
            '.lk', '.lr', '.ls', '.lt', '.lu', '.lv', '.ly', '.ma', '.mc', '.md', '.mg',
            '.mh', '.mk', '.ml', '.mm', '.mn', '.mo', '.mp', '.mq', '.mr', '.ms', '.mt',
            '.mu', '.mv', '.mw', '.mx', '.my', '.mz', '.na', '.nc', '.ne', '.nf', '.ng',
            '.ni', '.nl', '.no', '.np', '.nr', '.nu', '.nz', '.om', '.pa', '.pe', '.pf',
            '.pg', '.ph', '.pk', '.pl', '.pm', '.pn', '.pr', '.ps', '.pt', '.pw', '.py',
            '.qa', '.re', '.ro', '.rw', '.ru', '.sa', '.sb', '.sc', '.sd', '.se', '.sg',
            '.sh', '.si', '.sj', '.sk', '.sl', '.sm', '.sn', '.so', '.sr', '.st', '.sv',
            '.sy', '.sz', '.tc', '.td', '.tf', '.tg', '.th', '.tj', '.tk', '.tm', '.tn',
            '.to', '.tp', '.tr', '.tt', '.tv', '.tw', '.tz', '.ua', '.ug', '.uk', '.um',
            '.us', '.uy', '.uz', '.va', '.vc', '.ve', '.vg', '.vi', '.vn', '.vu', '.ws',
            '.wf', '.ye', '.yt', '.yu', '.za', '.zm', '.zw');

    var mai = nname.trim();
    var val = true;

    var dot = mai.lastIndexOf(".");
    var dname = mai.substring(0, dot);
    var ext = mai.substring(dot, mai.length);
    //alert(ext);

    if (dot > 2 && dot < 57)
    {
        for (var i = 0; i < arr.length; i++)
        {
            if (ext == arr[i])
            {
                val = true;
                break;
            } else
            {
                val = false;
            }
        }
        if (val == false)
        {
//	 	  	 alert("Your domain extension "+ext+" is not correct");
            return true;
        } else
        {
            for (var j = 0; j < dname.length; j++)
            {
                var dh = dname.charAt(j);
                var hh = dh.charCodeAt(0);
                if ((hh > 47 && hh < 59) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123) || hh == 45 || hh == 46)
                {
                    if ((j == 0 || j == dname.length - 1) && hh == 45)
                    {
                        alert("Domain name should not begin or end with '-'");
                        return false;
                    }
                }

            }
        }
    }


    return true;
}

function showOverviewPage() {
    if (document.getElementById("descDivId").style.display == 'none') {
        $("#toolLi").removeClass("selected");
        document.getElementById("toolDivId").style.display = 'none';
        document.getElementById("rateItPopUp").style.display = 'none';
        document.getElementById("descDivId").style.display = 'block';
        $("#overLi").addClass("selected");
        $("#toolLi a:first").css('color', '#5a5a5a');
    }

}

function showToolPage() {
    if (document.getElementById("toolDivId").style.display == 'none') {
        $("#overLi").removeClass("selected");
        document.getElementById("descDivId").style.display = 'none';
        document.getElementById("toolDivId").style.display = 'block';
        document.getElementById("rateItPopUp").style.display = 'block';
        $("#toolLi").addClass("selected");
        $("#toolLi a:first").css('color', '#fff');
    }

}

function openvideotutorial() {
    videotrack();
    $("#videoimage").hide();
    var state = document.getElementById("toolvideo");
    if (state.style.display == 'none') {
        document.getElementById("toolvideo").style.display = 'block';
    }
}

function changeHeaders(id1)
{
    document.getElementById(id1).style.display = 'block';

}




