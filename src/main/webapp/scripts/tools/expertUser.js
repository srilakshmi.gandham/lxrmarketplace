/* Validating the input fields.*/
function validateUserFeed(updating, parentUrl) {
    var isValidAll = true;
    var userName = $('#nameFeed').val().trim();
    var userEmail = $('#emailFeed').val().trim();
    var userRole = $("#roleFeed").val().trim();
    var userId = $('#expertIdFeed').val();
    if (userName === "" || userName === null) {
        $('#nameError').html("Please enter name.");
        $('#nameError').show();
        isValidAll = false;
    } else {
        $('#nameError').hide();
    }
    if (userEmail === "" || userEmail === null) {
        $('#emailError').html("Please enter email.");
        $('#emailError').show();
        isValidAll = false;
    } else if (!isValidMail(userEmail)) {
        $('#emailError').html("Please enter valid email address.");
        $('#emailError').show();
        isValidAll = false;
    } else {
        $('#emailError').hide();
    }
    if (userRole === "" || userRole === null) {
        $('#roleError').html("Please select role.");
        $('#roleError').show();
        isValidAll = false;
    } else {
        $('#roleError').hide();
    }
    if (isValidAll) {
//        console.log("name " + userName + " email" + userEmail + " role"
//                + userRole + " updatig" + updating);
        $("#userExisted").empty();
        $("#userExisted").hide();
        var feedParams = {name: userName, email: userEmail,
            role: userRole, updating: updating,
            expertId: userId};
        return saveExpertUserFeed(feedParams, parentUrl);
    } else {
        $("#userSubmit,#userCancel").attr('disabled', false);
        return false;
    }
}
/*Constructing Post request to save/edit user data.*/
function saveExpertUserFeed(feedParams) {
    $.post("/expert-user.html?requestMethod=adminUserFeed", feedParams).done(function (userData) {
//        console.log("data" + userData + " and 0 is " + userData[0]);
        if (userData[0] === "User Existed") {
            $("#userExisted").show();
            $("#userSubmit,#userCancel").attr('disabled', false);
            $("#userExisted").html("An expert user is already existed with same email.");
        } else if (userData[0] !== "User Existed") {
            parent.window.location = userData[1] + "/expert-user.html";
        }
    });
}
/*Constructing the request to fetch expert user's list.*/
function fetchExpertUsersData() {
    jQuery.getJSON("/expert-user.html?requestmethod=expertUsersList",
            function (data) {
                constructExpertUsersTable(data);
            });
}

/*Constructing table with expert user's list.*/
function constructExpertUsersTable(data) {
    var tableRows = null;
    if (data.length > 0) {
        $("#emptyUsersData").hide();
        $("#expertUserResultTable").show();
        var tableRow = "<thead><tr>";
        tableRow += "<th><span class=\"selectALLWrap\" >Select</span></th>";
        tableRow += "<th class=\"userFields\">Name</th>";
        tableRow += "<th class=\"userFields\">Email</th>";
        tableRow += "<th class=\"userFields\">Role</th>";
        tableRow += "</tr></thead><tbody>";
        tableRows += tableRow;
        for (var i = 0; i < data.length; i++) {
            tableRow = "<tr>";
            tableRow += "<td class=\"userSelectWrapper\"><input type=\"checkbox\" id=\"expertUser" + data[i].expertId +
                    "\" onClick=\"storeExpertUserIDsArray(" + data[i].expertId + ")\" name=\"selected_User\" value= \""
                    + data[i].expertId + "\" ></td>";
            tableRow += "<td><span class=\"expertUserData\" >" + data[i].name + "</span></td>";
            tableRow += "<td><span class=\"expertUserData\" >" + data[i].email + "</span></td>";
            tableRow += "<td><span class=\"expertUserData\" >" + data[i].role + "</span></td>";
            tableRow += "</tr>";
            tableRows += tableRow;
        }
        tableRows += "</tbody>";
        $("#expertUserResultTable").html(tableRows);
    } else {
        $("#emptyUsersData").show();
    }
}
/*Storing the selected expert users row id's to perform edit/delete user data.*/
function storeExpertUserIDsArray(id) {
    var status = document.getElementById("expertUser" + id + "");
    if (status !== null) {
        var isSelected = status.checked;
        if (isSelected) {
            var index = selectedExpertUsersArray.indexOf(id);
            if (index === -1) {
                selectedExpertUsersArray.push(id);
            }
        } else {
            var unCheckedindex = selectedExpertUsersArray.indexOf(id);
            if (unCheckedindex > -1) {
                selectedExpertUsersArray.splice(unCheckedindex, 1);
            }
        }
    }

}

/*Validating the selected rows by user to modify.*/
function modifyExpertUserData() {
    if (selectedExpertUsersArray.length === 0) {
        $(".userSelectWrapper :checked").each(function () {
            selectedExpertUsersArray.push($(this).val());
        });
    }
    //console.log("In mdfy selectedKnowledgeBaseArray is " + selectedExpertUsersArray + " 
    //and length" + selectedExpertUsersArray.length);
    if (selectedExpertUsersArray.length === 0) {
        alert("Please select expert user to modify.");
        $('#user_options').prop('selectedIndex', 0);
        selectedExpertUsersArray = [];
        selectedExpertUsersArray.length = 0;
        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
        return false;
    } else if (selectedExpertUsersArray.length >= 2) {
        alert("Please select single expert user to modify.");
        $('#user_options').prop('selectedIndex', 0);
        selectedExpertUsersArray = [];
        selectedExpertUsersArray.length = 0;
        $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
        return false;
    } else if (selectedExpertUsersArray.length === 1) {
        var finalExpertUserID = selectedExpertUsersArray[0];
        fetchExpertContent(finalExpertUserID);

    }
}
/*Constructing the request to fetch selected expert user data to perform an edit operation.*/
function fetchExpertContent(selectedExpertID) {
    var params = {selectedExpertID: selectedExpertID};
    jQuery.getJSON("/expert-user.html?requestmethod=viewExpertUser", params,
            function (data) {
                if (data !== null) {
                    $('#expertIdFeed').val(data.expertId);
                    $('#updatingFeed').val(true);

                    $("#nameFeed").val(data.name);
                    $("#emailFeed").val(data.email);
                    $("#emailFeed,#nameFeed").prop("readonly", true);
                    $("#roleFeed").val(data.role);

                    $("#dnld-ok-out-box").show();
                    $("#destOkUrlPopUp").show();
                    $('#downloadpopup').show();
//                    $('#dnld-pop-out-box').show();

                    $('.popUpWrapper').html("Edit User:");
                } else {
                    $("#dispalyNone").show();
                }
            });
}

/*Reseting the input fields in this page.*/
function resetUserFields() {
    $('input[type=text],textarea,select,hidden').each(function () {
        $(this).val('');
    });
    $('#updating').val(false);
    $('.error').hide();
    $(".userSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
    selectedExpertUsersArray = [];
    selectedExpertUsersArray.length = 0;
    $("#emailFeed,#nameFeed").prop("readonly", false);
    $("#userExisted").empty();
}
/*Validating the email  value field.*/
function isValidMail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
    return filter.test(email);
}
/*Navigation to dashboard.*/
function naviToOtherTabs(menuOption) {
    var menuParams = {menuOption: menuOption};
    $.post("/ate-question.html?action=menu", menuParams).done(function (data) {
        if (data !== null) {
            parent.window.location = data;
        }
    });
}