/*Constructs the ajax call request for selected filter query.*/
function constructFilteredQuery(filteredType, filteredQuery) {
    var params = {filteredType: filteredType, filteredQuery: filteredQuery};
    jQuery.getJSON("/ate-question.html?fetch=ate-questions", params,
            function (answersData) {
                if (answersData === null) {
                    $("#userATEQuestion").hide();
                    $("#emptyQuestionData").show();
                } else if (answersData !== null) {
                    constructQuestionList(answersData);
                }
            }).fail(function (error) {
                console.log("Cause Is" + error + " and Filter call is failed.");
    });
}
/*Constructs the user ate questions list in  table format.*/
function constructQuestionList(answersData) {
    var tableRows = null;
    if (answersData.length > 0) {
        $("#userATEQuestion").show();
        $("#emptyQuestionData").hide();
        var tableRow = "<thead><tr>";
        tableRow += "<th class=\"questionFields\">S.No</th>";
        tableRow += "<th class=\"questionFields\">Date</th>";
        tableRow += "<th class=\"questionFields\">Tool</th>";
        tableRow += "<th class=\"questionFields\">User Email ID</th>";
        tableRow += "<th class=\"questionFields\">User Question</th>";
        tableRow += "<th class=\"questionFields\">Expert Assignee</th>";
        tableRow += "<th class=\"questionFields\">Status</th>";
        tableRow += "<th class=\"questionFields\">Rating</th>";
        tableRow += "<th class=\"questionFields\">Edit</th>";
        tableRow += "</tr></thead><tbody>";
        tableRows += tableRow;
        for (var i = 0; i < answersData.length; i++) {

            var ratingClass;
            var ratingMsg;
            var rating = answersData[i].rating;
            if (parseInt(rating) === 2) {
                ratingClass = "poor";
                ratingMsg = "Poor";
            } else if (parseInt(rating) === 3) {
                ratingClass = "fair";
                ratingMsg = "Fair";
            } else if (parseInt(rating) === 4) {
                ratingClass = "good";
                ratingMsg = "Good";
            } else if (parseInt(rating) === 5) {
                ratingClass = "excellent";
                ratingMsg = "Excellent";
            } else {
                ratingClass = "notRated";
                ratingMsg = "-";
            }

            tableRow = "<tr>";
            tableRow += "<td><div class=\"rowCount\" >" + (i + 1) + "</div></td>";
            tableRow += "<td><div class=\"questionContent\" >" + answersData[i].date + "</div></td>";
            tableRow += "<td><div class=\"questionContent\" >" + answersData[i].toolName + "</div></td>";
            tableRow += "<td><div class=\"questionContent\" >" + answersData[i].userEmail + "</div></td>";
            var shortedQuestion = trimQuestionContent(answersData[i].question);
            tableRow += "<td><div class=\"questionContent\" >" + shortedQuestion + "</div></td>";
            tableRow += "<td><div class=\"questionContent\" >" + answersData[i].expertName + "</div></td>";
            tableRow += "<td><div class=\"questionContent\" >" + answersData[i].status + "</div></td>";
            tableRow += "<td><div class=\"" + ratingClass + "\" >" + ratingMsg + "</div></td>";
            tableRow += "<td><div class=\"editQuestion\" title =\"Edit\" onClick=\"editQuestion(" + answersData[i].questionId + ")\" ></div></td>";
            tableRow += "</tr>";
            tableRows += tableRow;
        }
        tableRows += "</tbody>";
        $("#userATEQuestion").html(tableRows);
    } else {
        $("#userATEQuestion").hide();
        $("#emptyQuestionData").show();
    }
}
/*Validate the inputs of search query.*/
function validateSearchCriteria() {
    var adminQuery = $("#serachCriteria").val();
    if ((adminQuery.trim() === null) || (adminQuery.trim() === "") || (adminQuery.trim() === " ")) {
        $("#serachCriteria").focus();
        $("#serachCriteria").addClass('inputErr');
        $("#alertError").show();
        return false;
    } else {
        var selectedOption = $("#expertAnswerFilter option:selected").val();
        constructFilteredQuery(selectedOption, adminQuery);
        $("#serachCriteria").removeClass('inputErr');
        $("#alertError").hide();

    }
}
//Navigates to edit answer page.
function editQuestion(questionId) {
    parent.window.location = parentURL+ "/ate-edit-question.html?question-id="+questionId;
}
//Navigates to edit answer page
function closeCriteria() {
    $('input[type=text]').each(function () {
        $(this).val('');
    });
}

// Retruns the question in short format.
function trimQuestionContent(userQuestion) {
    if (userQuestion.length >= 50) {
        return userQuestion.substr(0, 49) + "...";
    } else {
        return userQuestion;
    }
}

