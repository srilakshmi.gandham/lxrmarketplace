
/*Old method name is fetchMediaContentUpdateNewURL*/
function processSocialMediaUpdateURLS(e) {
    try {
        e.preventDefault();
        $("#settingsSucMsg").hide();
        $('#loadImage').show();
        $('#save').attr('disabled', true);
        $('#cancel').attr('disabled', true);
        var f = $("#socialMediaTool");
        f.attr('action', "/social-media-analyzer.html?update='update'");
        f.submit();
    } catch (err) {
    }
};

function getJSONData() {
    try {
        $.ajax({
            type: "POST",
            url: "/social-media-analyzer.html?getSocialData='getSocialData'",
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                var changeStatus = data["changeStatus"];
                if (data["socialResponse"]["hideChannelData"]["hideFacebook"] === false) {
                    faecBookChannel(data, changeStatus);
                    facebookLikesChart(data);
                } else {
                    $("#hideFacebook").show();
                    $("#hideFacebook").css("display", "block");
                }
                if (data["socialResponse"]["hideChannelData"]["hideTwitter"] === false) {
                    twitterChannelData(data, changeStatus);
                    twitterLikesChart(data);
                    twitterTweetsChart(data);
                    twitterFollowersChart(data);

                } else {
                    $("#hideTwitter").show();
                    $("#hideTwitter").css("display", "block");
                }
                HashTags(data);
                /*if (data["socialResponse"]["hideChannelData"]["hideInstagarm"] === false) {
                 instagramChannelData(data, changeStatus);
                 instaPostsChart(data);
                 instaFollowersChart(data);
                 } else {
                 $("#hideInstagram").show();
                 $("#hideInstagram").css("display", "block");
                 }*/
                if (data["socialResponse"]["hideChannelData"]["hidePinterest"] === false) {
//                    alert("p:"+data["socialResponse"]["hideChannelData"]["hidePinterest"]);
                    pinterestData(data, changeStatus);
                    pinBoardsChart(data);
                    /*pinLikesChart(data);*/
                    pinPostsChart(data);
                    pinFollowersChart(data);
                } else {
                    $("#hidePinterest").show();
                    $("#hidePinterest").css("display", "block");
                }
                if (data["socialResponse"]["hideChannelData"]["hideYoutube"] === false) {
                    youTubeData(data, changeStatus);
                    yTSubscribersChart(data);
                    yTVideosChart(data);
                    yTViewsChart(data);
                } else {
                    $("#hideYouTube").show();
                    $("#hideYouTube").css("display", "block");
                }
                if (data["socialResponse"]["hideChannelData"]["hideGoogle"] === false) {
                    googlePlusData(data, changeStatus);
                    gFollowersChart(data);
                } else {
                    $("#hideGoogle").show();
                    $("#hideGoogle").css("display", "block");
                }

            },
            complete: function () {}
        });
    } catch (err) {
    }
}
function showSocialCompDomainRow() {
    if (!(unsubscribe)) {
        if (hiddenDomArray.length > 0) {
            var state = document.getElementById("d" + hiddenDomArray[0]);
            if (state.style.display === 'none') {
                document.getElementById("d" + hiddenDomArray[0]).style.display = 'block';
                shownDomArray.push(hiddenDomArray[0]);
            }
            hiddenDomArray = hiddenDomArray.slice(1, hiddenDomArray.length);
        }
    }
}

function deleteSocialCompDomainRow(domainId) {
    if (!(unsubscribe)) {
        if (shownDomArray !== null && shownDomArray.length > 0) {
            for (var i = 0; i < shownDomArray.length; i++) {
                if (domainId == shownDomArray[i]) {
                    var state = document.getElementById("d" + shownDomArray[i]);
                    if (state.style.display === 'block') {
                        state.style.display = 'none';
                        var inputDomArray = state.getElementsByTagName('input');
                        inputDomArray[0].value = "";
                        hiddenDomArray.push(shownDomArray[i]);
                        shownDomArray.splice(i, 1);
                    }
                }
            }
        }

    }
}
function showImportTble() {
    if (notWorkingUrl === "Your URL is not working, please check the URL.") {
        $("#notwrk").show();
    }
    if (unableFeatch === "We are Unable to Retrive Infromation.") {
        $("#unable").show();
    }
}
function HashTags(data) {
    if (data !== null && data !== " ") {
        var tempCount = data["socialResponse"]["hashTagJSON"].length - 1;
        var emptyHashCount = data["socialResponse"]["hashTagJSON"][tempCount];
        if (emptyHashCount === 1) {
            $("#hashTagDiv").hide();
        } else {
            for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
                var hashTagDiv = "";
                hashTagDiv += '<div class="hashTagP">' + '<p class="hashTagUrl">' + data["socialResponse"]["urlsJSON"][mI] + '</p>';
                hashTagDiv += '<p class="hashTagContent">' + data["socialResponse"]["hashTagJSON"][mI] + '</p>';
                hashTagDiv += '</div>';
                $("#hashTagDiv").append(hashTagDiv);
            }
        }
    }
}
function  twitterChannelData(data, changeStatus) {
    var twitterTable = "";
    if (data !== null && data !== " ") {
        twitterTable += '<tr>';
        twitterTable += '<th>' + 'Metrics' + '</th>';
        for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
            twitterTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][mI] + '</span>' + '</th>';
        }
        twitterTable += '</tr>';


        if (changeStatus === "true") {
            twitterTable += '<tr><td>' + 'Followers (Change)' + '</td>';
        } else if (changeStatus === "false") {
            twitterTable += '<tr><td>' + 'Followers' + '</td>';
        }
        for (var fI = 0; fI < data["socialResponse"]["urlsJSON"].length; fI++) {
            twitterTable += '<td>' + data["socialResponse"]["twitterFinalJSON"]["followers"][fI].followers + '</td>';
        }

        twitterTable += '</tr>';

        if (changeStatus === "true") {
            twitterTable += '<tr><td>' + 'Tweets (Change)' + '</td>';
        } else if (changeStatus === "false") {
            twitterTable += '<tr><td>' + 'Tweets' + '</td>';
        }
        for (var tI = 0; tI < data["socialResponse"]["urlsJSON"].length; tI++) {
            twitterTable += '<td>' + data["socialResponse"]["twitterFinalJSON"]["tweets"][tI].tweets + '</td>';
        }
        twitterTable += '</tr>';


        if (changeStatus === "true") {
            twitterTable += '<tr><td>' + 'Likes (Change)' + '</td>';
        } else if (changeStatus === "false") {
            twitterTable += '<tr><td>' + 'Likes' + '</td>';
        }

        for (var lI = 0; lI < data["socialResponse"]["urlsJSON"].length; lI++) {
            twitterTable += '<td>' + data["socialResponse"]["twitterFinalJSON"]["likes"][lI].likes + '</td>';
        }
        twitterTable += '</tr>';

        if (changeStatus === "true") {
            twitterTable += '<tr><td>' + 'Following (Change)' + '</td>';
        } else if (changeStatus === "false") {
            twitterTable += '<tr><td>' + 'Following' + '</td>';
        }

        for (var fwI = 0; fwI < data["socialResponse"]["urlsJSON"].length; fwI++) {
            twitterTable += '<td>' + data["socialResponse"]["twitterFinalJSON"]["following"][fwI].follwoing + '</td>';
        }
        twitterTable += '</tr>';
        $("#twitterTable").append(twitterTable);
    }

}
function  faecBookChannel(data, changeStatus) {
    var faceBookTable = "";
    if (data !== null && data !== " ") {
        faceBookTable += '<tr>';
        faceBookTable += '<th>' + 'Metrics' + '</th>';

        for (var fbI = 0; fbI < data["socialResponse"]["urlsJSON"].length; fbI++) {
            faceBookTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][fbI] + '</span>' + '</th>';
        }

        faceBookTable += '</tr>';

        //faceBookTable+='<tr><td>'+'Likes'+'</td>';

        if (changeStatus === "true") {
            faceBookTable += '<tr><td>' + 'Likes (Change)' + '</td>';
        } else if (changeStatus === "false") {
            faceBookTable += '<tr><td>' + 'Likes' + '</td>';
        }

        for (var lI = 0; lI < data["socialResponse"]["urlsJSON"].length; lI++) {
            faceBookTable += '<td>' + data["socialResponse"]["facebookFinalJSON"]["fLikes"][lI].fLikes + '</td>';
        }
        faceBookTable += '</tr>';

        $("#faceBookTable").append(faceBookTable);
    }
}
function  instagramChannelData(data, changeStatus) {
    var instagramTable = "";
    if (data !== null && data !== " ") {
        instagramTable += '<tr>';
        instagramTable += '<th>' + 'Metrics' + '</th>';
        ;

        for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
            instagramTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][mI] + '</span>' + '</th>';
        }
        instagramTable += '</tr>';

        //instagramTable+='<tr><td>'+'Followers'+'</td>';

        if (changeStatus === "true") {
            instagramTable += '<tr><td>' + 'Followers (Change)' + '</td>';
        } else if (changeStatus === "false") {
            instagramTable += '<tr><td>' + 'Followers' + '</td>';
        }

        for (var fI = 0; fI < data["socialResponse"]["urlsJSON"].length; fI++) {
            instagramTable += '<td>' + data["socialResponse"]["instagramFinalJSON"]["iFollowers"][fI].iFollowers + '</td>';
        }

        instagramTable += '</tr>';

        //instagramTable+='<tr><td>'+'Posts'+'</td>';

        if (changeStatus === "true") {
            instagramTable += '<tr><td>' + 'Posts (Change)' + '</td>';
        } else if (changeStatus === "false") {
            instagramTable += '<tr><td>' + 'Posts' + '</td>';
        }

        for (var pI = 0; pI < data["socialResponse"]["urlsJSON"].length; pI++) {
            instagramTable += '<td>' + data["socialResponse"]["instagramFinalJSON"]["iPosts"][pI].iPosts + '</td>';
        }
        instagramTable += '</tr>';

        //instagramTable+='<tr><td>'+'Following'+'</td>';

        if (changeStatus === "true") {
            instagramTable += '<tr><td>' + 'Following (Change)' + '</td>';
        } else if (changeStatus === "false") {
            instagramTable += '<tr><td>' + 'Following' + '</td>';
        }

        for (var fwI = 0; fwI < data["socialResponse"]["urlsJSON"].length; fwI++) {
            instagramTable += '<td>' + data["socialResponse"]["instagramFinalJSON"]["iFollowing"][fwI].iFollowing + '</td>';
        }
        instagramTable += '</tr>';

        $("#instaGramaTable").append(instagramTable);
    }
}
function  pinterestData(data, changeStatus) {
    var pinterestTable = "";
    if (data !== null && data !== " ") {
        pinterestTable += '<tr>';
        pinterestTable += '<th>' + 'Metrics' + '</th>';

        for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
            pinterestTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][mI] + '</span>' + '</th>';
        }
        pinterestTable += '</tr>';

        //pinterestTable+='<tr><td>'+'Followers'+'</td>';

        if (changeStatus === "true") {
            pinterestTable += '<tr><td>' + 'Followers (Change)' + '</td>';
        } else if (changeStatus === "false") {
            pinterestTable += '<tr><td>' + 'Followers' + '</td>';
        }

        for (var pF = 0; pF < data["socialResponse"]["urlsJSON"].length; pF++) {
            pinterestTable += '<td>' + data["socialResponse"]["pinterestJSON"]["pFollowers"][pF].pFollowers + '</td>';
        }

        pinterestTable += '</tr>';

        //pinterestTable+='<tr><td>'+'Pins'+'</td>';

        if (changeStatus === "true") {
            pinterestTable += '<tr><td>' + 'Pins (Change)' + '</td>';
        } else if (changeStatus === "false") {
            pinterestTable += '<tr><td>' + 'Pins' + '</td>';
        }

        for (var pI = 0; pI < data["socialResponse"]["urlsJSON"].length; pI++) {
            pinterestTable += '<td>' + data["socialResponse"]["pinterestJSON"]["pPins"][pI].pPins + '</td>';
        }
        pinterestTable += '</tr>';

        //pinterestTable+='<tr><td>'+'Boards'+'</td>';

        if (changeStatus === "true") {
            pinterestTable += '<tr><td>' + 'Boards (Change)' + '</td>';
        } else if (changeStatus === "false") {
            pinterestTable += '<tr><td>' + 'Boards' + '</td>';
        }


        for (var pB = 0; pB < data["socialResponse"]["urlsJSON"].length; pB++) {
            pinterestTable += '<td>' + data["socialResponse"]["pinterestJSON"]["pBoards"][pB].pBoards + '</td>';
        }

        pinterestTable += '</tr>';

        //pinterestTable+='<tr><td>'+'Following'+'</td>';
        if (changeStatus === "true") {
            pinterestTable += '<tr><td>' + 'Following (Change)' + '</td>';
        } else if (changeStatus === "false") {
            pinterestTable += '<tr><td>' + 'Following' + '</td>';
        }

        for (var pF = 0; pF < data["socialResponse"]["urlsJSON"].length; pF++) {
            pinterestTable += '<td>' + data["socialResponse"]["pinterestJSON"]["pFollwoing"][pF].pFollowing + '</td>';
        }
        pinterestTable += '</tr>';
        $("#pinterestTable").append(pinterestTable);
    }
}
function  youTubeData(data, changeStatus) {
    var youTubeTable = "";
    if (data !== null && data !== " ") {
        youTubeTable += '<tr>';
        youTubeTable += '<th>' + 'Metrics' + '</th>';

        for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
            youTubeTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][mI] + '</span>' + '</th>';
        }
        youTubeTable += '</tr>';

        // youTubeTable+='<tr><td>'+'Subscribers'+'</td>';

        if (changeStatus === "true") {
            youTubeTable += '<tr><td>' + 'Subscribers (Change)' + '</td>';
        } else if (changeStatus === "false") {
            youTubeTable += '<tr><td>' + 'Subscribers' + '</td>';
        }

        for (var yS = 0; yS < data["socialResponse"]["urlsJSON"].length; yS++) {
            youTubeTable += '<td>' + data["socialResponse"]["youtubeFinalJSON"]["ySubscribers"][yS].ySubscribers + '</td>';
        }

        youTubeTable += '</tr>';

        //youTubeTable+='<tr><td>'+'Videos'+'</td>';

        if (changeStatus === "true") {
            youTubeTable += '<tr><td>' + 'Videos (Change)' + '</td>';
        } else if (changeStatus === "false") {
            youTubeTable += '<tr><td>' + 'Videos' + '</td>';
        }


        for (var yV = 0; yV < data["socialResponse"]["urlsJSON"].length; yV++) {
            youTubeTable += '<td>' + data["socialResponse"]["youtubeFinalJSON"]["yVideos"][yV].yVideos + '</td>';
        }
        youTubeTable += '</tr>';

        //youTubeTable+='<tr><td>'+'Views'+'</td>';

        if (changeStatus === "true") {
            youTubeTable += '<tr><td>' + 'Views (Change)' + '</td>';
        } else if (changeStatus === "false") {
            youTubeTable += '<tr><td>' + 'Views' + '</td>';
        }

        for (var yW = 0; yW < data["socialResponse"]["urlsJSON"].length; yW++) {
            youTubeTable += '<td>' + data["socialResponse"]["youtubeFinalJSON"]["yViews"][yW].yViews + '</td>';
        }

        youTubeTable += '</tr>';

        $("#youTubeTable").append(youTubeTable);
    }
}
function  googlePlusData(data, changeStatus) {
    var googleTable = "";
    if (data !== null && data !== " ") {
        googleTable += '<tr>';
        googleTable += '<th>' + 'Metrics' + '</th>';
        ;

        for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
            googleTable += '<th>' + '<span>' + data["socialResponse"]["urlsJSON"][mI] + '</span>' + '</th>';
        }
        googleTable += '</tr>';

        //googleTable+='<tr><td>'+'Followers'+'</td>';

        if (changeStatus === "true") {
            googleTable += '<tr><td>' + 'Followers (Change)' + '</td>';
        } else if (changeStatus === "false") {
            googleTable += '<tr><td>' + 'Followers' + '</td>';
        }

        for (var gPF = 0; gPF < data["socialResponse"]["urlsJSON"].length; gPF++) {
            googleTable += '<td>' + data["socialResponse"]["googleFinalJSON"]["gFollowers"][gPF].gFollowers + '</td>';
        }
        googleTable += '</tr>';

        $("#googleTable").append(googleTable);
    }
}

function facebookLikesChart(data) {
    try {
        if (data !== null && data !== " ") {
            var likesData = google.visualization.arrayToDataTable(data["socialResponse"]["facebookFinalJSON"]["fLikesGraphData"]);
            var likesView = new google.visualization.DataView(likesData);
            likesView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
            var options = {title: "Likes", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
            var facebookLikesChart = new google.visualization.ColumnChart(document.getElementById("fbLikesGraph"));
            facebookLikesChart.draw(likesView, options);
        }
    } catch (err) {
    }
}
function twitterFollowersChart(data) {
    try {
        if (data !== null && data !== " ") {
            var followersData = google.visualization.arrayToDataTable(data["socialResponse"]["twitterFinalJSON"]["twFollowersGraph"]);
            var followersView = new google.visualization.DataView(followersData);
            followersView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
            var options = {title: "Followers", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
            var twitterFollowersChart = new google.visualization.ColumnChart(document.getElementById("twFollowersGraph"));
            twitterFollowersChart.draw(followersView, options);
        }
    } catch (err) {
    }
}


function twitterTweetsChart(data) {
    try {
        if (data !== null && data !== " ") {
            var TweetsData = google.visualization.arrayToDataTable(data["socialResponse"]["twitterFinalJSON"]["twTweetsGraph"]);
            var TweetsView = new google.visualization.DataView(TweetsData);
            TweetsView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
            var options = {title: "Tweets", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
            var tweetsChart = new google.visualization.ColumnChart(document.getElementById("twTweetsGraph"));
            tweetsChart.draw(TweetsView, options);
        }
    } catch (err) {
        console.log(err);
    }
}

function twitterLikesChart(data) {
    try{
        if (data !== null && data !== " ") {
            var LikesData = google.visualization.arrayToDataTable(data["socialResponse"]["twitterFinalJSON"]["twLikesGraph"]);
            var LikesView = new google.visualization.DataView(LikesData);
            LikesView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
            var options = {title: "Likes", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
            var twLikesChart = new google.visualization.ColumnChart(document.getElementById("twLikesGraph"));
            twLikesChart.draw(LikesView, options);
        }
    }catch (err){}
}


function instaFollowersChart(data) {
    if (data !== null && data !== " ") {
        var inFollowersData = google.visualization.arrayToDataTable(data["socialResponse"]["instagramFinalJSON"]["iFollowersGraph"]);
        var inFollowersView = new google.visualization.DataView(inFollowersData);
        inFollowersView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Followers", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var instaFollowersChart = new google.visualization.ColumnChart(document.getElementById("instaFollowersGraph"));
        instaFollowersChart.draw(inFollowersView, options);
    }
}
function instaPostsChart(data) {
    if (data !== null && data !== " ") {
        var inPostsData = google.visualization.arrayToDataTable(data["socialResponse"]["instagramFinalJSON"]["iPostsGraph"]);
        var inPostsDataView = new google.visualization.DataView(inPostsData);
        inPostsDataView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Posts", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var inPostsChart = new google.visualization.ColumnChart(document.getElementById("instaPostsGraph"));
        inPostsChart.draw(inPostsDataView, options);
    }
}

function pinFollowersChart(data) {
    try{
        if (data !== null && data !== " ") {
            var pFollowersData = google.visualization.arrayToDataTable(data["socialResponse"]["pinterestJSON"]["pFollowersGraph"]);
            var pFollowersView = new google.visualization.DataView(pFollowersData);
            pFollowersView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
            var options = {title: "Followers", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
            var pinFollowersChart = new google.visualization.ColumnChart(document.getElementById("pinFollowersGraph"));
            pinFollowersChart.draw(pFollowersView, options);
        }
    }catch (err){}
}


function pinPostsChart(data) {
    try{
    if (data !== null && data !== " ") {
        var pinPostsData = google.visualization.arrayToDataTable(data["socialResponse"]["pinterestJSON"]["pPinsGraph"]);
        var pinPostsView = new google.visualization.DataView(pinPostsData);
        pinPostsView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Pins", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var pinPostsChart = new google.visualization.ColumnChart(document.getElementById("pinPostsGraph"));
        pinPostsChart.draw(pinPostsView, options);
    }}catch (err){}
}


/*Commented on Aug 09,2017
 * Likes metric is removed from Pinterest API
 * 
 * function pinLikesChart(data) {
 if (data !== null && data !== " ") {
 var pinLikesData = google.visualization.arrayToDataTable(data["socialResponse"]["pinterestJSON"]["pLikesGraph"]);
 var pinLikesView = new google.visualization.DataView(pinLikesData);
 pinLikesView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
 var options = {title: "Likes", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
 var pinLikesChart = new google.visualization.ColumnChart(document.getElementById("pinLikesGraph"));
 pinLikesChart.draw(pinLikesView, options);
 }
 }*/

function pinBoardsChart(data) {
    try{
    if (data !== null && data !== " ") {
        var pinBoardsData = google.visualization.arrayToDataTable(data["socialResponse"]["pinterestJSON"]["pBoardsGraph"]);
        var pinBoardsView = new google.visualization.DataView(pinBoardsData);
        pinBoardsView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Boards", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var pinBoardsChart = new google.visualization.ColumnChart(document.getElementById("pinBoardsGraph"));
        pinBoardsChart.draw(pinBoardsView, options);
    }}catch (err){}
}

function yTSubscribersChart(data) {
    try{
    if (data !== null && data !== " ") {
        var yTSubscribersData = google.visualization.arrayToDataTable(data["socialResponse"]["youtubeFinalJSON"]["ySubscribersGraph"]);
        var yTSubscribersView = new google.visualization.DataView(yTSubscribersData);
        yTSubscribersView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Subscribers", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var yTSubscribersChart = new google.visualization.ColumnChart(document.getElementById("yTSubscrbGraph"));
        yTSubscribersChart.draw(yTSubscribersView, options);
    }}catch (err){}
}

function yTVideosChart(data) {
    try{
    if (data !== null && data !== " ") {
        var yTVideosData = google.visualization.arrayToDataTable(data["socialResponse"]["youtubeFinalJSON"]["yVideosGraph"]);
        var yTVideosView = new google.visualization.DataView(yTVideosData);
        yTVideosView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Videos", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var yTVideosChart = new google.visualization.ColumnChart(document.getElementById("yTVideosGraph"));
        yTVideosChart.draw(yTVideosView, options);
    }}catch (err){}
}

function yTViewsChart(data) {
    try{
    if (data !== null && data !== " ") {
        var yTViewsData = google.visualization.arrayToDataTable(data["socialResponse"]["youtubeFinalJSON"]["yViewsGraph"]);
        var yTViewsView = new google.visualization.DataView(yTViewsData);
        yTViewsView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Views", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var yTViewsChart = new google.visualization.ColumnChart(document.getElementById("yTViewsGraph"));
        yTViewsChart.draw(yTViewsView, options);
    }}catch (err){}
}


function gFollowersChart(data) {
    try{
    if (data !== null && data !== " ") {
        var gFollowersData = google.visualization.arrayToDataTable(data["socialResponse"]["googleFinalJSON"]["gFollowersGraph"]);
        var gFollowersView = new google.visualization.DataView(gFollowersData);
        gFollowersView.setColumns([0, 1, {calc: "stringify", sourceColumn: 1, type: "string", role: "annotation"}]);
        var options = {title: "Followers", legend: {position: "none"}, width: 400, height: 250, bar: {groupWidth: "15%"}, hAxis: {}, vAxis: {}};
        var gFollowersChart = new google.visualization.ColumnChart(document.getElementById("gPFollowersGraph"));
        gFollowersChart.draw(gFollowersView, options);
    }}catch (err){}
}

function updateUlrResult() {
    $("#socialResult").hide();
    $("#mediaSettingsDiv").show();
    allowAccessButtons();
}

function disableAccessButtons() {
    $('#save').attr('disabled', true);
    $('#cancel').attr('disabled', true);
}

function disableCompDomainRow() {
    /*console.log("In disableCompDomainRow");*/
    if (hiddenDomArray.length > 0) {
        var state = document.getElementById("d" + hiddenDomArray[0]);
        if (state.style.display === 'block') {
            document.getElementById("d" + hiddenDomArray[0]).style.display = 'block';
            document.getElementById("d" + hiddenDomArray[0]).disabled = true;
            $("#d" + hiddenDomArray[0]).attr('disabled', true);
            shownDomArray.push(hiddenDomArray[0]);
        }
        hiddenDomArray = hiddenDomArray.slice(1, hiddenDomArray.length);
    }
}

function allowAccessButtons() {
    $('#save').attr('disabled', false);
    $('#cancel').attr('disabled', false);
}

function showAcessSettings() {
    $("#mediaSettingsDiv").show();
    $("#socialResult").hide();
    allowAccessButtons();
//    document.getElementById("updateUrl").readOnly = false;
}





function showHelpPopup(helpimgvalue, idvalue) {
    var repOs = $('#helpimg' + helpimgvalue).position();
    var rep = $('#helpimg' + helpimgvalue); //outerHeight(true)
    $('#helpImgToolTip' + idvalue).css({
        'left': repOs.left + rep.outerWidth(true),
        'top': repOs.top + rep.outerHeight(true),
        'position': 'absolute',
        'z-index': 1000,
        'display': 'block'
    });
}

function hideHelpPopup(helpimgvalue, idvalue, e) {
    if (($(e.target).parents('#helpImgToolTip' + idvalue).length === 0
            && !$(e.target).is('#helpImgToolTip' + idvalue) && !$(e.target)
            .is('#helpimg' + helpimgvalue))
            || $(e.target).is('#otherTr')) {
        $('#helpImgToolTip' + idvalue).hide();
        document.getElementById("helpImgToolTip" + idvalue).style.display = "none";
    }
}

function hideTempPopup(helpimgvalue, idvalue, e) {
    if (document.getElementById("helpImgToolTip" + idvalue).style.display === 'block') {
        document.getElementById("helpImgToolTip" + idvalue).style.display = "none";
    }
}

function clearForm() {
    $('#mediaUrl').val("");
    $('#comptetiorUrls').val("");
    var f = $("#socialMediaTool");
    f.attr('action', "/social-media-analyzer.html?query='clearAll'");
    f.submit();
}
function showOverviewPage() {
    if (document.getElementById("descDivId").style.display === 'none') {
        $("#toolLi").removeClass("selected");
        document.getElementById("toolDivId").style.display = 'none';
        document.getElementById("rateItPopUp").style.display = 'none';
        document.getElementById("descDivId").style.display = 'block';
        $("#overLi").addClass("selected");
        $("#toolLi a:first").css('color', '#5a5a5a');
    }
}
function showToolPage() {
    if (document.getElementById("toolDivId").style.display === 'none') {
        $("#overLi").removeClass("selected");
        document.getElementById("descDivId").style.display = 'none';
        document.getElementById("toolDivId").style.display = 'block';
        document.getElementById("rateItPopUp").style.display = 'block';
        $("#toolLi").addClass("selected");
        $("#toolLi a:first").css('color', '#fff');
    }
}



function setMediaURlStyle() {
    if ($("#mediaUrl").val() === "" || $('#mediaUrl').val() === null) {
        $("#mediaUrl").addClass('edit-field-overlayed');
        $("#mediaUrl").attr("value", yourDomain);
    }
}
function removeMediaURlStyle() {
    if ($("#mediaUrl").val() !== "" && $('#mediaUrl').val() === yourDomain) {
        $("#mediaUrl").val("");
        $("#mediaUrl").removeClass();
    }
}

function setCompStyle() {
    if ($("#comptetiorUrls").val() === "" || $('#comptetiorUrls').val() === null) {
        $("#comptetiorUrls").addClass('edit-field-overlayed');
        $("#comptetiorUrls").attr("value", compDomNote);
    }
}
function removeCompStyle() {
    if ($("#comptetiorUrls").val() !== "" && $('#comptetiorUrls').val().trim() === compDomNote) {
        $("#comptetiorUrls").val("");
        $("#comptetiorUrls").removeClass();
    }
}
