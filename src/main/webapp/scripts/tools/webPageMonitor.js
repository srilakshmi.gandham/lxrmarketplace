function clearForm(){
    	  $('#competitorUrl').val("");
	  $('#notwrk1,#notwrk').html("");
	  $("#email").val("");
	  $("#notwrk").hide();
	    $.ajax({
            type: "POST",  
            url: "/competitor-webpage-monitor-tool.html?clearAll='clearAll'"
   		});
	 
  }
  
  
  function showSett(){
            $("#tbas").removeClass('cmpunselect').addClass('comptabselect');
            $("#tbac").removeClass('comptabselect').addClass('cmpunselect');
            $("#tbal").removeClass('comptabselect').addClass('cmpunselect');
//            $("#linksdiv").hide();
            $("#settingsdiv").show();
//            $("#contentdiv").hide();
         }
        function GetHeight() {
            
            var firstDiv = document.getElementById("newLinks"); 
            var firstheight =  firstDiv.offsetHeight;
            var secHgt = 250;
            var tempHeight = 50;
            if(firstheight <= 50){
                firstDiv.style.height = tempHeight+"px";
            }else if(firstheight > 50 && firstheight < 250){
                    firstDiv.style.height = firstheight+"px";
            }else if(firstheight >= 250){
                    firstDiv.style.height = secHgt+"px";
            }
        }
        
        function GetDivHeight(contntID) {
            
            var secondDiv = document.getElementById(contntID);
           var secondheight =  secondDiv.offsetHeight;
           var secHgt = 250;
           var tempHeight = 50;
           if(secondheight <= 50){
//             alert(secondheight);
            secondDiv.style.height = tempHeight+"px";
            }else if(secondheight > 50 && secondheight < 250){
                secondDiv.style.height = secondheight+"px";
            }else if(secondheight >= 250){
                secondDiv.style.height = secHgt+"px";
           }
        }
       
      
         
        function drawChart(googleChart,type) {
            	if(googleChart.length==0){
        		if((googleChart[0].length==0)||(googleChart[1].length==0)){
        		$('#linkchartdiv').empty();
        		$('#contentchartdiv').empty();
        		$('<p class="NoData">No data available</p>').appendTo('#linkchartdiv');
                           $('<p class="NoData">No data available</p>').appendTo('#contentchartdiv');
                }
        	}else{
        				var LinkData = new google.visualization.DataTable();
				  		LinkData.addColumn('date', 'Day');
				  		LinkData.addColumn('number', 'New Internal Links');
				  		LinkData.addColumn('number', 'Removed Internal Links');
				  	
				  		var ContentData = new google.visualization.DataTable();
				  		ContentData.addColumn('date', 'Day');
				  		ContentData.addColumn('number', 'New Words');
				  		ContentData.addColumn('number', 'Removed Words');
				  		
				  		 var LinkArray = googleChart[0];
				  		var ContnetArray = googleChart[1];
				  		 
				  		
				  		 
				  		 var linkSet=[];
				  		 var contentSet=[];
                                                 var setMaxDate=new Date((LinkArray[LinkArray.length-1][0]));
                                                 if(type==2){
				  			 setMaxDate.setDate(setMaxDate.getDate()+6);
//                                                         console.log("setMaxDate"+setMaxDate+" and type is "+type);
                                                     }else{
                                                         setMaxDate.setDate(setMaxDate.getDate()+0);
//                                                         console.log("setMaxDate"+setMaxDate+" and type is "+type);
                                                     }
			            
			                for(var i =0; i<LinkArray.length; i++){
				                var objSetArray1 = [];
				                objSetArray1.push(new Date(LinkArray[i][0]));
				                objSetArray1.push(LinkArray[i][1]); 
				                objSetArray1.push(LinkArray[i][2]); 
				                linkSet.push(objSetArray1);
				            }
				            for(var j =0; j<ContnetArray.length; j++){
				            	var objSetArray2 = [];
				                objSetArray2.push(new Date(ContnetArray[j][0]));
				                objSetArray2.push(ContnetArray[j][1]); 
				                objSetArray2.push(ContnetArray[j][2]);
				                contentSet.push(objSetArray2);
				            }
//				             console.log("linkSet"+linkSet);
//                                              console.log("contentSet"+contentSet);
				          LinkData.addRows(linkSet);
				        ContentData.addRows(contentSet);
//				  		console.log("linkSet"+linkSet);
//				  		console.log("contentSet"+contentSet);
				  		
				  		 var options = {
				  	           width:550,
				  	           legend: 'bottom',
				  	         chartArea: {'width':"80%",right:"0%"},
				  	         hAxis: {
				  		    		format: 'MMM dd',
                                                                 maxValue:setMaxDate,
				                     gridlines: {color: 'transparent'}
				                   }
                                                   
				  	         };
				  		
				  		  var LinkChart =new google.visualization.ColumnChart(document.getElementById('linkchartdiv'));
				  					LinkChart.draw(LinkData, options);
					      
				    	  var Contentchart =new google.visualization.ColumnChart(document.getElementById('contentchartdiv'));
						    	  Contentchart.draw(ContentData, options);
	    	  
           			}/* End of Main Else */
	         }
                 
                 
                 function showHelpPopup(helpimgvalue, idvalue) {
		var repOs = $('#helpimg' + helpimgvalue).position();
		var rep = $('#helpimg' + helpimgvalue); //outerHeight(true)
		$('#helpImgToolTip' + idvalue).css({
			'left' : repOs.left + rep.outerWidth(true),
			'top' : repOs.top + rep.outerHeight(true),
			'position' : 'absolute',
			'z-index' : 1000,
			'display' : 'block'
		});
	}

	function hideHelpPopup(helpimgvalue, idvalue, e) {
		if (($(e.target).parents('#helpImgToolTip' + idvalue).length == 0
				&& !$(e.target).is('#helpImgToolTip' + idvalue) && !$(e.target)
				.is('#helpimg' + helpimgvalue))
				|| $(e.target).is('#otherTr')) {
			$('#helpImgToolTip' + idvalue).hide();
			document.getElementById("helpImgToolTip" + idvalue).style.display = "none";
		}
	}
   
   function hideTempPopup(helpimgvalue, idvalue, e){
		if(document.getElementById("helpImgToolTip"+idvalue).style.display=='block'){
			document.getElementById("helpImgToolTip"+idvalue).style.display="none";
		}
	}