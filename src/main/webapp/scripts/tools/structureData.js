/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function generateStructureDataScript() {
     resultLoginValue = true;
    var markupType = $("#markupType").val();
    var formatType = $("#formatType").val();
    $(".copy-text").show();

//    if (markupType === "jobPosting" && $("#bsMaxValue").val().trim() === ""){
//        $('#bsMinValue').attr('name', 'baseSalary.value');
//    } else {
//        $('#bsMinValue').attr('name', 'baseSalary.minValue');
//    }

    if (formatType === "") {
        alert("Please select the format type.");
        $("#formatType").focus();
        return false;
    } else if (markupType === "") {
        alert("Please select the markup type.");
        $("#markupType").focus();
        return false;
    } else if (!inputValidations(markupType)) {
        return false;
    }
    toolLimitExceeded(userLoggedIn);
    $(".copy-text").hide();
    $("#loadImage").show();
    $.ajax({
        type: "GET",
        url: "/structured-data-generator.html",
        processData: true,
        data: $("#json-data-form").serialize(),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data) {
            $("#loadImage").hide();
            if (data[0] === "json-ld")
            {
                $("#result-div").show();
                // using JSON.stringify pretty print capability:
                var str = JSON.stringify(data[1], undefined, 4);
                $("#jsondata").empty();
                // display pretty printed object in text area:
                $("#jsondata").text("<script type='application/ld+json'>\n" + str + "\n</" + "script>");
            } else if (data[0] === "microdata") {
                $("#result-div").show();
                $("#jsondata").empty();
                $("#jsondata").text("" + data[1]);
            } else {
                $("#jsondata").val("<p style='color:red;text-align:center;'>Unable to fetch the data. please try again.</p>");
            }
            $('html, body').animate({scrollTop: $('#result-div').offset().top - 50}, 'slow');
            getAskExpertPopup(true);
        }
    });
}

function clearForm() {
    window.location.href = "/structured-data-generator.html?clear=clear";
}

//Ask Expert Block
function getToolRelatedIssues() {
    $.ajax({
        type: "POST",
        url: "/structured-data-generator.html?ate=ate&domain=",
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data[0] !== null)
            {
                $("#tool-issues").html(data[0].issues);
                $("#help_domain").val(data[0].domain);
                issues = data[0].issues;
                question = data[0].question;
                toolName = data[0].toolName;
                otherInputs = data[0].otherInputs;
                /*Bottom Analysis*/
                $("#bottom_help_domain").val(data[0].domain);
            }
        }
    });
}

/*If user comes from mail link we are auto populating the inputs with their data 
 * and gives result for that request*/
function getUserToolInputs(website, userInput) {

}
function generateInputTagsForMarkups(markupType) {
    var inputs = "";
    var inputArray = {};
    var requiredInputsArray = {};
    if (markupType === "video") {
        inputArray = [1, 3, 2, 4, 51, 5, 6, 7, 8];
        requiredInputsArray = [1, 3, 2, 4];
    } else if (markupType === "article") {
        inputArray = [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 2];
        requiredInputsArray = [18, 19, 20, 21, 22, 23, 26, 28];
    } else if (markupType === "product") {
        inputArray = [1, 15, 2, 9, 10, 67, 63, 11, 12, 13, 49, 14, 16, 50];
        requiredInputsArray = [1, 15, 9];
    } else if (markupType === "recipe") {
        inputArray = [1, 28, 9, 26, 2, 30, 52, 31, 32, 34, 35, 38, 39, 36, 37];
        requiredInputsArray = [1, 9];
    } else if (markupType === "localBusiness") {
        inputArray = [1, 15, 23, 9, 2, 40, 41, 42, 43, 44, 45, 46, 47, 48, 100, 65];
        requiredInputsArray = [1, 15, 9, 40, 41, 42, 43, 44];
    } else if (markupType === "event") {
        inputArray = [1, 15, 2, 9, 53, 54, 55, 40, 41, 42, 43, 44];
        requiredInputsArray = [1, 15, 53, 55, 40, 41, 42, 43, 44];
    } else if (markupType === "website") {
        inputArray = [1, 66, 15, 100, 56, 57];
        requiredInputsArray = [1, 15];
    } else if (markupType === "aggregateRating") {
        inputArray = [64, 67, 68, 63];
        requiredInputsArray = [64, 67, 68, 63];
    } else if (markupType === "organization") {
        inputArray = [69, 15, 70, 40, 41, 42, 43, 44, 48, 71, 72];
        requiredInputsArray = [69, 15, 40, 41, 42, 43, 44];
    } else if (markupType === "jobPosting") {
//        inputArray = [73, 74, 75, 2, 76, 77, 78, 79, 80, 40, 41, 42, 43, 44, 81, 82, 84, 85, 86, 87, 88, 89];
//        requiredInputsArray = [73, 75, 2, 76, 77, 78, 79, 80, 40, 41, 42, 43, 44, 81, 82, 84];
        inputArray = [73, 74, 75, 2, 76, 77, 78, 79, 80, 40, 41, 42, 43, 44, 81, 82, 84];
        requiredInputsArray = [73, 75, 2, 76, 77, 78, 79, 80, 40, 41, 42, 43, 44, 81, 82, 84];
    }
    if (markupType === "breadcrumbs") {
        inputs += addInputsforBreadcrumbs();
    } else {
        inputs += getInputsForMarkupType(inputArray, requiredInputsArray, markupType);
    }
    return inputs;
}


function getInputsForMarkupType(inputsArray, requiredInputsArray, markupType) {
    var mainDivClass = "class='col-xs-12 mobile-padding-none input-div-margin'";
    var labelDivClass = "class='col-lg-2 label-div'";
    var labelHideDivClass = "class='visible-md visible-lg lxrm-empty-div'";
    var labelClass = "class='lxrm-bold'";

    var inputsMap = getMapOfInputs();
    var mainDivStart = '<div class="col-xs-12 mobile-padding-none input-div-margin">';
    var inputDivStart = '<div class="col-lg-10 input-div">';
    var lalabeDivlStart = '<div class="col-lg-2 label-div"><div class="visible-md visible-lg lxrm-empty-div"></div>';
//    var labelStart = '<h4 class="input-label"><div style="float:left;">';
//    var requiredIconStart = '<span class="required-icon input-label-info">';
    var labelsMap = getMapofLabels();
//    var requiredIconEnd = '</span>';
//    var infoIconStart = '<span class="input-info-icon Regular"><span class="input-info-text">';
//    var inputsInfoMap = getInputInfoMap(markupType);
//    var infoIconEnd = '</span></span>';
//    var labelEnd = '</div></h4>';
    var divEnd = '</div>';
    var inputs = "";
    var j = 0;
    for (var i = 0; i < inputsArray.length; i++) {
        if (inputsArray[i] === 100) {
//            inputs += '<div class="tool-input-info Bold color2 empty-div">' + labelStart + '<span class="input-info-text Regular">' + inputsInfoMap[100] + '</span>' + labelEnd + inputsMap[100] + divEnd;
        } else {
            inputs += mainDivStart;
            if (inputsArray[i] === requiredInputsArray[j]) {
                if (labelsMap[inputsArray[i]] !== "" && labelsMap[inputsArray[i]] !== undefined) {
                    inputs += lalabeDivlStart + "<p" + labelClass + ">" + labelsMap[inputsArray[i]] + "</p>" + divEnd;
                    j++;
//                } else {
//                    inputs += inputsMap[inputsArray[i]];
                }
            } else {
                if (labelsMap[inputsArray[i]] !== "" && labelsMap[inputsArray[i]] !== undefined) {
                    inputs += lalabeDivlStart + "<p" + labelClass + ">" + labelsMap[inputsArray[i]] + "</p>" + divEnd;
//                } else {
//                    inputs += inputsMap[inputsArray[i]];
                }
            }
            inputs += inputDivStart + inputsMap[inputsArray[i]] + divEnd;
            inputs += divEnd;
        }
    }
    return inputs;
}

function addInputsforBreadcrumbs() {
    var divStart = '<div class="tool-input-info Bold color2">';
    var labelStart = '<h4 class="input-label"><div style="float:left;">';
    var requiredIconStart = '<span class="required-icon input-label-info">';
    var requiredIconEnd = '</span>';
    var infoIconStart = '<span class="input-info-icon Regular"><span class="input-info-text">';
    var infoIconEnd = '</span></span>';
    var labelEnd = '</div></h4>';
    var divEnd = '</div>';
    var nameInfo = "The title of the breadcrumb displayed for the user.";
    var itemInfo = "An individual crumb in the breadcrumbs trail. It contains the @id (a unique URL) and the name property.";
    var titleLabelArr = ["Your homepage title:", "Your 2nd level title:", "Your 3rd level title:"];
    var urlLabelArr = ["URL for home page:", "URL for 2nd level page:", "URL for 3rd level page:", "Your current page title:"];
    var inputs = '<div class="tool-input-info1 Bold color2 breadcrumb-select">'
            + '<h4 class="format-label">Select Breadcrumb Level: </h4>'
            + '<select name="level" id="level" onChange="changeBreadcrumbLevel()">'
            + '<option value="2" selected>Two Levels</option><option value="3">Three Levels</option>'
            + '</select></div>';
    var level = $('#level option:selected').val();
    if (level === "" || level === undefined) {
        level = 2;
    }
    for (var i = 0; i < level; i++) {
        inputs += divStart + labelStart + requiredIconStart + titleLabelArr[i] + requiredIconEnd
                + infoIconStart + nameInfo + infoIconEnd + labelEnd
                + '<input type="text" name="name" id="name' + i + '" class="input-field"/>' + divEnd;
        inputs += divStart + labelStart + requiredIconStart + urlLabelArr[i] + requiredIconEnd
                + infoIconStart + itemInfo + infoIconEnd + labelEnd
                + '<input type="text" name="item" id="item' + i + '" class="input-field"/>' + divEnd;
    }
    inputs += divStart + labelStart + requiredIconStart + urlLabelArr[3] + requiredIconEnd
            + infoIconStart + nameInfo + infoIconEnd + labelEnd
            + '<input type="text" name="name" id="name' + i + '" class="input-field"/>' + divEnd;
    return inputs;
}
function changeBreadcrumbLevel()
{
    var levelValue = $('#level option:selected').val();
    var breadcrumbInputs = addInputsforBreadcrumbs();
    $("#markuptype-inputs").html(breadcrumbInputs);
    $("#level").val(levelValue);
}

function getMapOfInputs() {
    var map = new Object();
    var timeOptions = timePickerIn24Hours();
//    var businessDays = '<div class="business-days"><div class="business-days-row"><input type="checkbox" name="openingHoursMo" id="openingHoursMo" value="Mo" class="input-field"/><div>Mon</div><select name="opensMo" id="opensMo">' + timeOptions + '</select><select name="closeMo" id="closeMo">' + timeOptions + '</select></div>';
    var formDiv = '<div class="form-group">';
    var formInnerDiv = '<div class="checkbox checkbox-success">';
    var checkBoxInput = '<input type="checkbox"';
    var timeDiv = '<div class="input-group bootstrap-timepicker timepicker lxr-time">';
    var timeInput = '<input type="text" class="form-control input-small"';
    var timeIcon = '<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>';
    var divEnd = "</div>";
    var businessDays = '<span>Select Business Hours</span><br/>' + formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursMo" id="openingHoursMo" value="Mo" />' + '<label for="openingHoursMo"> Mon &nbsp;</label>'
            + timeDiv + timeInput + ' id="monFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="monTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursTu" id="openingHoursTu" value="Tu"/>' + '<label for="openingHoursTu"> Tue &nbsp;</label>'
            + timeDiv + timeInput + ' id="tueFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="tueTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursWe" id="openingHoursWe" value="We"/>' + '<label for="openingHoursWe"> Wed &nbsp;</label>'
            + timeDiv + timeInput + ' id="wedFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="wedTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursTh" id="openingHoursTh" value="Th"/>' + '<label for="openingHoursTh"> Thu &nbsp;</label>'
            + timeDiv + timeInput + ' id="thuFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="thuTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursFr" id="openingHoursFr" value="Fr"/>' + '<label for="openingHoursFr"> Fri &nbsp;&nbsp;&nbsp;&nbsp;</label>'
            + timeDiv + timeInput + ' id="friFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="friTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursSa" id="openingHoursSa" value="Sa"/>' + '<label for="openingHoursSa"> Sat &nbsp;&nbsp;</label>'
            + timeDiv + timeInput + ' id="satFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="satTo"/>' + timeIcon + divEnd + divEnd + divEnd;
    businessDays += formDiv + formInnerDiv + checkBoxInput + ' name="openingHoursSu" id="openingHoursSu" value="Su"/>' + '<label for="openingHoursSu"> Sun &nbsp;&nbsp;</label>'
            + timeDiv + timeInput + ' id="sunFrom"/>' + timeIcon + divEnd + '<span> to </span>' + timeDiv + timeInput + ' id="sunTo"/>' + timeIcon + divEnd + divEnd + divEnd;
//    businessDays += '<div class="form-group">';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursTu" id="openingHoursTu" value="Tu" class="input-field"/><div>Tue</div><Select name="opensTu" id="opensTu">' + timeOptions + '</select><select name="closeTu" id="closeTu">' + timeOptions + '</select></div>';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursWe" id="openingHoursWe" value="We" class="input-field"/><div>Wed</div><select name="opensWe" id="opensWe">' + timeOptions + '</select><select name="closeWe" id="closeWe">' + timeOptions + '</select></div>';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursTh" id="openingHoursTh" value="Th" class="input-field"/><div>Thu</div><select name="opensTh" id="opensTh">' + timeOptions + '</select><select name="closeTh" id="closeTh">' + timeOptions + '</select></div>';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursFr" id="openingHoursFr" value="Fr" class="input-field"/><div>Fri</div><select name="opensFr" id="opensFr">' + timeOptions + '</select><select name="closeFr" id="closeFr">' + timeOptions + '</select></div>';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursSa" id="openingHoursSa" value="Sa" class="input-field"/><div>Sat</div><select name="opensSa" id="opensSa">' + timeOptions + '</select><select name="closeSa" id="closeSa">' + timeOptions + '</select></div>';
//    businessDays += '<div class="business-days-row"><input type="checkbox" name="openingHoursSu" id="openingHoursSu" value="Su" class="input-field"/><div>Sun</div><select name="opensSu" id="opensSu">' + timeOptions + '</select><select name="closeSu" id="closeSu">' + timeOptions + '</select></div></div>';
    var inputClass = 'class="form-control lxrsrt-fc"';
    map[1] = '<input type="text" name="name" id="name" ' + inputClass + ' placeholder="Name"/>';
    map[2] = '<textarea name="description" id="description" ' + inputClass + ' placeholder="Breif description"></textarea>';
    map[3] = '<input type="text" name="thumbnailUrl" id="thumbnailUrl" ' + inputClass + ' placeholder="Thumbnail URL"/>';
    map[4] = '<input type="text" name="uploadDate" id="uploadDate" ' + inputClass + ' placeholder="Upload Date"/>';
    map[5] = '<input type="text" name="contentUrl" id="contentUrl" ' + inputClass + ' placeholder="Video Content URL"/>';
    map[6] = '<input type="text" name="embedUrl" id="embedUrl" ' + inputClass + ' placeholder="Video Embeded URL"/>';
    map[7] = '<input type="text" name="interactionCount" id="interactionCount" ' + inputClass + ' placeholder="No. of"/>';
    map[8] = '<input type="text" name="expires" id="expires" ' + inputClass + ' placeholder="Expires Date"/>';
//                direct image url
    map[9] = '<input type="text" name="image" id="image" ' + inputClass + ' placeholder="Image URL"/>';
    map[10] = '<input type="text" name="brand.name" id="brand" ' + inputClass + ' placeholder=""/>';
    map[11] = '<input type="text" name="priceCurrency" id="priceCurrency" list="currency-list" ' + inputClass + ' placeholder="Country\'s Currency"/>' + getInputCurrencyList();
    map[12] = '<input type="text" name="price" id="price" ' + inputClass + ' placeholder="Price"/>';
    map[13] = '<select name="availability" id="availability" ' + inputClass + ' placeholder=""><option value="">-- Select Availability--</option><option value="InStock">In Stock</option><option value="OutOfStock">Out of Stock</option></select>';
    map[14] = '<input type="text" name="priceValidUntil" id="priceValidUntil" ' + inputClass + ' placeholder="Price Valid Upto"/>';
    map[15] = '<input type="text" name="url" id="url" ' + inputClass + ' placeholder="URL"/>';
    map[16] = '<input type="text" name="itemOffered" id="itemOffered" ' + inputClass + ' placeholder="Offered Item"/>';
//                map[17] = 'Main Entity Of Page<input type="text" name="mainEntityOfPage" id="" '+inputClass+' placeholder=""/>';
    map[18] = '<input type="text" name="headline" id="headline" ' + inputClass + ' placeholder="Headline"/>';
    map[19] = '<input type="text" name="image.url" id="imageUrl" ' + inputClass + ' placeholder="Image URL"/>';
    map[20] = '<input type="text" name="image.width" id="imageWidth" ' + inputClass + ' placeholder="Image width"/>';
    map[21] = '<input type="text" name="image.height" id="imageHeight" ' + inputClass + ' placeholder="Image height"/>';
    map[22] = '<input type="text" name="publisher.name" id="pName" ' + inputClass + ' placeholder="Publisher name"/>';
    map[23] = '<input type="text" name="logo.url" id="logourl" ' + inputClass + ' placeholder="Logo URL"/>';
    map[24] = '<input type="text" name="logo.width" id="logoWidth" ' + inputClass + ' placeholder="Logo width"/>';
    map[25] = '<input type="text" name="logo.height" id="logoHeight" ' + inputClass + ' placeholder="Logo height"/>';
    map[26] = '<input type="text" name="datePublished" id="datePublished" ' + inputClass + ' placeholder="Published Date"/>';
    map[27] = '<input type="text" name="dateModified" id="dateModified" ' + inputClass + ' placeholder="Updated Date"/>';
    map[28] = '<input type="text" name="author.name" id="aName" ' + inputClass + ' placeholder="Author name"/>';
    map[29] = '<input type="text" name="review" id="review" ' + inputClass + ' placeholder="Review"/>';
    map[30] = '<select name="prepTime" id="prepTime" ' + inputClass + '>' + timePickerIn12Hours() + '</select>';
    map[31] = '<select name="totalTime" id="totalTime" ' + inputClass + '>' + timePickerIn12Hours() + '</select>';
    map[32] = '<input type="text" name="recipeYield" id="recipeYield" ' + inputClass + ' placeholder="Recipe yield"/>';
    map[33] = '<input type="text" name="nutrition.servingSize" id="servingSize" ' + inputClass + ' placeholder="Serving size"/>';
    map[34] = '<input type="text" name="nutrition.calories" id="calories" ' + inputClass + ' placeholder="Calories"/>';
    map[35] = '<input type="text" name="nutrition.fatContent" id="fatContent" ' + inputClass + ' placeholder="Fat Content"/>';
    map[36] = '<textarea name="recipeIngredient" id="recipeIngredient" ' + inputClass + ' placeholder="Recipe ingredients"></textarea>';
    map[37] = '<textarea name="recipeInstructions" id="recipeInstructions" ' + inputClass + ' placeholder="Recipe instructions"></textarea>';
    map[38] = '<input type="text" name="aggregateRating.ratingValue" id="ratingValue" ' + inputClass + ' placeholder="Total avg. rating"/>';
    map[39] = '<input type="text" name="aggregateRating.reviewCount" id="reviewCount" ' + inputClass + ' placeholder="No. of ratings"/>';
    map[40] = '<input type="text" name="streetAddress" id="streetAddress" ' + inputClass + ' placeholder="Street address"/>';
    map[41] = '<input type="text" name="addressLocality" id="addressLocality" ' + inputClass + ' placeholder="City"/>';
    map[42] = '<input type="text" name="addressRegion" id="addressRegion" ' + inputClass + ' placeholder="State"/>';
    map[43] = '<input type="text" name="postalCode" id="postalCode" ' + inputClass + ' placeholder="Postal code"/>';
    map[44] = '<input type="text" name="addressCountry" id="addressCountry" ' + inputClass + ' placeholder="Country"/>';
    map[45] = '<input type="text" name="latitude" id="latitude" ' + inputClass + ' placeholder="Lattitude"/>';
    map[46] = '<input type="text" name="longitude" id="longitude" ' + inputClass + ' placeholder="Logitude"/>';
    map[47] = '<input type="text" name="hasMap" id="hasMap" ' + inputClass + ' placeholder="Map location URL"/>';
    map[48] = '<input type="text" name="telephone" id="telephone" ' + inputClass + ' placeholder="Telephone"/>';
    map[49] = '<select name="itemCondition" id="itemCondition" ' + inputClass + ' placeholder=""><option value="">-- Select --</option><option value="New">New</option><option value="Used">Used</option><option value="Refurbished">Refurbished</option></select>';
    map[50] = '<input type="text" name="seller.name" id="sname" ' + inputClass + ' placeholder="Seller Name"/>';
    map[51] = '<select name="duration" id="duration" ' + inputClass + '>' + timePickerIn12Hours() + '</select>';
    map[52] = '<select name="cookTime" id="cookTime" ' + inputClass + '>' + timePickerIn12Hours() + '</select>';
    map[53] = '<input type="text" name="startDate" id="startDate" ' + inputClass + ' placeholder="Start Date"/>';
    map[54] = '<input type="text" name="endDate" id="endDate" ' + inputClass + ' placeholder="End Date"/>';
    map[55] = '<input type="text" name="location.name" id="lname" ' + inputClass + ' placeholder="Location"/>';
    map[56] = '<input type="text" name="target" id="target" ' + inputClass + ' placeholder=""/>';
    map[57] = '<input type="text" name="query-input" id="query-input" ' + inputClass + ' placeholder=""/>';
    map[58] = '<input type="text" name="target" id="target" ' + inputClass + ' placeholder=""/>';
    map[59] = '<input type="text" name="query-input" id="query-input" ' + inputClass + ' placeholder=""/>';
    map[60] = '<input type="text" name="name" id="name" ' + inputClass + ' placeholder="Name"/>';
    map[61] = '<input type="text" name="position" id="position" ' + inputClass + ' placeholder=""/>';
    map[62] = '<input type="text" name="item" id="item" ' + inputClass + ' placeholder=""/>';
    map[63] = '<input type="text" name="reviewCount" id="reviewCount" ' + inputClass + ' placeholder="No. of reviews"/>';
    map[64] = '<input type="text" name="name" id="name" ' + inputClass + ' placeholder="Name"/>';
    map[65] = businessDays;
    map[66] = '<input type="text" name="alternateName" id="alternateName" ' + inputClass + ' placeholder="Alternative name"/>';
    map[67] = '<input type="text" name="ratingValue" id="ratingValue" ' + inputClass + ' placeholder="Total avg. rating"/>';
    map[68] = '<input type="text" name="ratingCount" id="ratingCount" ' + inputClass + ' placeholder="No. of ratings"/>';
    map[69] = '<input type="text" name="legalName" id="legalName" ' + inputClass + ' placeholder=""/>';
    map[70] = '<input type="text" name="logo" id="logoUrl" ' + inputClass + ' placeholder="Logo URL"/>';
    map[71] = '<input type="text" name="contactType" id="contactType" ' + inputClass + ' placeholder="Contact Type"/>';
    map[72] = '<textarea name="sameAs" id="sameAs" ' + inputClass + ' placeholder=""></textarea>';
    map[73] = '<input type="text" name="title" id="title" ' + inputClass + ' placeholder="Title"/>';
    map[74] = '<input type="hidden" name="identifier.name" id="iName" ' + inputClass + ' placeholder=""/>';
    map[75] = '<input type="text" name="identifier.value" id="iValue" ' + inputClass + ' placeholder=""/>';
    map[76] = '<input type="text" name="datePosted" id="datePosted" ' + inputClass + ' placeholder="Date Posted"/>';
    map[77] = '<input type="text" name="validThrough" id="validThrough" ' + inputClass + ' placeholder="Valid Untill"/>';
    map[78] = '<select name="employmentType" id="employmentType" ' + inputClass + '><option value="">-- Select --</option><option value="FULL_TIME">FULL TIME</option><option value="PART_TIME">PART TIME</option><option value="CONTRACTOR">CONTRACTOR</option><option value="TEMPORARY">TEMPORARY</option><option value="INTERN">INTERN</option><option value="VOLUNTEER">VOLUNTEER</option><option value="PER_DIEM">PER DIEM</option><option value="INTERN">INTERN</option></select>';
    map[79] = '<input type="text" name="hiringOrganization.name" id="hName" ' + inputClass + ' placeholder="Organization Name"/>';
    map[80] = '<input type="text" name="hiringOrganization.sameAs" id="hValue" ' + inputClass + ' placeholder="Organization social profiles"/>';
    map[81] = '<input type="text" name="currency" id="currency" list="currency-list" ' + inputClass + ' placeholder=""/>' + getInputCurrencyList();
    map[82] = '<input type="text" name="baseSalary.minValue" id="bsMinValue" ' + inputClass + ' placeholder="Min. Base Salary"/>';
    map[83] = '<input type="text" name="baseSalary.maxValue" id="bsMaxValue" ' + inputClass + ' placeholder="Max. Base Salary"/>';
    map[84] = '<select name="unitText" id="unitText" ' + inputClass + '><option value="">-- Select --</option><option value="HOUR">HOUR</option><option value="WEEK">WEEK</option><option value="MONTH">MONTH</option><option value="YEAR">YEAR</option></select>';
    map[100] = '<input type="text" name="empty" ' + inputClass + ' placeholder="">';
    return map;
}

function getMapofLabels() {
    var map = new Object();
    map[1] = 'Name: ';
    map[2] = 'Description: ';
    map[3] = 'Thumbnail Url: ';
    map[4] = 'Upload Date: ';
    map[5] = 'Content Url: ';
    map[6] = 'Embed Url: ';
    map[7] = 'Interaction Count: ';
    map[8] = 'Expires: ';
//                direct image url
    map[9] = 'Image: ';
    map[10] = 'Brand: ';
    map[11] = 'Offer Price Currency: ';
    map[12] = 'Offer Price: ';
    map[13] = 'Availability: ';
    map[14] = 'Valid Until: ';
    map[15] = 'URL: ';
    map[16] = 'Offered Item: ';
    map[17] = 'Main Entity Of Page: ';
    map[18] = 'Headline: ';
    map[19] = 'Image: ';
    map[20] = 'Image Width: ';
    map[21] = 'Image Height: ';
    map[22] = 'Publisher Name: ';
    map[23] = 'Logo: ';
    map[24] = 'Logo Width: ';
    map[25] = 'Logo Height: ';
    map[26] = 'Date Published: ';
    map[27] = 'Date Modified: ';
    map[28] = 'Author Name: ';
    map[29] = 'Review: ';
    map[30] = 'Preparation Time: ';
    map[31] = 'Total Time: ';
    map[32] = 'Recipe Yield: ';
    map[33] = 'Serving Size: ';
    map[34] = 'Calories: ';
    map[35] = 'Fat Content: ';
    map[36] = 'Recipe Ingredients: ';
    map[37] = 'Recipe Instructions: ';
    map[38] = 'Average Rating: ';
    map[39] = 'Number of Reviews: ';
    map[40] = 'Address: ';
    map[41] = 'City: ';
    map[42] = 'State/Region: ';
    map[43] = 'Zip/Postal Code: ';
    map[44] = 'Country: ';
    map[45] = 'Latitude: ';
    map[46] = 'Longitude: ';
    map[47] = 'Include a Map: ';
    map[48] = 'Telephone: ';
    map[49] = 'Item Condition: ';
    map[50] = 'Seller Name: ';
    map[51] = 'Duration: ';
    map[52] = 'Cook Time: ';
    map[53] = 'Start Date: ';
    map[54] = 'End Date: ';
    map[55] = 'Location: ';
    map[56] = 'Search Query URL: ';
    map[57] = 'Query Input Text: ';
    map[58] = 'Search Query URL (Android App): ';
    map[59] = 'Query Input Text: ';
    map[60] = 'Name: ';
    map[61] = 'Position: ';
    map[62] = 'Item Url: ';
    map[63] = 'Number of Reviews: ';
    map[64] = 'Item Reviewed Name: ';
    map[65] = 'Select Business Hours: ';
    map[66] = 'Alternate Name: ';
    map[67] = 'Average Rating: ';
    map[68] = 'Number of Ratings: ';
    map[69] = 'Name: ';
    map[70] = 'Logo: ';
    map[71] = 'Contact Type: ';
    map[72] = 'Social Profiles: ';
    map[73] = 'Job Title: ';
//  map[74] is hidden input
    map[75] = 'Job Code: ';
    map[76] = 'Date Posted: ';
    map[77] = 'Expire Date: ';
    map[78] = 'Employment Type: ';
    map[79] = 'Hiring Organization Name: ';
    map[80] = 'Hiring Organization Website: ';
    map[81] = 'Currency Code: ';
    map[82] = 'Base Salary: ';
//    map[83] = 'Max. Base Salary: ';
    map[84] = 'Base Salary Unit: ';
//    map[85] = 'Industry: ';
//    map[86] = 'Qualifications: ';
//    map[87] = 'Experience Requirements: ';
//    map[88] = 'Responsibilities: ';
//    map[89] = 'Skills: ';
    map[100] = 'Empty Label: ';
    return map;
}

function getInputInfoMap(markupType) {
    var inputInfoMap = new Object();
    var image = "<p>The representative image of the article. Only a marked-up image that directly belongs to the article should be specified.</p>";
    image += "<li>The URL of the image.</li>";
    image += "<li>Images should be at least 696 pixels wide.</li>";
    image += "<li>Images should be in .jpg, .png, or. gif format.</li>";
    image += "<li>Image URLs should be crawlable and indexable.</li>";
    var commonImageGuidelines = "<li>Images should be at least 696 pixels wide.</li>";
    commonImageGuidelines += "<li>Images should be in .jpg, .png, or. gif format.</li>";
    commonImageGuidelines += "<li>Image URLs should be crawlable and indexable.</li>";
    if (markupType === "video") {
        inputInfoMap[1] = "The title of the video";
        inputInfoMap[2] = "The description of the video.";
        inputInfoMap[3] = "A URL pointing to the video thumbnail image file. Images must be at least 160x90 pixels and at most 1920x1080 pixels. We recommend images in .jpg, .png, or .gif formats. All image URLs should be crawlable and indexable. Otherwise, we will not be able to display them on the search results page.";
        inputInfoMap[4] = "The date the video was first published, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[51] = "The duration of the video in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[5] = "A URL pointing to the actual video media file. This file should be in .mpg, .mpeg, .mp4, .m4v, .mov, .wmv, .asf, .avi, .ra, .ram, .rm, .flv, or other video file format. All files must be accessible via HTTP. Metafiles that require a download of the source via streaming protocols, such as RTMP, are not supported. Providing this file allows Google to generate video thumbnails and video previews and can help Google verify your video. Tip: You can confirm that it's really Googlebot accessing your content by using a reverse DNS lookup.";
        inputInfoMap[6] = "A URL pointing to a player for the specific video. Usually this is the information in the srcelement of an <embed> tag. Example: Dailymotion. Tip: you can ensure that only Googlebot accesses your content by using a reverse DNS lookup.";
        inputInfoMap[7] = "The number of times the video has been viewed.";
        inputInfoMap[8] = "If applicable, the date after which the video will no longer be available, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>. Don't supply this information if your video does not expire.";
    } else if (markupType === "article") {
        var headline = "<p>The headline of the article. Headlines should not exceed 110 characters.</p>";
        inputInfoMap[18] = headline;
        inputInfoMap[19] = image;
        inputInfoMap[20] = "The width of the image, in pixels. Images should be at least 696 pixels wide.";
        inputInfoMap[21] = "The height of the image, in pixels.";
        inputInfoMap[22] = "The name of the publisher.";
        inputInfoMap[23] = "The URL of the logo.";
        inputInfoMap[24] = "The width of the logo, in pixels.";
        inputInfoMap[25] = "The height of the logo, in pixels.";
        inputInfoMap[26] = "The date and time the article was first published, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[27] = "The date and time the article was most recently modified, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[28] = "The name of the author of an article.";
        inputInfoMap[2] = "A short description of the article.";
    } else if (markupType === "product") {
        inputInfoMap[1] = "The name of the product.";
        inputInfoMap[15] = "A URL to the product web page.";
        inputInfoMap[9] = "URL, required for Image Search, recommended for Search. The URL of a product photo. Pictures clearly showing the product, e.g. against a white background, are preferred." + commonImageGuidelines;
        inputInfoMap[2] = "Product description.";
        inputInfoMap[10] = "The brand of the product.";
        inputInfoMap[67] = "Average rating of the product.";
        inputInfoMap[63] = "Total number of ratings.";
        inputInfoMap[11] = "The currency used to describe the product price, in three-letter ISO 4217 format.";
        inputInfoMap[12] = "The price of the product. Follow schema.org usage guidelines.";
        inputInfoMap[13] = "required for Related Items feature in Image Search, recommended for Search. Value is taken from a constrained list of options, expressed in markup using URL links. Google also understands their short names (for example InStock or OutOfStock, without the full URL scope.)";
        inputInfoMap[14] = "The date (in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>) after which the price will no longer be available. (Your product snippet may not display if the priceValidUntil property indicates a past date.)";
        inputInfoMap[16] = "The item being sold. Typically, this includes a nested product, but it can also contain other item types or free text.";
        inputInfoMap[49] = "Item condition whether the item is New/Used/Refurbished";
        inputInfoMap[50] = "Name of the seller";
    } else if (markupType === "recipe") {
        inputInfoMap[1] = "The name of the dish.";
        inputInfoMap[9] = "Image of the completed dish. Must be a minimum of 232px by 130px, with a 16:9 aspect ratio. *Mark up an image to include it in the rich card. If you don't mark up a specific image, a prominent image from the page will be used in the card. Every page must contain at least one image (whether or not you include markup).";
        inputInfoMap[28] = "Creator of the recipe.";
        inputInfoMap[26] = "The date the recipe was published, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[2] = "A short summary describing the dish.";
        inputInfoMap[30] = "The length of time it takes to prepare the recipe for dish, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>. Can use min, max as child elements to specify a range of time. *Always use in combination with cookTime.";
        inputInfoMap[52] = "The time it takes to actually cook the dish, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>. Can use min, max as child elements to specify a range of time. *Always use in combination with prepTime.";
        inputInfoMap[31] = "The total time it takes to prepare the cook the dish, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>. Can use min, max as child elements to specify a range of time. * Use totalTime or a combination of both cookTime and prepTime.";
        inputInfoMap[32] = "The quantity produced by the recipe. For example: number of people served, or number of servings.";
        inputInfoMap[34] = "The number of calories in the recipe.";
        inputInfoMap[35] = "Fat content of the recipe in grams";
        inputInfoMap[38] = "Average rating for the recipe.";
        inputInfoMap[39] = "Total number of reviews.";
        inputInfoMap[36] = "An ingredient used in the recipe.";
        inputInfoMap[37] = "The steps to make the dish. Can contain the child element instruction, which can be used to annotate each step.";
    } else if (markupType === "localBusiness") {
        inputInfoMap[1] = "Name of the business.";
        inputInfoMap[15] = "The fully-qualified URL of the specific business location.This URL property should be a working link.";
        inputInfoMap[23] = "The URL of the logo of your business.";
        inputInfoMap[9] = "An image of the business. URL, required for Image Search, recommended for Search. The URL of a business photo. Pictures clearly showing the product, e.g. against a white background, are preferred.";
        inputInfoMap[2] = "Short description of your business.";
        inputInfoMap[40] = "The business's location street address.";
        inputInfoMap[41] = "The locality or city.";
        inputInfoMap[42] = "The region or state.";
        inputInfoMap[43] = "The postal code.";
        inputInfoMap[44] = "The country name. The 2-letter ISO 3166-1 alpha-2 country code is recommended.";
        inputInfoMap[45] = "The latitude of the business location. The precision should be at least 5 decimal places.";
        inputInfoMap[46] = "The longitude of the business location. The precision should be at least 5 decimal places.";
        inputInfoMap[47] = "URL of the google maps of your location.";
        inputInfoMap[48] = "A business phone number meant to be the primary contact method for customers. Be sure to include the country code and area code in the phone number.";
        inputInfoMap[65] = "Hours during which the business location is open and close. The time the business location opens and closes, in hh:mm:ss format.";
    } else if (markupType === "event") {
        inputInfoMap[1] = "The title of the event.";
        inputInfoMap[15] = "URL of a page providing details about the event. This URL property should be a working link. Provide fully qualified URL for your website. Example: https://example.com.";
        inputInfoMap[9] = "URL of an image or logo for the event or tour" + commonImageGuidelines;
        inputInfoMap[2] = "Description of the event.";
        inputInfoMap[53] = "The start date and time of the event, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[54] = "The end date and time of the event, in <a target='_blank' href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 format</a>.";
        inputInfoMap[55] = "The venue name.";
        inputInfoMap[40] = "The venue's street address.";
        inputInfoMap[41] = "The locality or city.";
        inputInfoMap[42] = "The region or state.";
        inputInfoMap[43] = "The postal code.";
        inputInfoMap[44] = "The country name.";
    } else if (markupType === "website") {
        inputInfoMap[1] = "The preferred name of your website.";
        inputInfoMap[66] = "An alternate name you want Google to consider.";
        inputInfoMap[15] = "The URL of your official website. This URL property should be a working link.";
        inputInfoMap[56] = "For websites, the URL of the handler that should receive and handle the search query. which would send a GET request to https://query.example.com/search?q=user%20search%20string. A property of WebSite indicating a supported action a user might take, in this case a search. This must be a URL that points to an address on the same domain as the content being searched. It must also include a variable name in braces that is a placeholder for the user's search query. Your server should assume and support UTF-8 values for user search queries.";
        inputInfoMap[57] = "Whatever placeholder you want to use in the search box(target). A placeholder string that is replaced by user's search query when the user clicks the 'Search' button in the search box. Example: https://query.example.com/search?q={placeholder_string}";
    } else if (markupType === "aggregateRating") {
        inputInfoMap[64] = "Annotation for the subject being reviewed. Like Local business, Movie, Book.";
        inputInfoMap[67] = "The average rating based on multiple ratings or reviews. (or) The average rating given to the reviewed item.";
        inputInfoMap[68] = "The number of ratings given to the reviewed item.";
        inputInfoMap[63] = "The number of reviews given to the reviewed item.";
    } else if (markupType === "organization") {
        inputInfoMap[69] = "The name of the person or organization.";
        inputInfoMap[15] = "The URL for the person's or organization's official website. This URL property should be a working link.";
        inputInfoMap[70] = "The URL of the logo of an organization.";
        inputInfoMap[40] = "The venue's street address.";
        inputInfoMap[41] = "The locality or city.";
        inputInfoMap[42] = "The region or state.";
        inputInfoMap[43] = "The postal code.";
        inputInfoMap[44] = "The country name.";
        inputInfoMap[48] = "A business phone number meant to be the primary contact method for customers. Be sure to include the country code and area code in the phone number.";
        inputInfoMap[71] = 'One of the following values, not case sensitive. (Additional contact types may be supported later.). Examples: "customer support", "technical support", "billing support", "bill payment", "sales", "reservations", "credit card support", "emergency", "baggage tracking", "roadside assistance", "package tracking"';
        inputInfoMap[72] = "A single or an [array] of URLs for the person's or organization's official social media profile page(s). Array of URLs must be each URL per each line(should be s   eperated by enter key)";
    } else if (markupType === "jobPosting") {
        inputInfoMap[2] = "<p>The full description of the job in HTML format.</p>"
                + "<p>The description should be a complete representation of the job, including job responsibilities, qualifications, skills, working hours, education requirements, and experience requirements. The description can't be the same as the title.</p>"
                + "<p>Additional guidelines:</p>"
                + "<li>You must format the description in HTML.</li>"
//                          &#60; ---> '<', &#62; ---> '>', &#92; ---> '\'
                + "<li>At minimum, add paragraph breaks using &#60;br&#62;, &#60;p&#62;, or &#92;n.</li>"
                + "<li>Valid tags include &#60;p>, &#60;ul&#62;, &#60;li&#62;, and headings &#60;h1&#62; through &#60;h5&#62;.</li>"
                + "<li>You can also use character-level formatting tags such as &#60;strong&#62; and &#60;em&#62;.</li>";
        inputInfoMap[73] = "<p>The title of the job (not the title of the posting). For example, \"Software Engineer\" or \"Barista\".</p>"
                + "<p>Best practices:</p>"
                + "<li>This field should be the title of the job only.</li>"
                + "<li>Don't include job codes, addresses, dates, salaries, or company names in the title.</li>"
                + "<li>Provide concise, readable titles.</li>"
                + "<li>Don't use special characters such as \"!\" and \"*\". Using unnecessary characters might cause your markup to be considered as Spammy Structured Markup.</li>";
        inputInfoMap[74] = "";
        inputInfoMap[75] = "<p>The hiring organization's unique identifier for the job.</p>";
        inputInfoMap[76] = "<p>The original date that employer posted the job in ISO 8601 format. For example, \"2017-01-24\".</p>";
        inputInfoMap[77] = "<p>The date when the job posting will expire in ISO 8601 format. For example, \"2017-02-24\".</p>"
                + "<p>If a job posting never expires, or you do not know when the job will expire, do not include this property. If the job is filled before the expiration date occurs, <a href=\"https://developers.google.com/search/docs/data-types/job-postings#remove\">remove the job posting.</a></p>";
        inputInfoMap[78] = "Type of employment.";
        inputInfoMap[79] = "<p>The organization offering the job position. This should be the name of the company (for example, “Starbucks, Inc”), and not the specific location that is hiring (for example, “Starbucks on Main Street”).</p>";
        inputInfoMap[80] = "This should be the URL of the company's website.";
        inputInfoMap[40] = "The company's street address.";
        inputInfoMap[41] = "The locality or city.";
        inputInfoMap[42] = "The region or state.";
        inputInfoMap[43] = "The postal code.";
        inputInfoMap[44] = "The country name.";
        inputInfoMap[81] = "<p>The base salary for the job.</p>";
        inputInfoMap[82] = "<p>Base salary for the job.</p>";
        inputInfoMap[83] = "<p>Maximum base salary for the job.</p>";
        inputInfoMap[84] = "<p>For the unitText of QuantitativeValue, use one of the following case-sensitive values: \"HOUR\", \"WEEK\", \"MONTH\", \"YEAR\".</p>";
    }
    return inputInfoMap;
}

function inputValidations(markupType) {
    var status = true;
    if (markupType === "article") {
        if ($("#headline").val() === "") {
            alert("Please enter article headline.");
            $("#headline").focus();
            status = false;
        } else if ($("#headline").val() !== "" && $("#headline").val().trim().length > 110) {
            alert("Article headline should not exceed 110 characters.");
            $("#headline").focus();
            status = false;
        } else if ($.trim($("#imageUrl").val()) === "") {
            alert("Please enter image URL.");
            $("#imageUrl").focus();
            status = false;
        } else if (!imageValidation($("#imageUrl").val())) {
            alert("Please enter a valid image URL.");
            $("#imageUrl").focus();
            status = false;
        } else if ($.trim($("#imageWidth").val()) === "") {
            alert("Please enter image width.");
            $("#imageWidth").focus();
            status = false;
        } else if ($.trim($("#imageWidth").val()) !== "" && !numberValidation($("#imageWidth").val())) {
            alert("Please enter a valid image width.");
            $("#imageWidth").focus();
            status = false;
        } else if (parseInt($("#imageWidth").val().trim()) < 696) {
            alert("Image width should be atleast 696px.");
            $("#imageWidth").focus();
            status = false;
        } else if ($.trim($("#imageHeight").val()) === "") {
            alert("Please enter image height.");
            $("#imageHeight").focus();
            status = false;
        } else if ($.trim($("#imageHeight").val()) !== "" && !numberValidation($("#imageHeight").val())) {
            alert("Please enter a valid image height.");
            $("#imageHeight").focus();
            status = false;
        } else if ($.trim($("#pName").val()) === "") {
            alert("Please enter publisher name.");
            $("#pName").focus();
            status = false;
        } else if ($.trim($("#logourl").val()) === "") {
            alert("Please enter logo URL.");
            $("#logourl").focus();
            status = false;
        } else if ($.trim($("#logourl").val()) !== "" && !imageValidation($("#logourl").val())) {
            alert("Please enter valid format of logo URL.");
            $("#logourl").focus();
            status = false;
        } else if ($.trim($("#logoWidth").val()) !== "" && !numberValidation($("#logoWidth").val())) {
            alert("Please enter a valid logo width.");
            $("#logoWidth").focus();
            status = false;
        } else if (parseInt($("#logoWidth").val().trim()) > 600) {
            alert("Logo width should be at most 600px.");
            $("#logoWidth").focus();
            status = false;
        } else if ($.trim($("#logoHeight").val()) !== "" && !numberValidation($("#logoHeight").val())) {
            alert("Please enter a valid logo height.");
            $("#logoHeight").focus();
            status = false;
        } else if (parseInt($("#logoHeight").val().trim()) > 60) {
            alert("Logo height should be at most 60px.");
            $("#logoHeight").focus();
            status = false;
        } else if ($("#datePublished").val().trim() === "") {
            alert("Please enter the published date of an article.");
            $("#datePublished").focus();
            status = false;
        } else if ($("#aName").val().trim() === "") {
            alert("Please enter author name.");
            $("#aName").focus();
            status = false;
        }
    } else if (markupType === "event") {
        if ($("#name").val() === "") {
            alert("Please enter the title of the event.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#url").val()) === "") {
            alert("Please enter an event URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" &&
                !($("#url").val().indexOf("http://") !== -1 || $("#url").val().indexOf("https://") !== -1)) {
            alert("Please enter a fully qualified url for your website.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" && !urlValidation($("#url").val())) {
            alert("Please enter a valid URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#image").val()) !== "" && !imageValidation($("#image").val())) {
            alert("Please enter image URL in a valid format.");
            $("#image").focus();
            status = false;
        } else if ($.trim($("#startDate").val()) === "") {
            alert("Please enter a start date of the event.");
            $("#startDate").focus();
            status = false;
        } else if ($("#lname").val().trim() === "") {
            alert("Please enter a venue location name.");
            $("#lname").focus();
            status = false;
        } else if ($("#streetAddress").val() === "") {
            alert("Please enter street address.");
            $("#streetAddress").focus();
            status = false;
        } else if ($("#addressLocality").val() === "") {
            alert("Please enter your city.");
            $("#addressLocality").focus();
            status = false;
        } else if ($("#addressRegion").val() === "") {
            alert("Please enter your state.");
            $("#addressRegion").focus();
            status = false;
        } else if ($.trim($("#postalCode").val()) === "") {
            alert("Please enter postal code.");
            $("#postalCode").focus();
            status = false;
        } else if ($("#addressCountry").val() === "") {
            alert("Please enter your country name.");
            $("#addressCountry").focus();
            status = false;
        }
    } else if (markupType === "localBusiness") {
        var weekDays = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
        if ($.trim($("#name").val()) === "") {
            alert("Please enter business Name.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#url").val()) === "") {
            alert("Please enter URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" && !urlValidation($("#url").val())) {
            alert("Please enter a valid URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#logourl").val()) !== "" && !imageValidation($("#logourl").val())) {
            alert("Please enter a valid format logo path.");
            $("#logourl").focus();
            status = false;
        } else if ($.trim($("#image").val()) === "") {
            alert("Please enter an image URL.");
            $("#image").focus();
            status = false;
        } else if ($.trim($("#image").val()) !== "" && !imageValidation($("#image").val())) {
            alert("Please enter a valid image path.");
            $("#image").focus();
            status = false;
        } else if ($("#streetAddress").val() === "") {
            alert("Please enter street address.");
            $("#streetAddress").focus();
            status = false;
        } else if ($("#addressLocality").val() === "") {
            alert("Please enter your city.");
            $("#addressLocality").focus();
            status = false;
        } else if ($("#addressRegion").val() === "") {
            alert("Please enter your state.");
            $("#addressRegion").focus();
            status = false;
        } else if ($.trim($("#postalCode").val()) === "") {
            alert("Please enter postal code.");
            $("#postalCode").focus();
            status = false;
        } else if ($("#addressCountry").val() === "") {
            alert("Please enter your country name.");
            $("#addressCountry").focus();
            status = false;
        } else if ($("#latitude").val() !== "" && !numberValidation($("#latitude").val())) {
            alert("Please enter latitude of your location in a valid format.");
            $("#latitude").focus();
            status = false;
        } else if ($("#longitude").val() !== "" && !numberValidation($("#longitude").val())) {
            alert("Please enter longitude of your location in a valid format.");
            $("#longitude").focus();
            status = false;
        }
        var weekFulldayString = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        for (var i = 0; (i < weekDays.length) && (status === true); i++) {
            if ($("#openingHours" + weekDays[i]).prop("checked") === true) {
                if ($.trim($("#opens" + weekDays[i]).val()) !== "") {
                    if ($.trim($("#close" + weekDays[i]).val()) === "") {
                        alert("Please select closing time of the day of " + weekFulldayString[i] + ".");
                        $("#close" + weekDays[i]).focus();
                        status = false;
                    }
                } else if ($.trim($("#close" + weekDays[i]).val()) !== "") {
                    if ($.trim($("#opens" + weekDays[i]).val()) === "") {
                        alert("Please select opening time of the day of " + weekFulldayString[i] + ".");
                        $("#opens" + weekDays[i]).focus();
                        status = false;
                    }
                }
            }
        }
    } else if (markupType === "product") {
        if ($.trim($("#name").val()) === "") {
            alert("Please enter product name.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#url").val()) === "") {
            alert("Please enter a product url.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" && !urlValidation($("#url").val())) {
            alert("Please enter a valid url.");
            $("#url").focus();
            status = false;
        } else if ($("#image").val() === "") {
            alert("Please enter image path.");
            $("#image").focus();
            status = false;
        } else if (!imageValidation($("#image").val())) {
            alert("Please enter a valid image path.");
            $("#image").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== ""
                && floatValidation($("#ratingValue").val()) && $.trim($("#ratingValue").val()) > 5) {
            alert("Average rating should not be morethan 5.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== "" && !floatValidation($("#ratingValue").val())) {
            alert("Please enter average rating in a valid format.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== "" && $.trim($("#reviewCount").val()) === "") {
            alert("Please enter number of reviews for the product.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) !== "" && !numberValidation($("#reviewCount").val())) {
            alert("Please enter number of reviews in a valid format.");
            $("#reviewCount").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) !== "" && $.trim($("#ratingValue").val()) === "") {
            alert("Please enter average rating for the product.");
            $("#ratingValue").focus();
            status = false;
        } else if (($.trim($("#priceCurrency").val()) !== "" || $.trim($("#availability").val()) !== ""
                || $.trim($("#priceValidUntil").val()) !== "" || $.trim($("#itemOffered").val()) !== ""
                || $.trim($("#itemCondition").val()) !== "" || $.trim($("#sname").val()) !== "")
                && $.trim($("#price").val()) === "") {
            alert("Please enter offer price of the product.");
            $("#price").focus();
            status = false;
        } else if ($.trim($("#price").val()) !== "" && !numberValidation($("#price").val())) {
            alert("Please enter offer price in a valid format.");
            $("#reviewCount").focus();
            status = false;
        } else if ($.trim($("#price").val()) !== "" && $.trim($("#priceCurrency").val()) === "") {
            alert("Please enter offer price currency.");
            $("#priceCurrency").focus();
            status = false;
        }
    } else if (markupType === "organization") {
        if ($.trim($("#legalName").val()) === "") {
            alert("Please enter the name of the organization.");
            $("#legalName").focus();
            status = false;
        } else if ($.trim($("#url").val()) === "") {
            alert("Please enter URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" && !(urlValidation($("#url").val()))) {
            alert("Please enter a valid URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#logoUrl").val()) !== "" && !imageValidation($("#logoUrl").val())) {
            alert("Please enter a valid logo url.");
            $("#logoUrl").focus();
            status = false;
        } else if ($("#streetAddress").val() === "") {
            alert("Please enter street address.");
            $("#streetAddress").focus();
            status = false;
        } else if ($("#addressLocality").val() === "") {
            alert("Please enter your city.");
            $("#addressLocality").focus();
            status = false;
        } else if ($("#addressRegion").val() === "") {
            alert("Please enter your state.");
            $("#addressRegion").focus();
            status = false;
        } else if ($("#postalCode").val() === "") {
            alert("Please enter postal code.");
            $("#postalCode").focus();
            status = false;
        } else if ($("#addressCountry").val() === "") {
            alert("Please enter your country name.");
            $("#addressCountry").focus();
            status = false;
        }
    } else if (markupType === "recipe") {
        if ($("#name").val() === "") {
            alert("Please enter name of the dish.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#image").val()) === "") {
            alert("Please enter an image URL.");
            $("#imageUrl").focus();
            status = false;
        } else if ($.trim($("#image").val()) !== "" && !imageValidation($("#image").val())) {
            alert("Please enter a valid image URL.");
            $("#imageUrl").focus();
            status = false;
        } else if ($.trim($("#recipeYield").val()) !== "" && !numberValidation($("#recipeYield").val())) {
            alert("Please enter a valid number of recipe yield.");
            $("#recipeYield").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== ""
                && floatValidation($("#ratingValue").val()) && $.trim($("#ratingValue").val()) > 5) {
            alert("Average rating should not be morethan 5.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== "" && !floatValidation($("#ratingValue").val())) {
            alert("Please enter average rating in a valid format.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== "" && $.trim($("#reviewCount").val()) === "") {
            alert("Please enter number of reviews for the recipe.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) !== "" && !numberValidation($("#reviewCount").val())) {
            alert("Please enter number of reviews in a valid format.");
            $("#reviewCount").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) !== "" && $.trim($("#ratingValue").val()) === "") {
            alert("Please enter average rating for the recipe.");
            $("#ratingValue").focus();
            status = false;
        }
    } else if (markupType === "video") {
        if ($("#name").val() === "") {
            alert("Please enter video title.");
            $("#name").focus();
            status = false;
        } else if ($("#description").val() === "") {
            alert("Please enter description of the video.");
            $("#description").focus();
            status = false;
        } else if ($.trim($("#thumbnailUrl").val()) === "") {
            alert("Please enter thumbnail image URL of video.");
            $("#thumbnailUrl").focus();
            status = false;
        } else if (!imageValidation($("#thumbnailUrl").val())) {
            alert("Please enter a valid thumbnail image URL of video.");
            $("#thumbnailUrl").focus();
            status = false;
        } else if ($.trim($("#uploadDate").val()) === "") {
            alert("Please enter upload date.");
            $("#uploadDate").focus();
            status = false;
        } else if ($.trim($("#contentUrl").val()) !== "" && !urlValidation($("#contentUrl").val())) {
            alert("Please enter a valid content URL.");
            $("#contentUrl").focus();
            status = false;
        } else if ($.trim($("#embedUrl").val()) !== "" && !urlValidation($("#embedUrl").val())) {
            alert("Please enter a valid embed URL.");
            $("#embedUrl").focus();
            status = false;
        } else if ($.trim($("#interactionCount").val()) !== "" && !numberValidation($("#interactionCount").val())) {
            alert("Please enter a valid interaction count.");
            $("#interactionCount").focus();
            status = false;
        }
    } else if (markupType === "website") {
        if ($("#name").val() === "") {
            alert("Please enter the website name.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#url").val()) === "") {
            alert("Please enter a website URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#url").val()) !== "" && !urlValidation($("#url").val())) {
            alert("Please enter a valid URL.");
            $("#url").focus();
            status = false;
        } else if ($.trim($("#target").val()) !== "" && !urlValidation($("#target").val())) {
            alert("Please enter a valid URL for search.");
            $("#target").focus();
            status = false;
        } else if ($.trim($("#target").val()) !== "" &&
                !($("#target").val().indexOf("http://") !== -1 || $("#target").val().indexOf("https://") !== -1)) {
            alert("Search query URL should be start with http:// or https://.");
            $("#target").focus();
            status = false;
        } else if ($.trim($("#target").val()) !== "" && $.trim($("#query-input").val()) === "") {
            alert("Please enter search parameter.");
            $("#query-input").focus();
            status = false;
        } else if ($.trim($("#query-input").val()) !== "" && $.trim($("#target").val()) === "") {
            alert("Please enter a search query URL.");
            $("#target").focus();
            status = false;
        }
    } else if (markupType === "aggregateRating") {
        if ($.trim($("#name").val()) === "") {
            alert("Please enter the reviewed item name.");
            $("#name").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) === "") {
            alert("Please enter average rating.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== ""
                && floatValidation($("#ratingValue").val()) && $.trim($("#ratingValue").val()) > 5) {
            alert("Average rating should not be morethan 5.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingValue").val()) !== "" && !floatValidation($("#ratingValue").val())) {
            alert("Please enter average rating in a valid format.");
            $("#ratingValue").focus();
            status = false;
        } else if ($.trim($("#ratingCount").val()) === "") {
            alert("Please enter number of ratings.");
            $("#ratingCount").focus();
            status = false;
        } else if ($.trim($("#ratingCount").val()) !== "" && !numberValidation($("#ratingCount").val())) {
            alert("Please enter number of ratings in a valid format.");
            $("#ratingCount").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) === "") {
            alert("Please enter number of reviews.");
            $("#reviewCount").focus();
            status = false;
        } else if ($.trim($("#reviewCount").val()) !== "" && !numberValidation($("#reviewCount").val())) {
            alert("Please enter number of reviews in a valid format.");
            $("#reviewCount").focus();
            status = false;
        }
    } else if (markupType === "breadcrumbs") {
        var level = parseInt($('#level option:selected').val());
        var titles = ["homepage", "2nd level", "current page"];
        if (level === 3) {
            titles = ["homepage", "2nd level", "3rd level", "current page"];
        }
        for (var i = 0; (i <= level) && (status === true); i++) {
            var name = $.trim($("#name" + i).val());
            var item = $.trim($("#item" + i).val());
            if (name === "") {
                alert("please enter a " + titles[i] + " title.");
                $("#name" + i).focus();
                status = false;
            } else if (i < level && item === "") {
                alert("please enter a " + titles[i] + " page URL.");
                $("#item" + i).focus();
                status = false;
            } else if (i < level && item !== "" && !urlValidation(item)) {
                alert("please enter a valid page URL.");
                $("#item" + i).focus();
                status = false;
            }
        }
    } else if (markupType === "jobPosting") {
        var hiringOrgName = $('#hName').val();
        $("#iName").val(hiringOrgName);
        if ($("#title").val().trim() === "") {
            alert("Please enter the job title.");
            $("#title").focus();
            status = false;
        } else if ($("#iValue").val().trim() === "") {
            alert("Please enter the job code.");
            $("#iValue").focus();
            status = false;
        } else if ($("#description").val().trim() === "") {
            alert("Please enter the description of the job.");
            $("#description").focus();
            status = false;
        } else if ($("#datePosted").val().trim() === "") {
            alert("Please enter the job posting date.");
            $("#datePosted").focus();
            status = false;
        } else if ($("#validThrough").val().trim() === "") {
            alert("Please enter the job posting expires date.");
            $("#validThrough").focus();
            status = false;
        } else if ($("#employmentType").val().trim() === "") {
            alert("Please select type of the employment.");
            $("#employmentType").focus();
            status = false;
        } else if ($("#hName").val().trim() === "") {
            alert("Please enter the hiring organization name.");
            $("#hName").focus();
            status = false;
        } else if ($("#hValue").val().trim() === "") {
            alert("Please enter the hiring organization website URL.");
            $("#hValue").focus();
            status = false;
        } else if (!urlValidation($("#hValue").val().trim())) {
            alert("Please enter a valid hiring organization website URL.");
            $("#hValue").focus();
            status = false;
        } else if ($("#streetAddress").val().trim() === "") {
            alert("Please enter street address.");
            $("#streetAddress").focus();
            status = false;
        } else if ($("#addressLocality").val().trim() === "") {
            alert("Please enter your city.");
            $("#addressLocality").focus();
            status = false;
        } else if ($("#addressRegion").val().trim() === "") {
            alert("Please enter your state.");
            $("#addressRegion").focus();
            status = false;
        } else if ($("#postalCode").val().trim() === "") {
            alert("Please enter postal code.");
            $("#postalCode").focus();
            status = false;
        } else if ($("#addressCountry").val().trim() === "") {
            alert("Please enter your country name.");
            $("#addressCountry").focus();
            status = false;
        } else if ($("#currency").val().trim() === "") {
            alert("Please enter currency code.");
            $("#currency").focus();
            status = false;
        } else if ($("#bsValue").val().trim() === "") {
            alert("Please enter the base salary.");
            $("#bsValue").focus();
            status = false;
//        } else if ($("#bsValue").val().trim() !== "" && (!(floatValidation($("#bsValue").val().trim())) 
//                && !(numberValidation($("#bsValue").val().trim())))) {
//            alert("Please enter the base salary in a valid format.");
//            $("#bsValue").focus();
//            status = false;
//        } else if ($("#bsMaxValue").val().trim() !== "" && (!(floatValidation($("#bsMaxValue").val().trim())) 
//                && !(numberValidation($("#bsMaxValue").val().trim())))) {
//            alert("Please enter the max. base salary in a valid format.");
//            $("#bsMaxValue").focus();
//            status = false;
        } else if ($("#unitText").val().trim() === "") {
            alert("Please enter the base salary unit.");
            $("#unitText").focus();
            status = false;
        }
    }
    return status;
}
function numberValidation(inputText) {
    var numbers = /^[0-9]+$/;
    if (inputText.match(numbers))
    {
        return true;
    }
    return false;
}
function floatValidation(inputText) {
    var numbers = /^[0-9]|([0-9]+\.+[0-9]{1,5})+$/;
    if (inputText.match(numbers))
    {
        return true;
    }
    return false;
}
function imageValidation(imageURL) {
    return(imageURL.match(/\.(jpeg|jpg|gif|png)$/) !== null);
}
function urlValidation(url) {
    var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if (!re.test(url)) {
        return false;
    } else {
        return true;
    }
}

//function selectElementContents(el) {
//    $('.ss_datesel_inp_cont input').prop('disabled', false);
//    $(".copy-text").hide();
//    var body = document.body, range, sel;
//    if (document.createRange && window.getSelection) {
//        range = document.createRange();
//        sel = window.getSelection();
//        sel.removeAllRanges();
//        try {
//            range.selectNodeContents(el);
//            sel.addRange(range);
//        } catch (e) {
//            range.selectNode(el);
//            sel.addRange(range);
//        }
//        document.execCommand("copy");
//        $(".copy-text").text("Copied");
//        $(".copy-text").show();
////        $('.copy-text').delay(5000).fadeOut(400);
//    } else if (body.createTextRange) {
//        range = body.createTextRange();
//        range.moveToElementText(el);
//        range.select();
//        document.execCommand("copy");
//        document.selection.empty();
//        clearSelection();
//        $(".copy-text").text("Copied");
//        $(".copy-text").show();
//        $('.copy-text').delay(5000).fadeOut(400);
//    }
//    $('.ss_datesel_inp_cont input').prop('disabled', true);
//}
function clearSelection() {
    if (document.selection) {
        document.selection.empty();
    } else if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }
}
function getInputCurrencyList() {
    var currencyList = '<datalist id="currency-list"><option value="USD" selected="selected">United States Dollars</option>'
            + '<option value="EUR">Euro</option>'
            + '<option value="GBP">United Kingdom Pounds</option>'
            + '<option value="DZD">Algeria Dinars</option>'
            + '<option value="ARP">Argentina Pesos</option>'
            + '<option value="AUD">Australia Dollars</option>'
            + '<option value="ATS">Austria Schillings</option>'
            + '<option value="BSD">Bahamas Dollars</option>'
            + '<option value="BBD">Barbados Dollars</option>'
            + '<option value="BEF">Belgium Francs</option>'
            + '<option value="BMD">Bermuda Dollars</option>'
            + '<option value="BRR">Brazil Real</option>'
            + '<option value="BGL">Bulgaria Lev</option>'
            + '<option value="CAD">Canada Dollars</option>'
            + '<option value="CLP">Chile Pesos</option>'
            + '<option value="CNY">China Yuan Renmimbi</option>'
            + '<option value="CYP">Cyprus Pounds</option>'
            + '<option value="CSK">Czech Republic Koruna</option>'
            + '<option value="DKK">Denmark Kroner</option>'
            + '<option value="NLG">Dutch Guilders</option>'
            + '<option value="XCD">Eastern Caribbean Dollars</option>'
            + '<option value="EGP">Egypt Pounds</option>'
            + '<option value="FJD">Fiji Dollars</option>'
            + '<option value="FIM">Finland Markka</option>'
            + '<option value="FRF">France Francs</option>'
            + '<option value="DEM">Germany Deutsche Marks</option>'
            + '<option value="XAU">Gold Ounces</option>'
            + '<option value="GRD">Greece Drachmas</option>'
            + '<option value="HKD">Hong Kong Dollars</option>'
            + '<option value="HUF">Hungary Forint</option>'
            + '<option value="ISK">Iceland Krona</option>'
            + '<option value="INR">India Rupees</option>'
            + '<option value="IDR">Indonesia Rupiah</option>'
            + '<option value="IEP">Ireland Punt</option>'
            + '<option value="ILS">Israel New Shekels</option>'
            + '<option value="ITL">Italy Lira</option>'
            + '<option value="JMD">Jamaica Dollars</option>'
            + '<option value="JPY">Japan Yen</option>'
            + '<option value="JOD">Jordan Dinar</option>'
            + '<option value="KRW">Korea (South) Won</option>'
            + '<option value="LBP">Lebanon Pounds</option>'
            + '<option value="LUF">Luxembourg Francs</option>'
            + '<option value="MYR">Malaysia Ringgit</option>'
            + '<option value="MXP">Mexico Pesos</option>'
            + '<option value="NLG">Netherlands Guilders</option>'
            + '<option value="NZD">New Zealand Dollars</option>'
            + '<option value="NOK">Norway Kroner</option>'
            + '<option value="PKR">Pakistan Rupees</option>'
            + '<option value="XPD">Palladium Ounces</option>'
            + '<option value="PHP">Philippines Pesos</option>'
            + '<option value="XPT">Platinum Ounces</option>'
            + '<option value="PLZ">Poland Zloty</option>'
            + '<option value="PTE">Portugal Escudo</option>'
            + '<option value="ROL">Romania Leu</option>'
            + '<option value="RUR">Russia Rubles</option>'
            + '<option value="SAR">Saudi Arabia Riyal</option>'
            + '<option value="XAG">Silver Ounces</option>'
            + '<option value="SGD">Singapore Dollars</option>'
            + '<option value="SKK">Slovakia Koruna</option>'
            + '<option value="ZAR">South Africa Rand</option>'
            + '<option value="KRW">South Korea Won</option>'
            + '<option value="ESP">Spain Pesetas</option>'
            + '<option value="XDR">Special Drawing Right (IMF)</option>'
            + '<option value="SDD">Sudan Dinar</option>'
            + '<option value="SEK">Sweden Krona</option>'
            + '<option value="CHF">Switzerland Francs</option>'
            + '<option value="TWD">Taiwan Dollars</option>'
            + '<option value="THB">Thailand Baht</option>'
            + '<option value="TTD">Trinidad and Tobago Dollars</option>'
            + '<option value="TRL">Turkey Lira</option>'
            + '<option value="VEB">Venezuela Bolivar</option>'
            + '<option value="ZMK">Zambia Kwacha</option>'
            + '<option value="EUR">Euro</option>'
            + '<option value="XCD">Eastern Caribbean Dollars</option>'
            + '<option value="XDR">Special Drawing Right (IMF)</option>'
            + '<option value="XAG">Silver Ounces</option>'
            + '<option value="XAU">Gold Ounces</option>'
            + '<option value="XPD">Palladium Ounces</option>'
            + '<option value="XPT">Platinum Ounces</option>'
            + '</datalist>';
    return currencyList;
}

function timePickerIn24Hours(hours, minutes) {
    var listOfhours = '';
    var listOfMinutes = '';
    if (hours) {
        listOfhours += '<datalist id="hours-list">';
        +'<option value="00">00</option>'
                + '<option value="01">01</option>'
                + '<option value="02">02</option>'
                + '<option value="03">03</option>'
                + '<option value="04">04</option>'
                + '<option value="05">05</option>'
                + '<option value="06">06</option>'
                + '<option value="07">07</option>'
                + '<option value="08">08</option>'
                + '<option value="09">09</option>'
                + '<option value="10">10</option>'
                + '<option value="11">11</option>'
                + '<option value="12">12</option>'
                + '<option value="13">13</option>'
                + '<option value="14">14</option>'
                + '<option value="15">15</option>'
                + '<option value="16">16</option>'
                + '<option value="17">17</option>'
                + '<option value="18">18</option>'
                + '<option value="19">19</option>'
                + '<option value="20">20</option>'
                + '<option value="21">21</option>'
                + '<option value="22">22</option>'
                + '<option value="23">23</option>';
        +'</datalist>';
    }
    if (minutes) {
        listOfMinutes += '<datalist id="minutes-list">'
                + '<option value="00">00</option>'
                + '<option value="05">05</option>'
                + '<option value="10">10</option>'
                + '<option value="15">15</option>'
                + '<option value="20">20</option>'
                + '<option value="25">25</option>'
                + '<option value="30">30</option>'
                + '<option value="35">35</option>'
                + '<option value="40">40</option>'
                + '<option value="45">45</option>'
                + '<option value="50">50</option>'
                + '<option value="55">55</option>'
                + '<option value="60">60</option>';
        +'</datalist>';
    }
    return listOfhours + listOfMinutes;
}
function timePickerIn12Hours() {
    var listOfHoursMinutes = '<option value="">-- Select --</option>'
            + '<option value="00:00">00:00</option>'
            + '<option value="00:10">00:10</option>'
            + '<option value="00:20">00:20</option>'
            + '<option value="00:30">00:30</option>'
            + '<option value="00:40">00:40</option>'
            + '<option value="00:50">00:50</option>'
            + '<option value="01:00">01:00</option>'
            + '<option value="01:10">01:10</option>'
            + '<option value="01:20">01:20</option>'
            + '<option value="01:30">01:30</option>'
            + '<option value="01:40">01:40</option>'
            + '<option value="01:50">01:50</option>'
            + '<option value="02:00">02:00</option>'
            + '<option value="02:10">02:10</option>'
            + '<option value="02:20">02:20</option>'
            + '<option value="02:30">02:30</option>'
            + '<option value="02:40">02:40</option>'
            + '<option value="02:50">02:50</option>'
            + '<option value="03:00">03:00</option>'
            + '<option value="03:10">03:10</option>'
            + '<option value="03:20">03:20</option>'
            + '<option value="03:30">03:30</option>'
            + '<option value="03:40">03:40</option>'
            + '<option value="03:50">03:50</option>'
            + '<option value="04:00">04:00</option>'
            + '<option value="04:10">04:10</option>'
            + '<option value="04:20">04:20</option>'
            + '<option value="04:30">04:30</option>'
            + '<option value="04:40">04:40</option>'
            + '<option value="04:50">04:50</option>'
            + '<option value="05:00">05:00</option>'
            + '<option value="05:10">05:10</option>'
            + '<option value="05:20">05:20</option>'
            + '<option value="05:30">05:30</option>'
            + '<option value="05:40">05:40</option>'
            + '<option value="05:50">05:50</option>'
            + '<option value="06:00">06:00</option>'
            + '<option value="06:10">06:10</option>'
            + '<option value="06:20">06:20</option>'
            + '<option value="06:30">06:30</option>'
            + '<option value="06:40">06:40</option>'
            + '<option value="06:50">06:50</option>'
            + '<option value="07:00">07:00</option>'
            + '<option value="07:10">07:10</option>'
            + '<option value="07:20">07:20</option>'
            + '<option value="07:30">07:30</option>'
            + '<option value="07:40">07:40</option>'
            + '<option value="07:50">07:50</option>'
            + '<option value="08:00">08:00</option>'
            + '<option value="08:10">08:10</option>'
            + '<option value="08:20">08:20</option>'
            + '<option value="08:30">08:30</option>'
            + '<option value="08:40">08:40</option>'
            + '<option value="08:50">08:50</option>'
            + '<option value="09:00">09:00</option>'
            + '<option value="09:10">09:10</option>'
            + '<option value="09:20">09:20</option>'
            + '<option value="09:30">09:30</option>'
            + '<option value="09:40">09:40</option>'
            + '<option value="09:50">09:50</option>'
            + '<option value="10:00">10:00</option>'
            + '<option value="10:10">10:10</option>'
            + '<option value="10:20">10:20</option>'
            + '<option value="10:30">10:30</option>'
            + '<option value="10:40">10:40</option>'
            + '<option value="10:50">10:50</option>'
            + '<option value="11:00">11:00</option>'
            + '<option value="11:10">11:10</option>'
            + '<option value="11:20">11:20</option>'
            + '<option value="11:30">11:30</option>'
            + '<option value="11:40">11:40</option>'
            + '<option value="11:50">11:50</option>'
            + '<option value="12:00">12:00</option>'
            + '<option value="12:10">12:10</option>'
            + '<option value="12:20">12:20</option>'
            + '<option value="12:30">12:30</option>'
            + '<option value="12:40">12:40</option>'
            + '<option value="12:50">12:50</option>';
    return listOfHoursMinutes;
}
function timePickerIn24Hours() {
    var listOfHoursMinutes = '<option value="">-- Select --</option>'
            + '<option value="00:00">00:00</option>'
            + '<option value="00:30">00:30</option>'
            + '<option value="01:00">01:00</option>'
            + '<option value="01:30">01:30</option>'
            + '<option value="02:00">02:00</option>'
            + '<option value="02:30">02:30</option>'
            + '<option value="03:00">03:00</option>'
            + '<option value="03:30">03:30</option>'
            + '<option value="04:00">04:00</option>'
            + '<option value="04:30">04:30</option>'
            + '<option value="05:00">05:00</option>'
            + '<option value="05:30">05:30</option>'
            + '<option value="06:00">06:00</option>'
            + '<option value="06:30">06:30</option>'
            + '<option value="07:00">07:00</option>'
            + '<option value="07:30">07:30</option>'
            + '<option value="08:00">08:00</option>'
            + '<option value="08:30">08:30</option>'
            + '<option value="09:00">09:00</option>'
            + '<option value="09:30">09:30</option>'
            + '<option value="10:00">10:00</option>'
            + '<option value="10:30">10:30</option>'
            + '<option value="11:00">11:00</option>'
            + '<option value="11:30">11:30</option>'
            + '<option value="12:00">12:00</option>'
            + '<option value="12:30">12:30</option>'
            + '<option value="13:00">13:00</option>'
            + '<option value="13:30">13:30</option>'
            + '<option value="14:00">14:00</option>'
            + '<option value="14:30">14:30</option>'
            + '<option value="15:00">15:00</option>'
            + '<option value="15:30">15:30</option>'
            + '<option value="16:00">16:00</option>'
            + '<option value="16:30">16:30</option>'
            + '<option value="17:00">17:00</option>'
            + '<option value="17:30">17:30</option>'
            + '<option value="18:00">18:00</option>'
            + '<option value="18:30">18:30</option>'
            + '<option value="19:00">19:00</option>'
            + '<option value="19:30">19:30</option>'
            + '<option value="20:00">20:00</option>'
            + '<option value="20:30">20:30</option>'
            + '<option value="21:00">21:00</option>'
            + '<option value="21:30">21:30</option>'
            + '<option value="22:00">22:00</option>'
            + '<option value="22:30">22:30</option>'
            + '<option value="23:00">23:00</option>'
            + '<option value="23:30">23:30</option>';
    return listOfHoursMinutes;
}

function showGoogleTestingTool() {
    $(".sdgt-tool").show();
//    Google structure data testing tool append by using embed
    if ($(".sdgt-tool").html().trim() === "") {
        $(".sdgt-tool").html('<embed src="https://search.google.com/structured-data/testing-tool/u/0/"/>');
    }
}