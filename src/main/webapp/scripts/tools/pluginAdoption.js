
/*Constructing the request to fetch account list.*/
function fetchAccounts() {
    jQuery.getJSON("/plugin-adoption.html?action=getAccounts",
            function (data) {
                constructPluginAccounts(data);
            }).fail(function () {
        $("#emptyAccountsData").show();
        $("#pluginAdoptionAccounts").closest('tr').remove();
        $("#pluginAdoptionAccounts").hide();

    });
}

/*Constructing table with expert user's list.*/
function constructPluginAccounts(data) {
    var tableRows = null;
    $("#pluginAdoptionAccounts").show();
    $("#emptyAccountsData").hide();
    $("#accountCountWarpper").empty();
    $("#accountCountWarpper").show();
    var tableRow = "<thead><tr>";
    tableRow += "<th><span class=\"selectALLWrap\" >Select</span></th>";
    tableRow += "<th class=\"userFields\">Account Name</th>";
    tableRow += "<th class=\"userFields\">Account Id</th>";
    tableRow += "<th class=\"userFields\">Status</th>";
    tableRow += "<th class=\"userFields\">Manager Email</th>";
    tableRow += "<th class=\"userFields\">Team Emails</th>";
    tableRow += "<th class=\"userFields\">Edit</th>";
    tableRow += "</tr></thead><tbody>";
    tableRows += tableRow;
    var accountsExisted = false;
    var countAccount = 0;
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            accountsExisted = true;
            tableRow = "<tr>";
            tableRow += "<td class=\"accountSelectWrapper\"><input type=\"checkbox\" id=\"pluginAccount" + data[key].accountId +
                    "\" name=\"selected_User\" value= \""
                    + data[key].accountId + "\" ></td>";
            tableRow += "<td><span class=\"pluginAccountData\" >" + data[key].accountName + "</span></td>";
            tableRow += "<td><span class=\"pluginAccountData\" >" + data[key].customerId + "</span></td>";
            if (data[key].accountStatus) {
                tableRow += "<td><span class=\"pluginAccountData\" >Active</span></td>";
            } else {
                tableRow += "<td><span class=\"pluginAccountData\" >In Active</span></td>";
            }
            tableRow += "<td><span class=\"pluginAccountData\" >" + data[key].managerEmail + "</span></td>";
            var teamMemberMails = "";
            if (data[key].pluginAdoptionTeamDTO.length !== 0) {
                for (var i = 0; i < data[key].pluginAdoptionTeamDTO.length; i++) {
                    teamMemberMails += "<p>" + data[key].pluginAdoptionTeamDTO[i].teamMemberEmail + "</p>";
                }
            } else {
                teamMemberMails += "<p>-</p>";
            }
            tableRow += "<td><span class=\"pluginAccountData\" >" + teamMemberMails + "</span></td>";
            tableRow += "<td><div class=\"editPluginAccount\" title =\"Edit\" onClick=\"editAccount(" + data[key].accountId + ")\" ></div></td>";
            tableRow += "</tr>";
            tableRows += tableRow;
            tableRows += "</tbody>";
            $("#pluginAdoptionAccounts").html(tableRows);
            countAccount++;
        }
    }
    if (!accountsExisted) {
        $("#emptyAccountsData").show();
        $("#pluginAdoptionAccounts").closest('tr').remove();
        $("#pluginAdoptionAccounts").hide();
    } else {
        $("#accountCountWarpper").html("Total Plugin Accounts: " + countAccount);
    }
}

function editAccount(accountId) {
    $(".accountSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
    var params = {acccountId: accountId};
    $('#accountName, #customerId, #managerEmail, #teamEmailId-0,#emailDyanmicWrapper input[type=text]').removeClass("errorValidation");
    jQuery.getJSON("/plugin-adoption.html?action=edit", params,
            function (data) {
                $("#accountId").empty();
                $("#accountStatus").empty();
                constructEditPluginAccounts(data);
            });
}
function constructEditPluginAccounts(data) {
    $("#accountTitle").html("Update Account");
    $("#accountStatusWrapper").show();
    if (data.accountStatus) {
        $("#accountState").html('INACTIVE');
        $("#accountStatus").val(0);
        $('#accountState').css('background-color', 'gray');
    } else {
        $("#accountState").html('ACTIVE');
        $("#accountStatus").val(1);
        $('#accountState').css('background-color', 'rgb(81, 136, 24)');
    }
    $("#accountId").val(data.accountId);
    $('#accountName').val(data.accountName);
    $('#customerId').val(data.customerId);
    $("#managerEmail").val(data.managerEmail);
    if (data.pluginAdoptionTeamDTO.length !== 0) {
        if (data.pluginAdoptionTeamDTO[0].teamMemberEmail !== null) {
            $('#teamEmailId-0').val(data.pluginAdoptionTeamDTO[0].teamMemberEmail);
        }
        /*Team Emails to edit*/
        for (var i = 1; i < data.pluginAdoptionTeamDTO.length; i++) {
            generateEditTeamEmail(data.pluginAdoptionTeamDTO[i].teamMemberEmail);
        }
    }
    $("#accountUpdate").show();
    $("#accountSubmit").hide();
    $("#adoptionPopUp").show();
    $("#accountAddPopUp").show();
}
function generateEditTeamEmail(teamMembereEmail) {
    var teamDiv = $("#emailDyanmicWrapper").children();
    var inputString = "";
    var result = "";
    if (teamDiv.length > 0) {
        result = $("#emailDyanmicWrapper").children().last().attr('id');
        var cloneId = parseInt(result.substr(result.length - 1, result.length - 1));
        inputString = cloneEditInputDiv((cloneId + 1), teamMembereEmail);
    } else {
        inputString = cloneEditInputDiv((teamDiv.length + 1), teamMembereEmail);
    }
    if (teamDiv.length === 0)
        $("#emailDyanmicWrapper").html(inputString);
    else
        $("#emailDyanmicWrapper").children(":last-child").after(inputString);

}
function cloneEditInputDiv(sno, teamEmail) {

    var childInput = '<div id="teamMemmberDiv-' + sno + '" class="teamEmailInput">' +
            '<input type="text" value=' + teamEmail + ' name="pluginAdoptionTeamDTO[' + sno + '].teamMemberEmail" class="teamMemberEmail" id="teamEmailId-' + sno + '" style="height:29px;margin-right:5px;">' +
            '<img class="remove" style=""  onClick="removeElement(\'' + sno + '\')" title="Remove" alt="Remove" src="../images/tools/remove.png" /></div>';
    return childInput;
}
function generateTeamEmail() {
    $('#processing').hide();
    $("#successMessage,#errorMessage").empty();
    $('#accountName, #customerId, #managerEmail, #teamEmailId-0,#emailDyanmicWrapper input[type=text]').removeClass("errorValidation");
    var teamDiv = $("#emailDyanmicWrapper").children();
    var inputString = "";
    var result = "";
    /*Checking existed team email is null or empty.*/
    var levelOneEmail = $('#teamEmailId-0').val().trim();
    var validExistedEmail = true;
    if (levelOneEmail.length === 0) {
        $('#teamEmailId-0').addClass("errorValidation");
        $("#errorMessage").html("Please enter team member email address");
        $("#errorMessage").show();
        validExistedEmail = false;
        return false;
    } else if (teamDiv.length > 0) {
        var emailLength = $("#emailDyanmicWrapper").children().length;
        for (var emailCount = 1; emailCount <= emailLength; emailCount++) {
            levelOneEmail = $('#teamEmailId-' + emailCount + '').val();
            if (levelOneEmail.length === 0) {
                $('#teamEmailId-' + emailCount + '').addClass("errorValidation");
                $("#errorMessage").html("Please enter team member email address");
                validExistedEmail = false;
                return false;
            }
        }
    }
    validExistedEmail = true;
    if (validExistedEmail) {
        if (teamDiv.length > 0) {
            result = $("#emailDyanmicWrapper").children().last().attr('id');
            var cloneId = parseInt(result.substr(result.length - 1, result.length - 1));
            inputString = cloneInputDiv((cloneId + 1));
        } else {
            inputString = cloneInputDiv((teamDiv.length + 1));
        }
        if (teamDiv.length === 0)
            $("#emailDyanmicWrapper").html(inputString);
        else
            $("#emailDyanmicWrapper").children(":last-child").after(inputString);
    }
}

function cloneInputDiv(sno) {

    var childInput = '<div id="teamMemmberDiv-' + sno + '" class="teamEmailInput">' +
            '<input type="text" name="pluginAdoptionTeamDTO[' + sno + '].teamMemberEmail" size=26 class="teamMemberEmail" id="teamEmailId-' + sno + '" style="height:29px;margin-right:5px;">' +
            '<img class="remove" style=""  onClick="removeElement(\'' + sno + '\')" title="Remove" alt="Remove" src="../images/tools/remove.png" /></div>';
    return childInput;
}
function removeElement(id)
{
    $("#teamMemmberDiv-" + id).remove();
}
function accountSubmit() {

    $('#accountName, #customerId, #managerEmail, #teamEmailId-0,#emailDyanmicWrapper input[type=text]').removeClass("errorValidation");
    if (validateAccountForm()) {
        $('#errorMessage').empty();
        $("#processing").show();
        $.ajax({
            type: "POST",
            url: "/plugin-adoption.html?action=save-account",
            processData: true,
            data: $("#pluginAdoptionDTO").serialize(),
            dataType: "json",
            success: function (data) {
                $("#processing").hide();
                $("#errorMessage,#successMessage").empty();
                $("#errorMessage,#successMessage").show();
                if (data === 'Account added successfully.') {
                    console.log("inside condition account added data is: " + data);
                    $('#successMessage').html(data);
                    $('#accountName, #customerId, #managerEmail, #teamEmailId-0').val("");
                    $("#emailDyanmicWrapper").empty();
                    setTimeout(function () {
                        $('#successMessage').fadeOut('slow');
                    }, 3000);
                    fetchAccounts();
                } else {
                    $('#errorMessage').html(data);
                    setTimeout(function () {
                        $('#errorMessage').fadeOut('slow');
                    }, 3000);
                }
            }
        });
    }
}

function updateAccountState() {
    var pluginAccountID = $("#accountId").val();
    var accountStatus = $("#accountStatus").val();
    var params = {acccountId: pluginAccountID, acccountStatus: accountStatus};
    jQuery.getJSON("/plugin-adoption.html?action=active-inactive", params,
            function (data) {
                alert(data);
                $("#errorMessage,#successMessage").empty();
                $("#errorMessage,#successMessage").show();
                if (data.includes("changed as Active")) {
                    $("#accountState").html('INACTIVE');
                    $("#accountStatus").val(0);
                    $('#successMessage').html(data);
                    $('#errorMessage').empty();
                    $('#accountState').css('background-color', 'gray');
                } else if (data.includes("changed as InActive")) {
                    $("#accountState").html('ACTIVE');
                    $("#accountStatus").val(1);
                    $('#errorMessage').html(data);
                    $('#successMessage').empty();
                    $('#accountState').css('background-color', '#14b45a');
                }
                fetchAccounts();
            });
}

function updateAccountDetails() {

    if (validateAccountForm()) {
        $("#processing").show();
        $.ajax({
            type: "POST",
            url: "/plugin-adoption.html?action=update",
            processData: true,
            data: $("#pluginAdoptionDTO").serialize(),
            dataType: "json",
            success: function (data) {
                $("#processing").hide();
                $("#errorMessage,#successMessage").empty();
                $("#errorMessage,#successMessage").show();
                if (data === 'Account details updated successfully.') {
                    $('#successMessage').html(data);
                    $('#errorMessage').empty();
                    fetchAccounts();
                } else {
                    $('#errorMessage').html(data);
                    $('#successMessage').empty();
                }
            }
        });
    }
}

function  validateAccountForm() {
    $("#successMessage,#errorMessage").empty();
    $('#accountName, #customerId, #managerEmail, #teamEmailId-0,#emailDyanmicWrapper input[type=text]').removeClass("errorValidation");
    var isValidAll = true;
    var errorNotification = "";
    var accName = $('#accountName').val().trim();
    if (accName === "" || accName === null) {
        $('#accountName').addClass("errorValidation");
        errorNotification = "Please enter all details.";
        isValidAll = false;
    }
    var custId = $('#customerId').val().trim();
    if (custId === "" || custId === null || custId.length === 0) {
        $('#customerId').addClass("errorValidation");
        if (errorNotification === "") {
            errorNotification = "Please enter all details.";
        }
        isValidAll = false;
    }
    var managerEmail = $("#managerEmail").val().trim();
    if (managerEmail.length === 0) {
        $('#managerEmail').addClass("errorValidation");
        if (errorNotification === "") {
            errorNotification = "Please enter all details.";
        }
        isValidAll = false;
    } else if (!isValidMail(managerEmail)) {
        $('#managerEmail').addClass("errorValidation");
        if (errorNotification === "" || errorNotification === "Please enter all details.") {
            errorNotification = "Please enter valid email address.";
        } else if (errorNotification !== "") {
            errorNotification = "Please enter all details and valid email address.";
        }
        isValidAll = false;
    }
    var emailLength = 0;
    var teamSupportEmail;
    var emailCount;
    if ($("#emailDyanmicWrapper").children().length > 0) {
        emailLength = $("#emailDyanmicWrapper").children().length + 7;
    }

    /*Validating the team email Id's.*/
    if (emailLength > 0) {
        for (emailCount = 0; emailCount <= emailLength; emailCount++) {
            teamSupportEmail = $('#teamEmailId-' + emailCount + '').val();
            if (teamSupportEmail !== undefined) {
                if (teamSupportEmail.length === 0) {
                    if (errorNotification === "") {
                        errorNotification = "Please enter all details.";
                    } else if (errorNotification !== "") {
                        errorNotification = "Please enter all details and valid email address.";
                    }
                    $('#teamEmailId-' + emailCount + '').addClass("errorValidation");
                    isValidAll = false;
                } else if (teamSupportEmail.length !== 0) {
                    if (!isValidMail(teamSupportEmail)) {
                        if (errorNotification === "") {
                            errorNotification = "Please enter valid email address.";
                        } else if (errorNotification !== "") {
                            errorNotification = "Please enter all details and valid email address.";
                        }
                        $('#teamEmailId-' + emailCount + '').addClass("errorValidation");
                        isValidAll = false;
                    }
                }

                for (var duplicateEmailCount = 0; duplicateEmailCount <= emailLength; duplicateEmailCount++) {
                    if (parseInt(emailCount) !== parseInt(duplicateEmailCount)) {
                        var checkEmail = $('#teamEmailId-' + duplicateEmailCount + '').val();
                        if (checkEmail !== undefined && checkEmail.length !== 0) {
                            if (checkEmail.trim() === teamSupportEmail.trim()) {
                                errorNotification = "You have entered duplicate team member mail id's.";
                                $('#teamEmailId-' + duplicateEmailCount + '').addClass("errorValidation");
                                $('#teamEmailId-' + emailCount + '').addClass("errorValidation");
                                isValidAll = false;
                                $('#errorMessage').show();
                            }
                        }
                    }
                }
            }
        }
    } else {
        teamSupportEmail = $('#teamEmailId-0').val().trim();
        if (teamSupportEmail.length !== 0) {
            if (!isValidMail(teamSupportEmail)) {
                if (errorNotification === "") {
                    errorNotification = "Please enter valid email address.";
                } else if (errorNotification !== "") {
                    errorNotification = "Please enter all details and valid email address.";
                }
                $('#teamEmailId-0').addClass("errorValidation");
                isValidAll = false;
            }
        }
    }


    var accountReg = $('#customerId').val().trim().match(/^[0-9\-]+$/);
    /*Validating the account Id.*/
    if (!accountReg) {
        if (errorNotification === "") {
            errorNotification = "Please enter valid account number.";
            $('#customerId').addClass("errorValidation");
        }
        isValidAll = false;
    }

    if (isValidAll) {
        var teamMemberEmailIds = [];
        /*Checking manager email is existed in team member emails*/
        for (emailCount = 0; emailCount <= emailLength; emailCount++) {
            teamSupportEmail = $('#teamEmailId-' + emailCount + '').val();
            if (teamSupportEmail !== undefined) {
                teamMemberEmailIds[emailCount] = teamSupportEmail.trim();
            }
        }
        if (teamMemberEmailIds.length > 0) {
            var indexFound = jQuery.inArray(managerEmail.trim(), teamMemberEmailIds);
            if (indexFound !== -1) {
                errorNotification = "Manager email and team member email should be different.";
                $("#errorMessage").html(errorNotification);
                return false;
            }
        }
        return true;
    } else {
        /*Preparing error notification.*/
        $("#errorMessage").html(errorNotification);
        return false;
    }
}

function resetAccountForm() {
    /*Reset all input fields*/
    $('#accountName, #customerId, #managerEmail, #teamEmailId-0,#teamMember-0').val("");
    $('#accountName, #customerId, #managerEmail, #teamEmailId-0').removeClass("errorValidation");
    $('#accountStatusWrapper,#accountUpdate').hide();
    $("#emailDyanmicWrapper,#errorMessage,#successMessage,#accountId,#accountStatus").empty();
    $(".accountSelectWrapper").find('input[type=checkbox]:checked').removeAttr('checked');
}
