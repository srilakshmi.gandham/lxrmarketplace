var urlsAnalysisLimit = 100;
if (isLocalUser) {
    urlsAnalysisLimit = 1000;
}
function enterKey(e) {
    if (window.event)
        keycode = window.event.keyCode;
    else if (e)
        keycode = e.which;
    if (keycode === 13) {
        return false;
    } else {
        return true;
    }
}
function getLines(id) {
    return $.trim($('#' + id).val().split("\n").length);
}
function getTruncData(id,manaullyURLLimit) {
    var data = $('#' + id).val();
    var len = data.split("\n").length;
    var trunData = "";
    if(len > manaullyURLLimit){
        len = manaullyURLLimit;
    }
    console.log("len: "+len+", manaullyURLLimit: "+manaullyURLLimit);
    /*if (len > 100) {
    alert(len > 100)*/
        var eachRow = data.split("\n");
        for (var i = 0; i < len; i++) {
            if (i === len-1) {
                console.log("i: "+i+", URL: "+eachRow[i]);
                trunData += eachRow[i];
                break;
            }else {
                console.log("i: "+i+", URL: "+eachRow[i]);
                trunData += eachRow[i] + "\n";
            }
        }
    return trunData;
}
function getResultFun() {
    console.log("isLocalUser:: "+isLocalUser+", urlsAnalysisLimit:: "+urlsAnalysisLimit);
    $("#inputUrlError").empty();
    if (!($.trim($("#manualUrl").val()) || $.trim($("#selColumn").text()))) {
        $("#inputUrlError").html("Please enter URL(S) or Upload File.");
        $("#manualUrl").focus();
        removeFileContent();
        return;
    } else if (getLines('manualUrl') > urlsAnalysisLimit) {
        if (isLocalUser) {
            $("#inputUrlError").html("Only 1000 URL(S) can be verified at a time!");
        } else {
            $("#inputUrlError").html("Only 100 URL(S) can be verified at a time!");
        }
    } else if (getLines('manualUrl') <= urlsAnalysisLimit) {
        var data = getTruncData('manualUrl', urlsAnalysisLimit);
        $("#manualUrl").val(data);
        $("#manaullyURL").val(data);
        $("#manualUrl").focus();
        $("#getResult").button('loading');

        $("#manualUrl, #url, #keyword").prop("readonly", true);
        document.getElementById("uploadedFile").disabled = true;
        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", "disabled");


        var f = $("#urlValidator");
        var column = $("#selColumn").text();
        f.attr('action', "url-auditor-tool.html?getResult='getResult'&column=" + column);
        if (column.length > 0) {
            f.attr('action', "url-auditor-tool.html?getResult='getResult'&fileField='testFile'&column=" + column);
        }
        f.submit();
    } else {
        $("#getResult").button('loading');

        $("#manualUrl, #url, #keyword").prop("readonly", true);
        document.getElementById("uploadedFile").disabled = true;
        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", "disabled");

        var f = $("#urlValidator");
        var column = $("#selColumn").text();
        f.attr('method', "POST");
        if (column.length > 0) {
//            alert("In colum if: "+ column.length+", keyword: "+$("#keyword").val());
            f.attr('action', "url-auditor-tool.html?getResult='getResult'&fileField='testFile'&column=" + column);
            f.submit();
            //  f.attr('action',  a+"?getResult='getResult'&fileField='testFile'&column="+column);
        } else {
//            alert("In colum else: "+ column.length+", keyword: "+$("#keyword").val());
            f.attr('action', "/url-auditor-tool.html?getResult='getResult'&column=" + column);
            f.submit();
        }
    }
}

function preventFun(event) {
    if ($("#destOkUrlPopUp").css("display") === "none") {
    } else {
        event.preventDefault();
    }
}


function showPopup() {
    var elem = document.getElementById("downloadButton");
    var left = getElementLeft("downloadFormat");
    var top = getElementTop("downloadFormat");
    $("#downloadFormat").width(($("#downloadButton").width()) - 6);
    $("#downloadFormat").show();
}

function showHelpPopup(helpimgvalue, idvalue) {
    var repOs = $('#helpimg' + helpimgvalue).position();
    var rep = $('#helpimg' + helpimgvalue); //outerHeight(true)
    $('#helpImgToolTip' + idvalue).css({
        'left': repOs.left + rep.outerWidth(true),
        'top': repOs.top + rep.outerHeight(true),
        'position': 'absolute',
        'z-index': 1000,
        'display': 'block'
    });
}

function hideHelpPopup(helpimgvalue, idvalue, e) {
    if (($(e.target).parents('#helpImgToolTip' + idvalue).length === 0
            && !$(e.target).is('#helpImgToolTip' + idvalue) && !$(e.target)
            .is('#helpimg' + helpimgvalue))
            || $(e.target).is('#otherTr')) {
        $('#helpImgToolTip' + idvalue).hide();
        document.getElementById("helpImgToolTip" + idvalue).style.display = "none";
    }
}

function hideTempPopup(helpimgvalue, idvalue, e) {
    if (document.getElementById("helpImgToolTip" + idvalue).style.display === 'block') {
        document.getElementById("helpImgToolTip" + idvalue).style.display = "none";
    }
}

function clearMail() {
    $("#aftermailsent").hide();
}
                                                    