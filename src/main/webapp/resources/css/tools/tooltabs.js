//tools vertical tabsrelated
$(document).ready(function () {
    // Tabs content hide and shows
    $(".content").css("display", "none");
    $(".active").css("display", "block");

    var contentheit = $('#firstcontent-1').outerHeight();

    wrapper = $(".tabs");
    tabs = wrapper.find(".tab");
    tabToggle = wrapper.find(".tab-toggle");
    function openTab() {
        var content = $(this).parent().next(".content"),
                activeItems = wrapper.find(".active");

        if (!$(this).hasClass('active')) {
            $(this).add(content).add(activeItems).toggleClass('active');
        }
        // Tabs content hide and shows
        $(".content").css("display", "none");
        $(".active").css("display", "block");
    }

    tabToggle.on('click', openTab);
    $(window).on('load', function () {
        tabToggle.first().trigger('click');
    });
});

function hideHowToFixAndShowContent(id) {
    $('#howtofix-' + id).hide();
    $('#firstcontent-' + id).show(1000);
}

function hideContentAndShowHowToFix(id) {
    $('#firstcontent-' + id).hide();
    var contentheight = $('#howtofix-' + id).outerHeight();
    $('.content').css('background-color', 'white');
    $('#howtofix-' + id).show(1000);
}