/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function sendMyEbook(param) {


    $("#ebookName,#ebookName").removeClass('inputErr');
    
    var name = $("#ebookName").val().trim();
    var email = $("#ebookEmail").val().trim();
    if (name === "") {
        $("#ebookName").addClass('inputErr');
        return false;
    } 
    if (email === "") {
        $("#ebookEmail").addClass('inputErr');
        return false;
    } else if (!isValidMail(email)) {
        $("#ebookEmail").addClass('inputErr');
        return false;
    }

    $("#ebookName").removeClass('inputErr');
    $("#ebookEmail").removeClass('inputErr');
    
    var $this = $(param);
    $this.button('loading');

    $.ajax({
        type: "POST",
        url: "/lxrm-send-user-request.html?emailId=" + email + "&name=" + name,
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data === 'success') {
                alert("Please check your mailbox to download the Ebook.");
            } else {
                alert("Soory! Please try again.");
            }
            $this.button('reset');
        },
        error: function (xhr) {
            $this.button('reset');
            alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
        }
    });

}

