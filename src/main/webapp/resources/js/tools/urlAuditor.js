var urlsAnalysisLimit = 100;
if (isLocalUser) {
    urlsAnalysisLimit = 1000;
}
function showSubTd(childID) {
    $('.urlChild' + childID + '').toggle();
    var status = $('#urlParent' + childID + '').html();
    if (status === '-') {
        $('#urlParent' + childID + '').text('+');
    } else if (status === '+') {
        $('#urlParent' + childID + '').text('-');
    }
}
function enterKey(e) {
    if (window.event)
        keycode = window.event.keyCode;
    else if (e)
        keycode = e.which;
    if (keycode == 13) {
        return false;
    } else {
        return true;
    }
}
function getLines(id) {
    return $.trim($('#' + id).val().split("\n").length);
}
function getTruncData(id, manaullyURLLimit) {
    var data = $('#' + id).val();
    var len = data.split("\n").length;
    var trunData = "";
    if (len > manaullyURLLimit) {
        len = manaullyURLLimit;
    }
    console.log("len: " + len + ", manaullyURLLimit: " + manaullyURLLimit);
    var eachRow = data.split("\n");
    for (var i = 0; i < len; i++) {
        if (i == len - 1) {
            console.log("i: " + i + ", URL: " + eachRow[i]);
            trunData += eachRow[i];
            break;
        } else {
            console.log("i: " + i + ", URL: " + eachRow[i]);
            trunData += eachRow[i] + "\n";
        }
    }
    return trunData;
}
function getResultFun() {
    console.log("isLocalUser:: "+isLocalUser+", urlsAnalysisLimit:: "+urlsAnalysisLimit);
    $("#inputUrlError").empty();
    if (!($.trim($("#manualUrl").val()) || $.trim($("#selColumn").text()))) {
        $("#inputUrlError").html("Please enter URL(S) or Upload File.");
        $("#manualUrl").focus();
        removeFileContent();
        return;
    } else if (getLines('manualUrl') > 100) {
        if (getLines('manualUrl') > 100) {
            alert("Only 100 URL(S) can be verified at a time!");
        }else if (isLocalUser && getLines('manualUrl') > 1000) {
            alert("Only 1000 URL(S) can be verified at a time!");
        }
        var data = getTruncData('manualUrl', 100);
        $("#manualUrl").val(data);
        $("#manaullyURL").val(data);
        $("#manualUrl").focus();
        $("#getResult").button('loading');

        $("#manualUrl, #url, #keyword").prop("readonly", true);
        document.getElementById("uploadedFile").disabled = true;
        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", "disabled");


        var f = $("#urlValidator");
        var column = $("#selColumn").text();
        f.attr('action', "url-auditor-tool.html?getResult='getResult'&column=" + column);
        if (column.length > 0) {
            f.attr('action', "url-auditor-tool.html?getResult='getResult'&fileField='testFile'&column=" + column);
        }
        f.submit();
    } else {
        $("#getResult").button('loading');

        $("#manualUrl, #url, #keyword").prop("readonly", true);
        document.getElementById("uploadedFile").disabled = true;
        $("#getResult ,#resetURL, .reportFormat, #sendReport, #browseFile, #selColumn").prop("disabled", "disabled");

        var f = $("#urlValidator");
        var column = $("#selColumn").text();
        f.attr('method', "POST");
        if (column.length > 0) {
//            alert("In colum if: "+ column.length+", keyword: "+$("#keyword").val());
            f.attr('action', "url-auditor-tool.html?getResult='getResult'&fileField='testFile'&column=" + column);
            f.submit();
            //  f.attr('action',  a+"?getResult='getResult'&fileField='testFile'&column="+column);
        } else {
//            alert("In colum else: "+ column.length+", keyword: "+$("#keyword").val());
            f.attr('action', "/url-auditor-tool.html?getResult='getResult'&column=" + column);
            f.submit();
        }
    }
}
function resetToolInputFields() {
    if ($.trim($('#fileName').text()) !== "" || $.trim($('#selColumn').text()) !== "" ||
            $.trim($('.qq-upload-list').text()) !== "") {
        var response = confirm('Uploaded file needs to be removed before entering the URL(s) manually.\nDo you want to remove now ?');
        if (response === true) {
            $.ajax({
                type: "POST",
                url: "/url-auditor-tool.html?clearResults=clearResults",
                processData: true,
                data: {},
                dataType: "json",
                success: function (responseJSON){
                    if(responseJSON[0] === "removed"){
                        removeFileContent();
                    }
                }, error: function (jqxhr, textStatus, error) {
                        if (textStatus.indexOf('error') !== -1) {
                            toolLimitExceededForAjax();
                        }
                    }, complete: function () {
                        removeFileContent();
                    }
            });
        } else {
            $('#manualUrl').val("");
        }
    }
}

function clearMail() {
    $("#aftermailsent").hide();
}
function removeFileContent() {
    $("#uploadedFile").val('');
   $("#fileName,#selColumn").empty();
}