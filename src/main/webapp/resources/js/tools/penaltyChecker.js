/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function bulidResultChartAndTable(isOnload) {
    var data = mainData;
    if (data[0] !== null && data[0].indexOf("working") !== -1) {
        $("#notWorking").html(data[0]);
        $("#notWorking").show();
    } else {
        $("#resultDiv").show();
        $("#website").html(data[1]);
        $("#sDate").html(data[2]);
        $("#eDate").html(data[3]);
        $("#organicTraffic").html(data[4]);
        startDateValue = new Date(Date.parse(data[0][0].date));
        endDateValue = new Date(Date.parse(data[0][data[0].length - 1].date));
        options.hAxis.viewWindow.min = startDateValue;
        options.hAxis.viewWindow.max = endDateValue;
        if (isOnload) {
            drawChart('websiteTrafficChart', data);
        } else {
            google.setOnLoadCallback(drawChart);
        }
        if (data[1] !== null && data[1] !== "") {
            var googleUpdateData = data[5];
            var resultTable = "<tr><th id='updateDateCol'>Updated Date</th><th id='trafficCol'>Organic Traffic</th>"
                    + "<th id='updateTypeCol'>Update Type</th><th id='updateCol'>Update</th></tr>";
            if (googleUpdateData.length > 0) {
                for (var i = 0; i < googleUpdateData.length; i++) {
                    var updateDate = dateFormat(new Date(googleUpdateData[i].updatedDate));
                    resultTable += "<tr><td>" + updateDate.getDate() + "-" + (updateDate.getMonth() + 1) + "-" + updateDate.getFullYear()
                            + "<td style='text-align:right;'>" + googleUpdateData[i].organicTraffic + "</td>"
                            + "<td>" + googleUpdateData[i].updateType + "</td>"
                            + "<td>" + googleUpdateData[i].description + "</td></tr>";
                }
            } else {
                resultTable += "<tr><td style='text-align:center;' colspan='4'>No google update falls on selected date range</td></tr>";
            }
            $("#resultDivTable").html(resultTable);
        }
//                    getAskExpertPopup(true);
    }
}
function drawChart() {
    var data = new google.visualization.DataTable();
    var successData = mainData;
    var pageViewAndReachData = successData[0];
    var googleUpdateData = successData[5];
    var maxValue = successData[7];
    if (pageViewAndReachData.length === 0) {
        $("#websiteTrafficChart").empty();
        $('<p class="no-chart-data">No data available.</p>').appendTo("#websiteTrafficChart");
        $("#deviceDownload").hide();
    } else {
        data.addColumn('date', 'Month');
        data.addColumn('number', 'Visits');
        var updatesList = successData[6];
        if (updatesList.length > 0) {
            for (var i = 0; i < updatesList.length; i++) {
                data.addColumn('number', updatesList[i]);
                data.addColumn({type: 'string', role: 'tooltip', p: {"html": true}});
            }
        }

        var dataObjArray = [];
        if (pageViewAndReachData !== null) {
            var googleDataIndex = 0;
            for (var i = 0; i < pageViewAndReachData.length; i++) {
                var dataSetArray = [];
                var alexaTrafficDate = "";
                var googleUpdatedDate = "";
                alexaTrafficDate = dateFormat(new Date(Date.parse(pageViewAndReachData[i].date)));
                if (googleDataIndex < googleUpdateData.length) {
                    googleUpdatedDate = dateFormat(new Date(Date.parse(googleUpdateData[googleDataIndex].updatedDate)));
                }
                if (new Date(alexaTrafficDate).getTime() > new Date(googleUpdatedDate).getTime()) {
                    dataSetArray.push(new Date(Date.parse(pageViewAndReachData[i].date)));
                    dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                    i--;
                } else if (new Date(alexaTrafficDate).getTime() === new Date(googleUpdatedDate).getTime()) {
                    dataSetArray.push(new Date(Date.parse(googleUpdateData[googleDataIndex].updatedDate)));
                    dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                } else {
                    dataSetArray.push(new Date(Date.parse(pageViewAndReachData[i].date)));
                    dataSetArray.push(parseInt(pageViewAndReachData[i].dailyreach));
                }
                if ((googleDataIndex < googleUpdateData.length) && (googleUpdateData.length > 0)
                        && (new Date(alexaTrafficDate).getTime() >= new Date(googleUpdatedDate).getTime()))
                {
                    for (var j = 0; j < updatesList.length; j++) {
                        if (updatesList[j] === googleUpdateData[googleDataIndex].updateType) {
                            dataSetArray.push(maxValue);
                            dataSetArray.push(googleUpdateData[googleDataIndex].toolTip);
                        } else {
                            dataSetArray.push(0);
                            dataSetArray.push("");
                        }
                    }
                    googleDataIndex++;
                } else {
                    for (var j = 0; j < updatesList.length; j++) {
                        dataSetArray.push(0);
                        dataSetArray.push("");
                    }
                }
                dataObjArray.push(dataSetArray);
            }
            data.addRows(dataObjArray);
        }

        var chart = new google.visualization.ComboChart(document.getElementById("websiteTrafficChart"));
        var deviceDownload = document.getElementById('deviceDownload');
        // Disabling the button while the chart is drawing.
        $("#previousButton").disabled = true;
        $("#nextButton").disabled = true;
        $("#zoomButton").disabled = true;
        // Wait for the chart to finish drawing before calling the getImageURI() method.
        google.visualization.events.addListener(chart, 'ready', function () {
            deviceDownload.innerHTML = '<img style="display:none" id="chartImg" src="' + chart.getImageURI() + '">';
            $("#previousButton").disabled = options.hAxis.viewWindow.min <= startDateValue;
            $("#nextButton").disabled = options.hAxis.viewWindow.max >= endDateValue;
            $("#zoomButton").disabled = false;
        });
        google.visualization.events.addListener(chart, 'select', selectHandler);
        function selectHandler() {
            var selection = chart.getSelection();
            var message = '';
            var popupText = "";
            for (var i = 0; i < selection.length; i++) {
                var item = selection[i];
                var date = new Date(data.getValue(selection[0].row, 0));
                var weekNumber = getWeekNumber(date);
                // if column is not line chart then will show popup for update information
                if (!(item.column === 1)) {
                    for (var k = 0; k < googleUpdateData.length; k++) {
                        var updateDate = new Date(Date.parse(googleUpdateData[k].updatedDate));
                        var updateWeek = getWeekNumber(updateDate);
                        if (updateWeek === weekNumber) {
                            $("#tooltip-heading").html("Google Update: " + googleUpdateData[k].updateType);
                            popupText = "<div><b>Updated Date</b>: " + googleUpdateData[k].updatedDate + "</div>";
                            popupText += "<div><b>Description</b>: " + googleUpdateData[k].description + "</div>";
                        }
                    }
                }
            }
            if (popupText !== "") {
                $('#tooltip-content').html(popupText);
                $('#tooltip-popup-out-box').show();
            }
        }
        google.visualization.events.addListener(chart, 'onmouseover', onMouseHoverOnBarChart);
        google.visualization.events.addListener(chart, 'onmouseout', OnMouseOutOnBarChart);
        chart.draw(data, options);
        function onMouseHoverOnBarChart() {
            $('#websiteTrafficChart').css('cursor', 'pointer');
        }
        function OnMouseOutOnBarChart() {
            $('#websiteTrafficChart').css('cursor', 'default');
        }
    }
}