/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var adSpyV2APP = angular.module('adSpyV2PreSales', []);

adSpyV2APP.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

adSpyV2APP.controller('AdSpyV2Controller', ['$scope', '$http', function ($scope, $http) {

        var self = this;
        self.adSpyProcessedList = [];
        self.selectedMetrics = [];
        self.fromDate = null;
        self.toDate = null;
        self.isResponseSuccess = false;
        self.updatedAPIBalanceCount;
        self.adPresence = false;
        self.adwordsBudget = false;
        var offLineURLSLimit = 100;
        var toolUsagelimitCount = 3000;
        var offlineReportEmail = "";
        var messageToUser = "The list of URLs provided to analyze exceeds the recommended limit of 100 URLs. If you wish to continue, the results of the analysis will be sent to your email offline within 30-60 minutes (Estimated). Alternately, you can use less than 100 URLs at a time to get instant results to view/download";
        var limitExceededMsgPrefix = "Your feed contains ";
        var limitExceededMsPostFix = " company URL(S). Please upload a maximum of " + toolUsagelimitCount + " company URL(S).";

        $scope.metrics = ["Website's Visits & Ad Presence", "Estimated Monthly Adwords Budget"];


        $scope.toggleSelection = function toggleSelection(metric) {
            var idx = self.selectedMetrics.indexOf(metric);
            console.log("idx::" + idx);
            if (idx > -1) {
                console.log("removed" + idx);
                self.selectedMetrics.splice(idx, 1);
                removeItem(metric);
            } else {
                console.log("added: " + metric);
                // is newly selected
                self.selectedMetrics.push(metric);
            }
        };

        function removeItem(item) {
            for (var i = 0; i < self.selectedMetrics.length; i++) {
                if (self.selectedMetrics[i] === item) {
                    console.log("removed" + item + ", at index:: " + i);
                    self.selectedMetrics.splice(i, 1);
                    i--; // Prevent skipping an item
                }
            }
        }

        $scope.processAdSpyQuery = function () {
            if (isGmailLoginSuccess) {
                clearErrorMessage();
                if (validateInputs()) {
                    var manaullyQueryText = $scope.queryDomains;
                    if (manaullyQueryText !== undefined && manaullyQueryText !== null && manaullyQueryText !== "") {
                        submitManullyQuery();
                    }
                    if ($("#domainsList")[0].files[0] !== undefined && $("#domainsList")[0].files[0] !== '' && $("#domainsList")[0].files[0] !== null) {
                        submitFileQuery();
                    }
                } else {
                    $("#adSpyResultWrapper").hide();
                    self.adSpyProcessedList = [];
                    $("#toolInfoMessage").empty();
                    self.adPresence = false;
                    self.adwordsBudget = false;
                    resetCheckBoxValue();
                }
            } else {
                gmailLoginSuccess();
                $("#googleLoginPopupModal").modal('show');
                var errorMessage = sessionStorage.getItem('PSMAError');
                if (errorMessage !== null) {
                    $("#loginErrorMessage").empty();
                    $("#errorBodyWrapper").show();
                    $("#loginErrorMessage").html(errorMessage);
                }
                $("#loginErrorMessage").empty();
                $("#errorBodyWrapper").hide();
            }

        };
        $scope.resetAdSpyForm = function () {
            $("#filePath").val('');
            $("#queryDomains").val('');
            $("#domainsList").val(null);
            $("#domainsList, #filePath, #inputUrlError, #toolInfoMessage,#metricSelectError").empty();
            $("#adSpyResultWrapper").hide();
            $('input[type=checkbox]').prop('checked', false);
            offlineReportEmail = "";
            self.adSpyProcessedList = [];
            self.fromDate = null;
            self.toDate = null;
            self.isResponseSuccess = false;
            self.adPresence = false;
            self.adwordsBudget = false;
            enableInputs();
            resetCheckBoxValue();
        };
        function resetCheckBoxValue() {
            self.selectedMetrics = [];
            $.each($("input[name='selectedOptions']:checked"), function () {
                $(this).prop("checked", false);
            });
        }

//        $("input:checkbox").change(function () {
//            var ischecked = $(this).is(':checked');
//            if (!ischecked)
//                alert('uncheckd ' + $(this).val());
//        });
        $scope.acceptConditions = function () {
//            alert("acceptConditions");
            $('#offlineError').show();
            $('#offlineError').empty();
            var emailText = $.trim($("#offlineEmail").val());
            var isValid = true;
            var errorMsg = "";
            if (emailText === null || emailText === "") {
                errorMsg = "Please enter your email id";
                isValid = false;
            } else if (!isValidMail(emailText)) {
                errorMsg = "Please enter a valid email";
                isValid = false;
            }
            if (!isValid) {
                $("#offlineEmail").focus();
                $("#offlineError").html(errorMsg);
                $('#offlineError').css("color", "red");
                return false;
            } else {
                $("#confirmationPopupModal").modal('hide');
                offlineReportEmail = emailText;
                submitFormRequest();
            }
        };
        $scope.cancelInput = function () {
            $("#confirmationPopupModal").modal('hide');
            $("#toolInfoMessage").empty();
            enableInputs();
        };
        $scope.downloadAnalysisReport = function () {
            var downloadForm = null;
            downloadForm = $("#adSpyV2Form");
            downloadForm.attr('method', "POST");
            downloadForm.attr('action', "/ad-spy-v2.html?request=download");
            downloadForm.submit();
        };
        $scope.sendAnalysisReport = function () {
            $('#mail-sent-status').empty();
            var emailid = $.trim($("#email").val());
            var status = true;
            var statusMsg = "";
            if (emailid === "") {
                statusMsg = "Please enter your email id";
                status = false;
            } else if (!isValidMail(emailid)) {
                statusMsg = "Please enter a valid email address.";
                status = false;
            }
            $('#mail-sent-status').show();
            if (!status) {
                $("#email").focus();
                $("#mail-sent-status").html(statusMsg);
                $('#mail-sent-status').css("color", "red");
                return false;
            } else {
                $('#mail-sent-status').html("sending...");
                $('#mail-sent-status').css("color", "green");
                $.ajax({
                    type: "POST",
                    url: "/ad-spy-v2.html?request=sendmail&email=" + emailid + "&toolType=netelixirusers",
                    processData: true, data: {},
                    dataType: "json",
                    success: function (data) {
                    }, complete: function () {
                        $('#mail-sent-status').empty();
                        $('#mail-sent-status').show();
                        $('#mail-sent-status').html("Mail sent successfully");
                        $('#mail-sent-status').css("color", "green");
                    }
                });
            }
        };
        $scope.clearAnalaysisCount = function () {
            $("#toolInfoMessage").empty();
        };
        $scope.resetManaully = function () {
            $scope.queryDomains = '';
            $scope.queryDomains = null;
            resetCheckBoxValue();
        };
        function submitManullyQuery() {
//            alert("in submitManullyQuery");
            var size = getCountofQueryDomains('queryDomains');
            console.log("submitManullyQuery size:: " + size);
            if (size > toolUsagelimitCount) {
                $("#toolInfoMessage").empty();
                $("#toolInfoMessage").html("<span class='lxr-danger' style='font-size:20px;'><i class='fa fa-times-circle'></i></span><span style='color:red' clas='lxrm-bold'>&nbsp;" + limitExceededMsgPrefix + size + limitExceededMsPostFix + "</span>");
                $("#adSpyResultWrapper").hide();
            } else if (size >= offLineURLSLimit) {
                $('#offlineError').empty();
                raiseConfirmation();
            } else if (size <= toolUsagelimitCount) {
                submitFormRequest();
            }
        }
        ;
        function submitFileQuery() {
//            alert("in submitFileQuery");
            disableInputs();
            $("#toolInfoMessage").empty();
            $("#toolInfoMessage").html("<span style='color:#333333' clas='lxrm-bold'>Validating the uploaded file feed, please wait..</span>");
            var requestURL = "/ad-spy-v2.html?request=validateURLSSize";
            var config = {
                headers: {
                    'Content-Type': undefined
                }
            };
            var oMyForm = new FormData();
            oMyForm.append("fileList", $scope.domainsList);
            /*Saving response result list*/
            $http.post(requestURL, oMyForm, config)
                    .then(function (response) {
                        if (response.data === -1 || response.data === 0) {
                            $("#toolInfoMessage").empty();
                            $("#toolInfoMessage").html("<span class='lxr-danger' style='font-size:20px;'><i class='fa fa-times-circle'></i></span><span style='color:red' clas='lxrm-bold'>&nbsp;Unable to prepare company URL(S) feed. Mandatory column is missing (OR) Error in reading data from file.</span>");
                            enableInputs();
                            offlineReportEmail = "";
                            self.adSpyProcessedList = [];
                            self.fromDate = null;
                            self.toDate = null;
                            self.isResponseSuccess = false;
                            self.adPresence = false;
                            self.adwordsBudget = false;
                        } else if (response.data !== 0 && response.data > toolUsagelimitCount) {
                            console.log("Error in submitFileQuery and size:: " + response.data);
                            $("#toolInfoMessage").empty();
                            $("#toolInfoMessage").html("<span class='lxr-danger' style='font-size:20px;'><i class='fa fa-times-circle'></i></span><span style='color:red' clas='lxrm-bold'>&nbsp;" + limitExceededMsgPrefix + response.data + limitExceededMsPostFix + "</span>");
                            $("#adSpyResultWrapper").hide();
                            offlineReportEmail = "";
                            self.adSpyProcessedList = [];
                            self.fromDate = null;
                            self.toDate = null;
                            self.isResponseSuccess = false;
                            self.adPresence = false;
                            self.adwordsBudget = false;
                            enableInputs();
                        } else if (response.data !== 0 && response.data >= offLineURLSLimit) {
                            raiseConfirmation();
                        } else if (response.data !== 0 && response.data <= offLineURLSLimit) {
                            submitFormRequest();
                        }
                    }).catch(function () {
                console.log("Exception in validating file");
            });
        }
        ;

        function submitFormRequest() {
//            alert("submitFormRequest");
            disableInputs();
            $("#adSpyResultWrapper").hide();
            self.adSpyProcessedList = [];
            $("#toolInfoMessage").empty();
            $("#toolInfoMessage").html("<span style='color:#333333' clas='lxrm-bold'>Analyzing the website(s) to identify the Ad Presence. Usually it takes less than 5 minutes for 100 websites(s).</span>");
            $("#getResult").button('loading');
            setTimeout(function () {
                getUpdatedList();
            }, 100);
            var requestURL = "/ad-spy-v2.html?request=analyze";
            var config = {
                headers: {
                    'Content-Type': undefined
                }
            };
            var userOAuthEmail = "";
            if (sessionStorage.getItem("userOAuthEmail") !== null) {
                userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
            }
            var oMyForm = new FormData();
            if ($scope.queryDomains !== undefined && $scope.queryDomains !== null && $scope.queryDomains !== "") {
                oMyForm.append("manuallyList", $scope.queryDomains);
            } else {
                oMyForm.append("manuallyList", "");
            }
            if ($("#domainsList")[0].files[0] !== undefined && $("#domainsList")[0].files[0] !== '' && $("#domainsList")[0].files[0] !== null) {
                oMyForm.append("domainsList", $scope.domainsList);
            } else {
                oMyForm.append("domainsList", new File([0], "emptyFile"));
            }
            /*This email to get anlaysis report if query list is more than 100*/
            oMyForm.append("offlineReportEmail", offlineReportEmail);
            /*This email is to track tool usage*/
            oMyForm.append("toolUsageUserEmail", userOAuthEmail);
            oMyForm.append("selectedMetrics", self.selectedMetrics);

            console.log("selectedMetrics: " + self.selectedMetrics.length);
            $("#apiInfoMessage").hide();
            $("#apiUpdatedMessage").hide();
            /*Saving response result list*/
            $http.post(requestURL, oMyForm, config)
                    .then(function (response) {
                        enableInputs();
                        console.log("self.isResponseSuccess: " + response.data[0] + " ,self.fromDate: " + response.data[3] + ", self.toDate: " + response.data[4] + ", self.adSpyProcessedList length: " + self.adSpyProcessedList.length);
                        console.log("self.adPresence: " + response.data[6] + ", self.adwordsBudget: " + response.data[7]);
                        $("#getResult").button('reset');
                        self.isResponseSuccess = response.data[0];
                        isConditionAccepted = false;
                        $("#apiInfoMessage").hide();
                        $("#apiUpdatedMessage").show();
                        self.updatedAPIBalanceCount = response.data[5];
                        if (self.isResponseSuccess) {
                            $("#adSpyResultWrapper").show();
                            self.adSpyProcessedList = [];
                            self.adSpyProcessedList = response.data[2];
                            console.log("self.adSpyProcessedList length: " + self.adSpyProcessedList.length);
                            self.fromDate = response.data[3];
                            self.toDate = response.data[4];
                            self.adPresence = response.data[6];
                            self.adwordsBudget = response.data[7];
                            $("#toolInfoMessage").empty();
                            setTimeout(function () {
                                $("#email").val(userOAuthEmail);
                                paginationFunctionV2();
                                $("#toolInfoMessage").empty();
                            }, 200);
                            $("#toolInfoMessage").empty();
                        } else {
                            $("#toolInfoMessage").empty();
                            $("#toolInfoMessage").html("<span class='lxr-danger' style='font-size:20px;'><i class='fa fa-times-circle'></i></span><span style='color:red' clas='lxrm-bold'>" + response.data[1] + "</span>");
                        }
                    })
                    .catch(function (data) {
                        console.log("Exception in processAdSpyQuery: ", data);
                    });
        }
        ;
        function getUpdatedList() {
            setTimeout(function () {
                getCountOfAnalyzedURLs();
            }, 2000);
        }
        ;
        function validateInputs() {
            var validation = true;
            $("#inputUrlError, #toolInfoMessage,#metricSelectError").empty();
            /*Checking manaully list or file list*/
            if (!($.trim($("#queryDomains").val()) || ($("#domainsList")[0].files[0] !== undefined && $("#domainsList")[0].files[0] !== '' && $("#domainsList")[0].files[0] !== null))) {
                $("#inputUrlError").html("Please enter company website URL (S) or upload file to analyze.");
                $("#queryDomains").focus();
                validation = false;
                return;
            }
            if (($.trim($("#queryDomains").val()) && ($("#domainsList")[0].files[0] !== undefined && $("#domainsList")[0].files[0] !== '' && $("#domainsList")[0].files[0] !== null))) {
                $("#inputUrlError").html("Please choose either manual list or upload file to analyze.");
                $("#queryDomains").focus();
                validation = false;
                return;
            }
            console.log("Bfre each selectedMetrics: " + self.selectedMetrics);
            if (self.selectedMetrics.length === 0) {
                $.each($("input[name='selectedOptions']:checked"), function () {
                    self.selectedMetrics.push($(this).val());
                });
            }
            console.log("After each selectedMetrics: " + self.selectedMetrics);
            if (self.selectedMetrics.length === 0) {
                $("#metricSelectError").html("Please select metrics.");
                validation = false;
                return;
            }
            return validation;
        }
        ;
        function raiseConfirmation() {
            var userOAuthEmail = "";
            if (sessionStorage.getItem("userOAuthEmail") !== null) {
                userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
            }
            $("#offlineEmail").val(userOAuthEmail);
            $("#offlineEmail").focus();
            $("#confirmationMessageWrraper").html(messageToUser);
            $("#confirmationPopupModal").modal('show');
            $('#confirmationPopupModal').modal({backdrop: 'static', keyboard: false});
        }
        ;
        /*GET request to get updated list from controller*/
        function getCountOfAnalyzedURLs() {
            $http.get('ad-spy-v2.html?request=analyzedCount')
                    .then(function (response) {
                        console.log("Analysis status:: " + (response.data[0]) + ", and count is" + response.data[1]);
                        if (!response.data[0]) {
                            $("#toolInfoMessage").empty();
                            $("#toolInfoMessage").html("<span class='lxrm-bold' style='color:green'>" + response.data[1] + "</span><span style='color:#333333'>&nbsp URL(s) are analyzed....</span>");
                            setTimeout(function () {
                                getUpdatedList();
                            }, 3000);
                        }
                    })
                    .catch(function (data) {
                        setTimeout(function () {
                            getUpdatedList();
                        }, 3000);
                        console.log("Exception in getCountOfAnalyzedURLs: " + data);
                    });
        }
        ;
        $('#clearFilePath').click(function () {
            $("#domainsList").val('');
            $("#filePath").val('');
            $("#domainsList").val(null);
            $("#domainsList, #filePath").empty();
            resetCheckBoxValue();
        });
        function clearErrorMessage() {
            $("#inputUrlError, #toolInfoMessage,#metricSelectError").empty();
            /*$(".error").hide();
             $(".error").empty();*/
            self.isResponseSuccess = false;
            self.adPresence = false;
            self.adwordsBudget = false;
        }
        ;
        function getCountofQueryDomains(id) {
//            alert(""+$('#' + id).val().trim().split("\n").length);
            return $.trim($('#' + id).val().trim().split("\n").length);
        }
        ;

        function disableInputs() {
            $("#queryDomains,#domainsList,#resetAdSpy,#getResult,#fileUploadWrapper,#clearFilePath").prop('disabled', true);
            document.getElementById('fileUploadWrapper').style.pointerEvents = 'none';
            document.getElementById('clearFilePath').style.pointerEvents = 'none';
        }
        ;
        function enableInputs() {
            $("#queryDomains,#domainsList,#resetAdSpy,#getResult,#fileUploadWrapper,#clearFilePath").prop('disabled', false);
            document.getElementById('fileUploadWrapper').style.pointerEvents = 'auto';
            document.getElementById('clearFilePath').style.pointerEvents = 'auto';
        }
        ;
        function paginationFunctionV2() {
            console.log("In paginationFunctionV2 adPresence::" + self.adPresence + ", adwordsBudget:: " + self.adwordsBudget);
            var tableId;
            if (self.adPresence && self.adwordsBudget) {
                tableId = "adSpyV2AnalysisAdPresenceAndBudget";
            } else if (self.adPresence && !self.adwordsBudget) {
                tableId = "adSpyV2AnalysisAdPresence";
            } else if (self.adwordsBudget && !self.adPresence) {
                tableId = "adSpyV2AnalysisAdBudget";
            }
            console.log("tableId::" + tableId);
            if ($.fn.dataTable.isDataTable("#" + tableId)) {
                var table = $("#" + tableId).DataTable();
                table.destroy();
            }
            $("#" + tableId).DataTable({
                scrollX: true,
                fixedHeader: true,
                orderCellsTop: true,
                "lengthMenu": [[10, 25, -1], [10, 25, "All"]],
            });
        }
        ;
    }]);