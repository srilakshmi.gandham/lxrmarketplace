/*Submitting usery query to controller*/
function processUserQuery(userType, buttonId) {
    $("#errorMessage, #userDomainError, #competitorOneError, #competitorTwoError").empty();
    $("#errorMessage, #userDomainError, #competitorOneError, #competitorTwoError").hide();
    var isValidate = validateInputFields(userType);
    if (isValidate) {
        $("#cancel").prop("disabled", true);
        $("#save").prop("disabled", true);
        $("#getCompetitorResult").prop("disabled", true);
        $("#cancel").attr("disabled", true);
        $("#save").attr("disabled", true);
        $("#getCompetitorResult").attr("disabled", true);
        var $this = $("#" + buttonId + "");
        $this.button('loading');
        var form = $("#socialMediaTool");
        if (userType === 'guestUser') {
            form.attr('method', "POST");
            form.attr('action', "/social-media-analyzer.html?getResult='getResult'");
            form.submit();
        } else if (userType === 'existingUser') {
            form.attr('method', "POST");
            form.attr('action', "/social-media-analyzer.html?update='update'");
            form.submit();
        }
        $('#mediaUrl, #competitorDomainOne, #competitorDomainTwo').prop("readonly", true);
        return true;
    }
    return  false;
}

function validateInputFields(userType) {
    $("#userDomainError, #competitorOneError, #competitorTwoError").empty();
    $("#userDomainError, #competitorOneError, #competitorTwoError").hide();
    var queryURL = $("#mediaUrl").val();
    /*Validation of user domain*/
    if (queryURL === null || queryURL.trim() === "") {
        $("#userDomainError").show();
        $("#userDomainError").html('Please enter your URL.');
        $("#mediaUrl").focus();
        return false;
    }
    if (!urlValidation(queryURL.trim())) {
        $("#userDomainError").show();
        $("#userDomainError").html('Please enter a valid URL.');
        $('#mediaUrl').focus();
        return false;
    }
    /*Merging competitors URLs and binding to model property.*/
    var competitorURLSList = "";
    if (userType === 'guestUser') {
        queryURL = $('#competitorDomainOne').val();
        if (queryURL !== null && queryURL !== "") {
            if (!urlValidation(queryURL.trim())) {
                $("#competitorOneError").show();
                $("#competitorOneError").html('Please enter a valid URL.');
                $('#competitorDomainOne').focus();
                return false;
            } else {
                competitorURLSList += queryURL.trim();
            }
        }
        queryURL = $('#competitorDomainTwo').val();
        if (queryURL !== null && queryURL !== "") {
            if (!urlValidation(queryURL.trim())) {
                $("#competitorTwoError").show();
                $("#competitorTwoError").html('Please enter a valid URL.');
                $('#competitorDomainTwo').focus();
                return false;
            } else {
                competitorURLSList += "\n" + queryURL.trim();
            }
        }
        $("#comptetiorUrls").val(competitorURLSList.trim());
    } else if (userType === 'existingUser') {
        queryURL = $('#comp2').val();
        if (queryURL !== null && queryURL !== "") {
            if (!urlValidation(queryURL.trim())) {
                $("#competitorOneError").show();
                $("#competitorOneError").html('Please enter a valid URL.');
                $('#comp2').focus();
                return false;
            }
        }
        queryURL = $('#comp3').val();
        if (queryURL !== null && queryURL !== "") {
            if (!urlValidation(queryURL.trim())) {
                $("#competitorTwoError").show();
                $("#competitorTwoError").html('Please enter a valid URL.');
                $('#comp3').focus();
                return false;
            }
        }
    }
    return true;
}

/*Constructing Query URLs common for all tabs headers.*/
function getQueryURLSData(data) {
    var queryURLSSpan = "";
    var postMetricContent = "";
    if (data["socialResponse"]["urlsJSON"].length >= 2) {
        /*manageInputFields('show');*/
        isResultPage = true;
    } else if (data["socialResponse"]["urlsJSON"].length === 1) {
        /*manageInputFields('hide');*/

    }
    
    for (var lI = 0; lI < data["socialResponse"]["urlsJSON"].length; lI++) {
        if (data["socialResponse"]["urlsJSON"].length === 1) {
            queryURLSSpan += '<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 lxr-socialmediar-col text-center splitWord"><i class="fa fa-briefcase" aria-hidden="true">';
//            $("#mediaUrl").val(data["socialResponse"]["urlsJSON"][0]);
        } else if (data["socialResponse"]["urlsJSON"].length === 2) {
            queryURLSSpan += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 lxr-socialmediar-col text-center splitWord"><i class="fa fa-briefcase" aria-hidden="true">';
            /*Populating the query URLs*/
            $("#competitorDomainOne").val(data["socialResponse"]["urlsJSON"][1]);
            applyEdit('competitorDomainOne', 'compOneURlEdit');
            applyEdit('comp2', 'compOneURlEdit');
        } else if (data["socialResponse"]["urlsJSON"].length === 3) {
            queryURLSSpan += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 lxr-socialmediar-col text-center splitWord"><i class="fa fa-briefcase" aria-hidden="true">';
            /*Populating the query URLs*/
            $("#competitorDomainOne").val(data["socialResponse"]["urlsJSON"][1]);
            $("#competitorDomainTwo").val(data["socialResponse"]["urlsJSON"][2]);
            applyEdit('competitorDomainOne', 'compOneURlEdit');
            applyEdit('competitorDomainTwo', 'compTwoURlEdit');
            applyEdit('comp3', 'compTwoURlEdit');
            applyEdit('comp2', 'compOneURlEdit');

        }
        queryURLSSpan += '</i>&nbsp;' + data["socialResponse"]["urlsJSON"][lI] + '</h5></div>';
        if (lI === 0) {
            postMetricContent += '<div class="col-xs-12 col-sm-12 col-md-12 lxr-socialmediar-col text-center splitWord"><i class="fa fa-briefcase" aria-hidden="true"></i>&nbsp;' + data["socialResponse"]["urlsJSON"][lI] + '</h5></div>';
            $(".postMetricsURL").html(postMetricContent);
        }
    }
    applyEdit('mediaUrl', 'userDomainEdit');
    $(".analysisURLs").html(queryURLSSpan);
}

function getMetricAnalysisInJson() {
    try {
        $.ajax({
            type: "POST",
            url: "/social-media-analyzer.html?getSocialData='getSocialData'",
            processData: true,
            data: {},
            dataType: "json",
            success: function (data) {
                if (data !== null) {
//                    $("#faceBookNoData, #twitterNoData, #pinterestNoData, #youtubeNoData, #googleNoData").empty();
                    $("#faceBookNoData, #twitterNoData, #pinterestNoData, #youtubeNoData").empty();
                    getQueryURLSData(data);
                    var changeStatus = data["changeStatus"];
                    if (changeStatus === "true") {
                        $("#channelActivityWrapper, #faceBookActivityWrapper, #twitterActivityWrapper, #pinterestActivityWrapper, #youTubeActivityWrapper, #googleActivityWrapper").show();
                    } else {
                        $("#channelActivityWrapper").hide();
                    }

                    if (data["socialResponse"]["hideChannelData"]["hideFacebook"] === false) {
                        /*FaceBook Channel*/
                        $("#faceBookLikesWrapper").html(getActivityGraph(data, 'facebookFinalJSON', 'fLikes', 'facebookmetrics', 'likesCount', changeStatus, true));
                    } else {
                        $("#faceBookNoData").html(generateNoDataforChannel());
                        $("#facebookActivityWrapper, #fbMetric").hide();
                    }
                    if (data["socialResponse"]["hideChannelData"]["hideTwitter"] === false) {
                        $("#twitterFollowersWrapper").html(getActivityGraph(data, 'twitterFinalJSON', 'followers', 'twittermetrics', 'followers', changeStatus, true));
                        $("#twitterTweetsWrapper").html(getActivityGraph(data, 'twitterFinalJSON', 'tweets', 'twittermetrics', 'tweets', changeStatus, false));
                        $("#twitterLikesWrapper").html(getActivityGraph(data, 'twitterFinalJSON', 'likes', 'twittermetrics', 'favourites', changeStatus, false));
                        $("#twitterFollowingWrapper").html(getActivityGraph(data, 'twitterFinalJSON', 'following', 'twittermetrics', 'following', changeStatus, false));
                    } else {
                        $("#twitterNoData").html(generateNoDataforChannel());
                        $("#twitterActivityWrapper, #twMetrics").hide();
                    }
                    if (data["socialResponse"]["hideChannelData"]["hidePinterest"] === false) {
                        /*Pinterest Channel*/
                        $("#pinterestFollowersWrapper").html(getActivityGraph(data, 'pinterestJSON', 'pFollowers', 'pinterestmetrics', 'followers', changeStatus, true));
                        $("#pinterestPinsWrapper").html(getActivityGraph(data, 'pinterestJSON', 'pPins', 'pinterestmetrics', 'pins', changeStatus, false));
                        $("#pinterestBoardsWrapper").html(getActivityGraph(data, 'pinterestJSON', 'pBoards', 'pinterestmetrics', 'boards', changeStatus, false));
                        $("#pinterestFollowingWrapper").html(getActivityGraph(data, 'pinterestJSON', 'pFollowing', 'pinterestmetrics', 'following', changeStatus, false));
                    } else {
                        $("#pinterestNoData").html(generateNoDataforChannel());
                        $("#pinterestActivityWrapper, #pinMetrics").hide();
                    }
                    if (data["socialResponse"]["hideChannelData"]["hideYoutube"] === false) {
//                        console.log("You: " + data["socialResponse"]["hideChannelData"]["hideYoutube"]);
                        $("#youtubeSubscribersWrapper").html(getActivityGraph(data, 'youtubeFinalJSON', 'ySubscribers', 'youtubemetrics', 'subscribers', changeStatus, true));
                        $("#youtubeVideosWrapper").html(getActivityGraph(data, 'youtubeFinalJSON', 'yVideos', 'youtubemetrics', 'videos', changeStatus, false));
                        $("#youtubeViewsWrapper").html(getActivityGraph(data, 'youtubeFinalJSON', 'yViews', 'youtubemetrics', 'views', changeStatus, false));
                    } else {
                        $("#youtubeNoData").html(generateNoDataforChannel());
                        $("#youtubeActivityWrapper, #ytMetrics").hide();
                    }
                    /*Google Channel
                    if (data["socialResponse"]["hideChannelData"]["hideGoogle"] === false) {
//                        console.log("Google: " + data["socialResponse"]["hideChannelData"]["hideGoogle"]);
                        $("#googleFollowersWrapper").html(getActivityGraph(data, 'googleFinalJSON', 'gFollowers', 'googleplusmetrics', 'followers', changeStatus, true));
                    } else {
                        $("#googleNoData").html(generateNoDataforChannel());
                        $("#googleActivityWrapper, #gpMetrics").hide();
                    }*/
                    constructHashTagTabData(data);
                }
            },
            complete: function () {}
        });
    } catch (err) {
    }
}
/*Params: 
 * data : JSONresponseData
 * channel: model class(Facebook, twitter, pinterest etc).
 * metric : likes, followers, pins etc..
 * applyActivity: checking to apply color top or bottom arrows of respective channels*/
function getGraphColor(data, channel, metric) {
    var graphColor;
    if (data !== null) {
        graphColor = data["socialResponse"][channel][metric];
//        alert("key:"+data["socialResponse"][channelName][metricName]);
//        console.log("graphColor: " + graphColor);
        if (applyActivity) {
            if (graphColor === "good") {
                $("#" + channel + "Top").css('color', '#219653');
            } else if (graphColor === "bad") {
                $("#" + channel + "Bottom").css('color', '#EB5757');
            }
        }
    }
    return graphColor;
}
/*Constructing HashTag Tab content for Tab-2*/
function constructHashTagTabData(data) {
    var hashTagContent = "";
    for (var mI = 0; mI < data["socialResponse"]["urlsJSON"].length; mI++) {
        if (data["socialResponse"]["urlsJSON"].length === 1) {
            hashTagContent += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 lxr-socialmediar-col">';
        } else if (data["socialResponse"]["urlsJSON"].length === 2) {
            hashTagContent += '<div class="col-xs-6 col-sm-6 col-md-6 lxr-socialmediar-col">';
        } else if (data["socialResponse"]["urlsJSON"].length === 3) {
            hashTagContent += '<div class="col-xs-4 col-sm-4 col-md-4 lxr-socialmediar-col">';
        }
        if (data["socialResponse"]["hashTagJSON"][mI] === "Not applicable.") {
            hashTagContent += '<div class="text-center">' + data["socialResponse"]["hashTagJSON"][mI] + '</div>';
        } else {
            hashTagContent += '<div style="word-wrap: break-word;">' + data["socialResponse"]["hashTagJSON"][mI] + '</div>';
        }
        hashTagContent += '</div>';
    }
    $("#twitterHashTagWrapper").html(hashTagContent);
}
/*Constructing metric graphs based on value.*/
function getActivityGraph(data, channelName, metric, growthChannelName, growthMetric, changeStatus, applyActivity) {
//    console.log("channelName: "+channelName+", metric: "+metric);
    var activityGraphData = "";
    var changeValue = 0;
    for (var lI = 0; lI < data["socialResponse"]["urlsJSON"].length; lI++) {
//        console.log("Metric value: "+data["socialResponse"][channelName][metric][lI][metric]);
//        console.log("Change value: "+data["socialResponse"][channelName][metric][lI]["Change"]);
        if (data["socialResponse"]["urlsJSON"].length === 1) {
            activityGraphData += '<div class="col-xs-12 col-sm-10 col-md-6 col-lg-6 lxr-socialmediar-col">';
        } else if (data["socialResponse"]["urlsJSON"].length === 2) {
            activityGraphData += '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 lxr-socialmediar-col">';
        } else if (data["socialResponse"]["urlsJSON"].length === 3) {
            activityGraphData += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 lxr-socialmediar-col">';
        }
        graphColor = data["socialResponse"][growthChannelName][growthMetric];
//        console.log("channelName: "+channelName+", metric: "+metric+", graphColor: "+graphColor);
        if (data["socialResponse"][channelName][metric][lI][metric] !== -1) {
            if (lI === 0) {
                if (graphColor === "warning") {
                    /*Waring: User metric value is less than the any one of competitor metric values.*/
                    activityGraphData += '<div class="progress lxr-progress-look" style="background-color: #FFDA97;">';
                    activityGraphData += '<div class="progress-bar " role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;background-color:#fda100">' + data["socialResponse"][channelName][metric][lI][metric] + '</div>';
                } else if (graphColor === "good") {
                    /*Good: User metric value is more than the competitor metric values.*/
                    activityGraphData += '<div class="progress lxr-progress-look" style="background-color: #9CEABC;">';
                    activityGraphData += '<div class="progress-bar " role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;background-color:#009a2d">' + data["socialResponse"][channelName][metric][lI][metric] + '</div>';
                } else if (graphColor === "bad") {
                    /*Red: User metric value is less than the competitor metric values.*/
                    activityGraphData += '<div class="progress lxr-progress-look" style="background-color: #FFA3A3;">';
                    activityGraphData += '<div class="progress-bar " role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;background-color:#dd4b39">' + data["socialResponse"][channelName][metric][lI][metric] + '</div>';
                } else if (graphColor === "noCompetitor") {
                    /*Hash: No competitor value exists.*/
                    activityGraphData += '<div class="progress lxr-progress-look" style="background-color: #BDBDBD;">';
                    activityGraphData += '<div class="progress-bar " role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%;background-color:#828282">' + data["socialResponse"][channelName][metric][lI][metric] + '</div>';
                }
            } else {
                activityGraphData += '<div class="progress lxr-progress-grey">';
                activityGraphData += '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%;">' + data["socialResponse"][channelName][metric][lI][metric] + '</div>';
            }
            activityGraphData += '</div>';
            /*Constructing of growth value*/
            if (changeStatus === "true") {
                changeValue = data["socialResponse"][channelName][metric][lI]["Change"];
                if (changeValue !== "NA") {
                    if (changeValue === 0) {
                        activityGraphData += '<span class="pull-right" style="color: #828282;">(Change: ' + changeValue + ')</span>';
                    } else if (changeValue > 0) {
                        activityGraphData += '<span class="pull-right" style="color: #6FCF97;">(Change: +' + changeValue + ')</span>';
                        if (applyActivity && lI === 0) {
                            applyGrowthChange(growthChannelName, "top");
                        }
                    } else if (changeValue < 0) {
                        activityGraphData += '<span class="pull-right" style="color: #EB5757;">(Change: ' + changeValue + ')</span>';
                        if (applyActivity && lI === 0) {
                            applyGrowthChange(growthChannelName, "bottom");
                        }
                    }
                }
            }
        } else {
            activityGraphData += '<div style="text-align:center;padding:10px;"><i class="fa fa-minus fa-1x" aria-hidden="true"></i></div>';
        }
        activityGraphData += '</div>';
    }
    return  activityGraphData;
}
/*Returns hash tag list from HashTagString.*/
function getHashTagList(hashTagsContent) {
    var hashTagArrayTemp = hashTagsContent.split("  ");
    return hashTagArrayTemp;
}/*Generate div with message for empty social data*/
function generateNoDataforChannel() {
    return "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 lxr-socialmediar-col' style='text-align:center;'>Unable to fetch your metrics.</div>";
}

function allowAccessButtons() {
    $('#save').attr('disabled', false);
    $('#cancel').attr('disabled', false);
}

function showAcessSettings() {
    $("#mediaSettingsDiv").show();
    $("#socialResult").hide();
    allowAccessButtons();
//    document.getElementById("updateUrl").readOnly = false;
}


function applyEdit(inputFieldId, editId) {
    $("#" + editId + "").show();
    $("#" + inputFieldId + "").addClass("gray");
}

function applyGrowthChange(channelName, arrowType) {
    if (arrowType === "top") {
        $("#" + channelName + "Top").css('color', '#219653');
    } else if (arrowType === "bottom") {
        $("#" + channelName + "Bottom").css('color', '#EB5757');
    }
}