/*NE16T1213-Sagar*/
/*Constructs dynamic directory html and add to the manually div wrapper*/
function generateNewDirectory(divName) {
    /*Manually generated parent div*/
    var manuallyDivChildern = $("#" + divName).children();
    var cloneId = 0;
    /*To store dynamic generated directory HTML*/
    var directoryHTML = "";
    /*To assign values to generated directory HTML div*/
    var assignValuesHTML = "";

    if (manuallyDivChildern.length === 0 && $("#importDirectivesWrapper").children().length !== 0) {
        var validChilderndivs = [];
        validChilderndivs = $("#importDirectivesWrapper").find("div[id*='directory']").each(function () {
            validChilderndivs.push(this.id);
        });
        if (validChilderndivs.length !== 0) {
            divName = "importDirectivesWrapper";
        }
        assignValuesHTML = $("#importDirectivesWrapper").children().last().attr('id');
        cloneId = parseInt(assignValuesHTML.replace("directory", ""));
    } else if (manuallyDivChildern.length !== 0) {
        assignValuesHTML = $("#manuallyDirectivesWrapper").children().last().attr('id');
        cloneId = parseInt(assignValuesHTML.replace("directory", ""));
    }
    /*Step 1: Getting count of existed childerns(Directory)*/
    if (cloneId > 0) {
        directoryHTML = constructDirectory((cloneId + 1));
    } else {
        directoryHTML = constructDirectory((cloneId + 1));
    }
    /*Step 2: Adding constructed directory to the manaually parent div*/
    if (manuallyDivChildern.length === 0) {
        $("#manuallyDirectivesWrapper").html(directoryHTML);
    } else {
        $("#manuallyDirectivesWrapper").children(":last-child").after(directoryHTML);
    }

    assignValuesHTML = $("#manuallyDirectivesWrapper").children().last().attr('id');
    cloneId = parseInt(assignValuesHTML.replace("directory", ""));
    /*Step 3: Getting the selected option to display input field to specify user agent name*/
    if ($.trim($("#userAgentName").val()) === "Specify")
    {
        $("#directory" + cloneId).find("#directorySpecifyWrapper" + cloneId).css('display', 'block');
    }
    /*Step 4: updating the values to generated div*/
    if (cloneId > 0) {
        updateDirectoryFields(cloneId);
    } else {
        updateDirectoryFields(cloneId + 1);
    }
}/*EOF generateNewDirectory*/


/*Update the values in input fields*/
function updateDirectoryFields(divId) {

    /*Getting User Entred Values.*/
    var directiveAction = $.trim($("#addDirectiveType").val());
    var userAgent = $.trim($("#userAgentName").val());
    var directory = $.trim($("#directoryOrFilePath").val());
    var specifyUserAgent = $.trim($("#specialUserAgentName").val());

    $("#directory" + divId).find("#directoryAction" + divId).val(directiveAction);
    $("#directory" + divId).find("#directoryUserAgent" + divId).val(userAgent);
    $("#directory" + divId).find("#directorySpecifyAgent" + divId).val(specifyUserAgent);
    $("#directory" + divId).find("#directoryTextPath" + divId).val(directory);

}
/*Preparing JSON object for generated directory list i.e Import or Manaually*/
function prepareJSONObjectofDirectoryList(totRobotsArr, directoryType) {
    var userAgent = "User-agent: ";
    var directoryChilderndivs = [];
    if (directoryType === 'import') {
        directoryChilderndivs = $("#importDirectivesWrapper").find("div[id*='directory']").each(function () {
            directoryChilderndivs.push(this.id);
        });
    } else if (directoryType === 'manually') {
        directoryChilderndivs = $("#manuallyDirectivesWrapper").find("div[id*='directory']").each(function () {
            directoryChilderndivs.push(this.id);
        });
    }

    if (directoryChilderndivs !== null && directoryChilderndivs.length > 0) {
        for (var i = 0; i < directoryChilderndivs.length; i++) {
            var idStr = directoryChilderndivs[i].id;
            var idNum = idStr.replace("directory", "");
            var addDirectiveTypeStr = "";//value
            var tempTotString = "";
            var uAMatched = false;
//            console.log("dir: " + ("#" + idStr) + ", i:" + i);
            var diretiveType = $.trim($("#" + idStr).find("#directoryAction" + idNum).val());
            var userAgentVal = $.trim($("#" + idStr).find("#directoryUserAgent" + idNum).val());
            var diretoryPathStr = $.trim($("#" + idStr).find("#directoryTextPath" + idNum).val());
            if (userAgentVal === "Specify") {
                userAgentVal = $.trim($("#" + idStr).find("#directorySpecifyAgent" + idNum).val());
            }

            var totUserAgentStr = userAgent + userAgentVal;
            if (diretiveType === "1") {
                addDirectiveTypeStr = "Allow: " + diretoryPathStr;
            } else if (diretiveType === "2") {
                addDirectiveTypeStr = "Disallow: " + diretoryPathStr;
            }
            if (userAgentVal !== null && userAgentVal !== "") {
                if (totRobotsArr !== null && totRobotsArr.length > 0) {
                    var arrLength = totRobotsArr.length;
                    for (var t = 0; t < totRobotsArr.length; t++) {
                        if (totRobotsArr[t] !== 'undefined' && totRobotsArr[t] !== null) {
                            var tempUserAgentVal = userAgentVal.toLowerCase();
                            if (totRobotsArr[t].userAg.toLowerCase() === tempUserAgentVal) {
                                uAMatched = true;
                                var tempJsonObjStr = totRobotsArr[t].totUAStr;
                                var tempArr = tempJsonObjStr.split("\n");
                                tempTotString = "\n" + addDirectiveTypeStr;
                                for (var d = 0; d < tempArr.length; d++) {
                                    if ($.trim(tempArr[d]) === $.trim(addDirectiveTypeStr)) {
                                        tempTotString = "";
                                        break;
                                    }
                                }
                                tempJsonObjStr = tempJsonObjStr + tempTotString;
                                totRobotsArr[t].totUAStr = tempJsonObjStr;
                            }
                        }
                    }
                    if (!uAMatched) {
                        tempTotString = "\n\n" + totUserAgentStr + "\n" + addDirectiveTypeStr;
                        var tempUserAgentVal = userAgentVal.toLowerCase();
                        totRobotsArr[arrLength] = {userAg: tempUserAgentVal, totUAStr: tempTotString};
                    }
                } else {
                    tempTotString = totUserAgentStr + "\n" + addDirectiveTypeStr;
                    var tempUserAgentVal = userAgentVal.toLowerCase();
                    totRobotsArr[0] = {userAg: tempUserAgentVal, totUAStr: tempTotString};
                }
            }
        }
    }
}
/*Request ajax call to get existed robot text content for query URL*/
function getRobotsTextData(requestType, robotsArray) {
    
    /*   Login POPUP on tool limit exceeded    */
    if (!toolLimitExceeded()) {
        return false;
    }
    var showAlert = true;
    if (parentUrl.indexOf("limit-exceeded") !== -1 && userLoggedIn === 0) {
        showAlert = false;
    }
    if (showAlert) {
        var finalURL = $.trim($('#robotsTxtUrl').val());
        $("#download").hide();
        $("#addDirectiveType").val(1);
        $("#userAgentName").val("All");
        $("#directoryOrFilePath").val("/");
        $("#specialUserAgentName").val("");

        disableButtons();

        var $this = $('#importRobots');
        $this.button('loading');
        $("#toolResultWrapper").hide();
        var ateDisplay = $('#contactForm').css('display');
        if (ateDisplay === 'block') {
            $("#contactForm").toggle("right");
        }
        if (requestType === 'import') {
            $.ajax({
                type: "POST",
                url: "/robots-txt-generator-tool.html?import=import",
                processData: true,
                data: {'finalURL': finalURL},
                dataType: "json",
                success: function (data) {
                    $this.button('reset');
                    $("#robotsTxtUrl").addClass('gray');
                    $("#edit-textbox").show();
                    constructDirectoryList(data, robotsArray,false);
                }, error: function (jqxhr, textStatus, error) {
                    if (textStatus.indexOf('error') !== -1) {
                        toolLimitExceededForAjax();
                    }
                },
                complete: function (xhr, textstatus) {
                    // xhr.responseText contains the response from the server
                    var allheaders = xhr.getAllResponseHeaders();
                    if (allheaders.indexOf("limitexceed") > 0) {
                        location.reload();
                    }
                    enableButtons();
                    $("#getResultSpin").hide();
                }
            });
        } else if (requestType === 'showExistedResult') {
            $.ajax({
                type: "POST",
                url: "/robots-txt-generator-tool.html?showExistedResult=showExistedResult",
                processData: true,
                data: {},
                dataType: "json",
                success: function (data) {
                    $this.button('reset');
                    $("#robotsTxtUrl").addClass('gray');
                    $("#edit-textbox").show();
                    constructDirectoryList(data, robotsArray,true);
                    enableButtons();
                }, error: function (jqxhr, textStatus, error) {
                    if (textStatus.indexOf('error') !== -1) {
                        toolLimitExceededForAjax();
                    }
                },
                complete: function (xhr, textstatus) {
                    // xhr.responseText contains the response from the server
                    var allheaders = xhr.getAllResponseHeaders();
                    if (allheaders.indexOf("limitexceed") > 0) {
                        location.reload();
                    }
                    enableButtons();
                    $("#getResultSpin").hide();
                }
            });
        }
    } else {
        $("#robotsTxtUrl").removeClass('gray');
        $("#edit-textbox").hide();
    }
}
/*Constructing the directory list from response data*/
function constructDirectoryList(responseData, robotsArray, backToSuccess) {
//    alert("responseData:"+responseData.length+", robotsArray:"+robotsArray);
    var directoryList = '';
    if (responseData[3] !== null && responseData[3] !== "") {
        $("#toolQueryMessage").empty();
        $("#toolQueryMessage").show();
        /*If Url does not contaning Robots.txt then issue related to Help in Creating Robots.txt is existed.
         Calling getToolRelatedIssues function.*/
        $("#toolQueryMessage").html(responseData[3]);
        $("#robotsTxtUrl").removeClass('gray');
        $("#edit-textbox").hide();
        setTimeout(getAskExpertPopup(true), 3000);
        resetInputFields();
    }
    if (responseData[1] !== null) {
        if (responseData[1].length >= 1) {
            resetInputFields();
        }
        if (responseData[0] !== null) {
            $("#sitemapUrl").val(responseData[0]);
        }
        if (responseData[2] !== null) {
            $("#robotTxtContent").val(responseData[2]);
        }
        if (responseData[4] !== null && responseData[4] !== "") {
            $('#robotsTxtUrl').val(responseData[4]);
        }else{
             $("#robotsTxtUrl").removeClass('gray');
              $("#edit-textbox").hide();
        }
        for (var i = 0; i < responseData[1].length; i++) {
            if (responseData[1][i] !== null && responseData[1][i] !== "") {
                var spclUASty = "";
                /*Parent Div*/
                directoryList += '<div id="emptyWrapper' + i + '" class="visible-xs visible-sm" style="height: 10px;clear: both"></div><div id="directory' + i + '" class="col-md-12 directiveWrapper">';
                directoryList += '<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">';
                /*Option to select action(Allow/Disallow) on robot*/
                directoryList += '<div class="col-md-2"><div class="input-group col-xs-12">';
                directoryList += '<select name="ruleType" class="form-control manaullyDirectiveField" id="directoryAction' + i + '" >';
                if (responseData[1][i].addDirectiveType === 1) {
                    directoryList += '<option value="1" selected="selected"> Allow</option><option value="2"> Disallow</option>';
                } else if (responseData[1][i].addDirectiveType === 2) {
                    directoryList += '<option value="1"> Allow</option><option value="2"  selected="selected"> Disallow</option>';
                }
                directoryList += '</select></div></div>';
                /*Crearting empty space*/
                directoryList += '<div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>';
                /*Selection of robots*/
                directoryList += '<div class="col-md-3" ><div class="input-group col-xs-12">';
                directoryList += '<select class="form-control manaullyDirectiveField" id="directoryUserAgent' + i + '" name="userAgentName" onchange="displaySpecifyUserAgent(' + i + ')">';
                for (var x = 0; x < robotsArray.length; x++) {
                    if (robotsArray[x] !== null && robotsArray[x] !== "") {
                        if ($.trim(responseData[1][i].userAgentName.toLowerCase()) === $.trim(robotsArray[x].toLowerCase())) {
                            directoryList += '<option value=' + robotsArray[x] + ' selected="selected">' + robotsArray[x] + '</option>';
                        } else {
                            directoryList += '<option value=' + robotsArray[x] + '>' + robotsArray[x] + '</option>';
                        }
                    }
                }
                directoryList += '</select></div></div>';
                /*Crearting empty space*/
                directoryList += '<div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>';

                if (responseData[1][i].userAgentName === "Specify") {
                    spclUASty = "display:block";
                } else {
                    spclUASty = "display:none";
                }
                /*Input field to specify robot name*/
                directoryList += '<div class="col-md-3" id="directorySpecifyWrapper' + i + '"  style="' + spclUASty + ';">';
                directoryList += '<div class="input-group col-xs-12"><input type="text" placeholder="Specify UserAgent" name="selectoptmain" value="' + responseData[1][i].specialUserAgentName + '" class="form-control manaullyDirectiveField" id="directorySpecifyAgent' + i + '" /></div><div class="visible-xs visible-sm" style="height: 10px;clear: both"></div></div>';

                /*Input field to specify robot name*/
                directoryList += '<div class="col-md-4"><div class="input-group col-xs-12">';
                directoryList += '<input type="text" name="selectoptdisplay" value="' + responseData[1][i].directoryOrFilePath + '" class="form-control manaullyDirectiveField" id="directoryTextPath' + i + '"/>';
                directoryList += '</div></div>';
                directoryList += '</div>';/*End of col-md-11 div*/
                directoryList += '<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1" style="padding:0;">';
                directoryList += '<button class="btn removeWrapper" type="button" onClick="removeDirectory(\'directory' + i + '\')"><i class="fa fa-times fa-2 deleteDirectory" aria-hidden="true" ></i></button>';
                directoryList += '</div>';
                directoryList += '</div>';
            }
        }
        $("#importDirectivesWrapper").html("" + directoryList);
        if(backToSuccess && responseData[1].length >= 1){
            setTimeout($("#getResult").click(), 1000);
        }
    }
}

function constructDirectory(divId) {
    var directoryHTML = '';
    directoryHTML = '<div id="emptyWrapper' + divId + '" class="visible-xs visible-sm" style="height: 10px;clear: both"></div><div id="directory' + divId + '" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 directiveWrapper">';
    directoryHTML += '<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">';
    directoryHTML += '<div class="col-md-2">';
    directoryHTML += '<div class="input-group col-xs-12"><select name="ruleType" class="form-control manaullyDirectiveField" id="directoryAction' + divId + '" ><option value="1">Allow</option><option value="2">Disallow</option></select></div>';
    directoryHTML += '</div>';
    directoryHTML += '<div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>';
    directoryHTML += '<div class="col-md-3">';
    directoryHTML += '<div class="input-group col-xs-12"><select name="robot" class="form-control manaullyDirectiveField" id="directoryUserAgent' + divId + '" onchange="displaySpecifyUserAgent(' + divId + ')">' +
            '<option value="All">All</option><option value="NinjaBot">NinjaBot</option><option value="Googlebot">Googlebot</option><option value="Googlebot-Mobile">Googlebot-Mobile</option><option value="Googlebot-Image">Googlebot-Image</option>' +
            '<option value="Mediapartners-Google">Mediapartners-Google</option><option value="Adsbot-Google">Adsbot-Google</option><option value="Bingbot">Bingbot</option><option value="Slurp">Slurp</option><option value="msnbot">msnbot</option><option value="msnbot-media">msnbot-media</option><option value="Teoma">Teoma</option>' +
            '<option value="twiceler">twiceler</option><option value="Gigabot">Gigabot</option><option value="Scrubby">Scrubby</option><option value="Robozilla">Robozilla</option><option value="ia_archiver">ia_archiver</option>' +
            '<option value="baiduspider">baiduspider</option><option value="naverbot">naverbot</option><option value="yeti">yeti</option><option value="yahoo-mmcrawler">yahoo-mmcrawler</option><option value="psbot">psbot</option>' +
            '<option value="asterias">asterias</option><option value="yahoo-blogs">yahoo-blogs</option><option value="Yandex">Yandex</option><option value="Specify">Specify</option></select></div>';
    directoryHTML += '</div>';
    directoryHTML += '<div class="visible-xs visible-sm" style="height: 10px;clear: both"></div>';
    directoryHTML += '<div class="col-md-3" id="directorySpecifyWrapper' + divId + '"  style="display:none;">';
    directoryHTML += '<div class="input-group col-xs-12"><input type="text" name="selectoptmain" placeholder="Specify UserAgent" class="form-control manaullyDirectiveField" id="directorySpecifyAgent' + divId + '" /></div><div class="visible-xs visible-sm" style="height: 10px;clear: both"></div></div>';
    directoryHTML += '<div class="col-md-4"><div class="input-group col-xs-12">';
    directoryHTML += '<input type="text" name="selectoptdisplay" class="form-control manaullyDirectiveField" id="directoryTextPath' + divId + '"/>';
    directoryHTML += '</div></div>';
    directoryHTML += '</div>';/*End of col-md-11 div*/
    directoryHTML += '<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1" style="padding:0;">';
//    directoryHTML += '<div class="input-group-btn"><button class="btn" type="button" style="background:none;" onClick="removeDirectory(\'directory' + divId + '\')"><i class="fa fa-times fa-2 deleteDirectory" style="color:red;" aria-hidden="true" ></i></button></div></div>';
    directoryHTML += '<button class="btn removeWrapper" type="button" onClick="removeDirectory(\'directory' + divId + '\')"><i class="fa fa-times fa-2 deleteDirectory" aria-hidden="true" ></i></button>';
    directoryHTML += '</div>';
    directoryHTML += '</div>';
    return directoryHTML;
}

/*Display the input field to speicfy user angent*/
function displaySpecifyUserAgent(divId) {
    /*Manually Generated directory*/
    if ($.trim($("#directoryUserAgent" + divId).val()) === "Specify") {
        $("#directorySpecifyWrapper" + divId).css('display', "block");
        $("#directorySpecifyWrapper" + divId).show();
    } else {
        $("#directorySpecifyWrapper" + divId).hide();
        $("#directorySpecifyAgent" + divId).val("");
        $("#directorySpecifyWrapper" + divId).css('display', "none");
    }
}
function removeDirectory(id) {
    $("#" + id).remove();
    var cloneId = parseInt(id.replace("directory", ""));
    $("#emptyWrapper" + cloneId + "").remove();
}

function enableButtons() {
    $('#getResult').attr('disabled', false);
    $('#clearDirectory').attr('disabled', false);
    $('#domain').attr('disabled', false);
    $('#download').attr('disabled', false);
    $('#importRobots').attr('disabled', false);
}

function disableButtons() {
    $('#importRobots').attr('disabled', true);
    $('#clearDirectory').attr('disabled', true);
    $('#domain').attr('disabled', true);
    $('#download').attr('disabled', true);
    $('#getResult').attr('disabled', true);
}


function resetInputFields() {
    $('#importDirectivesWrapper,#manuallyDirectivesWrapper, #messageWrapper ,#directiveError, #userAgentError ,#invalidSiteMap').empty();
    $("#directiveError, #userAgentError").hide();
    $("#addDirectiveType").val(1);
    $("#userAgentName").val("All");
    $("#directoryOrFilePath").val("/");
    $("#specialAgentWrapper").hide();
    $("#specialUserAgentName").val("");
    $("#sitemapUrl").val("");
     $("#toolResultWrapper").hide();
    $("#robotTxtContent").val("");
    
}