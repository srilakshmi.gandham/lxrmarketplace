/*
 * queryRequired = errorMessage
 * status = processQuery
 * analyzedSearchQuery = analyzedQueryWebPageURL
 * noData = isDataPulled
 * 
 * 
 * 
 */
var angularJS = angular.module('lxrmarketplacePSI', []);
angularJS.filter('capitalize', function () {
    return function (input) {
        return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input;
    }
});
angularJS.filter('nospace', function () {
    return function (value) {
//        return (!value) ? '' : value.replace(/ /g, '');
        return (!value) ? '' : value.trim();
    };
});
angularJS.filter('millSecondsToSecond', function () {
    return function (miliSeconds) {
//        console.log("miliSeconds:: " + miliSeconds);
        var oneMili = 1000.0;
        var seconds = 0;
        if (miliSeconds > 0) {
            seconds = ((miliSeconds / oneMili));
//            console.log("mili->bfre roundup:: " + seconds);
            seconds = parseFloat(Math.round(seconds * 100) / 100).toFixed(1);
        } else {
            seconds = miliSeconds;
        }
//        console.log("seconds:: " + seconds);
        return seconds;
    }
});
angularJS.controller('PageSpeedInsightsController', ['$scope', '$http', function ($scope, $http, $window) {
        var self = this;

        /*Google PSI metrics list prepared from API response for search Query*/
        self.deskTopPageSpeedInsight = null;
        /*Desktop: Audit rule list*/
        self.deskTopOpportunitiesList = [];
        self.deskTopDiagnosticsList = [];
        self.deskTopPassedAuditsList = [];

        self.mobilePageSpeedInsight = null;
        /*Mobile: Audit rule list*/
        self.mobileOpportunitiesList = [];
        self.mobileDiagnosticsList = [];
        self.mobilePassedAuditsList = [];

        self.pageSpeedInsightsResultList = [];
        self.isDataPulled = false;
        self.labDataRulesIds = ["first-contentful-paint", "speed-index", "interactive", "first-meaningful-paint", "first-cpu-idle", "max-potential-fid"];
        /*Constant values:*/
        var graphMSg = "of loads for this page have a";
        $scope.downLoadDisabled = false;
        $scope.sendEmail = false;
        self.errorMessage = null;

        /*Used to construct in tables*/

        var childRowTextLeft = "childRowTextLeft";
        var childRowTextRight = "childRowTextRight";
        var childRowTextCenter = "childRowTextCenter";
        var childRowImgClass = "tool-info";
        var thumbNailImgWidth = '50px';
        var thumbNailImgHeight = '50px';
        var noDataString = '<td class="lxrm-bold ' + childRowTextCenter + '">-</td>';
        var childRowTextLeft = "childRowTextLeft";
        var childRowTextRight = "childRowTextRight";
        var lxrmWordBreak = "lxrmWordBreak";


        /*Validating input fields.*/
        $scope.fetchPageSpeedInsights = function () {
            resultLoginValue = true;
            if (toolLimitExceeded()) {
                var processQuery = true;
                var query = $scope.queryWebPageURL;
                console.log("User Query: " + query);
                if (query === undefined || query === null || query === "") {
                    $('#editSearch').hide();
                    $("#queryWebPageURL").removeClass('gray');
                    $("#searchQueryError").empty();
                    $("#searchQueryError").text("Please enter a web page URL.");
                    processQuery = false;
                } else {
//                    self.errorMessage = null;
                    $("#searchQueryError").empty();
                }
                console.log("processQuery: " + processQuery);
                if (processQuery) {
                    var $this = $("#analyzePageSpeedInsights");
                    $this.button('loading');
                    atePopupStatus = true;
                    submitRequestToController(true);
                }
            }
        };
        /*When navigated to tool page from ATE confirmation page or Signup/Login page refreshes*/
        $scope.processPSIBackToSuccessPage = function () {
            setTimeout(function () {
                if (isBackToSuccess && sessionStorage.getItem("analyzedQueryWebPageURL") !== null) {
                    $scope.queryWebPageURL = sessionStorage.getItem("analyzedQueryWebPageURL");
                    atePopupStatus = true;
                    var $this = $("#analyzePageSpeedInsights");
                    $this.button('loading');
                    if (isLimitExceedLogin) {
                        submitRequestToController(true);
                    } else {
                        submitRequestToController(false);
                    }
                }
            }, 100);
        };

        $scope.setToolAdvisiorURL = function (website) {
            $scope.queryWebPageURL = website;
            var tempQueyURL = $scope.queryWebPageURL.trim();
            if (tempQueyURL !== null || tempQueyURL !== "") {
                console.log("In setToolAdvisiorURL the QueyURL:: " + tempQueyURL);
                angular.element('#analyzePageSpeedInsights').triggerHandler('click');
            }
        };

        /*Submiting user query to controller.*/
        function submitRequestToController(isNewRequest) {
            console.log("submitRequestToController");
            var $this = $("#analyzePageSpeedInsights");
            var formData = {"queryWebPageURL": $scope.queryWebPageURL.trim(), "newAnalysis": isNewRequest};
            sessionStorage.setItem("analyzedQueryWebPageURL", $scope.queryWebPageURL.trim());
            /*Saving response result list*/
            var config = {
                headers: {'Content-Type': 'application/json'}
            }
            resetObjects();
            disableAll();
            $scope.downLoadDisabled = true;
            $scope.sendEmail = true;
            $("#queryWebPageURL").addClass('gray');
            $http.post('page-speed-insights-tool.html?request=analyze', formData, config)
                    .then(function (response) {
                        //                        console.log("response: " + response.data);
                        if (response.headers('limitexceed') > 0) {
                            self.isDataPulled = false;
                            resetObjects();
                            toolLimitExceededForAjax();
                        } else if (response.data.length >= 1) {
                            $("#psiResultWrapper").show();
//                            console.log("response[0]:: " + response.data[0]);
                            if (response.data[0] !== null && response.data[0]) {
                                self.isDataPulled = response.data[0];
                                if (response.data[2] !== null) {
                                    self.mobilePageSpeedInsight = response.data[2];
//                                    console.log("Mobile score:" + self.mobilePageSpeedInsight.performanceScore);
                                }
                                if (response.data[3] !== null) {
                                    self.deskTopPageSpeedInsight = response.data[3];
                                }
                                if (response.data[4] !== null) {
                                    self.pageSpeedInsightsResultList = response.data[4];
                                }
                                setDeviceAuditRulesList();
                                console.log("atePopupStatus::" + atePopupStatus);
                                if (atePopupStatus) {
                                    atePopupStatus = false;
                                    if ((self.mobilePageSpeedInsight.statusCode === 200 || self.deskTopPageSpeedInsight.statusCode === 200) && self.isDataPulled) {
                                        getAskExpertPopup(true);
                                    }
                                }
                                $("#psiResultWrapper").css('display', 'block');
                                $("#psiResultWrapper").show();
                            } else {
                                self.isDataPulled = response.data[0];
                                if (response.data[1] !== null) {
                                    $("#searchQueryError").empty();
                                    $("#searchQueryError").text(response.data[1]);
                                }
                            }
                        } else {
                            self.isDataPulled = response.data[0];
                            if (response.data[1] !== null) {
                                $("#searchQueryError").empty();
                                $("#searchQueryError").text(response.data[1]);
                            }
                        }
                    }).catch(function (data) {
                console.log("Error in getting tool analysis result: " + data);
                self.isDataPulled = false;
                resetObjects();
                toolLimitExceededForAjax();
                atePopupStatus = false;
            }).finally(function () {
                $("#editSearch").show();
                enableAll();
                $this.button('reset');
            });
        }



        /*Functions related to deices data*/
        /*Functions related to score*/
        $scope.generateScoreGraphForDevices = function (deviceWrapperId) {
            var deviceScore;
            if (deviceWrapperId === "mobile") {
                deviceScore = self.mobilePageSpeedInsight.performanceScore;
            } else if (deviceWrapperId === "deskTop") {
                deviceScore = self.deskTopPageSpeedInsight.performanceScore;
            }
            $("#" + deviceWrapperId + "DeviceScore").data('value', deviceScore);
            $("#" + deviceWrapperId + "DeviceScore").attr("data-value", deviceScore);
            $("#" + deviceWrapperId + "DeviceScore").each(function () {
                var value = $(this).attr('data-value');
                var left = $(this).find('.progress-left .progress-bar');
                var right = $(this).find('.progress-right .progress-bar');
                if (value > 0) {
                    if (value <= 50) {
                        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)');
                    } else {
                        right.css('transform', 'rotate(180deg)')
                        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)');
                    }
                }
                $("#" + deviceWrapperId + "ProgressValue").text(deviceScore);
                if (deviceScore <= 49) {
                    $("#" + deviceWrapperId + "ProgressColorLeft").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorLeft").css('border-color', '#F14C43');
                    $("#" + deviceWrapperId + "ProgressColorRight").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorRight").css('border-color', '#F14C43');
                } else if (deviceScore >= 50 && deviceScore <= 89) {
                    $("#" + deviceWrapperId + "ProgressColorLeft").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorLeft").css('border-color', '#F6A402');
                    $("#" + deviceWrapperId + "ProgressColorRight").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorRight").css('border-color', '#F6A402');
                } else {
//                    (deviceScore >= 90) 
                    $("#" + deviceWrapperId + "ProgressColorLeft").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorLeft").css('border-color', '#55CF6A');
                    $("#" + deviceWrapperId + "ProgressColorRight").removeClass('border-primary');
                    $("#" + deviceWrapperId + "ProgressColorRight").css('border-color', '#55CF6A');
                }
                ;
            });
        };
        function percentageToDegrees(percentage) {
            return percentage / 100 * 360;
        }

        /*FCP && FID score*/
        $scope.constructFCPGraph = function (elementId, elementData) {
            console.log("************ FCP Graph elementId: " + elementId + " **** fast:: " + elementData['fast'] + ", average:: " + elementData['average'] + ", slow:: " + elementData['slow']);
            var minBarWidth = 10;
            var slowBarWidth = elementData['slow'];
            var averageBarWidth = elementData['average'];
            var fastBarWidth = elementData['fast'];
            var balanceToBeAdded;
            if (slowBarWidth < 10) {
//                console.log("slow");
                balanceToBeAdded = minBarWidth - slowBarWidth;
                slowBarWidth = slowBarWidth + balanceToBeAdded;
                if (fastBarWidth > averageBarWidth && fastBarWidth > minBarWidth) {
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
//                    console.log("For slow   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from fastBarWidth:: " + fastBarWidth);
                } else if (averageBarWidth > fastBarWidth && averageBarWidth > minBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("For slow   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from averageBarWidth:: " + averageBarWidth);
                }
            }
            if (averageBarWidth < 10) {
//                console.log("average");
                balanceToBeAdded = minBarWidth - averageBarWidth;
                averageBarWidth = averageBarWidth + balanceToBeAdded;
                if (fastBarWidth > slowBarWidth && fastBarWidth > minBarWidth) {
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
//                    console.log("For average   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from fastBarWidth:: " + fastBarWidth);
                } else if (slowBarWidth > fastBarWidth && slowBarWidth > minBarWidth) {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("For average   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from slowBarWidth:: " + slowBarWidth);
                }
            }
            if (fastBarWidth < 10) {
//                console.log("fast");
                balanceToBeAdded = minBarWidth - fastBarWidth;
                fastBarWidth = fastBarWidth + balanceToBeAdded;
                if (averageBarWidth > slowBarWidth && averageBarWidth > minBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("For fast   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from averageBarWidth:: " + fastBarWidth);
                } else if (slowBarWidth > averageBarWidth && slowBarWidth > minBarWidth) {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("For fast   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from slowBarWidth:: " + slowBarWidth);
                }
            }
            /*To handle cases like sum of all categorie is more than 100%*/
            if ((fastBarWidth + averageBarWidth + slowBarWidth) > 100) {
//                console.log("sum of total more than 100:: and sum is " + (fastBarWidth + averageBarWidth + slowBarWidth));
                balanceToBeAdded = (fastBarWidth + averageBarWidth + slowBarWidth) - 100;
                if (fastBarWidth > averageBarWidth && fastBarWidth > slowBarWidth) {
//                    console.log("More fastvbarwidth::" + fastBarWidth);
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
                } else if (averageBarWidth > slowBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("More averageBarWidth::" + averageBarWidth);
                } else {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("More slowBarWidth::" + slowBarWidth);
                }
            } else if ((fastBarWidth + averageBarWidth + slowBarWidth) < 100) {
//                console.log("sum of total less than 100:: and sum is " + (fastBarWidth + averageBarWidth + slowBarWidth));
                balanceToBeAdded = 100 - (fastBarWidth + averageBarWidth + slowBarWidth);
                if (fastBarWidth <= averageBarWidth && fastBarWidth <= slowBarWidth) {
                    fastBarWidth = fastBarWidth + balanceToBeAdded;
//                    console.log("less fastBarWidth::" + fastBarWidth);
                } else if (averageBarWidth <= fastBarWidth && averageBarWidth <= slowBarWidth) {
                    averageBarWidth = averageBarWidth + balanceToBeAdded;
//                    console.log("less averageBarWidth::" + averageBarWidth);
                } else {
                    slowBarWidth = slowBarWidth + balanceToBeAdded;
//                    console.log("less slowBarWidth::" + slowBarWidth);
                }
            }
//            console.log("final widths in fcp:: fastBarWidth: " + fastBarWidth + ", averageBarWidth: " + averageBarWidth + ", slowBarWidth: " + slowBarWidth);
            var graphString = "";
            graphString += '<div class="progress">';
            graphString += '<div class="progress-bar progress-bar-success lxrm-bold tool-info" role="progressbar" style="width:' + fastBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['fast'] + '% ' + graphMSg + ' fast (< 1 s) First Contentful Paint(FCP)">' + elementData['fast'] + '%</div>';
            graphString += '<div class="progress-bar progress-bar-warning lxrm-bold tool-info" role="progressbar" style="width:' + averageBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['average'] + '% ' + graphMSg + ' average (1 s ~ 2.5 s) First Contentful Paint(FCP)">' + elementData['average'] + '%</div>';
            graphString += '<div class="progress-bar progress-bar-danger lxrm-bold tool-info" role="progressbar"  style="width:' + slowBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['slow'] + '% ' + graphMSg + ' slow (>2.5 s) First Contentful Paint(FCP)">' + elementData['slow'] + '%</div>';
            graphString += '</div>';
            $("#" + elementId).html(graphString);
        };

        $scope.constructFIDGraph = function (elementId, elementData) {
//            console.log("*********** FID Graph elementId: " + elementId + " *** fast:: " + elementData['fast'] + ", average:: " + elementData['average'] + ", slow:: " + elementData['slow']);
            var minBarWidth = 10;
            var slowBarWidth = elementData['slow'];
            var averageBarWidth = elementData['average'];
            var fastBarWidth = elementData['fast'];
            var balanceToBeAdded;
            if (slowBarWidth < 10) {
//                console.log("FID slow:: "+slowBarWidth);
                balanceToBeAdded = minBarWidth - slowBarWidth;
                slowBarWidth = slowBarWidth + balanceToBeAdded;
                if (fastBarWidth > averageBarWidth && fastBarWidth > minBarWidth) {
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
//                    console.log("For slow   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from fastBarWidth the current slow:: " + fastBarWidth);
                } else if (averageBarWidth > fastBarWidth && averageBarWidth > minBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("For slow   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from averageBarWidth the current slow:::: " + averageBarWidth);
                }
            }
            if (averageBarWidth < 10) {
//                console.log("FID average:: "+averageBarWidth);
                balanceToBeAdded = minBarWidth - averageBarWidth;
                averageBarWidth = averageBarWidth + balanceToBeAdded;
                if (fastBarWidth > slowBarWidth && fastBarWidth > minBarWidth) {
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
//                    console.log("For average   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from fastBarWidth the current averageBarWidth:: " + fastBarWidth);
                } else if (slowBarWidth > fastBarWidth && slowBarWidth > minBarWidth) {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("For average   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from slowBarWidth the current averageBarWidth :::: " + slowBarWidth);
                }
            }
            if (fastBarWidth < 10) {
//                console.log("FID fast:: "+fastBarWidth);
                balanceToBeAdded = minBarWidth - fastBarWidth;
                fastBarWidth = fastBarWidth + balanceToBeAdded;
                if (averageBarWidth > slowBarWidth && averageBarWidth > minBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("For fast   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from averageBarWidth the current fast :: " + fastBarWidth);
                } else if (slowBarWidth > averageBarWidth && slowBarWidth > minBarWidth) {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("For fast   balanceToBeAdded::" + balanceToBeAdded + ", and it is adjusted from slowBarWidth the current fast :: " + slowBarWidth);
                }
            }
            /*To handle cases like sum of all categorie is more than 100%*/
            if ((fastBarWidth + averageBarWidth + slowBarWidth) > 100) {
//                console.log("sum of total more than 100 in fid::" + (fastBarWidth + averageBarWidth + slowBarWidth));
                balanceToBeAdded = (fastBarWidth + averageBarWidth + slowBarWidth) - 100;
//                console.log("sum of total more than 100 in fid and balanceToBeAdded::" +balanceToBeAdded);
//                console.log("Current fastBarWidth::" +fastBarWidth+", averageBarWidth:: "+averageBarWidth+", slowBarWidth:: "+slowBarWidth);
                if (fastBarWidth > averageBarWidth && fastBarWidth > slowBarWidth) {
//                    console.log("More fastvbarwidth::" + fastBarWidth);
                    fastBarWidth = fastBarWidth - balanceToBeAdded;
                } else if (averageBarWidth > slowBarWidth) {
                    averageBarWidth = averageBarWidth - balanceToBeAdded;
//                    console.log("More averageBarWidth::" + averageBarWidth);
                } else {
                    slowBarWidth = slowBarWidth - balanceToBeAdded;
//                    console.log("More slowBarWidth::" + slowBarWidth);
                }
            } else if ((fastBarWidth + averageBarWidth + slowBarWidth) < 100) {
//                console.log("sum of total less than 100 in fid::" + (fastBarWidth + averageBarWidth + slowBarWidth));
                balanceToBeAdded = 100 - (fastBarWidth + averageBarWidth + slowBarWidth);
//                console.log("sum of total less than 100 and balanceToBeAdded::" +balanceToBeAdded);
//                console.log("Current fastBarWidth::" +fastBarWidth+", averageBarWidth:: "+averageBarWidth+", slowBarWidth:: "+slowBarWidth);
                if (fastBarWidth <= averageBarWidth && fastBarWidth <= slowBarWidth) {
                    fastBarWidth = fastBarWidth + balanceToBeAdded;
//                    console.log("less fastBarWidth::" + fastBarWidth);
                } else if (averageBarWidth <= fastBarWidth && averageBarWidth <= slowBarWidth) {
                    averageBarWidth = averageBarWidth + balanceToBeAdded;
//                    console.log("less averageBarWidth::" + averageBarWidth);
                } else {
                    slowBarWidth = slowBarWidth + balanceToBeAdded;
//                    console.log("less slowBarWidth::" + slowBarWidth);
                }
            }
//            console.log("final widths in fid:: fastBarWidth: " + fastBarWidth + ", averageBarWidth:  " + averageBarWidth + ", slowBarWidth: " + slowBarWidth);
            var graphString = "";
            graphString += '<div class="progress">';
            graphString += '<div class="progress-bar progress-bar-success lxrm-bold tool-info" role="progressbar" style="width:' + fastBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['fast'] + '% ' + graphMSg + ' fast (< 50 ms) First Input Delay (FID)">' + elementData['fast'] + '%</div>';
            graphString += '<div class="progress-bar progress-bar-warning lxrm-bold tool-info" role="progressbar" style="width:' + averageBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['average'] + '% ' + graphMSg + ' average (50 ms ~ 250 ms) First Input Delay (FID)">' + elementData['average'] + '%</div>';
            graphString += '<div class="progress-bar progress-bar-danger lxrm-bold tool-info" role="progressbar"  style="width:' + slowBarWidth + '%"';
            graphString += 'data-placement="bottom" data-toggle="tooltip" title="' + elementData['slow'] + '% ' + graphMSg + ' slow (>250 ms) First Input Delay (FID)">' + elementData['slow'] + '%</div>';
            graphString += '</div>';
//            console.log("FId Graph string:: " + graphString);
            $("#" + elementId).html(graphString);
        };

        $scope.constructLabDataMetric = function (divId, borderType, device) {
            var labDataAuditRule;
            if (device === "mobile") {
                for (var i = 0; i <= self.mobilePageSpeedInsight.auditRules.length; i++) {
                    if (self.mobilePageSpeedInsight.auditRules[i].id === divId
                            && self.mobilePageSpeedInsight.auditRules[i].labDataFieldRule) {
                        labDataAuditRule = self.mobilePageSpeedInsight.auditRules[i];
                        break;
                    }
                }
                labDataAuditRule = self.mobilePageSpeedInsight.auditRules[i];
            } else if (device === "deskTop") {
//                console.log("leng of audit rules fro desktop:" + self.deskTopPageSpeedInsight.auditRules.length)
                for (var i = 0; i <= self.deskTopPageSpeedInsight.auditRules.length; i++) {
                    if (self.deskTopPageSpeedInsight.auditRules[i].id === divId
                            && self.deskTopPageSpeedInsight.auditRules[i].labDataFieldRule) {
                        labDataAuditRule = self.deskTopPageSpeedInsight.auditRules[i];
                        break;
                    }
                }
                labDataAuditRule = self.deskTopPageSpeedInsight.auditRules[i];
            }

            if (labDataAuditRule !== null) {
                var labDataMetricString = "";
                labDataMetricString += '<div class="col-xs-12 lxrm-ptb-Half">';
                labDataMetricString += '<div class="col-xs-12 lxrm-padding-none">';
                labDataMetricString += '<span class="col-xs-1 text-center ' + labDataAuditRule.scoreColor + '">';
                if (labDataAuditRule.scoreColor === "fast") {
                    labDataMetricString += '<i class="fa fa-circle" aria-hidden="true"></i></span>';
                } else if (labDataAuditRule.scoreColor === "average") {
                    labDataMetricString += '<i class="fa fa-stop" aria-hidden="true"></i></span>';
                } else if (labDataAuditRule.scoreColor === "slow") {
                    labDataMetricString += '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i></span>';
                }
                labDataMetricString += '<span class="col-xs-9 lxrm-bold lxrm-sub-header-3 metric-title labData-title">' + labDataAuditRule.title + '</span>';
                labDataMetricString += '<span class="col-xs-2 lxrm-bold tool-info lxrm-padding-none text-right' + labDataAuditRule.scoreColor + '">' + labDataAuditRule.displayValue + '</span>';
                labDataMetricString += '</div>';
//                labDataMetricString += '<div class="col-xs-12 lxrm-padding-none"><div class="col-xs-1"></div>';
                labDataMetricString += '<div class="col-xs-12 lxrm-padding-none">';
                labDataMetricString += '<div class="col-xs-12 first">' + labDataAuditRule.descriptionHTML + '</div>';
                labDataMetricString += '</div></div>';
                if (device === "deskTop") {
                    $("#" + divId + "-deskTop").html(labDataMetricString);
                    $("#" + divId + "-deskTop").find('div .first').hide();
                } else {
                    $("#" + divId).html(labDataMetricString);
                    $("#" + divId).children().find('div.first').hide();
                }
            }
        };
        $scope.activeMobileLabDataOptions = function () {
            $("#mobileLabDataOptionsWrapper input[name='mobileLabData']").click(function () {
                if ($('input:radio[name=mobileLabData]:checked').val() === "mobileLabDataMore") {
                    $("#mobileLabDataWrapper").children().find('div.first').show();
                    $("#mobileLabDataMoreLabel").addClass('active');
                    $("#mobileLabDataLessLabel").removeClass('active');
                }
                if ($('input:radio[name=mobileLabData]:checked').val() === "mobileLabDataLess") {
                    $("#mobileLabDataWrapper").children().find('div.first').hide();
                    $("#mobileLabDataMoreLabel").removeClass('active');
                    $("#mobileLabDataLessLabel").addClass('active');
                }
            });
        }
        $scope.activeDeskTopLabDataOptions = function () {
            $("#deskTopLabDataOptionsWrapper input[name='deskTopLabData']").click(function () {
                if ($('input:radio[name=deskTopLabData]:checked').val() === "deskTopLabDataMore") {
                    $("#deskTopLabDataWrapper").children().find('div.first').show();
                    $("#deskTopLabDataMoreLabel").addClass('active');
                    $("#deskTopLabDataLessLabel").removeClass('active');
                }
                if ($('input:radio[name=deskTopLabData]:checked').val() === "deskTopLabDataLess") {
                    $("#deskTopLabDataWrapper").children().find('div.first').hide();
                    $("#deskTopLabDataLessLabel").addClass('active');
                    $("#deskTopLabDataMoreLabel").removeClass('active');
                }
            });
        }
        /*Screen shot Thumbnail*/
        $scope.constructScreenShots = function (device, imageSource, divId) {
            var screenShotList;
            if (device === "mobile") {
                screenShotList = self.mobilePageSpeedInsight.screenShots;
            } else if (device === "deskTop") {
                screenShotList = self.deskTopPageSpeedInsight.screenShots;
            }
            if (imageSource === 'screenshot-thumbnails' && screenShotList !== null && screenShotList.length !== 0) {
                var screenShotString = "";
                screenShotString += "<ul class='list-inline'>";
                for (var i = 0; i < 10; i++) {
                    if (screenShotList[i].imageType !== undefined && screenShotList[i].imageType === "screenshot-thumbnails" && screenShotList[i].imageType !== "none") {
//                        console.log("screenShotList::: " + screenShotList[i].imageType + ", i::" + i);
                        screenShotString += '<li>';
                        screenShotString += '<img src="' + screenShotList[i].imageURL + '">';
                        screenShotString += '</li>';
                    }
                }
                screenShotString += "</ul>";
                $("#" + divId).html(screenShotString);
            }

            if (imageSource === 'final-screenshot' && screenShotList !== null && screenShotList.length !== 0) {
//                console.log("final-screenshot:: " + imageSource);
                var screenShotString = "";
                if (screenShotList.length === 11) {
                    screenShotString += '<div class="col-xs-12">';
                    screenShotString += '<img src="' + screenShotList[10].imageURL + '">';
                    screenShotString += '</div>';
                }
                $("#" + divId).html(screenShotString);
            }
        };
        /*Intailizing the audit rule list for devices*/
        function setDeviceAuditRulesList() {
            /*Mobile*/
            if (self.mobilePageSpeedInsight !== null && self.mobilePageSpeedInsight.statusCode === 200) {
                if (self.mobilePageSpeedInsight.auditRules !== null && self.mobilePageSpeedInsight.auditRules.length > 0) {
                    for (var i = 0; i < self.mobilePageSpeedInsight.auditRules.length; i++) {
                        if (self.mobilePageSpeedInsight.auditRules[i] !== null && self.mobilePageSpeedInsight.auditRules[i].auditCategorie === 'Opportunities') {
//                            console.log("Rule Categorie:: " + self.mobilePageSpeedInsight.auditRules[i].auditCategorie);
                            self.mobileOpportunitiesList.push(self.mobilePageSpeedInsight.auditRules[i]);
                        } else if (self.mobilePageSpeedInsight.auditRules[i] !== null && self.mobilePageSpeedInsight.auditRules[i].auditCategorie === 'Diagnostics') {
//                            console.log("Rule Categorie:: " + self.mobilePageSpeedInsight.auditRules[i].auditCategorie);
                            self.mobileDiagnosticsList.push(self.mobilePageSpeedInsight.auditRules[i]);
                        } else if (self.mobilePageSpeedInsight.auditRules[i] !== null && self.mobilePageSpeedInsight.auditRules[i].auditCategorie === 'PassedAudits') {
//                            console.log("Rule Categorie:: " + self.mobilePageSpeedInsight.auditRules[i].auditCategorie+",  labDataFieldRule::"+self.mobilePageSpeedInsight.auditRules[i].labDataFieldRule);
                            if (!self.mobilePageSpeedInsight.auditRules[i].labDataFieldRule) {
                                self.mobilePassedAuditsList.push(self.mobilePageSpeedInsight.auditRules[i]);
                            }
                        }
                    }
                }
            }

            /*Desktop*/
            if (self.deskTopPageSpeedInsight !== null && self.deskTopPageSpeedInsight.statusCode === 200) {
                if (self.deskTopPageSpeedInsight.auditRules !== null && self.deskTopPageSpeedInsight.auditRules.length > 0) {
                    for (var i = 0; i < self.deskTopPageSpeedInsight.auditRules.length; i++) {
                        if (self.deskTopPageSpeedInsight.auditRules[i] !== null && self.deskTopPageSpeedInsight.auditRules[i].auditCategorie === 'Opportunities') {
//                            console.log("Desktop Rule Categorie:: " + self.deskTopPageSpeedInsight.auditRules[i].auditCategorie);
                            self.deskTopOpportunitiesList.push(self.deskTopPageSpeedInsight.auditRules[i]);
                        } else if (self.deskTopPageSpeedInsight.auditRules[i] !== null && self.deskTopPageSpeedInsight.auditRules[i].auditCategorie === 'Diagnostics') {
//                            console.log("Desktop Rule Categorie:: " + self.deskTopPageSpeedInsight.auditRules[i].auditCategorie);
                            self.deskTopDiagnosticsList.push(self.deskTopPageSpeedInsight.auditRules[i]);
                        } else if (self.deskTopPageSpeedInsight.auditRules[i] !== null && self.deskTopPageSpeedInsight.auditRules[i].auditCategorie === 'PassedAudits') {
//                            console.log("Desktop Rule Categorie:: " + self.deskTopPageSpeedInsight.auditRules[i].auditCategorie+",  labDataFieldRule::"+self.deskTopPageSpeedInsight.auditRules[i].labDataFieldRule);
                            if (!self.deskTopPageSpeedInsight.auditRules[i].labDataFieldRule) {
                                self.deskTopPassedAuditsList.push(self.deskTopPageSpeedInsight.auditRules[i]);
                            }
                        }
                    }
                }
            }
        }
        function resetObjects() {
            self.deskTopPageSpeedInsight = null;
            self.deskTopOpportunitiesList = [];
            self.deskTopDiagnosticsList = [];
            self.deskTopPassedAuditsList = [];
            self.mobilePageSpeedInsight = null;
            self.mobileOpportunitiesList = [];
            self.mobileDiagnosticsList = [];
            self.mobilePassedAuditsList = [];
            self.pageSpeedInsightsResultList = [];
            self.isDataPulled = false;
            self.isResponseError = false;
            self.errorMessage = null;
            $("#searchQueryError").empty();
        }
        function addUnitToValueV2(value, unitType) {
            var unit = "";
            if (unitType === "bytes") {
                unit = getKibiBytesValue(value) + " KB";
            } else if (unitType === "ms" || unitType === "timespanMs") {
                unit = Math.ceil((value)) + " ms";
            } else {
                unit = value;
            }
            return unit;
        }

        function getKibiBytesValue(kibiValue) {
            return Math.ceil((kibiValue / GoogleAPIKibiBytes));
        }
        $scope.constructAuditRuleTable = function (parentDivId, deviceAuditRule, ruleSNo) {
            var tableId = parentDivId + ruleSNo + 'Table';
            var divId = parentDivId + ruleSNo;
            var tableHeader = deviceAuditRule.tableHeadersRow;
            var tableBody = deviceAuditRule.tableChildRow;
//            console.log("divId -->" + divId + ", tableId -->" + tableId);
            var headerColumIndex = -1;
            var auditRuleTableString = '';
            auditRuleTableString += '<table id="' + tableId + '" class="table table-striped auditRuleTable">';
            auditRuleTableString += '<thead><tr>';
            for (var count = 0; count < tableHeader.length; count++) {
                if (tableHeader[count].itemType === "none" || tableHeader[count].itemType === "thumbnail") {
                    auditRuleTableString += '<td class="col-xs-1 lxrm-bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                } else if (tableHeader[count].itemType === "url" || tableHeader[count].itemType === "link" || tableHeader[count].itemType === "text" || tableHeader[count].itemType === "code") {
                    auditRuleTableString += '<td class="col-xs-5 lxrm-bold ' + childRowTextLeft + ' ' + lxrmWordBreak + ' ">' + tableHeader[count].text + '</td>';
                } else {
                    auditRuleTableString += '<td class="col-xs-2 lxrm-bold ' + childRowTextRight + ' ' + lxrmWordBreak + ' ">' + tableHeader[count].text + '</td>';
                }
            }

            auditRuleTableString += '</tr></thead>';
            auditRuleTableString += '<tbody>';
            for (var childCount = 0; childCount < tableBody.length; childCount++) {
                try {
                    auditRuleTableString += '<tr>';
                    for (var headCount = 0; headCount < tableHeader.length; headCount++) {
                        headerColumIndex = tableHeader[headCount].headerColumnIndex;
//                        console.log("headerColumIndex::-> " + headerColumIndex);
                        switch (headerColumIndex) {
                            case 0:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column0, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                            case 1:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column1, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                            case 2:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column2, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                            case 3:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column3, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                            case 4:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column4, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                            case 5:
                                auditRuleTableString += getBodyContentOfChildRow(tableBody[childCount].column5, tableHeader[headCount].itemType, tableHeader[headCount].key);
                                break;
                        }
                        headerColumIndex = -1;
                    }
                } catch (err) {
                    console.log("Exception in constructing child row coloum data")
                }
                auditRuleTableString += '</tr>';
            }/*EOF of tableBody*/
            auditRuleTableString += '</tbody>';
            auditRuleTableString += '</table>';
            setTimeout(function () {
                $('#' + divId).html(auditRuleTableString);
            }, 600);
        };


        function getBodyContentOfChildRow(coloumText, itemType, key) {
            var auditRuleTableString = '';
            if (coloumText !== null && coloumText !== "") {
                /*Checking the value for img type*/
                if (itemType !== null && itemType === "thumbnail") {
                    auditRuleTableString += '<td class="' + childRowImgClass + '"><img src="' + coloumText + '" height="' + thumbNailImgHeight + '" width="' + thumbNailImgWidth + '"></td>';
                } else {
                    /*Checking the value for type numeric or string to apply text align style*/
                    if ($.isNumeric(coloumText) || itemType === "numeric") {
                        /*Checking the value for unit ie. KB (OR) ms */
                        if (itemType !== null && itemType !== "thumbnail") {
                            auditRuleTableString += '<td class="' + childRowTextRight + '">' + addUnitToValueV2(coloumText, itemType) + '</td>';
                        } else {
                            auditRuleTableString += '<td class="' + childRowTextRight + '">' + coloumText + '</td>';
                        }
                    } else {
                        if (itemType !== null && itemType === "code") {
                            auditRuleTableString += '<td class="' + childRowTextLeft + '">' + getHTMLTagCodeInTextArea(coloumText) + '</td>';
                        } else {
                            if ((coloumText.indexOf("<\"") !== -1) && (coloumText.indexOf("\">") !== -1)) {
                                auditRuleTableString += '<td class="' + childRowTextLeft + '">' + getThirdPartyRuleRowText(coloumText) + '</td>';
                            } else if (validateURLText(coloumText)) {
                                auditRuleTableString += '<td class="' + childRowTextLeft + '"><a  class="psiHyerLink" target="_blank" href="' + coloumText + '">' + coloumText + '</a></td>';
                            } else if (key === "cacheLifetimeMs") {
                                auditRuleTableString += '<td class="' + childRowTextRight + '">' + coloumText + '</td>';
                            } else {
                                auditRuleTableString += '<td class="' + childRowTextLeft + '">' + coloumText + '</td>';
                            }
                        }
                    }
                }
            } else {
                auditRuleTableString += noDataString;
            }
            return auditRuleTableString;
        }

        function getThirdPartyRuleRowText(apiResponse) {
            var first = apiResponse.indexOf("<\"");
//            console.log("first:: " + first);
            var second = apiResponse.indexOf("\">");
//            console.log("second:: " + second);
            var text = apiResponse.substring(0, (first - 1));
//            console.log("text:: " + text);
            var link = apiResponse.substring((first + 2), second);
//            console.log("link:: " + link);
            var finalView = "";
            if (validateURLText(link)) {
                finalView = "<a class=\"psiHyperLink\" target=\"_blank\" href=\"" + link + "\">" + text + "</a>";
            } else {
                finalView = text;
            }
            return finalView;
        }

        $scope.constructAuditRuleCriticalChainList = function (parentDivId, deviceAuditRule, ruleSNo) {
//            console.log("constructAuditRuleCriticalChainList::  " + parentDivId);
            var divId = parentDivId + ruleSNo;
            var criticalReuestList = deviceAuditRule.criticalRequestChain;
            var auditRuleCriticalString = '';
            auditRuleCriticalString += '<div class="tree tool-info">';
            auditRuleCriticalString += '<ul class="initialNavigationUL">';
            /*Initial Navigation*/
            auditRuleCriticalString += '<li><span class="none lxrm-bold"> <em>Initial Navigation</em></span>';
            auditRuleCriticalString += '<ul>';
            /*Host URL(Query URL)*/
            auditRuleCriticalString += '<li>';
            for (var index = 0; index < criticalReuestList.length; index++) {
                if (criticalReuestList[index].urlType === "host") {
                    auditRuleCriticalString += '<span><a class="psiHyperLink" target="_blank" href="' + criticalReuestList[index].url + '">' + criticalReuestList[index].url + '</a></span>';
                    break;
                }
            }
            /*Start of child URLs construction main*/
            auditRuleCriticalString += '<ul>';
            for (var index = 0; index < criticalReuestList.length; index++) {
                if (criticalReuestList[index].urlType === "children") {
                    auditRuleCriticalString += '<li>' + getChildURLContentForCriticalChain(criticalReuestList[index]) + '</li>';
                }
            }
            /*EOf of child URLs construction main*/
            auditRuleCriticalString += '<ul>';
//            /*EOf of child URLs construction main*/
//            auditRuleCriticalString += '</ll></ul>';
            /*EOF of Host URL(Query URL)*/
            auditRuleCriticalString += '</ll></ul>';
            /*EOF of Initial Navigation*/
            auditRuleCriticalString += '</ll></ul>';
            auditRuleCriticalString += '</div>';
            setTimeout(function () {
                $('#' + divId).html(auditRuleCriticalString);
            }, 600);
        };

        function getChildURLContentForCriticalChain(criticalRequestChain) {
            var childURLString = '<span><a class="psiHyperLink" target="_blank" href="' + criticalRequestChain.url + '">' + criticalRequestChain.url + '</a></span>';
            childURLString += '<span class="lxrm-bold"> - ' + getCriticalChainResponseTime(criticalRequestChain.startTime, criticalRequestChain.endTime) + '';
            childURLString += '&nbsp;,&nbsp;' + getKibiBytesValue(criticalRequestChain.responseReceivedTime) + ' KB </span>';
            return childURLString;
        }

        function getHTMLTagCodeInTextArea(htmlCode) {
            var displayString = '<textarea rows="6" cols="35" disabled="true" class="psiTextArea">' + htmlCode + '</textarea>';
            return displayString;
        }

        function getCriticalChainResponseTime(startTime, endTime) {
            var responseTimeString = Math.ceil((endTime - startTime) * 1000).toFixed(0) + ' ms';
            return responseTimeString;
        }

        $scope.downloadPSIReport = function () {
            if (userLoggedIn) {
                if (self.pageSpeedInsightsResultList !== null && self.pageSpeedInsightsResultList.length !== 0) {
                    requestPSIReportToDownload(self.pageSpeedInsightsResultList);
                } else {
                    alert("Data not available to download.");
                }
            } else {
                reportDownload = 1;
                $("#loginPopupModal").modal('show');
                return false;
            }
        };

        $scope.sendReportToMail = function () {
            if (self.pageSpeedInsightsResultList !== null && self.pageSpeedInsightsResultList.length !== 0) {
                requestPSIReportToEmail(self.pageSpeedInsightsResultList);
            } else {
                alert("Data not available to send report to email.");
            }
        };

        $scope.activeAngleUpDown = function () {
            setTimeout(function () {
                $(".card-header").click(function () {
                    var cardi = $(this).find('i').last().attr('class');
//                    alert("cardi"+cardi);                    
                    if (cardi === 'fa fa-angle-up') {
                        $(this).find('i').last().removeClass(cardi);
                        $(this).find('i').last().addClass('fa fa-angle-down');
                    } else if (cardi === 'fa fa-angle-down') {
                        $(this).find('i').last().addClass('fa-angle-up');
                        $(this).find('i').last().removeClass('fa-angle-down');
                    }
                });
            }, 600);
        };

        $scope.getPreviewContent = function (responseContent, divId, index) {
//            console.log("getPreviewContent id::" + divId + ", responseContent::" + responseContent);
            setTimeout(function () {
                var viewContent = responseContent;
                viewContent = viewContent.trim();
//                viewContent = viewContent.replace(/[\s]/g, '');
                if (index !== -1) {
                    divId = divId + index;
                }
                if ($("#" + divId).children().length <= 0) {
                    $("#" + divId).html(viewContent);
                }
            }, 600);
        };


        function validateURLText(str) {
            regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
            if (regexp.test(str)) {
                return true;
            } else {
                return false;
            }
        }
        function disableAll() {
            $('#queryWebPageURL').attr('disabled', true);
            $('#userPSIEmail').attr('disabled', true);
            $('#queryWebPageURL').attr('readonly', true);
            $('#userPSIEmail').attr('readonly', true);
            $('#downloadPSIReportId').attr('disabled', true);
            $('#sendPSIReportToMail').attr('disabled', true);
            $('#analyzePageSpeedInsights').attr('disabled', true);
            $scope.downLoadDisabled = true;
            $scope.sendEmail = true;
        }
        function enableAll() {
//            console.log("In enableAll");
            $('#queryWebPageURL').attr('disabled', false);
            $('#userPSIEmail').attr('disabled', false);
            $('#queryWebPageURL').attr('readonly', false);
            $('#userPSIEmail').attr('readonly', false);
            $('#downloadPSIReportId').attr('disabled', false);
            $('#sendPSIReportToMail').attr('disabled', false);
            $('#analyzePageSpeedInsights').attr('disabled', false);
            $scope.downLoadDisabled = false;
            $scope.sendEmail = false;
        }
    }]);/*EOF of myapp*/