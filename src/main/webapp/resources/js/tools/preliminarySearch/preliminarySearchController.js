/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var preSalesAPP = angular.module('lxrmarketplacePreSales', []);
preSalesAPP.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
preSalesAPP.controller('PreliminarySearchController', ['$scope', '$http', function ($scope, $http) {
        /*Validating input fields.*/
        var self = this;
        self.isResponseSuccess = false;
        $scope.processPreliminarySearch = function () {
            if (isGmailLoginSuccess) {
                resetFields();
                if (validateInputs()) {
                    var userEmailTrackTool = "";
                    if (sessionStorage.getItem("userOAuthEmail") !== null) {
                        userEmailTrackTool = sessionStorage.getItem("userOAuthEmail");
                    }
                    console.log("file: " + $scope.deckLogo);
                    $("#getResult").button('loading');
                    $("#resetPSMA,#getResult").prop('disabled', true);
                    var requestURL = "/preliminary-search.html?request=analyze";
                    var config = {
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    var oMyForm = new FormData();
                    oMyForm.append("userDomain", $scope.userDomain);
                    oMyForm.append("competitorDomains", $scope.competitorDomains);
                    oMyForm.append("deckLogo", $scope.deckLogo);
                    /*This email to get tool analysis report*/
                    oMyForm.append("userOfflineEmail", $scope.userOfflineEmail);
                    /*This email is to track tool usage*/
                    oMyForm.append("toolUsageUserEmail", userEmailTrackTool);

                    $("#apiInfoMessage").hide();
                    $("#apiUpdatedMessage").hide();

//                    $("#messageToUser").html("Analyzing the website URL (S), these may take a few minutes");
                    $("#messageToUser").empty();
                    $("#messageToUser").html("Website performance analysis initiated. Usually it takes less than 2-4 minutes to complete the analysis. Please wait while we prepare the report.");
                    $("#messageToUser").css('color', '#333333');
                    /*Saving response result list*/
                    $http.post(requestURL, oMyForm, config)
                            .then(function (response) {
                                $("#resetPSMA,#getResult").prop('disabled', false);
                                $("#getResult").button('reset');
                                $("#apiInfoMessage").hide();
                                $("#apiUpdatedMessage").show();
                                self.isResponseSuccess = response.data[0];
                                self.updatedBalanceCount = response.data[2];
                                $("#messageToUser").empty();
                                if (self.isResponseSuccess) {
                                    $("#messageToUser").html("Report is ready to download!");
                                    $("#messageToUser").css('color', 'green');
                                    $("#downloadWrapper").show();
                                } else {
                                    $('#notWorkingUrl').empty();
                                    $('#notWorkingUrl').show();
                                    $('#notWorkingUrl').html(response.data[1]);
                                }
                            })
                            .catch(function () {
                                console.log("Exception in processPreliminarySearch");
                                $("#resetPSMA,#getResult").prop('disabled', false);
                            });
                }
            } else {
                gmailLoginSuccess();
                $("#googleLoginPopupModal").modal('show');
                var errorMessage = sessionStorage.getItem('PSMAError');
                if (errorMessage !== null) {
                    $("#loginErrorMessage").empty();
                    $("#errorBodyWrapper").show();
                    $("#loginErrorMessage").html(errorMessage);
                }
                $("#loginErrorMessage").empty();
                $("#errorBodyWrapper").hide();
            }
        };
        $scope.resetPreliminarySearch = function () {
            $("#userDomain, #competitorDomains").val("");
            $("#deckLogo").val(null);
            $("#logoNameWrapper").empty();
            resetFields();
            $scope.userDomain = null;
            $scope.competitorDomains = null;
            $scope.userOfflineEmail = null;
            self.isResponseSuccess = false;
            $("#getResult").button('reset');
        };

        $scope.setEmailValue = function () {
            if (sessionStorage.getItem("userOAuthEmail") !== null) {
                $scope.userOfflineEmail = sessionStorage.getItem("userOAuthEmail");
                var userEmail = sessionStorage.getItem("userOAuthEmail");
            }
        };
        function validateInputs() {
            var validation = true;
            var queryText = $scope.userDomain;
            if (queryText === undefined || queryText === null || queryText === "") {
                $('#userDomainError').html("Please enter domain");
                $('#userDomainError').show();
                $('#userDomain').focus();
                validation = false;
            } else {
                queryText = queryText.trim();
                var validate = validateInputURL(queryText);
                console.log("User domain: " + queryText + ", isValid : " + validate);
                if (!validate) {
                    $("#userDomainError").html("Please enter a valid URL");
                    $('#userDomainError').show();
                    $("#userDomain").focus();
                    validation = false;
                }
            }
            queryText = $scope.competitorDomains;
            if (queryText !== undefined && queryText !== null && queryText !== "") {
                if (queryText.indexOf(',') !== -1) {
                    $('#competitorError').html("Competitor domains to be entered one per line and commas are not allowed.");
                    $('#competitorError').show();
                    $('#competitorDomains').focus();
                    validation = false;
                } else if (getCompetitorCount('competitorDomains') >= 5) {
                    $('#competitorError').html("Only 4 competitor domains can be verified at a time!");
                    $('#competitorError').show();
                    $('#competitorDomains').focus();
                    validation = false;
                } else if (getCompetitorCount('competitorDomains') <= 4) {
                    var listOfDomains = $('#competitorDomains').val().trim().split('\n');
                    var isValid;
                    $.each(listOfDomains, function (index, item) {
                        isValid = validateInputURL(item.trim());
                        console.log("Inside each domain: " + item + ", isValid : " + isValid);
                        if (!isValid) {
                            $("#competitorError").html("Please enter a valid URL(S)");
                            $('#competitorError').show();
                            $("#competitorDomains").focus();
                            validation = false;
                            return false;
                        }
                    });
                }
            }
            if ($("#deckLogo")[0].files[0] === undefined || $("#deckLogo")[0].files[0] === '' || $("#deckLogo")[0].files[0] === null) {
                $('#logoError').html("Please upload logo");
                $('#logoError').show();
                validation = false;
            }

            /*            queryText = null;
             queryText = $scope.userOfflineEmail;
             if (queryText === undefined || queryText === null || queryText === "") {
             $('#userOffilineEmailError').html("Please enter email");
             $('#userOffilineEmailError').show();
             $('#userOffilineEmailError').focus();
             validation = false;
             } else if (!isValidMail(queryText.trim())) {
             $("#userOffilineEmailError").html("Please enter a valid email");
             $('#userOffilineEmailError').show();
             $("#userOffilineEmail").focus();
             validation = false;
             }
             */
            return validation;
        }

        function resetFields() {
            $("#notWorkingUrl, #userDomainError,#competitorError,.error,#messageToUser").empty();
            $("#downloadWrapper,.error").hide();
            /*$(".error").hide();
             $(".error").empty();*/
            self.isResponseSuccess = false;
        }

        function getCompetitorCount(id) {
            return $.trim($('#' + id).val().trim().split("\n").length);
        }
        function validateInputURL(website) {
            website = website.trim();
            var regexp = new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm);
            var isValid = regexp.test(website);
            return isValid;
        }
    }]);
    