/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
myApp.filter('startFrom', function () {
    return function (input, start) {
        start = parseInt(start);
        if (input) {
            start = +start;
            return input.slice(start);
        }
        return [];
    };
});

myApp.controller('MicroInfluencerController', ['$scope', '$http', 'filterFilter', function ($scope, $http, filterFilter, $window) {
        var self = this;

        /*Micro influencers list prepared from API response for search Query*/
        self.apiResponseList = [];
        /*Micro influencers list to display results*/
        self.dataTableMatrixList = null;
        /*Micro influencers list to select min and max filter values*/
        self.filteredList = [];
        /*To view complete profile of selected of Micro influencer from response list*/
        self.viewProfile = null;
        self.influencerCount = null;
        self.isThreadCompleted = false;
        self.noData = false;
        $scope.isNewProcessing = false;

        $scope.currentPage = 1;
        $scope.entryLimit = 3; // items per page
        $scope.noOfPages = 5;

        $scope.status = "Looking for micro influencers ....";
        $scope.progress = 0;
        $scope.progressBarOptions = {
            min: 0,
            max: 1030,
            type: "percent"
        };
        self.noResultsQuery = null;

        /*Validating input fields.*/
        $scope.fetchMicroInfluencers = function () {
            resultLoginValue = true;
            if (toolLimitExceeded()) {
                $scope.queryRequired = "";
                var status = true;
                var query = $scope.searchQuery;
                console.log("Query: " + query);
                var isContainsSymbols = /^[a-zA-Z0-9- ]*$/.test(query);
                var specialCharchterCount = 0;
                if (!isContainsSymbols) {
                    specialCharchterCount = query.match(/[@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g).length;
                }
                if (query === undefined || query === null || query === "") {
                    $scope.queryRequired = "Please enter any keyword.";
                    status = false;
                } else if (((query.split(" ").length - 1 >= 10) || (specialCharchterCount >= 10) || (((query.split(" ").length - 1) + (specialCharchterCount) >= 10)))) {
                    $scope.queryRequired = "Sorry, your query is too complex. Please reduce complexity and try again. Limit your searches to 10 keywords and operators";
                    status = false;
                }
                if (status) {
                    var $this = $("#searchInfluencers");
                    $this.button('loading');
                    self.apiResponseList = [];
                    self.filteredList = [];
                    self.dataTableMatrixList = null;
                    self.noData = false;
                    self.noResultsQuery = null;
                    self.isThreadCompleted = false;
                    $("#editSearch").show();
                    $("#searchQuery").addClass("gray");
                    $("#searchInfluencers").attr("disabled", true);
                    $("#searchQuery").attr("readonly", true);
                    $("#resultWrapper").show();
                    submitRequestToController(true);
                }
            }
        };

        /*To view selected complete micro influencer profile.*/
        $scope.viewCompleteProfile = function (selectedInfluencer) {
            self.viewProfile = selectedInfluencer;
        };
        /*Generates the list for selected range of followers*/
        $scope.followersRangeSlider = {
            options: {
                step: 1,
                onChange: function () {
                    self.filteredList = [];
                    self.isThreadCompleted = true;
                    selectedMinValue = 0;
                    selectedMaxValue = 0;
                    angular.forEach(self.apiResponseList, function (influencer) {
                        if (influencer.followers >= $scope.followersRangeSlider.minValue && influencer.followers <= $scope.followersRangeSlider.maxValue) {
                            self.filteredList.push(influencer);
                        }
                    }, self.filteredList);
                    if (self.filteredList.length !== 0) {
                        selectedMinValue = $scope.followersRangeSlider.minValue;
                        selectedMaxValue = $scope.followersRangeSlider.maxValue;
                    } else {
                        selectedMinValue = -1;
                        selectedMaxValue = -1;
                    }
                    sessionStorage.setItem('selectedMinValue', selectedMinValue);
                    sessionStorage.setItem('selectedMaxValue', selectedMaxValue);
                    constructResultTable(self.filteredList);
                }
            }
        };
        /*When navigated to tool page from ATE confirmation page or Signup/Login page refreshes*/
        $scope.processBackToSuccessPage = function () {
            setTimeout(function () {
                if (isBackToSuccess && sessionStorage.getItem("analyzedSearchQuery") !== null) {
                    $scope.searchQuery = sessionStorage.getItem("analyzedSearchQuery");
                    self.apiResponseList = [];
                    self.filteredList = [];
                    self.dataTableMatrixList = null;
                    self.noData = false;
                    self.noResultsQuery = null;
                    self.isThreadCompleted = false;
                    $("#editSearch").show();
                    $("#searchQuery").addClass("gray");
                    $("#searchInfluencers").attr("disabled", true);
                    $("#searchQuery").attr("readonly", true);
                    $("#resultWrapper").show();
                    submitRequestToController(false);
                }
            }, 100);
        };
        /*Returns count of the balance cloumns for a row */
        $scope.getRemainingColumns = function (rowLength) {
            if (rowLength < 4) {
                var rLength = 4 - (rowLength % 4);
                if (rLength >= 1) {
                    return rLength;
                }
                return 0;
            }
        };

        /*Submiting user query to controller.*/
        function submitRequestToController(isNewRequest) {
            var $this = $("#searchInfluencers");
            var formData = {"searchQuery": $scope.searchQuery};
            sessionStorage.setItem("analyzedSearchQuery", $scope.searchQuery.trim());
            if (isNewRequest) {
                callToGetUpdatedList();
            }
            $scope.followersRangeSlider.minValue = 0;
            $scope.followersRangeSlider.maxValue = 0;
            /*Saving response result list*/
            $http.post('micro-influencer-generator.html?request=analyze', formData)
                    .then(function (response) {
                        /*Saving response result list*/
                        self.apiResponseList = [];
                        if (response.data.length >= 1) {
                            constructResultTable(response.data);
                            self.apiResponseList = response.data;
                            if (response.data.length >= 2) {
                                $scope.followersRangeSlider.minValue = self.apiResponseList[self.apiResponseList.length - 1].followers;
                                $scope.followersRangeSlider.maxValue = self.apiResponseList[0].followers;
                            }
                            if (self.apiResponseList.length == 1) {
                                selectedMinValue = self.apiResponseList[0].followers;
                                selectedMaxValue = self.apiResponseList[0].followers;
                            } else {
                                selectedMinValue = $scope.followersRangeSlider.minValue;
                                selectedMaxValue = $scope.followersRangeSlider.maxValue;
                            }
                            if (atePopupStatus) {
                                atePopupStatus = false;
                                getAskExpertPopup(true);
                            }
                        } else {
                            self.apiResponseList = response.data;
                            self.noResultsQuery = $scope.searchQuery;
                            self.noData = true;
                            self.dataTableMatrixList = null;
                            self.filteredList = [];
                        }
                        self.isThreadCompleted = true;
                        $("#searchInfluencers").attr("disabled", false);
                        $("#searchQuery").attr("readonly", false);
                    })
                    .catch(function (data) {
                        self.isThreadCompleted = false;
                        console.log("Error in getting tool analysis result: " + data);
                        $("#searchInfluencers").attr("disabled", false);
                        $("#searchQuery").attr("readonly", false);
                        $("#editSearch").hide();
                        $("#searchQuery").removeClass("gray");
                        toolLimitExceededForAjax();
                    })
                    .finally(function () {
                        $this.button('reset');
                        $scope.status = "";
                        $scope.progress = 0;
                    });
        }
        //To make request to get update list after submiting query
        function callToGetUpdatedList() {
            setTimeout(function () {
                getUpdatedList();
            }, 100);
        }
        /*Get request to get updated list from controller*/
        function getUpdatedList() {
            $http.get('micro-influencer-generator.html?request=updatedList')
                    .then(function (response) {
                        /*Saving response result list*/
                        if (response.data[2] !== null && response.data[2] === "LimitExceedded") {
                            toolLimitExceededForAjax();
                            resetForm(true);
                        } else {
                            self.isThreadCompleted = response.data[0];
                            if (response.data[1] !== null && !self.isThreadCompleted) {
                                constructResultTable(response.data[1]);
                                self.apiResponseList = [];
                                self.apiResponseList = response.data[1];
                                $scope.determinateValue = self.apiResponseList.length;
                                $scope.progress = self.apiResponseList.length;
                                if (self.apiResponseList.length <= 200) {
                                    $scope.status = "Looking in micro influencers topical interest";
                                } else if (self.apiResponseList.length >= 200 && self.apiResponseList.length <= 400) {
                                    $scope.status = "Looking in micro influencers profiles";
                                } else if (self.apiResponseList.length >= 400 && self.apiResponseList.length <= 600) {
                                    $scope.status = "Looking in micro influencers tweets";
                                } else if (self.apiResponseList.length >= 600 && self.apiResponseList.length <= 800) {
                                    $scope.status = "Looking in micro influencers location";
                                } else {
                                    $scope.status = "Looking for micro influencers other criteria";
                                }
                            }
                            if (!self.isThreadCompleted) {
                                setTimeout(function () {
                                    getUpdatedList();
                                }, 10000);
                            }
                        }
                    })
                    .catch(function (data) {
                        self.isThreadCompleted = false;
                        console.log("Error in updating list: " + data);
                        $("#searchQuery").attr("readonly", false);
                        $("#editSearch").hide();
                        $("#searchQuery").removeClass("gray");
                        toolLimitExceededForAjax();
                    });
        }

        function constructResultTable(resultList) {
            self.influencerCount = resultList.length;
            if (resultList.length !== 0) {
                self.dataTableMatrixList = convertObjectArrayintoMatrix(resultList, 4);
                /*Pagination*/
                $scope.totalItems = self.dataTableMatrixList.length;
                $scope.noOfPages = 5;
            } else {
                self.dataTableMatrixList = [];
                $scope.totalItems = 0;
                $scope.noOfPages = 0;
            }
        }

        /*Converting result list into (2 dimensionally) matrix [[n][4]] list to display 4 objects per row*/
        function convertObjectArrayintoMatrix(responseList, n) {
            self.dataTableMatrixList = null;
            var grid = [], count = 0, col, row = -1;
            for (var count = 0; count < responseList.length; count++) {
                col = count % n;
                if (col === 0) {
                    grid[++row] = [];
                }
                grid[row][col] = responseList[count];
            }
            return grid;
        }
        /*Clear all the fields*/
        function resetForm(resetForm) {
            self.apiResponseList = [];
            self.filteredList = [];
            self.dataTableMatrixList = null;
            if (resetForm) {
                $scope.searchQuery = "";
                $("#resultWrapper").hide();
            }
            self.noData = false;
            self.noResultsQuery = null;
            $("#searchInfluencers").attr("disabled", false);
            $("#searchQuery").attr("readonly", false);
            $("#editSearch").hide();
            $("#searchQuery").removeClass("gray");
        }
    }]);
/*Eof of  controller*/


