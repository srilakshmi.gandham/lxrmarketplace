

function loadGraphData() {
    fd = $("#fromDate").val().split("-");
    td = $("#toDate").val().split("-");
    
    fd = new Date(fd[2], fd[1] - 1, fd[0]);
    td = new Date(td[2], td[1] - 1, td[0]);
    
    var stDate, endDate;
    if (fd !== null)
        stDate = fd.getFullYear() + "-" + (fd.getMonth() + 1) + "-"
                + fd.getDate() + " 00:00:00";
    if (td !== null)
        endDate = td.getFullYear() + "-" + (td.getMonth() + 1) + "-"
                + td.getDate() + " 23:59:59";

    $.ajax({
        type: "POST",
        url: "/weekly-keyword-rank-checker-tool.html?dateRange='dateRange'&stDate=" + stDate + "&endDate=" + endDate + "&keywordId=" + selectedFirstKeyId,
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data !== null) {
                drawKeywordRankChart(data);
            }
        }

    });
}

function showKeywordChart(keywordId) {
    selectedFirstKeyId = keywordId;
    $.ajax({
        type: "POST",
        url: "/weekly-keyword-rank-checker-tool.html?keywordId=" + keywordId,
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data !== null) {
                drawKeywordRankChart(data);
            }

        }


    });
}



function showCompDomain(compDomId, a) {
    var selected1 = $(a).html();

    $("#cmpList").html(selected1);
    $("#list").css("display", "none");
    $.ajax({
        type: "POST",
        url: "/weekly-keyword-rank-checker-tool.html?compDomId=" + compDomId,
        processData: true,
        data: {},
        dataType: "json",
        success: function (data) {
            if (data[0] != null) {
                for (var i = 0; i <= data[0].length; i++) {
                    if (data[0][i] != null && data[0][i] != "") {
                        var progImg = "", bingProg = "", googleRank = "", bingRank = "";
                        if (data[0][i].keywordRankInG > 0) {
                            googleRank = "" + data[0][i].keywordRankInG;
                        } else if (data[0][i].keywordRankInG == -1) {
                            googleRank = "> 100";
                        } else if (data[0][i].keywordRankInG == 0) {
                            googleRank = "–";
                        }
                        if (data[0][i].keywordRankInB > 0) {
                            bingRank = "" + data[0][i].keywordRankInB;
                        } else if (data[0][i].keywordRankInB == 0) {
                            bingRank = "–";
                        } else if (data[0][i].keywordRankInB == -1) {
                            bingRank = "> 100";
                        }

                        if (userLoggedIn) {
                            if (data[0][i].progressInG == 1) {
                                progImg += "<span class=\"greenUp\"></span>"
                            } else if (data[0][i].progressInG == 0) {
                                progImg += "<span class=\"blueRight\"></span>"
                            } else if (data[0][i].progressInG < 0) {
                                if (data[0][i].progressInG == -1)
                                    progImg += "<span class=\"redDown\"></span>"
                            } else if (data[0][i].progressInG == 5 || data[0][i].progressInG == 10) {
                                progImg += "<span>" + "" + "</span>"
                            }
                            if (data[0][i].progressInB == 1) {
                                bingProg += "<span class=\"greenUp\"></span>"
                            } else if (data[0][i].progressInB == 0) {
                                bingProg += "<span class=\"blueRight\"></span>"
                            } else if (data[0][i].progressInB < 0) {
                                if (data[0][i].progressInB == -1)
                                    bingProg += "<span class=\"redDown\"></span>"
                            } else if (data[0][i].progressInB == 5 || data[0][i].progressInB == 10) {
                                bingProg += "<span>" + "" + "</span>"
                            }
                            $(".comp-grank")[i].innerHTML = "<span>" + googleRank + progImg + "</span>";
                            $(".comp-brank")[i].innerHTML = "<span>" + bingRank + bingProg + "</span>";
                        } else if (!userLoggedIn) {
                            $(".comp-grank")[i].innerHTML = "<span>" + googleRank + "</span>";
                            $(".comp-brank")[i].innerHTML = "<span>" + bingRank + "</span>";
                        }
                    }
                }
            }

        }
    });
}

function showdomainpopup() {
    $("#domainpopup").show();
}

function showDomainInfo() {
    var values = [];
    $('#compDomain').each(function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            values.push($this.val());
        }
    });
}

function showsettings() {
    $("#settingsdiv").show();
    $("#results").hide();
}

function deleteCompDomainRow(domainId) {
    if (shownDomArray !== null && shownDomArray.length > 0) {
        for (var i = 0; i < shownDomArray.length; i++) {
            if (domainId == shownDomArray[i]) {
                var state = document.getElementById("d" + shownDomArray[i]);
                if (state.style.display === 'block') {
                    state.style.display = 'none';
                    var inputDomArray = state.getElementsByTagName('input');
                    inputDomArray[0].value = "";
                    hiddenDomArray.push(shownDomArray[i]);
                    shownDomArray.splice(i, 1);
                }
            }
        }
    }

}


function showCompDomainRow(queryURl) {
    $('.no-comp-val').hide();
    if (hiddenDomArray.length > 0) {
        var state = document.getElementById("d" + hiddenDomArray[0]);
        if (state.style.display === 'none') {
            state.style.display = 'block';
            var inputDomArray = state.getElementsByTagName('input');
            if (queryURl === 'ADD') {
                inputDomArray[0].value = "";
            } else if (queryURl !== 'ADD' && queryURl !== "") {
                inputDomArray[0].value = queryURl;
            }
            shownDomArray.push(hiddenDomArray[0]);
        }
        hiddenDomArray = hiddenDomArray.slice(1, hiddenDomArray.length);
    } else {
        alert("Only 3 Competitors are allowed.");
    }

}

function showkeywordRow() {
    if (hiddenKeywordArray.length > 0) {
        var state = document.getElementById("k" + hiddenKeywordArray[0]);
        if (state.style.display === 'none') {
            shownKeywordArray.push(hiddenKeywordArray[0]);
            var keyWordState = document.getElementById("k" + hiddenKeywordArray[0]);
            if (keyWordState.style.display === 'none') {
//                state.style.display = 'block';
                state.style.display = '';
                var inputArray = keyWordState.getElementsByTagName('input');
                inputArray[0].value = "";
                inputArray[1].value = "";
                if (inputArray[3].type === 'checkbox') {
                    inputArray[3].checked = false;
                }
                if (inputArray[4].type === 'checkbox') {
                    inputArray[4].checked = false;
                }
                if (inputArray[5].type === 'checkbox') {
                    inputArray[5].checked = false;
                }

                var selectArray = keyWordState.getElementsByTagName('select');
                selectArray[0].value = 0;
            }
        }
        hiddenKeywordArray = hiddenKeywordArray.slice(1, hiddenKeywordArray.length);
    } else {
        alert("Only 5 Keywords are allowed.");
    }
}

function deleteKeywordRow(idVal) {
    if (shownKeywordArray != null && shownKeywordArray.length > 0) {
        if (shownKeywordArray.length == 1) {
            alert("At least one Keyword is Required");
            return false;
        }
        for (var i = 0; i < shownKeywordArray.length; i++) {
            if (idVal == shownKeywordArray[i]) {
                var state = document.getElementById("k" + shownKeywordArray[i]);
                if (state.style.display === 'block' || state.style.display === '') {

                    state.style.display = 'none';
                    var inputArray = state.getElementsByTagName('input');
                    inputArray[0].value = "";
                    inputArray[1].value = "";
                    if (inputArray[3].type === 'checkbox') {
                        inputArray[3].checked = false;
                    }
                    if (inputArray[4].type === 'checkbox') {
                        inputArray[4].checked = false;
                    }
                    if (inputArray[5].type === 'checkbox') {
                        inputArray[5].checked = false;
                    }

                    var selectArray = state.getElementsByTagName('select');
                    selectArray[0].value = 0;
                    hiddenKeywordArray.push(shownKeywordArray[i]);
                    shownKeywordArray.splice(i, 1);
                }
            }
        }
    }
}
function drawKeywordRankChart(graphData) {
    var data = new google.visualization.DataTable();
    if (graphData.length == 0) {
        $('#statschartdiv').empty();
        $('<span class="NoData">No data available</span>').appendTo('#statschartdiv');
    } else {
        data.addColumn('date', 'Days');
        data.addColumn('number', 'Google');
        data.addColumn('number', 'Bing');
        var dataSetArray = [];

        var setMaxDate = new Date((graphData[graphData.length - 1][0]));
        if (graphData.length == 1) {
            setMaxDate.setDate(setMaxDate.getDate() + 1);
        }

        var date = new Date(Date.parse(graphData[0][0]));
        for (var i = 0; i < graphData.length; i++) {
            var objSetArray = [];
            objSetArray.push(new Date(Date.parse(graphData[i][0])));
            objSetArray.push(parseInt(graphData[i][1]));
            objSetArray.push(parseInt(graphData[i][2]));
            dataSetArray.push(objSetArray);
        }
        data.addRows(dataSetArray);

        var Options = {
            width: 1100,
            height: 270,
            chartArea: {'width': 700, 'height': 220},
            legend: 'top',
            pointSize: 6,
            colors: ['#2FB2C6', '#FF944D'],
            lineWidth: 2.5,
            title: '',
            titleTextStyle: {
                color: '#555555'
            },
            animation: {
                duration: 1000,
                easing: 'linear',
                startup: true
            },
            series: {
            },
            hAxis: {
                format: 'MMM dd',
                gridlines: {color: 'transparent'},
                baseline: date,
                baselineColor: '#ccc',
                maxValue: setMaxDate
            },
            vAxis: {
                viewWindow: {min: 0},
                minValue: 0, maxValue: 1,
                baselineColor: '#ccc',
                //gridlines: {color: '#ccc'},
                //color: '#A3A3A3',
            }
        };
        var chart = new google.visualization.LineChart(document.getElementById('statschartdiv'));
        chart.draw(data, Options);
    }
}

