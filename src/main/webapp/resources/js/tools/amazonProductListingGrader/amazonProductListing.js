/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global file, isBackToSuccess, container, ProgressBar, $this, $switchLabel */

var app = angular.module('myApp', []);
app.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
app.controller('myCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {

        var tempParentUrl = $location.absUrl();
        if (tempParentUrl.indexOf("backToSuccessPage=") !== -1) {
            processBackToSuccessPageForURL();
        } else {

            setTimeout(function () {
                $('#loadingDiv').hide();
//                    document.getElementById("loader").style.display = "none";
            }, 1500);
        }
        $scope.amazonProductAnalysis = null;
        $scope.amazonProductListing = null;
        $scope.amazonProductInfo = null;
        $scope.amazonProductInfoList = null;
        $scope.repeatedUrls = null;
        $scope.isChecked = true;
        $("#resultInfo").hide();
        function progressBar(value) {
            $("#container").html("");
            var bar = new ProgressBar.Circle(container, {
                strokeWidth: 10,
                easing: 'easeInOut',
                duration: 1400,
                color: getColorByTotalScore(value),
                trailColor: '#D3D3D3',
                trailWidth: 9,
                svgStyle: null
            });
            bar.animate(value / 100); // Number from 0.0 to 1.0
        }
        $scope.submitUrl = function () {
            resultLoginValue = true;
            isPreUpload = false;
            var status = true;
//            $('#fileName').hide();
            $("#fileLabel").hide();
            $("#fileName").hide();
            $("#fileUpload").val("");
            $("#fileUpload1Error").hide();
            $("#searchQueryError").hide();
            var urls = $("#manuallyURLS").val();
            if (toolLimitExceeded()) {
                if (urls === undefined || urls === null || urls === "") {
//                    alert();
                    $("#searchQueryError").html("Please enter URLs.");
                    $("#searchQueryError").show();
                    status = false;
                    $("#resultInfo").hide();
                }
                if (getUrlsCount('manuallyURLS') > 100) {
                    $("#searchQueryError").html("Only 100 URL(S) can be verified at a time!");
                    $("#searchQueryError").show();
                    status = false;
                }
                if (status) {
                    $('#switchButton').attr("disabled", true);
                    $("#manuallyURLS").attr("disabled", true);
                    var $this = $("#loading");
                    $this.button('loading');
                    var requestURL = 'product-listing.html?request=URLS';
                    var config = {
                        headers: {'Content-Type': undefined}
                    };
                    var formData = new FormData();
                    formData.append("manuallyURLS", $scope.manuallyURLS);
                    $("#manuallyURLS").addClass("gray");
                    $http.post(requestURL, formData, config)
                            .then(function (response) {
//                                errorUrlsForpost();
                                $('#switchButton').attr("disabled", false);
                                $("#manuallyURLS").attr("disabled", false);
                                $scope.amazonProductListing = response.data[0];
                                $scope.amazonProductAnalysis = response.data[2];
                                $scope.amazonProductInfoList = response.data[1];
                                $scope.repeatedUrls = response.data[3];
                                if (response.headers('limitexceed') > 0) {
                                    $("#resultInfo").hide();
                                    toolLimitExceededForAjax();
                                } else if ($scope.amazonProductListing === null || $scope.amazonProductListing === "" || $scope.amazonProductListing.length === 0)
                                {
                                    $("#searchQueryError").show();
                                    $("#searchQueryError").html("URL(S) cannot be reviewed. There may be several reasons for this:<br>&#8226; URL(S) does not exist.&nbsp;  &#8226; URL(S) might not be a amazon product URL(S) &nbsp; &#8226; Website does not allow LXRMarketplace to read those URL(S).&nbsp;  &#8226; The robots.txt page does not allow access to our bot.");
                                    $("#resultInfo").hide();
                                } else if ($scope.amazonProductInfoList.length == 0) {
                                    $("#searchQueryError").hide();
                                    $('#ratingId').show();
                                    $('#reviewsId').show();
                                    $('#ratingLabel').show();
                                    $('#reviewLabel').show();
                                    $('#ratingProgress').show();
                                    $('#reviewProgress').show();
                                    $("#resultInfo").show();
                                    $scope.amazonProductInfo = $scope.amazonProductListing[0];
                                    progressBar($scope.amazonProductInfo.totalScore);
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }

                                    setTimeout(function () {
                                        sliderFunV2();
                                    }, 1000);
                                } else
                                {
                                    $("#searchQueryError").show();
                                    $("#searchQueryError").html("Few URL(S) cannot be reviewed. There may be several reasons for this:<br>&#8226; URL(S) does not exist.&nbsp;  &#8226; URL(S) might not be a amazon product URL(S) &nbsp; &#8226; Website does not allow LXRMarketplace to read those URL(S).&nbsp;  &#8226; The robots.txt page does not allow access to our bot.");
                                    $('#ratingId').show();
                                    $('#reviewsId').show();
                                    $('#ratingLabel').show();
                                    $('#reviewLabel').show();
                                    $('#ratingProgress').show();
                                    $('#reviewProgress').show();
                                    $("#resultInfo").show();
                                    $scope.amazonProductInfo = $scope.amazonProductListing[0];
                                    progressBar($scope.amazonProductInfo.totalScore);
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }
                                    setTimeout(function () {
                                        sliderFunV2();
                                    }, 1000);
                                }
                            })
                            .catch(function (data) {
//                                alert("catch");
                            })
                            .finally(function () {
                                $this.button('reset');
                            });
                } else {
                    return status;
                }
            }
        };
        $(' #manuallyURLS').on('click', function () {
            $("#searchQueryError").html("");
            $("#manuallyURLS").removeClass("gray");
            $("#manuallyURLS").attr("disabled", false);
        });
        $(' #browseFile').on('click', function () {
            $("#fileUpload1Error").html("");
        });
//        $("#clickOnId").on('click', function () {
//            $("#errorUrls").show();
//        });
//        function errorUrlsForpost() {
//        $scope.errorUrls = function () {
//            $("#errorUrls").show();
//        };
//        }
        function getColorByTotalScore(score) {
            var color = "";
            if (score <= 30) {
                color = 'red';
            } else if (score > 30 && score <= 70) {
                color = 'orange';
            } else if (score > 70) {
                color = 'green';
            }
            return color;
        }


        $scope.getColorByScore = function (score) {
            var color = "";
            if (score <= 30) {
                color = 'red';
            } else if (score > 30 && score <= 70) {
                color = 'orange';
            } else if (score > 70) {
                color = 'green';
            }
            return color;
        };
        $scope.getMessageByScore = function (score) {
            var message = "";
            if (score <= 30) {
                message = 'Need Improvement.';
            } else if (score > 30 && score <= 70) {
                message = 'Can Do Better!';
            } else if (score > 70) {
                message = 'Keep Going!';
            }
            return message;
        };
        $scope.indexBasedData = function (val)
        {
            $(" button").removeClass("button-clicked");
            $(" button").attr("disabled", false);
            $("#indexedButton" + val).addClass("button-clicked");
            $("#indexedButton" + val).attr("disabled", true);
            $scope.amazonProductInfo = $scope.amazonProductListing[val];
            progressBar($scope.amazonProductInfo.totalScore);
            $("#indexedButton").addClass('button-clicked');
        };
        $scope.getLength = function (obj) {
            if (obj !== null && obj !== undefined) {
                return Object.keys(obj).length;
            } else {
                return 0;
            }
        };
        /*When navigated to tool page from ATE confirmation page or Signup/Login page refreshes*/
        function processBackToSuccessPageForURL() {
            setTimeout(function () {
                $http.post('/product-listing.html?backToSuccessPage=backToSuccessPage')
                        .then(function (response) {
//                            alert();
                            $scope.amazonProductAnalysis = response.data[2];
                            if ($scope.amazonProductAnalysis.preUpload === true)
                            {
                                $scope.amazonProductListing = response.data[0];
                                $scope.repeatedUrls = response.data[1];
//                                alert($scope.amazonProductAnalysis.preUpload);
                                if ($scope.amazonProductListing !== null) {
                                    switchChangeFunction($scope.amazonProductAnalysis.preUpload);
                                    $('#upload').show();
                                    $('#urls').hide();
                                    $('#fileName').html($scope.amazonProductAnalysis.fileUpload);
                                    $('#fileName').show();
                                    $("#fileLabel").show();
                                    $('#ratingId').hide();
                                    $('#reviewsId').hide();
                                    $('#ratingLabel').hide();
                                    $('#reviewLabel').hide();
                                    $('#ratingProgress').hide();
                                    $('#reviewProgress').hide();
                                    $('#resultInfo').show();
                                    $scope.amazonProductInfo = $scope.amazonProductListing[0];
                                    progressBar($scope.amazonProductInfo.totalScore);
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }
                                } else {
                                    $('#resultInfo').hide();
                                }

                                setTimeout(function () {

                                    sliderFunV2();
                                }, 1000);
                            } else
                            {
                                $scope.amazonProductListing = response.data[0];
                                $scope.repeatedUrls = response.data[3];
                                $scope.amazonProductInfoList = response.data[1];
                                console.log($scope.amazonProductInfoList.length);
                                switchChangeFunction($scope.amazonProductAnalysis.preUpload);
                                $('#urls').show();
                                $('#upload').hide();
                                $scope.manuallyURLS = $scope.amazonProductAnalysis.manuallyURLS;
                                $("#manuallyURLS").val($scope.amazonProductAnalysis.manuallyURLS);
                                $("#manuallyURLS").attr("disabled", false);
                                if ($scope.amazonProductListing !== null && $scope.amazonProductListing !== "" && $scope.amazonProductListing.length !== 0) {
                                    $('#resultInfo').show();
                                    $scope.amazonProductInfo = $scope.amazonProductListing[0];
                                    progressBar($scope.amazonProductInfo.totalScore);
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }
                                } else {
                                    $('#resultInfo').hide();
                                }
                                setTimeout(function () {
                                    sliderFunV2();
                                }, 1000);
                            }
                        })
                        .catch(function (data) {
                        })
                        .finally(function () {
//                            $this.button('reset');
                            setTimeout(function () {
                                $('#loadingDiv').hide();
                            }, 1000);
                        });
            }, 200);
        }
//        
        function switchChangeFunction(uploadtype) {
            if (uploadtype) {
                $("#switchButton").prop('checked', true);
            } else {
                $("#switchButton").prop('checked', false);
            }
        }

        $scope.continueFileUpload = function () {
            resultLoginValue = true;
            isPreUpload = true;
            var status = true;
            $("#resultInfo").hide();
            $("#manuallyURLS").val("");
            $("#fileUpload1Error").hide();
            $("#searchQueryError").hide();
            $("#errorUrlsDiv").hide();
            if (toolLimitExceeded()) {
                var fileName = $("#fileUpload").val();
                if (fileName === undefined || fileName === null || fileName === "") {
                    $("#fileLabel").hide();
                    $("#fileUpload1Error").html("Please upload file");
                    $("#fileUpload1Error").show();
                    $("#fileName").hide();
                    status = false;
                    $("#resultInfo").hide();
                } else if (!(fileName.endsWith(".xls") || fileName.endsWith(".xlsx"))) {
                    $("#fileLabel").hide();
                    $("#fileUpload1Error").html("Please upload xlsx or xls file");
                    $("#fileUpload1Error").show();
                    status = false;
                    $("#resultInfo").hide();
                    $("#fileName").hide();
                    $('#fileUpload').val("");
                }
                var requestURL = "/product-listing.html?request=fileUpload";
                var config = {
                    headers: {'Content-Type': undefined}
                };
                var formData = new FormData();
                formData.append("file", $scope.fileModel);
                if (status)
                {
                    $scope.amazonProductInfoList = null;
                    $scope.repeatedUrls = null;
                    $('#switchButton').attr("disabled", true);
                    $("#resultInfo").hide();
                    $("#fileLabel").show();
                    $("#fileName").show();
                    var $this = $("#loading1");
                    $this.button('loading');
                    $http.post(requestURL, formData, config)
                            .then(function (response) {
                                $('#switchButton').attr("disabled", false);
                                $scope.amazonProductListing = response.data[0];
                                $scope.repeatedUrls = response.data[1];
                                $scope.amazonProductAnalysis = response.data[2];
                                if (response.headers('limitexceed') > 0) {
                                    toolLimitExceededForAjax();
                                    $("#fileLabel").hide();
                                    $('#resultInfo').hide();
                                    $("#fileName").hide();
                                    $('#fileUpload').val("");
                                } else if ($scope.amazonProductListing === null || $scope.amazonProductListing === "" || $scope.amazonProductListing.length === 0)
                                {
                                    $("#fileLabel").hide();
                                    $("#fileUpload1Error").show();
                                    $("#fileUpload1Error").html("This file cannot be reviewed. There may be several reasons for this:<br>&#8226; Not as per the amazon template. &nbsp; &#8226; Website does not allow LXRMarketplace to make reviews.&nbsp;  &#8226; The robots.txt page does not allow access to our bot.");
                                    $('#resultInfo').hide();
                                    $("#fileName").hide();
                                    $('#fileUpload').val("");
                                } else {
                                    $('#ratingId').hide();
                                    $('#reviewsId').hide();
                                    $('#ratingLabel').hide();
                                    $('#reviewLabel').hide();
                                    $('#ratingProgress').hide();
                                    $('#reviewProgress').hide();
                                    $('#resultInfo').show();
                                    $scope.amazonProductInfo = $scope.amazonProductListing[0];
                                    progressBar($scope.amazonProductInfo.totalScore);
//                                    changeTypeAfterResult();
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }
                                    setTimeout(function () {
                                        sliderFunV2();
                                    }, 1000);
                                }
                            })
                            .catch(function (data) {
                            })
                            .finally(function () {
                                $this.button('reset');
                            });
                } else {
                    return status;
                }
            }
        };
        function getUrlsCount(id)
        {
            var count = 0;
            var listUrls;
            var urlsAsString = $.trim($('#' + id).val().split("\n"));
            listUrls = urlsAsString.split(",");
//            alert(listUrls.length);
            for (var i = 0; i < listUrls.length; i++) {
                if (listUrls[i] === "") {
                    count++;
                }
            }
//            alert(count);
            return listUrls.length - count;
//            return $.trim($('#' + id).val().split("\n").length);
        }

    }]);

          