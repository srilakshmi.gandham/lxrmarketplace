/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var domainFinderAPP = angular.module('domainFinderPreSales', []);
domainFinderAPP.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
domainFinderAPP.controller('DomainFinderController', ['$scope', '$http', function ($scope, $http) {
        var self = this;
        var offLineURLSLimit = 100;
        var limitExceededCount = 3000;
        var messageToUser = "The list of company name provided to analyze exceeds the recommended limit of 100 company names. If you wish to continue, the results of the analysis will be sent to your email offline within 30 minutes. Alternately, you can use less than 100 company names at a time to get instant results to view/download";
        var limitExceededMsgPrefix = "Your feed contains ";
        var limitExceededMsPostFix = " company names. Please upload a maximum of " + limitExceededCount + " company names.";
        self.domainFinderResultList = [];
        self.isResponseSuccess = false;
        var offlineReportEmail = "";
        $scope.processDomainFinderQuery = function () {
            if (isGmailLoginSuccess) {
                clearErrorMessage();
                if (validateInputs()) {
                    var manaullyQueryText = $scope.queryCompanyNames;
                    if (manaullyQueryText !== undefined && manaullyQueryText !== null && manaullyQueryText !== "" && manaullyQueryText !== '') {
                        submitManullyQuery();
                    }
                    if ($("#keywordsFeedList")[0].files[0] !== undefined && $("#keywordsFeedList")[0].files[0] !== '' && $("#keywordsFeedList")[0].files[0] !== null) {
                        submitFeedQuery();
                    }
                }
            } else {
                gmailLoginSuccess();
                $("#googleLoginPopupModal").modal('show');
                var errorMessage = sessionStorage.getItem('DomainFinderError');
                if (errorMessage !== null) {
                    $("#loginErrorMessage").empty();
                    $("#errorBodyWrapper").show();
                    $("#loginErrorMessage").html(errorMessage);
                }
                $("#loginErrorMessage").empty();
                $("#errorBodyWrapper").hide();
            }

        };

        $scope.resetDomainForm = function () {
            $("#feedPath").val('');
            $("#queryCompanyNames").val('');
            $("#keywordsFeedList").val(null);
            $("#keywordsFeedList, #feedPath, #inputQueryError, #errorMessageInfo").empty();
            $("#domainFinderResultWrapper").hide();
            offlineReportEmail = "";
            self.domainFinderResultList = [];
            self.isResponseSuccess = false;
            enableInputs();
        };
        $scope.acceptConditions = function () {
//            alert("acceptConditions");
            $('#offlineError').show();
            $('#offlineError').empty();
            var emailText = $.trim($("#offlineEmail").val());
            var isValid = true;
            var errorMsg = "";
            if (emailText === null || emailText === "") {
                errorMsg = "Please enter your email id";
                isValid = false;
            } else if (!isValidMail(emailText)) {
                errorMsg = "Please enter a valid email";
                isValid = false;
            }
            if (!isValid) {
                $("#offlineEmail").focus();
                $("#offlineError").html(errorMsg);
                $('#offlineError').css("color", "red");
                return false;
            } else {
                $("#confirmationLimitPopupModal").modal('hide');
                offlineReportEmail = emailText;
                submitFormRequest();
            }
        };
        $scope.cancelInput = function () {
            $("#confirmationLimitPopupModal").modal('hide');
            $("#errorMessageInfo").empty();
            enableInputs();
        };
        $scope.downloadDomainFinderReport = function () {
            var downloadForm = null;
            downloadForm = $("#domainFinderForm");
            downloadForm.attr('method', "POST");
            downloadForm.attr('action', "/domain-finder.html?request=download");
            downloadForm.submit();
        };
        $scope.sendDomainFinderReport = function () {
            $('#mailSentStatus').empty();
            var emailid = $.trim($("#userEmail").val());
            var status = true;
            var statusMsg = "";
            if (emailid === "") {
                statusMsg = "Please enter your email id";
                status = false;
            } else if (!isValidMail(emailid)) {
                statusMsg = "Please enter a valid email address.";
                status = false;
            }
            $('#mailSentStatus').show();
            if (!status) {
                $("#userEmail").focus();
                $("#mailSentStatus").html(statusMsg);
                $('#mailSentStatus').css("color", "red");
                return false;
            } else {
                $('#mailSentStatus').html("sending...");
                $('#mailSentStatus').css("color", "green");
                $.ajax({
                    type: "POST",
                    url: "/domain-finder.html?request=sendmail&email=" + emailid + "&toolType=netelixirusers",
                    processData: true, data: {},
                    dataType: "json",
                    success: function (data) {
                    }, complete: function () {
                        $('#mailSentStatus').show();
                        $('#mailSentStatus').empty();
                        $('#mailSentStatus').html("Mail sent successfully");
                        $('#mailSentStatus').css("color", "green");
                    }
                });
            }
        };
        $scope.clearAnalayzedCount = function () {
            $("#errorMessageInfo").empty();
        };
        $scope.resetManaullyQuery = function () {
            $scope.queryCompanyNames = '';
            $scope.queryCompanyNames = null;
        };

        function submitManullyQuery() {
//            alert("in submitManullyQuery");
            var size = getCountofQueryDomains('queryCompanyNames');
            console.log("submitManullyQuery size:: " + size);
            if (size > limitExceededCount) {
                $("#errorMessageInfo").empty();
                $("#errorMessageInfo").html(limitExceededMsgPrefix + size + limitExceededMsPostFix);
                $("#errorMessageInfo").css("color", "red");
                $("#domainFinderResultWrapper").hide();
            } else if (size >= offLineURLSLimit) {
                $('#offlineError').empty();
                raiseConfirmation();
            } else if (size <= limitExceededCount) {
                submitFormRequest();
            }
        }
        ;

        function submitFeedQuery() {
            disableInputs();
            $("#errorMessageInfo").empty();
            $("#errorMessageInfo").html("<span style='color:#333333' clas='lxrm-bold'>Validating the uploaded file feed, please wait..</span>");
            var requestURL = "/domain-finder.html?request=validateKeywordsSize";
            var config = {
                headers: {
                    'Content-Type': undefined
                }
            };
            var oMyForm = new FormData();
            oMyForm.append("brandKeywordsFeed", $scope.keywordsFeedList);
            /*Saving response result list*/
            $http.post(requestURL, oMyForm, config)
                    .then(function (response) {
                        $("#errorMessageInfo").empty();
                        if (response.data === -1 || response.data === 0) {
                            console.log("error");
                            $("#errorMessageInfo").html("Unable to prepare company name(s) feed. Mandatory column is missing (OR) Error in reading data from file.");
                            $("#errorMessageInfo").css("color", "red");
                            $("#domainFinderResultWrapper").hide();
                            offlineReportEmail = "";
                            self.domainFinderResultList = [];
                            self.isResponseSuccess = false;
                        } else if (response.data !== 0 && response.data > limitExceededCount) {
                            console.log("Error in submitFeedQuery and size:: " + response.data);
                            $("#errorMessageInfo").html(limitExceededMsgPrefix + response.data + limitExceededMsPostFix);
                            $("#errorMessageInfo").css("color", "red");
                            $("#domainFinderResultWrapper").hide();
                            offlineReportEmail = "";
                            self.domainFinderResultList = [];
                            self.isResponseSuccess = false;
                        } else if (response.data !== 0 && response.data >= offLineURLSLimit) {
                            raiseConfirmation();
                        } else if (response.data !== 0 && response.data <= limitExceededCount) {
                            submitFormRequest();
                        }
                        enableInputs();
                    }).catch(function () {
                console.log("Exception in validating file");
            });
        }

        function submitFormRequest() {
//            alert("submitFormRequest");
            disableInputs();
            $("#domainFinderResultWrapper").hide();
            self.domainFinderResultList = [];
            $("#errorMessageInfo").empty();
            $("#errorMessageInfo").html("<span style='color:#333333' clas='lxrm-bold'>Analyzing the company name(s) to identify the Domain/URL. Usually it takes less than 2-4 minutes for 100 company name(s).</span>");
            $("#getResult").button('loading');
            setTimeout(function () {
                getUpdatedList();
            }, 200);
            var requestURL = "/domain-finder.html?request=analyze";
            var config = {
                headers: {
                    'Content-Type': undefined
                }
            };
            var userOAuthEmail = "";
            if (sessionStorage.getItem("userOAuthEmail") !== null) {
                userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
            }
            var oMyForm = new FormData();
            if ($scope.queryCompanyNames !== undefined && $scope.queryCompanyNames !== null && $scope.queryCompanyNames !== "") {
                oMyForm.append("brandKeywordsList", $scope.queryCompanyNames);
            } else {
                oMyForm.append("brandKeywordsList", "");
            }
            if ($("#keywordsFeedList")[0].files[0] !== undefined && $("#keywordsFeedList")[0].files[0] !== '' && $("#keywordsFeedList")[0].files[0] !== null) {
                oMyForm.append("brandKeywordsFeed", $scope.keywordsFeedList);
            } else {
                oMyForm.append("brandKeywordsFeed", new File([0], "emptyFile"));
            }
            /*This email to get anlaysis report if query list is more than 100*/
            if (offlineReportEmail === "") {
                oMyForm.append("offlineReportEmail", userOAuthEmail);
            } else {
                oMyForm.append("offlineReportEmail", offlineReportEmail);
            }
            /*This email is to track tool usage*/
            oMyForm.append("toolUsageUserEmail", userOAuthEmail);

            /*Saving response result list*/
            $http.post(requestURL, oMyForm, config)
                    .then(function (response) {
                        enableInputs();
                        $("#getResult").button('reset');
                        self.isResponseSuccess = response.data[0];
                        isConditionAccepted = false;
                        if (self.isResponseSuccess) {
                            $("#domainFinderResultWrapper").show();
                            self.domainFinderResultList = response.data[2];
//                            console.log("self.isResponseSuccess: " + self.isResponseSuccess + ",self.domainFinderResultList: " + self.domainFinderResultList + ",self.domainFinderResultList length: " + self.domainFinderResultList.length);
                            setTimeout(function () {
                                $("#userEmail").val(userOAuthEmail);
                                paginationFunctionV2();
                                $("#errorMessageInfo").empty();
                            }, 200);
                        } else {
                            $("#errorMessageInfo").empty();
                            $("#errorMessageInfo").html("<span class='lxr-danger' style='font-size:20px;'><i class='fa fa-times-circle'></i></span><span style='color:red' clas='lxrm-bold'>&nbsp;" + response.data[1] + "</span>");
                        }
                    })
                    .catch(function (data) {
                        console.log("Exception in processDomainsQuery: ", data);
                    });
        }
        ;

        function getUpdatedList() {
            setTimeout(function () {
                getCountOfAnalyzedKeywords();
            }, 2000);
        }
        ;

        function validateInputs() {
            var validation = true;
            $("#inputQueryError").empty();
            /*Checking manaully list or file list*/
            if (!($.trim($("#queryCompanyNames").val()) || ($("#keywordsFeedList")[0].files[0] !== undefined && $("#keywordsFeedList")[0].files[0] !== '' && $("#keywordsFeedList")[0].files[0] !== null))) {
                $("#inputQueryError").html("Please enter brand keyword(s) or upload file to analyze.");
                $("#queryCompanyNames").focus();
                validation = false;
                return;
            }
//            console.log("val: "+$.trim($("#queryCompanyNames").val()));
            if (($.trim($("#queryCompanyNames").val()) && ($("#keywordsFeedList")[0].files[0] !== undefined && $("#keywordsFeedList")[0].files[0] !== '' && $("#keywordsFeedList")[0].files[0] !== null && $("#queryCompanyNames").val() !== '' && $("#queryCompanyNames").val() !== ""))) {
                $("#inputQueryError").html("Please choose either manual list or upload file to analyze.");
                $("#queryCompanyNames").focus();
                validation = false;
                return;
            }
            return validation;
        }
        ;

        function raiseConfirmation() {
            var userOAuthEmail = "";
            if (sessionStorage.getItem("userOAuthEmail") !== null) {
                userOAuthEmail = sessionStorage.getItem('userOAuthEmail');
            }
            $("#offlineEmail").val(userOAuthEmail);
            $("#offlineEmail").focus();
            $("#confirmationMessageWrraper").html(messageToUser);
            $("#confirmationLimitPopupModal").modal('show');
            $('#confirmationLimitPopupModal').modal({backdrop: 'static', keyboard: false});
        }
        ;

        function getCountOfAnalyzedKeywords() {
            $http.get('/domain-finder.html?request=analyzedCount')
                    .then(function (response) {
//                        console.log("response.data[0]:"+response.data[0]+", response.data[1]:"+response.data[1]);
                        if (!response.data[0]) {
                            $("#errorMessageInfo").empty();
                            $("#errorMessageInfo").html("<span class='lxrm-bold' style='color:green'>" + response.data[1] + "</span><span style='color:#333333'>&nbsp company name(s) are analyzed....</span>");
                            setTimeout(function () {
                                getUpdatedList();
                            }, 2000);
                        }
                    })
                    .catch(function (data) {
                        console.log("Exception in getCountOfAnalyzedKeywords: " + data);
                    });
        }
        ;

        $('#clearFeedPath').click(function () {
            $("#feedPath").val('');
            $("#keywordsFeedList").val(null);
            $("#keywordsFeedList, #feedPath").empty();
        });

        function clearErrorMessage() {
            $("#inputQueryError, #errorMessageInfo").empty();
            /*$(".error").hide();
             $(".error").empty();*/
            self.isResponseSuccess = false;
        }
        ;

        function getCountofQueryDomains(id) {
//            alert(""+$('#' + id).val().trim().split("\n").length);
            return $.trim($('#' + id).val().trim().split("\n").length);
        }
        ;
        function disableInputs() {
            $("#queryCompanyNames,#keywordsFeedList,#resetDomainFinder,#getResult,#feedUploadWrapper,#clearFeedPath").prop('disabled', true);
            document.getElementById('feedUploadWrapper').style.pointerEvents = 'none';
            document.getElementById('clearFeedPath').style.pointerEvents = 'none';
        }
        ;
        function enableInputs() {
            $("#queryCompanyNames,#keywordsFeedList,#resetDomainFinder,#getResult,#feedUploadWrapper,#clearFeedPath").prop('disabled', false);
            document.getElementById('feedUploadWrapper').style.pointerEvents = 'auto';
            document.getElementById('clearFeedPath').style.pointerEvents = 'auto';
        }
        ;
        function paginationFunctionV2() {
            if ($.fn.dataTable.isDataTable('#domainFinderAnalysis')) {
                var table = $('#domainFinderAnalysis').DataTable();
                table.destroy();
            }
            $('#domainFinderAnalysis').DataTable({
                scrollX: true,
                fixedHeader: true,
                orderCellsTop: true,
                "lengthMenu": [[10, 25, -1], [10, 25, "All"]],
                columns: [
                    {width: '10%'},
                    {width: '35%'},
                    {width: '55%'}
                ]
            });
        }
        ;

    }]);