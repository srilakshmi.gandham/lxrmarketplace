/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var app1 = angular.module('myApp1', []);

app1.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind('change', function () {
                    var values = [];
                    angular.forEach(element[0].files, function (item) {
                        var value = {
                            // File Name 
                            name: item.name,
                            //File Size 
                            size: item.size,
                            //File URL to view 
                            url: URL.createObjectURL(item),
                            // File Input Value 
                            _file: item
                        };
                        values.push(value);
                    });
                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        };
    }]);
app1.controller('myCtrl1', ['$scope', '$http', '$location', function ($scope, $http, $location) {
        var fileStatus = false;
        var formatStatus = false;
        var tempParentUrl = $location.absUrl();
        if (tempParentUrl.indexOf("backToSuccessPage=") !== -1) {
            processBackToSuccessPageForImageCompression();
        } else {

            setTimeout(function () {
                $('#loadingDivForImageCompression').hide();
            }, 500);
        }
        $("#result").hide();
        $scope.download = function (fileName) {
            $("#name").val(fileName);
            $("#downloadForm").submit();
        };

//        $scope.compressedFiles = [];
        $scope.getFileDetails = function (e) {
//            $('#fileUpload1Error').html("");
            $scope.compressedFiles = [];
//            console.log($scope.compressedFiles.length);

//            $scope.compressedFiles = [];
            $scope.$apply(function () {
                // STORE THE FILE OBJECT IN AN ARRAY.
                for (var i = 0; i < e.files.length; i++) {
                    $scope.compressedFiles.push(e.files[i]);
                }
//                console.log($scope.compressedFiles.length);
            });
        };
        $scope.formatSizeUnits = function (index) {
//            console.log("index" + index);
//            console.log($scope.compressedFiles[index].size);
//            if ($scope.compressedFiles[index] != null) {
            var bytes = $scope.compressedFiles[index].size;
            if (bytes >= 1073741824) {
                bytes = (bytes / 1024 / 1024 / 1024).toFixed(2) + " GB";
            } else if (bytes >= 1048576) {
                bytes = (bytes / 1024 / 1024).toFixed(2) + " MB";
            } else if (bytes >= 1024) {
                bytes = (bytes / 1024).toFixed(2) + " KB";
            } else if (bytes > 1) {
                bytes = bytes + " bytes";
            } else if (bytes === 1) {
                bytes = bytes + " byte";
            } else {
                bytes = "0 bytes";
            }
            return bytes;
        };



        $scope.removeUnsupportedFile = function (item) {
            var index = $scope.compressedFiles.indexOf(item);
            $scope.compressedFiles.splice(index, 1);
        };

        $scope.imageData = null;
        $scope.getCompressedFile = function () {
            fileStatus = false;
            formatStatus = false;
            resultLoginValue = true;
            $("#fileUpload1Error").hide();
            var status = true;
            if (toolLimitExceeded()) {
                var files = $scope.compressedFiles;
//                console.log("files" + files.length);
                if (files === undefined || files.length == 0) {
//                    alert("if");
                    $("#fileUpload1Error").html("Please upload image file(s)");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (validation(files) === true && fileStatus === true && formatStatus === true) {
                    $("#fileUpload1Error").html("Please upload files size greater than 15KB and jpg and jpeg formats only");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (validation(files) === true && fileStatus === true) {
                    $("#fileUpload1Error").html("Please upload files size greater than 15KB only");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (validation(files) === true && formatStatus === true) {
                    $("#fileUpload1Error").html("Please upload files in jpg and jpeg formats only");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (totalFileSize(files) > 1073741824 && files.length > 5) {
                    $("#fileUpload1Error").html("Please upload less than or equal to 5 files whose size is less than or equal to 1GB");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (totalFileSize(files) > 1073741824) {
                    $("#fileUpload1Error").html("Please upload files whose size is less than or equal to 1GB");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                } else if (files.length > 5) {
                    $("#fileUpload1Error").html("Please upload less than or equal to 5 files");
                    $("#fileUpload1Error").show();
                    $("#result").hide();
                    status = false;
                }


                if (status) {
                    var $this = $("#loading");
                    $this.button('loading');
                    var requestURL = "/image-compression.html?request=analyze";
                    var config = {
                        headers: {'Content-Type': undefined}
                    };
                    var formData = new FormData();
                    for (var i = 0; i < $scope.compressedFiles.length; i++) {
                        if ($scope.compressedFiles[i] !== null) {
                            formData.append('imageFile', $scope.compressedFiles[i]);
                        }

                    }
                    $http.post(requestURL, formData, config)
                            .then(function (response) {
                                $scope.imageData = response.data;
//                                console.log(response.headers('limitexceed'));
                                if (response.headers('limitexceed') > 0) {
                                    toolLimitExceededForAjax();
                                    $("#result").hide();

                                } else if ($scope.imageData === null || $scope.imageData === "" || $scope.imageData === undefined)
                                {
                                    $("#fileUpload1Error").html("File Cannot Be Processed Because of the Following Reasons 1:Please upload jpeg,png or jpg file 2:Please upload a file having size greater than or equal to 15KB");
                                    $("#fileUpload1Error").show();
                                    $("#result").hide();
                                    $('#imageFile').val("");
                                } else {
                                    imageCompressionResult();
                                    $("#result").show();
                                    if (atePopupStatus) {
                                        atePopupStatus = false;
                                        getAskExpertPopup(true);
                                    }
                                }

                            })
                            .catch(function () {
                            })
                            .finally(function () {
                                $this.button('reset');
                            });

                } else {
                    return status;
                }
            }
        };
        function imageCompressionResult() {
            $('html, body').animate({scrollTop: $('#formData').offset().top}, 'slow');
        }
        function totalFileSize(files) {
            var totalSize;
            for (var i = 0; i < files.length; i++) {
                totalSize = totalSize + files[i].size;
            }
            return totalSize;
        }
        function  validation(files) {
            var validationStatus = false;
//            console.log(files.length);
            for (var i = 0; i < files.length; i++) {
                var fileName = files[i].name;
                var fileSize = files[i].size;
                if (!(fileName.endsWith(".jpeg") || fileName.endsWith(".jpg"))) {
//                    console.log(fileName);
                    formatStatus = true;
                    validationStatus = true;
                }
                if (fileSize < 15360) {
//                    console.log(fileSize);
                    fileStatus = true;
                    validationStatus = true;
                }
            }
            return validationStatus;
        }

        $(' #browseFile').on('click', function () {
            $("#fileUpload1Error").html("");
//            $scope.compressedFiles.empty();
            $("#imageFile").val("");

        });
        $scope.fileSizeEqual = function (imageInfo) {
//            console.log(imageInfo);
            return  conversionOfKiloBytesToBytes(imageInfo);

        };
        function conversionOfKiloBytesToBytes(imageInfo) {
            var actualImageSize;
            var compressedImageSize;
            actualImageSize = actualImage(imageInfo);
            compressedImageSize = compressedImage(imageInfo);
//            console.log(actualImageSize);
//            console.log(compressedImageSize);
            if (actualImageSize <= compressedImageSize) {
                return true;
            } else {
                return false;
            }

        }
        function compressedImage(imageInfo) {
            var compressedImageSize;
            if (imageInfo.compressedImageSize.split(" ")[1] === "KB") {
                compressedImageSize = imageInfo.compressedImageSize.split(" ")[0] * 1024;
            } else if (imageInfo.compressedImageSize.split(" ")[1] === "MB") {
                compressedImageSize = imageInfo.compressedImageSize.split(" ")[0] * 1024 * 1024;
            } else if (imageInfo.compressedImageSize.split(" ")[1] === "GB") {
                compressedImageSize = imageInfo.compressedImageSize.split(" ")[0] * 1024 * 1024 * 1024;
            } else if (imageInfo.compressedImageSize.split(" ")[1] === "Bytes") {
                compressedImageSize = imageInfo.compressedImageSize.split(" ")[0];
            }
            return compressedImageSize;
        }
        function actualImage(imageInfo) {
            var actualImageSize;
            if (imageInfo.imageSize.split(" ")[1] === "KB") {
                actualImageSize = imageInfo.imageSize.split(" ")[0] * 1024;
            } else if (imageInfo.imageSize.split(" ")[1] === "MB") {
                actualImageSize = imageInfo.imageSize.split(" ")[0] * 1024 * 1024;
            } else if (imageInfo.imageSize.split(" ")[1] === "GB") {
                actualImageSize = imageInfo.imageSize.split(" ")[0] * 1024 * 1024 * 1024;
            } else if (imageInfo.imageSize.split(" ")[1] === "Bytes") {
                actualImageSize = imageInfo.imageSize.split(" ")[0];
            }
            return actualImageSize;
        }

        function processBackToSuccessPageForImageCompression() {

//            alert(isBackToSuccess);
            setTimeout(function () {
                $http.post('/image-compression.html?backToSuccessPage=backToSuccessPage')
                        .then(function (response) {
//                            alert(response.data);
                            $scope.imageData = response.data;
                            if ($scope.imageData === null || $scope.imageData === "" || $scope.imageData === undefined) {
                                $('#result').hide();
//                                alert("if process");
                            } else {
                                imageCompressionResult();
//                                alert($scope.imageData.fileName);
//                                $scope.compressedFiles = $scope.imageData.fileName;
//                                $scope.compressedFiles = $scope.imageData.imageSize;
//                                alert($scope.compressedFiles);
//                                $('.filenames').val($scope.compressedFiles);
                                $('#result').show();
                                if (atePopupStatus) {
                                    atePopupStatus = false;
                                    getAskExpertPopup(true);
                                }
                            }
                        })
                        .catch(function (data) {
                        })
                        .finally(function () {
                            setTimeout(function () {
                                $('#loadingDivForImageCompression').hide();
                            }, 1000);
//                            $this.button('reset');
                        });
            }, 200);
        }
    }]);