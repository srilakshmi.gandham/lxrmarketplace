/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var myApp = angular.module("lxrmarketplace",['rzModule', 'ui.bootstrap','kendo.directives']);
var baseURL = window.location.protocol + "//" + window.location.host + "/";
//var baseURL = 'http://localhost:8080/';
//var baseURL = 'http://192.168.0.243:7071/';
//var baseURL = 'https://lxrmarketplace.com/';
myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
}]);
