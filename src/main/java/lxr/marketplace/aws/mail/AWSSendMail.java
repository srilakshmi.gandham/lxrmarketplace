package lxr.marketplace.aws.mail;

import java.io.InputStream;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.amazonaws.services.simpleemail.*;
import com.amazonaws.auth.PropertiesCredentials;
import java.io.IOException;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import org.apache.log4j.Logger;

public class AWSSendMail {

    private static Logger logger = Logger.getLogger(AWSSendMail.class);

    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        AWSSendMail.ctx = ctx;
    }

    public static boolean sendMailFromAwsVersion2(javax.mail.Message msg) {
        boolean mailSent = false;
        String errorMessage = null;
        try {
            Resource resource = ctx.getResource("classpath:/AwsCredentials.properties");
            InputStream is = resource.getInputStream();
            PropertiesCredentials credentials = new PropertiesCredentials(is);

            // Retrieve the AWS Access Key ID and Secret Key from AwsCredentials.properties.
            credentials.getAWSAccessKeyId();
            credentials.getAWSSecretKey();

//            AmazonSimpleEmailService ses = new AmazonSimpleEmailServiceClient(credentials);
            // Create a Properties object to contain connection configuration information.
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "aws");
            props.setProperty("mail.aws.user", credentials.getAWSAccessKeyId());
            props.setProperty("mail.aws.password", credentials.getAWSSecretKey());

            Session session = Session.getInstance(props);
            try {
                // Reuse one Transport object for sending all your messages
                // for better performance
                Transport t = new AWSJavaMailTransport(session, null);
                t.connect();
                t.sendMessage(msg, null);
                mailSent = true;
                // Close your transport when you're completely done sending all your messages
                t.close();
            } catch (AddressException e) {
                logger.error("Caught an AddressException, which means one or more of your addresses are improperly formatted. "+ e.getMessage());
            } catch (MessagingException e) {
                logger.error("Caught a MessagingException, which means that there was a "
                        + "problem sending your message to Amazon's E-mail Service check the "
                        + "stack trace for more information. "+ e.getMessage());
            }
        } catch (IOException e) {
            errorMessage = e.toString();
            logger.error("IOException in sendMailFromAwsVersion2 cause:: " + e.getMessage());
        } catch (Exception e) {
            errorMessage = e.toString();
            logger.error("Exception in sendMailFromAwsVersion2 cause:: " + e.getMessage());
        }
        if (!mailSent) {
            logger.info("Email not sent and error:: "+errorMessage);
        }
        return mailSent;
    }
}
