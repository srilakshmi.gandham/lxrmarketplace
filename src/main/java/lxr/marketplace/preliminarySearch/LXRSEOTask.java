/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.apiaccess.LxrSeoAPIService;

/**
 *
 * @author sagar
 */
public class LXRSEOTask implements Callable<LXRSEOStats>{
    
    private final String domain;
    private final LxrSeoAPIService lxrSeoAPIService;
    private CountDownLatch latchObj;

    public LXRSEOTask(String domain, LxrSeoAPIService lxrSeoAPIService, CountDownLatch latchObj) {
        this.domain = domain;
        this.lxrSeoAPIService = lxrSeoAPIService;
        this.latchObj = latchObj;
    }
    
    

    @Override
    public LXRSEOStats call() throws Exception {
        LXRSEOStats lxrseoStats = lxrSeoAPIService.getLxrSeoStats(domain);
        latchObj.countDown();
        return lxrseoStats;
        
    }
    
}
