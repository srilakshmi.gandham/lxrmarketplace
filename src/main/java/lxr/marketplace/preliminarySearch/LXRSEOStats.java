/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.util.List;
import java.util.Map;

/**
 *
 * @author sagar
 */
public class LXRSEOStats {

    /*SEO SCORE CARD Metrics*/
    private Map<String, Byte> seoMetrics;
    private List<String> siteErrors;
    private byte siteGraderScore;
    private String redirectedDomain;

    public Map<String, Byte> getSeoMetrics() {
        return seoMetrics;
    }

    public void setSeoMetrics(Map<String, Byte> seoMetrics) {
        this.seoMetrics = seoMetrics;
    }

    public byte getSiteGraderScore() {
        return siteGraderScore;
    }

    public void setSiteGraderScore(byte siteGraderScore) {
        this.siteGraderScore = siteGraderScore;
    }

    public List<String> getSiteErrors() {
        return siteErrors;
    }

    public void setSiteErrors(List<String> siteErrors) {
        this.siteErrors = siteErrors;
    }

    public String getRedirectedDomain() {
        return redirectedDomain;
    }

    public void setRedirectedDomain(String redirectedDomain) {
        this.redirectedDomain = redirectedDomain;
    }
    
    

   

    @Override
    public String toString() {
        return "LXRSEOStats{" + "seoMetrics=" + seoMetrics + ", siteErrors=" + siteErrors + ", siteGraderScore=" + siteGraderScore + '}';
    }

}
