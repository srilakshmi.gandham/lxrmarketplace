/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.text.SimpleDateFormat;
import lxr.marketplace.apiaccess.SimilarWebAPIService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lxr.marketplace.apiaccess.LxrSeoAPIService;
import lxr.marketplace.apiaccess.SimilarWebAPIServiceImpl;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class PreliminarySearchServiceImpl implements PreliminarySearchService {

    public static final Logger LOGGER = Logger.getLogger(PreliminarySearchServiceImpl.class);

    @Autowired
    private SimilarWebAPIService similarWebAPIService;

    @Autowired
    private LxrSeoAPIService lxrSeoAPIService;

    private static SimpleDateFormat viewFormat = new SimpleDateFormat("MMM yy");

    @Override
    public List<PreliminarySearchMetricsModel> processPreliminarySearch(Map<Integer, String> queryDomains) {
        String startDate = null;
        String endDate = null;
        Map<String, String> formatedDateRange = similarWebAPIService.getAPIFormatedDate(similarWebAPIService.getDataAvailableDateRange());
        if (formatedDateRange != null) {
            if (formatedDateRange.containsKey("startDate")) {
                startDate = (String) formatedDateRange.get("startDate");
            }
            if (formatedDateRange.containsKey("endDate")) {
                endDate = (String) formatedDateRange.get("endDate");
            }
        }
        List<PreliminarySearchMetricsModel> preliminaryList = null;
        if (startDate != null && endDate != null) {
            List<PreliminarySearchMetricsModel> tempPreliminaryList = new ArrayList<>();
            String queryDomain = null;
            int count = 0;
            int proxyHostNo = count;
            preliminaryList = new ArrayList<>();
            ExecutorService service = Executors.newFixedThreadPool((queryDomains.size() != 1 ? (queryDomains.size() - 1) : queryDomains.size()));
            CountDownLatch latchObj = new CountDownLatch(queryDomains.size());
            List<Future<PreliminarySearchMetricsModel>> psmaFutureObj = new ArrayList<>();
            for (Map.Entry<Integer, String> domain : queryDomains.entrySet()) {
                queryDomain = Common.getDomainNameFromSite(domain.getValue().trim());
                proxyHostNo = (count % 2 == 0) ? 0 : 1;
                count++;
                psmaFutureObj.add(service.submit(new PreliminarySearchTask(queryDomain, startDate, endDate, domain.getKey(), this.similarWebAPIService, this.lxrSeoAPIService, latchObj, proxyHostNo)));
            }
            try {
                latchObj.await();
            } catch (InterruptedException ex) {
                LOGGER.error("Interrupted latch object count cause:: ", ex);
            }
            try {
                if (!psmaFutureObj.isEmpty()) {
                    psmaFutureObj.forEach((psmaModel) -> {
                        if (psmaModel != null) {
                            try {
                                while (!psmaModel.isDone()) {
                                }
                                tempPreliminaryList.add(psmaModel.get());
                            } catch (InterruptedException | ExecutionException ex) {
                                LOGGER.error("Excpetion in getting from future obj cause:: ", ex);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                LOGGER.error("InterruptedException | ExecutionException when fetching data from API endpoints cause:: ", e);
            }
            preliminaryList.addAll(tempPreliminaryList);
            service.shutdown();
        }
        return preliminaryList;
    }

    public static String getGraphFormatedDate(String apiDate) {
        String formateDate = null;
        Date dateObj = SimilarWebAPIServiceImpl.getDateObject(apiDate);
        if (dateObj != null) {
            formateDate = viewFormat.format(dateObj);
        }
        return formateDate;
    }

    @Override
    public int getAPIBalanceLimitCount() {
        return similarWebAPIService.getAPIBalanceLimitCount();
    }

}
