/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import lxr.marketplace.apiaccess.SimilarWebAPIService;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author sagar
 */
public class DevicePerformanceTask implements Callable<Map<String, Double>> {

    public final String domain;
    public final String startDate;
    public final String endDate;
    public final SimilarWebAPIService similarWebAPIService;
    private CountDownLatch latchObj;
    private int proxyHostNo;

    public DevicePerformanceTask(String domain, String startDate, String endDate, SimilarWebAPIService similarWebAPIService, CountDownLatch latchObj, int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Map<String, Double> call() {
        /*Pull data from API for*/
        Map<String, Double> devicePerformance = similarWebAPIService.getDevicePerformance(this.domain, this.startDate, this.endDate,this.proxyHostNo);
        latchObj.countDown();
        return devicePerformance;
    }

}
