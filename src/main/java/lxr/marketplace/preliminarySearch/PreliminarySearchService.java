/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.util.List;
import java.util.Map;

/**
 *
 * @author sagar
 */
public interface PreliminarySearchService {
    
    public List<PreliminarySearchMetricsModel> processPreliminarySearch(Map<Integer,String> queryDomains);
    public int getAPIBalanceLimitCount();
}
