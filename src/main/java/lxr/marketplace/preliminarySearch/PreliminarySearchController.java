/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.PreSalesToolUsage;
import lxr.marketplace.util.PreSalesToolUsageDAOService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sagar
 */
@Controller
@RequestMapping("/preliminary-search.html")
public class PreliminarySearchController {

    public static final Logger LOGGER = Logger.getLogger(PreliminarySearchController.class);

    @Autowired
    private PreliminarySearchService preliminarySearchService;

    @Autowired
    private PreliminarySearchReportService preliminarySearchReportService;

    @Autowired
    private PreSalesToolUsageDAOService preSalesToolUsageDAOService;

    @Autowired
    private MessageSource messageSource;

    @Value("${PreliminarySearch.SourcePath}")
    private String pptPath;

    /*Returns to tool page*/
    @RequestMapping(method = RequestMethod.GET)
    public String getPreliminarySearchToolPage(Model model) {
        model.addAttribute("apiBalanceCount", preliminarySearchService.getAPIBalanceLimitCount());
        return "/views/preliminarySearch/preliminarySearch";
    }

    @RequestMapping(params = {"request=analyze"}, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object[] validateandProcessPremilinarySearch(@RequestParam("userDomain") String userDomain, @RequestParam("competitorDomains") String competitorDomains,
            @RequestParam("deckLogo") MultipartFile deckLogo, @RequestParam("toolUsageUserEmail") String toolUsageUserEmail,
            HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        Object[] responseObj = new Object[4];
        boolean isMetricsPulled = false;
        String errorMessage = null;
        /*Minimum API calls are required to get data for one domain.*/
        if (preliminarySearchService.getAPIBalanceLimitCount() > 4) {
            /*List of domains*/
            Map<Integer, String> queryDomains = new HashMap<>();
            /*Validating domain*/
            String queryDomain = null;
            if (userDomain != null && !userDomain.trim().isEmpty()) {
                queryDomain = Common.getUrlValidation(userDomain.trim(), session);
                if (queryDomain.equals("invalid") || queryDomain.equals("redirected")) {
                    errorMessage = userDomain;
                } else {
                    queryDomains.put(1, queryDomain);
                }
            }

            int count = 2;
            List<String> compDomainsList = null;
            if ((competitorDomains != null) && !(competitorDomains.equals("undefined"))) {
                compDomainsList = Common.convertStringToArrayList(competitorDomains.trim(), "\n");
            }

            if (compDomainsList != null && !compDomainsList.isEmpty()) {
                for (String domain : compDomainsList) {
                    if (domain != null) {
                        queryDomain = Common.getUrlValidation(domain.trim(), session);
                        if (queryDomain.equals("invalid") || queryDomain.equals("redirected")) {
                            errorMessage = errorMessage != null ? errorMessage + ", " + domain : domain;
                        } else {
                            queryDomains.put(count, queryDomain);
                        }
                    }
                    count++;
                }
            }
            errorMessage = (errorMessage != null && !errorMessage.trim().isEmpty()) ? errorMessage.concat(" " + messageSource.getMessage("MultipleURL.error", null, Locale.US)) : null;
            if (!queryDomains.isEmpty() && errorMessage == null) {
                String savedLogoPath = null;
                if (deckLogo != null && deckLogo.getSize() > 0) {
                    savedLogoPath = Common.saveLogoInDisk(deckLogo, pptPath, Common.createFileName("LXRM_PSMA_Logo"), 200, 80, "png");
                }
                if (savedLogoPath != null) {
                    session.setAttribute("logoName", savedLogoPath);
                    LOGGER.info("Domain Validation is successfully fetching metrics from API, for domain: " + userDomain + ", user email: " + toolUsageUserEmail);
                    List<PreliminarySearchMetricsModel> preliminarySearchList = preliminarySearchService.processPreliminarySearch(queryDomains);
                    if (preliminarySearchList != null) {
                        preliminarySearchList.stream().sorted((modelOne, modelTwo) -> Integer.compare(modelOne.getDomainId(), modelTwo.getDomainId()));
                        String fileName = null;
                        try {
                            fileName = preliminarySearchReportService.generatePreliminarySearchPPT(savedLogoPath, preliminarySearchList);
                        } catch (Exception e) {
                            LOGGER.error("Exception in generating report for domain: " + userDomain + " cause:", e);
                        }
                        if (fileName != null && !fileName.equals("")) {
                            isMetricsPulled = true;
                            /*To track tool usage*/
                            if (toolUsageUserEmail != null && !toolUsageUserEmail.trim().equals("")) {

                                PreSalesToolUsage preSalesToolUsage = new PreSalesToolUsage();
                                preSalesToolUsage.setToolId(45);
                                preSalesToolUsage.setUserEmail(toolUsageUserEmail);
                                preSalesToolUsage.setNumberOfRequests(queryDomains.size());
                                preSalesToolUsage.setCreatedDate(LocalDateTime.now());

                                Map<String, Integer> apiUsageList = new LinkedHashMap<>();
                                /*Adding Similar web api usage*/
                                apiUsageList.put(PreSalesToolUsageDAOService.SIMILARWEB, preliminarySearchList.size());
                                preSalesToolUsage.setApiList(apiUsageList);

                                boolean isToolUsageTracked = preSalesToolUsageDAOService.insertPreSalesToolUsage(preSalesToolUsage);
                                LOGGER.info("Tool usage tracking status::" + isToolUsageTracked + ", for requestCount:: " + queryDomains.size() + ", for responseCount:: " + preliminarySearchList.size());
                            }

                            session.setAttribute("preliminarySearchReportName", fileName);
                            /*Can not send auto mail because attachment size more than 10 mb*/
//                            Common.sendReportThroughMail(request, pptPath, fileName, userOfflineEmail, "Preliminary Search Marketing Analysis");
                        } else {
                            errorMessage = messageSource.getMessage("PreliminarySearch.report.error", null, Locale.US);
                        }
                    } else {
                        errorMessage = messageSource.getMessage("PreliminarySearch.service.error", null, Locale.US);
                    }
                } else {
                    errorMessage = messageSource.getMessage("PreliminarySearch.Image.error", null, Locale.US);
                }
            }
        } else {
            errorMessage = messageSource.getMessage("SimilarWeb.API.error", null, Locale.US);
        }
        responseObj[0] = isMetricsPulled;
        responseObj[1] = errorMessage;
        responseObj[2] = preliminarySearchService.getAPIBalanceLimitCount();
        return responseObj;
    }

    @RequestMapping(params = {"request=download"}, method = RequestMethod.POST)
    public String generatingPreliminarySearchPPTReport(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        if (session.getAttribute("preliminarySearchReportName") != null) {
            LOGGER.info("PSMA File Name:" + ((String) session.getAttribute("preliminarySearchReportName")));
            Common.downloadReport(response, pptPath, ((String) session.getAttribute("preliminarySearchReportName")), "ppt");
            return null;
        }
        return "/views/preliminarySearch/preliminarySearch";
    }

}
