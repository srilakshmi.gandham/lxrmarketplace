/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.util.LinkedHashMap;
import lxr.marketplace.apiaccess.SimilarWebAPIService;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author sagar
 */
public class PaidSearchVistsTask implements Callable<Map<String, Double>> {

    public final String domain;
    public final String startDate;
    public final String endDate;
    public final SimilarWebAPIService similarWebAPIService;
    private CountDownLatch latchObj;
    private int proxyHostNo;

    public PaidSearchVistsTask(String domain, String startDate, String endDate, SimilarWebAPIService similarWebAPIService, CountDownLatch latchObj, int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Map<String, Double> call() {

        Map<String, Double> paidVisits = similarWebAPIService.getPaidSearchVists(this.domain, this.startDate, this.endDate,this.proxyHostNo);
        Map<String, Double> formatedPaidVisits = new LinkedHashMap<>();
        if (paidVisits != null) {
            paidVisits.forEach((key, value) -> {
                formatedPaidVisits.put(PreliminarySearchServiceImpl.getGraphFormatedDate(key), value);
            });
        }
        /*Map<String, Integer> formatedPaidVisits = paidVisits.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,e -> e.getValue().intValue()));
        LOGGER.info("Paid Vists Metrics Value: " + paidVists.values().stream().mapToInt(Integer::intValue).sum());*/
        latchObj.countDown();
        /*Pull data from API for*/
        return paidVisits != null ? formatedPaidVisits : null;
    }

}
