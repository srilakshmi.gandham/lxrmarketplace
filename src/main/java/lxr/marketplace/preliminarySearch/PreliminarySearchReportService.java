/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.util.IOUtils;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFHyperlink;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTable;
import org.apache.poi.xslf.usermodel.XSLFTableCell;
import org.apache.poi.xslf.usermodel.XSLFTableRow;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.googlecode.charts4j.AxisLabels;
import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.AxisStyle;
import com.googlecode.charts4j.AxisTextAlignment;
import com.googlecode.charts4j.BarChart;
import com.googlecode.charts4j.BarChartPlot;
import static com.googlecode.charts4j.Color.BLACK;
import static com.googlecode.charts4j.Color.WHITE;
import com.googlecode.charts4j.Plots;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.DataUtil;
import com.googlecode.charts4j.Line;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.LineStyle;
import com.googlecode.charts4j.Fills;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.LegendPosition;
import com.googlecode.charts4j.Shape;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import javax.imageio.ImageIO;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 *
 * @author srilakshmi
 */
@Service
public class PreliminarySearchReportService {

    @Value("${PreliminarySearch.SourcePath}")
    private String preliminarySearch_filesPath;

    @Value("${PreliminarySearch.Template}")
    private String preliminarySearch_template;
    
    @Autowired
    private MessageSource messageSource;
    
    private XSLFSlide slide;
    private XSLFPictureShape picture;
    private XSLFPictureData pictureData;
    private static final Color GRAPH_SUCCESS_COLOR = new Color(129, 199, 132);
    private static final Color GRAPH_WARNING_COLOR = new Color(255, 193, 0);
    private static final Color GRAPH_DANGER_COLOR = new Color(235, 87, 87);
    private static final Color PROGRESS_BAR_BACKGROUND_COLOR = new Color(195, 195, 195);
    private static final Color SITE_ERROR_BACKGROUND_COLOR = new Color(219, 65, 65);

    private final DecimalFormat DEC_FMT = new DecimalFormat("###,##0.00");
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000L, "K");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static final Logger LOGGER = Logger.getLogger(PreliminarySearchReportService.class);

    public String generatePreliminarySearchPPT(String logoPath, List<PreliminarySearchMetricsModel> prelimSearDataList) {
        String finalFilePath = "";
        String fileName = "";
        LOGGER.info("Result: " + prelimSearDataList);
        String templateFile = preliminarySearch_filesPath + "/template/" + preliminarySearch_template;
        try {
            XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(templateFile));
            List<XSLFSlide> slides = ppt.getSlides();
            
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date = new Date();
            LOGGER.info("Current Date - "+ dateFormat.format(date));
        
            if (!"".equals(logoPath) || logoPath != null) {
                BufferedImage bimg = ImageIO.read(new FileInputStream(preliminarySearch_filesPath + logoPath));
                int width = bimg.getWidth();
                int height = bimg.getHeight();
                byte[] logoPic = IOUtils.toByteArray(new FileInputStream(preliminarySearch_filesPath + logoPath));

//            Logo in Slide 1
                slide = slides.get(0); int count = 0;
                for (XSLFShape shape : slide.getShapes()) {
                if(count == 1) {if (shape instanceof XSLFTextShape) {
                       XSLFTextShape tsh = (XSLFTextShape) shape;
                       XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                       paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                       XSLFTextRun textRun = paragraph.addNewTextRun();
                       textRun.setText(dateFormat.format(date)); // Current Date in Slide
                       textRun.setFontColor(new Color(245,129,32)); //NetElixir Orange
                       textRun.setFontFamily("Century Glothic");
                       textRun.setBold(true);
                       textRun.setFontSize(24.0);
                }}count++;
                }
                pictureData = ppt.addPicture(logoPic, PictureData.PictureType.PNG);
                picture = slide.createPicture(pictureData);
                Rectangle rec1;
                Rectangle rec2;
                if (width > 140) {
                    rec1 = new Rectangle(445, 317, width, height);
                    rec2 = new Rectangle(475, 105, width, height);
                } else {
                    rec1 = new Rectangle(445, 305, width, height);
                    rec2 = new Rectangle(520, 100, width-10, height-10);
                }
//                picture.setAnchor(new Rectangle(550, 390, 250, 80));
                picture.setAnchor(rec1);
//            Logo in Slide 10 to 9
                slide = slides.get(8);
                pictureData = ppt.addPicture(logoPic, PictureData.PictureType.PNG);
                picture = slide.createPicture(pictureData);
//                picture.setAnchor(new Rectangle(670, 120, 180, 50));
                picture.setAnchor(rec2);
            }
//            Method for Generating Graphs
            String barChart = drawBarChart(prelimSearDataList);
            String lineChart = drawLineChart(prelimSearDataList);
//          Graphs in Slide 7 to 6
            slide = slides.get(5);
//            Channel Performance Graph
            byte[] channelPerfPic;
            if (!"".equals(barChart)) {
                channelPerfPic = IOUtils.toByteArray(new FileInputStream(barChart));
                pictureData = ppt.addPicture(channelPerfPic, PictureData.PictureType.PNG);
                picture = slide.createPicture(pictureData);
                picture.setAnchor(new Rectangle(255, 35, 460, 135));
            }
//            Paid Search Graph
            byte[] paidSearchPic;
            if (!"".equals(lineChart)) {
                paidSearchPic = IOUtils.toByteArray(new FileInputStream(lineChart));
                pictureData = ppt.addPicture(paidSearchPic, PictureData.PictureType.PNG);
                picture = slide.createPicture(pictureData);
                picture.setAnchor(new Rectangle(255, 220, 460, 135));
            }
//            Search Traffic by Engines &  Device Performance Tables in Slide 8 to 7
            slide = slides.get(6);
            int countX = 0;
            for (XSLFShape shape : slide.getShapes()) {
                if (shape instanceof XSLFTable) {
                    XSLFTable tsh = (XSLFTable) shape;
                    List<XSLFTableRow> rows = tsh.getRows();
                    if (countX == 4) {
                        for (int i = 0; i < prelimSearDataList.size(); i++) {
                            String domain = "";
                            Map<String, Double> map = null;
                            XSLFTextRun tr1;

                            map = prelimSearDataList.get(i).getSearchTrafficByEngine();
                            domain = prelimSearDataList.get(i).getDomain();
                            List<XSLFTableCell> cells = rows.get(i + 1).getCells();

                            cells.get(0).setFillColor(Color.WHITE);     // Domain
                            tr1 = cells.get(0).appendText(domain, false);
                            tr1.setFontSize(12.0);
                            tr1.setFontColor(new Color(9, 115, 181));

                            cells.get(1).setFillColor(Color.WHITE);     // Google
//                            XSLFTextParagraph paragraph1 = cells.get(1).addNewTextParagraph();
//                            paragraph1.setTextAlign(TextParagraph.TextAlign.CENTER);
//                            XSLFTextRun tr2 = paragraph1.addNewTextRun();
                              XSLFTextRun tr2 = cells.get(1).appendText("", false);
                            if (map == null) {
                                if ("".equals(domain)) {
                                    tr2.setText("");
                                } else {
                                    tr2.setText("    0.00%");
                                }
                            } else if (map.get("Google") == 0 || map.get("Google") == 0.0) {
                                tr2.setText("    0.00%");
                            } else {
                                tr2.setText("    " + DEC_FMT.format(map.get("Google")) + "%");
                            }
                            tr2.setFontSize(12.0);
                            tr2.setFontColor(Color.BLACK);

                            cells.get(2).setFillColor(Color.WHITE);     // Yahoo
//                            XSLFTextParagraph paragraph2 = cells.get(2).addNewTextParagraph();
//                            paragraph2.setTextAlign(TextParagraph.TextAlign.CENTER);
//                            XSLFTextRun tr3 = paragraph2.addNewTextRun();
                            XSLFTextRun tr3 = cells.get(2).appendText("", false);
                            if (map == null) {
                                if ("".equals(domain)) {
                                    tr3.setText("");
                                } else {
                                    tr3.setText("    0.00%");
                                }
                            } else if (map.get("Yahoo") == 0 || map.get("Yahoo") == 0.0) {
                                tr3.setText("    0.00%");
                            } else {
                                tr3.setText("    " + DEC_FMT.format(map.get("Yahoo")) + "%");
                            }
                            tr3.setFontSize(12.0);
                            tr3.setFontColor(Color.BLACK);

                            cells.get(3).setFillColor(Color.WHITE);     // Bing
//                            XSLFTextParagraph paragraph3 = cells.get(3).addNewTextParagraph();
//                            paragraph3.setTextAlign(TextParagraph.TextAlign.CENTER);
//                            XSLFTextRun tr4 = paragraph3.addNewTextRun();
                            XSLFTextRun tr4 = cells.get(3).appendText("", false);
                            if (map == null) {
                                if ("".equals(domain)) {
                                    tr4.setText("");
                                } else {
                                    tr4.setText("    0.00%");
                                }
                            } else if (map.get("Bing") == 0 || map.get("Bing") == 0.0) {
                                tr4.setText("    0.00%");
                            } else {
                                tr4.setText("    " + DEC_FMT.format(map.get("Bing")) + "%");
                            }
                            tr4.setFontSize(12.0);
                            tr4.setFontColor(Color.BLACK);
                        }
                    } else {
                        for (int i = 0; i < prelimSearDataList.size(); i++) {
                            String domain = "";
                            Map<String, Double> map = null;
                            XSLFTextRun tr1;

                            map = prelimSearDataList.get(i).getDevicePerformance();
                            domain = prelimSearDataList.get(i).getDomain();
                            List<XSLFTableCell> cells = rows.get(i + 1).getCells();

                            cells.get(0).setFillColor(Color.WHITE);     // Domain                           
                            tr1 = cells.get(0).appendText(domain, false);

                            tr1.setFontSize(12.0);
                            tr1.setFontColor(new Color(9, 115, 181));

                            cells.get(1).setFillColor(Color.WHITE);     // Desktop                           
//                            XSLFTextParagraph paragraph1 = cells.get(1).addNewTextParagraph();
//                            paragraph1.setTextAlign(TextParagraph.TextAlign.CENTER);
//                            XSLFTextRun tr2 = paragraph1.addNewTextRun();
                            XSLFTextRun tr2 = cells.get(1).appendText("", false);
                            if (map == null) {
                                if ("".equals(domain)) {
                                    tr2.setText("");
                                } else {
                                    tr2.setText("     0.00%");
                                }
                            } else if (map.get("desktop") == 0 || map.get("desktop") == 0.0) {
                                tr2.setText("     0.00%");
                            } else {
                                tr2.setText("     " + DEC_FMT.format(map.get("desktop")) + "%");
                            }
                            tr2.setFontSize(12.0);
                            tr2.setFontColor(Color.BLACK);

                            cells.get(2).setFillColor(Color.WHITE);     // Mobile                          
//                            XSLFTextParagraph paragraph2 = cells.get(2).addNewTextParagraph();
//                            paragraph2.setTextAlign(TextParagraph.TextAlign.CENTER);
//                            XSLFTextRun tr3 = paragraph2.addNewTextRun();
                              XSLFTextRun tr3 = cells.get(2).appendText("     ", false);
                            if (map == null) {
                                if ("".equals(domain)) {
                                    tr3.setText("");
                                } else {
                                    tr3.setText("     0.00%");
                                }
                            } else if (map.get("mobile") == 0 || map.get("mobile") == 0.0) {
                                tr3.setText("     0.00%");
                            } else {
                                tr3.setText("     " + DEC_FMT.format(map.get("mobile")) + "%");
                            }
                            tr3.setFontSize(12.0);
                            tr3.setFontColor(Color.BLACK);
                        }
                    }

                }
                countX++;
            }
            PreliminarySearchMetricsModel model = prelimSearDataList.stream().filter(p -> p.getDomainId() == 1).findAny().get();
            LXRSEOStats stats = model != null ? model.getLxrSEOStats() : null;
//          Progress Bar, SEO Score Card, Site Errors in Slide 9 to 8
            slide = slides.get(7);
            if (stats == null) {
                slide = ppt.removeSlide(7);
            } else {
                int siteGraderScore = stats.getSiteGraderScore();
                String siteRedirectUrl = "";
                if (stats.getRedirectedDomain() != null) {
                    siteRedirectUrl = stats.getRedirectedDomain();
                }
                String winnerImagePath = null;
                String messagePhrase = null;
                Color msgColor;
                int countY = 0;
                if (siteGraderScore < 50) {
                    winnerImagePath = preliminarySearch_filesPath + "/template/" + "sitescore_bad.png";
                    messagePhrase = messageSource.getMessage("LXRSEO.score.bad.desc", null, Locale.US);
                    msgColor = GRAPH_DANGER_COLOR;
                } else if (siteGraderScore >= 50 && siteGraderScore < 70) {
                    winnerImagePath = preliminarySearch_filesPath + "/template/" + "sitescore_average.png";
                    messagePhrase = messageSource.getMessage("LXRSEO.score.average.desc", null, Locale.US);
                    msgColor = GRAPH_WARNING_COLOR;
                } else {
                    winnerImagePath = preliminarySearch_filesPath + "/template/" + "sitescore_good.png";
                    messagePhrase = messageSource.getMessage("LXRSEO.score.good.desc", null, Locale.US);
                    msgColor = GRAPH_SUCCESS_COLOR;
                }
                List<String> siteErrs = stats.getSiteErrors();
                Map<String, Byte> progBarScores = stats.getSeoMetrics();

                byte[] winnerImage = IOUtils.toByteArray(new FileInputStream(winnerImagePath));

                pictureData = ppt.addPicture(winnerImage, PictureData.PictureType.PNG);
                picture = slide.createPicture(pictureData);
                picture.setAnchor(new Rectangle(305, 25, 75, 75));

                for (XSLFShape shape : slide.getShapes()) {
                    if (shape instanceof XSLFTable) {
                        XSLFTable tsh = (XSLFTable) shape;
                        List<XSLFTableRow> rows = tsh.getRows();
                        List<XSLFTableCell> cells = rows.get(0).getCells();

                        if (countY == 5 || countY == 7 || countY == 9 || countY == 11 || countY == 13 || countY == 15 || countY == 17) {

                            cells.get(0).setFillColor(Color.WHITE);
                            XSLFTextRun tr1 = cells.get(0).appendText("", false);
                            tr1.setBold(true);
                            tr1.setFontSize(12.0);
                            tr1.setFontColor(Color.BLACK);
                            tr1.setFontFamily("Century Glothic");

                            cells.get(1).setFillColor(Color.WHITE);
                            XSLFTextRun tr2 = cells.get(1).appendText("", false);
                            tr2.setBold(true);
                            tr2.setFontSize(12.0);
                            tr2.setFontColor(Color.BLACK);
                            tr2.setFontFamily("Century Glothic");
                            String space = "         ";
                            switch (countY) {
                                case 5:
                                    tr1 = cells.get(0).appendText("ON PAGE", false);
                                    if (progBarScores == null || progBarScores.get("ON PAGE") == 0 || progBarScores.get("ON PAGE") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("ON PAGE") + "/100");
                                    }
                                    break;
                                case 7:
                                    tr1 = cells.get(0).appendText("MOBILE", false);
                                    if (progBarScores == null || progBarScores.get("MOBILE") == 0 || progBarScores.get("MOBILE") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("MOBILE") + "/100");
                                    }
                                    break;
                                case 9:
                                    tr1 = cells.get(0).appendText("SPEED", false);
                                    if (progBarScores == null || progBarScores.get("SPEED") == 0 || progBarScores.get("SPEED") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("SPEED") + "/100");
                                    }
                                    break;
                                case 11:
                                    tr1 = cells.get(0).appendText("LINKS", false);
                                    if (progBarScores == null || progBarScores.get("LINKS") == 0 || progBarScores.get("LINKS") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("LINKS") + "/100");
                                    }
                                    break;
                                case 13:
                                    tr1 = cells.get(0).appendText("TECHNICAL", false);
                                    if (progBarScores == null || progBarScores.get("TECHNICAL") == 0 || progBarScores.get("TECHNICAL") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("TECHNICAL") + "/100");
                                    }
                                    break;
                                case 15:
                                    tr1 = cells.get(0).appendText("SOCIAL", false);
                                    if (progBarScores == null || progBarScores.get("SOCIAL") == 0 || progBarScores.get("SOCIAL") == null) {
                                        tr2.setText(" " + space + 0 + "/100");
                                    } else {
                                        tr2.setText(space + progBarScores.get("SOCIAL") + "/100");
                                    }
                                    break;
                                default:
                                    break;
                            }

                        } else if (countY == 6 || countY == 8 || countY == 10 || countY == 12 || countY == 14 || countY == 16) {

                            switch (countY) {
                                case 6:
                                    if (progBarScores == null || progBarScores.get("ON PAGE") == 0 || progBarScores.get("ON PAGE") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("ON PAGE") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("ON PAGE")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("ON PAGE")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }
                                    break;
                                case 8:
                                    if (progBarScores == null || progBarScores.get("MOBILE") == 0 || progBarScores.get("MOBILE") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("MOBILE") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("MOBILE")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("MOBILE")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }
                                    break;
                                case 10:
                                    if (progBarScores == null || progBarScores.get("SPEED") == 0 || progBarScores.get("SPEED") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("SPEED") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("SPEED")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("SPEED")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }
                                    break;
                                case 12:
                                    if (progBarScores == null || progBarScores.get("LINKS") == 0 || progBarScores.get("LINKS") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("LINKS") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("LINKS")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("LINKS")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }

                                    break;
                                case 14:
                                    if (progBarScores == null || progBarScores.get("TECHNICAL") == 0 || progBarScores.get("TECHNICAL") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("TECHNICAL") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("TECHNICAL")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("TECHNICAL")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }
                                    break;
                                case 16:
                                    if (progBarScores == null || progBarScores.get("SOCIAL") == 0 || progBarScores.get("SOCIAL") == null) {
                                        cells.get(0).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    } else {
                                        tsh.setColumnWidth(0, progBarScores.get("SOCIAL") * 2.37);
                                        cells.get(0).setFillColor(getCellColor(progBarScores.get("SOCIAL")));
                                        tsh.setColumnWidth(1, (100 - progBarScores.get("SOCIAL")) * 2.37);
                                        cells.get(1).setFillColor(PROGRESS_BAR_BACKGROUND_COLOR);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    if (shape instanceof XSLFTextShape) {
                        XSLFTextShape tsh = (XSLFTextShape) shape;
                        switch (countY) {
                            case 4: {
                                tsh.addNewTextParagraph();
                                tsh.addNewTextParagraph();
                                tsh.addNewTextParagraph();
                                tsh.addNewTextParagraph();
                                tsh.addNewTextParagraph();
                                tsh.setFillColor(Color.WHITE);
                                XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                                XSLFTextRun textRun = paragraph.addNewTextRun();
                                textRun.setText("SITE SCORE");
                                textRun.setFontFamily("Century Glothic");
                                textRun.setBold(true);
                                textRun.setFontSize(10.2);
                                tsh.addNewTextParagraph();
                                XSLFTextParagraph paragraph2 = tsh.addNewTextParagraph();
                                paragraph2.setTextAlign(TextParagraph.TextAlign.CENTER);
                                XSLFTextRun textRun2 = paragraph2.addNewTextRun();
                                textRun2.setText(siteGraderScore + " / 100");
                                textRun2.setFontFamily("Century Glothic");
                                textRun2.setBold(true);
                                textRun2.setFontSize(15.2);
                                tsh.addNewTextParagraph();
                                XSLFTextParagraph paragraph3 = tsh.addNewTextParagraph();
                                paragraph3.setTextAlign(TextParagraph.TextAlign.CENTER);
                                XSLFTextRun textRun3 = paragraph3.addNewTextRun();
                                textRun3.setText(messagePhrase);
                                textRun3.setFontFamily("Century Glothic");
                                textRun3.setFontColor(msgColor);
                                textRun3.setFontSize(11.2);
                                textRun3.setBold(true);
                                tsh.addNewTextParagraph();
                                XSLFTextParagraph paragraph4 = tsh.addNewTextParagraph();
                                paragraph4.setTextAlign(TextParagraph.TextAlign.CENTER);
                                XSLFTextRun textRun4 = paragraph4.addNewTextRun();
                                textRun4.setText(siteRedirectUrl);
                                textRun4.setFontFamily("Century Glothic");
                                textRun4.setFontSize(10.0);
                                XSLFHyperlink link = textRun4.createHyperlink();
                                link.setAddress(siteRedirectUrl);
                                break;
                            }
                            case 18: { // Potential Errors
                                if (siteErrs == null) {
                                    tsh.addNewTextParagraph();
                                    XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                    paragraph.setTextAlign(TextParagraph.TextAlign.RIGHT);
                                    XSLFTextRun textRun = paragraph.addNewTextRun();
                                    textRun.setText("No Site Errors Found");
                                    textRun.setFontFamily("Century Glothic");
                                    textRun.setFontColor(Color.GREEN);
                                    textRun.setFontSize(10.5);
                                    textRun.setBold(true);
                                    tsh.setFillColor(Color.GREEN);
                                } else if (siteErrs.size() >= 1) {
                                    tsh.addNewTextParagraph();
                                    XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                    paragraph.setTextAlign(TextParagraph.TextAlign.RIGHT);
                                    XSLFTextRun textRun = paragraph.addNewTextRun();
                                    textRun.setText(siteErrs.get(0));
                                    textRun.setFontFamily("Century Glothic");
                                    textRun.setFontColor(Color.WHITE);
                                    textRun.setFontSize(10.5);
                                    tsh.setFillColor(SITE_ERROR_BACKGROUND_COLOR);
                                }
                                break;
                            }
                            case 19: {
                                if (siteErrs != null && siteErrs.size() >= 2) {
                                    tsh.addNewTextParagraph();
                                    XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                    paragraph.setTextAlign(TextParagraph.TextAlign.RIGHT);
                                    XSLFTextRun textRun = paragraph.addNewTextRun();
                                    textRun.setText(siteErrs.get(1));
                                    textRun.setFontFamily("Century Glothic");
                                    textRun.setFontColor(Color.WHITE);
                                    textRun.setFontSize(10.5);
                                    tsh.setFillColor(SITE_ERROR_BACKGROUND_COLOR);
                                }
                                break;
                            }
                            case 20: {
                                if (siteErrs != null && siteErrs.size() >= 3) {
                                    tsh.addNewTextParagraph();
                                    XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                    paragraph.setTextAlign(TextParagraph.TextAlign.RIGHT);
                                    XSLFTextRun textRun = paragraph.addNewTextRun();
                                    textRun.setText(siteErrs.get(2));
                                    textRun.setFontFamily("Century Glothic");
                                    textRun.setFontColor(Color.WHITE);
                                    textRun.setFontSize(10.5);
                                    tsh.setFillColor(SITE_ERROR_BACKGROUND_COLOR);
                                }
                                break;
                            }
                            case 21: {
                                if (siteErrs != null && siteErrs.size() >= 4) {
                                    tsh.addNewTextParagraph();
                                    XSLFTextParagraph paragraph = tsh.addNewTextParagraph();
                                    paragraph.setTextAlign(TextParagraph.TextAlign.RIGHT);
                                    XSLFTextRun textRun = paragraph.addNewTextRun();
                                    textRun.setText(siteErrs.get(3));
                                    textRun.setFontFamily("Century Glothic");
                                    textRun.setFontColor(Color.WHITE);
                                    textRun.setFontSize(10.5);
                                    tsh.setFillColor(SITE_ERROR_BACKGROUND_COLOR);
                                }
                                break;
                            }
                            default:
                                break;
                        }
                    }
                    countY++;
                }
            }
            String reptName = "LXRMarketplace_Preliminary_Search_" + model.getDomain();
            fileName = Common.createFileName(reptName) + ".pptx";
            finalFilePath = preliminarySearch_filesPath + fileName;
            try (//          Save Presentation
                    FileOutputStream out = new FileOutputStream(finalFilePath)) {
                ppt.write(out);
            }
        } catch (IOException ex) {
            LOGGER.error("IOException in generatePreliminarySearchPPT cause: ", ex);
        }
        return fileName;
    }

    public Color getCellColor(int pbScore) {
        if (pbScore < 50) {
            return GRAPH_DANGER_COLOR;
        } else if (pbScore >= 50 && pbScore < 70) {
            return GRAPH_WARNING_COLOR;
        } else {
            return GRAPH_SUCCESS_COLOR;
        }
    }

    public String drawBarChart(List<PreliminarySearchMetricsModel> prelimSearDataList) {
        String compWebiste1 = null;
        String compWebiste2 = null;
        String compWebiste3 = null;
        String compWebiste4 = null;
        String compWebiste5 = null;
        BarChartPlot data1 = null;
        BarChartPlot data2 = null;
        BarChartPlot data3 = null;
        BarChartPlot data4 = null;
        BarChartPlot data5 = null;
        BarChart chart = null;
        String barChartUrl = "";
        List<Double> emptyList = Collections.nCopies(7, 0.0);
        try {
            if (prelimSearDataList.size() >= 1) {
                compWebiste1 = prelimSearDataList.get(0).getDomain();
                if (prelimSearDataList.get(0).getChannelPerformance() == null) {
                    data1 = Plots.newBarChartPlot(Data.newData(new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("3286ba"), compWebiste1);
                } else {
                    data1 = Plots.newBarChartPlot(Data.newData(new ArrayList(prelimSearDataList.get(0).getChannelPerformance().values())), com.googlecode.charts4j.Color.newColor("3286ba"), compWebiste1);
                }
                chart = GCharts.newBarChart(data1);
                chart.setSize(600, 210);
                chart.setBarWidth(20);
                chart.setSpaceBetweenGroupsOfBars(60);
            }
            if (prelimSearDataList.size() >= 2) {
                compWebiste2 = prelimSearDataList.get(1).getDomain();
                if (prelimSearDataList.get(1).getChannelPerformance() == null) {
                    data2 = Plots.newBarChartPlot(Data.newData(new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("42d6d1"), compWebiste2);
                } else {
                    data2 = Plots.newBarChartPlot(Data.newData(new ArrayList(prelimSearDataList.get(1).getChannelPerformance().values())), com.googlecode.charts4j.Color.newColor("42d6d1"), compWebiste2);
                }
                chart = GCharts.newBarChart(data1, data2);
                chart.setSize(600, 210);
                chart.setBarWidth(20);
                chart.setSpaceBetweenGroupsOfBars(35);
            }
            if (prelimSearDataList.size() >= 3) {
                compWebiste3 = prelimSearDataList.get(2).getDomain();
                if (prelimSearDataList.get(2).getChannelPerformance() == null) {
                    data3 = Plots.newBarChartPlot(Data.newData(new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("F9A602"), compWebiste3);
                } else {
                    data3 = Plots.newBarChartPlot(Data.newData(new ArrayList(prelimSearDataList.get(2).getChannelPerformance().values())), com.googlecode.charts4j.Color.newColor("F9A602"), compWebiste3);
                }
                chart = GCharts.newBarChart(data1, data2, data3);
                chart.setSize(600, 210);
                chart.setBarWidth(15);
                chart.setSpaceBetweenGroupsOfBars(25);
            }
            if (prelimSearDataList.size() >= 4) {
                compWebiste4 = prelimSearDataList.get(3).getDomain();
                if (prelimSearDataList.get(3).getChannelPerformance() == null) {
                    data4 = Plots.newBarChartPlot(Data.newData(new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("e04141"), compWebiste4);
                } else {
                    data4 = Plots.newBarChartPlot(Data.newData(new ArrayList(prelimSearDataList.get(3).getChannelPerformance().values())), com.googlecode.charts4j.Color.newColor("e04141"), compWebiste4);
                }
                chart = GCharts.newBarChart(data1, data2, data3, data4);
                chart.setSize(600, 210);
                chart.setBarWidth(11);
                chart.setSpaceBetweenGroupsOfBars(24);
            }
            if (prelimSearDataList.size() >= 5) {
                compWebiste5 = prelimSearDataList.get(4).getDomain();
                if (prelimSearDataList.get(4).getChannelPerformance() == null) {
                    data5 = Plots.newBarChartPlot(Data.newData(new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("388c2b"), compWebiste5);
                } else {
                    data5 = Plots.newBarChartPlot(Data.newData(new ArrayList(prelimSearDataList.get(4).getChannelPerformance().values())), com.googlecode.charts4j.Color.newColor("388c2b"), compWebiste5);
                }
                chart = GCharts.newBarChart(data1, data2, data3, data4, data5);
                chart.setSize(650, 210);
                chart.setBarWidth(10);
                chart.setSpaceBetweenGroupsOfBars(21);
            }
            AxisStyle axisStyleX = AxisStyle.newAxisStyle(BLACK, 10, AxisTextAlignment.CENTER);
            AxisLabels xAxis1 = AxisLabelsFactory.newAxisLabels("Direct", "Email", "Referrals", "Social", "Organic", "Paid", "Display");
            xAxis1.setAxisStyle(axisStyleX);
            AxisLabels xAxis2 = AxisLabelsFactory.newAxisLabels(" ", " ", " ", " ", "Search", "Search", "Ads");
            xAxis2.setAxisStyle(axisStyleX);
            if (chart != null) {
                chart.addXAxisLabels(xAxis1);
                chart.addXAxisLabels(xAxis2);

                AxisStyle axisStyleY = AxisStyle.newAxisStyle(BLACK, 10, AxisTextAlignment.CENTER);
                AxisLabels yAxis = AxisLabelsFactory.newAxisLabels("0%", "50%", "100%");
                yAxis.setAxisStyle(axisStyleY);
                chart.addYAxisLabels(yAxis);
                chart.setTitle("");
                chart.setGrid(100, 50, 3, 2);
                chart.setBackgroundFill(Fills.newSolidFill(WHITE));
                chart.setAreaFill(Fills.newSolidFill(WHITE));
                chart.setLegendPosition(LegendPosition.TOP);

                String chartName = Common.createFileName("LXRM_PSMA_Bar_" + prelimSearDataList.get(0).getDomain()) + ".png";
                barChartUrl = saveGraphasImage(chart.toURLString(), preliminarySearch_filesPath + chartName);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in drawBarChart cause: " + e);
        }
        return barChartUrl;
    }

    public String drawLineChart(List<PreliminarySearchMetricsModel> prelimSearDataList) {
        String compWebiste1 = null;
        String compWebiste2 = null;
        String compWebiste3 = null;
        String compWebiste4 = null;
        String compWebiste5 = null;
        Line data1 = null;
        Line data2 = null;
        Line data3 = null;
        Line data4 = null;
        Line data5 = null;
        LineChart chart = null;
        String lineChartUrl = "";
        Double sum = 0.0;
        Double max = 0.0;
        List<Double> emptyList = Collections.nCopies(12, 0.0);
        try {
            if (prelimSearDataList.size() >= 1) {
                if (prelimSearDataList.get(0).getPaidSearchVisits() == null) {
                    compWebiste1 = prelimSearDataList.get(0).getDomain() + "(0)";
                    data1 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("3286ba"), compWebiste1);
                } else {
                    max = prelimSearDataList.get(0).getPaidSearchVisits().values().stream().mapToDouble(i -> i).max().getAsDouble();
                    sum = prelimSearDataList.get(0).getPaidSearchVisits().values().stream().mapToDouble(i -> i).sum();
                    compWebiste1 = prelimSearDataList.get(0).getDomain() + " (" + convertNumberFormat(sum.longValue()) + ")";
                    if (max == 0.0 || max == 0) {
                        data1 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(prelimSearDataList.get(0).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("3286ba"), compWebiste1);
                    } else {
                        data1 = Plots.newLine(DataUtil.scaleWithinRange(0, getMaxValue(prelimSearDataList), new ArrayList(prelimSearDataList.get(0).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("3286ba"), compWebiste1);
                    }
                }
                data1.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
                data1.addShapeMarkers(Shape.CIRCLE, WHITE, 12);
                data1.addShapeMarkers(Shape.CIRCLE, com.googlecode.charts4j.Color.newColor("3286ba"), 8);
                chart = GCharts.newLineChart(data1);
            }
            if (prelimSearDataList.size() >= 2) {
                if (prelimSearDataList.get(1).getPaidSearchVisits() == null) {
                    compWebiste2 = prelimSearDataList.get(1).getDomain() + "(0)";
                    data2 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("42d6d1"), compWebiste2);
                } else {
                    max = prelimSearDataList.get(1).getPaidSearchVisits().values().stream().mapToDouble(i -> i).max().getAsDouble();
                    sum = prelimSearDataList.get(1).getPaidSearchVisits().values().stream().mapToDouble(i -> i).sum();
                    compWebiste2 = prelimSearDataList.get(1).getDomain() + " (" + convertNumberFormat(sum.longValue()) + ")";
                    if (max == 0.0 || max == 0) {
                        data2 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(prelimSearDataList.get(1).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("42d6d1"), compWebiste2);
                    } else {
                        data2 = Plots.newLine(DataUtil.scaleWithinRange(0, getMaxValue(prelimSearDataList), new ArrayList(prelimSearDataList.get(1).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("42d6d1"), compWebiste2);
                    }
                }
                data2.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
                data2.addShapeMarkers(Shape.CIRCLE, WHITE, 12);
                data2.addShapeMarkers(Shape.CIRCLE, com.googlecode.charts4j.Color.newColor("42d6d1"), 8);
                chart = GCharts.newLineChart(data1, data2);
            }
            if (prelimSearDataList.size() >= 3) {
                if (prelimSearDataList.get(2).getPaidSearchVisits() == null) {
                    compWebiste3 = prelimSearDataList.get(2).getDomain() + "(0)";
                    data3 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("F9A602"), compWebiste3);
                } else {
                    max = prelimSearDataList.get(2).getPaidSearchVisits().values().stream().mapToDouble(i -> i).max().getAsDouble();
                    sum = prelimSearDataList.get(2).getPaidSearchVisits().values().stream().mapToDouble(i -> i).sum();
                    compWebiste3 = prelimSearDataList.get(2).getDomain() + " (" + convertNumberFormat(sum.longValue()) + ")";
                    if (max == 0.0 || max == 0) {
                        data3 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(prelimSearDataList.get(2).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("F9A602"), compWebiste3);
                    } else {
                        data3 = Plots.newLine(DataUtil.scaleWithinRange(0, getMaxValue(prelimSearDataList), new ArrayList(prelimSearDataList.get(2).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("F9A602"), compWebiste3);
                    }
                }
                data3.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
                data3.addShapeMarkers(Shape.CIRCLE, WHITE, 12);
                data3.addShapeMarkers(Shape.CIRCLE, com.googlecode.charts4j.Color.newColor("F9A602"), 8);
                chart = GCharts.newLineChart(data1, data2, data3);
            }
            if (prelimSearDataList.size() >= 4) {
                if (prelimSearDataList.get(3).getPaidSearchVisits() == null) {
                    compWebiste4 = prelimSearDataList.get(3).getDomain() + "(0)";
                    data4 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("e04141"), compWebiste4);
                } else {
                    max = prelimSearDataList.get(3).getPaidSearchVisits().values().stream().mapToDouble(i -> i).max().getAsDouble();
                    sum = prelimSearDataList.get(3).getPaidSearchVisits().values().stream().mapToDouble(i -> i).sum();
                    compWebiste4 = prelimSearDataList.get(3).getDomain() + " (" + convertNumberFormat(sum.longValue()) + ")";
                    if (max == 0.0 || max == 0) {
                        data4 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(prelimSearDataList.get(3).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("e04141"), compWebiste4);
                    } else {
                        data4 = Plots.newLine(DataUtil.scaleWithinRange(0, getMaxValue(prelimSearDataList), new ArrayList(prelimSearDataList.get(3).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("e04141"), compWebiste4);
                    }
                }
                data4.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
                data4.addShapeMarkers(Shape.CIRCLE, WHITE, 12);
                data4.addShapeMarkers(Shape.CIRCLE, com.googlecode.charts4j.Color.newColor("e04141"), 8);
                chart = GCharts.newLineChart(data1, data2, data3, data4);
            }
            if (prelimSearDataList.size() >= 5) {
                if (prelimSearDataList.get(4).getPaidSearchVisits() == null) {
                    compWebiste5 = prelimSearDataList.get(4).getDomain() + "(0)";
                    data5 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(emptyList)), com.googlecode.charts4j.Color.newColor("388c2b"), compWebiste5);
                } else {
                    max = prelimSearDataList.get(4).getPaidSearchVisits().values().stream().mapToDouble(i -> i).max().getAsDouble();
                    sum = prelimSearDataList.get(4).getPaidSearchVisits().values().stream().mapToDouble(i -> i).sum();
                    compWebiste5 = prelimSearDataList.get(4).getDomain() + " (" + convertNumberFormat(sum.longValue()) + ")";
                    if (max == 0.0 || max == 0) {
                        data5 = Plots.newLine(DataUtil.scaleWithinRange(0, 1, new ArrayList(prelimSearDataList.get(4).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("388c2b"), compWebiste5);
                    } else {
                        data5 = Plots.newLine(DataUtil.scaleWithinRange(0, getMaxValue(prelimSearDataList), new ArrayList(prelimSearDataList.get(4).getPaidSearchVisits().values())), com.googlecode.charts4j.Color.newColor("388c2b"), compWebiste5);
                    }
                }
                data5.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
                data5.addShapeMarkers(Shape.CIRCLE, WHITE, 12);
                data5.addShapeMarkers(Shape.CIRCLE, com.googlecode.charts4j.Color.newColor("388c2b"), 8);
                chart = GCharts.newLineChart(data1, data2, data3, data4, data5);
            }
            if (chart != null) {
                chart.setSize(650, 210);
                // Defining background and chart fills. 
                chart.setBackgroundFill(Fills.newSolidFill(WHITE));
                chart.setAreaFill(Fills.newSolidFill(WHITE));
                chart.setLegendPosition(LegendPosition.TOP);
                chart.setTitle("");
                chart.setGrid(100, 50, 3, 2);

                // Defining axis info and styles 
                AxisStyle axisStyle = AxisStyle.newAxisStyle(BLACK, 10, AxisTextAlignment.CENTER);
                String dateRanges = getDateRanges(prelimSearDataList);
                String[] datesArr = dateRanges.substring(1, (dateRanges.length() - 1)).split(",");
                AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(datesArr[0], datesArr[1], datesArr[2], datesArr[3], datesArr[4], datesArr[5], datesArr[6], datesArr[7], datesArr[8], datesArr[9], datesArr[10], datesArr[11]);
                xAxis.setAxisStyle(axisStyle);

                List<Integer> list = divideToEqualParts(getMaxValue(prelimSearDataList));
                String[] labels = getScales(list);
                AxisLabels yAxis = null;
                if (!list.isEmpty()) {
                    yAxis = AxisLabelsFactory.newAxisLabels("", labels[0], labels[1], labels[2], labels[3]);
                } else {
                    yAxis = AxisLabelsFactory.newAxisLabels("0", "50", "100");
                }
                yAxis.setAxisStyle(axisStyle);

                // Adding axis info to chart. 
                chart.addXAxisLabels(xAxis);
                chart.addYAxisLabels(yAxis);

                String chartName = Common.createFileName("LXRM_PSMA_Line_" + prelimSearDataList.get(0).getDomain()) + ".png";
                lineChartUrl = saveGraphasImage(chart.toURLString(), preliminarySearch_filesPath + chartName);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in drawLineChart cause: " + e);
        }
        return lineChartUrl;
    }

    public String getDateRanges(List<PreliminarySearchMetricsModel> prelimSearDataList) {
        List<String> dateRangeList = Collections.nCopies(12, "");
        for (int i = 0; i < prelimSearDataList.size(); i++) {
            if (prelimSearDataList.get(i).getPaidSearchVisits() != null) {
                dateRangeList = new ArrayList(prelimSearDataList.get(i).getPaidSearchVisits().keySet());
            }
        }
        return Arrays.toString(dateRangeList.toArray());
    }

    public int getMaxValue(List<PreliminarySearchMetricsModel> prelimSearDataList) {
        Double max = 0.0;
        List<Double> limit = new ArrayList<>();
        for (int i = 0; i < prelimSearDataList.size(); i++) {
            if (prelimSearDataList.get(i).getPaidSearchVisits() != null) {
                max = prelimSearDataList.get(i).getPaidSearchVisits().values().stream().mapToDouble(p -> p).max().getAsDouble();
            }
            limit.add(max);
        }
        max = limit.stream().mapToDouble(i -> i).max().getAsDouble();
        return roundUp(max.intValue());
    }

    public int roundUp(int src) {
        int len = String.valueOf(src).length() - 1;
        if (len == 0) {
            len = 1;
        }
        int d = (int) Math.pow((double) 10, (double) len);
        return (src + (d - 1)) / d * d;
    }

    public List<Integer> divideToEqualParts(int number) {
        int n = 4;
        List<Integer> list = new ArrayList<>();
        while (number > 0 && n > 0) {
            int a = (int) Math.floor(number / n / 50) * 50;
            number -= a;
            n--;
            list.add(a);
        }
        return list;
    }

    public String convertNumberFormat(long value) {
        if (value == Long.MIN_VALUE) {
            return convertNumberFormat(Long.MIN_VALUE + 1);
        }
        if (value < 0) {
            return "-" + convertNumberFormat(-value);
        }
        if (value < 1000) {
            return Long.toString(value);
        }
        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10);
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public String[] getScales(List<Integer> list) {
        String[] scale = new String[4];
        Long maxValue = 0l;
        if (!list.isEmpty()) {
            maxValue = list.stream().mapToLong(i -> i).max().getAsLong();
        }
        Long newMax = maxValue;
        for (int i = 0; i < 4; i++) {
            scale[i] = convertNumberFormat(newMax * (i + 1));
        }
        return scale;
    }

    public String saveGraphasImage(String imageUrl, String destinationFile) {
        try {
            URL url = new URL(imageUrl);
            OutputStream os;
            try (InputStream is = url.openStream()) {
                os = new FileOutputStream(destinationFile);
                byte[] b = new byte[2048];
                int length;
                while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
                }
            }
            os.close();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
        return destinationFile;
    }
}
