/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import lxr.marketplace.apiaccess.SimilarWebAPIService;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lxr.marketplace.apiaccess.LxrSeoAPIService;
import org.apache.log4j.Logger;

/**
 *
 * @author sagar
 */
public class PreliminarySearchTask implements Callable<PreliminarySearchMetricsModel> {

    public static final Logger LOGGER = Logger.getLogger(PreliminarySearchTask.class);

    private final String domain;
    private final String startDate;
    private final String endDate;
    private final int domainId;
    private final SimilarWebAPIService similarWebAPIService;
    private final LxrSeoAPIService lxrSeoAPIService;
    private CountDownLatch latchObj;
    private int proxyHostNo;

    public PreliminarySearchTask(String domain, String startDate, String endDate,
            int domainId, SimilarWebAPIService similarWebAPIService, LxrSeoAPIService lxrSeoAPIService, CountDownLatch latchObj, int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.domainId = domainId;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.lxrSeoAPIService = lxrSeoAPIService;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public PreliminarySearchMetricsModel call() {
        PreliminarySearchMetricsModel preliminarySearchMetrics = new PreliminarySearchMetricsModel();
        try {

            CountDownLatch apiMetricsLatch = new CountDownLatch((domainId == 1) ? 4 : 3);
            ExecutorService service = Executors.newFixedThreadPool((domainId == 1) ? 3 : 2);

            /*Starting all threads*/
            Future<LXRSEOStats> lxrseoFuture = null;
            if (domainId == 1) {
                /*This thread returns  lxrseo score for domain*/
                lxrseoFuture = service.submit(new LXRSEOTask(this.domain, this.lxrSeoAPIService, apiMetricsLatch));
            }

            /*This thread returns  ChannlePerformance and Search enginee traffic task score for domain*/
            Future<Map<String, Map<String, Double>>> channelAndSearchEngineTrafficFuture = service.submit(new ChannlePerformanceTask(this.domain, this.startDate, this.endDate, this.similarWebAPIService, apiMetricsLatch,this.proxyHostNo));

            /*This thread returns  ChannlePerformanceTask score for domain*/
            Future<Map<String, Double>> devicePerformanceFuture = service.submit(new DevicePerformanceTask(this.domain, this.startDate, this.endDate, this.similarWebAPIService, apiMetricsLatch,this.proxyHostNo));

            /*This thread returns  PaidSearchVistsTask score for domain*/
            Future<Map<String, Double>> paidSearchVistsFuture = service.submit(new PaidSearchVistsTask(this.domain, this.startDate, this.endDate, this.similarWebAPIService, apiMetricsLatch,this.proxyHostNo));

            /*Passing latch object to track count*/
            apiMetricsLatch.await();

            /*Collecting threads results*/
            preliminarySearchMetrics.setDomain(this.domain);
            preliminarySearchMetrics.setDomainId(this.domainId);
            if (domainId == 1) {
                if (lxrseoFuture != null) {
                    preliminarySearchMetrics.setLxrSEOStats(lxrseoFuture.get());
                }
            }
            if (channelAndSearchEngineTrafficFuture != null && channelAndSearchEngineTrafficFuture.isDone()) {
                Map<String, Map<String, Double>> combinedDataMap = channelAndSearchEngineTrafficFuture.get();
                if (combinedDataMap != null && combinedDataMap.containsKey("ChannelOverView")) {
                    preliminarySearchMetrics.setChannelPerformance(combinedDataMap.get("ChannelOverView"));
                }
                if (combinedDataMap != null && combinedDataMap.containsKey("SearchEngineetraffic")) {
                    preliminarySearchMetrics.setSearchTrafficByEngine(combinedDataMap.get("SearchEngineetraffic"));
                }
            }

            if (devicePerformanceFuture != null && devicePerformanceFuture.isDone()) {
                preliminarySearchMetrics.setDevicePerformance(devicePerformanceFuture.get());
            }
            if (paidSearchVistsFuture != null && paidSearchVistsFuture.isDone()) {
                preliminarySearchMetrics.setPaidSearchVisits(paidSearchVistsFuture.get());
            }

            service.shutdown();

        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error("InterruptedException | ExecutionException when fetching data from API endpoints cause: ", e);
        }
        latchObj.countDown();
        return preliminarySearchMetrics;
    }

}
