/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author sagar
 */
public class PreliminarySearchMetricsModel implements Serializable {

    private String domain;
    private int domainId;
    private Map<String, Double> channelPerformance;
    private Map<String, Double> devicePerformance;
    private Map<String, Double> paidSearchVisits;
    private Map<String, Double> searchTrafficByEngine;
//    private boolean userDomain;
    private LXRSEOStats lxrSEOStats;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Map<String, Double> getChannelPerformance() {
        return channelPerformance;
    }

    public void setChannelPerformance(Map<String, Double> channelPerformance) {
        this.channelPerformance = channelPerformance;
    }

    public Map<String, Double> getDevicePerformance() {
        return devicePerformance;
    }

    public void setDevicePerformance(Map<String, Double> devicePerformance) {
        this.devicePerformance = devicePerformance;
    }

    public Map<String, Double> getPaidSearchVisits() {
        return paidSearchVisits;
    }

    public void setPaidSearchVisits(Map<String, Double> paidSearchVisits) {
        this.paidSearchVisits = paidSearchVisits;
    }

    
//
//    public boolean isUserDomain() {
//        return userDomain;
//    }
//
//    public void setUserDomain(boolean userDomain) {
//        this.userDomain = userDomain;
//    }

    public LXRSEOStats getLxrSEOStats() {
        return lxrSEOStats;
    }

    public void setLxrSEOStats(LXRSEOStats lxrSEOStats) {
        this.lxrSEOStats = lxrSEOStats;
    }

    public Map<String, Double> getSearchTrafficByEngine() {
        return searchTrafficByEngine;
    }

    public void setSearchTrafficByEngine(Map<String, Double> searchTrafficByEngine) {
        this.searchTrafficByEngine = searchTrafficByEngine;
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    @Override
    public String toString() {
        return "PreliminarySearchMetricsModel{" + "domain=" + domain + ", domainId=" + domainId + ", channelPerformance=" + channelPerformance + ", devicePerformance=" + devicePerformance + ", paidSearchVisits=" + paidSearchVisits + ", searchTrafficByEngine=" + searchTrafficByEngine + ", lxrSEOStats=" + lxrSEOStats + '}';
    }

}
