/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.preliminarySearch;

import lxr.marketplace.apiaccess.SimilarWebAPIService;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author sagar
 */
public class ChannlePerformanceTask implements Callable<Map<String, Map<String, Double>>> {

    private final String domain;
    private final String startDate;
    private final String endDate;
    private final SimilarWebAPIService similarWebAPIService;
    private CountDownLatch latchObj;
    private final int proxyHostNo;

    public ChannlePerformanceTask(String domain, String startDate, String endDate,
            SimilarWebAPIService similarWebAPIService, CountDownLatch latchObj,int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Map<String, Map<String, Double>> call() {
        Map<String, Map<String, Double>> channelPerformance = similarWebAPIService.getChannlePerformanceAndTrafficShare(this.domain, this.startDate, this.endDate,this.proxyHostNo);
        latchObj.countDown();
        /*Pull data from API for*/
        return channelPerformance;
    }
}
