/**
 *
 * @author NE16T1213
 */
package lxr.marketplace.socialmedia;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import lxr.marketplace.apiaccess.FaceBookMetricsAPIService;
//import lxr.marketplace.apiaccess.InstagramMetricsAPIService;
import lxr.marketplace.apiaccess.TwitterMetricsAPIService;
import lxr.marketplace.apiaccess.YouTubeMetricsAPIService;
import lxr.marketplace.apiaccess.lxrmbeans.FacebookMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.GoogleMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.InstagramMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.TwitterMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.YouTubeMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.PinterestMetrics;
import java.util.concurrent.ConcurrentHashMap;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

public class SocialMediaThread implements Runnable {

    private static Logger logger = Logger.getLogger(SocialMediaThread.class);
    ApplicationContext context = ApplicationContextProvider.getApplicationContext();
    private final int API_THREADS_COUNT = 3;

    private Map<String, Object> socialmediaObjects;
    private ConcurrentHashMap<Long, String> socialThreadMap;/*socialThreadMap: To maintain thread size for a thread coming from controller for all 3 URl's.*/
    private int userNameMethod;
    private SocialCompDomains socialDomains;

    public SocialMediaThread() {
    }

    /*Parmeterized Constructor for creating thread objects*/
    public SocialMediaThread(SocialCompDomains socialDomains, ConcurrentHashMap<Long, String> socialThreadMap, int userNameMethod) {
        this.socialThreadMap = socialThreadMap;
        this.userNameMethod = userNameMethod;
        this.socialDomains = socialDomains;
    }

    public Map<String, Object> getSocialmediaObjects() {
        return socialmediaObjects;
    }

    public void setSocialmediaObjects(Map<String, Object> socialmediaObjects) {
        this.socialmediaObjects = socialmediaObjects;
    }

    public ConcurrentHashMap<Long, String> getConcurrentHashMap() {
        return socialThreadMap;
    }

    public void setConcurrentHashMap(ConcurrentHashMap<Long, String> socialThreadMap) {
        this.socialThreadMap = socialThreadMap;
    }

    @Override
    public void run() {
        try {
            Map<String, Object> socialmediaObjectsTemp = getSocialMediaObjects(socialDomains, userNameMethod);
            if (socialmediaObjectsTemp != null) {
                logger.debug("While setting socialmediaObjectsTemp.size()" + socialmediaObjectsTemp.size());
                setSocialmediaObjects(socialmediaObjectsTemp);
            }
        } catch (Exception e) {
            logger.error("Exception in main Run Method: ", e);
        }
        socialThreadMap.put(socialDomains.getDomainId(), socialDomains.getDomain());
    }

    public Map<String, Object> getSocialMediaObjects(SocialCompDomains socialDomains, int userNameMethod) {
        TwitterMetrics twittermetrics;
        FacebookMetrics facebookMetrics;
        GoogleMetrics googleplusmetrics;
        YouTubeMetrics youtubemetrics;
        InstagramMetrics instagrammetrics;
        PinterestMetrics pinterestMetrics;
        Map<String, Object> socialmedia;
        try {
            Map<String, String> userNames = null;
            if (userNameMethod == 1) {
                userNames = SocialMediaService.getSocialMediaUserNamesfromURL(socialDomains.getDomain());
            } else if (userNameMethod == 2) {
                userNames = new LinkedHashMap<>();
                userNames.put("Twitter", socialDomains.getChannelNames().getTwitterName());
                userNames.put("facebook", socialDomains.getChannelNames().getFacebookName());
                userNames.put("YouTube", socialDomains.getChannelNames().getYoutubeName());
                /*
                userNames.put("Pinterest", socialDomains.getChannelNames().getPinterestName());
                userNames.put("Instagram", socialDomains.getChannelNames().getInstagramName());
                userNames.put("Google", socialDomains.getChannelNames().getGooglePlusName());*/
            }/*End of choice*/

            if (userNames == null) {
                userNames = new LinkedHashMap<>();
            }

            socialmedia = new LinkedHashMap<>();

            String twitterUserName = null, facebookUserName = null, youtubeUserName = null, pinterestUserName = null;
            try {
                if (!((userNames.isEmpty()))) {
                    if ((String) userNames.get("Twitter") != null) {
                        twitterUserName = (String) userNames.get("Twitter");
                    } else {
                        twitterUserName = "NDA";
                    }
                    if ((String) userNames.get("facebook") != null) {
                        facebookUserName = (String) userNames.get("facebook");
                    } else {
                        facebookUserName = "NDA";
                    }
                    if ((String) userNames.get("YouTube") != null) {
                        youtubeUserName = (String) userNames.get("YouTube");
                    } else {
                        youtubeUserName = "NDA";
                    }
                    /*if ((String) userNames.get("Google") != null) {
                        googleUserName = (String) userNames.get("Google");
                    } else {
                        googleUserName = "NDA";
                    }
                    if ((String) userNames.get("Instagram") != null) {
                        instagramUserName = (String) userNames.get("Instagram");
                    } else {
                        instagramUserName = "NDA";
                    }
                    if ((String) userNames.get("Pinterest") != null) {
                        pinterestUserName = (String) userNames.get("Pinterest");
                    } else {
                        pinterestUserName = "NDA";
                    }*/
                } else {
                    twitterUserName = "NDA";
                    facebookUserName = "NDA";
                    youtubeUserName = "NDA";
                    /*pinterestUserName = "NDA";
                    googleUserName = "NDA";
                    instagramUserName = "NDA";*/

                }
            } catch (Exception e) {
                logger.error("Exception in userNames of getSocialMediaObjects: ", e);
            }

            //SocialMediaService socialMediaService=(SocialMediaService)context.getBean("socialMediaService");
            ConcurrentHashMap<Integer, String> concurrentHashMap = new ConcurrentHashMap<>();

            TwitterMetricsAPIService twitterMetricsAPIService = new TwitterMetricsAPIService(twitterUserName, concurrentHashMap);
            FaceBookMetricsAPIService faceBookMetricsAPIService = new FaceBookMetricsAPIService(facebookUserName, concurrentHashMap);
            YouTubeMetricsAPIService youTubeMetricsAPIService = new YouTubeMetricsAPIService(youtubeUserName, concurrentHashMap);
            /*GooglePlusMetricsAPIService googlePlusMetricsAPIService = new GooglePlusMetricsAPIService(googleUserName, concurrentHashMap);
            PinterestMetricsAPIService pinterestMetricsAPIService = new PinterestMetricsAPIService(pinterestUserName, concurrentHashMap);
            InstagramMetricsAPIService instagramMetricsAPIService = new InstagramMetricsAPIService(instagramUserName, concurrentHashMap);*/

            try {

                Thread twitterThread = new Thread(twitterMetricsAPIService);
                Thread facebookThread = new Thread(faceBookMetricsAPIService);
                Thread youtubeThread = new Thread(youTubeMetricsAPIService);
                /*Thread googleThread = new Thread(googlePlusMetricsAPIService);
                Thread instagramThread = new Thread(instagramMetricsAPIService);
                Thread pinterestThread = new Thread(pinterestMetricsAPIService);*/

 /*Creation or Removing of thread, Update API_THREADS_COUNT value*/
                twitterThread.start();
                facebookThread.start();
                youtubeThread.start();
                /*googleThread.start();
                instagramThread.start();
                pinterestThread.start();*/

                try {
                    while (!(concurrentHashMap.size() == API_THREADS_COUNT)) {
                        logger.debug("The size of concurrentHashMap while calling Social Channels API services: " + concurrentHashMap.size());
                        Thread.sleep(3000);
                    }
                } catch (InterruptedException e) {
                    logger.error("Exception in thread sleep", e);
                }

                twittermetrics = twitterMetricsAPIService.getTwitterMetrics();
                facebookMetrics = faceBookMetricsAPIService.getFacebookMetrics();
                youtubemetrics = youTubeMetricsAPIService.getYouTubeMetrics();
                pinterestMetrics = new PinterestMetrics();
                googleplusmetrics = new GoogleMetrics();
                instagrammetrics = new InstagramMetrics();
                /*googleplusmetrics = googlePlusMetricsAPIService.getGoogleMetrics();
                instagrammetrics = instagramMetricsAPIService.getInstagramMetrics();*/

 /*Note: Change in Key name should modify while fetching from map*/
                socialmedia.put("twittermetrics", twittermetrics);
                socialmedia.put("facebookmetrics", facebookMetrics);
                socialmedia.put("googleplusmetrics", googleplusmetrics);
                socialmedia.put("youtubemetrics", youtubemetrics);
                socialmedia.put("instagrammetrics", instagrammetrics);
                socialmedia.put("pinterestmetrics", pinterestMetrics);
//                return socialmedia;
            } catch (Exception e) {
                socialmedia = new HashMap<>();
                logger.error("Exception for thread  start and creation: ", e);
            }
            //return socialmedia;

        } catch (Exception e) {
            socialmedia = new HashMap<>();
            logger.error("Exception  when creating Social Media Objects: ", e);
        }
        return socialmedia;
    }
}
