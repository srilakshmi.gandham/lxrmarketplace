package lxr.marketplace.socialmedia;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class SocialCompDomains implements Serializable {
        private long domainId;
	private String domain;
	private boolean userDomain;
        private boolean isLoggedIn;
        private boolean isDeleted;
        private boolean unSubScribeStatus;
        private boolean weeklyReport;
        private Timestamp domainRegDate;
        private ChannelUserNames channelNames;
        private Date maximumDate;
        private boolean growthDataAvailable;

    public long getDomainId() {
        return domainId;
    }

    public void setDomainId(long domainId) {
        this.domainId = domainId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isUserDomain() {
        return userDomain;
    }

    public void setUserDomain(boolean userDomain) {
        this.userDomain = userDomain;
    }

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Timestamp getDomainRegDate() {
        return domainRegDate;
    }

    public void setDomainRegDate(Timestamp domainRegDate) {
        this.domainRegDate = domainRegDate;
    }

    public boolean isUnSubScribeStatus() {
        return unSubScribeStatus;
    }

    public void setUnSubScribeStatus(boolean unSubScribeStatus) {
        this.unSubScribeStatus = unSubScribeStatus;
    }

    public boolean isWeeklyReport() {
        return weeklyReport;
    }

    public void setWeeklyReport(boolean weeklyReport) {
        this.weeklyReport = weeklyReport;
    }

    public ChannelUserNames getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(ChannelUserNames channelNames) {
        this.channelNames = channelNames;
    }

    public Date getMaximumDate() {
        return maximumDate;
    }

    public void setMaximumDate(Date maximumDate) {
        this.maximumDate = maximumDate;
    }


    public boolean isGrowthDataAvailable() {
        return growthDataAvailable;
    }

    public void setGrowthDataAvailable(boolean growthDataAvailable) {
        this.growthDataAvailable = growthDataAvailable;
    }

   
        
	
}
