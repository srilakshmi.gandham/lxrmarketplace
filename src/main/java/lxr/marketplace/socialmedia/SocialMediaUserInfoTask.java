package lxr.marketplace.socialmedia;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

public class SocialMediaUserInfoTask implements Runnable {

    private static Logger logger = Logger.getLogger(SocialMediaUserInfoTask.class);

    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.ctx = ctx;
    }

    private final String email;
    private final String userName;
    private final long lxrmUserId;
    private final List<SocialCompDomains> socialMediaDomains;
    private boolean weeklyReportTemp = false;
    private final SocialMediaService socialMediaService;

    public SocialMediaUserInfoTask(String email, String userName, long lxrmUserId, List<SocialCompDomains> socialMediaDomains, SocialMediaService socialMediaService) {
        this.email = email;
        this.userName = userName;
        this.socialMediaDomains = socialMediaDomains;
        this.lxrmUserId = lxrmUserId;
        this.socialMediaService = socialMediaService;
    }

    @Override
    public void run() {
        try {
            String downloadFolder = (String) ctx.getBean("downloadFolder");
            Timestamp curdate = new Timestamp(Calendar.getInstance().getTimeInMillis());
            String currentDate = (new SimpleDateFormat("yyyy-MM-dd")).format(curdate);
            SimpleDateFormat forMailsDt = new SimpleDateFormat("MMMM dd, yyyy");
            SimpleDateFormat forDBDate = new SimpleDateFormat("yyyy-MM-dd");
            logger.info("Schedular started for social media domain Id : " + ((socialMediaDomains != null && !socialMediaDomains.isEmpty()) ? socialMediaDomains.get(0).getDomainId() : null) + ", user Email: " + email);
            if (socialMediaDomains != null) {
                List<SocialCompDomains> finalSchedularUrls = new ArrayList<>();
                String presentDate = forDBDate.format(Calendar.getInstance().getTime());
                for (SocialCompDomains dbExistedURL : socialMediaDomains) {
                    if (!(presentDate.equals(Common.convertTimeStampintoDate(dbExistedURL.getDomainRegDate())))) {
                        /*Checking the data is available for a given url at schedular time if no data is avaliable */
                        finalSchedularUrls.add(dbExistedURL);
                    }
                }
                if (finalSchedularUrls.size() >= 4) {
                    finalSchedularUrls = finalSchedularUrls.subList(0, 3);
                }
                /*Fetching the data for domains.*/
                socialMediaService.processSocialMediaAnalyzer(finalSchedularUrls, 2);

                Timestamp startDateInDb = null;
                for (SocialCompDomains sd : socialMediaDomains) {
                    if (sd.isUserDomain() && sd.isWeeklyReport() && (!sd.isUnSubScribeStatus())) {
                        weeklyReportTemp = sd.isWeeklyReport();
                        startDateInDb = sd.getDomainRegDate();
                    }
                }
                if (weeklyReportTemp == Boolean.TRUE) {
                    SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String tempCurDate = dtf.format(curdate);
                    String tempStartDt = dtf.format(startDateInDb);
                    Date startDateOne = dtf.parse(tempStartDt);
                    Date endDateOne = dtf.parse(tempCurDate);
                    long diff = socialMediaService.getDateDiff(startDateOne, endDateOne);
                    if (diff % 7 == 0) {
                        Timestamp endDate = socialMediaService.subtractDays(curdate, 1);
                        Timestamp startDate = socialMediaService.subtractDays(endDate, 6);
                        String toDate = forDBDate.format(endDate);
                        String fromDate = forDBDate.format(startDate);
                        /*Checking data is existed or not before sending PDF report.*/
                        SocialCompDomains compDomains = socialMediaDomains.stream().filter(sd -> sd.isUserDomain() && sd.isWeeklyReport()).collect(Collectors.toList()).get(0);
                        if (socialMediaService.checkDataExistedForDate(compDomains.getDomainId(), toDate)
                                && socialMediaService.checkDataExistedForDate(compDomains.getDomainId(), fromDate)) {
                            logger.info("Weekly report existed from: " + fromDate + ", toDate: " + toDate);
                            List<Map<String, Object>> allMediaObjectSchedular = new ArrayList<>();
                            Map<String, Object> socialmediaObjects;
                            boolean growthDataStatus = false;
                            Date date = null;
                            for (SocialCompDomains sd : socialMediaDomains) {
                                socialmediaObjects = null;
                                String domainRegDate = Common.convertTimeStampintoDate(sd.getDomainRegDate());
                                date = socialMediaService.getDateForSocialMediaUser(sd.getDomainId());
                                sd.setMaximumDate(date);
                                growthDataStatus = socialMediaService.checkGrwothDataAvailablity(date, sd.getDomainId());
                                sd.setGrowthDataAvailable(growthDataStatus);
                                if (!(domainRegDate.equals(currentDate))) {
                                    socialmediaObjects = socialMediaService.getSocialAnalysis(sd.getDomainId(), toDate, fromDate);
                                    /* socialmediaObjects =socialMediaService.getSocialAnalysis(sd.getDomainId(),socialMediaService.convertDateToString(date, 1),
                                                                                              socialMediaService.getPreviousDate(date, 1));*/
                                    if (socialmediaObjects != null) {
                                        allMediaObjectSchedular.add(socialmediaObjects);
                                    }
                                }
                            }

                            String todayDate = forMailsDt.format(endDate);
                            String seventhDate = forMailsDt.format(startDate);
                            if (!(allMediaObjectSchedular.isEmpty()) && (email != null && !email.trim().isEmpty()) && (userName != null && !userName.trim().isEmpty()) && (seventhDate != null && todayDate != null)) {
                                String dwnfileName = socialMediaService.generatePdfForSocialMediaAnalyzer(downloadFolder, allMediaObjectSchedular, socialMediaDomains, todayDate, seventhDate, lxrmUserId);
                                logger.debug("Download file name  for socail media analyzer weekly report pdf:  " + dwnfileName + ", SendMail to User Email: " + email + ", for user domain: " + socialMediaDomains.get(0).getDomain() + ", AllMediaObjectSchedular size:" + allMediaObjectSchedular.size() + ", userId: " + lxrmUserId);
//                                String waterMarkFile = socialMediaService.addWaterMark(dwnfileName, downloadFolder, lxrmUserId);
                                if (dwnfileName != null) {
                                    Common.sendWeeklyReportThroughMail(downloadFolder, dwnfileName, email, "Social Media Analyzer Weekly", userName, seventhDate, todayDate);
                                }
                            } else {
                                logger.info("Fail to generate socail media analyzer weekly report for User Email: " + email + ", user Id:" + lxrmUserId + ", user domain: " + ((socialMediaDomains != null && !socialMediaDomains.isEmpty()) ? socialMediaDomains.get(0).getDomain() : null) + ", AllMediaObjectSchedular size:" + allMediaObjectSchedular.size());
                            }
                        }
                    }
                }
            }
        } catch (BeansException | ParseException e) {
            logger.error("BeansException| ParseException in run cause:", e);
        }
    }
}
