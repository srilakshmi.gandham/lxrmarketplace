package lxr.marketplace.socialmedia;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.lxrmbeans.FacebookMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.TwitterMetrics;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.home.HomePageService;

import lxr.marketplace.session.Session;
import lxr.marketplace.session.ToolUsage;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/social-media-analyzer")
public class SocialMediaToolController {

    private static final Logger logger = Logger.getLogger(SocialMediaToolController.class);

    @Autowired
    private SocialMediaService socialMediaService;
    @Autowired
    private String downloadFolder;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private HomePageService homePageService;
    JSONObject userInputJson = null;
    private final SimpleDateFormat socialDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    private MessageSource messageSource;

//    "view/socialmediaanalyzer/socialMediaAnalyzerTool";
    private final String formView = "views/socialMediaAnalyzer/socialMediaAnalyzer";

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("socialMediaTool") SocialMedia socialMediaTool, ModelMap model, HttpSession session) throws JSONException {

        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        Map<String, Object> socialmediaObjects = null;
        Long userIdInSMT = null;
        long lxrmUserId = 0;
        long userIdInSocialUser = 0;
        Session userSession = null;
        userSession = (Session) session.getAttribute("userSession");
        List<SocialCompDomains> socialDomain = null;
        boolean isMainUnsubscribed = false;
        boolean isLogged = false;
        String ateParam = null;
        session.removeAttribute("socialDomain");
        if (userSession != null && userSession.getUserId() > 0) {
            lxrmUserId = userSession.getUserId();
        } else if (user != null && user.getId() > 0) {
            lxrmUserId = user.getId();
        }
        if (userLoggedIn && lxrmUserId > 0) {
            socialDomain = socialMediaService.createSocialCompDomainRows(lxrmUserId, userLoggedIn, false);
            /*While making guest user is auto login, updating the social media user id as logged in. */
            if (socialDomain != null) {
                if (socialDomain.isEmpty() && request.getParameterMap().containsKey("backToSuccessPage")) {
                    ateParam = request.getParameter("backToSuccessPage");
                    if (ateParam != null && ateParam.equalsIgnoreCase("'backToSuccessPage'")) {
                        socialDomain = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), false, false);
                        socialDomain.stream().forEach(socialUser -> socialUser.setIsLoggedIn(true));
                        socialMediaService.updateSocialGuestEntryToLoggedIn(socialDomain, user.getId());
                    }
                }
                for (SocialCompDomains sm : socialDomain) {
                    if ((sm.isUserDomain()) && (!(sm.isIsDeleted())) && ((sm.isIsLoggedIn()))) {
                        userIdInSMT = sm.getDomainId();
                        isMainUnsubscribed = sm.isUnSubScribeStatus();
                        isLogged = sm.isIsLoggedIn();
                    }
                }
            }
            if (userIdInSMT != null) {
                userIdInSocialUser = userIdInSMT;
            }
        }
        model.addAttribute("userIdInSocialUser", userIdInSocialUser);
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(29);
            toolObj.setToolName(Common.SOCIAL_MEDIA_ANALYZER);
            toolObj.setToolLink("social-media-analyzer.html");
            session.setAttribute("toolObj", toolObj);
            Map<Integer, ToolUsage> toolUsageInfo = (Map<Integer, ToolUsage>) userSession.getToolUsage();

            if (WebUtils.hasSubmitParameter(request, "getResult") && WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                session.setAttribute("loginrefresh", "loginrefresh");
                if (userLoggedIn) {
                    if (userIdInSocialUser == 0) {
                        List<SocialCompDomains> socialCompDomains = (List<SocialCompDomains>) session.getAttribute("socialGuestDomains");
                        if (socialCompDomains != null) {
                            socialMediaService.updateSocialGuestEntryToLoggedIn(socialCompDomains, user.getId());
                            session.removeAttribute("socialUserId");
                        } else {
                            logger.info("No Guest user rank id found in login refresh.");
                        }
                        return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                    } else if (isMainUnsubscribed) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        socialmediaObjects = new HashMap<>();
                        session.setAttribute("socialmediaObjects", socialmediaObjects);
                        model.addAttribute("unsubscribeset", true);
                        model.addAttribute("callAjax", false);
                        model.addAttribute("getSuccess", false);
                        socialMediaTool.setToolUnsubscription(true);
                        socialMediaTool.setWeeklyReport(true);
                        compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                            socialMediaTool.setMediaUrl(cDSR.getDomain());
                        });
                        model.addAttribute("messageToUser", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsubscribe option.");
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    } else {
                        return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                    }
                }
            } else if (userLoggedIn && (userIdInSocialUser > 0 || (toolUsageInfo != null && toolUsageInfo.get(toolObj.getToolId()) != null))
                    && !(WebUtils.hasSubmitParameter(request, "user-id") && WebUtils.hasSubmitParameter(request, "tracking-id"))) {
                if ((isLogged) && (userIdInSocialUser > 0)) {
                    if (isMainUnsubscribed) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        socialmediaObjects = new HashMap<>();
                        session.setAttribute("socialmediaObjects", socialmediaObjects);
                        model.addAttribute("unsubscribeset", true);
                        model.addAttribute("getSuccess", false);
                        model.addAttribute("callAjax", false);
                        socialMediaTool.setToolUnsubscription(true);
                        compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                            socialMediaTool.setMediaUrl(cDSR.getDomain());
                        });
                        socialMediaTool.setWeeklyReport(true);
                        session.setAttribute("socialMediaTool", socialMediaTool);
                        model.addAttribute("messageToUser", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsubscribe option.");
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    }
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                } else {
                    model.addAttribute("callAjax", false);
                    model.addAttribute("unsubscribeset", true);
                    model.addAttribute("getSuccess", false);
                    return formView;
                }
            } else if ((toolUsageInfo == null || (toolUsageInfo.get(29) == null))
                    && (!WebUtils.hasSubmitParameter(request, "user-id") && !WebUtils.hasSubmitParameter(request, "tracking-id"))) {
                if ((userLoggedIn) && (userIdInSocialUser > 0)) {
                    if (isMainUnsubscribed) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        socialmediaObjects = new HashMap<>();
                        session.setAttribute("socialmediaObjects", socialmediaObjects);
                        model.addAttribute("unsubscribeset", true);
                        model.addAttribute("getSuccess", false);
                        model.addAttribute("callAjax", false);
                        socialMediaTool.setToolUnsubscription(true);
                        socialMediaTool.setWeeklyReport(true);
                        socialMediaTool.setToolUnsubscription(true);
                        compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                            socialMediaTool.setMediaUrl(cDSR.getDomain());
                        });
                        session.setAttribute("socialMediaTool", socialMediaTool);
                        model.addAttribute("messageToUser", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsubscribe option.");
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    }
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                }
                model.addAttribute("callAjax", false);
                model.addAttribute("unsubscribeset", false);
                model.addAttribute("getSuccess", false);
                session.removeAttribute("socialMediaTool");
                session.removeAttribute("toolInValidList");
                return formView;
            }
        }
        model.addAttribute("callAjax", false);
        model.addAttribute("unsubscribeset", false);
        model.addAttribute("getSuccess", false);
        session.removeAttribute("socialMediaTool");
        session.removeAttribute("toolInValidList");
        return formView;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, HttpServletResponse response, ModelMap model,
            @ModelAttribute("socialMediaTool") SocialMedia socialMediaTool,
            BindingResult result, HttpSession session, @RequestParam(defaultValue = "") String query) throws JSONException {

        Login user = (Login) session.getAttribute("user");
        session = request.getSession();
        Map<String, Object> socialmediaObjects = null;

        if (socialMediaTool != null) {
            session.setAttribute("socialMediaTool", socialMediaTool);
        }

        if (session.getAttribute("requestFromMail") != null && ((session.getAttribute("user-id") != null
                && session.getAttribute("tracking-id") != null) || (session.getAttribute("question-id") != null))) {
            SqlRowSet sqlRowSet = null;
            if (session.getAttribute("user-id") != null && session.getAttribute("tracking-id") != null) {
                long decriptedUserId;
                long decriptedWebsiteTrackingId;
                decriptedUserId = (Long) session.getAttribute("user-id");
                decriptedWebsiteTrackingId = (Long) session.getAttribute("tracking-id");
                sqlRowSet = homePageService.checkUserIdAndTrackingId(decriptedUserId, decriptedWebsiteTrackingId);
            } else if (session.getAttribute("question-id") != null) {
                long questionId = (long) session.getAttribute("question-id");
                sqlRowSet = homePageService.checkQuestionId(questionId);
            }

            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    String userInput = sqlRowSet.getString("other_inputs");
                    JSONObject userInputJsonObj = new JSONObject(userInput);
                    socialMediaTool.setWeeklyReport((Boolean) userInputJsonObj.get("2"));
                    socialMediaTool.setToolUnsubscription((Boolean) userInputJsonObj.get("3"));
                    SocialCompDomains socialCompDomains = null;
                    List<SocialCompDomains> socialCompDomainsList = new LinkedList<>();

                    for (int i = 4; i < userInputJsonObj.length() - 2; i++) {
                        if (userInputJsonObj.has("domain" + Integer.toString(i))) {
                            socialCompDomains = new SocialCompDomains();
                            if (userInputJsonObj.get("domain" + Integer.toString(i)) != "") {
                                socialCompDomains.setDomain(userInputJsonObj.get("domain" + Integer.toString(i)).toString());
                                socialCompDomains.setDomainId((Integer) userInputJsonObj.get("dId" + Integer.toString(i)));
                                socialCompDomains.setUserDomain((Boolean) userInputJsonObj.get("isUserDomain" + Integer.toString(i)));
                                socialCompDomains.setWeeklyReport(socialMediaTool.isWeeklyReport());
                                if (userInputJsonObj.has("domainRegDate" + Integer.toString(i)) && userInputJsonObj.get("domainRegDate" + Integer.toString(i)).toString() != null
                                        && userInputJsonObj.get("domainRegDate" + Integer.toString(i)).toString().equals("")) {
                                    Timestamp domainRegDate = Timestamp.valueOf(userInputJsonObj.get("" + Integer.toString(i)).toString());
                                    socialCompDomains.setDomainRegDate(domainRegDate);
                                }
                                if (userInputJsonObj.has("maximumDate" + Integer.toString(i)) && userInputJsonObj.get("maximumDate" + Integer.toString(i)).toString() != null
                                        && userInputJsonObj.get("maximumDate" + Integer.toString(i)).toString().equals("")) {
                                    Timestamp maximumDate = Timestamp.valueOf(userInputJsonObj.get("" + Integer.toString(i)).toString());
                                    socialCompDomains.setMaximumDate(maximumDate);
                                }
                                socialCompDomains.setUnSubScribeStatus((Boolean) userInputJsonObj.get("isUnSubScribe" + Integer.toString(i)));
                                socialCompDomains.setGrowthDataAvailable((Boolean) userInputJsonObj.get("isGrowthDataAvailable" + Integer.toString(i)));
                                socialCompDomains.setIsLoggedIn(true);
                                socialCompDomains.setIsDeleted((Boolean) userInputJsonObj.get("isDeleted" + Integer.toString(i)));
                                ChannelUserNames channelUserNames = new ChannelUserNames();
                                if (userInputJsonObj.has("channelRow" + Integer.toString(i))) {
                                    channelUserNames.setChannelRowId((Long) userInputJsonObj.get("domain" + Integer.toString(i)));
                                    channelUserNames.setSocialMediaUserId((Long) userInputJsonObj.get("cSocialMediaUserId" + Integer.toString(i)));
                                    channelUserNames.setTwitterName(userInputJsonObj.get("cTN" + Integer.toString(i)).toString());
                                    channelUserNames.setFacebookName(userInputJsonObj.get("cFN" + Integer.toString(i)).toString());
//                                    channelUserNames.setInstagramName(userInputJsonObj.get("cIN" + Integer.toString(i)).toString());
                                    channelUserNames.setPinterestName(userInputJsonObj.get("cPN" + Integer.toString(i)).toString());
                                    channelUserNames.setYoutubeName(userInputJsonObj.get("cYN" + Integer.toString(i)).toString());
//                                    channelUserNames.setGooglePlusName(userInputJsonObj.get("cGN" + Integer.toString(i)).toString());
                                }
                                socialCompDomains.setChannelNames(channelUserNames);
                            } else {
                                socialCompDomains.setDomain("");
                            }
                            socialCompDomainsList.add(socialCompDomains);
                        } else {
                            break;
                        }
                    }
                    socialMediaTool.setCompDomainSettingRows(socialCompDomainsList);
                }
            }
        }
        session.removeAttribute("toolInValidList");
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(29);
            toolObj.setToolName("Social Media");
            toolObj.setToolLink("social-media-analyzer.html");
            session.setAttribute("toolObj", toolObj);
            boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            long userIdInSocialUser = 0;
            Session userSession = null;
            userSession = (Session) session.getAttribute("userSession");
            Map<Integer, ToolUsage> toolUsageInfo = null;
            session.setAttribute("currentTool", Common.SOCIAL_MEDIA_ANALYZER);
            List<SocialCompDomains> socialDomain = null;
            boolean isMainUnsubscribed = false;
            boolean isLogged = false;
            long lxrmUserId = 0;
            if (userLoggedIn) {
                lxrmUserId = user.getId();
            } else if (userSession != null && userSession.getUserId() > 0) {
                lxrmUserId = userSession.getUserId();
            }
            if (userSession != null) {
                toolUsageInfo = (Map<Integer, ToolUsage>) userSession.getToolUsage();
            }
            if (userLoggedIn) {
                socialDomain = socialMediaService.createSocialCompDomainRows(lxrmUserId, userLoggedIn, false);
                if (socialDomain != null) {
                    for (SocialCompDomains sm : socialDomain) {
                        if ((sm.isUserDomain())) {
                            userIdInSocialUser = sm.getDomainId();
                            isMainUnsubscribed = sm.isUnSubScribeStatus();
                            isLogged = sm.isIsLoggedIn();
                        }
                    }
                }
            }
            model.addAttribute("userIdInSocialUser", userIdInSocialUser);
            if (WebUtils.hasSubmitParameter(request, "download")) {
                try {
                    String reportType = null;
                    String toAddress = null;
                    if (request.getParameter("download").equalsIgnoreCase("sendmail")) {
                        /*Considering toAddress as SendMail request*/
                        toAddress = request.getParameter("email");
                        /*Storing file name in report type as overriding the request type value*/
                        reportType = getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, toAddress);
                        Common.sendReportThroughMail(request, downloadFolder, reportType, toAddress, Common.SOCIAL_MEDIA_ANALYZER);
                        return null;
                    } else if (request.getParameter("download").equalsIgnoreCase("download")) {
                        reportType = request.getParameter("download");
                        return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, reportType);
                    }
                } catch (Exception e) {
                    logger.error("Exception to handle download report request", e);
                }
            } else if (WebUtils.hasSubmitParameter(request, "getResult") && ((userLoggedIn && userIdInSocialUser == 0) || !userLoggedIn)) {
                String mediaUrl = null;
                if (user.getId() < 0) {
                    //convert the user to guest user on social  usage.
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    user = (Login) session.getAttribute("user");
                }

                Date date = Calendar.getInstance().getTime();
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    if (userLoggedIn) {
                        if (userIdInSocialUser == 0) {
                            List<SocialCompDomains> socialCompDomains = (List<SocialCompDomains>) session.getAttribute("socialGuestDomains");
                            if (socialCompDomains != null) {
                                socialMediaService.updateSocialGuestEntryToLoggedIn(socialCompDomains, user.getId());
                                session.removeAttribute("socialUserId");
                            } else {
                                logger.info("No Guest user rank id found in login refresh.");
                            }
                            return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                        } else if (isMainUnsubscribed) {
                            List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                            socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                            socialmediaObjects = new HashMap<>();
                            session.setAttribute("socialmediaObjects", socialmediaObjects);
                            model.addAttribute("unsubscribeset", true);
                            model.addAttribute("getSuccess", false);
                            model.addAttribute("callAjax", false);
                            socialMediaTool.setToolUnsubscription(true);
                            socialMediaTool.setWeeklyReport(true);
                            socialMediaTool.setToolUnsubscription(true);
                            compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                                socialMediaTool.setMediaUrl(cDSR.getDomain());
                            });
                            session.setAttribute("socialMediaTool", socialMediaTool);
                            model.addAttribute("messageToUser", "You have unsubscribed from the tool. To re-subscribe please uncheck the unsubscribe option.");
                            response.setCharacterEncoding("UTF-8");
                            return formView;

                        } else {
                            return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                        }
                    }
                }/*End of Login Refresh*/

                if ((toolUsageInfo != null && toolUsageInfo.get(29) != null)
                        || (toolUsageInfo == null || (toolUsageInfo.get(29) == null))) {
                    if (userLoggedIn) {
                        socialMediaService.deletePreviousSocialUserdata(user.getId(), userLoggedIn);
                    } else {
                        socialMediaService.deletePreviousSocialUserdata(userSession.getUserId(), userLoggedIn);
                    }
                }
                if (socialMediaTool.getMediaUrl() != null) {
                    mediaUrl = Common.getUrlValidation(socialMediaTool.getMediaUrl().trim(), session);
//                    mediaUrl = SocialMediaService.getUrlValidation(socialMediaTool.getMediaUrl().trim());
                } else {
                    mediaUrl = "invalid";
                }
                session.removeAttribute("toolInValidList");
                List<String> socialURL = new LinkedList<>();
                Map<Integer, String> inValidUrls = new HashMap<>();
                boolean isInvalidURLExistes = false;
                String inValidMessage = " ";
                if ((mediaUrl != null) && ((mediaUrl.equals("invalid")) || (mediaUrl.equals("redirected"))
                        || mediaUrl.equals("too many redirection")
                        || mediaUrl.equals(""))) {
                    if (socialMediaTool.getMediaUrl() != null) {
                        isInvalidURLExistes = true;
                        inValidUrls.put(1, socialMediaTool.getMediaUrl().trim());
                        inValidMessage += socialMediaTool.getMediaUrl().trim() + ", ";
                    }
                } else {
                    socialMediaTool.setMediaUrl(mediaUrl);
                    socialURL.add(mediaUrl);
                }
                if (socialMediaTool.getComptetiorUrls() != null) {
                    List<String> compdomains = null;
                    if (!(socialMediaTool.getComptetiorUrls().equals(""))) {
                        compdomains = Common.convertStringToArrayList(socialMediaTool.getComptetiorUrls().trim(), "\n");
                    } else {
                        compdomains = new LinkedList<>();
                    }
                    if (compdomains.size() > 0) {
                        if (compdomains.size() > 2) {
                            compdomains = compdomains.subList(0, 2);
                        }
                        int counter = 2;
                        for (String url : compdomains) {
                            String compURL = Common.getUrlValidation(url.trim(), session);
                            if ((compURL != null) && ((compURL.equals("invalid")) || (compURL.equals("redirected"))
                                    || compURL.equals("too many redirection"))) {
                                inValidMessage += url + ", ";
                                isInvalidURLExistes = true;
                            } else {
                                socialURL.add(compURL);
                            }
                            inValidUrls.put(counter, url);
                            counter++;
                        }
                    }
                }
                inValidMessage = inValidMessage.trim();
                if (inValidMessage.endsWith(",")) {
                    inValidMessage = inValidMessage.substring(0, inValidMessage.length() - 1);
                }
                inValidMessage = inValidMessage.concat(" " + messageSource.getMessage("MultipleURL.error", null, Locale.US));
                if (isInvalidURLExistes) {
                    session.setAttribute("toolInValidList", inValidUrls);
                    session.removeAttribute("userSMAList");
                    model.addAttribute("updateInvalid", "true");
                    model.addAttribute("messageToUser", inValidMessage);
                    model.addAttribute("callAjax", false);
                    model.addAttribute("unsubscribeset", false);
                    model.addAttribute("getSuccess", false);
                    return formView;
                }

                Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                Long socialUserId = null;
                if (user.getId() == -1) {
                    user = (Login) session.getAttribute("user");
                }
                int urlCount = 0;
                if (!(socialURL.isEmpty())) {
                    for (String url : socialURL) {
                        boolean userDomain = false;
                        if (urlCount == 0) {
                            userDomain = true;
                        }
                        if (userDomain) {
                            if (userLoggedIn) {
                                socialUserId = socialMediaService.insertSocialMediaUser(user.getId(), startTime, url, userLoggedIn, userDomain, false, true);
                            } else {
                                socialUserId = socialMediaService.insertSocialMediaUser((userSession.getUserId()), startTime, url, userLoggedIn, userDomain, false, true);
                            }
                        } else if (userLoggedIn) {
                            socialMediaService.insertSocialMediaUser(user.getId(), startTime, url, userLoggedIn, userDomain, false, true);
                        } else if (userSession != null) {
                            socialMediaService.insertSocialMediaUser((userSession.getUserId()), startTime, url, userLoggedIn, userDomain, false, true);
                        }
                        urlCount++;
                    }/*Endo of socialURL for each*/
                }
                if (socialUserId != null) {
                    session.removeAttribute("socialmediaObjects");
                    session.removeAttribute("AllMediaObject");
                    session.removeAttribute("socialDomain");
                    session.removeAttribute("socialGuestDomains");
                    List<Map<String, Object>> AllMediaObject = new LinkedList();
                    List<SocialCompDomains> socialDomains = null;

                    if (userLoggedIn) {
                        socialDomains = socialMediaService.createSocialCompDomainRows(user.getId(), userLoggedIn, true);
                    } else if (userSession != null) {
                        socialDomains = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, true);
                    }
                    if (socialDomains != null) {
                        if (socialDomains.size() > 3) {
                            socialDomains = socialDomains.subList(0, 2);
                        }
                        if (socialDomains.get(0).getDomainId() == socialUserId) {
                            socialMediaService.processSocialMediaAnalyzer(socialDomains, 1);
                        } else {
                            model.addAttribute("messageToUser", "We are unable to retrive infromation.");
                            response.setCharacterEncoding("UTF-8");
                            model.addAttribute("callAjax", false);
                            return formView;
                        }
                    }
                    String analysisURLs = "";
                    int count = 0;
                    Map<String, Object> socialmediaObjectsTemp;
                    for (SocialCompDomains sd : socialDomains) {
                        if ((!(sd.isIsDeleted()) && (sd.isUserDomain()) && (!(sd.isUnSubScribeStatus())))) {
                            socialUserId = sd.getDomainId();
                            date = socialMediaService.getDateForSocialMediaUser(sd.getDomainId());
                            socialMediaTool.setWeeklyReport(sd.isWeeklyReport());
                            socialMediaTool.setToolUnsubscription(sd.isUnSubScribeStatus());
                        }
                        if (!(sd.isUserDomain())) {
                            date = socialMediaService.getDateForSocialMediaUser(sd.getDomainId());
                        }
                        if (date == null) {
                            date = Calendar.getInstance().getTime();
                        }
                        socialmediaObjectsTemp = socialMediaService.getSocialAnalysis(sd.getDomainId(), socialMediaService.convertDateToString(date, 1),
                                socialMediaService.getPreviousDate(date, 1));
                        if (socialmediaObjectsTemp != null) {
                            AllMediaObject.add(socialmediaObjectsTemp);
                        }
                        String displayURL = SocialMediaService.trimWebsitesName(sd.getDomain());
                        if (count >= 1) {
                            analysisURLs += ", " + displayURL;
                        } else {
                            analysisURLs += displayURL;
                        }
                        count++;
                    }
                    analysisURLs += ".";
                    socialMediaTool.setReviewURLs(analysisURLs);
                    if (AllMediaObject.size() >= 1) {
                        socialmediaObjects = (Map<String, Object>) AllMediaObject.get(0);
                    } else {
                        socialmediaObjects = SocialMediaService.getEmptySocialMediaObject();
                    }
                    /*To genrate JSON Response*/
                    session.setAttribute("AllMediaObject", AllMediaObject);
                    session.setAttribute("socialmediaObjects", socialmediaObjects);
                    session.setAttribute("socialDomain", socialDomains);
                    session.setAttribute("socialGuestDomains", socialDomains);
                    model.addAttribute("socialmediaObjects", socialmediaObjects);
                    /*--END--*/
                    String analysisMessage = "Analysis as on " + socialMediaService.convertDateToString(date, 2) + ".";
                    model.addAttribute("analysisMessage", analysisMessage);
                    if (userLoggedIn) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomains, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        model.addAttribute("guestUser", "guestUser");
                        model.addAttribute("userIdInSocialUser", socialUserId);
                        model.addAttribute("mediaSettings", "true");
                    } else {
                        model.addAttribute("mediaSettings", "false");
                        session.setAttribute("GuestsocialUserId", socialUserId);
                    }

//                    if the request is coming from tool page then only add the user inputs to JSONObject
//                    if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    }
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson = new JSONObject();
                        userInputJson.put("2", socialMediaTool.isWeeklyReport());
                        userInputJson.put("3", socialMediaTool.isToolUnsubscription());
                        List<SocialCompDomains> socialCompDomainsList = new LinkedList<>();
                        if (socialMediaTool.getCompDomainSettingRows() != null && socialMediaTool.getCompDomainSettingRows().size() > 0) {
                            socialCompDomainsList = socialMediaTool.getCompDomainSettingRows();
                        } else {
                            socialCompDomainsList = socialDomains;
                        }
                        int i = 4;
                        for (SocialCompDomains socialCompDomains : socialCompDomainsList) {
                            userInputJson.put("domain" + i + "", socialCompDomains.getDomain());
                            userInputJson.put("dId" + i + "", socialCompDomains.getDomainId());
                            userInputJson.put("isUserDomain" + i + "", socialCompDomains.isUserDomain());
                            userInputJson.put("isWeeklyReport" + i + "", socialCompDomains.isWeeklyReport());
                            if (socialCompDomains.getDomainRegDate() != null) {
                                String domainRegDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getDomainRegDate());
                                userInputJson.put("domainRegDate" + i + "", domainRegDate);
                            }
                            if (socialCompDomains.getMaximumDate() != null) {
                                String maxDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getMaximumDate());
                                userInputJson.put("maximumDate" + i + "", maxDate);
                            }
                            userInputJson.put("isUnSubScribe" + i + "", socialCompDomains.isUnSubScribeStatus());
                            userInputJson.put("isGrowthDataAvailable" + i + "", socialCompDomains.isGrowthDataAvailable());
                            userInputJson.put("isLoggedIn" + i + "", socialCompDomains.isIsLoggedIn());
                            userInputJson.put("isDeleted" + i + "", socialCompDomains.isIsDeleted());
                            ChannelUserNames channelUserNames = null;
                            if (socialCompDomains.getChannelNames() != null) {
                                channelUserNames = socialCompDomains.getChannelNames();
                                userInputJson.put("channelRow" + i + "", channelUserNames.getChannelRowId());
                                userInputJson.put("cSocialMediaUserId" + i + "", channelUserNames.getSocialMediaUserId());
                                userInputJson.put("cTN" + i + "", channelUserNames.getTwitterName());
                                userInputJson.put("cFN" + i + "", channelUserNames.getFacebookName());
//                                userInputJson.put("cIN" + i + "", channelUserNames.getInstagramName());
                                userInputJson.put("cPN" + i + "", channelUserNames.getPinterestName());
                                userInputJson.put("cYN" + i + "", channelUserNames.getYoutubeName());
//                                userInputJson.put("cGN" + i + "", channelUserNames.getGooglePlusName());
                            }
                            i++;
                        }
                    }

                    model.addAttribute("getSuccess", true);
                    model.addAttribute("unsubscribeset", false);
                    model.addAttribute("callAjax", true);
                    socialMediaTool.setUpdateUrl(mediaUrl);
                    session.setAttribute("socialMediaTool", socialMediaTool);
                    if (socialMediaTool.getComptetiorUrls() != null && socialMediaTool.getCompDomainSettingRows() != null) {
                        session.setAttribute("userSMAList", 1 + socialMediaTool.getCompDomainSettingRows().size());
                    } else {
                        session.setAttribute("userSMAList", 1);
                    }
                    if (!(socialmediaObjects.isEmpty())) {
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    } else {
                        model.addAttribute("messageToUser", "We are unable to retrive infromation.");
                        response.setCharacterEncoding("UTF-8");
                        model.addAttribute("callAjax", false);
                        return formView;
                    }
                }
                return formView;
            } else if (WebUtils.hasSubmitParameter(request, "getSocialData")) {
                try {
                    Date date;
                    List<Map<String, Object>> allMediaObject = null;
                    List<SocialCompDomains> socialDomains = null;

                    if (session.getAttribute("socialDomain") != null) {
                        socialDomains = (List<SocialCompDomains>) session.getAttribute("socialDomain");
                    } else {
                        socialDomains = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, true);
                        for (SocialCompDomains domain : socialDomains) {
                            date = socialMediaService.getDateForSocialMediaUser(domain.getDomainId());
                            if (domain.getMaximumDate() == null) {
                                domain.setMaximumDate(date);
                            }
                            if (date == null) {
                                date = Calendar.getInstance().getTime();
                            }
                            boolean growthDataAvailablity = socialMediaService.checkGrwothDataAvailablity(date, domain.getDomainId());
                            domain.setGrowthDataAvailable(growthDataAvailablity);
                        }
                    }

                    String analysisDate = null;
                    if (session.getAttribute("socailAnalysisDate") != null) {
                        analysisDate = (String) session.getAttribute("socailAnalysisDate");
                    }
                    String presentDate = socialDateFormat.format(Calendar.getInstance().getTime());
                    String changeStatus = "false";
                    for (SocialCompDomains sD : socialDomains) {
                        if (sD.isUserDomain()) {
                            if (analysisDate == null) {
                                date = socialMediaService.getDateForSocialMediaUser(sD.getDomainId());
                                if (sD.getMaximumDate() == null) {
                                    sD.setMaximumDate(date);
                                }
                                if (date == null) {
                                    date = Calendar.getInstance().getTime();
                                }
                                analysisDate = socialDateFormat.format(date);
                            }

                            boolean maxmiumData = presentDate.equals(analysisDate);
                            boolean registrationDate = presentDate.equals(Common.convertTimeStampintoDate(sD.getDomainRegDate()));
                            if (!registrationDate) {
                                if (maxmiumData) {
                                    changeStatus = "true";
                                } else if (sD.isGrowthDataAvailable()) {
                                    changeStatus = "true";
                                }
                            }
                        }
                    }
                    if (session.getAttribute("AllMediaObject") != null) {
                        allMediaObject = (List<Map<String, Object>>) session.getAttribute("AllMediaObject");
                    } else {
                        allMediaObject = new LinkedList<>();
                        Map<String, Object> regUserSocialmedia;
                        for (SocialCompDomains sD : socialDomains) {
                            date = socialMediaService.getDateForSocialMediaUser(sD.getDomainId());
                            if (sD.getMaximumDate() == null) {
                                sD.setMaximumDate(date);
                            }
                            if (date == null) {
                                date = Calendar.getInstance().getTime();
                            }
                            regUserSocialmedia = socialMediaService.getSocialAnalysis(sD.getDomainId(), socialMediaService.convertDateToString(date, 1), socialMediaService.getPreviousDate(date, 1));
                            if (regUserSocialmedia != null) {
                                allMediaObject.add(regUserSocialmedia);
                            }
                        }
                    }
                    userSession.addToolUsage(toolObj.getToolId());
                    JSONObject responseObject = socialMediaService.generateJSONResponse(allMediaObject, socialDomains);
                    socialMediaService.constructAnlaysisResponseJSON(responseObject, changeStatus, response);
                    return null;
                } catch (Exception e) {
                    logger.error("Exception for getSocialData: ", e);

                }
            } else if (WebUtils.hasSubmitParameter(request, "update")) {
                Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                if (socialMediaTool != null && socialMediaTool.isToolUnsubscription()) {
                    socialMediaService.updateUserSettings((userSession.getUserId()), socialMediaTool.isToolUnsubscription(), socialMediaTool.isWeeklyReport(), startTime);
                    List<SocialCompDomains> socialDomainUnSub = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, false);
                    socialDomainUnSub.stream().filter((sd) -> ((sd.isIsLoggedIn()) && (sd.isUserDomain()))).map((sd) -> {
                        socialMediaTool.setMediaUrl(sd.getDomain());
                        return sd;
                    }).map((sd) -> {
                        socialMediaTool.setUpdateUrl(sd.getDomain());
                        return sd;
                    }).map((sd) -> {
                        socialMediaTool.setWeeklyReport(sd.isWeeklyReport());
                        return sd;
                    }).forEach((sd) -> {
                        socialMediaTool.setToolUnsubscription(sd.isUnSubScribeStatus());
                    });
                    List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                    socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                    socialmediaObjects = new HashMap<>();
                    session.setAttribute("socialmediaObjects", socialmediaObjects);
                    model.addAttribute("unsubscribeset", true);
                    model.addAttribute("getSuccess", false);
                    model.addAttribute("callAjax", false);
                    model.addAttribute("messageToUser", "You have unsubscribed from the tool. To re-subscribe please uncheck the unsubscribe option.");
                    response.setCharacterEncoding("UTF-8");
                    return formView;
                } else {
                    socialMediaService.updateResubscribeData((userSession.getUserId()), socialMediaTool.isWeeklyReport());
                    //List<SocialCompDomains> prvCompDomainRows = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, false);
                    List<SocialCompDomains> updateDomains = socialMediaTool.getCompDomainSettingRows();
                    String inValidMessage = " ";
                    int counter = 1;
                    boolean isInvalidURLExists = false;
                    session.removeAttribute("toolInValidList");
                    Map<Integer, String> inValidUrls = new HashMap<>();
                    for (SocialCompDomains validateDomains : updateDomains) {
                        if (validateDomains != null) {
                            String newCompUrl = "";
                            if (validateDomains.getDomain() != null && (!validateDomains.getDomain().equals("deleted")) && (!(validateDomains.getDomain().trim().equals("")))) {
//                                newCompUrl = SocialMediaService.getUrlValidation(validateDomains.getDomain().trim());
                                newCompUrl = Common.getUrlValidation(validateDomains.getDomain().trim(), session);
                            }
                            if ((newCompUrl.equals("invalid"))
                                    || (newCompUrl.equals("redirected"))
                                    || (newCompUrl.equals("too many redirection"))) {
                                if (!(validateDomains.getDomain().trim().equals(""))) {
                                    isInvalidURLExists = true;
                                    if (!validateDomains.isUserDomain()) {
                                        inValidUrls.put(counter, validateDomains.getDomain().trim());
                                    }
                                    inValidMessage += validateDomains.getDomain().trim() + ", ";
                                }
                            }
                        }
                        counter++;
                    }
                    inValidMessage = inValidMessage.trim();
                    if (inValidMessage.endsWith(",")) {
                        inValidMessage = inValidMessage.substring(0, inValidMessage.length() - 1);
                    }
                    inValidMessage = inValidMessage.concat(" " + messageSource.getMessage("MultipleURL.error", null, Locale.US));
                    if (isInvalidURLExists) {
                        session.setAttribute("toolInValidList", inValidUrls);
                        model.addAttribute("messageToUser", inValidMessage);
                        response.setCharacterEncoding("UTF-8");
                        return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                    }
                    List<SocialCompDomains> newModifiedCompDomains = socialMediaService.compareCompDomainsData((userSession.getUserId()), socialMediaTool);
                    //List<SocialCompDomains> modifiedCompDomainRows = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, false);
                    List<SocialCompDomains> newDomains = new LinkedList<>();
                    if (newModifiedCompDomains != null && newModifiedCompDomains.size() > 0) {
                        newModifiedCompDomains.stream().forEach((newMod) -> {
                            newDomains.add(newMod);
                        });
                    }

                    List<SocialCompDomains> socialUpdatedDomains = new LinkedList<>();
                    Timestamp updatetime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                    if (!(newDomains.isEmpty())) {
                        for (SocialCompDomains updatedURL : newDomains) {
                            String validUpdatedURL = Common.getUrlValidation(updatedURL.getDomain().trim(), session);
//                            String validUpdatedURL = SocialMediaService.getUrlValidation(updatedURL.getDomain().trim());
                            if ((validUpdatedURL != null) && ((!(validUpdatedURL.equalsIgnoreCase("invalid"))) || (!(validUpdatedURL.equalsIgnoreCase("redirected")))
                                    || (!(validUpdatedURL.equals("too many redirection")))) || (!(validUpdatedURL.equals("")))) {

                                long regSocialUserId = socialMediaService.insertSocialMediaUser(user.getId(), updatetime, validUpdatedURL, userLoggedIn, updatedURL.isUserDomain(), false, true);
                                SocialCompDomains newUpdateCompDomains = new SocialCompDomains();
                                newUpdateCompDomains.setDomainId(regSocialUserId);
                                newUpdateCompDomains.setDomain(validUpdatedURL);
                                newUpdateCompDomains.setUserDomain(updatedURL.isUserDomain());
                                newUpdateCompDomains.setIsLoggedIn(true);
                                newUpdateCompDomains.setIsDeleted(false);
                                newUpdateCompDomains.setWeeklyReport(true);
                                newUpdateCompDomains.setDomainRegDate(updatetime);
                                socialUpdatedDomains.add(newUpdateCompDomains);
                            }
                        }

//                                                                if the request is coming from tool page then only add the user inputs to JSONObject
//                                                                if the request is coming from mail then do not add the user inputs to JSONObject
                        boolean requestFromMail = false;
                        if (session.getAttribute("requestFromMail") != null) {
                            requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                            session.removeAttribute("requestFromMail");
                        }
                        if (!requestFromMail && !user.isAdmin()) {
                            userInputJson = new JSONObject();
                            userInputJson.put("2", socialMediaTool.isWeeklyReport());
                            userInputJson.put("3", socialMediaTool.isToolUnsubscription());
                            List<SocialCompDomains> socialCompDomainsList = new LinkedList<>();
                            if (socialMediaTool.getCompDomainSettingRows() != null) {
                                socialCompDomainsList = socialMediaTool.getCompDomainSettingRows();
                            }
                            int i = 4;
                            for (SocialCompDomains socialCompDomains : socialCompDomainsList) {
                                userInputJson.put("domain" + i + "", socialCompDomains.getDomain());
                                userInputJson.put("dId" + i + "", socialCompDomains.getDomainId());
                                userInputJson.put("isUserDomain" + i + "", socialCompDomains.isUserDomain());
                                userInputJson.put("isWeeklyReport" + i + "", socialCompDomains.isWeeklyReport());
                                if (socialCompDomains.getDomainRegDate() != null) {
                                    String domainRegDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getDomainRegDate());
                                    userInputJson.put("domainRegDate" + i + "", domainRegDate);
                                }
                                if (socialCompDomains.getMaximumDate() != null) {
                                    String maxDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getMaximumDate());
                                    userInputJson.put("maximumDate" + i + "", maxDate);
                                }
                                userInputJson.put("isUnSubScribe" + i + "", socialCompDomains.isUnSubScribeStatus());
                                userInputJson.put("isGrowthDataAvailable" + i + "", socialCompDomains.isGrowthDataAvailable());
                                userInputJson.put("isLoggedIn" + i + "", socialCompDomains.isIsLoggedIn());
                                userInputJson.put("isDeleted" + i + "", socialCompDomains.isIsDeleted());
                                ChannelUserNames channelUserNames = null;
                                if (socialCompDomains.getChannelNames() != null) {
                                    channelUserNames = socialCompDomains.getChannelNames();
                                    userInputJson.put("channelRow" + i + "", channelUserNames.getChannelRowId());
                                    userInputJson.put("cSocialMediaUserId" + i + "", channelUserNames.getSocialMediaUserId());
                                    userInputJson.put("cTN" + i + "", channelUserNames.getTwitterName());
                                    userInputJson.put("cFN" + i + "", channelUserNames.getFacebookName());
//                                    userInputJson.put("cIN" + i + "", channelUserNames.getInstagramName());
                                    userInputJson.put("cPN" + i + "", channelUserNames.getPinterestName());
                                    userInputJson.put("cYN" + i + "", channelUserNames.getYoutubeName());
//                                    userInputJson.put("cGN" + i + "", channelUserNames.getGooglePlusName());
                                }
                                i++;
                            }
                        }
                    }
                    if (socialUpdatedDomains.size() > 0) {
                        socialMediaService.processSocialMediaAnalyzer(socialUpdatedDomains, 1);
                    }
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);

                }/*End of if tool not unsubsribed(1st else) */
            } else if (WebUtils.hasSubmitParameter(request, "cancel")) {
                if (isMainUnsubscribed) {
                    List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                    socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                    socialmediaObjects = new HashMap<>();
                    session.setAttribute("socialmediaObjects", socialmediaObjects);
                    model.addAttribute("unsubscribeset", true);
                    model.addAttribute("getSuccess", false);
                    model.addAttribute("callAjax", false);
                    socialMediaTool.setToolUnsubscription(true);
                    compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                        socialMediaTool.setMediaUrl(cDSR.getDomain());
                    });
                    socialMediaTool.setWeeklyReport(true);
                    session.setAttribute("socialMediaTool", socialMediaTool);
                    model.addAttribute("messageToUser", "You have unsubscribed from the tool. To re-subscribe please uncheck the unsubscribe option.");
                    response.setCharacterEncoding("UTF-8");
                    return formView;
                }
                if (userLoggedIn) {
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                } else {
                    return getSocialUserData(session, model, response, socialMediaTool, (userSession.getUserId()), userLoggedIn, null);
                }
            } else if (WebUtils.hasSubmitParameter(request, "socialMediaATE")) {
                /*To  Handle Ask Our Expert request*/
                String socialURL = request.getParameter("socialUrl");
                List<SocialCompDomains> socialDomainsATE = null;
                if (session.getAttribute("socialDomain") != null) {
                    socialDomainsATE = (List<SocialCompDomains>) session.getAttribute("socialDomain");
                } else {
                    socialDomainsATE = socialMediaService.createSocialCompDomainRows((userSession.getUserId()), userLoggedIn, true);
                }
                List<Map<String, Object>> allMediaObject;
                if (session.getAttribute("AllMediaObject") != null) {
                    allMediaObject = (List<Map<String, Object>>) session.getAttribute("AllMediaObject");
                } else {
                    Date date;
                    allMediaObject = new LinkedList();
                    Map<String, Object> regUserSocialmedia;
                    for (SocialCompDomains sD : socialDomainsATE) {
                        date = socialMediaService.getDateForSocialMediaUser(sD.getDomainId());
                        if (sD.getMaximumDate() == null) {
                            sD.setMaximumDate(date);
                        }
                        if (date == null) {
                            date = Calendar.getInstance().getTime();
                        }
                        regUserSocialmedia = socialMediaService.getSocialAnalysis(sD.getDomainId(), socialMediaService.convertDateToString(date, 1), socialMediaService.getPreviousDate(date, 1));
                        if (regUserSocialmedia != null) {
                            allMediaObject.add(regUserSocialmedia);
                        }
                    }
                }
                /*For Analysis of Twitter: Help in researching relevant HashTags to target*/
 /*For analysis :If user had provided competitor websites and competitor websites have twitter handles.*/
                boolean hashTagIssues = false;
                boolean userTwitterAccount = false;
                /*For analysis: To check user added comptetiors urls or not  and to do analysis with comptetior hash tags. */
                boolean compHashTagExsited = false;
                /*For Analysis :If user didn't provide and competitor websites.*/
                boolean comptetiorTwitterAccount = false;

                TwitterMetrics userTwitter;
                TwitterMetrics comptetiorOneTwitter = null;
                TwitterMetrics comptetiorTwoTwitter = null;
                List<String> userHashTag = new LinkedList<>();
                List<String> competitorOneHashTag = new LinkedList<>();
                List<String> competitorTwoHashTag = new LinkedList<>();

                FacebookMetrics facebookResponse;
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                String toolIssues = "";
                String questionInfo = "";
                if (allMediaObject.size() >= 1) {
                    try {
                        Map<String, Object> socialmediaATE = null;
                        /*checking user hash tag content*/
                        socialmediaATE = allMediaObject.get(0);
                        userTwitter = (TwitterMetrics) socialmediaATE.get("twittermetrics");
                        /*Checking for comptetior twitter account status and hash tag content.*/
                        if (allMediaObject.size() >= 2) {
                            socialmediaATE = allMediaObject.get(1);
                            comptetiorOneTwitter = (TwitterMetrics) socialmediaATE.get("twittermetrics");
                        }
                        if (allMediaObject.size() >= 3) {
                            socialmediaATE = allMediaObject.get(2);
                            comptetiorTwoTwitter = (TwitterMetrics) socialmediaATE.get("twittermetrics");
                        }

                        socialmediaATE = allMediaObject.get(0);
                        facebookResponse = (FacebookMetrics) socialmediaATE.get("facebookmetrics");

                        if ((userTwitter.isTwitterAccountStatus()) || (facebookResponse.isFacebookAccountStatus())) {
                            questionInfo = "Need help in fixing following issues with my website '" + socialURL + "'.\n\n";
                            toolIssues = "<ul>";
                        }
                        if ((userTwitter.isTwitterAccountStatus())) {
                            /*Checking user twitter account status is existence for analysis 1*/
                            questionInfo += "- Auditing Twitter Bio for better visibility.\n";
                            questionInfo += "- Strategy to increase Twitter followers.\n";
                            toolIssues += "<li>" + "We can audit your Twitter bio for completeness." + "</li>";
                            toolIssues += "<li>" + "We can help you create a strategy to increase your Twitter follower count." + "</li>";
                            String tempUserHashTag = userTwitter.getTwitterHashTag();
                            userTwitterAccount = true;
                            if (!tempUserHashTag.equalsIgnoreCase("NoHashTag")) {
                                if (tempUserHashTag.endsWith(".")) {
                                    int dotIndex = tempUserHashTag.lastIndexOf(".");
                                    if (dotIndex == tempUserHashTag.length()) {
                                        tempUserHashTag = tempUserHashTag.substring(0, dotIndex - 1).trim();
                                    }
                                }
                                userHashTag = new LinkedList<>(Arrays.asList(tempUserHashTag.split("  ")));
                            } else {
                                userHashTag = new LinkedList<>();
                            }
                            /*checking comptetior hash tag content*/
                            if ((comptetiorOneTwitter != null) && (comptetiorOneTwitter.isTwitterAccountStatus())) {
                                comptetiorTwitterAccount = true;
                                String tempCompetitorHashTag = comptetiorOneTwitter.getTwitterHashTag();
                                if (!tempCompetitorHashTag.equalsIgnoreCase("NoHashTag")) {
                                    compHashTagExsited = true;
                                    if (tempCompetitorHashTag.endsWith(".")) {
                                        int dotIndex = tempCompetitorHashTag.lastIndexOf(".");
                                        if (dotIndex == tempCompetitorHashTag.length()) {
                                            tempCompetitorHashTag = tempCompetitorHashTag.substring(0, dotIndex - 1).trim();
                                        }
                                    }
                                    competitorOneHashTag = new LinkedList<>(Arrays.asList(tempCompetitorHashTag.split("  ")));
                                } else {
                                    competitorOneHashTag = new LinkedList<>();
                                }

                                if ((!userHashTag.isEmpty()) && (!competitorOneHashTag.isEmpty())) {
                                    List<String> tempUserHashTagList = new LinkedList<>();
                                    tempUserHashTagList.addAll(userHashTag);
                                    tempUserHashTagList.removeAll(competitorOneHashTag);
                                    if (!tempUserHashTagList.isEmpty()) {
                                        hashTagIssues = true;
                                    }
                                } else if ((userHashTag.isEmpty()) && (!competitorOneHashTag.isEmpty())) {
                                    hashTagIssues = true;
                                }
                            }/*EOF of comptetior one  twitter account status*/
 /*checking comptetior hash tag content*/
                            if ((comptetiorTwoTwitter != null) && (comptetiorTwoTwitter.isTwitterAccountStatus())) {
                                comptetiorTwitterAccount = true;
                                String tempCompetitorHashTag = comptetiorTwoTwitter.getTwitterHashTag();
                                if (!tempCompetitorHashTag.equalsIgnoreCase("NoHashTag")) {
                                    compHashTagExsited = true;
                                    if (tempCompetitorHashTag.endsWith(".")) {
                                        int dotIndex = tempCompetitorHashTag.lastIndexOf(".");
                                        if (dotIndex == tempCompetitorHashTag.length()) {
                                            tempCompetitorHashTag = tempCompetitorHashTag.substring(0, dotIndex - 1).trim();
                                        }
                                    }
                                    competitorTwoHashTag = new LinkedList<>(Arrays.asList(tempCompetitorHashTag.split("  ")));
                                }
                                if ((!userHashTag.isEmpty()) && (!competitorTwoHashTag.isEmpty())) {
                                    List<String> tempUserHashTagList = new LinkedList<>();
                                    tempUserHashTagList.addAll(userHashTag);
                                    tempUserHashTagList.removeAll(competitorTwoHashTag);
                                    if (!tempUserHashTagList.isEmpty()) {
                                        hashTagIssues = true;
                                    }
                                } else if ((userHashTag.isEmpty()) && (!competitorTwoHashTag.isEmpty())) {
                                    hashTagIssues = true;
                                }
                            }/*EOF of comptetior one  twitter account status*/


                        }/*EOF of user twitter account status*/

                        if ((userTwitterAccount) && (comptetiorTwitterAccount) && (compHashTagExsited) && (hashTagIssues)) {
                            questionInfo += "- Researching relevant Hashtags for Twitter.\n";
                            toolIssues += "<li>" + "Your competitors are targeting different hashtags than you. We can do a quick audit of your Twitter account to "
                                    + "help provide you with relevant hashtags you might want to target.</li>";

                        } else if ((userTwitterAccount) && (comptetiorTwitterAccount) && (!compHashTagExsited)) {
                            questionInfo += "- Researching relevant Hashtags for Twitter.\n";
                            toolIssues += "<li>" + "Your competitors are not active on Twitter, which gives you an added advantage in targeting your users. "
                                    + "We can do a quick audit of your Twitter account to help provide you with relevant hashtags you might want to target.</li>";
                        } else if (userTwitterAccount) {
                            questionInfo += "- Researching relevant Hashtags for Twitter.\n";
                            toolIssues += "<li>" + "We can do a quick audit of your Twitter account to help provide you with relevant hashtags you might want to target." + "</li>";
                        }

                        socialmediaATE = allMediaObject.get(0);
                        facebookResponse = (FacebookMetrics) socialmediaATE.get("facebookmetrics");
                        if (facebookResponse.isFacebookAccountStatus()) {
                            questionInfo += "- Auditing Facebook Bio for better visibility.\n";
                            questionInfo += "- Strategy to increase Facebook followers.\n";
                            toolIssues += "<li>" + "We can audit your Facebook bio for completeness." + "</li>";
                            toolIssues += "<li>" + "We can help you create a strategy to increase your Facebook followers count." + "</li>";
                        }
                        if (!toolIssues.equals("")) {
                            toolIssues += "</ul>";
                        }

                        lxrmAskExpert.setDomain(socialURL);
                        if (userInputJson != null) {
                            lxrmAskExpert.setOtherInputs(userInputJson.toString());
                        }
                        lxrmAskExpert.setQuestion(questionInfo);
                        lxrmAskExpert.setIssues(toolIssues);
                        lxrmAskExpert.setExpertInfo(questionInfo);
                        lxrmAskExpert.setToolName(toolObj.getToolName());
                        lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                                if the request is not from  mail then we should add this input to the session object
                        if (userInputJson != null) {
                            String toolUrl = "/" + toolObj.getToolLink();
                            userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), socialURL, toolIssues,
                                    questionInfo, userInputJson.toString(), toolUrl);
                            userInputJson = null;
                        }

                        return null;
                    } catch (Exception e) {
                        logger.error("Exception in fetching has tag: ", e);
                    }
                }/*EOF of checking size of allMediaObject is 1*/

                return null;
            } else if (userLoggedIn && (userIdInSocialUser > 0 || (toolUsageInfo != null && toolUsageInfo.get(29) != null))) {
                if (isLogged && userIdInSocialUser > 0) {
                    if (isMainUnsubscribed) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        socialmediaObjects = new HashMap<>();
                        session.setAttribute("socialmediaObjects", socialmediaObjects);
                        model.addAttribute("unsubscribeset", true);
                        model.addAttribute("getSuccess", false);
                        model.addAttribute("callAjax", false);
                        socialMediaTool.setToolUnsubscription(true);
                        compDomainSettingRows.stream().filter((cDSR) -> (cDSR.isUserDomain())).forEach((cDSR) -> {
                            socialMediaTool.setMediaUrl(cDSR.getDomain());
                        });
                        socialMediaTool.setWeeklyReport(true);
                        session.setAttribute("socialMediaTool", socialMediaTool);
                        model.addAttribute("messageToUser", "To re-subscribe, please uncheck the unsubscribe option.");
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    }
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                } else {
                    model.addAttribute("callAjax", false);
                    model.addAttribute("unsubscribeset", false);
                    model.addAttribute("getSuccess", false);
                    return formView;
                }
            } else if ((toolUsageInfo == null || (toolUsageInfo.get(29) == null))) {
                if (isLogged) {
                    if (isMainUnsubscribed) {
                        List<SocialCompDomains> compDomainSettingRows = socialMediaService.createCompDomainSettingRows(socialDomain, socialMediaTool);
                        socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
                        socialmediaObjects = new HashMap<>();
                        session.setAttribute("socialmediaObjects", socialmediaObjects);
                        model.addAttribute("unsubscribeset", true);
                        model.addAttribute("getSuccess", false);
                        model.addAttribute("callAjax", false);
                        socialMediaTool.setToolUnsubscription(true);
                        for (SocialCompDomains cDSR : compDomainSettingRows) {
                            if (cDSR.isUserDomain()) {
                                socialMediaTool.setMediaUrl(cDSR.getDomain());
                            }
                        }
                        socialMediaTool.setWeeklyReport(true);
                        session.setAttribute("socialMediaTool", socialMediaTool);
                        model.addAttribute("messageToUser", "To re-subscribe, please uncheck the unsubscribe option.");
                        response.setCharacterEncoding("UTF-8");
                        return formView;
                    }
                    return getSocialUserData(session, model, response, socialMediaTool, user.getId(), userLoggedIn, null);
                }
                model.addAttribute("callAjax", false);
                model.addAttribute("unsubscribeset", false);
                model.addAttribute("getSuccess", false);
                session.removeAttribute("toolInValidList");
                return formView;
            }
        }
        model.addAttribute("callAjax", false);
        model.addAttribute("unsubscribeset", false);
        model.addAttribute("getSuccess", false);
        session.removeAttribute("toolInValidList");
        return formView;
    }

    public String getSocialUserData(HttpSession session, ModelMap model, HttpServletResponse response, SocialMedia socialMediaTool, long lxrmUserid, boolean userLoggedIn, String reportType) {
        try {
            Login user = (Login) session.getAttribute("user");
            Date date = null;
            long socialUserId = 0;
            Timestamp dbStartTime = null;
            String dbTodayDate = null;
            Date analysisDate = null;
            List<Map<String, Object>> AllMediaObject = new LinkedList<>();
            Map<String, Object> socialmediaObjects = null;
            List<SocialCompDomains> socialDomainTemp = null;
            boolean requestFromMail = false;
            if (session.getAttribute("requestFromMail") != null) {
                requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                session.removeAttribute("requestFromMail");
            }
            if (requestFromMail) {
                socialDomainTemp = socialMediaTool.getCompDomainSettingRows();
            } else {
                socialDomainTemp = socialMediaService.createSocialCompDomainRows(lxrmUserid, userLoggedIn, false);
            }
            if (socialDomainTemp != null && socialDomainTemp.size() >= 4) {
                socialDomainTemp = socialDomainTemp.subList(0, 2);
                session.setAttribute("userSMAList", socialDomainTemp.size());
            }

//                    if the request is coming from tool page then only add the user inputs to JSONObject
//                    if the request is coming from mail then do not add the user inputs to JSONObject
            if (!requestFromMail && user != null && !user.isAdmin()) {
                userInputJson = new JSONObject();
                userInputJson.put("2", socialMediaTool.isWeeklyReport());
                userInputJson.put("3", socialMediaTool.isToolUnsubscription());
                List<SocialCompDomains> socialCompDomainsList = new LinkedList<>();
                if (socialMediaTool.getCompDomainSettingRows() != null && socialMediaTool.getCompDomainSettingRows().size() > 0) {
                    socialCompDomainsList = socialMediaTool.getCompDomainSettingRows();
                } else {
                    socialCompDomainsList = socialDomainTemp;
                }
                int i = 4;
                for (SocialCompDomains socialCompDomains : socialCompDomainsList) {
                    userInputJson.put("domain" + i + "", socialCompDomains.getDomain());
                    userInputJson.put("dId" + i + "", socialCompDomains.getDomainId());
                    userInputJson.put("isUserDomain" + i + "", socialCompDomains.isUserDomain());
                    userInputJson.put("isWeeklyReport" + i + "", socialCompDomains.isWeeklyReport());
                    if (socialCompDomains.getDomainRegDate() != null) {
                        String domainRegDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getDomainRegDate());
                        userInputJson.put("domainRegDate" + i + "", domainRegDate);
                    }
                    if (socialCompDomains.getMaximumDate() != null) {
                        String maxDate = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(socialCompDomains.getMaximumDate());
                        userInputJson.put("maximumDate" + i + "", maxDate);
                    }
                    userInputJson.put("isUnSubScribe" + i + "", socialCompDomains.isUnSubScribeStatus());
                    userInputJson.put("isGrowthDataAvailable" + i + "", socialCompDomains.isGrowthDataAvailable());
                    userInputJson.put("isLoggedIn" + i + "", socialCompDomains.isIsLoggedIn());
                    userInputJson.put("isDeleted" + i + "", socialCompDomains.isIsDeleted());
                    ChannelUserNames channelUserNames = null;
                    if (socialCompDomains.getChannelNames() != null) {
                        channelUserNames = socialCompDomains.getChannelNames();
                        userInputJson.put("channelRow" + i + "", channelUserNames.getChannelRowId());
                        userInputJson.put("cSocialMediaUserId" + i + "", channelUserNames.getSocialMediaUserId());
                        userInputJson.put("cTN" + i + "", channelUserNames.getTwitterName());
                        userInputJson.put("cFN" + i + "", channelUserNames.getFacebookName());
//                        userInputJson.put("cIN" + i + "", channelUserNames.getInstagramName());
                        userInputJson.put("cPN" + i + "", channelUserNames.getPinterestName());
                        userInputJson.put("cYN" + i + "", channelUserNames.getYoutubeName());
//                        userInputJson.put("cGN" + i + "", channelUserNames.getGooglePlusName());
                    }
                    i++;
                }
            }
            if (socialDomainTemp != null && socialDomainTemp.size() >= 1) {
                logger.info("Preparing data for tool user with size:" + socialDomainTemp.size() + ", for userId : " + socialDomainTemp.get(0).getDomainId());
            }
            String analysisURLs = "";
            int count = 0;
            boolean growthDataStatus = false;
            if (socialDomainTemp != null) {
                for (SocialCompDomains sd : socialDomainTemp) {
//                if ((sd.isIsLoggedIn()) && (!(sd.isIsDeleted()) && (sd.isUserDomain()) && (!(sd.isUnSubScribeStatus())))) {
                    if ((!(sd.isIsDeleted()) && (sd.isUserDomain()) && (!(sd.isUnSubScribeStatus())))) {
                        socialUserId = sd.getDomainId();
                        dbStartTime = sd.getDomainRegDate();
                        date = socialMediaService.getDateForSocialMediaUser(sd.getDomainId());
                        analysisDate = date;
                        sd.setMaximumDate(date);
                        growthDataStatus = socialMediaService.checkGrwothDataAvailablity(date, sd.getDomainId());
                        sd.setGrowthDataAvailable(growthDataStatus);
                        socialMediaTool.setWeeklyReport(sd.isWeeklyReport());
                        socialMediaTool.setMediaUrl(sd.getDomain());
                        socialMediaTool.setToolUnsubscription(sd.isUnSubScribeStatus());
                    }
                    if (!(sd.isUserDomain())) {
                        date = socialMediaService.getDateForSocialMediaUser(sd.getDomainId());
                        growthDataStatus = socialMediaService.checkGrwothDataAvailablity(date, sd.getDomainId());
                        sd.setGrowthDataAvailable(growthDataStatus);
                        sd.setMaximumDate(date);
                        if (date == null) {
                            date = Calendar.getInstance().getTime();
                        }
                    }
                    socialmediaObjects = socialMediaService.getSocialAnalysis(sd.getDomainId(), socialMediaService.convertDateToString(date, 1),
                            socialMediaService.getPreviousDate(date, 1));
                    if (socialmediaObjects != null && sd.getDomainId() >= 1) {
                        AllMediaObject.add(socialmediaObjects);
                    }
                    String displayURL = SocialMediaService.trimWebsitesName(sd.getDomain());
                    if (count >= 1) {
                        analysisURLs += ", " + displayURL;
                    } else {
                        analysisURLs += displayURL;
                    }
                    count++;
                }
            }

            session.setAttribute("socialDomain", socialDomainTemp);
            analysisURLs += ".";
            socialMediaTool.setReviewURLs(analysisURLs);
            model.addAttribute("userIdInSocialUser", socialUserId);
//            if (date == null) {
//                date = Calendar.getInstance().getTime();
//            }
            if (dbStartTime == null) {
                dbStartTime = socialMediaService.fetchSocialUserLoginDate(socialUserId);
            }
            String toDaydate = null;
            if (analysisDate != null) {
                toDaydate = socialDateFormat.format(analysisDate);
                if (toDaydate != null) {
                    session.setAttribute("socailAnalysisDate", toDaydate);
                }
            }
            String analysisMessage;
            String currentDate = null;
            String previousDate = null;
            if (dbStartTime != null) {
                dbTodayDate = socialDateFormat.format(dbStartTime);
                if (toDaydate != null && toDaydate.equalsIgnoreCase(dbTodayDate)) {
                    currentDate = socialMediaService.convertDateToString(analysisDate, 2);
                    analysisMessage = "Analysis as on " + currentDate + ".";
                } else {
                    currentDate = socialMediaService.convertDateToString(analysisDate, 2);
                    previousDate = socialMediaService.getPreviousDate(analysisDate, 2);
                    analysisMessage = "Analysis as on " + previousDate + " to " + currentDate + ".";
                }
            } else {
                currentDate = socialMediaService.convertDateToString(analysisDate, 2);
                previousDate = socialMediaService.getPreviousDate(analysisDate, 2);
                analysisMessage = "Analysis as on " + previousDate + " to " + currentDate + ".";
            }
            model.addAttribute("analysisMessage", analysisMessage);
            if (AllMediaObject.size() >= 1) {
                socialmediaObjects = (Map<String, Object>) AllMediaObject.get(0);
            }
            session.setAttribute("AllMediaObject", AllMediaObject);

            if (socialmediaObjects != null) {
                if (socialmediaObjects.isEmpty()) {
                    model.addAttribute("updateInvalid", "true");
                    model.addAttribute("unsubscribeset", false);
                    model.addAttribute("getSuccess", false);
                    model.addAttribute("callAjax", false);
                    model.addAttribute("mediaSettings", "false");
                    model.addAttribute("messageToUser", "Unable to fetch your social media metrics at this moment.");
                    response.setCharacterEncoding("UTF-8");
                    return formView;
                }
                session.setAttribute("socialmediaObjects", socialmediaObjects);
                model.addAttribute("socialmediaObjects", socialmediaObjects);
            }

            model.addAttribute("mediaSettings", "true");
            model.addAttribute("getSuccess", true);
            model.addAttribute("unsubscribeset", false);
            model.addAttribute("callAjax", true);
            response.setCharacterEncoding("UTF-8");
            if (reportType != null) {
                String downloadfileName = null;
                if (previousDate == null) {
                    previousDate = "";
                }
                downloadfileName = socialMediaService.generatePdfForSocialMediaAnalyzer(downloadFolder, AllMediaObject, socialDomainTemp, currentDate, previousDate, lxrmUserid);
//                downloadfileName = socialMediaService.addWaterMark(downloadfileName, downloadFolder, lxrmUserid);
                if (downloadfileName != null && !downloadfileName.trim().equals("")) {
                    if (reportType.equalsIgnoreCase("download")) {
                        Common.downloadReport(response, downloadFolder, downloadfileName, "pdf");
                    } else if (reportType.contains("@")) {
                        /*If request type is send mail this method returns the file name of created report.
                        Not returning form view to handle AJAX call request.*/
                        return downloadfileName;
                    }
                }
            }
            List<SocialCompDomains> tempSocialDomains = socialDomainTemp;
            List<SocialCompDomains> compDomainSettingRows = null;
            if (requestFromMail) {
                compDomainSettingRows = socialMediaService.createCompDomainSettingRows(tempSocialDomains, socialMediaTool);
            } else {
                compDomainSettingRows = socialMediaService.createCompDomainSettingRows(tempSocialDomains, socialMediaTool);
            }
            socialMediaTool.setCompDomainSettingRows(compDomainSettingRows);
            session.setAttribute("socialMediaTool", socialMediaTool);
            return formView;
        } catch (JSONException e) {
            logger.error("JSONException to handle preparing data set cause: ", e);
        } catch (Exception e) {
            logger.error("Exception to handle preparing data set cause: ", e);
        }
        model.addAttribute("callAjax", false);
        model.addAttribute("unsubscribeset", false);
        model.addAttribute("getSuccess", false);
        return formView;
    }

}
