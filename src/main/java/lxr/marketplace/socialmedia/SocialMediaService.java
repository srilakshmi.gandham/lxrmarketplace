package lxr.marketplace.socialmedia;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.apiaccess.FaceBookMetricsAPIService;
/*import lxr.marketplace.apiaccess.PinterestMetricsAPIService;
import lxr.marketplace.apiaccess.GooglePlusMetricsAPIService;
import lxr.marketplace.apiaccess.InstagramMetricsAPIService;*/
import lxr.marketplace.apiaccess.TwitterMetricsAPIService;
import lxr.marketplace.apiaccess.YouTubeMetricsAPIService;

import lxr.marketplace.apiaccess.lxrmbeans.FacebookMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.GoogleMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.InstagramMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.TwitterMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.YouTubeMetrics;
import lxr.marketplace.apiaccess.lxrmbeans.PinterestMetrics;

import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.beans.factory.annotation.Autowired;

import de.toolforge.googlechartwrapper.BarChart;
import de.toolforge.googlechartwrapper.BarChart.BarChartOrientation;
import de.toolforge.googlechartwrapper.BarChart.BarChartStyle;
import de.toolforge.googlechartwrapper.data.BarChartDataSerie;
import de.toolforge.googlechartwrapper.data.DataScalingSet;
import de.toolforge.googlechartwrapper.label.AxisLabel;
import de.toolforge.googlechartwrapper.label.AxisLabelContainer;
import de.toolforge.googlechartwrapper.label.AxisRange;
import de.toolforge.googlechartwrapper.label.AxisStyle;
import de.toolforge.googlechartwrapper.label.AxisType;
import de.toolforge.googlechartwrapper.label.ChartTitle;
import de.toolforge.googlechartwrapper.style.BarWidthAndSpacing;

import java.awt.Dimension;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Set;
import org.springframework.beans.BeansException;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SocialMediaService {

    private static Logger logger = Logger.getLogger(SocialMediaService.class);

    private TwitterMetrics twittermetrics = null;
    private FacebookMetrics facebookmetrics = null;
    private GoogleMetrics googleplusmetrics = null;
    private YouTubeMetrics youtubemetrics = null;
    private InstagramMetrics instagrammetrics = null;
    private PinterestMetrics pinterestMetrics = null;

    @Autowired
    private ApplicationContext context;

    @Autowired
    JdbcTemplate jdbcTemplate;

    private String downloadFolder = "/disk2/lxrmarketplace";

    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }
    final static String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
    final String USER_AGENT_MOBILE = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

    public Map<String, Object> getSocialAnalysis(long userId,
            String presentDate, String previousDate) {
        TwitterMetrics twitterData = null;
        FacebookMetrics facebookData = null;
        YouTubeMetrics youtubeData = null;
        GoogleMetrics googleplusData = new GoogleMetrics();
        InstagramMetrics instagramData = new InstagramMetrics();
        PinterestMetrics pinterestData = new PinterestMetrics();
        Map<String, Object> socialmedia = new LinkedHashMap<>();

        int previoussqlRowSetCount = 0;
        int presentsqlRowSetCount = 0;
        try {
            SqlRowSet previoussqlRowSet = getSocialChannelsMetricsData(previousDate, userId);
            /*SqlRowSet presentsqlRowSet = getPresentDayData(presentDate, userId);*/
            SqlRowSet presentsqlRowSet = getSocialChannelsMetricsData(presentDate, userId);
            if (presentsqlRowSet != null) {
                while ((presentsqlRowSet.next()) && (presentsqlRowSetCount == 0)) {
                    twitterData = new TwitterMetrics();
                    try {
                        if ((presentsqlRowSet.getLong("smt_tw_audience") != -1) && (presentsqlRowSet.getLong("smt_tw_activity") != -1)) {
                            twitterData.setTwitterAccountStatus(true);
                            twitterData.setFollowers(presentsqlRowSet.getLong("smt_tw_audience"));
                            twitterData.setTweets(presentsqlRowSet.getLong("smt_tw_activity"));
                            twitterData.setFollowing(presentsqlRowSet.getLong("smt_tw_following"));
                            twitterData.setFavourites(presentsqlRowSet.getLong("smt_tw_favourites"));
                            if (presentsqlRowSet.getString("tw_recent_post") != null && !presentsqlRowSet.getString("tw_recent_post").trim().isEmpty()) {
                                twitterData.setRecentTweetText(presentsqlRowSet.getString("tw_recent_post").trim());
                            }
                            if (presentsqlRowSet.getString("tw_post_image_path") != null && !presentsqlRowSet.getString("tw_post_image_path").trim().isEmpty()) {
                                twitterData.setReadPath(presentsqlRowSet.getString("tw_post_image_path").trim());
                                twitterData.setImageStatus(true);
                            }
                            twitterData.setRecentTweetFavourites(presentsqlRowSet.getLong("tw_post_favorites"));
                            twitterData.setRecentTweetRetweets(presentsqlRowSet.getLong("tw_post_retweets"));
                            twitterData.setEngagement((twitterData.getRecentTweetFavourites()) + (twitterData.getRecentTweetRetweets()));
                            twitterData.setAudienceEngaged(Common.getAudienceEngaged(twitterData.getEngagement(), twitterData.getFollowers()));
                            if (presentsqlRowSet.getString("tw_post_date") != null && !presentsqlRowSet.getString("tw_post_date").trim().isEmpty()) {
                                twitterData.setTweetDate(presentsqlRowSet.getString("tw_post_date"));
                            }
                            twitterData.setTwitterHashTag(presentsqlRowSet.getString("tw_hash_tag"));

                        } else {
                            twitterData.setTwitterAccountStatus(false);
                        }
                    } catch (InvalidResultSetAccessException e) {
                        twitterData.setTwitterAccountStatus(false);
                        logger.error("Exception in retrieving twitter metrics", e);
                    }
                    facebookData = new FacebookMetrics();
                    try {
                        if (presentsqlRowSet.getLong("smt_fb_audience") != -1) {
                            facebookData.setLikesCount(presentsqlRowSet.getLong("smt_fb_audience"));
                            if (presentsqlRowSet.getString("fb_recent_post") != null && !presentsqlRowSet.getString("fb_recent_post").trim().isEmpty()) {
                                facebookData.setPostdescription(presentsqlRowSet.getString("fb_recent_post").trim());
                            }
                            facebookData.setPostLikes(presentsqlRowSet.getLong("fb_post_likes"));
                            facebookData.setPostShares(presentsqlRowSet.getLong("fb_post_shares"));
                            facebookData.setPostcomments(presentsqlRowSet.getLong("fb_post_comments"));
                            facebookData.setEngagement((facebookData.getPostLikes() + facebookData.getPostShares() + facebookData.getPostcomments()));
                            facebookData.setAudienceEngaged(Common.getAudienceEngaged(facebookData.getEngagement(), facebookData.getLikesCount()));
                            if (presentsqlRowSet.getString("fb_post_image_path") != null && !presentsqlRowSet.getString("fb_post_image_path").trim().isEmpty()) {
                                facebookData.setPostImagePath(presentsqlRowSet.getString("fb_post_image_path").trim());
                                facebookData.setPostImageStatus(true);
                            }
                            if (presentsqlRowSet.getString("fb_post_date") != null && !presentsqlRowSet.getString("fb_post_date").trim().isEmpty()) {
                                facebookData.setPostCreatedTime(presentsqlRowSet.getString("fb_post_date"));
                            }
                            facebookData.setFacebookAccountStatus(true);
                        } else {
                            facebookData.setFacebookAccountStatus(false);
                        }
                    } catch (InvalidResultSetAccessException e) {
                        facebookData.setFacebookAccountStatus(false);
                        logger.error("Exception in retrieving facebook metrics", e);
                    }
                    youtubeData = new YouTubeMetrics();
                    try {
                        if (presentsqlRowSet.getLong("smt_yt_audience") != -1) {
                            youtubeData.setSubscribers(presentsqlRowSet.getLong("smt_yt_audience"));
                            youtubeData.setVideos(presentsqlRowSet.getLong("smt_yt_activity"));
                            youtubeData.setViews(presentsqlRowSet.getLong("smt_yt_views"));
                            if (presentsqlRowSet.getString("yt_post_date") != null && !presentsqlRowSet.getString("yt_post_date").trim().isEmpty()) {
                                youtubeData.setUserPostDate(presentsqlRowSet.getString("yt_post_date").trim());
                            }
                            if (presentsqlRowSet.getString("yt_recent_post") != null && !presentsqlRowSet.getString("yt_recent_post").trim().isEmpty()) {
                                youtubeData.setRecentMediaTitle(presentsqlRowSet.getString("yt_recent_post").trim());
                            }
                            if (presentsqlRowSet.getString("yt_image_path") != null && !presentsqlRowSet.getString("yt_image_path").trim().isEmpty()) {
                                youtubeData.setYoutubeUserLogPath(presentsqlRowSet.getString("yt_image_path").trim());
                                youtubeData.setImageStatus(true);
                            }
                            youtubeData.setRecentMediaComments(presentsqlRowSet.getLong("yt_post_comments"));
                            youtubeData.setRecentMediaLikes(presentsqlRowSet.getLong("yt_post_likes"));
                            youtubeData.setRecentMediaviews(presentsqlRowSet.getLong("yt_post_views"));
                            youtubeData.setEngagement((youtubeData.getRecentMediaComments() + youtubeData.getRecentMediaDisLikes() + youtubeData.getRecentMediaLikes() + youtubeData.getRecentMediaviews()));
                            youtubeData.setAudienceEngaged((Common.getAudienceEngaged(youtubeData.getEngagement(), youtubeData.getSubscribers())));
                            youtubeData.setYoutubeAccountStatus(true);
                        } else {
                            youtubeData.setYoutubeAccountStatus(false);
                        }
                    } catch (InvalidResultSetAccessException e) {
                        youtubeData.setYoutubeAccountStatus(false);
                        logger.error("Exception in retrieving youtube metrics", e);
                    }
                    try {
                        if (previoussqlRowSet != null) {
                            while ((previoussqlRowSet.next()) && (previoussqlRowSetCount == 0)) {
                                twitterData.setChange((getMetricChange(presentsqlRowSet.getLong("smt_tw_audience"),
                                        previoussqlRowSet.getLong("smt_tw_audience"))));
                                twitterData.setTwitterFollowingChange((getMetricChange(presentsqlRowSet.getLong("smt_tw_following"),
                                        previoussqlRowSet.getLong("smt_tw_following"))));
                                twitterData.setTwitterFavouritesChange((getMetricChange(presentsqlRowSet.getLong("smt_tw_favourites"),
                                        previoussqlRowSet.getLong("smt_tw_favourites"))));

                                twitterData.setTwitteractivityChange((getMetricChange(presentsqlRowSet
                                        .getLong("smt_tw_activity"),
                                        previoussqlRowSet.getLong("smt_tw_activity"))));
                                facebookData.setChange((getMetricChange(presentsqlRowSet.getLong("smt_fb_audience"),
                                        previoussqlRowSet.getLong("smt_fb_audience"))));
                                facebookData.setFaceBookActivityChange((getMetricChange(presentsqlRowSet
                                        .getLong("smt_fb_activity"),
                                        previoussqlRowSet.getLong("smt_fb_activity"))));
                                youtubeData.setChange((getMetricChange(presentsqlRowSet.getLong("smt_yt_audience"),
                                        previoussqlRowSet.getLong("smt_yt_audience"))));
                                youtubeData.setYouTubeViewsChange((getMetricChange(presentsqlRowSet.getLong("smt_yt_views"),
                                        previoussqlRowSet.getLong("smt_yt_views"))));
                                youtubeData.setYouTubeActivityChange((getMetricChange(presentsqlRowSet.getLong("smt_yt_activity"),
                                        previoussqlRowSet.getLong("smt_yt_activity"))));
                                previoussqlRowSetCount++;
                            }
                        }
                    } catch (InvalidResultSetAccessException e) {
                        logger.error("Exception in calculating growth metrics", e);
                    }

                    presentsqlRowSetCount++;
                }

            } else {
                twitterData = new TwitterMetrics();
                facebookData = new FacebookMetrics();
                googleplusData = new GoogleMetrics();
                youtubeData = new YouTubeMetrics();
                instagramData = new InstagramMetrics();
                pinterestData = new PinterestMetrics();
            }
            socialmedia.put("twittermetrics", twitterData);
            socialmedia.put("facebookmetrics", facebookData);
            socialmedia.put("googleplusmetrics", googleplusData);
            socialmedia.put("youtubemetrics", youtubeData);
            socialmedia.put("instagrammetrics", instagramData);
            socialmedia.put("pinterestmetrics", pinterestData);
        } catch (InvalidResultSetAccessException e) {
            socialmedia.put("twittermetrics", twitterData);
            socialmedia.put("facebookmetrics", facebookData);
            socialmedia.put("googleplusmetrics", googleplusData);
            socialmedia.put("youtubemetrics", youtubeData);
            socialmedia.put("instagrammetrics", instagramData);
            socialmedia.put("pinterestmetrics", pinterestData);
            logger.error("Exception in getScoialAnalysis ", e);

        }
        return socialmedia;
    }

    public SqlRowSet getSocialChannelsMetricsData(String previousDate, long socialUserId) {

        SqlRowSet sqlRowSet = null;
        String sqlQuery = "SELECT s.id, "
                + "t.smt_tw_audience, t.smt_tw_activity, t.smt_tw_following, t.smt_tw_favourites, "
                + "t.tw_recent_post, tw_post_retweets, t.tw_post_favorites, t.tw_post_image_path, t.tw_post_date, "
                + "t.insert_date AS twitter_insert_Date, t.tw_hash_tag, "
                + "f.smt_fb_audience, f.smt_fb_activity, fb_recent_post, "
                + "f.fb_post_likes, fb_post_comments, fb_post_shares, f.fb_post_image_path, "
                + "f.fb_post_date, f.insert_date AS facebook_insert_Date,"
                + "y.smt_yt_audience, y.smt_yt_activity, y.smt_yt_views, "
                + "y.yt_recent_post, y.yt_post_views, y.yt_post_likes, "
                + "y.yt_post_comments, y.yt_image_path, y.yt_post_date, "
                + "y.insert_date AS youtube_insert_Date, "
                + "s.id AS common_id FROM social_media_user s "
                + "INNER JOIN smt_twitter t ON s.id = t.media_user_id AND DATE_FORMAT(t.insert_date, '%Y-%m-%d') = '" + previousDate + "' "
                + "INNER JOIN smt_facebook f ON s.id = f.media_user_id AND DATE_FORMAT(f.insert_date, '%Y-%m-%d') = '" + previousDate + "' "
                + "INNER JOIN smt_youtube y ON s.id = y.media_user_id AND DATE_FORMAT(y.insert_date, '%Y-%m-%d') = '" + previousDate + "' "
                + "WHERE s.id = '" + socialUserId + "' AND s.is_deleted = 0";
        try {																	// toDate);
            sqlRowSet = this.jdbcTemplate.queryForRowSet(sqlQuery);
        } catch (DataAccessException e) {
            logger.info("Query to fetch social channels meida data for Date:" + sqlQuery);
            logger.error("DataAccessException in  getSocialChannelsMetricsData for social user id::" + socialUserId + ", and cause:: " + e);
        }
        return sqlRowSet;
    }

    public int getMetricChange(long present, long previous) {
        int growth;
        long tempSub;
        if (present > 0 && previous > 0) {
            tempSub = present - previous;
            growth = (int) tempSub;
        } else if (present > 0 && previous <= 0) {
            growth = (int) present;
        } else if (present <= 0 && previous > 0) {
            tempSub = present - previous;
            growth = (int) tempSub;
        } else {
            growth = 0;
        }
        if ((growth == -0) || (growth == -100)) {
            growth = 0;
        }
        return growth;
    }

    public double getAudieneceGrwoth(double present, double previous) {
        double grwoth = Common.round((((previous - present) / (present + previous)) * 100), 6);
        return grwoth;
    }

    public String getPreviousDate(Date myDate, int format) {
        SimpleDateFormat dateFormat;
        if (format == 1) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }

    public String convertDateToString(Date indate, int format) {
        String dateString;
        SimpleDateFormat dateFormat;
        if (format == 1) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
        }
        try {
            dateString = dateFormat.format(indate);
        } catch (Exception ex) {
            dateString = dateFormat.format(Calendar.getInstance().getTime());
            logger.error("Exception for convertDateToString cause: " + ex.getMessage());
        }
        return dateString;
    }

    /*Same Method will used to insert user domain of a user and update url's of login user */
    public Long insertSocialMediaUser(final long userId,
            final Timestamp startTime, final String mediaUrl,
            final boolean userLoggedIn, final boolean isUserDomain, final boolean is_deleted,
            final boolean weekly_report) {
        final String query = "insert into social_media_user(user_id,url,user_date,is_logged_in,is_userdomain,is_deleted,weekly_report) values (?,?,?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                this.jdbcTemplate.update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                            query, new String[]{"id"});
                    statement.setLong(1, userId);
                    statement.setString(2, mediaUrl);
                    statement.setTimestamp(3, startTime);
                    statement.setBoolean(4, userLoggedIn);
                    statement.setBoolean(5, isUserDomain);
                    statement.setBoolean(6, is_deleted);
                    statement.setBoolean(7, weekly_report);
                    return statement;
                }, keyHolder);
            }
        } catch (DataAccessException ex) {
            logger.error("Exception for inserting socialmedia user: ", ex);
        }
        return (Long) keyHolder.getKey();
    }

    public void deletePreviousSocialUserdata(long lXRMUserId, boolean userLoggedIn) {
        String query;
        if (userLoggedIn) {
            query = "select id from social_media_user where user_id =" + lXRMUserId + " and is_logged_in = 1 and is_deleted=0 ";
        } else {
            query = "select id from social_media_user where user_id =" + lXRMUserId + " and is_logged_in = 0 and is_deleted=0 ";
        }
        try {
            List<Integer> socialUserIdList = this.jdbcTemplate.queryForList(query, Integer.class);
            disableSocialMediaUserGuestData(socialUserIdList, lXRMUserId);
        } catch (DataAccessException e) {
            logger.error("Exception for deletePreviousSocialUserdata: ", e);
        }
    }

    public void insertTwitterAccount(final long userId,
            final TwitterMetrics twittermetrics, final Timestamp insertdate) {
        final String query = "insert into smt_twitter(media_user_id,smt_tw_audience,smt_tw_activity,smt_tw_following,smt_tw_favourites,"
                + "tw_recent_post,tw_post_retweets,tw_post_favorites,tw_post_image_path,tw_post_date,tw_hash_tag,insert_date)"
                + " values (?,?,?,?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_twitter(media_user_id,smt_tw_audience,smt_tw_activity,insert_date) values (?,?,?,?)";
        try {
            synchronized (this) {
                if (twittermetrics.isTwitterAccountStatus()) {
                    logger.debug("Insertinging entry in smt_twitter table for media url " + userId + "and query is");
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, twittermetrics.getFollowers());
                        statement.setLong(3, twittermetrics.getTweets());
                        statement.setLong(4, twittermetrics.getFollowing());
                        statement.setLong(5, twittermetrics.getFavourites());
                        if (twittermetrics.getRecentTweetText() != null) {
                            statement.setString(6, getPostText(twittermetrics.getRecentTweetText()));
                        } else {
                            statement.setString(6, null);
                        }
                        statement.setLong(7,
                                twittermetrics.getRecentTweetRetweets());
                        statement.setLong(8,
                                twittermetrics.getRecentTweetFavourites());
                        statement.setString(9, twittermetrics.getReadPath());
                        statement.setString(10, twittermetrics.getTweetDate());
                        statement.setString(11, twittermetrics.getTwitterHashTag());
                        statement.setTimestamp(12, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setLong(3, -1);
                        statement.setTimestamp(4, insertdate);
                        return statement;
                    });
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception inserting for twitter account: ", ex);
        }

    }

    public void insertFacebookAccount(final long userId,
            final FacebookMetrics facebookmetrics, final Timestamp insertdate) {
        final String query = "insert into smt_facebook(media_user_id,smt_fb_audience,smt_fb_activity,"
                + "fb_recent_post,fb_post_likes,fb_post_comments,"
                + "fb_post_shares,fb_post_image_path,fb_post_date,insert_date) values (?,?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_facebook(media_user_id,smt_fb_audience,insert_date) values (?,?,?)";
        try {
            synchronized (this) {
                if (facebookmetrics.isFacebookAccountStatus()) {
                    logger.debug("Insertinging entry in smt_facebook table for user " + userId + " and query " + query);
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, facebookmetrics.getLikesCount());
                        statement.setLong(3, 0);
                        if (facebookmetrics.getPostdescription() != null) {
                            statement.setString(4, getPostText(facebookmetrics.getPostdescription()));
                        } else {
                            statement.setString(4, null);
                        }
                        statement.setLong(5, facebookmetrics.getPostLikes());
                        statement.setLong(6, facebookmetrics.getPostcomments());
                        statement.setLong(7, facebookmetrics.getPostShares());
                        statement.setString(8, facebookmetrics.getPostImagePath());
                        statement.setString(9, facebookmetrics.getPostCreatedTime());
                        statement.setTimestamp(10, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setTimestamp(3, insertdate);
                        return statement;
                    });
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in inserting facebook account: ", ex);
        }

    }

    public void insertGooglePlusAccount(final long userId,
            final GoogleMetrics googleplusmetrics, final Timestamp insertdate) {
        final String query = "insert into smt_googleplus(media_user_id,smt_gp_audience,"
                + "gp_recent_post,gp_post_replies,gp_post_reshares,"
                + "gp_post_plusones,gp_post_image_path,gp_post_date,insert_date) values (?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_googleplus(media_user_id,smt_gp_audience,insert_date) values (?,?,?)";
        try {
            synchronized (this) {
                if (googleplusmetrics.isGoogleAccountStatus()) {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, googleplusmetrics.getFollowers());
                        if (googleplusmetrics.getRecentPostContent() != null) {
                            statement.setString(3, getPostText(googleplusmetrics.getRecentPostContent()));
                        } else {
                            statement.setString(3, null);
                        }
                        statement.setLong(4, googleplusmetrics.getRecentPostReplies());
                        statement.setLong(5, googleplusmetrics.getRecentPostResharers());
                        statement.setLong(6, googleplusmetrics.getRecentPosplusoners());
                        statement.setString(7, googleplusmetrics.getGoogleImagePath());
                        statement.setString(8, googleplusmetrics.getGooglePostDate());
                        statement.setTimestamp(9, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setTimestamp(3, insertdate);
                        return statement;
                    });
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception inserting googlePlus account: ", ex);
        }

    }

    public void insertYouTubeAccount(final long userId,
            final YouTubeMetrics youtubemetrics, final Timestamp insertdate) {
        final String query = "insert into smt_youtube(media_user_id,smt_yt_audience,smt_yt_activity,smt_yt_views,"
                + "yt_recent_post,yt_post_views,yt_post_likes,"
                + "yt_post_comments,yt_image_path,yt_post_date,insert_date) values (?,?,?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_youtube(media_user_id,smt_yt_audience,insert_date) values (?,?,?)";
        try {
            synchronized (this) {
                if (youtubemetrics.isYoutubeAccountStatus()) {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, youtubemetrics.getSubscribers());
                        statement.setLong(3, youtubemetrics.getVideos());
                        statement.setLong(4, youtubemetrics.getViews());
                        if (youtubemetrics.getRecentMediaTitle() != null) {
                            statement.setString(5, getPostText(youtubemetrics.getRecentMediaTitle()));
                        } else {
                            statement.setString(5, null);
                        }
                        statement.setLong(6, youtubemetrics.getRecentMediaviews());
                        statement.setLong(7, youtubemetrics.getRecentMediaLikes());
                        statement.setLong(8, youtubemetrics.getRecentMediaComments());
                        statement.setString(9, youtubemetrics.getYoutubeUserLogPath());
                        statement.setString(10, youtubemetrics.getUserPostDate());
                        statement.setTimestamp(11, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setTimestamp(3, insertdate);
                        return statement;
                    });
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception inserting youTube account: ", ex);
        }
    }

    public void insertInstagramAccount(final long userId,
            final InstagramMetrics instagrammetrics, final Timestamp insertdate) {
        final String query = "insert into smt_instagram(media_user_id,smt_inst_audience,smt_inst_activity,smt_inst_following,"
                + "inst_recent_post,inst_post_likes,inst_post_comments,"
                + "inst_post_image_path,inst_post_date,insert_date) values (?,?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_instagram(media_user_id,smt_inst_audience,insert_date) values (?,?,?)";
        try {
            synchronized (this) {
                if (instagrammetrics.isInstagramAccountStatus()) {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        //String tempInsta="bestbuygirlUr so beautiful😍😍😍😍😍😍 @elaromai #Nathanforyou #Comedycentral #WCW #Bestbuygirl";
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, instagrammetrics.getFollowers());
                        statement.setLong(3, instagrammetrics.getPosts());
                        statement.setLong(4, instagrammetrics.getFollwoing());
                        if (instagrammetrics.getPostText() != null) {
                            statement.setString(5, getPostText(instagrammetrics.getPostText()));
                        } else {
                            statement.setString(5, null);
                        }
                        statement.setLong(6, instagrammetrics.getRecentMediaLikes());
                        statement.setLong(7, instagrammetrics.getRecentMediaComments());
                        statement.setString(8, instagrammetrics.getInstapostImageUrl());
                        statement.setString(9, instagrammetrics.getPostDate());
                        statement.setTimestamp(10, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setTimestamp(3, insertdate);
                        return statement;
                    });
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception when inserting instagram account: ", ex);
        }
    }

    public void insertPinterestAccount(final long userId,
            final PinterestMetrics pinterestMetrics, final Timestamp insertdate) {
        /*Altered query removing likes metrics API upgraded.*/
 /*final String query = "insert into smt_pinterest(media_user_id,smt_pin_audience,smt_pin_activity,smt_pin_likes,smt_pin_boards,"
                + "smt_pin_following,pin_recent_post,pin_post_likes,pin_post_comments,pin_post_repins,"
                + "pin_post_image_path,pin_post_date,insert_date) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";*/
        final String query = "insert into smt_pinterest(media_user_id,smt_pin_audience,smt_pin_activity,smt_pin_boards,"
                + "smt_pin_following,pin_recent_post,pin_post_likes,pin_post_comments,pin_post_repins,"
                + "pin_post_image_path,pin_post_date,insert_date) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        final String noDataQuery = "insert into smt_pinterest(media_user_id,smt_pin_audience,insert_date) values (?,?,?)";
        try {
            synchronized (this) {
                if (pinterestMetrics.isPinterestAccountStatus()) {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                query, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setInt(2, pinterestMetrics.getFollowers());
                        statement.setInt(3, pinterestMetrics.getPins());
                        /*statement.setInt(4, pinterestMetrics.getLikes());*/
                        statement.setInt(4, pinterestMetrics.getBoards());
                        statement.setInt(5, pinterestMetrics.getFollowing());
                        if (pinterestMetrics.getPostText() != null) {
                            statement.setString(6, getPostText(pinterestMetrics.getPostText()));
                        } else {
                            statement.setString(6, null);
                        }
                        statement.setInt(7, pinterestMetrics.getPostLikes());
                        statement.setInt(8, pinterestMetrics.getPostComments());
                        statement.setInt(9, pinterestMetrics.getPostRepins());
                        statement.setString(10, pinterestMetrics.getPinterestImageUrl());
                        statement.setString(11, pinterestMetrics.getPostDate());
                        statement.setTimestamp(12, insertdate);
                        return statement;
                    });
                } else {
                    this.jdbcTemplate.update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                                noDataQuery, new String[]{"id"});
                        statement.setLong(1, userId);
                        statement.setLong(2, -1);
                        statement.setTimestamp(3, insertdate);
                        return statement;
                    });

                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception for inserting pinterest account: ", ex);
        }

    }

    public void updateGuestEntryToLoggedIn(long socialUserId, long userId) {
        String query = "update social_media_user set is_logged_in = 1, user_id = ?  where id = ? and is_deleted = 0 ";
        Object[] valLis = new Object[2];
        valLis[0] = userId;
        valLis[1] = socialUserId;
        this.jdbcTemplate.update(query, valLis);
    }

    public void updateUserWeeklyReport(long userId) {
        String query = "update social_media_user set weekly_report=1  where id = ? and is_deleted = 0";
        Object[] valLis = new Object[1];
        valLis[0] = userId;
        this.jdbcTemplate.update(query, valLis);
    }

    public void updateUserLoginDate(Timestamp loginDate, long userId) {
        String query = "update social_media_user set is_logged_in = 1, last_login_date= ?  where user_id = ? and is_deleted = 0";
        Object[] valLis = new Object[2];
        valLis[0] = userId;
        valLis[1] = loginDate;
        this.jdbcTemplate.update(query, valLis);
    }

    public Timestamp fetchSocialUserLastLoginDate(long id) {
        String query = "select last_login_date from social_media_user  where user_id = '" + id + "' and is_logged_in = 1 and is_deleted = 0 and is_userdomain = 1";
        logger.debug("Query to fetchSocialUserLastLoginDate: " + query);
        Timestamp loginDate = null;
        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    loginDate = sqlRowSet.getTimestamp("last_login_date");
                }
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in fetchSocialUserLastLoginDate: ", ex);
        }
        return loginDate;
    }

    public void updateUserSettings(final long socialUserId, boolean isUnsubscribe, boolean isWeeklyReport, Timestamp updatedTime) {
        final String query = "update social_media_user set weekly_report = ?,tool_unsubscribe = ?,tool_unsubsribe_date = ?  where user_id = ? and is_deleted = 0 ";
        Object[] valLis = new Object[4];
        valLis[0] = isWeeklyReport;
        valLis[1] = isUnsubscribe;
        valLis[2] = updatedTime;
        valLis[3] = socialUserId;
        this.jdbcTemplate.update(query, valLis);
    }

    public static Map<String, String> getSocialMediaUserNamesfromURL(String mediaUrl) {
        Map<String, String> userNames = new HashMap<>();
        try {
            List<String> links = extractLinks(mediaUrl);

            links.stream().forEach((link) -> {
                if (((link).contains("https://twitter.com"))
                        || ((link).contains("http://twitter.com"))
                        || ((link).contains("https://www.twitter.com"))
                        || ((link).contains("http://www.twitter.com"))) {
                    if (!(userNames.containsKey("Twitter"))) {
                        if (!link.contains("twitter.com/share")) {
                            String tempTwitterLink = getUrlValidation(link);
                            String TwitteruserName = getUserNameFromLink(tempTwitterLink);/*tempTwitterLink).substring(20);*/
                            if (TwitteruserName != null) {
                                if (TwitterMetricsAPIService.checkTwitterUserExistence(TwitteruserName)) {
                                    userNames.put("Twitter", TwitteruserName);
                                }
                            }
                        }/*End of share if condition*/
                    }
                } else if ((link).contains("twitter.com")) {
                    if (!(userNames.containsKey("Twitter"))) {
                        if (!link.contains("twitter.com/share")) {
                            String tempTwitterLink = getUrlValidation(link);
                            String TwitteruserName = getUserNameFromLink(tempTwitterLink);/*(tempTwitterLink).substring(19);*/
                            if (TwitteruserName != null) {
                                if (TwitterMetricsAPIService.checkTwitterUserExistence(TwitteruserName)) {
                                    userNames.put("Twitter", TwitteruserName);
                                }
                            }
                        }
                    }
                } else if (((link).contains("https://www.facebook.com"))
                        || ((link).contains("http://www.facebook.com"))
                        || ((link).contains("http://facebook.com"))
                        || ((link).contains("https://facebook.com"))) {
                    if (!(userNames.containsKey("facebook"))) {
                        if (!link.contains("www.facebook.com/sharer/")) {
                            String tempFaceBookLink = getUrlValidation(link);
                            String fbuserName = getUserNameFromLink(tempFaceBookLink);/*(tempFaceBookLink).substring(25);*/
                            if (fbuserName != null) {
                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbuserName)) {
                                    userNames.put("facebook", fbuserName);
                                } else {
                                    String fbChoice1 = getFaceBookUserName(tempFaceBookLink, "-");
                                    if (fbChoice1 != null) {
                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice1)) {
                                            userNames.put("facebook", fbChoice1);
                                        } else {
                                            String fbChoice2 = getFaceBookUserName(tempFaceBookLink, "/");
                                            if (fbChoice2 != null) {
                                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice2)) {
                                                    userNames.put("facebook", fbChoice2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if ((link).contains("facebook.com")) {
                    if (!(userNames.containsKey("facebook"))) {
                        if (!link.contains("facebook.com/sharer/")) {
                            String tempFaceBookLink = getUrlValidation(link);
                            String fbuserName = getUserNameFromLink(tempFaceBookLink);/*(tempFaceBookLink).substring(25);*/
                            if (fbuserName != null) {
                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbuserName)) {
                                    userNames.put("facebook", fbuserName);
                                } else {
                                    String fbChoice1 = getFaceBookUserName(tempFaceBookLink, "-");
                                    if (fbChoice1 != null) {
                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice1)) {
                                            userNames.put("facebook", fbChoice1);
                                        } else {
                                            String fbChoice2 = getFaceBookUserName(tempFaceBookLink, "/");
                                            if (fbChoice2 != null) {
                                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice2)) {
                                                    userNames.put("facebook", fbChoice2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (((link).contains("http://www.youtube.com/user"))
                        || ((link).contains("https://www.youtube.com/user"))
                        || ((link).contains("https://www.youtube.com/channel"))
                        || ((link).contains("http://www.youtube.com/channel"))
                        || ((link).contains("http://www.youtube.com/c/"))) {
                    if ((!((link).contains("www.youtube.com/v/"))) && (!((link).contains("youtube.com/embed/")))) {
                        if (!(userNames.containsKey("YouTube"))) {
                            String tempYouTubeLink = getUrlValidation(link);
                            String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/*getYouTubeUserName(tempYouTubeLink);*/
                            if (YouTubeuserName != null) {
                                if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                    userNames.put("YouTube", YouTubeuserName);
                                } else {
                                    String yTUsrName = getUserNameFromLink(link);/*getYouTubeUserName(tempYouTubeLink);*/
                                    if (yTUsrName != null) {
                                        if (YouTubeMetricsAPIService.checkYoutubeUserName(yTUsrName)) {
                                            userNames.put("YouTube", yTUsrName);
                                        }
                                    }

                                }
                            }
                        }
                    }
                } else if (((link).contains("http://www.youtube.com"))
                        || ((link).contains("https://www.youtube.com"))) {
                    if ((!((link).contains("www.youtube.com/v/"))) && (!((link).contains("youtube.com/embed/")))) {
                        if (!(userNames.containsKey("YouTube"))) {
                            String tempYouTubeLink = getUrlValidation(link);
                            String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/*getYouTubeUserName(tempYouTubeLink);*/
                            if (YouTubeuserName != null) {
                                if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                    userNames.put("YouTube", YouTubeuserName);
                                }
                            }
                        }
                    }
                } else if ((link).contains("youtube.com")) {
                    if ((!((link).contains("www.youtube.com/v/"))) && (!((link).contains("youtube.com/embed/")))) {
                        if (!(userNames.containsKey("YouTube"))) {
                            String tempYouTubeLink = getUrlValidation(link);
                            String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/* getYouTubeUserName(tempYouTubeLink);*/
                            if (YouTubeuserName != null) {
                                if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                    userNames.put("YouTube", YouTubeuserName);
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            logger.error("Exception for getSocialMediaUserNames: ", e);
        }
        return userNames;
    }

    public static List<String> extractLinks(String queryURL) {
        ArrayList<String> result = new ArrayList<>();
        Document doc = null;
        int statusCode;
        StringBuilder pageHTML = new StringBuilder("");
        StringBuilder redirectedUrl = new StringBuilder("");
        String httpCurrentUrl = "";
        try {
            if (!(queryURL == null || queryURL.equals(""))) {
                if (queryURL.startsWith("http://") || queryURL.startsWith("https://")) {
                    httpCurrentUrl = queryURL;
                } else {
                    httpCurrentUrl = "http://" + queryURL;
                }
                statusCode = Common.getCurrentStatusCodeAndHTML(queryURL, redirectedUrl, pageHTML);
                if (statusCode / 100 == 3) {
                    try {
                        URL urlredirected = new URL(redirectedUrl.toString());
                        URL urlCurrent = new URL(httpCurrentUrl);
                        if (urlredirected.getHost().equals(urlCurrent.getHost())) {
                            statusCode = Common.getCurrentStatusCodeAndHTML(queryURL, redirectedUrl, pageHTML);
                        }
                    } catch (MalformedURLException e) {
                        logger.error("MalformedURLException for fetching status code: ", e);
                    }

                }
            }
            if (!pageHTML.toString().equals("")) {
                doc = Jsoup.parse(pageHTML.toString(), httpCurrentUrl);
            }
            if (doc != null) {
                Elements links = doc.select("a[href]");
                Elements media = doc.select("[src]");
                Elements imports = doc.select("link[href]");
                Elements areaLinks = doc.select("area[href]");
                // href ...
                links.stream().forEach((link) -> {
                    result.add(link.attr("abs:href"));
                });
                // img ...
                media.stream().forEach((src) -> {
                    result.add(src.attr("abs:src"));
                });
                // js, css, ...
                imports.stream().forEach((link) -> {
                    result.add(link.attr("abs:href"));
                });
                areaLinks.stream().forEach((link) -> {
                    result.add(link.attr("abs:href"));
                });
            }
        } catch (Exception e) {
            logger.error("Exception for links extracting: ", e);
        }
        return result;
    }

    public static String getGoogleUserName(String url) {
        String username = null;
        int googleindex, comIndex, secondslash;
        try {
            googleindex = url.lastIndexOf(".com/");
            String comlink = url.substring(googleindex);
            comIndex = comlink.indexOf('/');
            String userlink = comlink.substring((comIndex + 1));
            if (userlink.startsWith("u/0/")) {
                userlink = userlink.replaceAll("u/0/", " ");
                userlink = userlink.trim();
            }
            if ((userlink).contains("/")) {
                secondslash = userlink.lastIndexOf('/');
                username = userlink.substring(0, secondslash);
                return username;
            } else {
                if (userlink.charAt(0) == '+') {
                    userlink = userlink.replace("+", " ");
                    userlink = userlink.trim();
                }
                if ((getRegularExpressionIndex(userlink)) != 0) {
                    userlink = userlink.substring(0, getRegularExpressionIndex(userlink));
                }
                username = userlink.substring(0);
                boolean plusIndcation = StringUtils.isNumeric(username);
                if (!plusIndcation) {
                    username = "+" + username;
                }
            }
        } catch (Exception e) {
            logger.error("Exception for getGoogleUserName username: " + username + ", cause: ", e);
        }
        return username;
    }

    /**
     * @param pdfDoc
     * @param heading
     * @param description
     * @param normalFont
     * @param orangeFont
     */
    public void addTitlePage(com.itextpdf.text.Document pdfDoc, String heading,
            String description, Font orangeFont, Font normalFont) {
        try {
            Paragraph paragraph = new Paragraph();
            paragraph.add(new Paragraph(heading, orangeFont));
            // paragraph.setSpacingBefore(10f);
            paragraph.setSpacingAfter(30f);
            paragraph.add(new Paragraph(description, normalFont));
            // paragraph.setSpacingBefore(13f);
            paragraph.setSpacingAfter(2f);
            pdfDoc.add(paragraph);
        } catch (DocumentException e) {
            logger.error("DocumentException for addTitlePage: ", e);
        }
    }

    public Paragraph addHashTag(String queryURL,
            String hashTag, Font urlFont, Font normalFont, boolean accountStatus) {
        Paragraph paragraph = new Paragraph();
        queryURL = trimWebsitesName(queryURL);
        if (queryURL != null) {
            paragraph.add(new Paragraph(trimWebsitesName(queryURL), urlFont));
        } else {
            paragraph.add(new Paragraph(trimWebsitesName(queryURL), urlFont));
        }
        paragraph.setSpacingAfter(5f);
        if (!hashTag.equalsIgnoreCase("NoHashTag")) {
            paragraph.add(new Paragraph(hashTag, normalFont));
        } else if (accountStatus) {
            paragraph.add(new Paragraph("No recent hash tags available.", normalFont));
        } else {
            paragraph.add(new Paragraph("Not applicable.", normalFont));
        }
        paragraph.setSpacingAfter(8f);
        return paragraph;
    }

    public Timestamp subtractDays(Timestamp d1, long days) {
        return new Timestamp((d1.getTime() - days * 86400000));
    }

    public long getDateDiff(Date firstDate, Date secondDate) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(firstDate);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(secondDate);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        // calendars are the same date
        if (cal1.equals(cal2)) {
            return 0;
        }
        // cal2 is before cal1 - make recursive call
        if (cal2.before(cal1)) {
            return -getDateDiff(secondDate, firstDate);
        }
        // cal1 is before cal2 - count days between
        long days = 0;
        while (cal1.before(cal2)) {
            cal1.add(Calendar.DATE, 1);
            days++;
        }
        return days;
    }

    public String addWaterMark(String fileName, String downloadFolder, long lxrmUserId) {
        try {
            logger.debug("Filepath: " + downloadFolder + fileName);
            PdfReader Read_PDF_To_Watermark = new PdfReader(downloadFolder + fileName);
            fileName = Common.createFileName("LXRMarketplace_Social_Media_Analyzer_Report" + "_" + lxrmUserId) + ".pdf";
            PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark,
                    new FileOutputStream("/disk2/lxrmarketplace/download/" + fileName + ""));
            PdfContentByte underContent = stamp.getUnderContent(1);
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            PdfGState gs = new PdfGState();
            gs.setFillOpacity(0.8f);
            underContent.beginText();
            underContent.setGState(gs);
            underContent.setFontAndSize(bf, 40);
            underContent.setRGBColorFill(224, 224, 224);
            underContent.showTextAligned(com.itextpdf.text.Element.ALIGN_CENTER, "https://lxrmarketplace.com", 300, 350, 0);
            underContent.endText();
            stamp.close();
        } catch (Exception i1) {
            logger.error("Exception when adding WaterMark to report: ", i1);
        }
        return fileName;
    }

    public void deleteGuestUserDataBySocialUserId(long socialUserId) {
        removeDataFromTables("Pinterest", socialUserId);
        removeDataFromTables("Instagram", socialUserId);
        removeDataFromTables("GooglePlus", socialUserId);
        removeDataFromTables("YouTube", socialUserId);
        removeDataFromTables("FaceBook", socialUserId);
        removeDataFromTables("Twitter", socialUserId);
        removeDataFromTables("MediaUser", socialUserId);
    }

    public void removeDataFromTables(String tableName, long socialUserId) {
        String query = null;
        switch (tableName) {
            case "MediaUser":
                query = "delete from social_media_user where id = ?";
                break;
            case "Twitter":
                query = "delete from smt_twitter where media_user_id = ?";
                break;
            case "FaceBook":
                query = "delete from smt_facebook where media_user_id = ?";
                break;
            case "YouTube":
                query = "delete from smt_youtube where media_user_id = ?";
                break;
            case "GooglePlus":
                query = "delete from smt_googleplus where media_user_id = ?";
                break;
            case "Instagram":
                query = "delete from smt_instagram where media_user_id = ?";
                break;
            case "Pinterest":
                query = "delete from smt_pinterest where media_user_id = ?";
                break;
        }
        try {
            this.jdbcTemplate.update(query, new Object[]{socialUserId});
        } catch (Exception ex) {
            logger.error("Exception for removeDataFromTables: ", ex);
        }
    }

    public Timestamp fetchSocialUserLoginDate(long id) {
        String query = "select user_date from social_media_user  where id = '" + id + "' and is_logged_in = 1 and is_deleted=0 and is_userdomain=1";
        Timestamp startDate = null;
        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet.next()) {
                startDate = sqlRowSet.getTimestamp("user_date");
            }
        } catch (DataAccessException e) {
            logger.error("Exception for fetchSocialUserLoginDate: ", e);
        }
        return startDate;
    }

    public boolean getLogInStatus(long userId) {
        String query = "select is_logged_in from social_media_user  where id = " + userId + " and is_deleted = 0 and is_logged_in = 1 and is_userdomain = 1";
        boolean isLogged = false;
        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet.next()) {
                isLogged = sqlRowSet.getBoolean("is_logged_in");
            }
        } catch (DataAccessException e) {
            logger.error("Exception for getLogInStatus: ", e);
        }
        return isLogged;
    }

    public static int getRegularExpressionIndex(String username) {
        Pattern regex = Pattern.compile("[^A-Za-z0-9_.-]");
        Matcher m = regex.matcher(username);
        if (m.find()) {
            return m.start();
        }
        return 0;
    }

    public boolean getUnsubscribtionStatus(long userId) {
        String query = "select tool_unsubscribe from social_media_user  where id = " + userId + " and is_deleted=0 and is_logged_in=1";
        boolean isUnsubScribe = false;
        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet.next()) {
                isUnsubScribe = sqlRowSet.getBoolean("tool_unsubscribe");
            }
        } catch (DataAccessException e) {
            logger.error("Exception for getUnsubscribtionStatus: ", e);
        }
        return isUnsubScribe;
    }

    /**
     * Author:NE16T1213 Date : 14th April 2015
     *
     * @param lxrmId
     * @param isWeeklyReport
     */
    public void updateResubscribeData(final long lxrmId, final boolean isWeeklyReport) {
        logger.debug("Updateing the resubscribe details for user: " + lxrmId);
        final String query = "update social_media_user set weekly_report=?,"
                + "tool_unsubscribe =0 where user_id = ? and is_deleted=0";
        Object[] valLis = new Object[2];
        valLis[0] = isWeeklyReport;
        valLis[1] = lxrmId;
        this.jdbcTemplate.update(query, valLis);
    }

//    public Timestamp checkRecordsForDate(final long mediauserId, final Timestamp checkdate) {
//        String query = "select f.insert_date"
//                + "from social_media_user s,"
//                + "smt_twitter t, smt_facebook f,"
//                + "smt_googleplus g ,smt_instagram i,"
//                + "smt_youtube y where s.id='" + mediauserId + "' and s.id=t.media_user_id"
//                + "and s.id=f.media_user_id and s.id=g.media_user_id and s.id=i.media_user_id"
//                + "and s.id=y.media_user_id and date_format(t.insert_date,'%Y-%m-%d')='" + checkdate + "'"
//                + "and date_format(f.insert_date,'%Y-%m-%d')='" + checkdate + "' and"
//                + "date_format(g.insert_date,'%Y-%m-%d')='" + checkdate + "' and"
//                + "date_format(i.insert_date,'%Y-%m-%d')='" + checkdate + "' and"
//                + "date_format(y.insert_date,'%Y-%m-%d')='" + checkdate + "' "
//                + "and s.is_deleted=0 and s.is_logged_in = 1 and s.tool_unsubscribe=0;";
//
//        Timestamp startDate = null;
//        try {
//            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
//            if (sqlRowSet != null) {
//                while (sqlRowSet.next()) {
//                    startDate = sqlRowSet.getTimestamp("insert_date");
//                }
//            }
//        } catch (DataAccessException e) {
//            logger.error("Exception for checkRecordsForDate: ", e);
//        }
//        return startDate;
//    }
    public void updateIsLoggedIn(long socialUserId) {
        final String query = "update social_media_user set is_logged_in = 0 where user_id= ?";
        Object[] valLis = new Object[1];
        valLis[0] = socialUserId;
        this.jdbcTemplate.update(query, valLis);
    }

    public static String getUrlValidation(String currentUrl) {
        try {
            if (!(currentUrl.equals(""))) {
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                int statusCode = 0;
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                }
                /*logger.info("the redirectd url is befor if in http"+redirectedUrl);*/
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpCurrentUrl;
                        }
                    }
                }
                statusCode = 0;
                redirectedUrl = new StringBuilder("");
                String httpsCurrentUrl = "";
                if (currentUrl.startsWith("https://")) {
                    httpsCurrentUrl = currentUrl;
                    /*statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl);*/
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);

                } else if (!currentUrl.startsWith("http://")) {
                    httpsCurrentUrl = "https://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                }
                /*logger.info("the redirectd url is befor if in https"+redirectedUrl);*/
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpsCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                        httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                        statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpsCurrentUrl;
                        } else {
                            return "invalid";
                        }
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            } else {
                return "invalid";
            }
        } catch (Exception e) {
            logger.error("Exception for getUrlValidation: ", e);
        }
        return "invalid";
    }

    public static int getStatusCodeVersion2(String currentUrl, StringBuilder redirectedUrl) {
        int responseCode = 0, iteration = 0;
        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 5) {
            try {
                URL currentUrlObj = new URL(currentUrl);
                HttpURLConnection currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
                logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                if (responseCode / 100 == 3) {
                    if (iteration != 4) {
                        String redirectedLocation = currentUrlConn.getHeaderField("Location");
                        String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                        if (redirectedLocation != null) {
                            if (!(redirectedLocation.startsWith("http"))) {
                                if (redirectedLocation.startsWith("/")) {
                                    currentUrl = tempUrl + redirectedLocation;
                                } else if (!(redirectedLocation.startsWith("/"))) {
                                    if (currentUrl.endsWith("/")) {
                                        currentUrl = currentUrl + redirectedLocation;
                                    } else {
                                        currentUrl = currentUrl + "/" + redirectedLocation;
                                    }
                                }
                            } else {
                                currentUrl = redirectedLocation;
                            }
                        }
                    } else {
                        redirectedUrl.append("");
                    }
                } else if (responseCode == 200 && iteration != 0) {
                    redirectedUrl.append(currentUrl);
                }
            } catch (Exception e) {
                iteration = 5;
                logger.error("Exception for connection to " + currentUrl + " Cause: " + e.getMessage());
            }
            iteration++;
        }
        return responseCode;
    }

    public List<SocialCompDomains> getSocailMediaUserId(long lxrmUserId, String updateURl, String date, boolean userLoggedIn) {
        String getSocailMediaUserIdQuery;
        List<SocialCompDomains> existedURL = new ArrayList<>();
        /*getSocailMediaUserIdQuery = "select * from social_media_user  where user_id ='"+id+"' and is_logged_in = 1 and is_deleted=0 and tool_unsubscribe=0 and url='"+url+" 'and date_format(social_media_user.user_date,'%Y-%m-%d')='"+date+"'";*/
        if (userLoggedIn) {
            getSocailMediaUserIdQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted = 1  and tool_unsubscribe=0 and url= ? and date_format(social_media_user.user_date,'%Y-%m-%d')= ? ";
        } else {
            getSocailMediaUserIdQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 0 and is_deleted = 1  and tool_unsubscribe=0 and url= ? and date_format(social_media_user.user_date,'%Y-%m-%d')= ? ";
        }
        logger.debug("Query To check the user id social media user table is " + getSocailMediaUserIdQuery + " and  lxrmUserId is " + lxrmUserId + " and updateURl is " + updateURl + " and  userLoggedIn is " + userLoggedIn);
        try {
            existedURL = this.jdbcTemplate.query(getSocailMediaUserIdQuery,
                    new Object[]{lxrmUserId, updateURl, date}, (ResultSet rs, int rowNum) -> {
                        SocialCompDomains domainSetting = new SocialCompDomains();
                        domainSetting.setDomainId(rs.getLong("id"));
                        domainSetting.setDomain(rs.getString("url"));
                        domainSetting.setUserDomain(rs.getBoolean("is_userdomain"));
                        domainSetting.setIsLoggedIn(rs.getBoolean("is_logged_in"));
                        domainSetting.setIsDeleted(rs.getBoolean("is_deleted"));
                        domainSetting.setDomainRegDate(rs.getTimestamp("user_date"));
                        domainSetting.setUnSubScribeStatus(rs.getBoolean("tool_unsubscribe"));
                        domainSetting.setWeeklyReport(rs.getBoolean("weekly_report"));
                        return domainSetting;
                    });
        } catch (DataAccessException e) {
            logger.error("Exception for getSocailMediaUserId: ", e);
        }
        return existedURL;
    }

    public Long fetchSocialUserIdFromSocialMediaUserGuest(long id) {
        String query = "select id from social_media_user  where user_id = ? and is_logged_in = 0";
        Long finalRankUserID = 0l;
        try {
            finalRankUserID = this.jdbcTemplate.query(query, new Object[]{id}, (ResultSet rs) -> {
                Long rankUserId = null;
                try {
                    if (rs.next()) {
                        rankUserId = rs.getLong("id");
                    }
                } catch (SQLException e) {
                    logger.error("Exception for inner class fetchSocialUserId: ", e);
                }
                return rankUserId;
            });
        } catch (DataAccessException ex) {
            logger.error("Exception for fetchSocialUserId From Guest User: ", ex);
        }
        return finalRankUserID;
    }

    public static String getUserNameFromLink(String link) {
        String userNamelink = null;
        try {
            URL url = new URL(link);
            userNamelink = url.getPath();
            if (userNamelink.startsWith("/user")) {
                userNamelink = userNamelink.replaceAll("/user", " ");
                userNamelink = userNamelink.trim();
            }
            if (userNamelink.startsWith("/channel")) {
                userNamelink = userNamelink.replaceAll("/channel", " ");
                userNamelink = userNamelink.trim();
            }
            if (userNamelink != null) {
                int firstindex = getRegularExpressionIndex(userNamelink);
                if ((firstindex != -1) && (firstindex == 0)) {
                    userNamelink = userNamelink.substring(1);
                    int secondindex = getRegularExpressionIndex(userNamelink);
                    if ((secondindex != -1) && (secondindex != 0)) {
                        userNamelink = userNamelink.substring(0, secondindex);
                        return userNamelink;
                    }
                    return userNamelink;
                } else if ((firstindex != -1) && (firstindex != 0)) {
                    userNamelink = userNamelink.substring(0, firstindex - 1);
                    return userNamelink;
                }
            }
        } catch (MalformedURLException e) {
            logger.error("Exception for getUserNameFromLink: ", e);
        }
        return userNamelink;
    }

    /*To get date of maximium data available for a url*/
    public Date getDateForSocialMediaUser(long userId) {
        String query = "select max(date_format(t.insert_date,'%Y-%m-%d')) as maxDate from smt_twitter t where t.media_user_id"
                + "=" + userId + "";
        Date resultDate = null;
        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    resultDate = convertStringtoDate(sqlRowSet.getString("maxDate"));
                }
            }
        } catch (DataAccessException e) {
            logger.error("Exception for getDateForSocialMediaUser: ", e);
        }
        return resultDate;
    }

    public boolean checkDataExistedForDate(long userId, String queryDate) {
        String query = "select * from smt_twitter t  where media_user_id = " + userId + " and date_format(t.insert_date,'%Y-%m-%d')= '" + queryDate + "'";
        boolean dataExisted = false;
        try {
            logger.debug("query: " + query);
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    Timestamp inserteDate = sqlRowSet.getTimestamp("insert_date");
                    if (queryDate.equals(Common.convertTimeStampintoString(inserteDate))) {
                        dataExisted = true;
                        break;
                    }
                }
            }
        } catch (DataAccessException e) {
            logger.error("Exception for checkDataExistedForDate: ", e);
        }
        return dataExisted;
    }

    public Date convertStringtoDate(String maxDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(maxDate);
        } catch (ParseException e) {
            logger.error("ParseException for convertStringtoDate: ", e);
        }
        return date;
    }

    /*Newly Added Methods Feb 10 2016*/
    public List<Long> insertComptetiorDomains(final long userId,
            final Timestamp startTime, final List<String> compDomain,
            final boolean userLoggedIn, final boolean isUserDomain, final boolean is_deleted,
            final boolean weekly_report) {
        final String query = "insert into social_media_user(user_id,url,user_date,is_logged_in,is_userdomain,is_deleted,weekly_report) values (?,?,?,?,?,?,?)";
        int[] keys = this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setLong(1, userId);
                statement.setString(2, compDomain.get(i));
                statement.setTimestamp(3, startTime);
                statement.setBoolean(4, userLoggedIn);
                statement.setBoolean(5, isUserDomain);
                statement.setBoolean(6, is_deleted);
                statement.setBoolean(7, weekly_report);
            }

            @Override
            public int getBatchSize() {
                return compDomain.size();
            }
        });
        List<Long> compDomainIds = new ArrayList<>();
        if (keys != null) {
            for (int i = 0; i < keys.length; i++) {
                compDomainIds.add(Long.valueOf(keys[i]));
            }
        }
        return compDomainIds;
    }

    public List<Map<String, Object>> processSocialMediaAnalyzer(List<SocialCompDomains> socialDomains, int fetchUserNames) {
        List<Map<String, Object>> allMediaObjects = new LinkedList();
        try {

            Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            ConcurrentHashMap<Long, String> socialThreadMap = new ConcurrentHashMap();
            Map<String, Object> socialmediaObjects = null;
            Map<String, Object> compFirstMediaObjects = null;
            Map<String, Object> compSecondMediaObjects = null;
            SocialMediaThread socialMediaThread = null;
            SocialMediaThread compURLFirstThread = null;
            SocialMediaThread compURLSecondThread = null;

            TwitterMetrics userTwM;
            FacebookMetrics userFBM;
            InstagramMetrics userInstM;
            PinterestMetrics userPintM;
            YouTubeMetrics userYTM;
            GoogleMetrics userGPM;

            if (socialDomains.size() >= 1) {
                if (fetchUserNames == 2) {
                    ChannelUserNames chUserNames = checkChannnelUserNames(socialDomains.get(0));
                    socialDomains.get(0).setChannelNames(chUserNames);
                }
                socialMediaThread = new SocialMediaThread(socialDomains.get(0), socialThreadMap, fetchUserNames);
            }
            if (socialDomains.size() >= 2) {
                if (socialDomains.get(1) != null) {
                    if (fetchUserNames == 2) {
                        ChannelUserNames chUserNames1 = checkChannnelUserNames(socialDomains.get(1));
                        socialDomains.get(1).setChannelNames(chUserNames1);
                    }

                    compURLFirstThread = new SocialMediaThread(socialDomains.get(1), socialThreadMap, fetchUserNames);
                }
            }
            if (socialDomains.size() >= 3) {
                if (socialDomains.get(2) != null) {
                    if (fetchUserNames == 2) {
                        ChannelUserNames chUserNames2 = checkChannnelUserNames(socialDomains.get(2));
                        socialDomains.get(2).setChannelNames(chUserNames2);
                    }
                    compURLSecondThread = new SocialMediaThread(socialDomains.get(2), socialThreadMap, fetchUserNames);
                }
            }
            /*Creating Thread class objects*/
            Thread userThread = null;
            Thread compFirstThread = null;
            Thread compSecondThread = null;
            if (socialMediaThread != null) {
                userThread = new Thread(socialMediaThread);
            }
            if (compURLFirstThread != null) {
                compFirstThread = new Thread(compURLFirstThread);
            }
            if (compURLSecondThread != null) {
                compSecondThread = new Thread(compURLSecondThread);
            }
            try {
                /*starting Thread class Objects*/
                if (userThread != null) {
                    userThread.start();
                }
                if (compFirstThread != null) {
                    compFirstThread.start();
                }
                if (compSecondThread != null) {
                    compSecondThread.start();
                }

                //TODO: Join 3 domain threads, remove try catch below
                try {
                    if (socialDomains.size() > 0) {
                        logger.debug("The socialThreadMap size should equal to: " + socialDomains.size());
                        while (!(socialThreadMap.size() == socialDomains.size())) {
                            logger.debug("Size of socialThreadMap is : " + socialThreadMap.size());
                            Thread.sleep(3000);
                        }
                    }
                } catch (InterruptedException e) {
                    logger.error("Exception in making Thread sleep: ", e);
                }
                if (socialDomains.size() >= 1) {
                    if (socialDomains.get(0) != null && socialMediaThread != null) {
                        socialmediaObjects = socialMediaThread.getSocialmediaObjects();
                    }
                }
                if (socialDomains.size() >= 2) {
                    if (socialDomains.get(1) != null && compURLFirstThread != null) {
                        compFirstMediaObjects = compURLFirstThread.getSocialmediaObjects();
                    }
                }
                if (socialDomains.size() >= 3) {
                    if (socialDomains.get(2) != null && compURLSecondThread != null) {
                        compSecondMediaObjects = compURLSecondThread.getSocialmediaObjects();
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in running social threads: ", e);
            }
            try {
                if (socialDomains.size() >= 1) {
                    if (socialmediaObjects != null) {
                        allMediaObjects.add(socialmediaObjects);

                        userTwM = (TwitterMetrics) socialmediaObjects.get("twittermetrics");
                        userFBM = (FacebookMetrics) socialmediaObjects.get("facebookmetrics");
                        userInstM = (InstagramMetrics) socialmediaObjects.get("instagrammetrics");
                        userPintM = (PinterestMetrics) socialmediaObjects.get("pinterestmetrics");
                        userYTM = (YouTubeMetrics) socialmediaObjects.get("youtubemetrics");
                        userGPM = (GoogleMetrics) socialmediaObjects.get("googleplusmetrics");

                        if (userTwM != null) {
                            insertTwitterAccount(socialDomains.get(0).getDomainId(), userTwM, startTime);
                        }
                        if (userFBM != null) {
                            insertFacebookAccount(socialDomains.get(0).getDomainId(), userFBM, startTime);
                        }
                        if (userGPM != null) {
                            insertGooglePlusAccount(socialDomains.get(0).getDomainId(), userGPM, startTime);
                        }
                        if (userYTM != null) {
                            insertYouTubeAccount(socialDomains.get(0).getDomainId(), userYTM, startTime);
                        }
                        if (userInstM != null) {
                            insertInstagramAccount(socialDomains.get(0).getDomainId(), userInstM, startTime);
                        }
                        if (userPintM != null) {
                            insertPinterestAccount(socialDomains.get(0).getDomainId(), userPintM, startTime);
                        }

                        /*For New Users who's request came from user interface.*/
                        if (fetchUserNames == 1) {
                            insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                    (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(0).getDomainId());
                        }
                        if ((socialDomains.get(0).getChannelNames() != null) && (socialDomains.get(0).getChannelNames().isInsertUserNames())) {
                            long channelUserID = socialDomains.get(0).getChannelNames().getChannelRowId();
                            long mediaUserID = socialDomains.get(0).getChannelNames().getSocialMediaUserId();
                            if ((channelUserID == 0) && (mediaUserID == 0)) {
                                /*For  Users who's  channel user names does not exists and request came from schedulers.*/
                                insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(0).getDomainId());
                            } else if ((channelUserID != 0) && (mediaUserID != 0)) {
                                updateChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(0).getChannelNames());
                            }
                        }

                    }
                }
            } catch (Exception e) {
                logger.error("Exception in inserting all socialmediaObjects for user: ", e);
            }
            try {
                if (socialDomains.size() >= 2) {
                    if (compFirstMediaObjects != null) {
                        allMediaObjects.add(compFirstMediaObjects);

                        userTwM = (TwitterMetrics) compFirstMediaObjects.get("twittermetrics");
                        userFBM = (FacebookMetrics) compFirstMediaObjects.get("facebookmetrics");
                        userInstM = (InstagramMetrics) compFirstMediaObjects.get("instagrammetrics");
                        userPintM = (PinterestMetrics) compFirstMediaObjects.get("pinterestmetrics");
                        userYTM = (YouTubeMetrics) compFirstMediaObjects.get("youtubemetrics");
                        userGPM = (GoogleMetrics) compFirstMediaObjects.get("googleplusmetrics");

                        if (userTwM != null) {
                            insertTwitterAccount(socialDomains.get(1).getDomainId(), userTwM, startTime);
                        }
                        if (userFBM != null) {
                            insertFacebookAccount(socialDomains.get(1).getDomainId(), userFBM, startTime);
                        }
                        if (userGPM != null) {
                            insertGooglePlusAccount(socialDomains.get(1).getDomainId(), userGPM, startTime);
                        }
                        if (userYTM != null) {
                            insertYouTubeAccount(socialDomains.get(1).getDomainId(), userYTM, startTime);
                        }
                        if (userInstM != null) {
                            insertInstagramAccount(socialDomains.get(1).getDomainId(), userInstM, startTime);
                        }
                        if (userPintM != null) {
                            insertPinterestAccount(socialDomains.get(1).getDomainId(), userPintM, startTime);
                        }
                        /*For New Users who's request came from user interface.*/
                        if (fetchUserNames == 1) {
                            insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                    (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(1).getDomainId());
                        }
                        if ((socialDomains.get(1).getChannelNames() != null) && (socialDomains.get(1).getChannelNames().isInsertUserNames())) {
                            long channelUserID = socialDomains.get(1).getChannelNames().getChannelRowId();
                            long mediaUserID = socialDomains.get(1).getChannelNames().getSocialMediaUserId();
                            if ((channelUserID == 0) && (mediaUserID == 0)) {
                                /*For  Users who's  channel user names does not exists and request came from schedulers.*/
                                insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(1).getDomainId());
                            } else if ((channelUserID != 0) && (mediaUserID != 0)) {
                                updateChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(1).getChannelNames());
                            }
                        }

                    }
                }
            } catch (Exception e) {
                logger.error("Exception in inserting all compFirstMediaObjects for user: ", e);
            }
            try {
                if (socialDomains.size() >= 3) {
                    if (compSecondMediaObjects != null && socialDomains.get(2) != null) {
                        allMediaObjects.add(compSecondMediaObjects);

                        userTwM = (TwitterMetrics) compSecondMediaObjects.get("twittermetrics");
                        userFBM = (FacebookMetrics) compSecondMediaObjects.get("facebookmetrics");
                        userInstM = (InstagramMetrics) compSecondMediaObjects.get("instagrammetrics");
                        userPintM = (PinterestMetrics) compSecondMediaObjects.get("pinterestmetrics");
                        userYTM = (YouTubeMetrics) compSecondMediaObjects.get("youtubemetrics");
                        userGPM = (GoogleMetrics) compSecondMediaObjects.get("googleplusmetrics");
                        if (userTwM != null) {
                            insertTwitterAccount(socialDomains.get(2).getDomainId(), userTwM, startTime);
                        }
                        if (userFBM != null) {
                            insertFacebookAccount(socialDomains.get(2).getDomainId(), userFBM, startTime);
                        }
                        if (userGPM != null) {
                            insertGooglePlusAccount(socialDomains.get(2).getDomainId(), userGPM, startTime);
                        }
                        if (userYTM != null) {
                            insertYouTubeAccount(socialDomains.get(2).getDomainId(), userYTM, startTime);
                        }
                        if (userInstM != null) {
                            insertInstagramAccount(socialDomains.get(2).getDomainId(), userInstM, startTime);
                        }
                        if (userPintM != null) {
                            insertPinterestAccount(socialDomains.get(2).getDomainId(), userPintM, startTime);
                        }
                        /*For New Users who's request came from user interface.*/
                        if (fetchUserNames == 1) {
                            insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                    (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(2).getDomainId());
                        }
                        if ((socialDomains.get(2).getChannelNames() != null) && (socialDomains.get(2).getChannelNames().isInsertUserNames())) {
                            long channelUserID = socialDomains.get(2).getChannelNames().getChannelRowId();
                            long mediaUserID = socialDomains.get(2).getChannelNames().getSocialMediaUserId();
                            if ((channelUserID == 0) && (mediaUserID == 0)) {
                                /*For  Users who's  channel user names does not exists and request came from schedulers.*/
                                insertChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(2).getDomainId());
                            } else if ((channelUserID != 0) && (mediaUserID != 0)) {
                                updateChannelNames((userTwM != null ? userTwM.getUserName() : null), (userFBM != null ? userFBM.getUserName() : null), (userInstM != null ? userInstM.getUserName() : null), (userPintM != null ? userPintM.getPinterestUserName() : null),
                                        (userYTM != null ? userYTM.getUserName() : null), (userGPM != null ? userGPM.getUserName() : null), socialDomains.get(2).getChannelNames());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in inserting all compSecondMediaObjects for user: ", e);
            }
            //responseObject=generateJSONResponse(allMediaObjects,socialURLs);
            return allMediaObjects;
        } catch (Exception e) {
            logger.error("Exception in processSocialMediaAnalyzer: ", e);
        }
        return allMediaObjects;
    }

    public void insertChannelNames(String twitterName, String facebookName, String instagramName, String pinterestName, String youtubeName, String googleName, long mediaUserId) {
        logger.debug("Inserting channel user names into social_channel_user_names");
        Timestamp insertDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String query = "insert  social_channel_user_names(media_user_id,twitter_name,facebook_name,instagram_name,pinterest_name,"
                + "youtube_name,goolgePlus_name,insert_date)"
                + " values (?,?,?,?,?,?,?,?)";
        try {
            synchronized (this) {
                this.jdbcTemplate.update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                            query, new String[]{"id"});
                    statement.setLong(1, mediaUserId);
                    statement.setString(2, twitterName);
                    statement.setString(3, facebookName);
                    statement.setString(4, instagramName);
                    statement.setString(5, pinterestName);
                    statement.setString(6, youtubeName);
                    statement.setString(7, googleName);
                    statement.setTimestamp(8, insertDate);
                    return statement;
                });
            }
        } catch (DataAccessException ex) {
            logger.error("DataAccessException Inserting insertChannelNames: ", ex);
        }

    }

    public void updateChannelNames(String twitterName, String facebookName, String instagramName, String pinterestName, String youtubeName, String googleName, ChannelUserNames channelUserNames) {
        Timestamp updateDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String updatQuery = "update  social_channel_user_names set twitter_name =?,facebook_name =?,"
                + "instagram_name =?,pinterest_name =?,youtube_name =?,goolgePlus_name =?,insert_date =? where media_user_id = ? and user_name_id = ?";
        try {
            synchronized (this) {
                logger.debug("Updating entry in smt_twitter table for media url " + channelUserNames.getSocialMediaUserId() + "and query is");
                this.jdbcTemplate.update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                            updatQuery, new String[]{"id"});
                    statement.setString(1, twitterName);
                    statement.setString(2, facebookName);
                    statement.setString(3, instagramName);
                    statement.setString(4, pinterestName);
                    statement.setString(5, youtubeName);
                    statement.setString(6, googleName);
                    statement.setTimestamp(7, updateDate);
                    statement.setLong(8, channelUserNames.getSocialMediaUserId());
                    statement.setLong(9, channelUserNames.getChannelRowId());
                    return statement;
                });
            }
        } catch (DataAccessException ex) {
            logger.error("Exception updating updateChannelNames: ", ex);
        }

    }

    public JSONObject generateJSONResponse(List<Map<String, Object>> allMediaObject, List<SocialCompDomains> socialURLs) {
        /*parent JSONObject*/
        JSONObject responseObject = new JSONObject();

        /*parent JSONOArray conatins analysis URL's.*/
        JSONArray urlsArray = new JSONArray();

        /*parent JSONOArray conatins analysis URL's respective registered dates.*/
        JSONArray urlsDateJSON = new JSONArray();

        /*All social medias metric JSONObjects.*/
        JSONObject facebookFinal = new JSONObject();
        JSONObject twitterFinal = new JSONObject();
        JSONObject pinterestFinal = new JSONObject();
        JSONObject youtubeFinal = new JSONObject();
        JSONObject googleFinal = new JSONObject();
        /*JSONObject instagramFinal = new JSONObject();*/

        FacebookMetrics facebookResponse;
        TwitterMetrics twitterResponse;
        PinterestMetrics pinterestResponse;
        YouTubeMetrics youTubeResponse;
        GoogleMetrics googleResponse;
        /*InstagramMetrics instagramResponse;*/

        JSONArray fLikesArray = new JSONArray();

        JSONArray hashTagJSON = new JSONArray();
        JSONArray tFollowersArray = new JSONArray();
        JSONArray tTweetsArray = new JSONArray();
        JSONArray tFollowingArray = new JSONArray();
        JSONArray tLikesArray = new JSONArray();

        JSONArray pFollowersArray = new JSONArray();
        JSONArray pPinsArray = new JSONArray();
        JSONArray pBoardsArray = new JSONArray();
        JSONArray pFollwoingArray = new JSONArray();

        JSONArray yFollowersArray = new JSONArray();
        JSONArray yVideosArray = new JSONArray();
        JSONArray yViewsArray = new JSONArray();
        JSONArray gFollowersArray = new JSONArray();

        SimpleDateFormat socialAnalysisDate = new SimpleDateFormat("yyyy-MM-dd");
        String presentDate = socialAnalysisDate.format(Calendar.getInstance().getTime());
        boolean userDomainRegistration = false;
        for (SocialCompDomains URL : socialURLs) {
            if (URL.isUserDomain()) {
                if (presentDate.equals(Common.convertTimeStampintoDate(URL.getDomainRegDate()))) {
                    userDomainRegistration = true;
                }
            }
            if ((URL.getDomainId() > 0)) {
                String displayURL = trimWebsitesName(URL.getDomain());
                urlsArray.put(displayURL);
                /*urlsDateJSON.put(Common.convertTimeStampintoDate(URL.getDomainRegDate()));*/
                boolean maxmiumData = false;
                maxmiumData = URL.getMaximumDate() != null ? presentDate.equals(convertDateToString(URL.getMaximumDate(), 1)) : presentDate.equals(convertDateToString(URL.getDomainRegDate(), 1));
                boolean registrationDate = presentDate.equals(Common.convertTimeStampintoDate(URL.getDomainRegDate()));
                if (!registrationDate) {
                    if (maxmiumData) {
                        urlsDateJSON.put(true);
                    } else if (URL.isGrowthDataAvailable()) {
                        if (userDomainRegistration) {
                            urlsDateJSON.put(false);
                        } else {
                            urlsDateJSON.put(true);
                        }
                    } else {
                        urlsDateJSON.put(false);
                    }
                } else {
                    urlsDateJSON.put(false);
                }
            }
        }

        int emptyHashCount = 0;
        int faceBookCount = 0, twitterCount = 0, pinterestCount = 0, youTubeCount = 0, googlePlusCount = 0;

        /*logger.info("urlsArray is " + urlsArray + "   and urlsDateJSON is " + urlsDateJSON);*/
        JSONObject metricObject = null;
        try {
            try {
                int urlCount = 0;
                for (Map<String, Object> socialmediaObjects : allMediaObject) {
                    /*Construction of Facebook JSONObject starts*/
 /*Likes*/
                    facebookResponse = (FacebookMetrics) socialmediaObjects.get("facebookmetrics");
                    metricObject = new JSONObject();
                    if (facebookResponse != null && facebookResponse.isFacebookAccountStatus()) {
                        metricObject.put("fLikes", facebookResponse.getLikesCount());
                        if ((urlsDateJSON.getBoolean(urlCount))) {
                            metricObject.put("Change", facebookResponse.getChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                    } else {
                        metricObject.put("fLikes", -1);
                        metricObject.put("Change", "NA");
                        faceBookCount++;
                    }
                    fLikesArray.put(metricObject);
                    /*EOF Facebook Construction of  JSONObject*/

 /*Construction of Twitter JSONObject starts*/
                    twitterResponse = (TwitterMetrics) socialmediaObjects.get("twittermetrics");
                    if (twitterResponse != null && twitterResponse.isTwitterAccountStatus()) {
                        /*Creating JSONObject to store Twitter Followers data*/
                        metricObject = new JSONObject();
                        metricObject.put("followers", twitterResponse.getFollowers());
                        if ((urlsDateJSON.getBoolean(urlCount))) {
                            metricObject.put("Change", twitterResponse.getChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        /*Placing the Twitter Followers JSONObject data into array*/
                        tFollowersArray.put(metricObject);

                        /*Creating JSONObject to store Twitter Tweets data*/
                        metricObject = new JSONObject();
                        metricObject.put("tweets", twitterResponse.getTweets());
                        if ((urlsDateJSON.getBoolean(urlCount))) {
                            metricObject.put("Change", twitterResponse.getTwitteractivityChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        /*Placing the Twitter Tweets JSONObject data into array*/
                        tTweetsArray.put(metricObject);

                        /*Creating JSONObject to store Twitter Favourites data*/
                        metricObject = new JSONObject();
                        metricObject.put("likes", twitterResponse.getFavourites());
                        if ((urlsDateJSON.getBoolean(urlCount))) {
                            metricObject.put("Change", twitterResponse.getTwitterFavouritesChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        /*Placing the Twitter Favourites JSONObject data into array*/
                        tLikesArray.put(metricObject);

                        /*Creating JSONObject to store Twitter Favourites data*/
                        metricObject = new JSONObject();
                        metricObject.put("following", twitterResponse.getFollowing());
                        if ((urlsDateJSON.getBoolean(urlCount))) {
                            metricObject.put("Change", twitterResponse.getTwitterFollowingChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        /*Placing the Twitter Favourites JSONObject data into array*/
                        tFollowingArray.put(metricObject);
                        /*Placing HashTag content*/
                        if (!twitterResponse.getTwitterHashTag().equalsIgnoreCase("NoHashTag")) {
                            hashTagJSON.put(twitterResponse.getTwitterHashTag());
                        } else {
                            emptyHashCount++;
                            hashTagJSON.put("No recent hash tags available.");
                        }
                    } else {
                        metricObject = new JSONObject();
                        metricObject.put("followers", -1);
                        metricObject.put("Change", "NA");
                        tFollowersArray.put(metricObject);

                        metricObject = new JSONObject();
                        metricObject.put("tweets", -1);
                        metricObject.put("Change", "NA");
                        tTweetsArray.put(metricObject);

                        metricObject = new JSONObject();
                        metricObject.put("likes", -1);
                        metricObject.put("Change", "NA");
                        tLikesArray.put(metricObject);

                        metricObject = new JSONObject();
                        metricObject.put("following", -1);
                        metricObject.put("Change", "NA");
                        tFollowingArray.put(metricObject);

                        hashTagJSON.put("Not applicable.");
                        emptyHashCount++;
                        twitterCount++;
                    }/*EOF of Twitter*/

 /*Construction of pinterest starts*/
                    pinterestResponse = (PinterestMetrics) socialmediaObjects.get("pinterestmetrics");
                    if (pinterestResponse != null && pinterestResponse.isPinterestAccountStatus()) {
                        /*Pinterest followers*/
                        metricObject = new JSONObject();
                        metricObject.put("pFollowers", pinterestResponse.getFollowers());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", pinterestResponse.getFollowersChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        pFollowersArray.put(metricObject);

                        /*Pins*/
                        metricObject = new JSONObject();
                        metricObject.put("pPins", pinterestResponse.getPins());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", pinterestResponse.getPinsChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        pPinsArray.put(metricObject);

                        /*Boards*/
                        metricObject = new JSONObject();
                        metricObject.put("pBoards", pinterestResponse.getBoards());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", pinterestResponse.getBoardsChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        pBoardsArray.put(metricObject);

                        /*Following*/
                        metricObject = new JSONObject();
                        metricObject.put("pFollowing", pinterestResponse.getFollowing());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", pinterestResponse.getFollowingChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        pFollwoingArray.put(metricObject);
                    } else {
                        /*Pinterest followers*/
                        metricObject = new JSONObject();
                        metricObject.put("pFollowers", -1);
                        metricObject.put("Change", "NA");
                        pFollowersArray.put(metricObject);

                        /*Pins*/
                        metricObject = new JSONObject();
                        metricObject.put("pPins", -1);
                        metricObject.put("Change", "NA");
                        pPinsArray.put(metricObject);

                        /*Boards*/
                        metricObject = new JSONObject();
                        metricObject.put("pBoards", -1);
                        metricObject.put("Change", "NA");
                        pBoardsArray.put(metricObject);

                        /*Following*/
                        metricObject = new JSONObject();
                        metricObject.put("pFollowing", -1);
                        metricObject.put("Change", "NA");
                        pFollwoingArray.put(metricObject);
                        pinterestCount++;
                    }
                    /*YouTube JSONObject*/
                    youTubeResponse = (YouTubeMetrics) socialmediaObjects.get("youtubemetrics");
                    if (youTubeResponse != null && youTubeResponse.isYoutubeAccountStatus()) {
                        /*Subscribers*/
                        metricObject = new JSONObject();
                        metricObject.put("ySubscribers", youTubeResponse.getSubscribers());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", youTubeResponse.getChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        yFollowersArray.put(metricObject);

                        /*Videos*/
                        metricObject = new JSONObject();
                        metricObject.put("yVideos", youTubeResponse.getVideos());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", youTubeResponse.getYouTubeActivityChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        yVideosArray.put(metricObject);

                        /*Views*/
                        metricObject = new JSONObject();
                        metricObject.put("yViews", youTubeResponse.getViews());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", youTubeResponse.getYouTubeViewsChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        yViewsArray.put(metricObject);
                    } else {
                        /*Subscribers*/
                        metricObject = new JSONObject();
                        metricObject.put("ySubscribers", -1);
                        metricObject.put("Change", "NA");
                        yFollowersArray.put(metricObject);

                        /*Videos*/
                        metricObject = new JSONObject();
                        metricObject.put("yVideos", -1);
                        metricObject.put("Change", "NA");
                        yVideosArray.put(metricObject);

                        /*Views*/
                        metricObject = new JSONObject();
                        metricObject.put("yViews", -1);
                        metricObject.put("Change", "NA");
                        yViewsArray.put(metricObject);
                        youTubeCount++;
                    }
                    /*Google Plus*/
                    googleResponse = (GoogleMetrics) socialmediaObjects.get("googleplusmetrics");
                    if (googleResponse != null && googleResponse.isGoogleAccountStatus()) {
                        /*Followers*/
                        metricObject = new JSONObject();
                        metricObject.put("gFollowers", googleResponse.getFollowers());
                        if (urlsDateJSON.getBoolean(urlCount)) {
                            metricObject.put("Change", googleResponse.getChange());
                        } else {
                            metricObject.put("Change", "NA");
                        }
                        gFollowersArray.put(metricObject);
                    } else {
                        /*Followers*/
                        metricObject = new JSONObject();
                        metricObject.put("gFollowers", -1);
                        metricObject.put("Change", "NA");
                        gFollowersArray.put(metricObject);
                        googlePlusCount++;
                    }
                    urlCount++;
                }/*End of For Loop*/
            } catch (JSONException e) {
                logger.error("Exception while preparing loop: ", e);
            }
            /*Placing the URL's in response*/
            responseObject.put("urlsJSON", urlsArray);

            if (emptyHashCount == socialURLs.size()) {
                hashTagJSON.put(1);/*To Hide recent hash tag's div */
            } else {
                hashTagJSON.put(0);
            }

            responseObject.put("hashTagJSON", hashTagJSON);

            /*FaceBook Response*/
            facebookFinal.put("fLikes", fLikesArray);
            /*Twitter Response*/
            twitterFinal.put("followers", tFollowersArray);
            twitterFinal.put("tweets", tTweetsArray);
            twitterFinal.put("likes", tLikesArray);
            twitterFinal.put("following", tFollowingArray);
            /*Pinterest Response*/
            pinterestFinal.put("pFollowers", pFollowersArray);
            pinterestFinal.put("pPins", pPinsArray);
            pinterestFinal.put("pBoards", pBoardsArray);
            pinterestFinal.put("pFollowing", pFollwoingArray);
            /*YouTube Response*/
            youtubeFinal.put("ySubscribers", yFollowersArray);
            youtubeFinal.put("yVideos", yVideosArray);
            youtubeFinal.put("yViews", yViewsArray);
            /*Google Response*/
            googleFinal.put("gFollowers", gFollowersArray);

            /*logger.info("FacebookFinalJSON is " + facebookFinal);
            logger.info("TwitterFinalJSON is " + twitterFinal);
            logger.info("youtubeFinalJSON is " + youtubeFinal);
            logger.info("instagramFinalJSON is " + instagramFinal);
            logger.info("pinterestJSON is " + pinterestFinal);
            logger.info("googleFinalJSON is " + googleFinal);*/
            responseObject.put("facebookFinalJSON", facebookFinal);
            responseObject.put("twitterFinalJSON", twitterFinal);
            responseObject.put("pinterestJSON", pinterestFinal);
            responseObject.put("googleFinalJSON", googleFinal);
            responseObject.put("youtubeFinalJSON", youtubeFinal);

            JSONObject hideChannelData = new JSONObject();
            if (faceBookCount == allMediaObject.size()) {
                hideChannelData.put("hideFacebook", true);
            } else {
                hideChannelData.put("hideFacebook", false);
            }
            if (twitterCount == allMediaObject.size()) {
                hideChannelData.put("hideTwitter", true);
            } else {
                hideChannelData.put("hideTwitter", false);
            }
            if (pinterestCount == allMediaObject.size()) {
                hideChannelData.put("hidePinterest", true);
            } else {
                hideChannelData.put("hidePinterest", false);
            }
            if (youTubeCount == allMediaObject.size()) {
                hideChannelData.put("hideYoutube", true);
            } else {
                hideChannelData.put("hideYoutube", false);
            }
            if (googlePlusCount == allMediaObject.size()) {
                hideChannelData.put("hideGoogle", true);
            } else {
                hideChannelData.put("hideGoogle", false);
            }
            /*logger.info("hideChannelData: " + hideChannelData+" ResponseObject:  " + responseObject);*/
            responseObject.put("hideChannelData", hideChannelData);
            JSONObject fieldsObejct = null;
            Map<String, String> fieldColorsMap = null;
            Map<String, Map<String, String>> colorsforMetrics = getColorsListForMetrics(allMediaObject);
            if (colorsforMetrics != null && !colorsforMetrics.isEmpty()) {
                for (Map.Entry<String, Map<String, String>> channelEntry : colorsforMetrics.entrySet()) {
                    fieldColorsMap = channelEntry.getValue();
                    if (fieldColorsMap != null && !fieldColorsMap.isEmpty()) {
                        fieldsObejct = new JSONObject();
                        for (Map.Entry<String, String> fieldEntry : fieldColorsMap.entrySet()) {
                            fieldsObejct.put(fieldEntry.getKey(), fieldEntry.getValue());
                        }
                    }
                    responseObject.put(channelEntry.getKey(), fieldsObejct);
                }
            }
            logger.debug("responseObject: " + responseObject);
        } catch (JSONException e) {
            logger.error("Exception when preparing JSONResponse: ", e);
        }
        return responseObject;
    }

    public void constructAnlaysisResponseJSON(JSONObject socialResponse, String changeStatus, HttpServletResponse response) {
        JSONObject arr = new JSONObject();
        PrintWriter writer = null;
        /*logger.info("Request In addInJsonforSocialResponse");*/
        try {
            writer = response.getWriter();
            arr.put("socialResponse", socialResponse);
            arr.put("changeStatus", changeStatus);
            //arr.put(changeStatus);
            writer.print(arr);
        } catch (IOException | JSONException e) {
            logger.error("Exception in addInJsonforSocialResponse:", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public List<SocialCompDomains> createSocialCompDomainRows(long lxrmUserId, boolean userLoggedIn, boolean checkDate) {
        String userQuery;
        Timestamp creationDate;
        String currentDate = null;
        List<SocialCompDomains> domSettings = null;
        if (checkDate) {
            creationDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
            currentDate = (new SimpleDateFormat("yyyy-MM-dd")).format(creationDate);
        }
        if (userLoggedIn) {
            if (checkDate) {
                /*userQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted = 0 order by id and date_format(user_date,'%Y-%m-%d') = "+currentDate+"";*/
                userQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted = 0 and date_format(user_date,'%Y-%m-%d') = '" + currentDate + "' order by is_userdomain DESC,user_date";
            } else {
                userQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted = 0 order by is_userdomain DESC,user_date";
            }
        } else {
            logger.debug("Preparing SocialCompDomains list for GUEST user");
            if (checkDate) {
                userQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 0 and is_deleted = 0 and date_format(user_date,'%Y-%m-%d') = '" + currentDate + "'  order by is_userdomain DESC,user_date";
            } else {
                userQuery = "select * from social_media_user  where user_id = ? and is_logged_in = 0 and is_deleted = 0 order by is_userdomain DESC,user_date";
            }
        }
        /*String query = "select id from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted=0";*/
        logger.debug("Query for createSocialCompDomainRows " + userQuery);
        try {
            domSettings = this.jdbcTemplate.query(userQuery,
                    new Object[]{lxrmUserId}, (ResultSet rs, int rowNum) -> {
                        SocialCompDomains domainSetting = new SocialCompDomains();
                        domainSetting.setDomainId(rs.getLong("id"));
                        domainSetting.setDomain(rs.getString("url"));
                        domainSetting.setUserDomain(rs.getBoolean("is_userdomain"));
                        domainSetting.setIsLoggedIn(rs.getBoolean("is_logged_in"));
                        domainSetting.setIsDeleted(rs.getBoolean("is_deleted"));
                        domainSetting.setDomainRegDate(rs.getTimestamp("user_date"));
                        domainSetting.setUnSubScribeStatus(rs.getBoolean("tool_unsubscribe"));
                        domainSetting.setWeeklyReport(rs.getBoolean("weekly_report"));
                        return domainSetting;
                    });
        } catch (DataAccessException e) {
            logger.error("Exception in createSocialCompDomainRows: ", e);
        }
        if (domSettings != null && domSettings.size() >= 4) {
            domSettings = domSettings.subList(0, 2);
        }
        return domSettings;
    }

    public List<SocialCompDomains> createCompDomainSettingRows(List<SocialCompDomains> domSettings,
            SocialMedia socialMediaTool) {
        socialMediaTool.setDomainCount(domSettings.size());
        logger.debug("CreateCompDomainSettingRows domain count is :" + socialMediaTool.getDomainCount());
        if (domSettings.size() <= 3) {
            int j = 0;
            for (int i = domSettings.size() + 1; i <= 3; i++) {
                SocialCompDomains domainSetting = new SocialCompDomains();
                domainSetting.setDomainId(j--);
                domainSetting.setDomain("");
                domainSetting.setUserDomain(false);
                domSettings.add(domainSetting);
            }
        }
        return domSettings;
    }

    public List<SocialCompDomains> compareCompDomainsData(long lxrmUserId, SocialMedia socialMediaTool) {
        List<SocialCompDomains> newCompDomains = new ArrayList<>();
        try {
            List<SocialCompDomains> currentCompDomainSettings = socialMediaTool.getCompDomainSettingRows();
            List<SocialCompDomains> compRows = createSocialCompDomainRows(lxrmUserId, true, false);
            List<SocialCompDomains> prvCompDomainSettingRows = createCompDomainSettingRows(compRows, socialMediaTool);
            List<Long> domIds = new ArrayList<>();
            if (currentCompDomainSettings != null) {
                if (prvCompDomainSettingRows != null) {
                    for (SocialCompDomains curCompDomain : currentCompDomainSettings) {
                        logger.debug("CompareCompDomainsData curCompDomain data " + curCompDomain.getDomainId() + " " + curCompDomain.getDomain());
                        /*if (curCompDomain.getDomainId() > 0 && curCompDomain.isUserDomain() == false) {*/
                        if (curCompDomain.getDomainId() > 0) {
                            for (SocialCompDomains prvCompDomain : prvCompDomainSettingRows) {
                                /*if (prvCompDomain.getDomainId() > 0 && prvCompDomain.isUserDomain() == false) {*/
                                if (prvCompDomain.getDomainId() > 0) {
                                    if (curCompDomain.getDomainId() == prvCompDomain.getDomainId()) {
                                        if (curCompDomain.getDomain() != null && curCompDomain.getDomain().trim().equals("")) {
                                            // update the old data
                                            domIds.add(prvCompDomain.getDomainId());
                                        } else if (curCompDomain.getDomain() != null
                                                && !(curCompDomain.getDomain().equalsIgnoreCase(prvCompDomain.getDomain()))) {
                                            // insert new data in to the table & update the old data
                                            domIds.add(prvCompDomain.getDomainId());
                                            // updateCompDomainStatus(prvCompDomain.getDomainId(),userId);
                                            // long domId = insertIntoRankCheckerDomain(userId, curCompDomain.getDomain(), userDomainStatus, startTime);
                                            SocialCompDomains cmpDom = new SocialCompDomains();
                                            cmpDom.setDomain(curCompDomain.getDomain());
                                            cmpDom.setUserDomain(curCompDomain.isUserDomain());
                                            newCompDomains.add(cmpDom);
                                        }
                                        break;
                                    }
                                }
                            }
                        } else if (curCompDomain.getDomainId() <= 0) {
                            /*else if (curCompDomain.getDomainId() <= 0 && curCompDomain.isUserDomain() == false) {*/
                            if (curCompDomain.getDomain() != null && !curCompDomain.getDomain().trim().equals("")) {
//		            			 long domId = insertIntoRankCheckerDomain(userId, curCompDomain.getDomain(), userDomainStatus, startTime);
                                SocialCompDomains cmpDom = new SocialCompDomains();
//								   cmpDom.setDomainId(domId);
                                cmpDom.setDomain(curCompDomain.getDomain());
                                cmpDom.setUserDomain(curCompDomain.isUserDomain());
                                newCompDomains.add(cmpDom);
                            }
                        }
                    }
                    if (domIds.size() > 0) {
                        updateSettingsCompDomainData(domIds, lxrmUserId);
                    }
                } else {
                    currentCompDomainSettings.stream().filter((curCompDomain) -> (curCompDomain.getDomain() != null && !curCompDomain.getDomain().trim().equals("") && curCompDomain.isUserDomain() == false)).map((curCompDomain) -> {
                        //		            	long domId = insertIntoRankCheckerDomain(userId, curCompDomain.getDomain(), userDomainStatus, startTime);
                        SocialCompDomains cmpDom = new SocialCompDomains();
                        //						cmpDom.setDomainId(domId);
                        cmpDom.setDomain(curCompDomain.getDomain());
                        cmpDom.setUserDomain(curCompDomain.isUserDomain());
                        return cmpDom;
                    }).forEach((cmpDom) -> {
                        newCompDomains.add(cmpDom);
                    });
                }
            }
        } catch (Exception e) {
            logger.error("Exception in compareCompDomainsData: ", e);
        }
        return newCompDomains;
    }

    /*To delete the comptetiors domains set is_deleted is =1 in social media user table*/
    public void updateSettingsCompDomainData(final List<Long> domIds, final long lxrmUserId) {
        final String query = "update social_media_user set is_deleted = ? where id = ? and user_id = ?";
        logger.debug("updateing settings CompDomainData query: " + query);
        try {
            synchronized (this) {
                this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        statement.setBoolean(1, true);
                        statement.setLong(2, domIds.get(i));
                        statement.setLong(3, lxrmUserId);
                    }

                    @Override
                    public int getBatchSize() {
                        return domIds.size();
                    }
                });
            }
        } catch (DataAccessException e) {
            logger.error("Exception in updateSettingsCompDomainData: ", e);
        }
    }

    /*To insert updated comptetiors URLS from setting  for a login user into database*/
    public void insertSocialMediaListUsers(final long userId,
            final Timestamp startTime, final List<SocialCompDomains> socialCompDomains,
            final boolean userLoggedIn, final boolean isUserDomain, final boolean is_deleted,
            final boolean weekly_report) {
        final String query = "insert into social_media_user(user_id,url,user_date,is_logged_in,is_userdomain,is_deleted,weekly_report) values (?,?,?,?,?,?,?)";
        try {
            synchronized (this) {
                this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        statement.setLong(1, userId);
                        statement.setString(2, socialCompDomains.get(i).getDomain());
                        statement.setTimestamp(3, startTime);
                        statement.setBoolean(4, userLoggedIn);
                        statement.setBoolean(5, isUserDomain);
                        statement.setBoolean(6, is_deleted);
                        statement.setBoolean(7, weekly_report);
                    }

                    @Override
                    public int getBatchSize() {
                        return socialCompDomains.size();
                    }
                });
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in insertSocialMediaListUsers: ", ex);
        }
    }

    /*To insert comptetiors URLS */
    public void insertSocialMediaCompUsers(final long userId,
            final Timestamp startTime, final List<String> socialCompDomains,
            final boolean userLoggedIn, final boolean isUserDomain, final boolean is_deleted,
            final boolean weekly_report) {
        final String query = "insert into social_media_user(user_id,url,user_date,is_logged_in,is_userdomain,is_deleted,weekly_report) values (?,?,?,?,?,?,?)";
        try {
            synchronized (this) {
                this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        statement.setLong(1, userId);
                        statement.setString(2, socialCompDomains.get(i));
                        statement.setTimestamp(3, startTime);
                        statement.setBoolean(4, userLoggedIn);
                        statement.setBoolean(5, isUserDomain);
                        statement.setBoolean(6, is_deleted);
                        statement.setBoolean(7, weekly_report);
                    }

                    @Override
                    public int getBatchSize() {
                        return socialCompDomains.size();
                    }
                });
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in insertSocialMediaListUsers: ", ex);
        }

    }

    /*To convert into Valid3ByteUTF8String from a recent post */
    public String convertIntoDBformat(String postText) {
        String LAST_3_BYTE_UTF_CHAR = "\uFFFF";
        String REPLACEMENT_CHAR = " ";
        String finalText = " ";
        final int length;
        try {
            if (postText != null) {
                length = postText.length();
                StringBuilder formatText = new StringBuilder(length);
                for (int offset = 0; offset < length;) {
                    final int codepoint = postText.codePointAt(offset);
                    if (codepoint > LAST_3_BYTE_UTF_CHAR.codePointAt(0)) {
                        formatText.append(REPLACEMENT_CHAR);
                    } else if (Character.isValidCodePoint(codepoint)) {
                        formatText.appendCodePoint(codepoint);
                    } else {
                        formatText.append(REPLACEMENT_CHAR);
                    }
                    offset += Character.charCount(codepoint);
                }
                finalText = formatText.toString();
            }
        } catch (Exception e) {
            logger.error("Exception  converting the post text is " + postText + ", cause: ", e);
        }
        return finalText;
    }

    public List<SocialCompDomains> searchSocialDomainRows(long lxrmUserId, String updateURl) {
        String query = "select * from social_media_user  where user_id = ? and is_logged_in = 1 and is_deleted=0 order by id ";
        List<SocialCompDomains> domSettings = null;
        try {
            domSettings = this.jdbcTemplate.query(query,
                    new Object[]{lxrmUserId, updateURl}, (ResultSet rs, int rowNum) -> {
                        SocialCompDomains domainSetting = new SocialCompDomains();
                        domainSetting.setDomainId(rs.getLong("id"));
                        domainSetting.setDomain(rs.getString("url"));
                        domainSetting.setUserDomain(rs.getBoolean("is_userdomain"));
                        domainSetting.setIsLoggedIn(rs.getBoolean("is_logged_in"));
                        domainSetting.setIsDeleted(rs.getBoolean("is_deleted"));
                        domainSetting.setDomainRegDate(rs.getTimestamp("user_date"));
                        domainSetting.setUnSubScribeStatus(rs.getBoolean("tool_unsubscribe"));
                        domainSetting.setWeeklyReport(rs.getBoolean("weekly_report"));
                        return domainSetting;
                    });
        } catch (DataAccessException e) {
            logger.error("Exception in createSocialCompDomainRows: ", e);
        }
        return domSettings;
    }


    /*Update guest users as login users from a SocialCompDomains list. */
    public void updateSocialGuestEntryToLoggedIn(final List<SocialCompDomains> socialCompDomains, final long userId) {
        String query = "update social_media_user set is_logged_in = 1, user_id = ?  where id = ? and is_deleted = 0 ";
        try {
            synchronized (this) {
                this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        statement.setLong(1, userId);
                        statement.setLong(2, socialCompDomains.get(i).getDomainId());
                    }

                    @Override
                    public int getBatchSize() {
                        return socialCompDomains.size();
                    }
                });
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in updateSocialGuestEntryToLoggedIn: ", ex);
        }
    }


    /*Update the GUEST user old data as disabled and this method duplicate to deleteFromSocialMediaUser. */
    public void disableSocialMediaUserGuestData(final List<Integer> socialUserIdList, final long lXRMUserId) {
        String query = "update social_media_user set is_deleted = 1 where id = ? and user_id = ?";
        try {
            synchronized (this) {
                this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        statement.setInt(1, socialUserIdList.get(i));
                        statement.setLong(2, lXRMUserId);
                    }

                    @Override
                    public int getBatchSize() {
                        return socialUserIdList.size();
                    }
                });
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in executing disableSocialMediaUserGuestData: ", ex);
        }
    }

    /*Updateing the is_deleted=0 in social media user for a lxrm user only  if the user is given same url on same date*/
    public boolean enableSocialMediaUserData(final long socialMediaId, final long lXRMUserId, final String date, boolean userLoggedIn) {
        String enableSMUDataQuery;
        boolean queryStatus = false;
        if (userLoggedIn) {
            enableSMUDataQuery = "update social_media_user set is_deleted = 0  where id = ? and user_id = ? and is_logged_in = 1 and tool_unsubscribe = 0 and date_format(social_media_user.user_date,'%Y-%m-%d')= ?";
        } else {
            enableSMUDataQuery = "update social_media_user set is_deleted = 0  where id = ? and user_id = ? and is_logged_in = 0 and tool_unsubscribe = 0 and date_format(social_media_user.user_date,'%Y-%m-%d')= ?";
        }
        logger.debug("update query in enableSocialMediaUserData :" + enableSMUDataQuery + " for socialMediaId is" + socialMediaId + " and lXRMUserId is " + lXRMUserId + " and date is " + date);
        Object[] valLis = new Object[3];
        valLis[0] = socialMediaId;
        valLis[1] = lXRMUserId;
        valLis[2] = date;
        try {
            int rowsEffceted = this.jdbcTemplate.update(enableSMUDataQuery, valLis);
            if (rowsEffceted == 1) {
                queryStatus = true;
            }
        } catch (DataAccessException ex) {
            logger.error("Exception in enableSocialMediaUserData: ", ex);
        }
        return queryStatus;
    }

    @SuppressWarnings("empty-statement")
    public String getMetricValueChangeString(long Value, int growth) {
        String metricValueStr = "<div class='emptyValue'><span class='NoDataSpan'> - </span></div>";
        String growthStr = null;
        if (growth == 0) {
            growthStr = "<span class=\"growthGray\"> ( " + growth + " ) </span>";
        } else if (growth > 0) {
            growthStr = "<span class=\"growthGreen\"> ( + " + growth + " ) </span>";
        } else if (growth < 0) {
            growthStr = "<span class=\"growthRed\"> ( " + growth + " ) </span>";
        }
        metricValueStr = "<div class='metricValue'><span class='m-Number'>" + Value + "</span>" + growthStr + "</div>";
        return metricValueStr;
    }

    public String getMetricValueString(long Value) {
        String metricValueStr;
        metricValueStr = "<div class='metricValue'><span class='m-Number'>" + Value + "</span></div>";
        return metricValueStr;
    }

    public String getMetricValueChangeStringInt(int Value, int growth) {
        String metricValueStr;
        String growthStr = null;
        try {
            if (growth == 0) {
                growthStr = "<span class=\"growthGray\"> ( " + growth + " ) </span>";
            } else if (growth > 0) {
                growthStr = "<span class=\"growthGreen\"> ( + " + growth + " ) </span>";
            } else if (growth < 0) {
                growthStr = "<span class=\"growthRed\"> ( " + growth + " ) </span>";
            }
            metricValueStr = "<div class='metricValue'><span class='m-Number'>" + Value + "</span>" + growthStr + "</div>";
        } catch (Exception e) {
            metricValueStr = "<div class='emptyValue'><span class='NoDataSpan'> - </span></div>";
            logger.error("Exception in getMetricValueChangeString: " + e);
        }
        return metricValueStr;
    }

    public String getMetricValueStringInt(int Value) {
        String metricValueStr;
        metricValueStr = "<div class='metricValue'><span class='m-Number'>" + Value + "</span></div>";
        return metricValueStr;
    }

    public String getNoDataValueString() {
        String metricValueStr;
        metricValueStr = "<div class='emptyValue'><span class='NoDataSpan'> - </span></div>";
        return metricValueStr;
    }

    public String generatePdfForSocialMediaAnalyzer(String targetFilePath, List<Map<String, Object>> allMediaObject, List<SocialCompDomains> socialDomains, String currentDate, String previousDate, long lxrmUserId) {

        int[] totalPages = {0};
        String filename = "";
        String filePath = "";
        filename = Common.createFileName("LXRMarketplace_Social_Media_Analyzer_Report" + "_" + lxrmUserId) + ".pdf";
        filePath = targetFilePath + filename;
        logger.info("SocialMediaAnalyzer report file path: " + filePath);
        /*this is for getting total number of pages present in pdf*/
        generatePdfReport(targetFilePath, allMediaObject, socialDomains, currentDate, previousDate, totalPages, filePath);

        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            logger.info("InterruptedException in making thread sleep cause: ", ex);
        }
        /* creating a pdf*/
        generatePdfReport(targetFilePath, allMediaObject, socialDomains, currentDate, previousDate, totalPages, filePath);
        return filename;
    }

    public void generatePdfReport(String targetFilePath, List<Map<String, Object>> allMediaObject, List<SocialCompDomains> socialDomains, String currentDate, String previousDate, int[] totalPages, String filePath) {

        TwitterMetrics clientTwitterMetrics = new TwitterMetrics();
        FacebookMetrics clientFacebookMetrics = new FacebookMetrics();
        GoogleMetrics clientGooglePlusMetrics = new GoogleMetrics();
        YouTubeMetrics clientYoutubeMetrics = new YouTubeMetrics();
        InstagramMetrics clientInstagramMetrics = new InstagramMetrics();
        PinterestMetrics clientPintrestMetrics = new PinterestMetrics();

        TwitterMetrics firstCompTWMetrics = new TwitterMetrics();
        FacebookMetrics firstCompFBMetrics = new FacebookMetrics();
        GoogleMetrics firstCompGPlusMetrics = new GoogleMetrics();
        YouTubeMetrics firstCompYTMetrics = new YouTubeMetrics();
        InstagramMetrics firstCompInstagramMetrics = new InstagramMetrics();
        PinterestMetrics firstPintrestMetrics = new PinterestMetrics();

        TwitterMetrics secondCompTWMetrics = new TwitterMetrics();
        FacebookMetrics secondCompFBMetrics = new FacebookMetrics();
        GoogleMetrics secondCompGPlusMetrics = new GoogleMetrics();
        YouTubeMetrics secondCompYTMetrics = new YouTubeMetrics();
        InstagramMetrics secondCompInstagramMetrics = new InstagramMetrics();
        PinterestMetrics secondPintrestMetrics = new PinterestMetrics();

        boolean fbStatusForAll;
        boolean twStatusForAll;
//        boolean gplusStatusForAll;
        boolean ytStatusForAll;
//        boolean instStatusForAll;
//        boolean pintStatusForAll;

        try {

            Map<String, Object> client = allMediaObject.get(0);
            if ((TwitterMetrics) client.get("twittermetrics") != null) {
                clientTwitterMetrics = (TwitterMetrics) client.get("twittermetrics");
            }
            if ((FacebookMetrics) client.get("facebookmetrics") != null) {
                clientFacebookMetrics = (FacebookMetrics) client.get("facebookmetrics");
            }
            if ((GoogleMetrics) client.get("googleplusmetrics") != null) {
                clientGooglePlusMetrics = (GoogleMetrics) client.get("googleplusmetrics");
            }
            if ((YouTubeMetrics) client.get("youtubemetrics") != null) {
                clientYoutubeMetrics = (YouTubeMetrics) client.get("youtubemetrics");
            }
            if ((InstagramMetrics) client.get("instagrammetrics") != null) {
                clientInstagramMetrics = (InstagramMetrics) client.get("instagrammetrics");
            }
            if ((PinterestMetrics) client.get("pinterestmetrics") != null) {
                clientPintrestMetrics = (PinterestMetrics) client.get("pinterestmetrics");
            }
            boolean clientFBStatus = clientFacebookMetrics.isFacebookAccountStatus();
            boolean clientTWStatus = clientTwitterMetrics.isTwitterAccountStatus();
            boolean clientGPlusStatus = clientGooglePlusMetrics.isGoogleAccountStatus();
            boolean clientYTStatus = clientYoutubeMetrics.isYoutubeAccountStatus();
            boolean clientInstStatus = clientInstagramMetrics.isInstagramAccountStatus();
            boolean clientPintStatus = clientPintrestMetrics.isPinterestAccountStatus();

            int listSize = allMediaObject.size();
            boolean firstFBCompStatus = false;
            boolean firstTWCompStatus = false;
            boolean firstYTCompStatus = false;

            boolean secondFBCompStatus = false;
            boolean secondTWCompStatus = false;
            boolean secondYTCompStatus = false;
            SimpleDateFormat socialAnalysisDate = new SimpleDateFormat("yyyy-MM-dd");
            String presentDate = socialAnalysisDate.format(Calendar.getInstance().getTime());
            boolean userDomainRegistration = false;
            List<Boolean> urlsGrowth = new ArrayList<>();

            for (SocialCompDomains URL : socialDomains) {
                if (URL.isUserDomain()) {
                    if (presentDate.equals(Common.convertTimeStampintoDate(URL.getDomainRegDate()))) {
                        userDomainRegistration = true;
                    }
                }
                if ((URL.getDomainId() > 0)) {
                    boolean maxmiumData = false;
                    maxmiumData = URL.getMaximumDate() != null ? presentDate.equals(convertDateToString(URL.getMaximumDate(), 1)) : presentDate.equals(convertDateToString(URL.getDomainRegDate(), 1));
                    boolean registrationDate = presentDate.equals(Common.convertTimeStampintoDate(URL.getDomainRegDate()));
                    if (!registrationDate) {
                        if (maxmiumData) {
                            urlsGrowth.add(true);
                        } else if (URL.isGrowthDataAvailable()) {
                            if (userDomainRegistration) {
                                urlsGrowth.add(false);
                            } else {
                                urlsGrowth.add(true);
                            }
                        } else {
                            urlsGrowth.add(false);
                        }
                    } else {
                        urlsGrowth.add(false);
                    }
                }
            }

            boolean clientDateDiff = false;
            boolean firstCompDateDiff = false;
            boolean secondCompDateDiff = false;

            if (urlsGrowth.size() >= 1) {
                clientDateDiff = urlsGrowth.get(0);
            }
            if (urlsGrowth.size() >= 2) {
                firstCompDateDiff = urlsGrowth.get(1);
            }
            if (urlsGrowth.size() >= 3) {
                secondCompDateDiff = urlsGrowth.get(2);
            }

            String todayDate = (new SimpleDateFormat("MMMM dd, yyyy")).format(Calendar.getInstance().getTimeInMillis());
            String clientRegDate = (new SimpleDateFormat("MMMM dd, yyyy")).format(socialDomains.get(0).getDomainRegDate());
            String reportInfo = "";

            if (todayDate.equals(clientRegDate) || (previousDate == null || previousDate.equals(""))) {
//                clientDateDiff = true;
                reportInfo = "Report for the day: " + clientRegDate + ".";
            } else if ((!previousDate.equals(""))) {
                reportInfo = "Date Range: " + previousDate + " Compared to " + currentDate + ".";
            }
            if (listSize > 1) {
                Map<String, Object> firstCompetitor = allMediaObject.get(1);
                if ((TwitterMetrics) firstCompetitor.get("twittermetrics") != null) {
                    firstCompTWMetrics = (TwitterMetrics) firstCompetitor.get("twittermetrics");
                }
                if ((FacebookMetrics) firstCompetitor.get("facebookmetrics") != null) {
                    firstCompFBMetrics = (FacebookMetrics) firstCompetitor.get("facebookmetrics");
                }
                if ((GoogleMetrics) firstCompetitor.get("googleplusmetrics") != null) {
                    firstCompGPlusMetrics = (GoogleMetrics) firstCompetitor.get("googleplusmetrics");
                }
                if ((YouTubeMetrics) firstCompetitor.get("youtubemetrics") != null) {
                    firstCompYTMetrics = (YouTubeMetrics) firstCompetitor.get("youtubemetrics");
                }
                if ((InstagramMetrics) firstCompetitor.get("instagrammetrics") != null) {
                    firstCompInstagramMetrics = (InstagramMetrics) firstCompetitor.get("instagrammetrics");
                }
                if ((PinterestMetrics) firstCompetitor.get("pinterestmetrics") != null) {
                    firstPintrestMetrics = (PinterestMetrics) firstCompetitor.get("pinterestmetrics");
                }

                firstFBCompStatus = firstCompFBMetrics.isFacebookAccountStatus();
                firstTWCompStatus = firstCompTWMetrics.isTwitterAccountStatus();
//                firstGPlusCompStatus = firstCompGPlusMetrics.isGoogleAccountStatus();
                firstYTCompStatus = firstCompYTMetrics.isYoutubeAccountStatus();
//                firstInstCompStatus = firstCompInstagramMetrics.isInstagramAccountStatus();
//                firstPintCompStatus = firstPintrestMetrics.isPinterestAccountStatus();
//                String firstCompRegDate = (new SimpleDateFormat("MMMM dd, yyyy")).format(socialDomains.get(1).getDomainRegDate());
//                if (todayDate.equals(firstCompRegDate)) {
//                    firstCompDateDiff = true;
//                }
            }

            if (listSize == 3) {
                Map<String, Object> secondCompetitor = allMediaObject.get(2);
                if ((TwitterMetrics) secondCompetitor.get("twittermetrics") != null) {
                    secondCompTWMetrics = (TwitterMetrics) secondCompetitor.get("twittermetrics");
                }
                if ((FacebookMetrics) secondCompetitor.get("facebookmetrics") != null) {
                    secondCompFBMetrics = (FacebookMetrics) secondCompetitor.get("facebookmetrics");
                }
                if ((GoogleMetrics) secondCompetitor.get("googleplusmetrics") != null) {
                    secondCompGPlusMetrics = (GoogleMetrics) secondCompetitor.get("googleplusmetrics");
                }

                if ((YouTubeMetrics) secondCompetitor.get("youtubemetrics") != null) {
                    secondCompYTMetrics = (YouTubeMetrics) secondCompetitor.get("youtubemetrics");
                }
                if ((InstagramMetrics) secondCompetitor.get("instagrammetrics") != null) {
                    secondCompInstagramMetrics = (InstagramMetrics) secondCompetitor.get("instagrammetrics");
                }
                if ((PinterestMetrics) secondCompetitor.get("pinterestmetrics") != null) {
                    secondPintrestMetrics = (PinterestMetrics) secondCompetitor.get("pinterestmetrics");
                }

                secondFBCompStatus = secondCompFBMetrics.isFacebookAccountStatus();
                secondTWCompStatus = secondCompTWMetrics.isTwitterAccountStatus();
//                secondGPlusCompStatus = secondCompGPlusMetrics.isGoogleAccountStatus();
                secondYTCompStatus = secondCompYTMetrics.isYoutubeAccountStatus();
//                secondInstCompStatus = secondCompInstagramMetrics.isInstagramAccountStatus();
//                secondPintCompStatus = secondPintrestMetrics.isPinterestAccountStatus();
                String secondCompRegDate = (new SimpleDateFormat("MMMM dd, yyyy")).format(socialDomains.get(2).getDomainRegDate());
//                if (todayDate.equals(secondCompRegDate)) {
//                    secondCompDateDiff = true;
//                }

            }

            fbStatusForAll = checkStatus(clientFBStatus, firstFBCompStatus, secondFBCompStatus);
            twStatusForAll = checkStatus(clientTWStatus, firstTWCompStatus, secondTWCompStatus);
//            gplusStatusForAll = checkStatus(clientGPlusStatus, firstGPlusCompStatus, secondGPlusCompStatus);
            ytStatusForAll = checkStatus(clientYTStatus, firstYTCompStatus, secondYTCompStatus);
//            instStatusForAll = checkStatus(clientInstStatus, firstInstCompStatus, secondInstCompStatus);
//            pintStatusForAll = checkStatus(clientPintStatus, firstPintCompStatus, secondPintCompStatus);

            String applicationUrl = null;
            if (context.getBean("domainName") != null) {
                applicationUrl = (String) context.getBean("domainName");
            }

//            String fbFansUrl, twFollowersUrl, twTweetsUrl, twLikesUrl, InstaFollowersUrl, InstaPostsUrl, pintrestFollowersUrl, pintrestPinsUrl, pintrestBoardsUrl;
//            String googleFollowersUrl, youtubeSubsUrl, youtubeVideosUrl, youtubeViewsUrl;
            String fbFansUrl, twFollowersUrl, twTweetsUrl, twLikesUrl, youtubeSubsUrl, youtubeVideosUrl, youtubeViewsUrl;
            switch (listSize) {
                case 3:
                    fbFansUrl = getBarChart(listSize, clientFacebookMetrics.getLikesCount(), firstCompFBMetrics.getLikesCount(), secondCompFBMetrics.getLikesCount(), "Likes", socialDomains);
                    twFollowersUrl = getBarChart(listSize, clientTwitterMetrics.getFollowers(), firstCompTWMetrics.getFollowers(), secondCompTWMetrics.getFollowers(), "Followers", socialDomains);
                    twTweetsUrl = getBarChart(listSize, clientTwitterMetrics.getTweets(), firstCompTWMetrics.getTweets(), secondCompTWMetrics.getTweets(), "Tweets", socialDomains);
                    twLikesUrl = getBarChart(listSize, clientTwitterMetrics.getFavourites(), firstCompTWMetrics.getFavourites(), secondCompTWMetrics.getFavourites(), "Likes", socialDomains);
                    youtubeSubsUrl = getBarChart(listSize, clientYoutubeMetrics.getSubscribers(), firstCompYTMetrics.getSubscribers(), secondCompYTMetrics.getSubscribers(), "Subscribers", socialDomains);
                    youtubeVideosUrl = getBarChart(listSize, clientYoutubeMetrics.getVideos(), firstCompYTMetrics.getVideos(), secondCompYTMetrics.getVideos(), "Videos", socialDomains);
                    youtubeViewsUrl = getBarChart(listSize, clientYoutubeMetrics.getViews(), firstCompYTMetrics.getViews(), secondCompYTMetrics.getViews(), "Views", socialDomains);
                    break;
                case 2:
                    fbFansUrl = getBarChart(listSize, clientFacebookMetrics.getLikesCount(), firstCompFBMetrics.getLikesCount(), 0, "Likes", socialDomains);
                    twFollowersUrl = getBarChart(listSize, clientTwitterMetrics.getFollowers(), firstCompTWMetrics.getFollowers(), 0, "Followers", socialDomains);
                    twTweetsUrl = getBarChart(listSize, clientTwitterMetrics.getTweets(), firstCompTWMetrics.getTweets(), 0, "Tweets", socialDomains);
                    twLikesUrl = getBarChart(listSize, clientTwitterMetrics.getFavourites(), firstCompTWMetrics.getFavourites(), 0, "Likes", socialDomains);
                    youtubeSubsUrl = getBarChart(listSize, clientYoutubeMetrics.getSubscribers(), firstCompYTMetrics.getSubscribers(), 0, "Subscribers", socialDomains);
                    youtubeVideosUrl = getBarChart(listSize, clientYoutubeMetrics.getVideos(), firstCompYTMetrics.getVideos(), 0, "Videos", socialDomains);
                    youtubeViewsUrl = getBarChart(listSize, clientYoutubeMetrics.getViews(), firstCompYTMetrics.getViews(), 0, "Views", socialDomains);
                    break;
                default:
                    fbFansUrl = getBarChart(listSize, clientFacebookMetrics.getLikesCount(), 0, 0, "Likes", socialDomains);
                    twFollowersUrl = getBarChart(listSize, clientTwitterMetrics.getFollowers(), 0, 0, "Followers", socialDomains);
                    twTweetsUrl = getBarChart(listSize, clientTwitterMetrics.getTweets(), 0, 0, "Tweets", socialDomains);
                    twLikesUrl = getBarChart(listSize, clientTwitterMetrics.getFavourites(), 0, 0, "Likes", socialDomains);
                    youtubeSubsUrl = getBarChart(listSize, clientYoutubeMetrics.getSubscribers(), 0, 0, "Subscribers", socialDomains);
                    youtubeVideosUrl = getBarChart(listSize, clientYoutubeMetrics.getVideos(), 0, 0, "Videos", socialDomains);
                    youtubeViewsUrl = getBarChart(listSize, clientYoutubeMetrics.getViews(), 0, 0, "Views", socialDomains);
                    break;
            }

            Font reportNameFont = new Font(FontFactory.getFont("Arial", 13, Font.BOLD, new BaseColor(255, 110, 0)));
            Font orangeFont = new Font(FontFactory.getFont("Arial", 14, Font.BOLD, new BaseColor(255, 110, 0)));

            Font elementDescFont = new Font(FontFactory.getFont("Arial", 11f, Font.BOLD, new BaseColor(38, 38, 38)));
            Font NoteParaFont = new Font(FontFactory.getFont("Arial", 11f, Font.NORMAL, new BaseColor(38, 38, 38)));
            Font reviewURLFont = new Font(FontFactory.getFont("Arial", 11f, Font.BOLD, new BaseColor(38, 38, 38)));

            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter writer = PdfWriter.getInstance(pdfDoc, stream);

            MyFooter event = new MyFooter(totalPages[0]);
            writer.setPageEvent(event);
            pdfDoc.open();
            writer.getCurrentPageNumber();
//            Image img = com.itextpdf.text.Image.getInstance(applicationUrl + "/resources/images/tools/lxr-logo.png");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);

            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Social Media Analyzer", reportNameFont);
            String reviewURL = trimWebsitesName(socialDomains.get(0).getDomain());
            Chunk urlHeading = new Chunk(reviewURL + ".", reviewURLFont);
            Chunk reviewHeading = new Chunk("Review of ", NoteParaFont);
            //    Chunk normalHeading = new Chunk();

            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};

            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);

            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);

            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            domHeading.add(reviewHeading);
            domHeading.add(urlHeading);

            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();

            PdfPCell dateName = new PdfPCell(new Phrase(reportInfo, NoteParaFont));

            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();

            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(10f);
            rpName.setPaddingBottom(10f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            imgTable.setSpacingAfter(25f);

            String headersDef = " With this report, you’ll be able to review the overall reach and engagement of social media channels for both you and your competitor's domain."
                    + "Our report helps you visualize the metrics that track your social media influence in order to improve the overall performance of your social campaigns.";
            addTitlePage(pdfDoc, "Social Media Analyzer: ", headersDef, orangeFont, NoteParaFont);

            Paragraph activityHeading = new Paragraph("ACTIVITY: ", orangeFont);
            activityHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            activityHeading.setSpacingBefore(1);

            Paragraph facebookHeading = getTitleAndImage("FACEBOOK", applicationUrl + "/resources/images/tools/facebook.png");
            facebookHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            facebookHeading.setSpacingAfter(8f);

            Paragraph centerAlignPara = null;

            /*Facebook profile table Construction*/
            PdfPCell metrics = new PdfPCell(new Phrase("Metrics", elementDescFont));
            PdfPTable facebookTable = new PdfPTable(listSize + 1);
            if (listSize > 2) {
                float[] tableWidth = {19f, 27f, 27f, 27f};
                facebookTable = new PdfPTable(tableWidth);
            }
            facebookTable.setWidthPercentage(93f);

            float[] fbGraphWidth = {99.9f};
//            PdfPTable facebookGraph = new PdfPTable(fbGraphWidth);

            Image fbFansImg = null;
            if (fbStatusForAll) {
                facebookTable.addCell(metrics);
                for (int i = 0; i < socialDomains.size(); i++) {
                    if (socialDomains.get(i).getDomain() != null) {
                        String faceBookURL = trimWebsitesName(socialDomains.get(i).getDomain());
                        centerAlignPara = new Paragraph("" + faceBookURL, elementDescFont);
                        centerAlignPara.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                        /*PdfPCell firstRow = new PdfPCell(new Phrase("" + faceBookURL, elementDescFont));*/
                        PdfPCell firstRow = new PdfPCell(centerAlignPara);
                        firstRow.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                        facebookTable.addCell(firstRow);
                    }
                }

                PdfPCell fbFans = createPdfCell("Likes", clientDateDiff);
                facebookTable.addCell(fbFans);
                PdfPCell mainSiteFBFollowers = new PdfPCell(getGrowthCell("" + clientFacebookMetrics.getLikesCount(), "" + clientFacebookMetrics.getChange(), clientFBStatus, clientDateDiff));
                mainSiteFBFollowers.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                facebookTable.addCell(mainSiteFBFollowers);
                if (listSize > 1) {
                    PdfPCell firstCompetitorFansfb = new PdfPCell(getGrowthCell("" + firstCompFBMetrics.getLikesCount(), "" + firstCompFBMetrics.getChange(), firstFBCompStatus, firstCompDateDiff));
                    firstCompetitorFansfb.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                    facebookTable.addCell(firstCompetitorFansfb);
                }
                if (listSize > 2) {
                    PdfPCell secondCompetitorFansfb = new PdfPCell(getGrowthCell("" + secondCompFBMetrics.getLikesCount(), "" + secondCompFBMetrics.getChange(), secondFBCompStatus, secondCompDateDiff));
                    facebookTable.addCell(secondCompetitorFansfb);
                }
                /*Facebook Fans Graph Construction*/
                try {
                    if (fbFansUrl != null) {
                        fbFansImg = Image.getInstance(new URL(fbFansUrl));
                        fbFansImg.scaleToFit(400f, 350f);
                        fbFansImg.setAlignment(5);
//                    createPdfGraphCell(fbFansImg);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Twitter Likes Graph for PDF", e);
                }
//                facebookGraph.addCell(fbFansGraph);
            } else {
                facebookTable = new PdfPTable(1);
                facebookTable.addCell(getRPLeftCell("noAccount"));
            }

            /*Twitter profile heading Construction*/
            Paragraph twitterHeading = getTitleAndImage("TWITTER", applicationUrl + "/resources/images/tools/twitter.png");
            twitterHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            twitterHeading.setSpacingAfter(8f);

            /* Hash Tag Para Heading Construction*/
            Paragraph hashTagPara = new Paragraph("HASH TAGS: ", orangeFont);
            hashTagPara.setSpacingBefore(10);
            hashTagPara.setSpacingAfter(10);

            /*Twitter profile Table Construction*/
            PdfPTable twitterTable = new PdfPTable(listSize + 1);
            if (listSize > 2) {
                float[] tableWidth = {19f, 27f, 27f, 27f};
                twitterTable = new PdfPTable(tableWidth);
            }
            twitterTable.setWidthPercentage(93f);
            Image twTweetsImg = null;
            Image twFollowersImg = null;
            Image twLikesImg = null;
            if (twStatusForAll) {
                twitterTable.addCell(metrics);
                for (int i = 0; i < socialDomains.size(); i++) {
                    if (socialDomains.get(i).getDomain() != null) {
                        String twiiterURL = trimWebsitesName(socialDomains.get(i).getDomain());
                        PdfPCell firstRow = new PdfPCell(new Phrase("" + twiiterURL, elementDescFont));
                        firstRow.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                        twitterTable.addCell(firstRow);
                    }
                }

                PdfPCell twitterFollowers = createPdfCell("Followers", clientDateDiff);
                twitterTable.addCell(twitterFollowers);

                PdfPCell mainSiteTwFollowers = new PdfPCell(getGrowthCell("" + clientTwitterMetrics.getFollowers(), "" + clientTwitterMetrics.getChange(), clientTWStatus, clientDateDiff));
                twitterTable.addCell(mainSiteTwFollowers);

                if (listSize > 1) {
                    PdfPCell firstCompetitorTwFollowers = new PdfPCell(getGrowthCell("" + firstCompTWMetrics.getFollowers(), "" + firstCompTWMetrics.getChange(), firstTWCompStatus, firstCompDateDiff));
                    twitterTable.addCell(firstCompetitorTwFollowers);
                }
                if (listSize > 2) {
                    PdfPCell secondCompetitorTwFollowers = new PdfPCell(getGrowthCell("" + secondCompTWMetrics.getFollowers(), "" + secondCompTWMetrics.getChange(), secondTWCompStatus, secondCompDateDiff));
                    twitterTable.addCell(secondCompetitorTwFollowers);
                }

                PdfPCell twitterTweets = createPdfCell("Tweets", clientDateDiff);
                twitterTable.addCell(twitterTweets);

                PdfPCell mainSiteTwTweets = new PdfPCell(getGrowthCell("" + clientTwitterMetrics.getTweets(), "" + clientTwitterMetrics.getTwitteractivityChange(), clientTWStatus, clientDateDiff));
                twitterTable.addCell(mainSiteTwTweets);

                if (listSize > 1) {
                    PdfPCell firstCompetitorTwTweets = new PdfPCell(getGrowthCell("" + firstCompTWMetrics.getTweets(), "" + firstCompTWMetrics.getTwitteractivityChange(), firstTWCompStatus, firstCompDateDiff));
                    twitterTable.addCell(firstCompetitorTwTweets);
                }
                if (listSize > 2) {
                    PdfPCell secondCompetitorTwTweets = new PdfPCell(getGrowthCell("" + secondCompTWMetrics.getTweets(), "" + secondCompTWMetrics.getTwitteractivityChange(), secondTWCompStatus, secondCompDateDiff));
                    twitterTable.addCell(secondCompetitorTwTweets);
                }

                PdfPCell twitterLikes = createPdfCell("Likes", clientDateDiff);
                twitterTable.addCell(twitterLikes);

                PdfPCell mainSiteTwLikes = new PdfPCell(getGrowthCell("" + clientTwitterMetrics.getFavourites(), "" + clientTwitterMetrics.getTwitterFavouritesChange(), clientTWStatus, clientDateDiff));
                twitterTable.addCell(mainSiteTwLikes);

                if (listSize > 1) {
                    PdfPCell firstCompetitorTwLikes = new PdfPCell(getGrowthCell("" + firstCompTWMetrics.getFavourites(), "" + firstCompTWMetrics.getTwitterFavouritesChange(), firstTWCompStatus, firstCompDateDiff));
                    twitterTable.addCell(firstCompetitorTwLikes);
                }
                if (listSize > 2) {
                    PdfPCell secondCompetitorTwLikes = new PdfPCell(getGrowthCell("" + secondCompTWMetrics.getFavourites(), "" + secondCompTWMetrics.getTwitterFavouritesChange(), secondTWCompStatus, secondCompDateDiff));
                    twitterTable.addCell(secondCompetitorTwLikes);
                }

                PdfPCell twitterFavorites = createPdfCell("Following", clientDateDiff);
                twitterTable.addCell(twitterFavorites);

                PdfPCell mainSiteTwFavorites = new PdfPCell(getGrowthCell("" + clientTwitterMetrics.getFollowing(), "" + clientTwitterMetrics.getTwitterFollowingChange(), clientTWStatus, clientDateDiff));
                twitterTable.addCell(mainSiteTwFavorites);

                if (listSize > 1) {
                    PdfPCell firstCompetitorTwFavorites = new PdfPCell(getGrowthCell("" + firstCompTWMetrics.getFollowing(), "" + firstCompTWMetrics.getTwitterFollowingChange(), firstTWCompStatus, firstCompDateDiff));
                    twitterTable.addCell(firstCompetitorTwFavorites);
                }
                if (listSize > 2) {
                    PdfPCell secondCompetitorTwFavorites = new PdfPCell(getGrowthCell("" + secondCompTWMetrics.getFollowing(), "" + secondCompTWMetrics.getTwitterFollowingChange(), secondTWCompStatus, secondCompDateDiff));
                    twitterTable.addCell(secondCompetitorTwFavorites);
                }

                /*Twitter followers graph heading Construction*/
                try {
                    if (twFollowersUrl != null) {
                        twFollowersImg = Image.getInstance(new URL(twFollowersUrl));
                        twFollowersImg.scaleToFit(400f, 350f);
                        twFollowersImg.setAlignment(5);
//                    createPdfGraphCell(twFollowersImg);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Twitter Likes Graph for PDF", e);
                }
//                twitterGraph.addCell(twFollowersGraph);

                /*Twitter Tweets graph heading Construction*/
                try {
                    if (twTweetsUrl != null) {
                        twTweetsImg = Image.getInstance(new URL(twTweetsUrl));
                        twTweetsImg.scaleToFit(400f, 350f);
                        twTweetsImg.setAlignment(5);
                        //                    createPdfGraphCell(twTweetsImg);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Twitter Likes Graph for PDF", e);
                }
//                twitterGraph.addCell(twTweetsGraph);
                /*Twitter Likes graph heading Construction*/
                try {
                    if (twLikesUrl != null) {
                        twLikesImg = Image.getInstance(new URL(twLikesUrl));
                        twLikesImg.scaleToFit(400f, 350f);
                        twLikesImg.setAlignment(5);
                        //createPdfGraphCell(twLikesImg);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Twitter Likes Graph for PDF", e);
                }
//                twitterGraph.addCell(twLikesGraph);
            } else {
                twitterTable = new PdfPTable(1);
                twitterTable.addCell(getRPLeftCell("noAccount"));
            }

//      Youtube Info 
            Paragraph youtubeHeading = getTitleAndImage("YOUTUBE", applicationUrl + "/resources/images/tools/youtube.png");
            youtubeHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            youtubeHeading.setSpacingAfter(8f);

//      Youtube Table Construction
            PdfPTable youtubeTable = new PdfPTable(listSize + 1);
            if (listSize > 2) {
                float[] tableWidth = {19f, 27f, 27f, 27f};
                youtubeTable = new PdfPTable(tableWidth);
            }
            youtubeTable.setWidthPercentage(93f);
//            PdfPTable youtubeGraph = new PdfPTable(1);

            Image youtubeSubsImage = null;
            Image youtubeVideosImage = null;
            Image youtubeViewsImage = null;
            PdfPCell youtubeFirstRow = null;
            if (ytStatusForAll) {
                youtubeTable.addCell(metrics);
                for (int i = 0; i < socialDomains.size(); i++) {
                    if (socialDomains.get(i).getDomain() != null) {
                        String youTubeURL = trimWebsitesName(socialDomains.get(i).getDomain());
                        if (youTubeURL != null) {
                            youtubeFirstRow = new PdfPCell(new Phrase("" + youTubeURL, elementDescFont));
                        } else {
                            youtubeFirstRow = new PdfPCell(new Phrase("" + socialDomains.get(i).getDomain(), elementDescFont));
                        }
                        youtubeFirstRow.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                        youtubeTable.addCell(youtubeFirstRow);
                    }
                }

                PdfPCell youtubeSubscribers = createPdfCell("Subscribers", clientDateDiff);
                youtubeTable.addCell(youtubeSubscribers);

                PdfPCell mainSiteYoutubeSubs = new PdfPCell(getGrowthCell("" + clientYoutubeMetrics.getSubscribers(), "" + clientYoutubeMetrics.getChange(), clientYTStatus, clientDateDiff));
                youtubeTable.addCell(mainSiteYoutubeSubs);

                if (listSize > 1) {
                    PdfPCell firstCompYoutubeSubs = new PdfPCell(getGrowthCell("" + firstCompYTMetrics.getSubscribers(), "" + firstCompYTMetrics.getChange(), firstYTCompStatus, firstCompDateDiff));
                    youtubeTable.addCell(firstCompYoutubeSubs);
                }
                if (listSize > 2) {
                    PdfPCell secondCompYoutubeSubs = new PdfPCell(getGrowthCell("" + secondCompYTMetrics.getSubscribers(), "" + secondCompYTMetrics.getChange(), secondYTCompStatus, secondCompDateDiff));
                    youtubeTable.addCell(secondCompYoutubeSubs);
                }

                PdfPCell youtubeVideos = createPdfCell("Videos", clientDateDiff);
                youtubeTable.addCell(youtubeVideos);

                PdfPCell mainSiteYoutubeVideos = new PdfPCell(getGrowthCell("" + clientYoutubeMetrics.getVideos(), "" + clientYoutubeMetrics.getYouTubeActivityChange(), clientYTStatus, clientDateDiff));
                youtubeTable.addCell(mainSiteYoutubeVideos);

                if (listSize > 1) {
                    PdfPCell firstCompYoutubeVideos = new PdfPCell(getGrowthCell("" + firstCompYTMetrics.getVideos(), "" + firstCompYTMetrics.getYouTubeActivityChange(), firstYTCompStatus, firstCompDateDiff));
                    youtubeTable.addCell(firstCompYoutubeVideos);
                }
                if (listSize > 2) {
                    PdfPCell secondCompYoutubeVideos = new PdfPCell(getGrowthCell("" + secondCompYTMetrics.getVideos(), "" + secondCompYTMetrics.getYouTubeActivityChange(), secondYTCompStatus, secondCompDateDiff));
                    youtubeTable.addCell(secondCompYoutubeVideos);
                }

                PdfPCell youtubeViews = createPdfCell("Views", clientDateDiff);
                youtubeTable.addCell(youtubeViews);

                PdfPCell mainSiteYoutubeViews = new PdfPCell(getGrowthCell("" + clientYoutubeMetrics.getViews(), "" + clientYoutubeMetrics.getYouTubeViewsChange(), clientYTStatus, clientDateDiff));
                youtubeTable.addCell(mainSiteYoutubeViews);
                if (listSize > 1) {
                    PdfPCell firstCompYoutubeViews = new PdfPCell(getGrowthCell("" + firstCompYTMetrics.getViews(), "" + firstCompYTMetrics.getYouTubeViewsChange(), firstYTCompStatus, firstCompDateDiff));
                    youtubeTable.addCell(firstCompYoutubeViews);
                }
                if (listSize > 2) {
                    PdfPCell secondCompYoutubeViews = new PdfPCell(getGrowthCell("" + secondCompYTMetrics.getViews(), "" + secondCompYTMetrics.getYouTubeViewsChange(), secondYTCompStatus, secondCompDateDiff));
                    youtubeTable.addCell(secondCompYoutubeViews);
                }

//      Youtube Subscribers Graph
                try {
                    if (youtubeSubsUrl != null) {
                        youtubeSubsImage = Image.getInstance(new URL(youtubeSubsUrl));
                        youtubeSubsImage.scaleToFit(400f, 350f);
                        youtubeSubsImage.setAlignment(5);
//                    createPdfGraphCell(youtubeSubsImage);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Youtube Subscribers Graph for PDF" + e);
                }
//                youtubeGraph.addCell(youtubeSubsGraph);

//      Youtube Videos Graph
                try {
                    if (youtubeVideosUrl != null) {
                        youtubeVideosImage = Image.getInstance(new URL(youtubeVideosUrl));
                        youtubeVideosImage.scaleToFit(400f, 350f);
                        youtubeVideosImage.setAlignment(5);
//                    createPdfGraphCell(youtubeVideosImage);
                    }
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Youtube Videos Graph for PDF" + e);
                }
//                youtubeGraph.addCell(youtubeVideosGraph);

//      Youtube Views Graph
                try {
                    if (youtubeViewsUrl != null) {
                        youtubeViewsImage = Image.getInstance(new URL(youtubeViewsUrl));
                        youtubeViewsImage.scaleToFit(400f, 350f);
                        youtubeViewsImage.setAlignment(5);
                    }
//                    createPdfGraphCell(youtubeViewsImage);
                } catch (BadElementException | IOException e) {
                    logger.error("Exception in preparing Youtube Views Graph for PDF" + e);
                }
//                youtubeGraph.addCell(youtubeViewsGraph);
            } else {
                youtubeTable = new PdfPTable(1);
                youtubeTable.addCell(getRPLeftCell("noAccount"));
            }


            /*Recent Posts*/
            Paragraph recentPostHeading = new Paragraph("YOUR RECENT POSTS: ", orangeFont);
            recentPostHeading.setSpacingBefore(25);
            recentPostHeading.setSpacingAfter(10);
            /*Twitter Recent Posts*/
            Paragraph rpTwHeading = getTitleAndImage("TWITTER", applicationUrl + "/resources/images/tools/twitter.png");
            rpTwHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpTwHeading.setSpacingAfter(8f);
            float[] tableWidth = {25f, 75f};
            PdfPTable rpTwTable = new PdfPTable(tableWidth);
            /*Crerating recent post section based on Account Exsitences and Post Content/Image i.e
             Post image is existed (OR) Post descripition is existed.*/
            if ((clientTWStatus)
                    && ((clientTwitterMetrics.getReadPath() != null && !clientTwitterMetrics.getReadPath().equals(""))
                    || ((clientTwitterMetrics.getRecentTweetText() != null) && (!clientTwitterMetrics.getRecentTweetText().equals(""))))) {
                rpTwTable.setWidthPercentage(99.9f);
                PdfPCell leftTwCell = null;
                if (clientTwitterMetrics.getReadPath() != null && !clientTwitterMetrics.getReadPath().equals("")) {
                    leftTwCell = getRPLeftCell(downloadFolder + clientTwitterMetrics.getReadPath());
                }
                if (leftTwCell == null) {
                    leftTwCell = getRPLeftCell("");
                }
                rpTwTable.addCell(leftTwCell);
                Map<String, String> twitterTableInfo = new LinkedHashMap<>();
                twitterTableInfo.put("Retweets", "" + clientTwitterMetrics.getRecentTweetRetweets());
                twitterTableInfo.put("Favorites", "" + clientTwitterMetrics.getRecentTweetFavourites());
                twitterTableInfo.put("Engagement", "" + clientTwitterMetrics.getEngagement());
                twitterTableInfo.put("Audience Engaged", "" + clientTwitterMetrics.getAudienceEngaged() + "%");
                PdfPCell rightTwCell = getRightCell(clientTwitterMetrics.getTweetDate(), clientTwitterMetrics.getRecentTweetText(), twitterTableInfo);
                rpTwTable.addCell(rightTwCell);
            } else {
                rpTwTable = new PdfPTable(1);
                rpTwTable.addCell(getRPLeftCell("invalid"));
            }

            /* Constructing FaceBook Recent Post */
            Paragraph rpFBHeading = getTitleAndImage("FACEBOOK", applicationUrl + "/resources/images/tools/facebook.png");
            rpFBHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpFBHeading.setSpacingAfter(8f);

            PdfPTable rpFBTable = new PdfPTable(tableWidth);

            /*Crerating recent post section based on Account Exsitences and Post Content/Image i.e
             Post image is existed (OR) Post descripition is existed.*/
            if ((clientFBStatus)
                    && ((clientFacebookMetrics.getPostImagePath() != null && !clientFacebookMetrics.getPostImagePath().equals(""))
                    || ((clientFacebookMetrics.getPostdescription() != null) && (!clientFacebookMetrics.getPostdescription().equals(""))))) {
                rpFBTable.setWidthPercentage(99.9f);
                PdfPCell leftFBCell = null;
                if (clientFacebookMetrics.getPostImagePath() != null && !clientFacebookMetrics.getPostImagePath().equals("")) {
                    leftFBCell = getRPLeftCell(downloadFolder + clientFacebookMetrics.getPostImagePath());
                }
//            leftFBCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                if (leftFBCell == null) {
                    leftFBCell = getRPLeftCell("");
                }
                rpFBTable.addCell(leftFBCell);
                Map<String, String> facebookTableInfo = new LinkedHashMap<>();
                facebookTableInfo.put("Post Likes", "" + clientFacebookMetrics.getPostLikes());
                facebookTableInfo.put("Comments", "" + clientFacebookMetrics.getPostcomments());
                facebookTableInfo.put("Shares", "" + clientFacebookMetrics.getPostShares());
                facebookTableInfo.put("Engagement", "" + clientFacebookMetrics.getEngagement());
                facebookTableInfo.put("Audience Engaged", "" + clientFacebookMetrics.getAudienceEngaged() + "%");
                PdfPCell rightFBCell = getRightCell(clientFacebookMetrics.getPostCreatedTime(), clientFacebookMetrics.getPostdescription(), facebookTableInfo);
                rpFBTable.addCell(rightFBCell);
            } else {
                rpFBTable = new PdfPTable(1);
                rpFBTable.addCell(getRPLeftCell("invalid"));
            }

//      Youtube Recent Posts
            Paragraph rpYTHeading = getTitleAndImage("YOUTUBE", (applicationUrl + "/resources/images/tools/youtube.png"));
            rpYTHeading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpYTHeading.setSpacingAfter(8f);
            PdfPTable rpYTTable = new PdfPTable(tableWidth);
            /*Crerating recent post section based on Account Exsitences and Post Content/Image i.e
             Post image is existed (OR) Post descripition is existed.*/
            if (clientYTStatus
                    && ((clientYoutubeMetrics.getYoutubeUserLogPath() != null && !clientYoutubeMetrics.getYoutubeUserLogPath().equals(""))
                    || ((clientYoutubeMetrics.getRecentMediaTitle() != null) && (!clientYoutubeMetrics.getRecentMediaTitle().equals(""))))) {
                rpYTTable.setWidthPercentage(99.9f);
                PdfPCell leftYTCell = null;
                if (clientYoutubeMetrics.getYoutubeUserLogPath() != null && !clientYoutubeMetrics.getYoutubeUserLogPath().equals("")) {
                    leftYTCell = getRPLeftCell(downloadFolder + clientYoutubeMetrics.getYoutubeUserLogPath());
                }
                if (leftYTCell == null) {
                    leftYTCell = getRPLeftCell("");
                }
                rpYTTable.addCell(leftYTCell);
                Map<String, String> youtubeTableInfo = new LinkedHashMap<>();
                youtubeTableInfo.put("Views", "" + clientYoutubeMetrics.getRecentMediaviews());
                youtubeTableInfo.put("Likes", "" + clientYoutubeMetrics.getRecentMediaLikes());
                youtubeTableInfo.put("Comments", "" + clientYoutubeMetrics.getRecentMediaComments());
                youtubeTableInfo.put("Engagement", "" + clientYoutubeMetrics.getEngagement());
                youtubeTableInfo.put("Audience Engaged", "" + clientYoutubeMetrics.getAudienceEngaged() + "%");
                PdfPCell rightYTCell = getRightCell(clientYoutubeMetrics.getUserPostDate(), clientYoutubeMetrics.getRecentMediaTitle(), youtubeTableInfo);
                rpYTTable.addCell(rightYTCell);
            } else {
                rpYTTable = new PdfPTable(1);
                rpYTTable.addCell(getRPLeftCell("invalid"));
            }

            /*Adding facebook channel heading, table, graph to parent doc.*/
            pdfDoc.add(activityHeading);
            pdfDoc.add(facebookHeading);
            pdfDoc.add(facebookTable);
            try {

                if (fbStatusForAll) {
                    pdfDoc.add(fbFansImg);
                }
            } catch (Exception e) {
                logger.error("Exception when adding facebook graph image to pdf", e);
            }
            /*Checking condition reached end of page or not .*/
            if (fbStatusForAll) {
                if (writer.getVerticalPosition(true) - twitterTable.getRowHeight(0) - twitterTable.getRowHeight(1) < pdfDoc.bottom()) {
                    pdfDoc.newPage();
                }
            }
            /*Adding twitter channel heading, table, graph to parent doc.*/
            pdfDoc.add(twitterHeading);
            pdfDoc.add(twitterTable);
//            pdfDoc.newPage();
            try {
                if (twStatusForAll) {
                    if (twFollowersImg != null) {
                        pdfDoc.add(twFollowersImg);
                    }
                    if (twTweetsImg != null) {
                        pdfDoc.add(twTweetsImg);
                    }
                    pdfDoc.newPage();
                    if (twLikesImg != null) {
                        pdfDoc.add(twLikesImg);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception when adding twitter twFollowersImg/twTweetsImg/twLikesImgs graph image to pdf", e);
            }
            try {
                pdfDoc.add(hashTagPara);
                if (twStatusForAll) {
                    if ((!clientTwitterMetrics.getTwitterHashTag().equalsIgnoreCase("NoHashTag"))
                            || (!firstCompTWMetrics.getTwitterHashTag().equalsIgnoreCase("NoHashTag"))
                            || (!secondCompTWMetrics.getTwitterHashTag().equalsIgnoreCase("NoHashTag"))) {
                        if (socialDomains.size() >= 1) {
                            pdfDoc.add(addHashTag(socialDomains.get(0).getDomain(), clientTwitterMetrics.getTwitterHashTag(), elementDescFont, NoteParaFont, clientTwitterMetrics.isTwitterAccountStatus()));
                        }
                        if (socialDomains.size() >= 2) {
                            pdfDoc.add(addHashTag(socialDomains.get(1).getDomain(), firstCompTWMetrics.getTwitterHashTag(), elementDescFont, NoteParaFont, firstCompTWMetrics.isTwitterAccountStatus()));
                        }
                        if (socialDomains.size() >= 3) {
                            pdfDoc.add(addHashTag(socialDomains.get(2).getDomain(), secondCompTWMetrics.getTwitterHashTag(), elementDescFont, NoteParaFont, secondCompTWMetrics.isTwitterAccountStatus()));
                        }
                    } else {
                        pdfDoc.add(addHashTag(socialDomains.get(0).getDomain(), "NoHashTag", elementDescFont, NoteParaFont, false));
                    }
                } else {
                    pdfDoc.add(addHashTag(socialDomains.get(0).getDomain(), "NoHashTag", elementDescFont, NoteParaFont, false));
                }
            } catch (Exception e) {
                logger.error("Exception when adding HASH Tag para to pdf doc", e);
            }
            try {
                pdfDoc.add(youtubeHeading);
                if (writer.getVerticalPosition(true) - youtubeTable.getRowHeight(0) - youtubeTable.getRowHeight(1) < pdfDoc.bottom()) {
                    pdfDoc.newPage();
                }
                pdfDoc.add(youtubeTable);
                if (ytStatusForAll) {
                    if (youtubeSubsImage != null) {
                        pdfDoc.add(youtubeSubsImage);
                    }
                    if (youtubeVideosImage != null) {
                        pdfDoc.add(youtubeVideosImage);
                    }
                    if (youtubeViewsImage != null) {
                        pdfDoc.add(youtubeViewsImage);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in adding youtube metrics data to pdf doc cuase:", e);
            }
            /* Recent Posts Data Construction*/
            pdfDoc.newPage();

            pdfDoc.add(recentPostHeading);
            try {
                pdfDoc.add(rpFBHeading);
                pdfDoc.add(rpFBTable);
            } catch (Exception e) {
                rpFBTable = new PdfPTable(1);
                rpFBTable.addCell(getRPLeftCell("invalid"));
                pdfDoc.add(rpFBTable);
                logger.error("Exception in adding facebook recent post data to pdf doc cuase:", e);
            }
            try {
                pdfDoc.add(rpTwHeading);
                pdfDoc.add(rpTwTable);
            } catch (Exception e) {
                rpTwTable = new PdfPTable(1);
                rpTwTable.addCell(getRPLeftCell("invalid"));
                pdfDoc.add(rpTwTable);
                logger.error("Exception in adding twitter recent post data to pdf doc cuase:", e);
            }
            try {
//                if (writer.getVerticalPosition(true) - rpYTTable.getRowHeight(0) - rpYTTable.getRowHeight(1) < pdfDoc.bottom()) {
//                    pdfDoc.newPage();
//                }
                pdfDoc.add(rpYTHeading);
                pdfDoc.add(rpYTTable);
            } catch (Exception e) {
                rpYTTable = new PdfPTable(1);
                rpYTTable.addCell(getRPLeftCell("invalid"));
                pdfDoc.add(rpYTTable);
                logger.error("Exception in adding YouTube recent post data to pdf doc cuase:", e);
            }

            pdfDoc.close();
            totalPages[0] = writer.getCurrentPageNumber() - 1;
        } catch (BeansException | DocumentException | IOException e) {
            logger.error("Excpetion creating PDF " + e.getMessage() + " for domain id " + socialDomains.get(0).getDomainId() + ", cause is: ", e);
        }
    }

    public PdfPCell createPdfCell(String cellContent, boolean dateDiffStatus) {
        Font commonFont = new Font(FontFactory.getFont("Arial", 10f, Font.NORMAL, new BaseColor(38, 38, 38)));
        PdfPCell newCell = null;
        if (dateDiffStatus) {
            Chunk content = new Chunk(cellContent, commonFont);
            Chunk changeInfo = new Chunk(" (Change)", commonFont);
            Phrase contentChange = new Phrase();
            contentChange.add(content);
            contentChange.add(changeInfo);
            newCell = new PdfPCell(contentChange);
        } else {
            newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        }
        newCell.setMinimumHeight(20f);
        newCell.setVerticalAlignment(Rectangle.ALIGN_CENTER);
        return newCell;
    }

    public PdfPCell createPdfGraphCell(Image cellContent) {
        PdfPCell newCell = new PdfPCell(new Phrase(new Chunk(cellContent, 0, 0, true)));
        newCell.setBorder(Rectangle.NO_BORDER);
        newCell.setPaddingTop(10);
        newCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
        return newCell;
    }

    public String getBarChart(int listSize, long clientInfo, long firstCompetitor, long secondCompetitor, String title, List<SocialCompDomains> socialDomains) {
        BarChart bc = null;
        try {
            Dimension barChartDimension = new Dimension();
            barChartDimension.setSize(800, 350);
            bc = new BarChart(barChartDimension, BarChartOrientation.Horizontal, BarChartStyle.Grouped);
            List<Integer> barChartValues = new ArrayList();
            int client = (int) clientInfo;
            int firstComp = (int) firstCompetitor;
            int secondComp = (int) secondCompetitor;
            barChartValues.add(client);
            if (listSize == 1) {
                bc.setBarWidthAndSpacing(BarWidthAndSpacing.newRelativeResize(0, 8));
            } else if (listSize == 2) {
                bc.setBarWidthAndSpacing(BarWidthAndSpacing.newRelativeResize(4, 4));
            }
            if (listSize > 1) {
                barChartValues.add(firstComp);
            }
            if (listSize > 2) {
                barChartValues.add(secondComp);
                bc.setBarWidthAndSpacing(BarWidthAndSpacing.newRelativeResize(0, 4));
            }
            int maxRangeValue = Collections.max(barChartValues);
            BarChartDataSerie bcds1 = new BarChartDataSerie.BarChartDataSerieBuilder(barChartValues).build();

            bc.setAutoResizing(true);
            bcds1.setColor(new de.toolforge.googlechartwrapper.Color(51, 102, 204));
            bc.addBarChartDataSerie(bcds1);
            ChartTitle chartTitle = new ChartTitle(title);
            chartTitle.setStyle(de.toolforge.googlechartwrapper.Color.BLACK, 22);
            bc.setChartTitle(chartTitle);

            AxisLabelContainer x = new AxisLabelContainer(AxisType.XAxis);
            AxisStyle xAxis = new AxisStyle(Color.BLACK);
            xAxis.setFontSize(18);
            x.setAxisStyle(xAxis);
            List<String> domainList = trimWebsitesNames(socialDomains);
            x.addLabel(new AxisLabel(domainList.get(0)));
            if (listSize > 1) {
                x.addLabel(new AxisLabel(domainList.get(1)));
            }
            if (listSize > 2) {
                x.addLabel(new AxisLabel(domainList.get(2)));
            }
            bc.addAxisLabelContainer(x);
            AxisLabelContainer y = new AxisLabelContainer(AxisType.YAxis);
            AxisStyle yAxis = new AxisStyle(Color.BLACK);
            yAxis.setFontSize(18);
            y.setAxisStyle(yAxis);
            y.setAxisRange(new AxisRange(0, maxRangeValue));
            bc.addAxisLabelContainer(y);
            bc.addDataScalingSet(new DataScalingSet(0, maxRangeValue));
        } catch (Exception e) {
            logger.error("Exception in getBarChart: ", e);
        }
        return bc != null ? bc.getUrl() : null;
    }

    public PdfPTable getGrowthCell(String col1, String col2, boolean status, boolean dateStatus) {
        PdfPTable tableRowEmpty = new PdfPTable(1);
        try {
            BaseColor fontColor = null;
            if (col2.contains("-")) {
                fontColor = new BaseColor(255, 0, 0);
            } else if (col2.equals("0") || col2.equals("0.0")) {
                fontColor = new BaseColor(38, 38, 38);
            } else {
                fontColor = new BaseColor(80, 145, 6);
                col2 = "+ " + col2;
            }

            Font commonFont = new Font(FontFactory.getFont("Arial", 11f, Font.NORMAL, new BaseColor(38, 38, 38)));
            Font smallFont = new Font(FontFactory.getFont("Arial", 8f, Font.BOLD, fontColor));
            PdfPTable tableRow = new PdfPTable(1);
            tableRow.setWidthPercentage(99.9f);

            PdfPCell column1 = null;
            if (status && dateStatus) {
                Chunk info = new Chunk(col1, commonFont);
                Chunk growth = new Chunk(" (" + col2 + ")", smallFont);
                Phrase infoGrowth = new Phrase();
                infoGrowth.add(info);
                infoGrowth.add(growth);
                column1 = new PdfPCell(infoGrowth);
                column1.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            } else if (status && !dateStatus) {
                Phrase info = new Phrase(col1);
                column1 = new PdfPCell(info);
                column1.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            } else {
                column1 = new PdfPCell(new Phrase("-"));
                column1.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            }
            column1.setVerticalAlignment(Rectangle.ALIGN_CENTER);
            column1.setBorder(Rectangle.NO_BORDER);
            tableRow.addCell(column1);
            return tableRow;
        } catch (Exception e) {
            logger.error("Exception in getGrowthCell" + e);
        }
        return tableRowEmpty;
    }

    public PdfPCell getRightCell(String date, String postText, Map<String, String> tableInfo) {

        Font elementDescFont = new Font(FontFactory.getFont("Arial", 11f, Font.BOLD, new BaseColor(38, 38, 38)));
        Font noteParaFont = new Font(FontFactory.getFont("Arial", 11f, Font.NORMAL, new BaseColor(38, 38, 38)));
        if (postText != null && !postText.trim().isEmpty()) {
            postText = removeHtmlTags(postText);
        }
        PdfPCell rightCell = new PdfPCell();
        rightCell.setBorder(Rectangle.NO_BORDER);
        rightCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
        PdfPTable topTable = new PdfPTable(1);
        topTable.setWidthPercentage(99.9f);
        PdfPCell dateCell = new PdfPCell();
        PdfPCell descriptionCell = new PdfPCell();
        if (date != null && !date.equals("")) {
            dateCell.addElement(new Phrase(date, elementDescFont));
        } else {
            dateCell.addElement(new Phrase("--/--/----", elementDescFont));
        }
        if (postText != null) {
            /*if (postText.equals("") || postText.equals(" ")) {*/
            if (!postText.equals("") && !postText.equals(" ")) {
                descriptionCell.addElement(new Phrase(postText, noteParaFont));
            } else {
                descriptionCell.addElement(new Phrase(" ", noteParaFont));
            }
        } else {
            /*descriptionCell.addElement(new Phrase("Description Not Available", noteParaFont));*/
            descriptionCell.addElement(new Phrase(" ", noteParaFont));
        }
        dateCell.setPaddingBottom(10f);
        dateCell.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
        dateCell.setBorder(Rectangle.NO_BORDER);
        descriptionCell.setPaddingBottom(10f);
        descriptionCell.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
        descriptionCell.setBorder(Rectangle.NO_BORDER);
        topTable.addCell(dateCell);
        topTable.addCell(descriptionCell);
        rightCell.addElement(topTable);
        float[] rightTableWidth = {};
        if (tableInfo.size() == 4) {
            float[] rightTableWidth1 = {20f, 20f, 25f, 35f};
            rightTableWidth = rightTableWidth1;
        }
        if (tableInfo.size() == 5) {
            float[] rightTableWidth1 = {15f, 17f, 17f, 21f, 28f};
            rightTableWidth = rightTableWidth1;
        }
        PdfPTable rightTwTable = new PdfPTable(rightTableWidth);
        rightTwTable.setWidthPercentage(99.9f);

        tableInfo.entrySet().stream().map((entrySet) -> entrySet.getKey()).map((key) -> new PdfPCell(new Phrase(key, elementDescFont))).map((cellColumn) -> {
            //                cellColumn.setPaddingLeft(5);
            cellColumn.setVerticalAlignment(Rectangle.ALIGN_CENTER);
            return cellColumn;
        }).map((cellColumn) -> {
            cellColumn.setBorderColor(new BaseColor(210, 210, 210));
            return cellColumn;
        }).map((cellColumn) -> {
            cellColumn.setMinimumHeight(20f);
            return cellColumn;
        }).forEach((cellColumn) -> {
            rightTwTable.addCell(cellColumn);
        });
        tableInfo.entrySet().stream().map((entrySet) -> entrySet.getValue()).map((value) -> new PdfPCell(new Phrase(value, noteParaFont))).map((cellInfo) -> {
            //                cellInfo.setPaddingLeft(5);
            cellInfo.setVerticalAlignment(Rectangle.ALIGN_CENTER);
            return cellInfo;
        }).map((cellInfo) -> {
            /*cellInfo.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);*/
            cellInfo.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            return cellInfo;
        }).map((cellInfo) -> {
            cellInfo.setBorderColor(new BaseColor(210, 210, 210));
            return cellInfo;
        }).map((cellInfo) -> {
            cellInfo.setMinimumHeight(20f);
            return cellInfo;
        }).forEach((cellInfo) -> {
            rightTwTable.addCell(cellInfo);
        });

        rightCell.addElement(rightTwTable);
        rightCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
        return rightCell;
    }

    public PdfPCell getRPLeftCell(String imagePath) {
        PdfPCell leftCell = null;
//        String applicationUrl = (String) context.getBean("domainName");
        try {
            if (imagePath == null || imagePath.equals("")) {
                imagePath = "/disk2/lxrmarketplace/images/no_image.png";
                Image img = Image.getInstance(imagePath);
                img.scaleToFit(75, 75);
                leftCell = new PdfPCell(img);
                leftCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                leftCell.setPadding(2f);
                leftCell.setPaddingTop(8);
                leftCell.setPaddingRight(8);
            } else if (imagePath.equals("invalid")) {
                leftCell = new PdfPCell(new Phrase("Unable to fetch your recent post data."));
                leftCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            } else if (imagePath.equals("noAccount")) {
                leftCell = new PdfPCell(new Phrase("Unable to fetch your metrics."));
                leftCell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            } else {
                try {
//                    Image sampleImg = Image.getInstance(new URL(imageUrl));
                    Image sampleImg = Image.getInstance(imagePath);
                    if (sampleImg == null) {
                        imagePath = "/disk2/lxrmarketplace/images/no_image.png";
                        Image img = Image.getInstance(imagePath);
                        img.scaleToFit(75, 75);
                        leftCell = new PdfPCell(img);
                        leftCell.setPaddingTop(10);
                        leftCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                    } else {
                        sampleImg.setSpacingBefore(10);
                        leftCell = new PdfPCell(sampleImg);
                        leftCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                    }
                } catch (BadElementException | IOException e) {
                    imagePath = "/disk2/lxrmarketplace/images/no_image.png";
                    Image img = Image.getInstance(imagePath);
                    img.scaleToFit(75, 75);
                    leftCell = new PdfPCell(img);
                    leftCell.setPaddingTop(10);
                    leftCell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                    leftCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                    leftCell.setPaddingTop(8);

                }
                leftCell.setPadding(2f);
                leftCell.setPaddingRight(8);
            }
            leftCell.setBorder(Rectangle.NO_BORDER);
            leftCell.setVerticalAlignment(Rectangle.ALIGN_CENTER);
        } catch (BadElementException | IOException e) {
            logger.error("Exception for getRPLeftCell:", e);
        }
        return leftCell;
    }

    public List<String> trimWebsitesNames(List<SocialCompDomains> socialDomains) {
        List<String> list = new ArrayList<>();
        socialDomains.stream().map((domains) -> domains.getDomain()).forEach((url) -> {
            String trimUrl = "";
            if (url != null) {
                url = url.replaceAll("https:", "");
                url = url.replaceAll("http:", "");
                url = url.replaceAll("//", "");
                if (url.lastIndexOf("/") == url.length() - 1) {
                    url = url.substring(0, url.lastIndexOf("/"));
                }
                if (url.length() < 20) {
                    trimUrl = url;
                } else {
                    for (int i = 0; i < 20; i++) {
                        trimUrl = trimUrl + url.charAt(i);
                    }
                    trimUrl = trimUrl + "..";
                }
                list.add(trimUrl);
            }
        });
        return list;
    }

    public Paragraph getTitleAndImage(String channelName, String imagePath) {
        Font elementDescFont = new Font(FontFactory.getFont("Arial", 11f, Font.BOLD, new BaseColor(38, 38, 38)));
        Paragraph heading = new Paragraph();
        Chunk logoInfo = null;
        Image logo = null;
        try {
            if (imagePath != null) {
                logo = Image.getInstance(new URL(imagePath));
                if (logo != null) {
                    logo.setSpacingBefore(20);
                    logo.scaleToFit(14f, 14f);
                    logoInfo = new Chunk(logo, 0, 0, true);
                }
            }
        } catch (BadElementException | IOException e) {
            logger.error("BadElementException | IOException for getTitleAndImage cause: " + e);
        }
        Chunk channelInfo = new Chunk(" " + channelName, elementDescFont);
        if (logoInfo != null) {
            heading.add(logoInfo);
        }
        heading.add(channelInfo);
        heading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        heading.setSpacingAfter(15f);
        return heading;
    }

    public boolean checkStatus(boolean clientStatus, boolean firstCompStatus, boolean secondCompStatus) {
        return !(clientStatus == false && firstCompStatus == false && secondCompStatus == false);
    }

    public String removeHtmlTags(String html) {
        return Jsoup.parse(html).text();
    }

    public static String trimWebsitesName(String socialDomain) {
        String trimUrl = null;
        if (socialDomain != null) {
            socialDomain = socialDomain.replaceAll("https:", "");
            socialDomain = socialDomain.replaceAll("http:", "");
            socialDomain = socialDomain.replaceAll("//", "");
            if (socialDomain.lastIndexOf("/") == socialDomain.length() - 1) {
                trimUrl = socialDomain.substring(0, socialDomain.lastIndexOf("/"));
            } else {
                trimUrl = socialDomain;
            }
        }
        return trimUrl;
    }

    public static Map<String, Object> getEmptySocialMediaObject() {
        Map<String, Object> socialmedia = new HashMap<>();
        socialmedia.put("twittermetrics", new TwitterMetrics());
        socialmedia.put("facebookmetrics", new FacebookMetrics());
        socialmedia.put("googleplusmetrics", new GoogleMetrics());
        socialmedia.put("youtubemetrics", new YouTubeMetrics());
        socialmedia.put("instagrammetrics", new InstagramMetrics());
        socialmedia.put("pinterestmetrics", new PinterestMetrics());
        return socialmedia;
    }

    public static int getFBRegularExpressionIndex(String username) {
        Pattern regex = Pattern.compile("[_.-/?@]");
        Matcher m = regex.matcher(username);
        if (m.find()) {
            return m.start();
        }
        return 0;
    }

    public static String getFaceBookUserName(String fbURL, String param) {
        String fbUserName = null;
        try {
            URL url = new URL(fbURL);
            String splitFBURL = url.getPath();//splitFBURL(fbURL);
            String fbUserNamesArray[] = null;
            if (param.equalsIgnoreCase("-")) {
                fbUserNamesArray = splitFBURL.split("-");
            } else if (param.equalsIgnoreCase("/")) {
                fbUserNamesArray = splitFBURL.split("/");
            }
            if (fbUserNamesArray != null) {
                fbUserName = fbUserNamesArray[fbUserNamesArray.length - 1];
                int firstindex = getFBRegularExpressionIndex(fbUserName);
                if ((firstindex != -1) && (firstindex != 0)) {
                    fbUserName = fbUserName.substring(0, firstindex);
                }
            }
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException in getFaceBookUserName: ", e);
        }
        return fbUserName;
    }

    /*To get IndividualChannelUserNames for a URL*/
    public static Map<String, String> getChannelUserNamefromURL(String mediaUrl, char channel) {
        // String channelUserName = "";
        List<String> links = extractLinks(mediaUrl);
        Map<String, String> channelUserName = new HashMap();
        switch (channel) {
            case 'T':
                try {
                    links.stream().forEach((link) -> {
                        if (((link).contains("https://twitter.com"))
                                || ((link).contains("http://twitter.com"))
                                || ((link).contains("https://www.twitter.com"))
                                || ((link).contains("http://www.twitter.com"))) {
                            if (!link.contains("twitter.com/share")) {
                                if (!(channelUserName.containsKey("Twitter"))) {
                                    String tempTwitterLink = getUrlValidation(link);
                                    String TwitteruserName = getUserNameFromLink(tempTwitterLink);/*tempTwitterLink).substring(20);*/
                                    if (TwitteruserName != null) {
                                        if (TwitterMetricsAPIService.checkTwitterUserExistence(TwitteruserName)) {
                                            channelUserName.put("Twitter", TwitteruserName);
                                        }
                                    }
                                }
                            }/*End of share if condition*/
                        } else if ((link).contains("twitter.com")) {
                            if (!link.contains("twitter.com/share")) {
                                if (!(channelUserName.containsKey("Twitter"))) {
                                    String tempTwitterLink = getUrlValidation(link);
                                    String TwitteruserName = getUserNameFromLink(tempTwitterLink);/*(tempTwitterLink).substring(19);*/
                                    if (TwitteruserName != null) {
                                        if (TwitterMetricsAPIService.checkTwitterUserExistence(TwitteruserName)) {
                                            channelUserName.put("Twitter", TwitteruserName);
                                        }
                                    }
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    logger.error("Exception in twiter user name in getChannelUserNamefromURL" + e.getMessage());
                }
                break;
            case 'F':
                try {
                    for (String link : links) {
                        if (((link).contains("https://www.facebook.com"))
                                || ((link).contains("http://www.facebook.com"))
                                || ((link).contains("http://facebook.com"))
                                || ((link).contains("https://facebook.com"))) {
                            if (!link.contains("www.facebook.com/sharer/")) {
                                if (!(channelUserName.containsKey("facebook"))) {
                                    String tempFaceBookLink = getUrlValidation(link);
                                    String fbuserName = getUserNameFromLink(tempFaceBookLink);/*(tempFaceBookLink).substring(25);*/
                                    if (fbuserName != null) {
                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbuserName)) {
                                            channelUserName.put("facebook", fbuserName);
                                        } else {
                                            String fbChoice1 = getFaceBookUserName(tempFaceBookLink, "-");
                                            if (fbChoice1 != null) {
                                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice1)) {
                                                    channelUserName.put("facebook", fbuserName);
                                                } else {
                                                    String fbChoice2 = getFaceBookUserName(tempFaceBookLink, "/");
                                                    if (fbChoice2 != null) {
                                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice2)) {
                                                            channelUserName.put("facebook", fbuserName);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        } else if ((link).contains("facebook.com")) {
                            if (!link.contains("facebook.com/sharer/")) {
                                if (!(channelUserName.containsKey("facebook"))) {
                                    String tempFaceBookLink = getUrlValidation(link);
                                    String fbuserName = getUserNameFromLink(tempFaceBookLink);/*(tempFaceBookLink).substring(25);*/
                                    if (fbuserName != null) {
                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbuserName)) {
                                            channelUserName.put("facebook", fbuserName);
                                        } else {
                                            String fbChoice1 = getFaceBookUserName(tempFaceBookLink, "-");
                                            if (fbChoice1 != null) {
                                                if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice1)) {
                                                    channelUserName.put("facebook", fbChoice1);
                                                } else {
                                                    String fbChoice2 = getFaceBookUserName(tempFaceBookLink, "/");
                                                    if (fbChoice2 != null) {
                                                        if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(fbChoice2)) {
                                                            channelUserName.put("facebook", fbChoice2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }/*End of for each*/
                } catch (Exception e) {
                    logger.error("Exception for facebook user name", e);
                }
                break;
            case 'Y':
                try {

                    for (String link : links) {
                        if (((link).contains("http://www.youtube.com/user"))
                                || ((link).contains("https://www.youtube.com/user"))
                                || ((link).contains("https://www.youtube.com/channel"))
                                || ((link).contains("http://www.youtube.com/channel"))) {
                            if ((!((link).contains("www.youtube.com/v/")))) {
                                if (!(channelUserName.containsKey("YouTube"))) {
                                    String tempYouTubeLink = getUrlValidation(link);
                                    String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/*getYouTubeUserName(tempYouTubeLink);*/
                                    if (YouTubeuserName != null) {
                                        if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                            channelUserName.put("YouTube", YouTubeuserName);
                                        } else {
                                            String yTUsrName = getUserNameFromLink(link);/*getYouTubeUserName(tempYouTubeLink);*/
                                            if (yTUsrName != null) {
                                                if (YouTubeMetricsAPIService.checkYoutubeUserName(yTUsrName)) {
                                                    channelUserName.put("YouTube", YouTubeuserName);
                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        } else if (((link).contains("http://www.youtube.com"))
                                || ((link).contains("https://www.youtube.com"))) {
                            if ((!((link).contains("www.youtube.com/v/")))) {
                                if (!(channelUserName.containsKey("YouTube"))) {
                                    String tempYouTubeLink = getUrlValidation(link);
                                    String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/*getYouTubeUserName(tempYouTubeLink);*/
                                    if (YouTubeuserName != null) {
                                        if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                            channelUserName.put("YouTube", YouTubeuserName);
                                        }
                                    }
                                }
                            }
                        } else if ((link).contains("youtube.com")) {
                            if ((!((link).contains("www.youtube.com/v/")))) {
                                if (!(channelUserName.containsKey("YouTube"))) {
                                    String tempYouTubeLink = getUrlValidation(link);
                                    String YouTubeuserName = getUserNameFromLink(tempYouTubeLink);/* getYouTubeUserName(tempYouTubeLink);*/
                                    if (YouTubeuserName != null) {
                                        if (YouTubeMetricsAPIService.checkYoutubeUserName(YouTubeuserName)) {
                                            channelUserName.put("YouTube", YouTubeuserName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception for youtube user name", e);
                }
                break;

        }
        return channelUserName;
    }

    public void updateChannelUserName(SocialCompDomains socialDomains, String channelUserName, char channel) {
        String updateQuery = null;
        switch (channel) {
            case 'T':
                updateQuery = "UPDATE channel_user_names SET twitter_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;
            case 'F':
                updateQuery = "UPDATE channel_user_names SET facebook_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;
            case 'I':
                updateQuery = "UPDATE channel_user_names SET instagram_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;
            case 'P':
                updateQuery = "UPDATE channel_user_names SET pinterest_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;
            case 'Y':
                updateQuery = "UPDATE channel_user_names SET youtube_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;
            case 'G':
                updateQuery = "UPDATE channel_user_names SET goolgePlus_name = ? WHERE user_name_id = ? and media_user_id = ?";
                break;

        }
        try {
            Object[] valLis = new Object[]{channelUserName, socialDomains.getChannelNames().getChannelRowId(), socialDomains.getDomainId()};
            this.jdbcTemplate.update(updateQuery, valLis);
        } catch (DataAccessException ex) {
            logger.error("Exception for enableing SocialMediaUserData" + ex.getMessage());
        }
    }

    public ChannelUserNames checkChannnelUserNames(SocialCompDomains socialDomains) {
        ChannelUserNames channelNames = new ChannelUserNames();
        channelNames.setChannelRowId(socialDomains.getChannelNames().getChannelRowId());
        channelNames.setSocialMediaUserId(socialDomains.getChannelNames().getSocialMediaUserId());
        boolean checkStatus = false;
        Map<String, String> channelMap = null;
        try {
            String sTUN = socialDomains.getChannelNames().getTwitterName().trim();/*sTUN is Stored/Database TwitterUserName */
            if ((sTUN != null) && (!(sTUN.equals(" "))) && (!(sTUN.equals("")))) {
                channelNames.setTwitterName(sTUN);
            } else {
                logger.debug("Looking for Twitter Channel Name in checkChannnelUserNames for social domain" + socialDomains.getDomain() + " and domain id is " + socialDomains.getDomainId());
                channelMap = getChannelUserNamefromURL(socialDomains.getDomain(), 'T');
                if ((channelMap.get("Twitter")) != null) {
                    sTUN = channelMap.get("Twitter");
                    if (TwitterMetricsAPIService.checkTwitterUserExistence(sTUN)) {
                        channelNames.setTwitterName(sTUN);
                        checkStatus = true;
                    }
                }
            }
            /*For facebook channel*/
            String sFUN = socialDomains.getChannelNames().getFacebookName().trim();/*sFUN is Stored/Database FacebookUserName */
            if ((sFUN != null) && (!(sFUN.equals(" "))) && (!(sFUN.equals("")))) {
                channelNames.setFacebookName(sFUN);
            } else {
                logger.debug("Looking for Facebook Channel Name in checkChannnelUserNames for social domain" + socialDomains.getDomain() + " and domain id is " + socialDomains.getDomainId());
                channelMap = getChannelUserNamefromURL(socialDomains.getDomain(), 'F');
                if ((channelMap.get("facebook") != null)) {
                    sFUN = channelMap.get("facebook");
                    if (FaceBookMetricsAPIService.checkFaceBookUserNameExistence(sFUN)) {
                        channelNames.setFacebookName(sFUN);
                        checkStatus = true;
                    }
                }
            }
            String sYUN = socialDomains.getChannelNames().getYoutubeName().trim();/*sYUN is Stored/Database YoutubeUserName */
            if ((sYUN != null) && (!(sYUN.equals(" "))) && (!(sYUN.equals("")))) {
                channelNames.setYoutubeName(sYUN);
            } else {
                logger.debug("Looking for Youtube Channel Name in checkChannnelUserNames for social domain" + socialDomains.getDomain() + " and domain id is " + socialDomains.getDomainId());
                channelMap = getChannelUserNamefromURL(socialDomains.getDomain(), 'Y');
                if (channelMap.get("YouTube") != null) {
                    sYUN = channelMap.get("YouTube");
                    if (YouTubeMetricsAPIService.checkYouTubeUserExistence(sYUN)) {
                        channelNames.setYoutubeName(sYUN);
                        checkStatus = true;
                    }
                }
            }
            if (checkStatus) {
                channelNames.setInsertUserNames(true);
            }

        } catch (Exception e) {
            logger.error("Exception in checkChannnelUserNames: ", e);
        }
        return channelNames;
    }

    public boolean checkGrwothDataAvailablity(Date presentDate, long userId) {
        return userId != 0 ? (getDayDataCount(getPreviousDate(presentDate, 1), userId) > 0) : false;
    }

    public int getDayDataCount(String requeiredDate, long socialUserId) {
        String previousDayQuery = "select count(*) from (SELECT s.id, "
                + "t.smt_tw_audience, t.smt_tw_activity, t.smt_tw_following, t.smt_tw_favourites, "
                + "t.tw_recent_post, tw_post_retweets, t.tw_post_favorites, t.tw_post_image_path, t.tw_post_date, "
                + "t.insert_date AS twitter_insert_Date, t.tw_hash_tag, "
                + "f.smt_fb_audience, f.smt_fb_activity, fb_recent_post, "
                + "f.fb_post_likes, fb_post_comments, fb_post_shares, f.fb_post_image_path, "
                + "f.fb_post_date, f.insert_date AS facebook_insert_Date,"
                + "y.smt_yt_audience, y.smt_yt_activity, y.smt_yt_views, "
                + "y.yt_recent_post, y.yt_post_views, y.yt_post_likes, "
                + "y.yt_post_comments, y.yt_image_path, y.yt_post_date, "
                + "y.insert_date AS youtube_insert_Date, "
                + "s.id AS common_id FROM social_media_user s "
                + "INNER JOIN smt_twitter t ON s.id = t.media_user_id AND DATE_FORMAT(t.insert_date, '%Y-%m-%d') = '" + requeiredDate + "' "
                + "INNER JOIN smt_facebook f ON s.id = f.media_user_id AND DATE_FORMAT(f.insert_date, '%Y-%m-%d') = '" + requeiredDate + "' "
                + "INNER JOIN smt_youtube y ON s.id = y.media_user_id AND DATE_FORMAT(y.insert_date, '%Y-%m-%d') = '" + requeiredDate + "' "
                + "WHERE s.id = '" + socialUserId + "' AND s.is_deleted = 0) a";
        /*logger.info("Qery to fetch data for getDayDataCount:" + previousDayQuery);*/
        try {
            return this.jdbcTemplate.queryForObject(previousDayQuery, Integer.class);
        } catch (DataAccessException e) {
            logger.info("Query to getDayDataCount:: " + previousDayQuery);
            logger.error("Exception for getDayDataCount casue:: ", e);
        }
        return 0;
    }

    public String getPostText(String apiPostText) {
        String formatedPostText = null;
        if (apiPostText != null) {
            formatedPostText = removeHtmlTags(apiPostText);
        }
        if (formatedPostText != null) {
            formatedPostText = convertIntoDBformat(formatedPostText);
        }
        return formatedPostText;
    }

    public Map<String, Map<String, String>> getColorsListForMetrics(List<Map<String, Object>> userSocialAnalysis) {

        Map<String, Map<String, String>> channelColorsMap = new HashMap<>();
        Map<String, String> fieldColorsMap = null;
        /*Fetching channel names from list for user domain.*/
        Map<String, Object> userDomainMap = userSocialAnalysis.get(0);
        List<Map<String, Object>> competitorSocialAnalysis = null;
        if (userSocialAnalysis.size() >= 2) {
            competitorSocialAnalysis = userSocialAnalysis.subList(1, userSocialAnalysis.size());
        }

        /*This set contains facebook, twitter, googlepluse etc.. from user social analysis map object*/
        Set<String> userChennelSet = userDomainMap.keySet();

        /*Fetching user,competitor object and comparing values to competitor obejct values*/
        Object userChannelObj = null;
        Object competitorChannelObj = null;
        List<String> channelFieldsList = null;

        long userMetricValue = 0, competitorMetricValue = 0;
        String graphColor = null;
        boolean isBothCompetitorLess = false;
        boolean isBothCompetitorGreater = false;
        boolean isBothCompetitorEqual = false;
        boolean isCompetitorExists = false;

        /*Fetching all the declared fields*/
        Map<String, List<String>> channelFieldsMap = getFieldsListforSocialChannels();

        for (String userChannel : userChennelSet) {
            /*respective channel name*/
            userChannelObj = userDomainMap.get(userChannel);

            /*Placing the channel name into the fieldColorsMap*/
            fieldColorsMap = new HashMap<>();

            if (!channelFieldsMap.containsKey(userChannel)) {
                continue;
            }
            channelFieldsList = channelFieldsMap.get(userChannel);
            for (String fieldName : channelFieldsList) {
                /*Setting the default color.*/
                graphColor = context.getMessage("metric.warning", null, Locale.UK);
                isBothCompetitorLess = true;
                isBothCompetitorGreater = true;
                isBothCompetitorEqual = true;

                userMetricValue = getFieldValue(userChannelObj, fieldName);
                if (userMetricValue < 1) {
                    graphColor = context.getMessage("metric.default", null, Locale.UK);
                    fieldColorsMap.put(fieldName, graphColor);
                    continue;
                }
                isCompetitorExists = false;
                if (competitorSocialAnalysis != null) {
                    for (Map<String, Object> competiorObjectMap : competitorSocialAnalysis) {
                        competitorChannelObj = competiorObjectMap.get(userChannel);
                        competitorMetricValue = getFieldValue(competitorChannelObj, fieldName);
                        if (competitorMetricValue < 1) {
                            graphColor = context.getMessage("metric.default", null, Locale.UK);
                            fieldColorsMap.put(fieldName, graphColor);
                            continue;
                        }
                        isCompetitorExists = true;
                        if (userMetricValue < competitorMetricValue) {
                            isBothCompetitorEqual = false;
                            isBothCompetitorLess = false;
                        } else if (userMetricValue > competitorMetricValue) {
                            isBothCompetitorEqual = false;
                            isBothCompetitorGreater = false;
                        } else {
                            isBothCompetitorLess = false;
                            isBothCompetitorGreater = false;
                        }
                    }/*Eof of competitor*/
                }
                if (!isCompetitorExists) {
                    graphColor = context.getMessage("metric.default", null, Locale.UK);
                } else if (isBothCompetitorEqual) {
                    graphColor = context.getMessage("metric.default", null, Locale.UK);
                } else if (isBothCompetitorGreater) {
                    graphColor = context.getMessage("metric.bad", null, Locale.UK);
                } else if (isBothCompetitorLess) {
                    graphColor = context.getMessage("metric.good", null, Locale.UK);
                }
                /*Ex: 
                Key:Metric Name, Value: Metric color.
                likes(FaceBook):gray,
                 followers(Twitter): green,
                 borads(Pinterest): red.*/
                fieldColorsMap.put(fieldName, graphColor);
            }
            /*Ex: 
            Key:Channel name, Value: Metric color map.
                 FaceBook(likes,gray),
                 Twitter((followers,green),(tweets,red))
                etc..
             */
            channelColorsMap.put(userChannel, fieldColorsMap);
        }

        return channelColorsMap;
    }

    public long getFieldValue(Object channelObject, String fieldName) {
        long metricValue = 0;
        try {
            if (channelObject != null) {
                for (Field field : channelObject.getClass().getDeclaredFields()) {
                    if (field != null && field.getName().equals(fieldName)) {
                        field.setAccessible(true); // Seting modifier to public first.
                        if (field.getType().getName().equalsIgnoreCase("int") || field.getType().getName().equalsIgnoreCase("Integer")) {
                            metricValue = (int) field.get(channelObject);
                            break;
                        } else if (field.getType().getName().equalsIgnoreCase("long") || field.getType().getName().equalsIgnoreCase("Long")) {
                            metricValue = (long) field.get(channelObject);
                            break;
                        }
                    }
                }
            }
        } catch (IllegalAccessException iae) {
            logger.error("Exception for getFieldValue:", iae);
        }
        return metricValue;
    }

    public Map<String, List<String>> getFieldsListforSocialChannels() {
        Map<String, List<String>> channelFieldsMap = new LinkedHashMap<>();
        List<String> fieldsList = null;

        /*FaceBook*/
        fieldsList = new LinkedList<>();
        fieldsList.add("likesCount");
        channelFieldsMap.put("facebookmetrics", fieldsList);

        /*Twitter*/
        fieldsList = new LinkedList<>();
        fieldsList.add("followers");
        fieldsList.add("following");
        fieldsList.add("favourites");
        fieldsList.add("tweets");
        channelFieldsMap.put("twittermetrics", fieldsList);


        /*YouTube*/
        fieldsList = new LinkedList<>();
        fieldsList.add("subscribers");
        fieldsList.add("videos");
        fieldsList.add("views");
        channelFieldsMap.put("youtubemetrics", fieldsList);

        /*YouTube*/
        fieldsList = new LinkedList<>();
        fieldsList.add("followers");
        channelFieldsMap.put("googleplusmetrics", fieldsList);
        return channelFieldsMap;
    }
}/*End of Social Media Service*/

class MyFooter extends PdfPageEventHelper {

    Font ffont = new Font(FontFactory.getFont("Arial", 8f, Font.BOLD, new BaseColor(38, 38, 38)));
//        PdfReader pdfReader;
    int totalNumberOfPages;

    MyFooter(int totalPages) {
        this.totalNumberOfPages = totalPages;
    }

    @Override
    public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {
        PdfContentByte cb = writer.getDirectContent();
        Paragraph footerInfo = new Paragraph("Prepared by https://lxrmarketplace.com/ ", ffont);
        Paragraph footerPage = new Paragraph("Page " + writer.getPageNumber() + " of " + totalNumberOfPages, ffont);
        ColumnText.showTextAligned(cb, com.itextpdf.text.Element.ALIGN_LEFT,
                footerInfo, document.leftMargin(), document.bottom() - 5, 0);
        ColumnText.showTextAligned(cb, com.itextpdf.text.Element.ALIGN_RIGHT,
                footerPage, document.leftMargin() + 525, document.bottom() - 5, 0);
    }

}
