/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.socialmedia;

/**
 *
 * @author sagar
 */
public class ChannelUserNames {
    private long channelRowId;
    private long socialMediaUserId;
    private String twitterName;
    private String facebookName;
//    private String instagramName;
    private String pinterestName;
    private String youtubeName;
//    private String googlePlusName;
    /*To check wheather to insert data into social_channel_names*/
    private boolean insertUserNames;

    public String getTwitterName() {
        return twitterName;
    }

    public void setTwitterName(String twitterName) {
        this.twitterName = twitterName;
    }

    public String getFacebookName() {
        return facebookName;
    }

    public void setFacebookName(String facebookName) {
        this.facebookName = facebookName;
    }

//    public String getInstagramName() {
//        return instagramName;
//    }
//
//    public void setInstagramName(String instagramName) {
//        this.instagramName = instagramName;
//    }

    public String getPinterestName() {
        return pinterestName;
    }

    public void setPinterestName(String pinterestName) {
        this.pinterestName = pinterestName;
    }

    public String getYoutubeName() {
        return youtubeName;
    }

    public void setYoutubeName(String youtubeName) {
        this.youtubeName = youtubeName;
    }

//    public String getGooglePlusName() {
//        return googlePlusName;
//    }
//
//    public void setGooglePlusName(String googlePlusName) {
//        this.googlePlusName = googlePlusName;
//    }

    public long getChannelRowId() {
        return channelRowId;
    }

    public void setChannelRowId(long channelRowId) {
        this.channelRowId = channelRowId;
    }

    

    public long getSocialMediaUserId() {
        return socialMediaUserId;
    }

    public void setSocialMediaUserId(long socialMediaUserId) {
        this.socialMediaUserId = socialMediaUserId;
    }

    public boolean isInsertUserNames() {
        return insertUserNames;
    }

    public void setInsertUserNames(boolean insertUserNames) {
        this.insertUserNames = insertUserNames;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
