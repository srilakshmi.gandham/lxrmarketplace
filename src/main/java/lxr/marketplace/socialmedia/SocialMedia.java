package lxr.marketplace.socialmedia;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
public class SocialMedia implements Serializable{

        private List compDomainSettingRows = LazyList.decorate(new LinkedList(),FactoryUtils.instantiateFactory(SocialCompDomains.class));
	private String mediaUrl;
	private String updateUrl;
        private String comptetiorUrls;

	private boolean weeklyReport = true;
	private boolean toolUnsubscription;
        private boolean isUserDomain;
        private String reviewURLs;
        private long socialUserId;
        private Timestamp userDate;

        private int domainCount;
        
	public String getMediaUrl() {
		return mediaUrl;
	}

	public void setMediaUrl(String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}

	public String getUpdateUrl() {
		return updateUrl;
	}

	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}

	public boolean isWeeklyReport() {
		return weeklyReport;
	}

	public void setWeeklyReport(boolean weeklyReport) {
		this.weeklyReport = weeklyReport;
	}

	public boolean isToolUnsubscription() {
		return toolUnsubscription;
	}

	public void setToolUnsubscription(boolean toolUnsubscription) {
		this.toolUnsubscription = toolUnsubscription;
	}

        public String getComptetiorUrls() {
            return comptetiorUrls;
        }

        public void setComptetiorUrls(String comptetiorUrls) {
            this.comptetiorUrls = comptetiorUrls;
        }

    public boolean isIsUserDomain() {
        return isUserDomain;
    }

    public void setIsUserDomain(boolean isUserDomain) {
        this.isUserDomain = isUserDomain;
    }

    public long getSocialUserId() {
        return socialUserId;
    }

    public void setSocialUserId(long socialUserId) {
        this.socialUserId = socialUserId;
    }

    public Timestamp getUserDate() {
        return userDate;
    }

    public void setUserDate(Timestamp userDate) {
        this.userDate = userDate;
    }

    public List getCompDomainSettingRows() {
        return compDomainSettingRows;
    }

    public void setCompDomainSettingRows(List compDomainSettingRows) {
        this.compDomainSettingRows = compDomainSettingRows;
    }

    public int getDomainCount() {
        return domainCount;
    }

    public void setDomainCount(int domainCount) {
        this.domainCount = domainCount;
    }

    public String getReviewURLs() {
        return reviewURLs;
    }

    public void setReviewURLs(String reviewURLs) {
        this.reviewURLs = reviewURLs;
    }

    
    
    
}
