/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.socialmedia;

import java.time.DayOfWeek;
import java.time.LocalDate;
import lxr.marketplace.apiaccess.FaceBookMetricsAPIService;
import lxr.marketplace.apiaccess.TwitterMetricsAPIService;
import lxr.marketplace.apiaccess.YouTubeMetricsAPIService;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author vidyasagar.korada
 */
@Component
public class SocialAPISTrackingStatusTask {

    private static final Logger LOGGER = Logger.getLogger(SocialAPISTrackingStatusTask.class);

    @Autowired
    private FaceBookMetricsAPIService faceBookMetricsAPIService;
    @Autowired
    private TwitterMetricsAPIService twitterMetricsAPIService;
    @Autowired
    private YouTubeMetricsAPIService youTubeMetricsAPIService;
    @Autowired
    private String supportMail;

    @Value("${FaceBook.UserName}")
    private String facebookPageName;
    @Value("${Twitter.UserName}")
    private String twitterUserName;
    @Value("${YouTube.UserName}")
    private String youtubeChannelName;
    @Value("${SocialAPISTracking.dayOfWeek}")
    private String dayOfWeek;
    @Value("${SocialAPISTracking.scheduler}")
    private boolean schedulerStatus;

    /*Pulling key metrics from API Services*/
    public void trackAPIStatus() {
        if (schedulerStatus) {
            LOGGER.info("**************** Social Media Analyzer API's Tracking Status Task Scheduler Started****************");
            long fbLikes;
            long twitterFollowers;
            long youTubeSubscribers;

            fbLikes = faceBookMetricsAPIService.checkGraphAPIStatus(facebookPageName);
            twitterFollowers = twitterMetricsAPIService.checkTwitterAPIStatus(twitterUserName);
            youTubeSubscribers = youTubeMetricsAPIService.checkYouTubeAPIStatus(youtubeChannelName);

            LocalDate currentDate = LocalDate.now();
            DayOfWeek dOW = currentDate.getDayOfWeek();
            LOGGER.info("Individual API tracking status is completed for Current Date:: " + currentDate + ", DayOfWeek:: " + dOW.toString());
            if (dOW.toString().equalsIgnoreCase(dayOfWeek)) {
                String subject = "Weekly Update On Social Media Analyzer Tool API's Status";
                String body = "Below are the stats of social channels:: ";
                body = body.concat("<br/> FaceBook Page Id:: (" + facebookPageName + ") ---> Facebook Likes :: " + fbLikes);
                body = body.concat("<br/> Twiiter Page Id::  (" + twitterUserName + ") ---> Twitter Followers:: " + twitterFollowers);
                body = body.concat("<br/> YouTube Channel Id:: (" + youtubeChannelName + ") ---> YouTube Channel Subscribers:: " + youTubeSubscribers);
                SendMail.sendMail(supportMail, supportMail, subject, body);
            }
            LOGGER.info("**************** Social Media Analyzer API's Tracking Status Task Scheduler Completed****************");
        }
    }

}
