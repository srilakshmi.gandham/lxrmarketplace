/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.imageCompression;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.util.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author netelixir
 */
@Controller
public class ImageCompressionController {

    @Autowired
    private String downloadFolder;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @Autowired
    ImageCompressionService compressionService;

    @RequestMapping(value = "/image-compression.html")
    public String tool(HttpSession session) {
        session.removeAttribute("imageCompressionData");
        return "views/imageCompression/imageCompression";
    }
     @RequestMapping(value = "/image-compression.html", params = {"backToSuccessPage"})
    public String backToSuccessPage(HttpSession session) {
        return "views/imageCompression/imageCompression";
    }

    @ResponseBody
    @RequestMapping(value = "/image-compression.html",params = {"request=analyze"}, method = RequestMethod.POST)
    public List<ImageCompression> imageCompression(@RequestParam("imageFile") List<MultipartFile> files, HttpSession session, HttpServletResponse response) {
        List<ImageCompression> imageCompression = null;
        imageCompression = compressionService.imageCompression(files);
        if (imageCompression != null) {
            Session userSession = (Session) session.getAttribute("userSession");
            userSession.addToolUsage(43);
            session.setAttribute("imageCompressionData", imageCompression);
        } else {
            session.removeAttribute("imageCompressionData");
        }
        return imageCompression;
    }

    @RequestMapping(value = "/image-compression.html", params = {"imageName"})
    public String downloadFile(HttpServletResponse response, HttpSession session, @RequestParam("imageName") String fileName) {
        if (fileName != null) {
            Common.downloadReport(response, downloadFolder, fileName, compressionService.getFileExtension(fileName));
        }

        return null;
    }

    @RequestMapping(value = "/image-compression.html", params = {"ate=ate"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(HttpServletResponse response, @RequestParam("domain") String domain) {
        Object ateResponseObject[] = new Object[1];
        String toolIssues = null;
        String questionInfo = null;
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain);
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.IMAGE_COMPRESSION_TOOL);
        ateResponseObject[0] = lxrmAskExpert;
        return ateResponseObject;
    }

    @RequestMapping(value = "/image-compression.html", params = {"backToSuccessPage"}, method = RequestMethod.POST)
    @ResponseBody
    public List<ImageCompression> backToSuccess(HttpSession session) {
        List<ImageCompression> compression = null;
        if (session.getAttribute("imageCompressionData") != null) {
            compression = (List<ImageCompression>) session.getAttribute("imageCompressionData");
        }
        return compression;
    }

}
