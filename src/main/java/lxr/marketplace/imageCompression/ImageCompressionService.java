/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.imageCompression;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author netelixir
 */
@Service
public class ImageCompressionService {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ImageCompressionService.class);

    @Autowired
    private String downloadFolder;

    List<ImageCompression> imageCompression(List<MultipartFile> imageFile) {
        List<ImageCompression> compressions = new ArrayList<>();
        ImageCompression compression = null;
//        MultipartFile f =(MultipartFile) new File(downloadFolder);
        if (imageFile.size() > 0) {
            for (int i = 0; i < imageFile.size(); i++) {
                File file = convert(imageFile.get(i));
                compression = imageResizer(file);
                if (compression != null) {
                    compressions.add(compression);

                }
            }
        }
        return compressions;
    }

    public File convert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            file.transferTo(convFile);
        } catch (IllegalStateException | IOException ex) {
            logger.info("Exception occured while converting the file " + ex);
        }
        return convFile;

    }

    public ImageCompression imageResizer(File file) {
        File compressedImageFile = null;
        ImageCompression imageCompression = null;
        if (file != null && file.length() >= 15360) {
            try {
                imageCompression = new ImageCompression();
//                String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getName());
//                imageCompression.setFileName(fileNameWithoutExtension+"."+format);
                double imageBytes = file.length();
                imageCompression.setImageSize(sizeConverter(imageBytes));
                BufferedImage image = ImageIO.read(file);
                String fileExtension = getFileExtension(file.getName());
                if (file.getName().contains("#") || file.getName().contains("%")) {
                    String fileNameWithoutSpecialCharcter = file.getName().replaceAll("[#%]", "");
                    imageCompression.setFileName(fileNameWithoutSpecialCharcter);
                    compressedImageFile = new File(downloadFolder + fileNameWithoutSpecialCharcter);
                } else {
                    imageCompression.setFileName(file.getName());
                    compressedImageFile = new File(downloadFolder + file.getName());
                }
                OutputStream os = new FileOutputStream(compressedImageFile);
                Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(fileExtension);
                ImageWriter writer = (ImageWriter) writers.next();
                ImageOutputStream ios = ImageIO.createImageOutputStream(os);
                writer.setOutput(ios);
                ImageWriteParam param = writer.getDefaultWriteParam();
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                param.setCompressionQuality(0.35f);
                writer.write(null, new IIOImage(image, null, null), param);
                long compressedImageBytes = compressedImageFile.length();
                imageCompression.setCompressedImageSize(sizeConverter(compressedImageBytes));
                os.close();
                ios.close();
                writer.dispose();
            } catch (IOException ex) {
                logger.info("expection has occured while compressing the image" + ex);
            }
        }
        return imageCompression;
    }

    public String getFileExtension(String file) {
//        String fileName = file.getName();
        if (file.lastIndexOf(".") != -1 && file.lastIndexOf(".") != 0) {
            return file.substring(file.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

//    public boolean specialCharacters(String s) {
//
//        Pattern p = Pattern.compile("[!@#$%&*()_+=|<>?{}\\\\[\\\\]~-]");
//        Matcher m = p.matcher(s);
//        // boolean b = m.matches();
//        boolean b = m.find();
//        return b;
//    }

    public String sizeConverter(double Bytes) {
        if (Bytes < 1024) {
            String size = String.valueOf(Bytes);
            return size + " bytes";
        } else if (Bytes < 1048576) {
            double convBytes = Bytes / 1024;//kb
            String size = String.valueOf(roundTwoDecimals(convBytes));
            return size + " KB";
        } else if (Bytes < 1073741824) {
            double convBytes = Bytes / 1024 / 1024;//mb
            String size = String.valueOf(roundTwoDecimals(convBytes));
            return size + " MB";
        } else if (Bytes > 1073741824) {
            double convBytes = Bytes / 1024 / 1024 / 1024;//gb
            String size = String.valueOf(roundTwoDecimals(convBytes));

            return size + " GB";
        } else {
            return null;
        }
//        
    }

    public static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat(".##");
        return Double.valueOf(twoDForm.format(d));
    }
}
