/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.imageCompression;

/**
 *
 * @author netelixir
 */
public class ImageCompression {
    private String fileName;
    private String imageSize;
    private String compressedImageSize;

    public String getCompressedImageSize() {
        return compressedImageSize;
    }

    public void setCompressedImageSize(String compressedImageSize) {
        this.compressedImageSize = compressedImageSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }
    
            
    
}
