/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Calendar;
import lxr.marketplace.user.UserAnalysisInfo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class UsersWebsiteTracking {
    private static final Logger logger = Logger.getLogger(UsersWebsiteTracking.class);
    
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public void insertUserWebsiteTracking(final String website,final long lxrm_tool_id,final long lxrm_user_id){
        try{
            
            final String websitesQuery="insert into lxrm_websites_tracking(website,lxrm_tool_id,lxrm_user_id,insert_date) values(?,?,?,?)";
            Timestamp inserDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
            synchronized (this) {
                this.jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) con.prepareStatement(websitesQuery);
                    statement.setString(1,website);
                    statement.setLong(2,lxrm_tool_id);
                    statement.setLong(3,lxrm_user_id);
                    statement.setTimestamp(4, inserDate);
                    return statement;
                });
            }// End of synchronized
        }catch(Exception e){
            logger.error("Exception in inserting lxrm_websites_tracking ", e);
        }
    }
    public UserAnalysisInfo getToolUserAnalysisInfo(long userId, long websiteTrackingId){
        
        SqlRowSet sqlRowSet = null;
        UserAnalysisInfo userAnalysisInfo = null;
        String query = "SELECT * FROM lxrm_websites_tracking WHERE lxrm_user_id = ? and website_id = ?";
        Object[] params = {userId, websiteTrackingId};
        try {
            sqlRowSet = jdbcTemplate.queryForRowSet(query, params);
            while (sqlRowSet.next()) {
                userAnalysisInfo = new UserAnalysisInfo();
                userAnalysisInfo.setToolId(sqlRowSet.getLong("lxrm_tool_id"));
                userAnalysisInfo.setUserId(sqlRowSet.getLong("lxrm_user_id"));
                userAnalysisInfo.setWebsiteName(sqlRowSet.getString("website"));
                userAnalysisInfo.setAnalysisIssues(sqlRowSet.getString("issues"));
                userAnalysisInfo.setSuggestedQuestion(sqlRowSet.getString("suggested_question"));
                userAnalysisInfo.setUserInputs(sqlRowSet.getString("user_input"));
                userAnalysisInfo.setToolUrl(sqlRowSet.getString("tool_url"));
            }
        } catch (Exception e) {
            logger.error("Exception while fetching the tool info ", e);
        }
        return userAnalysisInfo;
    }
}
