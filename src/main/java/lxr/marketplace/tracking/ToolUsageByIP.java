package lxr.marketplace.tracking;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ToolUsageByIP {
	private String ip;
	private Map<Integer,Long> toolUsage; 
	private Calendar startDate;
	private Calendar endDate;
	
	ToolUsageByIP(String ip){
		this.ip = ip;
		toolUsage = new HashMap<Integer, Long>();
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Map<Integer, Long> getToolUsage() {
		return toolUsage;
	}

	public void setToolUsage(Map<Integer, Long> toolUsage) {
		this.toolUsage = toolUsage;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}
}
