package lxr.marketplace.tracking;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class IPToolUsageService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(IPToolUsageService.class);

    public ToolUsageByIP fetchToolUsageByIP(String ip, Calendar startCal, Calendar endCal) {
        ToolUsageByIP toolUsageByIP = new ToolUsageByIP(ip);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        endCal.set(Calendar.HOUR_OF_DAY, endCal.getActualMaximum(Calendar.HOUR_OF_DAY));
        endCal.set(Calendar.MINUTE, endCal.getActualMaximum(Calendar.MINUTE));
        endCal.set(Calendar.SECOND, endCal.getActualMaximum(Calendar.SECOND));
        endCal.set(Calendar.MILLISECOND, endCal.getActualMaximum(Calendar.MILLISECOND));

        startCal.set(Calendar.HOUR_OF_DAY, startCal.getActualMinimum(Calendar.HOUR_OF_DAY));
        startCal.set(Calendar.MINUTE, startCal.getActualMinimum(Calendar.MINUTE));
        startCal.set(Calendar.SECOND, startCal.getActualMinimum(Calendar.SECOND));
        startCal.set(Calendar.MILLISECOND, startCal.getActualMinimum(Calendar.MILLISECOND));

        String query = "select tool_id, sum(count)as count from tool_usage where session_id in "
                + "(select id from session where ip='" + ip + "'and start_time >= '"
                + df.format(startCal.getTime()) + "' and end_time <= '" + df.format(endCal.getTime()) + "') "
                + " group by tool_id";
        try {
            Map<Integer, Long> map = fetchData(query);
            toolUsageByIP.setToolUsage(map);
            return toolUsageByIP;
        } catch (Exception e) {
            logger.error(e);

        }
        return null;
    }

    public ToolUsageByIP fetchAllToolUsageByIP(String ip) {
        ToolUsageByIP toolUsageByIP = new ToolUsageByIP(ip);
        String query = "select tool_id,count from lxrm_session_by_ip where raw_ip='" + ip + "'";
        try {
            Map<Integer, Long> map = fetchData(query);
            toolUsageByIP.setToolUsage(map);
            return toolUsageByIP;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public Map<Integer, Long> fetchData(String query) {
        ResultSetExtractor<Object> mapExtractor = (ResultSet rs) -> {
            Map<Integer, Long> mapOfKeys = new HashMap<>();
            while (rs.next()) {
                Integer toolId = rs.getInt("tool_id");
                Long count = rs.getLong("count");
                mapOfKeys.put(toolId, count);
            }
            return mapOfKeys;
        };
        Map<Integer, Long> map = (HashMap<Integer, Long>) getJdbcTemplate().query(query, mapExtractor);
        return map;
    }
}
