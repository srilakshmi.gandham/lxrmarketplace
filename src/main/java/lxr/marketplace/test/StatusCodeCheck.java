/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.test;

import lxr.marketplace.seoninja.data.NinjaURL;
import lxr.marketplace.util.Common;

/**
 *
 * @author vemanna
 */
public class StatusCodeCheck {
    
    public static void main(String args[]){
        System.out.println("runing");
    }
    public static String checkCurrentURLAndGetHTML(NinjaURL ninjaURL, StringBuilder pageHTML){
	        String currentUrl = ninjaURL.getUrl();
	        String returnString = "";
	        if(!(currentUrl.equals("") || currentUrl == null)){
	            StringBuilder redirectedUrl = new StringBuilder("");
	            String httpCurrentUrl="";
	            int statusCode = 0;
	            if(currentUrl.startsWith("http")){
	                httpCurrentUrl = currentUrl;
	                statusCode = Common.getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
	            } else {
	                httpCurrentUrl = "http://" + currentUrl;        
	                statusCode = Common.getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
	            }
	            
	            if(statusCode == 200){  //URL is working properly
	                ninjaURL.setRedirectedURL(httpCurrentUrl);
	                ninjaURL.setUrlStatus(statusCode);
	                returnString = httpCurrentUrl;
	            }else if(statusCode >= 300 && statusCode < 400){    //URL redirecting
	                if(!redirectedUrl.toString().equals("")){
	                    ninjaURL.setUrlStatus(statusCode);
	                    ninjaURL.setRedirectedURL(redirectedUrl.toString());
	                    returnString = redirectedUrl.toString();
	                }else{      //URL is redirected multiple times
	                    ninjaURL.setUrlStatus(0);
	                    ninjaURL.setRedirectedURL("");
	                    returnString = "too many redirection";
	                }
	            }else{  //For Other Status Code of URL
	                ninjaURL.setUrlStatus(statusCode);
	                returnString = "invalid";
	            }          
	        }else{
	            ninjaURL.setUrlStatus(0);
	            returnString = "invalid";
	        }
	        return returnString;
	    }
    
}
