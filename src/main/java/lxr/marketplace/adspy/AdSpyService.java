/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.ProxyServerConnection;
import lxr.marketplace.apiaccess.SimilarWebAPIService;
import lxr.marketplace.apiaccess.SpyFuAPIService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class AdSpyService {

    private static final Logger LOGGER = Logger.getLogger(AdSpyService.class);

    @Autowired
    private ProxyServerConnection proxyServerConnection;

    @Autowired
    private SimilarWebAPIService similarWebAPIService;

    @Autowired
    private SpyFuAPIService spyFuAPIService;

    @Autowired
    private String downloadFolder;

    @Value("${AdSpy.numberOfResultsPerMail}")
    private int numberOfResultsPerMail;

    @Value("${AdSpy.shouldSendAutoMail}")
    private boolean shouldSendAutoMail;

    public static final String WEBSITES_VISITS_AND_AD_PRESENCE = "Website's Visits & Ad Presence";

    public static final String ESTIMATED_MONTHLY_ADWORDS_BUDGET = "Estimated Monthly Adwords Budget";

    public static final String START_DATE = "startDate";

    public static final String END_DATE = "endDate";

    /*AdSpy*/
    public List<AdSpyV1> findAdForSearchQuery(List<String> searchQueryList, String location) {
        List<AdSpyV1> companyAdSpyList = new ArrayList<>();
        int corePoolSize = 3;
        int maxPoolSize = 3;
        long keepAliveTime = 5000;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        int i = 1;
        boolean isTerminatedAdSpyThread = false;
        for (String searchQuery : searchQueryList) {
            int proxyHostNo = (i % 2 == 0) ? 0 : 1;
            i++;
            AdSpyThread adSpyThread = new AdSpyThread(proxyServerConnection, searchQuery, location, proxyHostNo, companyAdSpyList);
            threadPoolExecutor.execute(adSpyThread);
        }

        while (!isTerminatedAdSpyThread) {
            int active = threadPoolExecutor.getActiveCount();
            int qued = threadPoolExecutor.getQueue().size();
            if (active + qued > 0) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    LOGGER.error("Exception on thread interrupting:" + ex);
                }
            } else {
                isTerminatedAdSpyThread = true;
                threadPoolExecutor.shutdown();
            }
        }

        return companyAdSpyList;
    }

    /*AdSpy V2 Service*/
    public AdSpyModel processAdPresencesForSearchQueryList(List<String> searchQueryList, Set<String> selectedOptions, HttpSession session, HttpServletRequest request, HttpServletResponse response, String userEmail) {
        Map<String, String> formatedDateRange = null;
        boolean shouldProcessQuery = Boolean.TRUE;
        AdSpyModel adSpyModel = null;
        int corePoolSize = 3;
        int maxPoolSize = 3;
        long keepAliveTime = 5000;
        if (selectedOptions.contains(WEBSITES_VISITS_AND_AD_PRESENCE)) {
            formatedDateRange = similarWebAPIService.getAPIFormatedDate(similarWebAPIService.getDataAvailableDateRange());
            shouldProcessQuery = (formatedDateRange != null && (formatedDateRange.containsKey(START_DATE) && formatedDateRange.get(START_DATE) != null)
                    && (formatedDateRange.containsKey(END_DATE) && formatedDateRange.get(END_DATE) != null));
        }
        if (shouldProcessQuery) {
            adSpyModel = new AdSpyModel();
            adSpyModel.setSelectedMetrics(selectedOptions);
            adSpyModel.setQueryDomains(searchQueryList);
            if (formatedDateRange != null) {
                adSpyModel.setDateRange(formatedDateRange);
            }
            List<AdSpy> adSpyResultList = new ArrayList<>();
            LOGGER.info("Initializing AdSpy thread pool to pull metrics from API for query website count: " + searchQueryList.size());
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
            List<Future<AdSpy>> adSpyFutureObj = null;
//            Set<AdSpy> duplicateFilterList = new HashSet<>();
            CountDownLatch latchObj = null;
            int index = 1;
            int proxyHostNo;

            List<AdSpy> autoMailList = null;
            AdSpyModel tempModelForMail = null;
            int numberOfmails = 0;
            String subJectLine = null;
            int firstIndex = 0;
            int lastIndex = numberOfResultsPerMail;
            int totalResultsMailed = 0;
            latchObj = new CountDownLatch(searchQueryList.size());
            adSpyFutureObj = new ArrayList<>();
            if (searchQueryList.size() > numberOfResultsPerMail) {
                LOGGER.info("Total number of auto mails to be send:: " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()));
            }
            /*Collecting threads results*/
 /*Preparing result model*/
            tempModelForMail = new AdSpyModel();
            tempModelForMail.setSelectedMetrics(adSpyModel.getSelectedMetrics());
            tempModelForMail.setDateRange(adSpyModel.getDateRange());
            tempModelForMail.setQueryDomains(adSpyModel.getQueryDomains());
            /*Executing threads*/
            for (String searchQuery : searchQueryList) {
                proxyHostNo = (index % 2 == 0) ? 0 : 1;
                adSpyFutureObj.add(threadPoolExecutor.submit(new AdSpyV2Thread(searchQuery, this.similarWebAPIService, this.spyFuAPIService, index, adSpyModel, proxyHostNo, latchObj)));
                ++index;
            }
            if (!adSpyFutureObj.isEmpty()) {
                for (Future<AdSpy> adSpyFture : adSpyFutureObj) {
                    try {
                        adSpyResultList.add(adSpyFture.get());
                    } catch (InterruptedException | ExecutionException ex) {
                        LOGGER.error("Excpetion in getting from future obj cause: ", ex);
                    }
                    session.setAttribute("processedAdSpyURLs", adSpyResultList.size());
                    if ((adSpyResultList.size() % 75) == 0) {
                        LOGGER.info("Completed analysis for :" + adSpyResultList.size() + ", yet to be analyzed:" + (searchQueryList.size() - adSpyResultList.size()) + ", for total : " + searchQueryList.size());
                    }
                    adSpyResultList.sort((domainOne, domainTwo) -> domainOne.getKeywordId().compareTo(domainTwo.getKeywordId()));
                    /*Sending auto mail after analyzing (numberOfResultsPerMail) from result list if search query list > numberOfResultsPerMail*/
                    if (searchQueryList.size() > numberOfResultsPerMail && shouldSendAutoMail && !adSpyResultList.isEmpty()) {
                        /*For Even count*/
//                        LOGGER.info("value::" + (adSpyResultList.size() % numberOfResultsPerMail));
                        if ((adSpyResultList.size() % numberOfResultsPerMail) == 0) {
                            LOGGER.info("Sending auto mail for compelted result set, present set count:: " + (adSpyResultList.size()) + ", from FirstIndex:: " + firstIndex + " to Lastindex:: " + lastIndex);
                            autoMailList = adSpyResultList.subList(firstIndex, lastIndex);
                            if (autoMailList != null && !autoMailList.isEmpty()) {
                                /*Preparing result model*/
                                tempModelForMail.setAdSpyResult(autoMailList);
                                subJectLine = "Ad Spy Report (" + (numberOfmails + 1) + " out of " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()) + ")";
                                LOGGER.info("SubJectLine:: " + subJectLine);
                                LOGGER.info("Number of records in this mail :: " + (autoMailList.size()) + ", Present processced result size:: " + adSpyResultList.size());
                                LOGGER.info("--------------------");
                                LOGGER.info("First Company URL:: " + autoMailList.get(0).getCompanyName());
                                LOGGER.info("Last Company URL:: " + autoMailList.get(autoMailList.size() - 1).getCompanyName());
                                LOGGER.info("--------------------");
                                firstIndex = lastIndex;
                                lastIndex = firstIndex + numberOfResultsPerMail;
                                ++numberOfmails;
                                totalResultsMailed = totalResultsMailed + autoMailList.size();
                                /*Sending report to Mail*/
                                sendMailAdSpyV2ExcelReport(request, response, userEmail, tempModelForMail, null, subJectLine);
                            }

                        }
                    }
                }
                if (searchQueryList.size() > numberOfResultsPerMail) {
                    LOGGER.info("**************  Total Results Mailed:: " + totalResultsMailed + ", out of:: " + adSpyResultList.size() + " and balance results to be mailed:: " + (adSpyResultList.size() - totalResultsMailed) + "************************");
                }
                if (searchQueryList.size() > numberOfResultsPerMail && shouldSendAutoMail && !adSpyResultList.isEmpty()) {
                    /*For Odd count*/
                    if (totalResultsMailed != adSpyResultList.size()) {
                        firstIndex = totalResultsMailed;
                        lastIndex = firstIndex + (adSpyResultList.size() - totalResultsMailed);
                        LOGGER.info("Sending auto mail for compelted result set, present set count:: " + (adSpyResultList.size()) + ", from FirstIndex:: " + firstIndex + " to Lastindex:: " + lastIndex);
                        autoMailList = adSpyResultList.subList(firstIndex, lastIndex);
                        if (autoMailList != null && !autoMailList.isEmpty()) {
                            tempModelForMail.setAdSpyResult(autoMailList);
                            subJectLine = "Ad Spy Report (" + (numberOfmails + 1) + " out of " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()) + ")";
                            LOGGER.info("SubJectLine:: " + subJectLine);
                            LOGGER.info("Number of records in last mail :: " + (autoMailList.size()) + ", Present result size:: " + adSpyResultList.size());
                            LOGGER.info("--------------------");
                            LOGGER.info("First Company URL:: " + autoMailList.get(0).getCompanyName());
                            LOGGER.info("Last Company URL:: " + autoMailList.get(autoMailList.size() - 1).getCompanyName());
                            LOGGER.info("--------------------");
                            /*Sending report to Mail*/
                            sendMailAdSpyV2ExcelReport(request, response, userEmail, tempModelForMail, null, subJectLine);
                        }
                    }
                }

            }
            LOGGER.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            LOGGER.info("Processing ADSpy Request URLS is compeleted for requested URLS::: " + searchQueryList.size());
            LOGGER.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            threadPoolExecutor.shutdown();
            adSpyModel.setAdSpyResult(adSpyResultList);
        }
        session.setAttribute("isAdSpyV2ThreadTerminated", Boolean.TRUE);
        return adSpyModel;
    }

    public AdSpyModel processAdPresencesForSearchQueryListV2(List<String> searchQueryList, Set<String> selectedOptions, HttpSession session, HttpServletRequest request, HttpServletResponse response, String userEmail) {
        Map<String, String> formatedDateRange = null;
        boolean shouldProcessQuery = Boolean.TRUE;
        int corePoolSize = 1;
        int maxPoolSize = 1;
        long keepAliveTime = 4000;
        AdSpyModel adSpyModel = null;
        if (selectedOptions.contains(WEBSITES_VISITS_AND_AD_PRESENCE)) {
            formatedDateRange = similarWebAPIService.getAPIFormatedDate(similarWebAPIService.getDataAvailableDateRange());
            shouldProcessQuery = (formatedDateRange != null && (formatedDateRange.containsKey(START_DATE) && formatedDateRange.get(START_DATE) != null)
                    && (formatedDateRange.containsKey(END_DATE) && formatedDateRange.get(END_DATE) != null));
        }
        if (shouldProcessQuery) {
            adSpyModel = new AdSpyModel();
            adSpyModel.setSelectedMetrics(selectedOptions);
            adSpyModel.setQueryDomains(searchQueryList);
            if (formatedDateRange != null) {
                adSpyModel.setDateRange(formatedDateRange);
            }
            List<AdSpy> adSpyResultList = new ArrayList<>();
            LOGGER.info("Initializing AdSpy thread pool to pull metrics from API for query website count: " + searchQueryList.size());
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
            CountDownLatch latchObj = null;
            int index = 0;
            int proxyHostNo;

            List<AdSpy> autoMailList = null;
            AdSpyModel tempModelForMail = null;
            int numberOfmails = 0;
            String subJectLine = null;
            int firstIndex = 0;
            int lastIndex = numberOfResultsPerMail;
            int totalResultsMailed = 0;
            latchObj = new CountDownLatch(searchQueryList.size());
            if (searchQueryList.size() > numberOfResultsPerMail) {
                LOGGER.info("Total number of auto mails to be send:: " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()));
            }
            /*Collecting threads results*/
 /*Preparing result model*/
            tempModelForMail = new AdSpyModel();
            tempModelForMail.setSelectedMetrics(adSpyModel.getSelectedMetrics());
            tempModelForMail.setDateRange(adSpyModel.getDateRange());
            tempModelForMail.setQueryDomains(adSpyModel.getQueryDomains());
            LinkedHashMap<String, AdSpy> duplicateFilterMapObj = new LinkedHashMap<>();
            Callable<AdSpy> adSpyTask = null;
            Future<AdSpy> adSpyFutureObje = null;
            for (String searchQuery : searchQueryList) {
                ++index;
                proxyHostNo = (index % 2 == 0) ? 0 : 1;
                if (!duplicateFilterMapObj.containsKey(searchQuery)) {
                    try {
                        adSpyTask = new AdSpyV2Thread(searchQuery, this.similarWebAPIService, this.spyFuAPIService, index, adSpyModel, proxyHostNo, latchObj);
                        adSpyFutureObje = threadPoolExecutor.submit(adSpyTask);
                        adSpyResultList.add(adSpyFutureObje.get());
                        duplicateFilterMapObj.put(searchQuery, adSpyFutureObje.get());
                    } catch (InterruptedException | ExecutionException e) {
                        LOGGER.info("InterruptedException | ExecutionException in  adSpy thread task cause:: " + e.getLocalizedMessage());
                    }
                } else {
                    LOGGER.info("Duplicate found for:: " + searchQuery);
                    adSpyResultList.add(new AdSpy(duplicateFilterMapObj.get(searchQuery),index));
                    latchObj.countDown();
                }
                session.setAttribute("processedAdSpyURLs", adSpyResultList.size());
                if ((adSpyResultList.size() % 75) == 0) {
                    LOGGER.info("Completed analysis for :" + adSpyResultList.size() + ", yet to be analyzed:" + (searchQueryList.size() - adSpyResultList.size()) + ", for total : " + searchQueryList.size());
                }
                /*Sending auto mail after analyzing (numberOfResultsPerMail) from result list if search query list > numberOfResultsPerMail*/
                if (searchQueryList.size() > numberOfResultsPerMail && shouldSendAutoMail && !adSpyResultList.isEmpty()) {
                    adSpyResultList.sort((domainOne, domainTwo) -> domainOne.getKeywordId().compareTo(domainTwo.getKeywordId()));
                    /*For Even count*/
                    if ((adSpyResultList.size() % numberOfResultsPerMail) == 0) {
                        LOGGER.info("Sending auto mail for compelted result set, present set count:: " + (adSpyResultList.size()) + ", from FirstIndex:: " + firstIndex + " to Lastindex:: " + lastIndex);
                        autoMailList = adSpyResultList.subList(firstIndex, lastIndex);
                        if (autoMailList != null && !autoMailList.isEmpty()) {
                            /*Preparing result model*/
                            tempModelForMail.setAdSpyResult(autoMailList);
                            subJectLine = "Ad Spy Report (" + (numberOfmails + 1) + " out of " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()) + ")";
                            LOGGER.info("SubJectLine:: " + subJectLine);
                            LOGGER.info("Number of records in this mail :: " + (autoMailList.size()) + ", Present processced result size:: " + adSpyResultList.size());
                            LOGGER.info("--------------------");
                            LOGGER.info("First Company URL:: " + autoMailList.get(0).getCompanyName());
                            LOGGER.info("Last Company URL:: " + autoMailList.get(autoMailList.size() - 1).getCompanyName());
                            LOGGER.info("--------------------");
                            firstIndex = lastIndex;
                            lastIndex = firstIndex + numberOfResultsPerMail;
                            ++numberOfmails;
                            totalResultsMailed = totalResultsMailed + autoMailList.size();
                            /*Sending report to Mail*/
                            sendMailAdSpyV2ExcelReport(request, response, userEmail, tempModelForMail, null, subJectLine);
                        }

                    }
                }
            }
            adSpyResultList.sort((domainOne, domainTwo) -> domainOne.getKeywordId().compareTo(domainTwo.getKeywordId()));
            if (searchQueryList.size() > numberOfResultsPerMail) {
                LOGGER.info("**************  Total Results Mailed:: " + totalResultsMailed + ", out of:: " + adSpyResultList.size() + " and balance results to be mailed:: " + (adSpyResultList.size() - totalResultsMailed) + "************************");
            }
            if (searchQueryList.size() > numberOfResultsPerMail && shouldSendAutoMail && !adSpyResultList.isEmpty()) {
                /*For Odd count*/
                if (totalResultsMailed != adSpyResultList.size()) {
                    firstIndex = totalResultsMailed;
                    lastIndex = firstIndex + (adSpyResultList.size() - totalResultsMailed);
                    LOGGER.info("Sending auto mail for compelted result set, present set count:: " + (adSpyResultList.size()) + ", from FirstIndex:: " + firstIndex + " to Lastindex:: " + lastIndex);
                    autoMailList = adSpyResultList.subList(firstIndex, lastIndex);
                    if (autoMailList != null && !autoMailList.isEmpty()) {
                        tempModelForMail.setAdSpyResult(autoMailList);
                        subJectLine = "Ad Spy Report (" + (numberOfmails + 1) + " out of " + (ListUtils.partition(searchQueryList, numberOfResultsPerMail).size()) + ")";
                        LOGGER.info("SubJectLine:: " + subJectLine);
                        LOGGER.info("Number of records in last mail :: " + (autoMailList.size()) + ", Present result size:: " + adSpyResultList.size());
                        LOGGER.info("--------------------");
                        LOGGER.info("First Company URL:: " + autoMailList.get(0).getCompanyName());
                        LOGGER.info("Last Company URL:: " + autoMailList.get(autoMailList.size() - 1).getCompanyName());
                        LOGGER.info("--------------------");
                        /*Sending report to Mail*/
                        sendMailAdSpyV2ExcelReport(request, response, userEmail, tempModelForMail, null, subJectLine);
                    }
                }
            }
            LOGGER.info("************** Processing ADSpy Request URLS is compeleted for requested URLS::: " + searchQueryList.size());
            threadPoolExecutor.shutdown();
            adSpyModel.setAdSpyResult(adSpyResultList);
        }
        session.setAttribute("isAdSpyV2ThreadTerminated", Boolean.TRUE);
        return adSpyModel;
    }

    public int getAPIBalanceLimitCount() {
        return similarWebAPIService.getAPIBalanceLimitCount();
    }

    public Map<String, String> convertAPIDateIntoViewFormat(Map<String, String> apiResposneDates) {
        Map<String, String> resposneDates = new HashMap<>();
        SimpleDateFormat viewFormat = new SimpleDateFormat("MMM yyyy");
        SimpleDateFormat apiResponseFormat = new SimpleDateFormat("yyyy-MM");
        Date dateObj;
        try {
            for (Map.Entry<String, String> entry : apiResposneDates.entrySet()) {
                dateObj = apiResponseFormat.parse((String) entry.getValue());
                resposneDates.put(entry.getKey(), Common.convertDateInToAPIRequestFormat(dateObj));
                resposneDates.put("resultView" + WordUtils.capitalize(entry.getKey()), viewFormat.format(dateObj));
            }
        } catch (ParseException pe) {
            LOGGER.error("ParseException cause: ", pe);
        }
        return resposneDates;
    }

    static String getDomainNameFromSite(String site) {
        String domainName = "";
        try {
            if (!site.startsWith("http")) {
                site = "http://".concat(site);
            }
            URL urlObj = new URL(site);
            domainName = urlObj.getHost();
            if (domainName.startsWith("www.")) {
                domainName = domainName.substring(4);
            }
        } catch (MalformedURLException e) {
            LOGGER.error("Malformed exception in url", e);
        }
        return domainName.toLowerCase();
    }

    public List<String> readKeywordsFromFile(String fileName) {

        List<String> urls = null;
        Workbook workbook = null;
        Sheet sheet = null;
        Row rowObj = null;
        try {
            // Creating a Workbook from an Excel file (.xls or .xlsx)
            workbook = WorkbookFactory.create(new File(fileName));
            sheet = null;
            if (workbook != null) {
                sheet = workbook.getSheetAt(0);
            }
            if (sheet != null) {
                rowObj = sheet.getRow(0);
                if (rowObj != null) {
                    if (rowObj.getCell(0) != null && rowObj.getCell(0).getStringCellValue().trim().equalsIgnoreCase("Company URL")) {
                        urls = new LinkedList<>();
                    }
                }
            }
        } catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
            LOGGER.error("IOException | InvalidFormatException in readKeywordsFromFile cause: ", e);
        }
        if (sheet != null && urls != null) {
            for (Row row : sheet) {
                if (row != null) {
                    Cell cell = row.getCell(0);
                    if (cell != null && cell.getCellTypeEnum() == CellType.STRING) {
                        if (cell.getStringCellValue() != null && !cell.getStringCellValue().trim().equals("") && !cell.getStringCellValue().trim().equalsIgnoreCase("Company URL")) {
                            urls.add(cell.getStringCellValue().trim());
                        }
                    }
                }
            }
        }
        return urls;
    }

    public String generateXLSFileofAdSpy(List<AdSpyV1> keywordsAdsList, String downloadfolder) {
        String fileName = null;
        try {
//            keywordsAdsList.sort((AdSpy adOne, AdSpy adTwo)->adOne.getKeyword().compareTo(adTwo.getKeyword()));
            HSSFCellStyle mainheadCellSty;
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFFont headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            HSSFFont font = workBook.createFont();
            font.setBold(true);
            //for main heading
            mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            HSSFSheet sheet = workBook.createSheet("Ad Spy");
            sheet.setDisplayRowColHeadings(false);
            sheet.setDisplayGridlines(false);
            CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
            HSSFFont font1 = null;
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = null;
            HSSFCellStyle subColHeaderStyle = null;
            CreationHelper createHelper = workBook.getCreationHelper();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            //For logo and Reoprt name
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            cell.setCellValue("Ad Spy Report");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 800);

            subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            subColHeaderStyle.setAlignment(HorizontalAlignment.CENTER);

            cell = rows.createCell(4);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 4, 9));
            cell.setCellValue("Reviewed on: " + curDate);
            cell.setCellStyle(subColHeaderStyle);

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            font1 = workBook.createFont();
            font1.setBold(true);
            columnHeaderStyle.setFont(font1);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            subColHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            subColHeaderStyle.setBorderBottom(BorderStyle.THIN);
            subColHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderRight(BorderStyle.THIN);
            subColHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderLeft(BorderStyle.THIN);
            subColHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderTop(BorderStyle.THIN);
            subColHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle valueCellStyle = workBook.createCellStyle();
            valueCellStyle.setFont(font);
            valueCellStyle.setWrapText(true);
            valueCellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            valueCellStyle.setAlignment(HorizontalAlignment.LEFT);
            valueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            valueCellStyle.setBorderBottom(BorderStyle.THIN);
            valueCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderRight(BorderStyle.THIN);
            valueCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderLeft(BorderStyle.THIN);
            valueCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderTop(BorderStyle.THIN);
            valueCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            /*dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));*/
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMMM_dd_yy"));
            LOGGER.debug("Generating Report in .xls format");

            //Report Headings
            rows = sheet.createRow(1);
            rows.setHeight((short) 150);
            rows = sheet.createRow(2);
            rows.setHeight((short) 500);
            cell = rows.createCell(0);
            cell.setCellValue("S.No.");
            cell.setCellStyle(columnHeaderStyle);
            heaCelSty.setWrapText(true);

            cell = rows.createCell(1);
            cell.setCellValue("Company");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(2);
            cell.setCellValue("Ad Presence");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(3);
            cell.setCellValue("Company URL");
            cell.setCellStyle(columnHeaderStyle);

            LOGGER.info("The sheet is before if " + sheet);
//            sheet.createFreezePane(0, 3);

            if (keywordsAdsList != null && keywordsAdsList.size() > 0) {
                try {
                    int x = 3, k = 0;
                    for (int j = 0; j < keywordsAdsList.size(); j++) {
                        k = k + 1;
                        rows = sheet.createRow(x);
                        for (int i = 0; i <= 3; i++) {
                            cell = rows.createCell(i);
                            switch (i) {
                                case 0:
                                    cell.setCellValue(k);
                                    cell.setCellStyle(subColHeaderStyle);
                                    break;
                                case 1:
                                    cell.setCellValue(keywordsAdsList.get(j).getCompanyName());
                                    cell.setCellStyle(valueCellStyle);
                                    break;
                                case 2:
                                    cell.setCellValue(keywordsAdsList.get(j).isAddPresence() ? "YES" : "NO");
                                    cell.setCellStyle(subColHeaderStyle);
                                    break;
                                case 3:
                                    String baseURL = keywordsAdsList.get(j).getUrl();
                                    cell.setCellStyle(valueCellStyle);
                                    if (baseURL == null || baseURL.trim().equals("")) {
                                        baseURL = "-";
                                        cell.setCellStyle(columnHeaderStyle);
                                    }
                                    cell.setCellValue(baseURL);
                                    break;
                                default:
                                    break;
                            }
                        }
                        x = x + 1;
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception in creating UrlStats excel", e);
                }
            }
            if (keywordsAdsList == null || keywordsAdsList.isEmpty()) {
                rows = sheet.createRow(4);
                cell = rows.createCell(0);
                rows.setHeight((short) 500);
                sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 4));
                cell.setCellValue("No Records Found");
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
                cell.setCellStyle(columnHeaderStyle);

                columnHeaderStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                columnHeaderStyle.setFont(font1);
                columnHeaderStyle.setWrapText(true);
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            }

            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(0, 3500);
            sheet.setColumnWidth(1, 12500);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 10000);
            sheet.setColumnWidth(4, 12500);

            LOGGER.info(" AD SPY Report generated in .xls format");
            fileName = Common.createFileName("LXRMarketplace_Ad_Spy_Report") + ".xls";
            File file = new File(downloadfolder + fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (IOException e) {
            LOGGER.error("Exception in generateXLSFileofAdSpy: ", e);
        }
        return fileName;
    }

    public String generateXLSFileofAdSpyV2(AdSpyModel resultAdSpyModel, String downloadfolder) {

        List<AdSpy> adSpyList = null;
        Map<String, String> apiResponseDateRange = null;
        Set<String> selectedMetrics = null;
        String fileName = null;
        HSSFRow rows = null;
        HSSFCell cell = null;
        HSSFFont headfont = null, boldFont = null, metricTitleFont = null;
        HSSFSheet sheet = null;
        HSSFWorkbook workBook = null;

        HSSFCellStyle mainheadCellSty = null, subColHeaderStyle = null, valueCellStyle = null, valueRightCellStyle = null, metricCategoryStyle = null, metricTitlesStyle = null;

        int rowCount = 0;
        /*Step 1:*/
        workBook = new HSSFWorkbook();

        Runtime runtime = Runtime.getRuntime();
        runtime.freeMemory();

        headfont = workBook.createFont();
        headfont.setFontHeightInPoints((short) 20);
        headfont.setColor((HSSFColor.HSSFColorPredefined.ORANGE.getIndex()));

        boldFont = workBook.createFont();
        boldFont.setBold(true);

        metricTitleFont = workBook.createFont();
        metricTitleFont.setBold(true);
        metricTitleFont.setFontHeightInPoints((short) 11);
        metricTitleFont.setColor((HSSFColor.HSSFColorPredefined.WHITE.getIndex()));

        mainheadCellSty = workBook.createCellStyle();
        mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
        mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
        mainheadCellSty.setFont(headfont);

        subColHeaderStyle = getHSSFCellStyle(workBook, IndexedColors.BLACK.getIndex(), boldFont, HorizontalAlignment.LEFT, false, false);

        /*Step 2:*/
        sheet = workBook.createSheet("Ad Spy Report");
        sheet.setDisplayRowColHeadings(false);
        sheet.setDisplayGridlines(false);

        /*Step 3:*/
        CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);

        //For logo and Reoprt name
        rows = sheet.createRow(rowCount);
        rows.setHeight((short) 800);
        cell = rows.createCell(1);
        cell.setCellValue("Ad Spy Report");
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));
        cell.setCellStyle(mainheadCellSty);

        Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
        SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
        dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String curDate = dtf.format(startDat);
        cell = rows.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 4, 15));
        cell.setCellValue("Reviewed on: " + curDate);
        cell.setCellStyle(subColHeaderStyle);

        /*Step 4:*/
 /*Step 5:*/
        if (resultAdSpyModel != null && resultAdSpyModel.getAdSpyResult() != null && !resultAdSpyModel.getAdSpyResult().isEmpty()) {

            HSSFPalette palette = workBook.getCustomPalette();
            palette.setColorAtIndex(HSSFColor.HSSFColorPredefined.AQUA.getIndex(), (byte) 34, (byte) 166, (byte) 177);
            HSSFColor blueHssfColor = palette.getColor(HSSFColor.HSSFColorPredefined.AQUA.getIndex());

            HSSFPalette hashPalette = workBook.getCustomPalette();
            hashPalette.setColorAtIndex(HSSFColor.HSSFColorPredefined.PLUM.getIndex(), (byte) 81, (byte) 81, (byte) 81);
            HSSFColor hashHssfColor = hashPalette.getColor(HSSFColor.HSSFColorPredefined.PLUM.getIndex());

            subColHeaderStyle = getHSSFCellStyle(workBook, IndexedColors.ORANGE.getIndex(), boldFont, HorizontalAlignment.LEFT, false, false);

            if (resultAdSpyModel.getAdSpyResult() != null) {
                adSpyList = resultAdSpyModel.getAdSpyResult();
            }
            if (resultAdSpyModel.getDateRange() != null) {
                apiResponseDateRange = convertAPIDateIntoViewFormat(resultAdSpyModel.getDateRange());
            }
            if (resultAdSpyModel.getSelectedMetrics() != null) {
                selectedMetrics = resultAdSpyModel.getSelectedMetrics();
            }
            //Report Headings
            rows = sheet.createRow(++rowCount);
            rows.setHeight((short) 750);

            if (resultAdSpyModel.getSelectedMetrics().contains(WEBSITES_VISITS_AND_AD_PRESENCE) && apiResponseDateRange != null) {
                rows = sheet.createRow(++rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                if ((apiResponseDateRange.containsKey("resultViewStartDate") && apiResponseDateRange.get("resultViewStartDate") != null)
                        && (apiResponseDateRange.containsKey("resultViewEndDate") && apiResponseDateRange.get("resultViewEndDate") != null)) {
                    cell.setCellValue("Date Range for the data : " + (String) apiResponseDateRange.get("resultViewStartDate") + "  -  " + (String) apiResponseDateRange.get("resultViewEndDate"));
                } else {
                    cell.setCellValue("Date Range for the data : --");
                }
                cell.setCellStyle(subColHeaderStyle);
            }

            metricCategoryStyle = getHSSFCellStyle(workBook, blueHssfColor.getIndex(), metricTitleFont, HorizontalAlignment.CENTER, true, true);
            metricTitlesStyle = getHSSFCellStyle(workBook, hashHssfColor.getIndex(), metricTitleFont, HorizontalAlignment.CENTER, true, true);
            subColHeaderStyle = getHSSFCellStyle(workBook, IndexedColors.ORANGE.getIndex(), boldFont, HorizontalAlignment.CENTER, false, true);
            valueCellStyle = getHSSFCellStyle(workBook, IndexedColors.ORANGE.getIndex(), boldFont, HorizontalAlignment.LEFT, false, true);
            valueRightCellStyle = getHSSFCellStyle(workBook, IndexedColors.ORANGE.getIndex(), boldFont, HorizontalAlignment.RIGHT, false, true);

            Map<String, HSSFCellStyle> cellStylesMap = new HashMap<>();
            cellStylesMap.put("metricCategoryStyle", metricCategoryStyle);
            cellStylesMap.put("metricTitlesStyle", metricTitlesStyle);
            cellStylesMap.put("subColHeaderStyle", subColHeaderStyle);
            cellStylesMap.put("valueCellStyle", valueCellStyle);
            cellStylesMap.put("valueRightCellStyle", valueRightCellStyle);

            DecimalFormat numberFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            numberFormat.applyPattern("###,###.##");

            if (selectedMetrics != null) {
                if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE) && selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                    generateWebSiteVistsAndAdwordsBudgetSheet(sheet, adSpyList, ++rowCount, cellStylesMap, numberFormat);
                } else if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE) && !selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                    generateWebSiteVistsSheet(sheet, adSpyList, ++rowCount, cellStylesMap, numberFormat);
                } else if (selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET) && !selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) {
                    generateAdwordsBudgetSheet(sheet, adSpyList, ++rowCount, cellStylesMap, numberFormat);
                }
            }
        } else {
            subColHeaderStyle = getHSSFCellStyle(workBook, IndexedColors.BLACK.getIndex(), boldFont, HorizontalAlignment.CENTER, false, false);
            rows = sheet.createRow(++rowCount);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
            cell.setCellValue("No Records Found");
            cell.setCellStyle(subColHeaderStyle);
        }
        LOGGER.info("Ad Spy  Report generated in .xls format");
        fileName = Common.createFileName("LXRMarketplace_Ad_Spy_Report") + ".xls";
        File file = new File(downloadfolder + fileName);
        try {
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (IOException e) {
            LOGGER.error("IOException in generateXLSFileofAdSpyV2 cause:: ", e);
        }
        return fileName;
    }

    public void generateWebSiteVistsAndAdwordsBudgetSheet(HSSFSheet sheet, List<AdSpy> resultAdSpyList, int rowCount, Map<String, HSSFCellStyle> cellStylesMap, DecimalFormat numberFormat) {
        List<AdSpy> adSpyList = resultAdSpyList;

        HSSFCellStyle metricCategoryStyle = (HSSFCellStyle) cellStylesMap.get("metricCategoryStyle");
        HSSFCellStyle metricTitlesStyle = (HSSFCellStyle) cellStylesMap.get("metricTitlesStyle");
        HSSFCellStyle subColHeaderStyle = (HSSFCellStyle) cellStylesMap.get("subColHeaderStyle");
        HSSFCellStyle valueCellStyle = (HSSFCellStyle) cellStylesMap.get("valueCellStyle");
        HSSFCellStyle valueRightCellStyle = (HSSFCellStyle) cellStylesMap.get("valueRightCellStyle");

        HSSFRow rows = null;
        HSSFCell cell = null;

        rows = sheet.createRow(rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 2));
        cell.setCellValue("");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(3);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
        cell.setCellValue("Traffic");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(5);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
        cell.setCellValue("Channels Overview");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
        cell.setCellValue("Channels Analysis");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(9);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
        cell.setCellValue("Adwords Budget");
        cell.setCellStyle(metricCategoryStyle);

        rows = sheet.createRow(++rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        cell.setCellValue("S.No.");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(1);
        cell.setCellValue("Company URL");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(2);
        cell.setCellValue("Ad Presence");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(3);
        cell.setCellValue("Total Visits (Past 12 Months)");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(4);
        cell.setCellValue("Average Monthly Visits");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(5);
        cell.setCellValue("Paid Traffic");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(6);
        cell.setCellValue("Organic Traffic");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(7);
        cell.setCellValue("Paid Visits");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(8);
        cell.setCellValue("Organic Visits");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(9);
        cell.setCellValue("Last Month's Estimated Adwords Budget");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(10);
        cell.setCellValue("Approx Monthly Adwords Budget (Last Month's Est Adwords Budget * 6)");
        cell.setCellStyle(metricTitlesStyle);

        sheet.createFreezePane(0, rowCount + 1);

        if (adSpyList != null && adSpyList.size() > 0) {
            try {
                int x = ++rowCount, k = 0;
                for (int j = 0; j < adSpyList.size(); j++) {
                    k = k + 1;
                    rows = sheet.createRow(x);
                    rows.setHeight((short) 600);
                    for (int i = 0; i <= 10; i++) {
                        cell = rows.createCell(i);
                        switch (i) {
                            case 0:
                                cell.setCellValue(adSpyList.get(j).getKeywordId());
//                                cell.setCellValue(k);
                                cell.setCellStyle(subColHeaderStyle);
                                break;
                            case 1:
                                cell.setCellValue(adSpyList.get(j).getCompanyName());
                                cell.setCellStyle(valueCellStyle);
                                break;
                            case 2:
                                cell.setCellValue(adSpyList.get(j).isAddPresence() ? "YES" : "NO");
                                cell.setCellStyle(subColHeaderStyle);
                                break;
                            case 3:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getTrafficTotalVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 4:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getTrafficMonthlyVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 5:
                                cell.setCellValue((adSpyList.get(j).getPaidTraffic() != 0 ? (numberFormat.format(adSpyList.get(j).getPaidTraffic())) : adSpyList.get(j).getPaidTraffic()) + " %");
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 6:
                                cell.setCellValue((adSpyList.get(j).getOrganicTraffic() != 0 ? (numberFormat.format(adSpyList.get(j).getOrganicTraffic())) : adSpyList.get(j).getOrganicTraffic()) + " %");
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 7:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getPaidVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 8:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getOrganicVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 9:
                                cell.setCellValue("$ " + (adSpyList.get(j).getMonthlyAdwordsBudget() != 0 ? (numberFormat.format(adSpyList.get(j).getMonthlyAdwordsBudget())) : adSpyList.get(j).getMonthlyAdwordsBudget()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 10:
                                cell.setCellValue("$ " + (adSpyList.get(j).getApproxMonthlyAdwordsBudget() != 0 ? (numberFormat.format(adSpyList.get(j).getApproxMonthlyAdwordsBudget())) : adSpyList.get(j).getApproxMonthlyAdwordsBudget()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            default:
                                break;
                        }
                    }
                    x = x + 1;
                }
            } catch (Exception e) {
                LOGGER.error("Exception in creating UrlStats excel", e);
            }
        }

        sheet.setColumnWidth(0, 3500);
        sheet.setColumnWidth(1, 12500);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 7000);
        sheet.setColumnWidth(4, 7000);
        sheet.setColumnWidth(5, 7000);
        sheet.setColumnWidth(6, 7000);
        sheet.setColumnWidth(7, 7000);
        sheet.setColumnWidth(8, 7000);
        sheet.setColumnWidth(9, 10000);
        sheet.setColumnWidth(10, 10000);
    }

    public void generateWebSiteVistsSheet(HSSFSheet sheet, List<AdSpy> resultAdSpyList, int rowCount, Map<String, HSSFCellStyle> cellStylesMap, DecimalFormat numberFormat) {
        List<AdSpy> adSpyList = resultAdSpyList;

        HSSFCellStyle metricCategoryStyle = (HSSFCellStyle) cellStylesMap.get("metricCategoryStyle");
        HSSFCellStyle metricTitlesStyle = (HSSFCellStyle) cellStylesMap.get("metricTitlesStyle");
        HSSFCellStyle subColHeaderStyle = (HSSFCellStyle) cellStylesMap.get("subColHeaderStyle");
        HSSFCellStyle valueRightCellStyle = (HSSFCellStyle) cellStylesMap.get("valueRightCellStyle");
        HSSFCellStyle valueCellStyle = (HSSFCellStyle) cellStylesMap.get("valueCellStyle");

        HSSFRow rows = null;
        HSSFCell cell = null;

        rows = sheet.createRow(rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 2));
        cell.setCellValue("");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(3);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
        cell.setCellValue("Traffic");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(5);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
        cell.setCellValue("Channels Overview");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
        cell.setCellValue("Channels Analysis");
        cell.setCellStyle(metricCategoryStyle);

        rows = sheet.createRow(++rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        cell.setCellValue("S.No.");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(1);
        cell.setCellValue("Company URL");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(2);
        cell.setCellValue("Ad Presence");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(3);
        cell.setCellValue("Total Visits (Past 12 Months)");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(4);
        cell.setCellValue("Average Monthly Visits");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(5);
        cell.setCellValue("Paid Traffic");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(6);
        cell.setCellValue("Organic Traffic");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(7);
        cell.setCellValue("Paid Visits");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(8);
        cell.setCellValue("Organic Visits");
        cell.setCellStyle(metricTitlesStyle);

        sheet.createFreezePane(0, rowCount + 1);

        if (adSpyList != null && adSpyList.size() > 0) {
            try {
                int x = ++rowCount, k = 0;
                for (int j = 0; j < adSpyList.size(); j++) {
                    k = k + 1;
                    rows = sheet.createRow(x);
                    rows.setHeight((short) 600);
                    for (int i = 0; i <= 8; i++) {
                        cell = rows.createCell(i);
                        switch (i) {
                            case 0:
//                                cell.setCellValue(k);
                                cell.setCellValue(adSpyList.get(j).getKeywordId());
                                cell.setCellStyle(subColHeaderStyle);
                                break;
                            case 1:
                                cell.setCellValue(adSpyList.get(j).getCompanyName());
                                cell.setCellStyle(valueCellStyle);
                                break;
                            case 2:
                                cell.setCellValue(adSpyList.get(j).isAddPresence() ? "YES" : "NO");
                                cell.setCellStyle(subColHeaderStyle);
                                break;
                            case 3:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getTrafficTotalVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 4:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getTrafficMonthlyVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 5:
                                cell.setCellValue((adSpyList.get(j).getPaidTraffic() != 0 ? (numberFormat.format(adSpyList.get(j).getPaidTraffic())) : adSpyList.get(j).getPaidTraffic()) + " %");
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 6:
                                cell.setCellValue((adSpyList.get(j).getOrganicTraffic() != 0 ? (numberFormat.format(adSpyList.get(j).getOrganicTraffic())) : adSpyList.get(j).getOrganicTraffic()) + " %");
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 7:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getPaidVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 8:
                                cell.setCellValue(numberFormat.format(adSpyList.get(j).getOrganicVisits()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            default:
                                break;
                        }
                    }
                    x = x + 1;
                }
            } catch (Exception e) {
                LOGGER.error("Exception in creating UrlStats excel", e);
            }
        }
        sheet.setColumnWidth(0, 3500);
        sheet.setColumnWidth(1, 12500);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 7000);
        sheet.setColumnWidth(4, 7000);
        sheet.setColumnWidth(5, 7000);
        sheet.setColumnWidth(6, 7000);
        sheet.setColumnWidth(7, 7000);
        sheet.setColumnWidth(8, 7000);
    }

    public void generateAdwordsBudgetSheet(HSSFSheet sheet, List<AdSpy> resultAdSpyList, int rowCount, Map<String, HSSFCellStyle> cellStylesMap, DecimalFormat numberFormat) {
        List<AdSpy> adSpyList = resultAdSpyList;

        HSSFCellStyle metricCategoryStyle = (HSSFCellStyle) cellStylesMap.get("metricCategoryStyle");
        HSSFCellStyle metricTitlesStyle = (HSSFCellStyle) cellStylesMap.get("metricTitlesStyle");
        HSSFCellStyle subColHeaderStyle = (HSSFCellStyle) cellStylesMap.get("subColHeaderStyle");
        HSSFCellStyle valueRightCellStyle = (HSSFCellStyle) cellStylesMap.get("valueRightCellStyle");
        HSSFCellStyle valueCellStyle = (HSSFCellStyle) cellStylesMap.get("valueCellStyle");

        HSSFRow rows = null;
        HSSFCell cell = null;

        rows = sheet.createRow(rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
        cell.setCellValue("");
        cell.setCellStyle(metricCategoryStyle);

        cell = rows.createCell(2);
        sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
        cell.setCellValue("Adwords Budget");
        cell.setCellStyle(metricCategoryStyle);

        rows = sheet.createRow(++rowCount);
        rows.setHeight((short) 600);
        cell = rows.createCell(0);
        cell.setCellValue("S.No.");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(1);
        cell.setCellValue("Company URL");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(2);
        cell.setCellValue("Last Month's Estimated Adwords Budget");
        cell.setCellStyle(metricTitlesStyle);

        cell = rows.createCell(3);
        cell.setCellValue("Approx Monthly Adwords Budget (Last Month's Est Adwords Budget * 6)");
        cell.setCellStyle(metricTitlesStyle);

        sheet.createFreezePane(0, rowCount + 1);

        if (adSpyList != null && adSpyList.size() > 0) {
            try {
                int x = ++rowCount, k = 0;
                for (int j = 0; j < adSpyList.size(); j++) {
                    k = k + 1;
                    rows = sheet.createRow(x);
                    rows.setHeight((short) 600);
                    for (int i = 0; i <= 4; i++) {
                        cell = rows.createCell(i);
                        switch (i) {
                            case 0:
//                                cell.setCellValue(k);
                                cell.setCellValue(adSpyList.get(j).getKeywordId());
                                cell.setCellStyle(subColHeaderStyle);
                                break;
                            case 1:
                                cell.setCellValue(adSpyList.get(j).getCompanyName());
                                cell.setCellStyle(valueCellStyle);
                                break;
                            case 2:
                                cell.setCellValue("$ " + (adSpyList.get(j).getMonthlyAdwordsBudget() != 0 ? (numberFormat.format(adSpyList.get(j).getMonthlyAdwordsBudget())) : adSpyList.get(j).getMonthlyAdwordsBudget()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            case 3:
                                cell.setCellValue("$ " + (adSpyList.get(j).getApproxMonthlyAdwordsBudget() != 0 ? (numberFormat.format(adSpyList.get(j).getApproxMonthlyAdwordsBudget())) : adSpyList.get(j).getApproxMonthlyAdwordsBudget()));
                                cell.setCellStyle(valueRightCellStyle);
                                break;
                            default:
                                break;
                        }
                    }
                    x = x + 1;
                }
            } catch (Exception e) {
                LOGGER.error("Exception in creating UrlStats excel", e);
            }
        }
        if (adSpyList == null || adSpyList.isEmpty()) {
            rows = sheet.createRow(++rowCount);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
            cell.setCellValue("No Records Found");
            cell.setCellStyle(subColHeaderStyle);
        }

        sheet.setColumnWidth(0, 3500);
        sheet.setColumnWidth(1, 12500);
        sheet.setColumnWidth(2, 10000);
        sheet.setColumnWidth(3, 10000);
        sheet.setColumnWidth(4, 7000);
    }

    private HSSFCellStyle getHSSFCellStyle(HSSFWorkbook workBook, short hssfColorIndex, HSSFFont font, HorizontalAlignment postion, boolean fillPattern, boolean haveBorder) {
        HSSFCellStyle cellStyle = workBook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setWrapText(true);
        cellStyle.setAlignment(postion);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setFillForegroundColor(hssfColorIndex);
        if (fillPattern) {
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        if (haveBorder) {
            cellStyle.setBorderBottom(BorderStyle.THIN);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderLeft(BorderStyle.THIN);
            cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderRight(BorderStyle.THIN);
            cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderTop(BorderStyle.THIN);
            cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        }
        return cellStyle;
    }

    public boolean sendMailAdSpyV2ExcelReport(HttpServletRequest request, HttpServletResponse response, String userEmail, AdSpyModel resultAdSpyModel, String existedFile, String subjectLine) {
        boolean mailStatus = false;
        try {
            String fileName = existedFile;
            if (fileName == null && resultAdSpyModel != null) {
                fileName = generateXLSFileofAdSpyV2(resultAdSpyModel, downloadFolder);
            }
            if (fileName != null) {
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, fileName, userEmail, subjectLine);
                if (mailStatus) {
                    LOGGER.info("Report sended to user email:: " + userEmail);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Send report to email: " + userEmail + ", request: " + e.getMessage());
        }
        return mailStatus;
    }
}
