/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.apiaccess.ProxyServerConnection;
import lxr.marketplace.apiaccess.SpyFuAPIService;
import org.apache.log4j.Logger;

/**
 *
 * @author NE16T1213
 */
public class AdWordsBudgetTask implements Callable<Double> {
    private static final Logger LOGGER = Logger.getLogger(ProxyServerConnection.class);

    private final String domain;
    private final SpyFuAPIService spyFuAPIService;
    private CountDownLatch latchObj;
    private int proxyHostNo;

    public AdWordsBudgetTask(String domain, SpyFuAPIService spyFuAPIService, CountDownLatch latchObj, int proxyHostNo) {
        this.domain = domain;
        this.spyFuAPIService = spyFuAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Double call() {
        Double adWordsBudget = spyFuAPIService.getAdwordsBudget(domain, this.proxyHostNo);
        latchObj.countDown();
        return adWordsBudget;
    }

}
