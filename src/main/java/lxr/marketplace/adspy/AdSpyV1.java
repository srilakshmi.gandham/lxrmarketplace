/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author vidyasagar.korada
 */
public class AdSpyV1 implements Serializable{
     private StringBuffer queryKeywords;
    private String geoLocation;
    private MultipartFile keywordsFile;
    private String companyName;
    private String url;
    private boolean addPresence;
    
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    
    public StringBuffer getQueryKeywords() {
        return queryKeywords;
    }

    public void setQueryKeywords(StringBuffer queryKeywords) {
        this.queryKeywords = queryKeywords;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    public MultipartFile getKeywordsFile() {
        return keywordsFile;
    }

    public void setKeywordsFile(MultipartFile keywordsFile) {
        this.keywordsFile = keywordsFile;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAddPresence() {
        return addPresence;
    }

    public void setAddPresence(boolean addPresence) {
        this.addPresence = addPresence;
    }
    
    
    
}
