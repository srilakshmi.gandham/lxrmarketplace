/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.PreSalesToolUsage;
import lxr.marketplace.util.PreSalesToolUsageDAOService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author sagar
 */
@Controller
@RequestMapping("/ad-spy-v2.html")
public class AdSpyV2Controller {

    private static final Logger LOGGER = Logger.getLogger(AdSpyV2Controller.class);

    @Autowired
    private AdSpyService adSpyService;

    @Autowired
    private PreSalesToolUsageDAOService preSalesToolUsageDAOService;

    @Autowired
    private String downloadFolder;

    @Autowired
    private String uploadFolder;

    @Autowired
    private MessageSource messageSource;

    @Value("${PrseSales.Offline.URLLimit}")
    private int offlineURLLimit;

    @Value("${SimilarWeb.API.Threashold}")
    private int similarWebAPIThreashold;

    @Value("${AdSpy.numberOfResultsPerMail}")
    private int numberOfResultsPerMail;

//    @Value("${AdSpy.shouldSendAutoMail}")
//    private boolean shouldSendAutoMail;
    @RequestMapping(value = "/ad-spy-v2.html", method = RequestMethod.GET)
    public String showAdSpyV2(HttpSession session, Model model) {
        /*Minimum API calls are required to get data for one domain.*/
        model.addAttribute("apiBalanceCount", adSpyService.getAPIBalanceLimitCount());
        resetToolSessionObjects(session);
        return "/views/adSpy/adSpyV2";
    }

    @RequestMapping(params = {"request=analyze"}, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object[] processAddSpyV2Metrics(@RequestParam("manuallyList") String manuallyList, @RequestParam("domainsList") MultipartFile fileList,
            @RequestParam("offlineReportEmail") String offlineReportEmail, @RequestParam("toolUsageUserEmail") String toolUsageUserEmail, @RequestParam("selectedMetrics") String selectedMetrics, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        resetToolSessionObjects(session);
        Object[] responseObj = new Object[8];
        boolean isMetricsPulled = Boolean.FALSE;
        String errorMessage = null;
        Set<String> metricsList = new HashSet<>();;
        if (selectedMetrics != null && !selectedMetrics.trim().isEmpty()) {
            if (selectedMetrics.contains(",")) {
                metricsList.addAll(Arrays.asList(selectedMetrics.trim().split(",")));
                /*String[] metricSet = selectedMetrics.split(",");
                if (metricSet != null && metricSet.length > 0) {
                    metricsList.addAll(Arrays.asList(selectedMetrics.split(",")));
                }*/
            } else {
                metricsList.add(selectedMetrics.trim());
            }
            /*Minimum API calls are required to get data for one domain.*/
            boolean shouldProcessResult = metricsList.isEmpty() ? Boolean.FALSE : (metricsList.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) ? (adSpyService.getAPIBalanceLimitCount() > 4) : Boolean.TRUE;
            if (shouldProcessResult) {
                List<String> searchQueryList = null;
                AdSpyModel adSpyModel = null;
                Map<String, String> dataDateRanges = null;
                if (manuallyList != null && !manuallyList.trim().isEmpty()) {
                    searchQueryList = Common.convertStringToArrayList(manuallyList, "\n");
                } else if (fileList != null && fileList.getSize() != 1 && !fileList.getOriginalFilename().equals("emptyFile")) {
                    String fileName = Common.saveFile(fileList, uploadFolder);
                    if (fileName != null) {
                        searchQueryList = adSpyService.readKeywordsFromFile(uploadFolder.concat(fileName));
                    }
                }

                if (searchQueryList != null && !searchQueryList.isEmpty() && searchQueryList.size() <= similarWebAPIThreashold) {
                    resetToolSessionObjects(session);
                    adSpyModel = adSpyService.processAdPresencesForSearchQueryListV2(searchQueryList, metricsList, session, request, response, ((offlineReportEmail != null && !offlineReportEmail.trim().equals("")) ? offlineReportEmail : toolUsageUserEmail));
                }
                if (adSpyModel != null) {
                    if (adSpyModel.getAdSpyResult() != null && !adSpyModel.getAdSpyResult().isEmpty()) {
                        LOGGER.info("Completed pulling data from API, Preparing data set for response");
                        session.setAttribute("resultAdSpyModel", adSpyModel);
                        /*
                    Commented on July28,2018 Sorting not requried suggested by Presales Team.
                    resultAdSpyList.sort((AdSpy adOne, AdSpy adTwo) -> adOne.getCompanyName().compareTo(adTwo.getCompanyName()));
                         */
                        isMetricsPulled = Boolean.TRUE;
                        responseObj[2] = adSpyModel.getAdSpyResult();
                    }
                    if (adSpyModel.getDateRange() != null && isMetricsPulled) {
                        dataDateRanges = adSpyService.convertAPIDateIntoViewFormat(adSpyModel.getDateRange());
                        if (dataDateRanges != null) {
                            if (dataDateRanges.containsKey("resultViewStartDate")) {
                                responseObj[3] = ((String) dataDateRanges.get("resultViewStartDate"));
                            }
                            if (dataDateRanges.containsKey("resultViewEndDate")) {
                                responseObj[4] = ((String) dataDateRanges.get("resultViewEndDate"));
                            }
                        }
                    }
                    if (adSpyModel.getSelectedMetrics() != null) {
                        responseObj[6] = selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE);
                        responseObj[7] = selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET);
                    }
                    if (adSpyModel.getAdSpyResult().size() > offlineURLLimit && adSpyModel.getAdSpyResult().size() <= numberOfResultsPerMail && offlineReportEmail != null && !offlineReportEmail.trim().equals("")) {
                        LOGGER.info("Sending AdSpy tool analysis report for offlineURLLimit condition to user email:: " + offlineReportEmail);
//                        adSpyService.sendMailAdSpyV2ExcelReport(request, response, adSpyModel, offlineReportEmail, null);
                        adSpyService.sendMailAdSpyV2ExcelReport(request, response, offlineReportEmail, adSpyModel, null, "Ad Spy Report");
                    }
                    /*To track tool usage*/
                    if (toolUsageUserEmail != null && !toolUsageUserEmail.trim().equals("") && adSpyModel.getAdSpyResult() != null && adSpyModel.getSelectedMetrics() != null && searchQueryList != null) {
                        PreSalesToolUsage preSalesToolUsage = new PreSalesToolUsage();
                        preSalesToolUsage.setToolId(44);
                        preSalesToolUsage.setUserEmail(toolUsageUserEmail);
                        preSalesToolUsage.setNumberOfRequests(searchQueryList.size());
                        preSalesToolUsage.setCreatedDate(LocalDateTime.now());
                        Map<String, Integer> apiUsageList = new LinkedHashMap<>();
                        /*Adding Similar web api usage*/
                        if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) {
                            apiUsageList.put(PreSalesToolUsageDAOService.SIMILARWEB, adSpyModel.getAdSpyResult().size());
                        }
                        /*Adding SpyFu api usage*/
                        if (selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                            apiUsageList.put(PreSalesToolUsageDAOService.SPYFU, adSpyModel.getAdSpyResult().size());
                        }
                        preSalesToolUsage.setApiList(apiUsageList);
                        boolean isToolUsageTracked = preSalesToolUsageDAOService.insertPreSalesToolUsage(preSalesToolUsage);
                        LOGGER.info("Tool usage tracking status::" + isToolUsageTracked + ", for requestCount:: " + searchQueryList.size() + ", for responseCount:: " + adSpyModel.getAdSpyResult().size());
                    }
                } else if (searchQueryList != null && !searchQueryList.isEmpty() && searchQueryList.size() <= similarWebAPIThreashold) {
                    errorMessage = "Your feed contains" + searchQueryList.size() + "company URL(S). Please upload a maximum of " + similarWebAPIThreashold + " company URL(S).";
                } else {
                    errorMessage = messageSource.getMessage("PreSales.tool.error", null, Locale.US);
                }
            } else {
                errorMessage = messageSource.getMessage("SimilarWeb.API.error", null, Locale.US);
            }
        } else {
            errorMessage = messageSource.getMessage("AdSpy.options.error", null, Locale.US);
        }
        if (!isMetricsPulled) {
            session.setAttribute("processedAdSpyURLs", 0);
            session.setAttribute("isAdSpyV2ThreadTerminated", Boolean.TRUE);
        }
        responseObj[0] = isMetricsPulled;
        responseObj[1] = errorMessage;
        responseObj[5] = adSpyService.getAPIBalanceLimitCount();
        LOGGER.info("Response Data set is compeleted and response [0]::: " + responseObj[0]);
        return responseObj;
    }

    @RequestMapping(params = {"request=validateURLSSize"}, method = RequestMethod.POST)
    @ResponseBody
    public int validateQueryURLSSize(@RequestParam("fileList") MultipartFile fileList, HttpSession session, HttpServletResponse response) {
        String fileName = null;
        List<String> searchQueryList = null;
        if (fileList != null && fileList.getSize() != 1 && !fileList.getOriginalFilename().equals("emptyFile")) {
            fileName = Common.saveFile(fileList, uploadFolder);
        }
        if (fileName != null) {
            searchQueryList = adSpyService.readKeywordsFromFile(uploadFolder.concat(fileName));
        }

        return (fileName != null && searchQueryList != null) ? searchQueryList.size() : -1;
    }

    @RequestMapping(params = {"request=analyzedCount"}, method = RequestMethod.GET)
    @ResponseBody
    public Object[] getProcessedAdSpyURLsCount(HttpSession session, HttpServletResponse response) {
        LOGGER.debug("Request --> To get  updated count of analyzed URLs");
        Object[] responseObj = new Object[2];
        int processedURLsCount = 0;
        boolean isThreadTerminated = false;
        if (session.getAttribute("isAdSpyV2ThreadTerminated") != null) {
            isThreadTerminated = (boolean) session.getAttribute("isAdSpyV2ThreadTerminated");
        }
        if (session.getAttribute("processedAdSpyURLs") != null) {
            processedURLsCount = (int) session.getAttribute("processedAdSpyURLs");
        }
        if (isThreadTerminated) {
            LOGGER.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            LOGGER.info("Analysis COMPLETED");
            LOGGER.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        }
        responseObj[0] = isThreadTerminated;
        responseObj[1] = processedURLsCount;
        LOGGER.debug("Finished --> Updated count of analyzed URLs and isThreadTerminated: " + isThreadTerminated + ", processedURLsCount: " + processedURLsCount);
        return responseObj;
    }

    @RequestMapping(params = {"request=download"}, method = RequestMethod.POST)
    public String generatingAdSpyV2ExcelReport(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        try {
            String fileName = null;
            if (session.getAttribute("resultAdSpyModel") != null) {
                fileName = adSpyService.generateXLSFileofAdSpyV2(((AdSpyModel) session.getAttribute("resultAdSpyModel")), downloadFolder);
                session.setAttribute("AdSpyListXLSReport", fileName);
            }
            LOGGER.info("Downloadble Ad Spy V2 report: " + fileName);
            if (fileName != null && !fileName.trim().equals("")) {
                Common.downloadReport(response, downloadFolder, fileName, "xls");
                return null;
            }
        } catch (Exception e) {
            LOGGER.error("Exception in generatingAdSpyV2ExcelReport cause: ", e);
        }
        return "/views/adSpy/adSpyV2";
    }

    /*
     * Preparing Ad Spy report to send mail to user.
     */
    @RequestMapping(params = {"request=sendmail"}, method = RequestMethod.POST)
    @ResponseBody
    public boolean sendMailAdSpyV2ExcelReport(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("email") String userEmail) {
        boolean mailStatus = Boolean.FALSE;
        String fileName = null;
        AdSpyModel resultAdSpyModel = null;
        if (session.getAttribute("AdSpyListXLSReport") != null) {
            fileName = (String) session.getAttribute("AdSpyListXLSReport");
        }
        if (session.getAttribute("resultAdSpyModel") != null) {
            resultAdSpyModel = (AdSpyModel) session.getAttribute("resultAdSpyModel");
        }
        mailStatus = adSpyService.sendMailAdSpyV2ExcelReport(request, response, userEmail, resultAdSpyModel, fileName, "Ad Spy Report");
        return mailStatus;
    }

    public void resetToolSessionObjects(HttpSession session) {
//        session.removeAttribute("resultAdSpyList");
//        session.removeAttribute("dataDateRanges");
        session.removeAttribute("resultAdSpyModel");
        session.removeAttribute("AdSpyListXLSReport");
        session.removeAttribute("isAdSpyV2ThreadTerminated");
        session.removeAttribute("processedAdSpyURLs");

    }
}
