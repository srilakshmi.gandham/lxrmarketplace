/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lxr.marketplace.apiaccess.SimilarWebAPIService;
import lxr.marketplace.apiaccess.SpyFuAPIService;
import org.apache.log4j.Logger;

/**
 *
 * @author sagar
 */
public class AdSpyV2Thread implements Callable<AdSpy> {

    private static final Logger LOGGER = Logger.getLogger(AdSpyV2Thread.class);

    private final String domain;
    private final SimilarWebAPIService similarWebAPIService;
    private final SpyFuAPIService spyFuAPIService;
//    private Set<AdSpy> duplicateFilterList;
    private final AdSpyModel adSpyModel;
    private int index;
    private final int proxyHostNo;
    private CountDownLatch latchObj;

    public AdSpyV2Thread(String domain, SimilarWebAPIService similarWebAPIService, SpyFuAPIService spyFuAPIService, int index, AdSpyModel adSpyModel, int proxyHostNo, CountDownLatch latchObj) {
        this.domain = domain.trim();
        this.similarWebAPIService = similarWebAPIService;
        this.spyFuAPIService = spyFuAPIService;
        this.index = index;
//        this.duplicateFilterList = duplicateFilterList;
        this.adSpyModel = adSpyModel;
        this.proxyHostNo = proxyHostNo;
        this.latchObj = latchObj;
    }

    @Override
    public AdSpy call() {
//        AdSpy existingAdSpy = null;
        AdSpy adSpy = null;
        AdSpy tempAdSpy = null;
        try {
            if (this.adSpyModel.getSelectedMetrics().contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) {
                tempAdSpy = findAdPresence(this.domain, this.adSpyModel.getDateRange().get(AdSpyService.START_DATE), this.adSpyModel.getDateRange().get(AdSpyService.END_DATE), this.adSpyModel.getSelectedMetrics(), this.similarWebAPIService, this.spyFuAPIService, this.proxyHostNo);
                if (tempAdSpy != null) {
                    adSpy = new AdSpy(tempAdSpy);
                }
            } else {
                tempAdSpy = findAdPresence(this.domain, null, null, this.adSpyModel.getSelectedMetrics(), this.similarWebAPIService, this.spyFuAPIService, this.proxyHostNo);
                if (tempAdSpy != null) {
                    adSpy = new AdSpy(tempAdSpy);
                }
            }
            if (adSpy != null) {
                adSpy.setKeywordId(this.index);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in call when returning AdSpy object cause:: ", e);
        }
        latchObj.countDown();
        return adSpy;
    }

    public AdSpy findAdPresence(String domain, String startDate, String endDate, Set<String> selectedMetrics, SimilarWebAPIService similarWebAPIService, SpyFuAPIService spyFuAPIService, int proxyHostNo) {
        AdSpy adSpy = null;
        if (domain != null && !domain.equalsIgnoreCase("#NA") && !domain.equals("-") && !domain.equalsIgnoreCase("na")) {
            adSpy = new AdSpy();
            domain = domain.trim();
            if (domain.contains("http") || domain.contains("www.") || domain.endsWith("/")) {
                domain = AdSpyService.getDomainNameFromSite(domain.trim());
            }
//            adSpy.setCompanyName((domain + "  "+this.index));
            adSpy.setCompanyName(domain);
            Future<Map<String, Double>> monthlyVisitsFuture = null;
            Future<Map<String, Double>> channelsOverviewFuture = null;
            Future<Map<String, Long>> channelsAnalysisFuture = null;
            Future<Double> adwordsBudgetFuture = null;
            CountDownLatch apiMetricsLatch = null;
            ExecutorService service = null;
            if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE) && selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                apiMetricsLatch = new CountDownLatch(4);
                service = Executors.newFixedThreadPool(4);
            } else if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE) && !selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                apiMetricsLatch = new CountDownLatch(3);
                service = Executors.newFixedThreadPool(3);
            } else if (selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET) && !selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) {
                apiMetricsLatch = new CountDownLatch(1);
                service = Executors.newFixedThreadPool(1);
            }
            if (service != null) {
                if (selectedMetrics.contains(AdSpyService.WEBSITES_VISITS_AND_AD_PRESENCE)) {
                    monthlyVisitsFuture = service.submit(new MonthlyVisitsTask(domain, startDate, endDate, similarWebAPIService, apiMetricsLatch, proxyHostNo));
                    /*This thread returns  ChannlePerformanceTask score for domain*/
                    channelsOverviewFuture = service.submit(new ChannelsOverViewTask(domain, startDate, endDate, similarWebAPIService, apiMetricsLatch, proxyHostNo));
                    /*This thread returns  PaidSearchVistsTask score for domain*/
                    channelsAnalysisFuture = service.submit(new ChannelsAnalysisTask(domain, startDate, endDate, similarWebAPIService, apiMetricsLatch, proxyHostNo));
                }
                if (selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                    adwordsBudgetFuture = service.submit(new AdWordsBudgetTask(domain, spyFuAPIService, apiMetricsLatch, proxyHostNo));
                }
                try {
                    /*Passing latch object to track count*/
                    if (apiMetricsLatch != null) {
                        apiMetricsLatch.await();
                    }
                    if (monthlyVisitsFuture != null) {
                        Map<String, Double> monthlyVisitsMap = monthlyVisitsFuture.get();
                        if (monthlyVisitsMap != null && monthlyVisitsMap.containsKey("totalVists")) {
                            adSpy.setTrafficTotalVisits(monthlyVisitsMap.get("totalVists"));
                        }else{
                            LOGGER.info("TotalVists is not found in MonthlyVisitsMap for domain:: "+domain);
                        }
                        if (monthlyVisitsMap != null && monthlyVisitsMap.containsKey("monthlyVists")) {
                            adSpy.setTrafficMonthlyVisits(monthlyVisitsMap.get("monthlyVists"));
                        }else{
                            LOGGER.info("MonthlyVists is not found in MonthlyVisitsMap for domain:: "+domain);
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    LOGGER.error("InterruptedException | ExecutionException when fetching data from monthlyVisitsFuture cause:: ", e);
                }
                try {
                    if (channelsOverviewFuture != null) {
                        Map<String, Double> channelsOverViewMap = channelsOverviewFuture.get();
                        if (channelsOverViewMap != null && channelsOverViewMap.containsKey("organicTraffic")) {
                            adSpy.setOrganicTraffic(channelsOverViewMap.get("organicTraffic"));
                        }else{
                            LOGGER.info("OrganicTraffic is not found in ChannelsOverViewMap  for domain:: "+domain);
                        }
                        if (channelsOverViewMap != null && channelsOverViewMap.containsKey("paidTraffic")) {
                            adSpy.setPaidTraffic(channelsOverViewMap.get("paidTraffic"));
                        }else{
                            LOGGER.info("PaidTraffic is not found in ChannelsOverViewMap for domain:: "+domain);
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    LOGGER.error("InterruptedException | ExecutionException when fetching data from channelsOverviewFuture cause:: ", e);
                }
                try {
                    if (channelsAnalysisFuture != null) {
                        Map<String, Long> channelsAnalysisMap = channelsAnalysisFuture.get();
                        if (channelsAnalysisMap != null && channelsAnalysisMap.containsKey("organicVisits")) {
                            adSpy.setOrganicVisits(channelsAnalysisMap.get("organicVisits"));
                        }else{
                            LOGGER.info("OrganicVisits is not found in ChannelsAnalysisMap for domain:: "+domain);
                        }
                        if (channelsAnalysisMap != null && channelsAnalysisMap.containsKey("paidVisits")) {
                            adSpy.setPaidVisits(channelsAnalysisMap.get("paidVisits"));
                        }else{
                            LOGGER.info("PaidVisits is not found in ChannelsAnalysisMap for domain:: "+domain);
                        }
                    }

                } catch (InterruptedException | ExecutionException e) {
                    LOGGER.error("InterruptedException | ExecutionException when fetching data from channelsAnalysisFuture cause:: ", e);
                }
                try {
                    if (adwordsBudgetFuture != null) {
                        Double monthlyAdwordsBudget = adwordsBudgetFuture.get();
                        adSpy.setMonthlyAdwordsBudget(monthlyAdwordsBudget);
                        adSpy.setApproxMonthlyAdwordsBudget((monthlyAdwordsBudget * 6));
                    } else if (selectedMetrics.contains(AdSpyService.ESTIMATED_MONTHLY_ADWORDS_BUDGET)) {
                        LOGGER.info("AdwordsBudgetFuture is not completed for :: " + domain);
                    }
                } catch (InterruptedException | ExecutionException e) {
                    LOGGER.error("InterruptedException | ExecutionException when fetching data from adwordsBudgetFuture cause:: ", e);
                }
                service.shutdown();
            }
            adSpy.setAddPresence(adSpy.getPaidTraffic() > 0);
        }
        return adSpy;
    }
}
