/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import lxr.marketplace.apiaccess.ProxyServerConnection;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author vemanna
 */
public class AdSpyThread implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(AdSpyThread.class);

    private final ProxyServerConnection proxyServerConnection;
    private final String searchQuery;
    private final String location;
    private final int proxyHostNo;
    private final List<AdSpyV1> keywordsAdsList;

    public AdSpyThread(ProxyServerConnection proxyServerConnection, String searchQuery, String location, int proxyHostNo, List<AdSpyV1> keywordsAdsList) {
        this.proxyServerConnection = proxyServerConnection;
        this.searchQuery = searchQuery;
        this.location = location;
        this.proxyHostNo = proxyHostNo;
        this.keywordsAdsList = keywordsAdsList;
    }

    @Override
    public void run() {
        findAd();
    }

    private void findAd() {
        AdSpyV1 adSpyObj = new AdSpyV1();
        adSpyObj.setCompanyName(searchQuery);
        String encodedSearchQuery = null;
        try {
            encodedSearchQuery = URLEncoder.encode(searchQuery, "UTF-8");
            encodedSearchQuery = "https://www.google.com/search?q=" + encodedSearchQuery + "&oq=" + encodedSearchQuery + "&safe=active&adtest=on&btnG=Search&cr=countryUS";
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("UnsupportedEncodingException on converting search query into encoding format:", e);
        }
        HttpMethod httpMethod = null;
        if (encodedSearchQuery != null) {
            httpMethod = proxyServerConnection.getResponseFromAPIUsingProxyServer(encodedSearchQuery, proxyHostNo, "get");
        }
        if (httpMethod != null && httpMethod.getStatusCode() == HttpStatus.SC_OK) {
            InputStream reposne = null;
            try {
                reposne = httpMethod.getResponseBodyAsStream();

            } catch (IOException ex) {
                LOGGER.error("Exception on HttpMethod:" + ex);
            }

            if (reposne != null) {
                String text = null;
                try {
                    text = IOUtils.toString(reposne, StandardCharsets.UTF_8.name());
                } catch (IOException ex) {
                    LOGGER.debug("IOException converting response to string: ", ex);
                }
                try {
                    LOGGER.debug("ResponseBody for: " + searchQuery + "\n *************************" + httpMethod.getURI());
                } catch (URIException ex) {
                    java.util.logging.Logger.getLogger(AdSpyThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                Document document = Jsoup.parse(text);
                Element adSpyClass = document.select("div.ads-visurl").first();
                if (adSpyClass != null) {
                    String ad = adSpyClass.getElementsByClass("Z98Wse").first().text();
                    String site = adSpyClass.getElementsByClass("UdQCqe").first().text();
                    //Check if the site URL matches the Search Query Term. 
                    //The ad may be of different company. So it is important to check if the URL contains the keyword
                    //Also this Search Query Term should not be part of sub folder in the URL.
                    //It has to be part of main domain URL
                    if (site != null && site.trim().length() > 0) {
                        String domain = getDomainNameFromSite(site.trim());
                        //First step removing special characters if any in the search query
                        //Looking for only & : and ;. Hyphen (-) is left intentionally
                        String searchQueryClean = searchQuery.replaceAll(":", "");
                        searchQueryClean = searchQueryClean.replaceAll("&", "");
                        searchQueryClean = searchQueryClean.replaceAll(";", "");
                        StringTokenizer parseSearchQuery = new StringTokenizer(searchQueryClean, " ");
                        String keywordToCheck = parseSearchQuery.nextToken().toLowerCase();
                        if (domain.contains(keywordToCheck)) {
                            adSpyObj.setAddPresence(true);
                            adSpyObj.setUrl(site);
                        }
                    }
                }
            }
        }
        synchronized (keywordsAdsList) {
            keywordsAdsList.add(adSpyObj);
        }
    }

    private String getDomainNameFromSite(String site) {
        String domainName = "";
        try {
            if (!site.startsWith("http")) {
                site = "http://".concat(site);
            }
            URL urlObj = new URL(site);
            domainName = urlObj.getHost();
            if (domainName.startsWith("www.")) {
                domainName = domainName.substring(4);
            }
        } catch (MalformedURLException e) {
            LOGGER.error("Malformed exception in url", e);
        }
        return domainName.toLowerCase();
    }

}
