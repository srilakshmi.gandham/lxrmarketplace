/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

/**
 *
 * @author vemanna
 */
@Controller
public class AdSpyController {

    private static final Logger LOGGER = Logger.getLogger(AdSpyController.class);

    @Autowired
    private AdSpyService adSpyService;

    @Autowired
    private String downloadFolder;
    @Autowired
    private String uploadFolder;

    private final String FORM_VIEW = "views/adSpy/adSpy";

    @RequestMapping(value = "/ad-spy.html", method = RequestMethod.GET)
    public String showAdSpy(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("addSpy") AdSpyV1 adSpyObj, ModelMap model, HttpSession session) {
        HashMap<String, String> geoLocation = Common.loadGeoLocations();
        model.addAttribute("geoLocations", geoLocation);
        adSpyObj.setGeoLocation("us");
        session.removeAttribute("companyAdSpyList");
        session.removeAttribute("companyAdSpyListXLSReport");
        return FORM_VIEW;
    }

    @RequestMapping(value = "/ad-spy.html", method = RequestMethod.POST)
    public String processAddSpy(HttpServletRequest request, HttpServletResponse response,
            Model model, HttpSession session, @ModelAttribute("addSpy") AdSpyV1 adSpyObj) {
        String fileName = null;
        HashMap<String, String> geoLocation = Common.loadGeoLocations();
        model.addAttribute("geoLocations", geoLocation);
        session.removeAttribute("companyAdSpyList");
        session.removeAttribute("companyAdSpyListXLSReport");
        if (WebUtils.hasSubmitParameter(request, "request")) {
            List<String> searchQueryList = null;
            List<AdSpyV1> companyAdSpyList = null;
            if (adSpyObj.getKeywordsFile().getOriginalFilename() != null && !adSpyObj.getKeywordsFile().getOriginalFilename().equals("")) {
                model.addAttribute("fileName",adSpyObj.getKeywordsFile().getOriginalFilename());
                fileName = Common.saveFile(adSpyObj.getKeywordsFile(), uploadFolder);
                searchQueryList = adSpyService.readKeywordsFromFile(uploadFolder.concat(fileName));
            } else if (adSpyObj.getQueryKeywords() != null && !adSpyObj.getQueryKeywords().toString().equals("")) {
                searchQueryList = Common.convertStringToArrayList(adSpyObj.getQueryKeywords().toString(), "\n");
            }
            if (searchQueryList != null) {
                companyAdSpyList = adSpyService.findAdForSearchQuery(searchQueryList, adSpyObj.getGeoLocation());
                companyAdSpyList.sort((AdSpyV1 adOne, AdSpyV1 adTwo)->adOne.getCompanyName().compareTo(adTwo.getCompanyName()));
            }
            if (companyAdSpyList == null) {
                companyAdSpyList = new ArrayList<>();
            }
            session.setAttribute("companyAdSpyList", companyAdSpyList);
            if (adSpyObj.getQueryKeywords() != null) {
                adSpyObj.setQueryKeywords(adSpyObj.getQueryKeywords());
            }
            adSpyObj.setGeoLocation(adSpyObj.getGeoLocation());
            adSpyObj.setQueryKeywords(adSpyObj.getQueryKeywords());
            adSpyObj.setKeywordsFile(adSpyObj.getKeywordsFile());
            model.addAttribute("success", true);
            model.addAttribute("companyAdSpyList", companyAdSpyList);
            model.addAttribute("adSpyObj", adSpyObj);
        } else {
            adSpyObj.setGeoLocation("us");
            model.addAttribute("adSpyObj", adSpyObj);
        }

        return FORM_VIEW;
    }

    @RequestMapping(value = "/ad-spy.html", params = {"download=download"}, method = RequestMethod.POST)
    public void generatingAdSpyExcelReport(HttpServletRequest request, HttpServletResponse response,HttpSession session) {
        try {
            String fileName = null;
            if (session.getAttribute("companyAdSpyList") != null) {
                List<AdSpyV1> companyAdSpyList = (List<AdSpyV1>) session.getAttribute("companyAdSpyList");
                fileName = adSpyService.generateXLSFileofAdSpy(companyAdSpyList, downloadFolder);
                session.setAttribute("companyAdSpyListXLSReport", fileName);
            }
            LOGGER.info("Downloadble Ad Spy report: " + fileName);
            if (fileName != null && !fileName.trim().equals("")) {
                Common.downloadReport(response, downloadFolder, fileName, "xls");
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Download reports request: ", e);
        }
    }

    /*
     * Preparing Ad Spy report to send mail to user.
     */
    @RequestMapping(value = "/ad-spy.html", params = {"download=sendmail"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] sendMailAdSpyExcelReport(HttpServletRequest request, HttpServletResponse response,HttpSession session,
            @RequestParam("email") String userEmail) {
        boolean mailStatus = false;
        Object[] responseData = new Object[1];
        try {
            String fileName = null;
            if (session.getAttribute("companyAdSpyListXLSReport") != null) {
                fileName = (String) session.getAttribute("companyAdSpyListXLSReport");
            } else if (session.getAttribute("companyAdSpyList") != null) {
                List<AdSpyV1> companyAdSpyList = (List<AdSpyV1>) session.getAttribute("companyAdSpyList");
                fileName = adSpyService.generateXLSFileofAdSpy(companyAdSpyList, downloadFolder);
                session.setAttribute("companyAdSpyListXLSReport", fileName);
            }
            if (fileName != null) {
                LOGGER.info("Sending report through mail for user");
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, fileName, userEmail, "Ad Spy");
                if (!mailStatus) {
                    LOGGER.info("Sending report through mail for user email: " + userEmail);
                }
            }
            responseData[0] = mailStatus;
        } catch (Exception e) {
            LOGGER.error("Exception:Send report to email request: ", e);
        }
        return responseData;
    }
}
