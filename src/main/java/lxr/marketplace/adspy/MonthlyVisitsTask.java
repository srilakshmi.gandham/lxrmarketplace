/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.apiaccess.SimilarWebAPIService;

/**
 *
 * @author NE16T1213
 */
public class MonthlyVisitsTask implements Callable<Map<String, Double>> {
    private final String domain;
    private final String startDate;
    private final String endDate;
    private final SimilarWebAPIService similarWebAPIService;
    private CountDownLatch latchObj;
    private final int proxyHostNo;

    public MonthlyVisitsTask(String domain, String startDate, String endDate, SimilarWebAPIService similarWebAPIService, CountDownLatch latchObj, int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Map<String, Double> call() {
        Map<String, Double> monthlyVisits = null;
        monthlyVisits = similarWebAPIService.getMonthlyVisits(this.domain, this.startDate, this.endDate, this.proxyHostNo);
        latchObj.countDown();
        return monthlyVisits;
    }
}
