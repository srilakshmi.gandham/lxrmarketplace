/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author vidyasagar.korada
 */
public class AdSpyModel {
    private Set<String> selectedMetrics;
    private Map<String, String> dateRange;
    private List<AdSpy> adSpyResult;
    private List<String> queryDomains;

    public List<String> getQueryDomains() {
        return queryDomains;
    }

    public void setQueryDomains(List<String> queryDomains) {
        this.queryDomains = queryDomains;
    }

    public Map<String, String> getDateRange() {
        return dateRange;
    }

    public void setDateRange(Map<String, String> dateRange) {
        this.dateRange = dateRange;
    }

    public List<AdSpy> getAdSpyResult() {
        return adSpyResult;
    }

    public void setAdSpyResult(List<AdSpy> adSpyResult) {
        this.adSpyResult = adSpyResult;
    }

    public Set<String> getSelectedMetrics() {
        return selectedMetrics;
    }

    public void setSelectedMetrics(Set<String> selectedMetrics) {
        this.selectedMetrics = selectedMetrics;
    }
}
