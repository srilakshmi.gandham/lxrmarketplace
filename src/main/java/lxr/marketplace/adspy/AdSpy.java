/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.io.Serializable;
/**
 *
 * @author sagar
 */
public class AdSpy implements Serializable {
    /*Result fields*/
    private String companyName;
    /*AdSpy V1*/
    private String url;
    /*AdSpy V2*/
    private boolean addPresence;
    /*Traffic*/
    private double trafficTotalVisits;
    private double trafficMonthlyVisits;
    /*Channels Overview*/
    private double paidTraffic;
    private double organicTraffic;
    /*Channel Analysis*/
    private double paidVisits;
    private double organicVisits;

    private double monthlyAdwordsBudget;
    private double approxMonthlyAdwordsBudget;

    private Integer keywordId;

    public AdSpy() {
    }

    public AdSpy(AdSpy cloneObj) {
        this.companyName = cloneObj.companyName;
        this.addPresence = cloneObj.isAddPresence();
        /*Traffic*/
        this.trafficTotalVisits = cloneObj.trafficTotalVisits;
        this.trafficMonthlyVisits = cloneObj.trafficMonthlyVisits;
        /*Channels Overview*/
        this.paidTraffic = cloneObj.paidTraffic;
        this.organicTraffic = cloneObj.organicTraffic;
        /*Channel Analysis*/
        this.paidVisits = cloneObj.paidVisits;
        this.organicVisits = cloneObj.organicVisits;

        this.monthlyAdwordsBudget = cloneObj.monthlyAdwordsBudget;
        this.approxMonthlyAdwordsBudget = cloneObj.approxMonthlyAdwordsBudget;
        this.keywordId = cloneObj.getKeywordId();
    }
    
    public AdSpy(AdSpy cloneObj,int duplicateKeywordId) {
        this.companyName = cloneObj.companyName;
        this.addPresence = cloneObj.isAddPresence();
        /*Traffic*/
        this.trafficTotalVisits = cloneObj.trafficTotalVisits;
        this.trafficMonthlyVisits = cloneObj.trafficMonthlyVisits;
        /*Channels Overview*/
        this.paidTraffic = cloneObj.paidTraffic;
        this.organicTraffic = cloneObj.organicTraffic;
        /*Channel Analysis*/
        this.paidVisits = cloneObj.paidVisits;
        this.organicVisits = cloneObj.organicVisits;

        this.monthlyAdwordsBudget = cloneObj.monthlyAdwordsBudget;
        this.approxMonthlyAdwordsBudget = cloneObj.approxMonthlyAdwordsBudget;
        this.keywordId = duplicateKeywordId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAddPresence() {
        return addPresence;
    }

    public void setAddPresence(boolean addPresence) {
        this.addPresence = addPresence;
    }

   public double getTrafficTotalVisits() {
        return trafficTotalVisits;
    }

    public void setTrafficTotalVisits(double trafficTotalVisits) {
        this.trafficTotalVisits = trafficTotalVisits;
    }

    public double getTrafficMonthlyVisits() {
        return trafficMonthlyVisits;
    }

    public void setTrafficMonthlyVisits(double trafficMonthlyVisits) {
        this.trafficMonthlyVisits = trafficMonthlyVisits;
    }

    public double getPaidTraffic() {
        return paidTraffic;
    }

    public void setPaidTraffic(double paidTraffic) {
        this.paidTraffic = paidTraffic;
    }

    public double getOrganicTraffic() {
        return organicTraffic;
    }

    public void setOrganicTraffic(double organicTraffic) {
        this.organicTraffic = organicTraffic;
    }

    public double getPaidVisits() {
        return paidVisits;
    }

    public void setPaidVisits(double paidVisits) {
        this.paidVisits = paidVisits;
    }

    public double getOrganicVisits() {
        return organicVisits;
    }

    public void setOrganicVisits(double organicVisits) {
        this.organicVisits = organicVisits;
    }

    public double getMonthlyAdwordsBudget() {
        return monthlyAdwordsBudget;
    }

    public void setMonthlyAdwordsBudget(double monthlyAdwordsBudget) {
        this.monthlyAdwordsBudget = monthlyAdwordsBudget;
    }

    public double getApproxMonthlyAdwordsBudget() {
        return approxMonthlyAdwordsBudget;
    }

    public void setApproxMonthlyAdwordsBudget(double approxMonthlyAdwordsBudget) {
        this.approxMonthlyAdwordsBudget = approxMonthlyAdwordsBudget;
    }

    public Integer getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Integer keywordId) {
        this.keywordId = keywordId;
    }

}
