/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.adspy;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.apiaccess.SimilarWebAPIService;

/**
 *
 * @author NE16T1213
 */
public class ChannelsAnalysisTask implements Callable<Map<String, Long>> {

    private final String domain;
    private final String startDate;
    private final String endDate;
    private final SimilarWebAPIService similarWebAPIService;
    private CountDownLatch latchObj;
    private final int proxyHostNo;

    public ChannelsAnalysisTask(String domain, String startDate, String endDate, SimilarWebAPIService similarWebAPIService, CountDownLatch latchObj,int proxyHostNo) {
        this.domain = domain;
        this.startDate = startDate;
        this.endDate = endDate;
        this.similarWebAPIService = similarWebAPIService;
        this.latchObj = latchObj;
        this.proxyHostNo = proxyHostNo;
    }

    @Override
    public Map<String, Long> call() {
        /*Pull data from API for*/
        Map<String, Long> channelsAnalysisMap = null;
        channelsAnalysisMap = similarWebAPIService.getChannelsAnalysisMetrics(this.domain, this.startDate, this.endDate,this.proxyHostNo);
        latchObj.countDown();
        return channelsAnalysisMap;
    }
}
