/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.session.Session;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author netelixir
 */
@Controller
public class AmazonProductListingController {

    private static final Logger logger = Logger.getLogger(AmazonProductListingController.class);

    @Autowired
    AmazonProductListingService amazonProductListingService;

    @Autowired
    AmazonProductListingRestService amazonProductListingRestService;

    @Autowired
    private String downloadFolder;

    @RequestMapping(value = "/product-listing.html")
    public String tool(HttpSession session) {
        session.removeAttribute("amazonProductListingObjects");
        return "views/productListingGrader/amazonProductListing";
    }

    @RequestMapping(value = "/product-listing.html", params = {"backToSuccessPage"})
    public String backToSuccessPage(HttpSession session) {
//        session.removeAttribute("amazonProductListingObjects");
        return "views/productListingGrader/amazonProductListing";
    }

    @ResponseBody
    @RequestMapping(value = "/product-listing.html",params = {"request=URLS"}, method = RequestMethod.POST)
    public Object[] amazonProductListingGrader(HttpSession session, @RequestParam("manuallyURLS") String queryURL) {
        Object amazonProductListingList[] = null;
        List<String> errorUrlsList = null;
        List<String> urlList = null;
        Object finalAmazonProductListingList[] = new Object[4];
        amazonProductListingList = amazonProductListingService.getAmazonData(queryURL);
        if (amazonProductListingList != null && amazonProductListingList.length > 0) {
//            amazonProductListingList = new Object[2];
            finalAmazonProductListingList[0] = amazonProductListingList[0];
            finalAmazonProductListingList[1] = amazonProductListingList[1];
            finalAmazonProductListingList[3] = amazonProductListingList[2];
            errorUrlsList = (List<String>) amazonProductListingList[1];
            urlList = (List<String>) amazonProductListingList[0];
            AmazonProductsAnalysisType amazonProductsAnalysisType = new AmazonProductsAnalysisType();
            Session userSession = (Session) session.getAttribute("userSession");
            userSession.addToolUsage(41);
            amazonProductsAnalysisType.setManuallyURLS(queryURL);
            amazonProductsAnalysisType.setPreUpload(false);
            finalAmazonProductListingList[2] = amazonProductsAnalysisType;

        }
        if ( urlList.size() > 0) {
            if (errorUrlsList.size() != amazonProductListingService.convertStringToArrayList(queryURL).size()) {
                session.setAttribute("amazonProductListingObjects", finalAmazonProductListingList);
            }
        } else {
            session.removeAttribute("amazonProductListingObjects");
            logger.info("no object for urls upload");
        }

        return finalAmazonProductListingList;

    }

    @ResponseBody
//    @Produces({MediaType.APPLICATION_JSON})
    @RequestMapping(value = "/product-listing.html",params = {"request=fileUpload"}, method = RequestMethod.POST)
    public Object[] amazonProductListingGrader1(HttpSession session, @RequestParam("file") MultipartFile file) {

        Object amazonFileUploadObject[] = null;
        List<String> fileProductList = null;
        Object finalAmazonFileUploadObject[] = new Object[3];
        AmazonProductsAnalysisType amazonProductsAnalysisType = new AmazonProductsAnalysisType();
        amazonFileUploadObject = amazonProductListingService.getUploadedFileAmazonData(file);
        fileProductList = (List<String>) amazonFileUploadObject[0];
        if (amazonFileUploadObject!= null && fileProductList.size() > 0) {
            finalAmazonFileUploadObject[0]= amazonFileUploadObject[0];
            finalAmazonFileUploadObject[1]= amazonFileUploadObject[1];
            Session userSession = (Session) session.getAttribute("userSession");
            userSession.addToolUsage(41);
            File file1 = convert(file);
            amazonProductsAnalysisType.setFileUpload(file1.getName());
            amazonProductsAnalysisType.setPreUpload(true);
            finalAmazonFileUploadObject[2]= amazonProductsAnalysisType;
            session.setAttribute("amazonProductListingObjects", finalAmazonFileUploadObject);
        } else {
            session.removeAttribute("amazonProductListingObjects");

            logger.info("no object for file upload");
        }

        return finalAmazonFileUploadObject;

    }

    /*Save the query in lxrm website tracking*/
    @RequestMapping(value = "/product-listing.html", params = {"ate=ate"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(HttpServletResponse response, @RequestParam("domain") String domain) {
        Object ateResponseObject[] = new Object[1];
        String toolIssues = null;
        String questionInfo = null;
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain);
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.AMAZON_PRODUCT_LISTING_GRADER);
        ateResponseObject[0] = lxrmAskExpert;
        return ateResponseObject;
    }

    @RequestMapping(value = "/product-listing.html", params = {"backToSuccessPage"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] backToSuccess(HttpSession session, Model model) {
        Object amazonProductListingObject[] = new Object[3];
        if (session.getAttribute("amazonProductListingObjects") != null) {
            amazonProductListingObject = (Object[]) session.getAttribute("amazonProductListingObjects");
//            session.getAttribute("url");
        }
        return amazonProductListingObject;
    }

    @RequestMapping(value = "/download-sample-file.html")
    public String downloadFile(HttpServletResponse response) {
        String dwnfileName = "LXRM_sample-template-for-footwear.xlsx";
        Common.downloadReport(response, downloadFolder, dwnfileName, "xlsx");

        return null;
    }

    public File convert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
        } catch (IOException e) {
            logger.error("erro in converting multipartFile to file",e);
        }
        return convFile;
    }
}