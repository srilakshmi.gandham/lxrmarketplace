/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.select.Elements;
//import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
//import org.springframework.ui.Model;
//import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
class AmazonProductListingRestService {

//    @Autowired
//    AmazonProductListingThread amazonProductListingThread;
//    public static void main(String args[]) {
//        AmazonProductListingService amazonProductListingService = new AmazonProductListingService();
//        amazonProductListingService.amazonProductListingGrader("https://www.amazon.in/Nokia-105-Black/dp/B0745BNFYV/ref=sr_1_1?s=electronics&rps=1&ie=UTF8&qid=1535103997&sr=1-1&refinements=p_98%3A10440597031%2Cp_36%3A-500000");
//    }
//    private final static int poolSize = 5;
//    private final static int maxPoolSize = 10;
//    private final static long keepAliveTime = 5000;
//public static ovid main(String args[]){
//        String docHtml = convertfileToString();
//        Document urlDoc = Jsoup.parse(docHtml);
    private static final Logger logger = Logger.getLogger(AmazonProductListingRestService.class);
    
    public List<AmazonProductListing> amazonProductListingGrader(List<String> finalUrlList) {
        List<AmazonProductListing> amazonProductListingList = new ArrayList<>();
        try {
            ExecutorService service = Executors.newFixedThreadPool(finalUrlList.size());
            CountDownLatch latchObj = new CountDownLatch(finalUrlList.size());
            Future<AmazonProductListing> amazonProductListingThreadObj;
            logger.info("started the thread to get the list of objects fro amazon urls");
            for (int i = 0; i < finalUrlList.size(); i++) {
                amazonProductListingThreadObj = service.submit(new AmazonProductListingThread(finalUrlList.get(i), latchObj, this));
                if (amazonProductListingThreadObj != null) {
                    amazonProductListingList.add(amazonProductListingThreadObj.get());
                }
            }
            latchObj.await();
            service.shutdown();
            
        } catch (InterruptedException e) {
            logger.error("InterruptedException in creating amazonProduct Listing Thread thread: ", e);
        } catch (ExecutionException ex) {
            logger.error("Exception occured when executing a task using service.submit()", ex);
        }
        logger.info("All the product urls info list is collected using latchObj and threads");
        return amazonProductListingList;
    }
    
    public Object[] getFileList(File file) {
        Object amazonFileUploadObject[] = new Object[2];
        List<AmazonProductListing> amazonProductListingList = new ArrayList<>();
        Map< String, Integer> al = new HashMap<>();
        Map<String, Integer> repeatedSkuIDs = new HashMap<>();
        Map<String, Integer> SkuIDsCount = new HashMap();
        AmazonProductListing amazonProductListing;
        try {
            FileInputStream inputStream = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            logger.info("no.of sheets in the uploaded sheet" +workbook.getNumberOfSheets());
            XSSFSheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();
            while (iterator.hasNext()) {
                amazonProductListing = new AmazonProductListing();
                int imageCount = 0;
                int featuresCount = 0;
                Row nextRow = iterator.next();
                // This whole row is empty
                // Handle it as needed
                //first two rows are skipped as per template
                if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
                    continue; //just skip the rows 
                }
                if (nextRow.getRowNum() >= 3 && al.isEmpty()) {
                    break;
                }
//                if (nextRow.getRowNum() >= 3  && (al.containsKey("item_sku") || al.containsKey("sku")) && !(al.containsKey("title") || al.containsKey("item_name")) && !(al.containsKey("description") || al.containsKey("product_description")) && !(al.containsKey("main-image-url") || al.containsKey("main_image_url"))&& !(al.containsKey("bullet-point1") || al.containsKey("bullet_point1"))) {
//                    amazonProductListing.setErrorRow(nextRow.getRowNum());
//                    amazonProductListingList.add(amazonProductListing);
//                    break;
//                }
                boolean isEmptyRow = false;
                if (nextRow.getRowNum() >= 3 && nextRow.getCell(1) != null && !nextRow.getCell(1).getStringCellValue().equals("")) {
                    String cellValue = nextRow.getCell(1).getStringCellValue();
                    if (SkuIDsCount.containsKey(cellValue)) {
                        SkuIDsCount.put(cellValue, SkuIDsCount.get(cellValue) + 1);
                        isEmptyRow = true;
                        continue;
                    } else {
                        SkuIDsCount.put(cellValue, 1);
                    }
                }
                Iterator<Cell> cellIterator = nextRow.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String colVal = cell.getStringCellValue();
//                    logger.info(al);
                    if (nextRow.getRowNum() == 2) {
                        if ("product_description".equals(colVal) || "title".equals(colVal) || "description".equals(colVal) || "sku".equals(colVal) || "item_sku".equals(colVal) || "item_name".equals(colVal)) {
                            int index = cell.getColumnIndex();
                            al.put(cell.getStringCellValue(), index);
                        }
                        if (colVal.contains("bullet") || colVal.contains("image")) {
                            int index = cell.getColumnIndex();
                            al.put(cell.getStringCellValue(), index);
                        }
                        continue;
                    }
                    
                    for (Map.Entry<String, Integer> entry : al.entrySet()) {
                        if (entry.getValue() == cell.getColumnIndex() && ("item_sku".equals(entry.getKey()) || "sku".equals(entry.getKey())) && cell.getStringCellValue().isEmpty()) {
                            isEmptyRow = true;
                            break;
                        }
                        if (entry.getValue() == cell.getColumnIndex() && cell.getStringCellValue() != null && !cell.getStringCellValue().isEmpty()) {
                            
                            if ("product_description".equals(entry.getKey()) || "description".equals(entry.getKey())) {
                                amazonProductListing.setDescriptionScore(DescriptionScore(cell.getStringCellValue().length()));
                                amazonProductListing.setDescription(cell.getStringCellValue());
                            } else if ("title".equals(entry.getKey()) || "item_name".equals(entry.getKey())) {
                                amazonProductListing.setTitleScore(titleScore(cell.getStringCellValue().length()));
                                amazonProductListing.setTitle(cell.getStringCellValue());
                            } else if (entry.getKey().contains("image")) {
                                imageCount++;
                            } else if (entry.getKey().contains("bullet")) {
                                featuresCount++;
                            } else if ("sku".equals(entry.getKey()) || "item_sku".equals(entry.getKey())) {
                                amazonProductListing.setSkuId(cell.getStringCellValue());
                            }
                        }
                    }
                    if (isEmptyRow) {
                        break;
                    }
                    
                }
                if (!isEmptyRow && !al.isEmpty() && nextRow.getRowNum() != 2) {
                    amazonProductListing.setImagesCount(imageCount);
                    amazonProductListing.setImageCountScore(imagesScore(imageCount));
                    amazonProductListing.setFeaturesScore(featuresScore(featuresCount));
                    amazonProductListing.setFeatures(featuresCount);
                    int avg = (int) Math.round((0.2 * (amazonProductListing.getTitleScore())) + (0.1 * (amazonProductListing.getDescriptionScore())) + (0.3 * (amazonProductListing.getFeaturesScore())) + (0.4 * (amazonProductListing.getImageCountScore())));
                    amazonProductListing.setTotalScore(avg);
                    
                    amazonProductListingList.add(amazonProductListing);
                }
            }
            repeatedSkuIDs = SkuIDsCount.entrySet().stream()
                    .filter(Map -> Map.getValue() >= 2)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
//            logger.info(repeatedSkuIDs);
            workbook.close();
            inputStream.close();
        } catch (IOException e) {
            logger.error("The xlsx file cannot be read", e);
        }
        amazonFileUploadObject[0] = amazonProductListingList;
        amazonFileUploadObject[1] = repeatedSkuIDs;
//        logger.info(amazonProductListingList);
        return amazonFileUploadObject;
        
    }
    
    public File convert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
            logger.error("The multipartFile cannot be converted into file", e);
        }
        return convFile;
    }
    
    public Object[] reviewAndRatings(Elements reviewRatings) {
        Object[] reviewRatingsObj = new Object[3];
        final String[] split = reviewRatings.first().text().split(" ");
        for (String reviewRatingOutput : split) {
            if (isInteger(reviewRatingOutput.replaceAll(",", "")) || isFloat(reviewRatingOutput.replaceAll(",", "."))) {
                if (reviewRatingsObj[0] != null) {
                    if (reviewRatingsObj[0] != null && reviewRatingsObj[1] != null) {
                        reviewRatingsObj[2] = Integer.parseInt(reviewRatingOutput.replaceAll(",", ""));
                    } else {
                        reviewRatingsObj[1] = Integer.parseInt(reviewRatingOutput.replaceAll(",", ""));
                    }
                } else {
                    reviewRatingsObj[0] = Float.parseFloat(reviewRatingOutput.replaceAll(",", "."));
                }
            }
        }
        return reviewRatingsObj;
    }
    
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
    
    private static boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
        } catch (NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

//    public List getAllSkuIds(File file) {
//        FileInputStream inputStream;
//        List skuIds = new ArrayList<>();
//        try {
//            inputStream = new FileInputStream(file);
//
//            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
//            logger.debug(workbook.getNumberOfSheets());
//            XSSFSheet firstSheet = workbook.getSheetAt(0);
//            Iterator<Row> iterator = firstSheet.iterator();
//            while (iterator.hasNext()) {
//                Row nextRow = iterator.next();
//                if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1 || nextRow.getRowNum() == 2) {
//                    continue; //just skip the rows 
//                }
//                Iterator<Cell> cellIterator = nextRow.cellIterator();
//                while (cellIterator.hasNext()) {
//                    Cell cell = cellIterator.next();
//                    if (cell.getColumnIndex() == 1 && !"".equals(cell.getStringCellValue())) {
//                        skuIds.add(cell.getStringCellValue());
//
//                    }
//                }
//            }
//            workbook.close();
//            inputStream.close();
//        } catch (IOException ex) {
//            java.util.logging.Logger.getLogger(AmazonProductListingRestService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return skuIds;
//    }
    int titleScore(int titleLength) {
        int score = 0;
        if (titleLength >= 190 && titleLength <= 210) {
            score = 100;
        } else if (titleLength >= 0 && titleLength <= 129) {
            score = (int) Math.round(0.114082 * (Math.pow(titleLength, 1.24)));
        } else if (titleLength >= 130 && titleLength <= 270) {
            score = (int) Math.round(-0.010006 * (Math.pow(titleLength, 2)) + (3.96329 * titleLength) - 292.5);
        } else if (titleLength > 270) {
            score = 50;
        }
        return score;
    }
    
    int DescriptionScore(int desLength) {
        int desScore = 0;
        if (desLength >= 1900 && desLength <= 2100) {
            desScore = 100;
        } else if (desLength >= 0 && desLength <= 1299) {
            desScore = (int) Math.round(1.078372 + 0.00976169 * desLength + 0.0000155635 * (Math.pow(desLength, 2)));
        } else if (desLength >= 1300 && desLength <= 2700) {
            desScore = (int) Math.round(-0.000118162 * (Math.pow(desLength, 2)) + 0.47168 * desLength - 371.446);
        } else if (desLength > 2700) {
            desScore = 40;
        }
        return desScore;
    }
    
    int featuresScore(int features) {
        int featuresScore = 0;
        
        if (features == 5 || features > 5) {
            featuresScore = 100;
        } else if (features == 4) {
            featuresScore = 75;
        } else if (features == 3) {
            featuresScore = 50;
        } else if (features == 2) {
            featuresScore = 25;
        } else if (features == 1) {
            featuresScore = 10;
        } else {
            featuresScore = 0;
        }
        return featuresScore;
    }
    
    int imagesScore(int imagesCount) {
        int imgScore = 0;
        if (imagesCount > 8) {
            imgScore = 100;
        } else if (imagesCount == 8) {
            imgScore = 90;
        } else if (imagesCount == 7) {
            imgScore = 80;
        } else if (imagesCount == 6) {
            imgScore = 70;
        } else if (imagesCount == 5) {
            imgScore = 60;
        } else if (imagesCount == 4) {
            imgScore = 50;
        } else if (imagesCount == 3) {
            imgScore = 30;
        } else if (imagesCount == 2) {
            imgScore = 20;
        } else if (imagesCount == 1) {
            imgScore = 10;
        }
        return imgScore;
    }
    
}