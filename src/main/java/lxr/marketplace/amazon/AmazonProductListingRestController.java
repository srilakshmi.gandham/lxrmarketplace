/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author netelixir
 */
@Controller
public class AmazonProductListingRestController {

    private static final Logger logger = Logger.getLogger(AmazonProductListingRestController.class);
    @Autowired
    private AmazonProductListingRestService amazonProductListingRestService;

    @ResponseBody
    @RequestMapping(value = "/productinfo-for-urlList", method = RequestMethod.POST)
    public Object[] amazonProductListingGrader(@RequestParam("manualUrls") List<String> finalUrlList) {
        Object amazonUrlObject[] = new Object[3];
        List<String> nonRedundantList = finalUrlList.stream().distinct().collect(Collectors.toList());
        List<AmazonProductListing> amazonProductListingList = amazonProductListingRestService.amazonProductListingGrader(nonRedundantList);
        if (amazonProductListingList != null) {
            /**
             * Object filtering is done to get the URL products which are not error URLS.
             */
            amazonUrlObject[0] = amazonProductListingList.stream()
                    .filter(u -> u.getErrorManualUrls() == null)
                    .collect(Collectors.toList());

            /**
             *Object filtering is done to get the URL products which are  error URLS
             */
            amazonUrlObject[1] = amazonProductListingList.stream()
                    .filter(u -> u.getErrorManualUrls() != null)
                    .map(AmazonProductListing::getErrorManualUrls)
                    .collect(Collectors.toList());

            /**
             * To get the number of repeated URLS in list of URLS.
             */
            Map<String, Long> numberOfRepeatedUrls = finalUrlList.stream()
                    .filter(i -> Collections.frequency(finalUrlList, i) > 1)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            amazonUrlObject[2] = numberOfRepeatedUrls;
        }

        return amazonUrlObject;
    }

    @RequestMapping(value = "/productinfo-for-fileUploading.html", method = RequestMethod.POST)
    @ResponseBody
    public Object[] getFile(@RequestParam("file") MultipartFile file) {
        logger.info("rest controller process");
        return amazonProductListingRestService.getFileList(amazonProductListingRestService.convert(file));
    }
}