/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

/**
 *
 * @author netelixir
 */
//@JsonAutoDetect
public class AmazonProductsAnalysisType {

    private boolean preUpload;
    private String manuallyURLS;
    private String fileUpload;

    public boolean isPreUpload() {
        return preUpload;
    }

    public void setPreUpload(boolean preUpload) {
        this.preUpload = preUpload;
    }

    public String getManuallyURLS() {
        return manuallyURLS;
    }

    public void setManuallyURLS(String manuallyURLS) {
        this.manuallyURLS = manuallyURLS;
    }

//    @JsonProperty
    public String getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(String fileUpload) {
        this.fileUpload = fileUpload;
    }

}
