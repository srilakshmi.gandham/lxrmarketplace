/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

/**
 *
 * @author netelixir
 */
public class AmazonProductListing {
//    private int titleLength;
//    private int descriptionLength;
//    private int featuresLength;

    private int imagesCount;
    private int videoCount;
    private int reviews;
    private float rating;
    private String url;
    private StringBuffer manualUlrs;
    private String title;
    private String description;
    private int features;
    private int statusCode;
    private int titleScore;
    private int featuresScore;
    private int descriptionScore;
    private int imageCountScore;
    private int ratingScore;
    private int reviewsScore;
    private int totalScore;
    private String skuId;
    private String errorManualUrls;
    private int errorRow;

    public int getErrorRow() {
        return errorRow;
    }

    public void setErrorRow(int errorRow) {
        this.errorRow = errorRow;
    }

    public String getErrorManualUrls() {
        return errorManualUrls;
    }

    public void setErrorManualUrls(String errorManualUrls) {
        this.errorManualUrls = errorManualUrls;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public StringBuffer getManualUlrs() {
        return manualUlrs;
    }

    public void setManualUlrs(StringBuffer manualUlrs) {
        this.manualUlrs = manualUlrs;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }

    public int getTitleScore() {
        return titleScore;
    }

    public void setTitleScore(int titleScore) {
        this.titleScore = titleScore;
    }

    public int getFeaturesScore() {
        return featuresScore;
    }

    public void setFeaturesScore(int featuresScore) {
        this.featuresScore = featuresScore;
    }

    public int getDescriptionScore() {
        return descriptionScore;
    }

    public void setDescriptionScore(int descriptionScore) {
        this.descriptionScore = descriptionScore;
    }

    public int getImageCountScore() {
        return imageCountScore;
    }

    public void setImageCountScore(int imageCountScore) {
        this.imageCountScore = imageCountScore;
    }

    public int getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(int ratingScore) {
        this.ratingScore = ratingScore;
    }

    public int getReviewsScore() {
        return reviewsScore;
    }

    public void setReviewsScore(int reviewsScore) {
        this.reviewsScore = reviewsScore;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getTitle() {
        return title;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFeatures() {
        return features;
    }

    public void setFeatures(int features) {
        this.features = features;
    }

    public int getImagesCount() {
        return imagesCount;
    }

    public void setImagesCount(int imagesCount) {
        this.imagesCount = imagesCount;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
