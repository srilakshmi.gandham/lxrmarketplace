/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author netelixir
 */
public class AmazonProductListingThread implements Callable<AmazonProductListing> {

    private static Logger logger = Logger.getLogger(AmazonProductListingThread.class);
    String url = null;
    private CountDownLatch latchObj;
    private final AmazonProductListingRestService amazonProductListingRestService;

    public AmazonProductListingThread(String url, CountDownLatch latchObj, AmazonProductListingRestService amazonProductListingRestService) {
        this.url = url;
        this.latchObj = latchObj;
        this.amazonProductListingRestService = amazonProductListingRestService;
    }

    @Override
    public AmazonProductListing call() throws Exception {
        AmazonProductListing amazonProductListing = null;
        int featureLength = 0;
        int imagesCount = 0;
        int videoCount = 0;
        int count = 0;

        int review = 0;
        String title = "";
        float ratings = 0.0f;
        String description = "";
        boolean x = false;

        Object amazonUrlObject[] = urlDocument(url);
        if (amazonUrlObject != null) {
            amazonProductListing = new AmazonProductListing();
            Document urlDoc = (Document) amazonUrlObject[0];
            if (amazonUrlObject[1] != null) {
                amazonProductListing.setErrorManualUrls((String) amazonUrlObject[1]);
            } else if (urlDoc != null) {
//                FileUtils.writeStringToFile(new File("/home/netelixir/Desktop/resize/test.txt"), urlDoc.body().html());
                try {
                    amazonProductListing.setUrl(url);
                    Elements metaTitle = urlDoc.select("head title");
                    if (metaTitle != null && metaTitle.first() != null) {
                        title = metaTitle.first().text().trim();
                        amazonProductListing.setTitle(title);
                    }
                    if (metaTitle == null || metaTitle.first() == null) {
                        metaTitle = urlDoc.select("meta[name=title]");
                        if (metaTitle != null && metaTitle.size() > 0 && metaTitle.first() != null && metaTitle.first().attr("content") != null) {
                            title = metaTitle.first().attr("content").trim();
                            amazonProductListing.setTitle(title);
                        }
                    }

                    amazonProductListing.setTitleScore(amazonProductListingRestService.titleScore(title.length()));
//            amazonProductListing1.add(amazonProductListing);

                    Elements metaDescription = urlDoc.select("meta[name=description]");
                    if (metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null) {
                        metaDescription = urlDoc.select("meta[name=Description]");
                    }
                    if (metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null) {
                        metaDescription = urlDoc.select("meta[http-equiv=description]");
                    }
                    if (metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null) {
                        metaDescription = urlDoc.select("meta[http-equiv=Description]");
                    }
                    if (metaDescription.size() != 0 && metaDescription.first() != null && metaDescription.first().attr("content") != null
                            && !metaDescription.first().attr("content").trim().equals("")) {
                        description = metaDescription.first().attr("content").trim();
                        amazonProductListing.setDescription(description);
                        amazonProductListing.setDescriptionScore(amazonProductListingRestService.DescriptionScore(description.length()));
                    } else {
                        logger.error("no description");
                        amazonProductListing.setDescriptionScore(0);
                    }

                    logger.debug(description);
                    Elements features = urlDoc.select("div[id=feature-bullets] > ul li span[class=a-list-item]");
                    if (features.isEmpty()) {
                        features = urlDoc.select("div[id=feature-bullets] ul li span[class=a-list-item]");
                    }
                    if (features != null && features.size() != 0) {
                        featureLength = features.size();
                        if (urlDoc.select("div[id=feature-bullets] ul li[id=replacementPartsFitmentBullet] span[class=a-list-item]").size() > 0) {
                            featureLength = features.size() - urlDoc.select("div[id=feature-bullets] ul li[id=replacementPartsFitmentBullet] span[class=a-list-item]").size();
                        }
                        amazonProductListing.setFeatures(featureLength);

                        amazonProductListing.setFeaturesScore(amazonProductListingRestService.featuresScore(featureLength));
                    } else {
                        amazonProductListing.setFeaturesScore(0);
                    }

                    logger.debug(featureLength);
                    Elements images = urlDoc.select("#altImages .item img");
//                String img = images.attr("src");
                    Elements videos = urlDoc.select(".videoBlockIngress img");
                    if (images == null || images.size() == 0) {
                        images = urlDoc.select("#img-canvas img");

                    }
                    if (images == null || images.size() == 0) {
                        images = urlDoc.select(".a-carousel-row-inner  .js-masrw-show-screenshots");
                        x = true;
                    }

                    if (videos.isEmpty()) {
                        for (Element el : images) {
                            boolean src = el.absUrl("src").contains("png");
                            if (src == true) {
                                count++;
                            }
                        }

                    }
                    if (images.size() > 1 && x == true || images.select(".a-carousel-row-inner  .js-masrw-show-screenshots").size() != 0) {
                        imagesCount = images.size();
                        amazonProductListing.setImagesCount(imagesCount);
                        amazonProductListing.setImageCountScore(amazonProductListingRestService.imagesScore(imagesCount));
                    }
                    if (images.size() == 1) {
                        imagesCount = images.size();
                        amazonProductListing.setImagesCount(imagesCount);
                        amazonProductListing.setImageCountScore(amazonProductListingRestService.imagesScore(imagesCount));
                    }
                    if (videos.size() == 0 && images.size() > 1 && x == false && count == 0) {
                        imagesCount = images.size();
                        if (urlDoc.select("#altImages .item span[id=altIngressSpan]").size() > 0) {

//                     int extraImages =  Character.getNumericValue(urlDoc.select("#altImages .item span[id=altIngressSpan]").first().text().charAt(0));
                            imagesCount = images.size() + Character.getNumericValue(urlDoc.select("#altImages .item span[id=altIngressSpan]").first().text().charAt(0));
                        }
                        amazonProductListing.setImagesCount(imagesCount);
                        amazonProductListing.setImageCountScore(amazonProductListingRestService.imagesScore(imagesCount));

                    }
                    if (images.size() != 0 && (videos.size() != 0 || count != 0) && images.size() > 1) {
                        if (urlDoc.select("#altImages .item span[id=altIngressSpan]").size() > 0 || count != 0 && videos.isEmpty()) {
                            if (urlDoc.select("#altImages .item span[id=altIngressSpan]").size() > 0 && count != 0) {
                                imagesCount = images.size() + Character.getNumericValue(urlDoc.select("#altImages .item span[id=altIngressSpan]").first().text().charAt(0));
                                imagesCount = imagesCount - count;
                                videoCount = count;
                            }
                            if (count != 0 && urlDoc.select("#altImages .item span[id=altIngressSpan]").size() == 0 && videos.isEmpty()) {
                                imagesCount = images.size() - count;
                                videoCount = count;
                            }
                        }
                        if (!videos.isEmpty()) {
                            imagesCount = images.size() - videos.size();
                            videoCount = videos.size();
                        }
                        amazonProductListing.setVideoCount(videoCount);
                        amazonProductListing.setImagesCount(imagesCount);
                        if (imagesCount >= 8 && videoCount != 0) {
                            amazonProductListing.setImageCountScore(100);
                        } else if (imagesCount > 5 && imagesCount <= 7 && videoCount != 0) {
                            amazonProductListing.setImageCountScore(95);
                        } else if (imagesCount >= 4 && imagesCount < 6 && videoCount != 0) {
                            amazonProductListing.setImageCountScore(90);
                        } else if (imagesCount < 4 && videoCount != 0) {
                            amazonProductListing.setImageCountScore(80);
                        }
                    }
//            
                    Elements reviewsAndRating = urlDoc.select("#centerCol #averageCustomerReviews");
                    Elements reviewsAndRating1 = urlDoc.select("#dp-cmps-expand-header-last");
                    Elements reviews = urlDoc.select("#reviewSummary .totalReviewCount");
                    Elements eleRatings = urlDoc.select("#reviewSummary .a-icon-alt");
                    if (reviewsAndRating.size() == 0) {
                        reviewsAndRating = urlDoc.select("#leftCol #averageCustomerReviews");

                    }
                    if (reviewsAndRating.size() == 0) {
                        reviewsAndRating = urlDoc.select("#titleBar-left #averageCustomerReviews");

                    }
                    if (reviews.size() > 0 && eleRatings.size() > 0) {
                        review = Integer.parseInt(reviews.first().text());
                        amazonProductListing.setReviews(review);
                        if (review >= 0 && review <= 300) {
                            amazonProductListing.setReviewsScore((int) Math.round(1.530568 + 0.9328403 * (review) - 0.004671487 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 3))));
                        } else if (review > 300) {
                            amazonProductListing.setReviewsScore(100);
                        }
                        ratings = Float.parseFloat(eleRatings.first().text().split(" ")[0]);
                        amazonProductListing.setRating(ratings);
                        if (ratings >= 0 && ratings <= 4.5) {
                            amazonProductListing.setRatingScore((int) Math.round(0.5121811 + 1.950129 * (ratings) + 4.052737 * (Math.pow(ratings, 2))));

                        } else if (ratings > 4.5) {
                            amazonProductListing.setRatingScore(100);
                        }
                    }
                    if (reviewsAndRating1.size() > 0 && !reviewsAndRating1.first().text().equals("")) {
                        Object[] reviewRatings = new Object[3];
                        reviewRatings = amazonProductListingRestService.reviewAndRatings(reviewsAndRating1);
                        if (reviewRatings[0] != null) {
                            amazonProductListing.setReviews((Integer) reviewRatings[2]);
                            amazonProductListing.setRating((Float) reviewRatings[0]);
                            review = (Integer) reviewRatings[2];
                            ratings = (Float) reviewRatings[0];
                            if (review >= 0 && review <= 300) {
                                amazonProductListing.setReviewsScore((int) Math.round(1.530568 + 0.9328403 * (review) - 0.004671487 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 3))));
                            } else if (review > 300) {
                                amazonProductListing.setReviewsScore(100);
                            }
                            if (ratings >= 0 && ratings <= 4.5) {
                                amazonProductListing.setRatingScore((int) Math.round(0.5121811 + 1.950129 * (ratings) + 4.052737 * (Math.pow(ratings, 2))));

                            } else if (ratings > 4.5) {
                                amazonProductListing.setRatingScore(100);
                            }
                        } else {
                            amazonProductListing.setReviews(0);
                            amazonProductListing.setRating(0);
                            amazonProductListing.setReviewsScore(0);
                            amazonProductListing.setRatingScore(0);
                        }
                    }
                    if (reviewsAndRating.size() > 0 && !reviewsAndRating.first().text().equals("") && reviewsAndRating1.isEmpty()) {
                        if (!"Be".equals(reviewsAndRating.first().text().split(" ")[0])) {
                            Object[] reviewRatings = new Object[3];
                            reviewRatings = amazonProductListingRestService.reviewAndRatings(reviewsAndRating);
//                        ratings = Float.parseFloat(reviewsAndRating.first().text().split(" ")[0]);
//                        review = Integer.parseInt(reviewsAndRating.first().text().split(" ")[5].replaceAll(",", ""));
                            if (reviewRatings[0] != null) {
                                amazonProductListing.setReviews((Integer) reviewRatings[2]);
                                amazonProductListing.setRating((Float) reviewRatings[0]);
                                review = (Integer) reviewRatings[2];
                                ratings = (Float) reviewRatings[0];
                                if (review >= 0 && review <= 300) {
                                    amazonProductListing.setReviewsScore((int) Math.round(1.530568 + 0.9328403 * (review) - 0.004671487 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 2)) + 0.000008899701 * (Math.pow(review, 3))));
                                } else if (review > 300) {
                                    amazonProductListing.setReviewsScore(100);
                                }
                                if (ratings >= 0 && ratings <= 4.5) {
                                    amazonProductListing.setRatingScore((int) Math.round(0.5121811 + 1.950129 * (ratings) + 4.052737 * (Math.pow(ratings, 2))));

                                } else if (ratings > 4.5) {
                                    amazonProductListing.setRatingScore(100);
                                }
                            }
//                            else {
//                            amazonProductListing.setReviews(0);
//                            amazonProductListing.setRating(0);
//                            amazonProductListing.setReviewsScore(0);
//                            amazonProductListing.setRatingScore(0);
//                        }
                        }
                    }
                    if (amazonProductListing.getFeaturesScore() == 0 && amazonProductListing.getImageCountScore() == 0 && amazonProductListing.getRatingScore() == 0 && amazonProductListing.getReviewsScore() == 0) {

                        amazonProductListing.setErrorManualUrls(url);
                    } else {
//                    logger.debug(review);
                        int avg = (int) Math.round((0.2 * (amazonProductListing.getTitleScore())) + (0.05 * (amazonProductListing.getDescriptionScore())) + (0.1 * (amazonProductListing.getFeaturesScore())) + (0.2 * (amazonProductListing.getImageCountScore())) + (0.2 * (amazonProductListing.getRatingScore())) + (0.25 * (amazonProductListing.getReviewsScore())));
                        amazonProductListing.setTotalScore(avg);

                    }
                } catch (Exception e) {
                    amazonProductListing.setErrorManualUrls(url);
                }
            } else {
                logger.info("no content in webpage");
//             return null;
            }
        }

        latchObj.countDown();
        return amazonProductListing;
    }

    Object[] urlDocument(String urlString) {
        boolean urlStatus = false;
        Object amazonUrlObject[] = new Object[2];
        if (urlString.contains("/B0") || urlString.contains("/dp") || urlString.contains("/gp")) {

            Document urlDoc = null;
//        List<String> list =  new ArrayList<>();
//        List<AmazonProductListing> amazonProductListing=null;

            StringBuilder pageHTML = new StringBuilder("");
            String httpDomainUrl = "";
            if (urlString.startsWith("http://") || urlString.startsWith("https://")) {
                httpDomainUrl = urlString;
            } else {
                httpDomainUrl = "http://" + urlString;
            }

            try {
                URL currentAmazonUrlObj = new URL(httpDomainUrl);
                if (currentAmazonUrlObj.getHost().contains("amazon")) {
                    int statusCode = Common.jsoupRelatedcheckCurrentURLAndGetHTML(urlString, pageHTML);
//        int statusCode = Common.checkCurrentURLAndGetHTML(urlString, pageHTML);
                    if (statusCode == 200 && !pageHTML.toString().equals("")) {

                        /*Approach 2: preparing doc by using jsoup*/
                        urlDoc = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
                        amazonUrlObject[0] = urlDoc;
                        urlStatus = true;
                    }
                }
            } catch (MalformedURLException ex) {
                logger.error("ConnectException in getStatusCode when connection to " + urlString + ", Cause: " + ex.getMessage());
            }

        }
        if (!urlStatus) {
            amazonUrlObject[1] = urlString;
            logger.info("status code is not 200");
        }

        return amazonUrlObject;
    }

//    private int titleScore(int titleLength) {
//        int score = 0;
//        if (titleLength >= 0 && titleLength <= 128) {
//            score = (int) Math.round(0.114082 * (Math.pow(titleLength, 1.24)));
//        } else if (titleLength >= 129 && titleLength <= 270) {
//            score = (int) Math.round(-0.010006 * (Math.pow(titleLength, 2)) + (3.96329 * titleLength) - 293);
//        } else if (titleLength > 270) {
//            score = 50;
//        }
//        return score;
//    }
//
//    private int DescriptionScore(int desLength) {
//        int desScore = 0;
//        if (desLength >= 0 && desLength < 1298) {
//            desScore = (int) Math.round(1.078372 + 0.00976169 * desLength + 0.0000155635 * (Math.pow(desLength, 2)));
//        } else if (desLength >= 1299 && desLength <= 2100) {
//            desScore = (int) Math.round(-277.6581 + 0.346731 * desLength - 0.0000793661 * (Math.pow(desLength, 2)));
//        }
//        return desScore;
//    }
//
//    private int featuresScore(int features) {
//        int featuresScore = 0;
//
//        if (features == 5 || features > 5) {
//            featuresScore = 100;
//        } else if (features == 4) {
//            featuresScore = 75;
//        } else if (features == 3) {
//            featuresScore = 50;
//        } else if (features == 2) {
//            featuresScore = 25;
//        } else if (features == 1) {
//            featuresScore = 10;
//        } else {
//            featuresScore = 0;
//        }
//        return featuresScore;
//    }
//
//    private int imagesScore(int imagesCount) {
//        int imgScore = 0;
//        if (imagesCount > 8) {
//            imgScore = 100;
//        } else if (imagesCount == 8) {
//            imgScore = 90;
//        } else if (imagesCount == 7) {
//            imgScore = 80;
//        } else if (imagesCount == 6) {
//            imgScore = 70;
//        } else if (imagesCount == 5) {
//            imgScore = 60;
//        } else if (imagesCount == 4) {
//            imgScore = 50;
//        } else if (imagesCount == 3) {
//            imgScore = 30;
//        } else if (imagesCount == 2) {
//            imgScore = 20;
//        } else if (imagesCount == 1) {
//            imgScore = 10;
//        }
//        return imgScore;
//    }
}
