/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.amazon;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author netelixir
 */
@Service
public class AmazonProductListingService {
    
    private static final Logger logger = Logger.getLogger(AmazonProductListingService.class);
    
    @Value("${lxrm.apps.rest.hostingURL}")
    String lxrmappshostingURL;
    
    @Value("${amazonProductListingServiceUrl}")
    String amazonProductListingServiceUrl;
    
    @Value("${amazonProductListingFileUploadServiceUrl}")
    String amazonProductListingFileUploadServiceUrl;
    
    public Object[] getAmazonData(String queryURL) {
        List<String> finalUrlList = convertStringToArrayList(queryURL, "\n");
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        for (String url : finalUrlList) {
            if (url != null && !url.equals("")) {
                map.add("manualUrls", url);
            }
        }
        HttpHeaders headers = new HttpHeaders();
//        headers.add("url", queryURL);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(map, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object[]> response = restTemplate.exchange(lxrmappshostingURL.concat(amazonProductListingServiceUrl), HttpMethod.POST, entity, Object[].class);
        Object[] amazonProductData = new Object[3];
        if (response.getBody() != null) {
            amazonProductData = response.getBody();
        }
        return amazonProductData;
    }
    
    public Object[] getUploadedFileAmazonData(MultipartFile file) {
//        String requestUrl = "http://localhost:8080/productinfo-for-fileUploading.html";
//        List<AmazonProductListing> amazonProductData = null;
        Object[] amazonProductData = null;
        String tempFileName = null;
        FileOutputStream fo = null;
        try {
            tempFileName = "/tmp/" + file.getOriginalFilename();
            fo = new FileOutputStream(tempFileName);
            fo.write(file.getBytes());
            LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
            map.add("file", new FileSystemResource(tempFileName));
            
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            
            HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Object[]> response = restTemplate.exchange(lxrmappshostingURL.concat(amazonProductListingFileUploadServiceUrl), HttpMethod.POST, requestEntity, Object[].class);
            
            if (response.getBody() != null && !response.getBody().equals("")) {
                amazonProductData = response.getBody();
            }
            return amazonProductData;
        } catch (IOException | RestClientException e) {
            logger.error("cannot get the object from restController using resttemplate", e);
        } finally {
            File oldfile = new File(tempFileName);
            if (oldfile.exists()) {
                oldfile.delete();
                
            }
            if (fo != null) {
                try {
                    fo.close();
                } catch (IOException ex) {
                    logger.error("cannot close the file", ex);
                }
            }
        }
        return null;
    }
    
    public ArrayList<String> convertStringToArrayList(String queryURL) {
        return convertStringToArrayList(queryURL, "[;\\n]");
    }
    
    public ArrayList<String> convertStringToArrayList(String queryURL,
            String separator) {
        ArrayList<String> keywordList = new ArrayList<>();
        if (queryURL != null && queryURL.length() > 0 && !queryURL.trim().equals("")) {
            Pattern p = Pattern.compile(separator);
            String[] list = p.split(queryURL);
            for (String list1 : list) {
                if (list1.trim().length() > 0) {
                    keywordList.add(list1.trim());
                }
            }
        }
        return keywordList;
    }
    
}