package lxr.marketplace.metataggenerator;

/**
 * Bean Class
 */
public class MetaTagGeneratorTool {

    /**
     * Meta Title Content
     */
    private String title;
    /**
     * Meta Description Content
     */
    private String description;
    /**
     * Meta keyword Content
     */
    private String keyword;
    /**
     * Meta Author Content
     */
    private String author;
    /**
     * Final meta tag Content
     */
    private String metaTagContent;
    /**
     * given URL to import Content
     */
    private String url;

    /**
     * Setter and Getter Methods
     */
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

   
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMetaTagContent() {
        return metaTagContent;
    }

    public void setMetaTagContent(String metaTagContent) {
        this.metaTagContent = metaTagContent;
    }

}
