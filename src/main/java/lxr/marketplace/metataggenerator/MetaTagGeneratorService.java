package lxr.marketplace.metataggenerator;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.Common;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

/**
 * import Meta content from given URL.
 */
@Service
public class MetaTagGeneratorService{

    private static final Logger logger = Logger.getLogger(MetaTagGeneratorService.class);
    /**
     * To check Jsoup object is created.
     */
    boolean jsoupconnerror;

    public boolean isJsoupconnerror() {
        return jsoupconnerror;
    }

    public void setJsoupconnerror(boolean jsoupconnerror) {
        this.jsoupconnerror = jsoupconnerror;
    }

    /**
     * Import's meta tag content from a given URL by using JSOUP API
     *
     * @return
     */
    public MetaTagGeneratorTool importMetaTagsContent(String queryURL) {
        int timeout = 30000;
        int sllepOut = 10000;
        String userAgent = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
        /*  Modified on oct-7 2015 "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
        Document doc = null;
        long timeoutIteration = 0;
        Connection jsoupConn = null;
        long noOfTime = 3;
        Elements metaTags;
        String metaTagName;
        String description = null;
        String keywords = null;
        String title = null;
        String author = null;
        MetaTagGeneratorTool metaTagGenerator = null;
        /*Approach 1: preparing doc from page*/
        StringBuilder pageHTML = new StringBuilder("");
        Common.checkCurrentURLAndGetHTML(queryURL, pageHTML);
        if (!pageHTML.toString().equals("")) {
            String httpDomainUrl = "";
            if (queryURL.startsWith("http://") || queryURL.startsWith("https://")) {
                httpDomainUrl = queryURL;
            } else {
                httpDomainUrl = "http://" + queryURL;
            }
            /*Approach 2: preparing doc by using jsoup*/
            doc = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
        }
        /*Approach 3: preparing doc by using jsoup*/
        if (doc == null) {
            do {
                timeoutIteration += 1;
                try {
                    jsoupConn = Jsoup.connect(queryURL);
                    jsoupConn.timeout(timeout);
                    jsoupConn.userAgent(userAgent);
                    jsoupConn.followRedirects(true);
                    doc = jsoupConn.get();
                    break;
                } catch (IOException ex) {
                    jsoupconnerror = true;
                    try {
                        Thread.sleep(sllepOut * timeoutIteration);
                    } catch (InterruptedException e) {
                        logger.error("InterruptedException", e);
                    }
                }
            } while (timeoutIteration < noOfTime);
        }

        if (doc != null) {
            if (doc.select("title").first() != null) {
                title = doc.select("title").first().text().trim();
            }
            if (title == null && doc.getElementsByTag("head").select("title").first() != null) {
                title = doc.getElementsByTag("head").select("title").first().text().trim();
            }
            metaTags = doc.select("meta");
            if (metaTags.size() > 0) {
                for (Element metaTag : metaTags) {
                    metaTagName = metaTag.attr("name");
                    if (metaTagName.equalsIgnoreCase("description")) {
                        description = metaTag.attr("content");
                    }
                    if (metaTagName.equalsIgnoreCase("keywords")) {
                        keywords = metaTag.attr("content");
                    }
                    if (metaTagName.equalsIgnoreCase("author")) {
                        author = metaTag.attr("content");
                    }
                }
            }

            /**
             * Set's Title,Description,keywords,Author content from given URL to
             * the MetaTagGeneratorTool bean class
             *
             */
            metaTagGenerator = new MetaTagGeneratorTool();
            metaTagGenerator.setUrl(queryURL);
            if (title != null) {
                metaTagGenerator.setTitle(title.trim());
            }
            if (description != null) {
                metaTagGenerator.setDescription(description.trim());
            }
            if (keywords != null) {
                metaTagGenerator.setKeyword(keywords.trim());
            }
            if (author != null) {
                metaTagGenerator.setAuthor(author.trim());
            }
        }
        return metaTagGenerator;
    }

    /**
     * @param metaTagGeneratorTool
     * @return Final MetaTag Content
     */
    protected String buildMetaTag(MetaTagGeneratorTool metaTagGeneratorTool) {
        String metaTagContent = "";
        if(metaTagGeneratorTool.getTitle() != null){
            metaTagContent = metaTagContent+"<title>"+metaTagGeneratorTool.getTitle().trim()+"</title>";
        }
        
        if(metaTagGeneratorTool.getDescription() != null && !metaTagGeneratorTool.getDescription().trim().equals("")){
            if(!metaTagContent.equals("")){
                metaTagContent = metaTagContent+"\n<meta name=\"description\" content=\"" + metaTagGeneratorTool.getDescription().trim() + "\">";
            }else{
                metaTagContent = metaTagContent+"<meta name=\"description\" content=\"" + metaTagGeneratorTool.getDescription().trim() + "\">";
            }
        }
        
        if(metaTagGeneratorTool.getKeyword()!= null && !metaTagGeneratorTool.getKeyword().trim().equals("")){
            if(!metaTagContent.equals("")){
                metaTagContent = metaTagContent+"\n<meta name=\"keywords\" content=\"" + metaTagGeneratorTool.getKeyword().trim() + "\">";
            }else{
                metaTagContent = metaTagContent+"<meta name=\"keywords\" content=\"" + metaTagGeneratorTool.getKeyword().trim() + "\">";
            }
        }
        
        if(metaTagGeneratorTool.getAuthor()!= null && !metaTagGeneratorTool.getAuthor().trim().equals("")){
            if(!metaTagContent.equals("")){
                metaTagContent = metaTagContent+"\n<meta name=\"author\" content=\"" + metaTagGeneratorTool.getAuthor().trim() + "\">";
            }else{
                metaTagContent = metaTagContent+"<meta name=\"author\" content=\"" + metaTagGeneratorTool.getAuthor().trim() + "\">";
            }
        }
        logger.debug("Generated meta tag content: " + metaTagContent);
        return metaTagContent;
    }

    public void constructMetaTagJSON(MetaTagGeneratorTool metaTagGeneratorTool, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(0, metaTagGeneratorTool);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }
}
