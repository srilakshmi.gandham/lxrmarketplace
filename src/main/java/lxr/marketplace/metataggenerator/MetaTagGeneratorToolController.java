package lxr.marketplace.metataggenerator;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;

import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/meta-tag-generator-tool")
public class MetaTagGeneratorToolController {

    private static Logger logger = Logger.getLogger(MetaTagGeneratorToolController.class);
    @Autowired
    private final MetaTagGeneratorService metaTagGeneratorService;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @Autowired
    public MetaTagGeneratorToolController(MetaTagGeneratorService metaTagGeneratorService) {
        this.metaTagGeneratorService = metaTagGeneratorService;
    }

    @Autowired
    private MessageSource messageSource;

    JSONObject userInputJson = null;
    public static final String FORM_VIEW = "views/metaTagGenerator/metaTagGenerator";

    /**
     * @param request
     * @param response
     * @param metaTagGeneratorTool
     * @param model
     * @param session
     * @return MetaTagGeneratorTool page
     */
    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("metaTagGeneratorTool") MetaTagGeneratorTool metaTagGeneratorTool, ModelMap model, HttpSession session) {
        /*Navigation of user to existed result page click on cancel from auk our expert confirmation page.*/
        if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            response.setCharacterEncoding("UTF-8");
            model.addAttribute("status", "success");
            if (session.getAttribute("metaTagGeneratorTool") != null) {
                metaTagGeneratorTool = (MetaTagGeneratorTool) session.getAttribute("metaTagGeneratorTool");
                model.addAttribute("metaTagGeneratorTool", metaTagGeneratorTool);
            }
        }
        /*End of Navigation to existed result page.*/
        return FORM_VIEW;
    }

//	@RequestMapping(params="query")
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request,
            HttpServletResponse response, ModelMap model, @ModelAttribute("metaTagGeneratorTool") MetaTagGeneratorTool metaTagGeneratorTool,
            BindingResult result, HttpSession session, @RequestParam(defaultValue = "") String query) throws JSONException {
        Session userSession = (Session) session.getAttribute("userSession");
        Login user = (Login) session.getAttribute("user");

        /*if (metaTagGeneratorTool != null) {
            session.setAttribute("metaTagGeneratorTool", metaTagGeneratorTool);
        }*/
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(27);
            toolObj.setToolName("Meta Tag Generator Tool");
            toolObj.setToolLink("meta-tag-generator-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.META_TAG_GENERATOR);
            if (query.equals("clearAll")) {
                session.removeAttribute("metaTagGeneratorTool");
                session.removeAttribute("metaTagGenerationType");
                session.removeAttribute("metaTagGeneratorofATE");
//                session.removeAttribute("metaInvalidURl");
            } else if (query.equals("'getResult'")) {
                session.removeAttribute("metaTagGeneratorTool");
                String queryURL = request.getParameter("queryURL").trim();
                userSession.addToolUsage(toolObj.getToolId());
                MetaTagGeneratorTool metaTagGeneratorResult = null;
                metaTagGeneratorResult = new MetaTagGeneratorTool();
                String metaTagGenerationType = null;
                if (queryURL != null && !queryURL.trim().equals("")) {
                    metaTagGeneratorResult.setUrl(queryURL);
                    metaTagGenerationType = "import";
                    session.setAttribute("metaTagGenerationType", metaTagGenerationType);
                } else {
                    metaTagGenerationType = "userDefined";
                    session.setAttribute("metaTagGenerationType", metaTagGenerationType);
                }
                metaTagGeneratorResult.setTitle(request.getParameter("titleContent"));
                metaTagGeneratorResult.setDescription(request.getParameter("descriptionContent"));
                metaTagGeneratorResult.setKeyword(request.getParameter("keywordContent"));
                metaTagGeneratorResult.setAuthor(request.getParameter("authorContent"));
                metaTagGeneratorResult.setMetaTagContent(metaTagGeneratorService.buildMetaTag(metaTagGeneratorResult));
                session.setAttribute("metaTagGeneratorTool", metaTagGeneratorResult);

                //                if the request is coming from tool page then only add the user inputs to JSONObject
//                if the request is coming from mail then do not add the user inputs to JSONObject
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    userInputJson.put("2", metaTagGeneratorTool.getTitle());
                    userInputJson.put("3", metaTagGeneratorTool.getDescription());
                    userInputJson.put("4", metaTagGeneratorTool.getKeyword());
                    userInputJson.put("5", metaTagGeneratorTool.getAuthor());
                }
                response.setCharacterEncoding("UTF-8");
                /*To be used in ask our expert for checking  type of meta tag generation.*/
                session.setAttribute("metaTagGeneratorofATE", metaTagGeneratorResult);
                model.addAttribute("status", "success");
                Common.modifyDummyUserInToolorVideoUsage(request, response);

                metaTagGeneratorService.constructMetaTagJSON(metaTagGeneratorResult, response);

                return null;
            } else if (query.equals("'importMeta'")) {
                String queryURL = request.getParameter("metaURL").trim();
                queryURL = Common.getUrlValidation(queryURL.trim(), session);
                if (queryURL.equals("invalid") || queryURL.equals("redirected")) {
                    session.removeAttribute("metaTagGeneratorTool");
                    session.removeAttribute("metaTagGenerationType");
                    session.removeAttribute("metaTagGeneratorofATE");
                    metaTagGeneratorTool.setUrl(request.getParameter("metaURL"));
                    metaTagGeneratorTool.setDescription(messageSource.getMessage("URL.error", null, Locale.US));
                    metaTagGeneratorService.constructMetaTagJSON(metaTagGeneratorTool, response);
                } else {
                    String metaTagGenerationType = "import";
                    session.setAttribute("metaTagGenerationType", metaTagGenerationType);
                    metaTagGeneratorTool.setUrl(queryURL);
                    metaTagGeneratorTool = metaTagGeneratorService.importMetaTagsContent(queryURL);
                    metaTagGeneratorService.constructMetaTagJSON(metaTagGeneratorTool, response);
                    session.setAttribute("metaTagGeneratorofATE", metaTagGeneratorTool);
                }
                return null;
            } else if (query.equals("'meatTagATE'")) {
                /*To  Handle Ask Our Expert request*/
                String metaTagURL = request.getParameter("metaURL").trim();
                String title = null;
                String description = null;

                MetaTagGeneratorTool metaTagGeneratorofATE = null;
                if (session.getAttribute("metaTagGeneratorofATE") != null) {
                    metaTagGeneratorofATE = (MetaTagGeneratorTool) session.getAttribute("metaTagGeneratorofATE");
                    title = metaTagGeneratorofATE.getTitle();
                    description = metaTagGeneratorofATE.getDescription();
                }
                boolean isseuesExisted = false;
                if (title != null) {
                    if (title.equals("") || title.length() <= 40 || title.length() >= 66) {
                        isseuesExisted = true;
                    }
                }
                if (description != null) {
                    if (description.equals("") || (description.length() <= 70) || (description.length() >= 161)) {
                        isseuesExisted = true;
                    }
                }
                String metaTagGenerationType = null;
                if (session.getAttribute("metaTagGenerationType") != null) {
                    metaTagGenerationType = (String) session.getAttribute("metaTagGenerationType");
                }
                /*if ((title.length() >= 40 || title.length() >= 66) || (description.equals(""))
                        || (!(description.equals("")) && (description.length() <= 70) || (description.length() >= 161))
                        || ((!metaTagType.equals("")) && (metaTagType.equals("userDefined")))) {
                    isseuesExisted = true;
                }
                if (session.getAttribute("metaInvalidURl") != null) {
                    inValidUrl = (Boolean) session.getAttribute("metaInvalidURl");
                }*/
 /*Refer the Sheet Tool Based Analysis Analysis Name: Help in Submitting Meta Tag generator*/
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                String toolIssues = "";
                String questionInfo = "";
                if (isseuesExisted && (metaTagGenerationType != null && metaTagGenerationType.equals("import"))) {
                    questionInfo = "Need help in fixing following issues with my website '" + metaTagURL + "'.\n\n";
                    toolIssues = "<ul>";

                    if ((title != null) && ((title.length() <= 40 || title.length() >= 66))) {
                        questionInfo += "- Optimizing Title length conforming with SEO best practices.\n";
                        toolIssues += "<li>" + "Your website's title does not utilize best practices for SEO optimization." + "</li>";
                    }
                    if ((description != null) && description.equals("")) {
                        questionInfo += "- Creating Meta Description length conforming with SEO best practices.\n";
                        toolIssues += "<li>" + "Your website does not currently have a meta description. "
                                + "We can help you create a meta description that utilizes the best practices in SEO." + "</li>";
                    } else if ((description != null) && ((description.length() <= 70) || (description.length() >= 161))) {
                        questionInfo += "- Optimizing Meta Description length conforming with SEO best practices.\n";
                        toolIssues += "<li>" + "Your website's meta description does not utilize best practices for SEO optimization." + "</li>";
                    }
                    toolIssues += "</ul>";
                } else if (metaTagGeneratorofATE != null) {
                    questionInfo = "Need help in fixing following issues with my website '" + metaTagURL + "'.\n\n";
                    toolIssues = "<ul>";
                    questionInfo += "- Creating Meta Tags conforming with SEO best practices.\n";
                    toolIssues += "<li>" + "We can help you create an optimal meta description that utilizes the best practices in SEO." + "</li>";
                    toolIssues += "</ul>";
                    if (metaTagGenerationType != null && !metaTagGenerationType.equals("import")) {
                        metaTagURL = "";
                    }
                }

//                logger.info("questionInfo " + questionInfo + " toolIssues" + toolIssues);
                lxrmAskExpert.setDomain(metaTagURL.trim());
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(toolObj.getToolName());
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), metaTagURL, toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }

                return null;
            }/*End of Ask Our Export */ else if (query.equals("'backToSuccessPage'")) {
            }
        }
        return "views/metaTagGenerator/metaTagGenerator";
    }
}
