/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.seotip;

import java.sql.Timestamp;

/**
 *
 * @author VidyaSagar
 */
public class LxrmSEOTip {
    
    private long lxrmSEOTipId;
    private String name;
    private String email;
    private Timestamp activationDate;
    private Timestamp unSubscribeDate;
    private boolean isUnsubscribe;    

    public long getLxrmSEOTipId() {
        return lxrmSEOTipId;
    }

    public void setLxrmSEOTipId(long lxrmSEOTipId) {
        this.lxrmSEOTipId = lxrmSEOTipId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Timestamp activationDate) {
        this.activationDate = activationDate;
    }

    public Timestamp getUnSubscribeDate() {
        return unSubscribeDate;
    }

    public void setUnSubscribeDate(Timestamp unSubscribeDate) {
        this.unSubscribeDate = unSubscribeDate;
    }

    public boolean isIsUnsubscribe() {
        return isUnsubscribe;
    }

    public void setIsUnsubscribe(boolean isUnsubscribe) {
        this.isUnsubscribe = isUnsubscribe;
    }
    
    
    
    
}
