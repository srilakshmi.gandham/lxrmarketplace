/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.seotip;

import java.sql.Timestamp;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class LxrSeoTipService {
    
    private static Logger logger = Logger.getLogger(LxrSeoTipService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insertSEOTipUser(LxrmSEOTip lxrmSEOTip) {
        String query = "insert into lxrm_user_aquisition(name, email,activation_date,unsubscribe, aquisition_type) "
                + "values (?, ?, ?, ?, ?)";
        Timestamp creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            synchronized (this) {
                Object[] args = {lxrmSEOTip.getName(), lxrmSEOTip.getEmail(), creationTime, false, "SEO Tip"};
                this.jdbcTemplate.update(query, args);
                logger.info("lxrm_user_aquisition inserted; userName: " + lxrmSEOTip.getName() + " Email: " + lxrmSEOTip.getEmail());
            }
        } catch (Exception ex) {
            logger.error("Exception in inserting SEO Tip User: ", ex);
        }
    }

    public LxrmSEOTip checkingForExistingUser(String email) {
        SqlRowSet sqlRowSet;
        LxrmSEOTip lxrmSEOTip = null;
        String query = "select * from lxrm_user_aquisition where email = ?";
        try {
            synchronized (this) {
                sqlRowSet = this.jdbcTemplate.queryForRowSet(query, email);
                if (sqlRowSet != null) {
                    while (sqlRowSet.next()) {
                        lxrmSEOTip = new LxrmSEOTip();
                        lxrmSEOTip.setName(sqlRowSet.getString(2));
                        lxrmSEOTip.setEmail(sqlRowSet.getString(3));
                        lxrmSEOTip.setActivationDate(sqlRowSet.getTimestamp(4));
                        lxrmSEOTip.setUnSubscribeDate(sqlRowSet.getTimestamp(5));
                        lxrmSEOTip.setIsUnsubscribe(sqlRowSet.getBoolean(6));
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception in checking for existing record in lxrm_user_aquisition ", ex);
        }
        return lxrmSEOTip;
    }

    public void updateSEOTipUser(LxrmSEOTip lxrmSEOTip) {
        String query = "update lxrm_user_aquisition SET activation_date = ?, unsubscribe_date = null, unsubscribe = ?  where email = ?";
        Timestamp creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            synchronized (this) {
                Object[] args = {creationTime, false, lxrmSEOTip.getEmail()};
                this.jdbcTemplate.update(query, args);
                logger.info("lxrm_user_aquisition updated; creationTime: " + creationTime);
            }
        } catch (Exception ex) {
            logger.error("Exception in updating SEO Tip User", ex);
        }
    }
}
