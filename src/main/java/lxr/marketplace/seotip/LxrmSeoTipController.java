package lxr.marketplace.seotip;

/**
 *
 * @author VidyaSagar
 */
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.user.Login;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/lxrm-seo-tip.html")
public class LxrmSeoTipController {

    private static final Logger logger = Logger.getLogger(LxrmSeoTipController.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    private SignupService signupService;
    @Autowired
    private LxrSeoTipService lxrSeoTipService;
    @Autowired
    private String supportMail;

    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
        logger.info("In registerSEOTipUser of GET");
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn) {
            LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(user.getUserName());
            if (existingSEOTipUser != null) {
                if (existingSEOTipUser.isIsUnsubscribe()) {
                    modelMap.addAttribute("nonExistingUser", "true");
                } else {
                    session.setAttribute("existingSEOTipUser", existingSEOTipUser);
                    modelMap.addAttribute("existingSEOTipUser", "true");
                }
            } else {
                modelMap.addAttribute("nonExistingUser", "true");
            }
        } else {
            modelMap.addAttribute("showSEOTip", "true");
        }
        return "view/seotip/seoTip";
    }

    @RequestMapping(method = RequestMethod.POST)
    public void onSubmit(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("name") String userName, @RequestParam("email") String userEmail) {
        logger.debug("In onSubmit of POST");

        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (WebUtils.hasSubmitParameter(request, "seoTipReg")) {
            SendMail sendMail = new SendMail();
            LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(userEmail);
            if (existingSEOTipUser == null) {
                LxrmSEOTip lxrmSEOTip = new LxrmSEOTip();
                lxrmSEOTip.setName(userName);
                lxrmSEOTip.setEmail(userEmail);
                long newLxrmSeoTipId = insertNewLxrmSEOTipUser(lxrmSEOTip);
                session.setAttribute("NewSEOTipUser", lxrmSEOTip);
                String subject = EmailBodyCreator.subjectOnSEOTipSubscription;
                String bodyText = EmailBodyCreator.bodyForSEOTipSubscription();
                SendMail.sendMail(supportMail, lxrmSEOTip.getEmail(), subject, bodyText);
                addInJsonforLogin("NewUser", response);
            } else if (existingSEOTipUser.isIsUnsubscribe()) {
                lxrSeoTipService.updateSEOTipUser(existingSEOTipUser);
                String subject = EmailBodyCreator.subjectOnSEOTipSubscription;
                String bodyText = EmailBodyCreator.bodyForSEOTipSubscription();
                SendMail.sendMail(supportMail, existingSEOTipUser.getEmail(), subject, bodyText);
                addInJsonforLogin("NewUser", response);
            } else {
                session.setAttribute("existingSEOTipUser", existingSEOTipUser);
                addInJsonforLogin("Already Existed", response);
            }
        } else if (WebUtils.hasSubmitParameter(request, "checkSEO")) {
            logger.info("In checkSEO of POST");
            if (userLoggedIn) {
                logger.info("In user logged in checkSEO of POST");
                LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(user.getUserName());
                if (existingSEOTipUser != null) {
                    if (existingSEOTipUser.isIsUnsubscribe()) {
                        addInJsonforLogin("nonExistingUser", response);
                    } else {
                        addInJsonforLogin("existingSEOTipUser", response);
                        session.setAttribute("existingSEOTipUser", existingSEOTipUser);
                    }
                } else {
                    addInJsonforLogin("nonExistingUser", response);
                }
            } else {
                addInJsonforLogin("showSEOTip", response);
            }
        } else if (WebUtils.hasSubmitParameter(request, "unSubscribe")) {
            logger.info("In  for unSubscribe of POST");
            Login unSubuser = (Login) session.getAttribute("user");
//            UserUnSubscribe is existing seo tip user ,added when user is checking in lxrm_user_aquisition
            LxrmSEOTip userUnSubscribe = (LxrmSEOTip) session.getAttribute("existingSEOTipUser");
            LxrmSEOTip NewSEOTipUser = (LxrmSEOTip) session.getAttribute("NewSEOTipUser");
            if (userLoggedIn) {
                logger.info("In unSubscribe of POST for loggedin user");
                LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(user.getUserName());
                if (existingSEOTipUser != null) {
                    updateSEOTipUser(unSubuser.getUserName(), true);
                    addInJsonforLogin("UnSubSucess", response);
                }
            } else if ((userUnSubscribe) != null) {
                logger.info("In unSubscribe of POST for guest user");
                LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(userUnSubscribe.getEmail());
                if (existingSEOTipUser != null) {
                    updateSEOTipUser(userUnSubscribe.getEmail(), true);
                    addInJsonforLogin("UnSubSucess", response);
                }

            } else if ((NewSEOTipUser) != null) {
                logger.info("In unSubscribe of POST for NewSEOTipUser user");
                LxrmSEOTip existingSEOTipUser = caseInsensitiveFindInSEOTip(NewSEOTipUser.getEmail());
                if (existingSEOTipUser != null) {
                    updateSEOTipUser(NewSEOTipUser.getEmail(), true);
                    addInJsonforLogin("UnSubSucess", response);
                }
            }
        }
    }

    public LxrmSEOTip caseInsensitiveFindInSEOTip(String email) {
        logger.info("In caseInsensitiveFindInSEOTip for email is " + email);
        String query = "select LOWER(email),id,name,activation_date,unsubscribe_date,unsubscribe "
                + "from lxrm_user_aquisition where email = LOWER('" + email + "')";
        logger.debug("Query for email is " + query);
        try {
            LxrmSEOTip lxrmSEOTip = (LxrmSEOTip) jdbcTemplate.queryForObject(query, new RowMapper() {
                LxrmSEOTip lxrmSEOTipUser;

                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    lxrmSEOTipUser = new LxrmSEOTip();
                    lxrmSEOTipUser.setEmail(rs.getString(1));
                    lxrmSEOTipUser.setLxrmSEOTipId(rs.getLong(2));
                    lxrmSEOTipUser.setName(rs.getString(3));
                    lxrmSEOTipUser.setActivationDate(rs.getTimestamp(4));
                    lxrmSEOTipUser.setUnSubscribeDate(rs.getTimestamp(5));
                    lxrmSEOTipUser.setIsUnsubscribe(rs.getBoolean(6));
                    return lxrmSEOTipUser;
                }
            });

            return lxrmSEOTip;
        } catch (Exception e) {
            logger.error("Exception in seraching SEOTIP user" + e.getMessage());
        }
        return null;
    }

    public long insertNewLxrmSEOTipUser(final LxrmSEOTip lxrmSEOTip) {
        logger.debug("In insertNewUser in LxrmSEOTip");

        final String query = "insert into lxrm_user_aquisition(name, email,activation_date,unsubscribe) values (?, ?, ?, ?)";
        final Timestamp creaTedTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {

                jdbcTemplate.update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"lxrm_seo_tip_id"});

                        if (lxrmSEOTip.getName() == null || lxrmSEOTip.getName().equals("")) {
                            statement.setString(1, "");
                        } else {
                            try {
                                statement.setBytes(1, lxrmSEOTip.getName().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(1, lxrmSEOTip.getName());
                                logger.error("Exception in UnsupportedEncodingException SEOTIP user registration" + e.getMessage());
                            }
                        }

                        if (lxrmSEOTip.getEmail().toLowerCase() == null || lxrmSEOTip.getEmail().toLowerCase().equals("")) {
                            statement.setString(2, "");
                        } else {
                            try {
                                statement.setBytes(2, lxrmSEOTip.getEmail().toLowerCase().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(2, lxrmSEOTip.getEmail().toLowerCase());
                                logger.error("Exception in UnsupportedEncodingException SEOTIP user registration" + e.getMessage());
                            }
                        }

                        statement.setTimestamp(3, creaTedTime);
                        statement.setBoolean(4, false);

                        return statement;
                    }
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error("Exception in SEOTIP user registration" + ex.getMessage());
        }
        return (Long) keyHolder.getKey();
    }

    public void updateSEOTipUser(final String userEmial, boolean isUnsubscribe) {
        logger.debug("In updateSEOTipUser for unsubscribeing for user with email" + userEmial);
        final Timestamp unSubTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String query = "update lxrm_user_aquisition set unsubscribe=?,"
                + "unsubscribe_date = ? where email = ? ";

        Object[] valLis = new Object[3];
        valLis[0] = isUnsubscribe;
        valLis[1] = unSubTime;
        valLis[2] = userEmial;
        try {
            jdbcTemplate.update(query, valLis);
        } catch (Exception ex) {
            logger.error("Exception in update User SEOTIP Data for unsubscribe in " + ex.getMessage());
        }
    }

    public void addInJsonforLogin(String stringData, HttpServletResponse response) {
        logger.debug("In ajax call" + stringData);
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(0, stringData);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }
}
