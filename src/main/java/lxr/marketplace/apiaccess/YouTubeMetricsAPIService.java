package lxr.marketplace.apiaccess;

import lxr.marketplace.apiaccess.lxrmbeans.YouTubeMetrics;
import lxr.marketplace.util.Common;
import lxr.marketplace.socialmedia.APIKeys;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class YouTubeMetricsAPIService implements Runnable {

    private static Logger logger = Logger.getLogger(YouTubeMetricsAPIService.class);
    private JSONObject[] UserProfileandRecentActiviteJosn = null;
    private String youtubeUser;
    private YouTubeMetrics youTubeMetrics;
    private ConcurrentHashMap<Integer, String> youTubeMap;

    public YouTubeMetricsAPIService() {
    }

    public YouTubeMetricsAPIService(String youtubeUser, ConcurrentHashMap<Integer, String> youTubeMap) {
        this.youtubeUser = youtubeUser;
        this.youTubeMap = youTubeMap;
    }

    @Override
    public void run() {
        try {
            setYouTubeMetrics(getYouTubeMetrics(youtubeUser));
        } catch (Exception e) {
            setYouTubeMetrics(getYouTubeMetrics(youtubeUser));
            logger.error("Exception for YOUTUBE API Service: ", e);
        }
        youTubeMap.put(4, "YouTube");
    }

    public ConcurrentHashMap<Integer, String> getYouTubeMap() {
        return youTubeMap;
    }

    public void setYouTubeMap(ConcurrentHashMap<Integer, String> youTubeMap) {
        this.youTubeMap = youTubeMap;
    }

    public String getYoutubeUser() {
        return youtubeUser;
    }

    public void setYoutubeUser(String youtubeUser) {
        this.youtubeUser = youtubeUser;
    }

    public YouTubeMetrics getYouTubeMetrics() {
        return youTubeMetrics;
    }

    public void setYouTubeMetrics(YouTubeMetrics youTubeMetrics) {
        this.youTubeMetrics = youTubeMetrics;
    }

    public static boolean checkYoutubeUserName(String username) {
        return checkYouTubeUserExistence(username);
    }

    public static boolean checkYouTubeUserExistence(String username) {
        try {
            /*Checking user existence's based on channel name */
            JSONObject respsonseJSon = Common.getResponseFromAPI(getYoutubeUserProfileURL(username));
            logger.debug("respsonseJSon: " + respsonseJSon);
            JSONArray itemsArray = null;
            if (respsonseJSon != null && respsonseJSon.has("items")) {
                itemsArray = respsonseJSon.getJSONArray("items");
            }
            if (itemsArray != null && itemsArray.length() > 0) {
                return true;
            } else if ((itemsArray != null && itemsArray.length() == 0) || itemsArray == null) {
                /*Checking user existence's based on channel ID */
                JSONObject respsonseJSonID = Common.getResponseFromAPI(getYoutubeUserProfile(username));
                JSONArray itemsArrayID = respsonseJSonID.getJSONArray("items");
                if (itemsArrayID != null) {
                    return itemsArrayID.length() == 1;
                }
            }
        } catch (Exception e) {
            logger.error("Exception for checkYouTubeUserExistence:", e);
        }
        return false;
    }

    public YouTubeMetrics getYouTubeMetrics(String userName) {
        youTubeMetrics = new YouTubeMetrics();
        if (!(youtubeUser.equals("NDA"))) {
            UserProfileandRecentActiviteJosn = new JSONObject[12];
            String channelID = null;
            try {
                getYouTubeUserProfileJosnObject(userName, APIKeys.googleKey);
                if (UserProfileandRecentActiviteJosn[0] != null) {
                    if (UserProfileandRecentActiviteJosn[0].has("subscriberCount")) {
                        youTubeMetrics.setSubscribers(UserProfileandRecentActiviteJosn[0].getLong("subscriberCount"));
                    }
                    if (UserProfileandRecentActiviteJosn[0].has("videoCount")) {
                        youTubeMetrics.setVideos(UserProfileandRecentActiviteJosn[0].getLong("videoCount"));
                    }
                    if (UserProfileandRecentActiviteJosn[0].has("viewCount")) {
                        youTubeMetrics.setViews(UserProfileandRecentActiviteJosn[0].getLong("viewCount"));
                    }
                }
                if (UserProfileandRecentActiviteJosn[1] != null) {
                    if (UserProfileandRecentActiviteJosn[1].has("id")) {
                        channelID = UserProfileandRecentActiviteJosn[1].getString("id");
                        youTubeMetrics.setUserName(UserProfileandRecentActiviteJosn[1].getString("id"));
                    }
                }
                youTubeMetrics.setYoutubeAccountStatus(Boolean.TRUE);
                if (channelID != null) {
                    getYouTubeUserRecentMediaJosnObject(channelID, APIKeys.googleKey);
                }

                if (UserProfileandRecentActiviteJosn[2] != null) {
                    if (UserProfileandRecentActiviteJosn[2].has("title")) {
                        String recentMediaTitle = UserProfileandRecentActiviteJosn[2].getString("title");
                        if (recentMediaTitle != null) {
                            recentMediaTitle = recentMediaTitle.trim();
                            if (UserProfileandRecentActiviteJosn[2].has("description")) {
                                recentMediaTitle = recentMediaTitle + " - " + UserProfileandRecentActiviteJosn[2].getString("description");
                            }
                        } else {
                            recentMediaTitle = " ";
                        }
                        youTubeMetrics.setRecentMediaTitle(recentMediaTitle);
                    }
                    if (UserProfileandRecentActiviteJosn[2].has("publishedAt")) {
                        youTubeMetrics.setUserPostDate(Common.getDateFormatFromGoogleandYouTube(UserProfileandRecentActiviteJosn[2].getString("publishedAt")));
                    }

                }
                if (UserProfileandRecentActiviteJosn[4] != null) {
                    if (UserProfileandRecentActiviteJosn[4].has("commentCount")) {
                        youTubeMetrics.setRecentMediaComments(UserProfileandRecentActiviteJosn[4].getLong("commentCount"));
                    }
                    if (UserProfileandRecentActiviteJosn[4].has("likeCount")) {
                        youTubeMetrics.setRecentMediaLikes(UserProfileandRecentActiviteJosn[4].getLong("likeCount"));
                    }
                    if (UserProfileandRecentActiviteJosn[4].has("viewCount")) {
                        youTubeMetrics.setRecentMediaviews(UserProfileandRecentActiviteJosn[4].getLong("viewCount"));
                    }
                    if (UserProfileandRecentActiviteJosn[4].has("dislikeCount")) {
                        youTubeMetrics.setRecentMediaDisLikes(UserProfileandRecentActiviteJosn[4].getLong("dislikeCount"));
                    }
                    youTubeMetrics.setEngagement(youTubeMetrics.getRecentMediaComments() + youTubeMetrics.getRecentMediaLikes() + youTubeMetrics.getRecentMediaviews() + youTubeMetrics.getRecentMediaDisLikes());
                    youTubeMetrics.setAudienceEngaged(Common.getAudienceEngaged(youTubeMetrics.getEngagement(), youTubeMetrics.getSubscribers()));
                }
                String tempyoutube = null;
                if (UserProfileandRecentActiviteJosn[3] != null) {
                    if (UserProfileandRecentActiviteJosn[3].has("url")) {
                        tempyoutube = UserProfileandRecentActiviteJosn[3].getString("url");
                    }
                }
                if (tempyoutube != null) {
                    String postid = "youTubeLogoFor" + userName.substring(1, userName.length() - 1);
                    String writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(tempyoutube);
                    if (Common.resizeImageFromUrlandSave(tempyoutube, writePath)) {
                        youTubeMetrics.setImageStatus(Boolean.TRUE);
                        youTubeMetrics.setYoutubeUserLogPath("/social_images/" + postid + "." + Common.getImageFormat(tempyoutube));
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in setYouTubeMetrics of YouTubeAPIService", e);
            }
        }
        return youTubeMetrics;
    }

    /*To get metrics for channel profile.*/
    public void getYouTubeUserProfileJosnObject(String userName, String key) {
        JSONObject youtubeJson, subEntryyoutubeJson, statisticsJson = null;
        JSONArray entryYouTubeJson = null;
        try {
            /*logger.info("The request to youtube api in getYouTubeUserProfileJosnObject "+getYoutubeUserProfileURL(userName));*/
 /*Checking user existence's based on channel name */
            youtubeJson = Common.getResponseFromAPI(getYoutubeUserProfileURL(userName));
            if (youtubeJson != null && youtubeJson.has("items")) {
                entryYouTubeJson = youtubeJson.getJSONArray("items");
            }
            if ((entryYouTubeJson != null && entryYouTubeJson.length() == 0) || entryYouTubeJson == null) {
                /*Checking user existence's based on channel ID */
                JSONObject youtubeIDJson = Common.getResponseFromAPI(getYoutubeUserProfile(userName));
                entryYouTubeJson = youtubeIDJson.getJSONArray("items");
            }
            if (entryYouTubeJson != null && entryYouTubeJson.length() != 0) {
                subEntryyoutubeJson = entryYouTubeJson.getJSONObject(0);
                if (subEntryyoutubeJson.has("statistics")) {
                    statisticsJson = subEntryyoutubeJson.getJSONObject("statistics");
                }
                UserProfileandRecentActiviteJosn[0] = statisticsJson;
                UserProfileandRecentActiviteJosn[1] = subEntryyoutubeJson;
            }
        } catch (JSONException e) {
            logger.error("Exceptin for getYouTubeUserProfileJosnObject cause:: ", e);
        }
    }

    /*To get metrics for recent post*/
    public void getYouTubeUserRecentMediaJosnObject(String channelId, String key) {
        JSONObject rMstatisticsObject = null, snippetJson = null, mediumJson = null, subrMStatisticsObj, subentryJsonObject, contentDetailsJson, uploadJson, recentMediaStatisticsJson;
        JSONArray entryJsonArray = null, rMStatisticsArray;
        String videoId = null;
        int videoIDApproach = 1;
        try {
            JSONObject youtubeJson = Common.getResponseFromAPI(getYoutubeUserVideoIdURL(channelId));
            logger.debug("The reponse from  youtube api in getYouTubeUserRecentMediaJosnObject " + youtubeJson);
            try {
                if ((Common.checkJSONArrayInJSONObjectResponse(youtubeJson, "items")) != null) {
                    entryJsonArray = Common.checkJSONArrayInJSONObjectResponse(youtubeJson, "items");
                    /*Seraching/Fetching recent video statstics and data by using Activities: list method in YouTube API
                                                                    Reference URL:https://developers.google.com/youtube/v3/docs/activities/list#try-it
                     */
                    if (entryJsonArray.length() == 0) {
                        youtubeJson = Common.getResponseFromAPI(getYoutubeUserVideoIdURLV2(channelId));
                        if ((Common.checkJSONArrayInJSONObjectResponse(youtubeJson, "items")) != null) {
                            entryJsonArray = Common.checkJSONArrayInJSONObjectResponse(youtubeJson, "items");
                            videoIDApproach = 2;/*Seraching/Fetching recent video statstics and data by using Search: list method in YouTube API
                                                                    Reference URL:https://developers.google.com/youtube/v3/docs/search/list#try-it
                             */
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in selecting the video fetching process", e);
            }
            if (entryJsonArray == null) {
                entryJsonArray = new JSONArray();
            }

            if (entryJsonArray.length() != 0) {
                subentryJsonObject = entryJsonArray.getJSONObject(0);
            } else {
                subentryJsonObject = new JSONObject();
            }
            if ((Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "snippet")) != null) {

                snippetJson = Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "snippet");
            }
            UserProfileandRecentActiviteJosn[2] = snippetJson;/*Recent video Title*/
            try {
                if (snippetJson != null) {
                    JSONObject thumbnailsJson;
                    if ((Common.checkJSONObjectInJSONObjectResponse(snippetJson, "thumbnails")) != null) {
                        thumbnailsJson = Common.checkJSONObjectInJSONObjectResponse(snippetJson, "thumbnails");
                    } else {
                        thumbnailsJson = new JSONObject();
                    }

                    if ((Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "standard")) != null) {
                        mediumJson = Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "standard");
                    } else if ((Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "medium")) != null) {
                        mediumJson = Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "medium");
                    } else if ((Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "default")) != null) {
                        mediumJson = Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "default");
                    } else if ((Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "high")) != null) {
                        mediumJson = Common.checkJSONObjectInJSONObjectResponse(thumbnailsJson, "high");
                    }
                }
            } catch (Exception e) {
                logger.error("Exception for thumbnailsJson & mediumJson from recent post json: ", e);
            }

            UserProfileandRecentActiviteJosn[3] = mediumJson;/*Recent video Image*/
            if (videoIDApproach == 1) {
                if ((Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "contentDetails")) != null) {
                    contentDetailsJson = Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "contentDetails");
                } else {
                    contentDetailsJson = new JSONObject();
                }

                if ((Common.checkJSONObjectInJSONObjectResponse(contentDetailsJson, "upload")) != null) {
                    uploadJson = Common.checkJSONObjectInJSONObjectResponse(contentDetailsJson, "upload");
                    videoId = uploadJson.getString("videoId");
                }
                JSONObject playlistItem, resourceId;
                if ((Common.checkJSONObjectInJSONObjectResponse(contentDetailsJson, "playlistItem")) != null) {
                    playlistItem = Common.checkJSONObjectInJSONObjectResponse(contentDetailsJson, "playlistItem");
                    if (playlistItem != null) {
                        resourceId = Common.checkJSONObjectInJSONObjectResponse(playlistItem, "resourceId");
                        if (resourceId != null) {
                            videoId = resourceId.getString("videoId");
                        }
                    }

                }
            }/*End of Apporach 1.*/

            if (videoIDApproach == 2) {
                if ((Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "id")) != null) {
                    JSONObject videoIDObjt = Common.checkJSONObjectInJSONObjectResponse(subentryJsonObject, "id");
                    videoId = videoIDObjt.getString("videoId");
                }
            }/*End of Apporach 2.*/
            if (videoId != null) {
                recentMediaStatisticsJson = Common.getResponseFromAPI(getYoutubeUserRecentVideoStatisticsURL(videoId, key));
            } else {
                recentMediaStatisticsJson = new JSONObject();
            }

            if ((Common.checkJSONArrayInJSONObjectResponse(recentMediaStatisticsJson, "items")) != null) {
                rMStatisticsArray = Common.checkJSONArrayInJSONObjectResponse(recentMediaStatisticsJson, "items");
            } else {
                rMStatisticsArray = new JSONArray();
            }
            if (rMStatisticsArray.length() != 0) {
                subrMStatisticsObj = rMStatisticsArray.getJSONObject(0);
            } else {
                subrMStatisticsObj = new JSONObject();
            }
            if ((Common.checkJSONObjectInJSONObjectResponse(subrMStatisticsObj, "statistics")) != null) {
                rMstatisticsObject = Common.checkJSONObjectInJSONObjectResponse(subrMStatisticsObj, "statistics");
            }
            /*UserProfileandRecentActiviteJosn[2] = snippetJson;Recent video Title*/
 /*UserProfileandRecentActiviteJosn[3] = mediumJson;Recent video Image*/
            UserProfileandRecentActiviteJosn[4] = rMstatisticsObject;/*Recent video statistics i.e likes,shares,dislikes*/
        } catch (Exception e) {
            logger.error("Exceptin in getYouTubeUserProfileJosnObject", e);
        }
    }

    /*Returns URL based on channel userName*/
    public static String getYoutubeUserProfileURL(String userName) {
        /*Commented on 05-06-2015 due to API CHANGES "http://gdata.youtube.com/feeds/api/users/"+ userName + "?alt=json&v=2";*/
        logger.debug("Request URL in getYoutubeUserProfileURL: " + "https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername=" + userName + "&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername=" + userName + "&key=" + APIKeys.googleKey + "";
    }

    /*Returns URL based on channel ID*/
    public static String getYoutubeUserProfile(String channelId) {
        /*Commented on 05-06-2015 due to API CHANGES "http://gdata.youtube.com/feeds/api/users/"+ userName + "?alt=json&v=2";*/
        logger.debug("Request URL in getYoutubeUserProfile: " + "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=" + channelId + "&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=" + channelId + "&key=" + APIKeys.googleKey + "";
    }

    public static String getYoutubeUserVideoIdURL(String userName) {
        /*Commented on 05-06-2015 due to API CHANGES "https://gdata.youtube.com/feeds/api/users/"+ userName + "/events?v=2&alt=json&key="+youTubeAPIKey+"";*/
        logger.debug("Request URL in getYoutubeUserVideoIdURL: " + "https://www.googleapis.com/youtube/v3/activities?part=id%2Csnippet%2CcontentDetails&channelId=" + userName + "&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/youtube/v3/activities?part=id%2Csnippet%2CcontentDetails&channelId=" + userName + "&key=" + APIKeys.googleKey + "";
    }

    public static String getYoutubeUserVideoIdURLV2(String userName) {
        logger.debug("Request URL in getYoutubeUserVideoIdURLV2: " + "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + userName + "&order=date&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + userName + "&order=date&key=" + APIKeys.googleKey + "";
    }

    public String getYoutubeUserRecentVideoStatisticsURL(String vidoeId, String viodeoId) {
        logger.debug("Request URL in getYoutubeUserRecentVideoStatisticsURL: " + "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + vidoeId + "&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + vidoeId + "&key=" + APIKeys.googleKey + "";
    }

    /*Returns subscribers of  youtube channel
                (OR) 
    Sends the exception message to support mail
     */
    public long checkYouTubeAPIStatus(String userName) {
        long pageSubscribers = 0;
        JSONObject youtubeJson, subEntryyoutubeJson, statisticsJson = null;
        JSONArray entryYouTubeJson = null;
        try {
            /*Checking user existence's based on channel name */
            youtubeJson = Common.getResponseFromAPI(getYoutubeUserProfileURL(userName));
            if (youtubeJson != null && youtubeJson.has("items")) {
                entryYouTubeJson = youtubeJson.getJSONArray("items");
            }
            if (entryYouTubeJson != null && entryYouTubeJson.length() != 0) {
                subEntryyoutubeJson = entryYouTubeJson.getJSONObject(0);
                if (subEntryyoutubeJson.has("statistics")) {
                    statisticsJson = subEntryyoutubeJson.getJSONObject("statistics");
                }
            }
            if (statisticsJson != null && statisticsJson.has("subscriberCount")) {
                /*Subscribers count*/
                pageSubscribers = statisticsJson.getLong("subscriberCount");
            }
            if (statisticsJson == null || pageSubscribers == 0) {
                logger.info("NO subscribers count is found for youtube channel::" + userName + ", ");
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.YOUTUBE_DATA_V3);
                errorObj.setToolId(29);
                errorObj.setErrorMessage("Dialy alert error message:: "+(youtubeJson != null ? youtubeJson.toString() : ("No JSON resposne found for Youtube API for userName:: " + userName)));
                APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER, Boolean.FALSE);
            }
        } catch (JSONException e) {
            logger.error("Exceptin in  checkYouTubeAPIStatus cause:: ", e);
        }
        return pageSubscribers;
    }
}