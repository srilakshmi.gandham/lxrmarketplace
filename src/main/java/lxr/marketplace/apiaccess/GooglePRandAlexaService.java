package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import lxr.marketplace.seoninja.JenkinsHash;

public class GooglePRandAlexaService {
	public int getGooglePR(String domain) {
		String result = "";
		JenkinsHash jenkinsHash = new JenkinsHash();
		long hash = jenkinsHash.hash(("info:" + domain).getBytes());

		// Append a 6 in front of the hashing value.
		String url = "http://toolbarqueries.google.com/tbr?client=navclient-auto&hl=en&" + "ch=6"
				+ hash + "&ie=UTF-8&oe=UTF-8&features=Rank&q=info:" + domain;
//		System.out.println("Sending request to : " + url);
		try {
			URLConnection conn = new URL(url).openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String input;
			while ((input = br.readLine()) != null) {
				result = input.substring(input.lastIndexOf(":") + 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if ("".equals(result)) {
			return 0;
		} else {
			return Integer.valueOf(result);
		}
	}
	
//	public void getResults(String domain, DomainInfo domainOverview) {
	public long getAlexaRank(String domain) {
		// HttpSession session = request.getSession(true);
		// svtool= new SeoToolKit();
		String url = "http://data.alexa.com/data?cli=10&dat=snbamz&url=" + domain;
		long alexRank = 0;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(url);
			doc.getDocumentElement().normalize();
//			System.out.println("url:" + url + "\n" + "Root element: " + doc.getDocumentElement().getNodeName());
			// loop through each item
			NodeList items = doc.getElementsByTagName("SD");
//			System.out.println("sd elements:" + items.getLength());
			for (int i = 0; i < items.getLength(); i++) {
				Node n = items.item(i);
				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;
				Element e = (Element) n;
				if (i == 0) {
					// get the "title element" from this item (only one)
					NodeList titleList = e.getElementsByTagName("TITLE");
					Element titleElement = (Element) titleList.item(0);
//					System.out.println("TITLE TEXT : " + titleElement.getAttribute("TEXT"));
					NodeList createdDateList = e.getElementsByTagName("CREATED");
					Element createdDateElement = (Element) createdDateList.item(0);
//					System.out.println("CREATED DATE : " + createdDateElement.getAttribute("DATE"));
				}
				if (i == 1) {
					NodeList priority = e.getElementsByTagName("POPULARITY");
					Element alexaRank = (Element) priority.item(0);
//					System.out.println("URL : " + alexaRank.getAttribute("URL"));
//					System.out.println("alexaRank : " + alexaRank.getAttribute("TEXT"));
					if(alexaRank.getAttribute("TEXT") != null && 
							!alexaRank.getAttribute("TEXT").equals("")){
						alexRank = Long.parseLong(alexaRank.getAttribute("TEXT"));
//						competitorResearchTool.setAlexaTraficRank(Integer.parseInt(alexaRank.getAttribute("TEXT")));
					}
				}

			}
		} catch (SAXParseException err) {
//			System.out.println("** Parsing error" + ", line "
//					+ err.getLineNumber() + ", uri " + err.getSystemId());
//			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			/*Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();*/

		} catch (Exception e) {
			/*e.printStackTrace();*/
		}
		if("".equals(alexRank)){
			return 0;
		}else{
			return alexRank;
		}
	}

}