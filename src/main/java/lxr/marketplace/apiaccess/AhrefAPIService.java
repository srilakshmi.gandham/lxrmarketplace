package lxr.marketplace.apiaccess;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;

import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefRefDomains;
import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

public class AhrefAPIService {

    private static Logger logger = Logger.getLogger(AhrefAPIService.class);
    private String ahrefAPIKey;
    private String ahrefAPIUrl;

    public void setAhrefAPIKey(String ahrefAPIKey) {
        this.ahrefAPIKey = ahrefAPIKey;
    }

    public void setAhrefAPIUrl(String ahrefAPIUrl) {
        this.ahrefAPIUrl = ahrefAPIUrl;
    }

    public String getFirst100BackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, int offset) {
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&target=" + domainName + "&mode=subdomains&limit=100"
                + "&from=backlinks&order_by=ahrefs_rank%3Adesc&output=json";
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        if (jsonObject != null) {
            String error = parseAhrefJson(jsonObject, backLinks);
            return error;
        } else {
            return "No Data";
        }
    }

    public String getBackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, int offset, long limit) {
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&target=" + domainName + "&mode=subdomains&limit=" + limit
                + "&from=backlinks&order_by=ahrefs_rank%3Adesc&output=json";
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        if (jsonObject != null) {
            String error = parseAhrefJson(jsonObject, backLinks);
            return error;
        } else {
            return "No Data";
        }
    }

    public String getReferringDomainsData(String domainName, ArrayList<AhrefRefDomains> referringDoamins, int limit) {
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&target=" + domainName + "&mode=domain&limit=" + limit
                + "&from=refdomains&output=json";
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        String error = null;
        if (jsonObject != null) {
            try {
                if (jsonObject.has("error")) {
                    String errorResponse = jsonObject.toString();
                    if (errorResponse != null && (!errorResponse.contains("bad") && !errorResponse.contains("billing error"))) {
                        APIError errorObj = new APIError();
                        errorObj.setApiName(APIErrorHandlingService.AHREF);
                        errorObj.setToolId(20);
                        errorObj.setErrorMessage(jsonObject.toString() + ", for domain: " + domainName+" for metric ReferringDomains");
                        APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.INBOUND_LINK_CHECKER,Boolean.TRUE);
                    }
                } else {
                    error = parseAhrefReferringDoaminsJson(jsonObject, referringDoamins);
                }
            } catch (Exception e) {
                logger.error("JSONException in getDomainLevelData: ", e);
            }
        } else {
            error = "No Data";
        }
        return error;
    }

    private String parseAhrefJson(JSONObject jsonObject, ArrayList<AhrefBacklink> backLinks) {
        String errorMessage = "";
        try {
            errorMessage = jsonObject.getString("error");
        } catch (JSONException e) {
        }
        if (errorMessage == null || (errorMessage.equals(""))) {
            try {
                JSONArray results = jsonObject.getJSONArray("refpages");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject result = results.getJSONObject(i);
                    addAhrefBacklinkObject(result, backLinks);
                }
            } catch (JSONException e) {
                logger.error(e);
                return "No Results";
            } catch (Exception e) {
                logger.error(e);
                return "No Results";
            }
            return "No Error";
        } else {
            return errorMessage;
        }
    }

    private void addAhrefBacklinkObject(JSONObject result, ArrayList<AhrefBacklink> backLinks) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat ahrefsFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        AhrefBacklink ahrefBacklink = new AhrefBacklink();
        try {
            String sourceUrl = result.getString("url_from");
            ahrefBacklink.setSourceURL(html2text(sourceUrl));
        } catch (JSONException | UnsupportedEncodingException e) {
            ahrefBacklink.setSourceURL("");
            logger.error("url_from not found in json result sent from Ahref");
        }
        try {
            String targetUrl = result.getString("url_to");
            ahrefBacklink.setTargetURL(html2text(targetUrl));
        } catch (JSONException | UnsupportedEncodingException e) {
            ahrefBacklink.setTargetURL("");
            logger.error("url_to not found in json result sent from Ahref");
        }
        try {
            float rating = Float.parseFloat(result.getString("ahrefs_rank"));
            ahrefBacklink.setRating(rating);
        } catch (JSONException e) {
            ahrefBacklink.setRating(0);
            logger.error("ahrefs_rank not found in json result sent from Ahref");
        }
        try {
            float domainRating = Float.parseFloat(result.getString("domain_rating"));
            ahrefBacklink.setDomainRating(domainRating);
        } catch (JSONException e) {
            ahrefBacklink.setDomainRating(0);
            logger.error("domain_rating not found in json result sent from Ahref");
        }
//        try {
//            String ip = result.getString("ip_from");
//            ahrefBacklink.setSourceIp(ip);
//        } catch (JSONException e) {
//            ahrefBacklink.setSourceIp("0.0.0.0");
//            logger.error("ip_from not found in json result sent from Ahref");
//        }
        try {
            String title = result.getString("title");
            ahrefBacklink.setTitle(title);
        } catch (JSONException e) {
            ahrefBacklink.setTitle("");
            logger.error("title not found in json result sent from Ahref");
        }
        try {
            long internalLinks = Long.parseLong(result.getString("links_internal"));
            ahrefBacklink.setInternalLinks(internalLinks);
        } catch (JSONException e) {
            ahrefBacklink.setInternalLinks(0);
            logger.error("links_internal not found in json result sent from Ahref");
        }
        try {
            long externalLinks = Long.parseLong(result.getString("links_external"));
            ahrefBacklink.setExternalLinks(externalLinks);
        } catch (JSONException e) {
            ahrefBacklink.setExternalLinks(0);
            logger.error("links_external not found in json result sent from Ahref");
        }
//        try {
//            long size = Long.parseLong(result.getString("page_size"));
//            ahrefBacklink.setSize(size);
//        } catch (JSONException e) {
//            ahrefBacklink.setSize(0);
//            logger.error("page_size not found in json result sent from Ahref");
//        }

        try {
            String lastVisited = result.getString("last_visited");
            Date lastVisitedDate = ahrefsFormat.parse(lastVisited);
            ahrefBacklink.setLastVisited(lastVisitedDate.getTime() / 1000);
            String visitedDateString = dateFormat.format(lastVisitedDate);
            ahrefBacklink.setLastVisitedDateString(visitedDateString);
        } catch (JSONException e) {
            ahrefBacklink.setLastVisited(Calendar.getInstance().getTimeInMillis() / 1000);
            Calendar cal = Calendar.getInstance();
            String visitedDateString = dateFormat.format(cal.getTime());
            ahrefBacklink.setLastVisitedDateString(visitedDateString);
            logger.error("last_visited not found in json result sent from Ahref");
        } catch (ParseException e) {
            ahrefBacklink.setLastVisited(Calendar.getInstance().getTimeInMillis() / 1000);
            Calendar cal = Calendar.getInstance();
            String visitedDateString = dateFormat.format(cal.getTime());
            ahrefBacklink.setLastVisitedDateString(visitedDateString);
            logger.error("unable to parse last_visited from Ahref");
        }
//        try {
//            String firstSeen = result.getString("first_seen");
//            Date firstSeenDate = ahrefsFormat.parse(firstSeen);
//            ahrefBacklink.setFirstSeen(firstSeenDate.getTime() / 1000);
//        } catch (JSONException e) {
//            ahrefBacklink.setFirstSeen(Calendar.getInstance().getTimeInMillis() / 1000);
//            logger.error("first_seen not found in json result sent from Ahref");
//        } catch (ParseException e) {
//            ahrefBacklink.setFirstSeen(Calendar.getInstance().getTimeInMillis() / 1000);
//            logger.error("unable to parse first_seen from Ahref");
//        }
        try {
            String anchorText = result.getString("anchor");
            ahrefBacklink.setAnchorText(anchorText);
        } catch (JSONException e) {
            ahrefBacklink.setAnchorText("");
            logger.error("anchor not found in json result sent from Ahref");
        }
        try {
            String anchorText = result.getString("alt");
            ahrefBacklink.setAltText(anchorText);
        } catch (JSONException e) {
            ahrefBacklink.setAltText("");
            logger.error("alt not found in json result sent from Ahref");
        }
        try {
            String type = result.getString("link_type");
            ahrefBacklink.setAnchorType(type);
        } catch (JSONException e) {
            ahrefBacklink.setAnchorType("");
            logger.error("link_type not found in json result sent from Ahref");
        }
        try {
            boolean noFollow = result.getBoolean("nofollow");
            if (noFollow) {
                ahrefBacklink.setNoFollowUrl("No Follow");
            } else {
                ahrefBacklink.setNoFollowUrl("Do Follow");
            }

            ahrefBacklink.setNoFollow(noFollow);
        } catch (JSONException e) {
            ahrefBacklink.setNoFollow(false);
            logger.error("nofollow not found in json result sent from Ahref");
        }
//        try {
//            String preText = result.getString("text_pre");
//            ahrefBacklink.setPreText(preText);
//        } catch (JSONException e) {
//            ahrefBacklink.setPreText("");
//            logger.error("text_pre not found in json result sent from Ahref");
//        }
//        try {
//            String postText = result.getString("text_post");
//            ahrefBacklink.setPostText(postText);
//        } catch (JSONException e) {
//            ahrefBacklink.setPostText("");
//            logger.error("text_post not found in json result sent from Ahref");
//        }
        backLinks.add(ahrefBacklink);
    }

    private String parseAhrefReferringDoaminsJson(JSONObject jsonObject, ArrayList<AhrefRefDomains> referringDoamins) {
        String errorMessage = "";
        try {
            errorMessage = jsonObject.getString("error");
        } catch (JSONException e) {
        }
        if (errorMessage.equals("")) {
            try {
                if (jsonObject.has("refdomains")) {
                    JSONArray results = jsonObject.getJSONArray("refdomains");
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject result = results.getJSONObject(i);
                        addAhrefReferringDomainObject(result, referringDoamins);
                    }
                }
            } catch (JSONException e) {
                logger.error("JSONException in parseAhrefReferringDoaminsJson cause: " + e.getMessage());
                return "No Results";
            } catch (Exception e) {
                logger.error("Exception in parseAhrefReferringDoaminsJson cause: " + e);
                return "No Results";
            }
            return "No Error";
        } else {
            return errorMessage;
        }
    }

    private void addAhrefReferringDomainObject(JSONObject result, ArrayList<AhrefRefDomains> referringDoamins) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat ahrefsFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        AhrefRefDomains ahrefRefDomain = new AhrefRefDomains();
        try {
            String targetUrl = result.getString("refdomain");
            ahrefRefDomain.setRefdomain(targetUrl);
        } catch (JSONException e) {
            ahrefRefDomain.setRefdomain("");
            logger.error("refdomain not found in json result sent from Ahref");
        }

        try {
            int backlinks = Integer.parseInt(result.getString("backlinks"));
            ahrefRefDomain.setBacklinks(backlinks);
        } catch (JSONException e) {
            ahrefRefDomain.setBacklinks(0);
            logger.error("backlinks not found in json result sent from Ahref");
        }
        try {
            int refpages = Integer.parseInt(result.getString("refpages"));
            ahrefRefDomain.setRefpages(refpages);
        } catch (JSONException e) {
            ahrefRefDomain.setRefpages(0);
            logger.error("refpages not found in json result sent from Ahref");
        }

        try {
            float domainRating = Float.parseFloat(result.getString("domain_rating"));
            ahrefRefDomain.setDomainRating(domainRating);
        } catch (JSONException e) {
            ahrefRefDomain.setDomainRating(0);
            logger.error("domain_rating not found in json result sent from Ahref");
        }
        try {
            String firstSeen = result.getString("first_seen");
            Date firstSeenDate = ahrefsFormat.parse(firstSeen);
            String firstSeenString = dateFormat.format(firstSeenDate);
            ahrefRefDomain.setFirstSeen(firstSeenString);
        } catch (JSONException e) {
            Calendar cal = Calendar.getInstance();
            String firstSeenString = dateFormat.format(cal.getTime());
            ahrefRefDomain.setFirstSeen(firstSeenString);
            logger.error("first_seen not found in json result sent from Ahref");
        } catch (ParseException e) {
            Calendar cal = Calendar.getInstance();
            String firstSeenString = dateFormat.format(cal.getTime());
            ahrefRefDomain.setFirstSeen(firstSeenString);
            logger.error("unable to parse first_seen from Ahref");
        }

        try {
            String lastVisited = result.getString("last_visited");
            Date lastVisitedDate = ahrefsFormat.parse(lastVisited);
            String lastVisitedString = dateFormat.format(lastVisitedDate);
            ahrefRefDomain.setLastVisited(lastVisitedString);
        } catch (JSONException e) {
            Calendar cal = Calendar.getInstance();
            String lastVisitedString = dateFormat.format(cal.getTime());
            ahrefRefDomain.setLastVisited(lastVisitedString);
            logger.error("last_visited not found in json result sent from Ahref");
        } catch (ParseException e) {
            Calendar cal = Calendar.getInstance();
            String lastVisitedString = dateFormat.format(cal.getTime());
            ahrefRefDomain.setLastVisited(lastVisitedString);
            logger.error("unable to parse last_visited from Ahref");
        }

        referringDoamins.add(ahrefRefDomain);
    }

    public long fetchNumberOfBacklinks(String domainName) {
        long backlinksCount = 0;
        if (domainName != null && !domainName.isEmpty()) {
            String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&from=metrics_extended&target=" + domainName
                    + "&mode=subdomains&output=json";
            logger.debug("Query URL for fetchNumberOfBacklinks: " + queryUrl);
            JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
            if (jsonObject != null) {
                try {
                    if (jsonObject.has("metrics")) {
                        JSONObject innerObject = jsonObject.getJSONObject("metrics");
                        if (innerObject.has("backlinks")) {
                            String backlinks = innerObject.getString("backlinks");
                            if (backlinks != null && !backlinks.equals("")) {
                                backlinksCount = Long.parseLong(backlinks);
                            }
                        }
                    }
                } catch (JSONException | NumberFormatException e) {
                    logger.error("JSONException | NumberFormatException in fetchNumberOfBacklinks cause: " + e.getMessage());
                }
            }
        }
        return backlinksCount;
    }

    public long fetchNumberOfBacklinksForaURL(String url) {
        long backlinksCount = 0;
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&from=metrics_extended&target=" + url
                + "&mode=exact&output=json";
        logger.debug("Query URL for fetchNumberOfBacklinksForaURL: " + queryUrl);
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        if (jsonObject != null) {
            try {
                if (jsonObject.has("metrics")) {
                    JSONObject innerObject = jsonObject.getJSONObject("metrics");
                    if (innerObject.has("backlinks")) {
                        String backlinks = innerObject.getString("backlinks");
                        if (backlinks != null && !backlinks.equals("")) {
                            backlinksCount = Long.parseLong(backlinks);
                        }
                    }
                }
            } catch (JSONException | NumberFormatException e) {
                logger.error("JSONException | NumberFormatException in fetchNumberOfBacklinksForaURL cause: " + e.getMessage());
            }
        }
        return backlinksCount;
    }

    public AhrefBackLinkDomainInfo getDomainLevelData(String domain) {
        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&from=metrics_extended&target=" + domain + "&mode=subdomains&output=json";
        logger.debug("Query URL get DomainLevelData from Ahref: " + queryUrl);
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        if (jsonObject != null) {
            try {
                if (jsonObject.has("error")) {
                    String errorResponse = jsonObject.toString();
                    if (errorResponse != null && (!errorResponse.contains("bad") && !errorResponse.contains("billing error"))) {
                        APIError errorObj = new APIError();
                        errorObj.setApiName(APIErrorHandlingService.AHREF);
                        errorObj.setToolId(20);
                        errorObj.setErrorMessage(jsonObject.toString()+", for domain: "+domain+" for metric Domain level data");
                        APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.INBOUND_LINK_CHECKER,Boolean.TRUE);
                    }
                } else {
                    ahrefBackLinkDomainInfo = new AhrefBackLinkDomainInfo();
                    addAhrefBacklinkObject(jsonObject, ahrefBackLinkDomainInfo);
                }
            } catch (Exception e) {
                logger.error("JSONException in getDomainLevelData: ", e);
            }
        }
        return ahrefBackLinkDomainInfo;
    }

    private void addAhrefBacklinkObject(JSONObject result, AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo) {
        try {
            String backlinks = result.getJSONObject("metrics").getString("backlinks");
            if (backlinks != null && !backlinks.equals("")) {
                ahrefBackLinkDomainInfo.setAhrefsbackLinks(Long.parseLong(backlinks));
            }

        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String refdomains = result.getJSONObject("metrics").getString("refdomains");
            if (refdomains != null && !refdomains.equals("")) {
                ahrefBackLinkDomainInfo.setRefdomains(Long.parseLong(refdomains));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }
        try {
            String noFollow = result.getJSONObject("metrics").getString("nofollow");
            if (noFollow != null && !noFollow.equals("")) {
                ahrefBackLinkDomainInfo.setNoFollowLinks(Long.parseLong(noFollow));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String doFollow = result.getJSONObject("metrics").getString("dofollow");
            if (doFollow != null && !doFollow.equals("")) {
                ahrefBackLinkDomainInfo.setDoFollowLinks(Long.parseLong(doFollow));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String canonical = result.getJSONObject("metrics").getString("canonical");
            if (canonical != null && !canonical.equals("")) {
                ahrefBackLinkDomainInfo.setCanonical(Long.parseLong(canonical));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String govLinksCount = result.getJSONObject("metrics").getString("gov");
            if (govLinksCount != null && !govLinksCount.equals("")) {
                ahrefBackLinkDomainInfo.setGovLinks(Long.parseLong(govLinksCount));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String eduLinksCount = result.getJSONObject("metrics").getString("edu");
            if (eduLinksCount != null && !eduLinksCount.equals("")) {
                ahrefBackLinkDomainInfo.setEduLinks(Long.parseLong(eduLinksCount));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String rssLinksCount = result.getJSONObject("metrics").getString("rss");
            if (rssLinksCount != null && !rssLinksCount.equals("")) {
                ahrefBackLinkDomainInfo.setRssLinks(Long.parseLong(rssLinksCount));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String internalLinksCount = result.getJSONObject("metrics").getString("links_internal");
            if (internalLinksCount != null && !internalLinksCount.equals("")) {
                ahrefBackLinkDomainInfo.setInternalLinks(Long.parseLong(internalLinksCount));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String externalLinksCount = result.getJSONObject("metrics").getString("links_external");
            if (externalLinksCount != null && !externalLinksCount.equals("")) {
                ahrefBackLinkDomainInfo.setExternalLinks(Long.parseLong(externalLinksCount));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String refPages = result.getJSONObject("metrics").getString("refpages");
            if (refPages != null && !refPages.equals("")) {
                ahrefBackLinkDomainInfo.setRefPages(Long.parseLong(refPages));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        try {
            String html_pages = result.getJSONObject("metrics").getString("html_pages");
            if (html_pages != null && !html_pages.equals("")) {
                ahrefBackLinkDomainInfo.setHtmlPages(Long.parseLong(html_pages));
            }
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

    }

    public long[] fetchBacklinkCheckerHeaderData(String domain) {
        long[] backlinkMetrics = {0l, 0l, 0l, 0l};
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&from=metrics_extended&target=" + domain
                + "&mode=subdomains&output=json";
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        if (jsonObject != null) {
            try {
                String backlinks = jsonObject.getJSONObject("metrics").getString("backlinks");
                if (backlinks != null && !backlinks.equals("")) {
                    backlinkMetrics[0] = Long.parseLong(backlinks);
                }
            } catch (JSONException | NumberFormatException e) {
                logger.error(e);
            }
            try {
                String refdomains = jsonObject.getJSONObject("metrics").getString("refdomains");
                if (refdomains != null && !refdomains.equals("")) {
                    backlinkMetrics[1] = Long.parseLong(refdomains);
                }
            } catch (JSONException | NumberFormatException e) {
                logger.error(e);
            }
            try {
                String noFollow = jsonObject.getJSONObject("metrics").getString("nofollow");
                if (noFollow != null && !noFollow.equals("")) {
                    backlinkMetrics[2] = Long.parseLong(noFollow);
                }
            } catch (JSONException | NumberFormatException e) {
                logger.error(e);
            }

            try {
                String doFollow = jsonObject.getJSONObject("metrics").getString("dofollow");
                if (doFollow != null && !doFollow.equals("")) {
                    backlinkMetrics[3] = Long.parseLong(doFollow);
                }
            } catch (JSONException | NumberFormatException e) {
                logger.error(e);
            }
        }
        return backlinkMetrics;
    }

    public int fetchDomainRating(String domain) {
        String queryUrl = ahrefAPIUrl + "?token=" + ahrefAPIKey + "&from=domain_rating&target=" + domain
                + "&mode=domain&output=json";
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        int domainRating = 0;
        try {
            domainRating = jsonObject.getJSONObject("domain").getInt("domain_rating");
        } catch (JSONException | NumberFormatException e) {
            logger.error(e);
        }

        return domainRating;
    }

    public String html2text(String html) throws UnsupportedEncodingException {
        return Jsoup.parse(URLDecoder.decode(html, "UTF-8")).text();
    }
}