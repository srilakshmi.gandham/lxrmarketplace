package lxr.marketplace.apiaccess;

import lxr.marketplace.util.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GooglePlusAPIService {
	private String apiURL = "https://clients6.google.com/rpc?";
	private String apiKey = "AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ";
	public static void main(String [] args){
		GooglePlusAPIService googlePlusAPIService = new GooglePlusAPIService();
		googlePlusAPIService.fetchGooglePlusURLData("http://www.lxrmarketplace.com/");
	}
	
	public long fetchGooglePlusURLData(String url){
		String queryURL = apiURL+"key=" + apiKey;
//		System.out.println("Google+ API URL: " + queryURL);
		JSONArray postObject = null;
		long googlePlusShare = 0;
		try {
			postObject = createPostObject(url);		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			JSONArray response = Common.sendPostURLwithJSON(queryURL, postObject);
			//sample response : 
			//[{ "id": "p", "result": {  "kind": "pos#plusones",  "id": "http://www.netelixir.com/",  
			//"isSetByViewer": false,  "metadata": {"type": "URL","globalCounts": {"count": 48.0}  },
			//"abtk": "AEIZW7Sum9pCOYiDeitd7I2zVL29lq1c0JNXPV5qq9/J8ICHrqHoyF4CxfFqs1qcZWAL9kyWjIS4"}}]
			googlePlusShare = parseJson(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return googlePlusShare;
	}
	
	private JSONArray createPostObject(String url) throws JSONException{
		JSONObject params = new JSONObject();
		params.put("nolog", true);
		params.put("id", url);
		params.put("source", "widget");
		params.put("userId", "@viewer");
		params.put("groupId", "@self");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("method", "pos.plusones.get");
		jsonObject.put("id", "p");
		jsonObject.put("params", params);
		
		jsonObject.put("jsonrpc", "2.0");
		jsonObject.put("key", "p");
		jsonObject.put("apiVersion", "v1");
		JSONArray array = new JSONArray();
		array.put(jsonObject);

		return array;
	}
	
	private long parseJson(JSONArray response){
		long gShares = 0;
		try {
			JSONObject jsonObject = response.getJSONObject(0);
			JSONObject result = jsonObject.getJSONObject("result");
			JSONObject metadata = result.getJSONObject("metadata");
			JSONObject globalCounts = metadata.getJSONObject("globalCounts");
			String count = globalCounts.getString("count");
			gShares = (long) Double.parseDouble(count);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return gShares;
	}
}
