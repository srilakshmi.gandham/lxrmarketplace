
package lxr.marketplace.apiaccess.lxrmbeans;
public class PinterestMetrics {
    
	private boolean pinterestAccountStatus;
	private String pinterestUserName;
	
        private int  followers; 
	private int following;
	private int boards;
	private int pins;
//        private int likes;
        
	private int postRepins;
	private int postComments;
	private int postLikes;
	private int engagement;
	private double audienceEngaged = 0.0;
	private String postText = null;
	private String postDate = null;
	private boolean pinterestImage = false;
	private String pinterestImageUrl = null;
	
        private int followersChange;
        private int followingChange;
        private int pinsChange;
        private int likesChange;
        private int boardsChange;
        
        
	
	public PinterestMetrics(){
		pinterestUserName=" ";
                followers=0;
		following=0;
		boards=0;
		pins=0;
//                likes=0;
		postComments=0;
		postLikes=0;
		postRepins=0;
		engagement=0;
		audienceEngaged=0.0;
		postDate=" ";
		pinterestImage=false;
		pinterestImageUrl=" ";
		postText=" ";
	followersChange=0;
        followingChange=0;
        pinsChange=0;
        likesChange=0;
        boardsChange=0;
        this.pinterestAccountStatus = false;
	}
       
	public String getPinterestUserName() {
		return pinterestUserName;
	}

	public void setPinterestUserName(String pinterestUserName) {
		this.pinterestUserName = pinterestUserName;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}

	public int getBoards() {
		return boards;
	}

	public void setBoards(int boards) {
		this.boards = boards;
	}

	public int getPins() {
		return pins;
	}

	public void setPins(int pins) {
		this.pins = pins;
	}

	public int getPostRepins() {
		return postRepins;
	}

	public void setPostRepins(int postRepins) {
		this.postRepins = postRepins;
	}

	public int getPostComments() {
		return postComments;
	}

	public void setPostComments(int postComments) {
		this.postComments = postComments;
	}

	public int getPostLikes() {
		return postLikes;
	}

	public void setPostLikes(int postLikes) {
		this.postLikes = postLikes;
	}

	public int getEngagement() {
		return engagement;
	}

	public void setEngagement(int engagement) {
		this.engagement = engagement;
	}

	public double getAudienceEngaged() {
		return audienceEngaged;
	}

	public void setAudienceEngaged(double audienceEngaged) {
		this.audienceEngaged = audienceEngaged;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public boolean isPinterestImage() {
		return pinterestImage;
	}

	public void setPinterestImage(boolean pinterestImage) {
		this.pinterestImage = pinterestImage;
	}

	public String getPinterestImageUrl() {
		return pinterestImageUrl;
	}

	public void setPinterestImageUrl(String pinterestImageUrl) {
		this.pinterestImageUrl = pinterestImageUrl;
	}

//    public int getLikes() {
//        return likes;
//    }
//
//    public void setLikes(int likes) {
//        this.likes = likes;
//    }

    public int getFollowersChange() {
        return followersChange;
    }

    public void setFollowersChange(int followersChange) {
        this.followersChange = followersChange;
    }

    public int getFollowingChange() {
        return followingChange;
    }

    public void setFollowingChange(int followingChange) {
        this.followingChange = followingChange;
    }

    public int getPinsChange() {
        return pinsChange;
    }

    public void setPinsChange(int pinsChange) {
        this.pinsChange = pinsChange;
    }

    public int getLikesChange() {
        return likesChange;
    }

    public void setLikesChange(int likesChange) {
        this.likesChange = likesChange;
    }

    public int getBoardsChange() {
        return boardsChange;
    }

    public void setBoardsChange(int boardsChange) {
        this.boardsChange = boardsChange;
    }

    public boolean isPinterestAccountStatus() {
        return pinterestAccountStatus;
    }

    public void setPinterestAccountStatus(boolean pinterestAccountStatus) {
        this.pinterestAccountStatus = pinterestAccountStatus;
    }

   
    
    
    
	
    
}
