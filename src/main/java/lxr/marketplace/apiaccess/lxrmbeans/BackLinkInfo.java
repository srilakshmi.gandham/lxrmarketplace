package lxr.marketplace.apiaccess.lxrmbeans;

import java.util.Calendar;

public class BackLinkInfo {
	
	private String sourceURL;
	private String anchorText;
	private Calendar lastCrawledDate;
	private boolean noFollow;
	private String targetURL;
	private float targetCitationFlow;
	private float targetTrustFlow; 
	private float sourceCitationFlow;
	private float sourceTrustFlow;
	
	public BackLinkInfo(){
		
	}
	
	public BackLinkInfo(String sourceURL, String anchorText,Calendar lastCrawledDate, boolean noFollow, String targetURL,
			float targetCitationFlow, float targetTrustFlow, float sourceCitationFlow, float sourceTrustFlow) {
		super();
		this.sourceURL = sourceURL;
		this.anchorText = anchorText;
		this.lastCrawledDate = lastCrawledDate;
		this.noFollow = noFollow;
		this.targetURL = targetURL;
		this.targetCitationFlow = targetCitationFlow;
		this.targetTrustFlow = targetTrustFlow;
		this.sourceCitationFlow = sourceCitationFlow;
		this.sourceTrustFlow = sourceTrustFlow;
	}

	public String getSourceURL() {
		return sourceURL;
	}
	public void setSourceURL(String sourceURL) {
		this.sourceURL = sourceURL;
	}
	public String getAnchorText() {
		return anchorText;
	}
	public void setAnchorText(String anchorText) {
		this.anchorText = anchorText;
	}
	public Calendar getLastCrawledDate() {
		return lastCrawledDate;
	}
	public void setLastCrawledDate(Calendar lastCrawledDate) {
		this.lastCrawledDate = lastCrawledDate;
	}
	public boolean isNoFollow() {
		return noFollow;
	}
	public void setNoFollow(boolean noFollow) {
		this.noFollow = noFollow;
	}
	public String getTargetURL() {
		return targetURL;
	}
	public void setTargetURL(String targetURL) {
		this.targetURL = targetURL;
	}
	public float getTargetCitationFlow() {
		return targetCitationFlow;
	}
	public void setTargetCitationFlow(float targetCitationFlow) {
		this.targetCitationFlow = targetCitationFlow;
	}
	public float getTargetTrustFlow() {
		return targetTrustFlow;
	}
	public void setTargetTrustFlow(float targetTrustFlow) {
		this.targetTrustFlow = targetTrustFlow;
	}
	public float getSourceCitationFlow() {
		return sourceCitationFlow;
	}
	public void setSourceCitationFlow(float sourceCitationFlow) {
		this.sourceCitationFlow = sourceCitationFlow;
	}
	public float getSourceTrustFlow() {
		return sourceTrustFlow;
	}
	public void setSourceTrustFlow(float sourceTrustFlow) {
		this.sourceTrustFlow = sourceTrustFlow;
	}	
}
