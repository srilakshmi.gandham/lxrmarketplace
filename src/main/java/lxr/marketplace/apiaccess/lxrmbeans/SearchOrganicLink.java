package lxr.marketplace.apiaccess.lxrmbeans;

public class SearchOrganicLink {
	private String url;
	private String displayUrl;
	private String title;
	private String snippet;
	private int position;
	
	public SearchOrganicLink(){
		url = "";
		displayUrl="";
		title = "";
		snippet = "";
	}
	
	public SearchOrganicLink(String url, String displayUrl, String title, String snippet) {
		super();
		this.url = url;
		this.displayUrl = displayUrl;
		this.title = title;
		this.snippet = snippet;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getUrl() {
		return url;
	}

	public String getDisplayUrl() {
		return displayUrl;
	}

	public String getTitle() {
		return title;
	}

	public String getSnippet() {
		return snippet;
	}
	
}
