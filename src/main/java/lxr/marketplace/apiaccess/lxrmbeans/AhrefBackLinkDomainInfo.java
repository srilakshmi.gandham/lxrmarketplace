/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess.lxrmbeans;

/**
 *
 * @author santosh
 */
public class AhrefBackLinkDomainInfo {

    private long domainId;
    private String domainName;
    private long fetchedBackLinks;
    private long ahrefsbackLinks;
    private long refPages;
    private long noFollowLinks;
    private long doFollowLinks;
    private long canonical;
    private long govLinks;
    private long eduLinks;
    private long rssLinks;
    private long internalLinks;
    private long externalLinks;
    private long refdomains;
    private long googleIndexedPages;
    private long htmlPages;
    private String backLinkFetchedDate;
    private String refDomainFetchedDate;

    public long getDomainId() {
        return domainId;
    }

    public void setDomainId(long domainId) {
        this.domainId = domainId;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public long getFetchedBackLinks() {
        return fetchedBackLinks;
    }

    public void setFetchedBackLinks(long fetchedBackLinks) {
        this.fetchedBackLinks = fetchedBackLinks;
    }

    public long getAhrefsbackLinks() {
        return ahrefsbackLinks;
    }

    public void setAhrefsbackLinks(long ahrefsbackLinks) {
        this.ahrefsbackLinks = ahrefsbackLinks;
    }

    public long getRefPages() {
        return refPages;
    }

    public void setRefPages(long refPages) {
        this.refPages = refPages;
    }

    public long getNoFollowLinks() {
        return noFollowLinks;
    }

    public void setNoFollowLinks(long noFollowLinks) {
        this.noFollowLinks = noFollowLinks;
    }

    public long getDoFollowLinks() {
        return doFollowLinks;
    }

    public void setDoFollowLinks(long doFollowLinks) {
        this.doFollowLinks = doFollowLinks;
    }

    public long getCanonical() {
        return canonical;
    }

    public void setCanonical(long canonical) {
        this.canonical = canonical;
    }

    public long getGovLinks() {
        return govLinks;
    }

    public void setGovLinks(long govLinks) {
        this.govLinks = govLinks;
    }

    public long getEduLinks() {
        return eduLinks;
    }

    public void setEduLinks(long eduLinks) {
        this.eduLinks = eduLinks;
    }

    public long getRssLinks() {
        return rssLinks;
    }

    public void setRssLinks(long rssLinks) {
        this.rssLinks = rssLinks;
    }

    public long getInternalLinks() {
        return internalLinks;
    }

    public void setInternalLinks(long internalLinks) {
        this.internalLinks = internalLinks;
    }

    public long getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(long externalLinks) {
        this.externalLinks = externalLinks;
    }

    public long getRefdomains() {
        return refdomains;
    }

    public void setRefdomains(long refdomains) {
        this.refdomains = refdomains;
    }

    public long getGoogleIndexedPages() {
        return googleIndexedPages;
    }

    public void setGoogleIndexedPages(long googleIndexedPages) {
        this.googleIndexedPages = googleIndexedPages;
    }

    public String getBackLinkFetchedDate() {
        return backLinkFetchedDate;
    }

    public void setBackLinkFetchedDate(String backLinkFetchedDate) {
        this.backLinkFetchedDate = backLinkFetchedDate;
    }

    public long getHtmlPages() {
        return htmlPages;
    }

    public void setHtmlPages(long htmlPages) {
        this.htmlPages = htmlPages;
    }

    public String getRefDomainFetchedDate() {
        return refDomainFetchedDate;
    }

    public void setRefDomainFetchedDate(String refDomainFetchedDate) {
        this.refDomainFetchedDate = refDomainFetchedDate;
    }

    @Override
    public String toString() {
        return "AhrefBackLinkDomainInfo{" + "domainId=" + domainId + ", domainName=" + domainName + ", fetchedBackLinks=" + fetchedBackLinks + ", ahrefsbackLinks=" + ahrefsbackLinks + ", refPages=" + refPages + ", noFollowLinks=" + noFollowLinks + ", doFollowLinks=" + doFollowLinks + ", canonical=" + canonical + ", govLinks=" + govLinks + ", eduLinks=" + eduLinks + ", rssLinks=" + rssLinks + ", internalLinks=" + internalLinks + ", externalLinks=" + externalLinks + ", refdomains=" + refdomains + ", googleIndexedPages=" + googleIndexedPages + ", htmlPages=" + htmlPages + ", backLinkFetchedDate=" + backLinkFetchedDate + ", refDomainFetchedDate=" + refDomainFetchedDate + '}';
    }
    
    
}
