package lxr.marketplace.apiaccess.lxrmbeans;

public class FacebookMetrics {

    private String name;

    private long likesCount;
    private String userName;
    private String id;

    private long engagement;
    private double audienceEngaged;

    private boolean facebookAccountStatus;

    private long postLikes;
    private long postcomments;
    private long postShares;
    private String postImagePath;
    private String postdescription;
    private String postCreatedTime;
    private int change;
    private int faceBookActivityChange;
    private int faceBookEngagementChange;
    private boolean postImageStatus;

    public FacebookMetrics() {
        likesCount = 0;
        userName = null;
        id = null;
        engagement = 0;
        audienceEngaged = 0.0;
        facebookAccountStatus = false;
        postLikes = 0;
        postcomments = 0;
        postShares = 0;
        postImagePath = null;
        postdescription = null;
        postCreatedTime = null;
        change = 0;
        faceBookActivityChange = 0;
        faceBookEngagementChange = 0;
        postImageStatus = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public long getEngagement() {
        return engagement;
    }

    public void setEngagement(long engagement) {
        this.engagement = engagement;
    }

    public double getAudienceEngaged() {
        return audienceEngaged;
    }

    public void setAudienceEngaged(double audienceEngaged) {
        this.audienceEngaged = audienceEngaged;
    }

    public boolean isFacebookAccountStatus() {
        return facebookAccountStatus;
    }

    public void setFacebookAccountStatus(boolean facebookAccountStatus) {
        this.facebookAccountStatus = facebookAccountStatus;
    }

    public String getPostdescription() {
        return postdescription;
    }

    public void setPostdescription(String postdescription) {
        this.postdescription = postdescription;
    }

    public long getPostLikes() {
        return postLikes;
    }

    public void setPostLikes(long postLikes) {
        this.postLikes = postLikes;
    }

    public long getPostcomments() {
        return postcomments;
    }

    public void setPostcomments(long postcomments) {
        this.postcomments = postcomments;
    }

    public String getPostImagePath() {
        return postImagePath;
    }

    public void setPostImagePath(String postImagePath) {
        this.postImagePath = postImagePath;
    }

    public long getPostShares() {
        return postShares;
    }

    public void setPostShares(long postShares) {
        this.postShares = postShares;
    }

    public String getPostCreatedTime() {
        return postCreatedTime;
    }

    public void setPostCreatedTime(String postCreatedTime) {
        this.postCreatedTime = postCreatedTime;
    }

    public boolean isPostImageStatus() {
        return postImageStatus;
    }

    public void setPostImageStatus(boolean postImageStatus) {
        this.postImageStatus = postImageStatus;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getFaceBookActivityChange() {
        return faceBookActivityChange;
    }

    public void setFaceBookActivityChange(int faceBookActivityChange) {
        this.faceBookActivityChange = faceBookActivityChange;
    }

    public int getFaceBookEngagementChange() {
        return faceBookEngagementChange;
    }

    public void setFaceBookEngagementChange(int faceBookEngagementChange) {
        this.faceBookEngagementChange = faceBookEngagementChange;
    }

}
