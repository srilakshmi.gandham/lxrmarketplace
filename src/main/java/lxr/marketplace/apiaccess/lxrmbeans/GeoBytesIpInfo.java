package lxr.marketplace.apiaccess.lxrmbeans;

public class GeoBytesIpInfo {
	private String ip;
	private String country;
	private String city;
	private String region;
	private String countryCode;
	
	
	public GeoBytesIpInfo(String ip) {
		super();
		this.ip = ip;
	}
	public GeoBytesIpInfo(String ip, String country, String city, String region,
			String countryCode) {
		super();
		this.ip = ip;
		this.country = country;
		this.city = city;
		this.region = region;
		this.countryCode = countryCode;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
}
