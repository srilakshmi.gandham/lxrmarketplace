package lxr.marketplace.apiaccess.lxrmbeans;

import java.util.Calendar;

public class DomainInfo {
	private String domain;
	private long alexaRank;
	private long refDomainCount;
	private long extBackLinkCount;
	private long indexedURLCount;
	private long crawledURLCount;
	private Calendar firstCrawldate;
	private Calendar lastCrawlDate;
	private String countryCode;
	private float citationFlow;
	private float trustFlow;

	public DomainInfo() {

	}

	public DomainInfo(String domain, long alexaRank, long refDomainCount, long extBackLinkCount,
			long indexedURLCount, long crawledURLCount, Calendar firstCrawldate,
			Calendar lastCrawlDate, String countryCode, float citationFlow, float trustFlow) {
		super();
		this.domain = domain;
		this.alexaRank = alexaRank;
		this.refDomainCount = refDomainCount;
		this.extBackLinkCount = extBackLinkCount;
		this.indexedURLCount = indexedURLCount;
		this.crawledURLCount = crawledURLCount;
		this.firstCrawldate = firstCrawldate;
		this.lastCrawlDate = lastCrawlDate;
		this.countryCode = countryCode;
		this.citationFlow = citationFlow;
		this.trustFlow = trustFlow;
	}
	public String toString(){
		return "Domain: "+ domain +" | alexaRank: "+ alexaRank +" | Ref. Domains: "
				+ refDomainCount +" | Backlinks: "+ extBackLinkCount +" | Indexed URLs: "+ indexedURLCount
				+" | Citation Flow: "+ citationFlow +" | Trust Flow: "+ trustFlow;
		
	}
	
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public long getAlexaRank() {
		return alexaRank;
	}

	public void setAlexaRank(long alexaRank) {
		this.alexaRank = alexaRank;
	}

	public long getRefDomainCount() {
		return refDomainCount;
	}

	public void setRefDomainCount(long refDomainCount) {
		this.refDomainCount = refDomainCount;
	}

	public long getExtBackLinkCount() {
		return extBackLinkCount;
	}

	public void setExtBackLinkCount(long extBackLinkCount) {
		this.extBackLinkCount = extBackLinkCount;
	}

	public long getIndexedURLCount() {
		return indexedURLCount;
	}

	public void setIndexedURLCount(long indexedURLCount) {
		this.indexedURLCount = indexedURLCount;
	}

	public long getCrawledURLCount() {
		return crawledURLCount;
	}

	public void setCrawledURLCount(long crawledURLCount) {
		this.crawledURLCount = crawledURLCount;
	}

	public Calendar getFirstCrawldate() {
		return firstCrawldate;
	}

	public void setFirstCrawldate(Calendar firstCrawldate) {
		this.firstCrawldate = firstCrawldate;
	}

	public Calendar getLastCrawlDate() {
		return lastCrawlDate;
	}

	public void setLastCrawlDate(Calendar lastCrawlDate) {
		this.lastCrawlDate = lastCrawlDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public float getCitationFlow() {
		return citationFlow;
	}

	public void setCitationFlow(float citationFlow) {
		this.citationFlow = citationFlow;
	}

	public float getTrustFlow() {
		return trustFlow;
	}

	public void setTrustFlow(float trustFlow) {
		this.trustFlow = trustFlow;
	}
}
