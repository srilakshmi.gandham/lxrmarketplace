package lxr.marketplace.apiaccess.lxrmbeans;

public class InstagramaMetrics {
	private String userName;
	private long Posts;
	private long followers;
	private long follwoing;
	private long recentMediaComments;
	private long recentMediaLikes;
	private long engagement;
	private double audienceEngaged = 0.0;

	private String postDate = null;
	private boolean InstagramImage = false;
	private String InstapostImageUrl = null;
	private String postText = null;
	private double change = 0.0;
	private double instagramActivityChange;
	private double instagramEngagementChange;

	public double getInstagramEngagementChange() {
		return instagramEngagementChange;
	}

	public void setInstagramEngagementChange(double instagramEngagementChange) {
		this.instagramEngagementChange = instagramEngagementChange;
	}

	private boolean instagramAccountStatus;

	public InstagramaMetrics() {

		this.userName = null;
		this.Posts = 0;
		this.followers = 0;
		this.follwoing = 0;
		this.recentMediaComments = 0;
		this.recentMediaLikes = 0;
		this.engagement = 0;
		this.postDate = null;
		this.InstagramImage = false;
		this.InstapostImageUrl = null;
		this.postText = null;
		this.audienceEngaged = 0.0;
		this.instagramAccountStatus = false;
		this.change = 0.0;
		this.instagramActivityChange=0.0;
		this.instagramEngagementChange=0.0;

	}

	public InstagramaMetrics(String userName, long posts, long followers,
			long follwoing, long recentMediaComments, long recentMediaLikes,
			String postDate, boolean InstagramImage, String InstapostImageUrl,
			String postText, boolean instagramAccountStatus,
			double audienceEngaged, long engagement) {
		super();
		this.userName = userName;
		this.Posts = posts;
		this.followers = followers;
		this.follwoing = follwoing;
		this.recentMediaComments = recentMediaComments;
		this.recentMediaLikes = recentMediaLikes;
		this.engagement = engagement;
		this.audienceEngaged = audienceEngaged;
		this.postDate = postDate;
		this.InstagramImage = InstagramImage;
		this.InstapostImageUrl = InstapostImageUrl;
		this.postText = postText;
		this.instagramAccountStatus = instagramAccountStatus;

	}

	public InstagramaMetrics(String userName, long posts, long followers,
			long follwoing, long recentMediaComments, long recentMediaLikes,
			String postDate, boolean InstagramImage, String InstapostImageUrl,
			String postText, boolean instagramAccountStatus,
			double audienceEngaged, long engagement, double change) {
		super();
		this.userName = userName;
		this.Posts = posts;
		this.followers = followers;
		this.follwoing = follwoing;
		this.recentMediaComments = recentMediaComments;
		this.recentMediaLikes = recentMediaLikes;
		this.engagement = engagement;
		this.audienceEngaged = audienceEngaged;
		this.postDate = postDate;
		this.InstagramImage = InstagramImage;
		this.InstapostImageUrl = InstapostImageUrl;
		this.postText = postText;
		this.instagramAccountStatus = instagramAccountStatus;
		this.setChange(change);

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getPosts() {
		return Posts;
	}

	public void setPosts(long posts) {
		Posts = posts;
	}

	public long getFollowers() {
		return followers;
	}

	public void setFollowers(long followers) {
		this.followers = followers;
	}

	public long getFollwoing() {
		return follwoing;
	}

	public void setFollwoing(long follwoing) {
		this.follwoing = follwoing;
	}

	public long getRecentMediaComments() {
		return recentMediaComments;
	}

	public void setRecentMediaComments(long recentMediaComments) {
		this.recentMediaComments = recentMediaComments;
	}

	public long getRecentMediaLikes() {
		return recentMediaLikes;
	}

	public void setRecentMediaLikes(long recentMediaLikes) {
		this.recentMediaLikes = recentMediaLikes;
	}

	public long getEngagement() {
		return engagement;
	}

	public void setEngagement(long engagement) {
		this.engagement = engagement;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public boolean isInstagramImage() {
		return InstagramImage;
	}

	public void setInstagramImage(boolean instagramImage) {
		InstagramImage = instagramImage;
	}

	public String getInstapostImageUrl() {
		return InstapostImageUrl;
	}

	public void setInstapostImageUrl(String instapostImageUrl) {
		InstapostImageUrl = instapostImageUrl;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public double getAudienceEngaged() {
		return audienceEngaged;
	}

	public void setAudienceEngaged(double audienceEngaged) {
		this.audienceEngaged = audienceEngaged;
	}

	public boolean isInstagramAccountStatus() {
		return instagramAccountStatus;
	}

	public void setInstagramAccountStatus(boolean instagramAccountStatus) {
		this.instagramAccountStatus = instagramAccountStatus;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}

	public double getInstagramActivityChange() {
		return instagramActivityChange;
	}

	public void setInstagramActivityChange(double instagramActivityChange) {
		this.instagramActivityChange = instagramActivityChange;
	}

}