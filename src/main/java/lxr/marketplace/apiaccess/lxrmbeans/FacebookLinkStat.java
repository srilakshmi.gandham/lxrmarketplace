package lxr.marketplace.apiaccess.lxrmbeans;

public class FacebookLinkStat {
	private long likeCount;
	private long totalCount;
	private long shareCount;
	private long clickCount;
	private long commentCount;
	public FacebookLinkStat(long likeCount, long totalCount, long shareCount,
			long clickCount, long commentCount) {
		super();
		this.likeCount = likeCount;
		this.totalCount = totalCount;
		this.shareCount = shareCount;
		this.clickCount = clickCount;
		this.commentCount = commentCount;
	}
	public long getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(long likeCount) {
		this.likeCount = likeCount;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public long getShareCount() {
		return shareCount;
	}
	public void setShareCount(long shareCount) {
		this.shareCount = shareCount;
	}
	public long getClickCount() {
		return clickCount;
	}
	public void setClickCount(long clickCount) {
		this.clickCount = clickCount;
	}
	public long getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}
}
