package lxr.marketplace.apiaccess.lxrmbeans;

import java.io.Serializable;

public class TwitterMetrics implements Serializable {

    private String userName;
    private long followers;
    private long following;
    private long favourites;
    private long tweets;
    private String recentTweetText;
    private long recentTweetRetweets;
    private long recentTweetFavourites;
    private long engagement;
    private double audienceEngaged;
    private boolean imageStatus;
    private String readPath;
    private String tweetDate;
    private int change;
    private int twitteractivityChange;//Tweets change
    private int twitterFollowingChange;//
    private int twitterFavouritesChange;//
    private String twitterHashTag;

    private boolean twitterAccountStatus;

    public TwitterMetrics() {
        this.userName = null;
        this.followers = 0;
        this.following = 0;
        this.favourites = 0;
        this.recentTweetText = null;
        this.twitterHashTag = null;
        this.recentTweetRetweets = 0;
        this.recentTweetFavourites = 0;
        this.tweets = 0;
        this.engagement = 0;
        this.readPath = null;
        this.imageStatus = false;
        this.tweetDate = null;
        this.audienceEngaged = 0.0;
        this.twitterAccountStatus = false;
        this.change = 0;
        this.twitteractivityChange = 0;
        this.twitterFollowingChange = 0;
        this.twitterFavouritesChange = 0;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public long getFollowing() {
        return following;
    }

    public void setFollowing(long following) {
        this.following = following;
    }

    public long getTweets() {
        return tweets;
    }

    public void setTweets(long tweets) {
        this.tweets = tweets;
    }

    public String getRecentTweetText() {
        return recentTweetText;
    }

    public void setRecentTweetText(String recentTweetText) {
        this.recentTweetText = recentTweetText;
    }

    public long getRecentTweetRetweets() {
        return recentTweetRetweets;
    }

    public void setRecentTweetRetweets(long recentTweetRetweets) {
        this.recentTweetRetweets = recentTweetRetweets;
    }

    public long getRecentTweetFavourites() {
        return recentTweetFavourites;
    }

    public void setRecentTweetFavourites(long recentTweetFavourites) {
        this.recentTweetFavourites = recentTweetFavourites;
    }

    public long getFavourites() {
        return favourites;
    }

    public void setFavourites(long favourites) {
        this.favourites = favourites;
    }

    public long getEngagement() {
        return engagement;
    }

    public void setEngagement(long engagement) {
        this.engagement = engagement;
    }

    public boolean isImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(boolean imageStatus) {
        this.imageStatus = imageStatus;
    }

    public String getReadPath() {
        return readPath;
    }

    public void setReadPath(String readPath) {
        this.readPath = readPath;
    }

    public String getTweetDate() {
        return tweetDate;
    }

    public void setTweetDate(String tweetDate) {
        this.tweetDate = tweetDate;
    }

    public double getAudienceEngaged() {
        return audienceEngaged;
    }

    public void setAudienceEngaged(double audienceEngaged) {
        this.audienceEngaged = audienceEngaged;
    }

    public boolean isTwitterAccountStatus() {
        return twitterAccountStatus;
    }

    public void setTwitterAccountStatus(boolean twitterAccountStatus) {
        this.twitterAccountStatus = twitterAccountStatus;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getTwitteractivityChange() {
        return twitteractivityChange;
    }

    public void setTwitteractivityChange(int twitteractivityChange) {
        this.twitteractivityChange = twitteractivityChange;
    }

    public int getTwitterFollowingChange() {
        return twitterFollowingChange;
    }

    public void setTwitterFollowingChange(int twitterFollowingChange) {
        this.twitterFollowingChange = twitterFollowingChange;
    }

    public int getTwitterFavouritesChange() {
        return twitterFavouritesChange;
    }

    public void setTwitterFavouritesChange(int twitterFavouritesChange) {
        this.twitterFavouritesChange = twitterFavouritesChange;
    }

    public String getTwitterHashTag() {
        return twitterHashTag;
    }

    public void setTwitterHashTag(String twitterHashTag) {
        this.twitterHashTag = twitterHashTag;
    }

}
