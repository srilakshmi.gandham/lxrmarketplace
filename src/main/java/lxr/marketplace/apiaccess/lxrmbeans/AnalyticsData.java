package lxr.marketplace.apiaccess.lxrmbeans;

public class AnalyticsData {

    private long visits;
    private long conversions;
    private double revenue;
    private long organicVisits;
    private long organicConversions;
    private double organicRevenue;
    private long newUsers;

    public long getVisits() {
        return visits;
    }

    public void setVisits(long visits) {
        this.visits = visits;
    }

    public void setVisits(String visits) {
        try {
            this.visits = Long.parseLong(visits);
        } catch (NumberFormatException e) {
            this.visits = 0;
        }
    }

    public long getConversions() {
        return conversions;
    }

    public void setConversions(long conversions) {
        this.conversions = conversions;
    }

    public void setConversions(String conversions) {
        try {
            this.conversions = Long.parseLong(conversions);
        } catch (NumberFormatException e) {
            this.conversions = 0;
        }
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public void setRevenue(String revenue) {
        try {
            this.revenue = Double.parseDouble(revenue);
        } catch (NumberFormatException e) {
            this.revenue = 0;
        }
    }

    public long getOrganicVisits() {
        return organicVisits;
    }

    public void setOrganicVisits(long organicVisits) {
        this.organicVisits = organicVisits;
    }

    public void setOrganicVisits(String organicVisits) {
        try {
            this.organicVisits = Long.parseLong(organicVisits);
        } catch (NumberFormatException e) {
            this.organicVisits = 0;
        }
    }

    public long getOrganicConversions() {
        return organicConversions;
    }

    public void setOrganicConversions(long organicConversions) {
        this.organicConversions = organicConversions;
    }

    public void setOrganicConversions(String organicConversions) {
        try {
            this.organicConversions = Long.parseLong(organicConversions);
        } catch (NumberFormatException e) {
            this.organicConversions = 0;
        }
    }

    public double getOrganicRevenue() {
        return organicRevenue;
    }

    public void setOrganicRevenue(double organicRevenue) {
        this.organicRevenue = organicRevenue;
    }

    public void setOrganicRevenue(String organicRevenue) {
        try {
            this.organicRevenue = Double.parseDouble(organicRevenue);
        } catch (NumberFormatException e) {
            this.organicRevenue = 0;
        }
    }

    public long getNewUsers() {
        return newUsers;
    }

    public void setNewUsers(long newUsers) {
        this.newUsers = newUsers;
    }

    public void setNewUsers(String newUsers) {
        try {
            this.newUsers = Long.parseLong(newUsers);
        } catch (NumberFormatException e) {
            this.newUsers = 0;
        }
    }

    public static class Builder {

        private long visits;
        private long conversions;
        private double revenue;
        private long organicVisits;
        private long organicConversions;
        private double organicRevenue;
        private long newUsers;

        public Builder visits(long visits) {
            this.visits = visits;
            return this;
        }

        public Builder visits(String visits) {
            try {
                this.visits = Long.parseLong(visits);
            } catch (NumberFormatException e) {
                this.visits = 0;
            }
            return this;
        }

        public Builder conversions(long conversions) {
            this.conversions = conversions;
            return this;
        }

        public Builder conversions(String conversions) {
            try {
                this.conversions = Long.parseLong(conversions);
            } catch (NumberFormatException e) {
                this.conversions = 0;
            }
            return this;
        }

        public Builder revenue(double revenue) {
            this.revenue = revenue;
            return this;
        }

        public Builder revenue(String revenue) {
            try {
                this.revenue = Double.parseDouble(revenue);
            } catch (NumberFormatException e) {
                this.revenue = 0;
            }
            return this;
        }

        public Builder organicVisits(long organicVisits) {
            this.organicVisits = organicVisits;
            return this;
        }

        public Builder organicVisits(String organicVisits) {
            try {
                this.organicVisits = Long.parseLong(organicVisits);
            } catch (NumberFormatException e) {
                this.organicVisits = 0;
            }
            return this;
        }

        public Builder organicConversions(long organicConversions) {
            this.organicConversions = organicConversions;
            return this;
        }

        public Builder organicConversions(String organicConversions) {
            try {
                this.organicConversions = Long.parseLong(organicConversions);
            } catch (NumberFormatException e) {
                this.organicConversions = 0;
            }
            return this;
        }

        public Builder organicRevenue(double organicRevenue) {
            this.organicRevenue = organicRevenue;
            return this;
        }

        public Builder organicRevenue(String organicRevenue) {
            try {
                this.organicRevenue = Double.parseDouble(organicRevenue);
            } catch (NumberFormatException e) {
                this.organicRevenue = 0;
            }
            return this;
        }

        public Builder newUsers(long newUsers) {
            this.newUsers = newUsers;
            return this;
        }

        public Builder newUsers(String newUsers) {
            try {
                this.newUsers = Long.parseLong(newUsers);
            } catch (NumberFormatException e) {
                this.newUsers = 0;
            }
            return this;
        }

        public AnalyticsData build() {
            return new AnalyticsData(this);
        }
    }

    public AnalyticsData() {

    }

    public AnalyticsData(Builder builder) {
        this.visits = builder.visits;
        this.conversions = builder.conversions;
        this.revenue = builder.revenue;
        this.organicVisits = builder.organicVisits;
        this.organicConversions = builder.organicConversions;
        this.organicRevenue = builder.organicRevenue;
        this.newUsers = builder.newUsers;
    }
}
