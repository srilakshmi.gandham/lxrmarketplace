package lxr.marketplace.apiaccess.lxrmbeans;

public class InstagramMetrics {
        private boolean instagramAccountStatus;
	private String userName;
	private long Posts;
	private long followers;
	private long follwoing;
	private long recentMediaComments;
	private long recentMediaLikes;
	private long engagement;
	private double audienceEngaged = 0.0;

	private String postDate = null;
	private boolean InstagramImage = false;
	private String InstapostImageUrl = null;
	private String postText = null;
	private int change;
	private int instagramActivityChange;
        private int follwoingChange;
	private int instagramEngagementChange;
	
	
	public InstagramMetrics() {

		this.userName = " ";
		this.Posts = 0;
		this.followers = 0;
		this.follwoing = 0;
		this.recentMediaComments = 0;
		this.recentMediaLikes = 0;
		this.engagement = 0;
		this.postDate =" ";
		this.InstagramImage = false;
		this.InstapostImageUrl =" ";
		this.postText =" ";
		this.audienceEngaged = 0.0;
		this.change = 0;
		this.instagramActivityChange=0;
		this.instagramEngagementChange=0;
                this.follwoingChange=0;
                this.instagramAccountStatus = false;
		
	}

	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getPosts() {
		return Posts;
	}
	public void setPosts(long posts) {
		Posts = posts;
	}
	public long getFollowers() {
		return followers;
	}
	public void setFollowers(long followers) {
		this.followers = followers;
	}
	public long getFollwoing() {
		return follwoing;
	}
	public void setFollwoing(long follwoing) {
		this.follwoing = follwoing;
	}
	public long getRecentMediaComments() {
		return recentMediaComments;
	}
	public void setRecentMediaComments(long recentMediaComments) {
		this.recentMediaComments = recentMediaComments;
	}
	public long getRecentMediaLikes() {
		return recentMediaLikes;
	}
	public void setRecentMediaLikes(long recentMediaLikes) {
		this.recentMediaLikes = recentMediaLikes;
	}
	public long getEngagement() {
		return engagement;
	}
	public void setEngagement(long engagement) {
		this.engagement = engagement;
	}
	public double getAudienceEngaged() {
		return audienceEngaged;
	}
	public void setAudienceEngaged(double audienceEngaged) {
		this.audienceEngaged = audienceEngaged;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public boolean isInstagramImage() {
		return InstagramImage;
	}
	public void setInstagramImage(boolean instagramImage) {
		InstagramImage = instagramImage;
	}
	public String getInstapostImageUrl() {
		return InstapostImageUrl;
	}
	public void setInstapostImageUrl(String instapostImageUrl) {
		InstapostImageUrl = instapostImageUrl;
	}
	public String getPostText() {
		return postText;
	}
	public void setPostText(String postText) {
		this.postText = postText;
	}

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getInstagramActivityChange() {
        return instagramActivityChange;
    }

    public void setInstagramActivityChange(int instagramActivityChange) {
        this.instagramActivityChange = instagramActivityChange;
    }

    public int getFollwoingChange() {
        return follwoingChange;
    }

    public void setFollwoingChange(int follwoingChange) {
        this.follwoingChange = follwoingChange;
    }

    public int getInstagramEngagementChange() {
        return instagramEngagementChange;
    }

    public void setInstagramEngagementChange(int instagramEngagementChange) {
        this.instagramEngagementChange = instagramEngagementChange;
    }

    public boolean isInstagramAccountStatus() {
        return instagramAccountStatus;
    }

    public void setInstagramAccountStatus(boolean instagramAccountStatus) {
        this.instagramAccountStatus = instagramAccountStatus;
    }
	
	

    
        
        
}
