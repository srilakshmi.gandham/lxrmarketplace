package lxr.marketplace.apiaccess.lxrmbeans;

public class GoogleMetrics {

    private String userName;
    private long followers;
    private long circledByCount;
    private String recentPostContent;
    private long recentPostReplies;
    private long recentPosplusoners;
    private long recentPostResharers;
    private long engagement;
    private double audienceEngaged;
    private boolean googleImageStatus = false;
    private String googleImagePath = null;
    private String googlePostDate = null;
    private boolean googleAccountStatus;
    private int change;
    private int googleActivityChange;
    private int googleEngagementChange;

    public GoogleMetrics() {
        this.userName = " ";
        this.followers = 0;
        this.circledByCount = 0;
        this.recentPostContent = " ";
        this.recentPostReplies = 0;
        this.recentPosplusoners = 0;
        this.recentPostResharers = 0;
        this.engagement = 0;
        this.setAudienceEngaged(0.0);
        this.setGoogleImagePath(null);
        this.setGoogleImageStatus(false);
        this.setGooglePostDate(null);
        this.googleAccountStatus = false;
        this.googleActivityChange = 0;
        this.change = 0;
        this.googleEngagementChange = 0;
    }

    public void setCircledByCount(long circledByCount) {
        this.circledByCount = circledByCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long l) {
        this.followers = l;
    }

    public long getCircledByCount() {
        return circledByCount;
    }

    public void setCircledByCount(int circledByCount) {
        this.circledByCount = circledByCount;
    }

    public String getRecentPostContent() {
        return recentPostContent;
    }

    public void setRecentPostContent(String recentPostContent) {
        this.recentPostContent = recentPostContent;
    }

    public long getRecentPostReplies() {
        return recentPostReplies;
    }

    public void setRecentPostReplies(long recentPostReplies) {
        this.recentPostReplies = recentPostReplies;
    }

    public long getRecentPosplusoners() {
        return recentPosplusoners;
    }

    public void setRecentPosplusoners(long recentPosplusoners) {
        this.recentPosplusoners = recentPosplusoners;
    }

    public long getRecentPostResharers() {
        return recentPostResharers;
    }

    public void setRecentPostResharers(long recentPostResharers) {
        this.recentPostResharers = recentPostResharers;
    }

    public long getEngagement() {
        return engagement;
    }

    public void setEngagement(long engagement) {
        this.engagement = engagement;
    }

    public String getGoogleImagePath() {
        return googleImagePath;
    }

    public void setGoogleImagePath(String googleImagePath) {
        this.googleImagePath = googleImagePath;
    }

    public String getGooglePostDate() {
        return googlePostDate;
    }

    public void setGooglePostDate(String googlePostDate) {
        this.googlePostDate = googlePostDate;
    }

    public double getAudienceEngaged() {
        return audienceEngaged;
    }

    public void setAudienceEngaged(double audienceEngaged) {
        this.audienceEngaged = audienceEngaged;
    }

    public boolean isGoogleAccountStatus() {
        return googleAccountStatus;
    }

    public void setGoogleAccountStatus(boolean googleAccountStatus) {
        this.googleAccountStatus = googleAccountStatus;
    }

    public boolean isGoogleImageStatus() {
        return googleImageStatus;
    }

    public void setGoogleImageStatus(boolean googleImageStatus) {
        this.googleImageStatus = googleImageStatus;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getGoogleActivityChange() {
        return googleActivityChange;
    }

    public void setGoogleActivityChange(int googleActivityChange) {
        this.googleActivityChange = googleActivityChange;
    }

    public int getGoogleEngagementChange() {
        return googleEngagementChange;
    }

    public void setGoogleEngagementChange(int googleEngagementChange) {
        this.googleEngagementChange = googleEngagementChange;
    }

}
