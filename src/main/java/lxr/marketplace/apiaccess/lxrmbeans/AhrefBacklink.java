package lxr.marketplace.apiaccess.lxrmbeans;

import java.io.Serializable;

public class AhrefBacklink implements Serializable {

    private String sourceURL;
    private String targetURL;
    private float rating;
    private float domainRating;
    private long internalLinks;
    private long externalLinks;
    private String title;
    private long lastVisited;
    private String lastVisitedDateString;
    private String altText;
    private String anchorText;
    private String anchorType;
    private boolean noFollow;
    private String noFollowUrl;

    public String getSourceURL() {
        return sourceURL;
    }

    public void setSourceURL(String sourceURL) {
        this.sourceURL = sourceURL;
    }

    public String getTargetURL() {
        return targetURL;
    }

    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public long getInternalLinks() {
        return internalLinks;
    }

    public void setInternalLinks(long internalLinks) {
        this.internalLinks = internalLinks;
    }

    public long getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(long externalLinks) {
        this.externalLinks = externalLinks;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(long lastVisited) {
        this.lastVisited = lastVisited;
    }

    public String getLastVisitedDateString() {
        return lastVisitedDateString;
    }

    public void setLastVisitedDateString(String lastVisitedDateString) {
        this.lastVisitedDateString = lastVisitedDateString;
    }

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public String getAnchorText() {
        return anchorText;
    }

    public void setAnchorText(String anchorText) {
        this.anchorText = anchorText;
    }

    public String getAnchorType() {
        return anchorType;
    }

    public void setAnchorType(String anchorType) {
        this.anchorType = anchorType;
    }

    public boolean isNoFollow() {
        return noFollow;
    }

    public void setNoFollow(boolean noFollow) {
        this.noFollow = noFollow;
    }

    public String getNoFollowUrl() {
        return noFollowUrl;
    }

    public void setNoFollowUrl(String noFollowUrl) {
        this.noFollowUrl = noFollowUrl;
    }

    public float getDomainRating() {
        return domainRating;
    }

    public void setDomainRating(float domainRating) {
        this.domainRating = domainRating;
    }

}
