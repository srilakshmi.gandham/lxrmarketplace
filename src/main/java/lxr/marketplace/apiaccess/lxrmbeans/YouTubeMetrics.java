package lxr.marketplace.apiaccess.lxrmbeans;

public class YouTubeMetrics {

    private String userName;
    private long subscribers;
    private long videos;
    private long views;
    private long recentMediaviews;
    private long recentMediaLikes;
    private long recentMediaDisLikes;
    private long recentMediaComments;
    private String recentMediaTitle;
    private long engagement;

    private String youtubeUserLogPath;
    private String userPostDate;
    private boolean imageStatus;
    private double audienceEngaged;
    private int change;
    private int youTubeActivityChange;
    private int youTubeViewsChange;
    private boolean youtubeAccountStatus;

    public YouTubeMetrics() {
        this.userName = null;
        this.subscribers = 0;
        this.videos = 0;
        this.views = 0;
        this.recentMediaviews = 0;
        this.recentMediaLikes = 0;
        this.recentMediaDisLikes = 0;
        this.recentMediaComments = 0;
        this.engagement = 0;

        this.youtubeUserLogPath = null;
        this.userPostDate = null;
        this.imageStatus = false;
        this.audienceEngaged = 0.0;
        this.youtubeAccountStatus = false;
        this.change = 0;
        this.youTubeActivityChange = 0;
        this.youTubeViewsChange = 0;
    }

    public long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(long subscribers) {
        this.subscribers = subscribers;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public long getRecentMediaviews() {
        return recentMediaviews;
    }

    public void setRecentMediaviews(long recentMediaviews) {
        this.recentMediaviews = recentMediaviews;
    }

    public long getRecentMediaLikes() {
        return recentMediaLikes;
    }

    public void setRecentMediaLikes(long recentMediaLikes) {
        this.recentMediaLikes = recentMediaLikes;
    }

    public long getRecentMediaDisLikes() {
        return recentMediaDisLikes;
    }

    public void setRecentMediaDisLikes(long recentMediaDisLikes) {
        this.recentMediaDisLikes = recentMediaDisLikes;
    }

    public long getRecentMediaComments() {
        return recentMediaComments;
    }

    public void setRecentMediaComments(long recentMediaComments) {
        this.recentMediaComments = recentMediaComments;
    }

    public String getRecentMediaTitle() {
        return recentMediaTitle;
    }

    public void setRecentMediaTitle(String recentMediaTitle) {
        this.recentMediaTitle = recentMediaTitle;
    }

    public long getEngagement() {
        return engagement;
    }

    public void setEngagement(long engagement) {
        this.engagement = engagement;
    }

    public String getYoutubeUserLogPath() {
        return youtubeUserLogPath;
    }

    public void setYoutubeUserLogPath(String youtubeUserLogPath) {
        this.youtubeUserLogPath = youtubeUserLogPath;
    }

    public String getUserPostDate() {
        return userPostDate;
    }

    public void setUserPostDate(String userPostDate) {
        this.userPostDate = userPostDate;
    }

    public boolean isImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(boolean imageStatus) {
        this.imageStatus = imageStatus;
    }

    public double getAudienceEngaged() {
        return audienceEngaged;
    }

    public void setAudienceEngaged(double audienceEngaged) {
        this.audienceEngaged = audienceEngaged;
    }

    public boolean isYoutubeAccountStatus() {
        return youtubeAccountStatus;
    }

    public void setYoutubeAccountStatus(boolean youtubeAccountStatus) {
        this.youtubeAccountStatus = youtubeAccountStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getVideos() {
        return videos;
    }

    public void setVideos(long videos) {
        this.videos = videos;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getYouTubeActivityChange() {
        return youTubeActivityChange;
    }

    public void setYouTubeActivityChange(int youTubeActivityChange) {
        this.youTubeActivityChange = youTubeActivityChange;
    }

    public int getYouTubeViewsChange() {
        return youTubeViewsChange;
    }

    public void setYouTubeViewsChange(int youTubeViewsChange) {
        this.youTubeViewsChange = youTubeViewsChange;
    }

}
