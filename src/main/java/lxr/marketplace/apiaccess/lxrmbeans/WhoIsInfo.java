package lxr.marketplace.apiaccess.lxrmbeans;

public class WhoIsInfo {
	
	
	//Domain Age Details
	private String domainName;
	private String domainAge;
	private String createdDate;
	private String updatedDate;
	private String regExpiration;
	private int httpStatus;
//	private String domainNameExt;
	

	//Registerer Details
	private String registeredDomName;
	private String registererName;
	private String emailOfRegisterer;
	
	//Registrant Details
	private String registrantName;
	private String registrantState;
	private String registrantCountry;
	private String registrantOrganization;
	private String registrantStreet;
	private String registrantCity;
	private String registrantPostalCode;
	private String registrantPhone;
	private String registrantEmail;
	
	//Administrative Contact Details
	private String adminName;
	private String adminOrganization;
	private String adminStreet;
	private String adminCity;
	private String adminState;
	private String adminPostalCode;
	private String adminCountry;
	private String adminPhone;
	private String adminEmail;
	
	//Technical Contact Details
	private String techName;
	private String techOrganization;
	private String techStreet;
	private String techCity;
	private String techState;
	private String techPostalCode;
	private String techCountry;
	private String techPhone;
	private String techEmail;
	
	
	public String getRegisteredDomName() {
		return registeredDomName;
	}
	public void setRegisteredDomName(String registeredDomName) {
		this.registeredDomName = registeredDomName;
	}
	public String getDomainAge() {
		return domainAge;
	}
	public void setDomainAge(String domainAge) {
		this.domainAge = domainAge;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getRegExpiration() {
		return regExpiration;
	}
	public void setRegExpiration(String regExpiration) {
		this.regExpiration = regExpiration;
	}
	public int getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
	public String getRegistererName() {
		return registererName;
	}
	public void setRegistererName(String registererName) {
		this.registererName = registererName;
	}
	public String getEmailOfRegisterer() {
		return emailOfRegisterer;
	}
	public void setEmailOfRegisterer(String emailOfRegisterer) {
		this.emailOfRegisterer = emailOfRegisterer;
	}
	public String getRegistrantName() {
		return registrantName;
	}
	public void setRegistrantName(String registrantName) {
		this.registrantName = registrantName;
	}
	public String getRegistrantState() {
		return registrantState;
	}
	public void setRegistrantState(String registrantState) {
		this.registrantState = registrantState;
	}
	public String getRegistrantCountry() {
		return registrantCountry;
	}
	public void setRegistrantCountry(String registrantCountry) {
		this.registrantCountry = registrantCountry;
	}
	public String getRegistrantOrganization() {
		return registrantOrganization;
	}
	public void setRegistrantOrganization(String registrantOrganization) {
		this.registrantOrganization = registrantOrganization;
	}
	public String getRegistrantStreet() {
		return registrantStreet;
	}
	public void setRegistrantStreet(String registrantStreet) {
		this.registrantStreet = registrantStreet;
	}
	public String getRegistrantCity() {
		return registrantCity;
	}
	public void setRegistrantCity(String registrantCity) {
		this.registrantCity = registrantCity;
	}
	public String getRegistrantPostalCode() {
		return registrantPostalCode;
	}
	public void setRegistrantPostalCode(String registrantPostalCode) {
		this.registrantPostalCode = registrantPostalCode;
	}
	public String getRegistrantPhone() {
		return registrantPhone;
	}
	public void setRegistrantPhone(String registrantPhone) {
		this.registrantPhone = registrantPhone;
	}
	public String getRegistrantEmail() {
		return registrantEmail;
	}
	public void setRegistrantEmail(String registrantEmail) {
		this.registrantEmail = registrantEmail;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminOrganization() {
		return adminOrganization;
	}
	public void setAdminOrganization(String adminOrganization) {
		this.adminOrganization = adminOrganization;
	}
	public String getAdminStreet() {
		return adminStreet;
	}
	public void setAdminStreet(String adminStreet) {
		this.adminStreet = adminStreet;
	}
	public String getAdminCity() {
		return adminCity;
	}
	public void setAdminCity(String adminCity) {
		this.adminCity = adminCity;
	}
	public String getAdminState() {
		return adminState;
	}
	public void setAdminState(String adminState) {
		this.adminState = adminState;
	}
	public String getAdminPostalCode() {
		return adminPostalCode;
	}
	public void setAdminPostalCode(String adminPostalCode) {
		this.adminPostalCode = adminPostalCode;
	}
	public String getAdminCountry() {
		return adminCountry;
	}
	public void setAdminCountry(String adminCountry) {
		this.adminCountry = adminCountry;
	}
	public String getAdminPhone() {
		return adminPhone;
	}
	public void setAdminPhone(String adminPhone) {
		this.adminPhone = adminPhone;
	}
	public String getAdminEmail() {
		return adminEmail;
	}
	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	public String getTechName() {
		return techName;
	}
	public void setTechName(String techName) {
		this.techName = techName;
	}
	public String getTechOrganization() {
		return techOrganization;
	}
	public void setTechOrganization(String techOrganization) {
		this.techOrganization = techOrganization;
	}
	public String getTechStreet() {
		return techStreet;
	}
	public void setTechStreet(String techStreet) {
		this.techStreet = techStreet;
	}
	public String getTechCity() {
		return techCity;
	}
	public void setTechCity(String techCity) {
		this.techCity = techCity;
	}
	public String getTechState() {
		return techState;
	}
	public void setTechState(String techState) {
		this.techState = techState;
	}
	public String getTechPostalCode() {
		return techPostalCode;
	}
	public void setTechPostalCode(String techPostalCode) {
		this.techPostalCode = techPostalCode;
	}
	public String getTechCountry() {
		return techCountry;
	}
	public void setTechCountry(String techCountry) {
		this.techCountry = techCountry;
	}
	public String getTechPhone() {
		return techPhone;
	}
	public void setTechPhone(String techPhone) {
		this.techPhone = techPhone;
	}
	public String getTechEmail() {
		return techEmail;
	}
	public void setTechEmail(String techEmail) {
		this.techEmail = techEmail;
	}
	
public String getRegistrantAddress(WhoIsInfo whoIsInfo){
      String registrantAddress="NA";

        if((whoIsInfo.getRegistrantStreet().equals("NA")) && (whoIsInfo.getRegistrantCity().equals("NA")) 
                && (whoIsInfo.getRegistrantState().equals("NA")) && (whoIsInfo.getRegistrantPostalCode().equals("NA"))){
            return registrantAddress;
        }else{
            registrantAddress = "";
            if(!(whoIsInfo.getRegistrantStreet().equals("NA"))){
                registrantAddress += whoIsInfo.getRegistrantStreet().trim()+", ";
            }
            if(!(whoIsInfo.getRegistrantCity().equals("NA"))){
                registrantAddress += whoIsInfo.getRegistrantCity().trim()+", ";
            }
            if(!(whoIsInfo.getRegistrantState().equals("NA"))){
                registrantAddress += whoIsInfo.getRegistrantState().trim()+", ";
            }
            if(!(whoIsInfo.getRegistrantPostalCode().equals("NA"))){
                registrantAddress += whoIsInfo.getRegistrantPostalCode().trim()+".";
            }
        }
            return registrantAddress;
        }
public String getTechnicalAddress(WhoIsInfo whoIsInfo){
    String technicalAddress = "NA";
    if(((whoIsInfo.getTechStreet().equals("NA"))) && ((whoIsInfo.getTechCity().equals("NA")))
        &&((whoIsInfo.getTechState().equals("NA"))) && ((whoIsInfo.getTechPostalCode().equals("NA")))){
        return technicalAddress;        
    }else{
        technicalAddress = "";
        if(!(whoIsInfo.getTechStreet().equals("NA"))){
           technicalAddress += whoIsInfo.getTechStreet().trim()+", ";
        }
        if(!(whoIsInfo.getTechCity().equals("NA"))){
            technicalAddress += whoIsInfo.getTechCity().trim()+", ";
        }
        if(!(whoIsInfo.getTechState().equals("NA"))){
            technicalAddress += whoIsInfo.getTechState().trim()+", ";
        }
        if(!(whoIsInfo.getTechPostalCode().equals("NA"))){
            technicalAddress += whoIsInfo.getTechPostalCode().trim()+".";
        }
    }
    return technicalAddress;
  
}
public String getAdministrativeAddress(WhoIsInfo whoIsInfo){
    String administrativeAddress="NA";
   if(((whoIsInfo.getAdminStreet().equals("NA"))) && ((whoIsInfo.getAdminCity().equals("NA")))
        && ((whoIsInfo.getAdminState().equals("NA"))) && ((whoIsInfo.getAdminPostalCode().equals("NA")))){
       return administrativeAddress;   
   }else{
       administrativeAddress = "";
       if(!(whoIsInfo.getAdminStreet().equals("NA"))){
           administrativeAddress += whoIsInfo.getAdminStreet()+", ";
        }
       if(!(whoIsInfo.getAdminCity().equals("NA"))){
           administrativeAddress += whoIsInfo.getAdminCity()+", ";
       }
       if(!(whoIsInfo.getAdminState().equals("NA"))){
           administrativeAddress += whoIsInfo.getAdminState()+", ";
       }
       if(!(whoIsInfo.getAdminPostalCode().equals("NA"))){
           administrativeAddress += whoIsInfo.getAdminPostalCode()+".";
       }
   }
    
   return administrativeAddress;   
}

}
