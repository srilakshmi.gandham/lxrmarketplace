/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess.lxrmbeans;

import java.io.Serializable;

/**
 *
 * @author NE16T1213-Sagar
 */
public class AhrefRefDomains implements Serializable{
    private String refdomain;
    private int backlinks;
    private int refpages;
    private float domainRating;
    private String firstSeen;
    private String lastVisited;

    public String getRefdomain() {
        return refdomain;
    }

    public void setRefdomain(String refdomain) {
        this.refdomain = refdomain;
    }

    public int getBacklinks() {
        return backlinks;
    }

    public void setBacklinks(int backlinks) {
        this.backlinks = backlinks;
    }

    public int getRefpages() {
        return refpages;
    }

    public void setRefpages(int refpages) {
        this.refpages = refpages;
    }

    public String getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(String firstSeen) {
        this.firstSeen = firstSeen;
    }

    public String getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(String lastVisited) {
        this.lastVisited = lastVisited;
    }

    public float getDomainRating() {
        return domainRating;
    }

    public void setDomainRating(float domainRating) {
        this.domainRating = domainRating;
    }

}
