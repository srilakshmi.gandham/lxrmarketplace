package lxr.marketplace.apiaccess;

import lxr.marketplace.apiaccess.lxrmbeans.FacebookMetrics;
import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Page;
import com.restfb.Version;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.json.JsonObject;
import com.restfb.json.JsonArray;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentHashMap;
import lxr.marketplace.socialmedia.APIKeys;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class FaceBookMetricsAPIService extends DefaultFacebookClient implements Runnable {

    private static Logger logger = Logger.getLogger(FaceBookMetricsAPIService.class);

    private String faceBookUser;

    private FacebookMetrics facebookMetrics;

    private Long socialUSerId;

    public String getFaceBookUser() {
        return faceBookUser;
    }

    public void setFaceBookUser(String faceBookUser) {
        this.faceBookUser = faceBookUser;
    }

    public Long getSocialUSerId() {
        return socialUSerId;
    }

    public void setSocialUSerId(Long socialUSerId) {
        this.socialUSerId = socialUSerId;
    }

    private ConcurrentHashMap<Integer, String> faceBookMap;

    public FaceBookMetricsAPIService() {
    }

    public FaceBookMetricsAPIService(String faceBookUser, ConcurrentHashMap<Integer, String> faceBookConcurrentHashMap) {
        this.faceBookUser = faceBookUser;
        this.faceBookMap = faceBookConcurrentHashMap;
    }

    public ConcurrentHashMap<Integer, String> getFaceBookMap() {
        return faceBookMap;
    }

    public void setFaceBookMap(ConcurrentHashMap<Integer, String> faceBookMap) {
        this.faceBookMap = faceBookMap;
    }

    public void setFacebookMetrics(FacebookMetrics facebookMetrics) {
        this.facebookMetrics = facebookMetrics;
    }

    public FacebookMetrics getFacebookMetrics() {
        return facebookMetrics;
    }

    @Override
    public void run() {
        try {
            this.setFacebookMetrics(getFacebookMetrics(faceBookUser));
        } catch (Exception e) {
            this.setFacebookMetrics(new FacebookMetrics());
            logger.error("Exception in Facebook API Serivce:", e);
        }
        faceBookMap.put(2, "facebook");
    }

    public FacebookMetrics getFacebookMetrics(String UserName) {
        return fetchFbPageRecords(UserName);
    }

    public static boolean checkFaceBookUser(String username) {
        return checkFaceBookUserNameExistence(username);
    }

    public static boolean checkFaceBookUserNameExistence(String userName) {
        try {
//            AccessToken accessToken = new DefaultFacebookClient(Version.LATEST).obtainAppAccessToken(APIKeys.faceBookAppId, APIKeys.faceBookAppSecret);
            //FacebookClient facebookClient = new DefaultFacebookClient(accessToken.getAccessToken(), Version.LATEST);
            FacebookClient facebookClient = new DefaultFacebookClient(APIKeys.faceBookAcessToken, Version.LATEST);
            facebookClient.fetchObject(userName, Page.class);
            return true;
        } catch (FacebookOAuthException e) {
            logger.error("Error from Facebook API, Response will be updated through mail cause:" + e.getMessage());
            /*
            com.restfb.exception.FacebookOAuthException: Received Facebook error response of type OAuthException:
            Error validating access token: The session has been invalidated because the user changed their password or 
            Facebook has changed the session for security reasons. (code 190, subcode 460)
             
            logger.error("Error from Facebook API, Response will be updated through mail cause:" + e.getMessage());
            if (!e.getMessage().contains(userName) && !e.getMessage().contains("803") && !e.getMessage().contains("subcode null") && e.getMessage().contains("Error validating access token")) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException from Facebook graph API cause: " + e.getMessage());
                }
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.FACEBOOK);
                errorObj.setToolId(29);
                errorObj.setErrorMessage(e.getMessage());
                APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER,Boolean.TRUE);
            }*/
        } catch (Exception e) {
            logger.error("Exception for checking face book user name existence: " + e.getMessage());
        }
        return false;
    }

    public FacebookMetrics fetchFbPageRecords(String userName) {
        facebookMetrics = new FacebookMetrics();
        if (!(userName.equals("NDA"))) {
            try {
                JsonObject pageJSONObject = null;
                JsonObject postJSONObject = null;
                JsonObject summaryJSONObject = null;
                JsonObject innerJSONObject = null;
                JsonArray innerJSONArray = null;
                String postId = null;
                FacebookClient facebookClient = null;
                try {
//                AccessToken apiAccessToken = new DefaultFacebookClient(Version.LATEST).obtainAppAccessToken(APIKeys.faceBookAppId, APIKeys.faceBookAppSecret);
                    facebookClient = new DefaultFacebookClient(APIKeys.faceBookAcessToken, APIKeys.faceBookAppSecret, Version.LATEST);
                    pageJSONObject = facebookClient.fetchObject(userName, JsonObject.class, Parameter.with("fields", "id,fan_count,likes,cover,picture,posts.limit(1)"));
                } catch (FacebookOAuthException e) {
                    logger.error("FacebookOAuthException from Facebook graph API cause: " + e.getMessage());
                } catch (Exception e) {
                    logger.error("Exception for checking face book user name existence: " + e.getMessage());
                }
                if (pageJSONObject != null) {
                    /*Page UserName*/
                    if (pageJSONObject.has("id")) {
                        facebookMetrics.setUserName(pageJSONObject.getString("id"));
                    }
                    /*Page Fans count/ Likes.*/
                    if (pageJSONObject.has("fan_count")) {
                        facebookMetrics.setLikesCount(Long.parseLong(pageJSONObject.getString("fan_count")));
                        facebookMetrics.setFacebookAccountStatus(Boolean.TRUE);
                    }
                    /*Page Recent Post Id.*/
                    if (pageJSONObject.has("posts")) {
                        innerJSONObject = pageJSONObject.getJsonObject("posts");
                        if (innerJSONObject != null && innerJSONObject.has("data")) {
                            innerJSONArray = innerJSONObject.getJsonArray("data");
                            if (innerJSONArray != null && innerJSONArray.length() > 0) {
                                summaryJSONObject = innerJSONArray.getJsonObject(0);
                                if (summaryJSONObject != null && summaryJSONObject.has("id")) {
                                    postId = summaryJSONObject.get("id").toString();
                                }
                                /*Page Recent Post Descripition.*/
                                if (summaryJSONObject != null && summaryJSONObject.has("message")) {
                                    facebookMetrics.setPostdescription(summaryJSONObject.get("message").toString());
                                }
                                if (summaryJSONObject != null && summaryJSONObject.has("story") && facebookMetrics.getPostdescription() == null) {
                                    facebookMetrics.setPostdescription(summaryJSONObject.get("story").toString());
                                }
                            }
                        }
                    }
                    /*To get Post object*/
                    if (postId != null && facebookClient != null) {
                        postJSONObject = facebookClient.fetchObject(postId, JsonObject.class, Parameter.with("fields", "full_picture,picture,created_time,comments.limit(0).summary(true),likes.limit(0).summary(true),reactions.limit(0).summary(true),shares"));
                        if (postJSONObject != null) {
                            if (postJSONObject.has("comments")) {
                                summaryJSONObject = postJSONObject.getJsonObject("comments");
                                /*To get summary object from post Comments objects*/
                                if (summaryJSONObject != null && summaryJSONObject.has("summary")) {
                                    innerJSONObject = summaryJSONObject.getJsonObject("summary");
                                    if (innerJSONObject != null && innerJSONObject.has("total_count")) {
                                        facebookMetrics.setPostcomments(Long.parseLong(innerJSONObject.getString("total_count")));
                                    }
                                }
                            }

                            /*To get summary object from post Likes objects*/
                            if (postJSONObject.has("likes")) {
                                summaryJSONObject = postJSONObject.getJsonObject("likes");
                                if (summaryJSONObject != null && summaryJSONObject.has("summary")) {
                                    innerJSONObject = summaryJSONObject.getJsonObject("summary");
                                    if (innerJSONObject != null && innerJSONObject.has("total_count")) {
                                        facebookMetrics.setPostLikes(Long.parseLong(innerJSONObject.getString("total_count")));
                                    }
                                }
                            }

                            if (postJSONObject.has("full_picture") && postJSONObject.getString("full_picture") != null) {
                                facebookMetrics.setPostImagePath(getImageReadPath(userName, postJSONObject.getString("full_picture").trim()));
                            } else if (postJSONObject.has("picture") && postJSONObject.getString("picture") != null) {
                                facebookMetrics.setPostImagePath(getImageReadPath(userName, postJSONObject.getString("picture").trim()));
                            }
                            if (postJSONObject.has("created_time") && postJSONObject.getString("created_time") != null) {
                                facebookMetrics.setPostCreatedTime(getFacebookFormatedDate(postJSONObject.getString("created_time").trim()));
                            }
                            if (postJSONObject.has("shares")) {
                                innerJSONObject = postJSONObject.getJsonObject("shares");
                                if (innerJSONObject != null && innerJSONObject.has("count")) {
                                    facebookMetrics.setPostShares(Long.parseLong(innerJSONObject.getString("count")));
                                }
                            }
                        }
                    }
                    if (facebookMetrics.getPostImagePath() == null && pageJSONObject.has("cover")) {
                        innerJSONObject = pageJSONObject.getJsonObject("cover");
                        if (innerJSONObject != null && innerJSONObject.has("source")) {
                            facebookMetrics.setPostImagePath(getImageReadPath(userName, innerJSONObject.getString("source").trim()));
                        }
                    }
                    if (facebookMetrics.getPostImagePath() == null && pageJSONObject.has("picture")) {
                        innerJSONObject = pageJSONObject.getJsonObject("picture");
                        if (innerJSONObject != null && innerJSONObject.has("data")) {
                            summaryJSONObject = innerJSONObject.getJsonObject("data");
                            if (summaryJSONObject != null && summaryJSONObject.has("url")) {
                                facebookMetrics.setPostImagePath(getImageReadPath(userName, summaryJSONObject.getString("url").trim()));
                            }
                        }
                    }
                }

                facebookMetrics.setEngagement(facebookMetrics.getPostLikes() + facebookMetrics.getPostcomments() + facebookMetrics.getPostShares());
                facebookMetrics.setAudienceEngaged(Common.getAudienceEngaged(facebookMetrics.getEngagement(), (facebookMetrics.getLikesCount())));
            } catch (Exception e) {
                logger.error("Exception for fetchFbPageRecords: ", e);
            }
        }
        return facebookMetrics;
    }

    /*Get recent post date*/
    public static String getFacebookFormatedDate(String postDate) {
        String faceBookDate = null;
        if (postDate != null) {
            try {
                SimpleDateFormat incomingFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                SimpleDateFormat outgoingFormat = new SimpleDateFormat("MMMM dd, yyyy");
                faceBookDate = outgoingFormat.format(incomingFormat.parse(postDate));
            } catch (Exception e) {
                logger.error("Exception for getFormatedDateFacebook", e);
            }
        }
        return faceBookDate;
    }

    /*Save recent post image in local disk and return path*/
    private String getImageReadPath(String userName, String postImageURL) {
        userName = userName.substring(1, userName.length() - 1);
        String postReadPath = null;
        String postWritepath = null;
        if (postImageURL != null) {
            postWritepath = "/disk2/lxrmarketplace/social_images/FaceBookPost" + userName + "." + Common.getImageFormat(postImageURL);
        }
        if (Common.resizeImageFromUrlandSave(postImageURL, postWritepath)) {
            postReadPath = "/social_images/FaceBookPost" + userName + "." + Common.getImageFormat(postImageURL);
        }
        return postReadPath;
    }

    private JsonObject serachObjectInJsonObject(JsonObject infoObj, String paramName) {
        JsonObject objName = null;
        try {
            objName = infoObj.getJsonObject(paramName);
        } catch (Exception e) {
            logger.error("Exception in serachObjectInJsonObject cause: " + e.getMessage());
        }
        return objName;
    }

    /*Returns likes of facebook page
            (OR) 
    Sends the exception message to support mail
     */
    public long checkGraphAPIStatus(String pageName) {
        /*Stores the likes of page
        (OR) When api status is down stores the exception message */
        long pageLikes = 0;
        String errorMessage = null;
        if (pageName != null) {
            JsonObject pageJSONObject = null;
            FacebookClient facebookClient = null;
            try {
//                AccessToken apiAccessToken = new DefaultFacebookClient(Version.LATEST).obtainAppAccessToken(APIKeys.faceBookAppId, APIKeys.faceBookAppSecret);
                facebookClient = new DefaultFacebookClient(APIKeys.faceBookAcessToken, APIKeys.faceBookAppSecret, Version.LATEST);
                pageJSONObject = facebookClient.fetchObject(pageName, JsonObject.class, Parameter.with("fields", "id,fan_count,likes,cover,picture,posts.limit(1)"));
            } catch (FacebookOAuthException e) {
                errorMessage = ":: " + e.getErrorMessage() + ", and error type message:: " + e.getErrorType() + ", exception message:: " + e.getMessage();
                logger.error("Error from Facebook API, Response will be updated through mail cause: ", e);
            } catch (Exception e) {
                errorMessage = ":: " + e.getMessage();
                logger.error("Exception in checkGraphAPIStatus cause:: " + e.getMessage());
            }
            if (pageJSONObject != null) {
                /*Page Fans count/ Likes.*/
                pageLikes = pageJSONObject.getLong("fan_count");
            }
            if (pageJSONObject == null || pageLikes == 0) {
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.FACEBOOK);
                errorObj.setToolId(29);
                errorObj.setErrorMessage("Dialy alert error message::: " + errorMessage);
                APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER, Boolean.FALSE);
            }
        }
        return pageLikes;
    }
}
