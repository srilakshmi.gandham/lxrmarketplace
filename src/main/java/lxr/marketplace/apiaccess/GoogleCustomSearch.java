package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import lxr.marketplace.apiaccess.lxrmbeans.SearchOrganicLink;
import lxr.marketplace.util.Common;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class GoogleCustomSearch {

    private static Logger logger = Logger.getLogger(GoogleCustomSearch.class);

    private static final int URL_COUNT_LIMIT = 100;

    private String googleCustomSearchEngnId;
    private String googleCustomSearchApiUrl;
    private String googleCustomSearchApiKeyLxrm;

    private static final String GOOGLE_SEARCH_QUERY_FOR_PROXY = "https://www.google.com/search?q=";
    private static final String GOOGLE_USAGE_LIMIT = "dailyLimitExceeded";
    public static final String GOOGLE_USAGE_LIMIT_ERROR = "Google fetch limit exceeded";

    private int proxyPort;
    private String proxyUserName;
    private String proxyPassword;
    private List<String> proxyHost;

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public void setProxyUserName(String proxyUserName) {
        this.proxyUserName = proxyUserName;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public void setProxyHost(List<String> proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getGoogleCustomSearchEngnId() {
        return googleCustomSearchEngnId;
    }

    public void setGoogleCustomSearchEngnId(String googleCustomSearchEngnId) {
        this.googleCustomSearchEngnId = googleCustomSearchEngnId;
    }

    public String getGoogleCustomSearchApiUrl() {
        return googleCustomSearchApiUrl;
    }

    public void setGoogleCustomSearchApiUrl(String googleCustomSearchApiUrl) {
        this.googleCustomSearchApiUrl = googleCustomSearchApiUrl;
    }

    public void setGoogleCustomSearchApiKeyLxrm(String googleCustomSearchApiKeyLxrm) {
        this.googleCustomSearchApiKeyLxrm = googleCustomSearchApiKeyLxrm;
    }

    public long countIndexedPagesInGoogle(String googleCustSearchApiKey, String domain) {
//        long indexedPages;
//        indexedPages = getIndexedPagesByCustomSearch(googleCustSearchApiKey, domain);
//        if (indexedPages == 0) {
//            indexedPages = getIndexPagesByScreenScraping(domain);
//        }
//        return indexedPages;
//    }
//    public long getIndexedPagesByCustomSearch(String googleCustSearchApiKey, String domain) {
        long indexedPages = 0;
        try {
            String queryString = "key=" + googleCustSearchApiKey + "&cx=" + googleCustomSearchEngnId
                    + "&googlehost=google.com&ie=utf8&oe=utf8&filter=0&q="
                    + URLEncoder.encode("site:" + domain, "UTF-8");

//            JSONObject jsonObject = fetchSearchData(googleCustomSearchApiUrl + "?" + queryString, 1);
            JSONObject jsonObject = fetchSearchDataV2(googleCustomSearchApiUrl + "?" + queryString, 1, true);
            notifyGoogleSearchAPIError(jsonObject, Common.INBOUND_LINK_CHECKER);
            indexedPages = getNumberOfSearchResult(jsonObject);
        } catch (UnsupportedEncodingException e) {
            indexedPages = -1;
            logger.error("UnsupportedEncodingException in countIndexedPagesInGoogle cuase: " + e.getMessage());
        } catch (Exception e) {
            indexedPages = -1;
            logger.error("Exception in countIndexedPagesInGoogle cuase: " + e);
        }
        return indexedPages;
    }

    public long getIndexPagesByScreenScraping(String domain) {
        long indexedPages = 0;
        String hostName = null;
        URL url;
        try {
            url = new URL(domain);
            hostName = url.getHost();
            hostName = hostName.replace("www.", "");
        } catch (MalformedURLException ex) {
            logger.error("Exception in converting to rootdomain: ", ex);
        }
        String query = "site:" + (hostName != null ? hostName : domain);
        Document document = getGoogleDocumentBySearchQuery(query, proxyPort, 1);
        if (document != null) {
            Elements resultStats = document.select("div#resultStats");
            String indexPagesCount = resultStats.first().text().trim().replaceAll("[^0-9]", "");
            indexedPages = Long.parseLong(indexPagesCount);
        }
        return indexedPages;
    }

    /* function to get document object for google search */
    public Document getGoogleDocumentBySearchQuery(String searchQuery, int proxyHostNo, int numberOfPages) {
        Document document = null;
//        HttpMethod method = connectProxyServerNew(searchQuery, proxyHostNo, numberOfPages);
        HttpMethod method = connectProxyServer(searchQuery, proxyHostNo, numberOfPages);
        if (method.getStatusCode() == HttpStatus.SC_OK) {
            try {
                String response = method.getResponseBodyAsString();
                document = Jsoup.parse(response);
            } catch (IOException ex) {
                logger.error("Exception in getting document through screen scrapping:", ex);
            }
        }
        return document;
    }

    /* function to get search data and top 5 links for single keyword single domain */
    public SearchOrganicLink getGoogleSearchDataV1(String googleCustSearchApiKey, String searchQuery, String domain,
            ArrayList<SearchOrganicLink> top5Links, String gl, String lang) {
        String queryString;
        SearchOrganicLink domainLink = null;
        long searchResultCount = 0;
        int searchLimit = URL_COUNT_LIMIT;
        try {
            queryString = "key=" + googleCustSearchApiKey + "&cx=" + googleCustomSearchEngnId
                    + "&googlehost=google.com&gl=" + gl + "&lr=" + lang + "&ie=utf8&oe=utf8&filter=0&q="
                    + URLEncoder.encode(searchQuery, "UTF-8");
            for (int i = 1; i <= searchLimit; i = i + 10) {
                JSONObject jsonObject;
                try {
                    jsonObject = fetchSearchData(googleCustomSearchApiUrl + "?" + queryString, i);
                    domainLink = searchInJsonV1(jsonObject, domain, i);
                    if (i == 1) {
                        searchResultCount = getNumberOfSearchResult(jsonObject);
                        if (searchResultCount < searchLimit) {
                            searchLimit = (int) searchResultCount;
                        }
                        getTop5Result(jsonObject, top5Links);
                    }
                    if (domainLink != null) {
                        logger.info("Domain Found at Position: " + domainLink.getPosition() + " Link: " + domainLink.getUrl());
                        break;
                    }
                } catch (IOException e) {
                    logger.error(e);
                } catch (JSONException e) {
                    logger.error(e);
                }
            }
        } catch (UnsupportedEncodingException e1) {
            logger.error(e1);
        }
        return domainLink;
    }

    /* function to get search data for single keyword multiple domains */
    public HashMap<String, SearchOrganicLink> getGoogleSearchDataV2(String googleCustSearchApiKey, String searchQuery,
            List<String> domains, String gl, String lang, int proxyHost) {
        String queryString;
        HashMap<String, SearchOrganicLink> results = new HashMap<>();
        Set<String> domainSet = new HashSet<>();
        boolean resultLessThanLimit = false;
        boolean gotResult = false;
        for (String domain : domains) {
            domainSet.add(domain);
            SearchOrganicLink tempResult = new SearchOrganicLink();
            tempResult.setPosition(-1);
            results.put(domain, tempResult);
        }
        long searchResultCount = 0;
        int searchLimit = URL_COUNT_LIMIT;
        try {
            queryString = "key=" + googleCustSearchApiKey + "&cx=" + googleCustomSearchEngnId
                    + "&googlehost=google.com&gl=" + gl + "&lr=" + lang + "&ie=utf8&oe=utf8&filter=0&q="
                    + URLEncoder.encode(searchQuery, "UTF-8");
            for (int i = 1; i <= searchLimit; i = i + 10) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = fetchSearchData(googleCustomSearchApiUrl + "?" + queryString, i);
                    if (jsonObject != null) {
                        searchInJsonV2(jsonObject, domainSet, i, results);
                        searchResultCount = getNumberOfSearchResult(jsonObject);
                        gotResult = true;
                        if (i == 1) {
                            if (searchResultCount < searchLimit) {
                                searchLimit = (int) searchResultCount;
                                resultLessThanLimit = true;
                            }
                        }
                        if (domainSet.isEmpty()) {
                            break;
                        }
                    }
                } catch (IOException | JSONException e) {
                    logger.error("IOException | JSONException in getGoogleSearchDataV2 cause:: ", e);
                }
            }
        } catch (UnsupportedEncodingException e1) {
            logger.error("UnsupportedEncodingException in getGoogleSearchDataV2 cause:: ", e1);
        }
        if (resultLessThanLimit || !gotResult) {
            for (String domain : domains) {
                SearchOrganicLink tempResult = results.get(domain);
                if (tempResult.getPosition() == -1) {
                    tempResult.setPosition(0);
                }
            }
        }
        return results;
    }

    /*Service calling from :   TopRankedWebsitesbyKeywordService.
    To get Search data and top 10 links for single keyword*/
    public List<SearchOrganicLink> getGoogleSearchDataForaKeyword(String googleCustSearchApiKey, String searchQuery,
            ArrayList<SearchOrganicLink> top10Links, String gl, String lang, int proxyHostNo) {
        String queryString = null;
        JSONObject jsonObject = null;
        HttpMethod method = null;
        logger.debug("Query URL for GoogleSearch Data For a Keyword: " + queryString);
        try {
            method = connectProxyServer(searchQuery, gl, lang, proxyHostNo, 11);
            if (method.getStatusCode() == HttpStatus.SC_OK) {
                String response = method.getResponseBodyAsString();
                if (response != null) {
                    Document document = Jsoup.parse(response);
                    if (document != null) {
                        Elements links = document.select("div.g h3.r a[href]:not(.sla)");
                        List<String> allLiks = new ArrayList<>();
                        if (links != null && !links.isEmpty()) {
                            links.stream().map((link) -> link.attr("href")).map((finalLink) -> modifyDomainForGoogleCustomSearch(finalLink, false)).filter((temp) -> (!"".equals(temp) && temp != null && temp.length() > 0)).forEach((temp) -> {
                                allLiks.add(temp);
                            });
                        }
                        if (top10Links != null && !allLiks.isEmpty()) {
                            getTop10ResultForProxy(allLiks, top10Links);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            logger.error("IOException in getting document through screen scrapping:", ex);
        } catch (Exception ex) {
            logger.error("Exception in getting document through screen scrapping:", ex);
        }
        if (top10Links != null && top10Links.isEmpty()) {
            try {
                queryString = "key=" + googleCustSearchApiKey + "&cx=" + googleCustomSearchEngnId
                        + "&googlehost=google.com&gl=" + gl + "&lr=" + lang + "&ie=utf8&oe=utf8&filter=0&q="
                        + URLEncoder.encode(searchQuery, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                queryString = null;
                logger.error("UnsupportedEncodingException in encoding search query cause: ", e);
            }
            if (queryString != null && googleCustomSearchApiUrl != null) {
                //jsonObject = fetchSearchData(googleCustomSearchApiUrl + "?" + queryString, 1);
                jsonObject = fetchSearchDataV2(googleCustomSearchApiUrl + "?" + queryString, 1, false);
                if (jsonObject != null) {
                    getTop10Result(jsonObject, top10Links);
                } else {
                    if (isGoogleRepsonseIsInvalidquery(jsonObject)) {
                        notifyGoogleSearchAPIError(jsonObject, Common.TOP_RANKED_WEBSITES_BY_KEYWORD);
                    }
                }
            }
        }
        return top10Links;
    }

    /*Service calling from :   Domain Finder Service.
    To get Search data and top 10 links for company keyword*/
    public String getGoogleSearchDataForaBrandKeyword(String searchQuery) {
        String domainURL = null;
//        HttpMethod method = null;
//        int startIndex = 1;
        JSONObject jsonObject = null;
        if (googleCustomSearchApiUrl != null) {
            /*Pulling data from Google custom search API using proxy*/
 /*Commenting the proxy service code google blocks proxy server IPS
            https://docs.proxymesh.com/article/12-world-proxy
            try {
                String tempSearchQuery = googleCustomSearchApiUrl + "?" + searchQuery + "&start=" + startIndex + "&num=10";
                
                method = Common.connectAPIURLEndpointUsingProxyServer(tempSearchQuery, proxyHostNo);
                if (method != null && method.getStatusCode() == HttpStatus.SC_OK) {
                    jsonObject = new JSONObject(method.getResponseBodyAsString());
                }
            } catch (IOException | JSONException ex) {
                logger.error("IOException|JSONException in getting document through screen scrapping:", ex);
            }
            if (jsonObject != null) {
                domainURL = getDomainURLFromGoogleCustomSearchResponse(jsonObject);
            }*/
 /*If failed connecting pulling data from Google custom search API then connecting directly to API by IP of application host*/
            if ((jsonObject == null) || ((domainURL != null && domainURL.equalsIgnoreCase(GoogleCustomSearch.GOOGLE_USAGE_LIMIT_ERROR) || domainURL == null))) {
                jsonObject = fetchSearchDataV2(googleCustomSearchApiUrl + "?" + searchQuery, 1, false);
            }
            if (jsonObject != null) {
                domainURL = getDomainURLFromGoogleCustomSearchResponse(jsonObject);
            }
        }
        return domainURL;
    }

    private String getDomainURLFromGoogleCustomSearchResponse(JSONObject responseJSON) {
        JSONObject innerObject = null;
        String link = null;
        JSONArray jsonArray = null;
        boolean isValidJSON = false;
        try {
            if (responseJSON != null && responseJSON.has("items")) {
                jsonArray = responseJSON.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() != 0) {
                    innerObject = jsonArray.getJSONObject(0);
                    if (innerObject != null) {
                        if (innerObject.has("link")) {
                            link = innerObject.getString("link");
                        }
                    }
                }
                isValidJSON = true;
            } else if (responseJSON != null && responseJSON.has("error")) {
                /*
                Response format for limit exceeds
                {
                    "error": {
                     "errors": [
                      {
                       "domain": "usageLimits",
                       "reason": "dailyLimitExceeded",
                       "message": "Daily Limit Exceeded. The quota will be reset at midnight Pacific Time (PT). You may monitor your quota usage and adjust limits in the API Console: https://console.developers.google.com/apis/api/customsearch.googleapis.com/quotas?project=**********",
                       "extendedHelp": "https://console.developers.google.com/apis/api/customsearch.googleapis.com/quotas?project=***********"
                      }
                     ],
                     "code": 403,
                     "message": "Daily Limit Exceeded. The quota will be reset at midnight Pacific Time (PT). You may monitor your quota usage and adjust limits in the API Console: https://console.developers.google.com/apis/api/customsearch.googleapis.com/quotas?project=***********"
                    }
                    }
                 */
                JSONObject innserObject2 = responseJSON.getJSONObject("error");
                int code = 0;
                if (innserObject2.has("code")) {
                    code = innserObject2.getInt("code");
                }
                if (innserObject2.has("errors")) {
                    jsonArray = innserObject2.getJSONArray("errors");
                }
                if (jsonArray != null && jsonArray.length() != 0 && code > HttpStatus.SC_OK) {
                    innerObject = jsonArray.getJSONObject(0);
                    if (innerObject != null) {
                        if (innerObject.has("reason") && innerObject.getString("reason").equalsIgnoreCase(GOOGLE_USAGE_LIMIT)) {
                            link = GOOGLE_USAGE_LIMIT_ERROR;
                        }
                    }
                }
                isValidJSON = true;
            } else if (responseJSON != null) {
                isValidJSON = false;
                logger.debug("JSON: " + responseJSON.toString());
            }
        } catch (JSONException e) {
            logger.error("JSONException in getDomainURLFromGoogleCustomSearchResponse cause: ", e);
        }
        if (!isValidJSON) {
            link = GOOGLE_USAGE_LIMIT_ERROR;
        }
        return link;
    }

    private long getNumberOfSearchResult(JSONObject jsonObject) {
        long searchResultCount = 0;
        try {
            if (jsonObject != null && jsonObject.has("searchInformation")) {
                JSONObject searchInformation = jsonObject.getJSONObject("searchInformation");
                searchResultCount = searchInformation.getLong("totalResults");
            }
        } catch (JSONException e) {
            searchResultCount = -1;
            logger.error("JSONException in getNumberOfSearchResult cause: " + e.getMessage());
        }
        return searchResultCount;
    }

    private JSONObject fetchSearchDataV2(String queryString, int startIndex, boolean waitForSuccessResposne) {
        /*String countString = "&start=" + startIndex + "&num=10";
        String requestString = queryString + countString;
        JSONObject jsonObject = Common.getJSONFromAPIResponse(requestString);*/
        logger.debug("fetchSearchDataV2 Query URL: " + queryString + "&start=" + startIndex + "&num=10");
        return Common.getJSONFromAPIResponse(queryString + "&start=" + startIndex + "&num=10", waitForSuccessResposne,null);
    }

    private JSONObject fetchSearchData(String queryString, int startIndex) throws IOException, JSONException {
        JSONObject jsonObject = null;
        HttpURLConnection conn = null;
        try {
            String countString = "&start=" + startIndex + "&num=10";
            String requestString = queryString + countString;
            logger.debug("Query URL for fetch Search Data: " + requestString);
            URL url = new URL(requestString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            BufferedReader bufferedReader = null;
            if (conn.getResponseCode() == HttpStatus.SC_OK) {
                bufferedReader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            }
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            if (bufferedReader != null) {
                while ((inputStr = bufferedReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
            }
            if (responseStrBuilder.length() != 0) {
                jsonObject = new JSONObject(responseStrBuilder.toString());
            }
        } catch (IOException | JSONException e) {
            logger.error("Exception in fetchSearchData Method of GoogleCustom search", e);
        }
        return jsonObject;
    }

    private SearchOrganicLink searchInJsonV1(JSONObject jsonObject, String domain, int page) {
        SearchOrganicLink domainLink = null;
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject rec = jsonArray.getJSONObject(i);
                String link = rec.getString("link");
                if (link.contains(domain)) {
                    String displayLink = rec.getString("displayLink");
                    String snippet = rec.getString("snippet");
                    String title = rec.getString("title");
                    domainLink = new SearchOrganicLink(link, displayLink, title, snippet);
                    domainLink.setPosition(page + i);
                    break;
                }
            }
        } catch (JSONException e) {
            logger.error("Exception in searchInJsonV1 Method of GoogleCustom search", e);
        }
        return domainLink;
    }

    private void searchInJsonV2(JSONObject jsonObject, Set<String> domainSet,
            int page, HashMap<String, SearchOrganicLink> results) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray("items");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            String link = rec.getString("link");
            Set<String> tempDomainSet = new HashSet<>(domainSet);
            for (String domain : tempDomainSet) {
                String modifiedDomain = modifyDomain(domain);
                String modifiedLink = modifyDomain(link);
                if (modifiedLink.equalsIgnoreCase(modifiedDomain)) {
                    String displayLink = rec.getString("displayLink");
                    String snippet = rec.getString("snippet");
                    String title = rec.getString("title");
                    SearchOrganicLink domainLink = new SearchOrganicLink(link, displayLink, title, snippet);
                    domainLink.setPosition(page + i);
                    results.put(domain, domainLink);
                    domainSet.remove(domain);
                    break;
                }
            }
            if (domainSet.isEmpty()) {
                break;
            }
        }
    }

    private static String modifyDomain(String domain) {
        String modifiedDomain = domain;
        if (modifiedDomain.contains("?")) {
            modifiedDomain = modifiedDomain.substring(0, modifiedDomain.indexOf('?'));
        }
        if (modifiedDomain.contains("http://")) {
            modifiedDomain = modifiedDomain.replace("http://", "");
        } else if (modifiedDomain.contains("https://")) {
            modifiedDomain = modifiedDomain.replace("https://", "");
        }
        if (modifiedDomain.contains("/")) {
            modifiedDomain = modifiedDomain.substring(0, modifiedDomain.indexOf('/'));
        }
        modifiedDomain = modifiedDomain.replaceFirst("^(ww[a-zA-Z0-9-]{0,}\\.)", "");
        return modifiedDomain;
    }

    private void getTop5Result(JSONObject jsonObject, ArrayList<SearchOrganicLink> top5Links) {
        if (jsonObject.has("items")) {
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                for (int i = 0; i < jsonArray.length() && i < 5; i++) {
                    JSONObject rec = jsonArray.getJSONObject(i);
                    String link = rec.getString("link");
                    String displayLink = rec.getString("displayLink");
                    String snippet = rec.getString("snippet");
                    String title = rec.getString("title");
                    SearchOrganicLink googleOrganicLink = new SearchOrganicLink(link, displayLink,
                            title, snippet);
                    googleOrganicLink.setPosition(i + 1);
                    top5Links.add(googleOrganicLink);
                }
            } catch (JSONException e) {
                logger.error("Exception in getTop5Result Method of GoogleCustom search", e);
            }
        }
    }

    private void getTop10Result(JSONObject jsonObject, ArrayList<SearchOrganicLink> top10Links) {
        if (jsonObject != null && jsonObject.has("items")) {
            JSONObject innerObject = null;
            String link = null;
            String displayLink = null;
            String snippet = null;
            String title = null;
            JSONArray jsonArray = null;
            try {
                jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length() && i < 10; i++) {
                        try {
                            innerObject = jsonArray.getJSONObject(i);
                            if (innerObject != null) {
                                logger.debug("innerObject: " + innerObject);
                                if (innerObject.has("link")) {
                                    link = innerObject.getString("link");
                                }
                                if (innerObject.has("displayLink")) {
                                    displayLink = innerObject.getString("displayLink");
                                }
                                if (innerObject.has("snippet")) {
                                    snippet = innerObject.getString("snippet");
                                }
                                if (innerObject.has("title")) {
                                    title = innerObject.getString("title");
                                }
                                SearchOrganicLink googleOrganicLink = new SearchOrganicLink(link, displayLink,
                                        title, snippet);
                                googleOrganicLink.setPosition(i + 1);
                                top10Links.add(googleOrganicLink);
                            }
                        } catch (JSONException e) {
                            logger.error("JSONException in getTop10Result Method of GoogleCustom search API cause:", e);
                        }
                    }
                }
            } catch (JSONException e) {
                logger.error("JSONException in getTop10Result Method of GoogleCustom search API cause: ", e);
            }
        }

        /*for (int i = 0; i < jsonArray.length() && (i < 10 || top10Links.size() < 10); i++) {
                    JSONObject rec = jsonArray.getJSONObject(i);
                    String link = rec.getString("link");
                    String displayLink = rec.getString("displayLink");
                    String snippet = rec.getString("snippet");
                    String title = rec.getString("title");
                    SearchOrganicLink googleOrganicLink = new SearchOrganicLink(link, displayLink,
                            title, snippet);
                    googleOrganicLink.setPosition(i + 1);
                    top10Links.add(googleOrganicLink);
                }*/
    }

    private boolean isGoogleRepsonseIsInvalidquery(JSONObject jsonObject) {
        boolean isInvalidQuery = false;
        try {
            logger.debug("jsonObject: " + jsonObject);
            if (jsonObject != null && jsonObject.has("queries")) {
                int totalResults = 0;
                JSONObject subJsonObject = jsonObject.getJSONObject("queries");
                JSONArray jsonArray = null;
                if (subJsonObject != null) {
                    jsonArray = subJsonObject.getJSONArray("request");
                }
                if (jsonArray != null) {
                    subJsonObject = null;
                    subJsonObject = jsonArray.getJSONObject(0);
                }
                if (subJsonObject != null && subJsonObject.has("totalResults")) {
                    totalResults = subJsonObject.getInt("totalResults");
                }
                if (totalResults == 0 && !jsonObject.has("items")) {
                    isInvalidQuery = true;
                }
            }
        } catch (JSONException e) {
            logger.error("Exception in isGoogleRepsonseIsInvalidquery cause: ", e);
        }
        return isInvalidQuery;
    }

    private List<String> getTop10ResultV2(JSONObject jsonObject, int numberOfResults) {
        String link = null;
        List<String> top10Links = null;
        try {
            logger.debug("jsonObject: " + jsonObject);
            if (jsonObject != null && jsonObject.has("items")) {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray != null && jsonArray.length() != 0) {
                    top10Links = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length() && i < numberOfResults; i++) {
                        JSONObject rec = jsonArray.getJSONObject(i);
                        logger.debug("rec: " + rec);
                        if (rec.has("link")) {
                            link = rec.getString("link");
                        }
                        top10Links.add(link);
                    }
                }
            }
        } catch (JSONException e) {
            logger.error("Exception in getTop10ResultV2 cause: ", e);
        }
        return top10Links;
    }

    public HashMap<String, SearchOrganicLink> getkeywordRankData(String searchQuery, List<String> domains,
            String gl, String lang, int proxyHost) {
        HashMap<String, SearchOrganicLink> results = new HashMap<>();
        boolean isProxyFail = fetchKeywordRankDataByProxy(searchQuery, domains, gl, lang, proxyHost, results);
        if (isProxyFail) {
            logger.info("FAIL: fetching keyword rank using proxy for keyword: " + searchQuery);
            logger.info("keyword data fetching starts" + searchQuery);
            logger.info("keyword: " + searchQuery + " for domains: " + domains);
            logger.info("proxyHost Number: " + proxyHost);
            results = getGoogleSearchDataV2(googleCustomSearchApiKeyLxrm, searchQuery, domains, gl, lang, proxyHost);
            logger.info("keyword data fetching ends: " + searchQuery);
        }
        return results;
    }

    public boolean fetchKeywordRankDataByProxy(String searchQuery, List<String> domains,
            String gl, String lang, int proxyHost, HashMap<String, SearchOrganicLink> results) {
        boolean isProxyFail = false;
        Set<String> domainSet = new HashSet<>();
        for (String domain : domains) {
            domainSet.add(domain);
            SearchOrganicLink tempResult = new SearchOrganicLink();
            tempResult.setPosition(-1);
            results.put(domain, tempResult);
        }
        HttpMethod method = connectProxyServer(searchQuery, gl, lang, proxyHost, URL_COUNT_LIMIT);
        if (method != null && method.getStatusCode() == HttpStatus.SC_OK) {
            try {
                String response = method.getResponseBodyAsString();
                Document document = Jsoup.parse(response);
                if (document != null) {
                    Elements links = document.select("div.g h3.r a[href]:not(.sla)");
                    List<String> allLinks = new ArrayList<>();
                    if (links.size() > 0) {
                        links.stream().map((link) -> link.attr("href")).map((finalLink) -> modifyDomainForGoogleCustomSearch(finalLink, true)).filter((temp) -> (!"".equals(temp) && temp != null && temp.length() > 0)).forEach((temp) -> {
                            allLinks.add(temp);
                        });
                    }

                    searchResultsForGoogleProxy(allLinks, domainSet, results);
                    isProxyFail = allLinks.isEmpty();
                    if (!isProxyFail) {
                        logger.info("SUCCESS: fetching keyword rank using proxy for keyword: " + searchQuery);
                    }
                }
            } catch (IOException ex) {
                logger.error("Exception in getting document through screen scrapping through proxy:", ex);
                isProxyFail = true;
            }
        } else {
            isProxyFail = true;
        }

        return isProxyFail;
    }

    private HttpMethod connectProxyServer(String searchQuery, String gl, String lang, int proxyHostNo, int numberOfSearchResults) {
        String selectedProxyHost = null;
        if (proxyHost != null && !proxyHost.isEmpty()) {
            if (proxyHostNo == 0) {
                selectedProxyHost = proxyHost.get(0);
            } else {
                selectedProxyHost = proxyHost.get(1);
            }
        }
        HttpClient client = new HttpClient();
        HostConfiguration config = client.getHostConfiguration();
        config.setProxy(selectedProxyHost != null ? selectedProxyHost : String.valueOf(1), proxyPort);

        Credentials credentials = new UsernamePasswordCredentials(proxyUserName, proxyPassword);
        AuthScope authScope = new AuthScope(selectedProxyHost, proxyPort);
        client.getState().setProxyCredentials(authScope, credentials);
        HttpMethod method = null;
        try {
            String encodedSearchQuery = URLEncoder.encode(searchQuery, "UTF-8");
            String finalURL = GOOGLE_SEARCH_QUERY_FOR_PROXY + encodedSearchQuery + "&gl=" + gl + "&lr=" + lang + "&num=" + numberOfSearchResults;
            logger.info("Query URL for connectProxyServer: " + finalURL);
            method = new GetMethod(finalURL);
            client.executeMethod(method);
        } catch (UnsupportedEncodingException e) {
            logger.error("Exception on converting search query into encoding format cause: ", e);
        } catch (IOException ex) {
            logger.error("Exception in connect to proxy server cause: ", ex);
        }
        return method;
    }

    private HttpMethod connectProxyServer(String searchQuery, int proxyHostNo, int numberOfSearchResults) {
        String selectedProxyHost = null;
        if (proxyHost != null && !proxyHost.isEmpty()) {
            if (proxyHostNo == 0) {
                selectedProxyHost = proxyHost.get(0);
            } else {
                selectedProxyHost = proxyHost.get(1);
            }
        }
        HttpClient client = new HttpClient();
        HostConfiguration config = client.getHostConfiguration();
        config.setProxy(selectedProxyHost != null ? selectedProxyHost : String.valueOf(1), proxyPort);
        Credentials credentials = new UsernamePasswordCredentials(proxyUserName, proxyPassword);
        AuthScope authScope = new AuthScope(selectedProxyHost, proxyPort);
        client.getState().setProxyCredentials(authScope, credentials);
        HttpMethod method = null;
        try {
            String encodedSearchQuery = URLEncoder.encode(searchQuery, "UTF-8");
            String finalURL = GOOGLE_SEARCH_QUERY_FOR_PROXY + encodedSearchQuery + "&num=" + numberOfSearchResults;
            method = new GetMethod(finalURL);
            client.executeMethod(method);
        } catch (UnsupportedEncodingException e) {
            logger.error("UnsupportedEncodingException on converting search query into encoding format cause: ", e);
        } catch (IOException ex) {
            logger.error("IOException in connect to proxy server cause: ", ex);
        }
        return method;
    }

    private HttpMethod connectProxyServerV2(String searchQuery, String geoGraphicLocation, String language, int proxyHostNo, int numberOfSearchResults) {
        String selectedProxyHost = null;
        if (proxyHost != null && !proxyHost.isEmpty()) {
            if (proxyHostNo == 0) {
                selectedProxyHost = proxyHost.get(0);
            } else {
                selectedProxyHost = proxyHost.get(1);
            }
        }
        String finalURL = null;
        URL currentUrlObj = null;
        HttpMethod method = null;
        try {
            finalURL = GOOGLE_SEARCH_QUERY_FOR_PROXY + URLEncoder.encode(searchQuery, "UTF-8") + "&gl=" + geoGraphicLocation + "&lr=" + language + "&num=" + numberOfSearchResults;
            if (finalURL != null) {
                logger.info("finalURL: " + finalURL);
                currentUrlObj = new URL(finalURL);
            }
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            logger.error("UnsupportedEncodingException in creating erncodee URL cause: ", e);
        }
        if (currentUrlObj != null) {
            try {
                method = new GetMethod(currentUrlObj.toString());
                HttpClient client = new HttpClient();
                client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
                client.getParams().setSoTimeout(1000 * 1);
                client.getParams().setConnectionManagerTimeout(1000 * 1);

                HostConfiguration config = client.getHostConfiguration();
                config.setProxy(selectedProxyHost != null ? selectedProxyHost : String.valueOf(1), proxyPort);
                client.getState().setProxyCredentials(new AuthScope(selectedProxyHost, proxyPort), new UsernamePasswordCredentials(proxyUserName, proxyPassword));
                client.executeMethod(method);
            } catch (IOException e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("IOException in connectProxyServerV2 cause: ", e);
            } catch (Exception e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("Exception in connectProxyServerV2 cause: ", e);
            }
        }
        return method;
    }

    /*This service can be used to connect any API endpoint URL using proxy mesh API,
    it helps to make continous hits using random public IP's */
    private HttpMethod connectAPIURLEndpointUsingProxyServer(String searchQuery, int proxyHostNo) {
        String selectedProxyHost = null;
        if (proxyHost != null && !proxyHost.isEmpty()) {
            if (proxyHostNo == 0) {
                selectedProxyHost = proxyHost.get(0);
            } else {
                selectedProxyHost = proxyHost.get(1);
            }
        }
        URL currentUrlObj = null;
        HttpMethod method = null;
        if (searchQuery != null) {
            try {
                currentUrlObj = new URL(searchQuery);
            } catch (MalformedURLException ex) {
                logger.error("MalformedURLException in connectGoogleCustomSearchUsingProxyServer cause: ", ex);
            }
        }
        if (currentUrlObj != null) {
            try {
                method = new GetMethod(currentUrlObj.toString());
                HttpClient client = new HttpClient();
                client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
                client.getParams().setSoTimeout(1000 * 1);
                client.getParams().setConnectionManagerTimeout(1000 * 1);
                HostConfiguration config = client.getHostConfiguration();
                config.setProxy(selectedProxyHost != null ? selectedProxyHost : String.valueOf(1), proxyPort);
                client.getState().setProxyCredentials(new AuthScope(selectedProxyHost, proxyPort), new UsernamePasswordCredentials(proxyUserName, proxyPassword));
                client.executeMethod(method);
            } catch (IOException e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("IOException in connectGoogleCustomSearchUsingProxyServer cause: ", e);
            } catch (Exception e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("Exception in connectGoogleCustomSearchUsingProxyServer cause: ", e);
            }
        }
        return method;
    }

    private static String modifyDomainForGoogleCustomSearch(String domain, boolean isDomain) {
//        If isDomain true then this will return only domain else it will return complete url
        String modifiedDomain = domain;
        if (!modifiedDomain.contains("/search")) {
            if (modifiedDomain.contains("?q=")) {
                modifiedDomain = modifiedDomain.substring(modifiedDomain.indexOf("q=") + 2, modifiedDomain.indexOf("&"));
            }
            if (isDomain) {
                if (modifiedDomain.contains("http://")) {
                    modifiedDomain = modifiedDomain.replace("http://", "");
                } else if (modifiedDomain.contains("https://")) {
                    modifiedDomain = modifiedDomain.replace("https://", "");
                }
                modifiedDomain = modifiedDomain.replaceFirst("^(ww[a-zA-Z0-9-]{0,}\\.)", "");
            }
            return modifiedDomain;
        }
        return "";
    }

    private void getTop10ResultForProxy(List<String> allLiks, ArrayList<SearchOrganicLink> top10Links) {
        for (int i = 0; i < allLiks.size() && i < 10; i++) {
            SearchOrganicLink googleOrganicLink = new SearchOrganicLink(allLiks.get(i), "", "", "");
            googleOrganicLink.setPosition(i + 1);
            top10Links.add(googleOrganicLink);
        }
    }

    public void searchResultsForGoogleProxy(List<String> allLinks, Set<String> domainSet,
            HashMap<String, SearchOrganicLink> results) {
        for (int i = 0; i < allLinks.size(); i++) {
            String link = allLinks.get(i);
            Set<String> tempDomainSet = new LinkedHashSet<>(domainSet);
            for (String domain : tempDomainSet) {
                String modifiedDomain = modifyDomain(domain);
                String modifiedLink = modifyDomain(link);
                if (modifiedLink.equalsIgnoreCase(modifiedDomain)) {
                    SearchOrganicLink domainLink = new SearchOrganicLink(link, "", "", "");
                    domainLink.setPosition(i + 1);
                    results.put(domain, domainLink);
                    domainSet.remove(domain);
                    break;
                }
            }
            if (domainSet.isEmpty()) {
                break;
            }
        }
    }

    private void notifyGoogleSearchAPIError(JSONObject responseJSON, String toolName) {
        try {
            if (responseJSON != null && responseJSON.has("error")) {
                logger.error("Error from Google Custom API, Response will be updated through mail: " + responseJSON);
                APIErrorHandlingService.raiseGoogleAPIError(responseJSON, APIErrorHandlingService.GOOGLE_CUSTOM_SEARCH, toolName, 30, null);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException in notifyGoogleSearchAPIErrorMail cause:", e);
        }
    }
}