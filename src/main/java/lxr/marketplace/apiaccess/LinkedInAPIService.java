package lxr.marketplace.apiaccess;

import lxr.marketplace.util.Common;

import org.json.JSONException;
import org.json.JSONObject;

public class LinkedInAPIService {

	String apiURL = "http://www.linkedin.com/countserv/count/share?";
	//sample response: {"count":17,"fCnt":"17","fCntPlusOne":"18","url":"http:\/\/stylehatch.co"}
	public static void main(String [] args){
		LinkedInAPIService linkedInAPIService = new LinkedInAPIService();
		long linkedInShare = linkedInAPIService.fetchURLLinkedInShares("http://www.lxrmarketplace.com");
//		System.out.println("#linkedInShare: " + linkedInShare);
	}
	public long fetchURLLinkedInShares(String url){
		String queryURL = apiURL + "url="+url+"&format=json";
		JSONObject linkedInResponse = Common.createJsonObjectFromURL(queryURL);
		if(linkedInResponse != null){
			try {
				return Long.parseLong(linkedInResponse.getString("count"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}		
}
