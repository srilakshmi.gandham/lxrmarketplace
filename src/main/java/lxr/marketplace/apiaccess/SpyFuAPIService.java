/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.io.InputStream;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.PreSalesToolUsageDAOService;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class SpyFuAPIService {

    private static final Logger LOGGER = Logger.getLogger(SpyFuAPIService.class);

    @Value("${SpyFu.API.URL.Prefix}")
    private String API_URL_Prefix;

    @Value("${SpyFu.API.KEY}")
    private String API_Key;

    @Value("${SpyFu.API.URL.AdWordsBudget}")
    private String API_URL_AdWordsBudget;

    @Autowired
    private ProxyServerConnection proxyServerConnection;

    public Double getAdwordsBudget(String queryDomain, int proxyHostNo) {
        double monthlyAdWordsBudget = 0;
        String endPointURL = API_URL_Prefix + API_URL_AdWordsBudget + "?domain=" + queryDomain + "&api_key=" + API_Key;
        LOGGER.debug("Tool: Ad Spy V2, Metric: Est. Monthly Adwords Budget, Endpoint URL: " + endPointURL);
        try {
            JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, ProxyServerConnection.PROXYSERVER, "monthly_adwords_budget", queryDomain);
            if (jsonObject != null && jsonObject.has("monthly_adwords_budget")) {
                monthlyAdWordsBudget = jsonObject.getDouble("monthly_adwords_budget");
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getAdwordsBudget cause: ", e);
        } catch (Exception e) {
            LOGGER.error("Exception in getAdwordsBudget cause: ", e);
        }
        return monthlyAdWordsBudget;
    }

    /*getJSONObjectFromProxyServerResposne*/
    public JSONObject getJSONObjectFromProxyServerResposne(String endPointURL, int proxyHostNo, String connectType, String parentKey, String queryDomain) {
        JSONObject responseJSON = null;
        HttpMethod httpMethod = null;
        InputStream inputStream = null;
        boolean isDataPulled = Boolean.FALSE;
        int count = 0;
        while (!isDataPulled) {
            /*Try with okHTTP*/
            responseJSON = Common.getJSONFromAPIResponseV2(endPointURL);
            /*Checking for required data presences*/
            isDataPulled = validateAPIResponse(responseJSON, parentKey, queryDomain, endPointURL);
            /*If failed try with proxy*/
            if (!isDataPulled) {
                try {
                    httpMethod = proxyServerConnection.getResponseFromAPIUsingProxyServer(endPointURL, proxyHostNo, "get");
                    if (httpMethod != null && httpMethod.getStatusCode() == HttpStatus.SC_OK) {
                        inputStream = httpMethod.getResponseBodyAsStream();
                        responseJSON = new JSONObject(IOUtils.toString(inputStream));
                    }
                } catch (Exception e) {
                    LOGGER.debug("Exception in getting resposne from proxy server for endpoint URL::" + endPointURL + ", cause:: " + e.getMessage());
                }
                /*Checking for required data presences*/
                isDataPulled = validateAPIResponse(responseJSON, parentKey, queryDomain, endPointURL);
            }
            /*If proxy failed try make thread sleep and try after 1 min*/
            if (!isDataPulled) {
                LOGGER.debug("Pulling data from proxy is failed for query domain :: "+queryDomain+", at count:: "+count);
                try {
                    LOGGER.debug("*******************Pulling data from spyfu using OkHttpClient thread entering into a sleep stage for proxy no:: " + proxyHostNo);
                    Thread.sleep(1000 * 10);
                    LOGGER.debug("Pulling data from spyfu using OkHttpClient thread exited from sleep stage for proxy no:: " + proxyHostNo);
                } catch (InterruptedException e) {
                    LOGGER.debug("*******************InterruptedException in making thread sleep for endpoint URL::" + endPointURL + ", cause:: " + e.getMessage());
                }
            }
            ++count;
            /*Doing explictly*/
            isDataPulled = isDataPulled ? Boolean.TRUE : count >= 10 ? Boolean.TRUE : Boolean.FALSE;
        }
        return responseJSON;
    }
    
    private boolean validateAPIResponse(JSONObject responseJSON, String parentKey, String queryDomain, String endPointURL) {
        boolean isValidResponse = Boolean.FALSE;
        if ((responseJSON != null && responseJSON.has(parentKey))) {
            isValidResponse = Boolean.TRUE;
        } else if (responseJSON != null) {
            /*To handle data not found*/
            String errorResponse = responseJSON.toString().toLowerCase();
            if (errorResponse.contains("204") || errorResponse.contains("500")) {
                isValidResponse = Boolean.TRUE;
            }
            LOGGER.info("Parent key is not found and response :: " + errorResponse + ",  queryDomain :: " + queryDomain + ", isValidResponse:: " + isValidResponse);
            APIError errorObj = new APIError();
            errorObj.setApiName(PreSalesToolUsageDAOService.SIMILARWEB);
            errorObj.setToolId(44);
            errorObj.setErrorMessage(responseJSON.toString() + ",  endpoint URL:: " + endPointURL);
            APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, "PreSales tools", Boolean.TRUE);
        }
        return isValidResponse;
    }
}