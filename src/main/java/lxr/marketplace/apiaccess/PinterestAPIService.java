package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class PinterestAPIService {
	private static Logger logger = Logger.getLogger(PinterestAPIService.class);
	private String apiURL = "http://api.pinterest.com/v1/urls/count.json?";

	public long fetchURLPinsCount(String url){
		String wwwUrl = null;
		JSONObject wwwPinterestResponse = null;
		Long wwwPinsCount = (long) 0;
			
			if(url.contains("http://")){
				if(!url.contains("www")){
					wwwUrl  = url.replace("http://","http://www.");	
				}else if(url.contains("www")){
					wwwUrl  = url.replace("http://www.","http://");	
				}
			}else if(url.contains("https://")){
				if(!url.contains("www")){
					wwwUrl  = url.replace("https://","https://www.");	
				}else if(url.contains("www")){
					wwwUrl  = url.replace("https://www.","https://");	
				}				
			}
			
		String queryURL = apiURL + "callback=data&url="+url;
		if(wwwUrl != null){
			String wwwqueryURL = apiURL + "callback=data&url="+wwwUrl;
			wwwPinterestResponse = createJsonObjectFromURL(wwwqueryURL);
		}
		//Sample Response: ({"count": 0, "url": "http://netlixir.com/"})
		JSONObject pinterestResponse = createJsonObjectFromURL(queryURL);
		
		if(pinterestResponse != null || wwwPinterestResponse != null){
			try {
				Long pinsCount = Long.parseLong(pinterestResponse.getString("count"));
				if(wwwPinterestResponse != null){
					wwwPinsCount = Long.parseLong(wwwPinterestResponse.getString("count"));
				}
				Long totalPinsCount = pinsCount + wwwPinsCount;
				logger.debug(" pinsCount :"+pinsCount+" wwwPinsCount :"+wwwPinsCount+ " totalPinsCount :"+totalPinsCount);
				return totalPinsCount;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
    private JSONObject createJsonObjectFromURL(String queryUrl){
    	URL url = null;
    	JSONObject jsonObject = null;
    	try {
			url = new URL(queryUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		URLConnection urlConnection;
		try {
			urlConnection = url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = br.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			String pinterestResponse = responseStrBuilder.toString();
			pinterestResponse = pinterestResponse.substring(5, pinterestResponse.length() - 1);
			jsonObject = new JSONObject(pinterestResponse);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
    }
}
