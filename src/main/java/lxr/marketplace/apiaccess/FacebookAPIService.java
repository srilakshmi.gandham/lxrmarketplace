package lxr.marketplace.apiaccess;

import lxr.marketplace.apiaccess.lxrmbeans.FacebookLinkStat;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class FacebookAPIService {
	private static Logger logger = Logger.getLogger(FacebookAPIService.class);
	private String apiURL = "https://graph.facebook.com/fql?";
	
	public static void main(String[] args) {
		FacebookAPIService facebookAPIService = new FacebookAPIService();
		facebookAPIService.fetchFacebookStats("http://www.lxrmarketplace.com");
	}
	
	public FacebookLinkStat fetchFacebookStats(String url){	// weeks data
		String query = "q=SELECT%20like_count,%20total_count,%20share_count,%20click_count,%20comment_count%20FROM%20link_stat%20WHERE%20url%20=%20%22" + url + "%22";
		JSONObject facebookResponse = null;
		facebookResponse = Common.createJsonObjectFromURL(apiURL + query);
		//sample response: {"data":[{"comment_count":9,"click_count":0,"share_count":32,"like_count":28,"total_count":69}]}
		long likeCount = 0, totalCount = 0, shareCount = 0, clickCount = 0, commentCount = 0;
		FacebookLinkStat fbLinkStat = null;
		if(facebookResponse != null){			
			try {
				JSONArray data = facebookResponse.getJSONArray("data");
				likeCount = data.getJSONObject(0).getLong("like_count");
				totalCount = data.getJSONObject(0).getLong("total_count");
				shareCount = data.getJSONObject(0).getLong("share_count");
				clickCount = data.getJSONObject(0).getLong("click_count");
				commentCount = data.getJSONObject(0).getLong("comment_count");			
			} catch (JSONException e) {
				logger.info("Exception in fetching facebook data for " + url);
				e.printStackTrace();
			}
		}
		fbLinkStat = new FacebookLinkStat(likeCount, totalCount, shareCount, clickCount, commentCount);
		return fbLinkStat;
	}
}


