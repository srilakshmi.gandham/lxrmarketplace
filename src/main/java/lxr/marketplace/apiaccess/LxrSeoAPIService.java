/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import lxr.marketplace.preliminarySearch.LXRSEOStats;
import org.json.JSONObject;
/**
 *
 * @author sagar
 */
public interface LxrSeoAPIService {
    
    public LXRSEOStats getLxrSeoStats(String queryDomain);
    public JSONObject getLxrSeoURLInfo(String queryDomain);
    public void startLxrSeoStatsThread(String urlInfo);
    public JSONObject getLxrSeoStats(long domainId);
    public LXRSEOStats constructLXRSEOStats(long domainId);
    public byte getIndividualMetricScore(JSONObject metricJsonObject, String metricName);
    
}