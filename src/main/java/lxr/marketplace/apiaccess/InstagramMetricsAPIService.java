package lxr.marketplace.apiaccess;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import lxr.marketplace.apiaccess.lxrmbeans.InstagramMetrics;
import lxr.marketplace.util.Common;
import lxr.marketplace.socialmedia.APIKeys;
import org.apache.commons.lang3.StringUtils;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class InstagramMetricsAPIService implements Runnable {

    private static Logger logger = Logger.getLogger(InstagramMetricsAPIService.class);

    static String instagramClientId = APIKeys.instagramClientId;

    private String instagramUser;
    private InstagramMetrics instagramMetrics;
    private ConcurrentHashMap<Integer, String> instagramMap;

    public InstagramMetricsAPIService() {
    }

    public InstagramMetricsAPIService(String instagramUser, ConcurrentHashMap<Integer, String> instagramMap) {
        this.instagramUser = instagramUser;
        this.instagramMap = instagramMap;
    }

    public ConcurrentHashMap<Integer, String> getInstagramMap() {
        return instagramMap;
    }

    public void setInstagramMap(ConcurrentHashMap<Integer, String> instagramMap) {
        this.instagramMap = instagramMap;
    }

    public String getInstagramUser() {
        return instagramUser;
    }

    public void setInstagramUser(String instagramUser) {
        this.instagramUser = instagramUser;
    }

    public InstagramMetrics getInstagramMetrics() {
        return instagramMetrics;
    }

    public void setInstagramMetrics(InstagramMetrics instagramMetrics) {
        this.instagramMetrics = instagramMetrics;
    }

    @Override
    public void run() {
        try {
            setInstagramMetrics(getInstagramMetricProfileData(instagramUser));
        } catch (Exception e) {
            setInstagramMetrics(new InstagramMetrics());
            logger.error("Exception for instagram Metrics API Service", e);
        }
        instagramMap.put(5, "Instagram");
    }

    public static boolean checkInstagramUserExistence(String username) {
        return getInstagramProfileId(username) != null;
    }

    public static String getInstagramProfileId(String userName) {
        try {
            JSONObject instagramJson = Common.getResponseFromAPI(getSearchUserIdUrl(userName));
            if (instagramJson != null) {
                JSONArray instagramdataJson = instagramJson.getJSONArray("data");
                if (instagramdataJson != null) {
                    JSONObject userIdJson = Common.checkJSONObjectInJSONArrayResponse(instagramdataJson);
                    return Common.checkStringInJSONObjectResponse(userIdJson, "id");
                }
            }
        } catch (JSONException e) {
            logger.error("JSONException for getInstagramProfileId: " + e.getMessage());
        }
        return null;
    }

    public InstagramMetrics getInstagramMetricProfileData(String userName) {
        instagramMetrics = new InstagramMetrics();
        if (!(instagramUser.equals("NDA"))) {
            String instagramUserName;
            if (StringUtils.isNumeric(userName)) {
                instagramUserName = userName;
            } else {
                instagramUserName = getInstagramProfileId(userName);
            }
            JSONObject basicUserProfile[] = null, setuserMediaJson[];
            setuserMediaJson = getInstagramRecentMediaInfo(instagramUserName);
            if (instagramUserName != null) {
                basicUserProfile = getInstagramProfileInfo(instagramUserName);
                setuserMediaJson = getInstagramRecentMediaInfo(instagramUserName);
            }
            String beanUserName = null;
            try {
                if (basicUserProfile[1] != null) {
                    beanUserName = instagramUserName;
                    instagramMetrics.setPosts(basicUserProfile[1].getInt("media"));
                    instagramMetrics.setFollowers(basicUserProfile[1].getInt("followed_by"));
                    instagramMetrics.setFollwoing(basicUserProfile[1].getInt("follows"));
                    instagramMetrics.setInstagramAccountStatus(true);
                }
                if (setuserMediaJson[0] != null) {
                    instagramMetrics.setRecentMediaComments(setuserMediaJson[0].getInt("count"));
                }
                if (setuserMediaJson[1] != null) {
                    instagramMetrics.setRecentMediaLikes(setuserMediaJson[1].getInt("count"));
                }
                if (setuserMediaJson[3] != null) {
                    instagramMetrics.setPostDate(this.getInstagramPostTime(setuserMediaJson[3].getString("created_time")));
                }
                if (setuserMediaJson[4] != null) {
                    instagramMetrics.setPostText(setuserMediaJson[4].getString("text").trim());
                }
                instagramMetrics.setEngagement(instagramMetrics.getRecentMediaLikes() + instagramMetrics.getRecentMediaComments());
                instagramMetrics.setAudienceEngaged(Common.getAudienceEngaged(instagramMetrics.getEngagement(), instagramMetrics.getFollowers()));
                String tempPostUrl = null;
                String postid = null;
                String writePath = null;
                if (setuserMediaJson[2] != null) {
                    if (setuserMediaJson[2].getString("url") != null) {
                        tempPostUrl = setuserMediaJson[2].getString("url");
                        beanUserName = beanUserName.substring(2, beanUserName.length() - 2);
                        postid = beanUserName + "InstagramPost";
                        writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(tempPostUrl);

                    }
                } else if (basicUserProfile[0].getString("profile_picture") != null) {
                    tempPostUrl = basicUserProfile[0].getString("profile_picture");
                    beanUserName = beanUserName.substring(2, beanUserName.length() - 2);
                    postid = beanUserName + "InstagramPost";
                    writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(tempPostUrl);

                }
                if (200 == Common.checkImageUrl(tempPostUrl)) {
                    if ((Common.resizeImageFromUrlandSave(tempPostUrl, writePath))) {
                        instagramMetrics.setInstagramAccountStatus(true);
                        instagramMetrics.setInstapostImageUrl("/social_images/" + postid + "." + Common.getImageFormat(tempPostUrl));
                    }
                }
            } catch (Exception e) {
                logger.error("Exception for getInstagramMetricProfileData " + e.getMessage());
            }
        }
        return instagramMetrics;
    }

    public JSONObject[] getInstagramProfileInfo(String userId) {
        JSONObject[] InstagarmMediaJSONArray = new JSONObject[2];
        JSONObject userProfileJson = null, instagramdataJson = null;
        try {
            JSONObject instagramProfileJson = Common.getResponseFromAPI(getUserProfileInformationUrl(userId));
            /*logger.debug("The Resposne  from  instagarm api in getInstagramProfileInfo"+instagramProfileJson);*/
            if ((Common.checkJSONObjectInJSONObjectResponse(instagramProfileJson, "data")) != null) {
                instagramdataJson = instagramProfileJson.getJSONObject("data");
            }
            if ((Common.checkJSONObjectInJSONObjectResponse(instagramdataJson, "counts")) != null) {
                userProfileJson = instagramdataJson.getJSONObject("counts");
            }
            //userProfileJson = instagramdataJson.getJSONObject("counts");
            InstagarmMediaJSONArray[0] = instagramdataJson;
            InstagarmMediaJSONArray[1] = userProfileJson;
            return InstagarmMediaJSONArray;
        } catch (Exception e) {
            logger.error("Exeception for getInstagramProfileInfo" + e.getMessage());
        }
        return InstagarmMediaJSONArray;
    }

    @SuppressWarnings("empty-statement")
    public JSONObject[] getInstagramRecentMediaInfo(String userId) {
        JSONObject userMediaJson = null, userCommentsJson = null, captionJson = null, userlikesJson = null, imagesJson = null, low_resolution = null;
        JSONObject[] InstagarmMediaJSONArray = new JSONObject[5];
        JSONArray instagramdataJson = null;
        try {
            JSONObject instagramRecentMediaJson = Common.getResponseFromAPI(getUserRecentMediaUrl(userId));;
            if ((Common.checkJSONArrayInJSONObjectResponse(instagramRecentMediaJson, "data")) != null) {
                instagramdataJson = instagramRecentMediaJson.getJSONArray("data");
                /*logger.info("the data in instagramdataJson"+instagramdataJson);*/
 /*logger.info("the length of instagramdataJson"+instagramdataJson.length());*/
            }
            /*logger.info("The response from instagarm api in getInstagramRecentMediaInfo"+instagramdataJson);*/
            if (instagramdataJson != null) {
                userMediaJson = instagramdataJson.getJSONObject(0);
            }

            if (userMediaJson != null) {
                if ((Common.checkJSONObjectInJSONObjectResponse(userMediaJson, "comments")) != null) {
                    userCommentsJson = userMediaJson.getJSONObject("comments");
                }
                if ((Common.checkJSONObjectInJSONObjectResponse(userMediaJson, "caption")) != null) {
                    captionJson = userMediaJson.getJSONObject("caption");
                }
                if ((Common.checkJSONObjectInJSONObjectResponse(userMediaJson, "likes")) != null) {
                    userlikesJson = userMediaJson.getJSONObject("likes");
                }
                if ((Common.checkJSONObjectInJSONObjectResponse(userMediaJson, "images")) != null) {
                    imagesJson = userMediaJson.getJSONObject("images");
                }
            }/*End of userMediaJson*/
            if (imagesJson != null) {
                if ((Common.checkJSONObjectInJSONObjectResponse(imagesJson, "low_resolution")) != null) {
                    low_resolution = imagesJson.getJSONObject("low_resolution");
                } else if ((Common.checkJSONObjectInJSONObjectResponse(imagesJson, "thumbnail")) != null) {
                    low_resolution = imagesJson.getJSONObject("thumbnail");
                } else if ((Common.checkJSONObjectInJSONObjectResponse(imagesJson, "standard_resolution")) != null) {
                    low_resolution = imagesJson.getJSONObject("standard_resolution");
                } else {
                    low_resolution = null;
                }
            }
            InstagarmMediaJSONArray[0] = userCommentsJson;
            InstagarmMediaJSONArray[1] = userlikesJson;
            InstagarmMediaJSONArray[2] = low_resolution;
            InstagarmMediaJSONArray[3] = userMediaJson;
            InstagarmMediaJSONArray[4] = captionJson;
            return InstagarmMediaJSONArray;
        } catch (Exception e) {
            logger.error("Exeception for getInstagramRecentMediaInfo" + e.getMessage());
        }
        return InstagarmMediaJSONArray;
    }

    public static String getSearchUserIdUrl(String userName) {
        return "https://api.instagram.com/v1/users/search?q=" + userName + "&format=json&client_id=" + APIKeys.instagramClientId + "";
    }

    public String getUserProfileInformationUrl(String UserId) {
        return "https://api.instagram.com/v1/users/" + UserId + "?format=json&client_id=" + APIKeys.instagramClientId + "";
    }

    public String getUserRecentMediaUrl(String UserId) {
        return "https://api.instagram.com/v1/users/" + UserId + "/media/recent/?format=json&client_id=" + APIKeys.instagramClientId + "";
    }

    public String getInstagramPostTime(String instaPosttime) {
        long millis = Long.parseLong(instaPosttime) * 1000;
        Date date = new Date(millis);
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

}
