package lxr.marketplace.apiaccess;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import lxr.marketplace.apiaccess.lxrmbeans.BackLinkInfo;
import lxr.marketplace.apiaccess.lxrmbeans.DomainInfo;
import lxr.marketplace.apiaccess.majesticseoutil.MajesticSeoAPIService;
import lxr.marketplace.apiaccess.majesticseoutil.MajesticSeoDataTable;
import lxr.marketplace.apiaccess.majesticseoutil.MajesticSeoResponse;

public class MajesticSeoService {
	private static String app_api_key = "5962699FB65728934065E7D922216F74";
	// private static String enterpriseEndpoint = "http://enterprise.majesticseo.com/api_command";
	private static String developerEndPoint = "http://developer.majesticseo.com/api_command";

	public void getTopBackLinks(String itemToQuery, ArrayList<BackLinkInfo> backLinks,
			ArrayList<DomainInfo> refDomains) {
		String endpoint = developerEndPoint;
		Map<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("URL", itemToQuery);
		parameters.put("datasource", "fresh");
		parameters.put("Count", "100");
		parameters.put("ShowDomainInfo", "1");
//		parameters.put("GetRootDomainData", "1");

		MajesticSeoAPIService apiService = new MajesticSeoAPIService(app_api_key, endpoint);
		MajesticSeoResponse response = apiService.executeCommand("GetTopBackLinks", parameters);

		if (response.isOK()) {
			MajesticSeoDataTable results = response.getTableForName("URL");
			for (Map<String, String> row : results.getTableRows()) {
				BackLinkInfo backLink = createBackLinkObjFromRow(row);
				backLinks.add(backLink);				
			}			
			MajesticSeoDataTable domainInfo = response.getTableForName("DomainsInfo");
			for (Map<String, String> row : domainInfo.getTableRows()) {
				DomainInfo refDomain = createRefDomainObjFromRow(row);
				refDomains.add(refDomain);
			}
		} else {
			System.out.println("ERROR MESSAGE: " + response.getErrorMessage());
		}
	}

	public void getBackLinkData(String itemToQuery, ArrayList<BackLinkInfo> backLinks,
			ArrayList<DomainInfo> refDomains) {
		String endpoint = developerEndPoint;
		// set up parameters
		Map<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("item", itemToQuery);
		parameters.put("datasource", "fresh");
		parameters.put("Count", "100");
		parameters.put("Mode", "1");
		parameters.put("ShowDomainInfo", "1");
//		parameters.put("GetRootDomainData", "1");

		MajesticSeoAPIService apiService = new MajesticSeoAPIService(app_api_key, endpoint);
		MajesticSeoResponse response = apiService.executeCommand("GetBackLinkData", parameters);

		if (response.isOK()) {
			MajesticSeoDataTable results = response.getTableForName("BackLinks");
//			String totalBackLinks = results.getParamForName("TotalBackLinks");
//			System.out.println("TotalExternalBackLinks :"+totalBackLinks);
			

			for (Map<String, String> row : results.getTableRows()) {
				BackLinkInfo backLink = createBackLinkObjFromRow(row);
				backLinks.add(backLink);				
			}
			
			MajesticSeoDataTable domainInfo = response.getTableForName("DomainsInfo");
			for (Map<String, String> row : domainInfo.getTableRows()) {
				DomainInfo refDomain = createRefDomainObjFromRow(row);
				refDomains.add(refDomain);
			}
		} else {
			System.out.println("ERROR MESSAGE: " + response.getErrorMessage());
		}
	}
	
	/* We can get domains on Ip and domains on subnet both from this method 
	 * Currently commented code is to get domains on subnet*/
	
	public ArrayList<DomainInfo> getReverseIpLookup(String itemToQuery) {
		String endpoint = developerEndPoint;
		ArrayList<DomainInfo> domainsOnIp = new ArrayList<DomainInfo>();
//		ArrayList<DomainInfo> domainsOnSubnet = new ArrayList<DomainInfo>();
		if (!itemToQuery.matches("[0-9\\.]+")) {
			try {
				InetAddress domain = java.net.InetAddress.getByName(itemToQuery);
				itemToQuery = domain.getHostAddress();
				System.out.println(itemToQuery);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		Map<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("Domain", itemToQuery);
		parameters.put("datasource", "fresh");
		parameters.put("MaxDomains", "100");

		MajesticSeoAPIService apiService = new MajesticSeoAPIService(app_api_key, endpoint);
		MajesticSeoResponse response = apiService.executeCommand("GetHostedDomains", parameters);

		if (response.isOK()) {
			MajesticSeoDataTable ipResults = response.getTableForName("DomainsOnIP");

			for (Map<String, String> row : ipResults.getTableRows()) {
				DomainInfo domain = new DomainInfo();
				domain.setDomain(row.get("Domain"));
				if(row.get("ExtBackLinks") != null && !row.get("ExtBackLinks").equals("")){
					domain.setExtBackLinkCount(Long.parseLong(row.get("ExtBackLinks")));
				}
				if( row.get("RefDomains") != null && !row.get("RefDomains").equals("")){
					domain.setRefDomainCount(Long.parseLong(row.get("RefDomains")));
				}
				domainsOnIp.add(domain);
			}

//			MajesticSeoDataTable subnetResults = response.getTableForName("DomainsOnSubnet");
//			for (Map<String, String> row : subnetResults.getTableRows()) {
//				DomainInfo domain = new DomainInfo();
//				domain.setDomain(row.get("Domain"));
//				if(row.get("ExtBackLinks") != null && !row.get("ExtBackLinks").equals("")){
//					domain.setExtBackLinkCount(Long.parseLong(row.get("ExtBackLinks")));
//				}
//				if( row.get("RefDomains") != null && !row.get("RefDomains").equals("")){
//					domain.setRefDomainCount(Long.parseLong(row.get("RefDomains")));
//				}
//				domainsOnSubnet.add(domain);
//			}
		} else {
			System.out.println("ERROR MESSAGE: " + response.getErrorMessage());
		}
		return domainsOnIp;
	}
	
	public DomainInfo getDomainOverview(String domain){//No alexaRank,crawledURLCount,firstCrawldate,countryCode
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		DomainInfo domainObj = new DomainInfo();
		String endpoint = developerEndPoint;
		
		Map<String, String> parameters = new LinkedHashMap<String, String>();		
		parameters.put("datasource", "fresh");
		parameters.put("items", "1");
		try {
			parameters.put("item0", URLEncoder.encode(domain, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		parameters.put("Mode", "1");
		parameters.put("ShowDomainInfo", "1");

		MajesticSeoAPIService apiService = new MajesticSeoAPIService(app_api_key, endpoint);
		MajesticSeoResponse response = apiService.executeCommand("GetIndexItemInfo", parameters);
		if (response.isOK()) {
			MajesticSeoDataTable domainOverview = response.getTableForName("Results");		
			for (Map<String, String> row : domainOverview.getTableRows()) {
				if(row.get("Status").equals("Found")){
					Date lastCrawledDate = new Date();
					try {
						String lastCrawldate = row.get("LastCrawlDate");
						if(lastCrawldate != null && !lastCrawldate.equals("")){
							lastCrawledDate = sdf.parse(row.get("LastCrawlDate"));
						}else{
							lastCrawledDate = null;
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Calendar lastCrawledCal = Calendar.getInstance();
					if(lastCrawledDate == null){
						lastCrawledCal = null;
					}else{
						lastCrawledCal.setTime(lastCrawledDate);
					}
					domainObj.setDomain(row.get("Item"));
					if( row.get("RefDomains") != null && !row.get("RefDomains").equals("")){
						domainObj.setRefDomainCount(Long.parseLong(row.get("RefDomains")));
					}
					if( row.get("ExtBackLinks") != null && !row.get("ExtBackLinks").equals("")){
						domainObj.setExtBackLinkCount(Long.parseLong(row.get("ExtBackLinks")));
					}
					if( row.get("IndexedURLs") != null && !row.get("IndexedURLs").equals("")){
						domainObj.setIndexedURLCount(Long.parseLong(row.get("IndexedURLs")));
					}
					if(lastCrawledCal != null){domainObj.setLastCrawlDate(lastCrawledCal);}
					if (row.get("CitationFlow") != null && !row.get("CitationFlow").equals("")) {
						domainObj.setCitationFlow(Float.parseFloat(row.get("CitationFlow")));
					} else {
						domainObj.setCitationFlow(0);
					}
					if (row.get("TrustFlow") != null && !row.get("TrustFlow").equals("")) {
						domainObj.setTrustFlow(Float.parseFloat(row.get("TrustFlow")));
					} else {
						domainObj.setTrustFlow(0);
					}
				}else{
					domainObj = null;
				}
			}
		}else {
			System.out.println("ERROR MESSAGE: " + response.getErrorMessage());
		}
		System.out.println("Domain Overview: "+ domainObj);
		return domainObj;
	}
	
	public DomainInfo createRefDomainObjFromRow(Map<String, String> row){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		DomainInfo refDomain = new DomainInfo();
		Date firstCrawledDate = new Date();
		Date lastCrawledDate = new Date();
		try {
			firstCrawledDate = sdf.parse(row.get("FirstCrawled"));
			lastCrawledDate = sdf.parse(row.get("LastSuccessfulCrawl"));
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		Calendar firstCrawledCal = Calendar.getInstance();
		firstCrawledCal.setTime(firstCrawledDate);
		Calendar lastCrawledCal = Calendar.getInstance();
		lastCrawledCal.setTime(lastCrawledDate);
		
		refDomain.setDomain(row.get("Domain"));
		if( row.get("AlexaRank") != null && !row.get("AlexaRank").equals("")){
			refDomain.setAlexaRank(Long.parseLong(row.get("AlexaRank")));
		}
		if( row.get("RefDomains") != null && !row.get("RefDomains").equals("")){
			refDomain.setRefDomainCount(Long.parseLong(row.get("RefDomains")));
		}
		if( row.get("ExtBackLinks") != null && !row.get("ExtBackLinks").equals("")){
			refDomain.setExtBackLinkCount(Long.parseLong(row.get("ExtBackLinks")));
		}
		if( row.get("IndexedURLs") != null && !row.get("IndexedURLs").equals("")){
			refDomain.setIndexedURLCount(Long.parseLong(row.get("IndexedURLs")));
		}
		if( row.get("CrawledURLs") != null && !row.get("CrawledURLs").equals("")){
			refDomain.setCrawledURLCount(Long.parseLong(row.get("CrawledURLs")));
		}
		refDomain.setFirstCrawldate(firstCrawledCal);
		refDomain.setLastCrawlDate(lastCrawledCal);
		refDomain.setCountryCode(row.get("CountryCode"));
		if (row.get("CitationFlow") != null && !row.get("CitationFlow").equals("")) {
			refDomain.setCitationFlow(Float.parseFloat(row.get("CitationFlow")));
		} else {
			refDomain.setCitationFlow(0);
		}
		if (row.get("TrustFlow") != null && !row.get("TrustFlow").equals("")) {
			refDomain.setTrustFlow(Float.parseFloat(row.get("TrustFlow")));
		} else {
			refDomain.setTrustFlow(0);
		}
		return refDomain;
	}
	
	public BackLinkInfo createBackLinkObjFromRow(Map<String, String> row){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		BackLinkInfo backLink = new BackLinkInfo();
		Date lastCrawledDate = new Date();
		try {
			lastCrawledDate = sdf.parse(row.get("Date"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(lastCrawledDate);
		
		backLink.setSourceURL(row.get("SourceURL"));
		backLink.setAnchorText(row.get("AnchorText"));
		backLink.setLastCrawledDate(cal);
		backLink.setNoFollow(row.get("FlagNoFollow").equals("1") ? true : false);
		backLink.setTargetURL(row.get("TargetURL"));
		if (row.get("TargetCitationFlow") != null && !row.get("TargetCitationFlow").equals("")){
			backLink.setTargetCitationFlow(Float.parseFloat(row.get("TargetCitationFlow")));
		} else {
			backLink.setTargetCitationFlow(0);
		}
		if (row.get("TargetTrustFlow") != null && !row.get("TargetTrustFlow").equals("")) {
			backLink.setTargetTrustFlow(Float.parseFloat(row.get("TargetTrustFlow")));
		} else {
			backLink.setTargetTrustFlow(0);
		}
		if (row.get("SourceCitationFlow") != null && !row.get("SourceCitationFlow").equals("")) {
			backLink.setSourceCitationFlow(Float.parseFloat(row.get("SourceCitationFlow")));
		} else {
			backLink.setSourceCitationFlow(0);
		}
		if (row.get("SourceTrustFlow") != null && !row.get("SourceTrustFlow").equals("")) {
			backLink.setSourceTrustFlow(Float.parseFloat(row.get("SourceTrustFlow")));
		} else {
			backLink.setSourceTrustFlow(0);
		}
		return backLink;
	}
}