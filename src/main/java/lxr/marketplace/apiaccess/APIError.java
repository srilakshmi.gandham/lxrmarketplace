/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.sql.Timestamp;

/**
 *
 * @author sagar
 */
public class APIError {
    
    public String apiName;
    public Timestamp issueIdentified;
    public String errorMessage;
    public int toolId;

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public Timestamp getIssueIdentified() {
        return issueIdentified;
    }

    public void setIssueIdentified(Timestamp issueIdentified) {
        this.issueIdentified = issueIdentified;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getToolId() {
        return toolId;
    }

    public void setToolId(int toolId) {
        this.toolId = toolId;
    }
    
    
    
}