package lxr.marketplace.apiaccess;

import lxr.marketplace.apiaccess.lxrmbeans.GoogleMetrics;
import lxr.marketplace.util.Common;
import lxr.marketplace.socialmedia.APIKeys;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GooglePlusMetricsAPIService implements Runnable {

    private static Logger logger = Logger.getLogger(GooglePlusMetricsAPIService.class);
    private String googleUser;
    private GoogleMetrics googleMetrics;
    private ConcurrentHashMap<Integer, String> googleMap;

    public ConcurrentHashMap<Integer, String> getGoogleMap() {
        return googleMap;
    }

    public void setGoogleMap(ConcurrentHashMap<Integer, String> googleMap) {
        this.googleMap = googleMap;
    }

    public String getGoogleUser() {
        return googleUser;
    }

    public void setGoogleUser(String googleUser) {
        this.googleUser = googleUser;
    }

    public GoogleMetrics getGoogleMetrics() {
        return googleMetrics;
    }

    public void setGoogleMetrics(GoogleMetrics googleMetrics) {
        this.googleMetrics = googleMetrics;
    }

    public GooglePlusMetricsAPIService() {
    }

    public GooglePlusMetricsAPIService(String googleUser, ConcurrentHashMap<Integer, String> googleMap) {
        this.googleUser = googleUser;
        this.googleMap = googleMap;
    }

    @Override
    public void run() {
        try {
            setGoogleMetrics(getGoogleMetrics(googleUser));
        } catch (Exception e) {
            setGoogleMetrics(new GoogleMetrics());
            logger.error("Exception in GooglePlus API Service: ", e);
        }
        googleMap.put(3, "Google");
    }

    public static boolean checkGoogleUser(String username) {
        return checkGoogleUserExistence(username);
    }

    /*Search for google user profile based on user names from query URL*/
    public static boolean checkGoogleUserExistence(String googleUserName) {
        try {
            /*Response contains a user profile for user existsince or contains error code.*/
            JSONObject googlePlusJson = getGooglePlusUserProfileJson(googleUserName);
            logger.debug("googlePlusJson: " + googlePlusJson);
            if (googlePlusJson != null) {
                if (googlePlusJson.has("error")) {
                    APIErrorHandlingService.raiseGoogleAPIError(googlePlusJson, APIErrorHandlingService.GOOGLE_PLUS, Common.SOCIAL_MEDIA_ANALYZER, 29, googleUserName);
                    Thread.sleep(5000);
                }
                if (googlePlusJson.has("displayName")) {
                    String beanUserName = googlePlusJson.getString("displayName");
                    if (beanUserName != null) {
                        return true;
                    }
                }
            }
        } catch (InterruptedException | JSONException e) {
            logger.error("Exception in checkGoogleUserExistence: ", e);
        }
        return false;
    }

    public GoogleMetrics getGoogleMetrics(String googleuserName) {
        googleMetrics = new GoogleMetrics();
        if (!(googleUser.equals("NDA"))) {
            try {
                /*                if ((googleuserName).indexOf("+") == 0) {
                    userName = getGooglePlusUserIDJSONObject(googleuserName.replace("+", " ").trim());
                } else {
                    userName = googleuserName;
                }*/
                JSONObject googlePlusJson = getGooglePlusUserProfileJson(googleuserName);
                if (googlePlusJson != null) {
                    if (googlePlusJson.has("error")) {
                        try {
                            APIErrorHandlingService.raiseGoogleAPIError(googlePlusJson, APIErrorHandlingService.GOOGLE_PLUS, Common.SOCIAL_MEDIA_ANALYZER, 29, googleuserName);
                            Thread.sleep(5000);
                        } catch (InterruptedException ex) {
                            logger.error("InterruptedException in checkGoogleUserExistence cause: " + ex.getMessage());
                        }
                    }
                }
                if (googlePlusJson != null) {
                    if ((Common.checkStringInJSONObjectResponse(googlePlusJson, "id")) != null) {
                        googleMetrics.setUserName(googlePlusJson.getString("id"));
                    }
                    if ((Common.checkStringInJSONObjectResponse(googlePlusJson, "circledByCount")) != null) {
                        googleMetrics.setFollowers(googlePlusJson.getLong("circledByCount"));
                    }
                    if ((Common.checkStringInJSONObjectResponse(googlePlusJson, "plusOneCount")) != null) {
                        googleMetrics.setCircledByCount(googlePlusJson.getLong("plusOneCount"));
                    }
                }
                googleMetrics.setGoogleAccountStatus(true);
                JSONObject[] GoogleUserActivitesJson = getGooglePlusUserActivitesJson(googleuserName);
                if (GoogleUserActivitesJson[0] != null) {
                    googleMetrics.setRecentPostContent(GoogleUserActivitesJson[0].getString("content").trim());
                }
                if (GoogleUserActivitesJson[1] != null) {
                    googleMetrics.setRecentPostReplies(GoogleUserActivitesJson[1].getLong("totalItems"));
                }
                if (GoogleUserActivitesJson[2] != null) {
                    googleMetrics.setRecentPosplusoners(GoogleUserActivitesJson[2].getLong("totalItems"));
                }
                if (GoogleUserActivitesJson[3] != null) {
                    googleMetrics.setRecentPostResharers(GoogleUserActivitesJson[3].getLong("totalItems"));
                }
                if (GoogleUserActivitesJson[6] != null && GoogleUserActivitesJson[6].has("published")) {
                    String tempgoogleDate = GoogleUserActivitesJson[6].getString("published");
                    if (tempgoogleDate != null) {
                        googleMetrics.setGooglePostDate(Common.getDateFormatFromGoogleandYouTube(tempgoogleDate));
                    }
                }

                googleMetrics.setEngagement(googleMetrics.getRecentPostReplies() + googleMetrics.getRecentPosplusoners() + googleMetrics.getRecentPostResharers());
                googleMetrics.setAudienceEngaged(Common.getAudienceEngaged(googleMetrics.getEngagement(), googleMetrics.getFollowers()));
                String googlePostImageUrl = null;
                String postid = null;
                String goolgewritePath = null;
                if (GoogleUserActivitesJson[4] != null) {
                    if (GoogleUserActivitesJson[4].getString("url") != null) {
                        googlePostImageUrl = GoogleUserActivitesJson[4].getString("url");
                        postid = "Google" + googleuserName.substring(1, googleuserName.length() - 1);
                        goolgewritePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(googlePostImageUrl);
                    }
                }
                if ((Common.resizeImageFromUrlandSave(googlePostImageUrl, goolgewritePath))) {
                    String googleImagePath = "/social_images/" + postid + "." + Common.getImageFormat(googlePostImageUrl);
                    googleMetrics.setGoogleImageStatus(true);
                    googleMetrics.setGoogleImagePath(googleImagePath);
                }
                if (googleMetrics.getRecentPostContent().equals("") && (GoogleUserActivitesJson[5] != null && GoogleUserActivitesJson[5].getString("displayName") != null)) {
                    googleMetrics.setRecentPostContent(GoogleUserActivitesJson[5].getString("displayName"));
                }
            } catch (Exception e) {
                logger.error("Excpetion for  seting GoogleMetrics of GoolgePlusAPIService: ", e);
            }
        }
        return googleMetrics;
    }

    /*To get Google plus Id for query URL*/
    public static String getGooglePlusUserIDJSONObject(String userName) {
        String userId = null;
//        JSONObject googlePlusJson = Common.getResponseFromAPI(getGooglePlusUserIDURL(userName));
        JSONObject googlePlusJson = Common.getJSONFromAPIResponse(getGooglePlusUserProfileURL(userName));
        try {
            if (googlePlusJson != null && googlePlusJson.has("items")) {
                JSONArray googlePlusDataJson = googlePlusJson.getJSONArray("items");
                if (googlePlusDataJson != null) {
                    JSONObject subuserIdJson = googlePlusDataJson.getJSONObject(0);
                    if (subuserIdJson != null) {
                        userId = (String) subuserIdJson.get("id");
                    }
                }
            }
            if ((googlePlusJson != null) && (googlePlusJson.has("error"))) {
                try {
                    APIErrorHandlingService.raiseGoogleAPIError(googlePlusJson, APIErrorHandlingService.GOOGLE_PLUS, Common.SOCIAL_MEDIA_ANALYZER, 29, userName);
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException in checkGoogleUserExistence cause: " + ex.getMessage());
                }
            }
        } catch (JSONException e) {
            logger.error("Exception for getGooglePlusUserIDJSONObject", e);
        }
        return userId;
    }

    public static JSONObject getGooglePlusUserProfileJson(String userID) {
        return Common.getJSONFromAPIResponse(getGooglePlusUserProfileURL(userID));
    }

    /*To get metrics related to recent post */
    public JSONObject[] getGooglePlusUserActivitesJson(String userID) {
        JSONObject[] GoogleUserActivitesJson = new JSONObject[7];
        JSONArray itemsJsonArray, attachments;
        JSONObject subitemsJson = null, plusActivteobject = null, replies = null, plusoners = null, resharers = null, subattachments = null, fullImage = null;
        JSONObject googlePlusJson;
        try {
            googlePlusJson = Common.getResponseFromAPI(getGooglePlusUserRecentActiviteURL(userID));
            /*logger.info("The response from google api in getGooglePlusUserActivitesJson"+googlePlusJson);*/
            if ((Common.checkJSONArrayInJSONObjectResponse(googlePlusJson, "items")) != null) {
                /*googlePlusJson.getJSONArray("items") != null*/
                itemsJsonArray = googlePlusJson.getJSONArray("items");
                if (itemsJsonArray != null && itemsJsonArray.length() != 0) {
                    subitemsJson = itemsJsonArray.getJSONObject(0);
                } else {
                    subitemsJson = new JSONObject();
                    GoogleUserActivitesJson[0] = plusActivteobject;
                    GoogleUserActivitesJson[1] = replies;
                    GoogleUserActivitesJson[2] = plusoners;
                    GoogleUserActivitesJson[3] = resharers;
                    GoogleUserActivitesJson[6] = subitemsJson;
                    GoogleUserActivitesJson[4] = fullImage;
                }
                if ((Common.checkJSONObjectInJSONObjectResponse(subitemsJson, "object")) != null) {
                    plusActivteobject = subitemsJson.getJSONObject("object");

                    if ((Common.checkJSONObjectInJSONObjectResponse(plusActivteobject, "replies")) != null) {
                        replies = plusActivteobject.getJSONObject("replies");
                    }
                    if ((Common.checkJSONObjectInJSONObjectResponse(plusActivteobject, "plusoners")) != null) {
                        plusoners = plusActivteobject.getJSONObject("plusoners");
                    }
                    if ((Common.checkJSONObjectInJSONObjectResponse(plusActivteobject, "resharers")) != null) {
                        resharers = plusActivteobject.getJSONObject("resharers");
                    }
                    GoogleUserActivitesJson[0] = plusActivteobject;
                    GoogleUserActivitesJson[1] = replies;
                    GoogleUserActivitesJson[2] = plusoners;
                    GoogleUserActivitesJson[3] = resharers;
                    GoogleUserActivitesJson[6] = subitemsJson;
                    GoogleUserActivitesJson[5] = subattachments;
                    /*JSONObject attachments*/
                    if ((Common.checkJSONArrayInJSONObjectResponse(plusActivteobject, "attachments")) != null) {
                        attachments = plusActivteobject.getJSONArray("attachments");
                        subattachments = attachments.getJSONObject(0);
                        GoogleUserActivitesJson[5] = subattachments;

                        if ((Common.checkJSONObjectInJSONObjectResponse(subattachments, "image")) != null) {
                            fullImage = subattachments.getJSONObject("image");

                        } else if ((Common.checkJSONObjectInJSONObjectResponse(subattachments, "thumbnails")) != null) {
                            JSONArray thumbnails = subattachments.getJSONArray("thumbnails");
                            JSONObject subthumbnails = thumbnails.getJSONObject(0);
                            fullImage = subthumbnails.getJSONObject("image");
                        }
                    } else if ((Common.checkJSONObjectInJSONObjectResponse(subitemsJson, "actor")) != null) {
                        JSONObject actor = subitemsJson.getJSONObject("actor");
                        fullImage = actor.getJSONObject("image");
                    }
                    GoogleUserActivitesJson[4] = fullImage;
                }
            }
            /*End of ITEMS*/
        } catch (Exception e) {
            GoogleUserActivitesJson[0] = plusActivteobject;		// For Content
            GoogleUserActivitesJson[1] = replies;			// For Content replies
            GoogleUserActivitesJson[2] = plusoners;
            GoogleUserActivitesJson[3] = resharers;
            GoogleUserActivitesJson[5] = subattachments;
            GoogleUserActivitesJson[6] = subitemsJson;
            GoogleUserActivitesJson[4] = fullImage;
            logger.error("Exception for  getGooglePlusUserActivitesJson: ", e);
        }
        return GoogleUserActivitesJson;
    }

    public static String getGooglePlusUserIDURL(String userName) {
        logger.debug("Request URL in getGooglePlusUserIDURL: " + "https://www.googleapis.com/plus/v1/people?query=" + userName + "&maxResults=1&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/plus/v1/people?query=" + userName + "&maxResults=1&key=" + APIKeys.googleKey + "";
    }

    public static String getGooglePlusUserProfileURL(String userID) {
        logger.debug("Request URL in getGooglePlusUserProfileURL: " + "https://www.googleapis.com/plus/v1/people/" + userID + "?key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/plus/v1/people/" + userID + "?key=" + APIKeys.googleKey + "";
    }

    public String getGooglePlusUserRecentActiviteURL(String userID) {
        logger.debug("Request URL in getGooglePlusUserRecentActiviteURL: " + "https://www.googleapis.com/plus/v1/people/" + userID + "/activities/public?maxResults=1&key=" + APIKeys.googleKey + "");
        return "https://www.googleapis.com/plus/v1/people/" + userID + "/activities/public?maxResults=1&key=" + APIKeys.googleKey + "";
    }

}