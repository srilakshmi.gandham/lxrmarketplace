package lxr.marketplace.apiaccess;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.Analytics.Data.Ga.Get;
import com.google.api.services.analytics.model.Account;
import com.google.api.services.analytics.model.Accounts;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.Profile;
import com.google.api.services.analytics.model.Profiles;
import com.google.api.services.analytics.model.Webproperties;
import com.google.api.services.analytics.model.Webproperty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import lxr.marketplace.apiaccess.lxrmbeans.AnalyticsData;
import org.springframework.stereotype.Service;

@Service
public class GoogleAnalyticsAPIService {

    private static Logger logger = Logger.getLogger(GoogleAnalyticsAPIService.class);
    // Production & Testing credentials
    private static String CLIENT_ID = "527122863964-iq1qjemo034cakohar8ihhq56qll196b.apps.googleusercontent.com";
    private static String CLIENT_SECRET = "xFtudue9Foh_HpvCOifZwXMJ";

//    private static String REDIRECT_URL = "http://localhost:8080/penalty-checker-tool.html?gAnalytics=gAnalytics";
//    private static String REDIRECT_URL = "https://localhost/penalty-checker-tool.html?gAnalytics=gAnalytics";
    private static String REDIRECT_URL = "https://lxrmarketplace.com/penalty-checker-tool.html?gAnalytics=gAnalytics";
//    private static String REDIRECT_URL = "https://testlxrmarketplace.netelixir.com/penalty-checker-tool.html?gAnalytics=gAnalytics";

    private static String APPLICATION_NAME = "LXRMarketplace";

    public String generateAuthorizationUrl() {
        GoogleAnalyticsAPIService googleAnalytics = new GoogleAnalyticsAPIService();
        String authorizationUrl = googleAnalytics.analyticsUserLogin();
        logger.debug("authorizationUrl : " + authorizationUrl);
        return authorizationUrl;
    }

    public String analyticsUserLogin() {
        GoogleAuthorizationCodeRequestUrl authorizationUrl1 = new GoogleAuthorizationCodeRequestUrl(CLIENT_ID, REDIRECT_URL, Arrays.asList("https://www.googleapis.com/auth/analytics.readonly"));
        authorizationUrl1.setAccessType("offline");
        authorizationUrl1.setApprovalPrompt("force");
        String authorizationUrl = authorizationUrl1.build();
        logger.debug("Authorization Url: " + authorizationUrl);
        return authorizationUrl;
    }

    public List<String> getAnalyticsData(String accessToken, String refreshToken, String profileId) {
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(netHttpTransport)
                .setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
        credential.setAccessToken(accessToken);
        credential.setRefreshToken(refreshToken);
        Analytics analytics = new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_YEAR, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String timeStamp1 = sdf.format(cal1.getTime());
        String timeStamp2 = sdf.format(cal2.getTime());
        logger.debug("DateRange: " + timeStamp1 + " : " + timeStamp2);
        try {
            List<String> analyticsData = fetchOneDaysAnalyticsData(analytics, profileId, timeStamp1);
            return analyticsData;
        } catch (IOException e) {
            logger.info("Exception while fetching the Analytics Data: ", e);
        }
        return null;
    }

    /* METHOD NOT IN USE */
 /* public GaData createQuery(Analytics analytics, String profileId, String timeStamp1, String timeStamp2) throws IOException{
		Get apiQuery = null;
		apiQuery = analytics.data().ga().get("ga:" + profileId, timeStamp1, timeStamp2,
				"ga:visits, ga:visitors, ga:pageviewsPerVisit, ga:itemRevenue, ga:transactions");
		apiQuery.setDimensions("ga:day");
		apiQuery.setSort("-ga:day");
        try {
            GaData gaData = apiQuery.execute();
            logger.debug("total results:" + gaData.getTotalResults());
            long num = 0;
            for (int i = 1; i <= gaData.getTotalResults(); i = i + 10000) {
                if (num < gaData.getTotalResults()) {
                    apiQuery.setStartIndex(i);
                    apiQuery.setMaxResults(10000);
                    GaData gaData1 = apiQuery.execute();
                    printData(gaData1);
                    num = num + 10000;
                }
            }
            return gaData;
        } catch (GoogleJsonResponseException e) {
        	e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
		return null;
	}*/
 /* METHOD NOT IN USE */
 /* public String fetchCurrency(String accessToken, String refreshToken, String profileId){
		logger.info("fetching currency code");
		String currency =""; 
		NetHttpTransport netHttpTransport = new NetHttpTransport();
		JacksonFactory jacksonFactory = new JacksonFactory();
		GoogleCredential credential =new GoogleCredential.Builder().setTransport(netHttpTransport)
		.setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
		credential.setAccessToken(accessToken);
		credential.setRefreshToken(refreshToken);
		Analytics analytics=new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();
		logger.info("fetching currency code analytics object" + analytics );
		Calendar cal2 = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String timeStamp2 = sdf.format(cal2.getTime());
		try {
			Get apiQuery = null;
			apiQuery = analytics.data().ga().get("ga:" + profileId,timeStamp2, timeStamp2,"ga:itemRevenue");
			//apiQuery.setDimensions("ga:date");
			apiQuery.setDimensions("ga:currencyCode");
            GaData gaData = apiQuery.execute();  
            List<List<String>> rowValues = gaData.getRows();
            logger.info("rowValues for currency :"+ rowValues.get(0).get(0));
	        currency = gaData.get("ga:currencyCode").toString();
	        logger.info("currency code :"+ currency);
	        return currency;
		}catch (Exception e) {
			logger.error("exception in fetching currency code");
			e.printStackTrace();
		}
		return currency;
	}*/
    public List<String> fetchOneDaysAnalyticsData(Analytics analytics, String profileId, String timeStamp) throws IOException {
        Get apiQuery = null;
//		apiQuery = analytics.data().ga().get("ga:" + profileId, timeStamp, timeStamp,
//				"ga:visits, ga:visitors, ga:pageviewsPerVisit, ga:itemRevenue, ga:transactions");
        apiQuery = analytics.data().ga().get("ga:" + profileId, timeStamp, timeStamp,
                "ga:sessions, ga:users, ga:itemRevenue, ga:transactions");
        apiQuery.setDimensions("ga:date");
        apiQuery.setSort("-ga:date");

        try {
            GaData gaData = apiQuery.execute();
            printData(gaData);
            List<List<String>> rowValues = gaData.getRows();
            if (rowValues != null) {
                logger.debug("rowValues :" + rowValues.get(0).get(0));
                return rowValues.get(0);
            } else {
                return null;
            }
        } catch (GoogleJsonResponseException e) {
            logger.error(e);

        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    public List<String> printData(GaData gaData) {
        List<List<String>> rowValues = gaData.getRows();
        if (rowValues != null) {
            return rowValues.get(0);
        } else {
            return null;
        }
    }

    /*	public void printNewData(GaData gaData){
		List<List<String>> totgDataRows = gaData.getRows();
		if(totgDataRows != null &&  totgDataRows.size() > 0){
			List<String> oneGdataRow = totgDataRows.get(0);
			for (String value : oneGdataRow) {
			}
		}
	}*/
    public GoogleTokenResponse createAnalyticsSetup(String authorizationCode) {
        GoogleTokenResponse analyticsResponse = null;
        try {
            logger.debug("creating setup");
            NetHttpTransport netHttpTransport = new NetHttpTransport();
            JacksonFactory jacksonFactory = new JacksonFactory();
            analyticsResponse = new GoogleAuthorizationCodeTokenRequest(netHttpTransport,
                    jacksonFactory, CLIENT_ID, CLIENT_SECRET, authorizationCode, REDIRECT_URL).execute();
            logger.debug("analyticsResponse :" + analyticsResponse);
            if (analyticsResponse != null) {
                logger.info("Access Token:" + analyticsResponse.getAccessToken());
                logger.info("Refresh Token:" + analyticsResponse.getRefreshToken());
                logger.info("Scope:" + analyticsResponse.getScope());
                logger.info("Expires In:" + analyticsResponse.getExpiresInSeconds());
                return analyticsResponse;
            } else {
                logger.info("No Response from Analytics");
            }
        } catch (IOException ioe) {
            logger.error(ioe);
        }
        return analyticsResponse;
    }

    public String fetchUserAnalyticsProfiles(GoogleTokenResponse analyticsResponse, Profiles[] profilesArray) {
        String accountId = null;
        Profiles profiles;
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(netHttpTransport)
                .setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
        credential.setAccessToken(analyticsResponse.getAccessToken());
        credential.setRefreshToken(analyticsResponse.getRefreshToken());
        Analytics analytics = new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();
        try {
            // Get a list of Google Analytics accounts the user
            // has access to.
            Accounts accounts = analytics.management().accounts().list().execute();
            if (accounts != null && accounts.size() > 0) {

                for (Account account : accounts.getItems()) {
                    accountId = account.getId();
                    logger.info("Account ID: " + account.getId());
                    logger.info("Account Name: " + account.getName());
                    logger.info("Account Created: " + account.getCreated());
                    logger.info("Account Updated: " + account.getUpdated());

                    profiles = analytics.management().profiles()
                            .list("" + account.getId(), "~all").execute();
                    for (Profile profile : profiles.getItems()) {
                        logger.debug("Account ID: " + profile.getAccountId());
                        logger.debug("Web Property ID: " + profile.getWebPropertyId());
                        logger.debug("Web Property Internal ID: " + profile.getInternalWebPropertyId());
                        logger.debug("Profile ID: " + profile.getId());
                        logger.debug("Profile Name: " + profile.getName());
                        logger.debug("Profile Currency: " + profile.getCurrency());
                        logger.debug("Profile Timezone: " + profile.getTimezone());
                        logger.debug("Profile Updated: " + profile.getUpdated());
                        logger.debug("Profile Created: " + profile.getCreated());
                        logger.debug("Profile WebsiteUrl: " + profile.getWebsiteUrl());
                    }
                    if (profiles != null) {
//				    	  if(profiles.size()==1){
//				    		  Profile profile = (Profile) profiles.get(0);
//				    		  getAnalyticsData(analyticsResponse.getAccessToken(), analyticsResponse.getRefreshToken(),profile.getId());
//			    				return null;
//			    		     }
                        logger.info("profiles   " + profiles);
                        profilesArray[0] = profiles;
                        return accountId;
                    }
                }
                //insertUserAnalyticsData(analyticsResponse,accounts);
            } else {
                logger.info("user dont have any Analytics account");
            }
        } catch (Exception e) {
            logger.error("user dont have any Google Analytics account");
            if (e instanceof HttpResponseException) {
                handleApiError((HttpResponseException) e, jacksonFactory);
            }
            return null;
        }
        return null;
    }

    //For LXRSEO settings
    public String fetchUserAnalyticsProfilesVersion2(String accountId, String accessToken, String refreshToken,
            Profiles[] profilesArray) {
        Profiles profiles;
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(netHttpTransport)
                .setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
        credential.setAccessToken(accessToken);
        credential.setRefreshToken(refreshToken);
        Analytics analytics = new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();
        try {
            // Get a list of Google Analytics accounts the user has access to.					
            profiles = analytics.management().profiles().list("" + accountId, "~all").execute();
            if (profiles != null) {
                for (Profile profile : profiles.getItems()) {
                    logger.debug("Account ID: " + profile.getAccountId());
                    logger.debug("Web Property ID: " + profile.getWebPropertyId());
                    logger.debug("Web Property Internal ID: " + profile.getInternalWebPropertyId());
                    logger.debug("Profile ID: " + profile.getId());
                    logger.debug("Profile Name: " + profile.getName());
                    logger.debug("Profile Currency: " + profile.getCurrency());
                    logger.debug("Profile Timezone: " + profile.getTimezone());
                    logger.debug("Profile Updated: " + profile.getUpdated());
                    logger.debug("Profile Created: " + profile.getCreated());
                }
                logger.info("profiles   " + profiles);
                profilesArray[0] = profiles;
                return accountId;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("User dont have any Google Analytics account");
            if (e instanceof HttpResponseException) {
                handleApiError((HttpResponseException) e, jacksonFactory);
            }
            return null;
        }
    }

//	public Profiles createSetUp(String authorizationCode,long campId){
//		GoogleTokenResponse analyticsResponse = null;
//		
//		try {
//			logger.info("creating setup");
//			NetHttpTransport netHttpTransport = new NetHttpTransport();
//			JacksonFactory jacksonFactory = new JacksonFactory();
//			analyticsResponse = new GoogleAuthorizationCodeTokenRequest(netHttpTransport,
//                    jacksonFactory,CLIENT_ID, CLIENT_SECRET,authorizationCode,REDIRECT_URL).execute();
////			analyticsResponse = new GoogleAccessTokenRequest.GoogleAuthorizationCodeGrant(
////					netHttpTransport, jacksonFactory, CLIENT_ID, CLIENT_SECRET, authorizationCode,
////					REDIRECT_URL).execute();
//			logger.info("analyticsResponse :"+analyticsResponse);
//			if (analyticsResponse != null) {
//				logger.info("Access Token:" + analyticsResponse.getAccessToken());
//				logger.info("Refresh Token:" + analyticsResponse.getRefreshToken());
//				logger.info("Scope:" + analyticsResponse.getScope());
//				logger.info("Expires In:" + analyticsResponse.getExpiresInSeconds());
//				//TODO: SAVE ACCESS TOKEN, REFRESH TOKEN, EXPIRES IN in our database
//                if(analyticsResponse.getAccessToken() != null && analyticsResponse.getRefreshToken() != null) {
//                logger.info("insert analytics credentials in our database");
//                  insertUserAnalyticsData(analyticsResponse,campId); 
//    				
//                	// Use the access and refresh tokens to get a new
//					// GoogleAccessProtectedResource.
////					GoogleAccessProtectedResource googleAccessProtectedResource = new GoogleAccessProtectedResource(
////							analyticsResponse.accessToken, netHttpTransport, jacksonFactory, CLIENT_ID,
////							CLIENT_SECRET, analyticsResponse.refreshToken);
////
//					
//					
//					GoogleCredential credential =new GoogleCredential.Builder().setTransport(netHttpTransport)
//					.setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
//					credential.setAccessToken(analyticsResponse.getAccessToken());
//					credential.setRefreshToken(analyticsResponse.getRefreshToken());
//					Analytics analytics=new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();
//					try {
//						// Get a list of Google Analytics accounts the user
//						// has access to.
//						Accounts accounts = analytics.management().accounts().list().execute();
//						if(accounts != null && accounts.size() !=0){
//							for (Account account : accounts.getItems()) {
//								logger.info("Account ID: " + account.getId());
//								logger.info("Account Name: " + account.getName());
//								logger.info("Account Created: " + account.getCreated());
//								logger.info("Account Updated: " + account.getUpdated());
//	
//								Profiles profiles = analytics.management().profiles()
//										.list("" + account.getId(), "~all").execute();
//								
//								//TODO: SHOW LIST OF PROFILE IDs in setup screen and let user choose his profile id
//								for (Profile profile : profiles.getItems()) {
//									logger.info("Account ID: " + profile.getAccountId());
//									logger.info("Web Property ID: " + profile.getWebPropertyId());
//									logger.info("Web Property Internal ID: "
//											+ profile.getInternalWebPropertyId());
//									logger.info("Profile ID: " + profile.getId());
//									logger.info("Profile Name: " + profile.getName());
//									logger.info("Profile Currency: " + profile.getCurrency());
//									logger.info("Profile Timezone: " + profile.getTimezone());
//									logger.info("Profile Updated: " + profile.getUpdated());
//									logger.info("Profile Created: " + profile.getCreated());	
//								}
//								if(profiles != null){
//							    	  if(profiles.size()==1){
//							    		  Profile profile = (Profile) profiles.get(0);
//							    		  getAnalyticsData(analyticsResponse.getAccessToken(), analyticsResponse.getRefreshToken(),profile.getId());
//						    				return null;
//						    		     }
//							    	 return profiles; 
//							      }
//						 	}
//							//insertUserAnalyticsData(analyticsResponse,accounts);
//						}else{
//							logger.info("user dont have any Analytics account");
//						}
//					} catch(Exception e){
//						logger.info("user dont have any Google Analytics account");
//						//e.printStackTrace();
//						if( e instanceof HttpResponseException){
//							handleApiError((HttpResponseException) e,jacksonFactory);
//						}
//						return null;
//					}
////					}catch (com.google.api.client.http.HttpResponseException e) {
////						// Handle API specific errors.
////						 handleApiError(e,jacksonFactory);
////						//e.printStackTrace();
////
////					} catch (IOException ioe) {
////						// Handle errors that occur while parsing the response.
////						ioe.printStackTrace();
////					}
//				}
//			} else {
//				logger.info("No Response from Analytics");
//			}
//		} catch (IOException ioe) {
//			ioe.printStackTrace();
//		}
//		return null;
//	}
    private void handleApiError(HttpResponseException errorResponse, JacksonFactory jacksonFactory) {
        if (errorResponse.getStatusCode() == 401) {
            // Code 401 means the user authorization token is invalid.
            // The application can address this error by enabling the user to get a new token.
            logger.info("Invalid authorization token, please get a new one.");
        } else {
            // All other status codes indicate an API or service issue.
            // See the error message for details.
            logger.info(errorResponse.getStatusCode() + " Error: " + errorResponse.getStatusMessage());
        }
    }

    public List<String> fetchAllAnalyticsProfiles(GoogleTokenResponse analyticsResponse,
            List<Profiles> accountProfilesList, HashMap<String, String> analyticsWebPropertiesList) {
        return fetchAllAnalyticsProfiles(analyticsResponse.getAccessToken(), analyticsResponse.getRefreshToken(), accountProfilesList, analyticsWebPropertiesList);
    }

    public List<String> fetchAllAnalyticsProfiles(String accessToken, String refreshToken, List<Profiles> accountProfilesList,
            HashMap<String, String> analyticsWebPropertiesList) {
        List<String> accountNames = new ArrayList<>();
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(netHttpTransport)
                .setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
        credential.setAccessToken(accessToken);
        credential.setRefreshToken(refreshToken);
        Analytics analytics = new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();
        try {
            Accounts accounts = analytics.management().accounts().list().execute();
            if (accounts != null && accounts.size() > 0) {
                for (Account account : accounts.getItems()) {
                    logger.info("Account ID: " + account.getId() + " Account Name: " + account.getName());
                    Profiles accProfiles = analytics.management().profiles().list("" + account.getId(), "~all").execute();

                    try {
                        Webproperties properties = analytics.management().webproperties().list("" + account.getId()).execute();
                        for (Webproperty property : properties.getItems()) {
                            logger.info("Property Name: " + property.getName() + " Id:" + property.getId());
                            analyticsWebPropertiesList.put(property.getId(), property.getName());
                        }
                    } catch (GoogleJsonResponseException e) {
                        logger.info(+e.getDetails().getCode() + " : " + e.getDetails().getMessage());
                    }

                    if (accProfiles != null && accProfiles.size() > 0) {
                        accountProfilesList.add(accProfiles);
                        accountNames.add(account.getName());
                        for (Profile profile : accProfiles.getItems()) {
                            logger.info("Account ID: " + profile.getAccountId() + " Profile ID: " + profile.getId() + " Profile Name: " + profile.getName());
                        }
                    }
                }
            } else {
                logger.info("user dont have any Analytics account");
            }
        } catch (Exception e) {
            logger.error("user dont have any Google Analytics account");
            if (e instanceof HttpResponseException) {
                handleApiError((HttpResponseException) e, jacksonFactory);
            }
        }
        return accountNames;
    }

    public Map<String, AnalyticsData> fetchLastnDaysAnalyticsData(String accessToken, String refreshToken, String profileId, String startDate, String endDate, String analyticsFetchType, String device) {
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(netHttpTransport)
                .setJsonFactory(jacksonFactory).setClientSecrets(CLIENT_ID, CLIENT_SECRET).build();
        credential.setAccessToken(accessToken);
        credential.setRefreshToken(refreshToken);
        Analytics analytics = new Analytics.Builder(netHttpTransport, jacksonFactory, credential).setApplicationName(APPLICATION_NAME).build();

        Map<String, AnalyticsData> analyticsDataSet = new LinkedHashMap<>();
        try {
            fetchAnalyticsOrganicData(analytics, profileId, startDate, endDate, analyticsDataSet, analyticsFetchType, device);
//            fetchAnalyticsTotalData(analytics, profileId, timeStamp1, timeStamp2, analyticsDataSet, analyticsStatus, "week");
            return analyticsDataSet;
        } catch (IOException e) {
            logger.error("Eception in fetching analytics data:", e);
        }
        return null;
    }

    private void fetchAnalyticsOrganicData(Analytics analytics, String profileId, String startDay, String endDay,
            Map<String, AnalyticsData> analyticsDataSet, String type, String device) throws IOException {
        Get apiQuery = analytics.data().ga().get("ga:" + profileId, startDay, endDay,
                //                "ga:sessions, ga:transactions, ga:transactionRevenue");
                "ga:sessions, ga:transactions, ga:transactionRevenue, ga:newUsers");

        String typeDimention = "";
        if (type.equalsIgnoreCase("date")) {
            typeDimention = "ga:date";
//            apiQuery.setDimensions("ga:date");
//            apiQuery.setSegment("sessions::condition::ga:medium==organic");
        } else if (type.equalsIgnoreCase("month")) {
            typeDimention = "ga:yearMonth";
//            apiQuery.setDimensions("ga:yearMonth");
//            apiQuery.setFilters("ga:medium==organic");
//            apiQuery.setSegment("sessions::condition::ga:medium==organic");
        } else if (type.equalsIgnoreCase("week")) {
            typeDimention = "ga:yearWeek";
//            apiQuery.setDimensions("ga:yearWeek");
//            apiQuery.setFilters("ga:medium==organic");
//            apiQuery.setSegment("sessions::condition::ga:medium==organic");
        }
        String deviceFilter = "";
        if (device.equals("mobile")) {
            deviceFilter = ",ga:deviceCategory==mobile";
        } else if (device.equals("tablet")) {
            deviceFilter = ",ga:deviceCategory==tablet";
        } else if (device.equals("desktop")) {
            deviceFilter = ",ga:deviceCategory==desktop";
        }

        try {
            if (!device.equals("")) {
//                apiQuery.setDimensions("ga:source," + typeDimention + ",ga:deviceCategory");
                apiQuery.setDimensions(typeDimention + ",ga:deviceCategory");
//                apiQuery.setSort("-" + typeDimention);
                apiQuery.setFilters("ga:medium==organic");
                GaData gaData = apiQuery.execute();
                List<List<String>> rowValues = gaData.getRows();
                if (rowValues != null) {
                    for (List<String> analyticsDataRow : rowValues) {
                        if (!analyticsDataSet.containsKey(analyticsDataRow.get(0))) {
                            if (analyticsDataRow.get(1).equals(device)) {
                                AnalyticsData analyticsData = new AnalyticsData.Builder()
                                        .organicVisits(analyticsDataRow.get(2))
                                        .organicConversions(analyticsDataRow.get(3))
                                        .organicRevenue(analyticsDataRow.get(4))
                                        //                                .build();
                                        .newUsers(analyticsDataRow.get(5)).build();
                                analyticsDataSet.put(analyticsDataRow.get(0), analyticsData);
                            }
                        } else if (analyticsDataRow.get(1).equals(device)) {
                            AnalyticsData analyticsData = analyticsDataSet.get(analyticsDataRow.get(0));
                            analyticsData.setOrganicVisits(analyticsDataRow.get(2));
                            analyticsData.setOrganicConversions(analyticsDataRow.get(3));
                            analyticsData.setOrganicRevenue(analyticsDataRow.get(4));
                            analyticsData.setNewUsers(analyticsDataRow.get(5));
                        }
                    }
                }
            } else {
                apiQuery.setDimensions(typeDimention);
//                apiQuery.setSort("-" + typeDimention);
                apiQuery.setFilters("ga:medium==organic");
                GaData gaData = apiQuery.execute();
                List<List<String>> rowValues = gaData.getRows();
                if (rowValues != null) {
                    for (List<String> analyticsDataRow : rowValues) {
                        if (!analyticsDataSet.containsKey(analyticsDataRow.get(0))) {
                            AnalyticsData analyticsData = new AnalyticsData.Builder()
                                    .organicVisits(analyticsDataRow.get(1))
                                    .organicConversions(analyticsDataRow.get(2))
                                    .organicRevenue(analyticsDataRow.get(3))
                                    //                                .build();
                                    .newUsers(analyticsDataRow.get(4)).build();
                            analyticsDataSet.put(analyticsDataRow.get(0), analyticsData);
                        } else {
                            AnalyticsData analyticsData = analyticsDataSet.get(analyticsDataRow.get(0));
                            analyticsData.setOrganicVisits(analyticsDataRow.get(1));
                            analyticsData.setOrganicConversions(analyticsDataRow.get(2));
                            analyticsData.setOrganicRevenue(analyticsDataRow.get(3));
                            analyticsData.setNewUsers(analyticsDataRow.get(4));
                        }
                    }
                }
            }
        } catch (GoogleJsonResponseException e) {
            logger.error("GoogleJsonResponseException while fetching analytics data: ", e);
        } catch (IOException e) {
            logger.error("IOException while fetching analytics data: ", e);
        }
    }

    private void fetchAnalyticsTotalData(Analytics analytics, String profileId, String satrtDay, String endDay,
            Map<String, AnalyticsData> analyticsDataSet, StringBuilder analyticsStatus, String type) throws IOException {
        Get apiQuery = null;
        apiQuery = analytics.data().ga().get("ga:" + profileId, satrtDay, endDay,
                "ga:sessions, ga:transactions, ga:transactionRevenue");
        if (type.equalsIgnoreCase("date")) {
            apiQuery.setDimensions("ga:date");
            apiQuery.setSort("-ga:date");
        } else if (type.equalsIgnoreCase("month")) {
            apiQuery.setDimensions("ga:yearMonth");
        }

        try {
            GaData gaData = apiQuery.execute();
            List<List<String>> rowValues = gaData.getRows();
            if (rowValues != null) {
                for (List<String> analyticsDataRow : rowValues) {
                    if (!analyticsDataSet.containsKey(analyticsDataRow.get(0))) {
                        AnalyticsData analyticsData = new AnalyticsData.Builder()
                                .visits(analyticsDataRow.get(1))
                                .conversions(analyticsDataRow.get(2))
                                .revenue(analyticsDataRow.get(3)).build();
                        logger.info("fetchAnalyticsTotalData");
//                        logger.info("Visits: " + analyticsDataRow.get(1) + " Conversions: " + analyticsDataRow.get(2) + " Revenue: " + analyticsDataRow.get(3));
                        analyticsDataSet.put(analyticsDataRow.get(0), analyticsData);
                    } else {
                        AnalyticsData analyticsData = analyticsDataSet.get(analyticsDataRow.get(0));
                        analyticsData.setVisits(analyticsDataRow.get(1));
                        analyticsData.setConversions(analyticsDataRow.get(2));
                        analyticsData.setRevenue(analyticsDataRow.get(3));
                    }
                }
            }
        } catch (GoogleJsonResponseException gre) {
            analyticsStatus.append("connection failed");
            logger.error(gre);
        } catch (IOException e) {
            analyticsStatus.append("connection failed");
            logger.error(e);
        }
    }

}