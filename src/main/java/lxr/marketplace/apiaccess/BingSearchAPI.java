package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lxr.marketplace.apiaccess.lxrmbeans.SearchOrganicLink;
import lxr.marketplace.util.Common;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BingSearchAPI {

    private static Logger logger = Logger.getLogger(BingSearchAPI.class);
    private static final int URL_COUNT_LIMIT = 100;
    private static final int RESULTS_PER_QUERY = 50;
    private String bingSearchApiKey;
    private String bingSearchApiSubscriptionKey;
    // public static void main(String[] args) {
    // BingSearchAPI bingSearchAPI = new BingSearchAPI();
    // try {
    // // bingSearchAPI.bingIndexedPagesCount("www.lxrmarketplace.com");
    // ArrayList<SearchOrganicLink> top5Links = new
    // ArrayList<SearchOrganicLink>();
    // bingSearchAPI.getBingSearchDataV1("netelixir", "www.netelixir.com",
    // top5Links, "th-TH");
    // } catch (IOException e) {
    // e.printStackTrace();
    // } catch (JSONException e) {
    // e.printStackTrace();
    // }
    // }

//        public static void main(String[] args) {
//               BingSearchAPI a = new BingSearchAPI();
//               List<String> domains = new ArrayList<>();
//               domains.add("www.bestbuy.com");
//               a.getBingSearchDataV5("laptops", domains, "");
//        }
    public String getBingSearchApiSubscriptionKey() {
        return bingSearchApiSubscriptionKey;
    }

    public void setBingSearchApiSubscriptionKey(String bingSearchApiSubscriptionKey) {
        this.bingSearchApiSubscriptionKey = bingSearchApiSubscriptionKey;
    }

    public String getBingSearchApiKey() {
        return bingSearchApiKey;
    }

    public void setBingSearchApiKey(String bingSearchApiKey) {
        this.bingSearchApiKey = bingSearchApiKey;
    }

    public long bingIndexedPagesCount(String domain) throws IOException,
            JSONException {
        // String bingUrl =
        // "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Composite?"
        // + "Sources=" + URLEncoder.encode("'web'", "UTF-8") + "&Query='"
        // + URLEncoder.encode("site:" + domain, "UTF-8") +
        // "'&$top=1&$format=json";
        String bingUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Composite?Query='"
                + URLEncoder.encode("site:" + domain, "UTF-8")
                + "'&$top=1&$format=json";
        byte[] accountKeyBytes = Base64.encodeBase64((bingSearchApiKey + ":" + bingSearchApiKey).getBytes());
        String accountKeyEnc = new String(accountKeyBytes);

        URL url = new URL(bingUrl);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setRequestProperty("Authorization", String.format("Basic %s", accountKeyEnc));
        BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = br.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());
        JSONArray jsonArray = jsonObject.getJSONObject("d").getJSONArray(
                "results");
        long indexedPages = jsonArray.getJSONObject(0).getLong("WebTotal");
        logger.debug("IndexedPages for " + domain + " = " + indexedPages);
        // System.out.println("IndexedPages for " + domain +
        // " = "+indexedPages);
        return indexedPages;
    }

    public SearchOrganicLink getBingSearchDataV1(String keyword,
            String domainToSearch, ArrayList<SearchOrganicLink> top5Links,
            String market) throws IOException, JSONException {
        // String bingUrl =
        // "https://api.datamarket.azure.com/Bing/Search/Web?Query='"
        // + URLEncoder.encode(keyword, "UTF-8") + "'&Market='" + market +
        // "'&$top=" + URL_COUNT_LIMIT
        // + "&$format=json";
        String bingUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?Query='"
                + URLEncoder.encode(keyword, "UTF-8")
                + "'&Market='" + market
                + "'&$top=" + URL_COUNT_LIMIT + "&$format=json";
        byte[] accountKeyBytes = Base64
                .encodeBase64((bingSearchApiKey + ":" + bingSearchApiKey)
                        .getBytes());
        String accountKeyEnc = new String(accountKeyBytes);

        URL url = new URL(bingUrl);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setRequestProperty("Authorization",
                String.format("Basic %s", accountKeyEnc));
        BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = br.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());
        SearchOrganicLink domainLink = searchInJsonV1(jsonObject, domainToSearch);
        logger.debug("Position: " + domainLink.getPosition() + " Link: " + domainLink.getUrl());
        getTop5Result(jsonObject, top5Links);
        // printTopLinks(top5Links);
        return domainLink;
    }

    public SearchOrganicLink searchInJsonV1(JSONObject jsonObject, String domain) {
        SearchOrganicLink domainLink = null;
        try {
            JSONArray jsonArray = jsonObject.getJSONObject("d").getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject rec = jsonArray.getJSONObject(i);
                String link = rec.getString("Url");
                if (link.contains(domain)) {
                    String displayLink = rec.getString("DisplayUrl");
                    String snippet = rec.getString("Description");
                    String title = rec.getString("Title");
                    domainLink = new SearchOrganicLink(link, displayLink, title, snippet);
                    domainLink.setPosition(i + 1);
                    break;
                }
            }
        } catch (JSONException e) {
            logger.error(e);
        }
        return domainLink;
    }

    public HashMap<String, SearchOrganicLink> getBingSearchDataV2(
            String keyword, List<String> domains, String market) {
        boolean resultLessThanLimit = false;
        boolean gotResult = false;
        HashMap<String, SearchOrganicLink> results = new HashMap<>();
        Set<String> domainSet = new HashSet<>();
        for (String domain : domains) {
            domainSet.add(domain);
            SearchOrganicLink tempResult = new SearchOrganicLink();
            tempResult.setPosition(-1);
            results.put(domain, tempResult);
        }
        int skip = 0;
        while (!domainSet.isEmpty()
                && skip + RESULTS_PER_QUERY <= URL_COUNT_LIMIT) {
            // String bingUrl =
            // "https://api.datamarket.azure.com/Bing/Search/Web?Query='"
            // + URLEncoder.encode(keyword, "UTF-8") +
            // "'&Market='"+market+"'&$top=" + RESULTS_PER_QUERY +
            // "&$skip="+skip
            // + "&$format=json";
            String bingUrl = "";
            try {
                bingUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?Query='"
                        + URLEncoder.encode(keyword, "UTF-8")
                        + "'&Market='"
                        + market
                        + "'&$top="
                        + RESULTS_PER_QUERY
                        + "&$skip="
                        + skip
                        + "&$format=json";
            } catch (UnsupportedEncodingException e) {
                logger.error(e);
                break;
            }
            byte[] accountKeyBytes = Base64.encodeBase64((bingSearchApiKey
                    + ":" + bingSearchApiKey).getBytes());
            String accountKeyEnc = new String(accountKeyBytes);
            URL url;
            try {
                url = new URL(bingUrl);
            } catch (MalformedURLException e) {
                logger.error(e);
                break;
            }
            URLConnection urlConnection;
            StringBuilder responseStrBuilder = new StringBuilder();
            try {
                urlConnection = url.openConnection();
                urlConnection.setRequestProperty("Authorization",
                        String.format("Basic %s", accountKeyEnc));
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (urlConnection.getInputStream())));
                String inputStr;
                while ((inputStr = br.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
            } catch (IOException e) {
                logger.error(e);
                break;
            }

            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(responseStrBuilder.toString());
            } catch (JSONException e) {
                logger.error(e);
                break;
            }
            try {
                resultLessThanLimit = searchInJsonV2(jsonObject, domainSet, results, skip);
            } catch (JSONException e) {
                logger.error(e);
                break;
            }
            gotResult = true;
            skip += RESULTS_PER_QUERY;
        }
        if (resultLessThanLimit || !gotResult) {
            for (String domain : domains) {
                SearchOrganicLink tempResult = results.get(domain);
                if (tempResult.getPosition() == -1) {
                    tempResult.setPosition(0);
                }
            }
        }
        return results;
    }

    public HashMap<String, SearchOrganicLink> getBingSearchDataV5(
            String keyword, List<String> domains, String market) {
        boolean resultLessThanLimit = false;
        boolean gotResult = false;
        HashMap<String, SearchOrganicLink> results = new HashMap<>();
        Set<String> domainSet = new HashSet<>();
        for (String domain : domains) {
            domainSet.add(domain);
            SearchOrganicLink tempResult = new SearchOrganicLink();
            tempResult.setPosition(-1);
            results.put(domain, tempResult);
        }
//		int skip = 0;
        while (!domainSet.isEmpty() && !gotResult) {
//				&& skip + RESULTS_PER_QUERY <= URL_COUNT_LIMIT) {
            // String bingUrl =
            // "https://api.datamarket.azure.com/Bing/Search/Web?Query='"
            // + URLEncoder.encode(keyword, "UTF-8") +
            // "'&Market='"+market+"'&$top=" + RESULTS_PER_QUERY +
            // "&$skip="+skip
            // + "&$format=json";
            JSONObject jsonObject = bingKeywordSearchResultsV5(keyword, market);
            if (jsonObject != null) {
                try {
                    resultLessThanLimit = searchInJsonV5(jsonObject, domainSet, results);
                } catch (JSONException e) {
                    logger.error("Exception in fetching data from json object", e);
                    break;
                }
            }
            gotResult = true;
//			skip += RESULTS_PER_QUERY;
        }
        if (resultLessThanLimit || !gotResult) {
            for (String domain : domains) {
                SearchOrganicLink tempResult = results.get(domain);
                if (tempResult.getPosition() == -1) {
                    tempResult.setPosition(0);
                }
            }
        }
        return results;
    }

    public List<SearchOrganicLink> getBingSearchDataForaKeyword(String keyword, ArrayList<SearchOrganicLink> top10Links,
            String market) throws IOException, JSONException {
        // String bingUrl =
        // "https://api.datamarket.azure.com/Bing/Search/Web?Query='"
        // + URLEncoder.encode(keyword, "UTF-8") + "'&Market='" + market +
        // "'&$top=" + URL_COUNT_LIMIT
        // + "&$format=json";
        String bingUrl = "https://api.datamarket.azure.com/Bing/SearchWeb/v1/Web?Query='"
                + URLEncoder.encode(keyword, "UTF-8")
                + "'&Market='" + market
                + "'&$top=" + URL_COUNT_LIMIT + "&$format=json";
        byte[] accountKeyBytes = Base64
                .encodeBase64((bingSearchApiKey + ":" + bingSearchApiKey)
                        .getBytes());
        String accountKeyEnc = new String(accountKeyBytes);

        URL url = new URL(bingUrl);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setRequestProperty("Authorization",
                String.format("Basic %s", accountKeyEnc));
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (urlConnection.getInputStream())));
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        while ((inputStr = br.readLine()) != null) {
            responseStrBuilder.append(inputStr);
        }
        JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());
        logger.debug("bingUrl: " + bingUrl);
        getTop10Result(jsonObject, top10Links);
        // printTopLinks(top5Links);
        return top10Links;
    }

    public boolean searchInJsonV2(JSONObject jsonObject, Set<String> domainSet,
            HashMap<String, SearchOrganicLink> results, int skip) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONObject("d").getJSONArray(
                "results");
        for (int i = 0; i < jsonArray.length() && !domainSet.isEmpty(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            String link = rec.getString("Url");
            for (String domain : domainSet) {
                if (link.contains(domain)) {
                    String displayLink = rec.getString("DisplayUrl");
                    String snippet = rec.getString("Description");
                    String title = rec.getString("Title");
                    SearchOrganicLink domainLink = new SearchOrganicLink(
                            link, displayLink, title, snippet);
                    domainLink.setPosition(i + 1 + skip);
                    results.put(domain, domainLink);
                    domainSet.remove(domain);
                    break;
                }
            }
        }
        if (jsonArray.length() < 50) {
            return true;
        } else {
            return false;
        }
    }

    public boolean searchInJsonV5(JSONObject jsonObject, Set<String> domainSet,
            HashMap<String, SearchOrganicLink> results) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONObject("webPages").getJSONArray(
                "value");
        for (int i = 0; i < jsonArray.length() && !domainSet.isEmpty(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
//                        System.out.println("new API " +rec);
            String link = rec.getString("url");

            for (String domain : domainSet) {
                if (link.contains(domain)) {
                    String displayLink = rec.getString("displayUrl");
//                                         System.out.println("new API " +displayLink);
                    String snippet = rec.getString("snippet");
                    String title = rec.getString("name");
                    SearchOrganicLink domainLink = new SearchOrganicLink(
                            link, displayLink, title, snippet);
                    domainLink.setPosition(i + 1);
                    results.put(domain, domainLink);
                    domainSet.remove(domain);
                    break;
                }
            }
        }
        if (jsonArray.length() < 50) {
            return true;
        } else {
            return false;
        }
    }

    public void getTop5Result(JSONObject jsonObject, ArrayList<SearchOrganicLink> top5Links) {
        try {
            JSONArray jsonArray = jsonObject.getJSONObject("d").getJSONArray("results");
            for (int i = 0; i < jsonArray.length() && i < 5; i++) {
                JSONObject rec = jsonArray.getJSONObject(i);
                String link = rec.getString("Url");
                String displayLink = rec.getString("DisplayUrl");
                String snippet = rec.getString("Description");
                String title = rec.getString("Title");
                SearchOrganicLink bingOrganicLink = new SearchOrganicLink(link,
                        displayLink, title, snippet);
                bingOrganicLink.setPosition(i + 1);
                top5Links.add(bingOrganicLink);
            }
        } catch (JSONException e) {
            logger.error(e);
        }
    }

    public void getTop10Result(JSONObject jsonObject, ArrayList<SearchOrganicLink> top10Links) {
        try {
            JSONArray jsonArray = jsonObject.getJSONObject("d").getJSONArray(
                    "results");
            for (int i = 0; i < jsonArray.length() && i < 10; i++) {
                JSONObject rec = jsonArray.getJSONObject(i);
                String link = rec.getString("Url");
                String displayLink = rec.getString("DisplayUrl");
                String snippet = rec.getString("Description");
                String title = rec.getString("Title");
                SearchOrganicLink bingOrganicLink = new SearchOrganicLink(link,
                        displayLink, title, snippet);
                bingOrganicLink.setPosition(i + 1);
                top10Links.add(bingOrganicLink);
            }
        } catch (JSONException e) {
            logger.error(e);
        }
    }

    public List<SearchOrganicLink> getBingSearchDataForaKeywordV5(String keyword, ArrayList<SearchOrganicLink> top10Links,
            String market) throws IOException, JSONException {
        JSONObject jsonObject = bingKeywordSearchResultsV5(keyword, market);
        if (jsonObject != null) {
            logger.debug("jsonObject: " + jsonObject.toString());
            notifyBingSearchAPIError(jsonObject);
            getTop10ResultsV5(jsonObject, top10Links);
        }
        return top10Links;
    }

    public JSONObject bingKeywordSearchResultsV5(String keyword, String market) {
        String queryURL = "https://api.cognitive.microsoft.com/bing/v5.0/search?q=" + keyword
                + "&count=" + URL_COUNT_LIMIT + "&offset=0&mkt=" + market + "&safesearch=Moderate";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(queryURL)
                .get()
                .addHeader("ocp-apim-subscription-key", bingSearchApiSubscriptionKey)
                .addHeader("cache-control", "no-cache")
                .build();
        logger.debug("Query URL for bingKeywordSearchResultsV5: " + request.toString() + ", and queryURL: " + queryURL);
        Response response = null;
        String jsonData = null;
        JSONObject jsonObject = null;
        try {
            response = client.newCall(request).execute();
            jsonData = response.body().string();
            if (jsonData != null) {
                jsonObject = new JSONObject(jsonData);
            }
        } catch (JSONException | IOException e) {
            logger.error("JSONException | IOException in json data cause: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception in bingKeywordSearchResultsV5 cause: " + e);
        }
        return jsonObject;
    }

    public void getTop10ResultsV5(JSONObject jsonObject, ArrayList<SearchOrganicLink> top10Links) {
        String link = null;
        String displayLink = null;
        String snippet = null;
        String title = null;
        try {
            if (jsonObject.has("webPages")) {
                JSONObject innerObject = jsonObject.getJSONObject("webPages");
                JSONArray jsonArray = null;
                if (innerObject != null && innerObject.has("value")) {
                    jsonArray = innerObject.getJSONArray("value");
                }
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length() && i < 10; i++) {
                        JSONObject rec = jsonArray.getJSONObject(i);
                        logger.debug("rec: " + rec);
                        if (rec.has("url")) {
                            link = getUrlFromSearchQueryURL(rec.getString("url"));
                        }
                        if (rec.has("displayUrl")) {
                            displayLink = rec.getString("displayUrl");
                        }
                        if (rec.has("snippet")) {
                            snippet = rec.getString("snippet");
                        }
                        if (rec.has("name")) {
                            title = rec.getString("name");
                        }
                        SearchOrganicLink bingOrganicLink = new SearchOrganicLink(link, displayLink, title, snippet);
                        bingOrganicLink.setPosition(i + 1);
                        top10Links.add(bingOrganicLink);
                    }
                }
            }
        } catch (JSONException e) {
            logger.error("JSONException in getTop10ResultsV5 cause: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception in getTop10ResultsV5 cause: " + e);
        }
    }

    public String getUrlFromSearchQueryURL(String searchQueryURL) {
        String mainUrl = "";
        try {
            List<NameValuePair> params = URLEncodedUtils.parse(new URI(searchQueryURL), "UTF-8");
            List<NameValuePair> fileterdURL = null;
            if (!params.isEmpty()) {
                fileterdURL = params.stream().filter(e -> e.getName().equalsIgnoreCase("r")).collect(Collectors.toList());
            }
            if (fileterdURL != null && !fileterdURL.isEmpty()) {
                mainUrl = fileterdURL.get(0).getValue();
            }
        } catch (URISyntaxException ex) {
            logger.error("URISyntaxException in getUrlFromSearchQueryURL cause: " + ex.getMessage());
        } catch (Exception e) {
            logger.error("Exception in getUrlFromSearchQueryURL cause: " + e);
        }
        if (mainUrl.isEmpty()) {
            mainUrl = searchQueryURL;
        }
        return mainUrl;
    }

    private void notifyBingSearchAPIError(JSONObject responseJSON) {
        try {
            /*Sample Error fromat from API:
            {"message":"Access denied due to invalid subscription key. Make sure to provide a valid key for an active subscription.","statusCode":401} */

            if (responseJSON != null && (responseJSON.has("statusCode") && responseJSON.getInt("statusCode") > 200)) {
                logger.error("Error from Bing Custom API, Response will be updated through mail: " + responseJSON.toString());
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.BING_SEARCH);
                errorObj.setToolId(30);
                errorObj.setErrorMessage(responseJSON.toString());
                APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.TOP_RANKED_WEBSITES_BY_KEYWORD,Boolean.TRUE);
            }
        } catch (Exception e) {
            logger.error("Exception in notifyGoogleSearchAPIErrorMail cause:", e);
        }
    }
}
