/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lxr.marketplace.adspy.AdSpyService;
import lxr.marketplace.util.Common;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import lxr.marketplace.util.PreSalesToolUsageDAOService;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author sagar
 */
@Service
public class SimilarWebAPIServiceImpl implements SimilarWebAPIService {

    private static final Logger LOGGER = Logger.getLogger(SimilarWebAPIServiceImpl.class);

    @Value("${SimilarWeb.API.URL.Prefix}")
    private String API_URL_Prefix;

    @Value("${SimilarWeb.API.URL.Postfix}")
    private String API_URL_Postfix;

    @Value("${SimilarWeb.API.KEY}")
    private String API_Key;

    @Value("${SimilarWeb.API.URL.PaidSearchVisits}")
    private String API_URL_PaidSearchVisits;

    @Value("${SimilarWeb.API.URL.DevicePerformance}")
    private String API_URL_DevicePerformance;

    @Value("${SimilarWeb.API.URL.ChannelOverview}")
    private String API_URL_ChannelOverview;

    @Value("${SimilarWeb.API.URL.MonthlyVisits}")
    private String API_URL_MonthlyVisits;

    @Value("${SimilarWeb.API.Call}")
    /*Trail or Test*/
    private boolean apiCall;

    private static final List<String> GOOGLE_ENGINE_TYPES = Arrays.asList("Google Search", "Syndicated Search", "News Search", "Image Search", "Video Search", "Maps Search", "Google Images", "Google Shopping", "Google News", "Google Videos", "Shopping Search");

    @Autowired
    private ProxyServerConnection proxyServerConnection;

    /*This method returns below metrics 
    1.Channel Performance.
    2.Search  Traffic by Engine
     */
    @Override
    public Map<String, Map<String, Double>> getChannlePerformanceAndTrafficShare(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        String endPointURL = API_URL_Prefix + queryDomain + API_URL_ChannelOverview + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
        /*Channel Performance metrics*/
        double directShare = 0.0;
        double emailShare = 0.0;
        double referralShare = 0.0;
        double socialShare = 0.0;
        double organicShare = 0.0;
        double paidShare = 0.0;
        double displayShare = 0.0;
        double otherShare = 0.0;
        /*Search  Traffic by Engine*/
        double totalTrafficShare = 0.0;
        double bingTraffic = 0.0;
        double googleTraffic = 0.0;
        double yahooTraffic = 0.0;
        Map<String, Double> channelOverViewMap = new LinkedHashMap<>();;
        Map<String, Double> searchEngineeTrafficMap = new LinkedHashMap<>();
        Map<String, Map<String, Double>> channelAndTrafficMap = new LinkedHashMap<>();
        String channelSource = null;
        String searchEngine = null;
        if (apiCall) {
            try {
                JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "overview", queryDomain);
                if (jsonObject != null && jsonObject.has("overview")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("overview");
                    for (int count = 0; count < jsonArray.length(); count++) {
                        jsonObject = jsonArray.getJSONObject(count);
                        if (jsonObject.has("source_type")) {
                            channelSource = jsonObject.getString("source_type").toLowerCase();
                            /*Channel Performance metrics*/
                            switch (channelSource) {
                                case "direct":
                                    directShare = directShare + jsonObject.getDouble("share");
                                    break;
                                case "mail":
                                    emailShare = emailShare + jsonObject.getDouble("share");
                                    break;
                                case "email":
                                    emailShare = emailShare + jsonObject.getDouble("share");
                                    break;
                                case "referral":
                                    referralShare = referralShare + jsonObject.getDouble("share");
                                    break;
                                case "social":
                                    socialShare = socialShare + jsonObject.getDouble("share");
                                    break;
                                case "search / organic":
                                    organicShare = organicShare + jsonObject.getDouble("share");
                                    totalTrafficShare = totalTrafficShare + jsonObject.getDouble("share");
                                    break;
                                case "search / paid":
                                    paidShare = paidShare + jsonObject.getDouble("share");
                                    totalTrafficShare = totalTrafficShare + jsonObject.getDouble("share");
                                    break;
                                case "display ad":
                                    displayShare = displayShare + jsonObject.getDouble("share");
                                    break;
                                case "other":
                                    otherShare = otherShare + jsonObject.getDouble("share");
                                    break;
                                default:
                                    break;

                            }
                            if (jsonObject.has("domain")) {
                                searchEngine = jsonObject.getString("domain");
                                if (searchEngine.equalsIgnoreCase("Bing Search")) {
                                    bingTraffic = bingTraffic + jsonObject.getDouble("share");
                                } else if (GOOGLE_ENGINE_TYPES.contains(searchEngine)) {
                                    googleTraffic = googleTraffic + jsonObject.getDouble("share");
                                } else if (searchEngine.equalsIgnoreCase("Yahoo Search")) {
                                    yahooTraffic = yahooTraffic + jsonObject.getDouble("share");
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getChannlePerformanceAndTrafficShare cause: ", e);
            }

            if (directShare == 0) {
                directShare = otherShare;
            } else if (emailShare == 0) {
                emailShare = otherShare;
            } else if (displayShare == 0) {
                displayShare = otherShare;
            } else if (referralShare == 0) {
                referralShare = otherShare;
            } else if (socialShare == 0) {
                socialShare = otherShare;
            } else if (organicShare == 0) {
                organicShare = otherShare;
            } else if (paidShare == 0) {
                paidShare = otherShare;
            }
            channelOverViewMap.put("Direct", (directShare * 100));
            channelOverViewMap.put("Email", (emailShare * 100));
            channelOverViewMap.put("Referrals", (referralShare * 100));
            channelOverViewMap.put("Social", (socialShare * 100));
            channelOverViewMap.put("Organic Search", (organicShare * 100));
            channelOverViewMap.put("Paid Search", (paidShare * 100));
            channelOverViewMap.put("Display Ads", (displayShare * 100));

            searchEngineeTrafficMap.put("Google", calculatePercentage(googleTraffic, totalTrafficShare));
            searchEngineeTrafficMap.put("Yahoo", calculatePercentage(yahooTraffic, totalTrafficShare));
            searchEngineeTrafficMap.put("Bing", calculatePercentage(bingTraffic, totalTrafficShare));
        }
        /*Adding default values*/
        if (channelOverViewMap.isEmpty()) {
            channelOverViewMap.put("Direct", 0.0);
            channelOverViewMap.put("Email", 0.0);
            channelOverViewMap.put("Referrals", 0.0);
            channelOverViewMap.put("Social", 0.0);
            channelOverViewMap.put("Organic Search", 0.0);
            channelOverViewMap.put("Paid Search", 0.0);
            channelOverViewMap.put("Display Ads", 0.0);
        }

        if (searchEngineeTrafficMap.isEmpty()) {
            searchEngineeTrafficMap.put("Google", 0.0);
            searchEngineeTrafficMap.put("Yahoo", 0.0);
            searchEngineeTrafficMap.put("Bing", 0.0);
        }

        channelAndTrafficMap.put("ChannelOverView", channelOverViewMap);
        channelAndTrafficMap.put("SearchEngineetraffic", searchEngineeTrafficMap);

        return channelAndTrafficMap;
    }

    @Override
    public Map<String, Double> getPaidSearchVists(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        Map<String, Double> paidVists = new LinkedHashMap<>();
        String channelPerformanceType = null;
        JSONArray metricValuesArray = null;
        JSONObject metricValuesObject = null;
        if (apiCall) {
            try {
                String endPointURL = API_URL_Prefix + queryDomain + API_URL_PaidSearchVisits + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
                JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "visits", queryDomain);
                if (jsonObject != null && jsonObject.has("visits")) {
                    jsonObject = jsonObject.getJSONObject("visits");
                    JSONArray jsonArray = jsonObject.getJSONArray(queryDomain);
                    for (int count = 0; count < jsonArray.length(); count++) {
                        jsonObject = jsonArray.getJSONObject(count);
                        if (jsonObject.has("source_type")) {
                            channelPerformanceType = jsonObject.getString("source_type");
                        }
                        if ((channelPerformanceType != null && channelPerformanceType.equalsIgnoreCase("Search")) && jsonObject.has("visits")) {
                            metricValuesArray = jsonObject.getJSONArray("visits");
                            for (int index = 0; index < metricValuesArray.length(); index++) {
                                metricValuesObject = metricValuesArray.getJSONObject(index);
                                if (metricValuesObject.has("date") && metricValuesObject.has("paid")) {
                                    paidVists.put(metricValuesObject.getString("date"), metricValuesObject.getDouble("paid"));
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("Exception in getPaidSearchVists cause: ", e);
            }
        }
        return paidVists;
    }

    @Override
    public Map<String, Double> getDevicePerformance(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        String endPointURL = API_URL_Prefix + queryDomain + API_URL_DevicePerformance + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
        Map<String, Double> devicePerformance = new LinkedHashMap<>();;
        JSONObject innerJSONObject = null;
        JSONObject jsonObject = null;
        if (apiCall) {
            jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "meta", queryDomain);
            if (jsonObject != null && jsonObject.has("meta")) {
                try {
                    innerJSONObject = jsonObject.getJSONObject("meta");
                    if (innerJSONObject != null && innerJSONObject.has("request")) {
                        if (jsonObject.has("desktop_visit_share")) {
                            devicePerformance.put("desktop", (jsonObject.getDouble("desktop_visit_share") * 100));
                        }
                        if (jsonObject.has("mobile_web_visit_share")) {
                            devicePerformance.put("mobile", (jsonObject.getDouble("mobile_web_visit_share") * 100));
                        }
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getDevicePerformance cause: ", e);
                }
            }
        }
        return devicePerformance;
    }

    @Override
    public Map<String, String> getDataAvailableDateRange() {
        JSONObject jsonObject = getJSONObjectFromProxyServerResposne("https://api.similarweb.com/capabilities?api_key=" + API_Key, 0, null, "web_desktop_data", "NA");
        Map<String, String> dateRange = null;
        try {
            if (jsonObject != null && jsonObject.has("web_desktop_data")) {
                if (jsonObject.has("remaining_hits")) {
                    LOGGER.info("After this call remaining_hits:: " + jsonObject.getString("remaining_hits"));
                }
                JSONObject jsonSubObject = jsonObject.getJSONObject("web_desktop_data");
                if (jsonSubObject != null && jsonSubObject.has("snapshot_interval")) {
                    JSONObject jsonInnerObject = jsonSubObject.getJSONObject("snapshot_interval");
                    if (jsonInnerObject != null) {
                        dateRange = new LinkedHashMap<>();
                        if (jsonInnerObject.has("start_date")) {
                            dateRange.put(AdSpyService.START_DATE, jsonInnerObject.getString("start_date"));
                        }
                        if (jsonInnerObject.has("end_date")) {
                            dateRange.put(AdSpyService.END_DATE, jsonInnerObject.getString("end_date"));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getDataAvailableDateRange cause: ", e);
        }
        return dateRange;
    }

    @Override
    public Map<String, String> getAPIFormatedDate(Map<String, String> avaialbleDateRanges) {
        Map<String, String> dateRange = null;
        if (avaialbleDateRanges != null && avaialbleDateRanges.containsKey(AdSpyService.END_DATE)) {
            dateRange = new LinkedHashMap<>();
            Calendar endDateCal = Calendar.getInstance();
            endDateCal.setTime(getDateObject(avaialbleDateRanges.get(AdSpyService.END_DATE)));
            dateRange.put(AdSpyService.END_DATE, Common.convertDateInToAPIRequestFormat(endDateCal.getTime()));
            Calendar startDate = endDateCal;
            startDate.add(Calendar.YEAR, -1);
            startDate.add(Calendar.MONTH, 1);
            dateRange.put(AdSpyService.START_DATE, Common.convertDateInToAPIRequestFormat(endDateCal.getTime()));
            LOGGER.debug("StartDate: " + dateRange.get(AdSpyService.START_DATE) + ", EndDate:" + dateRange.get(AdSpyService.END_DATE));
        }
        return dateRange;
    }

    public static Date getDateObject(String date) {
        Date endDate = null;
        if (date != null) {
            try {
                SimpleDateFormat apiResponseFormat = new SimpleDateFormat("yyyy-MM-dd");
                endDate = apiResponseFormat.parse(date);
            } catch (ParseException ex) {
                LOGGER.error("ParseException in getDataAvailableDateRange cause: ", ex);
            }
        }
        return endDate;
    }

    @Override
    public int getAPIBalanceLimitCount() {
        JSONObject jsonObject = Common.getJSONFromAPIResponse("https://api.similarweb.com/capabilities?api_key=" + API_Key, Boolean.TRUE, "error");
        int balanceCount = 0;
        try {
            if (jsonObject != null && jsonObject.has("remaining_hits")) {
                balanceCount = jsonObject.getInt("remaining_hits");
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getAPIBalanceLimitCount cause: ", e);
        }
        return balanceCount;
    }

    private static double calculatePercentage(double number, double totalNumber) {
        return ((number * 100) / totalNumber);
    }

    /*AdSpy V2 tool.*/
    @Override
    public Map<String, Double> getMonthlyVisits(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        String endPointURL = API_URL_Prefix + queryDomain + API_URL_MonthlyVisits + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
        double totalVists = 0;
        double monthlyVists = 0;
        Map<String, Double> monthlyVisitsMap = new HashMap<>();
        if (apiCall) {
            JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "visits", queryDomain);
            try {
                if (jsonObject != null && jsonObject.has("visits")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("visits");
                    for (int count = 0; count < jsonArray.length(); count++) {
                        jsonObject = jsonArray.getJSONObject(count);
                        totalVists = totalVists + jsonObject.getDouble("visits");
                    }
                    totalVists = Math.round(totalVists);
                    monthlyVists = Math.round(totalVists / jsonArray.length());
                    monthlyVisitsMap.put("totalVists", totalVists);
                    monthlyVisitsMap.put("monthlyVists", monthlyVists);
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getMonthlyVisits cause: ", e);
            }
        }
        if (monthlyVisitsMap.isEmpty()) {
            LOGGER.info("monthlyVisitsMap is empty for domain:: " + queryDomain);
            monthlyVisitsMap.put("totalVists", 0.0);
            monthlyVisitsMap.put("monthlyVists", 0.0);
        }
        return monthlyVisitsMap;
    }

    /*AdSpy V2 tool.*/
    @Override
    public Map<String, Double> getChannelsOverViewMetrics(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        String endPointURL = API_URL_Prefix + queryDomain + API_URL_ChannelOverview + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
        Map<String, Double> channelsOverviewMap = new HashMap<>();
        if (apiCall) {
            JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "overview", queryDomain);
            try {
                if (jsonObject != null && jsonObject.has("overview")) {
                    List<Double> paidTraffic = new ArrayList<>();
                    List<Double> organicTraffic = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray("overview");
                    for (int count = 0; count < jsonArray.length(); count++) {
                        jsonObject = jsonArray.getJSONObject(count);
                        /*Filtering Paid Traffic source_type: Search / Paids*/
                        if (jsonObject.getString("source_type").equalsIgnoreCase("Search / Paid")) {
                            paidTraffic.add(jsonObject.getDouble("share"));
                        }
                        /*Filtering Organic Traffic source_type: Search / Paids*/
                        if (jsonObject.getString("source_type").equalsIgnoreCase("Search / Organic")) {
                            organicTraffic.add(jsonObject.getDouble("share"));
                        }
                    }
                    channelsOverviewMap.put("organicTraffic", organicTraffic.isEmpty() ? 0.0 : (organicTraffic.stream().mapToDouble(Double::doubleValue).sum()) * 100);
                    channelsOverviewMap.put("paidTraffic", paidTraffic.isEmpty() ? 0.0 : (paidTraffic.stream().mapToDouble(Double::doubleValue).sum()) * 100);
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getChannelsOverViewMetrics cause: ", e);
            }
        }
        if (channelsOverviewMap.isEmpty()) {
            LOGGER.info("channelsOverviewMap is empty for domain:: " + queryDomain);
            channelsOverviewMap.put("organicTraffic", 0.0);
            channelsOverviewMap.put("paidTraffic", 0.0);
        }
        return channelsOverviewMap;
    }

    /*AdSpy V2 tool.*/
    @Override
    public Map<String, Long> getChannelsAnalysisMetrics(String queryDomain, String startDate, String endDate, int proxyHostNo) {
        String endPointURL = API_URL_Prefix + queryDomain + API_URL_PaidSearchVisits + "?api_key=" + API_Key + "&start_date=" + startDate + "&end_date=" + endDate + API_URL_Postfix;
        LOGGER.debug("getChannelsAnalysisMetrics Request time in: " + Calendar.getInstance().getTimeInMillis());
        Map<String, Long> channelsAnalysisMap = new HashMap<>();
        if (apiCall) {
            JSONObject jsonObject = getJSONObjectFromProxyServerResposne(endPointURL, proxyHostNo, null, "visits", queryDomain);
            try {
                if (jsonObject != null && jsonObject.has("visits")) {
                    List<Double> paidVisits = new ArrayList<>();
                    List<Double> organicVisits = new ArrayList<>();
                    jsonObject = jsonObject.getJSONObject("visits");
                    JSONArray jsonArray = jsonObject.getJSONArray(queryDomain);
                    JSONArray subJSONArray = null;
                    for (int count = 0; count < jsonArray.length(); count++) {
                        jsonObject = jsonArray.getJSONObject(count);
                        if (jsonObject.getString("source_type").equalsIgnoreCase("Search")) {
                            /*Saving visits array*/
                            subJSONArray = jsonObject.getJSONArray("visits");
                            break;
                        }
                    }
                    if (subJSONArray != null) {
                        for (int count = 0; count < subJSONArray.length(); count++) {
                            jsonObject = subJSONArray.getJSONObject(count);
                            paidVisits.add(jsonObject.getDouble("paid"));
                            organicVisits.add(jsonObject.getDouble("organic"));
                        }
                    }
                    channelsAnalysisMap.put("organicVisits", organicVisits.isEmpty() ? 0 : Math.round(organicVisits.stream().mapToDouble(Double::doubleValue).sum()));
                    channelsAnalysisMap.put("paidVisits", paidVisits.isEmpty() ? 0 : Math.round(paidVisits.stream().mapToDouble(Double::doubleValue).sum()));
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getChannelsAnalysisMetrics cause: ", e);
            }
        }
        if (channelsAnalysisMap.isEmpty()) {
            LOGGER.info("channelsAnalysisMap is empty for domain:: " + queryDomain);
            channelsAnalysisMap.put("organicVisits", 0l);
            channelsAnalysisMap.put("paidVisits", 0l);
        }
        return channelsAnalysisMap;
    }

    /*getJSONObjectFromProxyServerResposne*/
    public JSONObject getJSONObjectFromProxyServerResposne(String endPointURL, int proxyHostNo, String connectType, String parentKey, String queryDomain) {
        JSONObject responseJSON = null;
        HttpMethod httpMethod = null;
        InputStream inputStream = null;
        boolean isDataPulled = Boolean.FALSE;
        int count = 0;
        while (!isDataPulled) {
            /*Try with okHTTP*/
            responseJSON = Common.getJSONFromAPIResponseV2(endPointURL);
            /*Checking for required data presences*/
            isDataPulled = validateAPIResponse(responseJSON, parentKey, queryDomain, endPointURL);
            /*If failed try with proxy*/
            if (!isDataPulled) {
                try {
                    httpMethod = proxyServerConnection.getResponseFromAPIUsingProxyServer(endPointURL, proxyHostNo, "get");
                    if (httpMethod != null && httpMethod.getStatusCode() == HttpStatus.SC_OK) {
                        inputStream = httpMethod.getResponseBodyAsStream();
                        responseJSON = new JSONObject(IOUtils.toString(inputStream));
                    }
                } catch (Exception e) {
                    LOGGER.debug("Exception in getting resposne from proxy server for endpoint URL::" + endPointURL + ", cause:: " + e.getMessage());
                }
                /*Checking for required data presences*/
                isDataPulled = validateAPIResponse(responseJSON, parentKey, queryDomain, endPointURL);
            }
            /*If proxy failed make thread sleep and try after 'n' min*/
            if (!isDataPulled) {
                LOGGER.debug("Pulling data from proxy is failed for query domain :: " + queryDomain + ", at count:: " + count);
                try {
                    LOGGER.debug("*******************Pulling data from similar web using OkHttpClient thread entering into a sleep stage for proxy no:: " + proxyHostNo + ", at count:: " + count);
                    Thread.sleep(1000 * 10);
                    LOGGER.debug("Pulling data from similar web using OkHttpClient thread exited from sleep stage for proxy no:: " + proxyHostNo + ", at count:: " + count);
                } catch (InterruptedException e) {
                    LOGGER.debug("*******************InterruptedException in making thread sleep for endpoint URL::" + endPointURL + ", cause:: " + e.getMessage());
                }
            }
            ++count;
            /*Doing explictly*/
            isDataPulled = isDataPulled ? Boolean.TRUE : count >= 10 ? Boolean.TRUE : Boolean.FALSE;
        }
        return responseJSON;
    }

    private boolean validateAPIResponse(JSONObject responseJSON, String parentKey, String queryDomain, String endPointURL) {
        boolean isValidResponse = Boolean.FALSE;
        if ((responseJSON != null && responseJSON.has(parentKey))) {
            isValidResponse = Boolean.TRUE;
        } else if (responseJSON != null) {
            /*To handle data not found*/
            String errorResponse = responseJSON.toString().toLowerCase();
            /*{"meta":{"error_message":"Data not found","error_code":401,"status":"Error"}} */
//                if (errorResponse.contains("error") || errorResponse.contains("data not found") || errorResponse.contains("error_code")) {
            if (errorResponse.contains("data not found") || errorResponse.contains("401")) {
                isValidResponse = Boolean.TRUE;
            }
            LOGGER.info("Parent key is not found and response :: " + errorResponse + ",  queryDomain :: " + queryDomain + ", isValidResponse:: " + isValidResponse);
            APIError errorObj = new APIError();
            errorObj.setApiName(PreSalesToolUsageDAOService.SIMILARWEB);
            errorObj.setToolId(44);
            errorObj.setErrorMessage(responseJSON.toString() + ",  endpoint URL:: " + endPointURL);
            APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, "PreSales tools", Boolean.TRUE);
        }
        return isValidResponse;
    }
}