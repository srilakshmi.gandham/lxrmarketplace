/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.io.IOException;
import java.time.LocalDateTime;
import lxr.marketplace.user.EmailValidationModel;
import lxr.marketplace.util.Common;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class BoltEmailVerificationAPIService {

    private static final Logger LOGGER = Logger.getLogger(BoltEmailVerificationAPIService.class);

    @Value("${Bolt.EmailVerification.endPointURL}")
    private String emailAPIURL;
    @Value("${Bolt.EmailVerification.apiKey}")
    private String emailAPIKey;
    @Value("${Bolt.EmailVerification.apiScoreFilter}")
    private float emailAPIScoreFilter;

    /*API request, success, failure cases
    *https://api.bolt.helloaiko.com/check?key=bolt_2e547cc057e4d38e&email=agesthsiios@gmail.com
    *{"email":"sagar.korada@gmail.com","valid":true,"score":"0.9","reason":""}
    *{"email":"agesthsiios@gmail.com","valid":false,"score":"0.0","reason":"Email provider returned a 550 (Mailbox does not exist)."}
    *"{\n\t\"email\": \"sagar.korada@gmail.com\"\n}"
     */
    public EmailValidationModel validateUserEmail(String emailToBeVerfyied) {
        LOGGER.info("Request to validate email using api for email:: " + emailToBeVerfyied);
        EmailValidationModel emailValidationModel = null;
        boolean validEmail = Boolean.FALSE;
        String requestBody = "{\n\t\"email\": \"" + emailToBeVerfyied + "\"\n}";
        JSONObject jsonObject = null;
        try {
            jsonObject = getEmailScoreFromAPI(emailAPIURL, emailAPIKey, requestBody);
            if (validateAPIResponse(jsonObject, "email", "valid", emailToBeVerfyied, emailAPIURL)) {
                emailValidationModel = new EmailValidationModel();
                /*Mandatory to check all three key & values from api response
                Finalize the result when all three cases is true*/
                if (jsonObject.has("score")) {
                    validEmail = jsonObject.getDouble("score") >= emailAPIScoreFilter;
                    emailValidationModel.setEmailVerificationScore(jsonObject.getDouble("score"));
                }
                if (jsonObject.has("valid")) {
                    validEmail = jsonObject.getBoolean("valid");
                    emailValidationModel.setEmailVerified(jsonObject.getBoolean("valid"));
                }
                if (jsonObject.has("email")) {
                    validEmail = emailToBeVerfyied.trim().equals(jsonObject.getString("email").trim());
                    emailValidationModel.setEmailToBeVerified(jsonObject.getString("email").trim());
                }
                if (validEmail) {
                    emailValidationModel.setCreatedDate(LocalDateTime.now());
                } else {
                    if (jsonObject.has("reason")) {
                        LOGGER.info("Email verification is failed and cause:: " + jsonObject.getString("reason") + ",  and response:: " + jsonObject);
                    }
                }
            }
        } catch (JSONException e) {
            LOGGER.error("JSON | IO Exception in validateUserEmail for responsebody: " + jsonObject + ", cause:: " + e.getMessage());
        }
        LOGGER.info("Validating email using api for email:: " + emailToBeVerfyied + ", is compelted and status:: " + (emailValidationModel != null ? emailValidationModel.isEmailVerified() : " UNKNOWN"));
        return emailValidationModel;
    }

    private JSONObject getEmailScoreFromAPI(String apiURL, String apiKey, String requestBody) {
        Response response = null;
        JSONObject jsonObject = null;
        String tempRespone = null;
        try {
            response = Common.getOkHttpResponseWithAuthorization(apiURL, requestBody, apiKey, MediaType.APPLICATION_JSON_VALUE);
            if (response != null) {
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        tempRespone = body.string();
                    }
                }
            }
            if (tempRespone != null && !tempRespone.trim().isEmpty()) {
                jsonObject = new JSONObject(tempRespone.trim());
            }
        } catch (JSONException | IOException e) {
            if (tempRespone != null) {
                LOGGER.error("JSON | IO Exception in getJSONFromAPIResponse with waitForSuccessRepsonse for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
            } else {
                LOGGER.debug("JSON | IO Exception in getJSONFromAPIResponse with waitForSuccessRepsonse cause:: " + e.getMessage());
            }
        } catch (Exception e) {
            if (tempRespone != null) {
                LOGGER.error("Exception in getJSONFromAPIResponse with waitForSuccessRepsonse for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
            } else {
                LOGGER.debug("Exception in getJSONFromAPIResponse with waitForSuccessRepsonse cause:: " + e.getMessage());
            }
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return jsonObject;
    }

    private boolean validateAPIResponse(JSONObject responseJSON, String parentKey, String parentKey2, String emailToBeVerfyied, String endPointURL) {
        boolean isValidResponse = Boolean.FALSE;
        if ((responseJSON != null && responseJSON.has(parentKey) && responseJSON.has(parentKey2))) {
            isValidResponse = Boolean.TRUE;
        } else if (responseJSON != null) {
            LOGGER.info("Parent key is not found and response :: " + responseJSON.toString().toLowerCase() + ",  for email :: " + emailToBeVerfyied + ", isValidResponse:: " + isValidResponse);
            APIError errorObj = new APIError();
            errorObj.setApiName("Bolt Helloaiko API");
            errorObj.setToolId(0);
            errorObj.setErrorMessage(responseJSON.toString() + ",  endpoint URL:: " + endPointURL);
            APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, "LXRM Email Verification API", Boolean.TRUE);
        }
        return isValidResponse;
    }

}
