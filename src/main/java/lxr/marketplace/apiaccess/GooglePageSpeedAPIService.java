package lxr.marketplace.apiaccess;

import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class GooglePageSpeedAPIService {

    private static final Logger logger = Logger.getLogger(GooglePageSpeedAPIService.class);
    private static final String APIURL = "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?";
    public final static String DESKTOP = "desktop";
    public final static String MOBILE = "mobile";

    public int fetchGooglePageSpeedScore(String homepageURL, String strategy) {
        int pageSpeedScores = 0;
        JSONObject googlePageSpeedResponse = fetchingGooglePagesSpeedObject(homepageURL, strategy);
        if (googlePageSpeedResponse != null) {
            try {
                JSONObject ruleGroup = googlePageSpeedResponse.getJSONObject("ruleGroups");
                JSONObject speed = ruleGroup.getJSONObject("SPEED");
                String score = speed.getString("score");
                pageSpeedScores = Integer.parseInt(score);
            } catch (JSONException e) {
                logger.error("Exception in fetching Google page speed: " + e);
            }
        }
        return pageSpeedScores;
    }

    private JSONObject fetchingGooglePagesSpeedObject(String homepageURL, String strategy) {
        JSONObject jsonObject;
        String queryURL = APIURL + "url=" + homepageURL;
        if (strategy.equals(DESKTOP)) {
            queryURL += "&strategy=" + DESKTOP;
        } else if (strategy.equals(MOBILE)) {
            queryURL += "&strategy=" + MOBILE;
        }
        logger.info("PageSpeed Insights final URL:"+queryURL);
        jsonObject = Common.createJsonObjectFromURL(queryURL);
        return jsonObject;

    }

}
