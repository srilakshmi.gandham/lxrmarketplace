/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lxr.marketplace.preliminarySearch.LXRSEOStats;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author NE16T1213/Sagar
 */
@Service
public class LxrSeoAPIServiceImpl implements LxrSeoAPIService {

    private static final Logger LOGGER = Logger.getLogger(LxrSeoAPIServiceImpl.class);

    @Value("${LXRSEO_BaseURL}")
    private String LXRSEO_BaseURL;

    @Value("${LXRSEO_Request_Type}")
    private String LXRSEO_Request_Type;

    @Autowired
    private MessageSource messageSource;

    @Value("${SimilarWeb.API.Call}")
    /*Trail or Test*/
    private boolean apiCall;

    @Override
    public LXRSEOStats getLxrSeoStats(String queryDomain) {
        LXRSEOStats lxrStats = null;
        if (true) {
            JSONObject urlInfo = getLxrSeoURLInfo(queryDomain);
            if (urlInfo != null) {
                try {
                    if ((urlInfo.has("urlStatus") && urlInfo.getInt("urlStatus") == 200) && (urlInfo.has("errorMessage") && (urlInfo.getString("errorMessage").equals("null")))) {

                        startLxrSeoStatsThread(urlInfo.toString());
                        /*Making thread sleep to wait for seconds before making first request based on domainId. */
                        Thread.sleep(1000 * 30);
                        lxrStats = constructLXRSEOStats(urlInfo.getLong("domainId"));
                    } else {
                        LOGGER.info("Unable to crawl domain: " + queryDomain + ", status code: " + urlInfo.getInt("urlStatus") + ", error: " + urlInfo.getString("errorMessage"));
                    }
                } catch (InterruptedException | JSONException e) {
                    LOGGER.error("InterruptedException in getLxrSeoStats cause: ", e);
                }
            }
        }
        return lxrStats;
    }

    @Override
    public JSONObject getLxrSeoURLInfo(String queryDomain) {
        String endPointURL = LXRSEO_BaseURL + "/sitegrader/check?domainName=" + queryDomain + "&source=" + LXRSEO_Request_Type + "&userId=0";
        JSONObject urlInfo = null;
        urlInfo = Common.getJSONFromAPIResponse(endPointURL);
        return urlInfo;
    }

    @Override
    public void startLxrSeoStatsThread(String urlInfo) {
        String endPointURL = LXRSEO_BaseURL + "/sitegrader/data/fetch/0/" + LXRSEO_Request_Type + "?urlInfo=" + urlInfo;
        Common.getJSONFromAPIResponse(endPointURL);
    }

    @Override
    public LXRSEOStats constructLXRSEOStats(long domainId) {
        LXRSEOStats lxrStats = null;
        Map<String, Byte> metricsScore;
        JSONObject jsonObject = getLxrSeoStats(domainId);
        int errorCount = 4;
        try {
            if (jsonObject != null && (jsonObject.has("siteGraderScore") && (jsonObject.getInt("siteGraderScore") != 0))) {
                lxrStats = new LXRSEOStats();
                metricsScore = new LinkedHashMap<>();
                metricsScore.put("ON PAGE", getIndividualMetricScore(jsonObject, "onPage"));
                metricsScore.put("MOBILE", getIndividualMetricScore(jsonObject, "mobile"));
                metricsScore.put("SPEED", getIndividualMetricScore(jsonObject, "speed"));
                metricsScore.put("LINKS", getIndividualMetricScore(jsonObject, "links"));
                metricsScore.put("TECHNICAL", getIndividualMetricScore(jsonObject, "technical"));
                metricsScore.put("SOCIAL", getIndividualMetricScore(jsonObject, "social"));
                lxrStats.setSeoMetrics(metricsScore);
                lxrStats.setSiteGraderScore((byte) jsonObject.getInt("siteGraderScore"));
                lxrStats.setRedirectedDomain(jsonObject.getString("redirectedURL"));
                List<String> siteErrors = new LinkedList<>();
                JSONObject innerObject = null;
                /*Mobile errors*/
                innerObject = jsonObject.getJSONObject("mobile");
                if (innerObject != null) {
                    if (innerObject.has("amp") && !innerObject.getBoolean("amp")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.amp.bad.desc", null, Locale.US));
                    }
                    if (innerObject.has("mobileFriendly") && !innerObject.getBoolean("mobileFriendly")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.mobile.bad.tagline", null, Locale.US));
                    }

                    /*if (innerObject.has("mobileWebsite") && !innerObject.getBoolean("mobileWebsite")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.website.bad.tagline", null, Locale.US));
                    }*/
                    if (innerObject.has("mobileViewPort") && !innerObject.getBoolean("mobileViewPort")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.viewport.bad.tagline", null, Locale.US));
                    }
                }
                /*Mobile errors*/
                innerObject = jsonObject.getJSONObject("speed");
                if (innerObject != null && siteErrors.size() < errorCount) {
                    if (innerObject.has("desktopPageLoadTime") && (innerObject.getDouble("desktopPageLoadTime") > 8)) {
                        String message = messageSource.getMessage("LXRSEO.desktopspeed.bad.desc", new Object[]{String.valueOf(innerObject.getDouble("desktopPageLoadTime"))}, Locale.US);
                        message = message.replaceAll("itll", "it'll");
                        siteErrors.add(message);
                    }

                    /*                    if (innerObject.has("mobilePageLoadTime") && (innerObject.getDouble("mobilePageLoadTime") > 8)) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.mobilespeed.bad.desc", new Object[]{String.valueOf(innerObject.getDouble("mobilePageLoadTime"))}, Locale.US));
                    }*/
                }
                innerObject = jsonObject.getJSONObject("technical");
                if (innerObject != null && siteErrors.size() < errorCount) {
                    if (innerObject.has("brokenLinksCount") && (innerObject.getInt("brokenLinksCount") >= 1)) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.brokenLinks.bad.desc", new Object[]{String.valueOf(innerObject.getInt("brokenLinksCount"))}, Locale.US));
                    }
                    if (innerObject.has("robotsTxt") && !innerObject.getBoolean("robotsTxt")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.robots.bad.header", null, Locale.US));
                    }
                    if (innerObject.has("validSSLCertificate") && !innerObject.getBoolean("validSSLCertificate")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.security.bad.header", null, Locale.US));
                    }
                    if (innerObject.has("wwwResolved") && !innerObject.getBoolean("wwwResolved")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.WWWresolve.bad.header", null, Locale.US));
                    }
                    if (innerObject.has("customerErrorPage") && !innerObject.getBoolean("customerErrorPage")) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.error.bad.header", null, Locale.US));
                    }
                    if (innerObject.has("language") && innerObject.getString("language") == null) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.language.bad.header", null, Locale.US));
                    }
                    /*if (innerObject.has("structuredDataScore") && innerObject.getInt("structuredDataScore") == 0) {
                        siteErrors.add(messageSource.getMessage("LXRSEO.structured.bad.header", null, Locale.US));
                    }*/
                }
                innerObject = jsonObject.getJSONObject("social");
                if (siteErrors.size() < errorCount && innerObject != null && (innerObject.has("blogPresence") && !innerObject.getBoolean("blogPresence"))) {
                    siteErrors.add(messageSource.getMessage("LXRSEO.blog.bad.desc", null, Locale.US));
                }
                innerObject = jsonObject.getJSONObject("onPage");
                if (siteErrors.size() < errorCount && innerObject != null && (innerObject.has("faviconURL") && innerObject.getString("faviconURL") == null)) {
                    siteErrors.add(messageSource.getMessage("LXRSEO.favicon.bad.desc", null, Locale.US));
                }
                /*Preparing sub list*/
                if (siteErrors.size() >= errorCount + 1) {
                    siteErrors.subList(0, errorCount);
                }
                lxrStats.setSiteErrors(siteErrors);
            } else {
                LOGGER.info("LXRSEO WebStats not found for lxrseo domain id: " + domainId);
            }
        } catch (JSONException ex) {
            LOGGER.error("JSONException in constructLXRSEOStats cause: ", ex);
        }
        return lxrStats;
    }

    @Override
    public JSONObject getLxrSeoStats(long domainId) {
        String endPointURL = LXRSEO_BaseURL + "/sitegrader/data/" + domainId + "/" + LXRSEO_Request_Type;
        int siteGraderScore = 0;
        JSONObject jsonObject = null;
        LOGGER.info("Waiting to fetch LXRSEO stats for domain Id: " + domainId);
        int count = 0;
        while (siteGraderScore == 0) {
            try {
                jsonObject = Common.getJSONFromAPIResponse(endPointURL);
                if (jsonObject != null && jsonObject.has("siteGraderScore")) {
                    siteGraderScore = jsonObject.getInt("siteGraderScore");
                    if (siteGraderScore == 0) {
                        Thread.sleep(1000 * 50);
                    }
                } else if (jsonObject == null) {
                    LOGGER.info("No response from service for domain ID: " + domainId + ", at hit no:: " + count);
                    if (count >= 6) {
                        count = 10;
                    }
                }
            } catch (JSONException | InterruptedException e) {
                LOGGER.error("JSONException | InterruptedException in getLxrSeoStats cause: ", e);
            }
            if (count >= 10) {
                LOGGER.info("Terminating calling thread for LXRSEO Service for domain ID: " + domainId + ", at hit no:: " + count + ", and endPointURL:: " + endPointURL);
                break;
            }
            count++;
        }
        LOGGER.info("Compelted fetching LXRSEO stats for domain Id: " + domainId);
        return jsonObject;
    }

    @Override
    public byte getIndividualMetricScore(JSONObject metricJsonObject, String metricName) {
        byte metricScore = 0;
        JSONObject innerObject = null;
        if (metricJsonObject != null && metricJsonObject.has(metricName)) {
            try {
                innerObject = metricJsonObject.getJSONObject(metricName);
                if (innerObject != null && innerObject.has(metricName.concat("Score"))) {
                    metricScore = (byte) innerObject.getInt(metricName.concat("Score"));
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getIndividualMetricScore cause: ", e);
            }
        }
        return metricScore;
    }
}