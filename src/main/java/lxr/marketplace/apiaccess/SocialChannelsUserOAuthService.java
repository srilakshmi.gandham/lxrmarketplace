/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.Arrays;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.UserSocialProfile;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class SocialChannelsUserOAuthService {

    private static final Logger LOGGER = Logger.getLogger(SocialChannelsUserOAuthService.class);

    @Value("${lxrm.facebook.login.api.oauthSuccesRedirectURL}")
    private String facebookRedirectURL;
    @Value("${lxrm.google.login.api.oauthSuccesRedirectURL}")
    private String googleRedirectURL;

    @Value("${lxrm.facebook.login.api.endpoint}")
    private String facebookLoginAPIEndpoint;
    @Value("${lxrm.facebook.login.graphapi.endpoint}")
    private String facebookGraphApiEndpoint;

    @Value("${lxrm.facebook.login.api.clientId}")
    private String facebookClientId;
    @Value("${lxrm.facebook.login.api.clientSecret}")
    private String facebookClientSecret;

    @Value("${lxrm.google.login.api.clientId}")
    private String googleClientId;
    @Value("${lxrm.google.login.api.clientSecret}")
    private String googleClientSecret;
    @Value("${lxrm.google.login.api.auhtAccessScope}")
    private String googleOauhtAccessScope;

    @Autowired
    private String domainName;

    public String getFacebookLoginOAuthURL() {
        return facebookLoginAPIEndpoint.concat(facebookClientId).concat("&redirect_uri=").concat(domainName.concat(facebookRedirectURL));
    }

    public Signup getUserSocialProfileFromFaceBook(String oAuthSuccessResponseCode) {
        Signup socialProfileSignUp = null;
        LOGGER.info("In getUserSocialProfileFromFaceBook ::");
        String profileAccessToken = getFBAccessTokenForUserProfileData(getFacebookAccessTokenURL(oAuthSuccessResponseCode));
        if (profileAccessToken != null) {
            LOGGER.info("profileAccessToken success ::");
            JSONObject fbUserProfileData = getFBUserProfileDataJSON(profileAccessToken);
            if (fbUserProfileData != null) {
                LOGGER.info("fbUserProfileData success ::"+fbUserProfileData.toString());
                try {
                    /*Saving user social profile data*/
                    UserSocialProfile userSocialProfile = null;
                    if (fbUserProfileData.has("id")) {
                        userSocialProfile = new UserSocialProfile();
                        userSocialProfile.setProfileId(fbUserProfileData.getString("id"));
                        LOGGER.info("userSocialProfile success ::"+userSocialProfile.getProfileId());
                    }
                    if (userSocialProfile != null && userSocialProfile.getProfileId() != null) {
                        userSocialProfile.setOauthAccessToken(oAuthSuccessResponseCode);
                        userSocialProfile.setSocialChannel(UserSocialProfile.FACEBOOK);
                        userSocialProfile.setProfielCapturedTime(LocalDateTime.now());
                        socialProfileSignUp = new Signup();
                        if (fbUserProfileData.has("email")) {
                            socialProfileSignUp.setEmail(fbUserProfileData.getString("email"));
                        }
                        if (fbUserProfileData.has("name")) {
                            socialProfileSignUp.setName(fbUserProfileData.getString("name"));
                        }
                        socialProfileSignUp.setSocialSignUp(Boolean.TRUE);
                        socialProfileSignUp.setUserSocialProfile(userSocialProfile);
                         LOGGER.info("socialProfileSignUp success ::");
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getUserSocialProfileFromFaceBook cause:: ", e);
                }
            }
        }
        return socialProfileSignUp;
    }

    private String getFacebookAccessTokenURL(String oAuthSuccessResponseCode) {
        return facebookGraphApiEndpoint.concat("/oauth/access_token?client_id=").concat(facebookClientId).concat("&redirect_uri=").concat(domainName.concat(facebookRedirectURL)).concat("&client_secret=").concat(facebookClientSecret).concat("&code=").concat(oAuthSuccessResponseCode);
    }

    private String getFBAccessTokenForUserProfileData(String accessTokenEndpointURL) {
        String accessToken = null;
        URL fbGraphURL = null;
        URLConnection fbURLConnection = null;
        StringBuffer stringBuffer = null;
        String response = null;
        try {
            fbGraphURL = new URL(accessTokenEndpointURL);
            LOGGER.info("fbGraphURL success ::");
        } catch (MalformedURLException e) {
            LOGGER.error("MalformedURLException in getFBAccessTokenForUserProfileData cause:: ", e);
        }
        if (fbGraphURL != null) {
            try {
                fbURLConnection = fbGraphURL.openConnection();
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fbURLConnection.getInputStream()))) {
                    stringBuffer = new StringBuffer();
                    while ((response = bufferedReader.readLine()) != null) {
                        stringBuffer.append(response).append("\n");
                    }
                }
                JSONObject jsonObject = new JSONObject(stringBuffer.toString());
                if (jsonObject.has("access_token")) {
                     LOGGER.info("access_token success ::");
                    accessToken = jsonObject.getString("access_token");
                }
            } catch (IOException | JSONException ex) {
                LOGGER.error("IOException | JSONException in getFBAccessTokenForUserProfileData cause:: ", ex);
            }
        }
        return accessToken;
    }

    private JSONObject getFBUserProfileDataJSON(String profileAccessToken) {
        JSONObject jsonObject = Common.getJSONFromAPIResponse(facebookGraphApiEndpoint.concat("/me?fields=id,name,email&access_token=").concat(profileAccessToken));
        return jsonObject;
    }

    public Signup getUserSocialProfileFromGoogle(String oAuthSuccessResponseCode) {
        Signup socialProfileSignUp = null;
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        GoogleTokenResponse googleTokenResponse = null;
        GoogleIdToken googleIdToken = null;
        GoogleIdToken.Payload googlePayload = null;
        try {
            googleTokenResponse = new GoogleAuthorizationCodeTokenRequest(netHttpTransport, jacksonFactory, googleClientId.trim(), googleClientSecret.trim(), oAuthSuccessResponseCode, (domainName.concat(googleRedirectURL))).execute();
            if (googleTokenResponse != null) {
                googleIdToken = googleTokenResponse.parseIdToken();
            }
            if (googleIdToken != null) {
                googlePayload = googleIdToken.getPayload();
            }
            if (googlePayload != null) {
                if (googlePayload.getEmail() != null) {
                    /*Saving user social profile data*/
                    UserSocialProfile userSocialProfile = null;
                    if (googlePayload.getSubject() != null) {
                        userSocialProfile = new UserSocialProfile();
                        userSocialProfile.setProfileId(googlePayload.getSubject());
                    }
                    if (userSocialProfile != null && userSocialProfile.getProfileId() != null) {
                        if (googleTokenResponse != null && googleTokenResponse.getAccessToken() != null) {
                            userSocialProfile.setOauthAccessToken(googleTokenResponse.getAccessToken());
                        }
                        userSocialProfile.setSocialChannel(UserSocialProfile.GMAIL);
                        userSocialProfile.setProfielCapturedTime(LocalDateTime.now());
                        socialProfileSignUp = new Signup();
                        if (googlePayload.getEmail() != null) {
                            socialProfileSignUp.setEmail(googlePayload.getEmail());
                        }
                        if ((String) googlePayload.get("name") != null) {
                            socialProfileSignUp.setName((String) googlePayload.get("name"));
                        } else {
                            socialProfileSignUp.setName(socialProfileSignUp.getEmail().split("@")[0]);
                        }
                        socialProfileSignUp.setSocialSignUp(Boolean.TRUE);
                        socialProfileSignUp.setUserSocialProfile(userSocialProfile);
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error("IOException in getUserSocialProfileFromGoogle cause:: ", e);
        }
        return socialProfileSignUp;
    }

    public String getGoogleLoginOAuthUrl() {
        String googleOAuthURL = null;
        GoogleAuthorizationCodeRequestUrl googleAuthorizationCodeRequestUrl = new GoogleAuthorizationCodeRequestUrl(googleClientId.trim(), (domainName.concat(googleRedirectURL)), Arrays.asList(googleOauhtAccessScope));
        googleAuthorizationCodeRequestUrl.setAccessType("offline");
        googleAuthorizationCodeRequestUrl.setApprovalPrompt("force");
        googleOAuthURL = googleAuthorizationCodeRequestUrl.build();
        return googleOAuthURL;
    }

}
