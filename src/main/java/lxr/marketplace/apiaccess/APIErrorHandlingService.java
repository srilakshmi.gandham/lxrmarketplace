/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

/**
 *
 * Functionality to be implemented: In lxrmarketplace we are using different
 * API'S endpoints, The problem we are facing is that whenever any API
 * (Endpoints using part of tools ) is down or responding with an error, our
 * team is unaware of API status until we check manually.
 *
 * Solution: Whenever we make a request to an API endpoint, if API responded
 * with an error/ status code more than 400 an email has to be sent to the
 * support team.
 *
 * Algorithm
 *
 * 1. Created a table for API down history with columns: API Name, Issue
 * Identified DateTime, Reason 2. Whenever we identify an exception in code due
 * to API downtime check if there is an entry in last 5 minutes. 3. If No, first
 * add an entry in db then send mail 4. If there is already an entry in the last
 * 5 minutes, ignore it.
 *
 * @author sagar/NE16T1213
 */
@Service
public class APIErrorHandlingService {

    private static final Logger LOGGER = Logger.getLogger(APIErrorHandlingService.class);

    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    static JdbcTemplate jdbcTemplate = (JdbcTemplate) ctx.getBean("jdbcTemplate");

    private static final int ERROR_TIME_DIFFERENCE = 4;

    public static final String TWITTER = "Twitter";
    public static final String FACEBOOK = "Facebook";
    public static final String PINTEREST = "Pinterest";
    public static final String BING_SEARCH = "Bing Search";
    public static final String AHREF = "Ahref";
    /*Imp Info: 
    Common key for below API Name and Tool name
    Project name: LXRMarketplace
    Gmail: lxrmarketplaceseotool@gmail.com
    PageSpeed Insights API - PageSpeed Insights
    Custom Search API- Top Ranked Websites by Keyword
    Google+(Plus) API  - Social Media Analyzer
    YouTube Data API v3 - Social Media Analyzer
     */
    public static final String GOOGLE_PLUS = "Google Plus";
    public static final String YOUTUBE_DATA_V3 = "Youtube Data V3";
    public static final String GOOGLE_PAGESPEED_INSIGHTS = "Google Pagespeed Insights";
    public static final String GOOGLE_CUSTOM_SEARCH = "Google Custom Search";

    /*Twitter API Error codes Ref: https://developer.twitter.com/en/docs/basics/response-codes.html*/
    private static final List<Integer> TWITTER_ERROR_STATUS_CODES = Arrays.asList(32, 64, 68, 89, 99, 131, 135, 215, 401, 410, 416, 503);
    private static final List<Integer> GOOGLE_ERROR_STATUS_CODES = Arrays.asList(400, 401);

    public static void validateAndHandleAPIErrors(APIError apiErrorObj, String ToolName, boolean shouldConsiderDifference) {
        /*Fetching recent error time for specific API*/
        Timestamp recentErrortime = null;
        if (shouldConsiderDifference) {
            recentErrortime = getRecentAPIIssueIdentifiedDate(apiErrorObj.getApiName());
        }
        Timestamp newErrorTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        int differenceInTime = 0;
        long errorId = 0;
        if (recentErrortime != null) {
            /*get time difference in seconds*/
            long milliseconds = newErrorTime.getTime() - recentErrortime.getTime();
            int seconds = (int) milliseconds / 1000;
            /*Calculate hours || minutes || seconds*/
            differenceInTime = (seconds % 3600) / 60;
            /*int hours = seconds / 3600;
            seconds = (seconds % 3600) % 60;*/
        }
        /*Checking the difference*/
        LOGGER.debug("DifferenceInTime: " + differenceInTime + ", Error_Time_Difference: " + ERROR_TIME_DIFFERENCE);
        if (recentErrortime == null || differenceInTime > ERROR_TIME_DIFFERENCE) {
            /*Inserting new record*/
            apiErrorObj.setIssueIdentified(newErrorTime);
            errorId = insertingErrorData(apiErrorObj);
            if ((errorId > 0 && differenceInTime > ERROR_TIME_DIFFERENCE) || (errorId > 0 && recentErrortime == null)) {
                /*Sending Mail to support team*/
                SendMail.sendToolAPIMessageMail(apiErrorObj.getApiName() + " API Error - " + ToolName + " Tool", "API Error Message: ".concat(apiErrorObj.getErrorMessage()));
                LOGGER.info("Mai send to support team for API Name: " + apiErrorObj.getApiName() + ", timeStamp: " + newErrorTime + " for tool: " + ToolName);
            }
        }
    }

    private static long insertingErrorData(APIError apiErrorObj) {
        String query = "insert into lxrm_api_errors (api_name, issue_identified, error_message, tool_id) values (?, ?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update((Connection con) -> {
                PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                statement.setString(1, apiErrorObj.getApiName());
                statement.setTimestamp(2, apiErrorObj.getIssueIdentified());
                if (apiErrorObj.getErrorMessage().length() >= 10024) {
                    statement.setString(3, apiErrorObj.getErrorMessage().substring(0, apiErrorObj.getErrorMessage().length() - 1));
                } else {
                    statement.setString(3, apiErrorObj.getErrorMessage());
                }
                statement.setString(3, apiErrorObj.getErrorMessage());
                statement.setInt(4, apiErrorObj.getToolId());
                return statement;
            }, keyHolder);
        } catch (DataAccessException ex) {
            LOGGER.error("DataAccessException in insertingErrorData cause:", ex);
        } catch (Exception e) {
            LOGGER.error("Exception on insertingErrorData cause: ", e);
        }
        return (Long) keyHolder.getKey();
    }

    private static Timestamp getRecentAPIIssueIdentifiedDate(String apiName) {
        String query = "SELECT issue_identified FROM lxrm_api_errors where api_name = ? and issue_identified > (now() - INTERVAL " + ERROR_TIME_DIFFERENCE + " MINUTE) order by id desc";
        LOGGER.info("Query: " + query + ", for API: " + apiName);
        List<Timestamp> recentIssueIdentified = null;
        try {
            recentIssueIdentified = jdbcTemplate.query(query, new Object[]{apiName}, (ResultSet rs, int rowNum) -> {
                Timestamp errorTimestamp = rs.getTimestamp("issue_identified");
                return errorTimestamp;
            });
        } catch (DataAccessException e) {
            LOGGER.error("DataAccessException on getRecentAPIIssueIdentifiedDate cause: ", e);
        } catch (Exception e) {
            LOGGER.error("Exception on getRecentAPIIssueIdentifiedDate cause: ", e);
        }
        return (recentIssueIdentified != null && recentIssueIdentified.size() >= 1) ? recentIssueIdentified.get(0) : null;
    }

    public static void raiseTwitterAPIError(String response, String toolName, int toolId, String userName) {
        if (response != null && response.contains("errors")) {
            LOGGER.error("Error in Twitter API for tool: " + toolName + ", Response will be updated through mail: " + response + ", ");
            Integer statusCode = 0;
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                if (jsonObject.has("errors")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("errors");
                    if (jsonArray != null && jsonArray.length() >= 1) {
                        JSONObject innerObject = jsonArray.getJSONObject(0);
                        if (innerObject != null && innerObject.has("code")) {
                            statusCode = innerObject.getInt("code");
                        }
                    }
                }
            } catch (JSONException ex) {
                LOGGER.error("InterruptedException in checkGoogleUserExistence cause: " + ex.getMessage());
            }
            if (TWITTER_ERROR_STATUS_CODES.contains(statusCode)) {
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.TWITTER);
                errorObj.setToolId(toolId);
                errorObj.setErrorMessage(response + ", for UserName: " + (userName != null ? userName : null));
                validateAndHandleAPIErrors(errorObj, toolName, Boolean.TRUE);
            }
        }
    }

    public static void raiseGoogleAPIError(JSONObject googlePlusJson, String apiName, String toolName, int toolId, String userName) {
        /*Checking for errors: Sample response for invalid key request:
            {"error": {"errors": [{"domain": "usageLimits","reason": "keyInvalid","message": "Bad Request"}],"code": 400,"message": "Bad Request"}}*/
 /*Checking for errors: Sample response for user not found:
            {"error":{"code":404,"message":"Not Found","errors":[{"reason":"notFound","domain":"global","message":"Not Found"}]}} */
        int statusCode = 0;
        try {
            if (googlePlusJson != null) {
                LOGGER.error("Error from Google API, Response will be updated through mail cause: " + googlePlusJson.toString() + ", for userName: " + userName);
                if (googlePlusJson.has("error")) {
                    JSONObject errorInnerObject = googlePlusJson.getJSONObject("error");
                    if (errorInnerObject != null && errorInnerObject.has("code")) {
                        statusCode = errorInnerObject.getInt("code");
                    }
                }
            }
        } catch (JSONException ex) {
            LOGGER.error("Exception in raiseGoogleAPIError cause: " + ex.getMessage());
        }
        if (GOOGLE_ERROR_STATUS_CODES.contains(statusCode)) {
            APIError errorObj = new APIError();
            errorObj.setApiName(apiName);
            errorObj.setToolId(toolId);
            errorObj.setErrorMessage((googlePlusJson != null ? googlePlusJson.toString() : null) + ", for UserName: " + (userName != null ? userName : null));
            validateAndHandleAPIErrors(errorObj, toolName, Boolean.TRUE);
        }

    }

}