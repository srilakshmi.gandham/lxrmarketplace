/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class ProxyServerConnection {

    private static final Logger LOGGER = Logger.getLogger(ProxyServerConnection.class);

    @Autowired
    private int proxyPort;
    @Autowired
    private String proxyUserName;
    @Autowired
    private String proxyPassword;
    @Autowired
    private String proxyHost1;
    @Autowired
    private String proxyHost2;

    public static final String PROXYSERVER = "ProxyServer";

    public HttpMethod getResponseFromAPIUsingProxyServer(String queryURL, int proxyHostNo, String requestType) {
        String proxyHost = null;
        HttpMethod method = null;
        switch (proxyHostNo) {
            case 0:
                proxyHost = proxyHost1;
                break;
            case 1:
                proxyHost = proxyHost2;
                break;
        }
        HttpClient client = new HttpClient();
        HostConfiguration config = client.getHostConfiguration();
        config.setProxy(proxyHost, proxyPort);
        Credentials credentials = new UsernamePasswordCredentials(proxyUserName, proxyPassword);
        AuthScope authScope = new AuthScope(proxyHost != null ? proxyHost : proxyHost2, proxyPort);
        client.getState().setProxyCredentials(authScope, credentials);
        try {
            switch (requestType) {
                case "post":
                    method = new PostMethod(queryURL);
                    break;
                case "get":
                    method = new GetMethod(queryURL);
                    break;
            }
            client.executeMethod(config, method);
        } catch (Exception ex) {
            method = null;
            LOGGER.error("IOException in connect to proxy server using proxyHost:: " + proxyHost + ", cause:: " + ex.getMessage());
        }
        return method;
    }
}
