package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentHashMap;

import lxr.marketplace.apiaccess.lxrmbeans.TwitterMetrics;
import lxr.marketplace.socialmedia.APIKeys;
import lxr.marketplace.util.Common;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TwitterMetricsAPIService implements Runnable {

    private ConcurrentHashMap<Integer, String> twitterMap;

    private String finalTwitterUser;
    private long socialUserId;
    private TwitterMetrics twitterMetrics = null;
    private static volatile boolean dontCallTwitterAPI = false;
    private static Logger logger = Logger.getLogger(TwitterMetricsAPIService.class);

    public TwitterMetrics getTwitterMetrics() {
        return twitterMetrics;
    }

    public void setTwitterMetrics(TwitterMetrics twitterMetrics) {
        this.twitterMetrics = twitterMetrics;
    }

    public ConcurrentHashMap<Integer, String> getTwitterMap() {
        return twitterMap;
    }

    public void setTwitterMap(ConcurrentHashMap<Integer, String> twitterMap) {
        this.twitterMap = twitterMap;
    }

    public String getFinalTwitterUser() {
        return finalTwitterUser;
    }

    public void setFinalTwitterUser(String finalTwitterUser) {
        this.finalTwitterUser = finalTwitterUser;
    }

    public long getSocialUserId() {
        return socialUserId;
    }

    public void setSocialUserId(long socialUserId) {
        this.socialUserId = socialUserId;
    }

    public TwitterMetricsAPIService() {
    }

    public TwitterMetricsAPIService(String twitterUSer, ConcurrentHashMap<Integer, String> twitterMap) {
        this.finalTwitterUser = twitterUSer;
        this.twitterMap = twitterMap;
    }

    @Override
    public void run() {
        try {
            if (dontCallTwitterAPI) {
                logger.info("Twitter API thread is enetering into sleeping mode");
                Thread.sleep(900000);
                logger.info("Twitter API thread is exited from sleeping mode");
                dontCallTwitterAPI = false;
            }
            this.setTwitterMetrics(getTwitterMetricsJson(finalTwitterUser));
        } catch (InterruptedException e) {
            this.setTwitterMetrics(new TwitterMetrics());
            logger.error("Exception in Twitter api service: ", e);
        }
        twitterMap.put(1, "Twitter");
    }/* End of Run */


    public boolean checkForTwitterUser(String userName) {
        return checkTwitterUserExistence(userName);
    }

    public static boolean checkTwitterUserExistence(String userName) {
        BufferedReader streamReader = null;
        StringBuilder responseStrBuilder = null;
        HttpResponse response = null;
        JSONObject twitterResponseData = null;
        try {
            OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
            consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
            logger.debug("The request to twitter api in checkTwitterUserExistence" + getTwitterUserSerachUrl(getTwitterAPIEncodedQuery(userName)));
            HttpGet httpget = new HttpGet(getTwitterUserSerachUrl(getTwitterAPIEncodedQuery(userName)));
            consumer.sign(httpget);
            try {
                HttpClient client = HttpClientBuilder.create().build();
                response = client.execute(httpget);
                streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                responseStrBuilder = new StringBuilder();
            } catch (IOException | UnsupportedOperationException e) {
                logger.error("Exception in checkTwitterUserExistence Of DefaultHttpClient" + e.getMessage());
            }
            String inputStr;
            if (streamReader != null && responseStrBuilder != null) {
                while ((inputStr = streamReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
            }
            if (responseStrBuilder != null) {
                logger.debug("twitter response: " + responseStrBuilder.toString());
                /*The resposne may conatins JSONArray|JSONObject */
                try {
                    JSONArray responseJson = new JSONArray(responseStrBuilder.toString());
                    if (responseJson.length() >= 1) {
                        twitterResponseData = responseJson.getJSONObject(0);
                    }
                } catch (Exception e) {

                }
                /*try {
                    APIErrorHandlingService.raiseTwitterAPIError(responseStrBuilder.toString(), Common.SOCIAL_MEDIA_ANALYZER, 29, userName);
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    logger.error("Exception in checking for error object: " + e.getMessage());
                }*/
            }
        } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException e) {
            logger.error("Exception in checkTwitterUserExistence: " + e.getMessage());
        }
        return twitterResponseData != null;
    }

    public TwitterMetrics getTwitterMetricsJson(String userName) {
        twitterMetrics = new TwitterMetrics();
        if (!(finalTwitterUser.equals("NDA"))) {
            String twitterUserID = null;
            try {
                /*Checking the API limit
            int remainingLimit = checkTwitterAPIRequestLimit();
            logger.info("Twitter API remainingLimit: " + remainingLimit);*/
                boolean serachBasedonID = StringUtils.isNumeric(userName);
                JSONObject[] twitterJSONObject;
                if (serachBasedonID) {
                    twitterJSONObject = getTwitterDataID(userName);
                } else {
                    twitterJSONObject = gettwitterJsonArray(userName);
                }

                if (twitterJSONObject[3] != null) {
                    boolean emptyResponse = twitterJSONObject[3].getBoolean("emptyResponse");
                    if (emptyResponse) {
                        return twitterMetrics;
                    }
                }
                if (twitterJSONObject[0] != null) {
//                beanUserName = twitterJSONObject[0].getString("name");
                    if (twitterJSONObject[0].has("id_str")) {
                        twitterUserID = twitterJSONObject[0].getString("id_str");
                        twitterMetrics.setUserName(twitterJSONObject[0].getString("id_str"));
                    }
                    if (twitterJSONObject[0].has("followers_count")) {
                        twitterMetrics.setFollowers(twitterJSONObject[0].getLong("followers_count"));
                    }
                    if (twitterJSONObject[0].has("friends_count")) {
                        twitterMetrics.setFollowing(twitterJSONObject[0].getLong("friends_count"));
                    }
                    if (twitterJSONObject[0].has("favourites_count")) {
                        twitterMetrics.setFavourites(twitterJSONObject[0].getLong("favourites_count"));
                    }
                    if (twitterJSONObject[0].has("statuses_count")) {
                        twitterMetrics.setTweets(twitterJSONObject[0].getLong("statuses_count"));
                    }
                    twitterMetrics.setTwitterAccountStatus(Boolean.TRUE);

                }
                if (twitterUserID != null) {
                    twitterMetrics.setTwitterHashTag(gettwitterHashTagJsonArray(twitterUserID));
                } else {
                    twitterMetrics.setTwitterHashTag("NoHashTag");
                }
                String postid;
                String imageHTTPURL = null;
                String imageHTTPSURL = null;
                String format = new SimpleDateFormat("yyyy_MMM_dd_HHmmss_SSS").format(new java.util.Date());
                if (twitterJSONObject[1] != null) {
                    if (twitterJSONObject[1].has("text")) {
                        twitterMetrics.setRecentTweetText(twitterJSONObject[1].getString("text").trim());
                    }
                    if (twitterJSONObject[1].has("retweet_count")) {
                        twitterMetrics.setRecentTweetRetweets(twitterJSONObject[1].getLong("retweet_count"));
                    }
                    if (twitterJSONObject[1].has("favorite_count")) {
                        twitterMetrics.setRecentTweetFavourites(twitterJSONObject[1].getLong("favorite_count"));
                    }
//                    twitterMetrics.setEngagement(twitterJSONObject[1].getLong("retweet_count") + twitterJSONObject[1].getLong("favorite_count"));
                    twitterMetrics.setEngagement(twitterMetrics.getRecentTweetRetweets() + twitterMetrics.getRecentTweetFavourites());
                    twitterMetrics.setAudienceEngaged(Common.getAudienceEngaged(twitterMetrics.getEngagement(), twitterMetrics.getFollowers()));
                    String temptweetDate = null;
                    if (twitterJSONObject[1].has("created_at")) {
                        temptweetDate = twitterJSONObject[1].getString("created_at");
                    }
                    if (temptweetDate != null) {
                        twitterMetrics.setTweetDate(Common.getFormatedDateForTwitter(temptweetDate));
                    } else {
                        twitterMetrics.setTweetDate(null);
                    }
                    if (twitterJSONObject[1].has("id_str")) {
                        postid = "twitter" + twitterJSONObject[1].getString("id_str") + format;
                    } else {
                        postid = "twitter" + twitterUserID + format;
                    }
                } else {
                    postid = "twitter_" + format;
                }
                String writePath;
                if (twitterJSONObject[2] != null) {
                    if (twitterJSONObject[2].has("media_url")) {
                        imageHTTPURL = twitterJSONObject[2].getString("media_url");
                    }
                    if (twitterJSONObject[2].has("media_url_https")) {
                        imageHTTPSURL = twitterJSONObject[2].getString("media_url_https");
                    }
                } else if (twitterJSONObject[0].has("profile_image_url_https")) {
                    if (twitterJSONObject[0].has("profile_image_url")) {
                        imageHTTPURL = twitterJSONObject[0].getString("profile_image_url");
                    }
                    if (twitterJSONObject[0].has("profile_image_url_https")) {
                        imageHTTPSURL = twitterJSONObject[0].getString("profile_image_url_https");
                    }
                } else if (twitterJSONObject[0].has("profile_background_image_url")) {
                    if (twitterJSONObject[0].has("profile_background_image_url")) {
                        imageHTTPURL = twitterJSONObject[0].getString("profile_background_image_url");
                    }
                    if (twitterJSONObject[0].has("profile_background_image_url_https")) {
                        imageHTTPSURL = twitterJSONObject[0].getString("profile_background_image_url_https");
                    }
                } else {
                    twitterMetrics.setImageStatus(Boolean.FALSE);
                    twitterMetrics.setReadPath(null);
                }
                String readPath;
                if (imageHTTPSURL != null) {
                    writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(imageHTTPSURL);
                    if ((Common.resizeImageFromUrlandSave(imageHTTPSURL, writePath))) {
                        readPath = "/social_images/" + postid + "." + Common.getImageFormat(imageHTTPSURL);
                        twitterMetrics.setImageStatus(Boolean.TRUE);
                        twitterMetrics.setReadPath(readPath);
                    }
                } else if (imageHTTPURL != null) {
                    writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(imageHTTPURL);
                    if ((Common.resizeImageFromUrlandSave(imageHTTPURL, writePath))) {
                        readPath = "/social_images/" + postid + "." + Common.getImageFormat(imageHTTPURL);
                        twitterMetrics.setImageStatus(Boolean.TRUE);
                        twitterMetrics.setReadPath(readPath);
                    }
                } else {
                    twitterMetrics.setImageStatus(Boolean.FALSE);
                    twitterMetrics.setReadPath(null);
                }
            } catch (Exception e) {
                logger.error("Exception for getTwitterMetricsJson: ", e);
            }
        }
        return twitterMetrics;
    }

    public JSONObject[] gettwitterJsonArray(String userName) {
        JSONObject recenttweetJson = null;
        JSONObject TwitterJson = null;
        JSONObject[] twitterJSONObject = null;
        JSONObject subMedia = null, errorResponseJSON = null;
        JSONArray responseJson = null;
        boolean checkResponse = true;
        JSONObject errorObject = null;
        try {
            twitterJSONObject = new JSONObject[4];
            OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
            consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
            HttpGet httpget = new HttpGet(getTwitterUserSerachUrl(getTwitterAPIEncodedQuery(userName)));
            consumer.sign(httpget);
            /*for (Header h : httpget.getAllHeaders()) {
                logger.debug("Twitter Header Part For Twitter API : " + h.getName() + ": " + h.getValue());
            }*/
            HttpClient client = HttpClientBuilder.create().build();
            //.setContentCharset("UTF-8");
            /*response.getParams().setCharacterEncoding("UTF-8");*/
            HttpResponse response = client.execute(httpget);
            try {
                Header[] twiiterHeader = response.getAllHeaders();
                ArrayList<Header> arrayList = new ArrayList<>(Arrays.asList(twiiterHeader));
                for (Header header : arrayList) {
                    String key = header.getName();
                    String value = header.getValue();
                    if (value != null && key.equalsIgnoreCase("x-rate-limit-remaining")) {
                        int apiBalance = Integer.parseInt(value);
                        logger.info("x-rate-limit-remaining is to call twitter api " + apiBalance);
                        if (apiBalance <= 25) {
                            logger.info("The last api call before thread sleep for user is " + userName);
                            dontCallTwitterAPI = true;
                        }
                    }
                    try {
                        if (value != null && key.equalsIgnoreCase("status")) {
                            if (!(value.trim().equalsIgnoreCase("200 OK"))) {
                                checkResponse = false;
                            } else if ((value.trim().equalsIgnoreCase("429 Too Many Requests"))) {
                                checkResponse = false;
                            }
                        }
                    } catch (Exception e) {
                        checkResponse = true;
                        logger.error("Excpetion in checking response code: " + userName);
                    }
                }
            } catch (NumberFormatException e) {
                dontCallTwitterAPI = false;
                logger.error("Excpetion in x-rate-limit-remaining: " + e);
            }
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            if (checkResponse) {
                responseJson = new JSONArray(responseStrBuilder.toString());
            } else {
                errorResponseJSON = new JSONObject(responseStrBuilder.toString());
            }
            /*logger.info("The respons-e form twitter api in gettwitterJsonArray "+responseJson);*/
            if (responseJson != null) {
                if (responseJson.getJSONObject(0) != null) {
                    if ((Common.checkJSONObjectInJSONArrayResponse(responseJson)) != null) {
                        TwitterJson = responseJson.getJSONObject(0);
                    }
                    if (TwitterJson != null && (Common.checkJSONObjectInJSONObjectResponse(TwitterJson, "status")) != null) {
                        recenttweetJson = TwitterJson.getJSONObject("status");
                    }
                    twitterJSONObject[0] = TwitterJson;
                    twitterJSONObject[1] = recenttweetJson;
                    if (recenttweetJson != null) {
                        if ((Common.checkJSONObjectInJSONObjectResponse(recenttweetJson, "entities")) != null) {
                            JSONObject entities = recenttweetJson.getJSONObject("entities");

                            if ((Common.checkJSONArrayInJSONObjectResponse(entities, "media")) != null) {
                                /*entities.getJSONArray("media");*/
 /*JSONArray responsemedia=getWhoisObj(entities,"media");*/
                                JSONArray media = entities.getJSONArray("media");
                                subMedia = media.getJSONObject(0);
                                twitterJSONObject[2] = subMedia;
                            }
                        }
                    }
                }
                /*if (!checkResponse) {
                    try {
                        APIErrorHandlingService.raiseTwitterAPIError(responseStrBuilder.toString(), Common.SOCIAL_MEDIA_ANALYZER, 29, userName);
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        logger.error("InterruptedException in making thread sleep cause: " + ex.getMessage());
                    }
                }*/
            } else if (errorResponseJSON != null) {
                /*APIErrorHandlingService.raiseTwitterAPIError(responseStrBuilder.toString(), Common.SOCIAL_MEDIA_ANALYZER, 29, userName);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException in making thread sleep cause: " + ex.getMessage());
                }*/
                try {
                    JSONArray errorsArray = null;
                    if (errorResponseJSON.has("errors")) {
                        errorsArray = errorResponseJSON.getJSONArray("errors");
                    }
                    if (errorsArray != null) {
                        if (errorsArray.getJSONObject(0) != null) {
                            JSONObject messageObject = errorsArray.getJSONObject(0);
                            int responseCode = messageObject.getInt("code");
                            String responseMessage = messageObject.getString("message");
                            if ((responseCode != 200) && (!(responseMessage.equalsIgnoreCase("OK")))) {
                                logger.info("Twitter Response API code: " + responseCode + ", Message" + responseMessage);
                                errorObject = new JSONObject();
                                boolean emptyResponse = true;
                                errorObject.put("emptyResponse", emptyResponse);
                            }
                        }
                    }
                } catch (JSONException e) {
                }
            }
        } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException | UnsupportedOperationException | JSONException e) {
            logger.error("Exception In gettwitterJsonArray: ", e);
        } finally {
            twitterJSONObject[0] = TwitterJson;
            twitterJSONObject[1] = recenttweetJson;
            twitterJSONObject[2] = subMedia;
            twitterJSONObject[3] = errorObject;
        }
        return twitterJSONObject;
    }

    public static int checkTwitterAPIRequestLimit() {
        OAuthConsumer consumer;
        BufferedReader streamReader = null;
        HttpResponse response;
        StringBuilder responseStrBuilder = null;
        int remainingLimit = 0;
        try {
            try {
                consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
                consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
                HttpGet httpget = new HttpGet(getTwitterAPILimitUrl());
                consumer.sign(httpget);
                /*for (Header h : httpget.getAllHeaders()) {
                    logger.debug("Twitter Header Part For Twitter API : " + h.getName() + ": " + h.getValue() + "   ==" + h);
                }*/
                HttpClient client = HttpClientBuilder.create().build();
                response = client.execute(httpget);
                streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                responseStrBuilder = new StringBuilder();
            } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException | UnsupportedOperationException e) {
                logger.error("Exception in checkTwitterUserExistence Of  CommonsHttpOAuthConsumer: ", e);
            }
            String inputStr;
            if (streamReader != null) {
                while ((inputStr = streamReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
            }
            JSONObject responseJson = null;
            JSONObject resourcesJSON = null;
            JSONObject usersJSON = null;
            JSONObject usersSearchJSON = null;
            if (responseStrBuilder != null) {
                responseJson = new JSONObject(responseStrBuilder.toString());
            }
            if (responseJson != null) {
                if ((Common.checkJSONObjectInJSONObjectResponse(responseJson, "resources")) != null) {
                    resourcesJSON = Common.checkJSONObjectInJSONObjectResponse(responseJson, "resources");
                }
            }
            if (resourcesJSON != null) {
                if ((Common.checkJSONObjectInJSONObjectResponse(resourcesJSON, "users")) != null) {
                    usersJSON = Common.checkJSONObjectInJSONObjectResponse(resourcesJSON, "users");
                }
            }
            if (usersJSON != null) {
                if ((Common.checkJSONObjectInJSONObjectResponse(usersJSON, "/users/search")) != null) {
                    usersSearchJSON = Common.checkJSONObjectInJSONObjectResponse(usersJSON, "/users/search");
                }
            }
            if (usersSearchJSON != null) {
                try {
                    remainingLimit = usersSearchJSON.getInt("remaining");
                    return remainingLimit;
                } catch (Exception e) {
                    logger.error("Exception for remainingLimit checkTwitterAPIRequestLimit ", e);
                }
            }
            return remainingLimit;

        } catch (IOException | JSONException e) {
            logger.error("Exception in checkTwitterUserExistence: ", e);
        }
        return 0;
    }

    public String getHashTagfromJSONObject(JSONArray twitterResponse) {
        String userHashTag = "";
        int hashCount = 0;
        try {
            for (int i = 0; i < twitterResponse.length(); i++) {
                if (twitterResponse.getJSONObject(i) != null) {
                    JSONObject statusObject = twitterResponse.getJSONObject(i);
                    if (Common.checkJSONObjectInJSONObjectResponse(statusObject, "entities") != null) {
                        JSONObject entitiesObject = statusObject.getJSONObject("entities");
                        if (Common.checkJSONArrayInJSONObjectResponse(entitiesObject, "hashtags") != null) {
                            JSONArray hashtagsArray = entitiesObject.getJSONArray("hashtags");
                            if (hashtagsArray.length() != 0) {
                                String tempHashTag = "";
                                for (int j = 0; j < hashtagsArray.length(); j++) {
                                    try {
                                        JSONObject subHashObject = hashtagsArray.getJSONObject(j);
                                        if (Common.checkStringInJSONObjectResponse(subHashObject, "text") != null) {
                                            tempHashTag = tempHashTag + "#" + subHashObject.getString("text") + "  ";
                                            hashCount++;
                                        }
                                    } catch (Exception e) {
                                        logger.error("exception in hash tag extrcation when finding text" + e);
                                    }
                                }
                                userHashTag = userHashTag + tempHashTag;
                            }
                        }
                    }
                }
                if (hashCount >= 30) {
                    break;
                }
            }
        } catch (Exception e) {
            logger.error("Exception for getHashTagfromJSONObject: ", e);
        }
        return userHashTag;
    }

    public String gettwitterHashTagJsonArray(String userID) {
        JSONObject errorResponseJSON = null;
        JSONArray responseJson = null;
        boolean checkResponse = true;
        try {
            OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
            consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
            HttpGet httpget = new HttpGet(getTwitterUserHashTagURL(userID));
            consumer.sign(httpget);
            HttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(httpget);
            try {
                Header[] twiiterHeader = response.getAllHeaders();
                ArrayList<Header> arrayList = new ArrayList<>(Arrays.asList(twiiterHeader));
                for (Header header : arrayList) {
                    String key = header.getName();
                    String value = header.getValue();
                    if (value != null && key.equalsIgnoreCase("x-rate-limit-remaining")) {
                        logger.info(Integer.parseInt(value) + "x-rate-limit-remaining  to call twitter api ");
                        if (Integer.parseInt(value) <= 25) {
                            logger.info("The last api call before thread sleep for user is " + userID);
                            //dontCallTwitterAPI=true;
                        }
                    }
                    try {
                        if (value != null && key.equalsIgnoreCase("status")) {
                            if (!(value.trim().equalsIgnoreCase("200 OK"))) {
                                checkResponse = false;
                            } else if ((value.trim().equalsIgnoreCase("429 Too Many Requests"))) {
                                checkResponse = false;
                            }
                        }
                    } catch (Exception e) {
                        checkResponse = true;
                        logger.error("Excpetion in checking response code: ", e);
                    }
                }
            } catch (Exception e) {
                //dontCallTwitterAPI=false;
                logger.error("Excpetion in x-rate-limit-remaining: ", e);
            }

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            try {
                if (checkResponse) {
                    responseJson = new JSONArray(responseStrBuilder.toString());
                } else {
                    errorResponseJSON = new JSONObject(responseStrBuilder.toString());
                }
            } catch (Exception e) {
                logger.error("Exception in fetching twitter response: ", e);
            }
            /*logger.info("The respons-e form twitter api in gettwitterJsonArray "+responseJson);*/
            if (responseJson != null) {
                String userHashTag = getHashTagfromJSONObject(responseJson);
                if (!userHashTag.isEmpty()) {
                    if (userHashTag.length() >= 1800) {
                        userHashTag = userHashTag.substring(0, 1799);
                    }
                    userHashTag += ".";
                    return userHashTag;
                } else {
                    return "NoHashTag";
                }
            } else if (errorResponseJSON != null) {
                JSONArray errorsArray = null;
                if ((Common.checkJSONArrayInJSONObjectResponse(errorResponseJSON, "errors")) != null) {
                    errorsArray = Common.checkJSONArrayInJSONObjectResponse(errorResponseJSON, "errors");
                }
                if (errorsArray != null) {
                    if (errorsArray.getJSONObject(0) != null) {
                        JSONObject messageObject = errorsArray.getJSONObject(0);
                        int responseCode = messageObject.getInt("code");
                        String responseMessage = messageObject.getString("message");
                        if ((responseCode != 200) && (!(responseMessage.equalsIgnoreCase("OK")))) {
                            logger.info("Twitter Response API code " + responseCode + " Message" + responseMessage);
                            return "NoHashTag";
                        }
                    }
                }

            }
        } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException | UnsupportedOperationException | JSONException e) {
            logger.error("Exception In gettwitterJsonArray: ", e);
        }
        return "NoHashTag";
    }

    public static String getTwitterUserHashTagURL(String userID) {
        logger.debug("Request URL in getTwitterUserHashTagURL: " + "https://api.twitter.com/1.1/statuses/user_timeline.json?count=100&trim_user=false&exclude_replies=true&contributor_details=true&include_rts=false&user_id=" + userID + "");
        return "https://api.twitter.com/1.1/statuses/user_timeline.json?count=100&trim_user=false&exclude_replies=true&contributor_details=true&include_rts=false&user_id=" + userID + "";
    }

    public static String getTwitterUserSerachUrl(String userName) {
        if (userName.startsWith("@")) {
            userName = userName.replace("@", " ").trim();
        }
        logger.debug("Request URL in getTwitterUserSerachUrl: " + "https://api.twitter.com/1.1/users/search.json?q=" + userName + "&page=1&count=1");
        return "https://api.twitter.com/1.1/users/search.json?q=" + userName + "&page=1&count=1";
    }

    /*Based on twitter user Id Url */
    public static String getTwitterUserDataIDUrl(String userID) {
        logger.debug("Request URL in getTwitterUserDataIDUrl: " + "https://api.twitter.com/1.1/users/show.json?user_id=" + userID);
        return "https://api.twitter.com/1.1/users/show.json?user_id=" + userID;
    }

    public static String getTwitterAPILimitUrl() {
        logger.debug("Request URL in getTwitterAPILimitUrl: " + "https://api.twitter.com/1.1/application/rate_limit_status.json?resources=users");
        return "https://api.twitter.com/1.1/application/rate_limit_status.json?resources=users";
    }

    /*To fetch user twitter data  based on twitter user Id*/
    public JSONObject[] getTwitterDataID(String userID) {
        JSONObject recenttweetJson = null;
        JSONObject[] twitterJSONObject = null;
        JSONObject subMedia = null, errorResponseJSON = null;
        JSONObject responseJson = null;
        boolean checkResponse = true;
        JSONObject errorObject = null;
        try {
            twitterJSONObject = new JSONObject[4];
            OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
            consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
            //logger.info("The request to twitter api in gettwitterJsonArray"+getTwitterUserSerachUrl(userName));
            HttpGet httpget = new HttpGet(getTwitterUserDataIDUrl(userID));
            consumer.sign(httpget);
            HttpClient client = HttpClientBuilder.create().build();
            //.setContentCharset("UTF-8");
            /*response.getParams().setCharacterEncoding("UTF-8");*/
            HttpResponse response = client.execute(httpget);

            try {
                Header[] twiiterHeader = response.getAllHeaders();
                ArrayList<Header> arrayList = new ArrayList<>(Arrays.asList(twiiterHeader));
                for (Header header : arrayList) {
                    String key = header.getName();
                    String value = header.getValue();
                    if (value != null && key.equalsIgnoreCase("x-rate-limit-remaining")) {
                        int apiBalance = Integer.parseInt(value);
                        logger.info("x-rate-limit-remaining is to call twitter api " + apiBalance);
                        if (apiBalance <= 2) {
                            logger.info("The last api call before thread sleep for user is " + userID);
                            dontCallTwitterAPI = true;
                        }
                    }
                    try {
                        if (value != null && key.equalsIgnoreCase("status")) {
                            if (!(value.trim().equalsIgnoreCase("200 OK"))) {
                                checkResponse = false;
//                            } else if ((value.trim().equalsIgnoreCase("429 Too Many Requests"))) {
                            } else {
                                checkResponse = false;
                            }
                        }
                    } catch (Exception e) {
                        checkResponse = true;
                        logger.error("Excpetion in checking response code: " + userID);
                    }
                }
            } catch (Exception e) {
                dontCallTwitterAPI = false;
                logger.error("Excpetion in x-rate-limit-remaining: ", e);
            }

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            logger.debug("Twitter API Stream Reader :" + streamReader + " - Twitter API Response String Builder : " + responseStrBuilder);
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            if (checkResponse) {
                responseJson = new JSONObject(responseStrBuilder.toString());
            } else {
                errorResponseJSON = new JSONObject(responseStrBuilder.toString());
            }
            /*logger.info("The respons-e form twitter api in gettwitterJsonArray "+responseJson);*/
            if (responseJson != null) {
                // if (responseJson.getJSONObject(0) != null) {
//                                        if((Common.checkJSONObjectInJSONArrayResponse(responseJson))!=null){
//                                                    TwitterJson = responseJson.getJSONObject(0);
//                                            }

                /* try {
                    APIErrorHandlingService.raiseTwitterAPIError(responseStrBuilder.toString(), Common.SOCIAL_MEDIA_ANALYZER, 29, null);
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException in checkTwitterUserExistence cause: " + ex.getMessage());
                }*/
                if ((Common.checkJSONObjectInJSONObjectResponse(responseJson, "status")) != null) {
                    recenttweetJson = responseJson.getJSONObject("status");
                }
                twitterJSONObject[0] = responseJson;
                twitterJSONObject[1] = recenttweetJson;
                if (recenttweetJson != null) {
                    if ((Common.checkJSONObjectInJSONObjectResponse(recenttweetJson, "entities")) != null) {
                        JSONObject entities = recenttweetJson.getJSONObject("entities");

                        if ((Common.checkJSONArrayInJSONObjectResponse(entities, "media")) != null) {
                            /*entities.getJSONArray("media");*/
 /*JSONArray responsemedia=getWhoisObj(entities,"media");*/
                            JSONArray media = entities.getJSONArray("media");
                            subMedia = media.getJSONObject(0);
                            twitterJSONObject[2] = subMedia;
                        }
                    }
                }
                //}
            } else if (errorResponseJSON != null) {
                /*try {
                    APIErrorHandlingService.raiseTwitterAPIError(responseStrBuilder.toString(), Common.SOCIAL_MEDIA_ANALYZER, 29, null);
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException in checkTwitterUserExistence cause: " + ex.getMessage());
                }*/
                JSONArray errorsArray = null;
                if ((Common.checkJSONArrayInJSONObjectResponse(errorResponseJSON, "errors")) != null) {
                    errorsArray = Common.checkJSONArrayInJSONObjectResponse(errorResponseJSON, "errors");
                }
                if (errorsArray != null) {
                    if (errorsArray.getJSONObject(0) != null) {
                        JSONObject messageObject = errorsArray.getJSONObject(0);
                        int responseCode = messageObject.getInt("code");
                        String responseMessage = messageObject.getString("message");
                        if ((responseCode != 200) && (!(responseMessage.equalsIgnoreCase("OK")))) {
                            logger.info("Twitter Response API code " + responseCode + " Message" + responseMessage);
                            errorObject = new JSONObject();
                            boolean emptyResponse = true;
                            errorObject.put("emptyResponse", emptyResponse);
                        }
                    }
                }

            }
        } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException | UnsupportedOperationException | JSONException e) {
            logger.error("Exception for getTwitterDataID: ", e);
        } finally {
            twitterJSONObject[0] = responseJson;
            twitterJSONObject[1] = recenttweetJson;
            twitterJSONObject[2] = subMedia;
            twitterJSONObject[3] = errorObject;
        }
        return twitterJSONObject;
    }

    public static String getTwitterAPIEncodedQuery(String queryUserName) {
        String encodeQuery = null;
        try {
            encodeQuery = URLEncoder.encode(queryUserName, "UTF-8");
//            encodeQuery = encodeQuery.replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            logger.error("Exception converting in getAPIQueryString: ", e);
        }
        if (encodeQuery != null) {
            StringBuilder builderObj = new StringBuilder(encodeQuery.length());
            char focus;
            for (int i = 0; i < encodeQuery.length(); i++) {
                focus = encodeQuery.charAt(i);
                if (focus == '*') {
                    builderObj.append("%2A");
                } else if (focus == '+') {
                    builderObj.append("%20");
                } else if (focus == '%' && (i + 1) < encodeQuery.length()
                        && encodeQuery.charAt(i + 1) == '7' && encodeQuery.charAt(i + 2) == 'E') {
                    builderObj.append('~');
                    i += 2;
                } else {
                    builderObj.append(focus);
                }
            }
            encodeQuery = builderObj.toString();
        }
        return encodeQuery;
    }

    /*Returns subscribers of  twitter page
                (OR)
    Sends the exception message to support mail
     */
    public long checkTwitterAPIStatus(String userName) {
        long twitterFollowers = 0;
        JSONObject twitterResponse = null;
        if (userName != null) {
            twitterResponse = getTwitterAccount(userName);
        }
        if (twitterResponse != null && twitterResponse.has("followers_count")) {
            try {
                twitterFollowers = twitterResponse.getLong("followers_count");
            } catch (JSONException e) {
                logger.error("JSONException in checkTwitterAPIStatus for username:: " + userName + ", case::", e);
            }
        }
        return twitterFollowers;
    }

    /*Sendmail when api status down or returns twitter repsonse for user name*/
    public JSONObject getTwitterAccount(String userName) {
        logger.info("In getTwitterAccount for username::" + userName);
        JSONObject twitterJson = null;
        JSONArray responseJson = null;
        try {
            OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
            consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
            HttpGet httpget = new HttpGet(getTwitterUserSerachUrl(getTwitterAPIEncodedQuery(userName)));
            consumer.sign(httpget);
            /*for (Header h : httpget.getAllHeaders()) {
                logger.debug("Twitter Header Part For Twitter API : " + h.getName() + ": " + h.getValue());
            }*/
            HttpClient client = HttpClientBuilder.create().build();
            //.setContentCharset("UTF-8");
            /*response.getParams().setCharacterEncoding("UTF-8");*/
            HttpResponse response = client.execute(httpget);
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            try {
                responseJson = new JSONArray(responseStrBuilder.toString());
            } catch (Exception e) {
                logger.error("Exception in getTwitterAccount when converting resposne string into JSONAray cause:: ", e);
            }
            /*logger.info("The respons-e form twitter api in gettwitterJsonArray "+responseJson);*/
            if (responseJson != null && responseJson.length() != 0) {
                twitterJson = responseJson.getJSONObject(0);
            }
            if (twitterJson == null) {
                APIError errorObj = new APIError();
                errorObj.setApiName(APIErrorHandlingService.TWITTER);
                errorObj.setToolId(29);
                errorObj.setErrorMessage("Dialy alert error message::: " + responseStrBuilder.toString() + ", for UserName: " + (userName != null ? userName : null));
                APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER, Boolean.FALSE);
            }
        } catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException | IOException | UnsupportedOperationException | JSONException e) {
            logger.error("Exception In gettwitterJsonArray: ", e);
        }
        return twitterJson;
    }

}
