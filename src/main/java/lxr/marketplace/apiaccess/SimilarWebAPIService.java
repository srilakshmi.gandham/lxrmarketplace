/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.util.Map;

/**
 *
 * @author sagar
 */
public interface SimilarWebAPIService {

    public Map<String,Map<String, Double>> getChannlePerformanceAndTrafficShare(String queryDomain, String startEnd, String endDate,int proxyHostNo);

    public Map<String, Double> getPaidSearchVists(String queryDomain, String startEnd, String endDate,int proxyHostNo);

    public Map<String, Double> getDevicePerformance(String queryDomain, String startEnd, String endDate,int proxyHostNo);

    public Map<String, String> getDataAvailableDateRange();

    public Map<String, String> getAPIFormatedDate(Map<String, String> avaialbleDateRanges);

   public int getAPIBalanceLimitCount();
   
   public Map<String, Double> getMonthlyVisits(String queryDomain, String startDate, String endDate,int proxyHostNo);
   
   public Map<String, Double> getChannelsOverViewMetrics(String queryDomain, String startDate, String endDate,int proxyHostNo);
   
   public Map<String, Long> getChannelsAnalysisMetrics(String queryDomain, String startDate, String endDate,int proxyHostNo);

//    public Date getDateObject(String date);
}