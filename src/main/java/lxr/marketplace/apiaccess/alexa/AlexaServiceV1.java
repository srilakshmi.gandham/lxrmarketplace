/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess.alexa;

/**
 *
 * @author sagar
 */
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Makes a request to the Alexa Web Information Service TrafficHistory action.
 */
@Service
public class AlexaServiceV1 {

    private static final Logger LOGGER = Logger.getLogger(AlexaServiceV1.class);
    private static final String SERVICE_HOST = "awis.amazonaws.com";
    protected static final String SERVICE_ENDPOINT = "awis.us-west-1.amazonaws.com";
    private static final String SERVICE_URI = "/api";
    private static final String SERVICE_REGION = "us-west-1";
    private static final String SERVICE_NAME = "awis";
    private static final String AWS_BASE_URL = "https://" + SERVICE_HOST + SERVICE_URI;
    private static final String HASH_ALGORITHM = "HmacSHA256";
    private static final String DATEFORMAT_AWS = "yyyyMMdd'T'HHmmss'Z'";
    private static final String DATEFORMAT_CREDENTIAL = "yyyyMMdd";

    static String accessKeyId = "AKIAIELBWW5UBYLNTWSA";
    static String secretAccessKey = "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";

    private static String sha256(String textToHash) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] byteOfTextToHash = textToHash.getBytes("UTF-8");
        byte[] hashedByteArray = digest.digest(byteOfTextToHash);
        return bytesToHex(hashedByteArray);
    }

    static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        Mac mac = Mac.getInstance(HASH_ALGORITHM);
        mac.init(new SecretKeySpec(key, HASH_ALGORITHM));
        return mac.doFinal(data.getBytes("UTF8"));
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
        byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
        byte[] kDate = HmacSHA256(dateStamp, kSecret);
        byte[] kRegion = HmacSHA256(regionName, kDate);
        byte[] kService = HmacSHA256(serviceName, kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
    }

    /**
     * Makes a request to the specified Url and return the results as a String
     *
     * @param requestUrl url to make request to
     * @param authorization
     * @param amzDate
     * @return the XML document as a String
     * @throws IOException
     */
    public static String makeRequest(String requestUrl, String authorization, String amzDate) throws IOException {
        URL url = new URL(requestUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Accept", "application/xml");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("X-Amz-Date", amzDate);
        conn.setRequestProperty("Authorization", authorization);

        InputStream in = (conn.getResponseCode() / 100 == 2 ? conn.getInputStream() : conn.getErrorStream());

        // Read the response
        StringBuffer sb = new StringBuffer();
        int c;
        int lastChar = 0;
        while ((c = in.read()) != -1) {
            if (c == '<' && (lastChar == '>')) {
                sb.append('\n');
            }
            sb.append((char) c);
            lastChar = c;
        }
        in.close();
        return sb.toString();
    }

    /**
     * Makes a request to the Alexa Web Information Service TrafficHistory
     * action
     *
     * @param args
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        // Read command line parameters
        String site = "lxrguide.com/";
        String responseGroup = "History";
        String actionName = "TrafficHistory";
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -31);
        String domain = Common.getDomainFromUrl(site);
        LOGGER.debug("domain: " + domain);
        getTrafficStatsDoc(domain, cal.getTime(), responseGroup, actionName, 31);
    }

    private static String getAamazonDate(Date date) {
        SimpleDateFormat formatAWS = new SimpleDateFormat(DATEFORMAT_AWS);
        formatAWS.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatAWS.format(date);
    }

    private static String getDateStamp(Date date) {
        SimpleDateFormat formatCredential = new SimpleDateFormat(DATEFORMAT_CREDENTIAL);
        formatCredential.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatCredential.format(date);
    }

    public static Document getTrafficStatsDoc(String site, Date date, String responseGroupName, String actionName, int range) {
        try {

            Calendar cal = Calendar.getInstance();
            String amzDate = getAamazonDate(cal.getTime());
            String dateStamp = getDateStamp(cal.getTime());
            String startDate = getDateStamp(date);

            String canonicalQuery = "Action=" + actionName + "&Range=" + range + "&ResponseGroup=" + responseGroupName + "&Start=" + startDate + "&Url=" + site;

            String canonicalHeaders = "host:" + SERVICE_ENDPOINT + "\n" + "x-amz-date:" + amzDate + "\n";
            String signedHeaders = "host;x-amz-date";

            String payloadHash = sha256("");

            String canonicalRequest = "GET" + "\n" + SERVICE_URI + "\n" + canonicalQuery + "\n" + canonicalHeaders + "\n" + signedHeaders + "\n" + payloadHash;

            // ************* TASK 2: CREATE THE STRING TO SIGN*************
            // Match the algorithm to the hashing algorithm you use, either SHA-1 or
            // SHA-256 (recommended)
            String algorithm = "AWS4-HMAC-SHA256";
            String credentialScope = dateStamp + "/" + SERVICE_REGION + "/" + SERVICE_NAME + "/" + "aws4_request";
            String stringToSign = algorithm + '\n' + amzDate + '\n' + credentialScope + '\n' + sha256(canonicalRequest);

            // ************* TASK 3: CALCULATE THE SIGNATURE *************
            // Create the signing key
            byte[] signingKey = getSignatureKey(secretAccessKey, dateStamp, SERVICE_REGION, SERVICE_NAME);

            // Sign the string_to_sign using the signing_key
            String signature = bytesToHex(HmacSHA256(stringToSign, signingKey));

            String uri = AWS_BASE_URL + "?" + canonicalQuery;

            LOGGER.debug("Making request to:\n" + uri + "\n");

            // Make the Request
            String authorization = algorithm + " " + "Credential=" + accessKeyId + "/" + credentialScope + ", " + "SignedHeaders=" + signedHeaders + ", " + "Signature=" + signature;

//            long startTime = System.currentTimeMillis();
            String xmlResponse = makeRequest(uri, authorization, amzDate);
//            long elapsedTime = System.currentTimeMillis() - startTime;

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlResponse)));

            LOGGER.debug("Alexa Response: " + xmlResponse);
            return doc;
        } catch (Exception ex) {
            LOGGER.error("Exception when fetching Alexa Data", ex);
        }
        return null;
    }

    public List<AlexaDailyTraffic> fetchTrafficHistory(String siteUrl, Calendar startDay, int range) {
        String domain = Common.getDomainFromUrl(siteUrl);
        LOGGER.debug("Fetching Traffic History for " + domain);
        List<AlexaDailyTraffic> trafficHistory = null;
        Document doc = getTrafficStatsDoc(domain, startDay.getTime(), "History", "TrafficHistory", range);
        if (doc != null) {
            LOGGER.debug("Fetched Traffic History for " + domain + ". Parsing..");
            trafficHistory = parseDocForTrafficHistory(doc);
            LOGGER.debug("Traffic History Parsing completed for " + domain + " size: " + trafficHistory.size());
        }
        return trafficHistory;
    }

    public static List<AlexaDailyTraffic> parseDocForTrafficHistory(Document doc) {
        List<AlexaDailyTraffic> trafficHistory = new ArrayList();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (doc != null) {
            doc.getDocumentElement().normalize();
            NodeList nHistoricalData = doc.getElementsByTagName("aws:HistoricalData");
            if (nHistoricalData != null && nHistoricalData.item(0) != null && nHistoricalData.item(0).getNodeType() == Node.ELEMENT_NODE) {
                try {
                    Element eHistoricalData = (Element) nHistoricalData.item(0);
                    NodeList nDailyData = eHistoricalData.getElementsByTagName("aws:Data");
                    for (int i = 0; i < nDailyData.getLength(); i++) {

                        //parse each node and create a AlexaDailyTraffic object
                        Node nNode = nDailyData.item(i);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eNode = (Element) nDailyData.item(i);

                            try {
                                Calendar cal = Calendar.getInstance();
                                String dateString = eNode.getElementsByTagName("aws:Date").item(0).getTextContent();
                                Date date = df.parse(dateString);
                                cal.setTime(date);

                                float pageViewsPerUser = 0;
                                Element pageViews = (Element) eNode.getElementsByTagName("aws:PageViews").item(0);
                                Node nPageViewsPerUser = pageViews.getElementsByTagName("aws:PerUser").item(0);
                                if (nPageViewsPerUser.getNodeType() == Node.ELEMENT_NODE) {
                                    String sPageViewsPerUser = nPageViewsPerUser.getTextContent();
                                    try {
                                        pageViewsPerUser = Float.parseFloat(sPageViewsPerUser);
                                    } catch (NumberFormatException e) {
                                    }
                                }

                                String rankString = eNode.getElementsByTagName("aws:Rank").item(0).getTextContent();
                                long rank = Long.parseLong(rankString.replaceAll(",", ""));

                                float reachPerMill = 0;
                                Element eReach = (Element) eNode.getElementsByTagName("aws:Reach").item(0);
                                Node nReachPerMill = eReach.getElementsByTagName("aws:PerMillion").item(0);
                                if (nReachPerMill.getNodeType() == Node.ELEMENT_NODE) {
                                    String sReachPerMill = nReachPerMill.getTextContent();
                                    try {
                                        reachPerMill = Float.parseFloat(sReachPerMill.replaceAll(",", ""));
                                    } catch (NumberFormatException e) {
                                    }
                                }

                                AlexaDailyTraffic alexaDailyTraffic = new AlexaDailyTraffic();
                                alexaDailyTraffic.setCal(cal);
                                alexaDailyTraffic.setPageViews(pageViewsPerUser);
                                alexaDailyTraffic.setRank(rank);
                                alexaDailyTraffic.setReachPerMill(reachPerMill);
                                trafficHistory.add(alexaDailyTraffic);
                            } catch (NullPointerException | ParseException e) {
                                LOGGER.error("Exception in Alexa response is null..", e);
                            }
//                                LOGGER.error("Date in Alexa response is in some different format..", e);

                        }
                    }
                } catch (NullPointerException e) {
                    LOGGER.error("NullPointerException! while fetching alexa history ", e);
                }
            }
        }
        return trafficHistory;
    }

}