package lxr.marketplace.apiaccess.alexa;

import java.util.Calendar;
/**
 * Stores daily page view, rank and reach per million data
 * @author netelixir
 *
 */
public class AlexaDailyTraffic {
    private Calendar cal;
    private float pageViews;
    private long rank;
    private float reachPerMill;
    
    public AlexaDailyTraffic(){
        super(); 
    }
    public AlexaDailyTraffic(Calendar cal, float pageViews, long rank,
            float reachPerMill) {
        super();
        this.cal = cal;
        this.pageViews = pageViews;
        this.rank = rank;
        this.reachPerMill = reachPerMill;
    }
    public Calendar getCal() {
        return cal;
    }
    public void setCal(Calendar cal) {
        this.cal = cal;
    }
    public float getPageViews() {
        return pageViews;
    }
    public void setPageViews(float pageViews) {
        this.pageViews = pageViews;
    }
    public float getReachPerMill() {
        return reachPerMill;
    }
    public void setReachPerMill(float reachPerMill) {
        this.reachPerMill = reachPerMill;
    }
    public long getRank() {
        return rank;
    }
    public void setRank(long rank) {
        this.rank = rank;
    }
}
