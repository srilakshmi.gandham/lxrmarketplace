/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess.alexa;

/**
 *
 * @author anil
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class AlexaAPIService {

    private static Logger logger = Logger.getLogger(AlexaAPIService.class);
    private static final String SERVICE_HOST = "awis.amazonaws.com";
    private static final String AWS_BASE_URL = "https://" + SERVICE_HOST + "/?";
    private static final String HASH_ALGORITHM = "HmacSHA256";

    private static final String DATEFORMAT_AWS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private final String accessKeyId;
    private final String secretAccessKey;

    public AlexaAPIService(String accessKeyId, String secretAccessKey) {
        super();
        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
    }

    public List<AlexaDailyTraffic> fetchTrafficHistory(String domain, Calendar startDay, int range) {
        String query = null;
        List<AlexaDailyTraffic> trafficHistory = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
            String sStartDay = df.format(startDay.getTime());
            Map<String, String> queryParams = new TreeMap<>();
            queryParams.put("Start", sStartDay);
            queryParams.put("Range", "" + range);
            query = buildQuery("TrafficHistory", "History", domain, queryParams);
            Document doc = createDocumentFromQuery(query);
            if (doc != null) {
                trafficHistory = parseDocForTrafficHistory(doc);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return trafficHistory;
    }

    
    /**
     * Parse document to get daily reach and page view trend
     *
     * @param doc om.w3c documen from alexa xml response
     * @return List of objects containing daily page view and reach
     */
    public List<AlexaDailyTraffic> parseDocForTrafficHistory(Document doc) {
        List<AlexaDailyTraffic> trafficHistory = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (doc != null) {
            doc.getDocumentElement().normalize();
            NodeList nHistoricalData = doc.getElementsByTagName("aws:HistoricalData");
            if (nHistoricalData != null && nHistoricalData.item(0) != null && nHistoricalData.item(0).getNodeType() == Node.ELEMENT_NODE) {
                try {
                    Element eHistoricalData = (Element) nHistoricalData.item(0);
                    NodeList nDailyData = eHistoricalData.getElementsByTagName("aws:Data");
                    for (int i = 0; i < nDailyData.getLength(); i++) {

                        //parse each node and create a AlexaDailyTraffic object
                        Node nNode = nDailyData.item(i);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eNode = (Element) nDailyData.item(i);

                            try {
                                Calendar cal = Calendar.getInstance();
                                String dateString = eNode.getElementsByTagName("aws:Date").item(0).getTextContent();
                                Date date = df.parse(dateString);
                                cal.setTime(date);

                                float pageViewsPerUser = 0;
                                Element pageViews = (Element) eNode.getElementsByTagName("aws:PageViews").item(0);
                                Node nPageViewsPerUser = pageViews.getElementsByTagName("aws:PerUser").item(0);
                                if (nPageViewsPerUser.getNodeType() == Node.ELEMENT_NODE) {
                                    String sPageViewsPerUser = nPageViewsPerUser.getTextContent();
                                    try {
                                        pageViewsPerUser = Float.parseFloat(sPageViewsPerUser);
                                    } catch (NumberFormatException e) {
                                    }
                                }
                                long rank = 0;
                                String rankString = eNode.getElementsByTagName("aws:Rank").item(0).getTextContent();
//                                newly added by anil
                                if(rankString != null && !rankString.equals("")){
                                    rank = Long.parseLong(rankString.replaceAll(",", "").toString());
                                }

                                float reachPerMill = 0;
                                Element eReach = (Element) eNode.getElementsByTagName("aws:Reach").item(0);
                                Node nReachPerMill = eReach.getElementsByTagName("aws:PerMillion").item(0);
                                if (nReachPerMill.getNodeType() == Node.ELEMENT_NODE) {
                                    String sReachPerMill = nReachPerMill.getTextContent();
                                    try {
                                        reachPerMill = Float.parseFloat(sReachPerMill.replaceAll(",", "").toString());
                                    } catch (NumberFormatException e) {
                                    }
                                }

                                AlexaDailyTraffic alexaDailyTraffic = new AlexaDailyTraffic(cal, pageViewsPerUser, rank, reachPerMill);
                                trafficHistory.add(alexaDailyTraffic);
                            } catch (NullPointerException e) {
                                logger.error("Exception in Alexa response is null.."+e);
                            } catch (ParseException e) {
                                logger.error("Date in Alexa response is in some different format.."+e);
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    logger.error("NullPointerException! while fetching alexa history " + e);
                }
            }
        }
        return trafficHistory;
    }
    
    /**
     * Connects to Alexa API with a query and creates document by parsing the
     * xml response
     *
     * @param query to alexa
     * @return document generated from alexa xml response
     */
    private Document createDocumentFromQuery(String query) {
        String toSign = "GET\n" + SERVICE_HOST + "\n/\n" + query;
        String signature;
        String uri = null;
        try {
            signature = generateSignature(toSign);
            uri = AWS_BASE_URL + query + "&Signature=" + URLEncoder.encode(signature, "UTF-8");
        } catch (UnsupportedEncodingException | SignatureException e) {
            logger.error("Exception on creating document from query:\n" + e);
        }

        logger.debug("Making Alexa request to:\n" + uri);

        // Make the Request
        Document doc = makeRequest(uri);
        return doc;
    }
    
    /**
     * Builds the query string
     * @param action
     * @param responseGroup
     * @param site
     * @param queryParams
     * @return 
     * @throws java.io.UnsupportedEncodingException
     */
    protected String buildQuery(String action, String responseGroup, String site, Map<String, String> queryParams)
            throws UnsupportedEncodingException {

        final String timestamp = getTimestampFromLocalTime(Calendar.getInstance().getTime());

        queryParams.put("Action", action);
        queryParams.put("ResponseGroup", responseGroup);
        queryParams.put("AWSAccessKeyId", accessKeyId);
        queryParams.put("Timestamp", timestamp);
        queryParams.put("Url", site);
        queryParams.put("SignatureVersion", "2");
        queryParams.put("SignatureMethod", HASH_ALGORITHM);

        String query = "";
        boolean first = true;
        for (String name : queryParams.keySet()) {
            if (first) {
                first = false;
            } else {
                query += "&";
            }

            query += name + "=" + URLEncoder.encode(queryParams.get(name), "UTF-8");
        }

        return query;
    }
    
    /**
     * Generates a timestamp for use with AWS request signing
     *
     * @param date current date
     * @return timestamp
     */
    protected static String getTimestampFromLocalTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT_AWS);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(date);
    }
    
    /**
     * Computes RFC 2104-compliant HMAC signature.
     *
     * @param data The data to be signed.
     * @return The base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException when signature generation fails
     */
    protected String generateSignature(String data)
            throws java.security.SignatureException {
        String result;
        try {
            // get a hash key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(
                    secretAccessKey.getBytes(), HASH_ALGORITHM);

            // get a hasher instance and initialize with the signing key
            Mac mac = Mac.getInstance(HASH_ALGORITHM);
            mac.init(signingKey);

            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());

            // base64-encode the hmac
            // result = Encoding.EncodeBase64(rawHmac);
//            result = new BASE64Encoder().encode(rawHmac);
            result = DatatypeConverter.printBase64Binary(rawHmac);

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }

    /**
     * Makes a request to the specified Url and return the results as a document
     * by parsing the xml response
     *
     * @param requestUrl url to make request to
     * @return dom.w3c document
     */
    public static Document makeRequest(String requestUrl) {
        Document doc = null;
        try {
            URL url = new URL(requestUrl);
            URLConnection conn = url.openConnection();
            InputStream in = conn.getInputStream();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(in);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Exception while alexa data request making", e);
        }

        return doc;
    }
}