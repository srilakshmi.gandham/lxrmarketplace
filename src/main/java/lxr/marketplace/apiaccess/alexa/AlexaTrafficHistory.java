package lxr.marketplace.apiaccess.alexa;

import java.util.Calendar;
import java.util.List;

public class AlexaTrafficHistory {
    private long domainId;
    private Calendar startDay;
    private int range;
    private List<AlexaDailyTraffic> trafficHistory;
    
    public AlexaTrafficHistory(long domainId) {
        super();
        this.domainId = domainId;
    }
    
    public long getDomainId() {
        return domainId;
    }
    public void setDomainId(long domainId) {
        this.domainId = domainId;
    }
    public Calendar getStartDay() {
        return startDay;
    }
    public void setStartDay(Calendar startDay) {
        this.startDay = startDay;
    }
    public int getRange() {
        return range;
    }
    public void setRange(int range) {
        this.range = range;
    }
    public List<AlexaDailyTraffic> getTrafficHistory() {
        return trafficHistory;
    }
    public void setTrafficHistory(List<AlexaDailyTraffic> trafficHistory) {
        this.trafficHistory = trafficHistory;
    } 
    
    
}
