package lxr.marketplace.apiaccess;

import it.grabz.grabzit.GrabzItClient;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ScreenshotAPIService extends JdbcDaoSupport {

/*	public static void main(String[] args) {
		ScreenshotAPIService screenshotAPIService = new ScreenshotAPIService();		
	}
*/
	public void fetchScrreenshot(String filePath, String url){
		logger.info("Fetching scrrenshot for " + url + " filePath:"+filePath);
		try{
    		/**   grabzit free api key **/		
    //		GrabzItClient grabzIt = new GrabzItClient("ZGY5OTVhYjgxNTAxNGZiYTljZTMzNGE1YjFiYTcxNTU=", 
    //				"Pz8/Pz8/Pz8/fRg/Lj8/dj9aPz8/Pz8oPwdSFj8/Cz8=");  
    			
    			
    		/**   grabzit live api key **/	
    		GrabzItClient grabzIt = new GrabzItClient("MDM5MGVlNGQwYTI0NDdkOTg5N2Y4NjU3ODQ1MzhhODE=", 
    				"A34/bj8RanE/P3c/Pz8/RD8/Kz8/Px5uMT8/Pz8/P2M=");
    		logger.debug("grabzIt........."+grabzIt);
    		
            try {
                grabzIt.SetImageOptions(url);            
            } catch (UnsupportedEncodingException e) {
                logger.error("UnsupportedEncodingException while setting image options to fetch screenshot: " + url);
            }        
            try {
                File file = new File(filePath);
                if(file.exists()){
                    file.delete();
                }
                grabzIt.SaveTo(filePath);
                logger.info("Scrrenshot fetched for " + url + " filePath:"+filePath);
            } catch (Exception e) {
                logger.error("Exception while creating grabzit image for url: " + url);
            }
        }catch(Exception e1){
    		e1.printStackTrace();
    	}
	}	
}
