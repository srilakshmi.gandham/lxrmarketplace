/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import lxr.marketplace.socialmedia.APIKeys;
import lxr.marketplace.youTubeChannelAudit.ChannelVideoInfo;
import lxr.marketplace.youTubeChannelAudit.YouTubeChannelInfo;
import static lxr.marketplace.util.Common.getJSONFromAPIResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class YouTubeAPIService {

    private static final Logger LOGGER = Logger.getLogger(YouTubeAPIService.class);

    @Value("${YouTube.API.Source.URL}")
    private String apiEndpointVersionURL;

    @Autowired
    private MessageSource messageSource;

    public YouTubeChannelInfo getYouTubeChannelOverview(String channelName) {
        YouTubeChannelInfo youTubeChannelInfo = null;
        JSONObject responseJSON = getJSONFromAPIResponse(getAPIEndPointURL("channelId", channelName, null));
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        JSONObject subInnerThreeJSON = null;
        JSONObject subInnerFourJSON = null;
        JSONArray innerJSONArray = null;
        if (responseJSON != null && responseJSON.has("items")) {
            try {
                youTubeChannelInfo = new YouTubeChannelInfo();
                innerJSONArray = responseJSON.getJSONArray("items");
                if (innerJSONArray != null && innerJSONArray.length() != 0) {
                    subInnerOneJSON = innerJSONArray.getJSONObject(0);
                    if (subInnerOneJSON != null) {
                        if (subInnerOneJSON.has(("id"))) {
                            youTubeChannelInfo.setChannelId(subInnerOneJSON.getString("id"));
                        }
                        if (subInnerOneJSON.has(("snippet"))) {
                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("snippet");
                        }

                        if (subInnerTwoJSON != null) {
                            if (subInnerTwoJSON.has("title")) {
                                youTubeChannelInfo.setTitle(subInnerTwoJSON.getString("title"));
                            }
                            if (subInnerTwoJSON.has("description")) {
                                youTubeChannelInfo.setDescription(subInnerTwoJSON.getString("description"));
                            }
                            if (subInnerTwoJSON.has("publishedAt")) {
                                youTubeChannelInfo.setPublishedAt(subInnerTwoJSON.getString("publishedAt"));
                            }
                            if (subInnerTwoJSON.has("country")) {
                                youTubeChannelInfo.setCountry(subInnerTwoJSON.getString("country"));
                            }
                            if (subInnerTwoJSON.has("thumbnails")) {
                                subInnerThreeJSON = subInnerTwoJSON.getJSONObject("thumbnails");
                            }
                        }
                        if (subInnerThreeJSON != null && subInnerThreeJSON.has("medium")) {
                            subInnerFourJSON = subInnerThreeJSON.getJSONObject("medium");
                        }
                        if (subInnerFourJSON != null && subInnerFourJSON.has("url")) {
                            youTubeChannelInfo.setThumbnailsURL(subInnerFourJSON.getString("url"));
                        }

                        if (subInnerOneJSON.has(("statistics"))) {
                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("statistics");
                        }
                        if (subInnerTwoJSON != null) {
                            if (subInnerTwoJSON.has("viewCount")) {
                                youTubeChannelInfo.setVideosViewsCount(subInnerTwoJSON.getInt("viewCount"));
                            }
                            if (subInnerTwoJSON.has("commentCount")) {
                                youTubeChannelInfo.setVideosCommentsCount(subInnerTwoJSON.getInt("commentCount"));
                            }
                            if (subInnerTwoJSON.has("subscriberCount")) {
                                youTubeChannelInfo.setSubscribersCount(subInnerTwoJSON.getInt("subscriberCount"));
                            }
                            if (subInnerTwoJSON.has("videoCount")) {
                                youTubeChannelInfo.setVideosCount(subInnerTwoJSON.getInt("videoCount"));
                            }
                        }
                        youTubeChannelInfo.setAnalysisDate(LocalDateTime.now());
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getYouTubeChannelOverview cause:: " + e);
            }

            List<ChannelVideoInfo> videosList = null;
            if (youTubeChannelInfo != null && youTubeChannelInfo.getVideosCount() > 0) {
//            videosList = getChannelVideoInfoList(youTubeChannelInfo.getChannelId(), youTubeChannelInfo.getVideosCount());
                videosList = getChannelVideoInfoList(youTubeChannelInfo.getChannelId(), 1);
                if (videosList != null) {
                    youTubeChannelInfo.setVideoInfoList(videosList);
                }
            }
        }

        LOGGER.info("************************ Compelted **************************");
        LOGGER.info("youTubeChannelInfo:: " + youTubeChannelInfo);
        return youTubeChannelInfo;
    }

    public List<ChannelVideoInfo> getChannelVideoInfoList(String channelId, int numberOfVideos) {
        List<ChannelVideoInfo> videosList = new ArrayList<>();
        List<ChannelVideoInfo> pageWiseVideosList = null;
        JSONObject responseJSON = null;
        JSONArray innerJSONArray = null;
        int loopCount = 0;
        String nextPageToken = null;
        String apiEndpointURL = null;
        while (((videosList.size() != numberOfVideos) && (videosList.size() <= numberOfVideos))) {
            /*1st iteration*/
            if (videosList.isEmpty() && nextPageToken == null) {
                responseJSON = getJSONFromAPIResponse(getAPIEndPointURL("videosList", channelId, null));
            } else if (nextPageToken != null) {
                /*From 2nd upto end pages iteration*/
                apiEndpointURL = getAPIEndPointURL("nextPageToken", channelId, nextPageToken);
                if (apiEndpointURL != null) {
                    responseJSON = getJSONFromAPIResponse(apiEndpointURL);
                }
            }
            if (responseJSON != null) {
                try {
                    if (responseJSON.has("nextPageToken")) {
                        nextPageToken = responseJSON.getString("nextPageToken");
                    } else {
                        nextPageToken = null;
                    }
                    if (responseJSON.has("items")) {
                        innerJSONArray = responseJSON.getJSONArray("items");
                        pageWiseVideosList = getPaginationWiseVideosList(innerJSONArray);
                        videosList.addAll(pageWiseVideosList);
                    }
                } catch (JSONException e) {
                    System.out.println("JSONException in getChannelVideoInfoList cause:: " + e);
                }

            }
            ++loopCount;
            LOGGER.info("==================================================");
            LOGGER.info("Number of loop is::" + loopCount + " and size of collected videos is::" + videosList.size());
            LOGGER.info("==================================================");
            /*Exit if next page is not identified (approx may be last page) from loop explictily*/
            if (!videosList.isEmpty() && nextPageToken == null) {
                break;
            }
        }
        return videosList;
    }

    public List<ChannelVideoInfo> getPaginationWiseVideosList(JSONArray responseArray) {
        List<ChannelVideoInfo> videosList = new ArrayList<>();
        String videoId = null;
        ChannelVideoInfo channelVideoInfo = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        if (responseArray != null) {
            for (int count = 0; count < responseArray.length(); count++) {
                try {
                    subInnerOneJSON = responseArray.getJSONObject(count);
                    if (subInnerOneJSON != null && subInnerOneJSON.has("id")) {
                        subInnerTwoJSON = subInnerOneJSON.getJSONObject("id");
                        if (subInnerTwoJSON != null && subInnerTwoJSON.has("videoId")) {
                            videoId = subInnerTwoJSON.getString("videoId");
                            if (videoId != null) {
                                channelVideoInfo = getChannelVideoInfo(videoId);
                                if (channelVideoInfo != null) {
                                    videosList.add(channelVideoInfo);
                                    System.out.println("==================================================");
                                    System.out.println("sub list video size::" + videosList.size());
                                    System.out.println("==================================================");
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                   LOGGER.error("JSONException in getPaginationWiseVideosList cause:: " + e);
                }
            }
        }
        return videosList;
    }

    public ChannelVideoInfo getChannelVideoInfo(String videoId) {
        ChannelVideoInfo videoInfo = null;
        JSONObject responseJSON = getJSONFromAPIResponse(getAPIEndPointURL("videoInfo", videoId, null));
        if (responseJSON != null && responseJSON.has("items")) {
            JSONObject subInnerOneJSON = null;
            JSONObject subInnerTwoJSON = null;
            JSONObject subInnerThreeJSON = null;
            JSONObject subInnerFourJSON = null;
            JSONArray innerJSONArray = null;
            JSONArray subInnerJSONArray = null;
            try {
                innerJSONArray = responseJSON.getJSONArray("items");
                if (innerJSONArray != null && innerJSONArray.length() != 0) {
                    subInnerOneJSON = innerJSONArray.getJSONObject(0);
                    if (subInnerOneJSON != null) {
                        if (subInnerOneJSON.has(("snippet"))) {
                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("snippet");
                        }
                        if (subInnerTwoJSON != null) {
                            videoInfo = new ChannelVideoInfo();
                            if (subInnerOneJSON.has(("id"))) {
                                videoInfo.setVideoId(subInnerOneJSON.getString("id"));
                                videoInfo.setVideoURL(messageSource.getMessage("YouTube.API.VideoHostingURL", null, Locale.US).concat(subInnerOneJSON.getString("id")));
                            }

                            if (subInnerTwoJSON.has("publishedAt")) {
                                videoInfo.setVideoPublishedAt(subInnerTwoJSON.getString("publishedAt"));
                            }

                            if (subInnerTwoJSON.has("title")) {
                                videoInfo.setVideoTitle(subInnerTwoJSON.getString("title"));
                            }
                            if (subInnerTwoJSON.has("description")) {
                                videoInfo.setVideoDescription(subInnerTwoJSON.getString("description"));
                            }
                            if (subInnerTwoJSON.has("thumbnails")) {
                                subInnerThreeJSON = subInnerTwoJSON.getJSONObject("thumbnails");
                            }
                            if (subInnerThreeJSON != null && subInnerThreeJSON.has("medium")) {
                                subInnerFourJSON = subInnerThreeJSON.getJSONObject("medium");
                            }
                            if (subInnerFourJSON != null && subInnerFourJSON.has("url")) {
                                videoInfo.setVideoThumbnailURL(subInnerFourJSON.getString("url"));
                            }
                            /*EOF  Thubnail URL*/
                            if (subInnerTwoJSON.has("tags")) {
                                subInnerJSONArray = subInnerTwoJSON.getJSONArray("tags");
                                if (subInnerJSONArray != null) {
                                    String tag = null;
                                    List<String> tagsList = new ArrayList<>();
                                    for (int cout = 0; cout < subInnerJSONArray.length(); cout++) {
                                        tag = subInnerJSONArray.getString(cout);
                                        if (tag != null) {
                                            tagsList.add(tag);
                                        }
                                    }
                                    videoInfo.setTags(tagsList);
                                }
                            }
                            /*EOF Videos tags*/
                        }

                        if (subInnerOneJSON.has(("statistics"))) {
                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("statistics");
                        }
                        if (subInnerTwoJSON != null && videoInfo != null) {
                            if (subInnerTwoJSON.has("viewCount")) {
                                videoInfo.setViewsCount(subInnerTwoJSON.getInt("viewCount"));
                            }
                            if (subInnerTwoJSON.has("commentCount")) {
                                videoInfo.setCommentsCount(subInnerTwoJSON.getInt("commentCount"));
                            }
                            if (subInnerTwoJSON.has("likeCount")) {
                                videoInfo.setLikesCount(subInnerTwoJSON.getInt("likeCount"));
                            }
                            if (subInnerTwoJSON.has("dislikeCount")) {
                                videoInfo.setDislikesCount(subInnerTwoJSON.getInt("dislikeCount"));
                            }
                            if (subInnerTwoJSON.has("favoriteCount")) {
                                videoInfo.setFavoriteCount(subInnerTwoJSON.getInt("favoriteCount"));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("inner json:: " + innerJSONArray);
                LOGGER.error("JSONException in getChannelVideoInfo cause:: " + e);
            }
        }
        return videoInfo;
    }

    public String getAPIEndPointURL(String resourceType, String resourceId, String pageToken) {
        String apiEndPointURL = null;
        switch (resourceType) {
            case "channelId":
                apiEndPointURL = apiEndpointVersionURL + "/channels?part=contentDetails%2Csnippet%2Cstatistics&forUsername=" + resourceId + "&fields=etag%2CeventId%2Citems%2Ckind%2CnextPageToken%2CpageInfo%2CprevPageToken%2CtokenPagination%2CvisitorId&key=" + APIKeys.googleKey;
                break;
            case "videosList":
                apiEndPointURL = apiEndpointVersionURL + "/search?part=id&channelId=" + resourceId + "&order=date&type=video&maxResults=2&key=" + APIKeys.googleKey;
                break;
            case "nextPageToken":
                if (pageToken != null) {
                    apiEndPointURL = apiEndpointVersionURL + "/search?part=id%2Csnippet&channelId=UCg3Y4MPb-uK94D0iEc2u1_A&order=date&type=video&maxResults=8&pageToken=" + pageToken + "&key=" + APIKeys.googleKey;
                }
                break;
            case "videoInfo":
                apiEndPointURL = apiEndpointVersionURL + "/videos?part=snippet%2Cstatistics&id=" + resourceId + "&key=" + APIKeys.googleKey;
                break;
        }

        LOGGER.debug("resourceType::" + resourceType + ", apiEndPointURL:: " + apiEndPointURL + ", pageToken:: " + pageToken);
        return apiEndPointURL;
    }
}