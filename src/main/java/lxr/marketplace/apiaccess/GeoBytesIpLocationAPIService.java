package lxr.marketplace.apiaccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import lxr.marketplace.apiaccess.lxrmbeans.GeoBytesIpInfo;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class GeoBytesIpLocationAPIService {
	private static Logger logger = Logger.getLogger(GeoBytesIpLocationAPIService.class);
	private String geoBytesAPIKey;

	public void setGeoBytesAPIKey(String geoBytesAPIKey) {
		this.geoBytesAPIKey = geoBytesAPIKey;
	}

	String getResultGeoBytes(String ip) throws IOException{
		URL link = new URL("http://www.geobytes.com/IpLocator.htm?GetLocation&template=php3.txt&IpAddress="+ip);

		BufferedReader in = new BufferedReader(new InputStreamReader(link.openStream()));
		String inputLine;
		String wholeText = "";
		while ((inputLine = in.readLine()) != null){
			wholeText += inputLine;             
		}

		in.close();
		return wholeText;
	}
	
	public String getCountryFromGeoBytes(String ip) throws InterruptedException{
		String link = "http://www.geobytes.com/IpLocator.htm?GetLocation&template=php3.txt&IpAddress="+ip;
		int timeout = 60000;
		String userAgent ="Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
				/*
				 * Modified on oct-7 2015
				 * "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
		long timeoutIteration = 0;
		long noOfTime = 10;
		int sllepOut = 5000;
		Connection jsoupCon = null;
		Document document = null;
		do {
			timeoutIteration += 1;
			try {
				jsoupCon = Jsoup.connect(link);
				jsoupCon.timeout(timeout);
				jsoupCon.userAgent(userAgent);
				document = jsoupCon.get();
				break;
			} catch (IOException ex) {
				System.out.println("Exception to get Connection 1 at iteration " + timeoutIteration);
				Thread.sleep(sllepOut * timeoutIteration);
			}
		} while (timeoutIteration < noOfTime);
		
		Elements country = document.getElementsByAttributeValueMatching("name", "country");
		String country1 = country.get(1).attr("Content");
		return country1;
	}
	
	public GeoBytesIpInfo getCountryFromIpinfodb(String ip, GeoBytesIpInfo ipInfo) throws InterruptedException, IOException, ParseException{
		
		String urlString = "http://api.ipinfodb.com/v3/ip-city/?key="+geoBytesAPIKey+"&format=json&ip="+ip;
		URL url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
		StringBuilder responseStrBuilder = new StringBuilder();
		String inputStr;
		while ((inputStr = br.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		JSONObject jsonObject;
	
		try {
			jsonObject = new JSONObject(responseStrBuilder.toString());
			ipInfo.setCountry(jsonObject.getString("countryName"));
			ipInfo.setRegion(jsonObject.getString("regionName"));
			ipInfo.setCity(jsonObject.getString("cityName"));
			ipInfo.setCountryCode(jsonObject.getString("countryCode"));
			logger.info("***************"+ipInfo.getIp()+"\t"+ipInfo.getCountry());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ipInfo;
	}
	
	public List<String> readFile() throws IOException{
		List<String> ips = new ArrayList<String>(); 
		String filePath = "/home/netelixir/Desktop/lxrmuserips.txt";
		BufferedReader br = new BufferedReader(new FileReader(filePath));
	    try {
	        String line = br.readLine();

	        while (line != null) {
	        	ips.add(line);
	            line = br.readLine();
	        }
	    } catch(Exception e){ 
	    	e.printStackTrace();
	    }finally {
	        br.close();
	    }
	    return ips;
	}
	
	public void createCsv(List<GeoBytesIpInfo> ipInfos) throws IOException{
		String header = "\"IP\",\"Country\",\"City\",\"Region\",\"Country Code\"\n";
		File file = new File("/home/netelixir/Desktop/iptoCountry.csv");
		Writer writer = null;
		try {
			OutputStream out = new FileOutputStream(file);
			writer = new OutputStreamWriter(out, Charset.forName("UTF-8"));
			if (!file.isFile()) {
				writer.write('\uFEFF');
			}
			writer.write(header);
			for(GeoBytesIpInfo ipInfo : ipInfos){
				String data = "\""+ipInfo.getIp()+"\",\""+ipInfo.getCountry()+"\",\""+ipInfo.getCity()+"\",\""
			+ipInfo.getRegion()+"\",\""+ipInfo.getCountryCode()+"\"\n";
				writer.write(data);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			writer.close();
		}
	}
	
}
