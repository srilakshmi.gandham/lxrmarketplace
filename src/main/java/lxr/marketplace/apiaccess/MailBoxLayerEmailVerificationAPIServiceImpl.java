/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.apiaccess;

import java.time.LocalDateTime;
import lxr.marketplace.user.EmailValidationModel;
import lxr.marketplace.user.EmailVerfication;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class MailBoxLayerEmailVerificationAPIServiceImpl implements EmailVerfication {

    private static final Logger LOGGER = Logger.getLogger(MailBoxLayerEmailVerificationAPIServiceImpl.class);

    @Value("${MailBox.EmailVerification.apiURLPreFix}")
    private String mailBoxLayerAPIURL;
    @Value("${MailBox.EmailVerification.apiURLPostFix}")
    private String mailBoxLayerAPIURLPostFix;
    @Value("${MailBox.EmailVerification.apiKey}")
    private String mailBoxLayerAPIKey;
    @Value("${EmailVerification.apiScoreFilter}")
    private double emailValidationScoreFilter;

    @Override
    public EmailValidationModel validateAndGenerateScoreForEmail(String emaiToBeVerified) {
        EmailValidationModel emailValidationModel = null;
        boolean validEmail = Boolean.FALSE;
        boolean isDataPulled = Boolean.FALSE;
        JSONObject jsonObject = null;
        try {
            String apiEndPointURL = getEmailVerficationAPIEndPointURL(emaiToBeVerified);
            jsonObject = Common.getJSONFromAPIResponseV2(apiEndPointURL);
            /*Checking for required data presences*/
            isDataPulled = validateAPIResponse(jsonObject, "email", "score", emaiToBeVerified, apiEndPointURL);
            if (isDataPulled) {
                emailValidationModel = new EmailValidationModel();
                /*Mandatory to check all three key & values from api response
                Finalize the result when all three cases is true*/
                if (jsonObject.has("mx_found")) {
                    validEmail = jsonObject.getBoolean("mx_found");
                }
                if (jsonObject.has("smtp_check")) {
                    validEmail = jsonObject.getBoolean("smtp_check");
                }
                if (jsonObject.has("email")) {
                    validEmail = emaiToBeVerified.trim().equals(jsonObject.getString("email").trim());
                    emailValidationModel.setEmailToBeVerified(jsonObject.getString("email").trim());
                }
                if (jsonObject.has("score")) {
                    validEmail = (((Double) jsonObject.getDouble("score")) >= emailValidationScoreFilter);
                    emailValidationModel.setEmailVerificationScore(jsonObject.getDouble("score"));
                }
                if (jsonObject.has("disposable")) {
                    emailValidationModel.setDisposable(jsonObject.getBoolean("disposable"));
                    if (emailValidationModel.isDisposable()) {
                        LOGGER.info("The email to be verified :: " + emaiToBeVerified + ", is disposable");
                    }
                }
                emailValidationModel.setEmailVerified(validEmail);
                emailValidationModel.setCreatedDate(LocalDateTime.now());
                if (!validEmail) {
                    LOGGER.info("Email verification is failed, api response:: " + jsonObject);
                }
            }
        } catch (JSONException e) {
            emailValidationModel = null;
            LOGGER.error("JSON | IO Exception in validateAndGenerateScoreForEmail for responsebody: " + jsonObject + ", cause:: " + e.getMessage());
        } catch (Exception e) {
            emailValidationModel = null;
            LOGGER.error("Exception in validateAndGenerateScoreForEmail for responsebody: " + jsonObject + ", cause:: " + e.getMessage());
        }
        LOGGER.info("Validating email using api for email:: " + emaiToBeVerified + ", is compelted and status:: " + (emailValidationModel != null ? emailValidationModel.isEmailVerified() : " UNKNOWN"));
        return emailValidationModel;
    }

    /*
    API request, success, failure cases
    Success case::
            { "email":"lxrmarketplaceseotool@gmail.com","did_you_mean":"","user":"lxrmarketplaceseotool","domain":"gmail.com",
          "format_valid":true,"mx_found":true,"smtp_check":true,"catch_all":null,"role":false,"disposable":false,
          "free":true,"score":0.8 }
   Error case:: 
        {"success": false,"error": {
        "code": 210,
        "type": "no_email_address_supplied",
        "info": "Please specify an email address. [Example: support@apilayer.com]" }}

     */
    private boolean validateAPIResponse(JSONObject responseJSON, String parentKey, String parentKey2, String emailToBeVerfyied, String endPointURL) {
        boolean isValidResponse = Boolean.FALSE;
        if ((responseJSON != null && responseJSON.has(parentKey) && responseJSON.has(parentKey2))) {
            isValidResponse = Boolean.TRUE;
        } else if (responseJSON != null) {
            LOGGER.info("Parent key is not found and response :: " + responseJSON.toString().toLowerCase() + ",  for email :: " + emailToBeVerfyied + ", isValidResponse:: " + isValidResponse);
            APIError errorObj = new APIError();
            errorObj.setApiName("Mailboxlayer Email verification API");
            errorObj.setToolId(0);
            errorObj.setErrorMessage(responseJSON.toString() + ",  endpoint URL:: " + endPointURL);
            APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, "LXRM Email Verification API", Boolean.TRUE);
        }
        return isValidResponse;
    }

    public String getEmailVerficationAPIEndPointURL(String emailToBeVerified) {
        LOGGER.info("Request to validate email using api for email:: " + emailToBeVerified);
        String endpointURL = null;
        endpointURL = mailBoxLayerAPIURL.concat(mailBoxLayerAPIKey).concat("&email=").concat(emailToBeVerified).concat(mailBoxLayerAPIURLPostFix);
        return endpointURL;
    }

}
