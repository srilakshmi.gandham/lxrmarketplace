package lxr.marketplace.apiaccess;

import org.json.JSONException;
import org.json.JSONObject;

import lxr.marketplace.util.Common;

public class TwitterAPIService {

	String apiURL = "http://cdn.api.twitter.com/1";

	public long fetchURLTweets(String url){
		String queryURL = apiURL + "/urls/count.json?url="+url;
		JSONObject twitterResponse = Common.createJsonObjectFromURL(queryURL);
		//Sample Response: {"count":528,"url":"http://stylehatch.co/"}
		if(twitterResponse != null){
			try {
				return Long.parseLong(twitterResponse.getString("count"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}	
}
