package lxr.marketplace.apiaccess;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;

import lxr.marketplace.apiaccess.lxrmbeans.PinterestMetrics;
import lxr.marketplace.util.Common;
import lxr.marketplace.socialmedia.APIKeys;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PinterestMetricsAPIService implements Runnable {

    private static Logger logger = Logger.getLogger(PinterestMetricsAPIService.class);

    private String pinterestUser;
    private PinterestMetrics pinterestMetrics;
    private ConcurrentHashMap<Integer, String> pinterestMap;

    public PinterestMetricsAPIService() {
    }

    public PinterestMetricsAPIService(String pinterestUser, ConcurrentHashMap<Integer, String> pinterestMap) {
        this.pinterestUser = pinterestUser;
        this.pinterestMap = pinterestMap;
    }

    public String getPinterestUser() {
        return pinterestUser;
    }

    public void setPinterestUser(String pinterestUser) {
        this.pinterestUser = pinterestUser;
    }

    public PinterestMetrics getPinterestMetrics() {
        return pinterestMetrics;
    }

    public void setPinterestMetrics(PinterestMetrics pinterestMetrics) {
        this.pinterestMetrics = pinterestMetrics;
    }

    public ConcurrentHashMap<Integer, String> getPinterestMap() {
        return pinterestMap;
    }

    public void setPinterestMap(ConcurrentHashMap<Integer, String> pinterestMap) {
        this.pinterestMap = pinterestMap;
    }

    @Override
    public void run() {
        try {
            setPinterestMetrics(getPinterestUSerData(pinterestUser));
        } catch (Exception e) {
            setPinterestMetrics(new PinterestMetrics());
            logger.error("Exception for Pinterest Metrics API Service: ", e);
        }
        pinterestMap.put(6, "Pinterest");
    }

    public static boolean checkPinterestUserExitsence(String userName) {
        JSONObject pinterestJson = Common.getResponseFromAPI(getSearchUserIdUrl(userName));
        /*Valid data resposne format of api:
            {"data":{"counts":{"followers":11441,"following":100,"boards":54,"pins":1628},"id":"61783963546420821","first_name":"PartyLite ","username":"partylite"}}*/

 /*if (pinterestJson != null) {
            logger.debug("pinterestJson: " + pinterestJson);
            JSONObject pinterestdataJson = Common.checkJSONObjectInJSONObjectResponse(pinterestJson, "data");
            if (pinterestdataJson != null) {
                return true;
            }
        }*/
        return pinterestJson != null ? pinterestJson.has("data") : Boolean.FALSE;

        /*Disabling the send mail when api status down
        if (pinterestJson != null) {
            Sample format of error:
            status	"failure"
            message	"Authorization failed."
            code	3
            data	null
            try {
//                if (pinterestJson.has("message") && pinterestJson.has("status")) {
                if ((pinterestJson.has("status")
                        || pinterestJson.has("code") || (pinterestJson.has("message") && pinterestJson.getString("message").contains("Authorization failed")))
                        && (pinterestJson.has("message") && !pinterestJson.getString("message").contains("User not found"))) {
                    logger.error("Error from Pinterest API, Response will be updated through mail : " + pinterestJson.toString());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        logger.error("InterruptedException in checkPinterestUserExitsence cause: " + ex.getMessage());
                    }
                    APIError errorObj = new APIError();
                    errorObj.setApiName(APIErrorHandlingService.PINTEREST);
                    errorObj.setToolId(29);
                    errorObj.setErrorMessage(pinterestJson.toString());
                    APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER, Boolean.TRUE);
                }
            } catch (JSONException e) {
                logger.error("Exception in checkPinterestUserExitsence checking for errors", e);
            }
        }*/
    }

    public PinterestMetrics getPinterestUSerData(String userName) {
        pinterestMetrics = new PinterestMetrics();
        if (!(pinterestUser.equals("NDA"))) {
            try {
                JSONObject pinterestdataJson;
                JSONObject postDataJson;
                JSONObject counts;
                pinterestdataJson = getPinterestProfile(userName);
                if (pinterestdataJson != null) {
                    pinterestMetrics.setPinterestUserName((String) pinterestdataJson.get("id"));
                    counts = pinterestdataJson.getJSONObject("counts");
                    if (counts != null) {
                        if (counts.get("pins") != null) {
                            pinterestMetrics.setPins((int) (Integer) counts.get("pins"));
                        }
                        if (counts.get("following") != null) {
                            pinterestMetrics.setFollowing((int) (Integer) counts.get("following"));
                        }
                        if (counts.get("followers") != null) {
                            pinterestMetrics.setFollowers((int) (Integer) counts.get("followers"));
                        }
                        if (counts.get("boards") != null) {
                            pinterestMetrics.setBoards((int) (Integer) counts.get("boards"));
                        }
                        /*if (counts.get("likes") != null) {
                            pinterestMetrics.setLikes((int) (Integer) counts.get("likes"));
                        }*/
                    }
                    pinterestMetrics.setPinterestAccountStatus(true);
                }
                postDataJson = getRecentPostMetrics(userName);
                String tempPostUrl = null;
                if (postDataJson != null) {
                    if (postDataJson.get("created_at") != null) {
                        pinterestMetrics.setPostDate(getPinterestPostTime((String) postDataJson.get("created_at")));
                    }
                    if (postDataJson.get("note") != null) {
                        pinterestMetrics.setPostText((String) postDataJson.get("note"));
                    }
                    pinterestMetrics.setPostRepins((int) getRecentPostRepin(userName));
                    if (postDataJson.get("counts") != null) {
                        counts = Common.checkJSONObjectInJSONObjectResponse(postDataJson, "counts");
                        if (counts != null) {
                            if (counts.get("comments") != null) {
                                pinterestMetrics.setPostComments((int) (Integer) counts.get("comments"));
                            }
                            /*Metric Like is renamed as Save from API JUN 30, 2017
                            Ref: https://blog.pinterest.com/en/goodbye-button*/
                            if (counts.get("saves") != null) {
                                pinterestMetrics.setPostLikes((int) (Integer) counts.get("saves"));
                            }
                            pinterestMetrics.setEngagement(pinterestMetrics.getPostComments() + pinterestMetrics.getPostLikes() + pinterestMetrics.getPostRepins());
                            pinterestMetrics.setAudienceEngaged(Common.getAudienceEngaged(pinterestMetrics.getEngagement(), pinterestMetrics.getFollowers()));
                        }
                    }
                    if (postDataJson.get("image") != null) {
                        tempPostUrl = fetchURlfromJSONObject((JSONObject) postDataJson.get("image"));
                        if (tempPostUrl != null) {
                            String postid = (String) postDataJson.get("id");
                            String pinUserName = userName;
                            if (userName.length() > 4) {
                                pinUserName = pinUserName.substring(2, pinUserName.length() - 2);
                            }
                            postid = pinUserName + postid + "PinterestPost";
                            String writePath = "/disk2/lxrmarketplace/social_images/" + postid + "." + Common.getImageFormat(tempPostUrl);
                            if ((Common.resizeImageFromUrlandSave(tempPostUrl, writePath))) {
                                pinterestMetrics.setPinterestImage(true);
                                pinterestMetrics.setPinterestImageUrl("/social_images/" + postid + "." + Common.getImageFormat(tempPostUrl));
                            }
                        }
                    }

                }
                return pinterestMetrics;
            } catch (JSONException e) {
                logger.error("Exception for getting pinterest Profile data of a user: ", e);
            }
        }
        return pinterestMetrics;
    }

    /*To get below metrics for query profile .
    pins, following, followers, boards*/
    public static JSONObject getPinterestProfile(String userName) {
        JSONObject pinterestdataJson = null;
        try {
            JSONObject pinterestJson = Common.getResponseFromAPI(getSearchUserIdUrl(userName));
            if (pinterestJson != null && pinterestJson.has("data")) {
                pinterestdataJson = pinterestJson.getJSONObject("data");
            }
            /*Disabling the send mail when api status down
            if (pinterestJson != null) {
                if ((pinterestJson.has("status")
                        || pinterestJson.has("code") || (pinterestJson.has("message") && pinterestJson.getString("message").contains("Authorization failed")))
                        && (pinterestJson.has("message") && !pinterestJson.getString("message").contains("User not found"))) {
                    logger.error("Error from Pinterest API, Response will be updated through mail");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        logger.error("InterruptedException in checkPinterestUserExitsence cause: " + ex.getMessage());
                    }
                    APIError errorObj = new APIError();
                    errorObj.setApiName(APIErrorHandlingService.PINTEREST);
                    errorObj.setToolId(29);
                    errorObj.setErrorMessage(pinterestJson.toString());
                    APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER,Boolean.TRUE);
                }
            }*/
        } catch (JSONException e) {
            logger.error("JSONException for getPinterestProfile: ", e);
        }
        return pinterestdataJson;
    }

    /*To get below metrics for recent post.
    comments, save, created date, Image URL of recent post. */
    public static JSONObject getRecentPostMetrics(String userName) {
        JSONObject postIDJson = null, postDataJson = null, subPostJSON = null, subPins = null;
        JSONArray pinsArray = null;
        String mediaId = null;
        try {
            postIDJson = Common.getResponseFromAPI(getRecentMediaIdUrl(userName));
            /*Note:postIDJson response will not inlucde recent post commnet,created time metrics,
                So constructing  seprate  url to get metrics based on recent post Id.
             */
            if (postIDJson != null) {
                postIDJson = Common.checkJSONObjectInJSONObjectResponse(postIDJson, "data");
                if (postIDJson.get("pins") != null) {
                    pinsArray = postIDJson.getJSONArray("pins");
                }
                if (pinsArray != null) {
                    subPins = pinsArray.getJSONObject(0);
                }
                if (subPins != null) {
                    mediaId = (String) subPins.get("id");
                }
                if (mediaId != null) {
                    postDataJson = Common.getResponseFromAPI(getRecentMediaUrl(mediaId));
                    subPostJSON = Common.checkJSONObjectInJSONObjectResponse(postDataJson, "data");
                }
            }
        } catch (JSONException e) {
            logger.error("JSONException for getRecentPostJSON: ", e);
        }
        return subPostJSON;
    }

    /*To get repin count metric for recent post.*/
    public static Integer getRecentPostRepin(String userName) {
        JSONObject postIDJson = null, subPins = null;
        JSONArray pinsArray = null;
        Integer repinCount = 0;
        try {
            postIDJson = Common.getResponseFromAPI(getRecentMediaIdUrl(userName));
            /*Note:postIDJson response will inlucde post Id, repin count.
             */
            if (postIDJson != null) {
                postIDJson = Common.checkJSONObjectInJSONObjectResponse(postIDJson, "data");
                if (postIDJson.get("pins") != null) {
                    pinsArray = postIDJson.getJSONArray("pins");
                }
                if (pinsArray != null) {
                    subPins = pinsArray.getJSONObject(0);
                }
                if (subPins != null) {
                    repinCount = (Integer) subPins.get("repin_count");
                }
            }
        } catch (JSONException e) {
            logger.error("JSONException for getRecentPostRepin: ", e);
        }
        return repinCount;
    }

    /*Convert recent post date from API format into string */
    public static String getPinterestPostTime(String Posttime) {
        String formattedDate = null;
        try {
            Calendar cal = javax.xml.bind.DatatypeConverter.parseDateTime(Posttime);
            formattedDate = new SimpleDateFormat("MMMM dd, yyyy").format(cal.getTime());
        } catch (Exception e) {
            logger.error("Exception for Pinterest recent post time: ", e);
        }
        return formattedDate;
    }

    public static String fetchURlfromJSONObject(JSONObject imageJson) {
        String postImageURL = null;
        try {
            JSONObject original = Common.checkJSONObjectInJSONObjectResponse(imageJson, "original");
            if (original != null && original.get("url") != null) {
                postImageURL = (String) original.get("url");
            }
        } catch (JSONException e) {
            logger.error("JSONException for fetchURlfromJSONObject: ", e);
        }
        return postImageURL;
    }

    public static String getSearchUserIdUrl(String userName) {
        logger.debug("Request URL in getSearchUserIdUrl: " + "https://api.pinterest.com/v1/users/" + userName + "/?access_token=" + APIKeys.pinterestAccessToken + "&fields=first_name,id,username,counts");
        return "https://api.pinterest.com/v1/users/" + userName + "/?access_token=" + APIKeys.pinterestAccessToken + "&fields=first_name,id,username,counts";
    }

    public static String getRecentMediaIdUrl(String userName) {
        logger.debug("Request URL in getRecentMediaIdUrl: " + "http://widgets.pinterest.com/v3/pidgets/users/" + userName + "/pins/");
        return "http://widgets.pinterest.com/v3/pidgets/users/" + userName + "/pins/";
    }

    public static String getRecentMediaUrl(String postID) {
        logger.debug("Request URL in getRecentMediaUrl: " + "https://api.pinterest.com/v1/pins/" + postID + "/?access_token=" + APIKeys.pinterestAccessToken + "&fields=id%2Clink%2Cnote%2Curl%2Cimage%2Cmedia%2Cmetadata%2Ccounts%2Coriginal_link%2Ccreated_at");
        return "https://api.pinterest.com/v1/pins/" + postID + "/?access_token=" + APIKeys.pinterestAccessToken + "&fields=id%2Clink%2Cnote%2Curl%2Cimage%2Cmedia%2Cmetadata%2Ccounts%2Coriginal_link%2Ccreated_at";
    }

    /*Returns followers of pinterest page
                (OR) 
    Sends the exception message to support mail
     */
    public long checkPinterestAPIStatus(String userName) {
        long pageFollowers = 0;
        JSONObject pinterestdataJson = null;
        JSONObject counts = null;
        JSONObject pinterestJson = null;
        if (userName != null) {
            pinterestJson = Common.getResponseFromAPI(getSearchUserIdUrl(userName));
            try {
                if (pinterestJson != null && pinterestJson.has("data")) {
                    pinterestdataJson = pinterestJson.getJSONObject("data");
                }
                if (pinterestdataJson != null && pinterestdataJson.has("counts")) {
                    counts = pinterestdataJson.getJSONObject("counts");
                }
                if (counts != null && counts.has("followers")) {
                    pageFollowers = counts.getLong("followers");
                }
                /*Looking for error message and sends mail api status with exception*/
                if (counts == null || pageFollowers == 0) {
                    logger.error("Error from Pinterest API, Response will be updated through mail");
                    APIError errorObj = new APIError();
                    errorObj.setApiName(APIErrorHandlingService.PINTEREST);
                    errorObj.setToolId(29);
                    errorObj.setErrorMessage("Dialy alert error message:: "+(pinterestJson != null ? pinterestJson.toString() : ("No JSON resposne found for Pinterest API for userName " + userName)));
                    APIErrorHandlingService.validateAndHandleAPIErrors(errorObj, Common.SOCIAL_MEDIA_ANALYZER, Boolean.FALSE);
                }
            } catch (JSONException e) {
                logger.error("JSONException in checkPinterestAPIStatus causee:: ", e);
            }
        }
        return pageFollowers;
    }
}