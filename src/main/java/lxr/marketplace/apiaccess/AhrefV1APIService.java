package lxr.marketplace.apiaccess;
import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class AhrefV1APIService {
	private static Logger logger = Logger.getLogger(AhrefV1APIService.class);
	private String ahrefAPIKey;
	private String ahrefAPIUrl;


	public void setAhrefAPIKey(String ahrefAPIKey) {
		this.ahrefAPIKey = ahrefAPIKey;
	}

	public void setAhrefAPIUrl(String ahrefAPIUrl) {
		this.ahrefAPIUrl = ahrefAPIUrl;
	}
         //TODO : Method not in use
        
/*	public String getFirst100BackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, int offset){
		String queryUrl = ahrefAPIUrl + "get_backlinks.php?target="+domainName+"&count=100&mode=subdomains" +
				"&output=json&offset="+offset+"&AhrefsKey=" + ahrefAPIKey;
		logger.info("Fetching Backlink data for url: "+domainName +"\nAhref Backlink query: " + queryUrl);
		JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
		if(jsonObject != null){
			String error = parseAhrefJson(jsonObject, backLinks);
			return error;
		}else{
			return "No Data";
		}
	}
*/	
        //TODO : Method not in use
        
/*	public String getBackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, int offset, int count){
		String queryUrl = ahrefAPIUrl + "get_backlinks.php?target="+domainName+"&count="+count+"&mode=subdomains" +
				"&output=json&offset="+offset+"&AhrefsKey=" + ahrefAPIKey;
		logger.info("Fetching Backlink data for url: "+domainName +"\nAhref Backlink query: " + queryUrl);
		JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
		if(jsonObject != null){
			String error = parseAhrefJson(jsonObject, backLinks);
			return error;
		}else{
			return "No Data";
		}
	}*/
	
//	public String getAllBackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, int offset){
//		long countBacklinks = fetchNumberOfBacklinks(domainName);
//		if(countBacklinks != 0){
//			String queryUrl = ahrefAPIUrl + "get_backlinks.php?target="+domainName+"&count="+countBacklinks+"&mode=subdomains" +
//					"&output=json&offset="+offset+"&AhrefsKey=" + ahrefAPIKey;
//			logger.info("Fetching Backlink data for url: "+domainName +"\nAhref Backlink query: " + queryUrl);
//			JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
//			if(jsonObject != null){
//				String error = parseAhrefJson(jsonObject, backLinks);
//				return error;
//			}else{
//				return "No Data";
//			}
//		}else{
//			return "No Data";
//		}
//	}
	
         //TODO : Method not in use
        
/*	private String parseAhrefJson(JSONObject jsonObject, ArrayList<AhrefBacklink> backLinks){
		String errorMessage = "";
		try {
			errorMessage = jsonObject.getString("error");
		} catch (JSONException e) {
			logger.info("No error in Ahref backlink data fetching");
		}
		if(errorMessage == null || (errorMessage != null && errorMessage.equals(""))){
			try {
				JSONArray results = jsonObject.getJSONArray("Result");
				for (int i = 0; i < results.length(); i++) {
					JSONObject result = results.getJSONObject(i);
					addAhrefBacklinkObject(result, backLinks);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return "No Results";
			}
			return "No Error";
		}else{
			return errorMessage;
		}
	}
*/

 //TODO : Method not in use
        
        
/*	private void addAhrefBacklinkObject(JSONObject result, ArrayList<AhrefBacklink> backLinks){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		boolean blank = true;
		long index = -1;
		float rating = 0;
		String sourceUrl = "";
		String ip = "";		
		String title = "";
		int internalLinks = 0;
		int externalLinks = 0;
		long size = 0;
		
		try {
			index = Long.parseLong(result.getString("Index"));
			blank = false;
		} catch (JSONException e) {
			logger.error("Index not found in json result sent from Ahref");
		}
		try {
			rating = Float.parseFloat(result.getString("Rating"));
			blank = false;
		} catch (JSONException e) {
			logger.error("Rating not found in json result sent from Ahref");
		}
		try {
			sourceUrl = result.getString("UrlFrom");
			blank = false;
		} catch (JSONException e) {
			logger.error("UrlFrom not found in json result sent from Ahref");
		}
		try {
			ip = result.getString("IpFrom");
			blank = false;
		} catch (JSONException e) {
			logger.error("IP not found in json result sent from Ahref");
		}		
		try {
			title = result.getString("Title");
			blank = false;
		} catch (JSONException e) {
			logger.error("Title not found in json result sent from Ahref");
		}
		try {
			internalLinks = Integer.parseInt(result.getString("LinksInternal"));
			blank = false;
		} catch (JSONException e) {
			logger.error("LinksInternal not found in json result sent from Ahref");
		}
		try {
			externalLinks = Integer.parseInt(result.getString("LinksExternal"));
			blank = false;
		} catch (JSONException e) {
			logger.error("LinksExternal not found in json result sent from Ahref");
		}
		try {
			size = Long.parseLong(result.getString("Size"));
			blank = false;
		} catch (JSONException e) {
			logger.error("Size not found in json result sent from Ahref");
		}
		
		if(!blank){
			try {
				JSONArray links = result.getJSONArray("Links");
				for (int i = 0; i < links.length(); i++) {
					
					AhrefBacklink ahrefBacklink = new AhrefBacklink();
					ahrefBacklink.setIndex(index);
					ahrefBacklink.setRating(rating);
					ahrefBacklink.setSourceURL(sourceUrl);
					ahrefBacklink.setSourceIp(ip);
					ahrefBacklink.setTitle(title);
					ahrefBacklink.setInternalLinks(internalLinks);
					ahrefBacklink.setExternalLinks(externalLinks);
					ahrefBacklink.setSize(size);
					JSONObject link = links.getJSONObject(i);
					try{
						String targetUrl = link.getString("UrlTo");
						ahrefBacklink.setTargetURL(targetUrl);
					} catch (JSONException e) {
						ahrefBacklink.setTargetURL("");
						logger.error("Exception with :UrlTo ");
					}
					try{
						long visited = Long.parseLong(link.getString("Visited"));
						ahrefBacklink.setLastVisited(visited);
						Calendar cal = Calendar.getInstance();
						cal.setTimeInMillis(visited * 1000);
						String visitedDateString =  sdf.format(cal.getTime());
						ahrefBacklink.setLastVisitedDateString(visitedDateString);
					}catch (JSONException e) {
						ahrefBacklink.setLastVisited(Calendar.getInstance().getTimeInMillis());
						Calendar cal = Calendar.getInstance();
						String visitedDateString =  sdf.format(cal.getTime());
						ahrefBacklink.setLastVisitedDateString(visitedDateString);
						logger.error("Exception with :Visited ");
					}
					try{
						long firstSeen = Long.parseLong(link.getString("FirstSeen"));
						ahrefBacklink.setFirstSeen(firstSeen);
					}catch (JSONException e) {
						ahrefBacklink.setFirstSeen(Calendar.getInstance().getTimeInMillis());
						logger.error("Exception with :FirstSeen ");
					}
					try {
						String anchorText = link.getString("Anchor");
						ahrefBacklink.setAnchorText(anchorText);
					} catch (JSONException e) {
						ahrefBacklink.setAnchorText("");
						logger.error("Exception with :Anchor ");
					}
					try{
						String type = link.getString("Type");
						ahrefBacklink.setAnchorType(type);
					} catch (JSONException e) {
						ahrefBacklink.setAnchorType("");
						logger.error("Exception with :Type ");
					}
					try{
						ahrefBacklink.setNoFollow(false);
						JSONArray flags = link.getJSONArray("Flag");
						for (int j = 0; j < flags.length(); j++) {
							String flag = flags.getString(j);
							if(flag.equals("nf")){
								ahrefBacklink.setNoFollow(true);
							}
						}
					} catch (JSONException e) {}
					try{
						String preText = link.getString("TextPre");
						ahrefBacklink.setPreText(preText);
					} catch (JSONException e) {
						ahrefBacklink.setPreText("");
						logger.error("Exception with :TextPre ");
					}
					try{
						String postText = link.getString("TextPost");
						ahrefBacklink.setPostText(postText);
					} catch (JSONException e) {
						ahrefBacklink.setPostText("");
						logger.error("Exception with :TextPost ");
					}
					backLinks.add(ahrefBacklink);
//					System.out.println("One object Added, size = "+backLinks.size());
				}
			} catch (JSONException e) {
				logger.error("No Backlink found  in json result sent from Ahref");
			}
		}
	}
*/
	
	public long fetchNumberOfBacklinks(String domainName){
		long backlinksCount = 0;
		String queryUrl = ahrefAPIUrl + "get_backlinks_count.php?target="+domainName+"&mode=subdomains" +
				"&output=json&AhrefsKey=" + ahrefAPIKey;
		JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
		if(jsonObject != null){
			try {
				String backlinks = jsonObject.getJSONObject("Result").getString("Backlinks");
				if(backlinks != null && !backlinks.equals("")){
					backlinksCount = Long.parseLong(backlinks);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (NumberFormatException ex){
				ex.printStackTrace();
			}
		}
		return backlinksCount;
	}
	
	public long fetchNumberOfRefferingDomains(String domainName){
		long refDomainCount = 0;
		String queryUrl = ahrefAPIUrl + "get_ref_domains_ips_count.php?target="+domainName+"&mode=subdomains" +
				"&output=json&AhrefsKey=" + ahrefAPIKey;
		JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
		if(jsonObject != null){
			try {
				String refDomainCountString = jsonObject.getJSONObject("Result").getString("Domains");
				if(refDomainCountString != null && !refDomainCountString.equals("")){
					refDomainCount = Long.parseLong(refDomainCountString);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (NumberFormatException ex){
				ex.printStackTrace();
			}
		}
		return refDomainCount;
	}
	
	public long [] fetchBacklinkNoFollowLinkCount(String domainName){
		long [] backlinkNoFollowLinkCount = {0l,0l};
		String queryUrl = ahrefAPIUrl + "get_backlinks_count_ext.php?target="+domainName+"&mode=subdomains" +
				"&output=json&AhrefsKey=" + ahrefAPIKey;
		JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
		if(jsonObject != null){
			try {
				String backlinks = jsonObject.getJSONObject("Result").getString("Backlinks");
				if(backlinks != null && !backlinks.equals("")){
					backlinkNoFollowLinkCount[0] = Long.parseLong(backlinks);
				}
				String noFollowLinks = jsonObject.getJSONObject("Result").getString("NoFollow");
				if(noFollowLinks != null && !noFollowLinks.equals("")){
					backlinkNoFollowLinkCount[1] = Long.parseLong(noFollowLinks);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (NumberFormatException ex){
				ex.printStackTrace();
			}
		}
		return backlinkNoFollowLinkCount;
	}
	
	public long [] fetchBacklinkCheckerHeaderData(String domain){		
		long [] backlinkNoFollowLinkCount = fetchBacklinkNoFollowLinkCount(domain);
		long refDomainCount = fetchNumberOfRefferingDomains(domain);
		return new long[] {backlinkNoFollowLinkCount[0], refDomainCount, backlinkNoFollowLinkCount[1]};
	}
}