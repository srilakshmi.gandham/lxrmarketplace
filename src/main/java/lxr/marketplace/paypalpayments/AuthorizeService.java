package lxr.marketplace.paypalpayments;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import org.apache.log4j.Logger;
import lxr.marketplace.authnet.rebill.ArrayOfCustomerPaymentProfileType;
import lxr.marketplace.authnet.rebill.CreateCustomerProfileResponseType;
import lxr.marketplace.authnet.rebill.CreateCustomerProfileTransactionResponseType;
import lxr.marketplace.authnet.rebill.CreditCardType;
import lxr.marketplace.authnet.rebill.CustomerPaymentProfileType;
import lxr.marketplace.authnet.rebill.CustomerProfileType;
import lxr.marketplace.authnet.rebill.MerchantAuthenticationType;
import lxr.marketplace.authnet.rebill.OrderExType;
import lxr.marketplace.authnet.rebill.PaymentType;
import lxr.marketplace.authnet.rebill.ProfileTransAuthCaptureType;
import lxr.marketplace.authnet.rebill.ProfileTransactionType;
import lxr.marketplace.authnet.rebill.Service;
import lxr.marketplace.authnet.rebill.ServiceSoap;
import lxr.marketplace.authnet.rebill.ValidationModeEnum;

public class AuthorizeService {
	
	private static Logger logger = Logger.getLogger(AuthorizeService.class);
	private static ServiceSoap service_soap = null;
	public static final String WSDL_URL = "https://apitest.authorize.net/soap/v1/Service.asmx?wsdl";
	
	// TODO: Specify you merchant account name
	public static final String MERCHANT_NAME = "5gYf6HX37";

	// TODO: Specify you merchant transaction key
	public static final String TRANSACTION_KEY = "4tN45x6FZYH862nm";
	
	private static MerchantAuthenticationType m_auth = null;
	
	public static MerchantAuthenticationType getMerchantAuthentication(){
		if(m_auth == null){
			m_auth = new MerchantAuthenticationType();
			m_auth.setName(MERCHANT_NAME);
			m_auth.setTransactionKey(TRANSACTION_KEY);
		}
		return m_auth;
	}
	
	public static ServiceSoap getServiceSoap(){
		if(service_soap != null) return service_soap;
		return getServiceSoap(WSDL_URL);
	}
	
	public static ServiceSoap getServiceSoap(String url){
		URL wsdl_url = null;
		try {
			wsdl_url = new URL(url);
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if(wsdl_url == null){
			return null;
		}

		Service rebill_ws = new Service(wsdl_url, new QName("https://api.authorize.net/soap/v1/", "Service"));
		service_soap = rebill_ws.getServiceSoap();
		return service_soap;
	}
	
	
	public List<Long> saveCardDetailsInAuthorizeService(String[] dataValues, String [] errorMessage, String email) {      
		long tempPaymentProfileId = 0;
		
		ServiceSoap soap = getServiceSoap();
		List<Long> authProfileIds = new ArrayList<Long>();
		CustomerPaymentProfileType new_payment_profile = new CustomerPaymentProfileType();
		
		PaymentType new_payment = new PaymentType();
		CreditCardType new_card = new CreditCardType();
		new_card.setCardNumber(dataValues[2]);
		
		try{
			javax.xml.datatype.XMLGregorianCalendar cal = javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar();
			cal.setMonth(Integer.parseInt(dataValues[3]));
			cal.setYear(Integer.parseInt(dataValues[4]));
			new_card.setExpirationDate(cal);
		}catch(javax.xml.datatype.DatatypeConfigurationException dce){
			logger.debug(dce.getMessage());
		}
		new_card.setCardCode(dataValues[5]);
		new_payment.setCreditCard(new_card);
		new_payment_profile.setPayment(new_payment);

		CustomerProfileType m_new_cust = new CustomerProfileType();
		m_new_cust.setEmail(email);
//		m_new_cust.setEmail("sireesha.netelixir@gmail.com");
		m_new_cust.setDescription("New Subscribed Customer : " + Long.toString(System.currentTimeMillis()));

		ArrayOfCustomerPaymentProfileType pay_list = new ArrayOfCustomerPaymentProfileType();
		pay_list.getCustomerPaymentProfileType().add(new_payment_profile);
		
		m_new_cust.setPaymentProfiles(pay_list);   //
		
		CreateCustomerProfileResponseType response = soap.createCustomerProfile(getMerchantAuthentication(),m_new_cust,ValidationModeEnum.TEST_MODE);
		response.getCustomerShippingAddressIdList();
		logger.info("Response from server-----"+response);
		if(response != null){
			response.getCustomerPaymentProfileIdList().getLong().get(0);
			logger.debug("Response Code: " + response.getResultCode().value());
			for(int i = 0; i < response.getMessages().getMessagesTypeMessage().size(); i++){
				logger.debug("Message: " + response.getMessages().getMessagesTypeMessage().get(i).getText());
				if(response.getResultCode().value().equals("Error")){ // NOT SUCCESS
					 errorMessage[i] = response.getMessages().getMessagesTypeMessage().get(i).getText();
				}
			}
			long new_cust_id = response.getCustomerProfileId();
			if(new_cust_id > 0){
				logger.info("New Customer ID = " + new_cust_id);
			}
			if (response.getCustomerPaymentProfileIdList() != null){
				for(long paymentProfileId : response.getCustomerPaymentProfileIdList().getLong()){
					tempPaymentProfileId = paymentProfileId;
					logger.info("With payment profile ID = " + tempPaymentProfileId);
				}
			}
			if(new_cust_id > 0 && tempPaymentProfileId > 0 ){
				authProfileIds.add(new_cust_id);
				authProfileIds.add(tempPaymentProfileId);
			}
		}	
		return authProfileIds;
	}
	
	public LxrMCreditCard getAuthSavedCardInfo(List<Long> authProfileIds,String[] dataValues){
		LxrMCreditCard savedCreditCard = new LxrMCreditCard();
		int num = dataValues[2].length();
		int xCount = num-4;
		String tempCardNum="";
		for(int i=1;i<=xCount;i++){
			tempCardNum+="x";
		}
		tempCardNum+=dataValues[2].substring(xCount);
		logger.debug("tempCardNum-------"+tempCardNum);
		savedCreditCard.setCustProfileId(authProfileIds.get(0));
		savedCreditCard.setCardNo(tempCardNum);
		savedCreditCard.setPaymentProfileId(authProfileIds.get(1));
		savedCreditCard.setType(dataValues[6]);
		savedCreditCard.setCountryCode(dataValues[8]);
		savedCreditCard.setPostalCode(dataValues[7]);
		savedCreditCard.setFirstName(dataValues[0]);
		savedCreditCard.setLastName(dataValues[1]);
		savedCreditCard.setPayPalCardId("");
		savedCreditCard.setCvv(dataValues[5]);
		savedCreditCard.setExpireMonth(Integer.parseInt(dataValues[3]));
		savedCreditCard.setExpireYear(Integer.parseInt(dataValues[4]));
		savedCreditCard.setState("");
		savedCreditCard.setCity("");
		savedCreditCard.setServiceProvider(2);
		savedCreditCard.setAddressLine1("");
		savedCreditCard.setAddressLine2("");
		return savedCreditCard;
	}
	
	
	public String authorizeByStoredCreditDebitCardUsingAuthorizeNet(long custProfileId,long paymentProfileId,StringBuilder failurMessage,double amount1) {
		String compResponse = "";
		java.math.BigDecimal amount = new java.math.BigDecimal(amount1);
		ServiceSoap soap = getServiceSoap();
		logger.info("custProfileId is "+custProfileId+".................amount is "+amount1+"and amount is....."+amount);
		if(custProfileId > 0 && paymentProfileId > 0){
			ProfileTransAuthCaptureType auth_capture = new ProfileTransAuthCaptureType();
			auth_capture.setCustomerProfileId(custProfileId);
			auth_capture.setCustomerPaymentProfileId(paymentProfileId);
			auth_capture.setAmount(amount);
			OrderExType order = new OrderExType();
			
			order.setInvoiceNumber("invoice1234");
			auth_capture.setOrder(order);
			auth_capture.setRecurringBilling(true);
			
			ProfileTransactionType trans = new ProfileTransactionType();
			trans.setProfileTransAuthCapture(auth_capture);
			
			CreateCustomerProfileTransactionResponseType response = soap.createCustomerProfileTransaction(getMerchantAuthentication(), trans, null);
			compResponse = response.getDirectResponse();
			
			logger.debug("Response Code: " + response.getResultCode().value() +"\ndirect response: "+response.getDirectResponse());
			for(int i = 0; i < response.getMessages().getMessagesTypeMessage().size(); i++){
				if(response.getResultCode().value().equals("Error")){
					failurMessage.append(response.getMessages().getMessagesTypeMessage().get(i).getText());
				}
				logger.debug("Message: " + response.getMessages().getMessagesTypeMessage().get(i).getText());
			}
		}
		return compResponse;
	}
	
	public void getTransactionDetails(){
		
	}
}


