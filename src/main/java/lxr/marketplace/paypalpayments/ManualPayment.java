package lxr.marketplace.paypalpayments;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Capture;
import com.paypal.api.payments.CreditCard;
import com.paypal.core.rest.PayPalResource;

public class ManualPayment {

    /**
     * @param args
     */
    public static void main(String[] args) {
        long userId = 0;
        String downloadFolder = "/disk2/lxrmarketplace/download/";
        String firstName = "Gary";
        String lastName = "Couch";
        
        
        PayPalService payPalService = new PayPalService();
        LxrMPaymentService lxrMPaymentService = new LxrMPaymentService();
        
        Properties paypalProperties = new Properties();
        Map<String, String> paypalProps = new HashMap<String, String>();
        paypalProps.put("http.ConnectionTimeOut", "5000");
        paypalProps.put("http.Retry", "1");
        paypalProps.put("http.ReadTimeOut", "30000");
        paypalProps.put("http.MaxConnection", "100");
        paypalProps.put("http.GoogleAppEngine", "false");
        paypalProps.put("service.EndPoint", "https://api.paypal.com");
        paypalProps.put("clientID", "ASCbYBBnEdigrk2LA5_GGHeG8zNzFXP3yW4eFCDPUnlYcrzE07Tt4O4nZ7ud");
        paypalProps.put("clientSecret", "EHL_CBALg2tMXOBU0vVgagfU1zqbbpq3sAihfD-epkTTuCjrteJDamOGmp27");
        
        paypalProperties.putAll(paypalProps);
        PayPalResource.initConfig(paypalProperties);
        
        
        payPalService.setLxrMPaymentService(lxrMPaymentService);
        payPalService.setPaypalProperties(paypalProperties);
        /*Code for manual authorization and capture*/
        StringBuilder failureMessage = new StringBuilder();
        Amount amount2 = new Amount();
        amount2.setCurrency("USD");
        amount2.setTotal("49.00");
        
        /*Only authorize*/
//        payPalService.authorizeByStoredCreditDebitCard("CARD-96V341767R2799230KNEBL5Y", amount2, failureMessage2);
        /*Authorize and capture*/
//        Capture responseCap = payPalService.payByStoredCardAuthorizeCapture("CARD-13S394312Y604694DKNRTLAA", 
//                amount2, failureMessage);
//        String repfileName = lxrMPaymentService.createInvoiceFromCapture(responseCap, downloadFolder, 
//                firstName,lastName,1);
        
        
        /*CODE FOR MANUAL AUTHORIZATION AND CAPTURE USING RAW CARD DETAILS*/                               
      CreditCard creditCard2 = new CreditCard();
      creditCard2.setNumber("4736909381877724");
      creditCard2.setExpireMonth(7);
      creditCard2.setExpireYear(2015);
      creditCard2.setCvv2("336");
      creditCard2.setFirstName(firstName);
      creditCard2.setLastName(lastName);
      creditCard2.setType("visa");
      try{
          Capture responseCapture = payPalService.payByCardAuthorizeCapture(creditCard2, amount2, failureMessage, userId);
      }catch(Exception e){
          e.printStackTrace();
      }         

    }

}
