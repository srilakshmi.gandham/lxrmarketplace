package lxr.marketplace.paypalpayments;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.paypal.api.payments.Authorization;
import com.paypal.api.payments.Capture;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Transaction;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;

public class LxrMPaymentService extends JdbcDaoSupport {
	private static Logger logger = Logger
			.getLogger(LxrMPaymentService.class);

	public long saveCreditCardInLxr(final CreditCard newCreditCard,final LxrMCreditCard lxrMCrditCard,final long userId) {
		final String query = "INSERT INTO credit_card_info (user_id, paypal_card_id, card_no, first_name, "
				+ "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
				+ "address_line_1, address_line_2, city, country_code, postal_code," +
				" state,cust_profile_id,payment_profile_id,service_provider) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		final SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss'Z'");
		logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			synchronized (this) {

				getJdbcTemplate().update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
						statement.setLong(1, userId);
						statement.setString(2, newCreditCard.getId());
						statement.setString(3, newCreditCard.getNumber());
						statement.setString(4, newCreditCard.getFirstName());
						statement.setString(5, newCreditCard.getLastName());
						statement.setString(6, newCreditCard.getType());
						try {
							Date validUntil = sdf.parse(newCreditCard.getValidUntil());
							statement.setTimestamp(7, new Timestamp(validUntil.getTime()));
						} catch (Exception e) {
							statement.setTimestamp(7, null);
							logger.error("Exception in getting 'valid until' field for credit card of "
									+ newCreditCard.getFirstName() + ". Saving the card without this info.");
							e.printStackTrace();
						}
						try {
							statement.setInt(8, newCreditCard.getExpireMonth());
						} catch (Exception e) {
							logger.error("Exception in parsing expire month for credit card of "
									+ newCreditCard.getFirstName() + "saving the card without this info.");
							statement.setInt(8, 0);
						}
						try {
							statement.setInt(9, newCreditCard.getExpireYear());
						} catch (Exception e) {
							logger.error("Exception in parsing expire year for credit card of "
									+ newCreditCard.getFirstName()+ "saving the card without this info.");
							statement.setInt(9, 0);
						}
						Calendar cal = Calendar.getInstance();
						statement.setTimestamp(10, new Timestamp(cal.getTimeInMillis()));
						statement.setBoolean(11, false);
						if (newCreditCard.getBillingAddress() != null) {
							statement.setString(12, newCreditCard.getBillingAddress().getLine1());
							statement.setString(13, newCreditCard.getBillingAddress().getLine2());
							statement.setString(14, newCreditCard.getBillingAddress().getCity());
							statement.setString(15, newCreditCard.getBillingAddress().getCountryCode());
							statement.setString(16, newCreditCard.getBillingAddress().getPostalCode());
							statement.setString(17, newCreditCard.getBillingAddress().getState());
						}else{
							statement.setString(12, "");
							statement.setString(13, "");
							statement.setString(14, "");
							statement.setString(15, lxrMCrditCard.getCountryCode());
							statement.setString(16, lxrMCrditCard.getPostalCode());
							statement.setString(17, "");
						}
						statement.setString(18,"");
						statement.setString(19,"");
						statement.setInt(20,1);
						return statement;
					}
				}, keyHolder);
			}
			logger.info("Credit Card is saved in lxrmarketplace database for user with id = "+ userId);
			return (Long) keyHolder.getKey();
		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public LxrMCreditCard fetchCreditCardDetails(long userId) {
		LxrMCreditCard lxrMCreditCard = null;
		String query = "select * from credit_card_info where user_id = " + userId + " and is_deleted = 0";
		logger.info("Fetching creditcard in lxrmarketplace database for user with id = " + userId);
		try {
			lxrMCreditCard = (LxrMCreditCard) getJdbcTemplate().queryForObject(query, 
					new RowMapper<Object>() {
				LxrMCreditCard lmcc;
				public Object mapRow(ResultSet rs, int rowNum)throws SQLException {

					lmcc = new LxrMCreditCard();
					lmcc.setLxrCardId(rs.getLong("card_id"));
					lmcc.setUserId(rs.getLong("user_id"));
					lmcc.setPayPalCardId(rs.getString("paypal_card_id"));
					lmcc.setCardNo(rs.getString("card_no"));
					lmcc.setFirstName(rs.getString("first_name"));
					lmcc.setLastName(rs.getString("last_name"));
					lmcc.setType(rs.getString("card_type"));
					lmcc.setExpireMonth(rs.getInt("expire_month"));
					lmcc.setExpireYear(rs.getInt("expire_year"));

					lmcc.setAddressLine1(rs.getString("address_line_1"));
					lmcc.setAddressLine1(rs.getString("address_line_2"));
					lmcc.setCity(rs.getString("city"));
					lmcc.setCountryCode(rs.getString("country_code"));
					lmcc.setPostalCode(rs.getString("postal_code"));
					lmcc.setState(rs.getString("state"));
					lmcc.setCustProfileId(rs.getLong("cust_profile_id"));
					lmcc.setPaymentProfileId(rs.getLong("payment_profile_id"));
					lmcc.setServiceProvider(rs.getInt("service_provider"));
					
					return lmcc;
				}
			});
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}
		return lxrMCreditCard;
	}
	
	public void deletePreviousCard(long userId){
		String query = "UPDATE credit_card_info SET is_deleted = 1 WHERE user_id = " + userId;
		getJdbcTemplate().update(query);
	}
	public void saveCardTransactionInfo(Payment createdPayment, LxrMCreditCard lxrMCreditCard, long toolId){
		if(createdPayment == null){
			return;
		}
		List<Transaction> transactions = createdPayment.getTransactions();
		logger.info("createdPayment: "+createdPayment+"\nTransaction Informations\nPayment Id: " 
				+ createdPayment.getId()+ "\nCreated Time: " + createdPayment.getCreateTime() + "\nUpdated Time: " 
				+ createdPayment.getUpdateTime() + "\nPayment State:  " + createdPayment.getState()
				+"\nPayment Intent " + createdPayment.getIntent()+"\nTransaction Id: "
				+transactions.get(0).getRelatedResources().get(0).getSale().getId());
		
		final String query = "INSERT INTO paypal_transaction_stats (created_time, updated_time, " +
				"state, intent, tool_id, currency, amount, paypal_payment_id, description, " +
				"lxr_card_id, payer_user_id, is_card,  transaction_id) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Timestamp creationTime = null;
		Timestamp updationTime = null;
		try {
			Date createdTime = sdf.parse(createdPayment.getCreateTime());
			creationTime =  new Timestamp(createdTime.getTime());
		} catch (Exception e) {
			logger.error("Exception in getting transaction creation time for user with id = "
					+ lxrMCreditCard.getUserId());
			e.printStackTrace();
		}
		try {
			Date updatedTime = sdf.parse(createdPayment.getUpdateTime());
			updationTime =  new Timestamp(updatedTime.getTime());
		} catch (Exception e) {
			logger.error("Exception in getting transaction updation time for user with id = "
					+ lxrMCreditCard.getUserId());
			e.printStackTrace();
		}
		try {
			getJdbcTemplate().update(query, new Object[] {creationTime, updationTime, createdPayment.getState(),
				createdPayment.getIntent(), toolId, transactions.get(0).getAmount().getCurrency(), 
				Double.parseDouble(transactions.get(0).getAmount().getTotal()), createdPayment.getId(), 
				transactions.get(0).getDescription(), lxrMCreditCard.getLxrCardId(), 
				lxrMCreditCard.getUserId(), true,
				transactions.get(0).getRelatedResources().get(0).getSale().getId()
			});
			logger.info("Transaction information saved in marketplace database for user "
					+ lxrMCreditCard.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * saves transaction failure information from saved credit/debit card
	 * @param failureMessage
	 * @param lxrMCreditCard
	 * @param toolId
	 * @param intent type of transaction where failure happened possible values: authorization, sale, capture
	 * @return
	 */
	public boolean saveFailedCardTransactionInfo(String failureMessage, LxrMCreditCard lxrMCreditCard, 
	        long toolId, String intent){
		Timestamp creationTime = null;
		Timestamp updationTime = null;
		Calendar now = Calendar.getInstance();
		creationTime =  new Timestamp(now.getTimeInMillis());
		updationTime =  new Timestamp(now.getTimeInMillis());

	    Object [] dataParams = new Object[]{creationTime, updationTime, "failure", intent, toolId, "", 
	            0.00, "", "", lxrMCreditCard.getLxrCardId(), lxrMCreditCard.getUserId(), true, "", 
	            failureMessage};
	    
	    return savePayPalTransactionInfo(dataParams, lxrMCreditCard.getUserId());
	}
	
	public boolean savePayPalAuthorization(Authorization authorization, long userId, long toolId, long lxrmCardId){
	    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(authorization.getCreateTime());
            creationTime =  new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction creation time for user with id = "+ userId);
            e.printStackTrace();
        }
        try {
            Date updatedTime = sdf.parse(authorization.getUpdateTime());
            updationTime =  new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = "+ userId);
            e.printStackTrace();
        }
        Long lxrmCardIdObj = null;
        if(lxrmCardId > 0){
            lxrmCardIdObj = new Long(lxrmCardId);
        }
        Object [] dataParams = new Object[]{creationTime, updationTime, authorization.getState(), 
                "authorization", toolId, authorization.getAmount().getCurrency(), 
                authorization.getAmount().getTotal(), "","LXRSEO Subscription Authorization", lxrmCardIdObj, 
                userId, true, authorization.getId(), ""};
        
        return savePayPalTransactionInfo(dataParams, userId);
	}
	
	private CreditCard getPayPalCardFromLxrMCard(LxrMCreditCard lxrCreditCard){		
		CreditCard creditCard = new CreditCard();
		creditCard.setNumber(lxrCreditCard.getCardNo());
		creditCard.setExpireMonth(lxrCreditCard.getExpireMonth());
		creditCard.setExpireYear(lxrCreditCard.getExpireYear());
		creditCard.setFirstName(lxrCreditCard.getFirstName());
		creditCard.setLastName(lxrCreditCard.getLastName());
		creditCard.setType(lxrCreditCard.getType());
		
//		Address billingAddress = new Address();
//		billingAddress.setLine1(lxrCreditCard.getAddressLine1());
//		billingAddress.setLine2(lxrCreditCard.getAddressLine2());
//		billingAddress.setCity(lxrCreditCard.getCity());
//		billingAddress.setCountryCode(lxrCreditCard.getCountryCode());
//		billingAddress.setPostalCode(lxrCreditCard.getPostalCode());
//		billingAddress.setState(lxrCreditCard.getState());		
//		creditCard.setBillingAddress(billingAddress);
		return creditCard;
	}
	public boolean savePayPalCapture(Capture capture, long userId, long toolId, long lxrmCardId){
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(capture.getCreateTime());
            creationTime =  new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction creation time for user with id = "+ userId);
            e.printStackTrace();
        }
        try {
            Date updatedTime = sdf.parse(capture.getUpdateTime());
            updationTime =  new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = "+ userId);
            e.printStackTrace();
        }
        
        Object [] dataParams = new Object[]{creationTime, updationTime, capture.getState(), 
                "capture", toolId, capture.getAmount().getCurrency(), 
                capture.getAmount().getTotal(), "","LXRSEO subscription capture", lxrmCardId, 
                userId, true, capture.getId(), ""};
        
        return savePayPalTransactionInfo(dataParams, userId);
    }
	
	private boolean savePayPalTransactionInfo(Object[] dataParams, long userId){
	    boolean saved = false;
	    final String query = "INSERT INTO paypal_transaction_stats (created_time, updated_time, " +
                "state, intent, tool_id, currency, amount, paypal_payment_id, description, " +
                "lxr_card_id, payer_user_id, is_card, transaction_id, failure_message) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        try {
            getJdbcTemplate().update(query, dataParams);
            logger.info("PayPal Transaction information saved in marketplace database for user "+ userId);
            saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saved;
	}
	
	public boolean saveDummyPayPalTransactionInfo(long userId, long lxrmCardId){
	    Object [] dataParams = new Object[]{new Timestamp(Calendar.getInstance().getTimeInMillis()), 
	            new Timestamp(Calendar.getInstance().getTimeInMillis()), "authorized", 
                "authorization", 12, "USD", "0.05", "","LXRSEO Subscription Authorization", lxrmCardId, 
                userId, true, "000000000", ""};
        boolean saved = false;
        final String query = "INSERT INTO paypal_transaction_stats (created_time, updated_time, " +
                "state, intent, tool_id, currency, amount, paypal_payment_id, description, " +
                "lxr_card_id, payer_user_id, is_card, transaction_id, failure_message) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        try {
            getJdbcTemplate().update(query, dataParams);
            logger.info("PayPal Transaction information saved in marketplace database for user "+ userId);
            saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saved;
    }
	
	public boolean saveLXRSEOFaildCardDetails(long userId, Calendar failTime, String cardType, 
	        String failureMessage){
	    boolean saved = false;
        final String query = "INSERT INTO lxrseo_card_failure (user_id, fail_time, card_type, " +
                "failure_message) VALUES (?, ?, ?, ?) ";
        try {
            getJdbcTemplate().update(query, new Object[]{userId, failTime, cardType, failureMessage});
            logger.info("LXRSEO Card Provision information saved in marketplace database for user "+ userId);
            saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saved;
	}
	
	public boolean checkUserTransactions(Login user,long tool_id){
	   String query = "select count(payment_id) from paypal_transaction_stats where payer_user_id="+user.getId()+" and tool_id="+tool_id;
	   int n = getJdbcTemplate().queryForInt(query);
	   if(n == 0){
		   return false;
	   }else{
		   return true;
	   }
	}
	
	public boolean checkUserCardInfo(long userId){
		String query = "select count(card_id) from credit_card_info where user_id="+userId;
		int n = getJdbcTemplate().queryForInt(query);
		if(n == 0){
			return false;
		}else{
			return true;
		}
	}
	
	//To save user authorize.net transaction info
	 public  Object[] getAuthorizeTransactionInfo(String compResponse, long userId, long toolId, long lxrmCardId,Timestamp creationTime){
	     Timestamp updationTime = null;
	     String authState ="";
	     try {
	         updationTime =  creationTime;
	     }catch (Exception e) {
	         updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
	         logger.error("Exception in getting transaction updation time for user with id = "+ userId);
	         e.printStackTrace();
	     }
	     Object [] dataParams = null;
	     if(!compResponse.trim().equals("")){
	    	 String[] param = compResponse.split(",");
	    	 if(param[0].equals(""+1)){
	    		 authState ="Approved";
	    	 }else if(param[0].equals(""+2)){
	    		 authState ="Declined"; 
	    	 }else if(param[0].equals(""+3)){
	    		 authState ="Error";
	    	 }else if(param[0].equals(""+4)){
	    		 authState ="Held for Review";
	    	 }
	    	 dataParams = new Object[]{creationTime, updationTime, authState, param[11], toolId, "USD", 
		             param[9], "","LXRSEO subscription capture", lxrmCardId,userId, true, param[6], param[3]};
	    	 logger.info("param[9]......................amount is :"+param[9]+" authState .........."+authState);
	    }
	     return dataParams;
	 }
	
	 public boolean saveAuthorizationTranscationInfoInLxrTab(Object [] dataParams,long userId ){
		 boolean saved = false;
		 if(dataParams!=null && dataParams.length > 0){
	    	 saved = savePayPalTransactionInfo(dataParams, userId);
	    }
	     return saved;
	 }
	
		public long saveCreditCardInLxrForAuth(final LxrMCreditCard newCreditCard,final long userId) {
			final String query = "INSERT INTO credit_card_info (user_id, paypal_card_id, card_no, first_name, "
					+ "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
					+ "address_line_1, address_line_2, city, country_code, postal_code, state,cust_profile_id," +
					"payment_profile_id,service_provider) VALUES "
					+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
			try {
				KeyHolder keyHolder = new GeneratedKeyHolder();
				synchronized (this) {

					getJdbcTemplate().update(new PreparedStatementCreator() {
						public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
							PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
							statement.setLong(1, userId);
							statement.setString(2, "");
							statement.setString(3, newCreditCard.getCardNo());
							statement.setString(4, newCreditCard.getFirstName());
							statement.setString(5, newCreditCard.getLastName());
							statement.setString(6, newCreditCard.getType());
							statement.setTimestamp(7, null);
							try {
								statement.setInt(8, newCreditCard.getExpireMonth());
							} catch (Exception e) {
								logger.error("Exception in parsing expire month for credit card of "
										+ newCreditCard.getFirstName() + "saving the card without this info.");
								statement.setInt(8, 0);
							}
							try {
								statement.setInt(9, newCreditCard.getExpireYear());
							} catch (Exception e) {
								logger.error("Exception in parsing expire year for credit card of "
										+ newCreditCard.getFirstName()+ "saving the card without this info.");
								statement.setInt(9, 0);
							}
							Calendar cal = Calendar.getInstance();
							statement.setTimestamp(10, new Timestamp(cal.getTimeInMillis()));
							statement.setBoolean(11, false);
							statement.setString(12, newCreditCard.getAddressLine1());
							statement.setString(13, newCreditCard.getAddressLine2());
							statement.setString(14, newCreditCard.getCity());
							statement.setString(15, newCreditCard.getCountryCode());
							statement.setString(16, newCreditCard.getPostalCode());
							statement.setString(17, newCreditCard.getState());
							statement.setLong(18, newCreditCard.getCustProfileId());
							statement.setLong(19, newCreditCard.getPaymentProfileId());
							statement.setLong(20, newCreditCard.getServiceProvider());
							return statement;
						}
					}, keyHolder);
				}
				logger.info("Credit Card is saved in lxrmarketplace database for user with id = "+ userId);
				return (Long) keyHolder.getKey();
			} catch (Exception ex) {
				ex.printStackTrace();
				return 0;
			}
		}
	
	public String generateInvoiceForPayment(String targetFilePath,String transactId, String transactDate, 
	        String subStartTime, String extendedDate, String firstName, String lastName, String amount){
		logger.info("generating invoice for user after payment success");
		try{
        	Font blackFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD | Font.UNDERLINE , new BaseColor(0, 0, 0)));
        	Font smallFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.5f, Font.BOLD , new BaseColor(0, 0, 0)));
        	Font forBoldAnchor = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10f , Font.BOLD | Font.UNDERLINE , new BaseColor(0, 0, 0)));
        	Font normalFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.8f, Font.NORMAL));
        	Font normalHeadingFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD));
        	String repName = "LXRSEO_Invoice";
  			String reptName = Common.removeSpaces(repName);
			String filename = Common.createFileName(reptName) +".pdf";
			String filePath = targetFilePath + filename;
			File file = new File(filePath);
			FileOutputStream stream = new FileOutputStream(file);
		    com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 60, 30, 30, 20);
			PdfWriter.getInstance(pdfDoc, stream);
			pdfDoc.open();
			
			Paragraph paragraphSpaceh = new Paragraph(" ");
			pdfDoc.add(paragraphSpaceh);
			 
			logger.info(" Report generated in .pdf format");
			Image img = com.itextpdf.text.Image.getInstance( "/disk2/lxrmarketplace/images/lxr-seo-logo-mail.png");
			img.scaleToFit( 90f, 50f );
			Annotation anno = new Annotation( 0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com" );
			img.setAnnotation( anno );
			//Paragraph paragraph = new Paragraph();
	       // paragraph.setAlignment(Element.ALIGN_CENTER);
//		      document.add(paragraph);
			 Phrase mainHeading = new Phrase();
			 Chunk tempReportName = new Chunk("INVOICE" , blackFont);
			 tempReportName.setLineHeight(20f);
			 mainHeading.add(tempReportName);
			 PdfPTable imgTable = new PdfPTable(2);
			 imgTable.setWidthPercentage(99.9f);
			 PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0,true)));
       	     PdfPCell rpName = new PdfPCell(mainHeading);
       	     rpName.setVerticalAlignment(Element.ALIGN_CENTER);
       	     rpName.setPaddingLeft(-60f);
    	     rpName.setRightIndent(-5f);
    	     rpName.setPaddingTop(56f);
       	     logo.setBorder(Rectangle.NO_BORDER);
       	     rpName.setBorder(Rectangle.NO_BORDER);
       	     imgTable.addCell(logo);
       	     imgTable.addCell(rpName);
        	 pdfDoc.add( imgTable);
        	 Paragraph totalTxtStyle = new Paragraph();
        	 totalTxtStyle.setSpacingAfter(5f);
        	 totalTxtStyle.setSpacingBefore(4f);
        	 Paragraph paragraphSpace1 = new Paragraph(" ");
 			 pdfDoc.add(paragraphSpace1);
 			  Phrase transactionDetails = new Phrase();
 			 Chunk transactionId = new Chunk("Transaction Id :"+transactId+" " , forBoldAnchor);
 			 transactionId.setLineHeight(20f);
 			 transactionDetails.add(transactionId);
 			 totalTxtStyle.add(new Paragraph(transactionDetails));
 			 Phrase transtDate = new Phrase();
 			 Chunk transactionDate = new Chunk("Transaction Date : "+transactDate +" " , forBoldAnchor);
 			 transactionDate.setLineHeight(20f);
 			 transtDate.add(transactionDate);
 			 totalTxtStyle.add(new Paragraph(transtDate));
 			 pdfDoc.add(totalTxtStyle);
 			 Paragraph paragraphSpace = new Paragraph(" ");
 			 pdfDoc.add(paragraphSpace);
 			 float[] widths = {60f, 15f,15f};
 			 PdfPTable dataTable = new PdfPTable(widths);
 			 PdfPCell[] cells = new PdfPCell[3];
       	     PdfPCell cell1 = new PdfPCell(new Phrase("Item Description",normalHeadingFont));
    	     PdfPCell cell2 = new PdfPCell(new Phrase("Quantity",normalHeadingFont));
    	     PdfPCell cell3 = new PdfPCell(new Phrase("Amount",normalHeadingFont));
    	     cells[0] = cell1;cells[1] = cell2;cells[2] = cell3;
    	     dataTable.addCell(cells[0]);dataTable.addCell(cells[1]);dataTable.addCell(cells[2]);
    	     dataTable.completeRow();
    	     PdfPCell[] dcells1 = new PdfPCell[3];
    	     PdfPCell dataCell1 = new PdfPCell(new Phrase("LXRSEO Subscription valid from "+ subStartTime +" to "+ extendedDate  +".",normalFont));
    	     PdfPCell dataCell2 = new PdfPCell(new Phrase(" 1 ",normalFont));
    	     PdfPCell dataCell3 = new PdfPCell(new Phrase(amount, normalFont));
    	     dcells1[0] = dataCell1;dcells1[1] = dataCell2;dcells1[2] = dataCell3;
    	     dataTable.addCell(dcells1[0]);dataTable.addCell(dcells1[1]);dataTable.addCell(dcells1[2]);
    	     dcells1[0].setBorderColor(BaseColor.GRAY);
    	     dcells1[0].setBorderWidth(0.2f);
    	     dcells1[0].setPadding(8);
    	     dcells1[0].setSpaceCharRatio(6f);
    	     dataTable.completeRow();
    	     PdfPCell[] dcells2 = new PdfPCell[3];
    	     PdfPCell dtCell1 = new PdfPCell(new Phrase("VAT",normalFont));
    	     PdfPCell dtCell2 = new PdfPCell(new Phrase(" - ",normalFont));
    	     PdfPCell dtCell3 = new PdfPCell(new Phrase("$0.00",normalFont));
    	     dcells2[0] = dtCell1;dcells2[1] = dtCell2;dcells2[2] = dtCell3;
    	     dataTable.addCell(dcells2[0]);dataTable.addCell(dcells2[1]);dataTable.addCell(dcells2[2]);
    	     dataTable.completeRow();
    	     PdfPCell[] dcells3 = new PdfPCell[3];
    	     PdfPCell Fcell1 = new PdfPCell(new Phrase("Total amount paid",normalHeadingFont));
    	     PdfPCell Fcell2 = new PdfPCell(new Phrase(" - ",normalHeadingFont));
    	     PdfPCell Fcell3 = new PdfPCell(new Phrase(amount, normalHeadingFont));
    	     dcells3[0] = Fcell1;dcells3[1] = Fcell2;dcells3[2] = Fcell3;
    	     dataTable.addCell(dcells3[0]);dataTable.addCell(dcells3[1]);dataTable.addCell(dcells3[2]);
    	     dataTable.completeRow();
    	     dataTable.setWidthPercentage(99.9f);
    	     pdfDoc.add(dataTable);
    	     
    	     Paragraph paragraphSpace2 = new Paragraph(" ");
 			 pdfDoc.add(paragraphSpace2);
 			 
    	     Paragraph totalTextStyle = new Paragraph();
             totalTextStyle.setSpacingAfter(5f);
             totalTextStyle.setSpacingBefore(4f);
    	     Phrase toDetails = new Phrase();
    	     Chunk billedTo = new Chunk("Billed To : " , smallFont);
    	     billedTo.setLineHeight(20f);
    	     toDetails.add(billedTo);
    	     totalTextStyle.add(new Paragraph(toDetails));
    	     Phrase userDetails = new Phrase();
 			 Chunk userName = new Chunk(""+firstName+" "+lastName+"" , smallFont);
 			 userName.setLineHeight(20f);
 			 userDetails.add(userName);
 			 totalTextStyle.add(new Paragraph(userDetails));
 			 Phrase cpyDetails = new Phrase();
			 Chunk cmpName = new Chunk("Your credit card statements will display our Company Name \"NetElixir Inc.\"." , smallFont);
			 cmpName.setLineHeight(20f);
			 cpyDetails.add(cmpName);
			 totalTextStyle.add(new Paragraph(cpyDetails));
			  pdfDoc.add(totalTextStyle);
//			  pdfDoc.setPageSize(20,15,10,10);
			 //pdfDoc.setMargins(20f,15f,10f,10f);
			 pdfDoc.close();
			 return filename;
		}catch(Exception e){
			logger.error("Exception in generating Invoice");
			e.printStackTrace();
		}
		return null;
	}
	
	public String createInvoiceFromPayment(Payment paymentObj,String targetFilePath,String firstName,String lastName, int noOfDays){
		logger.info("generating invoice for user after payment success");
		try{
			String transactId="";
			Timestamp paymentTransactionDate = null;
			final SimpleDateFormat sdfy = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
			final SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
			List<Transaction> transactions = paymentObj.getTransactions();
			if(transactions!= null){
				transactId = transactions.get(0).getRelatedResources().get(0).getSale().getId();
				if(transactId != null){
					logger.debug("transactionId : "+ transactId);
					Date paymentUpdatedTime = sdfy.parse(paymentObj.getUpdateTime());
					paymentTransactionDate =  new Timestamp(paymentUpdatedTime.getTime());
					Calendar cal = Calendar.getInstance();  
					cal.setTime(paymentUpdatedTime);  
					cal.add(Calendar.DATE, noOfDays);
					String extendedDate = sdf1.format(new Timestamp(cal.getTimeInMillis()));
					String updatedTime = sdf1.format(paymentTransactionDate);
					String transactDate = sdf.format(paymentTransactionDate);
					String fileName = generateInvoiceForPayment(targetFilePath,transactId,transactDate,updatedTime,extendedDate,firstName,lastName, "$"+paymentObj.getTransactions().get(0).getAmount().getTotal());
					return fileName;
				}
			}
		}catch(Exception e){
			logger.error("Exception in generating Invoice");
			e.printStackTrace();
		}
		return null;
	}

	public String createInvoiceFromCapture(Capture captureObj, String targetFilePath, String firstName, 
	        String lastName, Date subStartDate, int months){
		logger.info("generating invoice for user after payment success");
		try{
			String captureId="";
			Timestamp paymentTransactionDate = null;
			final SimpleDateFormat sdfy = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
			final SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
			if(captureObj!= null){
				captureId = captureObj.getId();			
				if(captureId != null){
					logger.debug("captureId : "+ captureId);
					Date paymentUpdatedTime = sdfy.parse(captureObj.getUpdateTime());
					paymentTransactionDate =  new Timestamp(paymentUpdatedTime.getTime());
					Calendar cal = Calendar.getInstance();  
					cal.setTime(subStartDate);  
					cal.add(Calendar.MONTH, months);
					cal.add(Calendar.DATE, -1);
					String extendedDate = sdf1.format(new Timestamp(cal.getTimeInMillis()));
					String updatedTime = sdf1.format(subStartDate);
					String transactDate = sdf.format(paymentTransactionDate);
					String fileName = generateInvoiceForPayment(targetFilePath, captureId, transactDate, 
					        updatedTime,extendedDate,firstName,lastName, "$"+captureObj.getAmount().getTotal());
					return fileName;
				}
			}
		}catch(Exception e){
			logger.error("Exception in generating Invoice");
			e.printStackTrace();
		}
		return null;
	}
	
	public String createInvoiceFromAuthorizeNetResonse(Object[] responseObj, String targetFilePath, 
	        String firstName,String lastName,Timestamp paymentTransactionDate, int noOfDays){
		logger.info("generating invoice for user after payment success");
		try{
			String captureId="";
			final SimpleDateFormat sdfy = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
			final SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy");
			if(responseObj!= null && responseObj.length > 0){
				captureId =  ""+responseObj[10];			
				if(captureId != null){
					logger.info("captureId : "+ captureId);
					String tansactionDate = sdfy.format(paymentTransactionDate);
					Date paymentUpdatedTime = sdfy.parse(tansactionDate);
					paymentTransactionDate =  new Timestamp(paymentUpdatedTime.getTime());
					Calendar cal = Calendar.getInstance();  
					cal.setTime(paymentUpdatedTime);  
					cal.add(Calendar.DATE, noOfDays);
					String extendedDate = sdf1.format(new Timestamp(cal.getTimeInMillis()));
					String updatedTime = sdf1.format(paymentTransactionDate);
					String transactDate = sdf.format(paymentTransactionDate);
					String fileName = generateInvoiceForPayment(targetFilePath,captureId,transactDate,updatedTime,extendedDate,firstName,lastName, "$"+responseObj[6]);
					return fileName;
				}
			}
		}catch(Exception e){
			logger.error("Exception in generating Invoice");
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean updateLxrmCardId(long lxrCardId, long userId){
	    String query = "UPDATE paypal_transaction_stats SET lxr_card_id = " + lxrCardId
	            + " WHERE payer_user_id = "+userId+" AND lxr_card_id IS NULL";
	    boolean updated = false;
	    try{
	        getJdbcTemplate().execute(query);
	        updated = true;
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    return updated;
	}
	
	public long saveDummyCreditCardInLxr(final long userId) {
        final String query = "INSERT INTO credit_card_info (user_id, paypal_card_id, card_no, first_name, "
                + "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
                + "address_line_1, address_line_2, city, country_code, postal_code," +
                " state,cust_profile_id,payment_profile_id,service_provider) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {

                getJdbcTemplate().update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                        statement.setLong(1, userId);
                        statement.setString(2, "DUMMY");
                        statement.setString(3, "xxxxxxxxxxxx0000");
                        statement.setString(4, "Free");
                        statement.setString(5, "User");
                        statement.setString(6, "visa");
                        try {
                            Date validUntil = sdf.parse("2017-01-21");
                            statement.setTimestamp(7, new Timestamp(validUntil.getTime()));
                        } catch (Exception e) {
                            statement.setTimestamp(7, null);
                            e.printStackTrace();
                        }
                        try {
                            statement.setInt(8, 6);
                        } catch (Exception e) {
                            statement.setInt(8, 0);
                        }
                        try {
                            statement.setInt(9, 2017);
                        } catch (Exception e) {
                            statement.setInt(9, 0);
                        }
                        Calendar cal = Calendar.getInstance();
                        statement.setTimestamp(10, new Timestamp(cal.getTimeInMillis()));
                        statement.setBoolean(11, false);
                        statement.setString(12, "");
                        statement.setString(13, "");
                        statement.setString(14, "");
                        statement.setString(15, "us");
                        statement.setString(16, "08540");
                        statement.setString(17, "");
                        
                        statement.setString(18,"");
                        statement.setString(19,"");
                        statement.setInt(20,1);
                        return statement;
                    }
                }, keyHolder);
            }
            logger.info("Credit Card is saved in lxrmarketplace database for user with id = "+ userId);
            return (Long) keyHolder.getKey();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }
}
