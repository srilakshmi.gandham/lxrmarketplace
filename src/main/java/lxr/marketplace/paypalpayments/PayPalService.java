package lxr.marketplace.paypalpayments;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Authorization;
import com.paypal.api.payments.Capture;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.CreditCardToken;
import com.paypal.api.payments.FundingInstrument;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.PayPalRESTException;


public class PayPalService {
	
	private static Logger logger = Logger.getLogger(PayPalService.class);
	private Properties paypalProperties; 
	private LxrMPaymentService lxrMPaymentService;
	
	public void setPaypalProperties(Properties paypalProperties) {
		this.paypalProperties = paypalProperties;
	}

    public void setLxrMPaymentService(LxrMPaymentService lxrMPaymentService) {
        this.lxrMPaymentService = lxrMPaymentService;
    }

    /**
	 * First authorizes and then saves an user provided credit card into PayPal database.
	 * If there is error in authorizing or saving the credit card, it sends message through 
	 * the reference object 'errorMessage'
	 * 
	 * @param lxrCreditCard: contains user provided details about his/her credit/debit card  
	 * @param errorMessage: passed a reference object to get error message from PayPal
	 * @return PayPal CreditCard Object
	 */
	
	public CreditCard saveCreditCard(LxrMCreditCard lxrCreditCard, String [] errorMessage, long userId) {       
		CreditCard savedCreditCard = null;
		String accessToken = null;	
		Authorization authorization = null;
		accessToken = createAccessToken();
		logger.debug("Access Token created to save credit card. Token: " + accessToken);
		
		//Authorizing credit card
		CreditCard creditCard = getPayPalCardFromLxrMCard(lxrCreditCard);
		logger.debug("creditCard "+ creditCard);
		StringBuilder sb = new StringBuilder();
		Amount amount = new Amount();
		amount.setCurrency("USD");
		amount.setTotal("0.05");
		authorization = getCardAuthorization(lxrCreditCard.getUserId(), creditCard, amount, sb);
		if(accessToken != null && authorization != null && authorization.getState() != null 
				&& authorization.getState().equals("authorized")){	
			logger.info("Credit Card authorized for user "+lxrCreditCard.getUserId()
					+". Trying to save this card in paypal");
			
			try {			    
				savedCreditCard = creditCard.create(accessToken);   //saving credit card in paypal
				lxrMPaymentService.savePayPalAuthorization(authorization, userId, 12, 0);
				authorization.doVoid(createApiContext());           //voiding the authorization
			} catch(PayPalRESTException e) {
				logger.error("Exception in saving credit card in paypal.\nERROR MESSAGE: " 
						+ e.getMessage());
				e.printStackTrace();
				if(errorMessage.length == 3){
				    errorMessage[2] = "Card Saving Failure - "+e.getMessage();
                }
				//TODO: add error message in errorMessage 
				return null;
			}
			if(savedCreditCard != null){
				logger.info("Credit card is saved in PayPal for user "+lxrCreditCard.getUserId());
			}else{
				logger.error("Credit card is not saved in PayPal for user "+lxrCreditCard.getUserId());
			}
		}else{
			if(errorMessage[0].equals("")){
				errorMessage[0] = "2";
				errorMessage[1] = "Failed to Authorize this card through PayPal. " +
					"Use a different Credit / Debit Card or Try Again.";
				if(errorMessage.length == 3){
				    if(accessToken == null){
				        errorMessage[2] = "Access Token Creation Failure";
				    }else if(authorization == null || authorization.getState() == null 
			                || !authorization.getState().equals("authorized")){
				        errorMessage[2] = "Authorization Failure - "+sb.toString();
				    }
				}
			}
			logger.error("!!Failure in authorizing credit card for user "
					+lxrCreditCard.getUserId()+" Error Message: " + errorMessage[1]);
		}	
		return savedCreditCard;
	}
	
	/**
	 * Sale transaction through PayPal using stored card
	 * As this method failed in some cases in production, we are not going to use this method
	 * 
	 * @param lxrMCreditCard
	 * @param amount
	 * @param failurMessage reference passed from calee to get any error message from 
	 * 		  transaction process
	 * @return Payment object containing information about the transaction
	 */
//	public Payment payByStoredCreditDebitCard(LxrMCreditCard lxrMCreditCard, Amount amount, 
//			StringBuilder failurMessage) {
//		Payment createdPayment = null;
//		APIContext apiContext = createApiContext();
//		
//		if(!(apiContext == null || lxrMCreditCard == null)) {	
//			CreditCardToken creditCardToken = new CreditCardToken();
//			creditCardToken.setCreditCardId(lxrMCreditCard.getPayPalCardId());
//	
//			Transaction transaction = new Transaction();
//			transaction.setAmount(amount);
//			transaction.setDescription("LXRSEO subscription renewal, userId=" + lxrMCreditCard.getUserId());
//	
//			List<Transaction> transactions = new ArrayList<Transaction>();
//			transactions.add(transaction);
//	
//			FundingInstrument fundingInstrument = new FundingInstrument();
//			fundingInstrument.setCreditCardToken(creditCardToken);
//	
//			List<FundingInstrument> fundingInstrumentList = new ArrayList<FundingInstrument>();
//			fundingInstrumentList.add(fundingInstrument);
//	
//			Payer payer = new Payer();
//			payer.setFundingInstruments(fundingInstrumentList);
//			payer.setPaymentMethod("credit_card");
//	
//			Payment payment = new Payment();
//			payment.setIntent("sale");
//			payment.setPayer(payer);
//			payment.setTransactions(transactions);
//	
//			try {
//				createdPayment = payment.create(apiContext);
//				logger.info("PayPal payment of amount " +amount.getTotal() +" made by user "
//						+lxrMCreditCard.getUserId() + " from his card (paypal_card_id) "
//						+lxrMCreditCard.getPayPalCardId());
//			} catch (PayPalRESTException e) {
//				logger.error("Exception in paypal transaction for user with id = "+lxrMCreditCard.getUserId() + 
//						"\nERROR MESSAGE: " +e.getMessage()+"ERROR CAUSE: "+e.getCause());
//				failurMessage.append(e.getMessage());
//				e.printStackTrace();
//				//TODO: Handle the exceptions properly (Read timed out)
//			}
//		}
//		return createdPayment;
//	}
	
	/**
	 * first authorizes an amount from stored card and immediately captures that amount
	 * 
	 * @param lxrMCreditCard
	 * @param amount
	 * @param failurMessage
	 * @return
	 */
	public Capture payByStoredCardAuthorizeCapture(String payPalCardId, Amount amount,StringBuilder failurMessage) {
		Capture responseCapture = null;
		Payment responsePayment = authorizeByStoredCreditDebitCard(payPalCardId, 
				amount, failurMessage);
		
		if(responsePayment != null){
			Authorization authorization = responsePayment.getTransactions().get(0)
									.getRelatedResources().get(0).getAuthorization();
			if(authorization != null){
				
				responseCapture = captureFromAuthorization(authorization, amount, failurMessage);
			}
		}
		return responseCapture;
	}	
	
	/*  FOR LXRMARKETPLACE TOOL PAYMENTS */
	public Capture payByCardAuthorizeCapture(CreditCard creditCard, Amount amount,StringBuilder failurMessage,long userId) {
		Capture responseCapture = null;
		Authorization authorization = getCardAuthorization(userId,creditCard, amount, failurMessage);
		
		if(authorization != null){
//			creditCard = getPaypalCardInfo(creditCard,authorization,errorMessage);
			responseCapture = captureFromAuthorization(authorization, amount, failurMessage);
		}
		return responseCapture;
	}
	
	/*AUTHORIZING THE CARD DIRCTLY FOR THE SPECIFIED AMOUNT ACCORDING TO TOOL*/
	public Authorization payByCardAuthorize(CreditCard creditCard, Amount amount,StringBuilder failurMessage,long userId,String [] errorMessage) {
		
		Authorization authorization = getCardAuthorization(userId,creditCard, amount, failurMessage);		
		return authorization;
	}
	
	/*CAPTURING THE AMOUNT FROM THE AUTHORIZED CARD DETAILS*/
	public Capture payByCardCapture(CreditCard creditCard,Authorization authorization, Amount amount,StringBuilder failurMessage,long userId,String [] errorMessage){
		Capture responseCapture = null;
		if(authorization != null){
			responseCapture = captureFromAuthorization(authorization, amount, failurMessage);
		}
		return responseCapture;
	}
	
	
	/*SAVING CARD INFO IN PAYPAL*/
	public CreditCard getPaypalCardInfo(CreditCard creditCard,Authorization authorization,String [] errorMessage){
		logger.debug(" authorization"+ authorization);
		CreditCard savedCreditCard = null;
		String accessToken = null;
		accessToken = createAccessToken();
		if(accessToken != null && authorization != null && authorization.getState() != null 
				&& authorization.getState().equals("authorized")){	
			logger.info("Credit Card authorized for user "+". Trying to save this card in paypal");
			try {			    
				savedCreditCard = creditCard.create(accessToken);   //saving credit card in paypal
//				lxrMPaymentService.savePayPalAuthorization(authorization, userId, 12, 0);
//				authorization.doVoid(createApiContext());           //voiding the authorization
			} catch(PayPalRESTException e) {
				logger.error("Exception in saving credit card in paypal.\nERROR MESSAGE: " 
						+ e.getMessage());
				e.printStackTrace();
				//TODO: add error message in errorMessage 
				return null;
			}
			if(savedCreditCard != null){
				logger.info("Credit card is saved in PayPal for user ");
			}else{
				logger.debug("Credit card is not saved in PayPal for user ");
			}
		}else{
			if(errorMessage[0].equals("")){
				errorMessage[0] = "2";
				errorMessage[1] = "Failed to Authorize this card through PayPal. " +
					"Use a different Credit / Debit Card or Try Again.";
			}
			logger.error("!!Failure in authorizing credit card for user "
				+" Error Message: " + errorMessage[1]);
		}
		return savedCreditCard;
		
	}
	
	private Authorization getCardAuthorization(long userId, CreditCard creditCard,Amount amount, StringBuilder sb){
		APIContext apiContext = createApiContext();
		Authorization authorization = null;
		logger.info("Authorizing card for user "+userId +" with amount = $" + amount.getTotal());
		// ###Details
		// Let's you specify details of a payment amount.

		// ###Amount
		// Let's you specify a payment amount.


		// ###Transaction
		// A transaction defines the contract of a
		// payment - what is the payment for and who
		// is fulfilling it. Transaction is created with
		// a `Payee` and `Amount` types
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("LXRMarketplace authorization for user with lxrm_user_id: "+userId);

		// The Payment creation API requires a list of
		// Transaction; add the created `Transaction`
		// to a List
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		// ###FundingInstrument
		// A resource representing a Payeer's funding instrument.
		// Use a Payer ID (A unique identifier of the payer generated
		// and provided by the facilitator. This is required when
		// creating or using a tokenized funding instrument)
		// and the `CreditCardDetails`
		FundingInstrument fundingInstrument = new FundingInstrument();
		fundingInstrument.setCreditCard(creditCard);

		// The Payment creation API requires a list of
		// FundingInstrument; add the created `FundingInstrument`
		// to a List
		List<FundingInstrument> fundingInstruments = new ArrayList<FundingInstrument>();
		fundingInstruments.add(fundingInstrument);

		// ###Payer
		// A resource representing a Payer that funds a payment
		// Use the List of `FundingInstrument` and the Payment Method
		// as 'credit_card'
		Payer payer = new Payer();
		payer.setFundingInstruments(fundingInstruments);
		payer.setPaymentMethod("credit_card");

		// ###Payment
		// A Payment Resource; create one using
		// the above types and intent as 'authorize'
		Payment payment = new Payment();
		payment.setIntent("authorize");
		payment.setPayer(payer);
		payment.setTransactions(transactions);

		Payment responsePayment;
		int iteration = 3;
		boolean authorized = false;
		boolean otherException = false;
		while (iteration > 0 && !authorized && !otherException){
			try {
				responsePayment = payment.create(apiContext);
				authorization = responsePayment.getTransactions().get(0).getRelatedResources().get(0).getAuthorization();
				logger.info("authorization is " + authorization);
				authorized = true;
			} catch (PayPalRESTException e) {
				//TODO: needs exception handling
				sb.append(e.getMessage());
				logger.error("AUTHORIZATION ERROR MESSAGE: "+e.getMessage());
				logger.error("!!Exception in authorizing credit card for user "+userId);
				e.printStackTrace();
				if(!e.getMessage().contains("Read timed out")){//TODO: check for internal server error exception
					otherException = true;
					//TODO: add error message properly depending on exception
				}else{				
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}catch(Exception e){
			    sb.append("Exception in authorising the Card");
			    otherException = true;
			}
			iteration --;
		}
		return authorization;
	}
	

	
	private CreditCard getPayPalCardFromLxrMCard(LxrMCreditCard lxrCreditCard){		
		CreditCard creditCard = new CreditCard();
		creditCard.setNumber(lxrCreditCard.getCardNo());
		creditCard.setExpireMonth(lxrCreditCard.getExpireMonth());
		creditCard.setExpireYear(lxrCreditCard.getExpireYear());
		creditCard.setFirstName(lxrCreditCard.getFirstName());
		creditCard.setLastName(lxrCreditCard.getLastName());
		creditCard.setType(lxrCreditCard.getType());
		
//		Address billingAddress = new Address();
//		billingAddress.setLine1(lxrCreditCard.getAddressLine1());
//		billingAddress.setLine2(lxrCreditCard.getAddressLine2());
//		billingAddress.setCity(lxrCreditCard.getCity());
//		billingAddress.setCountryCode(lxrCreditCard.getCountryCode());
//		billingAddress.setPostalCode(lxrCreditCard.getPostalCode());
//		billingAddress.setState(lxrCreditCard.getState());		
//		creditCard.setBillingAddress(billingAddress);
		return creditCard;
	}
	
	public Payment authorizeByStoredCreditDebitCard(String paypalCardId, Amount amount, 
			StringBuilder failurMessage) {
		Payment createdPayment = null;
		APIContext apiContext = createApiContext();
		
		if(apiContext != null && paypalCardId != null && amount != null) {
			CreditCardToken creditCardToken = new CreditCardToken();
			creditCardToken.setCreditCardId(paypalCardId);
	
			Transaction transaction = new Transaction();
			transaction.setAmount(amount);
			transaction.setDescription("LXRSEO subscription authorization");
	
			List<Transaction> transactions = new ArrayList<Transaction>();
			transactions.add(transaction);
	
			FundingInstrument fundingInstrument = new FundingInstrument();
			fundingInstrument.setCreditCardToken(creditCardToken);
	
			List<FundingInstrument> fundingInstrumentList = new ArrayList<FundingInstrument>();
			fundingInstrumentList.add(fundingInstrument);
	
			Payer payer = new Payer();
			payer.setFundingInstruments(fundingInstrumentList);
			payer.setPaymentMethod("credit_card");
	
			Payment payment = new Payment();
			payment.setIntent("authorize");
			payment.setPayer(payer);
			payment.setTransactions(transactions);
	
			try {
				createdPayment = payment.create(apiContext);
			} catch (PayPalRESTException e) {
				logger.error("Exception in paypal transaction for user with paypal card id= "+ paypalCardId+
						"\nERROR MESSAGE: " +e.getMessage()+"ERROR CAUSE: "+e.getCause());
				failurMessage.append(e.getMessage());
			}
		}
		return createdPayment;
	}

	public Capture captureFromAuthorization(Authorization authorization, Amount amount, StringBuilder failurMessage) {
		Capture responseCapture = null;
		APIContext apiContext = createApiContext();
		if(authorization!= null && apiContext != null && amount != null) {		
			Capture capture = new Capture();
            capture.setAmount(amount);
            capture.setIsFinalCapture(true);
            try {
				responseCapture = authorization.capture(apiContext, capture);
			} catch (PayPalRESTException e) {
				logger.error("Paypal Rest API Exception when capturing an authorized amount.\n" +
						"Failure Message: " + e.getMessage());
				e.printStackTrace();
				failurMessage.append(e.getMessage());
			}
		}
		return responseCapture;
	}
	
	/** 
	 * Creating API Context needed for requesting PayPal rest API 
	 * It first creates access token then creates apicontext from it
	 */
	private APIContext createApiContext(){
		APIContext apiContext = null;
		String accessToken = createAccessToken();
		
		if(accessToken != null){
			try{
				apiContext = new APIContext(accessToken);
			} catch(Exception e){
				logger.error("PayPal apicontext is not created due to exception");
				e.printStackTrace();
			}
		}		
		return apiContext;
	}
	
	/** 
	 * Creating Access Token needed for requesting paypal rest api 
	 * It uses PayPal resource properties from sdk_config.properties file configured through 
	 * application-context.xml 
	 */
	
	private String createAccessToken(){		
		String accessToken = null;
		if(paypalProperties != null){
			Map<String, String> map = new HashMap<String, String>();
			for (String key : paypalProperties.stringPropertyNames()) {
			    map.put(key, paypalProperties.getProperty(key));
			}
			String clientId = paypalProperties.getProperty("clientID");
			String clientSecret = paypalProperties.getProperty("clientSecret");		
			
			try {
				OAuthTokenCredential tokenCredential = new OAuthTokenCredential(clientId, clientSecret, map);
				accessToken = tokenCredential.getAccessToken();				
			} catch (PayPalRESTException e1) {
				logger.error("Exception in creating accessToken for paypal request.");
				e1.printStackTrace();
			} catch (NullPointerException e){
				logger.error("Exception in creating accessToken for paypal request due to null value.");
				e.printStackTrace();
			}
		}
		return accessToken;
	}
	

	/** 
	 * Method to capture payment from previous authorization 
	 * authorizationId: id provided by paypal when authorized
	 * amount: amount need to be captured
	 * this method is not in use now
	 */
	
	public Capture captureFromPreviousAuthorization(String authorizationId, Amount amount){ 
		Authorization authorization = createAuthorizationObjFromId(authorizationId);
		StringBuilder failurMessage = new StringBuilder();
		Capture responseCapture = captureFromAuthorization(authorization, amount, failurMessage);
		return responseCapture;
	}
	
	/** 
	 * Method to create authorization object from authorization id 
	 * authorizationId: id provided by PayPal when authorized
	 * this method is not in use now
	 */
	
	private Authorization createAuthorizationObjFromId(String authorizationId){
		APIContext apiContext = createApiContext();
		if(apiContext == null) {
			return null;
		}
		try {
			Authorization authorization = Authorization.get(apiContext, authorizationId);
			return authorization;
		} catch (PayPalRESTException e1) {
			logger.error("No Authorization found with authorization id as : " + authorizationId);
			e1.printStackTrace();
			return null;
		}
	}
	
	
	
	
	/**
	 * NOT IN USE
	 * Creates PayPal URL to direct user to PayPal site, where user can pay through their PayPal
	 * account   
	 * @param amount
	 * @param cancelUrl
	 * @param redirectUrl In Case of failure, where to redirect in our site
	 * @param map
	 * @return final URL
	 */
	public String createPayPalUrl(Amount amount, String cancelUrl, String redirectUrl, 
			Map<String, String> map) {
		String approvalUrl = null;
		APIContext apiContext = createApiContext();
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("paypal transaction");

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = new Payment();
		payment.setIntent("authorize");
		payment.setPayer(payer);
		payment.setTransactions(transactions);

		// ###Redirect URLs
		RedirectUrls redirectUrls = new RedirectUrls();
		String guid = UUID.randomUUID().toString().replaceAll("-", "");
		logger.debug("GUID: " + guid);
		// redirectUrls.setCancelUrl(req.getScheme() + "://"
		// + req.getServerName() + ":" + req.getServerPort()
		// + req.getContextPath() + "/paymentwithpaypal?guid=" + guid);
		// redirectUrls.setReturnUrl(req.getScheme() + "://"
		// + req.getServerName() + ":" + req.getServerPort()
		// + req.getContextPath() + "/paymentwithpaypal?guid=" + guid);
		redirectUrls.setCancelUrl(cancelUrl + "?guid=" + guid);
		redirectUrls.setReturnUrl(redirectUrl+ "?guid=" + guid);
		payment.setRedirectUrls(redirectUrls);

		try {
			Payment createdPayment = payment.create(apiContext);
			logger.info("Created paypal account payment with id = "
					+ createdPayment.getId() + " and status = " + createdPayment.getState());
			Iterator<Links> links = createdPayment.getLinks().iterator();
			while (links.hasNext()) {
				Links link = links.next();
				if (link.getRel().equalsIgnoreCase("approval_url")) {
					approvalUrl = link.getHref();
					logger.debug("Approval URL: " + approvalUrl);
				}
			}
			map.put(guid, createdPayment.getId());
		} catch (PayPalRESTException e) {
			logger.error(e.getMessage());
		}
		return approvalUrl;
	}
	
	/**
	 * NOT IN USE
	 * @param payerId
	 * @param guid
	 * @param map
	 * @return
	 */
	public Payment confirmPayPalPayment(String payerId, String guid, Map<String, String> map){
		Payment createdPayment = null;
		APIContext apiContext = createApiContext();
		if (payerId != null) {
			Payment payment = new Payment();
			if (guid != null) {
				payment.setId(map.get(guid));
			}
	
			PaymentExecution paymentExecution = new PaymentExecution();
			paymentExecution.setPayerId(payerId);
			try {
				createdPayment = payment.execute(apiContext, paymentExecution);
				logger.info("Paypal transaction confirmation response:\n"+  Payment.getLastResponse());
			} catch (PayPalRESTException e) {
				logger.info("Exception in confirming paypal transaction confirmation");
				e.printStackTrace();
			}
		}
		return createdPayment;
	}
}


