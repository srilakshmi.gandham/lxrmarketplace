package lxr.marketplace.paypalpayments;

public class LxrMCreditCard {

    public static int PAYPAL = 1;
    public static int AUTHORIZE = 2;

    private long lxrCardId;
    private long userId;
    private String payPalCardId;
    private String cardNo;
    private String firstName;
    private String lastName;
    private String type;
    private String cvv;

    private int expireMonth;
    private int expireYear;
    private String validUntil;

    private long custProfileId;
    private long paymentProfileId;
    private int serviceProvider;

    /**
     * ******** Address *******
     */
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String countryCode;
    private String postalCode;
    private String state;

    private long promoId;
    
    private long paymentId;
    private String serviceProviderCardId;
    private String amount;

    public long getCustProfileId() {
        return custProfileId;
    }

    public void setCustProfileId(long custProfileId) {
        this.custProfileId = custProfileId;
    }

    public long getPaymentProfileId() {
        return paymentProfileId;
    }

    public void setPaymentProfileId(long paymentProfileId) {
        this.paymentProfileId = paymentProfileId;
    }

    public int getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(int serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public long getLxrCardId() {
        return lxrCardId;
    }

    public void setLxrCardId(long lxrCardId) {
        this.lxrCardId = lxrCardId;
    }

    public String getPayPalCardId() {
        return payPalCardId;
    }

    public void setPayPalCardId(String payPalCardId) {
        this.payPalCardId = payPalCardId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(int expireMonth) {
        this.expireMonth = expireMonth;
    }

    public int getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(int expireYear) {
        this.expireYear = expireYear;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public long getPromoId() {
        return promoId;
    }

    public void setPromoId(long promoId) {
        this.promoId = promoId;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }
    
    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }


    public String getServiceProviderCardId() {
        return serviceProviderCardId;
    }

    public void setServiceProviderCardId(String serviceProviderCardId) {
        this.serviceProviderCardId = serviceProviderCardId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    
}
