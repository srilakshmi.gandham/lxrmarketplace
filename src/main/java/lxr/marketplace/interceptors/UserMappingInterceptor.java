package lxr.marketplace.interceptors;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import lxr.marketplace.admin.automail.EmailTrackingInfoService;
import lxr.marketplace.admin.settings.URLRedirection;
import lxr.marketplace.apiaccess.SocialChannelsUserOAuthService;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.SessionService;
import lxr.marketplace.tracking.IPToolUsageService;
import lxr.marketplace.tracking.ToolUsageByIP;
import lxr.marketplace.user.CookieService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.PageEntry;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

public class UserMappingInterceptor extends HandlerInterceptorAdapter {

    private final static Logger LOGGER = Logger.getLogger(UserMappingInterceptor.class);

    private LoginService loginService;
    private SessionService sessionService;
    private CookieService cookieService;
    private IPToolUsageService iPToolUsageService;
    private String supportMail;
    private EmailTrackingInfoService emailTrackingInfoService;
    private final EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;
    @Autowired
    private ToolService toolService;
    @Autowired
    private SocialChannelsUserOAuthService socialChannelsUserOAuthService;

    public UserMappingInterceptor() {
        super();
        encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
    }

    private static final int MAX_TOOL_USAGE_LIMIT = 1;

    public EmailTrackingInfoService getEmailTrackingInfoService() {
        return emailTrackingInfoService;
    }

    public void setIPToolUsageService(IPToolUsageService iPToolUsageService) {
        this.iPToolUsageService = iPToolUsageService;
    }

    public void setEmailTrackingInfoService(
            EmailTrackingInfoService emailTrackingInfoService) {
        this.emailTrackingInfoService = emailTrackingInfoService;
    }

    public void setCookieService(CookieService cookieService) {
        this.cookieService = cookieService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        LOGGER.debug("In Interceptor for url: " + request.getRequestURI() + "?" + request.getQueryString() + "  and getCharacterEncoding" + request.getCharacterEncoding());

        HttpSession session = request.getSession(true);
        Session userSession = (Session) session.getAttribute("userSession");

        String applicationUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();

        /*-------------Blocking user by IP Address-------------*/
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        List<String> discardedIpsList = new ArrayList<>();
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
        if (context.getServletContext().getAttribute("discardedIps") != null) {
            discardedIpsList = (List<String>) context.getServletContext().getAttribute("discardedIps");
        }
        if (request.getRequestURI().contains("accessdenied")) {
            return true;
        }
        if (discardedIpsList != null && !discardedIpsList.isEmpty()) {
            for (int i = 0; i < discardedIpsList.size(); i++) {
                if (ipAddress.equals(discardedIpsList.get(i))) {
                    response.sendRedirect(applicationUrl + "/accessdenied.html");
                    return true;
                } else {
                    if (session.getAttribute("ipAddress") != null) {
                        session.removeAttribute("ipAddress");
                    }
                    session.setAttribute("ipAddress", ipAddress);
                }
            }
        }

        /*Social channel login Oauth URL generation*/
        if (session.getAttribute("facebookLoginOAuthURL") == null) {
            session.setAttribute("facebookLoginOAuthURL", socialChannelsUserOAuthService.getFacebookLoginOAuthURL());
        }
        if (session.getAttribute("googleLoginOAuthURL") == null) {
            session.setAttribute("googleLoginOAuthURL", socialChannelsUserOAuthService.getGoogleLoginOAuthUrl());
        }

        /*------------- Cookie checking or setting, user object setting -------------*/
        Login user = null;
        if (session.getAttribute("user") == null && !request.getRequestURI().contains("rest")) {

            Cookie cookie = cookieService.getCookie(response, request);
            Long guestUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(cookie.getValue()));
            if (guestUserId >= 0) {
                user = loginService.find(guestUserId);
                //If any user modifies his cookie in his browser and there is no user table entry for that value
                if (user == null) {
                    boolean islocal = false;
                    cookie = cookieService.createCookie(response, islocal, false);
                    cookie.setPath("/");
                    guestUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(cookie.getValue()));
                    user = loginService.find(guestUserId);
                }
            } else {   //Set cookie as -1 on users first request
                user = new Login();
                user.setId(-1l);
                user.setActive(true);
                user.setAdmin(false);
                user.setUnsubscribed(false);
            }

            Common.getUserInformation(request, userSession);
            if (userSession.getIp() != null) {
                LOGGER.info("IP in user session looking for usage count::: " + userSession.getIp());
                ToolUsageByIP toolUsageByIP = iPToolUsageService.fetchAllToolUsageByIP(userSession.getIp());
                session.setAttribute("toolUsageByIP", toolUsageByIP);
            }

            userSession.setUserId(guestUserId);
            session.setAttribute("marketplaceCookie", cookie);
            session.setAttribute("user", user);
            session.setAttribute("userLoggedIn", false);

            if (!user.isAdmin() && userSession.getUserId() > 0) {
                sessionService.insert(userSession);
            }

            if (request.getQueryString() != null && request.getQueryString().contains("&lxrmp")) {
                LOGGER.debug(applicationUrl + request.getRequestURI() + "?" + request.getQueryString().replace("&lxrmp", ""));
                response.sendRedirect(request.getRequestURI() + "?" + request.getQueryString().replace("&lxrmp", ""));
                return false;
            }

            if (request.getQueryString() != null && (request.getQueryString().contains("getResult")
                    || request.getQueryString().contains("download")
                    || request.getQueryString().contains("cancelSettingsData"))) {
                response.sendRedirect(request.getRequestURI());
                return false;
            }

        }

        /*------------- To set tools for home page based on user Id-------------*/
        if ((session.getAttribute("user") != null && session.getAttribute("sortedToolsBasedOnUser") == null)
                || (session.getAttribute("user") != null && session.getAttribute("getUserMostUsedTools") != null
                && (boolean) session.getAttribute("getUserMostUsedTools"))) {
            user = (Login) session.getAttribute("user");
            Map<Integer, Tool> newUserSortedTools = (Map<Integer, Tool>) context.getServletContext().getAttribute("tools");
            if (user.getId() > 0) {
                Map<Integer, Tool> sortedToolsBasedOnUser = toolService.populateToolsOnUserBasis(user.getId(), newUserSortedTools);
                if (sortedToolsBasedOnUser == null) {
                    sortedToolsBasedOnUser = newUserSortedTools;
                }
                session.setAttribute("sortedToolsBasedOnUser", sortedToolsBasedOnUser);
                session.setAttribute("getUserMostUsedTools", false);
            } else {
                session.setAttribute("sortedToolsBasedOnUser", newUserSortedTools);
            }

        }

        /*-------------       Redirecting user based on login status , Restricting Tool Usage By IP       -------------*/
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        String requestURI = request.getRequestURI();
        ServletContext ctx = request.getServletContext();
        if (userLoggedIn) {
            if (requestURI.contains("/signup.html")) {
                response.sendRedirect(applicationUrl + "/lxrmarketplace.html");
                return false;
            }
        }
        /*When tool usage is reached to limit, If user want to get tool analysis report by opting send mail allowing hime to send/get email*/
        boolean sendMailStatus = ((WebUtils.hasSubmitParameter(request, "download") && (request.getParameter("download").equalsIgnoreCase("'sendmail'")
                || request.getParameter("download").equalsIgnoreCase("sendmail")))
                || (WebUtils.hasSubmitParameter(request, "request") && request.getParameter("request").equalsIgnoreCase("sendmail")));
        if (!userLoggedIn) {
            if (requestURI.contains("/lxrm-payment.html") || requestURI.contains("/lxrm-thanks.html")) {
                response.sendRedirect(applicationUrl + "/lxrmarketplace.html");
                return false;
            }

            if ((request.getQueryString() == null || (request.getQueryString() != null && !request.getQueryString().contains("limit-exceeded=1")
                    && !request.getQueryString().equals("query=%27desc%27") && !request.getQueryString().contains("updatedList")))
                    && !sendMailStatus) {

                String limitExceedStr = "";
                Map<Integer, Tool> toolsMap = (Map<Integer, Tool>) ctx.getAttribute("tools");
                List<Tool> totalTools = new ArrayList<>(toolsMap.values());
                ToolUsageByIP toolUsageByIP = (ToolUsageByIP) session.getAttribute("toolUsageByIP");
                if (toolUsageByIP != null && totalTools.size() > 0) {
                    for (Tool tool : totalTools) {
                        /* requestURI.equals(tool.getToolLink()) && tool.getToolId() == 38) second condition 
                        Tool no: 38 is tool advisor. So we need to track the tool advisor 
                        but tool advisor doesnot have any specified toolLink:"/"
                        Tool No: 0 is Ask the expert Tool link is empty this will satify with each and every link
                         */
                        if ((requestURI.equals("/" + tool.getToolLink()) || ((requestURI.equals("/") || requestURI.equals("/tool-advisor.json")) && tool.getToolId() == 38)) && tool.getToolId() != 0) {
                            long toolUsageCount = 0;
                            if (toolUsageByIP.getToolUsage().containsKey(tool.getToolId())) {
                                toolUsageCount = toolUsageByIP.getToolUsage().get(tool.getToolId());
                            }
                            if (userSession.getToolUsage() != null && userSession.getToolUsage().containsKey(tool.getToolId())) {
                                toolUsageCount += userSession.getToolUsage().get(tool.getToolId()).getCount();
                            }
                            session.setAttribute("toolUsageCount", toolUsageCount);
                            if (toolUsageCount >= MAX_TOOL_USAGE_LIMIT) {
                                if (tool.getToolId() == 40 && !userLoggedIn && (request.getQueryString() != null && request.getQueryString().toLowerCase().contains("analyze"))) {
                                    session.setAttribute(Common.MICRO_INFLUENCER_GENERATOR, toolUsageCount);
                                }
                                if (toolUsageCount == MAX_TOOL_USAGE_LIMIT && request.getQueryString() != null && request.getQueryString().toLowerCase().contains("ate")) {
                                    return true;
                                } else {
                                    if (request.getQueryString() != null && request.getQueryString().toLowerCase().contains("user-id")) {
                                        return true;
                                    } else {
                                        limitExceedStr = "?limit-exceeded=1";
                                    }
                                    response.sendRedirect(request.getRequestURI() + limitExceedStr);
                                    return false;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        List<URLRedirection> urlRedirections = (List<URLRedirection>) ctx.getAttribute("adminURLRedirections");
        if (urlRedirections != null && urlRedirections.size() > 0 && !request.getRequestURI().equals("/login.html")) {
            for (int i = 0; i < urlRedirections.size(); i++) {

                URLRedirection url = urlRedirections.get(i);
                if (url.isStatus()) {
                    if (url.getUrl().toLowerCase().equals(request.getRequestURI().toLowerCase())) {
                        response.sendRedirect(applicationUrl + url.getRedirectionURL());
                        return false;
                    }
                }
            }
        }

        /*------- On sendmail request from tools, updating user table by the new email id  -------*/
 /*if (WebUtils.hasSubmitParameter(request, "download")
                && (request.getParameter("download").equalsIgnoreCase("'sendmail'")
                || request.getParameter("download").equalsIgnoreCase("sendmail"))
                || (WebUtils.hasSubmitParameter(request, "request") && request.getParameter("request").equalsIgnoreCase("sendmail"))) {*/
        if (sendMailStatus) {
            user = (Login) session.getAttribute("user");
            String sendMailId = (String) session.getAttribute("sendMailId");
            String toAddrs = request.getParameter("email");
            String toolType = request.getParameter("toolType");
            if (toolType != null) {
                LOGGER.info("toolType:: " + toolType);
            }
            if ((toAddrs != null && !toAddrs.contains("@lxrmarketplace.com") && toolType == null)) {
                if (sendMailId == null || !sendMailId.equals(toAddrs)) {
                    if (userLoggedIn && user != null && (user.getUserName() == null || user.getUserName().isEmpty())) {
                        user.setUserName(toAddrs);
                        loginService.updateEmail(request, user.getId(), user.getUserName());
                    }
                    if (user != null && user.getName() == null) {
                        if (user.getActivationDate() == null) {
                            Calendar cal = Calendar.getInstance();
                            user.setActivationDate(cal);
                            loginService.updateActDate(user.getId(), new Timestamp(cal.getTimeInMillis()), 1);
                            String subject = EmailBodyCreator.subjectOnWelcome;
                            String bodyText = EmailBodyCreator.bodyForSendMailOnActivation(toAddrs);
                            SendMail.sendMail(supportMail, toAddrs, subject, bodyText);
                        }
                        user.setUserName(toAddrs);
                        String guestUserEmail = "GuestUser" + user.getId() + "<" + toAddrs + ">";
                        loginService.updateEmail(request, user.getId(), guestUserEmail);
                    }
                    session.setAttribute("sendMailId", toAddrs);
                }
            }
        }
        /*if (WebUtils.hasSubmitParameter(request, "download")) {}*/
 /*-------------       Page Tracking     -------------*/
        String uri = request.getRequestURI();
        if (!(uri.equals("/login.html") || uri.equals("/usersession.html") || uri.equals("/dashboard.html")) && !request.getRequestURI().contains("rest")) {
            //other than iframe request and page closed request
            String query = request.getQueryString();
            PageEntry pageEntry;
            if (query != null && !query.equals("") && !query.equals("null")) {
                pageEntry = new PageEntry(uri, query, Calendar.getInstance());
            } else {
                pageEntry = new PageEntry(uri, Calendar.getInstance());
            }
            userSession.getPageEntryList().add(pageEntry);
        }
        if (request.getQueryString() != null && request.getQueryString().contains("limit-exceeded=1")) {
            response.addHeader("limitexceed", "1");
        }

        return super.preHandle(request, response, handler);
    }
}
