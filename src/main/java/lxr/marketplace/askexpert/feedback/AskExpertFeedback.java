/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.askexpert.feedback;

/**
 *
 * @author vemanna
 */
public class AskExpertFeedback {

    private long questionId;
    private int rating;
    private String comment;

    public AskExpertFeedback() {

    }

    public AskExpertFeedback(long questionId, int rating) {
        this.questionId = questionId;
        this.rating = rating;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
