/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.askexpert.feedback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.ResultSetWrappingSqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class AskExpertFeedbackService {

    private static Logger logger = Logger.getLogger(AskExpertFeedbackService.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private String supportMail;

    public void updateAskExpertFeedback(AskExpertFeedback askExpertFeedback) {
        String query = "UPDATE lxrm_ate_questions_feedback SET rating = ?, comment = ? WHERE question_id = ?";
        String comment="";
        if(askExpertFeedback.getComment() != null && !"".equals(askExpertFeedback.getComment())){
            comment = askExpertFeedback.getComment().trim();
        }
        this.jdbcTemplate.update(query, askExpertFeedback.getRating(), comment, askExpertFeedback.getQuestionId());
    }

    public int checkingFeedbackCount(long questionId, long userId) {
        int rowsCount = 0;
        String query = "SELECT CASE WHEN "
                + "(SELECT count(*) FROM lxrm_ate_questions WHERE user_id = ? AND question_id = ?) = 0 "
                + "then -1 "
                + "else "
                + "(SELECT count(*) FROM lxrm_ate_questions_feedback WHERE question_id = ?) end count";

        rowsCount = this.jdbcTemplate.queryForObject(query, Integer.class, userId, questionId, questionId);
        return rowsCount;
    }

    public void insertAskExpertFeedback(AskExpertFeedback askExpertFeedback) {
        Timestamp timeStamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String query = "INSERT INTO lxrm_ate_questions_feedback(question_id, rating, comment, created_date) "
                + "values( ?, ?, ?, ?)";
         String comment="";
        if(askExpertFeedback.getComment() != null && !"".equals(askExpertFeedback.getComment())){
            comment = askExpertFeedback.getComment().trim();
        }
        this.jdbcTemplate.update(query, askExpertFeedback.getQuestionId(), askExpertFeedback.getRating(), comment, timeStamp);
    }

    public void updateAskExpertFeedbackMialSent(List<Long> feedbackMailSentQuesIds) {
        String query = "UPDATE lxrm_ate_questions SET is_feedback_mail_sent = ? WHERE question_id = ?";
        this.jdbcTemplate.batchUpdate(query, feedbackMailSentQuesIds, feedbackMailSentQuesIds.size(), (PreparedStatement ps, Long quesId) -> {
            ps.setBoolean(1, true);
            ps.setLong(2, quesId);
        });
    }

    //Should send feedback mail 2days after user getting the answer.
    public SqlRowSet getFeedbackMailUsersList() {
        Timestamp timeStamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String currentDate = (new SimpleDateFormat("yyyy-MM-dd")).format(timeStamp);
        String query = "SELECT ques.question_id, ques.user_id, ques.question, u.email, u.name "
                + "FROM lxrm_ate_questions ques, user u where u.id = ques.user_id AND ques.is_published = 1 "
                + "AND DATE(answer_date) <= SUBDATE('" + currentDate + "', 2) AND ques.is_feedback_mail_sent = 0 "
                + "AND ques.question_id  NOT IN(select question_id from lxrm_ate_questions_feedback);";

        logger.info("Query for feedbac mail:" + query);
        SqlRowSet sqlRowSet = null;
        try {
            sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
        } catch (Exception e) {
            logger.error("Exception on fetching ATE Feedback Mail Users List:", e);
        }

        return sqlRowSet;
    }

    public SqlRowSet fetchingExpertsData(long questionid) {
        String query = "select u.email as userMailId ,e.email as expertMailId,ae.email as reviewerMail,q.question,"
                + "q.answer,t.name as toolName from lxrm_ate_questions q, lxrm_ate_experts e,tool t ,"
                + "lxrm_ate_experts ae,user u where q.expert_id = e.expert_id and q.reviewer_id = ae.expert_id "
                + "and q.tool_id = t.id and q.user_id = u.id and q.question_id=?";
        return this.jdbcTemplate.queryForRowSet(query, questionid);
    }

    public void sendFeedbackMailToExperts(AskExpertFeedback askExpertFeedback) {

        SqlRowSet sqlRowSet = fetchingExpertsData(askExpertFeedback.getQuestionId());
        boolean containsData = sqlRowSet.isBeforeFirst();
        if (containsData) {
            ResultSetWrappingSqlRowSet rss = (ResultSetWrappingSqlRowSet) sqlRowSet;
            ResultSet rs = rss.getResultSet();
            try {
                while (rs.next()) {
                    String userEmailId = rs.getString(1);
                    String expertEmailId = rs.getString(2);
                    String reviewerMailId = rs.getString(3);
                    String question = rs.getString(4);
                    String answer = new String(rs.getBytes(5));
                    String toolName = rs.getString(6);

                    String rating = "";
                    switch (askExpertFeedback.getRating()) {
                        case 2:
                            rating = "Poor";
                            break;
                        case 3:
                            rating = "Fair";
                            break;
                        case 4:
                            rating = "Good";
                            break;
                        case 5:
                            rating = "Excellent";
                            break;
                    }
                    String subject = EmailBodyCreator.subjectOnFeedBackRatingAndComments;
                    String bodyContent = EmailBodyCreator.bodyForFeedBackReport(rating, askExpertFeedback.getComment(), userEmailId, question, answer, toolName);
                    SendMail.sendMail(supportMail, expertEmailId, reviewerMailId, subject, bodyContent);
                }
            } catch (SQLException ex) {
                logger.error("Exception on fetching ATE info in sending feedback mail:", ex);
            }
        }
    }
}
