/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.askexpert.feedback;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
public class AskExpertFeedbackController {

    private static Logger logger = Logger.getLogger(AskExpertFeedbackController.class);
    @Autowired
    private LoginService loginService;
    @Autowired
    private AskExpertFeedbackService askExpertFeedbackService;

    @RequestMapping(value = "/ask-expert-feedback.html", method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("askExpertFeedback", new AskExpertFeedback());
        return "views/askExpertFeedBack/askExpertFeedback";
    }

    @RequestMapping(value = "/ask-expert-feedback-thankyou.html", method = RequestMethod.GET)
    public String ateFeedbackThankyou() {
        return "views/askExpertFeedBack/askExpertFeedbackThankYou";
    }

    @RequestMapping(value = "/ask-expert-feedback.html", params = "check=login", method = RequestMethod.GET)
    public @ResponseBody
    Object checkEncriptedValuesAndLogin(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("rating") int rating, @RequestParam("userId") String encriptedUserId,
            @RequestParam("questionId") String encriptedQuestionId) throws Exception {
        HttpSession session = request.getSession();
        Object data[] = new Object[2];
        boolean userLoggedIn = false, isProcess = true;
        Login login = null;
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                login = (Login) session.getAttribute("user");
            }
        }

        long decriptedUserId;
        EncryptAndDecryptUsingDES encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
        try {
            //Decripting userId came from mail
            decriptedUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(encriptedUserId));
            //If already login then checking login user id and decrpting user id is same or not
            if (login != null) {
                if (decriptedUserId != login.getId()) {
                    isProcess = false;
                }
            }
        } catch (NumberFormatException e) {
            decriptedUserId = 0;
            logger.error("Exception on Decripting the user id:", e);
        }

        if (decriptedUserId != 0 && isProcess) {
            login = loginService.find(decriptedUserId); // Checking userId is present or not
            //If user id present and not admin then insrting feedback and did auto login
            if (login != null) {
//                if (login.isAdmin()) {
//                    session.setAttribute("user", login);
//                    data[0] = "success";
//                    data[1] = getURL(request) + "/dashboard.html";
//                } else {
                session.setAttribute("user", login);
                session.setAttribute("sendMailId", login.getUserName());
                session.setAttribute("userLoggedIn", true);
                data[0] = "success";

                // Inserting feedback
                long questionId = 0;
                try {
                    questionId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(encriptedQuestionId));
                } catch (NumberFormatException e) {
                    logger.error("Exception on Decripting the question id:", e);
                }
                if (questionId != 0) {
                    AskExpertFeedback askExpertFeedback = new AskExpertFeedback(questionId, rating);
                    insertAndUpdateFeedback(askExpertFeedback, login.getId());
                    data[1] = getURL(request) + "/ask-expert-feedback.html?question-id=" + questionId + "&rating=" + rating;
                } else {
                    data[1] = getURL(request) + "/user-messages.html";
                }
//                }
            } else {
                data[0] = "error";
                data[1] = getURL(request) + "/#home";
            }
        } else if (decriptedUserId != 0 && !isProcess) {
            //TODO: This is very rarely scenario, at present we are ignoring
            data[0] = "It seems to be another user logged in. We can't process this request.";
        } else {
            data[0] = "error";
            data[1] = getURL(request) + "/#home";
        }
        return data;
    }

    @RequestMapping(value = "/ask-expert-feedback.html", params = "submit=feedback", method = RequestMethod.POST)
    public String updateAskExpertFeedback(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("askExpertFeedback") AskExpertFeedback askExpertFeedback, ModelMap model) throws IOException {
        HttpSession session = request.getSession();
        Login login = (Login) session.getAttribute("user");
        boolean isUpdated = insertAndUpdateFeedback(askExpertFeedback, login.getId());
        if (askExpertFeedback.getRating() == 2 || !"".equals(askExpertFeedback.getComment().trim())) {
            askExpertFeedbackService.sendFeedbackMailToExperts(askExpertFeedback);
        }
        if (isUpdated) {
            String redirectURL = getURL(request) + "/ask-expert-feedback-thankyou.html";
            response.sendRedirect(redirectURL);
        } else {
            model.addAttribute("errorMessage", "This question is mapped with other user. please select right question.");
        }

        return "views/askExpertFeedBack/askExpertFeedback";
    }

    /**
     * This method is inserting and updating ATE feedback
     *
     * @param askExpertFeedback is object of AskExpertFeedback
     */
    private boolean insertAndUpdateFeedback(AskExpertFeedback askExpertFeedback, long userId) {
        boolean isUpdated = true;
        //Checking already feedback record is available or not, if not then inserting otherwise updating existing record
        int rowsCount = askExpertFeedbackService.checkingFeedbackCount(askExpertFeedback.getQuestionId(), userId);
        if (rowsCount == -1) {
            isUpdated = false;
        } else if (rowsCount > 0) {
            askExpertFeedbackService.updateAskExpertFeedback(askExpertFeedback);
        } else {
            askExpertFeedbackService.insertAskExpertFeedback(askExpertFeedback);
        }
        return isUpdated;
    }

    private String getURL(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }
}
