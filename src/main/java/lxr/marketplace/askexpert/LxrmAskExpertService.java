/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.askexpert;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.Tool;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class LxrmAskExpertService {

    private static Logger logger = Logger.getLogger(LxrmAskExpertService.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Tool getToolInfo(long toolId) {
        Tool tool = new Tool();
        SqlRowSet sqlRowSet = null;
        String query = "select id, name, tool_link from tool where id=?";
        try {
            sqlRowSet = jdbcTemplate.queryForRowSet(query, new Object[]{toolId});
        } catch (DataAccessException e) {
            logger.info("Exception when fetching the tool info");
        }

        if (sqlRowSet != null) {
            while (sqlRowSet.next()) {
                tool.setToolId(sqlRowSet.getInt("id"));
                tool.setToolName(sqlRowSet.getString("name"));
                tool.setToolLink(sqlRowSet.getString("tool_link"));
            }
        }
        return tool;
    }

    public boolean insertAfterPayment(LxrmAskExpert lxrmAskExpert, long paymentId, long userId) {
        Calendar cal = Calendar.getInstance();
        if (paymentId != 0) {
            String query = "insert into lxrm_ate_questions (tool_id, user_id, question, answer, created_date, payment_transaction_id, domain, other_inputs, device_category) values(?,?,?,?,?,?,?,?,?)";
            Object[] params = {lxrmAskExpert.getToolId(), userId, lxrmAskExpert.getQuestion(), "", cal, paymentId, lxrmAskExpert.getDomain(), lxrmAskExpert.getOtherInputs(), lxrmAskExpert.getDeviceCategory()};
            return insertATEQuestion(query, params);
        } else {
            String query = "insert into lxrm_ate_questions (tool_id, user_id, question, answer, created_date, domain, other_inputs, device_category) values(?,?,?,?,?,?,?,?)";
            Object[] params = {lxrmAskExpert.getToolId(), userId, lxrmAskExpert.getQuestion(), "", cal, lxrmAskExpert.getDomain(), lxrmAskExpert.getOtherInputs(), lxrmAskExpert.getDeviceCategory()};
            return insertATEQuestion(query, params);
        }

    }

    public boolean insertATEQuestion(String query, Object[] params) {
        boolean insertStatus = false;
        try {
            jdbcTemplate.update(query, params);
            insertStatus = true;
        } catch (DataAccessException e) {
            logger.error("DataAccessException when fetching the tool info cause: ", e);
        }
        return insertStatus;
    }

    public long getPaymentId(long userId) {
        String query = "select payment_id from lxrm_payment_transaction_stats where "
                + "updated_time = (select max(updated_time) from lxrm_payment_transaction_stats where payer_user_id=?) "
                + "and payer_user_id=?";
        Object[] params = {userId, userId};
        try {
            return jdbcTemplate.queryForObject(query, params, Long.class);
        } catch (DataAccessException e) {
            logger.error("Exception when fetching the tool info", e);
        }
        return 0;
    }

    public void addInJson(LxrmAskExpert lxrmAskExpert, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(0, lxrmAskExpert);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public List<ATEUserMessages> getATEAnswersList(long userId) {

        String query = "select ate.*, fbk.rating, fbk.comment from ((select ques.question_id, ques.question, "
                + "ques.answer,ques.file_name,ques.created_date from lxrm_ate_questions ques where "
                + "ques.user_id =" + userId + " and ques.answer != '' and is_published=true)ate "
                + "LEFT JOIN "
                + "lxrm_ate_questions_feedback fbk ON ate.question_id = fbk.question_id) "
                + "ORDER BY ate.created_date DESC;";

        List<ATEUserMessages> allATEAnswersList = this.jdbcTemplate.query(query, (ResultSet rs, int i) -> {
            ATEUserMessages expertAnswer = new ATEUserMessages();
            expertAnswer.setQuestionId(rs.getLong("question_id"));
            expertAnswer.setUserQuestion(rs.getString("question").trim());
            String answer;
            try {
                answer = new String(rs.getBytes("answer"));
            } catch (Exception e) {
                answer = rs.getString("answer");
                logger.error("Exception converting bytes into string " + e.getMessage());
            }
            expertAnswer.setAdminAnswer(answer.trim());
            expertAnswer.setAttachedFile(rs.getString("file_name"));
            expertAnswer.setAddedDate(rs.getTimestamp("created_date"));
            expertAnswer.setRating(rs.getInt("rating"));
            expertAnswer.setComment(rs.getString("comment"));
            return expertAnswer;
        });
        return allATEAnswersList;
    }

    public SqlRowSet getUnViewdAnswersCount(long userId) {
        String query = "SELECT * FROM "
                + "(SELECT count(*) as unviewed_answers_count FROM lxrm_ate_questions "
                + "WHERE view_status = false AND user_id = ? AND is_published = true) a, "
                + "(SELECT count(*) as total_answers_count FROM lxrm_ate_questions WHERE user_id = ?  AND is_published = true) b";
        SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query, userId, userId);
        return sqlRowSet;
    }

    public void updatedAllUnViewedAnswers(long userId) {
        String query = "UPDATE lxrm_ate_questions SET view_status = true WHERE user_id = ? AND is_published = true";
        this.jdbcTemplate.update(query, userId);
    }
}
