/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.askexpert;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.IntStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.ate.answers.ExpertQuestionsService;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.admin.expertuser.ExpertUserService;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anil
 */
@Controller
public class LxrmAskExpertController {

    private static Logger logger = Logger.getLogger(LxrmAskExpertController.class);

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private String supportMail;
    @Autowired
    private LoginService loginService;
    @Autowired
    private SignupService signupService;
    @Autowired
    private MarketplacePaymentService marketplacePaymentService;
    @Autowired
    ExpertQuestionsService expertQuestionsService;
    @Autowired
    ExpertUserService expertUserService;

    @RequestMapping(value = "/user-messages.html", method = RequestMethod.GET)
    public String init() {
        return "views/askTheExpert/askExpertAnswers";
    }

    @RequestMapping(value = "/lxrm-ask-question.html", method = RequestMethod.GET)
    public String submitPaidQuestionGETMethod(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("lxrmAskExpert") LxrmAskExpert lxrmAskExpert, HttpSession session, Model model) throws IOException {
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (session.getAttribute("lxrmAskExpert") != null) {
            lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
        }
        if (userLoggedIn) {
            lxrmAskExpert.setEmail(user.getUserName());
        }
        model.addAttribute(lxrmAskExpert);
        return "views/askTheExpert/ateConfirmation";
    }

    @RequestMapping(value = "/lxrm-ask-question.html", method = RequestMethod.POST)
    public String submitPaidQuestion(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("lxrmAskExpert") LxrmAskExpert lxrmAskExpert, HttpSession session, Model model) throws IOException {
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        String password = null;
        String getHelp = request.getParameter("toolhelp");
        if ((getHelp != null && !getHelp.equals("true")) || getHelp == null) {
            if (!userLoggedIn) {
                // checking for existing user
                Signup existingUser = signupService.getUserDetails(lxrmAskExpert.getEmail(), 0);
                if (existingUser == null) {
                    //checking wheather the user is guest user or not
                    existingUser = signupService.getUserDetails(lxrmAskExpert.getEmail(), 1);
                }
                if (existingUser == null) {
                    Login loginUser = loginService.find(user.getId());
                    if (user.getId() <= 0 || (loginUser != null && loginUser.getUserName() != null && !loginUser.getUserName().equalsIgnoreCase("Email") && loginUser.getUserName().trim().length() > 0)) {
                        user = new Login();
                        user.setId(-1);
                        session.setAttribute("user", user);
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                    }
                    user = (Login) session.getAttribute("user");
                } else {
                    user.setId(existingUser.getId());
                    if (existingUser.getName().equals("")) {
                        user.setName(lxrmAskExpert.getEmail().split("@")[0]);
                    } else {
                        user.setName(existingUser.getName());
                    }
                    user.setUserName(existingUser.getEmail());
                    if (existingUser.getPassword() != null && !existingUser.isSocialSignUp()) {
                        user.setPassword(existingUser.getPassword());
                        password = Common.decryptedPassword(existingUser.getPassword());
                    }
                    user.setDomainName(existingUser.getDomainName());
                    user.setActive(existingUser.isActive());
                    user.setAdmin(existingUser.isAdmin());
                    user.setActivationDate(existingUser.getActivationTime());
                    user.setUserUsageStats(loginService.getUserUsageStats(existingUser.getId()));
                    user.setSocialLogin(existingUser.isSocialSignUp());
                }

                // if user password is empty then generate and send login details to the user's mail
                if (password == null && (user != null && !user.isSocialLogin())) {
                    user.setUserName(lxrmAskExpert.getEmail());
                    if (user.getName() == null) {
                        user.setName(lxrmAskExpert.getEmail().split("@")[0]);
                    }
                    password = Common.encryptPassword(Common.autoGeneratePassword().trim());
                    user.setEncrypted(password != null ? Boolean.TRUE : Boolean.FALSE);
                    user.setPassword(password);
                    boolean islocal = Common.checkLocal(request);
                    boolean userInfoUpdate = signupService.updateUserInfo(user, islocal);

                    //need to send user login details to user
                    if (userInfoUpdate) {
                        user.setPassword(Common.decryptedPassword(password));
                        String scheme = request.getScheme();
                        String server = request.getServerName();
                        String url = scheme + "://" + server;
                        Signup signup = new Signup();
                        signup.setName(user.getName());
                        signup.setEmail(user.getUserName());
                        signup.setPassword(user.getPassword());
                        signup.setEncrypted(user.isEncrypted());
                        expertUserService.sendActivationMailofLXRMUser(signup, url);

                    }
                    session.setAttribute("user", user);
                }
                if (user != null && (user.getName() == null || user.getName().equals(""))) {
                    user.setName(lxrmAskExpert.getEmail().split("@")[0]);
                }
                if (user != null && user.isActive() && !user.isDeleted()) {
                    userLoggedIn = Boolean.TRUE;
                    session.setAttribute("userLoggedIn", Boolean.TRUE);
                }
            }
        } else if (session.getAttribute("lxrmAskExpert") != null) {
            lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
        }
        if (userLoggedIn) {
            /*This case may raise
        If emai is not captured from social login channels API, when user login/signup using social channels.*/
            if (lxrmAskExpert.getEmail() != null && (user != null && (user.getUserName() == null || user.getUserName().isEmpty()))) {
                user.setUserName(lxrmAskExpert.getEmail());
                loginService.updateEmail(request, user.getId(), user.getUserName());
            } else if (user != null && user.getUserName() != null && !user.getUserName().isEmpty()) {
                lxrmAskExpert.setEmail(user.getUserName());
            }
        }
        Tool relatedToolObj = lxrmAskExpertService.getToolInfo(lxrmAskExpert.getToolId());
        if (relatedToolObj.getToolName() != null) {
            lxrmAskExpert.setToolName(relatedToolObj.getToolName());
        } else {
            //if the request is coming from homepage
            relatedToolObj.setToolName("LXRMarketplace home");
            relatedToolObj.setToolLink("");
            lxrmAskExpert.setToolName(relatedToolObj.getToolName());
            lxrmAskExpert.setToolId(0);
        }
        double toolPricing = 0;
        String downParam = "'Question'";
        // get the tool pricing info
        Object[] toolInfo = marketplacePaymentService.fetchDataFromToolTab(0, downParam);
        if (toolInfo != null && toolInfo.length > 0) {
            toolPricing = (double) (Double) toolInfo[0];
        }
//        session.removeAttribute("user");
        lxrmAskExpert.setPrice(toolPricing);
        if (user != null) {
            request.getSession().setAttribute("user", user);
            if (user.getName() != null && !user.getName().isEmpty()) {
                session.setAttribute("username", user.getName());
            }
        }
        session.setAttribute("lxrmAskExpert", lxrmAskExpert);
        session.setAttribute("relatedToolObj", relatedToolObj);
        model.addAttribute(lxrmAskExpert);
        return "views/askTheExpert/ateConfirmation";
    }

    //user question confirmation
    @RequestMapping(value = "lxrm-question-payment.html", method = RequestMethod.POST)
    public String makePayment(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("lxrmAskExpert") LxrmAskExpert lxrmAskExpert) throws IOException {
        logger.info("user payment confirmation");
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        LxrmAskExpert lxrmate = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
        lxrmate.setQuestion(lxrmAskExpert.getQuestion());
        lxrmate.setDomain(lxrmAskExpert.getDomain());
        lxrmate.setDeviceCategory("Desktop");

        session.setAttribute("lxrmAskExpert", lxrmate);

        Tool tool = (Tool) session.getAttribute("relatedToolObj");
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        double amount = 0;
        String buttonName = "";
        String downParam = "'Question'";
        Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Object[] toolInfo = marketplacePaymentService.fetchDataFromToolTab(0, downParam);
        if (toolInfo != null && toolInfo.length > 0) {
            logger.debug("toolInfo[0] is......" + toolInfo[0]);
            amount = (double) (Double) toolInfo[0];
            buttonName = (String) toolInfo[1];
        }
        Object[] vals = {"", 0, user.getId(), createdTime, lxrmAskExpert.getDomain(), amount, buttonName, "Ask The Expert", tool.getToolLink(), "NA"};
        session.setAttribute("valsObjArray", vals);
//        response.sendRedirect(url + "/lxrm-payment.html");
        response.sendRedirect("/lxrm-payment.html");
        return null;
    }

    @RequestMapping(value = "lxrm-ate-user-info.html", method = RequestMethod.POST)
    public String inserUserAfterPayment(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        LxrmAskExpert lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
        boolean isAppliedFreePromoCode = false;
        if (session.getAttribute("freePromoCode") != null) {
            isAppliedFreePromoCode = (boolean) session.getAttribute("freePromoCode");
            session.removeAttribute("freePromoCode");
        }
        long paymentId = 0;
        if (!isAppliedFreePromoCode) {
            paymentId = lxrmAskExpertService.getPaymentId(user.getId());
        }

        lxrmAskExpertService.insertAfterPayment(lxrmAskExpert, paymentId, user.getId());
        String subject = EmailBodyCreator.subjectForAskExpertQuestionToOurTeam;
        String displayTxt = EmailBodyCreator.bodyForAskExpertQuestionToOurTeam(lxrmAskExpert.getQuestion(), lxrmAskExpert.getToolName());
        List<ExpertUser> expertUserList = expertQuestionsService.getAllExperts();
        String[] expertsMails = new String[expertUserList.size() - 1];
        int i = 0;
        for (ExpertUser expertUser : expertUserList) {
            if (!expertUser.getRole().equals("admin")) {
                expertsMails[i] = expertUser.getEmail();
                i++;
            }
        }
        SendMail.sendMail(supportMail, expertsMails, subject, displayTxt);
        session.removeAttribute("relatedToolObj");

        /*Remvoing session objects related to SEO SiteMapBuilder tool*/
        session.removeAttribute("ld");
        session.removeAttribute("ps");
        session.removeAttribute("pl");
        session.removeAttribute("tp");
        session.removeAttribute("sitemapurls");
        session.removeAttribute("loginrefresh");
        session.removeAttribute("finalUrl");
        session.removeAttribute("notWorkingUrl");
        /*End of sitemap builder tool related code.*/
        return null;
    }
//    For cancel the question from question confirmation page

    @RequestMapping(value = "ask-expert-cancel.html", method = RequestMethod.POST)
    public String cancelQuestion(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        logger.info("user's question is cancelled and redirecting to previous tool");
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        HttpSession session = request.getSession();
        if (session != null && session.getAttribute("lxrmAskExpert") != null) {
            LxrmAskExpert lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
            Tool tool = lxrmAskExpertService.getToolInfo(lxrmAskExpert.getToolId());
            model.addAttribute("success", "success");
            model.addAttribute("domain", lxrmAskExpert.getDomain());
            int[] singlePageRequestTools = {24, 36, 35, 37, 27, 40, 41, 42, 43};
            if (tool.getToolId() > 0) {
                if ((lxrmAskExpert.getPageUrl().contains("?") && !lxrmAskExpert.getPageUrl().contains("?limit-exceeded=1"))) {
//                if (lxrmAskExpert.getIssues() != null && !lxrmAskExpert.getIssues().equals("")) {
                    response.sendRedirect(url + "/" + tool.getToolLink() + "?backToSuccessPage='backToSuccessPage'");
//                } else {
//                    response.sendRedirect(url + "/" + tool.getToolLink());
//                }
                } else if (IntStream.of(singlePageRequestTools).anyMatch(id -> id == tool.getToolId())) {
                    if ((lxrmAskExpert.getIssues() != null && !lxrmAskExpert.getIssues().equals("")) || (IntStream.of(singlePageRequestTools).anyMatch(id -> id == lxrmAskExpert.getToolId()))) {
                        response.sendRedirect(url + "/" + tool.getToolLink() + "?backToSuccessPage='backToSuccessPage'");
                    } else {
                        response.sendRedirect(url + "/" + tool.getToolLink());
                    }
                } else {
                    response.sendRedirect(url + "/" + tool.getToolLink());
                }
            } else {
                response.sendRedirect(url + "/");
            }
            session.removeAttribute("lxrmAskExpert");
            session.removeAttribute("relatedToolObj");
        } else {
            response.sendRedirect(url + "/");
        }
        return null;
    }

    @RequestMapping(value = "/user-messages.html", params = "ate=allAnswers")
    @ResponseBody
    public List<ATEUserMessages> fetchingATEAnswers(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("userId") long userId) {
        HttpSession session = request.getSession();
        //updating viewed answers
        if (session.getAttribute("totalUnViewedAnswersCount") != null && (int) session.getAttribute("totalUnViewedAnswersCount") > 0) {
            session.setAttribute("totalUnViewedAnswersCount", 0);
            lxrmAskExpertService.updatedAllUnViewedAnswers(userId);
        }
        List<ATEUserMessages> allATEAnswers = lxrmAskExpertService.getATEAnswersList(userId);
        return allATEAnswers;
    }

    @RequestMapping(value = "/user-messages.html", params = "edit=rating")
    @ResponseBody
    public String editRating(HttpServletRequest request, HttpServletResponse response, @RequestParam("questionId") long questionId,
            @RequestParam("rating") int rating, @RequestParam("comment") String comment) throws IOException {
        HttpSession session = request.getSession();
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (comment != null && !"null".equals(comment) && !"".equals(comment)) {
            session.setAttribute("comment", comment);
        } else if (session.getAttribute("comment") != null) {
            session.removeAttribute("comment");
        }
        String data;
        if (rating != 0) {
            data = url + "/ask-expert-feedback.html?question-id=" + questionId + "&rating=" + rating;
        } else {
            data = url + "/ask-expert-feedback.html?question-id=" + questionId;
        }

        return data;
    }

    @RequestMapping(value = "/user-messages.html", params = "check = unViewdAnswers")
    @ResponseBody
    public Object fetchingUnViewedATEAnswersCount(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("userId") long userId) {
        HttpSession session = request.getSession();
        Object[] data = new Object[2];
        SqlRowSet sqlRowSet = lxrmAskExpertService.getUnViewdAnswersCount(userId);
        boolean containsData = sqlRowSet.isBeforeFirst();
        if (containsData) {
            while (sqlRowSet.next()) {
                int totalAnswersCount = sqlRowSet.getInt("total_answers_count");
                int totalUnViewedAnswersCount = sqlRowSet.getInt("unviewed_answers_count");
                data[0] = totalAnswersCount;
                data[1] = totalUnViewedAnswersCount;

            }
        } else {
            data[0] = 0;
            data[1] = 0;
        }
        session.setAttribute("totalAnswersCount", data[0]);
        session.setAttribute("totalUnViewedAnswersCount", data[1]);
        return data;
    }

    @RequestMapping(value = "/ate-view-attached-file.html", method = RequestMethod.GET)
    public String downloadAttachedFile(HttpServletRequest request, HttpServletResponse response, @RequestParam("attachedFile") String attachedFile) {
        expertQuestionsService.downloadFile(response, attachedFile);
        return null;
    }

}
