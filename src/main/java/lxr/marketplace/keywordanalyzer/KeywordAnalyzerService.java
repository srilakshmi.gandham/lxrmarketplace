package lxr.marketplace.keywordanalyzer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.apache.log4j.Logger;

//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.FileItemFactory;
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import com.Ostermiller.util.CSVParser;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.jdbc.Statement;
import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.context.NoSuchMessageException;

public class KeywordAnalyzerService {

    private static Logger logger = Logger.getLogger(KeywordAnalyzerService.class);

    final String headerFileName = "X-File-Name";
    private String productTable;
    public String tablename;

    String[] reportFieldList;
    public Map<String, String> fieldToDataTypeMap = new HashMap<>();
    @Resource(name = "jdbcTemplate")
    JdbcTemplate jdbcTemplate;
    ArrayList al = new ArrayList();

    public String getProductTable() {
        return productTable;
    }

    public void setProductTable(String productTable) {
        this.productTable = productTable;
    }

    public String[] getReportFieldList() {
        return reportFieldList;
    }

    public void setReportFieldList(String[] reportFieldList) {
        this.reportFieldList = reportFieldList;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String[] getListByOption(String filePath, String options,
            int selColInd, boolean headerRow, int keyMetric, HttpServletResponse response) {
        FileInputStream fis = null;
        String fields[] = null;

        try {
            File f = new File(filePath);
            fis = new FileInputStream(f);
            if (filePath.endsWith(".csv")) {
                fields = getListFromCsv(fis, filePath, options, headerRow, keyMetric, response);
            } else if (filePath.endsWith(".tsv")) {
                fields = getListFromTsv(fis, filePath, options, headerRow, keyMetric, response);
            }
            return fields;
        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return null;
    }

    public String[] getListFromCsv(FileInputStream fis, String filePath,
            String options, boolean headerRow, int keyMetric,
            HttpServletResponse response) {
        CSVParser csvp = new CSVParser(fis);
        logger.info("retrieving list from " + filePath);
        try {
            if (options.equalsIgnoreCase("columnList")) {
                String[][] values = csvp.getAllValues();

                if (values == null || values.length == 0) {
                    String[] columns = {"empty"};
                    return columns;
                }

                int count = 0;
                int i = 0;
                for (i = 0; i < values.length; i++) {
                    for (String s : values[i]) {
                        if (s.equals("Impressions") || s.equals("Clicks")) {
                            count++;
                        }
                    }
                    if (count >= 1) {
                        break;
                    }
                }

                if (count >= 1 && values.length <= i + 1) {
                    String[] columns = {"nodata"};
                    return columns;
                } else if (count >= 1 && i != values.length) {
                    String[] columns = values[i];
                    return columns;
                } else {
                    String[] columns = {"noheader"};
                    return columns;

                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public String[] getListFromTsv(FileInputStream fis, String filePath,
            String options, boolean headerRow, int keyMetric,
            HttpServletResponse response) {
        CSVParser csvp = new CSVParser(fis);
        logger.info("retrieving list from " + filePath);
        try {
            if (options.equalsIgnoreCase("columnList")) {
                csvp.changeDelimiter('\t');
                String[][] values = csvp.getAllValues();

                if (values == null || values.length == 0) {
                    String[] columns = {"empty"};
                    return columns;
                }
                int count = 0;
                int i = 0;
                for (i = 0; i < values.length; i++) {
                    for (String s : values[i]) {
                        if (s.equals("Impressions") || s.equals("Clicks")) {
                            count++;
                        }
                    }
                    if (count >= 1) {
                        break;
                    }
                }

                if (count >= 1 && values.length <= i + 1) {
                    String[] columns = {"nodata"};
                    return columns;
                } else if (count >= 1 && i != values.length) {
                    String[] columns = values[i];
                    return columns;
                } else {
                    String[] columns = {"noheader"};
                    return columns;

                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public void setFieldToDataTypeMap() {

        fieldToDataTypeMap.put("Year", "year INT(20)");//for adcenter
        fieldToDataTypeMap.put("Month", "month VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Week", "week VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Gregorian date", "gregorian_date VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Hour", "hour VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Language", "language VARCHAR(255)");
        fieldToDataTypeMap.put("Account name", "Account_name VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Campaign name", "campaign VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Ad distribution", "Ad_distribution VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Device type", "device_type VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Delivered match type", "delivered_match_type VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Bid match type", "bid_match_type VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Language and market", "language_and_market VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Account number", "account_number VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Ad ID", "ad_id VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Account ID", "account_id VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Ad group item ID", "ad_grop_item_id VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Ad group ID", "ad_group_id VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Campaign ID", "campaign_id VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Currency code", "currency_code VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Ad type", "ad_type VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Pricing method", "pricing_method VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Keyword Relevance", "keyword_relevance VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Landing Page Relevance", "landing_page_relevance VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Landing Page User Experience", "landing_page_User_experience VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Conversion rate (%)", "conversion_rate VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Keyword", "keyword VARCHAR(255)");
        fieldToDataTypeMap.put("Keyword state", "keyword_state VARCHAR(255)");
        fieldToDataTypeMap.put("Campaign", "campaign VARCHAR(255)");
        fieldToDataTypeMap.put("Ad group", "ad_group VARCHAR(255)");
        fieldToDataTypeMap.put("Status", "status VARCHAR(255)");
        fieldToDataTypeMap.put("Current maximum CPC", "max_cpc VARCHAR(255)"); //for adcenter
        fieldToDataTypeMap.put("Max. CPC", "max_cpc VARCHAR(255)");
        fieldToDataTypeMap.put("Match type", "match_type VARCHAR(30)");
        fieldToDataTypeMap.put("Impressions", "impressions BIGINT(30)");
        fieldToDataTypeMap.put("Clicks", "clicks BIGINT(20)");
        fieldToDataTypeMap.put("CTR", "ctr VARCHAR(255)");
        fieldToDataTypeMap.put("Avg. CPC", "avg_cpc VARCHAR(255)");
        fieldToDataTypeMap.put("Average CPC", "avg_cpc DOUBLE(10,2)");  //for adcenter
        fieldToDataTypeMap.put("Cost", "cost DOUBLE(20,2)");
        fieldToDataTypeMap.put("Spend", "cost DOUBLE(20,2)"); //for adcenter
        fieldToDataTypeMap.put("Avg. position", "avg_position DOUBLE(10,2)");
        fieldToDataTypeMap.put("Conv. (1-per-click)", "conversions BIGINT(30)");
        fieldToDataTypeMap.put("Conversions", "conversions BIGINT(30)");//for adcenter
        fieldToDataTypeMap.put("Conv. (many-per-click)", "conv_mpc BIGINT(30)");
        fieldToDataTypeMap.put("Cost / conv. (1-per-click)", "cost_by_conv_1pc DOUBLE(10,2)");
        fieldToDataTypeMap.put("Conv. rate (1-per-click)", "conv_rate_1pc VARCHAR(255)");
        fieldToDataTypeMap.put("Conv. Rate (1-per-click)", "Conv_Rate_1pc VARCHAR(255)");
        fieldToDataTypeMap.put("Cost/Conv. (many-per-click)", "costby_conv_mpc VARCHAR(255)");
        fieldToDataTypeMap.put("Conv. Rate (many-per-click)", "conv_Rate_mpc VARCHAR(255)");
        fieldToDataTypeMap.put("Conversion rate (%)", "conv_rate VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("CPA", "CPA VARCHAR(255)"); //for adcenter
        fieldToDataTypeMap.put("Avg. CPM", "Avg_CPM VARCHAR(255)");
        fieldToDataTypeMap.put("Destination URL", "destination_url VARCHAR(1024)");
        fieldToDataTypeMap.put("Quality score", "quality_score INT(10)");
        fieldToDataTypeMap.put("Quality Score", "quality_Score VARCHAR(255)"); //for adcenter
        fieldToDataTypeMap.put("First page CPC", "first_page_cpc DOUBLE(10,2)");
        fieldToDataTypeMap.put("Revenue", "revenue DOUBLE(20,2)");
        fieldToDataTypeMap.put("Total conv. value", "revenue DOUBLE(20,2)");
        fieldToDataTypeMap.put("Total conv. Value", "revenue DOUBLE(20,2)");
        fieldToDataTypeMap.put("Top of page CPC", "top_of_page_cpc FLOAT(15,2)");
        fieldToDataTypeMap.put("View-through conv.", "view_through_conv VARCHAR(255)");
        fieldToDataTypeMap.put("Cost / conv. (many-per-click)", "cost_conv_many_per_click VARCHAR(255)");
        fieldToDataTypeMap.put("Conv. rate (many-per-click)", "conv_rate_many_per_click VARCHAR(255)");
        fieldToDataTypeMap.put("Conv. value / cost", "conv_value_cost VARCHAR(255)");
        fieldToDataTypeMap.put("Conv. value / click", "conv_value_click VARCHAR(255)");
        fieldToDataTypeMap.put("Value / conv. (1-per-click)", "value_conv_1_per_click VARCHAR(255)");
        fieldToDataTypeMap.put("Value / conv. (many-per-click)", "value_conv_many_per_click VARCHAR(255)");
        fieldToDataTypeMap.put("Historic Keyword Relevance", "historic_keyword_relevance VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Historic Quality Score", "historic_quality_score VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Historic Landing Page Relevance", "historic_landing_page_relevance VARCHAR(255)");//for adcenter
        fieldToDataTypeMap.put("Historic Landing Page User Experience", "historic_landing_page_user_experience VARCHAR(255)");//for adcenter

        //TODO: put new key value pair if there is columns with different kind
        //TODO: put new key but same value if there is columns with different 
        //name but of same kind as mentioned above
    }

    public String generateTableCreationQuery() {
        setFieldToDataTypeMap();
        String tableQuery = "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
        int blankCount = 0;
        for (String fieldname : reportFieldList) {
            String temp = fieldToDataTypeMap.get(fieldname);
            //logger.info("datafield in db: "+temp);
            if (temp == null) {
                if (fieldname == "") {
                    temp = "blank" + blankCount++ + " VARCHAR(255)";
                } else {
                    temp = fieldname.toLowerCase().replaceAll("\\s+", "");
                    temp = temp.replaceAll("\\W+", "_") + " VARCHAR(255)";
                }
            }
            logger.debug("fieldname: " + fieldname + " table column: " + temp);
            tableQuery += ", " + temp;
        }
        return tableQuery;
    }

    public String generateDataInsertQuery() {
        String query = "INSERT INTO " + getProductTable() + " ( ";
        int blankCount = 0;
        for (String fieldname : reportFieldList) {
            String temp = fieldToDataTypeMap.get(fieldname);
            if (temp == null) {
                if (fieldname == "") {
                    temp = "blank" + blankCount++ + " VARCHAR(255)";
                } else {
                    temp = fieldname.toLowerCase().replaceAll("\\s+", "");
                    temp = temp.replaceAll("\\W+", "_") + " VARCHAR(255)";
                }
            }
            query += temp.substring(0, temp.indexOf(' ')) + ", ";
        }
        query = query.substring(0, (query.length() - 2)) + " ) VALUES (?"
                + repeat(",?", (reportFieldList.length - 1)) + ")";
        /*logger.info("data insert query: " + query);*/
        return query;
    }

    private String repeat(String str, int i) {
        String newString = "";
        for (int j = 0; j < i; j++) {
            newString += str;
        }
        return newString;
    }

    public String saveFile(String realPath, HttpServletRequest request,
            HttpServletResponse response) {
        String fileName = "", newFilename = "";
        InputStream is = null;
        FileOutputStream fos = null;
        if (request.getHeader(headerFileName) != null) {
            fileName = request.getHeader(headerFileName);
            if (fileName.trim().contains(" ")) {
                fileName = fileName.replace(" ", "%");
            }
            int ind = fileName.lastIndexOf(".");
            newFilename = fileName.substring(0, ind);
            String ext = fileName.substring((ind + 1), fileName.length());
            newFilename += "_" + Calendar.getInstance().getTime().getTime()
                    + "." + ext;
            try {
                is = request.getInputStream();
                File f = new File(realPath + newFilename);
                fos = new FileOutputStream(f);
                IOUtils.copy(is, fos);
                response.setContentType("application/json");
                response.setStatus(HttpServletResponse.SC_OK);
            } catch (Exception e) {
                logger.error(e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } finally {
                try {
                    fos.close();
                    is.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        } else if (request.getParameter("fileName") != null) {
            fileName = request.getParameter("fileName");
            if (fileName.trim().contains(" ")) {
                fileName = fileName.replace(" ", "%");
            }
            int ind = fileName.lastIndexOf(".");
            newFilename = fileName.substring(0, ind);
            String ext = fileName.substring((ind + 1), fileName.length());
            newFilename += "_" + Calendar.getInstance().getTime().getTime() + "." + ext;

            response.setContentType("text/html");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            final long permitedSize = 314572800;
            long fileSize;
            try {
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    FileItemFactory factory = new DiskFileItemFactory();
                    ServletFileUpload upload = new ServletFileUpload(factory);
                    List items = upload.parseRequest(request);
                    for (int i = 0; i < items.size(); i++) {
                        FileItem fileItem = (FileItem) items.get(i);
                        if (fileItem.isFormField()) {
                            String any_parameter = fileItem.getString();
                        } else {
                            fileName = fileItem.getName();
                            fileSize = fileItem.getSize();
                            if (fileSize <= permitedSize) {
                                File uploadedFile = new File(realPath + newFilename);
                                fileItem.write(uploadedFile);
                            } else {
                                logger.info("file is too big");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return newFilename;
    }

    public void fileToDBTable(String fileName, String uploadFolder) {

        String newfileName, sourceFilePath = uploadFolder.concat(fileName);

        Connection conn = null;
        PreparedStatement preSta = null;

        setJdbcTemplate(jdbcTemplate);

        try {
            if (fileName.contains(".csv")) {
                newfileName = modifingCsvFile(fileName, uploadFolder);
                FileInputStream fileInputStream = new FileInputStream(newfileName);
                readCsvAndPopulateDB(fileInputStream, conn, preSta);

            } else if (fileName.contains(".tsv")) {
                FileInputStream fileInputStream = new FileInputStream(sourceFilePath);
                readTsvAndPopulateDB(fileInputStream, conn, preSta);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (preSta != null) {
                    preSta.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ex) {
                logger.error("Excpetion while fileToDBTable" + ex.getMessage());
            }
        }

    }

    public String modifingCsvFile(String fileName, String uploadFolder) {
        String outputfile = uploadFolder.concat("modified" + fileName);
        String sourceFilePath = uploadFolder.concat(fileName);
        try {
            FileInputStream fin = new FileInputStream(sourceFilePath);
            FileOutputStream fout = new FileOutputStream(outputfile);
            PrintWriter pw = new PrintWriter(fout);
            BufferedReader br = new BufferedReader(new InputStreamReader(fin));
            String line = null;
            int countread = 0, countwrite = 0;
            while ((line = br.readLine()) != null) {
                String temp;

                temp = line.replaceAll(",\"\"\"", ",\"{") + "\n";
                temp = temp.replaceAll("\"\"\",", "}\",") + "\n";
                countread++;
                if (!temp.equals(null)) {
                    pw.append(temp);
                    countwrite++;
                }
                pw.flush();
            }
            br.close();
            pw.close();

        } catch (Exception e) {
            logger.error("Exception in modifingCsvFile" + e.getMessage());
        }
        return outputfile;
    }

    void readCsvAndPopulateDB(FileInputStream fileInputStream, Connection conn,
            PreparedStatement preSta) throws IOException,
            SQLException {
        CSVParser csvp = new CSVParser(fileInputStream);
        String[][] values = csvp.getAllValues();
        int count = 0;
        int i = 0;
        //to find header row
        for (i = 0; i < values.length; i++) {
            for (String s : values[i]) {
                if (s.contains("\"Impressions\"") || s.contains("\"Clicks\"") || s.equals("Impressions") || s.equals("Clicks")) {
                    count++;
                    break;
                }

            }
            if (count >= 1) {
                break;
            }
        }
        int headerRowNum = i;
        //to find lastdata row
        int k, count1 = 0;
        for (k = values.length - 1; k > 0; k--) {
            for (String s : values[k]) {
                if (s.equals("\"Total\"") || s.equals("Total") || s.equals("Total - Display Network") || s.equals("\"Total - Display Network\"") || s.equals("\"Total - Search\"") || s.equals("Total - Search") || s.equalsIgnoreCase("Ã‚Â©2011 Microsoft Corporation. All rights reserved. ") || s.equalsIgnoreCase("\"Ã‚Â©2011 Microsoft Corporation. All rights reserved. \"")) {
                    count1++;
                    break;
                }
            }
        }
        int length = values[headerRowNum].length;
        String[] fields = new String[length];
        for (int l = 0; l < values[headerRowNum].length; l++) {
            if (values[headerRowNum][l].startsWith("\"") && values[headerRowNum][l].endsWith("\"")) {
                fields[l] = values[headerRowNum][l].substring(1, values[headerRowNum][l].length() - 1);
            } else {
                fields[l] = values[headerRowNum][l];
            }
        }//keeps column names from uploaded report
        int firstDataRowNum, lastDataRowNum = 0, recCount = 0;
        firstDataRowNum = headerRowNum + 1;
        lastDataRowNum = values.length - 1 - count1;
        logger.debug("firstDataRowNum: " + firstDataRowNum + " lastDataRowNum: " + lastDataRowNum + " count1: " + count1);
        setReportFieldList(fields);

        setProductTable(Common.generateTableName("KEYWORDSTATS"));
        tablename = getProductTable();
        String tablequery = generateTableCreationQuery();
        Common.createTable(getProductTable(), tablequery, jdbcTemplate);
        String insertQuery = generateDataInsertQuery();
        try {
            conn = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException e) {
            logger.error("Exception while getting connection object in readCsvAndPopulateDB" + e.getMessage());
        }
        preSta = conn.prepareStatement(insertQuery);
        for (int rowInd = firstDataRowNum; rowInd <= lastDataRowNum; rowInd++) {
            for (String s : values[rowInd]) {

                if (s != null) {
                    for (int colInd = 0; colInd < reportFieldList.length; colInd++) {

                        if (values[rowInd][colInd].startsWith("{") && values[rowInd][colInd].endsWith("}")) {
                            String str;
                            str = "\"" + values[rowInd][colInd].substring(1, values[rowInd][colInd].length() - 1) + "\"";
                            /*preSta.setString(colInd + 1, str);*/
                            if ((!str.trim().isEmpty()) && (str.trim().length() != 0)) {
                                preSta.setString(colInd + 1, str);
                            }
                        } else if (reportFieldList[colInd].equals("Revenue") || reportFieldList[colInd].equals("Cost") || reportFieldList[colInd].equals("Total conv. value") || reportFieldList[colInd].equals("Spend")) {
                            String str = values[rowInd][colInd].replaceAll(",", "");
                            if (!str.equals("")) {
                                if(str.contains("$") && str.startsWith("$")){
                                    str = str.replace("$"," ").trim();
                                }
                                double cost = Double.parseDouble(str);
                                preSta.setDouble(colInd + 1, cost);//String(colInd+1,cost);
                            } else /*preSta.setString(colInd+1,str);*/ if ((!str.trim().isEmpty()) && (str.trim().length() != 0)) {
                                preSta.setString(colInd + 1, str);
                            }

                        } else /*preSta.setString(colInd + 1, values[rowInd][colInd]);*/ if ((!values[rowInd][colInd].trim().isEmpty()) && (values[rowInd][colInd].trim().length() != 0)) {
                            preSta.setString(colInd + 1, values[rowInd][colInd]);
                        }
                    }
                }
            }
            preSta.addBatch();
            recCount++;
            if (recCount == 1000) {
                recCount = 0;
                try {
                    logger.info("The rows completed" + rowInd + "among total number of " + lastDataRowNum);
                    preSta.executeBatch();
                } catch (SQLException sQLException) {
                    logger.error("SQLException while inserting data into readCsvAndPopulateDB " + sQLException.getMessage());
                }
                preSta.close();
                preSta = null;
                preSta = conn.prepareStatement(insertQuery);
            }
        }
        try {
            logger.info("The remaining  rows are adding to batch " + lastDataRowNum);
            preSta.executeBatch();
        } catch (SQLException sQLException) {
            logger.error("SQLException: 2" + sQLException);
        }

    }

    void readTsvAndPopulateDB(FileInputStream fileInputStream, Connection conn,
            PreparedStatement preSta) throws IOException,
            SQLException {
        CSVParser csvp = new CSVParser(fileInputStream);
        csvp.changeDelimiter('\t');
        csvp.changeQuote('\f');
        String[][] values = csvp.getAllValues();
        int count = 0;
        int i = 0;
        for (i = 0; i < values.length; i++) {
            for (String s : values[i]) {
                if (s.contains("\"Impressions\"") || s.contains("\"Clicks\"") || s.equals("Impressions") || s.equals("Clicks")) {
                    count++;
                }

            }
            if (count >= 1) {
                break;
            }
        }
        int headerRowNum = i;
        int k, count1 = 0;
        for (k = values.length - 1; k > 0; k--) {
            for (String s : values[k]) {
                if (s.equals("\"Total\"") || s.equals("Total") || s.equals("Total - Display Network") || s.equals("\"Total - Display Network\"") || s.equals("\"Total - Search\"") || s.equals("Total - Search") || s.equalsIgnoreCase("Ã‚Â©2011 Microsoft Corporation. All rights reserved. ") || s.equalsIgnoreCase("\"Ã‚Â©2011 Microsoft Corporation. All rights reserved. \"")) {
                    count1++;
                    break;
                }
            }
        }
        int length = values[headerRowNum].length;
        String[] fields = new String[length];
        for (int l = 0; l < values[headerRowNum].length; l++) {
            if (values[headerRowNum][l].startsWith("\"")
                    && values[headerRowNum][l].endsWith("\"")) {
                fields[l] = values[headerRowNum][l].substring(1,
                        values[headerRowNum][l].length() - 1);
            } else {
                fields[l] = values[headerRowNum][l];
            }
        }//keeps column names from uploaded report
        int firstDataRowNum, lastDataRowNum = 0, recCount = 0;
        firstDataRowNum = headerRowNum + 1;
        lastDataRowNum = values.length - 1 - count1;
        setReportFieldList(fields);
        setProductTable(Common.generateTableName("KEYWORDSTATS"));
        String tablequery = generateTableCreationQuery();
        Common.createTable(getProductTable(), tablequery, jdbcTemplate);
        String insertQuery = generateDataInsertQuery();

        try {
            conn = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException e) {
            logger.error("Exception while getting connection object in readTsvAndPopulateDB" + e.getMessage());
        }
        preSta = conn.prepareStatement(insertQuery);
        for (int rowInd = firstDataRowNum; rowInd <= lastDataRowNum; rowInd++) {
            for (String s : values[rowInd]) {
                if (s != null) {
                    for (int colInd = 0; colInd < reportFieldList.length; colInd++) {
                        if (values[rowInd][colInd].startsWith("\"") && values[rowInd][colInd].endsWith("\"")) {

                            String str;
                            str = values[rowInd][colInd].substring(1, values[rowInd][colInd].length() - 1);
                            if (values[rowInd][colInd].startsWith("\"\"\"") && values[rowInd][colInd].endsWith("\"\"\"")) {
                                str = values[rowInd][colInd].substring(2, values[rowInd][colInd].length() - 2);
                                /*preSta.setString(colInd + 1, str);*/
                                if ((str.trim() != "") && (str.trim().length() != 0)) {
                                    preSta.setString(colInd + 1, str);
                                }
                            } else if (reportFieldList[colInd].equals("Revenue") || reportFieldList[colInd].equals("Cost") || reportFieldList[colInd].equals("Total conv. value") || reportFieldList[colInd].equals("Spend")) {
                                str = str.replaceAll(",", "");
                                if (!str.equals("")) {
                                    double cost = Double.parseDouble(str);
                                    preSta.setDouble(colInd + 1, cost);
                                } else /*preSta.setString(colInd+1,str);*/ if ((str.trim() != "") && (str.trim().length() != 0)) {
                                    preSta.setString(colInd + 1, str);
                                }
                            } else /*preSta.setString(colInd + 1, str);*/ if ((str.trim() != "") && (str.trim().length() != 0)) {
                                preSta.setString(colInd + 1, str);
                            }
                        } else /*preSta.setString(colInd + 1, values[rowInd][colInd]);*/ if ((values[rowInd][colInd].trim() != "") && (values[rowInd][colInd].trim().length() != 0)) {
                            preSta.setString(colInd + 1, values[rowInd][colInd]);
                        }
                    }
                }
            }
            preSta.addBatch();
            recCount++;
            if (recCount == 1000) {
                recCount = 0;
                try {
                    logger.info("The rows completed" + rowInd + "among total number of " + lastDataRowNum);
                    preSta.executeBatch();
                } catch (SQLException sQLException) {
                    logger.error("SQLException while inserting data into readTsvAndPopulateDB" + sQLException.getMessage());
                }
                preSta.close();
                preSta = null;
                preSta = conn.prepareStatement(insertQuery);
            }
        }

        try {
            preSta.executeBatch();
        } catch (SQLException sQLException) {
            logger.error("SQLException: 2" + sQLException.getMessage());
        }

    }

    public String getDatasql(int keyMetric, int i) {
        //to retrieve data for success screen
        String sql = "";
        Connection conn = null;
        //for bucket 1 top converting keywords
        setJdbcTemplate(jdbcTemplate);
        if (i == 0) {
            if (keyMetric == 1) {
                try {
                    createProcedureEightyTwenty(getProductTable(), keyMetric, conn, i);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sql = "{call SHOW_KEYWORDS()}";

            } else if (keyMetric == 2) {
                try {
                    createProcedureEightyTwenty(getProductTable(), keyMetric, conn, i);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sql = "{call SHOW_KEYWORDS()}";
            }
        }
        //for bucket 2 other converting keywords
        if (i == 1) {

            if (keyMetric == 1) {
                try {
                    createProcedureEightyTwenty(getProductTable(), keyMetric, conn, i);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sql = "{call SHOW_KEYWORDS()}";

            } else if (keyMetric == 2) {

                try {
                    createProcedureEightyTwenty(getProductTable(), keyMetric, conn, i);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sql = "{call SHOW_KEYWORDS()}";

            }
        }
        //for bucket 3 queryForClicksButNoConversion
        if (i == 2) {
            if (keyMetric == 1) {
                sql = "select count(keyword),sum(conversions),round(sum(cost),2) from " + getProductTable() + " where conversions=0 and clicks>0 order by cost desc ";
            } else if (keyMetric == 2) {
                sql = "select count(keyword),round(sum(revenue),2),round(sum(cost),2) from " + getProductTable() + " where revenue=0 and clicks>0 order by cost desc ";
            }
        } else if (i == 3) {
            //for bucket 4 queryForImpressionButNoClicks
            if (keyMetric == 1) {
                sql = "select count(keyword),sum(conversions),round(sum(cost),2) from " + getProductTable() + " where clicks=0 and Impressions>0 order by impressions desc ";
            } else if (keyMetric == 2) {
                sql = "select count(keyword),round(sum(revenue),2),round(sum(cost),2) from " + getProductTable() + " where clicks=0 and Impressions>0 order by impressions desc ";
            }
        } else if (i == 4) {
            //for bucket 5 queryForNoImpressions
            if (keyMetric == 1) {
                sql = "select count(keyword),sum(conversions),round(sum(cost),2) from " + getProductTable() + " where Impressions=0  order by id desc ";
            } else if (keyMetric == 2) {
                sql = "select count(keyword),round(sum(revenue),2),round(sum(cost),2) from " + getProductTable() + "  where Impressions=0  order by id desc ";
            }
        }
        return sql;
    }

    public void createProcedureEightyTwenty(String table, int keymetric, Connection conn, int reporttype)
            throws SQLException {
        String createProcedure = null;
        String colname, metric;

        if (reporttype == 0) {
            metric = "<=";
        } else {
            metric = ">";
        }

        if (keymetric == 1) {
            colname = "conversions";
        } else {
            colname = "revenue";
        }
        String queryDrop
                = "DROP PROCEDURE IF EXISTS SHOW_KEYWORDS";

        createProcedure
                = "create procedure SHOW_KEYWORDS() "
                + "begin "
                + "set @position=0; "
                + "select count(keyword),sum(" + colname + "),round(sum(cost),2)  from("
                + "select  keyword," + colname + ",cost, @position:=@position" + "+" + colname + " as position "
                + " from " + table + " where " + colname + " >0 order by " + colname + " desc, cost desc, impressions desc) a where "
                + " position" + metric + "(select round(.8*sum(" + colname + ")) as 'egty' from " + table + " );"
                + "end";

        Statement stmt = null;
        Statement stmtDrop = null;
        try {
            conn = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            stmtDrop = (Statement) conn.createStatement();
            stmtDrop.execute(queryDrop);
        } catch (SQLException e) {
            logger.info(e);
        } finally {
            if (stmtDrop != null) {
                stmtDrop.close();
            }
        }

        try {
            logger.info("createProcedure: " + createProcedure);
            stmt = (Statement) conn.createStatement();
            stmt.executeUpdate(createProcedure);
        } catch (SQLException e) {
            logger.error("Exception in createProcedureEightyTwenty" + e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void createProcedureEightyTwentyForReports(String table, int keymetric, Connection conn, int reporttype)
            throws SQLException {
        String createProcedure = null;
        String colname, metric;

        if (reporttype == 0) {
            metric = "<=";
        } else {
            metric = ">";
        }

        if (keymetric == 1) {
            colname = "conversions";
        } else {
            colname = "revenue";
        }
        String queryDrop
                = "DROP PROCEDURE IF EXISTS SHOW_KEYWORDS";

        createProcedure
                = "create procedure SHOW_KEYWORDS() "
                + "begin "
                + "set @position=0; "
                + "select *  from("
                + "select  *, @position:=@position" + "+" + colname + " as position "
                + " from " + table + " where " + colname + " >0 order by " + colname + " desc, cost desc, impressions desc) a where "
                + " position" + metric + "(select round(.8*sum(" + colname + ")) as 'egty' from " + table + " );"
                + "end";

        Statement stmt = null;
        Statement stmtDrop = null;
        try {
            conn = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            stmtDrop = (Statement) conn.createStatement();
            stmtDrop.execute(queryDrop);
        } catch (SQLException e) {
            logger.info(e);
        } finally {
            if (stmtDrop != null) {
                stmtDrop.close();
            }
        }

        try {
            logger.info("createProcedure" + createProcedure);
            stmt = (Statement) conn.createStatement();
            stmt.executeUpdate(createProcedure);
        } catch (SQLException e) {
            logger.info(e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public ArrayList<List> getList(String dataSql) {
        //logger.info(dataSql);
        SqlRowSet sqlRowSet;
        sqlRowSet = jdbcTemplate.queryForRowSet(dataSql);
        //logger.info(sqlRowSet);
        al.add(sqlRowSet);
        //logger.info(al);
        return al;
    }

    public static double round(double d, int decimalPlace) {
        // see the Javadoc about why we use a String in the constructor
        // http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    public void getTotals(HttpSession session, int keyMetric) {
        int totkeys = 0;
        int totconv = 0;
        double totrev = 0.0;
        double totcost = 0.0;
        ArrayList arr = (ArrayList) session.getAttribute("al");
        if (arr.isEmpty() == false) {
            for (int i = 0; i < arr.size(); i++) {
                SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);
                SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                sqlRowSet.first();
                for (int j = 1; j <= sqlRowSetMetDat.getColumnCount(); j++) {
                    if ((sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j))) != null) {
                        if (j == 1) {
                            totkeys = totkeys + Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j)));
                        }
                        if (j == 2) {
                            if (keyMetric == 1) {
                                totconv = totconv + Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j)));
                                session.setAttribute("totconv", totconv);
                            } else if (keyMetric == 2) {

                                totrev = totrev + Double.parseDouble(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j)));
                                session.setAttribute("totrev", totrev);
                                totrev = round(totrev, 2);
                            }
                        }
                        if (j == 3) {
                            totcost = totcost + Double.parseDouble(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j)));
                            totcost = round(totcost, 2);
                        }
                    }
                }
            }
            session.setAttribute("totkeys", totkeys);
            session.setAttribute("totcost", totcost);
        }
    }

    public String generateReportData(int keyMetric, String appPath, HttpSession session, String reportType) throws DocumentException, BadElementException, IOException {
        logger.info("creating report...");
        Connection conn = null;
        String queryForTopConvertingKeyword = "";
        String queryForOtherConvertingKeyword = "";
        String queryForClicksButNoConversion = "";
        String queryForImpressionButNoClicks = "select * from "
                + getProductTable()
                + " where impressions > 0 and clicks = 0 order by impressions desc";
        ;
        String queryForNoImpressions = "select * from " + getProductTable()
                + " where impressions = 0 order by id";
        String filename = "";
        String sortquery = "SELECT * FROM " + getProductTable() + " ORDER BY ";
        String repName = "LXRMarketplace_Keyword_Performance_Analyzer_Report";
        SqlRowSet[] sqlRowSet = new SqlRowSet[5];
        try {
            int i = 0;
            for (i = 0; i < 3; i++) {
                if (i == 0) {
                    if (keyMetric == 1) {
                        try {
                            createProcedureEightyTwentyForReports(getProductTable(), keyMetric, conn, 0);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        queryForTopConvertingKeyword = "{call SHOW_KEYWORDS()}";
                        sqlRowSet[0] = jdbcTemplate.queryForRowSet(queryForTopConvertingKeyword);
                    } else if (keyMetric == 2) {
                        try {
                            createProcedureEightyTwentyForReports(getProductTable(), keyMetric, conn, 0);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        queryForTopConvertingKeyword = "{call SHOW_KEYWORDS()}";
                        sqlRowSet[0] = jdbcTemplate.queryForRowSet(queryForTopConvertingKeyword);
                    }
                } else if (i == 1) {
                    if (keyMetric == 1) {
                        try {
                            createProcedureEightyTwentyForReports(getProductTable(), keyMetric, conn, 1);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        queryForOtherConvertingKeyword = "{call SHOW_KEYWORDS()}";
                        sqlRowSet[1] = jdbcTemplate.queryForRowSet(queryForOtherConvertingKeyword);
                    } else if (keyMetric == 2) {
                        try {
                            createProcedureEightyTwentyForReports(getProductTable(), keyMetric, conn, 1);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        queryForOtherConvertingKeyword = "{call SHOW_KEYWORDS()}";
                        sqlRowSet[1] = jdbcTemplate.queryForRowSet(queryForOtherConvertingKeyword);
                    }

                } else if (i == 2) {
                    if (keyMetric == 1) { // conversion
                        sortquery += "conversions desc, cost desc, impressions desc";
                        queryForClicksButNoConversion = "select * from "
                                + getProductTable()
                                + " where cost > 0 and conversions = 0 order by cost desc";
                        sqlRowSet[2] = jdbcTemplate.queryForRowSet(queryForClicksButNoConversion);
                    } else if (keyMetric == 2) { // revenue
                        sortquery += "revenue, cost, impressions";
                        queryForClicksButNoConversion = "select * from "
                                + getProductTable()
                                + " where cost > 0 and revenue = 0 order by cost desc";
                        sqlRowSet[2] = jdbcTemplate.queryForRowSet(queryForClicksButNoConversion);
                    }
                }
            }
            sqlRowSet[3] = jdbcTemplate.queryForRowSet(queryForImpressionButNoClicks);
            sqlRowSet[4] = jdbcTemplate.queryForRowSet(queryForNoImpressions);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }

//      
        if (reportType.equalsIgnoreCase("xls")) {

            filename = createXLSFile(sqlRowSet, appPath, repName, session);
        } else if (reportType.equalsIgnoreCase("pdf")) {

            filename = createPDFFile(appPath, repName, session, summaryColumns(keyMetric), summaryHeader(), recomendationsColumns(), sqlRowSet);
        } else if (reportType.equalsIgnoreCase("'sendmail'")) {

            filename = createPDFFile(appPath, repName, session, summaryColumns(keyMetric), summaryHeader(), recomendationsColumns(), sqlRowSet);
        }
        return filename;
    }

    public String createXLSFile(SqlRowSet[] sqlRowSet, String appPath,
            String repName, HttpSession session) {
        int keyMetric = Integer.parseInt(session.getAttribute("keyMetric").toString());
        try {
            int colCount;
            int rowCnt = -1;

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFFont font = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            heaCelSty.setAlignment(HorizontalAlignment.CENTER);

            HSSFFont headfont = workBook.createFont();
            headfont.setFontHeightInPoints((short) 16);
            headfont.setBold(true);
            heaCelSty.setFont(headfont);

            // Column Heading Cell Style
            font = workBook.createFont();
            font.setBold(true);

            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());

            // Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy h:mm:ss"));
            logger.info("Generating Report in .xls format");
            getSummarySheet(workBook, cell, summaryColumns(keyMetric), session, summaryHeader(), recomendationsColumns());

            for (int i = 0; i < sqlRowSet.length; i++) {
                rowCnt = 0;
                String sheetName = getSheetName(i + 1, keyMetric);
                HSSFSheet sheet = workBook.createSheet(sheetName);
                sheet.setDefaultColumnWidth(17);

                rows = sheet.createRow(rowCnt);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                cell.setCellValue(sheetName);
                sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));
                cell.setCellStyle(heaCelSty);

                SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet[i].getMetaData();
                colCount = sqlRowSetMetDat.getColumnCount();
                int count = 0;

                rowCnt = rowCnt + 2;
                rows = sheet.createRow(rowCnt);
                setFieldToDataTypeMap();
                if (i == 0 || i == 1) {
                    colCount = colCount - 1;
                }
                for (int j = 2; j <= colCount; j++) {
                    String colname = null;
                    if ((!sqlRowSetMetDat.getColumnLabel(j).equals("id")) || (!sqlRowSetMetDat.getColumnLabel(j).equals("position"))) {
                        cell = rows.createCell(j - 2);
                        for (String fieldname1 : reportFieldList) {
                            String temp1 = fieldToDataTypeMap.get(fieldname1);
                            if (temp1 == null) {
                                temp1 = fieldname1.toLowerCase().replace(" ", "") + " VARCHAR(255)";
                            }
                            if (sqlRowSetMetDat.getColumnLabel(j).equals("keyword")) {
                                colname = "Keyword";
                                break;
                            } else if (sqlRowSetMetDat.getColumnLabel(j).equals("landing_page_relevance")) {
                                colname = "Landing Page Relevance";
                                break;
                            } else if (sqlRowSetMetDat.getColumnLabel(j).equals("keyword_relevance")) {
                                colname = "Keyword Relevance";
                                break;
                            } else if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j))) {
                                if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j)) && fieldname1.equals("Cost")) {
                                    colname = "Cost";
                                    break;
                                } else if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j)) && fieldname1.equals("Spend")) {
                                    colname = "Spend";
                                    break;
                                } else {
                                    colname = fieldname1;
                                    break;
                                }
                            }
                        }

                        cell.setCellValue(colname);
                        cell.setCellStyle(columnHeaderStyle);
                    }
                }
                logger.info("completed printing header for " + sqlRowSet[i]);
                rowCnt++;
                while (sqlRowSet[i].next()) {
                    rows = sheet.createRow(rowCnt);
                    for (int colInd = 2; colInd <= colCount; colInd++) {

                        cell = rows.createCell(colInd - 2);
                        if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellValue(sqlRowSet[i]
                                    .getDate(sqlRowSetMetDat
                                            .getColumnName(colInd)));

                        } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellValue(sqlRowSet[i]
                                    .getLong(sqlRowSetMetDat
                                            .getColumnName(colInd)));
                            //numericStyle.setDataFormat(format.getFormat("0"));

                        } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.INTEGER) {
                            if (sqlRowSet[i].getInt(sqlRowSetMetDat
                                    .getColumnName(colInd)) != 0) {
                                numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                                numericStyle.setDataFormat(format.getFormat("0.0"));
                                cell.setCellValue(sqlRowSet[i]
                                        .getInt(sqlRowSetMetDat.getColumnName(colInd)));
                            }

                        } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellValue(sqlRowSet[i]
                                    .getTimestamp(sqlRowSetMetDat
                                            .getColumnName(colInd)));
                            cell.setCellStyle(dateStyle);
                        } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                            if (sqlRowSet[i].getLong(sqlRowSetMetDat
                                    .getColumnName(colInd)) != 0) {
                                numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                                cell.setCellValue(sqlRowSet[i]
                                        .getLong(sqlRowSetMetDat
                                                .getColumnName(colInd)));
                            }
                        } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DOUBLE) {

                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            numericStyle.setDataFormat(format.getFormat("0.0"));
                            cell.setCellValue(sqlRowSet[i]
                                    .getDouble(sqlRowSetMetDat.getColumnName(colInd)));

                        } else if (sqlRowSetMetDat.getColumnLabel(colInd).equals("quality_Score")
                                || sqlRowSetMetDat.getColumnLabel(colInd).equals("max_cpc")
                                || sqlRowSetMetDat.getColumnLabel(colInd).equals("ctr")) {
                            cell.setCellStyle(numericStyle);
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellValue(sqlRowSet[i]
                                    .getString(sqlRowSetMetDat
                                            .getColumnName(colInd)));
                        } else {

                            cell.setCellValue(sqlRowSet[i]
                                    .getString(sqlRowSetMetDat
                                            .getColumnName(colInd)));
                        }

                    }
                    rowCnt++;
                    count++;
                }
                logger.info("row count: " + count);
            }

            logger.info(" Report generated in .xls format");
            String reptName = Common.removeSpaces(repName);
            String filename = Common.createFileName(reptName)+ ".xls";
            String filePath = appPath + filename;
            File file = new File(filePath);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
            file = null;
            return filename;
        } catch (Exception e) {
            logger.error("Exception in createXLSFile: ",e);
        }

        return null;
    }

    public PdfPCell createPdfCell1(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
        return newCell;
    }

    public PdfPCell createPdfCellForValues(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
        return newCell;
    }

    public PdfPCell createPdfCell2(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorder(Rectangle.NO_BORDER);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public PdfPCell createPdfCell(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        return newCell;
    }

    public String createPDFFile(String appPath, String repName, HttpSession session, String[] SummaryColumns, String[] summaryHeader, String[] RecomendationsColumns, SqlRowSet[] sqlRowSetvalues)
            throws DocumentException, FileNotFoundException, BadElementException, IOException {

        ArrayList arr = (ArrayList) session.getAttribute("al");
        int keymetric = Integer.parseInt(session.getAttribute("keyMetric").toString());
        com.itextpdf.text.Font reportNameFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, com.itextpdf.text.Font.BOLD, new BaseColor(255, 110, 0)));
        com.itextpdf.text.Font blackFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        try {
            String filename = "";

            repName = "LXRMarketplace_Keyword_Analysis_Report";
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            logger.info("in side of createPdfFile. filename................. " + filename);
            String filePath = appPath + filename;
            logger.info("in side of createPdfFile. filePath................. " + filePath);
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            logger.info(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Keyword Performance Analysis Report", reportNameFont);
            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            rpName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Date: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            dateName.setBorder(Rectangle.NO_BORDER);
            dateName.setPaddingRight(40f);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(27f);
            rpName.setPaddingBottom(27f);
            rpName.setPaddingTop(6f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);

            float[] resultTabWidths = {20f, 14f, 14f, 10f, 32f};
            PdfPTable testTab = new PdfPTable(resultTabWidths);
            testTab.setWidthPercentage(99.9f);
            pdfDoc.add(Chunk.NEWLINE);

            for (int k = 0; k < summaryHeader.length; k++) {
                if (k == 2) {
                    if (keymetric == 1) {
                        PdfPCell summaryHead = createPdfCell("" + summaryHeader[k]);
                        testTab.addCell(summaryHead);
                    } else if (keymetric == 2) {
                        PdfPCell summaryHead = createPdfCell("Revenue");
                        testTab.addCell(summaryHead);
                    }
                } else {
                    PdfPCell summaryHead = createPdfCell("" + summaryHeader[k]);
                    testTab.addCell(summaryHead);
                }
            }

            if (SummaryColumns.length != RecomendationsColumns.length && SummaryColumns.length != arr.size()) {
                logger.error("Mismatch in size of arrays in summary report");
            } else {
                for (int s = 0; s < SummaryColumns.length; s++) {

                    PdfPCell summarycol = createPdfCell("" + SummaryColumns[s]);
                    testTab.addCell(summarycol);

                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(s);
                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();

                    for (int q = 1; q <= sqlRowSetMetDat.getColumnCount(); q++) {
                        if ((sqlRowSet.getString(sqlRowSetMetDat
                                .getColumnName(q))) != null) {
                            if (q == 1) {
                                int totkeys = Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)));
                                PdfPCell totalKeys = createPdfCellForValues("" + totkeys);
                                totalKeys.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                testTab.addCell(totalKeys);
                            } else if (q == 2) {
                                if (keymetric == 1) {

                                    int totconv = Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)));
                                    PdfPCell totalconv = createPdfCellForValues("" + totconv);
                                    totalconv.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                    testTab.addCell(totalconv);

                                } else if (keymetric == 2) {

                                    float totrev = Float.parseFloat(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)));

                                    PdfPCell totalrev = createPdfCellForValues("" + totrev);
                                    totalrev.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                    testTab.addCell(totalrev);
                                }
                            } else if (q == 3) {
                                float totkeys = Float.parseFloat(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)));
                                PdfPCell totalKeys1 = createPdfCellForValues("" + totkeys);
                                totalKeys1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                testTab.addCell(totalKeys1);
                            }
                        } else {
                            PdfPCell emptyStmt = createPdfCellForValues("--");
                            emptyStmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            testTab.addCell(emptyStmt);
                        }

                    }
                    for (int k = s; k <= s; k++) {
                        SqlRowSet sqlRowSet0 = (SqlRowSet) arr.get(k);
                        SqlRowSetMetaData sqlRowSetMetDat0 = sqlRowSet.getMetaData();
                        sqlRowSet.first();
                        if (k == s) {
                            if ((sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(1))) == null || (sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(1).toString().trim())).equals("0")) {
                                PdfPCell emptyStmt = createPdfCellForValues("--");
                                emptyStmt.setHorizontalAlignment(Element.ALIGN_LEFT);
                                testTab.addCell(emptyStmt);
                            } else {
                                PdfPCell recomendationscol = createPdfCellForValues(RecomendationsColumns[s]);
                                recomendationscol.setHorizontalAlignment(Element.ALIGN_LEFT);
                                testTab.addCell(recomendationscol);
                            }
                        }
                    }
                    if (s == 4) {

                        PdfPCell totalHeader = createPdfCell("Total");
                        testTab.addCell(totalHeader);

                        int totkeys = 0;
                        int totconv = 0;
                        float totrev = 0;
                        double totcost = 0.0;

                        if (session.getAttribute("totkeys") != null) {
                            totkeys = Integer.parseInt(session.getAttribute("totkeys").toString());
                        }
                        PdfPCell totKeysvalues = createPdfCellForValues("" + totkeys);
                        totKeysvalues.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        testTab.addCell(totKeysvalues);

                        if (keymetric == 1) {

                            if (session.getAttribute("totconv") != null) {
                                totconv = Integer.parseInt(session.getAttribute("totconv").toString());
                            }
                            PdfPCell totconvvalues = createPdfCellForValues("" + totconv);
                            totconvvalues.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            testTab.addCell(totconvvalues);

                        } else if (keymetric == 2) {

                            if (session.getAttribute("totrev") != null) {
                                totrev = Float.parseFloat(session.getAttribute("totrev").toString());
                            }
                            PdfPCell totrevvalues = createPdfCellForValues("" + totrev);
                            totrevvalues.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            testTab.addCell(totrevvalues);

                        }

                        if (session.getAttribute("totcost") != null) {
                            totcost = Double.parseDouble(session.getAttribute("totcost").toString());
                        }

                        PdfPCell totalCost = createPdfCellForValues("" + totcost);
                        totalCost.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        testTab.addCell(totalCost);
                        testTab.addCell(createPdfCell(""));
                    }
                }
                pdfDoc.add(testTab);
                pdfDoc.newPage();
                float[] resultTabWidths1 = {10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f};
                PdfPTable testTab1 = null;
                for (int i = 0; i < sqlRowSetvalues.length; i++) {
                    testTab1 = new PdfPTable(resultTabWidths1);
                    testTab1.getFooterHeight();
                    testTab1.setWidthPercentage(99.9f);
                    pdfDoc.add(Chunk.NEWLINE);
                    if (keymetric == 1) {
                        PdfPCell summaryHead = createPdfCell2("" + SummaryColumns[i]);
                        summaryHead.setColspan(9);
                        testTab1.addCell(summaryHead);
//                    
                    } else if (keymetric == 2) {
                        PdfPCell summaryHead = createPdfCell2("" + SummaryColumns[i]);
                        summaryHead.setColspan(9);
                        testTab1.addCell(summaryHead);
                    }
                    int colCount;
                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSetvalues[i].getMetaData();
                    colCount = sqlRowSetMetDat.getColumnCount();

                    setFieldToDataTypeMap();
                    if (i == 0 || i == 1) {
                        colCount = colCount - 1;
                    }
                    for (int j = 2; j <= colCount; j++) {
                        String colname = null;
                        if ((!sqlRowSetMetDat.getColumnLabel(j).equals("id")) || (!sqlRowSetMetDat.getColumnLabel(j).equals("position"))) {

                            for (String fieldname1 : reportFieldList) {
                                String temp1 = fieldToDataTypeMap.get(fieldname1);
                                if (temp1 == null) {
                                    temp1 = fieldname1.toLowerCase().replace(" ", "") + " VARCHAR(255)";
                                }
                                if (sqlRowSetMetDat.getColumnLabel(j).equals("keyword")) {
                                    colname = "Keyword";
                                    break;
                                } else if (sqlRowSetMetDat.getColumnLabel(j).equals("landing_page_relevance")) {
                                    colname = "Landing Page Relevance";
                                    break;
                                } else if (sqlRowSetMetDat.getColumnLabel(j).equals("keyword_relevance")) {
                                    colname = "Keyword Relevance";
                                    break;
                                } else if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j))) {
                                    if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j)) && fieldname1.equals("Cost")) {
                                        colname = "Cost";
                                        break;
                                    } else if (temp1.contains(sqlRowSetMetDat.getColumnLabel(j)) && fieldname1.equals("Spend")) {
                                        colname = "Spend";
                                        break;
                                    } else {
                                        colname = fieldname1;
                                        break;
                                    }
                                }
                            }
                            PdfPCell colnam = createPdfCell("" + colname);
                            testTab1.addCell(colnam);
                        }
                    }

                    boolean isContainData = sqlRowSetvalues[i].isBeforeFirst();
                    if (!isContainData) {
                        PdfPCell noDataCell = createPdfCell1("No Data Available");
                        noDataCell.setBorderColor(BaseColor.GRAY);
                        noDataCell.setBorderWidth(0.1f);
                        noDataCell.setPadding(6f);
                        noDataCell.setColspan(9);
                        noDataCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                        testTab1.addCell(noDataCell);
                    } else {
                        while (sqlRowSetvalues[i].next()) {
                            for (int colInd = 2; colInd <= colCount; colInd++) {
                                if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getDate(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);
                                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getLong(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);
                                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.INTEGER) {
                                    if (sqlRowSetvalues[i].getInt(sqlRowSetMetDat.getColumnName(colInd)) != 0) {
                                        PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getInt(sqlRowSetMetDat.getColumnName(colInd)));
                                        testTab1.addCell(cell1);
                                    }
                                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getTimestamp(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);
                                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                                    if (sqlRowSetvalues[i].getLong(sqlRowSetMetDat.getColumnName(colInd)) != 0) {
                                        PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getLong(sqlRowSetMetDat.getColumnName(colInd)));
                                        testTab1.addCell(cell1);
                                    }
                                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DOUBLE) {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getDouble(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);
                                } else if (sqlRowSetMetDat.getColumnLabel(colInd).equals("quality_Score")
                                        || sqlRowSetMetDat.getColumnLabel(colInd).equals("max_cpc")
                                        || sqlRowSetMetDat.getColumnLabel(colInd).equals("ctr")) {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getString(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);

                                } else {
                                    PdfPCell cell1 = createPdfCell1("" + sqlRowSetvalues[i].getString(sqlRowSetMetDat.getColumnName(colInd)));
                                    testTab1.addCell(cell1);
                                }
                            }
                        }

                    }
                    pdfDoc.add(testTab1);
                }
                pdfDoc.close();
                return filename;
            }
        } catch (DocumentException | IOException | NoSuchMessageException e) {
            logger.error("Exception in download Reports request " + e.getMessage());
        }
        return null;
    }

    public String getSheetName(int i, int keyMetric) {
        String sheetName = "";
        if (i == 0) {
            sheetName = "Summary Report";
        } else if (i == 1) {
            if (keyMetric == 1) {
                sheetName = "Top Converting Keywords";
            } else if (keyMetric == 2) {
                sheetName = "Top Revenue Generating Keywords";
            }
        } else if (i == 2) {
            if (keyMetric == 1) {
                sheetName = "Other Converting Keywords";
            } else if (keyMetric == 2) {
                sheetName = "Other Revenue Generating Keywords";
            }
        } else if (i == 3) {
            if (keyMetric == 1) {
                sheetName = "Keywords with Clicks but no Conversions";
            } else if (keyMetric == 2) {
                sheetName = "Keywords with Clicks but no Revenue";
            }
        } else if (i == 4) {
            sheetName = "Keywords with Impressions but no Clicks";
        } else if (i == 5) {
            sheetName = "Keywords with no Impressions";
        }
        return sheetName;
    }

    public static String createSampleTempinCSV() {
        String[] headerRowColumns = {"Keyword status", "Keyword", "Campaign", "Ad Group",
            "Impressions", "Clicks", "Cost", "Conversions", "Revenue"};
        PrintStream ps = null;
        try {
            String fileName = "LXRMarketplace_SampleFormat.csv";
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            ps = new PrintStream(stream);
            ps.print("* Mentioned below are the required columns (need not be in the same sequence)");
            ps.println();
            for (int i = 0; i < headerRowColumns.length; i++) {
                if (i < (headerRowColumns.length - 1)) {
                    ps.print(headerRowColumns[i] + ",");
                } else {
                    ps.print(headerRowColumns[i]);
                }
            }
            return fileName;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public StringBuilder getkeywordsXml(HttpServletRequest request) {
        HttpSession session = request.getSession();

        try {
            StringBuilder topkeywordxml = new StringBuilder();
            topkeywordxml.append("<graph caption='Keywords' showShadow='1' pieBorderThickness='0' enableRotation ='1' pieRadius='65' decimalPrecision='0' showValues='1'  showPercentageInLabel='0' baseFontSize='12' skipOverlapLabels='1' hoverCapBgColor='ffffff' pieBorderAlpha='50' animation='0' nameTBDistance='4' hoverCapSepChar=' ' bgAlpha='0'>");
            ArrayList arr = (ArrayList) session.getAttribute("al");
            if (arr.isEmpty() == false) {
                for (int i = 0; i < arr.size(); i++) {
                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);

                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();

                    for (int j = 1; j <= 1; j++) {
                        String topkeywordxmlData = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString());
                        if (topkeywordxmlData != null && !topkeywordxmlData.equals("0")) {
                            if (i == 0) {
                                topkeywordxml.append("<set  value='" + topkeywordxmlData + "' isSliced='0'  color='#4D7CB3'></set>");
                            }
                            if (i == 1) {
                                topkeywordxml.append("<set  value='" + topkeywordxmlData + "' isSliced='0'  color='#BE5653'></set>");
                            }
                            if (i == 2) {
                                topkeywordxml.append("<set  value='" + topkeywordxmlData + "' isSliced='0'  color='#96BB4A'></set>");
                            }
                            if (i == 3) {
                                topkeywordxml.append("<set  value='" + topkeywordxmlData + "' isSliced='0'  color='#78579F'></set>");
                            }
                            if (i == 4) {
                                topkeywordxml.append("<set  value='" + topkeywordxmlData + "' isSliced='0'  color='#6D7B8D'></set>");
                            }

                        }

                    }
                }
                topkeywordxml.append("</graph>");
            }
            //logger.info("topkeywordxml     :    "+topkeywordxml);
            return topkeywordxml;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public JSONArray getGooglePIEChart(HttpServletRequest request) {

        logger.info("In getGooglePIChart");
        HttpSession session = request.getSession();
        try {
            ArrayList arr = (ArrayList) session.getAttribute("al");

            JSONArray PIEChartArray = new JSONArray();
            JSONArray keywordsArray = new JSONArray();
            JSONArray ConversionRevenuArray = new JSONArray();
            JSONArray CostArray = new JSONArray();

            String[] ConversionsMetricsArray = {"Top Converting Keywords", "Other Converting Keywords", "Keywords with Clicks but no Conversions",
                "Keywords with Impressions but no Clicks", "Keywords with no Impressions"};
            String[] RevenueMetricsArray = {"TopRevenue Generating Keywords", "Other Revenue Generating Keywords", "Keywords with Clicks but no Revenue",
                "Keywords with Impressions but no Clicks", "Keywords with no Impressions"};

            /*logger.info("The ConversionsMetricsArray"+ConversionsMetricsArray+" and RevenueMetricsArray"+RevenueMetricsArray);*/
            int keyMetric1 = (Integer) session.getAttribute("keyMetric");

            if (arr.isEmpty() == false) {
                logger.info("The size of an array" + arr.size());

                for (int i = 0; i < arr.size(); i++) {
                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);

                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();
                    for (int j = 1; j <= 3; j++) {
                        String topkeywordxmlData = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString());
                        if (topkeywordxmlData != null && !topkeywordxmlData.equals("0") && !topkeywordxmlData.equals("0.0")) {
                            if (j == 1) {
                                int temp = Integer.parseInt(topkeywordxmlData);
                                if (keyMetric1 == 1) {
                                    keywordsArray.put(new JSONArray().put(ConversionsMetricsArray[i]).put(temp));
                                } else if (keyMetric1 == 2) {
                                    keywordsArray.put(new JSONArray().put(RevenueMetricsArray[i]).put(temp));
                                }
                            }
                            if (j == 2) {
                                if (keyMetric1 == 1) {
                                    int temp = Integer.parseInt(topkeywordxmlData);
                                    ConversionRevenuArray.put(new JSONArray().put(ConversionsMetricsArray[i]).put(temp));
                                } else if (keyMetric1 == 2) {
                                    double temp = Double.parseDouble(topkeywordxmlData);
                                    ConversionRevenuArray.put(new JSONArray().put(RevenueMetricsArray[i]).put(temp));
                                }
                            }
                            if (j == 3) {
                                double temp = Double.parseDouble(topkeywordxmlData);
                                if (keyMetric1 == 1) {
                                    CostArray.put(new JSONArray().put(ConversionsMetricsArray[i]).put(temp));
                                } else if (keyMetric1 == 2) {
                                    CostArray.put(new JSONArray().put(RevenueMetricsArray[i]).put(temp));
                                }

                            }
                        }

                    }
                }
                /*logger.info("The Content of keywordChartRow"+keywordsArray);
				logger.info("The Content of ConversionRevenuArray"+ConversionRevenuArray);
				logger.info("The Content of CostArray"+CostArray);*/
                PIEChartArray.put(keywordsArray);
                PIEChartArray.put(ConversionRevenuArray);
                PIEChartArray.put(CostArray);
                /*logger.info("PIEChartArray Content"+PIEChartArray);*/
                return PIEChartArray;
            }
            return PIEChartArray;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;

    }

    public StringBuilder getConversionsXml(HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            StringBuilder conversionsXml = new StringBuilder();
            conversionsXml.append("<graph caption='Conversions'  showShadow='1' skipOverlapLabels='0' hoverCapBgColor='ffffff' pieBorderThickness='0' enableRotation ='1' pieRadius='65' decimalPrecision='0'  baseFontSize='12' pieBorderAlpha='50' animation='0' nameTBDistance='4' hoverCapSepChar=' ' bgAlpha='0'>");
            ArrayList arr = (ArrayList) session.getAttribute("al");
            if (arr.isEmpty() == false) {
                for (int i = 0; i < arr.size(); i++) {
                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);
                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();
                    for (int j = 2; j <= 2; j++) {
                        String conversionsXmlData = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString());
                        if (conversionsXmlData != null && !conversionsXmlData.equals("0")) {
                            if (i == 0) {
                                conversionsXml.append("<set  value='" + conversionsXmlData + "' isSliced='0'  color='#4D7CB3'></set>");
                            } else if (i == 1) {
                                conversionsXml.append("<set  value='" + conversionsXmlData + "' isSliced='0'  color='#BE5653'/>");
                            } else if (i == 2) {
                                conversionsXml.append("<set  value='" + conversionsXmlData + "' isSliced='0'   color='#96BB4A'/>");
                            } else if (i == 3) {
                                conversionsXml.append("<set  value='" + conversionsXmlData + "' isSliced='0'  color='#78579F'/>");
                            } else if (i == 4) {
                                conversionsXml.append("<set  value='" + conversionsXmlData + "' isSliced='0'  color='#6D7B8D'/>");
                            }
                        }
                    }
                }
                conversionsXml.append("</graph>");
            }
            //logger.info("conversionsXml     :    "+conversionsXml);
            return conversionsXml;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public StringBuilder getRevenueXml(HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            StringBuilder revenueXml = new StringBuilder();
            revenueXml.append("<graph caption='Revenue'  showShadow='1' skipOverlapLabels='0' hoverCapBgColor='ffffff' pieBorderThickness='0' enableRotation ='1' pieRadius='65' decimalPrecision='2' baseFontSize='12' pieBorderAlpha='50' animation='0' nameTBDistance='4' hoverCapSepChar=' ' bgAlpha='0'>");
            ArrayList arr = (ArrayList) session.getAttribute("al");
            if (arr.isEmpty() == false) {
                for (int i = 0; i < arr.size(); i++) {
                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);
                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();
                    for (int j = 2; j <= 2; j++) {
                        String revenueXmlData = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(j).toString());
                        if (revenueXmlData != null && !revenueXmlData.equals("0.0")) {
                            if (i == 0) {
                                revenueXml.append("<set  value='" + revenueXmlData + "' isSliced='0'  color='#4D7CB3'></set>");
                            } else if (i == 1) {
                                revenueXml.append("<set  value='" + revenueXmlData + "' isSliced='0'  color='#BE5653'/>");
                            } else if (i == 2) {
                                revenueXml.append("<set  value='" + revenueXmlData + "' isSliced='0'   color='#96BB4A'/>");
                            } else if (i == 3) {
                                revenueXml.append("<set  value='" + revenueXmlData + "' isSliced='0'  color='#78579F'/>");
                            } else if (i == 4) {
                                revenueXml.append("<set  value='" + revenueXmlData + "' isSliced='0'  color='#6D7B8D'/>");
                            }
                        }
                    }
                }
                revenueXml.append("</graph>");
            }
            //logger.info("revenueXml     :    "+revenueXml);
            return revenueXml;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    public StringBuilder getCostXml(HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            StringBuilder CostXml = new StringBuilder();
            CostXml.append("<graph caption='Cost' showShadow='1' pieBorderThickness='0' hoverCapBgColor='ffffff' pieRadius='65' enableRotation ='1'  baseFontSize='12' pieBorderAlpha='50' animation='0' nameTBDistance='4' hoverCapSepChar=' ' pieFillAlpha='100'>");

            ArrayList arr = (ArrayList) session.getAttribute("al");
            if (arr.isEmpty() == false) {
                for (int i = 0; i < arr.size(); i++) {
                    SqlRowSet sqlRowSet = (SqlRowSet) arr.get(i);
                    SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                    sqlRowSet.first();
                    for (int j = 3; j <= 3; j++) {
                        String costXmlData = sqlRowSet.getString(sqlRowSetMetDat
                                .getColumnName(j).toString());
                        if (costXmlData != null && !costXmlData.equals("0.0")) {
                            if (i == 0) {
                                CostXml.append("<set value='" + costXmlData + "' isSliced='0'  color='#4D7CB3'/>");
                            } else if (i == 1) {
                                CostXml.append("<set value='" + costXmlData + "' isSliced='0'  color='#BE5653'/>");
                            } else if (i == 2) {
                                CostXml.append("<set value='" + costXmlData + "' isSliced='0'  color='#96BB4A'/>");
                            } else if (i == 3) {
                                CostXml.append("<set value='" + costXmlData + "' isSliced='0'   color='#78579F'/>");
                            } else if (i == 4) {
                                CostXml.append("<set value='" + costXmlData + "' isSliced='0'  color='#6D7B8D'/>");
                            }
                        }
                    }
                }
                CostXml.append("</graph>");
            }
            //logger.info("CostXml     :    "+CostXml);
            return CostXml;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void getSummarySheet(HSSFWorkbook workBook, HSSFCell cell, String[] SummaryColumns,
            HttpSession session, String[] summaryHeader, String[] RecomendationsColumns) {
        ArrayList arr = (ArrayList) session.getAttribute("al");
        int keymetric = Integer.parseInt(session.getAttribute("keyMetric").toString());
        int colWidth[] = {18, 15, 12, 10, 85};
        int rowCnt = 0;
        HSSFRow rows;

        //for main heading
        HSSFFont fontHeading = workBook.createFont();
        fontHeading.setBold(true);
        HSSFCellStyle mainheadCellSty;
        mainheadCellSty = workBook.createCellStyle();
        mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
        mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
        mainheadCellSty.setFont(fontHeading);
        HSSFFont headfont = workBook.createFont();
        //To increase the  font-size
        headfont.setFontHeightInPoints((short) 20);
        headfont.setColor(HSSFColor.ORANGE.index);
        HSSFSheet sheet = workBook.createSheet(getSheetName(0, 0));
        boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
        rows = sheet.createRow(0);
        cell = rows.createCell(1);
        cell.setCellValue("Keyword Performance Analyzer Report");
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));
        mainheadCellSty.setFont(headfont);
        cell.setCellStyle(mainheadCellSty);
        rows.setHeight((short) 800);

        HSSFCellStyle dateCellSty;
        dateCellSty = workBook.createCellStyle();
        dateCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
        dateCellSty.setAlignment(HorizontalAlignment.RIGHT);
        dateCellSty.setFont(fontHeading);
        rows = sheet.createRow(4);
        cell = rows.createCell(0);
        rows.setHeight((short) 500);
        Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
        SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
        dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String curDate = dtf.format(startDat);
        sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 4));
        rows.setHeight((short) 500);
        cell.setCellValue("Date: " + curDate);
        cell.setCellStyle(dateCellSty);
        if (logoAdded) {
            rowCnt = 6;
        }

        rows = sheet.createRow(rowCnt++);
        HSSFFont font = workBook.createFont();
        HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
        CellStyle numericStyle = workBook.createCellStyle();
        CellStyle recommendedStyle = workBook.createCellStyle();
        recommendedStyle.setAlignment(HorizontalAlignment.CENTER);
        DataFormat format = workBook.createDataFormat();
        font = workBook.createFont();
        font.setBold(true);
        columnHeaderStyle.setFont(font);

        for (int i = 0; i < summaryHeader.length; i++) {
            sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
        }

        for (int k = 0; k < summaryHeader.length; k++) {
            cell = rows.createCell(k);
            if (k == 2) {
                if (keymetric == 1) {
                    cell.setCellValue(summaryHeader[k]);
                } else if (keymetric == 2) {
                    cell.setCellValue("Revenue");
                }
            } else {
                cell.setCellValue(summaryHeader[k]);
            }
            cell.setCellStyle(columnHeaderStyle);
        }

        int colcount = 0;
        if (SummaryColumns.length != RecomendationsColumns.length && SummaryColumns.length != arr.size()) {
            logger.error("Mismatch in size of arrays in summary report");
        } else {
            int freeRowCount = rowCnt;
            for (int s = 0; s < SummaryColumns.length; s++) {
                rows = sheet.createRow(rowCnt);
                switch (s) {
                    case 2:
                        rows.setHeight((short) 1200);
                        break;
                    case 3:
                        rows.setHeight((short) 1000);
                        break;
                    default:
                        rows.setHeight((short) 800);
                        break;
                }
                colcount = 0;

                cell = rows.createCell(colcount++);
                cell.setCellValue(SummaryColumns[s]);
                SqlRowSet sqlRowSet = (SqlRowSet) arr.get(s);
                SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                sqlRowSet.first();

                for (int q = 1; q <= sqlRowSetMetDat.getColumnCount(); q++) {
                    cell = rows.createCell(colcount++);

                    if ((sqlRowSet.getString(sqlRowSetMetDat
                            .getColumnName(q))) != null) {
                        if (q == 1) {
                            int totkeys = Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)).toString());
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellValue(totkeys);
                        } else if (q == 2) {
                            if (keymetric == 1) {
                                numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                                int totconv = Integer.parseInt(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)).toString());
                                cell.setCellValue(totconv);
                            } else if (keymetric == 2) {
                                numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                                float totrev = Float.parseFloat(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)).toString());
                                numericStyle.setDataFormat(format.getFormat("0.00"));
                                cell.setCellStyle(numericStyle);
                                cell.setCellValue(totrev);
                            }
                        } else if (q == 3) {
                            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                            cell.setCellStyle(numericStyle);
                            numericStyle.setDataFormat(format.getFormat("0.00"));
                            float totkeys = Float.parseFloat(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(q)).toString());
                            cell.setCellValue(totkeys);
                        }
                    } else {
                        numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                        cell.setCellStyle(numericStyle);
                        cell.setCellValue("--");
                    }

                }
                cell = rows.createCell(colcount);
                if (s == 0) {
                    for (int k = 0; k <= 0; k++) {
                        SqlRowSet sqlRowSet0 = (SqlRowSet) arr.get(k);
                        SqlRowSetMetaData sqlRowSetMetDat0 = sqlRowSet.getMetaData();
                        sqlRowSet.first();
                        for (int p = 1; p <= 1; p++) {
                            if (k == 0) {
                                if ((sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(p).toString())) == null || (sqlRowSet0.getString(sqlRowSetMetDat0.getColumnName(p).toString().trim())).equals("0")) {
                                    cell.setCellValue("--");
                                } else {
                                    cell.setCellValue(RecomendationsColumns[s]);
                                }
                            }
                        }
                    }
                }
                if (s == 1) {
                    for (int k1 = 1; k1 <= 1; k1++) {
                        SqlRowSet sqlRowSet1 = (SqlRowSet) arr.get(k1);
                        SqlRowSetMetaData sqlRowSetMetDat1 = sqlRowSet.getMetaData();
                        sqlRowSet1.first();
                        for (int p1 = 1; p1 <= 1; p1++) {
                            if (k1 == 1) {
                                if ((sqlRowSet1.getString(sqlRowSetMetDat1.getColumnName(p1).toString())) == null || (sqlRowSet1.getString(sqlRowSetMetDat1.getColumnName(p1).toString().trim())).equals("0")) {
                                    cell.setCellValue("--");
                                } else {
                                    cell.setCellValue(RecomendationsColumns[s]);
                                }
                            }
                        }
                    }
                }
                if (s == 2) {
                    for (int k2 = 2; k2 <= 2; k2++) {
                        SqlRowSet sqlRowSet2 = (SqlRowSet) arr.get(k2);
                        SqlRowSetMetaData sqlRowSetMetDat2 = sqlRowSet.getMetaData();
                        sqlRowSet2.first();
                        for (int p2 = 1; p2 <= 1; p2++) {
                            if (k2 == 2) {
                                if ((sqlRowSet2.getString(sqlRowSetMetDat2.getColumnName(p2).toString())) == null || (sqlRowSet2.getString(sqlRowSetMetDat2.getColumnName(p2).toString().trim())).equals("0")) {
                                    cell.setCellValue("--");
                                } else {
                                    cell.setCellValue(RecomendationsColumns[s]);
                                }
                            }
                        }
                    }
                }
                if (s == 3) {
                    for (int k3 = 3; k3 <= 3; k3++) {
                        SqlRowSet sqlRowSet3 = (SqlRowSet) arr.get(k3);
                        SqlRowSetMetaData sqlRowSetMetDat3 = sqlRowSet.getMetaData();
                        sqlRowSet3.first();
                        for (int p3 = 1; p3 <= 1; p3++) {
                            if (k3 == 3) {
                                if ((sqlRowSet3.getString(sqlRowSetMetDat3.getColumnName(p3).toString())) == null || (sqlRowSet3.getString(sqlRowSetMetDat3.getColumnName(p3).toString().trim())).equals("0")) {
                                    cell.setCellValue("--");
                                } else {
                                    cell.setCellValue(RecomendationsColumns[s]);
                                }
                            }
                        }
                    }
                }
                if (s == 4) {
                    for (int k4 = 4; k4 <= 4; k4++) {
                        SqlRowSet sqlRowSet4 = (SqlRowSet) arr.get(k4);
                        SqlRowSetMetaData sqlRowSetMetDat4 = sqlRowSet.getMetaData();
                        sqlRowSet4.first();
                        for (int p4 = 1; p4 <= 1; p4++) {
                            if (k4 == 4) {
                                if ((sqlRowSet4.getString(sqlRowSetMetDat4.getColumnName(p4).toString())) == null || (sqlRowSet4.getString(sqlRowSetMetDat4.getColumnName(p4).toString().trim())).equals("0")) {
                                    cell.setCellValue("--");
                                } else {
                                    cell.setCellValue(RecomendationsColumns[s]);
                                }
                            }
                        }
                    }
                }
                rowCnt++;
            }
            rows = sheet.createRow(rowCnt);
            colcount = 0;
            cell = rows.createCell(colcount++);
            cell.setCellValue("Total");
            cell = rows.createCell(colcount++);
            int totkeys = 0;
            int totconv = 0;
            float totrev = 0;
            double totcost = 0.0;
            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
            if (session.getAttribute("totkeys") != null) {
                totkeys = Integer.parseInt(session.getAttribute("totkeys").toString());
            }
            cell.setCellValue(totkeys);
            cell = rows.createCell(colcount++);
            if (keymetric == 1) {
                numericStyle.setAlignment(HorizontalAlignment.RIGHT);
                if (session.getAttribute("totconv") != null) {
                    totconv = Integer.parseInt(session.getAttribute("totconv").toString());
                }
                cell.setCellValue(totconv);
            } else if (keymetric == 2) {
                numericStyle.setDataFormat(format.getFormat("0.00"));
                cell.setCellStyle(numericStyle);
                if (session.getAttribute("totrev") != null) {
                    totrev = Float.parseFloat(session.getAttribute("totrev").toString());
                }
                cell.setCellValue(totrev);
            }
            cell = rows.createCell(colcount++);
            numericStyle.setAlignment(HorizontalAlignment.RIGHT);
            numericStyle.setDataFormat(format.getFormat("0.00"));
            cell.setCellStyle(numericStyle);
            if (session.getAttribute("totcost") != null) {
                totcost = Double.parseDouble(session.getAttribute("totcost").toString());
            }
            cell.setCellValue(totcost);

        }

    }

    public String[] summaryHeader() {

        return new String[]{"Keyword Category", "No. of Keywords", "Conversions", "Cost", "Recommendations"};

    }

    public String[] summaryColumns(int keyMetric) {
        if (keyMetric == 1) {
            return new String[]{"Top Converting Keywords", "Other Converting Keywords", "Keywords with Clicks but no Conversions", "Keywords with Impressions but no Clicks", "Keywords with no Impressions"};
        } else if (keyMetric == 2) {
            return new String[]{"Top Revenue Generating Keywords", "Other Revenue Generating Keywords", "Keywords with Clicks but no Revenue", "Keywords with Impressions but no Clicks", "Keywords with no Impressions"};
        } else {
            return null;
        }
    }

    public String[] recomendationsColumns() {
        return new String[]{"1. Reallocate budget & Invest More \n2. Increase Bids & Maintain Optimal positions \n3. Add Keyword Variations ",
            "1. Optimize setup to increase keyword profitability \n2. Focus first on top converting keywords in the group \n3. Reduce spending on high cost keywords with low conversions.",
            "1. Stop advertising on keywords that have been in this set for more than 3 months \n2. Try adding other keyword variations \n3. Shift budget from this set to converting keywords \n4. Check search queries linked with these keywords and add the irrelevant ones as negatives \n5. Make ad copies more specific and powerful",
            "1. Check the themes and relevancy of the campaigns and ad groups for these keywords \n2. Have them grouped in the smaller campaigns and ad groups that match their themes more closely \n3. Check if the keywords are triggering right ads \n4. Try using Keyword Insertion and Keyword Dynamic ads for these keywords ",
            "1. Group these keywords into smaller, tighter keyword sets \n2. Check the match types of the keywords and try using broad match for all the keywords that have \n  only restricted match types(Phrase and Exact)"};
    }

    public boolean dropPrevTable(String prevTableName) {
        boolean dropped = false;
        logger.info("deleting previous database table for table name: " + prevTableName);
        try {
            jdbcTemplate.execute("drop table " + prevTableName);
            dropped = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dropped;
    }

    public static String compressFile(String filePath, String fileName, boolean dummy) {
        String file = "";
        try {
            File f = new File(filePath + fileName.substring(0, fileName.lastIndexOf(".")) + ".zip");
            FileOutputStream fos = new FileOutputStream(f);
            ZipOutputStream targetStream = new ZipOutputStream(fos);
            targetStream.setMethod(ZipOutputStream.DEFLATED);

            //String dataFileName = "";
            FileInputStream fis = new FileInputStream(filePath + fileName);
            BufferedInputStream sourceStream = new BufferedInputStream(fis);

            ZipEntry theEntry = new ZipEntry(fileName);
            targetStream.putNextEntry(theEntry);

            byte[] data = new byte[1024];
            int bCnt;
            while ((bCnt = sourceStream.read(data, 0, 1024)) != -1) {
                targetStream.write(data, 0, bCnt);
            }
            targetStream.flush();
            targetStream.closeEntry();
            targetStream.close();
            sourceStream.close();
            file = fileName.substring(0, fileName.lastIndexOf(".")) + ".zip";
        } catch (Exception exce) {
            exce.printStackTrace();
            return null;
        }
        return file;
    }

}
