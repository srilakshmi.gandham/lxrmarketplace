package lxr.marketplace.keywordanalyzer;

import org.springframework.web.multipart.MultipartFile;

public class KeywordAnalyzerTool {

    private int keyMetric;
    private String upFileName;
    private MultipartFile uploadFile;

    public String getUpFileName() {
        return upFileName;
    }

    public void setUpFileName(String upFileName) {
        this.upFileName = upFileName;
    }

    public int getKeyMetric() {
        return keyMetric;
    }

    public void setKeyMetric(int keyMetric) {
        this.keyMetric = keyMetric;
    }

    public MultipartFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(MultipartFile uploadFile) {
        this.uploadFile = uploadFile;
    }

}
