package lxr.marketplace.keywordanalyzer;

import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class KeywordAnalyzerController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(KeywordAnalyzerController.class);

    String uploadFolder;
    String downloadFolder;

    @Resource
    JdbcTemplate jdbcTemplate;
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    LoginService loginService;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public static void setLogger(Logger logger) {
        KeywordAnalyzerController.logger = logger;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public KeywordAnalyzerController() {
        setCommandClass(KeywordAnalyzerTool.class);
        setCommandName("keywordAnalyzerTool");
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        JSONArray arr = new JSONArray();
        session.setAttribute("json", arr);
        KeywordAnalyzerService keywordAnalyzerService = new KeywordAnalyzerService();
        KeywordAnalyzerTool keywordAnalyzerTool = (KeywordAnalyzerTool) command;
        keywordAnalyzerService = new KeywordAnalyzerService();
        keywordAnalyzerService.setJdbcTemplate(jdbcTemplate);
        String dwnfileName = "";
        String tablename = "";
        String[] fields = null;
//        String fileName = "";
//        if (session.getAttribute("fileName") != null) {
//            fileName = session.getAttribute("fileName").toString();
//        }
        if (session.getAttribute("keywordAnalyzerTempTable") != null) {
            tablename = session.getAttribute("keywordAnalyzerTempTable").toString();
        }
        if (session.getAttribute("fields") != null) {
            fields = (String[]) session.getAttribute("fields");
        }
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(4);
            toolObj.setToolName("PPC Performance Tool : Keyword Analyzer");
            toolObj.setToolLink("ppc-keyword-performance-analyzer-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.KEYWORD_ANALYZER);
//            if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
//                if (!fileName.equals("")) {
//                    File f = new File(uploadFolder.concat(fileName));
//                    if (f.exists()) {
//                        f.delete();
//                        logger.info("previously uploaded file is deleted");
//                    }
//                }
//                fileName = keywordAnalyzerService.saveFile(uploadFolder, request, response);
//                session.setAttribute("fileName", fileName);
//                return null;
//            } else 
            if (WebUtils.hasSubmitParameter(request, "sampleTemp")) {
                String tempFileName = "Keyword Report";
                tempFileName = KeywordAnalyzerService.createSampleTempinCSV();
                File f = new File(tempFileName);
                response.setContentType("xls");
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
                FileInputStream fis = new FileInputStream(f);
                FileCopyUtils.copy(fis, response.getOutputStream());
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                session.removeAttribute("keywordAnalyzerTool");
                session.removeAttribute("totkeys");
                session.removeAttribute("totrev");
                session.removeAttribute("totcost");
                session.removeAttribute("totconv");
                session.removeAttribute("json");
                arr = new JSONArray();
                session.setAttribute("json", arr);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                mAndV.addObject("clearAll", "clearAll");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "getResult")) {
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                    mAndV.addObject("getSuccess", "success");
                    return mAndV;
                }

                MultipartFile multipartFile = keywordAnalyzerTool.getUploadFile();
                int keyMetric = keywordAnalyzerTool.getKeyMetric();
                String fileName = null;

                if (multipartFile.getOriginalFilename() != null && !multipartFile.getOriginalFilename().equals("")) {
                    fileName = Common.saveFile(multipartFile, uploadFolder);
                    session.setAttribute("fileName", fileName);
                } else if (session.getAttribute("fileName") != null) {
                    fileName = (String) session.getAttribute("fileName");
                }
                if (fileName != null) {

                    keywordAnalyzerTool.setUpFileName(fileName);

                    String columnsUploaded[] = readFile(uploadFolder.concat(fileName), request, response, keyMetric, arr);
                    String colValidation = arr.toString();
                    colValidation = colValidation.substring(1, colValidation.length() - 1);
                    if (fileName.contains(".csv") && colValidation.equals("\"nocolumns\"")) {
                        String newFileName = fileName.substring(0, fileName.indexOf(".csv")) + ".tsv";
                        //TODO: Renaming file to .tsv
                        File file = new File(uploadFolder.concat(fileName));
                        File file2 = new File(uploadFolder.concat(newFileName));
                        /*if (file2.exists()) {
                            throw new java.io.IOException("file exists");
                        }*/
                        boolean success = file.renameTo(file2);
                        if (success) {
                            fileName = newFileName;
                            arr = new JSONArray();
                            columnsUploaded = readFile(uploadFolder.concat(fileName), request, response, keyMetric, arr);
                            colValidation = arr.toString();
                            colValidation = colValidation.substring(1, colValidation.length() - 1);
                        }
                    }
                    if (colValidation.equalsIgnoreCase("\"allcolumns\"")) {
                        session.setAttribute("json", arr);
                        session.setAttribute("keywordAnalyzerTool", keywordAnalyzerTool);
                        if (columnsUploaded != null) {
                            Common.modifyDummyUserInToolorVideoUsage(request, response);
                            if (session.getAttribute("keywordAnalyzerTempTable") != null) {
                                keywordAnalyzerService.dropPrevTable(tablename);
                            }
                            keywordAnalyzerService.fileToDBTable(fileName, uploadFolder);
                            tablename = keywordAnalyzerService.getProductTable();
                            session.setAttribute("keywordAnalyzerTempTable", tablename);
                            fields = keywordAnalyzerService.getReportFieldList();
                            session.setAttribute("fields", fields);
                        }
                        session.setAttribute("keyMetric", keyMetric);
                        for (int i = 0; i < 5; i++) {
                            String dataSql = keywordAnalyzerService.getDatasql(keywordAnalyzerTool.getKeyMetric(), i);//,tablename);
                            ArrayList al = keywordAnalyzerService.getList(dataSql);
                            session.setAttribute("al", al);
                        }
                        keywordAnalyzerService.getTotals(session, keyMetric);

                        ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                        mAndV.addObject("getSuccess", "success");
                        return mAndV;
                    } else {
                        session.setAttribute("json", arr);
                        session.removeAttribute("al");
                        session.removeAttribute("keywordAnalyzerTool");
                        ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                        return mAndV;
                    }
                } else {
                    ModelAndView mAndV = new ModelAndView("keywordanalyzer/KeywordAnalyzer", null);
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                keywordAnalyzerService.setProductTable(tablename);
                keywordAnalyzerService.setReportFieldList(fields);
                int keyMetricsel = (Integer) session.getAttribute("keyMetric");
                String downParam = request.getParameter("report-type");
                if (downParam.equalsIgnoreCase("pdf") || downParam.equalsIgnoreCase("xls")) {
                    dwnfileName = keywordAnalyzerService.generateReportData(keyMetricsel, downloadFolder, session, downParam);
                    Common.downloadReport(response, downloadFolder, dwnfileName, downParam);
                } else if (downParam.equalsIgnoreCase("'sendmail'")) {
                    String toAddrs = request.getParameter("email");
                    String toolName = "PPC Keyword Performance Analyzer";//TODO: get tool name from DB
                    try {
                        dwnfileName = keywordAnalyzerService.generateReportData(keyMetricsel, downloadFolder, session, downParam);
                        Common.sendReportThroughMail(request, downloadFolder, dwnfileName, toAddrs, toolName);
                        return null;
                    } catch (DocumentException | IOException e) {
                        logger.error("Exception on sending pdf report:", e);
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "getGoogleData")) {
                Session userSession = (Session) session.getAttribute("userSession");
                userSession.addToolUsage(toolObj.getToolId());
                logger.info("For Parameter  getGoogleData");
                org.json.JSONArray keywordChartRow = keywordAnalyzerService.getGooglePIEChart(request);
                if (keywordChartRow != null) {
                    response.setContentType("application/json");
                    PrintWriter writer = null;
                    try {
                        writer = response.getWriter();
                        writer.print(keywordChartRow);
                        writer.flush();
                        writer.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        logger.error(e.getCause());
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        writer.print("{success: false}");
                    } finally {
                        writer.flush();
                        writer.close();
                    }
                } else {
                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                    return mAndV;
                }
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                ArrayList aList = (ArrayList) session.getAttribute("al");
                double totWastingCost = 0;
                if (aList != null && aList.isEmpty() == false) {
                    for (int i = 0; i < aList.size(); i++) {
                        if (i == 2) {
                            SqlRowSet sqlRowSet = (SqlRowSet) aList.get(i);
                            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
                            totWastingCost += sqlRowSet.getDouble(sqlRowSetMetDat.getColumnName(3));
                        }
                    }
                }
                String toolIssues = "";
                String questionInfo = "";
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                if (totWastingCost > 0) {
                    questionInfo = "Need help in fixing following issues.\n\n";
                    questionInfo += "- " + "Audit non performing keywords for profitability." + "\n";
                    toolIssues += "You are currently wasting <span style='color:red;'>" + totWastingCost + "</span> on keywords that are not relevant to your website. "
                            + "We can provide a list of negative keywords that can help to reduce your spend.";
                }
                lxrmAskExpert.setDomain("");
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(Common.KEYWORD_ANALYZER);
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                KeywordAnalyzerTool keywordAnalyzerExisteing = (KeywordAnalyzerTool) session.getAttribute("keywordAnalyzerTool");
                if (keywordAnalyzerExisteing == null) {
                    session.removeAttribute("keywordChartRow");
                    session.removeAttribute("keyMetric");
                    session.removeAttribute("keywordAnalyzerTool");
                    session.removeAttribute("totkeys");
                    session.removeAttribute("totrev");
                    session.removeAttribute("totcost");
                    session.removeAttribute("totconv");
                    session.removeAttribute("json");
                    arr = new JSONArray();
                    session.setAttribute("json", arr);
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                return mAndV;
            } else {
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
                return mAndV;
            }
        } else {
            ModelAndView mAndV = new ModelAndView(getFormView(), "keywordAnalyzerTool", keywordAnalyzerTool);
            return mAndV;
        }
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        KeywordAnalyzerTool keywordAnalyzerTool = (KeywordAnalyzerTool) session.getAttribute("keywordAnalyzerTool");
        if (keywordAnalyzerTool == null) {
            keywordAnalyzerTool = new KeywordAnalyzerTool();
            keywordAnalyzerTool.setKeyMetric(0);
        }
        session.removeAttribute("json");
        return keywordAnalyzerTool;
    }

    protected String[] readFile(String fileName, HttpServletRequest request, HttpServletResponse response,
            int keyMetric, JSONArray arr) {
        KeywordAnalyzerService keywordAnalyzerService = null;
        keywordAnalyzerService = new KeywordAnalyzerService();
        String[] mandatorycolNames;

        switch (keyMetric) {
            case 1:
                mandatorycolNames = new String[]{"keyword", "conversions", "cost", "impressions", "clicks"};
                break;
            case 2:
                mandatorycolNames = new String[]{"keyword", "revenue", "cost", "impressions", "clicks"};
                break;
            default:
                arr.add("wrongkeymetric");
                return null;
        }
        try {
            String columns[] = keywordAnalyzerService.getListByOption(fileName, "columnList", 0, true, keyMetric, response);
            keywordAnalyzerService.setFieldToDataTypeMap();
            if (columns != null && !columns[0].equals("empty")) {
                if (!columns[0].equals("noheader")) {
                    if (!columns[0].equals("nodata")) {
                        boolean flag = false;
                        boolean x = true;
                        for (String mandatorycolName : mandatorycolNames) {
                            for (String column : columns) {
                                flag = false;
                                String temp = keywordAnalyzerService.fieldToDataTypeMap.get(column);
                                if ((temp != null) && mandatorycolName.equals(temp.substring(0, temp.indexOf(' ')))) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag) {
                                mandatorycolName = mandatorycolName.substring(0, 1).toUpperCase() + mandatorycolName.substring(1);
                                arr.add(mandatorycolName);
                                x = false;
                            }
                        }
                        if (x) {
                            arr.add("allcolumns");
                        } else {
                            logger.error("Missing Columns " + Arrays.toString(mandatorycolNames));
                        }
                        return mandatorycolNames;
                    } else if (columns[0].equals("nodata")) {
                        arr.add("nodata");
                        return null;
                    }

                } else if (columns[0].equals("noheader")) {
                    arr.add("nocolumns");
                    return null;
                }
            } else if (columns != null && columns[0].equals("empty")) {
                arr.add("empty");
                return null;
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

}
