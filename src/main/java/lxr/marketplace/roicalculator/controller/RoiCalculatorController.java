package lxr.marketplace.roicalculator.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RoiCalculatorController {

    private static Logger logger = Logger.getLogger(RoiCalculatorController.class);
    @RequestMapping(value = "return-on-investment-calculator-tool.html")
    protected String getROICalculator(Model model, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Tool toolObj = new Tool();
        toolObj.setToolId(3);
        toolObj.setToolName("ROI Calculator");
        toolObj.setToolLink("return-on-investment-calculator-tool.html");
        session.setAttribute("toolObj", toolObj);
        session.setAttribute("currentTool", Common.ROI_CALCULATOR);
        return "views/roiCalculator/roiCalculator";
    }
}
