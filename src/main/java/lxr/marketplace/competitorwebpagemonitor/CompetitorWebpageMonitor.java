package lxr.marketplace.competitorwebpagemonitor;

import java.sql.Timestamp;


public class CompetitorWebpageMonitor {
	private String competitorUrl;
	private String email;
	private int alertFrequency;
	private long user_Id; 
	private String newUserEmail;
	private Timestamp Startdate;
	private boolean emailUnsubscription;
	private boolean toolUnsubscription;
	
	public String getCompetitorUrl() {
		return competitorUrl;
	}
	public boolean isEmailUnsubscription() {
		return emailUnsubscription;
	}
	public void setEmailUnsubscription(boolean emailUnsubscription) {
		this.emailUnsubscription = emailUnsubscription;
	}
	public boolean isToolUnsubscription() {
		return toolUnsubscription;
	}
	public void setToolUnsubscription(boolean toolUnsubscription) {
		this.toolUnsubscription = toolUnsubscription;
	}
	public Timestamp getStartdate() {
		return Startdate;
	}
	public void setStartdate(Timestamp startdate) {
		Startdate = startdate;
	}
	public void setCompetitorUrl(String competitorUrl) {
		competitorUrl = competitorUrl.trim();
		this.competitorUrl = competitorUrl;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAlertFrequency() {
		return alertFrequency;
	}
	public void setAlertFrequency(int alertFrequency) {
		this.alertFrequency = alertFrequency;
	}
	public long getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(long user_Id) {
		this.user_Id = user_Id;
	}
	public String getNewUserEmail() {
		return newUserEmail;
	}
	public void setNewUserEmail(String newUserEmail) {
		this.newUserEmail = newUserEmail;
	}
	
//	public String getAlertName() {
//		return alertName;
//	}
//	public void setAlertName(String alertName) {
//		this.alertName = alertName;
//	}

}
