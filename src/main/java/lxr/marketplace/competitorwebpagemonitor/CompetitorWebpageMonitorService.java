package lxr.marketplace.competitorwebpagemonitor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.parser.MarketplaceHTMLTextParser;

public class CompetitorWebpageMonitorService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(CompetitorWebpageMonitorService.class);

    String imageFolder, contentFolder, tempFolder;
    String supportMail;
    boolean jsoupconnerror = false;

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public static Logger getLogger() {
        return logger;
    }

    public String getContentFolder() {
        return contentFolder;
    }

    public void setContentFolder(String contentFolder) {
        this.contentFolder = contentFolder;
    }

    public void setImageFolder(String imageFolder) {
        this.imageFolder = imageFolder;
    }

    public String getImageFolder() {
        return imageFolder;
    }

    public void setTempFolder(String tempFolder) {
        this.tempFolder = tempFolder;
    }

    public String getTempFolder() {
        return tempFolder;
    }

    public boolean isJsoupconnerror() {
        return jsoupconnerror;
    }

    public void setJsoupconnerror(boolean jsoupconnerror) {
        this.jsoupconnerror = jsoupconnerror;
    }

    public void getCmpResults(String url, long tempInfoId, long userId, CmpSummary cmpsummary, int val[]) {
        url = CompetitorWebpageMonitorService.getUrlValidation(url);
        int timeout = 40000;
        int sllepOut = 10000;
        String userAgent = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
        /* Modified on oct-7 2015
	* "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
        Document doc = null;
        long timeoutIteration = 0;
        Connection jsoupConn = null;
        long noOfTime = 3;
        int contentType = 0;
        String oldContent = "";
        String content = "";
        String bodyText = "";
        StringBuffer changetext = new StringBuffer();
        Elements imgTags;
        MarketplaceHTMLTextParser parser = new MarketplaceHTMLTextParser();
        ArrayList<String> textDiff = new ArrayList<>();
        int wordCount[] = new int[3];
        Timestamp newTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        if ((!url.equals("invalid")) && (!url.equals("redirected"))) {
            do {
                timeoutIteration += 1;
                try {
                    jsoupConn = Jsoup.connect(url);
                    jsoupConn.timeout(timeout);
                    jsoupConn.userAgent(userAgent);
                    doc = jsoupConn.get();
                    break;
                } catch (Exception ex) {
                    jsoupconnerror = true;
                    try {
                        Thread.sleep(sllepOut * timeoutIteration);
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
            } while (timeoutIteration < noOfTime);
        }
        if (doc != null) {
            bodyText = doc.select("body").text();
            imgTags = doc.select("img");
            content = parser.parse(doc);
            oldContent = getPreviousContent(contentFolder, tempInfoId);//checks file exists or not.
            String[] newLines = content.trim().split("[\\.\\:\\!\\?]\\s+");
            String[] oldLines = oldContent.trim().split("[\\.\\:\\!\\?]\\s+");
            parser.findTextDiff(newLines, oldLines, textDiff, wordCount);
            createFile(contentFolder, "" + tempInfoId, content);
            String TM = "" + (char) 226 + (char) 132 + (char) 162;
            textDiff.stream().map((text) -> text.replaceAll(TM, "™").replaceAll("Â®", "®").replaceAll("Â©", "©").replaceAll("" + (char) 153, "™").replaceAll("" + (char) 160, " ").replaceAll("" + (char) 153, "�")).forEach((text) -> {
                changetext.append(text);
            });
            cmpsummary.setBodyText(changetext.toString());
            cmpsummary.setRemovedWords(wordCount[1]);
            cmpsummary.setNewWords(wordCount[0]);
            cmpsummary.setTotalWords(wordCount[2]);
            cmpsummary.setRemark(" ");
            try {
                URL newUrl = new URL(url);
                String domain = newUrl.getProtocol() + "://" + newUrl.getHost();
                getAnchorLinks(domain, doc, tempInfoId, contentType, userId, newTime, cmpsummary);
                getImageTagInfo(domain, imgTags, doc, tempInfoId, contentType, userId, newTime, val);
            } catch (MalformedURLException e) {
                logger.error(e);
            } catch (Exception e1) {
                logger.error(e1);
            }
        } else if (doc == null) {
//            logger.info("Document is null");
            CompetitorWebpageMonitor cmpWebMonitorInfo = new CompetitorWebpageMonitor();
            if (userId != 0) {
                cmpWebMonitorInfo = checkUserRecords(userId, cmpWebMonitorInfo);
                String tempURL = cmpWebMonitorInfo.getCompetitorUrl();
                int responseCode = 0;
                HttpURLConnection currentUrlConn = null;
                if (tempURL != null) {
                    try {
                        URL currentUrlObj = new URL(tempURL);
                        currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                        currentUrlConn.setInstanceFollowRedirects(false);
                        currentUrlConn.setConnectTimeout(50000);
                        currentUrlConn.setReadTimeout(10000);
                        currentUrlConn.setRequestProperty("User-Agent", userAgent);
                        responseCode = currentUrlConn.getResponseCode();
                        String remark = "Document is null :" + responseCode;
                        CmpSummary cmpSummary = getSummaryData(tempInfoId);
                        if (responseCode == 200 || responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {
                            List<CmpData> cmpDataList = getCmpData(tempInfoId);
                            insertCmpDataVersion2(cmpDataList, newTime);
                            if (cmpSummary != null) {
                                if (cmpSummary.getTotalUrls() == cmpSummary.getNewUrls() && cmpSummary.getRemovedUrls() == 0
                                        && cmpSummary.getTotalWords() == cmpSummary.getNewWords() && cmpSummary.getRemovedWords() == 0) {
                                    cmpSummary.setNewUrls(0);
                                    cmpSummary.setNewWords(0);
                                    cmpSummary.setInfo_id(tempInfoId);
                                    cmpSummary.setBodyText(" ");
                                }
                                cmpSummary.setStartDate(newTime);
                                cmpSummary.setRemark(remark);
                                insertSummaryInfo(cmpSummary);
                            } else {
                                remark = "First day document is null";
                                CmpSummary cmp = new CmpSummary();
                                cmp = setSummaryInfo(cmp, tempInfoId, newTime, remark);
                                insertSummaryInfo(cmp);
                            }
                        } else if (responseCode == 404) {
                            String usermailFromDb = cmpWebMonitorInfo.getEmail();
                            if (usermailFromDb.contains("GuestUser")) {
                                String guestEmail = "GuestUser" + userId + "<";
                                int strtindex = guestEmail.length();
                                usermailFromDb = usermailFromDb.substring(strtindex, usermailFromDb.lastIndexOf(">"));
                            }
                            CmpSummary cmpSummary1 = new CmpSummary();
                            cmpSummary1 = setSummaryInfo(cmpSummary1, tempInfoId, newTime, remark);
                            insertSummaryInfo(cmpSummary1);
                            String subjectLine = EmailBodyCreator.subjectOnCompetitorWebpageManitor;
                            String bodyMessage = EmailBodyCreator.bodyForCompetitorWebpageManitorURLStatus(tempURL);
                            SendMail.sendMail(supportMail, usermailFromDb, subjectLine, bodyMessage);
                        }
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        }
    }

    public CmpSummary setSummaryInfo(CmpSummary cmp, long tempInfoId, Timestamp newTime, String remark) {
        cmp.setInfo_id(tempInfoId);
        cmp.setBodyText(" ");
        cmp.setTotalUrls(0);
        cmp.setRemovedUrls(0);
        cmp.setNewUrls(0);
        cmp.setTotalWords(0);
        cmp.setNewWords(0);
        cmp.setRemovedWords(0);
        cmp.setStartDate(newTime);
        cmp.setRemark(remark);
        return cmp;
    }

    public boolean createFile(String folder, String infoId, String content) {
        boolean fileCreated = false;
        File fileName = new File(folder + "/" + infoId + ".txt");
        try {
            fileName.createNewFile();
            Common.writeTexttoFile(fileName, content);
            fileCreated = true;
        } catch (IOException e) {
            logger.error(e);
        }
        return fileCreated;
    }

    public String getPreviousContent(String folder, long id) {
        String oldContent = "";
        String path = folder + "/" + id + ".txt";
        try {
            File f = new File(path);
            if (f.exists()) {
                oldContent = readFromFile(contentFolder + "/" + id + ".txt");
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return oldContent;
    }

    public String readFromFile(String filePath) {
        FileInputStream inputStream1 = null;
        String oldBodyText = "";
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF8"));
            oldBodyText = IOUtils.toString(in);
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }
        return oldBodyText;
    }

    public long getTotalWordCount(String url, String content) {
        int index = 0;
        long numWords = 0;
        char c1 = 0;
        try {
            if (content != null) {
                boolean prevwhitespace = true;
                while (index < content.length()) {
                    char c = content.charAt(index++);
                    if ((index + 0) < content.length()) {
                        c1 = content.charAt(index + 0);
                    }
                    boolean currwhitespace = Character.isWhitespace(c);
                    if (c == ',' && !(c1 == ' ')) {
                        numWords++;
                    }
                    if (prevwhitespace && !currwhitespace) {
                        numWords++;
                    }
                    prevwhitespace = currwhitespace;
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return numWords;
    }

    public List<CmpLinksInfo> getAnchorLinks(String domain, Document doc, long tempInfoId, int contentType, long userId, Timestamp newTime, CmpSummary cmpSummary) {
        List<CmpLinksInfo> totLinksList = new ArrayList<>();
        CmpLinksInfo cmpDet;
        contentType = 1;
        List<CmpLinksInfo> tempLinksList = new ArrayList<>();
        Elements anchors = doc.select("a");
        List<CmpLinksInfo> prvList = getLinksInfoList(userId, tempInfoId);
        tempLinksList.addAll(prvList);
        List<Long> curListofIds = new ArrayList<>();
        String finalCurURl = removeProtocol(domain);
        if (anchors.size() > 0) {
            for (Element anchor : anchors) {
                if (anchor != null && !anchor.toString().equals("")) {
                    cmpDet = new CmpLinksInfo();
                    String tempAnchor = anchor.attr("abs:href").trim();
                    String anchorText = anchor.text().trim();
                    if (tempAnchor != null && !tempAnchor.trim().equals("")) {
                        String finalTempAnchor = removeProtocol(tempAnchor);
                        if (finalTempAnchor.startsWith(finalCurURl)) {
                            cmpDet.setAncLink(tempAnchor);
                            cmpDet.setAnchorText(anchorText);
                            boolean found = false;
                            for (CmpLinksInfo cur : totLinksList) {
                                int change;
                                change = cmpDet.compareTo(cur);
                                if (change == 0) {
                                    long linkId = cur.getLinkId();
                                    cmpDet.setLinkId(linkId);
                                    curListofIds.add(linkId);
                                    totLinksList.remove(cur);
                                    found = true;
                                    break;

                                }
                            }
                            if (!found) {
                                for (CmpLinksInfo prv : prvList) {
                                    int change;
                                    change = cmpDet.compareTo(prv);
                                    if (change == 0) {
                                        long linkId = prv.getLinkId();
                                        cmpDet.setLinkId(linkId);
                                        curListofIds.add(linkId);
                                        prvList.remove(prv);
                                        found = true;
                                        break;

                                    }
                                }
                            }
                            if (!found) {
                                long linkId = insertLinksInfo(cmpDet);
                                cmpDet.setLinkId(linkId);
                                curListofIds.add(linkId);
                            }
                            totLinksList.add(cmpDet);
                        }

                    }
                }
            }
            insertCmpData(tempInfoId, contentType, curListofIds, newTime);
        }
        compareLinkResults(tempLinksList, curListofIds, curListofIds.size(), newTime, tempInfoId, cmpSummary);
        return totLinksList;
    }

    public List<CmpImgInfo> getImageTagInfo(String domain, Elements imgTags, Document doc, long tempInfoId, int contentType, long userId, Timestamp newTime, int val[]) {
        List<CmpImgInfo> imgInfoList = new ArrayList<>();
        String src = "";
        contentType = 2;
        List<CmpImgInfo> tempImgList = new ArrayList<>();
        List<Long> curListofIds = new ArrayList<>();
        String finalCurURl = removeProtocol(domain);
        List<CmpImgInfo> imgList = getTotalImgsList(userId, tempInfoId);
        tempImgList.addAll(imgList);
        List<CmpImgInfo> idsList = new ArrayList<>();
        if (imgTags.size() > 0 || imgTags != null) {
            for (Element imgtag : imgTags) {
                InputStream in = null;
                createAttachmentFolder(imageFolder, tempInfoId);
                createAttachmentFolder(tempFolder, tempInfoId);
                src = imgtag.absUrl("src").trim();
                String altText = imgtag.attr("alt").toString();
                CmpImgInfo cmp = new CmpImgInfo();
                if (src != null && !src.trim().equals("")) {
                    String finalSrc = removeProtocol(src);
                    if (finalSrc.startsWith(finalCurURl)) {
                        String tempSrc = imgtag.attr("src").trim();
                        if (tempSrc.startsWith("..")) {
                            tempSrc = tempSrc.replace("..", "");
                            String newUrl = domain + tempSrc;
                            src = newUrl.trim();
                        }
                        cmp.setImgSrc(src);
                        cmp.setAltText(altText);
                        int indexname = src.lastIndexOf("/");
                        if (indexname == src.length()) {
                            src = src.substring(1, indexname);
                        }
                        indexname = src.lastIndexOf("/");
                        String name = src.substring(indexname, src.length());

                        URL url;
                        try {
                            String finalTempSrc = src.trim().replace(" ", "%20");
                            url = new URL(finalTempSrc);
                            in = url.openStream();
                            if (name.startsWith("/")) {
                                name = name.replace("/", "");
                            }
                            if (name.contains("?")) {
                                name = name.substring(0, name.indexOf("?"));
                            }
                            cmp.setImgName(name);
                            String extensionName = "";
                            if (name.lastIndexOf(".") > 0) {
                                extensionName = name.substring(name.lastIndexOf(".") + 1);
                            } else if (name.lastIndexOf(".") <= 0) {
                                extensionName = "gif";
                            }
                            resize(in, Thread.currentThread().getId(), extensionName, tempInfoId, tempFolder, cmp);
                            boolean found = false;
                            for (CmpImgInfo cur : imgInfoList) {
                                int change;
                                change = cmp.compareTo(cur);
                                if (change == 0) {
                                    long Id = cur.getImgId();
                                    cmp.setImgId(Id);
                                    curListofIds.add(Id);
                                    imgInfoList.remove(cur);
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                for (CmpImgInfo prv : imgList) {
                                    int change;
                                    change = cmp.compareTo(prv);
                                    if (change == 0) {
                                        long imgId = prv.getImgId();
                                        cmp.setImgId(imgId);
                                        curListofIds.add(imgId);
                                        imgList.remove(prv);
                                        found = true;
                                        break;
                                    }
                                }
                            }

                            if (!found) {
                                long imgId = insertImageInfo(cmp);
                                cmp.setImgId(imgId);
                                curListofIds.add(imgId);
                                copyImagesToCmpImages(tempInfoId, imgId, extensionName, cmp);
                                idsList.add(cmp);
                            }
                            imgInfoList.add(cmp);
                        } catch (Exception e1) {
                            logger.error(e1);
                        } finally {
                            try {
                                if (in != null) {
                                    in.close();
                                }
                            } catch (IOException e) {
                                logger.error(e);
                            }
                        }
                    }
                }
            }
            if (idsList != null) {
                updateImageInfo(idsList);
            }
            insertCmpData(tempInfoId, contentType, curListofIds, newTime);
            File folderLoc = new File(tempFolder + "/" + tempInfoId);
            if (deleteDir(folderLoc)) {
                compareImgResults(tempImgList, curListofIds, val);
            }
        }
        return imgInfoList;
    }

    public void copyImagesToCmpImages(long tempInfoId, long imgId, String extensionName, CmpImgInfo cmp) {
        try {
            String finalImgPath = imageFolder + tempInfoId + "/" + imgId + "." + extensionName;
            String temPath = tempFolder + tempInfoId + "/" + Thread.currentThread().getId() + "." + extensionName;
            BufferedImage inputImage = ImageIO.read(new File(temPath));
            String formatName = "";
            if (finalImgPath.lastIndexOf(".") > 0) {
                formatName = finalImgPath.substring(finalImgPath.lastIndexOf(".") + 1);
            } else if (finalImgPath.lastIndexOf(".") <= 0) {
                formatName = "gif";
            }
            if (finalImgPath != null) {
                ImageIO.write(inputImage, formatName, new File(finalImgPath));
                cmp.setImgPath(finalImgPath);
            } else {
                cmp.setImgPath("");
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String children1 : children) {
                boolean success = deleteDir(new File(dir, children1));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public static void resize(InputStream in, long imgId, String extensionName, long tempInfoId, String tempFolder, CmpImgInfo cmp) {
        double scaledWidth = 300;
        double scaledHeight = 100;
        String outputImagePath = tempFolder + tempInfoId + "/" + imgId + "." + extensionName;
        try {
            // reads input image
            BufferedImage inputImage = ImageIO.read(in);
            BufferedImage outputImage;
            if (inputImage != null) {
                double height = (double) inputImage.getHeight();
                cmp.setImgHeight(height);
                double width = (double) inputImage.getWidth();
                double finalHeight = 0, finalWidth = 0;
                cmp.setImgWidth(width);
                // extracts extension of output file
                String formatName = outputImagePath.substring(outputImagePath.lastIndexOf(".") + 1);
                if (width > 0 && height > 0) {
                    if (height > scaledHeight && width > scaledWidth) {
                        finalHeight = scaledHeight;
                        finalWidth = (width / height) * scaledHeight;
                        if (finalWidth > scaledWidth) {
                            finalHeight = (height / width) * scaledWidth;
                            finalWidth = scaledWidth;
                        }
                    } else if (height <= scaledHeight && width > scaledWidth) {
                        finalHeight = (height / width) * scaledWidth;
                        finalWidth = scaledWidth;
                    } else if (height > scaledHeight && width <= scaledWidth) {
                        finalHeight = scaledHeight;
                        finalWidth = (width / height) * scaledHeight;
                    } else if (height <= scaledHeight && width <= scaledWidth) {
                        finalHeight = (int) height;
                        finalWidth = (int) width;
                    }
                    // creates output image
                    if (inputImage.getType() == BufferedImage.TYPE_CUSTOM) {
                        outputImage = new BufferedImage((int) finalWidth, (int) finalHeight, 6);
                    } else {
                        outputImage = new BufferedImage((int) finalWidth, (int) finalHeight, inputImage.getType());
                    }
                    // scales the input image to the output image
                    Graphics2D g2d = outputImage.createGraphics();
                    g2d.drawImage(inputImage, 0, 0, (int) finalWidth, (int) finalHeight, null);
                    g2d.dispose();
                    // writes to output file
                    if (outputImagePath != null && !outputImagePath.equals("")) {
                        ImageIO.write(outputImage, formatName, new File(outputImagePath));
                        cmp.setImgPath(outputImagePath);
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public boolean createAttachmentFolder(String folder, long id) {
        boolean created = false;
        String path = folder + id;
        File f = new File(path);
        if (!f.isDirectory()) {
            created = f.mkdir();
        }
        return created;
    }

    public List<CmpImgInfo> getTotalImgsList(long userId, long infoId) {
        String query = "";
        long id;
        if (infoId != 0) {
            id = infoId;
            query = "SELECT C.img_id,C.image_src,C.alt_text,C.img_path,C.img_width,C.img_height,C.img_name FROM cmp_images C "
                    + "JOIN (select content_id from cmp_data where content_type=2 and cmp_data.info_id= ? "
                    + "and date= (select max(date) from cmp_data where content_type = 2 and  cmp_data.info_id= ? )) U on C.img_id = U.content_id ";  //"and C.is_deleted = 0"
        } else {
            id = userId;
            /*query = "SELECT C.img_id,C.image_src,C.alt_text,C.img_path,C.img_width,C.img_height,C.img_name  FROM (select * from cmp_images  where is_deleted = 0) C" +
 		 	   		"JOIN (select content_id from cmp_data where content_type=2 and cmp_data.info_id=" +
 		 	   		"(select info_id from cmp_url_Info  where user_id = ? and is_deleted = 0) and date =" +
 		 	   		" (select max(date) from cmp_data where content_type = 2 and  cmp_data.info_id= " +
 		 	   		"(select info_id from cmp_url_Info  where user_id = ? and is_deleted = 0))) U on C.img_id = U.content_id " ;   
                  
            Reason: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference to mail from Pavan Sir Subj:LXRM Tool Changes.*/
            query = "SELECT C.img_id,C.image_src,C.alt_text,C.img_path,C.img_width,C.img_height,C.img_name  FROM (select * from cmp_images  where is_deleted = 0) C"
                    + "JOIN (select content_id from cmp_data where content_type=2 and cmp_data.info_id="
                    + "(select info_id from cmp_url_info  where user_id = ? and is_deleted = 0) and date ="
                    + " (select max(date) from cmp_data where content_type = 2 and  cmp_data.info_id= "
                    + "(select info_id from cmp_url_info  where user_id = ? and is_deleted = 0))) U on C.img_id = U.content_id ";
        }
        try {
            List<CmpImgInfo> cmpDetails = getJdbcTemplate().query(query, new Object[]{id, id}, (ResultSet rs, int rowNum) -> {
                CmpImgInfo cmpDetail = new CmpImgInfo();
                cmpDetail.setImgId(rs.getLong("img_id"));
                cmpDetail.setImgSrc(rs.getString("image_src"));
                cmpDetail.setAltText(new String(rs.getBytes("alt_text")));
                cmpDetail.setImgPath(rs.getString("img_path"));
                cmpDetail.setImgName(rs.getString("img_name"));
                cmpDetail.setImgWidth(rs.getInt("img_width"));
                cmpDetail.setImgHeight(rs.getInt("img_height"));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public long insertUserInfo(final CompetitorWebpageMonitor cmpWebpageMonitor) {
        /*Reason: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference to mail from Pavan Sir Subj:LXRM Tool Changes.	
            final String query = "insert into cmp_url_Info(user_id,cmp_url,frequency,start_date,emial_unsubscription," +
				"tool_unsubscription, emial_unsub_date,tool_unsub_date,remark) values (?,?,?,?,?,?,?,?,?)";*/
        final String query = "insert into cmp_url_info(user_id,cmp_url,frequency,start_date,emial_unsubscription,"
                + "tool_unsubscription, emial_unsub_date,tool_unsub_date,remark) values (?,?,?,?,?,?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"info_id"});
                    statement.setLong(1, cmpWebpageMonitor.getUser_Id());
                    statement.setString(2, cmpWebpageMonitor.getCompetitorUrl());
                    statement.setLong(3, cmpWebpageMonitor.getAlertFrequency());
                    statement.setTimestamp(4, cmpWebpageMonitor.getStartdate());
                    statement.setBoolean(5, cmpWebpageMonitor.isEmailUnsubscription());
                    statement.setBoolean(6, cmpWebpageMonitor.isToolUnsubscription());
                    statement.setTimestamp(7, null);
                    statement.setTimestamp(8, null);
                    statement.setString(9, "");
                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }

        return (Long) keyHolder.getKey();
    }

    public void insertNewEmailId(final CompetitorWebpageMonitor cmpWebpageMonitor, final long infoId) {
        final String query = "insert into cmpweb_emails(info_id,date,email) values(?,?,?)";
        final Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"info_id"});
                    statement.setLong(1, infoId);
                    statement.setTimestamp(2, startTime);
                    statement.setString(3, cmpWebpageMonitor.getEmail());
                    return statement;
                });
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void insertCmpData(final long tempInfoId, final int content_type, final List<Long> ids, final Timestamp newTime) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(newTime);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        final String curDate = (new SimpleDateFormat("yyyy-MM-dd")).format(cal1.getTime());
        final String query = "insert into cmp_data(info_id,date,content_type,content_id) values (?,?,?,?)";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {

                ps.setLong(1, tempInfoId);
                ps.setString(2, curDate);
                ps.setInt(3, content_type);
                ps.setLong(4, (long) ids.get(i));
            }

            @Override

            public int getBatchSize() {
                return ids.size();
            }
        });
    }

    public long insertImageInfo(final CmpImgInfo cmp) {
        final String query = "insert into cmp_images(image_src,alt_text,img_name,img_width,img_height) values (?,?,?,?,?) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"img_id"});
                    statement.setString(1, cmp.getImgSrc());
                    String myStr = cmp.getAltText();
                    try {
                        byte[] buff = myStr.getBytes("UTF-8");
                        statement.setBytes(2, buff);
                    } catch (UnsupportedEncodingException e) {
                        logger.error(e);
                    }
                    statement.setString(3, cmp.getImgName());
                    statement.setDouble(4, cmp.getImgWidth());
                    statement.setDouble(5, cmp.getImgHeight());
                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return (Long) keyHolder.getKey();
    }

    public void updateImageInfo(final List<CmpImgInfo> idsList) {
        final String query = "update cmp_images set img_path= ?  where img_id= ?";

        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                CmpImgInfo cmpImg = idsList.get(i);
                ps.setString(1, cmpImg.getImgPath());
                ps.setLong(2, cmpImg.getImgId());
            }

            @Override
            public int getBatchSize() {
                return idsList.size();
            }
        });
    }

    public long insertLinksInfo(final CmpLinksInfo cmpInfo) {
        final String query = "insert into cmp_links(links,link_desc,anchor_text) values (?,?,?) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"link_id"});
                    statement.setString(1, cmpInfo.getAncLink());
                    statement.setString(2, cmpInfo.getLinkDesc());
                    String myStr = cmpInfo.getAnchorText();
                    try {
                        byte[] buff = myStr.getBytes("UTF-8");
                        statement.setBytes(3, buff);
                    } catch (UnsupportedEncodingException e) {
                        logger.error(e);
                    }
                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return (Long) keyHolder.getKey();
    }

    public List<CmpLinksInfo> getLinksInfoList(long userId, long infoId) {
        String query = "";
        if (infoId != 0) {
            query = "SELECT C.link_id,C.links,C.link_desc,C.anchor_text FROM cmp_links C INNER JOIN "
                    + "(select content_id from cmp_data where content_type=1 and cmp_data.info_id= ? and"
                    + " date = (select max(date) from cmp_data "
                    + "where content_type=1 and cmp_data.info_id= ? )) U on C.link_id = U.content_id  "; //and C.is_deleted = 0" 
        } else {
            /*query = "SELECT C.link_id,C.links,C.link_desc,C.anchor_text FROM cmp_links  C" +
    	    	   		" INNER JOIN (select content_id from cmp_data where content_type=1 and cmp_data.info_id=" +
    	    	   		"(select info_id from cmp_url_Info  where user_id = ? and is_deleted = 0) and date =" +
    	    	   		" (select max(date) from cmp_data where content_type=1 and cmp_data.info_id= " +
    	    	   		"(select info_id from cmp_url_Info  where user_id =? and is_deleted = 0))) U on C.link_id = U.content_id";
                    
                    Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference to mail 
                    from Pavan Sir Subj:LXRM Tool Changes.*/

            query = "SELECT C.link_id,C.links,C.link_desc,C.anchor_text FROM cmp_links  C"
                    + " INNER JOIN (select content_id from cmp_data where content_type=1 and cmp_data.info_id="
                    + "(select info_id from cmp_url_info  where user_id = ? and is_deleted = 0) and date ="
                    + " (select max(date) from cmp_data where content_type=1 and cmp_data.info_id= "
                    + "(select info_id from cmp_url_info  where user_id =? and is_deleted = 0))) U on C.link_id = U.content_id";
        }
        try {
            List<CmpLinksInfo> cmpDetails = getJdbcTemplate().query(query, new Object[]{infoId, infoId}, (ResultSet rs, int rowNum) -> {
                CmpLinksInfo cmpDetail = new CmpLinksInfo();
                cmpDetail.setLinkId(rs.getLong("link_id"));
                cmpDetail.setAncLink(rs.getString("links"));
                cmpDetail.setLinkDesc(rs.getString("link_desc"));
                cmpDetail.setAnchorText(new String(rs.getBytes("anchor_text")));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public void compareLinkResults(List<CmpLinksInfo> prvList, List<Long> curIds, int totalLinks, Timestamp newTime, long tempInfoId, CmpSummary cmpSummary) {
        cmpSummary.setInfo_id(tempInfoId);
        cmpSummary.setStartDate(newTime);
        cmpSummary.setTotalUrls(totalLinks);
        List<Long> prvIds = new ArrayList<>();
        prvList.stream().map((cmp) -> cmp.getLinkId()).forEach((prvId1) -> {
            prvIds.add(prvId1);
        });
        prvList.stream().map((cmp) -> cmp.getLinkId()).filter((prvId) -> (curIds.contains(prvId))).map((prvId) -> {
            prvIds.remove(prvId);
            return prvId;
        }).forEach((prvId) -> {
            curIds.remove(prvId);
        });
        long addedLinks;
        cmpSummary.setRemovedUrls(prvIds.size());
        addedLinks = curIds.size();
        cmpSummary.setNewUrls(addedLinks);
        insertSummaryInfo(cmpSummary);
    }

    public void insertSummaryInfo(final CmpSummary cmpSummary) {
        final String query = "insert into cmp_summary(info_id,date,total_urls,new_urls,removed_urls,total_words,new_words,removed_words,text_changes,remark) values (?,?,?,?,?,?,?,?,?,?)";

        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query);
                    statement.setLong(1, cmpSummary.getInfo_id());
                    statement.setTimestamp(2, cmpSummary.getStartDate());
                    statement.setLong(3, cmpSummary.getTotalUrls());
                    statement.setLong(4, cmpSummary.getNewUrls());
                    statement.setLong(5, cmpSummary.getRemovedUrls());
                    statement.setLong(6, cmpSummary.getTotalWords());
                    statement.setLong(7, cmpSummary.getNewWords());
                    statement.setLong(8, cmpSummary.getRemovedWords());
                    String myStr = cmpSummary.getBodyText();
                    try {
                        byte[] buff = myStr.getBytes("UTF-8");
                        statement.setBytes(9, buff);
                    } catch (UnsupportedEncodingException e) {
                        logger.error(e);
                    }
                    statement.setString(10, cmpSummary.getRemark());

                    return statement;
                });
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public List<CmpLinksInfo> getCurLinkResults(String currDate, long userId, CompetitorWebpageMonitor cmpWebpageMonitor, long infoId) {

//    		String currFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(currDate);
        /*String  query = "SELECT C.link_id,C.links,C.link_desc,C.anchor_text FROM cmp_links C " +
            		"INNER JOIN (select content_id  from cmp_data where content_type=1  and " +
            		"date_format(date,'%y-%m-%d')= date_format('"+currDate+"','%y-%m-%d') and info_id = " +
            		"(select info_id from cmp_url_Info  where user_id ="+userId+" and is_deleted = 0)) U" +
            		" on  C.link_id = U.content_id ";
            Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] 
            with reference to mail from Pavan Sir Subj:LXRM Tool Changes.*/
        String query = "SELECT C.link_id,C.links,C.link_desc,C.anchor_text FROM cmp_links C "
                + "INNER JOIN (select content_id  from cmp_data where content_type=1  and "
                + "date_format(date,'%y-%m-%d')= date_format('" + currDate + "','%y-%m-%d') and info_id = "
                + "(select info_id from cmp_url_info  where user_id =" + userId + " and is_deleted = 0)) U"
                + " on  C.link_id = U.content_id ";
        try {
            List<CmpLinksInfo> cmpDetails = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                CmpLinksInfo cmpDetail = new CmpLinksInfo();
                cmpDetail.setLinkId(rs.getLong("link_id"));
                cmpDetail.setAncLink(rs.getString("links"));
                cmpDetail.setLinkDesc(rs.getString("link_desc"));
                cmpDetail.setAnchorText(new String(rs.getBytes("anchor_text")));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public List<CmpLinksInfo> getPrvLinkResults(String newDate, long userId, CompetitorWebpageMonitor cmpWebpageMonitor, long infoId) {

        String query = "SELECT  C.link_id,C.links,C.link_desc,C.anchor_text FROM  cmp_links C "
                + "INNER JOIN (select content_id  from cmp_data where  content_type=1  and "
                + "date_format(date,'%y-%m-%d')= date_format('" + newDate + "','%y-%m-%d') and info_id = " + infoId + ") U "
                + "on C.link_id = U.content_id ";
        try {
            List<CmpLinksInfo> cmpDetails = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                CmpLinksInfo cmpDetail = new CmpLinksInfo();
                cmpDetail.setLinkId(rs.getLong("link_id"));
                cmpDetail.setAncLink(rs.getString("links"));
                cmpDetail.setLinkDesc(rs.getString("link_desc"));
                cmpDetail.setAnchorText(new String(rs.getBytes("anchor_text")));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public Map<Integer, List<CmpLinksInfo>> getLinkDetailedResults(CompetitorWebpageMonitor cmpWebpageMonitor, long userId, long infoId, Date fetchDate) {
        long MILLISECONDS_IN_DAY = 1000l * 60l * 60l * 24l;
        int frequency = 0;
        Calendar today = Calendar.getInstance();
        Date currDate = Calendar.getInstance().getTime();
        String curFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(today.getTime());
        Calendar startDay = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        startDay.setTimeInMillis(cmpWebpageMonitor.getStartdate().getTime());
        Date newDate = null;
        if (cmpWebpageMonitor.getAlertFrequency() == 1) {
            frequency = 1;
            newDate = (new Date(currDate.getTime() - (frequency * MILLISECONDS_IN_DAY)));
        } else if (cmpWebpageMonitor.getAlertFrequency() == 2) {
            frequency = 7;
            long daysDiff = getDateDiff(startDay, today) % 7;
            Calendar lastdataDay = Calendar.getInstance();
            lastdataDay.add(Calendar.DATE, (int) -daysDiff);
            curFormatDt = df.format(lastdataDay.getTime());
            newDate = (new Date(lastdataDay.getTimeInMillis() - (frequency * MILLISECONDS_IN_DAY)));
        }
        List<CmpLinksInfo> tempPrvList = null;
        if (newDate != null) {
            String newFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(newDate);
            tempPrvList = getPrvLinkResults(newFormatDt, userId, cmpWebpageMonitor, infoId);

        }
        Map<Integer, List<CmpLinksInfo>> linksList = new HashMap<>();
        List<CmpLinksInfo> totalList = new ArrayList<>();
        List<CmpLinksInfo> newLinks = new ArrayList<>();
        List<CmpLinksInfo> existLinks = new ArrayList<>();
        List<CmpLinksInfo> tempCurList = getCurLinkResults(curFormatDt, userId, cmpWebpageMonitor, infoId);
        if (tempCurList.size() == 0 || tempCurList.isEmpty()) {
            String tempCurFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(fetchDate);
            tempCurList = getCurLinkResults(tempCurFormatDt, userId, cmpWebpageMonitor, infoId);

            //Step 1:
            List<Long> currIds = new ArrayList<Long>();
            for (CmpLinksInfo curr : tempCurList) {
                currIds.add(curr.getLinkId());
            }

            //Step 2:
            if (currIds != null) {
                for (long id1 : currIds) {
                    for (CmpLinksInfo cmp : tempCurList) {
                        if (id1 == cmp.getLinkId()) {
                            newLinks.add(cmp);
                            break;
                        }
                    }
                }

                logger.info("currIds for links  : " + currIds.size() + " on tempCurFormatDt" + tempCurFormatDt);
            }
            linksList.put(1, totalList);
            linksList.put(2, newLinks);
            linksList.put(3, existLinks);
            logger.info("links  : " + totalList.size() + " " + newLinks.size() + " " + existLinks.size() + "tempCurFormatDt" + tempCurFormatDt);
            return linksList;
        }//End of IF
        List<Long> prvIdsList2 = new ArrayList<>();
        List<Long> currIds = new ArrayList<>();
        for (CmpLinksInfo curr : tempCurList) {
            currIds.add(curr.getLinkId());
        }
        for (CmpLinksInfo prvList : tempPrvList) {
            prvIdsList2.add(prvList.getLinkId());
        }
        for (CmpLinksInfo cmp : tempPrvList) {
            Long prvId = cmp.getLinkId();
            if (currIds.contains(prvId)) {
                prvIdsList2.remove(prvId);
                currIds.remove(prvId);
            }
        }

        if (prvIdsList2 != null) {
            for (long id1 : prvIdsList2) {
                for (CmpLinksInfo cmp : tempPrvList) {
                    if (id1 == cmp.getLinkId()) {
                        existLinks.add(cmp);
                        break;
                    }
                }
            }
        }

        if (currIds != null) {
            for (long id1 : currIds) {
                for (CmpLinksInfo cmp : tempCurList) {
                    if (id1 == cmp.getLinkId()) {
                        newLinks.add(cmp);
                        break;
                    }
                }
            }
        }
        linksList.put(1, totalList);
        linksList.put(2, newLinks);
        linksList.put(3, existLinks);
        logger.info("links  : " + totalList.size() + " " + newLinks.size() + " " + existLinks.size());
        return linksList;
    }

    public List<CmpImgInfo> getCurImgResults(String currDate, long userId, CompetitorWebpageMonitor cmpWebpageMonitor, long infoId) {

        String query = "SELECT  C.img_id,C.image_src, C.alt_text, C.img_path, C.img_name, C.img_width,C.img_height FROM cmp_images C INNER JOIN "
                + "(select content_id from cmp_data where content_type=2 "
                + "and date_format(date,'%y-%m-%d')= date_format('" + currDate + "' ,'%y-%m-%d') and info_id = " + infoId + ") U "
                + "on C.img_id =U.content_id";
        try {
            List<CmpImgInfo> cmpDetails = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                CmpImgInfo cmpDetail = new CmpImgInfo();
                cmpDetail.setImgId(rs.getLong("img_id"));
                cmpDetail.setImgName(rs.getString("img_name"));
                cmpDetail.setImgSrc(rs.getString("image_src"));
                cmpDetail.setAltText(new String(rs.getBytes("alt_text")));
                cmpDetail.setImgWidth(rs.getInt("img_width"));
                cmpDetail.setImgHeight(rs.getInt("img_height"));
                cmpDetail.setImgPath(rs.getString("img_path"));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public List<CmpImgInfo> getPrvImgResults(String newDate, long userId, CompetitorWebpageMonitor cmpWebpageMonitor, long infoId) {

        String query = "SELECT  C.img_id,C.image_src, C.alt_text, C.img_path, C.img_name, C.img_width,C.img_height FROM "
                + "cmp_images C INNER JOIN (select content_id from cmp_data where content_type=2 "
                + "and date_format(date,'%y-%m-%d')= date_format('" + newDate + "' ,'%y-%m-%d') and info_id = " + infoId + ") U "
                + "on C.img_id =U.content_id ";
        try {
            List<CmpImgInfo> cmpDetails = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                CmpImgInfo cmpDetail = new CmpImgInfo();
                cmpDetail.setImgId(rs.getLong("img_id"));
                cmpDetail.setImgName(rs.getString("img_name"));
                cmpDetail.setImgSrc(rs.getString("image_src"));
                cmpDetail.setAltText(new String(rs.getBytes("alt_text")));
                cmpDetail.setImgWidth(rs.getInt("img_width"));
                cmpDetail.setImgHeight(rs.getInt("img_height"));
                cmpDetail.setImgPath(rs.getString("img_path"));
                return cmpDetail;
            });
            return cmpDetails;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public Map<Integer, List<CmpImgInfo>> getImgDetailedResults(CompetitorWebpageMonitor cmpWebpageMonitor, long userId, long infoId) {

        long MILLISECONDS_IN_DAY = 1000l * 60l * 60l * 24l;
        Calendar today = Calendar.getInstance();
        Date currDate = Calendar.getInstance().getTime();
        String curFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(today.getTime());
        Calendar startDay = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        startDay.setTimeInMillis(cmpWebpageMonitor.getStartdate().getTime());
        int frequency = 0;
        if (cmpWebpageMonitor.getAlertFrequency() == 1) {
            frequency = 1;
        } else if (cmpWebpageMonitor.getAlertFrequency() == 2) {
            frequency = 7;
            long daysDiff = getDateDiff(startDay, today) % 7;
            Calendar lastdataDay = Calendar.getInstance();
            lastdataDay.add(Calendar.DATE, (int) -daysDiff);
            curFormatDt = df.format(lastdataDay.getTime());
        }
        Date newDate = (new Date(currDate.getTime() - (frequency * MILLISECONDS_IN_DAY)));
        String newFormatDt = (new SimpleDateFormat("yyyy-MM-dd")).format(newDate);
        Map<Integer, List<CmpImgInfo>> imgList = new HashMap<>();
        List<CmpImgInfo> newList = new ArrayList<>();
        List<CmpImgInfo> existList = new ArrayList<>();
        List<CmpImgInfo> curList = getCurImgResults(curFormatDt, userId, cmpWebpageMonitor, infoId);
        List<CmpImgInfo> prvList = getPrvImgResults(newFormatDt, userId, cmpWebpageMonitor, infoId);
        List<Long> prvIds = new ArrayList<>();
        List<Long> prvIdsList2 = new ArrayList<>();
        List<Long> currIds = new ArrayList<>();
        curList.stream().forEach((curr) -> {
            currIds.add(curr.getImgId());
        });
        prvList.stream().forEach((prv) -> {
            prvIdsList2.add(prv.getImgId());
        });
        for (CmpImgInfo cmp : prvList) {
            Long prvId = cmp.getImgId();
            prvIds.add(prvId);
            if (currIds.contains(prvId)) {
                prvIdsList2.remove(prvId);
                currIds.remove(prvId);
            }
        }
        logger.info("in images prvIdsList2 images are " + prvIdsList2.size());
        logger.info("in images currIds images are " + currIds.size());
        if (prvIdsList2 != null) {
            prvList.stream().filter((cmp) -> (prvIdsList2.contains(cmp.getImgId()))).map((cmp) -> {
                CmpImgInfo cmpImg = new CmpImgInfo();
                cmpImg.setAltText(cmp.getAltText());
                cmpImg.setImgSrc(cmp.getImgSrc());
                cmpImg.setImgPath(cmp.getImgPath());
                cmpImg.setImgHeight(cmp.getImgHeight());
                cmpImg.setImgWidth(cmp.getImgWidth());
                cmpImg.setImgName(cmp.getImgName());
                return cmpImg;
            }).forEach((cmpImg) -> {
                existList.add(cmpImg);
            });
        }

        if (currIds != null) {
            curList.stream().filter((cmp) -> (currIds.contains(cmp.getImgId()))).map((cmp) -> {
                CmpImgInfo remImg = new CmpImgInfo();
                remImg.setAltText(cmp.getAltText());
                remImg.setImgSrc(cmp.getImgSrc());
                remImg.setImgPath(cmp.getImgPath());
                remImg.setImgHeight(cmp.getImgHeight());
                remImg.setImgWidth(cmp.getImgWidth());
                remImg.setImgName(cmp.getImgName());
                return remImg;
            }).forEach((remImg) -> {
                newList.add(remImg);
            });
        }

        imgList.put(1, newList);
        imgList.put(2, existList);
        return imgList;
    }

    public CompetitorWebpageMonitor findUserData(long user_id) {
        //query = "select user_id,cmp_url,frequency from cmp_url_Info where user_id="+user_id+" and is_deleted= false ";    
        /*
        String query = "select user_id,cmp_url,frequency,start_date,emial_unsubscription,tool_unsubscription from cmp_url_Info where user_id="+user_id+" and is_deleted=0";         
        Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] 
            with reference to mail from Pavan Sir Subj:LXRM Tool Changes.*/
        String query = "select user_id,cmp_url,frequency,start_date,emial_unsubscription,tool_unsubscription from cmp_url_info where user_id=" + user_id + " and is_deleted=0";
        try {
            CompetitorWebpageMonitor cmpWmTool = (CompetitorWebpageMonitor) getJdbcTemplate().queryForObject(query,
                    new RowMapper<Object>() {
                CompetitorWebpageMonitor cmpMonitor;

                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    cmpMonitor = new CompetitorWebpageMonitor();
                    cmpMonitor.setUser_Id(rs.getLong(1));
                    cmpMonitor.setCompetitorUrl(rs.getString(2));
                    cmpMonitor.setAlertFrequency(rs.getInt(3));
                    cmpMonitor.setStartdate(rs.getTimestamp(4));
                    cmpMonitor.setEmailUnsubscription(rs.getBoolean(5));
                    cmpMonitor.setToolUnsubscription(rs.getBoolean(6));
                    return cmpMonitor;
                }
            });

            return cmpWmTool;
        } catch (Exception e) {
            logger.error("Exception in findUserData" + e.getMessage() + e.getCause());
        }

        return null;
    }

    public SqlRowSet getlinkDataForChart(CmpSummary cmpsummary, long infoId, String queryStartDate) {
        SqlRowSet linksqlRowSet = null;
        try {
            final Timestamp endTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            String query = " select date_format(date,'%Y-%m-%d') as date, new_urls,removed_urls from cmp_summary"
                    + " where info_id=" + infoId + " and date_format(date,'%Y-%m-%d') between '"
                    + queryStartDate + "' and  date_format('" + endTime + "','%Y-%m-%d') order by date";
            logger.info("query for Link : " + query);

            linksqlRowSet = getJdbcTemplate().queryForRowSet(query);
        } catch (Exception e) {
            logger.error("Exception in getlinkDataForChart" + e.getMessage());
        }

        return linksqlRowSet;
    }

    public SqlRowSet getContentDataForChart(CmpSummary cmpsummary, long infoId, String queryStartDate) throws Exception {
        SqlRowSet contsqlRowSet = null;
        try {
            final Timestamp endTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            String query = " select date_format(date,'%Y-%m-%d') as date, new_words,removed_words from cmp_summary"
                    + " where info_id=" + infoId + " and date_format(date,'%Y-%m-%d') between '"
                    + queryStartDate + "' and  date_format('" + endTime + "','%Y-%m-%d') order by date";
            logger.info("query for charts : " + query);
            contsqlRowSet = getJdbcTemplate().queryForRowSet(query);
        } catch (Exception e) {
            logger.error("Exception in getContentDataForChart" + e.getMessage());
        }
        return contsqlRowSet;
    }

    public String getDatesForChart(CompetitorWebpageMonitor compWebpageMonitorTool, String[] chartDates, HttpSession session, Date summarydate) {
        logger.info("In getDatesForChart summarydate is " + summarydate);
        Calendar today = Calendar.getInstance();
        Calendar startDay = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        logger.info("In getDatesForChart  startdate is:" + compWebpageMonitorTool.getStartdate());
        startDay.setTimeInMillis(compWebpageMonitorTool.getStartdate().getTime());
        long dateDiff = getDateDiff(startDay, today);
        String queryStartDate = "";
        if (dateDiff < 0) {
            logger.error("start date is higher than today");
            return df.format(startDay.getTime());
        }
        if (compWebpageMonitorTool.getAlertFrequency() == 1) {
            Calendar tempDay = Calendar.getInstance();
            tempDay.setTime(summarydate);
            for (int i = 6; i >= 0; i--) {
                chartDates[i] = df.format(tempDay.getTime());
                tempDay.add(Calendar.DATE, -1);
            }
            tempDay.add(Calendar.DATE, 1);
            queryStartDate = df.format(tempDay.getTime());
            session.removeAttribute("lastdataDay");
        } else if (compWebpageMonitorTool.getAlertFrequency() == 2) {
            if (dateDiff < 7) {
                session.removeAttribute("lastdataDay");
                session.setAttribute("lastdataDay", startDay.getTime());
                queryStartDate = df.format(summarydate.getTime());
                Calendar tempDay = Calendar.getInstance();
                tempDay.setTime(summarydate);
                for (int i = 6; i >= 0; i--) {
                    chartDates[i] = df.format(tempDay.getTime());
                    tempDay.add(Calendar.DATE, -7);
                }
            } else {
                Calendar curDay = Calendar.getInstance();
                curDay.setTimeInMillis(summarydate.getTime());
                Calendar tempDay = Calendar.getInstance();
                tempDay.setTimeInMillis(compWebpageMonitorTool.getStartdate().getTime());
                long daysDiff = getDateDiff(tempDay, curDay) % 7;
                Calendar lastdataDay = Calendar.getInstance();
                lastdataDay.setTime(summarydate);
                lastdataDay.add(Calendar.DATE, (int) -daysDiff);
                session.removeAttribute("lastdataDay");
                session.setAttribute("lastdataDay", lastdataDay.getTime());
                for (int i = 6; i >= 0; i--) {
                    chartDates[i] = df.format(lastdataDay.getTime());
                    lastdataDay.add(Calendar.DATE, -7);
                }
                lastdataDay.add(Calendar.DATE, 7);
                queryStartDate = df.format(lastdataDay.getTime());
            }
        }
        return queryStartDate;
    }

    public static long getDateDiff(Calendar firstDate, Calendar secondDate) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(firstDate.getTime());
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(secondDate.getTime());
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        // calendars are the same date
        if (cal1.equals(cal2)) {
            return 0;
        }
        // cal2 is before cal1 - make recursive call
        if (cal2.before(cal1)) {
            return -getDateDiff(secondDate, firstDate);
        }
        // cal1 is before cal2 - count days between
        long days = 0;
        while (cal1.before(cal2)) {
            cal1.add(Calendar.DATE, 1);
            days++;
        }
        logger.info("days In getDateDiff: " + days);
        return days;
    }

    public StringBuilder buildLinkChartXml(SqlRowSet linksqlRowSet, String[] chartDates) {
        StringBuilder ChartXml = new StringBuilder();
        StringBuilder catxml = new StringBuilder();
        StringBuilder newlyaddedxml = new StringBuilder();
        StringBuilder removedxml = new StringBuilder();
        SqlRowSetMetaData sqlRowSetMetDat = linksqlRowSet.getMetaData();
        long newUrls = 0, removedUrls = 0;
        boolean allDataZero = true;
        boolean notReached = linksqlRowSet.next();
        DateFormat inputdf = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputdf = new SimpleDateFormat("dd MMM");
        String formatteddate = "";
        ChartXml.append("<graph  formatNumberScale='0' showLimits='0' canvasBorderColor='F4ECDC' canvasBorderThickness='0' bgColor='F4ECDC'  canvasbgcolor='F4ECDC' showNames='1' showSum= '1'  decimalPrecision='0'  animation='1'  numDivLines='0' showValues='1' yAxisMinValue='-50' >");
        if (notReached) {
            catxml.append("<categories>");
            newlyaddedxml.append("<dataset seriesName='New Internal Links' color='C8D898'>");
            removedxml.append("<dataset seriesName='Removed Internal Links' color='F87878'>");
            String date = "";

            for (String chartDate : chartDates) {
                if (notReached && chartDate.equals(linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)))) {
                    newUrls = linksqlRowSet.getLong(sqlRowSetMetDat.getColumnName(2));
                    removedUrls = linksqlRowSet.getLong(sqlRowSetMetDat.getColumnName(3));
                    if (newUrls > 0 || removedUrls > 0) {
                        allDataZero = false;
                    }

                    date = linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(1));
                    try {
                        formatteddate = outputdf.format(inputdf.parse(date));
                        SimpleDateFormat graphDateFormat = new SimpleDateFormat("MMM dd, yyyy");
                        String googleformatteddate = graphDateFormat.format(inputdf.parse(date));

                    } catch (ParseException e) {
                        logger.error(e);
                    }
                    catxml.append("<category name='" + formatteddate + "' showLabel='1' />");
                    newlyaddedxml.append("<set value='" + linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(2)) + "'></set>");
                    removedxml.append("<set value='-" + linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(3)) + "'></set>");
                    notReached = linksqlRowSet.next();
                } else {
                    try {
                        formatteddate = outputdf.format(inputdf.parse(chartDate));

                    } catch (ParseException e) {
                        logger.error(e);
                    }
                    catxml.append("<category name='" + formatteddate + "' showLabel='1' />");
                    newlyaddedxml.append("<set value='" + 0 + "'></set>");
                    removedxml.append("<set value='" + 0 + "'></set>");
                }
            }
            if (allDataZero) {
                ChartXml.append("</graph>");
            } else {
                newlyaddedxml.append("</dataset>");
                removedxml.append("</dataset>");
                catxml.append("</categories>");
                ChartXml.append(catxml).append(removedxml).append(newlyaddedxml).append("</graph>");
            }
        } else {
            ChartXml.append("</graph>");
        }
        return ChartXml;
    }

    public JSONArray buildLinkChartJSON(SqlRowSet linksqlRowSet, String[] chartDates) {
        JSONArray finalChartJson = null;
        try {
            finalChartJson = new JSONArray();
            SqlRowSetMetaData sqlRowSetMetDat = linksqlRowSet.getMetaData();

            boolean notReached = linksqlRowSet.next();
            DateFormat inputdf = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputdf = new SimpleDateFormat("MMM dd, yyyy");
            String formatteddate = "";
            if (notReached) {
                String date = "";
                for (String chartDate : chartDates) {
                    logger.debug(chartDate + "   :chartDate");
                    JSONArray subChartJSON = new JSONArray();
                    if (notReached && chartDate.equals(linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)))) {
                        date = linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(1));
                        logger.debug("The Date from DB  :" + date);
                        try {
                            formatteddate = outputdf.format(inputdf.parse(date));
                            logger.debug("the final date for google graph date  :" + formatteddate);

                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        subChartJSON.put(formatteddate);
                        try {
                            if ((linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(2)) != null)
                                    || (linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(3)) != null)) {
                                int newlyLinktempValue = Integer.parseInt(linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(2)));
                                subChartJSON.put(newlyLinktempValue);
                                int removedLinktempValue = Integer.parseInt(linksqlRowSet.getString(sqlRowSetMetDat.getColumnName(3)));
                                subChartJSON.put(removedLinktempValue);
                            } else {
                                subChartJSON.put(0).put(0);
                            }
                            logger.debug("The subChartJSON when data is prepared with DB date" + subChartJSON);
                            finalChartJson.put(subChartJSON);
                            notReached = linksqlRowSet.next();
                        } catch (Exception e) {
                            logger.error(e);
                        }

                    } else {
                        try {
                            formatteddate = outputdf.format(inputdf.parse(chartDate));
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        subChartJSON.put(formatteddate).put(0).put(0);
                        logger.debug("The subChartJSON when data is prepared with chartDate" + subChartJSON);
                        finalChartJson.put(subChartJSON);
                    }
                }
                logger.info("The Final Chart LinkChart in Succes" + finalChartJson);
            } else {
                finalChartJson.put(new JSONArray());
            }
            logger.debug("The Final Chart befor LinkChart return is  " + finalChartJson);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return finalChartJson;
    }

    public StringBuilder buildContentChartXml(SqlRowSet contsqlRowSet, String[] chartDates) {
        StringBuilder ChartXml = new StringBuilder();
        StringBuilder catxml = new StringBuilder();
        StringBuilder newlyaddedxml = new StringBuilder();
        StringBuilder removedxml = new StringBuilder();
        SqlRowSetMetaData sqlRowSetMetDat = contsqlRowSet.getMetaData();
        DateFormat inputdf = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputdf = new SimpleDateFormat("dd MMM");
        String formatteddate = "";
        long newContent = 0, removedContent = 0;
        boolean allDataZero = true;
        boolean notReached = contsqlRowSet.next();
        ChartXml.append("<graph setAdaptiveYMin='1' showLimits='0' formatNumberScale='0' chartRightMargin='25' canvasBorderColor='F4ECDC' xAxisNamePadding='10' showNames='1'   canvasBorderThickness='0' bgColor='F4ECDC'  canvasbgcolor='F4ECDC' showSum= '1' decimalPrecision='0'  numDivLines='0'  animation='1' showValues='1' yAxisMinValue='-50' >");
        if (notReached) {
            catxml.append("<categories>");
            newlyaddedxml.append("<dataset seriesName='New Words' color='C8D898'>");
            removedxml.append("<dataset seriesName='Removed Words' color='F87878'>");
            String date = "";

            for (String chartDate : chartDates) {/*   logger.info(chartDate+"   :chartDate");*/
                if (notReached && chartDate.equals(contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)))) {
                    newContent = contsqlRowSet.getLong(sqlRowSetMetDat.getColumnName(2));
                    removedContent = contsqlRowSet.getLong(sqlRowSetMetDat.getColumnName(3));
                    if (newContent > 0 || removedContent > 0) {
                        allDataZero = false;
                    }
                    date = contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(1));
                    logger.info("date  :" + date);
                    try {
                        formatteddate = outputdf.format(inputdf.parse(date));
                    } catch (ParseException e) {
                        logger.error(e);
                    }
                    catxml.append("<category name='" + formatteddate + "' showLabel='1' />");
                    newlyaddedxml.append("<set value='" + newContent + "'></set>");
                    removedxml.append("<set value='" + (removedContent > 0 ? "-" : "") + removedContent + "'></set>");
                    notReached = contsqlRowSet.next();
                } else {
                    try {
                        formatteddate = outputdf.format(inputdf.parse(chartDate));

                    } catch (ParseException e) {
                        logger.error(e);
                    }
                    catxml.append("<category name='" + formatteddate + "' showLabel='1' />");
                    newlyaddedxml.append("<set value='" + 0 + "'></set>");
                    removedxml.append("<set value='" + 0 + "'></set>");
                }
            }
            if (allDataZero) {
                ChartXml.append("</graph>");
            } else {
                newlyaddedxml.append("</dataset>");
                removedxml.append("</dataset>");
                catxml.append("</categories>");
                ChartXml.append(catxml).append(removedxml).append(newlyaddedxml).append("</graph>");
            }
        } else {
            ChartXml.append("</graph>");
        }
        return ChartXml;
    }

    public JSONArray buildContentChartJSON(SqlRowSet contsqlRowSet, String[] chartDates) {
        JSONArray finalChartJson = null;
        try {
            finalChartJson = new JSONArray();
            SqlRowSetMetaData sqlRowSetMetDat = contsqlRowSet.getMetaData();

            boolean notReached = contsqlRowSet.next();
            DateFormat inputdf = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputdf = new SimpleDateFormat("MMM dd, yyyy");
            String formatteddate = "";
            if (notReached) {
                String date = "";
                for (String chartDate : chartDates) {
                    logger.debug(chartDate + "   :chartDate");
                    JSONArray subChartJSON = new JSONArray();
                    if (notReached && chartDate.equals(contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)))) {
                        date = contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(1));
                        logger.debug("The Date from DB  :" + date);
                        try {
                            formatteddate = outputdf.format(inputdf.parse(date));
                            logger.debug("the final date for google graph date  :" + formatteddate);

                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        subChartJSON.put(formatteddate);
                        try {
                            if ((contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(2)) != null)
                                    || (contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(3)) != null)) {
                                int newlyLinktempValue = Integer.parseInt(contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(2)));
                                subChartJSON.put(newlyLinktempValue);
                                int removedLinktempValue = Integer.parseInt(contsqlRowSet.getString(sqlRowSetMetDat.getColumnName(3)));
                                subChartJSON.put(removedLinktempValue);
                            } else {
                                subChartJSON.put(0).put(0);
                            }
                            logger.debug("The subChartJSON when data is prepared with DB date" + subChartJSON);
                            finalChartJson.put(subChartJSON);
                            notReached = contsqlRowSet.next();
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        }
                    } else {
                        try {
                            formatteddate = outputdf.format(inputdf.parse(chartDate));
                            subChartJSON.put(formatteddate).put(0).put(0);
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        logger.debug("The subChartJSON when data is prepared with chartDate" + subChartJSON);
                        finalChartJson.put(subChartJSON);
                    }
                }
                logger.info("The Final Chart for Content in Succes" + finalChartJson);
            } else {
                finalChartJson.put(new JSONArray());
            }
            logger.debug("The Final Chart Content  before return is  " + finalChartJson);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return finalChartJson;
    }

    public CmpSummary getContentChanges(long infoId, CompetitorWebpageMonitor cmpWebpageMonitor, Date fetchDate) {
        Calendar today = Calendar.getInstance();
        Calendar startDay = Calendar.getInstance();
        logger.debug("startdate :" + cmpWebpageMonitor.getStartdate() + "frequency : " + cmpWebpageMonitor.getAlertFrequency());
        startDay.setTimeInMillis(cmpWebpageMonitor.getStartdate().getTime());
        long daysDiff = getDateDiff(startDay, today) % 7;
        Calendar lastdataDay = Calendar.getInstance();
        lastdataDay.add(Calendar.DATE, (int) -daysDiff);
        String query = "";
        String finalFetchDate = (new SimpleDateFormat("yyyy-MM-dd")).format(fetchDate);
        try {
            query = "select text_changes,total_urls,new_urls,removed_urls,total_words,new_words,removed_words from cmp_summary where info_id=" + infoId + " and date_format(date,'%Y-%m-%d') = date_format('" + finalFetchDate + "','%Y-%m-%d')";
            CmpSummary cmpWmSummary = getJdbcTemplate().query(query, new ResultSetExtractor<CmpSummary>() {
                CmpSummary cmpSumary;

                public CmpSummary extractData(ResultSet rs) {
                    try {
                        if (rs.next()) {
                            cmpSumary = new CmpSummary();
                            cmpSumary.setTotalWords(rs.getInt("total_words"));
                            cmpSumary.setNewWords(rs.getInt("new_words"));
                            cmpSumary.setRemovedWords(rs.getInt("removed_words"));
                            cmpSumary.setBodyText(new String(rs.getBytes("text_changes")));
                            cmpSumary.setTotalUrls(rs.getInt("total_urls"));
                            cmpSumary.setRemovedUrls(rs.getInt("removed_urls"));
                            cmpSumary.setNewUrls(rs.getInt("new_urls"));

                        }
                    } catch (SQLException e) {
                        logger.error(e.getMessage());
                    }
                    return cmpSumary;
                }
            });
            return cmpWmSummary;
        } catch (Exception e) {
            logger.error("Exception in getContentChanges: ", e);
        }

        return null;
    }

    public long fetchInfoId(long userId) {
        long infoId = 0;
        Long id = fetchInfoIdDb(userId);
        if (id != null) {
            logger.debug("infoId : " + id.longValue() + "id :" + id);
            infoId = id.longValue();
        }
        return infoId;
    }

    public Long fetchInfoIdDb(long userId) {
        /*Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] 
            with reference to mail from Pavan Sir Subj:LXRM Tool Changes.
            String query = "select info_id from cmp_url_Info where user_id="+userId+" and is_deleted=0";*/
        String query = "select info_id from cmp_url_info where user_id=" + userId + " and is_deleted=0";
        try {

            Long infoId = getJdbcTemplate().query(query, (ResultSet rs) -> {
                Long infiID = null;
                try {
                    if (rs.next()) {
                        infiID = new Long(rs.getLong("info_id"));
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return infiID;
            });
            return infoId;

        } catch (Exception ex) {
            logger.error("Exception in fetchInfoIdDb", ex);
        }
        return null;
    }

    public long fetchUserId(long infoId) {
        long userId = 0;
        String query = "select user_id from cmp_url_info where info_id=" + infoId;
        try {
            userId = (int) getJdbcTemplate().queryForLong(query);
        } catch (Exception ex) {
            logger.error("Exception in fetchUserId" + ex);
        }
        return userId;
    }

    public List getEmailFromCmpwebEmails(long userId) {
        String query = "select email from cmpweb_emails where date=(select max(date) from cmpweb_emails "
                + "where info_id=(select  info_id from cmp_url_info where user_id =" + userId + " and is_deleted= 0))";

        List<String> totEmails = new ArrayList<>();
        Map cmpWebData;
        try {
            List<Map<String, Object>> b = getJdbcTemplate().queryForList(query);
            for (int i = 0; i < b.size(); i++) {
                cmpWebData = b.get(i);
                totEmails.add((String) cmpWebData.get("email"));
            }

            return totEmails;
        } catch (Exception e) {
            logger.error("Exception in getEmailFromCmpwebEmails", e);
        }
        return null;
    }

    public CompetitorWebpageMonitor checkUserRecords(long userId, CompetitorWebpageMonitor cmpWebMonitorInfo) {
        String webEmail = "";
        List<String> cmpWebMails = getEmailFromCmpwebEmails(userId);
        if (cmpWebMails != null && cmpWebMails.size() > 0) {
            webEmail = cmpWebMails.get(0);
            logger.info("checking for user records with id:" + userId + "webmail is" + webEmail);
        }
        String query = "SELECT a.info_id,a.start_date,a.cmp_url,a.frequency,emial_unsubscription,tool_unsubscription,user.email FROM "
                + "(select * from cmp_url_info where is_deleted = 0)a INNER JOIN (user) ON (user.id = a.user_id) "
                + "where user.id =" + userId;
        logger.info("Queryy to check user record : " + query);
        final String email = webEmail;
        try {
            cmpWebMonitorInfo = (CompetitorWebpageMonitor) getJdbcTemplate().queryForObject(query,
                    new RowMapper<Object>() {
                CompetitorWebpageMonitor cmpWebMonitorInfo;

                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    cmpWebMonitorInfo = new CompetitorWebpageMonitor();
                    if (email != null && !email.trim().equals("")) {
                        cmpWebMonitorInfo.setEmail(email);
                    } else {
                        cmpWebMonitorInfo.setEmail(rs.getString("email"));
                    }
                    logger.debug("bean contained email is  :" + cmpWebMonitorInfo.getEmail());
                    cmpWebMonitorInfo.setCompetitorUrl(rs.getString("cmp_url"));
                    cmpWebMonitorInfo.setAlertFrequency(rs.getInt("frequency"));
                    cmpWebMonitorInfo.setEmailUnsubscription(rs.getBoolean("emial_unsubscription"));
                    cmpWebMonitorInfo.setToolUnsubscription(rs.getBoolean("tool_unsubscription"));
                    cmpWebMonitorInfo.setStartdate(rs.getTimestamp("start_date"));
                    cmpWebMonitorInfo.setUser_Id(userId);
                    return cmpWebMonitorInfo;
                }
            });
            cmpWebMonitorInfo.setUser_Id(userId);
            return cmpWebMonitorInfo;
        } catch (Exception e) {
            logger.error("Exception in checkUserRecords case:: "+e.getLocalizedMessage());
        }
        return null;
    }

    public int updateUserInfo(CompetitorWebpageMonitor cmpMonitor) {
        String query = "update cmp_url_info set is_deleted = ? where user_id=" + cmpMonitor.getUser_Id();

        Object[] valLis = new Object[1];
        valLis[0] = 1;
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.error("Exception in updateUserInfo" + ex.getMessage());
        }
        return 0;
    }

    public int updateUrlInfo(CompetitorWebpageMonitor cmpMonitor) {
        String query = "update cmp_url_info set frequency = ? where user_id=" + cmpMonitor.getUser_Id();
        Object[] valLis = new Object[1];
        valLis[0] = cmpMonitor.getAlertFrequency();
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.info("Exception in updateUrlInfo" + ex.getMessage());
        }
        return 0;
    }

    public int updateUnsubscriptionInfo(CompetitorWebpageMonitor cmpMonitor, long infoId, Timestamp unsubTime) {
        Timestamp toolUnsubDate = null, emailUnsubDate = null;
        String unsubMsg = "";
        /*Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference 
                 to mail from Pavan Sir Subj:LXRM Tool Changes.
    		String query = "update cmp_url_Info set tool_unsubscription = ?,emial_unsubscription = ?," +
    				" emial_unsub_date = ? , tool_unsub_date = ?,remark = ? where info_id="+infoId;*/

        String query = "update cmp_url_info set tool_unsubscription = ?,emial_unsubscription = ?,"
                + " emial_unsub_date = ? , tool_unsub_date = ?,remark = ? where info_id=" + infoId;

        Object[] valLis = new Object[5];
        valLis[0] = cmpMonitor.isToolUnsubscription();
        valLis[1] = cmpMonitor.isEmailUnsubscription();
        if (cmpMonitor.isEmailUnsubscription()) {
            emailUnsubDate = unsubTime;
        }
        if (cmpMonitor.isToolUnsubscription()) {
            toolUnsubDate = unsubTime;
            unsubMsg = "User Unsubscribed";
        }
        valLis[2] = emailUnsubDate;
        valLis[3] = toolUnsubDate;
        valLis[4] = unsubMsg;
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.error("Exception in updateUnsubscriptionInfo " + ex.getMessage());
        }
        return 0;
    }

    public int updateUserEmail(CompetitorWebpageMonitor cmpMonitor) {
        /*Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference 
                 to mail from Pavan Sir Subj:LXRM Tool Changes.
    		String query = "update cmp_url_Info set user_email=? where user_id="+cmpMonitor.getUser_Id();*/
        String query = "update cmp_url_info set user_email=? where user_id=" + cmpMonitor.getUser_Id();

        try {
            return getJdbcTemplate().update(query);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return 0;
    }

    public List<String> comparingUserInfo(long userId) {
        /*Note: Renamed the table cmp_url_Info. New table name: cmp_url_info [Note the lower case I] with reference 
                 to mail from Pavan Sir Subj:LXRM Tool Changes.
        String query="SELECT a.info_id,user.email,user.id,a.start_date FROM " +
    			"(select * from cmp_url_Info where is_deleted = 0)a INNER JOIN (user) ON (user.id = a.user_id)  where user.id ="+userId ;*/

        String query = "SELECT a.info_id,user.email,user.id,a.start_date FROM "
                + "(select * from cmp_url_info where is_deleted = 0)a INNER JOIN (user) ON (user.id = a.user_id)  where user.id =" + userId;

        Map userInfo;
        List<String> userData = new ArrayList<>();
        try {
            List<Map<String, Object>> b = getJdbcTemplate().queryForList(query);
            for (int i = 0; i < b.size(); i++) {
                userInfo = b.get(i);
                userData.add("" + userInfo.get("info_id"));
                userData.add((String) userInfo.get("email"));
            }
            return userData;
        } catch (Exception e) {
            logger.error("Exception in comparingUserInfo" + e.getMessage());
        }

        return null;

    }

    public List getCmpWebEmails(long infoId) {
        String query = "select email from cmpweb_emails where date ="
                + "(select max(date) from cmpweb_emails where info_id =" + infoId + ")";
        List<String> totEmails = new ArrayList<>();
        Map cmpWebData;
        try {
            List<Map<String, Object>> b = getJdbcTemplate().queryForList(query);
            for (int i = 0; i < b.size(); i++) {
                cmpWebData = b.get(i);
                totEmails.add((String) cmpWebData.get("email"));
            }

            return totEmails;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public boolean userToolData(long userId, String usermail) {
        boolean recordExists = false;
        int strtindex = 0;
        String guestEmail = "";
        List<String> userData = comparingUserInfo(userId);
        if (userData != null && userData.size() > 0) {
            for (int i = 0; i <= userData.size(); i++) {
                long infoId = Long.parseLong(userData.get(0));
                String email = userData.get(1).trim();
                if (email.contains("GuestUser")) {
                    guestEmail = "GuestUser" + userId + "<";
                    strtindex = guestEmail.length();
                    email = email.substring(strtindex, email.lastIndexOf(">"));
                }
                logger.debug("user table email" + email + " " + usermail);
                if (email.equalsIgnoreCase(usermail.trim())) {
                    recordExists = true;
                    break;
                } else {
                    List<String> cmpWebMails = getCmpWebEmails(infoId);
                    if (cmpWebMails != null && cmpWebMails.size() > 0) {
                        String webEmail = cmpWebMails.get(0);
                        logger.debug("webmail is" + webEmail + " " + usermail);
                        if (webEmail.equalsIgnoreCase(usermail)) {
                            recordExists = true;
                        }
                    }
                }
            }
        }
        logger.info("record exists" + recordExists);
        return recordExists;
    }

    public void compareImgResults(List<CmpImgInfo> prvList, List<Long> curIds, int val[]) {
        List<Long> prvIds = new ArrayList<>();
        prvList.stream().map((cmp) -> cmp.getImgId()).forEach((prvId1) -> {
            prvIds.add(prvId1);
        });
        prvList.stream().map((cmp) -> cmp.getImgId()).filter((prvId) -> (curIds.contains(prvId))).map((prvId) -> {
            curIds.remove(prvId);
            return prvId;
        }).forEach((prvId) -> {
            prvIds.remove(prvId);
        });
        if (curIds.size() > 0 && prvIds.size() > 0) {
            val[0] = ++val[0];
            logger.debug("in compare images : " + val[0]);
        }
    }

    public String removeProtocol(String userUrl) {
        String finalUrl = "";
        if (userUrl.startsWith("http://")) {
            finalUrl = userUrl.replace("http://", "");
        } else if (userUrl.startsWith("https://")) {
            finalUrl = userUrl.replace("https://", "");
        }
        if (finalUrl.startsWith("www.")) {
            finalUrl = finalUrl.replace("www.", "");
        }
        return finalUrl;
    }

    public CmpSummary getSummaryData(long infoId) {

        String query = "select * from cmp_summary where info_id = ? and is_deleted = 0  and date =(select max(date) from cmp_summary where info_id = ? and is_deleted = 0 )";
        try {
            CmpSummary cmpWmSummary = getJdbcTemplate().query(query, new Object[]{infoId, infoId}, new ResultSetExtractor<CmpSummary>() {
                CmpSummary cmpSumary;

                @Override
                public CmpSummary extractData(ResultSet rs) {
                    try {
                        if (rs.next()) {
                            cmpSumary = new CmpSummary();
                            cmpSumary.setInfo_id(rs.getLong("info_id"));
                            cmpSumary.setStartDate(rs.getTimestamp("date"));
                            cmpSumary.setTotalWords(rs.getInt("total_words"));
                            cmpSumary.setNewWords(rs.getInt("new_words"));
                            cmpSumary.setRemovedWords(rs.getInt("removed_words"));
                            cmpSumary.setBodyText(new String(rs.getBytes("text_changes")));
                            cmpSumary.setTotalUrls(rs.getInt("total_urls"));
                            cmpSumary.setRemovedUrls(rs.getInt("removed_urls"));
                            cmpSumary.setNewUrls(rs.getInt("new_urls"));

                        }
                    } catch (SQLException e) {
                        logger.error(e);
                    }
                    return cmpSumary;
                }
            });
            return cmpWmSummary;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public List<CmpData> getCmpData(long infoId) {

        String query = "select  * from cmp_data where info_id = ? and date= (select max(date) from cmp_data where info_id = ?)";
        logger.info("getCmpData query :" + query);

        try {
            List<CmpData> cmpData = getJdbcTemplate().query(query, new Object[]{infoId, infoId}, (ResultSet rs, int rowNum) -> {
                CmpData cmp = new CmpData();
                cmp.setInfo_id(rs.getLong("info_id"));
                cmp.setStartDate(rs.getTimestamp("date"));
                cmp.setContent_type(rs.getInt("content_type"));
                cmp.setContent_id(rs.getLong("content_id"));
                return cmp;
            });
            return cmpData;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }

    public void insertCmpDataVersion2(final List<CmpData> cmpDataList, final Timestamp newTime) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(newTime);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        final String curDate = (new SimpleDateFormat("yyyy-MM-dd")).format(cal1.getTime());
        final String query = "insert into cmp_data(info_id,date,content_type,content_id) values (?,?,?,?)";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                CmpData cmp = cmpDataList.get(i);
                ps.setLong(1, cmp.getInfo_id());
                ps.setString(2, curDate);
                ps.setInt(3, cmp.getContent_type());
                ps.setLong(4, cmp.getContent_id());
            }

            @Override
            public int getBatchSize() {
                return cmpDataList.size();
            }
        });
    }

    public void jsonKeywordChartData(JSONArray googleChartData, int type, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.put(0, googleChartData);
            arr.put(1, type);
            writer.print(arr);
        } catch (Exception e) {
            logger.error("Exception in jsonKeywordChartData" + e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public Date getSummaryMaxDateForUser(long infoId) {
        String query = "select max(date_format(s.date,'%Y-%m-%d')) as maxDate from cmp_summary s where s.info_id"
                + "=" + infoId + "";
        SqlRowSet sqlRowSet = null;
        Date resultDate = null;
        try {																	// toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    String tempresultDate = sqlRowSet.getString("maxDate");
                    logger.info("Max date is as string " + tempresultDate + "for user id " + infoId);
                    resultDate = convertStringtoDate(tempresultDate);
                    return resultDate;
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getDateForSocialMediaUser" + e.getMessage());
            return null;
        }
        return null;
    }

    public Date getCmpDataMaxDateForUser(long infoId) {
        String query = "select max(date_format(cd.date,'%Y-%m-%d')) as maxDate from cmp_data cd where cd.info_id"
                + "=" + infoId + "";
        SqlRowSet sqlRowSet = null;
        Date resultDate = null;
        try {																	// toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    String tempresultDate = sqlRowSet.getString("maxDate");
                    resultDate = convertStringtoDate(tempresultDate);
                    return resultDate;
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getDateForSocialMediaUser" + e.getMessage());
            return null;
        }
        return null;
    }

    public Date convertStringtoDate(String maxDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {

            date = formatter.parse(maxDate);
            return date;
        } catch (ParseException e) {
            logger.error("ParseException for convertStringtoDate" + e.getMessage());
            return date;
        } catch (Exception e) {
            logger.error("Exception for convertStringtoDate" + e.getMessage());
            return date;
        }

    }

    public String getPreviousDate(Date myDate, int format) {
        SimpleDateFormat dateFormat = null;
        if (format == 1) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.DATE, -1);

        return dateFormat.format(cal.getTime());

    }

    public static String getUrlValidation(String currentUrl) {
        try {
            if (!(currentUrl.equals(""))) {
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                int statusCode = 0;
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpCurrentUrl;
                        }
                    }
                }
                statusCode = 0;
                redirectedUrl = new StringBuilder("");
                String httpsCurrentUrl = "";
                if (currentUrl.startsWith("https://")) {
                    httpsCurrentUrl = currentUrl;
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);

                } else if (!currentUrl.startsWith("http://")) {
                    httpsCurrentUrl = "https://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpsCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                        httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                        statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpsCurrentUrl;
                        } else {
                            return "invalid";
                        }
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            } else {
                return "invalid";
            }
        } catch (Exception e) {
            logger.error("Exception in getUrlValidation" + e.getMessage());
        }
        return "invalid";
    }

    public static int getStatusCodeVersion2(String currentUrl, StringBuilder redirectedUrl) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        int iteration = 0;
        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 5) {
            try {
                URL currentUrlObj = new URL(currentUrl);
                if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                    currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                    currentUrlConn.setInstanceFollowRedirects(false);
                    currentUrlConn.setConnectTimeout(50000);
                    currentUrlConn.setReadTimeout(10000);
                    currentUrlConn.setRequestProperty("User-Agent", Common.USER_AGENT);
                    responseCode = currentUrlConn.getResponseCode();
                    logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                    if (responseCode / 100 == 3) {
                        if (iteration != 4) {
                            String redirectedLocation = currentUrlConn.getHeaderField("Location");
                            String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                            if (redirectedLocation != null) {
                                if (!(redirectedLocation.startsWith("http"))) {
                                    if (redirectedLocation.startsWith("/")) {
                                        currentUrl = tempUrl + redirectedLocation;
                                    } else if (!(redirectedLocation.startsWith("/"))) {
                                        if (currentUrl.endsWith("/")) {
                                            currentUrl = currentUrl + redirectedLocation;
                                        } else {
                                            currentUrl = currentUrl + "/" + redirectedLocation;
                                        }
                                    }
                                } else {
                                    currentUrl = redirectedLocation;
                                }
                            }
                        } else {
                            redirectedUrl.append("");
                        }
                    } else if (responseCode == 200 && iteration != 0) {
                        redirectedUrl.append(currentUrl);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in connection to " + currentUrl + " Cause: " + e.getMessage());
            }
            iteration++;
        }
        return responseCode;
    }
}
