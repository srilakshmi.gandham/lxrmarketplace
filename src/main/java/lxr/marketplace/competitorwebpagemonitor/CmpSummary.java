package lxr.marketplace.competitorwebpagemonitor;

import java.sql.Timestamp;

public class CmpSummary {
	long info_id;
    Timestamp startDate;
    long totalUrls;
    long newUrls;
    long removedUrls;
    long totalWords;
    long removedWords;
    long newWords;
    String bodyText;
    String remark;
    String resultDate;

    public String getResultDate() {
        return resultDate;
    }

    public void setResultDate(String resultDate) {
        this.resultDate = resultDate;
    }
    
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public long getInfo_id() {
		return info_id;
	}
	public void setInfo_id(long info_id) {
		this.info_id = info_id;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public long getTotalUrls() {
		return totalUrls;
	}
	public void setTotalUrls(long totalUrls) {
		this.totalUrls = totalUrls;
	}
	public long getNewUrls() {
		return newUrls;
	}
	public void setNewUrls(long newUrls) {
		this.newUrls = newUrls;
	}
	public long getRemovedUrls() {
		return removedUrls;
	}
	public void setRemovedUrls(long removedUrls) {
		this.removedUrls = removedUrls;
	}
	public long getTotalWords() {
		return totalWords;
	}
	public void setTotalWords(long totalWords) {
		this.totalWords = totalWords;
	}
	public long getRemovedWords() {
		return removedWords;
	}
	public void setRemovedWords(long removedWords) {
		this.removedWords = removedWords;
	}
	public long getNewWords() {
		return newWords;
	}
	public void setNewWords(long newWords) {
		this.newWords = newWords;
	}
	public String getBodyText() {
		return bodyText;
	}
	public void setBodyText(String bodyText) {
		String TM = ""+(char)226+(char)132+(char)162;
		bodyText = bodyText.replaceAll(TM, "™").replaceAll("Â®", "®").replaceAll("Â©", "©").replace((char)153,'™').replace((char)160, ' ').replace((char)153,'�');
		this.bodyText = bodyText;
	}

 
}
