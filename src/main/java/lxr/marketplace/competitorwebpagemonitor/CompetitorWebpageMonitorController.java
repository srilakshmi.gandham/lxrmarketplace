package lxr.marketplace.competitorwebpagemonitor;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.SessionService;
import lxr.marketplace.session.ToolUsage;
import lxr.marketplace.user.CookieService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class CompetitorWebpageMonitorController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(CompetitorWebpageMonitorController.class);
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    LoginService loginService;
    CookieService cookieService;
    SessionService sessionService;
    String downloadFolder;
    String cmpImgFolder;
    CompetitorWebpageMonitorService competitorWebpageMonitorService;
    CmpLinksInfo cmpLinksInfo;
    CmpImgInfo cmpImgInfo;
    private EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private MessageSource messageSource;
    JSONObject userInputJson = null;

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setEmailTemplateService(
            EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public void setCookieService(CookieService cookieService) {
        this.cookieService = cookieService;
    }

    public CompetitorWebpageMonitorService getCompetitorWebpageMonitorService() {
        return competitorWebpageMonitorService;
    }

    public void setCompetitorWebpageMonitorService(
            CompetitorWebpageMonitorService competitorWebpageMonitorService) {
        this.competitorWebpageMonitorService = competitorWebpageMonitorService;
    }

    public CompetitorWebpageMonitorController() {
        setCommandClass(CompetitorWebpageMonitor.class);
        setCommandName("compWebpageMonitorTool");
    }

    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        String cmpUrl;
        boolean recordExists = false;
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        CompetitorWebpageMonitor cmpWebpageMonitor = null;
        /*		int info=0;*/
        //int noTimesToolUsed=0;
        long regUserInfoId = 0;
        int strtindex = 0;
        String usermailFromDb = "";
        String guestEmail = "";
        String userUrl = "";
        String dbUrl = "";
        System.setProperty("file.encoding", "ISO-8859-1");
        Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Date summarydate = null;
        Date cmpDatadate = null;
        cmpWebpageMonitor = (CompetitorWebpageMonitor) command;
        if (cmpWebpageMonitor != null) {
            logger.info(cmpWebpageMonitor.getNewUserEmail());
            session.setAttribute("compWebpageMonitorTool", cmpWebpageMonitor);
        }
        if (user != null) {
            if (request.getParameter("query") == null || (request.getParameter("query") != null
                    && !request.getParameter("query").equals("'desc'"))) {

                logger.info("request.getParameter(query)................" + request.getParameter("query"));
                Tool toolObj = new Tool();
                toolObj.setToolId(9);
                toolObj.setToolName("Competitor Webpage Monitor ");
                toolObj.setToolLink("competitor-webpage-monitor-tool.html");
                session.setAttribute("toolObj", toolObj);
                logger.info(" JVM character set : " + Charset.defaultCharset());
                Session userSession = (Session) session.getAttribute("userSession");
                session.setAttribute("currentTool", Common.COMPETITOR_WEBPAGE_MONITOR);
                if (userSession.getUserId() > 0) {
                    regUserInfoId = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                    logger.info("The regUserInfoId" + regUserInfoId);
                    cmpWebpageMonitor.setUser_Id(user.getId());//setting value of userid to command object;
                } else {
                    logger.info("The user visted to lxr marketplace is first time and creating new cookie and the tool usgae is null");
                    if (user.getId() < 0) {
                        //convert the user to guest user on social  usage.
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                        user = (Login) session.getAttribute("user");
                    }
                    cmpWebpageMonitor.setUser_Id(user.getId());//setting value of userid to command object;}
                }
                session.removeAttribute("googleLinkJSON");
                session.removeAttribute("googleContentJSON");
                //noTimesToolUsed=toolService.findToolUsage(userSession.getUserId(),9l);
                Map<Integer, ToolUsage> toolUsageInfo = (Map<Integer, ToolUsage>) userSession.getToolUsage();
                if (WebUtils.hasSubmitParameter(request, "getResult")) {
                    session.removeAttribute("googleLinkJSON");
                    session.removeAttribute("googleContentJSON");
                    if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                        session.setAttribute("loginrefresh", "loginrefresh");
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                        mAndV.addObject("result", "show");
                        mAndV.addObject("settings", "show");
                        return mAndV;

                    }
                    CompetitorWebpageMonitor cmpWebMonitorInfo = new CompetitorWebpageMonitor();
                    cmpWebMonitorInfo = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), cmpWebMonitorInfo);
                    if (cmpWebMonitorInfo != null) {
                        if (cmpWebMonitorInfo.isToolUnsubscription()) {
                            logger.info("checking data for logged in user : " + user.getId());
                            //						CompetitorWebpageMonitor compWebpageMonitorTool = new CompetitorWebpageMonitor();
                            //						compWebpageMonitorTool = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(),compWebpageMonitorTool);
                            //compWebpageMonitorTool = competitorWebpageMonitorService.findUserData(user.getId());
                            //						if(cmpWebMonitorInfo != null){
                            //	info = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                            String email = cmpWebMonitorInfo.getEmail();
                            if (email.contains("GuestUser")) {
                                guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                strtindex = guestEmail.length();
                                email = email.substring(strtindex, email.lastIndexOf(">"));
                            }
                            if (cmpWebMonitorInfo.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            cmpWebpageMonitor.setEmail(email);
                            cmpWebpageMonitor.setCompetitorUrl(cmpWebMonitorInfo.getCompetitorUrl());
                            cmpWebpageMonitor.setAlertFrequency(cmpWebMonitorInfo.getAlertFrequency());

                            cmpWebpageMonitor.setStartdate(startTime);
                            //showDatatoUser(session, userSession, regUserInfoId, cmpWebMonitorInfo);
                            summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(regUserInfoId);
                            cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(regUserInfoId);
                            boolean prepareDashBoard = false;
                            if (summarydate != null && cmpDatadate != null) {
                                session.removeAttribute("summarydate");
                                session.removeAttribute("cmpDatadate");
                                session.setAttribute("summarydate", summarydate);
                                session.setAttribute("cmpDatadate", cmpDatadate);
                                if (summarydate.compareTo(cmpDatadate) == 0) {
                                    prepareDashBoard = showDatatoUser(session, userSession, regUserInfoId, cmpWebMonitorInfo, summarydate);
                                }
                            }
                            ModelAndView mAndV = null;
                            mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            if (prepareDashBoard) {
                                if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                    session.removeAttribute("frequency");
                                    session.setAttribute("frequency", "Days");
                                } else {
                                    session.setAttribute("frequency", "Weeks");
                                }
                                mAndV.addObject("result", "hide");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "false");
                                mAndV.addObject("Mailsent", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsucbribe option.");
                                return mAndV;
                            } else {
                                if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                    session.removeAttribute("frequency");
                                    session.setAttribute("frequency", "Days");
                                } else {
                                    session.setAttribute("frequency", "Weeks");
                                }
                                mAndV.addObject("result", "hide");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "false");
                                mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                return mAndV;
                            }
                            //						}
                        }
                    }
                    cmpUrl = CompetitorWebpageMonitorService.getUrlValidation(cmpWebpageMonitor.getCompetitorUrl().trim());
                    logger.debug("Final user valid  url is: " + cmpUrl);
                    boolean prepareDashBoard = false;
                    if (cmpUrl.equals("invalid") || cmpUrl.equals("redirected")) {
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                        String notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                        mAndV.addObject("UrlnotWorking", notWorkingUrl);
                        mAndV.addObject("result", "hide");
                        mAndV.addObject("settings", "hide");
                        mAndV.addObject("googleCharts", "false");
                        return mAndV;
                    } else {
                        session.setAttribute("cmpUrl", cmpUrl);
                        cmpWebpageMonitor.setCompetitorUrl(cmpUrl);
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                        user = (Login) session.getAttribute("user");
                        if (cmpWebMonitorInfo != null) {
                            usermailFromDb = cmpWebMonitorInfo.getEmail();
                            if (usermailFromDb.contains("GuestUser")) {
                                guestEmail = "GuestUser" + user.getId() + "<";
                                strtindex = guestEmail.length();
                                usermailFromDb = usermailFromDb.substring(strtindex, usermailFromDb.lastIndexOf(">"));
                            }
                            long info_id = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                            logger.info("url from datbase: " + cmpWebMonitorInfo.getCompetitorUrl());
                            userUrl = competitorWebpageMonitorService.removeProtocol(cmpWebpageMonitor.getCompetitorUrl().trim());
                            dbUrl = competitorWebpageMonitorService.removeProtocol(cmpWebMonitorInfo.getCompetitorUrl().trim());
                            if (userUrl.equalsIgnoreCase(dbUrl)) {//need to change to info value which is retrieved from datbase
                                logger.info(" User did not change the settings");
                                if (!cmpWebpageMonitor.getEmail().trim().equals(usermailFromDb)) {
                                    logger.info("user changed only his email id");
                                    competitorWebpageMonitorService.insertNewEmailId(cmpWebpageMonitor, info_id);
                                    ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                    mAndV.addObject("Mailsent", "Your settings have been updated successfully.");
                                    mAndV.addObject("result", "show");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "true");
                                    return mAndV;
                                } else if (cmpWebpageMonitor.getAlertFrequency() != cmpWebMonitorInfo.getAlertFrequency()) {
                                    if (cmpWebpageMonitor.getAlertFrequency() == 1 && cmpWebMonitorInfo.getAlertFrequency() == 2) {
                                        logger.info("updating user information.....");
                                        competitorWebpageMonitorService.updateUserInfo(cmpWebpageMonitor);
                                        cmpWebpageMonitor.setStartdate(startTime);
                                        long infoId = competitorWebpageMonitorService.insertUserInfo(cmpWebpageMonitor);
                                        //userSession.addToolUsage(9l);
                                        CmpSummary cmpSummary = new CmpSummary();
                                        int[] val = {0};
                                        String usermail = cmpWebMonitorInfo.getEmail();
                                        if (usermail.contains("GuestUser")) {
                                            guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                            strtindex = guestEmail.length();
                                            usermail = usermail.substring(strtindex, usermail.lastIndexOf(">"));
                                        }
                                        logger.info("user from mail :" + usermail);
                                        cmpWebpageMonitor.setEmail(usermail);
                                        competitorWebpageMonitorService.getCmpResults(cmpWebpageMonitor.getCompetitorUrl(), infoId, user.getId(), cmpSummary, val);
                                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                        mAndV.addObject("Mailsent", "You will receive a mail shortly regarding updates on the submitted URL.");
                                        mAndV.addObject("result", "show");
                                        mAndV.addObject("settings", "show");
                                        mAndV.addObject("googleCharts", "true");
                                        return mAndV;
                                    } else {
                                        competitorWebpageMonitorService.updateUrlInfo(cmpWebpageMonitor);
                                    }
                                } else {
                                    logger.info("view result to existing user");
                                    if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                        session.removeAttribute("frequency");
                                        session.setAttribute("frequency", "Days");
                                    } else {
                                        session.setAttribute("frequency", "Weeks");
                                    }
                                    cmpWebpageMonitor.setStartdate(cmpWebMonitorInfo.getStartdate());
                                    cmpWebpageMonitor.setEmail(usermailFromDb);
                                    summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(info_id);
                                    cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(info_id);
                                    if (summarydate != null && cmpDatadate != null) {
                                        session.removeAttribute("summarydate");
                                        session.removeAttribute("cmpDatadate");
                                        session.setAttribute("summarydate", summarydate);
                                        session.setAttribute("cmpDatadate", cmpDatadate);
                                        if (summarydate.compareTo(cmpDatadate) == 0) {
                                            prepareDashBoard = showDatatoUser(session, userSession, info_id, cmpWebpageMonitor, summarydate);
                                        }
                                    }
                                    ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                    if (prepareDashBoard) {
                                        mAndV.addObject("result", "show");
                                        mAndV.addObject("settings", "show");
                                        mAndV.addObject("googleCharts", "true");
                                        return mAndV;
                                    } else {
                                        if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                            session.removeAttribute("frequency");
                                            session.setAttribute("frequency", "Days");
                                        } else {
                                            session.setAttribute("frequency", "Weeks");
                                        }
                                        mAndV.addObject("result", "hide");
                                        mAndV.addObject("settings", "show");
                                        mAndV.addObject("googleCharts", "false");
                                        mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                        return mAndV;
                                    }
                                }
                            } else {
                                logger.info("updating user information.....");
                                competitorWebpageMonitorService.updateUserInfo(cmpWebpageMonitor);
                                cmpWebpageMonitor.setStartdate(startTime);
                                if (user.getId() == -1) {
                                    user = (Login) session.getAttribute("user");
                                }

//                                if the request is coming from tool page then only add the user inputs to JSONObject
//                                if the request is coming from mail then do not add the user inputs to JSONObject
                                boolean requestFromMail = false;
                                if (session.getAttribute("requestFromMail") != null) {
                                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                                    session.removeAttribute("requestFromMail");
                                }
                                if (!requestFromMail && user != null && !user.isAdmin()) {
                                    userInputJson = new JSONObject();
                                    userInputJson.put("2", cmpWebpageMonitor.getEmail());
                                    userInputJson.put("3", cmpWebpageMonitor.getAlertFrequency());
                                    userInputJson.put("4", cmpWebpageMonitor.isEmailUnsubscription());
                                    userInputJson.put("5", cmpWebpageMonitor.isToolUnsubscription());
                                }

                                long infoId = competitorWebpageMonitorService.insertUserInfo(cmpWebpageMonitor);
                                //userSession.addToolUsage(9l);
                                sendMailToUser(userSession, cmpWebpageMonitor, infoId, user);
                                //showDatatoUser(session, userSession, infoId, cmpWebpageMonitor);
                                summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(infoId);
                                cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(infoId);
                                if (summarydate != null && cmpDatadate != null) {
                                    session.removeAttribute("summarydate");
                                    session.removeAttribute("cmpDatadate");
                                    session.setAttribute("summarydate", summarydate);
                                    session.setAttribute("cmpDatadate", cmpDatadate);
                                    if (summarydate.compareTo(cmpDatadate) == 0) {
                                        prepareDashBoard = showDatatoUser(session, userSession, infoId, cmpWebpageMonitor, summarydate);
                                    }
                                }
                                ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                if (prepareDashBoard) {
                                    mAndV.addObject("result", "show");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "true");
                                    mAndV.addObject("Mailsent", "You will receive a mail shortly regarding updates on the submitted URL.");
                                    return mAndV;
                                } else {
                                    if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                        session.removeAttribute("frequency");
                                        session.setAttribute("frequency", "Days");
                                    } else {
                                        session.setAttribute("frequency", "Weeks");
                                    }
                                    mAndV.addObject("result", "hide");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "false");
                                    mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                    return mAndV;
                                }
                            }
                        } else {
                            logger.info("When first time user comes here");
                            cmpWebpageMonitor.setStartdate(startTime);
                            long infoId = 0;
                            Login userData = loginService.find(cmpWebpageMonitor.getUser_Id());
                            logger.info(cmpWebpageMonitor.getCompetitorUrl() + "cmpUrl is " + cmpUrl);
                            if (user.getId() == -1) {
                                user = (Login) session.getAttribute("user");
                            }

//                            if the request is coming from tool page then only add the user inputs to JSONObject
//                            if the request is coming from mail then do not add the user inputs to JSONObject
                            boolean requestFromMail = false;
                            if (session.getAttribute("requestFromMail") != null) {
                                requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                                session.removeAttribute("requestFromMail");
                            }
                            if (!requestFromMail && user != null && !user.isAdmin()) {
                                userInputJson = new JSONObject();
                                userInputJson.put("2", cmpWebpageMonitor.getEmail());
                                userInputJson.put("3", cmpWebpageMonitor.getAlertFrequency());
                                userInputJson.put("4", cmpWebpageMonitor.isEmailUnsubscription());
                                userInputJson.put("5", cmpWebpageMonitor.isToolUnsubscription());
                            }

                            infoId = competitorWebpageMonitorService.insertUserInfo(cmpWebpageMonitor);
                            if (userData != null && userData.getUserName() != null && !(userData.getUserName().equals("Email")) && !(userData.getUserName().equalsIgnoreCase(cmpWebpageMonitor.getEmail()))) {
                                //insert new mailId in guestuser_emails table for that user.
                                competitorWebpageMonitorService.insertNewEmailId(cmpWebpageMonitor, infoId);
                            }
                            if (userData == null || userData.getUserName() == null || userData.getUserName().equals("Email")) {
                                logger.info("usermailId  : " + cmpWebpageMonitor.getEmail());
//                                String guestUserEmail = "GuestUser" + user.getId() + "<" + cmpWebpageMonitor.getEmail() + ">";
                                String guestUserEmail = cmpWebpageMonitor.getEmail();
                                if (user != null) {
                                    loginService.updateEmail(request, user.getId(), guestUserEmail);
                                    user.setUserName(cmpWebpageMonitor.getEmail());
                                    session.setAttribute("user", user);
                                    session.setAttribute("sendMailId", cmpWebpageMonitor.getEmail());
                                    //update activation date for the user
                                    if (user.getActivationDate() == null) {
                                        Calendar cal = Calendar.getInstance();
                                        user.setActivationDate(cal);
                                        loginService.updateActDate(user.getId(), new Timestamp(cal.getTimeInMillis()), 1);
                                    }
                                }
                            }
                            //}
                            //userSession.addToolUsage(9l);
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            sendMailToUser(userSession, cmpWebpageMonitor, infoId, user);
                            session.removeAttribute("usernewmail");
                            session.setAttribute("usernewmail", cmpWebpageMonitor.getEmail());
                            CompetitorWebpageMonitor newUserCompWebpageMonitorTool = new CompetitorWebpageMonitor();
                            newUserCompWebpageMonitorTool = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), newUserCompWebpageMonitorTool);
                            summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(infoId);
                            cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(infoId);
                            if (summarydate != null && cmpDatadate != null) {
                                session.removeAttribute("summarydate");
                                session.removeAttribute("cmpDatadate");
                                session.setAttribute("summarydate", summarydate);
                                session.setAttribute("cmpDatadate", cmpDatadate);
                                if (summarydate.compareTo(cmpDatadate) == 0) {
                                    prepareDashBoard = showDatatoUser(session, userSession, infoId, newUserCompWebpageMonitorTool, summarydate);
                                }
                            }
                            ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", newUserCompWebpageMonitorTool);
                            if (prepareDashBoard) {
                                mAndV.addObject("Mailsent", "You will receive a mail if any changes detected  on your competitor url.");
                                mAndV.addObject("result", "show");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "true");
                                return mAndV;
                            } else {
                                if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                    session.removeAttribute("frequency");
                                    session.setAttribute("frequency", "Days");
                                } else {
                                    session.setAttribute("frequency", "Weeks");
                                }
                                mAndV.addObject("result", "hide");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "false");
                                mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                return mAndV;
                            }
                            //			           }
                        }
                    }
                } else if (WebUtils.hasSubmitParameter(request, "editSettings")) {
                    CompetitorWebpageMonitor cmpWebMonitorInfo = new CompetitorWebpageMonitor();
                    cmpWebMonitorInfo = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), cmpWebMonitorInfo);
                    if (cmpWebMonitorInfo.isToolUnsubscription() && cmpWebpageMonitor.isToolUnsubscription()) {
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                        if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                            session.removeAttribute("frequency");
                            session.setAttribute("frequency", "Days");
                        } else {
                            session.setAttribute("frequency", "Weeks");
                        }
                        mAndV.addObject("result", "hide");
                        mAndV.addObject("settings", "show");
                        mAndV.addObject("googleCharts", "false");
                        mAndV.addObject("Mailsent", "Your Settings are not updated successfully. Because you are unsubscribed from tool.");
                        return mAndV;
                    }
                    cmpWebpageMonitor.setStartdate(startTime);
                    if (cmpWebMonitorInfo != null) {
                        if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                            session.removeAttribute("frequency");
                            session.setAttribute("frequency", "Days");
                        } else {
                            session.removeAttribute("frequency");
                            session.setAttribute("frequency", "Weeks");
                        }
                        if (!cmpWebpageMonitor.isToolUnsubscription()) {
                            usermailFromDb = cmpWebMonitorInfo.getEmail();
                            if (usermailFromDb.contains("GuestUser")) {
                                guestEmail = "GuestUser" + user.getId() + "<";
                                strtindex = guestEmail.length();
                                usermailFromDb = usermailFromDb.substring(strtindex, usermailFromDb.lastIndexOf(">"));
                            }
                            userUrl = competitorWebpageMonitorService.removeProtocol(cmpWebpageMonitor.getCompetitorUrl().trim());
                            dbUrl = competitorWebpageMonitorService.removeProtocol(cmpWebMonitorInfo.getCompetitorUrl().trim());

                            boolean prepareDashBoard = false;
                            long info_id = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                            summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(info_id);
                            cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(info_id);
                            if (summarydate != null && cmpDatadate != null) {
                                session.removeAttribute("summarydate");
                                session.removeAttribute("cmpDatadate");
                                session.setAttribute("summarydate", summarydate);
                                session.setAttribute("cmpDatadate", cmpDatadate);
                                if (summarydate.compareTo(cmpDatadate) == 0) {
                                    prepareDashBoard = showDatatoUser(session, userSession, info_id, cmpWebpageMonitor, summarydate);
                                }
                            }
                            if ((userUrl.equalsIgnoreCase(dbUrl)) && (prepareDashBoard)) {//need to change to info value which is retrieved from datbase
                                logger.info(" User did not change the settings");
                                session.setAttribute("cmpUrl", userUrl);
                                if (!cmpWebpageMonitor.getEmail().trim().equals(usermailFromDb)) {
                                    logger.info("user changed only his email id from " + cmpWebMonitorInfo.getEmail() + "to " + cmpWebpageMonitor.getEmail());

                                    //							if(userLoggedIn){
                                    logger.info("user looged in with :" + user.getUserName() + " changed mail Id to" + cmpWebpageMonitor.getEmail());
                                    /*No need to update the email to user object, the updated email is applicable to this tool only
                                    user.setUserName(cmpWebpageMonitor.getEmail());
                                    session.setAttribute("user", user);*/
                                    //							}
                                    session.removeAttribute("cmpmonitor");
                                    competitorWebpageMonitorService.insertNewEmailId(cmpWebpageMonitor, info_id);
                                    //							competitorWebpageMonitorService.updateUserEmail(cmpWebpageMonitor);
                                } else if (cmpWebpageMonitor.getAlertFrequency() != cmpWebMonitorInfo.getAlertFrequency()) {
                                    session.removeAttribute("cmpmonitor");
                                    if (cmpWebpageMonitor.getAlertFrequency() == 1 && cmpWebMonitorInfo.getAlertFrequency() == 2) {
                                        logger.info("updating user information.....");
                                        competitorWebpageMonitorService.updateUserInfo(cmpWebpageMonitor);
                                        cmpWebpageMonitor.setStartdate(startTime);
                                        if (user.getId() == -1) {
                                            user = (Login) session.getAttribute("user");
                                        }

//                                            if the request is coming from tool page then only add the user inputs to JSONObject
//                                            if the request is coming from mail then do not add the user inputs to JSONObject
                                        boolean requestFromMail = false;
                                        if (session.getAttribute("requestFromMail") != null) {
                                            requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                                            session.removeAttribute("requestFromMail");
                                        }
                                        if (!requestFromMail && user != null && !user.isAdmin()) {
                                            userInputJson = new JSONObject();
                                            userInputJson.put("2", cmpWebpageMonitor.getEmail());
                                            userInputJson.put("3", cmpWebpageMonitor.getAlertFrequency());
                                            userInputJson.put("4", cmpWebpageMonitor.isEmailUnsubscription());
                                            userInputJson.put("5", cmpWebpageMonitor.isToolUnsubscription());
                                        }

                                        long infoId = competitorWebpageMonitorService.insertUserInfo(cmpWebpageMonitor);

                                        //userSession.addToolUsage(9l);
                                        CmpSummary cmpSummary = new CmpSummary();
                                        int[] val = {0};
                                        String usermail = cmpWebMonitorInfo.getEmail();
                                        if (usermail.contains("GuestUser")) {
                                            guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                            strtindex = guestEmail.length();
                                            usermail = usermail.substring(strtindex, usermail.lastIndexOf(">"));
                                        }
                                        logger.info("user from mail :" + usermail);
                                        cmpWebpageMonitor.setEmail(usermail);
                                        competitorWebpageMonitorService.getCmpResults(cmpWebpageMonitor.getCompetitorUrl(), infoId, user.getId(), cmpSummary, val);
                                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                        mAndV.addObject("result", "show");
                                        mAndV.addObject("settings", "show");
                                        mAndV.addObject("googleCharts", "true");
                                        mAndV.addObject("hideTabs", "false");
                                        mAndV.addObject("Mailsent", "You will receive a mail shortly regarding updates on the submitted URL.");
                                        return mAndV;
                                    } else {
                                        competitorWebpageMonitorService.updateUrlInfo(cmpWebpageMonitor);
                                    }
                                    //								session.removeAttribute("usernewmail");
                                    //								session.setAttribute("usernewmail",cmpWebpageMonitor.getEmail());
                                }

                                session.removeAttribute("cmpmonitor");
                                competitorWebpageMonitorService.updateUnsubscriptionInfo(cmpWebpageMonitor, info_id, startTime);
                                CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                                session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                            } else {
                                logger.info("Updating user new competitor URL information.....");
                                cmpUrl = CompetitorWebpageMonitorService.getUrlValidation(cmpWebpageMonitor.getCompetitorUrl().trim());
                                logger.debug("final your url is :" + cmpUrl);
                                if (cmpUrl.equals("invalid") || cmpUrl.equals("redirected")) {
                                    ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                    mAndV.addObject("result", "hide");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "false");
                                    String notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
//                                    mAndV.addObject("UrlnotWorking", "Submitted URL is not working, please check again.");
                                    mAndV.addObject("UrlnotWorking", notWorkingUrl);
                                    return mAndV;
                                } else {
                                    competitorWebpageMonitorService.updateUserInfo(cmpWebpageMonitor);
                                    session.setAttribute("cmpUrl", cmpUrl);
                                    cmpWebpageMonitor.setCompetitorUrl(cmpUrl);
                                    //userSession.addToolUsage(9l);
                                    if (user.getId() == -1) {
                                        user = (Login) session.getAttribute("user");
                                    }

//                                    if the request is coming from tool page then only add the user inputs to JSONObject
//                                    if the request is coming from mail then do not add the user inputs to JSONObject
                                    boolean requestFromMail = false;
                                    if (session.getAttribute("requestFromMail") != null) {
                                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                                        session.removeAttribute("requestFromMail");
                                    }
                                    if (!requestFromMail && user != null && !user.isAdmin()) {
                                        userInputJson = new JSONObject();
                                        userInputJson.put("2", cmpWebpageMonitor.getEmail());
                                        userInputJson.put("3", cmpWebpageMonitor.getAlertFrequency());
                                        userInputJson.put("4", cmpWebpageMonitor.isEmailUnsubscription());
                                        userInputJson.put("5", cmpWebpageMonitor.isToolUnsubscription());
                                    }

                                    long upDatedinfoId = competitorWebpageMonitorService.insertUserInfo(cmpWebpageMonitor);
                                    sendMailToUser(userSession, cmpWebpageMonitor, upDatedinfoId, user);
                                    //showDatatoUser(session, userSession, upDatedinfoId, cmpWebpageMonitor);
                                    summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(upDatedinfoId);
                                    cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(upDatedinfoId);
                                    if (summarydate != null && cmpDatadate != null) {
                                        session.removeAttribute("summarydate");
                                        session.removeAttribute("cmpDatadate");
                                        session.setAttribute("summarydate", summarydate);
                                        session.setAttribute("cmpDatadate", cmpDatadate);
                                        if (summarydate.compareTo(cmpDatadate) == 0) {
                                            prepareDashBoard = showDatatoUser(session, userSession, upDatedinfoId, cmpWebpageMonitor, summarydate);
                                        }
                                    }
                                }
                            }
                            session.removeAttribute("cmpmonitor");
                            CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                            session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                            session.removeAttribute("usernewmail");
                            session.setAttribute("usernewmail", cmpWebpageMonitor.getEmail());
                            ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            if (prepareDashBoard) {
                                mAndV.addObject("Mailsent", "Your settings have been updated successfully.");
                                mAndV.addObject("result", "show");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "true");
                                return mAndV;
                            } else {
                                mAndV.addObject("result", "hide");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "false");
                                mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                return mAndV;
                            }
                        } else {
                            long info_id = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                            session.removeAttribute("cmpmonitor");
                            competitorWebpageMonitorService.updateUnsubscriptionInfo(cmpWebpageMonitor, info_id, startTime);
                            CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                            session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                            ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            mAndV.addObject("result", "hide");
                            mAndV.addObject("settings", "show");
                            mAndV.addObject("googleCharts", "false");
                            mAndV.addObject("Mailsent", "Your Settings are updated successfully. You are unsubscribed from tool.");
                            return mAndV;
                        }

                    }
                } else if (WebUtils.hasSubmitParameter(request, "userResult")) {//for user who already used the tool and came from browser
                    logger.info("Request parameter: userResult.");
                    session.removeAttribute("googleLinkJSON");
                    session.removeAttribute("googleContentJSON");
                    String userMail = cmpWebpageMonitor.getNewUserEmail().trim();
                    logger.info("Fetch the result for user with email :" + userMail + "and userId : " + userSession.getUserId());
                    //CompetitorWebpageMonitor cmpWebMonitorInfo = new CompetitorWebpageMonitor();
                    recordExists = competitorWebpageMonitorService.userToolData(userSession.getUserId(), userMail);
                    if (recordExists) {
                        //fetch data for success screen
                        long info_id = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                        CompetitorWebpageMonitor cmpWebMonitorInfonew = new CompetitorWebpageMonitor();
                        cmpWebMonitorInfonew = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), cmpWebMonitorInfonew);

                        if (cmpWebMonitorInfonew.getAlertFrequency() == 1) {
                            session.removeAttribute("frequency");
                            session.setAttribute("frequency", "Days");
                        } else {
                            session.setAttribute("frequency", "Weeks");
                        }
                        //userSession.addToolUsage(9l);
                        String userRegisteredUrl = cmpWebMonitorInfonew.getCompetitorUrl();
                        session.setAttribute("cmpUrl", userRegisteredUrl);
                        String email = cmpWebMonitorInfonew.getEmail().trim();
                        logger.info("email of user :" + email);
                        if (email.contains("GuestUser")) {
                            guestEmail = "GuestUser" + user.getId() + "<";
                            strtindex = guestEmail.length();
                            email = email.substring(strtindex, email.lastIndexOf(">"));
                        }
                        cmpWebpageMonitor.setEmail(email);
                        cmpWebpageMonitor.setAlertFrequency(cmpWebMonitorInfonew.getAlertFrequency());
                        cmpWebpageMonitor.setCompetitorUrl(cmpWebMonitorInfonew.getCompetitorUrl());
                        cmpWebpageMonitor.setStartdate(cmpWebMonitorInfonew.getStartdate());
                        //showDatatoUser(session, userSession, info_id, cmpWebMonitorInfonew);
                        summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(info_id);
                        cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(info_id);
                        boolean prepareDashBoard = false;
                        if (summarydate != null && cmpDatadate != null) {
                            if (summarydate.compareTo(cmpDatadate) == 0) {
                                session.removeAttribute("summarydate");
                                session.removeAttribute("cmpDatadate");
                                session.setAttribute("summarydate", summarydate);
                                session.setAttribute("cmpDatadate", cmpDatadate);
                                prepareDashBoard = showDatatoUser(session, userSession, info_id, cmpWebMonitorInfonew, summarydate);
                            }
                        }
                        ModelAndView mAndV = null;
                        if (prepareDashBoard) {
                            CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                            session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                            mAndV = new ModelAndView(getSuccessView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            mAndV.addObject("result", "show");
                            mAndV.addObject("settings", "show");
                            return mAndV;
                        } else {
                            mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            mAndV.addObject("result", "hide");
                            mAndV.addObject("settings", "show");
                            mAndV.addObject("googleCharts", "false");
                            mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                            return mAndV;
                        }
                    } else {
                        session.removeAttribute("cmpmonitor");
                        //long uId = loginService.insertGuestUser(Common.checkLocal(request));
                        Cookie cookie = cookieService.createCookie(response, Common.checkLocal(request), false);
//						long newCookieval = Long.parseLong(cookie.getValue());
                        encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
                        long newCookieval = Long.parseLong(encryptAndDecryptUsingDES.decrypt(cookie.getValue()));
                        String guestUserEmail = "GuestUser" + newCookieval + "<" + userMail.trim() + ">";
                        loginService.updateEmail(request, newCookieval, guestUserEmail);
                        userSession.setUserId(newCookieval);
                        session.setAttribute("marketplaceCookie", cookie);
                        user = loginService.find(newCookieval);
                        user.setUserName(userMail.trim());
                        session.setAttribute("user", user);
                        sessionService.updateUserId(userSession);
                        //session.setAttribute("userSession", userSession);
                        logger.info("new user entry in user with userId : " + newCookieval + " and email " + userMail);
                        CompetitorWebpageMonitor compWebpageMonitorTool = new CompetitorWebpageMonitor();
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", compWebpageMonitorTool);
                        mAndV.addObject("result", "hide");
                        mAndV.addObject("settings", "hide");
                        return mAndV;
                    }
                } else if (WebUtils.hasSubmitParameter(request, "info_id")) {
                    /*To handle User trying to login through Email came from schedulers.*/
                    if (userLoggedIn) {
                        session.removeAttribute("googleLinkJSON");
                        session.removeAttribute("googleContentJSON");
                        long info_id = Long.parseLong(WebUtils.findParameterValue(request, "info_id"));
                        long uId = competitorWebpageMonitorService.fetchUserId(info_id);
                        long oldUserId = userSession.getUserId();
                        logger.info("Request from user email login for info_id: " + info_id + " and user id: " + uId + " OldUser ID :" + oldUserId);
                        userSession.setUserId(uId);
                        cookieService.changeCookieValue(request, response, uId);
                        user = loginService.find(uId);
                        session.setAttribute("user", user);
                        if (oldUserId >= 0) {
                            sessionService.updateUserId(userSession);
                            logger.info("usersession with id: " + userSession.getId() + "  & userId : " + userSession.getUserId());
                        } else {
                            sessionService.insert(userSession);
                        }
                        info_id = competitorWebpageMonitorService.fetchInfoId(uId);//to fetch the new info id of user if he changes the url
                        CompetitorWebpageMonitor cmpInfo = null;
                        cmpInfo = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), cmpInfo);
                        if (cmpInfo != null) {
                            String userRegistredUrl = cmpInfo.getCompetitorUrl();
                            session.setAttribute("cmpUrl", userRegistredUrl);
                            String usermail = cmpInfo.getEmail();
                            if (usermail.contains("GuestUser")) {
                                guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                strtindex = guestEmail.length();
                                usermail = usermail.substring(strtindex, usermail.lastIndexOf(">"));
                            }
                            logger.info("user from mail :" + usermail);
                            cmpWebpageMonitor.setEmail(usermail);
                            cmpWebpageMonitor.setAlertFrequency(cmpInfo.getAlertFrequency());
                            cmpWebpageMonitor.setCompetitorUrl(cmpInfo.getCompetitorUrl());
                            cmpWebpageMonitor.setStartdate(cmpInfo.getStartdate());
                            if (cmpInfo.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                        }
                        userSession.addToolUsage(toolObj.getToolId());
                        CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                        session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                        //showDatatoUser(session, userSession, info_id, cmpWebpageMonitor);
                        boolean prepareDashBoard = false;
                        summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(info_id);
                        cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(info_id);
                        if (summarydate != null && cmpDatadate != null) {
                            session.removeAttribute("summarydate");
                            session.removeAttribute("cmpDatadate");
                            session.setAttribute("summarydate", summarydate);
                            session.setAttribute("cmpDatadate", cmpDatadate);
                            if (summarydate.compareTo(cmpDatadate) == 0) {
                                prepareDashBoard = showDatatoUser(session, userSession, info_id, cmpWebpageMonitor, summarydate);
                            }
                        }
                        /*Navigation to sucess page.*/
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                        if (prepareDashBoard) {
                            mAndV.addObject("result", "show");
                            mAndV.addObject("settings", "show");
                            mAndV.addObject("googleCharts", "true");
                            mAndV.addObject("login", "show");
                            return mAndV;
                        } else {
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            mAndV.addObject("result", "hide");
                            mAndV.addObject("settings", "show");
                            mAndV.addObject("googleCharts", "false");
                            mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                            return mAndV;
                        }
                    } else {
                        cmpWebpageMonitor = new CompetitorWebpageMonitor();
                        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                        mAndV.addObject("result", "hide");
                        mAndV.addObject("settings", "hide");
                        mAndV.addObject("googleCharts", "false");
                        return mAndV;
                    }
                } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                    logger.debug("**********clearing data");
                    session.removeAttribute("cmpmonitor");
                    session.removeAttribute("linkChartXml");
                    session.removeAttribute("contentChartXml");
                    session.removeAttribute("CmpSummaryInfo");
                    session.removeAttribute("frequency");
                    session.removeAttribute("lastdataDay");
                    session.removeAttribute("googleLinkJSON");
                    session.removeAttribute("googleContentJSON");
                    session.removeAttribute("cmpUrl");
                    session.removeAttribute("summarydate");
                    session.removeAttribute("cmpDatadate");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                    mAndV.addObject("result", "hide");
                    mAndV.addObject("settings", "hide");
                    mAndV.addObject("googleCharts", "false");
                    mAndV.addObject("clearAll", "show");
                    return mAndV;
                } else if (WebUtils.hasSubmitParameter(request, "getGoogleChart")) {
                    try {
                        logger.info("In  request of getGoogleData a AJAX CALL");
                        long info_id = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                        CmpSummary cmpSummaryInfo = new CmpSummary();
                        String[] chartDates = new String[7];
                        String strtdate = "";
                        CompetitorWebpageMonitor compWebpageMonitorTool = new CompetitorWebpageMonitor();
                        compWebpageMonitorTool = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), compWebpageMonitorTool);
                        //compWebpageMonitorTool = competitorWebpageMonitorService.findUserData(user.getId());
                        if (compWebpageMonitorTool != null) {
                            if (!(compWebpageMonitorTool.isEmailUnsubscription() == true)) {
                                userSession.addToolUsage(toolObj.getToolId());
                            }
                            //	info = competitorWebpageMonitorService.fetchInfoId(userSession.getUserId());
                            String email = compWebpageMonitorTool.getEmail();
                            if (email.contains("GuestUser")) {
                                guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                strtindex = guestEmail.length();
                                email = email.substring(strtindex, email.lastIndexOf(">"));
                            }
                            if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                            } else {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Weeks");
                            }
                            cmpWebpageMonitor.setEmail(email);
                            cmpWebpageMonitor.setCompetitorUrl(compWebpageMonitorTool.getCompetitorUrl());
                            cmpWebpageMonitor.setAlertFrequency(compWebpageMonitorTool.getAlertFrequency());
                            //compWebpageMonitorTool.setStartdate(startTime);
                            if (session.getAttribute("summarydate") != null) {
                                summarydate = (Date) session.getAttribute("summarydate");
                            } else {
                                summarydate = Calendar.getInstance().getTime();
                            }
                            strtdate = competitorWebpageMonitorService.getDatesForChart(compWebpageMonitorTool, chartDates, session, summarydate);
                            try {
                                SqlRowSet googleLinksqlRowSet = competitorWebpageMonitorService.getlinkDataForChart(cmpSummaryInfo, info_id, strtdate);
                                SqlRowSet googleContsqlRowSet = competitorWebpageMonitorService.getContentDataForChart(cmpSummaryInfo, info_id, strtdate);
                                JSONArray googleLinkJSON = competitorWebpageMonitorService.buildLinkChartJSON(googleLinksqlRowSet, chartDates);
                                JSONArray googleContentJSON = competitorWebpageMonitorService.buildContentChartJSON(googleContsqlRowSet, chartDates);

                                if ((googleLinkJSON != null) && (googleContentJSON != null)) {
                                    JSONArray totalGoogleChartData = new JSONArray();
                                    totalGoogleChartData.put(googleLinkJSON);
                                    totalGoogleChartData.put(googleContentJSON);
                                    if (compWebpageMonitorTool.getAlertFrequency() == 1) {
                                        competitorWebpageMonitorService.jsonKeywordChartData(totalGoogleChartData, 1, response);
                                    } else {
                                        competitorWebpageMonitorService.jsonKeywordChartData(totalGoogleChartData, 2, response);
                                    }
                                } else {
                                    competitorWebpageMonitorService.jsonKeywordChartData(new JSONArray(), 1, response);
                                }
                            } catch (Exception e) {
                                competitorWebpageMonitorService.jsonKeywordChartData(new JSONArray(), 1, response);
                                logger.error("Exception in constructing Google charts for AJAX call" + e.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Exception in getGoogleChart request paramter" + e.getMessage());
                        competitorWebpageMonitorService.jsonKeywordChartData(new JSONArray(), 1, response);
                    }
                    return null;
                } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                    String domain = request.getParameter("domain");
                    String toolIssues = "";
                    String questionInfo = "";
                    LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                    questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
                    questionInfo += "- " + "Auditing competitor webpages for their SEO activities." + "\n";
                    toolIssues += "Do you want a full audit of how your competitors are executing their SEO?";
                    lxrmAskExpert.setDomain(domain);
                    if (userInputJson != null) {
                        lxrmAskExpert.setOtherInputs(userInputJson.toString());
                    }
                    lxrmAskExpert.setQuestion(questionInfo);
                    lxrmAskExpert.setIssues(toolIssues);
                    lxrmAskExpert.setExpertInfo(questionInfo);
                    lxrmAskExpert.setToolName(Common.COMPETITOR_WEBPAGE_MONITOR);
                    lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                    if the request is not from  mail then we should add this input to the session object
                    if (userInputJson != null) {
                        String toolUrl = "/" + toolObj.getToolLink();
                        userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), domain, toolIssues,
                                questionInfo, userInputJson.toString(), toolUrl);
                        userInputJson = null;
                    }

                    return null;
                } else if (((userLoggedIn && regUserInfoId > 0) || (toolUsageInfo != null && toolUsageInfo.get(toolObj.getToolId()) != null))
                        && !WebUtils.hasSubmitParameter(request, "question-id")) {
                    //session.removeAttribute("googleLinkJSON");
                    //session.removeAttribute("googleContentJSON");
                    logger.info("Checking data for logged in user with user.getId: " + user.getId() + "user with userSession Id is" + userSession.getUserId());
                    CompetitorWebpageMonitor compWebpageMonitorToolTemp = new CompetitorWebpageMonitor();
                    compWebpageMonitorToolTemp = competitorWebpageMonitorService.checkUserRecords(userSession.getUserId(), compWebpageMonitorToolTemp);
                    CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
                    session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                    boolean prepareDashBoard = false;
                    if (compWebpageMonitorToolTemp != null) {
                        if (compWebpageMonitorSettings != null) {
                            if (compWebpageMonitorSettings.isToolUnsubscription()) {
                                String email = compWebpageMonitorToolTemp.getEmail();
                                if (email.contains("GuestUser")) {
                                    guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                    strtindex = guestEmail.length();
                                    email = email.substring(strtindex, email.lastIndexOf(">"));
                                }
                                if (compWebpageMonitorToolTemp.getAlertFrequency() == 1) {
                                    session.removeAttribute("frequency");
                                    session.setAttribute("frequency", "Days");
                                } else {
                                    session.setAttribute("frequency", "Weeks");
                                }
                                cmpWebpageMonitor.setEmail(email);
                                cmpWebpageMonitor.setCompetitorUrl(compWebpageMonitorToolTemp.getCompetitorUrl());
                                cmpWebpageMonitor.setAlertFrequency(compWebpageMonitorToolTemp.getAlertFrequency());
                                cmpWebpageMonitor.setStartdate(startTime);
                                summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(regUserInfoId);
                                cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(regUserInfoId);

                                if (summarydate != null && cmpDatadate != null) {
                                    session.removeAttribute("summarydate");
                                    session.removeAttribute("cmpDatadate");
                                    session.setAttribute("summarydate", summarydate);
                                    session.setAttribute("cmpDatadate", cmpDatadate);
                                    if (summarydate.compareTo(cmpDatadate) == 0) {
                                        prepareDashBoard = showDatatoUser(session, userSession, regUserInfoId, compWebpageMonitorToolTemp, summarydate);
                                    }
                                }
                                ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                if (prepareDashBoard) {
                                    mAndV.addObject("result", "hide");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "false");
                                    mAndV.addObject("Mailsent", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsucbribe option.");
                                    return mAndV;
                                } else {
                                    session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                                    if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                        session.removeAttribute("frequency");
                                        session.setAttribute("frequency", "Days");
                                    } else {
                                        session.setAttribute("frequency", "Weeks");
                                    }
                                    mAndV.addObject("result", "hide");
                                    mAndV.addObject("settings", "show");
                                    mAndV.addObject("googleCharts", "false");
                                    mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                    return mAndV;
                                }
                            }
                            String userRegistredUrl = compWebpageMonitorToolTemp.getCompetitorUrl();
                            session.setAttribute("cmpUrl", userRegistredUrl);
                            String email = compWebpageMonitorToolTemp.getEmail();
                            if (email.contains("GuestUser")) {
                                guestEmail = "GuestUser" + userSession.getUserId() + "<";
                                strtindex = guestEmail.length();
                                email = email.substring(strtindex, email.lastIndexOf(">"));
                            }
                            if (compWebpageMonitorToolTemp.getAlertFrequency() == 1) {
                                session.removeAttribute("frequency");
                                session.setAttribute("frequency", "Days");
                                if (!userLoggedIn) {
                                    cmpWebpageMonitor = new CompetitorWebpageMonitor();
                                    ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                                    mAndV.addObject("result", "hide");
                                    mAndV.addObject("settings", "hide");
                                    mAndV.addObject("googleCharts", "false");
                                    return mAndV;
                                }
                            } else {
                                session.setAttribute("frequency", "Weeks");
                            }
                            cmpWebpageMonitor.setEmail(email);
                            cmpWebpageMonitor.setCompetitorUrl(compWebpageMonitorToolTemp.getCompetitorUrl());
                            cmpWebpageMonitor.setAlertFrequency(compWebpageMonitorToolTemp.getAlertFrequency());
                            compWebpageMonitorToolTemp.setStartdate(startTime);
                            summarydate = competitorWebpageMonitorService.getSummaryMaxDateForUser(regUserInfoId);
                            cmpDatadate = competitorWebpageMonitorService.getCmpDataMaxDateForUser(regUserInfoId);
                            if (summarydate != null && cmpDatadate != null) {
                                session.removeAttribute("summarydate");
                                session.removeAttribute("cmpDatadate");
                                session.setAttribute("summarydate", summarydate);
                                session.setAttribute("cmpDatadate", cmpDatadate);

                                if (summarydate.compareTo(cmpDatadate) == 0) {
                                    prepareDashBoard = showDatatoUser(session, userSession, regUserInfoId, compWebpageMonitorToolTemp, summarydate);
                                }
                                /*else{showDatatoUser(session, userSession, regUserInfoId, compWebpageMonitorToolTemp,Calendar.getInstance().getTime());}*/
                            }
                            /*else{showDatatoUser(session, userSession, regUserInfoId, compWebpageMonitorToolTemp,Calendar.getInstance().getTime());}//oldUserdate=Calendar.getInstance().getTime();*/
 /*Naviagtion to SuccessView()*/
                            ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
                            if (prepareDashBoard) {
                                mAndV.addObject("result", "show");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "true");
                                return mAndV;
                            } else {
                                session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
                                if (cmpWebpageMonitor.getAlertFrequency() == 1) {
                                    session.removeAttribute("frequency");
                                    session.setAttribute("frequency", "Days");
                                } else {
                                    session.setAttribute("frequency", "Weeks");
                                }
                                mAndV.addObject("result", "hide");
                                mAndV.addObject("settings", "show");
                                mAndV.addObject("googleCharts", "false");
                                mAndV.addObject("Mailsent", "We are unable to fetch your data for your query URL: " + cmpWebpageMonitor.getCompetitorUrl());
                                return mAndV;
                            }
                        }
                    }

                }
            }

        }
        cmpWebpageMonitor = new CompetitorWebpageMonitor();
        ModelAndView mAndV = new ModelAndView(getFormView(), "compWebpageMonitorTool", cmpWebpageMonitor);
        mAndV.addObject("result", "hide");
        mAndV.addObject("settings", "hide");
        mAndV.addObject("googleCharts", "false");
        return mAndV;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        CompetitorWebpageMonitor compWebpageMonitorTool = (CompetitorWebpageMonitor) session.getAttribute("compWebpageMonitorTool");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        Session userSession = (Session) session.getAttribute("userSession");
        if (compWebpageMonitorTool == null) {
            logger.info("user with id  :" + userSession.getUserId() + " is using.");
            if (userSession.getUserId() > 0 && !userLoggedIn) {
                compWebpageMonitorTool = competitorWebpageMonitorService.findUserData(userSession.getUserId());
            }
            if (compWebpageMonitorTool != null) {
                session.removeAttribute("cmpmonitor");
                session.setAttribute("cmpmonitor", "cmpmonitor");
            } else {
                compWebpageMonitorTool = new CompetitorWebpageMonitor();
            }
        }
        if (userSession.getUserId() > 0 && userLoggedIn) {
            CompetitorWebpageMonitor compWebpageMonitorSettings = competitorWebpageMonitorService.findUserData(userSession.getUserId());
            if (compWebpageMonitorSettings != null) {
                compWebpageMonitorTool.setToolUnsubscription(compWebpageMonitorSettings.isToolUnsubscription());
                compWebpageMonitorTool.setEmailUnsubscription(compWebpageMonitorSettings.isEmailUnsubscription());
                session.setAttribute("cmpSettingsData", compWebpageMonitorSettings);
            }
        }
        return compWebpageMonitorTool;
    }

    public boolean showDatatoUser(HttpSession session, Session userSession, long info_id, CompetitorWebpageMonitor cmpWebpageMonitor, Date fetchDate) {
        session.removeAttribute("linkResults");
        session.removeAttribute("imgResults");
        CmpSummary cmpSummaryInfo;
        cmpSummaryInfo = competitorWebpageMonitorService.getContentChanges(info_id, cmpWebpageMonitor, fetchDate);
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        if (fetchDate == null) {
            fetchDate = Calendar.getInstance().getTime();
        }
        String finaldateDay = sdf.format(fetchDate);
        Map<Integer, List<CmpLinksInfo>> linkResults = competitorWebpageMonitorService
                .getLinkDetailedResults(cmpWebpageMonitor, userSession.getUserId(), info_id, fetchDate);
        List<CmpLinksInfo> newLinksListTemp = null;
        if (linkResults != null && linkResults.size() > 0) {
            newLinksListTemp = linkResults.get(2);
            logger.info("The size of new temp links list:" + newLinksListTemp.size());
            session.setAttribute("linkResults", linkResults);
        }
        Map<Integer, List<CmpImgInfo>> imgResults = competitorWebpageMonitorService
                .getImgDetailedResults(cmpWebpageMonitor, userSession.getUserId(), info_id);
        session.setAttribute("imgResults", imgResults);
        session.removeAttribute("CmpSummaryInfo");
        session.removeAttribute("textChanges");
        if (cmpSummaryInfo != null) {
            cmpSummaryInfo.setResultDate(finaldateDay);
            session.setAttribute("CmpSummaryInfo", cmpSummaryInfo);
            logger.debug("textchanges  :" + cmpSummaryInfo.getBodyText());
            String cntChanges = cmpSummaryInfo.getBodyText().replaceAll("©", "&copy;").replaceAll("\"", "\\\\\"").replaceAll("\'", "\\\\\'");
            if (cntChanges.trim().length() != 0) {
                session.setAttribute("textChanges", cntChanges.trim());
            }
            return true;
        }
        return false;
    }

    public void sendMailToUser(Session userSession, CompetitorWebpageMonitor cmpWebpageMonitor, long infoId, Login user) {
        userSession.getUserId();
        String subject = EmailBodyCreator.subjectOnCompetitorWebpageManitor;
        String bodyText = EmailBodyCreator.bodyForCompetitorWebpageManitor(cmpWebpageMonitor.getCompetitorUrl());
        SendMail.sendMail("", cmpWebpageMonitor.getEmail(), subject, bodyText);
        CmpSummary cmpSummary = new CmpSummary();
        int[] val = {0};
        competitorWebpageMonitorService.getCmpResults(cmpWebpageMonitor.getCompetitorUrl(), infoId, user.getId(), cmpSummary, val);
    }

}
