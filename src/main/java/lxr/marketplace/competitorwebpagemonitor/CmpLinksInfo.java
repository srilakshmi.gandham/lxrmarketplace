package lxr.marketplace.competitorwebpagemonitor;

import org.apache.log4j.Logger;

public class CmpLinksInfo implements Comparable<CmpLinksInfo>{
	private static Logger logger = Logger.getLogger(CmpLinksInfo.class);
//	long userId;
//    String finalUrl;
	long infoId;
	String ancLink;
    String anchorText;
    String linkDesc;
	long linkId;
	
	public long getInfoId() {
		return infoId;
	}
	public void setInfoId(long infoId) {
		this.infoId = infoId;
	}
	public String getAncLink() {
		return ancLink;
	}
	public void setAncLink(String ancLink) {
		this.ancLink = ancLink;
	}
	public long getLinkId() {
		return linkId;
	}
	public void setLinkId(long linkId) {
		this.linkId = linkId;
	}
	public String getAnchorText() {
		return anchorText;
	}
	public void setAnchorText(String anchorText) {
		this.anchorText = anchorText;
	}
	public String getLinkDesc() {
		return linkDesc;
	}
	public void setLinkDesc(String linkDesc) {
		this.linkDesc = linkDesc;
	}
	
    public int compareTo(CmpLinksInfo cmp) {
    	//logger.info("ancLInk"+ancLink+" "+cmp.getAncLink()+"anchortext :"+anchorText+" "+cmp.getAnchorText());
//	  if(ancLink.equalsIgnoreCase(cmp.getAncLink()) && anchorText.equalsIgnoreCase(cmp.getAnchorText()) || linkDesc.equalsIgnoreCase(cmp.getLinkDesc())){
 	  if(ancLink.trim().equalsIgnoreCase(cmp.getAncLink().trim()) && anchorText.trim().equalsIgnoreCase(cmp.getAnchorText().trim())){
    	return 0;
	  }else{
		return 1;	
	   }
     }
}
