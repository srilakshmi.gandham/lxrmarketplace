package lxr.marketplace.competitorwebpagemonitor;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class CmpUserInfoTask implements Runnable {

    private static Logger logger = Logger.getLogger(CmpUserInfoTask.class);

    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.ctx = ctx;
    }
    private boolean terminated = false;
    private final long infoId;
    private final long userId;
    private final String cmpUrl;
    private final String email;
    private final boolean emailUnsubscription;

    public CmpUserInfoTask(long infoId, long userId, String cmpUrl, String email, boolean emailUnsubscription) {
        this.infoId = infoId;
        this.userId = userId;
        this.cmpUrl = cmpUrl;
        this.email = email;
        this.emailUnsubscription = emailUnsubscription;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public JdbcTemplate getJdbcTemplate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void run() {
        try {
            logger.info("********** fetching webpage info started for user **************" + email + "  " + cmpUrl);
            CompetitorWebpageMonitorService cmpSer = (CompetitorWebpageMonitorService) ctx.getBean("competitorWebpageMonitorService");
            CmpSummary cmpSummary = new CmpSummary();
            int val[] = {0};
            cmpSer.getCmpResults(cmpUrl, infoId, userId, cmpSummary, val);
            if ((cmpSummary.getBodyText() != null && cmpSummary.getBodyText().length() > 0) || cmpSummary.getNewUrls() > 0
                    || cmpSummary.getNewWords() > 0 || cmpSummary.getRemovedWords() > 0 || cmpSummary.getRemovedUrls() > 0 || val[0] == 1) {
                SendMail sendmail = new SendMail();
                if (!emailUnsubscription) {
                    String subject = EmailBodyCreator.subjectOnCompetitorWebpageManitor;
                    String bodyText = EmailBodyCreator.bodyForCompetitorWebpageManitorCmpInfo(infoId);
                    SendMail.sendMail("", email, subject, bodyText);
                }
            }
            logger.info("********** fetching webpage info completed for user **************" + email + "  " + cmpUrl);
        } catch (Exception e) {
            logger.error(e);
        }
    }

}
