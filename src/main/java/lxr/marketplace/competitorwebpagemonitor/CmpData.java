package lxr.marketplace.competitorwebpagemonitor;

import java.sql.Timestamp;

public class CmpData {
    Timestamp startDate;
    long info_id;
    long content_id;
    int content_type;
    
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public long getInfo_id() {
		return info_id;
	}
	public void setInfo_id(long info_id) {
		this.info_id = info_id;
	}
	public long getContent_id() {
		return content_id;
	}
	public void setContent_id(long content_id) {
		this.content_id = content_id;
	}
	public int getContent_type() {
		return content_type;
	}
	public void setContent_type(int content_type) {
		this.content_type = content_type;
	}
    
    
}
