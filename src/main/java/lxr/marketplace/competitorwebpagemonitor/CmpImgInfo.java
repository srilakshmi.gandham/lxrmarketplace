package lxr.marketplace.competitorwebpagemonitor;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.Serializable;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

public class CmpImgInfo implements Comparable<CmpImgInfo>,Serializable {
	private static Logger logger = Logger.getLogger(CmpImgInfo.class);
	long imgId;
	String imgSrc;
	String altText;
	String imgPath;
	String imgName;
    double imgHeight;
    double imgWidth;
//	String imgFolder = "/opt/lxrmarketplace/cmp_images/";
	
   public double getImgHeight() {
		return imgHeight;
	}
	public void setImgHeight(double imgHeight) {
		this.imgHeight = imgHeight;
	}
	public double getImgWidth() {
		return imgWidth;
	}
	public void setImgWidth(double imgWidth) {
		this.imgWidth = imgWidth;
	}
	public long getImgId() {
		return imgId;
	}
	public void setImgId(long imgId) {
		this.imgId = imgId;
	}
	public String getImgSrc() {
		return imgSrc;
	}
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public String getImgName() {
		return imgName;
	}
	public void setImgName(String imgName) {
		this.imgName = imgName;
	}
//	@Override
	public int compareTo(CmpImgInfo cmp) {	
		if(imgSrc.trim().equalsIgnoreCase(cmp.getImgSrc().trim()) && altText.trim().equalsIgnoreCase(cmp.getAltText().trim())){
//			if(compareImage(new File(imgPath),new File(cmp.getImgPath())))
	           return 0;
		}
			return 1;
	}
  
	public  boolean compareImage(File fileA, File fileB) {        
		   try {
		       // take buffer data from botm image files //
		       BufferedImage biA = ImageIO.read(fileA);
		       DataBuffer dbA = biA.getData().getDataBuffer();
		       int sizeA = dbA.getSize();                      
		       BufferedImage biB = ImageIO.read(fileB);
		       DataBuffer dbB = biB.getData().getDataBuffer();
		       int sizeB = dbB.getSize();
		       // compare data-buffer objects //
		       if(sizeA == sizeB) {
		           for(int i=0; i<sizeA; i++) { 
		               if(dbA.getElem(i) != dbB.getElem(i)) {
		                   return false;
		               }
		           }
		           return true;
		       }
		       else {
		           return false;
		       }
		   } 
		   catch (Exception e) { 
			   e.printStackTrace();
		     logger.info("Failed to compare image files ...");
		       return  false;
		   }
		}
}
