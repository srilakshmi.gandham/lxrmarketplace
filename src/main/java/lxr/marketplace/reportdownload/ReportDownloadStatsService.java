package lxr.marketplace.reportdownload;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ReportDownloadStatsService extends JdbcDaoSupport{

    public long fetchReportDownloadCount(long userId, long reportId){
        long reportCount = 0;
        String query = "select sum(count) from lxr_report_downloads where report_id = " + reportId 
                + " and session_id in (select id from session where user_id = " + userId + ")";
        logger.debug("Report Count Query: " + query)
;        reportCount = getJdbcTemplate().queryForLong(query);
        return reportCount;
    }
}
