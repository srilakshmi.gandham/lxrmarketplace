package lxr.marketplace.reportdownload;


public class UserReportDownloadStats {

    private long id;
    private long sessionId;
    private long reportId;
    private int count;
    
    
    public UserReportDownloadStats(long reportId) {
        super();
        this.reportId = reportId;
        count = 0;
    }
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public long getSessionId() {
        return sessionId;
    }
    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }
    public long getReportId() {
        return reportId;
    }
    public void setReportId(long reportId) {
        this.reportId = reportId;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    
    public void addReportDownload(){
        count++;
    }
}
