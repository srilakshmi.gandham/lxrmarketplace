/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.structuredata;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author netelixir
 */
@Service
public class StructureDataService {

    private static Logger logger = Logger.getLogger(StructureDataService.class);

    @Autowired
    ApplicationContext context;

    public Map<String, List<String>> convertStringToMap(String inputString) {
        String[] splited = inputString.split(";");
        Map<String, List<String>> proMap = new LinkedHashMap();
        List<String> valueList = null;
        for (String property : splited) {
            if (property.contains(":")) {
                String[] keyValue = property.split(":");
                valueList = new ArrayList();
                if (keyValue[1].contains(",")) {
                    String[] subValueList = keyValue[1].split(",");
                    valueList.addAll(Arrays.asList(subValueList));
                    proMap.put(keyValue[0], valueList);
                } else {
                    valueList.add(keyValue[1]);
                    proMap.put(keyValue[0], valueList);
                }
            } else {
                proMap.put(property, null);
            }
        }
        return proMap;
    }

    public Map<String, String> convertRequestParamsToMap(HttpServletRequest request) {
        String queryParams = request.getQueryString();
        String[] parts = queryParams.split("&");
        Map<String, String> reqTypeParamsMap = new HashMap();
        for (String part : parts) {
            String[] singleParam = part.split("=");
            if (singleParam[0].contains("%40")) {
                try {
                    singleParam[0] = html2text(singleParam[0]);
                } catch (UnsupportedEncodingException ex) {
                    logger.error("Exception when converting html to text ", ex);
                }
            }
            if (!request.getParameter(singleParam[0]).trim().equals("")) {
                reqTypeParamsMap.put(singleParam[0], request.getParameter(singleParam[0]));
            }
        }
        return reqTypeParamsMap;
    }

// This function is for generating the "JSON-LD" Structure data based on markup type
    public JSONObject convertMapToJsonObject(Map<String, List<String>> markupTypeParamsMap, Map<String, String> reqTypeParamsMap) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("@context", "http://schema.org/");
            jsonObject.put("@type", context.getMessage("type." + reqTypeParamsMap.get("markupType"), null, Locale.UK));
            for (Map.Entry<String, List<String>> entry : markupTypeParamsMap.entrySet()) {
                String key = entry.getKey();
                List<String> value = entry.getValue();
                if (value == null) {
                    if (reqTypeParamsMap.get(key) != null && reqTypeParamsMap.get(key).contains("\n")) {
                        if (key.equalsIgnoreCase("description")) {
                            String description = reqTypeParamsMap.get(key).replaceAll("\n", " ").replaceAll("\r", "");
                            jsonObject.put(key, description);
                        } else {
                            ArrayList<String> valueList = new ArrayList<>();
                            valueList.addAll(Arrays.asList(reqTypeParamsMap.get(key).split("\n")));
                            JSONArray jsonArray = new JSONArray();
                            for (int i = 0; i < valueList.size(); i++) {
                                jsonArray.add(valueList.get(i).replaceAll("\r", ""));
                            }
                            jsonObject.put(key, jsonArray);
                        }
                    } else {
                        jsonObject.put(key, reqTypeParamsMap.get(key));
                    }
                } else if (value.size() > 1 || (value.size() == 1 && key.equalsIgnoreCase("jobLocation"))) {
                    JSONObject tempObj = new JSONObject();
                    for (String string : value) {
                        if (string.contains("->")) {
                            String[] keyValue = string.split("->");
                            JSONObject subKeyJsonObj = new JSONObject();
                            if (keyValue[1].contains("-")) {
                                if (keyValue[0].contains(".")) {
                                    subKeyJsonObj.put("@type", context.getMessage("type." + keyValue[0].split("\\.")[1], null, Locale.UK));
                                } else {
                                    subKeyJsonObj.put("@type", context.getMessage("type." + keyValue[0], null, Locale.UK));
                                }
                                String[] subKeyValue = keyValue[1].split("-");
                                for (int i = 0; i < subKeyValue.length; i++) {
                                    if (subKeyValue[i].contains(".")) {
                                        if ((subKeyValue[i].contains("width") || subKeyValue[i].contains("height")
                                                || subKeyValue[i].contains("latitude") || subKeyValue[i].contains("longitude") //                                                || subKeyValue[i].contains("minValue") || subKeyValue[i].contains("maxValue") 
                                                //                                                || subKeyValue[i].contains("baseSalary.value")
                                                ) && reqTypeParamsMap.get(subKeyValue[i]) != null) {
                                            subKeyJsonObj.put(subKeyValue[i].split("\\.")[1], Long.parseLong(reqTypeParamsMap.get(subKeyValue[i])));
                                        } else {
                                            subKeyJsonObj.put(subKeyValue[i].split("\\.")[1], reqTypeParamsMap.get(subKeyValue[i]));
                                        }
                                    } else if ((subKeyValue[i].contains("width") || subKeyValue[i].contains("height")
                                            || subKeyValue[i].contains("latitude") || subKeyValue[i].contains("longitude") //                                            || subKeyValue[i].contains("minValue") || subKeyValue[i].contains("maxValue") 
                                            //                                            || subKeyValue[i].contains("baseSalary.value")
                                            ) && reqTypeParamsMap.get(subKeyValue[i]) != null) {
                                        subKeyJsonObj.put(subKeyValue[i], Long.parseLong(reqTypeParamsMap.get(subKeyValue[i])));
                                    } else {
                                        subKeyJsonObj.put(subKeyValue[i], reqTypeParamsMap.get(subKeyValue[i]));
                                    }
                                }
                                if (subKeyJsonObj.size() > 1) {
                                    if (keyValue[0].contains(".")) {
                                        tempObj.put(keyValue[0].split("\\.")[1], subKeyJsonObj);
                                    } else {
                                        tempObj.put(keyValue[0], subKeyJsonObj);
                                    }
                                }
                            } else if (keyValue[0].contains(".")) {
                                if ((keyValue[0].contains("width") || keyValue[0].contains("height")
                                        || keyValue[0].contains("latitude") || keyValue[0].contains("longitude") //                                        || keyValue[0].contains("minValue") || keyValue[0].contains("maxValue") 
                                        //                                        || keyValue[0].contains("baseSalary.value")
                                        ) && reqTypeParamsMap.get(keyValue[0]) != null) {
                                    tempObj.put(keyValue[0].split("\\.")[1], Long.parseLong(reqTypeParamsMap.get(keyValue[0])));
                                } else {
                                    tempObj.put(keyValue[0].split("\\.")[1], reqTypeParamsMap.get(keyValue[0]));
                                }
                            } else {
                                tempObj.put(keyValue[0], reqTypeParamsMap.get(keyValue[0]));
                            }
                        } else {
                            if (key.contains(".")) {
                                tempObj.put("@type", context.getMessage("type." + key.split("\\.")[1], null, Locale.UK));
                            } else {
                                tempObj.put("@type", context.getMessage("type." + key, null, Locale.UK));
                            }
                            if (string.equalsIgnoreCase("target") && reqTypeParamsMap.get(string) != null) {
                                tempObj.put(string, reqTypeParamsMap.get(string) + "{" + reqTypeParamsMap.get("query-input") + "}");
                            } else if (string.equalsIgnoreCase("query-input") && reqTypeParamsMap.get(string) != null) {
                                tempObj.put(string, "required name=" + reqTypeParamsMap.get(string));
                            } else if (string.contains(".")) {
                                if ((string.contains("width") || string.contains("height")
                                        || string.contains("latitude") || string.contains("longitude") //                                        || string.contains("minValue") || string.contains("maxValue")
                                        //                                        || string.contains("baseSalary.value")
                                        ) && reqTypeParamsMap.get(string) != null) {
                                    tempObj.put(string.split("\\.")[1], Long.parseLong(reqTypeParamsMap.get(string)));
                                } else {
                                    tempObj.put(string.split("\\.")[1], reqTypeParamsMap.get(string));
                                }
                            } else {
                                tempObj.put(string, reqTypeParamsMap.get(string));
                            }
                        }
                    }
                    if (tempObj.size() > 1 || (value.size() == 1 && key.equalsIgnoreCase("jobLocation"))) {
                        jsonObject.put(key, tempObj);
                    }
                } else {
                    JSONObject tempObj = new JSONObject();
                    if (key.contains(".")) {
                        tempObj.put("@type", context.getMessage("type." + key.split("\\.")[1], null, Locale.UK));
                    } else {
                        tempObj.put("@type", context.getMessage("type." + key, null, Locale.UK));
                    }
                    if (value.get(0).contains(".")) {
                        tempObj.put(value.get(0).split("\\.")[1], reqTypeParamsMap.get(value.get(0)));
                    } else {
                        tempObj.put(value.get(0), reqTypeParamsMap.get(value.get(0)));
                    }
                    if (tempObj.size() > 1) {
                        jsonObject.put(key, tempObj);
                    }
                }
            }
            if (reqTypeParamsMap.get("markupType").equals("localBusiness")) {
                String[] days = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
                String openingHours = "";
                for (String day : days) {
                    if (reqTypeParamsMap.containsKey("openingHours" + day)
                            && reqTypeParamsMap.containsKey("opens" + day)
                            && reqTypeParamsMap.containsKey("close" + day)) {
                        if (!openingHours.equals("")) {
                            openingHours += ", ";
                        }
                        openingHours += reqTypeParamsMap.get("openingHours" + day) + " " + reqTypeParamsMap.get("opens" + day) + "-" + reqTypeParamsMap.get("close" + day);
                    } else if (reqTypeParamsMap.containsKey("openingHours" + day)) {
                        if (!openingHours.equals("")) {
                            openingHours += ", ";
                        }
                        openingHours += reqTypeParamsMap.get("openingHours" + day);
                    }
                }
                if (!openingHours.equalsIgnoreCase("")) {
                    jsonObject.put("openingHours", openingHours);
                }
            }
            return jsonObject;
        } catch (JSONException e) {
            logger.error("Exception while preparing json object: " + e);
        }
        return null;
    }

// This function is for generating the "Microdata" structure data based on markup type
    public String generateMicrodataUsingMaps(Map<String, List<String>> markupTypeParamsMap, Map<String, String> reqTypeParamsMap, String markupType) {
        String microData = "<div itemscope itemtype=\"http://schema.org/" + context.getMessage("type." + reqTypeParamsMap.get("markupType"), null, Locale.UK) + "\">\n";
        for (Map.Entry<String, List<String>> entry : markupTypeParamsMap.entrySet()) {
            String key = entry.getKey();
            List<String> value = entry.getValue();
            if (value == null) {
                if (reqTypeParamsMap.get(key) != null && !reqTypeParamsMap.get(key).equals("")) {
                    String inputTag = context.getMessage("md." + markupType + "." + key, null, Locale.UK);
                    microData += getHtmlTagforInput(inputTag, key, reqTypeParamsMap.get(key));
                }
            } else if (value.size() > 1 || (value.size() == 1 && key.equalsIgnoreCase("jobLocation"))) {
//                String inputTag = "";
                String tempString = "";
                for (String string : value) {
                    if (string.contains("->")) {
                        String[] keyValue = string.split("->");
                        String subKeyString = "";
                        if (keyValue[1].contains("-")) {
                            if (markupType.equalsIgnoreCase("article")
                                    && (keyValue[0].contains("logo") || keyValue[0].contains("image"))) {
                                if (keyValue[0].contains(".")) {
                                    subKeyString += "         <img src=\"" + reqTypeParamsMap.get(keyValue[0].split("\\.")[1] + ".url") + "\"/>\n";
                                } else {
                                    subKeyString += "         <img src=\"" + reqTypeParamsMap.get(keyValue[0]) + ".url" + "\"/>\n";
                                }
                            }
                            String[] subKeyValues = keyValue[1].split("-");
                            for (String subKeyValue : subKeyValues) {
                                subKeyString += "   " + getSubProropertyHtmlTag(subKeyValue, reqTypeParamsMap.get(subKeyValue), markupType);
                            }
                            if (!subKeyString.equals("")) {
                                tempString += "   " + getProropertyHtmlTag(subKeyString, keyValue[0], markupType);
                            }
                        } else {
                            if (reqTypeParamsMap.get(keyValue[1]) != null) {
                                subKeyString += "   " + getSubProropertyHtmlTag(keyValue[1], reqTypeParamsMap.get(keyValue[1]), markupType);
                            }
                            if (!subKeyString.trim().equals("")) {
                                tempString += "   " + getProropertyHtmlTag(subKeyString, keyValue[0], markupType);
                            }
                        }
                    } else if (string.equalsIgnoreCase("target") && reqTypeParamsMap.get(string) != null) {
                        tempString += getSubProropertyHtmlTag(string, reqTypeParamsMap.get(string) + "{" + reqTypeParamsMap.get("query-input") + "}", markupType);
                    } else if (string.equalsIgnoreCase("query-input") && reqTypeParamsMap.get(string) != null) {
                        tempString += getSubProropertyHtmlTag(string, reqTypeParamsMap.get(string), markupType);
                    } else if (reqTypeParamsMap.get(string) != null) {
                        tempString += getSubProropertyHtmlTag(string, reqTypeParamsMap.get(string), markupType);
                    }
                }
                if (!tempString.equals("")) {
                    String imageTag = "";
                    if (markupType.equalsIgnoreCase("article")
                            && (key.contains("logo") || key.contains("image"))) {
                        if (key.contains(".")) {
                            imageTag += "      <img itemprop=\"" + key.split("\\.")[1] + "\" src=\"" + reqTypeParamsMap.get(key.split("\\.")[1] + ".url") + "\" />";
                        } else {
                            imageTag += "      <img itemprop=\"" + key + "\" src=\"" + reqTypeParamsMap.get(key + ".url") + "\" />";
                        }
                        tempString = imageTag + "\n" + tempString;
                    }
                    tempString = getProropertyHtmlTag(tempString, key, markupType);
                }
                microData += tempString;
            } else {
                String tempString = "";
                tempString += getSubProropertyHtmlTag(value.get(0), reqTypeParamsMap.get(value.get(0)), markupType);
                if (!tempString.equals("")) {
                    tempString = getProropertyHtmlTag(tempString, key, markupType);
                }
                microData += tempString;
            }
        }
        if (reqTypeParamsMap.get("markupType").equals("localBusiness")) {
            String[] days = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
            String openingHours = "";
            for (String day : days) {
                if (reqTypeParamsMap.containsKey("openingHours" + day) && reqTypeParamsMap.containsKey("opens" + day)
                        && reqTypeParamsMap.containsKey("close" + day)) {
                    String dayWise = reqTypeParamsMap.get("openingHours" + day) + " " + reqTypeParamsMap.get("opens" + day) + " - " + reqTypeParamsMap.get("close" + day);
                    openingHours += "   <time itemprop=\"openingHours\" datetime=\"" + dayWise + "\">" + dayWise + "</time>\n";
                } else if (reqTypeParamsMap.containsKey("openingHours" + day)) {
                    openingHours += "   <time itemprop=\"openingHours\" datetime=\"" + day + "\">" + day + "</time>\n";
                }
            }
            microData += openingHours;
        }
        microData += "</div>";
        return microData;
    }

    public String getSubProropertyHtmlTag(String property, String propertyValue, String markupType) {
        String subString = "";
        String inputTag;
        if (propertyValue != null && !propertyValue.equals("")) {
            if (property.equalsIgnoreCase("target")) {
                inputTag = context.getMessage("md." + markupType + "." + property, null, Locale.UK);
                subString = "   " + getHtmlTagforInput(inputTag, property, propertyValue);
            } else if (property.equalsIgnoreCase("query-input")) {
                inputTag = context.getMessage("md." + markupType + "." + property, null, Locale.UK);
                subString = "   " + getHtmlTagforInput(inputTag, property, propertyValue);
            } else if (property.contains(".")) {
                inputTag = context.getMessage("md." + markupType + "." + property.split("\\.")[1], null, Locale.UK);
                subString = "   " + getHtmlTagforInput(inputTag, property.split("\\.")[1], propertyValue);
            } else {
                inputTag = context.getMessage("md." + markupType + "." + property, null, Locale.UK);
                subString = "   " + getHtmlTagforInput(inputTag, property, propertyValue);
            }
        }
        return subString;
    }

    public String getProropertyHtmlTag(String subString, String property, String markupType) {
        String tempString = "";
        if (!subString.equals("")) {
            String inputTag = "";
            String itemtype = "";
            if (property.contains(".")) {
                itemtype = context.getMessage("type." + property.split("\\.")[1], null, Locale.UK);
                inputTag = context.getMessage("md." + markupType + "." + property.split("\\.")[1], null, Locale.UK);
                tempString = "   <" + inputTag + " itemprop=\"" + property.split("\\.")[1] + "\" itemscope itemtype=\"http://schema.org/" + itemtype + "\"" + ">\n";
            } else {
                itemtype = context.getMessage("type." + property, null, Locale.UK);
                inputTag = context.getMessage("md." + markupType + "." + property, null, Locale.UK);
                tempString = "   <" + inputTag + " itemprop=\"" + property + "\" itemscope itemtype=\"http://schema.org/" + itemtype + "\"" + ">\n";
            }
            tempString += subString + "\n";
            tempString += "   </" + inputTag + ">\n";
        }
        return tempString;
    }

    public String getHtmlTagforInput(String inputTag, String property, String propertyValue) {
        String htmlTag = "";
        if (propertyValue.contains("\n")) {
            if (property.equalsIgnoreCase("description")) {
                String description = propertyValue.replaceAll("\n", " ").replaceAll("\r", "");
                htmlTag += "<" + inputTag + " itemprop=\"" + property + "\">" + description + "</" + inputTag + ">";
            } else {
                ArrayList<String> valueList = new ArrayList<>();
                valueList.addAll(Arrays.asList(propertyValue.split("\n")));
                for (int i = 0; i < valueList.size(); i++) {
                    switch (inputTag) {
                        case "meta":
                            htmlTag += "<meta" + " itemprop=\"" + property + "\" content=\"" + valueList.get(i) + "\" />";
                            break;
                        case "link":
                            htmlTag += "<link" + " itemprop=\"" + property + "\" href=\"" + valueList.get(i) + "\" />";
                            break;
                        case "img":
                            htmlTag += "<img" + " itemprop=\"" + property + "\" src=\"" + valueList.get(i) + "\"  alt=\"image text\" />";
                            break;
                        case "input":
                            htmlTag += "<input" + " itemprop=\"" + property + "\" type=\"text\" name=\"" + valueList.get(i) + "\" required/>";
                            break;
                        default:
                            htmlTag += "<" + inputTag + " itemprop=\"" + property + "\">" + valueList.get(i) + "</" + inputTag + ">";
                            break;
                    }
                    if (i < valueList.size() - 1) {
                        htmlTag += "\n";
                    }
                }
            }
        } else {
            switch (inputTag) {
                case "meta":
                    htmlTag = "<meta" + " itemprop=\"" + property + "\" content=\"" + propertyValue + "\" />";
                    break;
                case "link":
                    htmlTag = "<link" + " itemprop=\"" + property + "\" href=\"" + propertyValue + "\" />";
                    break;
                case "time":
                    if (propertyValue.contains(":")) {
                        String[] dateTime = propertyValue.split(":");
                        htmlTag = "<time" + " datetime=\"PT" + dateTime[0] + "H" + dateTime[1] + "M\" itemprop=\"" + property + "\">" + propertyValue + "</" + inputTag + ">";
                    } else {
                        htmlTag = "<time" + " datetime=\"" + propertyValue + "\" itemprop=\"" + property + "\">" + propertyValue + "</" + inputTag + ">";
                    }
                    break;
                case "img":
                    htmlTag = "<img" + " itemprop=\"" + property + "\" src=\"" + propertyValue + "\"  alt=\"image text\" />";
                    break;
                case "input":
                    if (property.equalsIgnoreCase("query-input")) {
                        htmlTag += "<input" + " itemprop=\"" + property + "\" type=\"text\" name=\"" + propertyValue + "\" required/>";
                        htmlTag += "<input type=\"submit\"/>";
                    } else {
                        htmlTag += "<input" + " itemprop=\"" + property + "\" type=\"text\" name=\"" + propertyValue + "\" required/>";
                    }
                    break;
                default:
                    htmlTag = "<" + inputTag + " itemprop=\"" + property + "\">" + propertyValue + "</" + inputTag + ">";
                    break;
            }
        }
        return "   " + htmlTag + "\n";
    }

    public String html2text(String html) throws UnsupportedEncodingException {
        return Jsoup.parse(URLDecoder.decode(html, "UTF-8")).text();
    }
}
