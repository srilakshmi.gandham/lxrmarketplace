/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.structuredata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author netelixir
 */
@Controller
@RequestMapping("/structured-data-generator.html")
public class StructureDataController {

    private static Logger logger = Logger.getLogger(StructureDataController.class);

    @Autowired
    private ApplicationContext context;
    @Autowired
    private StructureDataService structureDataService;
    @Autowired
    ToolService toolService;
    @Autowired
    EmailTemplateService emailTemplateService;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @RequestMapping(method = RequestMethod.GET)
    public String getStructureDataPage(HttpSession session) {
        session.removeAttribute("reqTypeParamsMap");
        return "views/structuredData/structuredData";
    }

    @RequestMapping(params = {"clear=clear"})
    public String clear(HttpSession session) {
        session.removeAttribute("reqTypeParamsMap");
        return "views/structuredData/structuredData";
    }

    /**
     *
     * @param session
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(params = {"formatType=json-ld"})
    @ResponseBody
    public Object[] getJsonDataOfStructureData(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        Object[] responseData = new Object[2];
        Login user = (Login) session.getAttribute("user");
        try {
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(37);
                toolObj.setToolName(Common.STRUCTURED_DATA_GENERATOR);
                toolObj.setToolLink("structured-data-generator.html");
                session.setAttribute("toolObj", toolObj);
                Map<String, String> reqTypeParamsMap = structureDataService.convertRequestParamsToMap(request);
                session.setAttribute("reqTypeParamsMap", reqTypeParamsMap);
                String markupTypeParams = context.getMessage("markupType." + request.getParameter("markupType"), null, Locale.UK);
                Map<String, List<String>> markupTypeParamsMap = structureDataService.convertStringToMap(markupTypeParams);
                JSONObject jsonObject = structureDataService.convertMapToJsonObject(markupTypeParamsMap, reqTypeParamsMap);
                responseData[0] = "json-ld";
                responseData[1] = jsonObject;
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
            }
        } catch (JSONException e) {
            logger.error("In Structure data generator, while constructing json-ld data", e);
        }
        return responseData;
    }

    /**
     *
     * @param session
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(params = {"formatType=microdata"})
    @ResponseBody
    public Object[] getMicroDataOfStructureData(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        Object[] responseData = new Object[2];
        Login user = (Login) session.getAttribute("user");
        try {
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(37);
                toolObj.setToolName(Common.STRUCTURED_DATA_GENERATOR);
                toolObj.setToolLink("structured-data-generator.html");
                session.setAttribute("toolObj", toolObj);
                Map<String, String> reqTypeParamsMap = structureDataService.convertRequestParamsToMap(request);
                session.setAttribute("reqTypeParamsMap", reqTypeParamsMap);
                String markupType = request.getParameter("markupType");
                String markupTypeParams = context.getMessage("markupType." + request.getParameter("markupType"), null, Locale.UK);
                Map<String, List<String>> markupTypeParamsMap = structureDataService.convertStringToMap(markupTypeParams);
                String microData = structureDataService.generateMicrodataUsingMaps(markupTypeParamsMap, reqTypeParamsMap, markupType);
                responseData[0] = "microdata";
                responseData[1] = microData;
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
            }
        } catch (JSONException e) {
            logger.error("In Structure data generator, while constructing microdata", e);
        }
        return responseData;
    }

    /**
     *
     * @param session
     * @param request
     * @param response
     * @param name
     * @param item
     * @return
     */
    @RequestMapping(params = {"markupType=breadcrumbs", "formatType"})
    @ResponseBody
    public Object[] getBreadcrumbsStructureData(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        Object[] responseData = new Object[2];
        Login user = (Login) session.getAttribute("user");
        String name1 = request.getParameter("name1");
        String name2 = request.getParameter("name2");
        String name3 = request.getParameter("name3");
//        String name4 = request.getParameter("name4");
        String item1 = request.getParameter("item1");
        String item2 = request.getParameter("item2");
        String item3 = request.getParameter("item3");
//        String item4 = request.getParameter("item4");
        String position1 = request.getParameter("position1");
        String position2 = request.getParameter("position2");
        String position3 = request.getParameter("position3");
//        String position4 = request.getParameter("position4");
        try {
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(37);
                toolObj.setToolName(Common.STRUCTURED_DATA_GENERATOR);
                toolObj.setToolLink("structured-data-generator.html");
                session.setAttribute("toolObj", toolObj);
                String formatType = request.getParameter("formatType");
                List<String> namesList = new ArrayList<>();
                namesList.add(name1);
                namesList.add(name2);
                if (name3 != null) {
                    namesList.add(name3);
//                    namesList.add(name4);
                }
                List<String> itemList = new ArrayList<>();
                itemList.add(item1);
                itemList.add(item2);
                if (item3 != null) {
                    itemList.add(item3);
                }
                List<String> positionList = new ArrayList<>();
                positionList.add(position1);
                positionList.add(position2);
                positionList.add(position3);
                if (position3 != null) {
                    positionList.add(position3);
                }
                Map<String, String> reqTypeParamsMap = new HashMap<>();
                reqTypeParamsMap.put("formatType", formatType);
                reqTypeParamsMap.put("markupType", "breadcrumbs");
                reqTypeParamsMap.put("level", request.getParameter("level"));
                if (formatType.equalsIgnoreCase("json-ld")) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("@context", "http://schema.org");
                    jsonObject.put("@type", "BreadcrumbList");
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < namesList.size(); i++) {
//                        JSONObject jsonObj = new JSONObject();
                        JSONObject itemJsonObj = new JSONObject();
                        itemJsonObj.put("@type", "ListItem");
                        itemJsonObj.put("position", positionList.get(i));
                        reqTypeParamsMap.put("position" + i, positionList.get(i));
                        itemJsonObj.put("name", namesList.get(i));
                        reqTypeParamsMap.put("name" + i, namesList.get(i));
//                        if (i < namesList.size() - 1) {
                        itemJsonObj.put("item", itemList.get(i));
                        reqTypeParamsMap.put("item" + i, itemList.get(i));
//                        }
//                        jsonObj.put("@type", "ListItem");
//                        jsonObj.put("item", itemJsonObj);
                        jsonArray.add(itemJsonObj);
                    }
                    jsonObject.put("itemListElement", jsonArray);
                    responseData[0] = "json-ld";
                    responseData[1] = jsonObject;
                } else if (formatType.equalsIgnoreCase("microdata")) {
                    String microdata = "<ol itemscope itemtype=\"http://schema.org/BreadcrumbList\">\n";
                    int level = namesList.size();
                    for (int i = 0; i < level; i++) {
                        microdata += "   <li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">\n";
//                        if (i < namesList.size() - 1) {
                        microdata += "      <a itemtype=\"http://schema.org/Product\" itemprop=\"item\" href=\"" + itemList.get(i) + "\">\n"
                                + "         <span itemprop=\"name\" />" + namesList.get(i) + "<span>\n"
                                + "      </a>\n"
                                + "      <meta itemprop=\"position\" content=\"" + positionList.get(i) + "\" />\n";
                        reqTypeParamsMap.put("item" + i, itemList.get(i));
                        reqTypeParamsMap.put("name" + i, namesList.get(i));
                        reqTypeParamsMap.put("position" + i, positionList.get(i));
//                        } else {
//                            microdata += "      <span itemprop=\"name\" content=\"" + namesList.get(i) + "\" />\n";
//                            reqTypeParamsMap.put("name" + i, namesList.get(i));
//                        }
                        microdata += "   </li>\n";
//                        if (i < level - 1) {
//                            microdata += "   > \n";
//                        }
                    }
                    microdata += "</ol>";
                    responseData[0] = "microdata";
                    responseData[1] = microdata;
                }
                session.setAttribute("reqTypeParamsMap", reqTypeParamsMap);
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
            }
        } catch (JSONException e) {
            logger.error("In Structure data generator, while constructing breadcrumbs data", e);
        }
        return responseData;
    }

    /**
     *
     * @param session
     * @param request
     * @param response
     * @param name
     * @param ratingValue
     * @param formatType
     * @param ratingCount
     * @param reviewCount
     * @return
     */
    @RequestMapping(params = {"markupType=aggregateRating", "formatType"})
    @ResponseBody
    public Object[] getAggregateRating(HttpSession session, HttpServletRequest request, HttpServletResponse response,
            @RequestParam("name") String name, @RequestParam("ratingValue") String ratingValue,
            @RequestParam("formatType") String formatType, @RequestParam("ratingCount") String ratingCount,
            @RequestParam("reviewCount") String reviewCount) {
        Object[] responseData = new Object[2];
        Login user = (Login) session.getAttribute("user");
        try {
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(37);
                toolObj.setToolName(Common.STRUCTURED_DATA_GENERATOR);
                toolObj.setToolLink("structured-data-generator.html");
                session.setAttribute("toolObj", toolObj);
//                String formatType = request.getParameter("formatType");
                Map<String, String> reqTypeParamsMap = structureDataService.convertRequestParamsToMap(request);
                session.setAttribute("reqTypeParamsMap", reqTypeParamsMap);
                if (formatType.equalsIgnoreCase("json-ld")) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("@context", "http://schema.org/");
                    jsonObject.put("@type", "Product");
                    jsonObject.put("name", name);
                    JSONObject jsonsubObj = new JSONObject();
                    jsonsubObj.put("@type", "AggregateRating");
                    jsonsubObj.put("ratingValue", ratingValue);
                    jsonsubObj.put("ratingCount", ratingCount);
                    jsonsubObj.put("reviewCount", reviewCount);
                    jsonObject.put("aggregateRating", jsonsubObj);
                    responseData[0] = "json-ld";
                    responseData[1] = jsonObject;
                } else if (formatType.equalsIgnoreCase("microdata")) {
                    String aggRating = "<div itemscope itemtype=\"http://schema.org/Product\">\n"
                            + "   <span itemprop=\"name\">" + name + "</span><br>\n"
                            + "   <div itemprop=\"aggregateRating\" itemscope itemtype=\"http://schema.org/AggregateRating\">\n"
                            + "      <span itemprop=\"ratingValue\">" + ratingValue + "</span><br>\n"
                            + "      <span itemprop=\"ratingCount\">" + ratingCount + "</span><br>\n"
                            + "      <span itemprop=\"reviewCount\">" + reviewCount + "</span>\n"
                            + "   </div>\n"
                            + "</div>";
                    responseData[0] = "microdata";
                    responseData[1] = aggRating.trim();
                }
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
            }
        } catch (JSONException e) {
            logger.error("In Structure data generator, while constructing aggregateRating data", e);
        }
        return responseData;
    }

    @RequestMapping(params = {"ate=ate"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            Model model, @RequestParam("domain") String domain) {
        String toolIssues = "Want to get the full audit of your website on how Google will know about website?";
        String questionInfo = "- Auditing on-page SEO factors for my website.";
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain);
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.STRUCTURED_DATA_GENERATOR);
        lxrmAskExpertService.addInJson(lxrmAskExpert, response);
        return null;
    }

    @RequestMapping(params = {"backToSuccessPage"})
    public String backToSuccessPage(HttpSession session) {
        return "views/structuredData/structuredData";
    }
}
