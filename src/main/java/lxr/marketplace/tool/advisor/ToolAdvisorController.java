/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author vemanna
 */
@Controller
public class ToolAdvisorController {

    private static final Logger LOGGER = Logger.getLogger(ToolAdvisorController.class);

    @Autowired
    ToolAdvisorService toolAdvisorService;
    @Autowired
    ToolService toolService;

    @RequestMapping(value = "/tool-advisor.json", method = RequestMethod.GET)
    public @ResponseBody
    Object toolAdvisor(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam("domain") String domain) {

        Object[] data = new Object[4];
        String finalDoamin = Common.getUrlValidation(domain, session);
        LOGGER.info("Final domain: " + finalDoamin);
        if (finalDoamin.equals("invalid") || finalDoamin.equals("redirected")) {
            data[0] = 0;
            session.removeAttribute("topThreeTools");
            session.removeAttribute("toolAdvisorDomain");
            session.removeAttribute("topThreeToolIds");
        } else {
            Login user = (Login) session.getAttribute("user");
            List<String> stopWordsList = new ArrayList<>();
            WebApplicationContext context1 = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
            if (context1.getServletContext().getAttribute("stopwords") != null) {
                stopWordsList = (List<String>) context1.getServletContext().getAttribute("stopwords");
            }
            //Fetching tools usage count which are showing in tool advisor
            ToolsUsageCountDTO toolsUsageCountDTO = new ToolsUsageCountDTO();
            if (user.getId() != -1) {
                toolService.fetchingToolUsageForAdvisor(user.getId(), toolsUsageCountDTO);
            }
            List<Tool> topThreeTools = toolAdvisorService.fetchingTopThreeTools(toolsUsageCountDTO, session, stopWordsList, finalDoamin);
            data[0] = 1;
            data[1] = topThreeTools;
            List<Integer> topThreeToolIds = topThreeTools.stream().map(Tool::getToolId).collect(Collectors.toList());
            session.setAttribute("topThreeToolIds", topThreeToolIds);
            data[2] = topThreeToolIds;
            String topThreeToolIssues = topThreeTools.get(0).getToolUserInfo().getToolIssues()
                    + topThreeTools.get(1).getToolUserInfo().getToolIssues()
                    + topThreeTools.get(2).getToolUserInfo().getToolIssues();
            Session userSession = (Session) session.getAttribute("userSession");
            Common.modifyDummyUserInToolorVideoUsage(request, response);
            userSession.addToolUsage(38);
            userSession.addUserAnalysisInfo(user.getId(), 38, domain, topThreeToolIssues, "", new JSONObject().toString(), "");
            data[3] = finalDoamin;
            try {
                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json = ow.writeValueAsString(data);
                session.setAttribute("topThreeTools", json);
                session.setAttribute("toolAdvisorDomain", finalDoamin);
            } catch (JsonProcessingException ex) {
                LOGGER.error("Exception on converting json object:", ex);
            }
        }
        return data;
    }
}
