/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import java.util.Map;
import lxr.marketplace.pagespeedinsights.PageSpeedInsightsService;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class PageSpeedInsightsTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(PageSpeedInsightsTask.class);
    private final String finalURL;
    private final ToolAdvisorDTO toolAdvisorDTO;
    private final PageSpeedInsightsService pageSpeedInsightsService;

    public PageSpeedInsightsTask(String finalURL, ToolAdvisorDTO toolAdvisorDTO,PageSpeedInsightsService pageSpeedInsightsService) {
        super();
        this.finalURL = finalURL;
        this.toolAdvisorDTO = toolAdvisorDTO;
        this.pageSpeedInsightsService = pageSpeedInsightsService;
    }

    @Override
    public void run() {
        LOGGER.debug("PageSpeedInsightsTask started for: " + finalURL);
//        PageSpeedInsightsService pageSpeedInsightsService = new PageSpeedInsightsService();
        int desktopLightHouseScore = 0;
        int mobileLightHouseScore = 0;
        Map<String, Integer> lightHouseScore = pageSpeedInsightsService.getGoogleLightScoreForPageURL(finalURL);
        if (lightHouseScore != null && !lightHouseScore.isEmpty()) {
            if (lightHouseScore.containsKey(pageSpeedInsightsService.getDesktopUserAgent())) {
                desktopLightHouseScore = lightHouseScore.get(pageSpeedInsightsService.getDesktopUserAgent());
            }
            if (lightHouseScore.containsKey(pageSpeedInsightsService.getMobileUserAgent())) {
                mobileLightHouseScore = lightHouseScore.get(pageSpeedInsightsService.getMobileUserAgent());
            }
        }
        LOGGER.info("in PageSpeedInsightsTask for the finalURL :: "+finalURL+", destop score:: "+desktopLightHouseScore+", and mobileLightHouseScore:: "+mobileLightHouseScore);
        synchronized (toolAdvisorDTO) {
            if (desktopLightHouseScore < 50 && mobileLightHouseScore < 50) {
                toolAdvisorDTO.setPageSpeedInsightsIssue(Boolean.TRUE);
                toolAdvisorDTO.setPageSpeedInsightsMsg("<li><i></i>Your page speed lighthouse score is less than 50 in Desktop/Mobile. Check how can you improve your site speed using the Page Speed Insights Tool.</li>");
            } else if (desktopLightHouseScore < 50 && mobileLightHouseScore >= 50) {
                toolAdvisorDTO.setPageSpeedInsightsIssue(Boolean.TRUE);
                toolAdvisorDTO.setPageSpeedInsightsMsg("<li><i></i>Your page speed lighthouse score is less than 50 in Desktop. Check how can you improve your site speed using the Page Speed Insights Tool.</li>");
            } else if (desktopLightHouseScore >= 50 && mobileLightHouseScore < 50) {
                toolAdvisorDTO.setPageSpeedInsightsIssue(Boolean.TRUE);
                toolAdvisorDTO.setPageSpeedInsightsMsg("<li><i></i>Your page speed lighthouse score is less than 50 in Mobile. Check how can you improve your site speed using the Page Speed Insights Tool.</li>");
            }
            toolAdvisorDTO.notifyAll();
        }
        LOGGER.debug("PageSpeedInsightsTask completed for: " + finalURL);
    }

}
