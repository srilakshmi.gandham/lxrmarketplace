/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

/**
 *
 * @author vemanna
 */
public class ToolsUsageCountDTO {
    private int seoWebpageAnalysis;
    private int metaTagGenerator;
    private int brokenLinkChecker;
    private int inboundLinkChecker;
    private int mostUsedKeywords;
    private int pageSpeedInsights;
    private int robotsTxtGenerator;
    private int robotsTxtValidator;
    private int siteMapBuilder;
    private int competitorAnalysis ;

    public int getSeoWebpageAnalysis() {
        return seoWebpageAnalysis;
    }

    public void setSeoWebpageAnalysis(int seoWebpageAnalysis) {
        this.seoWebpageAnalysis = seoWebpageAnalysis;
    }

    public int getMetaTagGenerator() {
        return metaTagGenerator;
    }

    public void setMetaTagGenerator(int metaTagGenerator) {
        this.metaTagGenerator = metaTagGenerator;
    }

    public int getBrokenLinkChecker() {
        return brokenLinkChecker;
    }

    public void setBrokenLinkChecker(int brokenLinkChecker) {
        this.brokenLinkChecker = brokenLinkChecker;
    }

    public int getInboundLinkChecker() {
        return inboundLinkChecker;
    }

    public void setInboundLinkChecker(int inboundLinkChecker) {
        this.inboundLinkChecker = inboundLinkChecker;
    }

    public int getMostUsedKeywords() {
        return mostUsedKeywords;
    }

    public void setMostUsedKeywords(int mostUsedKeywords) {
        this.mostUsedKeywords = mostUsedKeywords;
    }

    public int getPageSpeedInsights() {
        return pageSpeedInsights;
    }

    public void setPageSpeedInsights(int pageSpeedInsights) {
        this.pageSpeedInsights = pageSpeedInsights;
    }

    public int getRobotsTxtGenerator() {
        return robotsTxtGenerator;
    }

    public void setRobotsTxtGenerator(int robotsTxtGenerator) {
        this.robotsTxtGenerator = robotsTxtGenerator;
    }

    public int getRobotsTxtValidator() {
        return robotsTxtValidator;
    }

    public void setRobotsTxtValidator(int robotsTxtValidator) {
        this.robotsTxtValidator = robotsTxtValidator;
    }

    public int getSiteMapBuilder() {
        return siteMapBuilder;
    }

    public void setSiteMapBuilder(int siteMapBuilder) {
        this.siteMapBuilder = siteMapBuilder;
    }

    public int getCompetitorAnalysis() {
        return competitorAnalysis;
    }

    public void setCompetitorAnalysis(int competitorAnalysis) {
        this.competitorAnalysis = competitorAnalysis;
    }
}
