/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

/**
 *
 * @author vemanna
 */
public class ToolAdvisorDTO {

    private boolean seoWebpageAnalysisIssue;
    private boolean metaTagGeneratorIssue;
    private boolean brokenLinkCheckerIssue;
    private boolean inboundLinkCheckerIssue;
    private boolean mostUsedKeywordsIssue;
    private boolean pageSpeedInsightsIssue;
    private boolean robotsTxtPresentIssue;
    private boolean robotsTxtValidatorIssue;
    private boolean siteMapIssue;
    private boolean competitorAnalysis ;

    private String titleMsg;
    private String metaDescMsg;
    private String h1TagMsg;
    private String imageAltTextMsg;
    private String brokenLinksMsg;
    private String domainRankMsg;
    private String doFollowMsg;
    private String mostUsedKeywordsMsg;
    private String pageSpeedInsightsMsg;
    private String robotsTxtPresentMsg;
    private String robotsTxtValidatorMsg;
    private String sitemMapXmlMsg;

    public boolean isSeoWebpageAnalysisIssue() {
        return seoWebpageAnalysisIssue;
    }

    public void setSeoWebpageAnalysisIssue(boolean seoWebpageAnalysisIssue) {
        this.seoWebpageAnalysisIssue = seoWebpageAnalysisIssue;
    }

    public boolean isMetaTagGeneratorIssue() {
        return metaTagGeneratorIssue;
    }

    public void setMetaTagGeneratorIssue(boolean metaTagGeneratorIssue) {
        this.metaTagGeneratorIssue = metaTagGeneratorIssue;
    }

    public boolean isBrokenLinkCheckerIssue() {
        return brokenLinkCheckerIssue;
    }

    public void setBrokenLinkCheckerIssue(boolean brokenLinkCheckerIssue) {
        this.brokenLinkCheckerIssue = brokenLinkCheckerIssue;
    }

    public boolean isInboundLinkCheckerIssue() {
        return inboundLinkCheckerIssue;
    }

    public void setInboundLinkCheckerIssue(boolean inboundLinkCheckerIssue) {
        this.inboundLinkCheckerIssue = inboundLinkCheckerIssue;
    }

    public boolean isMostUsedKeywordsIssue() {
        return mostUsedKeywordsIssue;
    }

    public void setMostUsedKeywordsIssue(boolean mostUsedKeywordsIssue) {
        this.mostUsedKeywordsIssue = mostUsedKeywordsIssue;
    }

    public boolean isPageSpeedInsightsIssue() {
        return pageSpeedInsightsIssue;
    }

    public void setPageSpeedInsightsIssue(boolean pageSpeedInsightsIssue) {
        this.pageSpeedInsightsIssue = pageSpeedInsightsIssue;
    }

    public boolean isRobotsTxtPresentIssue() {
        return robotsTxtPresentIssue;
    }

    public void setRobotsTxtPresentIssue(boolean robotsTxtPresentIssue) {
        this.robotsTxtPresentIssue = robotsTxtPresentIssue;
    }

    public boolean isRobotsTxtValidatorIssue() {
        return robotsTxtValidatorIssue;
    }

    public void setRobotsTxtValidatorIssue(boolean robotsTxtValidatorIssue) {
        this.robotsTxtValidatorIssue = robotsTxtValidatorIssue;
    }

    public boolean isSiteMapIssue() {
        return siteMapIssue;
    }

    public void setSiteMapIssue(boolean siteMapIssue) {
        this.siteMapIssue = siteMapIssue;
    }

    public boolean isCompetitorAnalysis() {
        return competitorAnalysis;
    }

    public void setCompetitorAnalysis(boolean competitorAnalysis) {
        this.competitorAnalysis = competitorAnalysis;
    }

    public String getTitleMsg() {
        return titleMsg;
    }

    public void setTitleMsg(String titleMsg) {
        this.titleMsg = titleMsg;
    }

    public String getMetaDescMsg() {
        return metaDescMsg;
    }

    public void setMetaDescMsg(String metaDescMsg) {
        this.metaDescMsg = metaDescMsg;
    }

    public String getH1TagMsg() {
        return h1TagMsg;
    }

    public void setH1TagMsg(String h1TagMsg) {
        this.h1TagMsg = h1TagMsg;
    }

    public String getImageAltTextMsg() {
        return imageAltTextMsg;
    }

    public void setImageAltTextMsg(String imageAltTextMsg) {
        this.imageAltTextMsg = imageAltTextMsg;
    }

    public String getBrokenLinksMsg() {
        return brokenLinksMsg;
    }

    public void setBrokenLinksMsg(String brokenLinksMsg) {
        this.brokenLinksMsg = brokenLinksMsg;
    }

    public String getDomainRankMsg() {
        return domainRankMsg;
    }

    public void setDomainRankMsg(String domainRankMsg) {
        this.domainRankMsg = domainRankMsg;
    }

    public String getDoFollowMsg() {
        return doFollowMsg;
    }

    public void setDoFollowMsg(String doFollowMsg) {
        this.doFollowMsg = doFollowMsg;
    }

    public String getMostUsedKeywordsMsg() {
        return mostUsedKeywordsMsg;
    }

    public void setMostUsedKeywordsMsg(String mostUsedKeywordsMsg) {
        this.mostUsedKeywordsMsg = mostUsedKeywordsMsg;
    }

    public String getPageSpeedInsightsMsg() {
        return pageSpeedInsightsMsg;
    }

    public void setPageSpeedInsightsMsg(String pageSpeedInsightsMsg) {
        this.pageSpeedInsightsMsg = pageSpeedInsightsMsg;
    }

    public String getRobotsTxtPresentMsg() {
        return robotsTxtPresentMsg;
    }

    public void setRobotsTxtPresentMsg(String robotsTxtPresentMsg) {
        this.robotsTxtPresentMsg = robotsTxtPresentMsg;
    }

    public String getRobotsTxtValidatorMsg() {
        return robotsTxtValidatorMsg;
    }

    public void setRobotsTxtValidatorMsg(String robotsTxtValidatorMsg) {
        this.robotsTxtValidatorMsg = robotsTxtValidatorMsg;
    }

    public String getSitemMapXmlMsg() {
        return sitemMapXmlMsg;
    }

    public void setSitemMapXmlMsg(String sitemMapXmlMsg) {
        this.sitemMapXmlMsg = sitemMapXmlMsg;
    }

    

}
