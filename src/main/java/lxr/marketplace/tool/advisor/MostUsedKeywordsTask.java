/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import java.util.List;
import javax.servlet.http.HttpSession;
import lxr.marketplace.mostusedkeywords.MostUsedKeyWordsResult;
import lxr.marketplace.mostusedkeywords.MostUsedKeywordService;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class MostUsedKeywordsTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(MostUsedKeywordsTask.class);
    private final String finalDomain;
    private final ToolAdvisorDTO toolAdvisorDTO;
    private final MostUsedKeywordService mostUsedKeywordService;
    private final List<String> stopWordsList;
    private final HttpSession session;

    public MostUsedKeywordsTask(String finalDomain, ToolAdvisorDTO toolAdvisorDTO, MostUsedKeywordService mostUsedKeywordService, List<String> stopWordsList, HttpSession session) {
        this.finalDomain = finalDomain;
        this.toolAdvisorDTO = toolAdvisorDTO;
        this.mostUsedKeywordService = mostUsedKeywordService;
        this.stopWordsList = stopWordsList;
        this.session = session;
    }

    @Override
    public void run() {
        LOGGER.debug("MostUsedKeywordsTask started");
        NinjaWebStats ninjaWebStats = new NinjaWebStats(finalDomain);
        List<MostUsedKeyWordsResult> mostUsedKeywordsList = mostUsedKeywordService.crawlDomainToFetchMostUsedKeywords(ninjaWebStats, stopWordsList, session, Common.ADVISOR_TOOL_LIMIT);
        mostUsedKeywordsList.stream().map((mostUsedKeyWordsResult) -> mostUsedKeyWordsResult.getTextCodeRatio()).filter((codeTextRatio) -> (codeTextRatio > 5.0)).map((_item) -> {
            toolAdvisorDTO.setMostUsedKeywordsIssue(true);
            return _item;
        }).forEach((_item) -> {
            toolAdvisorDTO.setMostUsedKeywordsMsg("<li><i></i>The Text/HTML ratio in some pages are not following the standard. Check the Most Used Keyword Tool to find out more.</li>");
        });
        LOGGER.debug("MostUsedKeywordsTask completed");
    }

}
