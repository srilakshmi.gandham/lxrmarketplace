/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import lxr.marketplace.apiaccess.AhrefAPIService;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class InboundLinkCheckerTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(InboundLinkCheckerTask.class);
    String finalDomain;
    AhrefAPIService ahrefAPIService;
    ToolAdvisorDTO toolAdvisorDTO;

    public InboundLinkCheckerTask(String finalDomain, AhrefAPIService ahrefAPIService, ToolAdvisorDTO toolAdvisorDTO) {
        this.finalDomain = finalDomain;
        this.ahrefAPIService = ahrefAPIService;
        this.toolAdvisorDTO = toolAdvisorDTO;
    }

    @Override
    public void run() {
        LOGGER.debug("InboundLinkCheckerTask started");
        int domainRating = ahrefAPIService.fetchDomainRating(finalDomain);
        if (domainRating < 40) {
            toolAdvisorDTO.setInboundLinkCheckerIssue(true);
            toolAdvisorDTO.setDomainRankMsg("<li><i></i>Your Domain Rank is less than 40. Check your backlink profile.</li>");
        }
        long[] backLinksData = ahrefAPIService.fetchBacklinkCheckerHeaderData(finalDomain);
        if (backLinksData[3] > backLinksData[2]) {
            toolAdvisorDTO.setInboundLinkCheckerIssue(true);
            toolAdvisorDTO.setDoFollowMsg("<li><i></i>You have a higher percentage of \"Do Follow\" links, which is not natural. Check your backlink profile.</li>");
        }
        LOGGER.debug("InboundLinkChecker Task completed");
    }

}
