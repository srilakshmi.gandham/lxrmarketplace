/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author vemanna, HomePageDataFetchingTask is using to fetch all home page
 * data like title, meta desc, header tags and image alt text missing. It helps
 * for analyzing of SEO Webpage Analysis, MetaTag Generator and Most Used
 * Keywords in Tool Advisor.
 */
public class HomePageDataTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(HomePageDataTask.class);

    private final String finalDomain;
    private final ToolAdvisorDTO toolAdvisorDTO;
    private final int webpageAnalysisUsageCunt;
    private final int metaTagGeneratorUsageCount;

    public HomePageDataTask(String finalDomain, ToolAdvisorDTO toolAdvisorDTO, int webpageAnalysisUsageCunt, int metaTagGeneratorUsageCount) {
        this.finalDomain = finalDomain;
        this.toolAdvisorDTO = toolAdvisorDTO;
        this.webpageAnalysisUsageCunt = webpageAnalysisUsageCunt;
        this.metaTagGeneratorUsageCount = metaTagGeneratorUsageCount;
    }

    @Override
    public void run() {
        LOGGER.debug("HomepageDataFetching task started");
        StringBuilder pageHTML = new StringBuilder("");
        Common.getStatusCodeAndHTML(finalDomain, new StringBuilder(""), pageHTML);
        if (!pageHTML.toString().equals("")) {
            Document doc = Common.getDocumentFromHTML(pageHTML.toString(), finalDomain);
            if (doc != null) {
                //Metatag Generator Task Issues
                if (metaTagGeneratorUsageCount < 5) {
                    String title = doc.title();
                    if (title == null || title.length() == 0) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setTitleMsg("<li><i></i>Your Home Page Title is missing. Create your meta tags.</li>");
                    } else if (title.length() <= 40) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setTitleMsg("<li><i></i>Your Home Page Title is too small. Modify your meta tags.</li>");
                    } else if (title.length() >= 65) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setTitleMsg("<li><i></i>Your Home Page Title is too large. Modify your meta tags.</li>");
                    }

                    String metaDesc = doc.select("meta[name=description]").first().attr("content");
                    if (metaDesc == null || metaDesc.length() == 0) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setMetaDescMsg("<li><i></i>Your Home Page Meta Description is missing. Create your meta tags.</li>");
                    } else if (metaDesc.length() <= 70) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setMetaDescMsg("<li><i></i>Your Home Page Meta Description is too small. Modify your meta tags.</li>");
                    } else if (metaDesc.length() >= 161) {
                        toolAdvisorDTO.setMetaTagGeneratorIssue(true);
                        toolAdvisorDTO.setMetaDescMsg("<li><i></i>Your Home Page Meta Description is too large. Modify your meta tags.</li>");
                    }
                }
                // Webpage Analysis Task Issues
                if (webpageAnalysisUsageCunt < 5) {
                    Elements h1Tag = doc.select("h1");
                    Elements images = doc.select("img");
                    Elements imagesWithAlt = doc.select("img[alt~=^(?!\\s*$).+]");

                    if (h1Tag.isEmpty() && (images.size() > 0 && images.size() > imagesWithAlt.size())) {
                        toolAdvisorDTO.setSeoWebpageAnalysisIssue(true);
                        toolAdvisorDTO.setH1TagMsg("<li><i></i>H1 tag is not present in your homepage.</li>");
                        int imageAltTextMissingPer = (((images.size() - imagesWithAlt.size()) * 100) / images.size());
                        toolAdvisorDTO.setImageAltTextMsg("<li><i></i>" + imageAltTextMissingPer + "% of the images in home page don't have any alt text. Analyze if your web page is SEO optimized.</li>");
                    } else if (h1Tag.isEmpty()) {
                        toolAdvisorDTO.setSeoWebpageAnalysisIssue(true);
                        toolAdvisorDTO.setH1TagMsg("<li><i></i>H1 tag is not present in your homepage. Analyze if your web page is SEO optimized.</li>");
                    } else if (images.size() > 0 && images.size() > imagesWithAlt.size()) {
                        toolAdvisorDTO.setSeoWebpageAnalysisIssue(true);
                        int imageAltTextMissingPer = (((images.size() - imagesWithAlt.size()) * 100) / images.size());
                        toolAdvisorDTO.setImageAltTextMsg("<li><i></i>" + imageAltTextMissingPer + "% of the images in home page don't have any alt text. Analyze if your web page is SEO optimized.</li>");
                    }
                }

            }
        }
        LOGGER.debug("HomepageDataFetching task completed");

    }

}
