/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class SiteMapTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(SiteMapTask.class);
    
    private final String finalDomain;
    private final ToolAdvisorDTO toolAdvisorDTO;

    public SiteMapTask(String finalDomain, ToolAdvisorDTO toolAdvisorDTO) {
        this.finalDomain = finalDomain;
        this.toolAdvisorDTO = toolAdvisorDTO;
    }

    @Override
    public void run() {
        LOGGER.debug("SiteMapTask started for: " + finalDomain);
        boolean sitemapPresent = Common.checkSitemapPresenceByURL(finalDomain);
        if (!sitemapPresent) {
            toolAdvisorDTO.setSiteMapIssue(true);
            toolAdvisorDTO.setSitemMapXmlMsg("<li><i></i>You are not using sitemap.xml in your website. Let's create a sitemap.xml file using the SEO Sitemap Builder tool.</li>");
        }
        LOGGER.debug("SiteMapTask completed for: " + finalDomain);
    }

}
