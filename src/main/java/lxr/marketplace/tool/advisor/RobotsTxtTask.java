/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import java.util.List;
import java.util.Map;
import lxr.marketplace.robotstxtvalidator.RobotsTxtValidatorResult;
import lxr.marketplace.robotstxtvalidator.RobotsTxtValidatorService;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class RobotsTxtTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(RobotsTxtTask.class);

    private final String finalDomain;
    private final ToolAdvisorDTO toolAdvisorDTO;
    private final RobotsTxtValidatorService robotsTxtValidatorService;
    private final int robotsTxtGenaratorUsageCount;
    private final int robotsTxtValidatorUsageCount;

    public RobotsTxtTask(String finalDomain, ToolAdvisorDTO toolAdvisorDTO, RobotsTxtValidatorService robotsTxtValidatorService,
            int robotsTxtGenaratorUsageCount, int robotsTxtValidatorUsageCount) {
        this.finalDomain = finalDomain;
        this.toolAdvisorDTO = toolAdvisorDTO;
        this.robotsTxtValidatorService = robotsTxtValidatorService;
        this.robotsTxtGenaratorUsageCount = robotsTxtGenaratorUsageCount;
        this.robotsTxtValidatorUsageCount = robotsTxtValidatorUsageCount;
    }

    @Override
    public void run() {
        LOGGER.debug("RobotsTxtTask started for: " + finalDomain);

        if (robotsTxtGenaratorUsageCount < 5 || robotsTxtValidatorUsageCount < 5) {
            boolean robotsTxtPresent = Common.checkPresenceOfRobotsTxt(finalDomain);
            if (!robotsTxtPresent) {
                /*Removing query params or request params from URL and preparing final query URL i.e protocol://HostName */
                String tempQueryURL = Common.getDomainWithProtocol(finalDomain);
                if (tempQueryURL != null && !tempQueryURL.equals(finalDomain)) {
                    robotsTxtPresent = Common.checkPresenceOfRobotsTxt(tempQueryURL);
                }
            }
            if (!robotsTxtPresent && robotsTxtGenaratorUsageCount < 5) {
                toolAdvisorDTO.setRobotsTxtPresentIssue(true);
                toolAdvisorDTO.setRobotsTxtPresentMsg("<li ><i></i>You are not using robots.txt in your website. Let's create a robots.txt file using the Robots.txt Generator tool.</li>");
            } else if (robotsTxtPresent && robotsTxtValidatorUsageCount < 5) {
                String finalURL;
                if (finalDomain.endsWith("/")) {
                    finalURL = finalDomain + "robots.txt";
                } else {
                    finalURL = finalDomain + "/robots.txt";
                }
                String robotTxtContent = robotsTxtValidatorService.fetchRobotTxtFileContent(finalURL).trim();
                Map<Integer, List<RobotsTxtValidatorResult>> validationMap = robotsTxtValidatorService.validateRobotsTxtContent(robotTxtContent);

                if (validationMap != null && validationMap.size() >= 0) {
                    outerloop:
                    for (Map.Entry<Integer, List<RobotsTxtValidatorResult>> entry : validationMap.entrySet()) {
                        List<RobotsTxtValidatorResult> robotsTxtValidatorResultList = entry.getValue();
                        for (RobotsTxtValidatorResult robotsTxtValidatorResult : robotsTxtValidatorResultList) {
                            if (robotsTxtValidatorResult.getErrorType() == 1) {
                                toolAdvisorDTO.setRobotsTxtValidatorIssue(true);
                                toolAdvisorDTO.setRobotsTxtValidatorMsg("<li ><i></i>Your robots.txt have a few errors. Check the Robots.txt Validator tool to fix the errors.</li>");
                                break outerloop;
                            }
                        }
                    }
                }
            }
        }
        LOGGER.debug("RobotsTxtTask completed ");
    }

}
