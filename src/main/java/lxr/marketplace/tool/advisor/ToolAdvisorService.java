/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.AhrefAPIService;
import lxr.marketplace.brokenlinkchecker.BrokenLinkCheckerService;
import lxr.marketplace.mostusedkeywords.MostUsedKeywordService;
import lxr.marketplace.pagespeedinsights.PageSpeedInsightsService;
import lxr.marketplace.robotstxtvalidator.RobotsTxtValidatorService;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class ToolAdvisorService {

    private static final Logger LOGGER = Logger.getLogger(ToolAdvisorService.class);

    @Autowired
    private MostUsedKeywordService mostUsedKeywordService;
    @Autowired
    BrokenLinkCheckerService brokenLinkCheckerService;
    @Autowired
    AhrefAPIService ahrefAPIService;
    @Autowired
    RobotsTxtValidatorService robotsTxtValidatorService;
    @Autowired
    ToolService toolService;
    @Autowired
    PageSpeedInsightsService pageSpeedInsightsService;

    @Autowired
    private ServletContext servletContext;

    public List<Tool> fetchingTopThreeTools(ToolsUsageCountDTO toolsUsageCountDTO, HttpSession session, List<String> stopWordsList,
            String finalDoamin) {

        LOGGER.info("Started Tool Advisor Tool for:" + finalDoamin);
        ToolAdvisorDTO toolAdvisorDTO = new ToolAdvisorDTO();
        //The thread is in new state if you create an instance of Thread class but before the invocation of start() method.
        Thread homePageDataFetchingThread = null;
        Thread robotsTxtThread = null;
        Thread siteMapThread = null;
        Thread pageSpeedInsightsThread = null;
        Thread inboundLinkCheckerThread = null;
        Thread brokenLinkCheckerThread = null;
        Thread mostUsedKeywordsThread = null;

        if (toolsUsageCountDTO.getSeoWebpageAnalysis() < 5 || toolsUsageCountDTO.getMetaTagGenerator() < 5) {
            HomePageDataTask homePageDataFetchingTask = new HomePageDataTask(finalDoamin, toolAdvisorDTO,
                    toolsUsageCountDTO.getSeoWebpageAnalysis(), toolsUsageCountDTO.getMetaTagGenerator());
            homePageDataFetchingThread = new Thread(homePageDataFetchingTask);
            homePageDataFetchingThread.start();
        }

        if (toolsUsageCountDTO.getRobotsTxtGenerator() < 5 || toolsUsageCountDTO.getRobotsTxtValidator() < 5) {
            RobotsTxtTask robotsTxtTask = new RobotsTxtTask(finalDoamin, toolAdvisorDTO, robotsTxtValidatorService,
                    toolsUsageCountDTO.getRobotsTxtGenerator(), toolsUsageCountDTO.getRobotsTxtValidator());
            robotsTxtThread = new Thread(robotsTxtTask);
            robotsTxtThread.start();
        }
        if (toolsUsageCountDTO.getSiteMapBuilder() < 5) {
            SiteMapTask siteMapTask = new SiteMapTask(finalDoamin, toolAdvisorDTO);
            siteMapThread = new Thread(siteMapTask);
            siteMapThread.start();
        }
        if (toolsUsageCountDTO.getPageSpeedInsights() < 5) {
            PageSpeedInsightsTask pageSpeedInsightsTask = new PageSpeedInsightsTask(finalDoamin, toolAdvisorDTO, pageSpeedInsightsService);
            pageSpeedInsightsThread = new Thread(pageSpeedInsightsTask);
            pageSpeedInsightsThread.start();
        }
        if (toolsUsageCountDTO.getInboundLinkChecker() < 5) {
            InboundLinkCheckerTask inboundLinkCheckerTask = new InboundLinkCheckerTask(finalDoamin, ahrefAPIService, toolAdvisorDTO);
            inboundLinkCheckerThread = new Thread(inboundLinkCheckerTask);
            inboundLinkCheckerThread.start();
        }

        if (toolsUsageCountDTO.getMostUsedKeywords() < 5) {
            MostUsedKeywordsTask mostUsedKeywordsTask = new MostUsedKeywordsTask(finalDoamin, toolAdvisorDTO, mostUsedKeywordService, stopWordsList, session);
            mostUsedKeywordsThread = new Thread(mostUsedKeywordsTask);
            mostUsedKeywordsThread.start();
        }

        try {
            if (homePageDataFetchingThread != null) {
                homePageDataFetchingThread.join();
            }
            if (robotsTxtThread != null) {
                robotsTxtThread.join();
            }
            if (siteMapThread != null) {
                siteMapThread.join();
            }
            if (pageSpeedInsightsThread != null) {
                pageSpeedInsightsThread.join();
            }
            if (inboundLinkCheckerThread != null) {
                inboundLinkCheckerThread.join();
            }

            if (mostUsedKeywordsThread != null) {
                mostUsedKeywordsThread.join();
            }
            if (toolsUsageCountDTO.getBrokenLinkChecker() < 5) {
                BrokenLinkCheckerTask brokenLinkCheckerTask = new BrokenLinkCheckerTask(finalDoamin, session, toolAdvisorDTO, brokenLinkCheckerService);
                brokenLinkCheckerThread = new Thread(brokenLinkCheckerTask);
                brokenLinkCheckerThread.start();
            }

        } catch (InterruptedException ex) {
            LOGGER.error("Exception on threads: ", ex);
        }

        Map<Integer, String> toolIssues = new LinkedHashMap<>();

        //Sorting top three tools 
        if (toolAdvisorDTO.isSeoWebpageAnalysisIssue()) {
            String issue = (toolAdvisorDTO.getH1TagMsg() == null ? "" : toolAdvisorDTO.getH1TagMsg());
            issue += (toolAdvisorDTO.getImageAltTextMsg() == null ? "" : toolAdvisorDTO.getImageAltTextMsg());
            toolIssues.put(8, issue);
        }
        if (toolAdvisorDTO.isRobotsTxtPresentIssue()) {
            String issue = toolAdvisorDTO.getRobotsTxtPresentMsg();
            toolIssues.put(24, issue);
        }
        if (toolAdvisorDTO.isSiteMapIssue()) {
            String issue = toolAdvisorDTO.getSitemMapXmlMsg();
            toolIssues.put(6, issue);
        }
        if (toolAdvisorDTO.isRobotsTxtValidatorIssue()) {
            String issue = toolAdvisorDTO.getRobotsTxtValidatorMsg();
            toolIssues.put(25, issue);
        }
        if (toolAdvisorDTO.isPageSpeedInsightsIssue()) {
            String issue = toolAdvisorDTO.getPageSpeedInsightsMsg();
            toolIssues.put(35, issue);
        }
        if (toolAdvisorDTO.isMetaTagGeneratorIssue()) {
            String titleIssue = toolAdvisorDTO.getTitleMsg();
            String metDescIssue = toolAdvisorDTO.getMetaDescMsg();
            String issue = (titleIssue == null ? "" : titleIssue) + " \n " + (metDescIssue == null ? "" : metDescIssue);
            toolIssues.put(27, issue);
        }
        if (toolAdvisorDTO.isInboundLinkCheckerIssue()) {
            String domainRankIssue = toolAdvisorDTO.getDomainRankMsg();
            String doFollowIssue = toolAdvisorDTO.getDoFollowMsg();
            String issue = (domainRankIssue == null ? "" : domainRankIssue) + "\n" + (doFollowIssue == null ? "" : doFollowIssue);
            toolIssues.put(20, issue);
        }
        if (toolAdvisorDTO.isBrokenLinkCheckerIssue()) {
            String issue = toolAdvisorDTO.getBrokenLinksMsg();
            toolIssues.put(31, issue);
        }
        if (toolAdvisorDTO.isMostUsedKeywordsIssue()) {
            String issue = toolAdvisorDTO.getMostUsedKeywordsMsg();
            toolIssues.put(34, issue);
        }

        //After sorting top 3 tools not found then checking top most used tools
        //Verifying this tool is already present or not in top 3 tool advisor list, 
        //if present then not adding else adding that tool
        if (!toolIssues.containsKey(35) && toolsUsageCountDTO.getPageSpeedInsights() < 5) {
            toolIssues.put(35, "");
        }

        if (toolsUsageCountDTO.getCompetitorAnalysis() < 5) {
            toolIssues.put(7, "");
        }

        if (!toolIssues.containsKey(20) && toolsUsageCountDTO.getInboundLinkChecker() < 5) {
            toolIssues.put(20, "");
        }

        //After checking top most used tools, then also top 3 tools not found then showing latest deployed tools
        if (toolIssues.size() < 3 && toolIssues.size() > 0) {
//            String toolIds = getToolIds(toolIssues, false);
            List<Integer> toolIdsList = getToolIds(toolIssues, false);
            String toolIdsString = "";
            for (Integer toolId : toolIdsList) {
                if (toolIdsString.equals("")) {
                    toolIdsString += toolId;
                } else {
                    toolIdsString += "," + toolId;
                }
            }
            int limit = (3 - toolIssues.size());
            List<Integer> toolIdList = toolService.getToolsIdsForToolAdvisor(toolIdsString, limit);
            if (toolIdList != null) {
                for (int i = 0; i < toolIdList.size(); i++) {
                    toolIssues.put(toolIdList.get(i), "");
                }
            }
            //After checking latest deployed tools, then also top 3 tools not found then showing  random tools
//            TODO: Need to add issue text for the below tools
            if (toolIssues.size() < 3) {
                if (!toolIssues.containsKey(8)) {
                    toolIssues.put(8, "");
                }
                if (!toolIssues.containsKey(35)) {
                    toolIssues.put(35, "");
                }
                if (!toolIssues.containsKey(34)) {
                    toolIssues.put(34, "");
                }
                if (!toolIssues.containsKey(20)) {
                    toolIssues.put(20, "");
                }
                if (!toolIssues.containsKey(30)) {
                    toolIssues.put(30, "");
                }
            }
        } else if (toolIssues.isEmpty()) {
            //If not found top 3 tools then showing random tools
            toolIssues.put(8, "");
            toolIssues.put(35, "");
            toolIssues.put(34, "");
        }

        List<Integer> finalToolIds = getToolIds(toolIssues, true);
        List<Tool> topThreeAdviosrTools = getToolsByToolIds(finalToolIds);
        topThreeAdviosrTools.stream().forEach((tool) -> {
            int toolId = tool.getToolId();
            if (toolIssues.containsKey(toolId)) {
                String issue = toolIssues.get(toolId);
                tool.getToolUserInfo().setToolIssues(issue);
            }
        });
        LOGGER.info("Completed Tool Advisor Tool for:" + finalDoamin);
        return topThreeAdviosrTools;
    }

    private List<Integer> getToolIds(Map<Integer, String> toolIssuesIdsList, boolean isLimit) {
        List<Integer> toolIdsList = new ArrayList();
        int i = 0;
        for (Map.Entry m : toolIssuesIdsList.entrySet()) {
            toolIdsList.add((Integer) m.getKey());
            i++;
            if (isLimit && i == 3) {
                break;
            }
        }
        return toolIdsList;
    }

    public List<Tool> getToolsByToolIds(List<Integer> toolIdsList) {
        Map<Integer, Tool> toolsMap = (Map<Integer, Tool>) servletContext.getAttribute("tools");
        List<Tool> toolsList = new ArrayList();
        toolIdsList.stream().forEach((Integer toolId) -> {
            toolsList.add(toolsMap.get(toolId));
        });
        return toolsList;
    }
}
