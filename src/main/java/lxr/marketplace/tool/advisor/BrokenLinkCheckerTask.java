/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.tool.advisor;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import lxr.marketplace.brokenlinkchecker.BrokenLinkCheckerService;
import lxr.marketplace.crawler.LxrmUrlStats;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;

/**
 *
 * @author vemanna
 */
public class BrokenLinkCheckerTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(BrokenLinkCheckerTask.class);

    private final String finalDomain;
    private final HttpSession session;
    private final ToolAdvisorDTO toolAdvisorDTO;
    private final BrokenLinkCheckerService brokenLinkCheckerService;

    public BrokenLinkCheckerTask(String finalDomain, HttpSession session, ToolAdvisorDTO toolAdvisorDTO, BrokenLinkCheckerService brokenLinkCheckerService) {
        this.finalDomain = finalDomain;
        this.session = session;
        this.toolAdvisorDTO = toolAdvisorDTO;
        this.brokenLinkCheckerService = brokenLinkCheckerService;
    }

    @Override
    public void run() {
        LOGGER.debug("BrokenLinkCheckerTask started");
        List<LxrmUrlStats> lxrmUrlStatsList = new ArrayList<>();
        NinjaWebStats ninjaWebStats = new NinjaWebStats(finalDomain);
        brokenLinkCheckerService.fetchWebsiteData(lxrmUrlStatsList, ninjaWebStats, session, Common.ADVISOR_TOOL_LIMIT);
        if (lxrmUrlStatsList.size() > 0) {
            int brokenlinksCount = (int) lxrmUrlStatsList.stream().filter(p -> p.getStatusCode() != 200 && p.getStatusCode() != 301).count();
            if (brokenlinksCount > 0) {
                toolAdvisorDTO.setBrokenLinkCheckerIssue(true);
                toolAdvisorDTO.setBrokenLinksMsg("<li><i></i>You have broken links on your site. Let's find the broken links using the Broken Link Checker tool.</li>");
            }
        }
        LOGGER.debug("BrokenLinkCheckerTask completed");
    }

}
