package lxr.marketplace.toprankedwebsitesbykeyword;

public class TopRankedWebsitesbyKeywordResults {
	private String domainName;
	private String resultURL;
	private int position;
	private int pageRank;
	private long urlBacklinkCount;
	private long domainBacklinkCount;
	
	
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getResultURL() {
		return resultURL;
	}
	public void setResultURL(String resultURL) {
		this.resultURL = resultURL;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getPageRank() {
		return pageRank;
	}
	public void setPageRank(int pageRank) {
		this.pageRank = pageRank;
	}
	public long getUrlBacklinkCount() {
		return urlBacklinkCount;
	}
	public void setUrlBacklinkCount(long urlBacklinkCount) {
		this.urlBacklinkCount = urlBacklinkCount;
	}
	public long getDomainBacklinkCount() {
		return domainBacklinkCount;
	}
	public void setDomainBacklinkCount(long domainBacklinkCount) {
		this.domainBacklinkCount = domainBacklinkCount;
	}
	
	
}
