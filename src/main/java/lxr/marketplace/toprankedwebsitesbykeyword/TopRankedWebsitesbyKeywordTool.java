package lxr.marketplace.toprankedwebsitesbykeyword;


public class TopRankedWebsitesbyKeywordTool {
	private String searchKeyword;
	private String location;
	private String language;
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	
	
}
