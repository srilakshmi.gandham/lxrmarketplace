package lxr.marketplace.toprankedwebsitesbykeyword;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/top-ranked-websites-bykeyword-tool.html")
public class TopRankedWebsitesbyKeywordToolController {
    
    private static Logger logger = Logger.getLogger(TopRankedWebsitesbyKeywordToolController.class);
    @Autowired
    private final TopRankedWebsitesbyKeywordService topRankedWebsitesbyKeywordService;
    private final String downloadFolder;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    
    JSONObject userInputJson = null;
    
    @Autowired
    public TopRankedWebsitesbyKeywordToolController(TopRankedWebsitesbyKeywordService topRankedWebsitesbyKeywordService, String downloadFolder) {
        this.topRankedWebsitesbyKeywordService = topRankedWebsitesbyKeywordService;
        this.downloadFolder = downloadFolder;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("topRankedWebsitesbyKeywordTool") TopRankedWebsitesbyKeywordTool topRankedWebsitesbyKeywordTool,
            ModelMap model, HttpSession session, @RequestParam(defaultValue = "") String query) {
        if ((WebUtils.hasSubmitParameter(request, "getResult") && WebUtils.hasSubmitParameter(request, "loginRefresh"))
                || WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            
            session.setAttribute("loginrefresh", "loginrefresh");
            
            HashMap<String, String> geoLocation = Common.loadGeoLocations();
            model.addAttribute("geoLocation", geoLocation);
            HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
            model.addAttribute("loadLanguages", loadLanguages);
            String searchKeyword = "";
            String location = "";
            String language = "";
            if (session.getAttribute("searchKwd") != null) {
                searchKeyword = (String) session.getAttribute("searchKwd");
                location = (String) session.getAttribute("location");
                language = (String) session.getAttribute("language");
            } else {
                location = "us";
                language = "lang_en";
            }
            topRankedWebsitesbyKeywordTool.setSearchKeyword(searchKeyword);
            topRankedWebsitesbyKeywordTool.setLocation(location);
            topRankedWebsitesbyKeywordTool.setLanguage(language);
            model.addAttribute(topRankedWebsitesbyKeywordTool);
            
            model.addAttribute("status", "success");
            
        } else {
            topRankedWebsitesbyKeywordTool.setLanguage("lang_en");
            topRankedWebsitesbyKeywordTool.setLocation("us");
            model.addAttribute("topRankedWebsitesbyKeywordTool", topRankedWebsitesbyKeywordTool);
            HashMap<String, String> geoLocation = Common.loadGeoLocations();
            model.addAttribute("geoLocation", geoLocation);
            HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
            model.addAttribute("loadLanguages", loadLanguages);
        }
        return "views/topRankedWebsiteByKeyword/topRankedWebsiteByKeyword";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, HttpServletResponse response,
            Model model, HttpSession session,
            @ModelAttribute("topRankedWebsitesbyKeywordTool") TopRankedWebsitesbyKeywordTool topRankedWebsitesbyKeywordTool) throws JSONException {
        Login user = (Login) session.getAttribute("user");
        if (topRankedWebsitesbyKeywordTool != null) {
            if (topRankedWebsitesbyKeywordTool.getSearchKeyword() != null && !topRankedWebsitesbyKeywordTool.getSearchKeyword().trim().isEmpty()) {
                topRankedWebsitesbyKeywordTool.setSearchKeyword(topRankedWebsitesbyKeywordTool.getSearchKeyword().trim());
            }
            session.setAttribute("topRankedWebsitesbyKeywordTool", topRankedWebsitesbyKeywordTool);
        }
        
        if (user != null) {
            //@In PAYMENT SCREEN USING THIS toolObj details

            Tool toolObj = new Tool();
            toolObj.setToolId(30);
            toolObj.setToolName("Top Ranked Websites by Keyword Tool");
            toolObj.setToolLink("top-ranked-websites-bykeyword-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.TOP_RANKED_WEBSITES_BY_KEYWORD);
            if (WebUtils.hasSubmitParameter(request, "getResult")) {
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    return "views/topRankedWebsiteByKeyword/topRankedWebsiteByKeyword";
                }
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());

                //  if the request is coming from tool page then only add the user inputs to JSONObject
                //  if the request is coming from mail then do not add the user inputs to JSONObject
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    if (topRankedWebsitesbyKeywordTool != null) {
                        userInputJson.put("2", topRankedWebsitesbyKeywordTool.getSearchKeyword());
                        userInputJson.put("3", topRankedWebsitesbyKeywordTool.getLocation());
                        userInputJson.put("4", topRankedWebsitesbyKeywordTool.getLanguage());
                    }
                }
                
                int proxyHost = 0;
                List<TopRankedWebsitesbyKeywordResults> googleResults = topRankedWebsitesbyKeywordService.getGoogleResults(topRankedWebsitesbyKeywordTool, proxyHost);
                session.setAttribute("googleResults", googleResults);
                List<TopRankedWebsitesbyKeywordResults> bingResults = topRankedWebsitesbyKeywordService.getBingResults(topRankedWebsitesbyKeywordTool);
                session.setAttribute("bingResults", bingResults);
                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                model.addAttribute("geoLocation", geoLocation);
                model.addAttribute("status", "success");
                HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                if (topRankedWebsitesbyKeywordTool != null) {
                    session.setAttribute("searchKwd", topRankedWebsitesbyKeywordTool.getSearchKeyword());
                    session.setAttribute("language", topRankedWebsitesbyKeywordTool.getLanguage());
                    session.setAttribute("location", topRankedWebsitesbyKeywordTool.getLocation());
                }
                model.addAttribute("loadLanguages", loadLanguages);
                return "views/topRankedWebsiteByKeyword/topRankedWebsiteByKeyword";
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                try {
                    String downParam = request.getParameter("download");
                    @SuppressWarnings("unchecked")
                    List<TopRankedWebsitesbyKeywordResults> googleResults = (List<TopRankedWebsitesbyKeywordResults>) session.getAttribute("googleResults");
                    @SuppressWarnings("unchecked")
                    List<TopRankedWebsitesbyKeywordResults> bingResults = (List<TopRankedWebsitesbyKeywordResults>) session.getAttribute("bingResults");
                    String kwd = (String) session.getAttribute("searchKwd");
                    String dwnfileName = topRankedWebsitesbyKeywordService.createPdfFile(kwd, downloadFolder, googleResults, bingResults, user);
                    if (dwnfileName != null && !dwnfileName.trim().equals("")) {
                        if (downParam.equalsIgnoreCase("download")) {
                            Common.downloadReport(response, downloadFolder, dwnfileName, "pdf");
                        } else if (downParam.equalsIgnoreCase("sendmail")) {
                            String toAddrs = request.getParameter("email").toString();
                            String toolName = "Top Ranked Websites by Keyword";
                            Common.sendReportThroughMail(request, downloadFolder, dwnfileName, toAddrs, toolName);
                            return null;
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception for download paramter of post", e);
                }
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                Session userSession = (Session) session.getAttribute("userSession");
                String toolIssues = "";
                String questionInfo = "";
                questionInfo = "Need help in fixing following issues.\n\n";
                questionInfo += "- " + "Auditing high ranked competitor webpages for relevent keywords." + "\n";
                toolIssues += "Do you want a full audit of your high-ranking competitors for the keywords that you are targeting?";
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                lxrmAskExpert.setDomain("");
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString().trim());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(Common.TOP_RANKED_WEBSITES_BY_KEYWORD);
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                        if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), "", toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }
                return null;
            }
        }
        HashMap<String, String> geoLocation = Common.loadGeoLocations();
        model.addAttribute("geoLocation", geoLocation);
        HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
        model.addAttribute("loadLanguages", loadLanguages);
        topRankedWebsitesbyKeywordTool.setSearchKeyword("");
        topRankedWebsitesbyKeywordTool.setLocation("us");
        topRankedWebsitesbyKeywordTool.setLanguage("lang_en");
        return "views/topRankedWebsiteByKeyword/topRankedWebsiteByKeyword";
    }
    
}
