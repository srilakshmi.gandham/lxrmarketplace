package lxr.marketplace.toprankedwebsitesbykeyword;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.apiaccess.AhrefAPIService;
import lxr.marketplace.apiaccess.BingSearchAPI;
import lxr.marketplace.apiaccess.GoogleCustomSearch;
import lxr.marketplace.apiaccess.lxrmbeans.SearchOrganicLink;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONException;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopRankedWebsitesbyKeywordService {

    private static final Logger logger = Logger.getLogger(TopRankedWebsitesbyKeywordService.class);
    @Autowired
    private BingSearchAPI bingSearchAPI;
    @Autowired
    private GoogleCustomSearch googleCustomSearch;
    @Autowired
    private String googleCustomSearchApiKeyLxrm;
    @Autowired
    private AhrefAPIService ahrefAPIService;

    public List<TopRankedWebsitesbyKeywordResults> getGoogleResults(TopRankedWebsitesbyKeywordTool topRankedWebsitesbyKeywordTool, int proxyHost) {
        List<TopRankedWebsitesbyKeywordResults> googleResults = new ArrayList<>();
        try {
            ArrayList<SearchOrganicLink> top10Links = new ArrayList<>();
            googleCustomSearch.getGoogleSearchDataForaKeyword(googleCustomSearchApiKeyLxrm,
                    topRankedWebsitesbyKeywordTool.getSearchKeyword().trim(), top10Links,
                    topRankedWebsitesbyKeywordTool.getLocation(), topRankedWebsitesbyKeywordTool.getLanguage(), proxyHost);
            if (top10Links.size() > 0) {
                for (SearchOrganicLink searchOraganicLink : top10Links) {
                    TopRankedWebsitesbyKeywordResults topRankedWebsitesbyKeywordResults = new TopRankedWebsitesbyKeywordResults();
                    topRankedWebsitesbyKeywordResults.setResultURL(searchOraganicLink.getUrl());
                    topRankedWebsitesbyKeywordResults.setPosition(searchOraganicLink.getPosition());
                    String domName = getDomain(searchOraganicLink.getUrl());
                    topRankedWebsitesbyKeywordResults.setDomainName(domName);
                    try {
                        long domainBackLinks = ahrefAPIService.fetchNumberOfBacklinks(domName);
                        long urlBackLinks = ahrefAPIService.fetchNumberOfBacklinksForaURL(searchOraganicLink.getUrl());
                        topRankedWebsitesbyKeywordResults.setDomainBacklinkCount(domainBackLinks);
                        topRankedWebsitesbyKeywordResults.setUrlBacklinkCount(urlBackLinks);
                    } catch (Exception e) {
                        logger.error("Exception in preparing backlink metric data for google results: ", e);
                    }
//					int pagerank = googlePRandAlexaService.getGooglePR(searchOraganicLink.getUrl());
//					topRankedWebsitesbyKeywordResults.setPageRank(pagerank);
                    googleResults.add(topRankedWebsitesbyKeywordResults);
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getGoogleResults cause: " + e);
        }

        return googleResults;

    }

    public List<TopRankedWebsitesbyKeywordResults> getBingResults(TopRankedWebsitesbyKeywordTool topRankedWebsitesbyKeywordTool) {
        List<TopRankedWebsitesbyKeywordResults> bingResults = new ArrayList<>();
        ArrayList<SearchOrganicLink> top10Links = new ArrayList<>();
        String market = Common.loadcustomSearchLanguages().get(topRankedWebsitesbyKeywordTool.getLanguage()) + "-"
                + Common.loadGeoLocations().get(topRankedWebsitesbyKeywordTool.getLocation());
        String bingMarketValue = Common.loadFinalBingMarkets().get(market);

        try {
            bingSearchAPI.getBingSearchDataForaKeywordV5(topRankedWebsitesbyKeywordTool.getSearchKeyword().trim(), top10Links, bingMarketValue);
            if (top10Links.size() > 0) {
                for (SearchOrganicLink searchOraganicLink : top10Links) {
                    TopRankedWebsitesbyKeywordResults topRankedWebsitesbyKeywordResults = new TopRankedWebsitesbyKeywordResults();
                    topRankedWebsitesbyKeywordResults.setResultURL(searchOraganicLink.getUrl());
                    String domName = getDomain(searchOraganicLink.getUrl());
                    topRankedWebsitesbyKeywordResults.setDomainName(domName);
                    topRankedWebsitesbyKeywordResults.setPosition(searchOraganicLink.getPosition());
                    try {
                        long domainBackLinks = ahrefAPIService.fetchNumberOfBacklinks(domName);
                        long urlBackLinks = ahrefAPIService.fetchNumberOfBacklinksForaURL(searchOraganicLink.getUrl());
                        topRankedWebsitesbyKeywordResults.setDomainBacklinkCount(domainBackLinks);
                        topRankedWebsitesbyKeywordResults.setUrlBacklinkCount(urlBackLinks);
                    } catch (Exception e) {
                        logger.error("Exception in preparing backlink metric data for bing results: ", e);
                    }
//					int pagerank = googlePRandAlexaService.getGooglePR(searchOraganicLink.getUrl());
//					topRankedWebsitesbyKeywordResults.setPageRank(pagerank);
                    bingResults.add(topRankedWebsitesbyKeywordResults);
                }
            }
        } catch (JSONException | IOException e) {
            logger.error("JSONException | IOException in getBingResults cause: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception in getBingResults cause: ", e);
        }
        return bingResults;

    }

    public String getDomain(String tempURL) {
        String hostName = "";
        try {
            if (!tempURL.startsWith("http")) {
                tempURL = "http://".concat(tempURL);
            }
            URL urlObj = new URL(tempURL);
            hostName = urlObj.getHost();
            if (hostName.startsWith("www.")) {
                hostName = hostName.substring(4);
            }
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException in getDomain cause: " + e.getMessage());
        }
        return hostName.toLowerCase();
    }

    public String createPdfFile(String keyword, String targetFilePath,
            List<TopRankedWebsitesbyKeywordResults> googleResults, List<TopRankedWebsitesbyKeywordResults> bingResults, Login user) {
        logger.info("in side of createPdfFile.................. ");

        Font elementFont = new Font(FontFactory.getFont("Arial", 12f, Font.BOLD, new BaseColor(255, 110, 0)));
        Font reportNameFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, Font.BOLD, new BaseColor(255, 110, 0)));
        Font blackFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0)));

        String filename = "";
        try {
            String repName = "LXRMarketplace_Top_Ranked_Websites_byKeyword" + user.getId();
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            logger.info("in side of createPdfFile. filename................. " + filename);
            String filePath = targetFilePath + filename;
            logger.info("in side of createPdfFile. filePath................. " + filePath);
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            logger.info(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Top Ranked Websites by Keyword Report", reportNameFont);
            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Date: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(27f);
            rpName.setPaddingBottom(27f);
            rpName.setPaddingTop(6f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);
            Phrase googleResultheading = new Phrase("Google Results for Search query: " + keyword, elementFont);

            pdfDoc.add(googleResultheading);
            pdfDoc.add(Chunk.NEWLINE);
            float[] resultTabWidths = {15f, 35f, 12f, 13f, 12f};
            PdfPTable googleResultTab = new PdfPTable(resultTabWidths);
            googleResultTab.setWidthPercentage(99.9f);
            addCellstoTable(googleResultTab, googleResults, "Rank");
            pdfDoc.add(googleResultTab);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.newPage();
            Phrase bingResultheading = new Phrase("Bing Results for Search query: " + keyword, elementFont);
            pdfDoc.add(bingResultheading);
            pdfDoc.add(Chunk.NEWLINE);
            PdfPTable bingResultTab = new PdfPTable(resultTabWidths);
            bingResultTab.setWidthPercentage(99.9f);
            addCellstoTable(bingResultTab, bingResults, "Rank");
            pdfDoc.add(bingResultTab);
            pdfDoc.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return filename;
    }

    public void addCellstoTable(PdfPTable resultTab, List<TopRankedWebsitesbyKeywordResults> results, String searchEng) {
        Font elementDescFont = new Font(FontFactory.getFont("Arial", 10f, Font.BOLD, new BaseColor(0, 0, 0)));
        Font NoteParaFont = new Font(FontFactory.getFont("Arial", 10f, Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell domainHeader = new PdfPCell(new Phrase("Domain", elementDescFont));
        domainHeader.setBorderColor(BaseColor.GRAY);
        domainHeader.setBorderWidth(0.1f);
        domainHeader.setPadding(6f);
        PdfPCell urlHeader = new PdfPCell(new Phrase("URL", elementDescFont));
        urlHeader.setBorderColor(BaseColor.GRAY);
        urlHeader.setBorderWidth(0.1f);
        urlHeader.setPadding(6f);

//        PdfPCell pageRankHeader = new PdfPCell(new Phrase("Page Rank", elementDescFont));
//        pageRankHeader.setBorderColor(BaseColor.GRAY);
//        pageRankHeader.setBorderWidth(0.1f);
//        pageRankHeader.setPadding(6f);
        PdfPCell backLinksHeader = new PdfPCell(new Phrase("URL Backlink Count", elementDescFont));
        backLinksHeader.setBorderColor(BaseColor.GRAY);
        backLinksHeader.setBorderWidth(0.1f);
        backLinksHeader.setPadding(6f);
        PdfPCell domBackLinksHeader = new PdfPCell(new Phrase("Domain Backlink Count", elementDescFont));
        domBackLinksHeader.setBorderColor(BaseColor.GRAY);
        domBackLinksHeader.setBorderWidth(0.1f);
        domBackLinksHeader.setPadding(6f);
        PdfPCell googlePstnHeader = new PdfPCell(new Phrase(searchEng, elementDescFont));
        googlePstnHeader.setBorderColor(BaseColor.GRAY);
        googlePstnHeader.setBorderWidth(0.1f);
        googlePstnHeader.setPadding(6f);
        resultTab.addCell(domainHeader);
        resultTab.addCell(urlHeader);
//        resultTab.addCell(pageRankHeader);
        resultTab.addCell(backLinksHeader);
        resultTab.addCell(domBackLinksHeader);
        resultTab.addCell(googlePstnHeader);
        resultTab.completeRow();

        if (results != null && results.size() > 0) {
            for (TopRankedWebsitesbyKeywordResults result : results) {
                try {
                    PdfPCell domainVal = new PdfPCell(new Phrase(result.getDomainName() + "", NoteParaFont));
                    domainVal.setBorderColor(BaseColor.GRAY);
                    domainVal.setBorderWidth(0.1f);
                    domainVal.setPadding(6f);
                    PdfPCell urlVal = new PdfPCell(new Phrase(result.getResultURL() + "", NoteParaFont));
                    urlVal.setBorderColor(BaseColor.GRAY);
                    urlVal.setBorderWidth(0.1f);
                    urlVal.setPadding(6f);

//	        		 PdfPCell pageRankVal = new PdfPCell(new Phrase(result.getPageRank()+"", NoteParaFont));
//	                 pageRankVal.setBorderColor(BaseColor.GRAY);
//	                 pageRankVal.setBorderWidth(0.1f);
//	                 pageRankVal.setPadding(6f);
//	                 pageRankVal.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                    PdfPCell domBackLinksVal = new PdfPCell(new Phrase("" + result.getDomainBacklinkCount(), NoteParaFont));
                    domBackLinksVal.setBorderColor(BaseColor.GRAY);
                    domBackLinksVal.setBorderWidth(0.1f);
                    domBackLinksVal.setPadding(6f);
                    domBackLinksVal.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                    PdfPCell backLinksVal = new PdfPCell(new Phrase("" + result.getUrlBacklinkCount(), NoteParaFont));
                    backLinksVal.setBorderColor(BaseColor.GRAY);
                    backLinksVal.setBorderWidth(0.1f);
                    backLinksVal.setPadding(6f);
                    backLinksVal.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                    PdfPCell googlePositionVal = new PdfPCell(new Phrase("" + result.getPosition(), NoteParaFont));
                    googlePositionVal.setBorderColor(BaseColor.GRAY);
                    googlePositionVal.setBorderWidth(0.1f);
                    googlePositionVal.setPadding(6f);
                    googlePositionVal.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                    resultTab.addCell(domainVal);
                    resultTab.addCell(urlVal);
//	                 resultTab.addCell(pageRankVal);
                    resultTab.addCell(backLinksVal);
                    resultTab.addCell(domBackLinksVal);
                    resultTab.addCell(googlePositionVal);
                    resultTab.completeRow();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } else {
            try {
                PdfPCell noResult = new PdfPCell(new Phrase("No results found" + "", NoteParaFont));
                noResult.setBorderColor(BaseColor.GRAY);
                noResult.setBorderWidth(0.1f);
                noResult.setHorizontalAlignment(Element.ALIGN_CENTER);
                noResult.setColspan(6);
                noResult.setPadding(6f);
                resultTab.addCell(noResult);
                resultTab.completeRow();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

}
