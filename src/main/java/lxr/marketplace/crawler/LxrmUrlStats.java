package lxr.marketplace.crawler;

import java.util.Comparator;
import lxr.marketplace.mostusedkeywords.MostUsedKeyWordsResult;

public class LxrmUrlStats implements Comparable<LxrmUrlStats>{

	private String url;
	private int statusCode;
	private int internalLinkCount;
	private String statusMsg;
        private int linkDepthId;
        private MostUsedKeyWordsResult  mostUsedKeyWordsResult;
	private String baseUrl;
        

	public LxrmUrlStats(String url) {
		super();
		this.url = url;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public int getInternalLinkCount() {
		return internalLinkCount;
	}
	public void setInternalLinkCount(int internalLinkCount) {
		this.internalLinkCount = internalLinkCount;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }
	
	public int compareTo(LxrmUrlStats o) {
	    int sortVal1 = calculateSortVal(this);
        int sortVal2 = calculateSortVal(o);                     
        return sortVal2 - sortVal1; 	
	}
	
	public static Comparator<LxrmUrlStats> StatusComparatorD = new Comparator<LxrmUrlStats>() {
		public int compare(LxrmUrlStats stat1, LxrmUrlStats stat2) {
		    int sortVal1 = calculateSortVal(stat1);
		    int sortVal2 = calculateSortVal(stat2);		    		    
			return sortVal1 - sortVal2;	
		}
	};
	
	private static int calculateSortVal(LxrmUrlStats urlStats){
	    int sortVal = 0;
	    if(urlStats.getStatusCode() == 408){
	        sortVal = 6;
        }else if((urlStats.getStatusCode() >= 400 && urlStats.getStatusCode() < 500)){
            sortVal = 7;
        }else if(urlStats.getStatusCode() >= 500 && urlStats.getStatusCode() < 600){
            sortVal = 5;
        }else if(urlStats.getStatusCode() == 0){
            sortVal = 4;
        }else if(urlStats.getStatusCode() == 301){
            sortVal = 2;
        }else if(urlStats.getStatusCode() >= 300 && urlStats.getStatusCode() < 400){
            sortVal = 3;
        }else{
            sortVal = 1;
        }
	    return sortVal;
	}
	
	public static Comparator<LxrmUrlStats> URLComparatorU = new Comparator<LxrmUrlStats>() {
		public int compare(LxrmUrlStats stat1, LxrmUrlStats stat2) {
			return stat1.getUrl().compareTo(stat2.getUrl());
		}
	};
	public static Comparator<LxrmUrlStats> URLComparatorD = new Comparator<LxrmUrlStats>() {
		public int compare(LxrmUrlStats stat1, LxrmUrlStats stat2) {
			return stat2.getUrl().compareTo(stat1.getUrl());
		}
	};

    public MostUsedKeyWordsResult getMostUsedKeyWordsResult() {
        return mostUsedKeyWordsResult;
    }

    public void setMostUsedKeyWordsResult(MostUsedKeyWordsResult mostUsedKeyWordsResult) {
        this.mostUsedKeyWordsResult = mostUsedKeyWordsResult;
    }

    public int getLinkDepthId() {
        return linkDepthId;
    }

    public void setLinkDepthId(int linkDepthId) {
        this.linkDepthId = linkDepthId;
    }



}
