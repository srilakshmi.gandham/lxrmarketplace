package lxr.marketplace.crawler;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import lxr.marketplace.seoninja.data.NinjaURL;
import lxr.marketplace.seoninja.data.NinjaURLCrawlerUtil;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import crawlercommons.robots.BaseRobotRules;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.Map;
import lxr.marketplace.mostusedkeywords.MostUsedKeyWordsResult;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.findPhrase.PhraseStat;

public class LxrmUrlStatsService {

    private static Logger logger = Logger.getLogger(LxrmUrlStatsService.class);

//        public LxrmUrlStatsService(MostKeywordCheckerService mostUsedKeywordsService){
//            this.mostUsedKeywordsService = mostUsedKeywordsService;
//        }
    public void fetchStats(Document document, LxrmUrlStats urlStats, List<NinjaURL> internalURLs, String domain, BaseRobotRules baseRobotRules) {
//		logger.info("document"+document);
        if (document == null) {
            urlStats.setInternalLinkCount(0);
            return;
        }
        //domainId, url, fetchDate, statusCode, titlePresent, title, titleScore, 
        //metaKeywordPresent, metaKeywords, metaDescScore, hTagScore, h1Text, wordScore, wordCount
        //iframeCount, totalImageCount, imgWithAltTextCount, internalLinkCount, internalLinkScore
//		logger.info("Fetching stats for " + urlStats.getUrl());
        domain = domain.trim();
        if (domain.startsWith("http://")) {
            domain = domain.replace("http://", "");
        } else if (domain.startsWith("https://")) {
            domain = domain.replace("https://", "");
        }

        if (domain.endsWith("/")) {
            domain = domain.substring(0, domain.length() - 1);
        }

        Elements containedUrls = document.select("a[href]");
        if (containedUrls != null) {
            for (Element link : containedUrls) {
                boolean dontAdd = false;
                String childUrl = link.attr("href").trim();
                if (childUrl.startsWith("#")) {
                    dontAdd = true;
                } else if (!childUrl.trim().equals("/") && !childUrl.trim().equals("")) {
                    childUrl = link.attr("abs:href");
                } else {
                    dontAdd = true;
                }
                if (childUrl.trim().equals("")) {
                    dontAdd = true;
                }
                if (!dontAdd) {
//					logger.info("in dontAdd condition"+dontAdd);

                    try {
                        childUrl = childUrl.replace(" ", "%20");

                        URL url = new URL(childUrl);
//                        URI childURI = new URI(childUrl);
                        String nullFragment = null;
                        URI childURI = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), nullFragment);
                        String urlDomain = childURI.getHost();
//						logger.info("in urlDomain"+urlDomain);
//				
                        if (childURI.getPort() == 80) {
                            childUrl = childUrl.replace(childURI.getHost() + ":80", childURI.getHost());
                        }
                        if (urlDomain != null && urlDomain.equals(domain)) {
//							logger.info("in side of urldomain if condition");
                            NinjaURL childNinjaURL = new NinjaURL(childUrl, NinjaURL.TO_BE_ADDED);
                            childNinjaURL.setBaseUrl(urlStats.getUrl());
                            internalURLs.add(childNinjaURL);
                            NinjaURL ninjaBaseUrl = internalURLs.stream().filter(n -> n.getBaseUrl().equals(urlStats.getUrl()))
                                    .findAny().orElse(null);
                            if (ninjaBaseUrl != null) {
                                childNinjaURL.setLinkDepthId(ninjaBaseUrl.getLinkDepthId() + 1);
                            }
                        }
                    } catch (MalformedURLException e) {
                        logger.error("URL " + childUrl + " is a malformed URL", e);
                    } catch (URISyntaxException e) {
                        logger.error("URI Syntax Exception when trying to add in crawling URLs Set- URL: " + link.attr("href").trim() + " Abs URL: " + childUrl + "", e);
                    }
                }
            }
        }
        int internalLinkCount = internalURLs.size();
        urlStats.setInternalLinkCount(internalLinkCount);

        Elements metaRefreshElements = document.select("head meta[http-equiv=refresh]");
        for (Element metaElement : metaRefreshElements) {
            String refreshProp = metaElement.attr("content");
            if (refreshProp != null && !refreshProp.equals("")) {
                String[] props = refreshProp.split(";\\s*");
                if (props.length > 0) {
                    for (String prop : props) {
                        if (prop.toLowerCase().contains("url")) {
                            String refreshLink = prop.replace("URL=", "").trim();
                            String finalLink = refreshLink;
                            if (finalLink != null && !finalLink.startsWith("http")) {
                                finalLink = "http://" + finalLink;
                            }
                            if (finalLink != null) {
                                boolean addLink = false;
                                try {
                                    URL uri = new URL(finalLink);
                                    if (finalLink.contains(domain)) {
                                        addLink = true;
                                    } else if (!uri.getHost().contains(".")) {
                                        if (domain.endsWith("/") || refreshLink.startsWith("/")) {
                                            finalLink = domain + refreshLink;
                                        } else {
                                            finalLink = domain + "/" + refreshLink;
                                        }
                                        addLink = true;
                                    }
                                } catch (Exception e) {
                                    if (domain.endsWith("/") || refreshLink.startsWith("/")) {
                                        finalLink = domain + refreshLink;
                                    } else {
                                        finalLink = domain + "/" + refreshLink;
                                    }
                                    addLink = true;
                                }
                                if (addLink) {
                                    NinjaURL refreshURL = new NinjaURL(finalLink, NinjaURL.TO_BE_ADDED);
                                    internalURLs.add(refreshURL);
                                }
                                break;
                            }
                        }
                    }
                }
            }

        }
        NinjaURLCrawlerUtil.filterCrawlingURLs(internalURLs, baseRobotRules);

    }

    public void addURLStatsInaList(List<LxrmUrlStats> lxrmUrlStatsList, final LxrmUrlStats urlStats) {
        synchronized (lxrmUrlStatsList) {
            lxrmUrlStatsList.add(urlStats);
        }
    }

    public void fetchKeywordMetric(MostUsedKeyWordsResult keywordMetric, Document urlDoc, List<String> stopWordList) {
        String bodyText = null;
        List<Map<String, PhraseStat>> relevantKeywords = null;
        if (urlDoc != null) {
            /*Calculating Text/HTML Ratio for a url*/
            keywordMetric.setTextCodeRatio(Common.getTextHtmlratio(urlDoc));
            bodyText = urlDoc.select("body").text();
            if (bodyText != null) {
                /*Considering all  keywords and stop words  to calculate total keywords of a page from body text.*/
                keywordMetric.setKeywordsCount((int) Common.getTotalWordCount(bodyText));
            }
        }
        Map<String, Integer> topKeywords = null;
        Map<String, Integer> totalKeywords = new LinkedHashMap<>();
        relevantKeywords = Common.getMeaningFullPhrases(bodyText, stopWordList);
        Map<String, PhraseStat> pharsesMap = null;
        if (relevantKeywords.size() >= 4) {
            relevantKeywords = relevantKeywords.subList(0, 3);
        }
        for (int i = 0; i < relevantKeywords.size(); i++) {
            if (relevantKeywords.get(i).size() > 0) {
                pharsesMap = relevantKeywords.get(i);
                /*Considering only 1,2,3 keywords pharses  to fetch top  2 keywords for top keywords of a page.*/
                for (Map.Entry<String, PhraseStat> WordsEntry : pharsesMap.entrySet()) {
                    String keyWord = WordsEntry.getKey();
                    PhraseStat phraseStat = WordsEntry.getValue();
                    /*Merging all pharses keywords into single map object to calculate top keywords of a website.*/
                    totalKeywords.put(keyWord, phraseStat.count);
                }
            }
        }
        topKeywords = Common.getMaxElementsFromList(relevantKeywords, 2, 6);
        /*Setting top keywords.*/
        keywordMetric.setTopKeyWords(topKeywords.keySet());
        /*Setting total keywords.*/
        keywordMetric.setTotalKeywords(totalKeywords);
    }

}
