package lxr.marketplace.crawler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lxr.marketplace.seoninja.data.LXRSEOURLFetchThread;
import lxr.marketplace.seoninja.data.NinjaURL;
import lxr.marketplace.seoninja.data.NinjaURLCrawlerUtil;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import crawlercommons.robots.BaseRobotRules;
import lxr.marketplace.mostusedkeywords.MostUsedKeyWordsResult;
import lxr.marketplace.sitemapbuilder.SiteMapBuilderService;
import org.springframework.beans.factory.annotation.Autowired;

public class LxrmUrlFecthThread implements Runnable {

    private static Logger logger = Logger.getLogger(LXRSEOURLFetchThread.class);
    private NinjaURL ninjaURL;
    private LxrmUrlStatsService urlStatsService;
    private NinjaWebStats ninjaWebStats;
    private static BaseRobotRules baseRobotRules;
    private List<LxrmUrlStats> lxrmUrlStatsList;

    Set<NinjaURL> crawlingURLs;
    Set<NinjaURL> urlQueue;

    private String crawlWebsite;
    private List<String> stopWords;

    @Autowired
    SiteMapBuilderService siteMapBuilderService;

    public void setUrlStatsService(LxrmUrlStatsService urlStatsService) {
        this.urlStatsService = urlStatsService;
    }

    public void setNinjaURL(NinjaURL ninjaURL) {
        this.ninjaURL = ninjaURL;
    }

    public static BaseRobotRules getBaseRobotRules() {
        return baseRobotRules;
    }

    public static void setBaseRobotRules(BaseRobotRules baseRobotRules) {
        LxrmUrlFecthThread.baseRobotRules = baseRobotRules;
    }

    public LxrmUrlFecthThread(NinjaURL ninjaURL, LxrmUrlStatsService urlStatsService,
            Set<NinjaURL> crawlingURLs, Set<NinjaURL> urlQueue, NinjaWebStats ninjaWebStats, List<LxrmUrlStats> lxrmUrlStatsList, String crawlWebsite, List<String> stopWords) {
        super();
        this.ninjaURL = ninjaURL;
        this.urlStatsService = urlStatsService;
        this.crawlingURLs = crawlingURLs;
        this.urlQueue = urlQueue;
        this.ninjaWebStats = ninjaWebStats;
        this.lxrmUrlStatsList = lxrmUrlStatsList;

        this.crawlWebsite = crawlWebsite;
        this.stopWords = stopWords;
    }

//    @Override
    public void run() {
        synchronized (ninjaURL) {
            ninjaURL.setCrawlStatus(NinjaURL.CONNECTING);
            ninjaURL.notifyAll();
        }
        StringBuilder pageHTML = new StringBuilder();
        String statusMessage = NinjaURLCrawlerUtil.checkCurrentURLAndGetHTML(ninjaURL, pageHTML);
//        logger.info("URL: "+ninjaURL.getUrl());
        if (!statusMessage.equals("too many redirection") && !statusMessage.equals("invalid")) {
            boolean processDoc = true;  //False when URL redirecting to some other domain or redirected URL already there in the queue
            boolean processRedirect = false;//True when URL is redirected to some other URL from the same domain (not only change in protocol)
            NinjaURL redirectedURL = null;
            if (ninjaURL.getUrlStatus() >= 300 && ninjaURL.getUrlStatus() < 400) {
                try {
                    URI redirectedURI = new URI(ninjaURL.getRedirectedURL());
                    if (redirectedURI.getPort() == 80) {  //sometimes default port 80 appears in url after domain name
                        String newRedirectedURL = ninjaURL.getRedirectedURL().replace(redirectedURI.getHost() + ":80",
                                redirectedURI.getHost());
                        ninjaURL.setRedirectedURL(newRedirectedURL);
                    }
                } catch (URISyntaxException e1) {
                    logger.error("In run LxrmUrlFecthThread method of LxrmUrlFecthThread class" + e1.getMessage());
                }

                /* Checking if redirected URL only differs in protocol */
                String tempURL = ninjaURL.getUrl();
                String tempRedirectedURL = ninjaURL.getRedirectedURL();
                if (tempURL.startsWith("http://")) {
                    tempURL = tempURL.replace("http://", "");
                } else if (tempURL.startsWith("https://")) {
                    tempURL = tempURL.replace("https://", "");
                }

                if (tempRedirectedURL.startsWith("http://")) {
                    tempRedirectedURL = tempRedirectedURL.replace("http://", "");
                } else if (tempRedirectedURL.startsWith("https://")) {
                    tempRedirectedURL = tempRedirectedURL.replace("https://", "");
                }

                if (!tempURL.equals(tempRedirectedURL)) {

                    /* Domain Checking of redirected URL */
                    boolean sameDomain = true;
                    String currentDomain = "", redirectedDomain = "";
                    try {
                        currentDomain = Common.getDomainName(ninjaURL.getUrl());
                        redirectedDomain = Common.getDomainName(ninjaURL.getRedirectedURL());
                    } catch (URISyntaxException e) {
                        logger.error(e.getMessage());
                    }
                    if (!currentDomain.equals(redirectedDomain)) { //redirected url is from some other domain
                        synchronized (ninjaURL) {
                            ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
                            ninjaURL.notifyAll();
                        }
                        sameDomain = false;
                        processDoc = false;
                    }

                    if (sameDomain) {
                        synchronized (crawlingURLs) {
                            /*  Storing Current URL Data in database  */
                            synchronized (ninjaURL) {
                                ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
                                ninjaURL.notifyAll();
                            }

                            redirectedURL = new NinjaURL(ninjaURL.getRedirectedURL());
                            if (crawlingURLs.contains(redirectedURL)) {   // redirected url already completed or being processed in threadpool                        
                                processDoc = false;
                                LxrmUrlStats urlStats = new LxrmUrlStats(ninjaURL.getUrl());
                                urlStats.setBaseUrl(ninjaURL.getBaseUrl());
                                urlStats.setStatusCode(ninjaURL.getUrlStatus());
                                urlStats.setLinkDepthId(ninjaURL.getLinkDepthId());
                                urlStatsService.addURLStatsInaList(lxrmUrlStatsList, urlStats);
                            } else { //otherwise add to crawling list and process the redirected url 
                                redirectedURL.setCrawlStatus(NinjaURL.CONNECTING);
                                redirectedURL.setRedirectedURL(ninjaURL.getRedirectedURL());
                                redirectedURL.setUrlStatus(Common.getURLStatus(ninjaURL.getRedirectedURL()));
                                crawlingURLs.add(redirectedURL);
                                processRedirect = true;
                            }
                            crawlingURLs.notifyAll();
                        }
                    }
                }
            }

            if (processDoc) { //url is not redirected or  url is redirected but not being processed by any other thread
                Document urlDoc = null;
                LxrmUrlStats urlStats = null;
                MostUsedKeyWordsResult mostUsedKeywords = null;
                List<NinjaURL> internalURLs = new ArrayList<>();
                if (processRedirect) { //URL is redirected to some other URL from the same domain (not only change in protocol)
                    urlStats = new LxrmUrlStats(redirectedURL.getRedirectedURL());
                    urlStats.setStatusCode(redirectedURL.getUrlStatus());
                    urlStats.setBaseUrl(ninjaURL.getBaseUrl());
                    urlStats.setLinkDepthId(ninjaURL.getLinkDepthId());

                    if (!pageHTML.toString().equals("")) {
                        String httpDomainUrl = "";
                        if (ninjaWebStats.getDomain().trim().startsWith("http://")) {
                            httpDomainUrl = ninjaWebStats.getDomain().trim();
                        } else {
                            httpDomainUrl = "http://" + ninjaWebStats.getDomain().trim();
                        }
                        urlDoc = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
                    } else {
                        urlDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedURL);
                    }
                    if (urlDoc != null) {
                        urlStatsService.fetchStats(urlDoc, urlStats, internalURLs, ninjaWebStats.getDomain(), getBaseRobotRules());

                        if (crawlWebsite.equals(Common.MOST_USED_KEYWORDS_DENSITY)) {
                            /*Fetching most used keywords for working and permenant urls.*/
                            if (((urlStats.getStatusCode() == 301) || (urlStats.getStatusCode() == 200))
                                    && (urlStats.getMostUsedKeyWordsResult() == null)) {
//                                String keywordUrl = Common.getPathFromUrl(urlStats.getUrl());
//                                if (!keywordUrl.equals("")) {
                                    mostUsedKeywords = new MostUsedKeyWordsResult(urlStats.getUrl());
                                    urlStatsService.fetchKeywordMetric(mostUsedKeywords, urlDoc, stopWords);
                                    urlStats.setMostUsedKeyWordsResult(mostUsedKeywords);
//                                }
                            }
                        }
                        urlStatsService.addURLStatsInaList(lxrmUrlStatsList, urlStats);
                        synchronized (urlQueue) {
                            urlQueue.addAll(internalURLs);
                            urlQueue.notifyAll();
                        }
                    }/*EOF of urlDoc */
                    synchronized (redirectedURL) {
                        redirectedURL.setCrawlStatus(NinjaURL.COMPLETED);
                        redirectedURL.notifyAll();
                    }
                } else {
                    urlStats = new LxrmUrlStats(ninjaURL.getUrl());
                    //urlStats = new LxrmUrlStats(ninjaURL.getRedirectedURL());
                    urlStats.setStatusCode(ninjaURL.getUrlStatus());
                    urlStats.setBaseUrl(ninjaURL.getBaseUrl());
                    urlStats.setLinkDepthId(ninjaURL.getLinkDepthId());
                    if (pageHTML != null && !pageHTML.toString().equals("")) {
                        String httpDomainUrl = "";
                        if (ninjaWebStats.getDomain().trim().startsWith("http://")) {
                            httpDomainUrl = ninjaWebStats.getDomain().trim();
                        } else {
                            httpDomainUrl = "http://" + ninjaWebStats.getDomain().trim();
                        }
                        urlDoc = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
                    } else {
                        urlDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedURL);
                    }
                    if (urlDoc != null) {
                        urlStatsService.fetchStats(urlDoc, urlStats, internalURLs, ninjaWebStats.getDomain(), getBaseRobotRules());
                        if (crawlWebsite.equals(Common.MOST_USED_KEYWORDS_DENSITY)) {
                            /*Fetching most used keywords for working and permenant urls.*/
                            if (((urlStats.getStatusCode() == 301) || (urlStats.getStatusCode() == 200))
                                    && (urlStats.getMostUsedKeyWordsResult() == null)) {
//                                String keywordUrl = Common.getPathFromUrl(urlStats.getUrl());
//                                if (!keywordUrl.equals("")) {
                                    mostUsedKeywords = new MostUsedKeyWordsResult(urlStats.getUrl());
                                    urlStatsService.fetchKeywordMetric(mostUsedKeywords, urlDoc, stopWords);
                                    urlStats.setMostUsedKeyWordsResult(mostUsedKeywords);
//                                }
                            }
                        }
                        urlStatsService.addURLStatsInaList(lxrmUrlStatsList, urlStats);
                        synchronized (urlQueue) {
                            urlQueue.addAll(internalURLs);
                            urlQueue.notifyAll();
                        }
                    }
                    synchronized (ninjaURL) {
                        ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
                        ninjaURL.notifyAll();
                    }
                }
            }
        } else {
            logger.info("URL with too many redirection or 404 or other url status: " + ninjaURL.getUrl() + " and status code is " + ninjaURL.getUrlStatus());
            LxrmUrlStats urlStats = new LxrmUrlStats(ninjaURL.getUrl());
            urlStats.setStatusCode(ninjaURL.getUrlStatus());
            urlStats.setBaseUrl(ninjaURL.getBaseUrl());
            urlStats.setLinkDepthId(ninjaURL.getLinkDepthId());
            urlStatsService.addURLStatsInaList(lxrmUrlStatsList, urlStats);
            synchronized (ninjaURL) {
                ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
                ninjaURL.notifyAll();
            }
        }
        try {
            Thread.sleep(1000); //Sleeping 1 seconds to give some time for the next crawl
        } catch (InterruptedException e) {
        }
    }

}
