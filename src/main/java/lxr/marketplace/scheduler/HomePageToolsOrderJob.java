/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.scheduler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author anil
 */
public class HomePageToolsOrderJob extends QuartzJobBean {

    final private Logger logger = Logger.getLogger(HomePageToolsOrderJob.class);

    
    private ToolService toolService;
    
    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public static ApplicationContext getContext(HttpServletRequest httpRequest) {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(
                httpRequest.getSession().getServletContext());
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        /**
         * HomePageToolsOrderJob will fetch the tools order based on usage and
         * inserting into lxrm_tool_sorting, Individual tool usage count and
         * lxrm over all usage stats
         */
        logger.info("Home page tools sorting scheduler starts");

        //Saving home page tools order based on usage in lxrm_tool_sorting
        boolean isHomepageToolsInseted = toolService.saveHompageToolsOrderByUsage();
        logger.info("isHomepageToolsInseted:: "+isHomepageToolsInseted);
        if (!isHomepageToolsInseted) {
            String schedulerName = "Home page tools sorting scheduler Failed.";
            String subjectLine = "Home page tools sorting scheduler Failed";
            SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
        }

        ServletContext servletContext = (ServletContext) jec.getJobDetail().getJobDataMap().get("servletContext");
        //Fetching list of tool object with all details 
        servletContext.setAttribute("tools", toolService.populateTools());
        //Fetching LXRM usage stats
        servletContext.setAttribute("lxrmUserStatstics", toolService.getLXRMUserStatstics());
        
        logger.info("Completed home page tools sorting scheduler.");

    }

}
