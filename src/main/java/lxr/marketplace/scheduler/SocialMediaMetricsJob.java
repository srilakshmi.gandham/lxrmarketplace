package lxr.marketplace.scheduler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lxr.marketplace.socialmedia.ChannelUserNames;
import lxr.marketplace.socialmedia.SocialCompDomains;
import lxr.marketplace.socialmedia.SocialMediaService;

import lxr.marketplace.socialmedia.SocialMediaUserInfoTask;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class SocialMediaMetricsJob extends QuartzJobBean {

    private static Logger logger = Logger.getLogger(SocialMediaMetricsJob.class);

    private ThreadPoolExecutor socialMediaThreadPoolExecutor = null;
    private final int poolSize = 10;
    private final int maxPoolSize = 50;
    private final long keepAliveTime = 360;
    private boolean terminatedSocialMediaThraed = false;
    private final LinkedBlockingQueue<Runnable> socialMediaQue = new LinkedBlockingQueue<>();

    private JdbcTemplate jdbcTemplate;

    private SocialMediaService socialMediaService;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SocialMediaService getSocialMediaService() {
        return socialMediaService;
    }

    public void setSocialMediaService(SocialMediaService socialMediaService) {
        this.socialMediaService = socialMediaService;
    }

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

        logger.info("********* Social Media Analyzer scheduler started *********");

        boolean dataUpdated = Boolean.FALSE;
        boolean isDataContain = Boolean.FALSE;
        SqlRowSet sqlRowSet = getListOfActiveUsers();
        int strtindex = 0;
        if (sqlRowSet != null) {
            isDataContain = sqlRowSet.isBeforeFirst();
        }
        logger.info("Social media metrics job isDataContains:: " + isDataContain);
        if (isDataContain && sqlRowSet != null) {
            socialMediaThreadPoolExecutor = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, socialMediaQue);
            Map<Login, List<SocialCompDomains>> domainsByUserId = new HashMap();
            SocialCompDomains domainSetting = null;
            ChannelUserNames chUserNames = null;
            Login loginObj = null;
            String userName = null;
            String email = null;
            try {
                while (sqlRowSet.next()) {
                    try {
                        domainSetting = new SocialCompDomains();
                        domainSetting.setDomainId(sqlRowSet.getLong("id"));
                        if (sqlRowSet.getString("url") != null) {
                            domainSetting.setDomain(sqlRowSet.getString("url"));
                        }
                        domainSetting.setUserDomain(sqlRowSet.getBoolean("is_userdomain"));
                        domainSetting.setIsLoggedIn(sqlRowSet.getBoolean("is_logged_in"));
                        domainSetting.setIsDeleted(sqlRowSet.getBoolean("is_deleted"));
                        domainSetting.setDomainRegDate(sqlRowSet.getTimestamp("user_date"));
                        domainSetting.setUnSubScribeStatus(sqlRowSet.getBoolean("tool_unsubscribe"));
                        domainSetting.setWeeklyReport(sqlRowSet.getBoolean("weekly_report"));

                        chUserNames = new ChannelUserNames();
                        chUserNames.setChannelRowId(sqlRowSet.getLong("user_name_id"));
                        chUserNames.setSocialMediaUserId(sqlRowSet.getLong("media_user_id"));
                        if (sqlRowSet.getString("twitter_name") != null) {
                            chUserNames.setTwitterName(sqlRowSet.getString("twitter_name"));
                        }
                        if (sqlRowSet.getString("facebook_name") != null) {
                            chUserNames.setFacebookName(sqlRowSet.getString("facebook_name"));
                        }
                        if (sqlRowSet.getString("youtube_name") != null) {
                            chUserNames.setYoutubeName(sqlRowSet.getString("youtube_name"));
                        }
//                    chUserNames.setInstagramName(sqlRowSet.getString("instagram_name"));
//                    chUserNames.setPinterestName(sqlRowSet.getString("pinterest_name"));
//                    chUserNames.setGooglePlusName(sqlRowSet.getString("goolgePlus_name"));

                        domainSetting.setChannelNames(chUserNames);

                        long userId = sqlRowSet.getLong("user_id");
                        userName = sqlRowSet.getString("username");
                        email = sqlRowSet.getString("email");
                        if (email != null && email.contains("GuestUser")) {
                            String guestEmail = "GuestUser" + userId + "<";
                            strtindex = guestEmail.length();
                            email = email.substring(strtindex, email.lastIndexOf(">"));
                        }

                        loginObj = new Login();
                        loginObj.setId(userId);
                        if (userName != null) {
                            loginObj.setName(userName);
                        }
                        if (email != null) {
                            loginObj.setUserName(email);
                        }
                        if (domainsByUserId.get(loginObj) != null) {
                            domainsByUserId.get(loginObj).add(domainSetting);
                        } else {
                            List<SocialCompDomains> listDomain = new ArrayList();
                            listDomain.add(domainSetting);
                            domainsByUserId.put(loginObj, listDomain);
                        }
                    } catch (Exception e) {
                        logger.info("Current Social meida analyzer job user Map size:: " + domainsByUserId.size());
                        logger.error("Exception when preparing the map of user objects for social media analyzer job :: ", e);
                        e.printStackTrace();
                    }
                }
                logger.info("Users count to get social media metrics in scheduler:: " + domainsByUserId.size());
                domainsByUserId.keySet().stream().forEach(login -> {
                    SocialMediaUserInfoTask socialMediaTask = new SocialMediaUserInfoTask(login.getUserName(), login.getName(), login.getId(), domainsByUserId.get(login), getSocialMediaService());
                    socialMediaThreadPoolExecutor.execute(socialMediaTask);
                });

                while (!terminatedSocialMediaThraed) {
                    try {
                        int active = socialMediaThreadPoolExecutor.getActiveCount();
                        int qued = socialMediaThreadPoolExecutor.getQueue().size();
                        if ((active + qued) > 0) {
                            Thread.sleep(2000);
                        } else {
                            socialMediaThreadPoolExecutor.shutdownNow();
                            terminatedSocialMediaThraed = Boolean.TRUE;
                        }
                    } catch (InterruptedException ex) {
                        logger.error("Exception when termnating thread:", ex);
                    }
                }
                dataUpdated = Boolean.TRUE;
            } catch (InvalidResultSetAccessException e) {
                logger.error("InvalidResultSetAccessException while preparing SMA user accounts cause: ", e);
                e.printStackTrace();
            } catch (Exception e) {
                logger.error("Exception while preparing SMA user accounts cause: " + e);
                e.printStackTrace();
            }
        }
        String schedulerName = isDataContain && dataUpdated ? "Social Media Analyzer  scheduler has been successfully fired." : "Social Media Analyzer  scheduler failed.";
        String subjectLine = isDataContain && dataUpdated ? "Social Media Analyzer scheduler fired successfully" : "Social Media Analyzer  scheduler failed.";
        SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
        logger.info("********* Social Media Analyzer scheduler finished *********");
    }

    private SqlRowSet getListOfActiveUsers() {
        SqlRowSet sqlRowSet = null;
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String currentDate = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
        String schedulerQuery = "select r1.*,r2.* from(select social_user.* ,user.email as email,user.name as username from"
                + "(select * from social_media_user where date_format(user_date,'%Y-%m-%d')"
                + "<= '" + currentDate + "' and is_logged_in = 1 and tool_unsubscribe = 0 and is_deleted = 0 order by id)"
                + " social_user,user where social_user.user_id = user.id and user.name IS NOT NULL)r1"
                + " left join(select *,max(insert_date) from social_channel_user_names group by media_user_id) r2"
                + " on r1.id=r2.media_user_id";
        logger.info("Query to fetch users for social media analyzer scheduler:" + schedulerQuery);
        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(schedulerQuery);
        } catch (DataAccessException e) {
            logger.error("DataAccessException on fetching social media users list:", e);
            e.printStackTrace();
        } catch (Exception e) {
            logger.error("Exception on fetching social media users list:", e);
            e.printStackTrace();
        }
        return sqlRowSet;
    }
}
