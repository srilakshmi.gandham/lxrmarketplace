package lxr.marketplace.scheduler;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import lxr.marketplace.admin.automail.AutomailService;
import lxr.marketplace.admin.automail.EmailTemplate;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.admin.automail.EmailTrackingInfoService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AdminAutoMailJob extends QuartzJobBean{ 
	private static Logger logger = Logger.getLogger(AdminAutoMailJob.class);
	private EmailTemplateService emailTemplateService;
	private AutomailService automailService;
	private LoginService loginService;
	EmailTrackingInfoService emailTrackingInfoService;
		
	public void setAutomailService(AutomailService automailService) {
		this.automailService = automailService;
	}
	
	public EmailTrackingInfoService getEmailTrackingInfoService() {
		return emailTrackingInfoService;
	}
	public void setEmailTrackingInfoService(
			EmailTrackingInfoService emailTrackingInfoService) {
		this.emailTrackingInfoService = emailTrackingInfoService;
	}
	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public EmailTemplateService getEmailTemplateService() {
		return emailTemplateService;
	}

	public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
		this.emailTemplateService = emailTemplateService;
	}

	protected void executeInternal(JobExecutionContext context)
	throws JobExecutionException {
		logger.info("************scheduler Started for admin automail job*******************");
		
		Calendar currCal = Calendar.getInstance();
		int hour = currCal.get(Calendar.HOUR_OF_DAY);
		int minute = currCal.get(Calendar.MINUTE)/30;
		Time time = new Time((hour*60+minute*30)*60*1000);
		List<EmailTemplate> allTemplates = emailTemplateService.getTemplatesForScheduler(time);
		
		
		for(EmailTemplate et : allTemplates){
			if(et.getCategory() == 3 || et.getCategory() == 4){
//				List<Login> usersList = null;
//				Map<String,Login> userMap = null;
//				//@DEPENDS ON USER OPTION SEND MAIL
//				if(et.getSendMailOption() == 1){
//					List<Login> allUsersList = loginService.getUsersList();
//					usersList = allUsersList;
//				}else if(et.getSendMailOption() == 2){
//					List<Login> allUsersListExceptLocalUsers = loginService.getAllUsersExceptLocalUser();
//					usersList = allUsersListExceptLocalUsers;
//				}else if(et.getSendMailOption() == 3){
//					List<Login> listOfUsers = (List<Login>)session.getAttribute("listOfUsers");
//					if(listOfUsers == null || listOfUsers.size() < 0){
//						 automailService.getUserListFromUploadedFile(session,automailUserListFolder+"temporary/"+et.getUserListFile());
//						listOfUsers = (List<Login>)session.getAttribute("listOfUsers");
//					}
//					 userMap = (Map<String,Login>)session.getAttribute("userMap");
//					usersList = listOfUsers;
//				}
//				if(usersList !=null && usersList.size() > 0){
//					automailService.sendMailIfOnDemand(et,usersList,userMap);
//				}
				
				
				String fromAddress = automailService.ftechFromAddress(et);
				if(fromAddress==null || fromAddress.trim().equals("")){
                                    /* Commented with reference to mail from Pavan Sir
                                        fromAddress = "LXRMarketplace Team<team@lxrmarketplace.com>";*/
                                        fromAddress = "LXRMarketplace Support<support@lxrmarketplace.com>";
				}
				if(et.getCategory() == 3){
					sendScheduledMail(et,fromAddress);				
				}else if(et.getCategory() == 4){
					sendOncePerEventMail(et,fromAddress);
				}
			}
		}
		
	}
	
	public void sendScheduledMail(EmailTemplate emailTemplate,String fromAddress){
		Calendar calToday =  Calendar.getInstance();
		calToday.set(Calendar.HOUR, 0);
		calToday.set(Calendar.HOUR_OF_DAY, 0);
		calToday.set(Calendar.MINUTE, 0);
		calToday.set(Calendar.SECOND, 0);
		calToday.set(Calendar.MILLISECOND, 0);
		int count = 1;
		int dayOfWeek = calToday.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek == 2){   // dayOfWeek == 2 means Monday we are not sending mails on saturday and sunday
								//to send the mails which are scheduled on saturday ,sunday and monday we are passing count as 3
			count = 3;
		}
		
		for(int i = count - 1; i >= 0; i--){
			long schDay = calToday.getTimeInMillis() - i* 24 * 60* 60 * 1000;	
			long endday = emailTemplate.getEndDay().getTimeInMillis();
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(schDay); 
			
			long startday = emailTemplate.getStartDay().getTimeInMillis();
			long diff = (schDay - startday) / (24 * 60 * 60 * 1000);
			logger.debug(" diff val is............."+diff);
			String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime());
			String query = "";
			if(emailTemplate.getSendMailOption()==2){
				query = "select * from user where is_active=1 and is_admin =0 and act_date <='"+date+"' and unsubscribe=0 and is_local=0";
			}else{
				query = "select * from user where is_active=1 and is_admin =0 and act_date <='"+date+"' and unsubscribe=0";
			}
			List<Login> allUsers = loginService.getUsersByQuery(query);
			if(emailTemplate.isSendOnToday()){
				if(diff==0){
					if(allUsers.size()>0){
						emailTrackingInfoService.addEventtoDB(emailTemplate);//To add event in event_details table for tracking the event	
					}
					for(Login user: allUsers){
						  if(fromAddress!=null && !fromAddress.trim().equals("")){
							  new SendMail().sendAdminMail(emailTemplate, user,fromAddress);
						  }
					}
					if(allUsers.size() > 0){
						emailTemplate.setLastMailSent(Calendar.getInstance());
						emailTemplateService.updateLastMailSentDate(emailTemplate);
					}
				}
			}else{
				if(schDay > endday){
					emailTemplate.setStatus(false);
					emailTemplateService.updateTemplateStatus(emailTemplate);
					return;
				}
				
				if(diff % emailTemplate.getDaysGap() == 0){
					if(allUsers.size()>0){
						emailTrackingInfoService.addEventtoDB(emailTemplate);//To add event in event_details table for tracking the event	
					}
					for(Login user: allUsers){
						  if(fromAddress!=null && !fromAddress.trim().equals("")){
							  new SendMail().sendAdminMail(emailTemplate, user,fromAddress);
						  }
					}
					if(allUsers.size() > 0){
						emailTemplate.setLastMailSent(Calendar.getInstance());
						emailTemplateService.updateLastMailSentDate(emailTemplate);
					}		
				}
			}
		}		
	}
	
	public void sendOncePerEventMail(EmailTemplate emailTemplate,String fromAddress){
		int event = emailTemplate.getEvent();
		int daysGap = emailTemplate.getDaysGap();
		Calendar today = Calendar.getInstance();
		int count = 1;
		int dayOfWeek = today.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek == 2){
			count = 3;
		}
		for(int i = 0; i < count; i++){
			Calendar schDay = Calendar.getInstance();
			schDay.setTimeInMillis(today.getTimeInMillis() - i* 24 * 60* 60 * 1000);
			String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(schDay.getTime());
			List<Login> selectedUsers = getUsersByEvent(event, daysGap, date);
			if(selectedUsers.size()>0){
				emailTrackingInfoService.addEventtoDB(emailTemplate);//To add event in event_details table for tracking the event	
			}
			for(Login user: selectedUsers){
				new SendMail().sendAdminMail(emailTemplate, user,fromAddress);
			}
			if(selectedUsers.size() > 0){
				emailTemplate.setLastMailSent(Calendar.getInstance());
				emailTemplateService.updateLastMailSentDate(emailTemplate);
			}
		}
		
	}
	
	public List<Login>  getUsersByEvent(int event, int daysGap, String date){
		if(event == 1){
			return getUsersForActivationEvent(daysGap, date);
		}else if(event == 2){
			return getUsersForFirstLogin(daysGap, date);//for first login or  for first sign up
		}else if(event == 3){
			return getUsersForFirstToolUsage(daysGap, date);
		}else{
			return null;
		}
	}
	
	public List<Login> getUsersForActivationEvent(int daysGap, String date){
		String query = "select * from user where is_admin =0 and DATEDIFF('"+date+"', act_date) = "+daysGap+" and unsubscribe=0";
		List<Login> users = loginService.getUsersByQuery(query);
		return users;
	}
	
	public List<Login> getUsersForFirstLogin(int daysGap, String date){
		String query = "select u.* from (select user_id,  MIN(start_time) as minST " +
				"from session group by user_id)min_val, user u where min_val.user_id = u.id " +
				"and DATEDIFF('"+date+"', min_val.minST) = "+daysGap+" and unsubscribe=0" ;
		List<Login> users = loginService.getUsersByQuery(query);
		return users;
	}
	
	public List<Login> getUsersForFirstToolUsage(int daysGap, String date){		
		String query = "select u.* from (select s.user_id,  MIN(s.start_time) as minST from " +
		"(select distinct session_id from tool_usage)tool_sessions, session s where " +
		"tool_sessions.session_id = s.id group by user_id)min_val, user u " +
		"where min_val.user_id = u.id and DATEDIFF('"+date+"', min_val.minST) = "+daysGap+" and unsubscribe=0";
		List<Login> users = loginService.getUsersByQuery(query);
		return users;
	}
}
