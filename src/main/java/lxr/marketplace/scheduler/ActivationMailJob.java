package lxr.marketplace.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;
import lxr.marketplace.util.EmailBodyCreator;

import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ActivationMailJob extends QuartzJobBean {

    private static Logger logger = Logger.getLogger(ActivationMailJob.class);
    private long lastExecutionDate;

    public void setLastExecutionDate(long lastExecutionDate) {
        this.lastExecutionDate = lastExecutionDate;
    }

    protected void executeInternal(JobExecutionContext context)
            throws JobExecutionException {
        logger.info("************scheduler Started for Activation Mail job*******************");
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        String url = jobDataMap.getString("url");
        String fromAddress = jobDataMap.getString("teamMail");
        SqlRowSet sqlRowSet;

        jobDataMap.put("lastExecutionDate", lastExecutionDate);
        try {
            sqlRowSet = getData(jobDataMap);
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
            sendMailToInactiveUsers(sqlRowSet, url, fromAddress);

        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
    }

    public synchronized void sendMailToInactiveUsers(SqlRowSet sqlRowSet,
            String url, String fromAddress) {
        int temp = 0;
        boolean isContainData = sqlRowSet.isBeforeFirst();
        if (isContainData) {
            while (sqlRowSet.next()) {
                String toAddr = sqlRowSet.getString("email");

                String name = sqlRowSet.getString("name");
                String userName = sqlRowSet.getString("email");
                String password = sqlRowSet.getString("password");
                long activationId = sqlRowSet.getLong("activation_id");

                String subject = EmailBodyCreator.subjectOnReminderMail;
                String bodyText = EmailBodyCreator.bodyForReminderMail(activationId, url, name, userName, password);
                SendMail.sendMail(fromAddress, toAddr, subject, bodyText);
                temp++;

            }
            logger.info("temp :" + temp);
        }

    }

    private SqlRowSet getData(JobDataMap jobDataMap) {
        SqlRowSet sqlRowSet = null;

        JdbcTemplate jdbcTemplate = (JdbcTemplate) jobDataMap.get("jdbcTemplate");
        Date currDate = new Date();
        String toDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(currDate);
        currDate.setDate(currDate.getDate() - 7);
        String fromDate = (new SimpleDateFormat("yyyy-MM-dd")).format(currDate);

        String query = "select id as Login_Id,name as Name,email as Emai_Id,is_active as Is_Active,activation_id as Activation_Id,password as password,"
                + "date_format(reg_date,'%d-%b-%Y') as Registration_Date, date_format(act_date,'%d-%b-%Y') as Activation_Date from user where "
                + "date_format(reg_date,'%Y-%m-%d') = ? and is_active=0";
        try {
            logger.info("InactiveUsers  :" + query + " _  on " + fromDate);//+ toDate);
            sqlRowSet = jdbcTemplate.queryForRowSet(query, new Object[]{
                fromDate});
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return sqlRowSet;
    }

}
