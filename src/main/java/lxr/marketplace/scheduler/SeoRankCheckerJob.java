package lxr.marketplace.scheduler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import lxr.marketplace.keywordrankchecker.SeoRankCheckerInfoTask;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class SeoRankCheckerJob extends QuartzJobBean {

    private static final Logger logger = Logger.getLogger(SeoRankCheckerJob.class);

    private LoginService loginService;
    @Resource(name = "jdbcTemplate")
    JdbcTemplate jdbcTemplate;
    ApplicationContext applicationContext;
    private ThreadPoolExecutor seoRankCheckerThreadPool = null;
    private final int poolSize = 5;
    private final int maxPoolSize = 50;
    private final long keepAliveTime = 360;
    private boolean terminated = false;
    private final LinkedBlockingQueue<Runnable> seoRankCheckerQue = new LinkedBlockingQueue<Runnable>();

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
        logger.info("*********Scheduler started for SEO Rank Checker Tool **********");
        SqlRowSet sqlRowSet = getListOfUsers();
        try {
            if (sqlRowSet != null) {
                seoRankCheckerThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, seoRankCheckerQue);
                while (sqlRowSet.next()) {
                    long rankUserId = sqlRowSet.getLong("id");
                    long userId = sqlRowSet.getLong("user_id");
                    String email = sqlRowSet.getString("email");
                    if (email != null && email.contains("GuestUser")) {
                        String guestEmail = "GuestUser" + userId + "<";
                        int strtindex = guestEmail.length();
                        email = email.substring(strtindex, email.lastIndexOf(">"));
                    }
                    boolean alertStatus = sqlRowSet.getBoolean("alert_status");
                    boolean weeklyReport = sqlRowSet.getBoolean("weekly_report");
                    String geogrphicLoc = sqlRowSet.getString("geographic_loc");
                    Timestamp startDateInDb = sqlRowSet.getTimestamp("start_date");
                    String language = sqlRowSet.getString("language");
                    String userName = sqlRowSet.getString("username");
                    SeoRankCheckerInfoTask seoRankCheckerTask = new SeoRankCheckerInfoTask(userId, rankUserId, email, geogrphicLoc, language, weeklyReport, alertStatus, startDateInDb, userName);
                    seoRankCheckerThreadPool.execute(seoRankCheckerTask);
                    Thread.sleep(3000);
                }

                while (!terminated) {
                    try {
                        int active = seoRankCheckerThreadPool.getActiveCount();
                        int qued = seoRankCheckerThreadPool.getQueue().size();
                        if ((active + qued) > 0) {
                            Thread.sleep(2000);
                        } else {
                            seoRankCheckerThreadPool.shutdownNow();
                            terminated = true;

                        }
                    } catch (InterruptedException ex) {
                        logger.error("in While--" + ex.getMessage());
                    } catch (Exception ex) {
                        logger.error("in While Exce--" + ex.getMessage());
                    }
                }
            }
        } catch (InvalidResultSetAccessException | InterruptedException e) {
            logger.error("Exception while preparing SMA user accounts cause: " + e.getMessage());
        }
        String schedulerName = "Weekly Keyword Rank Checker Tool scheduler has been successfully fired.";
        String subjectLine = "Weekly Keyword Rank Checker Tool scheduler fired successfully";
        SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
        logger.info("*********Scheduler completed for SEO Rank Checker Tool **********");
    }

    public SqlRowSet getListOfUsers() {
        SqlRowSet sqlRowSet = null;
        Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String currentDate = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
        String query = "select rank_user.* ,user.email as email,user.name as username from (select id,user_id,alert_status,weekly_report,"
                + "geographic_loc,start_date,language from rank_checker_user where date_format(start_date,'%Y-%m-%d') <= '" + currentDate + "' "
                + "and is_logged_in = 1 and tool_unsubscription = 0)rank_user,user where rank_user.user_id = user.id and user.name IS NOT NULL";
        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        } catch (DataAccessException e) {
            logger.error(e);
        }
        return sqlRowSet;
    }

}
