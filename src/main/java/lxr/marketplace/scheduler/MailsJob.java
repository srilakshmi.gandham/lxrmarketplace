/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.scheduler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import lxr.marketplace.admin.plugin.PluginAdoptionDTO;
import lxr.marketplace.admin.plugin.PluginAdoptionService;
import lxr.marketplace.admin.plugin.PluginAdoptionTeamDTO;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author netelixir
 */
public class MailsJob extends QuartzJobBean {

    final private Logger logger = Logger.getLogger(MailsJob.class);

    private PluginAdoptionService pluginAdoptionService;
    private String supportMail;
    private String pluginAdoptionMail;
    private String downloadFolder;
    private final int PLUGIN_REPORTS_COUNT = 20;

    public PluginAdoptionService getPluginAdoptionService() {
        return pluginAdoptionService;
    }

    public void setPluginAdoptionService(PluginAdoptionService pluginAdoptionService) {
        this.pluginAdoptionService = pluginAdoptionService;
    }

    public String getSupportMail() {
        return supportMail;
    }

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public String getPluginAdoptionMail() {
        return pluginAdoptionMail;
    }

    public void setPluginAdoptionMail(String pluginAdoptionMail) {
        this.pluginAdoptionMail = pluginAdoptionMail;
    }

    public String getDownloadFolder() {
        return downloadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {

        logger.info("Mail scheduler job started");
        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");
        String dayOfDate = formatter.format(cal.getTime());
        if (dayOfMonth == 1) {
            String fileName = pluginAdoptionService.generatePluginAdoptionConsolidateReoprt("lastMonth", "", "");
            if (fileName.contains(downloadFolder)) {
                fileName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length());
            }
            String[] files = new String[1];
            files[0] = fileName;
            String subjectLine = EmailBodyCreator.subjectOnLxrPluginForMonthlyReport;
            String bodyText = EmailBodyCreator.bodyOnLxrPluginForMonthlyReport();
            SendMail.sendMailWithAttachments(pluginAdoptionMail, supportMail, subjectLine, bodyText, "", downloadFolder, files);
            
            String schedulerName = "LXRPlugin Consalidate Report scheduler has been fired successfully.";
            String subject = "LXRPlugin Consalidate Report scheduler fired successfully";
            SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subject);
        }
        if (dayOfDate.equalsIgnoreCase("mon")) {
            Map<Long, PluginAdoptionDTO> weeklyAccUsageData = pluginAdoptionService.fethingWeeklyAccountUsageData();
            weeklyAccUsageData.entrySet().stream().map((m) -> (PluginAdoptionDTO) m.getValue()).forEach((pluginAdoptionDTO) -> {
                String accountName = pluginAdoptionDTO.getAccountName();
                int reportUsagePercent = (int) ((pluginAdoptionDTO.getReportsUsageCount() * 100) / PLUGIN_REPORTS_COUNT);
                String subjectLine = EmailBodyCreator.subjectOnLxrPluginWeeklyReport(accountName);
                String bodyText = "";
                if (reportUsagePercent >= 50) {
                    bodyText = EmailBodyCreator.bodyOnLxrPluginForWeeklyReport1(accountName);
                } else {
                    bodyText = EmailBodyCreator.bodyOnLxrPluginForWeeklyReport2(reportUsagePercent, accountName);
                }
                List<PluginAdoptionTeamDTO> pluginAdoptionTeamList = pluginAdoptionDTO.getPluginAdoptionTeamDTO();
                String[] teamMail;
                if (pluginAdoptionTeamList == null || pluginAdoptionTeamList.isEmpty()) {
                    teamMail = new String[1];
                    teamMail[0] = pluginAdoptionDTO.getManagerEmail();
                } else {
                    teamMail = new String[pluginAdoptionTeamList.size() + 1];
                    teamMail[0] = pluginAdoptionDTO.getManagerEmail();
                    int i = 1;
                    for (PluginAdoptionTeamDTO pluginAdoptionTeamDTO : pluginAdoptionTeamList) {
                        teamMail[i] = pluginAdoptionTeamDTO.getTeamMemberEmail();
                        i++;
                    }
                }

                SendMail.sendMail(supportMail, teamMail, subjectLine, bodyText);
            });
            String schedulerName = "LXRPlugin Adoption Report scheduler has been fired successfully.";
            String subjectLine = "LXRPlugin Adoption Report scheduler fired successfully";
            SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
        }

    }

}
