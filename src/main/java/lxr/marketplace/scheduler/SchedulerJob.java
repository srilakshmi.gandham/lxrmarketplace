/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.scheduler;

import lxr.marketplace.socialmedia.SocialAPISTrackingStatusTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author vidyasagar.korada
 */
@Configuration
@EnableScheduling
public class SchedulerJob {
    
    @Autowired
    private SocialAPISTrackingStatusTask socialAPISTrackingStatusTask;
    
    @Scheduled(cron = "${SocialAPISTracking.cron}")
    public void trackAPIStatusJob() {
        socialAPISTrackingStatusTask.trackAPIStatus();
    }
}


