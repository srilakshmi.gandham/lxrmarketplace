/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.scheduler;

import java.io.File;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author pavan
 */
@Component
public class FilesCleanUpJob extends QuartzJobBean {

    final private Logger logger = Logger.getLogger(FilesCleanUpJob.class);
    @Value("${disk2.filecleanup.regrex}")
    private String filecleanupRegrex;

    @Value("${disk2.filescleanup.targetfolder.path}")
    private String targetfolderPath;

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        if (filecleanupRegrex != null && targetfolderPath != null) {
            logger.info("************ FilesCleanUp Scheduler Started *******************");
            try {

                File files = new File(targetfolderPath);
                long cutoff = System.currentTimeMillis() - (180 * 24 * 60 * 60 * 1000);
                if (files != null) {
                    logger.debug("cutoff: " + cutoff + ", files:" + files);
                    files.list(new AgeFileFilter(cutoff));
                    File[] listOfFiles = files.listFiles((File dir, String name) -> name.matches(filecleanupRegrex));
                    logger.info("Count of listOfFiles to be deleted: " + listOfFiles.length);
                    if (listOfFiles.length > 0) {
                        for (File f : listOfFiles) {
                            f.delete();
                        }
                        logger.info("************ FilesCleanUp Scheduler Completed *******************");
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in files cleaning job", e);
            }
        }
    }
}
