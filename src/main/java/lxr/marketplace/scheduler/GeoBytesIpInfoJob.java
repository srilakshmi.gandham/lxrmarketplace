package lxr.marketplace.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import lxr.marketplace.apiaccess.GeoBytesIpLocationAPIService;
import lxr.marketplace.apiaccess.lxrmbeans.GeoBytesIpInfo;
import lxr.marketplace.session.SessionService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class GeoBytesIpInfoJob extends QuartzJobBean{


	private static Logger logger = Logger.getLogger(GeoBytesIpInfoJob.class);
	@Resource(name = "jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	ApplicationContext applicationContext;
	private ThreadPoolExecutor GeoBytesThreadPool = null;
	private GeoBytesIpLocationAPIService geoBytesIpLocationAPIService;
	private SessionService sessionService;
	private int poolSize = 10;
	private int maxPoolSize = 50;
	private long keepAliveTime = 360;
	private boolean terminated = false;
	private LinkedBlockingQueue<Runnable> GeoBytesQueue = new LinkedBlockingQueue<Runnable>();
	
	
	public void setGeoBytesIpLocationAPIService(
			GeoBytesIpLocationAPIService geoBytesIpLocationAPIService) {
		this.geoBytesIpLocationAPIService = geoBytesIpLocationAPIService;
	}
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}
	public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
	public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	public void setTerminated(boolean terminated) {
		this.terminated = terminated;
	}

	public boolean isTerminated() {
		return terminated;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		logger.info("*********Scheduler started for fetching Country for IP **********");
		SqlRowSet sqlRowSet;
		List<GeoBytesIpInfo> geoBytesIpInfoList = new ArrayList<GeoBytesIpInfo>();
		int limit = 0;
		try {
			sqlRowSet =  getListOfIps();
			if(sqlRowSet != null){
				GeoBytesThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize,keepAliveTime, TimeUnit.SECONDS, GeoBytesQueue);

				while(sqlRowSet.next()){
		            String ipAddress = sqlRowSet.getString("ip");
		            GeoBytesIpInfo ipInfo = new GeoBytesIpInfo(ipAddress);
		            Runnable IpInfoThrd = new IpInfoThread(ipInfo,ipAddress,geoBytesIpLocationAPIService);
		            GeoBytesThreadPool.execute(IpInfoThrd);
		        	Thread.sleep(3000);
		        	geoBytesIpInfoList.add(ipInfo);	
				}
				
				while (!terminated) {		
					try {
						int active = GeoBytesThreadPool.getActiveCount();
						int qued = GeoBytesThreadPool.getQueue().size();
						if ((active + qued) > 0) {
							Thread.sleep(2000);
					    } else {
								logger.info("while shutdown:");
								GeoBytesThreadPool.shutdownNow();
						logger.info("************scheduler Completed for for GeoBytesIpInfo job*******************"); 
							terminated = true;
							
						}
					  } catch (InterruptedException ex) {
						logger.error("in While--" + ex.getMessage());
					  } catch (Exception ex) {
						logger.error("in While Exce--" + ex.getMessage());
					  }
				  }
			 }
			if(geoBytesIpInfoList != null && geoBytesIpInfoList.size()> 0){
				sessionService.insertListInIpCountryTab(geoBytesIpInfoList);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}

	}

    public SqlRowSet getListOfIps(){
    	  SqlRowSet sqlRowSet = null;

		 String query = "select distinct ip from session";
        try {
			logger.info("GeoBytesThreadPool query  :" + query);//+ toDate);
			sqlRowSet = getJdbcTemplate().queryForRowSet(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sqlRowSet;
     }
    
}

class IpInfoThread implements Runnable {  
    private GeoBytesIpInfo geoBytesIpInfo; 
    private String ipAddress;
    private GeoBytesIpLocationAPIService geoBytesIpLocationAPIService;
    
    public IpInfoThread(GeoBytesIpInfo geoBytesIpInfo,String ipAddress,GeoBytesIpLocationAPIService geoBytesIpLocationAPIService){  
        this.geoBytesIpInfo=geoBytesIpInfo;
        this.ipAddress=ipAddress;
        this.geoBytesIpLocationAPIService=geoBytesIpLocationAPIService;
    }  
   
    public void run() {  
	 	try{
		 geoBytesIpInfo = geoBytesIpLocationAPIService.getCountryFromIpinfodb(ipAddress, geoBytesIpInfo);
        }catch(Exception e){
        	e.printStackTrace();
        }  
    }  
   
}  
