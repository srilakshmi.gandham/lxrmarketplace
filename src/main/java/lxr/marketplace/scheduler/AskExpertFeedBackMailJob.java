/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.scheduler;

import java.util.ArrayList;
import java.util.List;
import lxr.marketplace.askexpert.feedback.AskExpertFeedbackService;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author vemanna
 */
public class AskExpertFeedBackMailJob extends QuartzJobBean {

    final private Logger logger = Logger.getLogger(AskExpertFeedBackMailJob.class);

    private String supportMail;
    private AskExpertFeedbackService askExpertFeedbackService;

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public void setAskExpertFeedbackService(AskExpertFeedbackService askExpertFeedbackService) {
        this.askExpertFeedbackService = askExpertFeedbackService;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {

        logger.info("************ AskTheExpert Feedback Mail Scheduler Started *******************");
        SqlRowSet sqlRowSet = askExpertFeedbackService.getFeedbackMailUsersList();
        if (sqlRowSet != null) {
            boolean isContainData = sqlRowSet.isBeforeFirst();
            if (isContainData) {
                List<Long> feedbackMailSentQuesIds = new ArrayList<>();
                EncryptAndDecryptUsingDES encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
                while (sqlRowSet.next()) {
                    long questionId = sqlRowSet.getLong("question_id");
                    long userId = sqlRowSet.getLong("user_id");
                    String question = sqlRowSet.getString("question");
                    String name = sqlRowSet.getString("name");
                    String email = sqlRowSet.getString("email");

                    String encriptedUserId = encryptAndDecryptUsingDES.encrypt(String.valueOf(userId));
                    String encriptedQuestionId = encryptAndDecryptUsingDES.encrypt(String.valueOf(questionId));

                    String subject = EmailBodyCreator.subjectOnAskTheExpertFeedBackMail;
                    String body = EmailBodyCreator.bodyForAskTheExpertFeedBackMail(name, encriptedUserId, encriptedQuestionId, question);
                    boolean mailSent = SendMail.sendMail(supportMail, email, subject, body);
                    if (mailSent) {
                        feedbackMailSentQuesIds.add(questionId);
                    }

                }
                //updating feedback mail sent list in lxrm_ate_questions
                if (feedbackMailSentQuesIds.size() > 0) {
                    askExpertFeedbackService.updateAskExpertFeedbackMialSent(feedbackMailSentQuesIds);
                }
            }
        }
        logger.info("************ AskTheExpert Feedback Mail Scheduler Started *******************");
        String schedulerName = "ATE Feedback mail scheduler has been successfully fired.";
        String subjectLine = "ATE Feedback mail scheduler fired successfully";
        SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
    }
}
