package lxr.marketplace.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import lxr.marketplace.amazonpricetracker.AmazonPriceTrackerService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.UserOptionService;
import lxr.marketplace.util.UserOptions;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AmazonProductMailJob extends QuartzJobBean {
	private static Logger logger = Logger.getLogger(AmazonProductMailJob.class);
	
	@Resource(name = "jdbcTemplate")
	JdbcTemplate jdbcTemplate; 
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	protected void executeInternal(JobExecutionContext context)
	throws JobExecutionException {
		logger.info("**********************amazon scheduler started ***************");
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		String url = jobDataMap.getString("url");
		String fromAddress = jobDataMap.getString("teamMail");
		String downloadFolder = jobDataMap.getString("downloadFolder");;
		try {
			Date currDate = new Date();
			List<UserOptions> userlist  =   UserOptionService.getListForDay(5, currDate.getDay()+1, jdbcTemplate) ;
			for(int s=0;s< userlist.size();s++){
				 UserOptions user_opt =userlist.get(s);
				 long user_id = user_opt.getUserId();
				 LoginService loginser = new LoginService();
				 logger.info(user_id + "usr id");
				 Login usr = loginser.findUser(user_id,jdbcTemplate);
				 
				 AmazonPriceTrackerService aptService = new AmazonPriceTrackerService();
				 aptService.setJdbcTemplate(jdbcTemplate);	 
				 long i = aptService.addTask(user_id, user_opt.getUserDomain(), downloadFolder);
			}
			// sendMailToAmazonProductUsers(sqlRowSet, url, fromAddress);			
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}
	}
//need to change the content,subject, body  i.e send mail method 
/*public synchronized void sendMailToAmazonProductUsers(SqlRowSet sqlRowSet,
	String url, String fromAddress) {
SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
int colCount = sqlRowSetMetDat.getColumnCount();
 int temp=0;
 
		while (sqlRowSet.next()) {
		String toAddr = sqlRowSet.getString("email");
		String subject = "Amazon Product Report for your Products Uploaded";
		String bodyText = "";
		String name = sqlRowSet.getString("name");
		String userName = sqlRowSet.getString("email");
		String password = sqlRowSet.getString("password");
		SendMail sendMail = new SendMail();
		sendMail.SendReminderMail(toAddr, fromAddress, subject, bodyText,
				sqlRowSet.getLong("activation_id"), url, name,
				userName, password);
		temp++;
	
}logger.info("temp :" +temp);
}

/*private SqlRowSet getData(JobDataMap jobDataMap) {
SqlRowSet sqlRowSet = null;

JdbcTemplate jdbcTemplate = (JdbcTemplate) jobDataMap.get("jdbcTemplate");
Date currDate = new Date();
String toDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(currDate);
currDate.setDate(currDate.getDate() - 7);
String fromDate = (new SimpleDateFormat("yyyy-MM-dd")).format(currDate);
//need to change the query
Calendar calendar = Calendar.getInstance();  
calendar.setTime(currDate);  
logger.info(currDate.getDay());
 String query = "select * from user_options where scheduler_isenabled= 1 and scheduled_day = ? ";
try {
	logger.info("scheduleractiveUsers  :" + query + " _  on " + fromDate );//+ toDate);
	sqlRowSet = jdbcTemplate.queryForRowSet(query, new Object[] {currDate.getDay()});
} catch (Exception e) {
	logger.error(e.getMessage());
	logger.error(e.getCause());
}
return sqlRowSet;
}*/
}