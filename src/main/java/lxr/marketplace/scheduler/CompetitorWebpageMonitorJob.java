package lxr.marketplace.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import lxr.marketplace.competitorwebpagemonitor.CmpUserInfoTask;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class CompetitorWebpageMonitorJob extends QuartzJobBean {

    private static Logger logger = Logger.getLogger(CompetitorWebpageMonitorJob.class);
    private LoginService loginService;
    @Resource(name = "jdbcTemplate")
    JdbcTemplate jdbcTemplate;
    ApplicationContext applicationContext;
    private ThreadPoolExecutor cmpThreadPool = null;
    private int poolSize = 10;
    private int maxPoolSize = 50;
    private long keepAliveTime = 360;
    private boolean terminated = false;
    private LinkedBlockingQueue<Runnable> cmpWebpageMonitorQue = new LinkedBlockingQueue<Runnable>();

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void executeInternal(JobExecutionContext context)
            throws JobExecutionException {
        logger.info("************Scheduler Started for competitor webpage monitor job*******************");
        SqlRowSet sqlRowSet = getListofUsers((new SimpleDateFormat("yyyy-MM-dd")).format(new Date()));
        try {
            if (sqlRowSet != null) {
                cmpThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize,keepAliveTime, TimeUnit.SECONDS, cmpWebpageMonitorQue);
                while (sqlRowSet.next()) {
                    long infoId = sqlRowSet.getLong("info_id");
                    long userID = sqlRowSet.getLong("user_id");
                    String cmpUrl = sqlRowSet.getString("cmp_url");
                    String email = sqlRowSet.getString("email");
                    boolean emailUnsubscription = sqlRowSet.getBoolean("emial_unsubscription");
                    if (!(email.contains("GuestUser"))) {
                        CmpUserInfoTask cmpUserTask = new CmpUserInfoTask(infoId, userID, cmpUrl, email, emailUnsubscription);
                        cmpThreadPool.execute(cmpUserTask);
                        Thread.sleep(3000);
                    }
                }
                while (!terminated) {
                    try {
                        int active = cmpThreadPool.getActiveCount();
                        int qued = cmpThreadPool.getQueue().size();
                        if ((active + qued) > 0) {
                            Thread.sleep(2000);
                        } else {
                            cmpThreadPool.shutdownNow();
                            terminated = true;
                        }
                    } catch (InterruptedException ex) {
                        logger.error("in While--", ex);
                    } catch (Exception ex) {
                        logger.error("in While Exce--", ex);
                    }
                }
            }
        } catch (InvalidResultSetAccessException | InterruptedException e) {
            logger.error("InvalidResultSetAccessException in preparing list of Competitor web page monitior cause: " + e);
        }

        String schedulerName = "Competitor Webpage Monitor Tool scheduler has been successfully fired.";
        String subjectLine = "Competitor Webpage Monitor Tool scheduler fired successfully";
        SendMail.mailToFeedbackUponSchedulerFires(schedulerName, subjectLine);
        logger.info("************Scheduler finished for competitor webpage monitor job*******************");
    }

    public SqlRowSet getListofUsers(String currentDate) {
        SqlRowSet sqlRowSet = null;

        String query = "select cmp_url_info.*, c.email from (select b.info_id, cmpweb_emails.email from "
                + "(select info_id, max(date) as date from cmpweb_emails where info_id in (select info_id "
                + "from cmp_url_info where is_deleted = 0 and ((frequency=1 ) or (frequency=2 and  "
                + "DATEDIFF(date_format(start_date ,'%Y-%m-%d'),'" + currentDate + "')%7=0) )AND "
                + "date_format(start_date,'%Y-%m-%d')<= '" + currentDate + "') group by info_id)b join cmpweb_emails where b.info_id = "
                + "cmpweb_emails.info_id and b.date = cmpweb_emails.date union select a.info_id, user.email from "
                + "(select info_id, user_id from cmp_url_info where is_deleted = 0 and ((frequency=1 ) or (frequency=2 and "
                + "DATEDIFF(date_format(start_date,'%Y-%m-%d'),'" + currentDate + "')%7=0) )AND "
                + "date_format(start_date,'%Y-%m-%d')<='" + currentDate + "' and info_id not in "
                + "(select info_id from cmpweb_emails))a join user where a.user_id = user.id)c join cmp_url_info "
                + "where c.info_id = cmp_url_info.info_id and cmp_url_info.tool_unsubscription = 0";

        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        } catch (DataAccessException e) {
            logger.error("DataAccessException in getListofUsers cause:" + e);
        }
        return sqlRowSet;
    }

}
