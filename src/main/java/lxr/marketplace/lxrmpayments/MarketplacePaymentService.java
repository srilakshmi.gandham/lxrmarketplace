package lxr.marketplace.lxrmpayments;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.paypal.api.payments.Authorization;
import com.paypal.api.payments.Capture;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Transaction;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import net.sf.json.JSONArray;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

public class MarketplacePaymentService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(MarketplacePaymentService.class);

    public long saveCreditCardInLxr(final CreditCard newCreditCard, final LxrMCreditCard lxrMCrditCard, final long userId) {
        final String query = "INSERT INTO lxrm_credit_card_info (user_id, paypal_card_id, card_no, first_name, "
                + "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
                + "address_line_1, address_line_2, city, country_code, postal_code,"
                + " state,cust_profile_id,payment_profile_id) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        final SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss'Z'");
        logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {

                getJdbcTemplate().update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                        statement.setLong(1, userId);
                        statement.setString(2, newCreditCard.getId());
                        statement.setString(3, newCreditCard.getNumber());
                        statement.setString(4, newCreditCard.getFirstName());
                        statement.setString(5, newCreditCard.getLastName());
                        statement.setString(6, newCreditCard.getType());
                        try {
                            Date validUntil = sdf.parse(newCreditCard.getValidUntil());
                            statement.setTimestamp(7, new Timestamp(validUntil.getTime()));
                        } catch (ParseException | SQLException e) {
                            statement.setTimestamp(7, null);
                            logger.error("Exception in getting 'valid until' field for credit card of "
                                    + newCreditCard.getFirstName() + ". Saving the card without this info.", e);
                        }
                        try {
                            statement.setInt(8, newCreditCard.getExpireMonth());
                        } catch (Exception e) {
                            logger.error("Exception in parsing expire month for credit card of "
                                    + newCreditCard.getFirstName() + "saving the card without this info.", e);
                            statement.setInt(8, 0);
                        }
                        try {
                            statement.setInt(9, newCreditCard.getExpireYear());
                        } catch (Exception e) {
                            logger.error("Exception in parsing expire year for credit card of "
                                    + newCreditCard.getFirstName() + "saving the card without this info.", e);
                            statement.setInt(9, 0);
                        }
                        Calendar cal = Calendar.getInstance();
                        statement.setTimestamp(10, new Timestamp(cal.getTimeInMillis()));
                        statement.setBoolean(11, false);
                        if (newCreditCard.getBillingAddress() != null) {
                            statement.setString(12, newCreditCard.getBillingAddress().getLine1());
                            statement.setString(13, newCreditCard.getBillingAddress().getLine2());
                            statement.setString(14, newCreditCard.getBillingAddress().getCity());
                            statement.setString(15, newCreditCard.getBillingAddress().getCountryCode());
                            statement.setString(16, newCreditCard.getBillingAddress().getPostalCode());
                            statement.setString(17, newCreditCard.getBillingAddress().getState());
                        } else {
                            statement.setString(12, "");
                            statement.setString(13, "");
                            statement.setString(14, "");
                            statement.setString(15, lxrMCrditCard.getCountryCode());
                            statement.setString(16, lxrMCrditCard.getPostalCode());
                            statement.setString(17, "");
                        }
                        statement.setString(18, "");
                        statement.setString(19, "");
                        return statement;
                    }
                }, keyHolder);
            }
            logger.info("Credit Card is saved in lxrmarketplace database for user with id = " + userId);
            return (Long) keyHolder.getKey();
        } catch (Exception ex) {
            logger.error("Exception on inserting credit card details:", ex);
            return 0;
        }
    }

    public long saveCreditCardInLxrForAuth(final MarketplaceCreditCard newCreditCard, final long userId) {
        final String query = "INSERT INTO lxrm_credit_card_info (user_id, card_no, first_name, "
                + "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
                + "address_line_1, address_line_2, city, country_code, postal_code, state,cust_profile_id,"
                + "payment_profile_id) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {

                getJdbcTemplate().update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                        statement.setLong(1, userId);
                        statement.setString(2, newCreditCard.getCardNo());
                        statement.setString(3, newCreditCard.getFirstName());
                        statement.setString(4, newCreditCard.getLastName());
                        statement.setString(5, newCreditCard.getType());
                        statement.setTimestamp(6, null);
                        try {
                            statement.setInt(7, newCreditCard.getExpireMonth());
                        } catch (Exception e) {
                            logger.error("Exception in parsing expire month for credit card of "
                                    + newCreditCard.getFirstName() + "saving the card without this info.");
                            statement.setInt(7, 0);
                        }
                        try {
                            statement.setInt(8, newCreditCard.getExpireYear());
                        } catch (Exception e) {
                            logger.error("Exception in parsing expire year for credit card of "
                                    + newCreditCard.getFirstName() + "saving the card without this info.");
                            statement.setInt(8, 0);
                        }
                        Calendar cal = Calendar.getInstance();
                        statement.setTimestamp(9, new Timestamp(cal.getTimeInMillis()));
                        statement.setBoolean(10, false);
                        statement.setString(11, newCreditCard.getAddressLine1());
                        statement.setString(12, newCreditCard.getAddressLine2());
                        statement.setString(13, newCreditCard.getCity());
                        statement.setString(14, newCreditCard.getCountryCode());
                        statement.setString(15, newCreditCard.getPostalCode());
                        statement.setString(16, newCreditCard.getState());
                        statement.setLong(17, newCreditCard.getCustProfileId());
                        statement.setLong(18, newCreditCard.getPaymentProfileId());
                        return statement;
                    }
                }, keyHolder);
            }
            logger.info("Credit Card is saved in lxrmarketplace database for user with id = " + userId);
            return (Long) keyHolder.getKey();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public MarketplaceCreditCard fetchCreditCardDetails(long userId) {
        MarketplaceCreditCard lxrMCreditCard = null;
        String query = "select * from lxrm_credit_card_info where user_id = " + userId + " and is_deleted = 0";
        logger.info("Fetching creditcard in lxrmarketplace database for user with id = " + userId);
        try {
            lxrMCreditCard = (MarketplaceCreditCard) getJdbcTemplate().queryForObject(query,
                    new RowMapper<Object>() {
                MarketplaceCreditCard lmcc;

                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

                    lmcc = new MarketplaceCreditCard();
                    lmcc.setLxrCardId(rs.getLong("card_id"));
                    lmcc.setUserId(rs.getLong("user_id"));
                    lmcc.setCardNo(rs.getString("card_no"));
                    lmcc.setFirstName(rs.getString("first_name"));
                    lmcc.setLastName(rs.getString("last_name"));
                    lmcc.setType(rs.getString("card_type"));
                    lmcc.setExpireMonth(rs.getInt("expire_month"));
                    lmcc.setExpireYear(rs.getInt("expire_year"));

                    lmcc.setAddressLine1(rs.getString("address_line_1"));
                    lmcc.setAddressLine2(rs.getString("address_line_2"));
                    lmcc.setCity(rs.getString("city"));
                    lmcc.setCountryCode(rs.getString("country_code"));
                    lmcc.setPostalCode(rs.getString("postal_code"));
                    lmcc.setState(rs.getString("state"));
                    lmcc.setCustProfileId(rs.getLong("cust_profile_id"));
                    lmcc.setPaymentProfileId(rs.getLong("payment_profile_id"));
                    return lmcc;
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return lxrMCreditCard;
    }

    public void deletePreviousCard(long userId) {
        String query = "UPDATE lxrm_credit_card_info SET is_deleted = 1 WHERE user_id = " + userId;
        getJdbcTemplate().update(query);
    }

    public void saveCardTransactionInfo(Payment createdPayment, MarketplaceCreditCard lxrMCreditCard, long toolId) {
        if (createdPayment == null) {
            return;
        }
        List<Transaction> transactions = createdPayment.getTransactions();
        logger.info("createdPayment: " + createdPayment + "\nTransaction Informations\nPayment Id: "
                + createdPayment.getId() + "\nCreated Time: " + createdPayment.getCreateTime() + "\nUpdated Time: "
                + createdPayment.getUpdateTime() + "\nPayment State:  " + createdPayment.getState()
                + "\nPayment Intent " + createdPayment.getIntent() + "\nTransaction Id: "
                + transactions.get(0).getRelatedResources().get(0).getSale().getId());

        final String query = "INSERT INTO lxrm_payment_transaction_stats (created_time, updated_time, "
                + "state, intent, tool_id, currency, amount,authorization_code, description, "
                + "lxr_card_id, payer_user_id, is_card,  transaction_id) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(createdPayment.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            logger.error("Exception in getting transaction creation time for user with id = "
                    + lxrMCreditCard.getUserId() + ", ", e);
        }
        try {
            Date updatedTime = sdf.parse(createdPayment.getUpdateTime());
            updationTime = new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            logger.error("Exception in getting transaction updation time for user with id = "
                    + lxrMCreditCard.getUserId() + ", ", e);
        }
        try {
            getJdbcTemplate().update(query, new Object[]{creationTime, updationTime, createdPayment.getState(),
                createdPayment.getIntent(), toolId, transactions.get(0).getAmount().getCurrency(),
                Double.parseDouble(transactions.get(0).getAmount().getTotal()), createdPayment.getId(),
                transactions.get(0).getDescription(), lxrMCreditCard.getLxrCardId(),
                lxrMCreditCard.getUserId(), true,
                transactions.get(0).getRelatedResources().get(0).getSale().getId()
            });
            logger.info("Transaction information saved in marketplace database for user " + lxrMCreditCard.getUserId());
        } catch (Exception e) {
            logger.error("Transaction information saved in marketplace database for user " + lxrMCreditCard.getUserId() + ", ", e);
        }
    }

    /**
     * saves transaction failure information from saved credit/debit card
     *
     * @param failureMessage
     * @param lxrMCreditCard
     * @param toolId
     * @param intent type of transaction where failure happened possible values:
     * authorization, sale, capture
     * @return
     */
    public boolean saveFailedCardTransactionInfo(String failureMessage, MarketplaceCreditCard lxrMCreditCard,
            long toolId, String intent) {
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        Calendar now = Calendar.getInstance();
        creationTime = new Timestamp(now.getTimeInMillis());
        updationTime = new Timestamp(now.getTimeInMillis());

        Object[] dataParams = new Object[]{creationTime, updationTime, "failure", intent, toolId, "",
            0.00, "", "", lxrMCreditCard.getUserId(), true, "",
            failureMessage};

        return savePayPalTransactionInfo(dataParams, lxrMCreditCard.getUserId());
    }

    public boolean savePayPalAuthorization(Authorization authorization, long userId, long toolId, long lxrmCardId) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(authorization.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction creation time for user with id = " + userId + ", ", e);
        }
        try {
            Date updatedTime = sdf.parse(authorization.getUpdateTime());
            updationTime = new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = " + userId + ", ", e);
        }

        Object[] dataParams = new Object[]{creationTime, updationTime, authorization.getState(),
            "authorization", toolId, authorization.getAmount().getCurrency(),
            authorization.getAmount().getTotal(), "", "LXRSEO Subscription Authorization",
            userId, true, authorization.getId(), ""};

        return savePayPalTransactionInfo(dataParams, userId);
    }

    public boolean savePayPalCapture(Capture capture, long userId, int toolId, String toolName) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(capture.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction creation time for user with id = " + userId + ", ", e);
        }
        try {
            Date updatedTime = sdf.parse(capture.getUpdateTime());
            updationTime = new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = " + userId + ", ", e);
        }

        Object[] dataParams = new Object[]{creationTime, updationTime, capture.getState(),
            "capture", toolId, capture.getAmount().getCurrency(),
            capture.getAmount().getTotal(), "", toolName + " subscription capture",
            userId, true, capture.getId(), ""};

        return savePayPalTransactionInfo(dataParams, userId);
    }

    //To save user authorize.net transaction info
    public Object[] getAuthorizeTransactionInfo(String compResponse, long userId, long toolId, long lxrmCardId, Timestamp creationTime) {
        Timestamp updationTime = null;
        String authState = "";
        try {
            updationTime = creationTime;
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = " + userId + ", ", e);
        }
        Object[] dataParams = null;
        if (!compResponse.trim().equals("")) {
            String[] param = compResponse.split(",");
            if (param[0].equals("" + 1)) {
                authState = "Approved";
            } else if (param[0].equals("" + 2)) {
                authState = "Declined";
            } else if (param[0].equals("" + 3)) {
                authState = "Error";
            } else if (param[0].equals("" + 4)) {
                authState = "Held for Review";
            }
            dataParams = new Object[]{creationTime, updationTime, authState, param[11], toolId, "USD",
                param[9], param[4], "LXRSEO subscription capture", lxrmCardId, userId, true, param[6], param[3]};
            logger.info("param[9]......................amount is :" + param[9] + " authState .........." + authState);
        }
        return dataParams;
    }

    public boolean saveAuthorizationTranscationInfoInLxrTab(Object[] dataParams, long userId) {
        boolean saved = false;
        if (dataParams != null && dataParams.length > 0) {
            saved = savePayPalTransactionInfo(dataParams, userId);
        }
        return saved;
    }

    /* FOR PAYPAL SPECIFIC METHOD */
    public boolean savePayPalTransactionInfo(Object[] dataParams, long userId) {
        boolean saved = false;
        final String query = "INSERT INTO lxrm_payment_transaction_stats (created_time, updated_time, "
                + "state, intent, tool_id, currency, amount, paypal_payment_id, description, "
                + "payer_user_id, is_card, transaction_id, failure_message) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        try {
            getJdbcTemplate().update(query, dataParams);
            logger.info("PayPal Transaction information saved in marketplace database for user " + userId);
            saved = true;
        } catch (Exception e) {
            logger.error("Error while inserting card capture info in lxrm_payment_transaction_stats ", e);
        }
        return saved;
    }

    public boolean checkUserTransactions(Login user, long tool_id) {
        //boolean permitted=false;
        //TODO: PLEASE INDENT THE CODE, no need to take user object 
        String query = "select count(payment_id) from lxrm_payment_transaction_stats where payer_user_id=" + user.getId() + " and tool_id=" + tool_id;
        int n = getJdbcTemplate().queryForInt(query);
        if (n == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkUserCardInfo(long userId) {
        String query = "select count(card_id) from lxrm_credit_card_info where user_id=" + userId;
        int n = getJdbcTemplate().queryForInt(query);
        if (n == 0) {
            return false;
        } else {
            return true;
        }
    }

    public String generateInvoiceForPayment(String transactionId, String transactDate, String customerName, String toolName, String amount, String plan, String cardNo,String tragetFolder,long lxrmUserId) {
        try {
            BaseColor white = new BaseColor(249, 249, 249);
            BaseColor gray = new BaseColor(100, 100, 100);
            BaseColor orange = new BaseColor(255, 111, 13);
            BaseColor lightGray = new BaseColor(120, 120, 120);
            Font reportNameFont = new Font(FontFactory.getFont("HELVETICA", 17f, Font.BOLD, gray)); // black color
            Font subElementFont = new Font(FontFactory.getFont("HELVETICA", 14f, Font.BOLD, white));
            Font subElementLightGrayFont = new Font(FontFactory.getFont("HELVETICA", 12f, Font.BOLD, lightGray));
            Font lightGrayFont = new Font(FontFactory.getFont("HELVETICA", 12f, Font.NORMAL, lightGray));
            Font smallLightGrayFont = new Font(FontFactory.getFont("HELVETICA", 10f, Font.NORMAL, lightGray));
            String filename = Common.createFileName("LXRMarketplace_Invoice_"+lxrmUserId+"_") + ".pdf";
            String filePath = tragetFolder + filename;
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter writer = PdfWriter.getInstance(pdfDoc, stream);
            /*writer.setPageEvent(event);
            DateFormat sdf = new SimpleDateFormat("MMM dd, YYYY");*/

            pdfDoc.setMargins(25, 25, 75, 40);
            pdfDoc.open();
            PdfPTable header = new PdfPTable(2);
            header.setWidthPercentage(99.9f);
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(120f, 120f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(10f);
            logo.setHorizontalAlignment(Element.ALIGN_RIGHT);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);

            PdfPCell headingNameCell = new PdfPCell();
            headingNameCell.setBorder(Rectangle.NO_BORDER);
            headingNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            Phrase headingName = new Phrase("INVOICE", reportNameFont);
            headingNameCell.addElement(headingName);
            header.addCell(headingNameCell);
            header.addCell(logo);
            header.completeRow();
            /*Calendar today = Calendar.getInstance();
            PdfPCell dateCell = new PdfPCell(new Phrase("Date: " + sdf.format(today.getTime()), lightGrayFont));*/
            PdfPCell dateCell = new PdfPCell(new Phrase("Date: " + transactDate.trim(), lightGrayFont));
            dateCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(dateCell);

            PdfPCell addressCell = new PdfPCell(new Phrase("3 Independence Way, Suite #203\n" + "Princeton, NJ08540, US\n" + "support@lxrmarketplace.com", smallLightGrayFont));
            addressCell.setBorder(Rectangle.NO_BORDER);
            addressCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            header.addCell(addressCell);

            PdfPCell toAddCell = new PdfPCell(new Phrase("Billed To:\n\n" + customerName + "\n", lightGrayFont));
            toAddCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(toAddCell);
            PdfPCell emptyCell = new PdfPCell(new Phrase(""));
            emptyCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(emptyCell);

            PdfPTable invoiceAmtTable = new PdfPTable(1);
            invoiceAmtTable.setSpacingBefore(30f);
            invoiceAmtTable.setWidthPercentage(99.9f);
            PdfPCell amountCell = new PdfPCell(new Phrase("Amount: $" + amount, subElementFont));
            amountCell.setPaddingLeft(10f);
            amountCell.setMinimumHeight(50f);
            amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            amountCell.setBorder(Rectangle.NO_BORDER);
            amountCell.setBackgroundColor(orange);

            invoiceAmtTable.addCell(amountCell);

            float[] amtWidths = {1f, 3.5f, 3.5f, 2f};
            PdfPTable amtDetailsTable = new PdfPTable(amtWidths);
            amtDetailsTable.setWidthPercentage(99.9f);
            amtDetailsTable.addCell(addAmtDetailsCell("S No.", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Service", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Plan", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Amount", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell(String.valueOf(1), true, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell(toolName, true, "center", subElementLightGrayFont));
            if (!plan.equalsIgnoreCase("NA")) {
                amtDetailsTable.addCell(addAmtDetailsCell(plan, true, "center", subElementLightGrayFont));
            } else {
                amtDetailsTable.addCell(addAmtDetailsCell("-", true, "center", reportNameFont));
            }
            amtDetailsTable.addCell(addAmtDetailsCell("$"+amount, true, "right", subElementLightGrayFont));

            float[] subTotalAmtWidths = {5f, 3f, 2f};
            PdfPTable subTotalTable = new PdfPTable(subTotalAmtWidths);
            subTotalTable.setSpacingBefore(2f);
            subTotalTable.setWidthPercentage(99.9f);
            subTotalTable.addCell(addtotalAmtCell("", "", false, subElementLightGrayFont));
            subTotalTable.addCell(addtotalAmtCell("Sub Total", "Tax", false, subElementLightGrayFont));
            subTotalTable.addCell(addtotalAmtCell("$"+amount, "$0.00", true, subElementLightGrayFont));

            float[] totalAmtWidths = {6f, 4f};
            PdfPTable totalAmt = new PdfPTable(totalAmtWidths);
            totalAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
            totalAmt.setSpacingBefore(3f);
            totalAmt.setWidthPercentage(48);

            PdfPCell nameCell = new PdfPCell(new Phrase("Total Amount", subElementFont));
            nameCell.setMinimumHeight(30f);
            nameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            nameCell.setPaddingLeft(5f);
            nameCell.setBorder(Rectangle.NO_BORDER);
            nameCell.setBackgroundColor(orange);

            PdfPCell amtCell = new PdfPCell(new Phrase("$"+amount, subElementFont));
            amtCell.setBorder(Rectangle.NO_BORDER);
            amtCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            amtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            amtCell.setPaddingRight(10f);
            amtCell.setBackgroundColor(orange);
            totalAmt.addCell(nameCell);
            totalAmt.addCell(amtCell);

            PdfPTable payMethodTable = new PdfPTable(1);
            payMethodTable.setHorizontalAlignment(Element.ALIGN_LEFT);
            payMethodTable.setSpacingBefore(3f);
            payMethodTable.setWidthPercentage(48);

            PdfPCell payHeadingCell = new PdfPCell(new Phrase("PAYMENT METHOD", new Font(FontFactory.getFont("HELVETICA", 14f, Font.BOLD, gray))));
            payHeadingCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(payHeadingCell);
            PdfPCell creditCardCell = new PdfPCell();
            creditCardCell.addElement(new Phrase("Credit/Debit Card No", lightGrayFont));
            cardNo = (cardNo.substring(0,4).concat("******")).concat(cardNo.substring(12,cardNo.length()));
            creditCardCell.addElement(new Phrase(cardNo, lightGrayFont));
            creditCardCell.setPaddingTop(10f);
            creditCardCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(creditCardCell);
            PdfPCell transctnIdCell = new PdfPCell();
            transctnIdCell.addElement(new Phrase("Transaction ID", lightGrayFont));
            transctnIdCell.addElement(new Phrase(transactionId, lightGrayFont));
            transctnIdCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(transctnIdCell);

            PdfPTable termsTable = new PdfPTable(1);
            termsTable.setWidthPercentage(99.9f);
            termsTable.setSpacingBefore(25f);
            PdfPCell termsCell = new PdfPCell();
            termsCell.setBorder(Rectangle.NO_BORDER);
            termsCell.addElement(new Phrase(new Phrase("Terms:", smallLightGrayFont)));
            termsCell.addElement(new Phrase("* All the amounts and taxes shown are in US Dollars.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* Your bank/card statement will show a charge from NetElixir.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* This invoice is system generated hence signature and stamp is not required.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* In case of overdue/ defaults, the right to deactivate your services, is reserved.", smallLightGrayFont));
            termsTable.addCell(termsCell);

            pdfDoc.add(header);
            pdfDoc.add(invoiceAmtTable);
            pdfDoc.add(amtDetailsTable);
            pdfDoc.add(subTotalTable);
            pdfDoc.add(totalAmt);
            pdfDoc.add(payMethodTable);
            pdfDoc.add(termsTable);
            pdfDoc.close();

            return filename;
        } catch (DocumentException | IOException e) {
            System.out.println("Exception in generating Invoice for:" + customerName + "\n" + e);
        }
        return null;
    }

    public static PdfPCell addtotalAmtCell(String phrase1, String phrase2, boolean alignment, Font boldGrayFont) {
        Phrase totalNamePhrase = new Phrase();
        totalNamePhrase.setFont(boldGrayFont);
        totalNamePhrase.add(phrase1 + "\n\n");
        totalNamePhrase.add(phrase2);

        PdfPCell totalAmtCell = new PdfPCell(totalNamePhrase);
        totalAmtCell.setPaddingRight(10f);
        totalAmtCell.setPaddingBottom(5f);
        totalAmtCell.setMinimumHeight(25f);
        totalAmtCell.setBorder(Rectangle.NO_BORDER);
        totalAmtCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        if (alignment) {
            totalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        } else {
            totalAmtCell.setPaddingRight(60f);
            totalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }

        totalAmtCell.setBackgroundColor(new BaseColor(113, 207, 233));
        return totalAmtCell;
    }

    public static PdfPCell addAmtDetailsCell(String name, boolean bgColorStatus, String alignStatus, Font boldGrayFont) {
        PdfPCell cell = new PdfPCell(new Phrase(name, boldGrayFont));
        if (bgColorStatus) {
            cell.setBorderWidth(3f);
            cell.setBorderColor(BaseColor.WHITE);
            cell.setBackgroundColor(new BaseColor(222, 222, 222));
        } else {
            cell.setBorder(Rectangle.NO_BORDER);
        }

        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setMinimumHeight(35f);
        if (alignStatus.equalsIgnoreCase("center")) {
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        } else {
            cell.setPaddingRight(10f);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }

        return cell;
    }

    public String createInvoiceFromCapture(String captureId, String updatedTime, String customerName, String toolName, String amount, String plan, String cardNo,String tragetFolder,long lxrmUserId) {
        logger.info("Generating invoice for user after payment success");
        try {
            Timestamp paymentTransactionDate = null;
            final SimpleDateFormat sdfy = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
            if (captureId != null) {
//                Date paymentUpdatedTime = sdfy.parse(updatedTime);
                Date paymentUpdatedTime = sdfy.parse(updatedTime);
                paymentTransactionDate = new Timestamp(paymentUpdatedTime.getTime());
                Calendar cal = Calendar.getInstance();
                cal.setTime(paymentUpdatedTime);
                cal.add(Calendar.DATE, 30);
                String transactDate = sdf.format(paymentTransactionDate);
                String fileName = generateInvoiceForPayment(captureId, transactDate, customerName, toolName, amount, plan, cardNo,tragetFolder,lxrmUserId);
                return fileName;
            }
        } catch (Exception e) {
            logger.error("Exception in generating Invoice", e);
        }
        return null;
    }

    public Object[] fetchDataFromToolTab(int tool_id, String reportType) {
        String query = "select * from tools_pricing where tool_id =" + tool_id + " and report_type=" + reportType + "";
        logger.info("In side fetchDataFromToolTab() method" + tool_id + ".--------" + query + ".....reportType ....." + reportType);
        try {
            Object[] toolInfo = getJdbcTemplate().query(query, new ResultSetExtractor<Object[]>() {
                public Object[] extractData(ResultSet rs) {
                    logger.debug("rs values is ........" + rs);
                    Object[] toolData = null;
                    try {
                        if (rs.next()) {
                            toolData = new Object[2];
                            toolData[0] = rs.getDouble("amount");
                            logger.info("toolData[0] is......." + toolData[0]);

                            toolData[1] = rs.getString("display_string");
                            logger.info("toolData[0] is......." + toolData[1]);
                        }
                    } catch (SQLException e) {
                        logger.error("SQLException in fetchDataFromToolTab " + e.getMessage());
                    }
                    return toolData;
                }
            });
            return toolInfo;
        } catch (Exception ex) {
            logger.error("Exception in fetchDataFromToolTab " + ex.getMessage());
        }
        return null;
    }

    public void createFolder(String filePath, Login user) {
        File f = new File(filePath + user.getId());
        if (!f.isDirectory()) {
            f.mkdir();
        }
    }

    public void copyFileToFolder(String sourceLocation, String targetLocation, Login user, String fileName) {
        createFolder(targetLocation, user);
        try {
            File destDir = new File(targetLocation + user.getId() + "/");
            File srcFile = new File(sourceLocation + fileName);
            FileUtils.copyFileToDirectory(srcFile, destDir);
        } catch (Exception e) {
            logger.info("error while cpoying file to folder");
        }
    }

    public boolean updateLxrmCardId(long lxrCardId, long userId) {
        String query = "UPDATE lxrm_payment_transaction_stats SET lxr_card_id = " + lxrCardId + " WHERE payer_user_id = " + userId + " AND lxr_card_id IS NULL";
        boolean updated = false;
        try {
            getJdbcTemplate().execute(query);
            updated = true;
        } catch (Exception e) {
            logger.error("Transaction information saved in marketplace database for user " + userId + ": ", e);
        }
        return updated;
    }

    public PromoCodes fetchingPromoCodeInfo(String promoCode) {
        String query = "SELECT * FROM lxrm_promos WHERE promo_code= ?";
        PromoCodes promoCodes = null;
        try {
            promoCodes = this.getJdbcTemplate().queryForObject(query, new Object[]{promoCode}, (ResultSet rs, int i) -> {
                PromoCodes promos = new PromoCodes();
                promos.setPromoId(rs.getInt("promo_id"));
                promos.setPromoCode(rs.getString("promo_code"));
                promos.setStartDate(rs.getDate("start_date"));
                promos.setEndDate(rs.getDate("end_date"));
                promos.setPromoName(rs.getString("promo_name"));
                promos.setReusableCount(rs.getInt("reusable_count"));
                return promos;
            });
        } catch (EmptyResultDataAccessException e) {
        }

        return promoCodes;
    }

    public void addInJson(int statusCode, String message, HttpServletResponse response) {
        JSONArray arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, statusCode);
            arr.add(1, message);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public int checkPromoCodeUsedCount(long userId, int promoCodeId) {
        int count;
        String query = "SELECT applied_count FROM lxrm_user_promos_map WHERE user_id = ? AND promo_id= ?";
        try {
            count = this.getJdbcTemplate().queryForObject(query, Integer.class, userId, promoCodeId);
        } catch (EmptyResultDataAccessException e) {
            count = 0;
        } catch (IncorrectResultSizeDataAccessException e) {
            count = 1;
        }
        return count;
    }

    public void updatePromoCodeUsedCount(long userId, int promoCodeId, int promoCodeUsedCount) {
        if (promoCodeUsedCount == 0) {
            String query = "INSERT INTO lxrm_user_promos_map(user_id,promo_id,applied_count) values(?,?,?)";
            this.getJdbcTemplate().update(query, userId, promoCodeId, 1);
        } else {
            String query = "UPDATE lxrm_user_promos_map SET applied_count= ? where user_id= ? AND promo_id= ?";
            this.getJdbcTemplate().update(query, promoCodeUsedCount + 1, userId, promoCodeId);
        }
    }

}