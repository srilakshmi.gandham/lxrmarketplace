package lxr.marketplace.lxrmpayments;

import lxr.marketplace.payment.PaymentService;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.IntStream;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.payment.GatewayService;
import lxr.marketplace.payment.LXRMTransaction;
import lxr.marketplace.payment.PaymentModel;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import lxr.marketplace.paypalpayments.PayPalService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

@SuppressWarnings("deprecation")
public class MarketplacePaymentController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(MarketplacePaymentController.class);

    private MarketplacePaymentService marketplacePaymentService;
    private String downloadFolder;
    private ToolService toolService;
    private PayPalService payPalService;

    @Autowired
    private PaymentService paymentService;

    public PayPalService getPayPalService() {
        return payPalService;
    }

    public void setPayPalService(PayPalService payPalService) {
        this.payPalService = payPalService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setMarketplacePaymentService(
            MarketplacePaymentService marketplacePaymentService) {
        this.marketplacePaymentService = marketplacePaymentService;
    }

    public MarketplacePaymentController() {
        setCommandClass(LxrMCreditCard.class);
        setCommandName("marketplaceCreditCard");
    }

    @Value("${lxrm.invoice.path}")
    private String invoiceFolder;

    @Value("${lxrm.gateWayPayment.serviceURL}")
    private String gateWayPaymentServiceURL;

    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private LoginService loginService;

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        LxrMCreditCard marketplaceCreditCard = (LxrMCreditCard) command;
        String hostingURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
//        if (user != null && user.getId() != 0 && (user.getUserName() != null && !user.getUserName().isEmpty())) {
        if (user != null && user.getId() != 0) {
            HashMap<String, String> geoLocation = null;
            if (session.getAttribute("geoLocation") == null) {
                geoLocation = Common.loadGeoLocations();
                session.setAttribute("geoLocation", geoLocation);
            }
            Object[] vals = (Object[]) session.getAttribute("valsObjArray");
            if (vals != null && vals.length > 0) {
                if (WebUtils.hasSubmitParameter(request, "submit")) {
                    session.removeAttribute("toolObj");
                    logger.info("User with id = " + user.getId() + ", Provided Credit Card Details" + " and vals contains:: " + Arrays.asList(vals));
                    long paymentRecordId = 0;
                    StringBuilder failureMessage = new StringBuilder();
                    PaymentModel paymentModel = paymentService.createLXRMCreditCard(request, user);
                    if (paymentModel != null && (paymentModel.getBillingEmailId() != null && !paymentModel.getBillingEmailId().isEmpty())) {
                        paymentModel.setPrice((Double) vals[5]);
                        paymentModel.setToolId((Integer) vals[1]);
                        paymentModel.setDescription((String) vals[7]);
                        paymentModel.setToolName((String) vals[7]);
                        paymentModel.setSendInvoice(Boolean.TRUE);
                        paymentModel.setCurrencyType(messageSource.getMessage("lxrm.payment.curency", null, Locale.US));
                        paymentModel.setPlan((String) vals[9]);
                        paymentModel.setPayerUserId(user.getId());
                        if (user.isSocialLogin()
                                && (user.getUserName() == null
                                || (user.getUserName() != null && (user.getUserName().equalsIgnoreCase("email") || user.getUserName().isEmpty())))) {
                            user.setUserName(paymentModel.getBillingEmailId());
                            session.setAttribute("user", user);
                            session.setAttribute("sendMailId", paymentModel.getBillingEmailId());
                            loginService.updateEmail(request, user.getId(), user.getUserName());
                        }
                        /*Request for payment*/
                        LXRMTransaction lXRMTransaction = null;
                        lXRMTransaction = paymentService.processPaymentForLXRMService(paymentModel, failureMessage, invoiceFolder);
                        if (lXRMTransaction != null && lXRMTransaction.getState() != null && lXRMTransaction.getState().equals(GatewayService.CAPTURE_INTENT)) {
                            paymentRecordId = lXRMTransaction.getPaymentId();
                        }
                    } else {
                        failureMessage.append("Oh snap! ".concat(GatewayService.AUTH_FAILURE).concat(" Please try again."));
                    }
                    if (paymentRecordId > 0 && user.getUserName() != null && paymentModel != null) {
                        session.setAttribute("paymentModel", paymentModel);
                        Tool toolObj = toolService.getToolInfoFromToolTab((Integer) vals[1], (String) vals[6], (Double) vals[5]);
                        if (toolObj.getToolId() != 0) {
                            toolService.insertDataInDownloadReportDetailsTab(vals);
                            session.removeAttribute("relatedToolObj");
                        }
                        if (toolObj.getToolId() != 0) {
                            /*Sending tool analysis report to user email after payment sucess. 
                                            This was implemented on Jul 28, 2018 as per QA team suggestion.*/
                            Common.sendReportThroughMail(request, downloadFolder, (String) vals[0], user.getUserName(), (String) vals[7]);
                        }
                        session.setAttribute("toolObj", toolObj);
                        ModelAndView mAndV = new ModelAndView(new RedirectView("lxrm-thanks.html"), "marketplaceCreditCard", marketplaceCreditCard);
                        mAndV.addObject("paymentModel", paymentModel);
                        return mAndV;
                    }
                    if (paymentRecordId <= 0) {
                        ModelAndView mAndV = new ModelAndView(getFormView(), "marketplaceCreditCard", marketplaceCreditCard);
                        mAndV.addObject("errorMessage", failureMessage.length() != 0 ? failureMessage.toString() : "Oh snap! ".concat(GatewayService.AUTH_FAILURE).concat(" Please try again."));
//                        mAndV.addObject("failurMessage", failureMessage.length() != 0 ? failureMessage.toString() : "Failed to Authorize this card through Braintree. Use a different Credit / Debit Card or Try Again.");
                        return mAndV;
                    }

                } else if (WebUtils.hasSubmitParameter(request, "cancel")) {
                    vals = (Object[]) session.getAttribute("valsObjArray");
                    if (vals != null && vals.length > 0) {
                        String toolLink = (String) vals[8];
                        LxrmAskExpert lxrmAskExpert = (LxrmAskExpert) session.getAttribute("lxrmAskExpert");
                        if (lxrmAskExpert != null) {
                            session.removeAttribute("lxrmAskExpert");
                            int[] singlePageRequestTools = {24, 36, 37, 27, 40, 41, 42, 43, 35};
                            if ((lxrmAskExpert.getPageUrl().contains("getResult=")
                                    || lxrmAskExpert.getPageUrl().contains("backLink=") || lxrmAskExpert.getPageUrl().contains("ViewResult=")
                                    || lxrmAskExpert.getPageUrl().contains("query=") || lxrmAskExpert.getPageUrl().contains("backToSuccessPage=")
                                    || IntStream.of(singlePageRequestTools).anyMatch(id -> id == lxrmAskExpert.getToolId()))) {
                                response.sendRedirect(hostingURL + "/" + toolLink + "?backToSuccessPage='backToSuccessPage'");
                            } else {
                                response.sendRedirect(hostingURL + "/" + toolLink);
                            }
                        } else {
                            response.sendRedirect(toolLink + "?backToSuccessPage='backToSuccessPage'");
                        }
                        return null;

                    } else {
                        logger.debug("vals contain NULL.....");
                        ModelAndView mAndV = new ModelAndView(getFormView(), "marketplaceCreditCard", marketplaceCreditCard);
                        return mAndV;
                    }
                } else if (WebUtils.hasSubmitParameter(request, "checkPromo")) {
                    /**
                     * Fetching all PromoCodes from DB and Validating user
                     * entered PromoCode is valid or not
                     */
                    String userEnterdPromoCode = request.getParameter("promo");
                    PromoCodes promos = marketplacePaymentService.fetchingPromoCodeInfo(userEnterdPromoCode);
                    if (promos != null) {
                        Date promoCodeStartDate = promos.getStartDate();
                        Date promoCodeEndtDate = promos.getEndDate();
                        Calendar todayCal = Calendar.getInstance();
                        //Checking PromoCode is in valid or not
                        boolean isValidPromoDate = Common.checkDateBetweenTwoDates(todayCal.getTime(), promoCodeStartDate, promoCodeEndtDate);
                        if (isValidPromoDate) {
                            //Check howmany times already used
                            int previousUsedCount = marketplacePaymentService.checkPromoCodeUsedCount(user.getId(), promos.getPromoId());
                            if (previousUsedCount < promos.getReusableCount()) {
                                session.setAttribute("promoCodeId", promos.getPromoId());
                                session.setAttribute("promoCodeUsedCount", previousUsedCount);
                                marketplacePaymentService.addInJson(1, "Promo code applied successfully. " + promos.getPromoName(), response);
                                return null;
                            } else {
                                marketplacePaymentService.addInJson(0, "You have already availed this promo code. Please use any other active and valid promo code.", response);
                                return null;
                            }

                        } else {
                            marketplacePaymentService.addInJson(0, "Expired promo code. Please try again with any other active promo code.", response);
                            return null;
                        }

                    } else {
                        marketplacePaymentService.addInJson(0, "Invalid promo code. Please try again with any other valid promo code.", response);
                        return null;
                    }
                } else if (WebUtils.hasSubmitParameter(request, "applyPromo")) {
                    int promoCodeId = 0, promoCodeUsedCount = 0;
                    if (session.getAttribute("promoCodeId") != null) {
                        promoCodeId = (int) session.getAttribute("promoCodeId");
                    }
                    if (session.getAttribute("promoCodeUsedCount") != null) {
                        promoCodeUsedCount = (int) session.getAttribute("promoCodeUsedCount");
                    }
                    //Updating promocode used count
                    marketplacePaymentService.updatePromoCodeUsedCount(user.getId(), promoCodeId, promoCodeUsedCount);

                    Tool toolObj = toolService.getToolInfoFromToolTab((Integer) vals[1], (String) vals[6], (Double) vals[5]);
                    session.setAttribute("toolObj", toolObj);
                    session.setAttribute("freePromoCode", true);
                    ModelAndView mAndV = new ModelAndView(new RedirectView("lxrm-thanks.html"), "marketplaceCreditCard", marketplaceCreditCard);
                    return mAndV;
                }
            } else {
                ModelAndView mAndV = new ModelAndView(new RedirectView("lxrmarketplace.html"), "marketplaceCreditCard", marketplaceCreditCard);
                return mAndV;
            }

            ModelAndView mAndV = new ModelAndView(getFormView(), "marketplaceCreditCard", marketplaceCreditCard);
            return mAndV;
        } else {
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/");
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "marketplaceCreditCard", marketplaceCreditCard);
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        LxrMCreditCard marketplaceCreditCard = (LxrMCreditCard) session.getAttribute("marketplaceCreditCard");
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (marketplaceCreditCard == null) {
            marketplaceCreditCard = new LxrMCreditCard();
            marketplaceCreditCard.setUserId(user.getId());
            session.setAttribute("marketplaceCreditCard", marketplaceCreditCard);
            if (userLoggedIn) {
                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                session.setAttribute("geoLocation", geoLocation);
            }
        }
        return marketplaceCreditCard;
    }

}
