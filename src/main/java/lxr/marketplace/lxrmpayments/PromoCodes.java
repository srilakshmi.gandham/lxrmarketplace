/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.lxrmpayments;

import java.util.Date;

/**
 *
 * @author sagar
 */
public class PromoCodes {

    private int promoId;
    private String promoCode;
    private Date startDate;
    private Date endDate;
    private String promoName;
    private int reusableCount;

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int promoId) {
        this.promoId = promoId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    public int getReusableCount() {
        return reusableCount;
    }

    public void setReusableCount(int reusableCount) {
        this.reusableCount = reusableCount;
    }

}
