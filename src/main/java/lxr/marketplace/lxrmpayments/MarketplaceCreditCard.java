package lxr.marketplace.lxrmpayments;

public class MarketplaceCreditCard {

	public static int PAYPAL = 1;
	public static int AUTHORIZE = 2;
	
	private long lxrCardId;
	private long userId;
	private String cardNo;
	private String firstName;
	private String lastName;
	private String type;
	private String cvv;
	private String AuthorizationCode;
	private int expireMonth;
	private int expireYear;
	
	
	private long custProfileId;
	private long paymentProfileId;
	
	/********** Address ********/
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String countryCode;
	private String postalCode;
	private String state;
	

	public long getCustProfileId() {
		return custProfileId;
	}
	public void setCustProfileId(long custProfileId) {
		this.custProfileId = custProfileId;
	}
	public long getPaymentProfileId() {
		return paymentProfileId;
	}
	public void setPaymentProfileId(long paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}
	public long getLxrCardId() {
		return lxrCardId;
	}
	public void setLxrCardId(long lxrCardId) {
		this.lxrCardId = lxrCardId;
	}	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getExpireMonth() {
		return expireMonth;
	}
	public void setExpireMonth(int expireMonth) {
		this.expireMonth = expireMonth;
	}
	public int getExpireYear() {
		return expireYear;
	}
	public void setExpireYear(int expireYear) {
		this.expireYear = expireYear;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}	
	public String getAuthorizationCode() {
		return AuthorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		AuthorizationCode = authorizationCode;
	}
}

