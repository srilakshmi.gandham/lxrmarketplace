package lxr.marketplace.robotstxtgenerator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class RobotsTxtGenerator {
	private List robotsTxtDirectiveSettings = LazyList.decorate(new ArrayList(),FactoryUtils.instantiateFactory(RobotsTxtDirectiveSettings.class));
	String robotsTxtUrl;
	String sitemapUrl;
	long directiveCount;
	String robotTxtContent;
	
	private int addDirectiveType;         //1 means -- Allow
	  //2 means -- Disallow	

	private int userAgentType;            //1 means -- Predefined
		  //2 means -- User Defined	
	private String userAgentName;
	private String directoryOrFilePath;
	private String specialUserAgentName;
	
	
	public String getSpecialUserAgentName() {
		return specialUserAgentName;
	}
	public void setSpecialUserAgentName(String specialUserAgentName) {
		this.specialUserAgentName = specialUserAgentName;
	}
	public int getAddDirectiveType() {
		return addDirectiveType;
	}
	public void setAddDirectiveType(int addDirectiveType) {
		this.addDirectiveType = addDirectiveType;
	}
	public int getUserAgentType() {
		return userAgentType;
	}
	public void setUserAgentType(int userAgentType) {
		this.userAgentType = userAgentType;
	}
	public String getUserAgentName() {
		return userAgentName;
	}
	public void setUserAgentName(String userAgentName) {
		this.userAgentName = userAgentName;
	}
	public String getDirectoryOrFilePath() {
		return directoryOrFilePath;
	}
	public void setDirectoryOrFilePath(String directoryOrFilePath) {
		this.directoryOrFilePath = directoryOrFilePath;
	}
	public List getRobotsTxtDirectiveSettings() {
		return robotsTxtDirectiveSettings;
	}
	public void setRobotsTxtDirectiveSettings(List robotsTxtDirectiveSettings) {
		this.robotsTxtDirectiveSettings = robotsTxtDirectiveSettings;
	}
	public String getRobotsTxtUrl() {
		return robotsTxtUrl;
	}
	public void setRobotsTxtUrl(String robotsTxtUrl) {
		this.robotsTxtUrl = robotsTxtUrl.trim();
	}
	public String getSitemapUrl() {
		return sitemapUrl;
	}
	public void setSitemapUrl(String sitemapUrl) {
		this.sitemapUrl = sitemapUrl;
	}
	public long getDirectiveCount() {
		return directiveCount;
	}
	public void setDirectiveCount(long directiveCount) {
		this.directiveCount = directiveCount;
	}
	public String getRobotTxtContent() {
		return robotTxtContent;
	}
	public void setRobotTxtContent(String robotTxtContent) {
		this.robotTxtContent = robotTxtContent;
	}
	

}