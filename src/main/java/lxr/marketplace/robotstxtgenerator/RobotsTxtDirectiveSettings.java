package lxr.marketplace.robotstxtgenerator;

public class RobotsTxtDirectiveSettings {
	
	 int id;
	 int addDirectiveType;         //1 means -- Allow
										  //2 means -- Disallow	
	 
	 int userAgentType;            //1 means -- Predefined
										  //2 means -- User Defined	
	 String userAgentName;
	 String directoryOrFilePath;
	 String specialUserAgentName;
//	 private String addDirectiveName;
	
	
	public String getSpecialUserAgentName() {
		return specialUserAgentName;
	}
	public void setSpecialUserAgentName(String specialUserAgentName) {
		this.specialUserAgentName = specialUserAgentName;
	}
	public int getAddDirectiveType() {
		return addDirectiveType;
	}
	public void setAddDirectiveType(int addDirectiveType) {
		this.addDirectiveType = addDirectiveType;
	}
	public int getUserAgentType() {
		return userAgentType;
	}
	public void setUserAgentType(int userAgentType) {
		this.userAgentType = userAgentType;
	}
	public String getUserAgentName() {
		return userAgentName;
	}
	public void setUserAgentName(String userAgentName) {
		this.userAgentName = userAgentName;
	}
	public String getDirectoryOrFilePath() {
		return directoryOrFilePath;
	}
	public void setDirectoryOrFilePath(String directoryOrFilePath) {
		this.directoryOrFilePath = directoryOrFilePath;
	}
	 public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
		
}