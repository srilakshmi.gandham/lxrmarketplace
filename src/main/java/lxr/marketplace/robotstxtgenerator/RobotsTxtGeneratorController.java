package lxr.marketplace.robotstxtgenerator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class RobotsTxtGeneratorController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(RobotsTxtGeneratorController.class);
    private EmailTemplateService emailTemplateService;
    private ToolService toolService;
    private RobotsTxtGeneratorService robotsTxtGeneratorService;
    private String downloadFolder;
    JSONObject userInputJson = null;

    public String getDownloadFolder() {
        return downloadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }
    @Autowired
    private MessageSource messageSource;

    public void setRobotsTxtGeneratorService(
            RobotsTxtGeneratorService robotsTxtGeneratorService) {
        this.robotsTxtGeneratorService = robotsTxtGeneratorService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public RobotsTxtGeneratorController() {
        setCommandClass(RobotsTxtGenerator.class);
        setCommandName("robotsTxtGeneratorTool");
    }

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        RobotsTxtGenerator robotsTxtGenerator = (RobotsTxtGenerator) command;
        String robotTxtCOntent = "", finalURL = "";
        session.setAttribute("robotsTxtGenerator", robotsTxtGenerator);
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(24);
            toolObj.setToolName("Robots.txt Generator Tool");
            toolObj.setToolLink("robots-txt-generator-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.ROBOTS_TXT_GENERATOR);
            if (WebUtils.hasSubmitParameter(request, "getResult")) {
                JSONArray totRobotsArr = null;
                Session userSession = (Session) session.getAttribute("userSession");
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    RobotsTxtGenerator robotsTxtGeneratorATE = null;
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = null;
                    if (session.getAttribute("robotsTxtGeneratorATE") != null) {
                        robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("robotsTxtGeneratorATE");
                        mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGeneratorATE);
                        HashMap<String, String> robotNames = Common.loadRobotNames();
                        mAndV.addObject("robotNames", robotNames);
                        /*To make ajax call to show existed result from ask our exited page.*/
                        mAndV.addObject("showExistedResult", true);
                    } else {
                        mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
                        HashMap<String, String> robotNames = Common.loadRobotNames();
                        mAndV.addObject("robotNames", robotNames);
                    }
                    return mAndV;
                }

                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }

                robotsTxtGenerator.setSitemapUrl(request.getParameter("siteMap"));
                if (request.getParameter("totRobotsArr") != null) {
                    totRobotsArr = new JSONArray(request.getParameter("totRobotsArr"));
                    logger.debug("totRobotsArr.........." + totRobotsArr);
                    /*for(int i = 0; i < totRobotsArr.length(); i++){
                                        JSONObject robotsObj = totRobotsArr.getJSONObject(i);
                                        if(!(robotsObj.getString("userAg").trim().equals("") && robotsObj.getString("totUAStr").trim().equals(""))){
                                                logger.info("user agent val is  " + i + ": " +robotsObj.getString("totUAStr"));
                                        }
                                    }*/

                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson = new JSONObject();
                        userInputJson.put("2", request.getParameter("totRobotsArr"));
                    }
                }

                Map<String, String> finalResultMap = robotsTxtGeneratorService.createUserInputDataMap(totRobotsArr);
                String finalRobotsTxt = robotsTxtGeneratorService.generateRobotsTxtContent(finalResultMap, robotsTxtGenerator);
                robotsTxtGenerator.setRobotTxtContent(finalRobotsTxt);
                logger.debug("finalRobotsTxt......." + finalRobotsTxt);
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
                robotsTxtGeneratorService.addInJson(finalRobotsTxt, response);
                /*Both robotsTxtGenerator,finalRobotsTxt is using for request handle of download and AskOurExpert.*/
                session.setAttribute("finalRobotsTxt", finalRobotsTxt);
                session.setAttribute("robotsTxtGeneratorATE", robotsTxtGenerator);

                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
                HashMap<String, String> robotNames = Common.loadRobotNames();
                mAndV.addObject("robotNames", robotNames);
                if (session.getAttribute("robotsTextGeneratorValidURl") != null) {
                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }
//                                    if the request is coming from tool page then only add the user inputs to JSONObject
//                                    if the request is coming from mail then do not add the user inputs to JSONObject
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson.put("3", robotsTxtGenerator.getAddDirectiveType());
                        userInputJson.put("4", robotsTxtGenerator.getUserAgentName());
                        userInputJson.put("5", robotsTxtGenerator.getDirectoryOrFilePath());
                        userInputJson.put("6", robotsTxtGenerator.getSitemapUrl());
                        userInputJson.put("7", robotsTxtGenerator.getRobotTxtContent());
                        userInputJson.put("2", request.getParameter("totRobotsArr"));
                    }
                }
                /*To make ajax call to show existed result from ask our exited page.*/
                mAndV.addObject("showExistedResult", false);
                return mAndV;

            } else if (WebUtils.hasSubmitParameter(request, "import")) {
                HashMap<String, String> robotNames = Common.loadRobotNames();
                List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings = new ArrayList<RobotsTxtDirectiveSettings>();
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
                session.removeAttribute("userRobotURLATE");
                if (request.getParameter("finalURL") != null) {
                    robotsTxtGenerator.setRobotsTxtUrl(request.getParameter("finalURL").trim());
                    //Validate the URL
                    /*finalURL = robotsTxtGeneratorService.validateURL(robotsTxtGenerator.getRobotsTxtUrl());*/
                    finalURL = Common.getUrlValidation(robotsTxtGenerator.getRobotsTxtUrl(), session);
                    RobotsTxtDirectiveSettings[] robotsArray = null;
                    if (finalURL.equals("invalid") || finalURL.equals("redirected")) {
                        session.removeAttribute("tempRobotsTxtDirectiveSettings");
                        session.setAttribute("robotsTxtGenerator", robotsTxtGenerator);
                        session.removeAttribute("indexValList");
                        session.removeAttribute("finalRobotsTxt");
                        session.removeAttribute("robotsTxtGeneratorATE");
                        session.removeAttribute("robotTxtTypeATE");
                        session.removeAttribute("userRobotURLATE");
                        robotTxtCOntent = messageSource.getMessage("URL.error", null, Locale.US);
                    } else {
                        boolean robotsTxtPresent = Common.checkPresenceOfRobotsTxt(finalURL);
                        if (!robotsTxtPresent) {
                            /*Removing query params or request params from URL and preparing final query URL i.e protocol://HostName */
                            String tempQueryURL = Common.getDomainWithProtocol(finalURL);
                            if (tempQueryURL != null && !tempQueryURL.equals(finalURL)) {
                                finalURL = tempQueryURL;
                            }
                        }
                        session.setAttribute("userRobotURLATE", finalURL);
                        robotsTxtGenerator.setRobotsTxtUrl(finalURL);
                        if (finalURL.endsWith("/")) {
                            finalURL = finalURL + "robots.txt";
                        } else {
                            finalURL = finalURL + "/robots.txt";
                        }
                        session.removeAttribute("robotTxtTypeATE");
                        robotTxtCOntent = robotsTxtGeneratorService.fetchRobotTxtFileContent(robotsTxtGenerator, finalURL, robotsTxtDirectiveSettings, robotNames);
                        robotsArray = robotsTxtDirectiveSettings.toArray(new RobotsTxtDirectiveSettings[robotsTxtDirectiveSettings.size()]);
                        if (robotsArray.length == 0) {
                            robotTxtCOntent = messageSource.getMessage("URL.error", null, Locale.US);
                        }
                        session.setAttribute("robotsTextGeneratorValidURl", finalURL);
                        /*if(robotTxtCOntent !=null && !robotTxtCOntent.trim().equals("")){
                                                session.setAttribute("robotsTextGeneratorValidURl", finalURL);
                                        }*/
                    }
                    robotsTxtGeneratorService.addInJson2(robotsTxtGenerator.getSitemapUrl(), robotsArray, robotTxtCOntent, response, robotsTxtGenerator);
                    session.setAttribute("robotTxtTypeATE", "importRobotsType");
                    robotsTxtGenerator.setRobotTxtContent(robotTxtCOntent);
                    robotsTxtGenerator.setRobotsTxtDirectiveSettings(robotsTxtDirectiveSettings);

                    if (robotTxtCOntent.equals("")) {
                        session.setAttribute("emptyRobotsTxtATE", robotsTxtGenerator);
                    }
                }
                mAndV.addObject("robotNames", robotNames);
                /*To make ajax call to show existed result from ask our exited page.*/
                mAndV.addObject("showExistedResult", false);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                robotsTxtGenerator = new RobotsTxtGenerator();
                robotsTxtGenerator.setDirectoryOrFilePath("/");

                session.removeAttribute("tempRobotsTxtDirectiveSettings");
                session.setAttribute("robotsTxtGenerator", robotsTxtGenerator);
                session.removeAttribute("indexValList");
                session.removeAttribute("finalRobotsTxt");
                session.removeAttribute("robotsTxtGeneratorATE");
                session.removeAttribute("robotTxtTypeATE");
                session.removeAttribute("userRobotURLATE");

                //session.removeAttribute("robotsTxtATE");
                //session.removeAttribute("robotTxtContentATE");
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
                HashMap<String, String> robotNames = Common.loadRobotNames();
                mAndV.addObject("robotNames", robotNames);
                mAndV.addObject("clearAll", "clearAll");
                /*To make ajax call to show existed result from ask our exited page.*/
                mAndV.addObject("showExistedResult", false);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                /*Timestamp startTime= new Timestamp(Calendar.getInstance().getTimeInMillis());*/
                long startTime = Calendar.getInstance().getTimeInMillis();
                if (session.getAttribute("finalRobotsTxt") != null) {
                    /*toolFolderName purpose is to create TemporaryFolder for Robots.txt Generator tool, with reference to pavan sir mail.*/
                    String toolFolderName = downloadFolder + "RobotsTxtGenerator/";
                    /*String folderPath = robotsTxtGeneratorService.createTemporaryFolder(downloadFolder,(long)user.getId(),startTime);*/
                    String folderPath = robotsTxtGeneratorService.createTemporaryFolder(toolFolderName, (long) user.getId(), startTime);
                    String filename = robotsTxtGeneratorService.createRobotsTxtFile((String) session.getAttribute("finalRobotsTxt"), folderPath);
                    if (filename != null) {
                        Common.downloadReport(response, folderPath, "/" + filename, "txt");
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
                HashMap<String, String> robotNames = Common.loadRobotNames();
                mAndV.addObject("robotNames", robotNames);
                mAndV.addObject("toolNotWorkingUrl", "");
                /*To make ajax call to show existed result from ask our exited page.*/
                mAndV.addObject("showExistedResult", false);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "robotsATE")) {
                logger.debug("Handling request ATE for this tool");
                /*To  Handle Ask Our Expert request*/
                Session userSession = (Session) session.getAttribute("userSession");
                String robotsURL = request.getParameter("robotsURL");

                /*To check issues related to analysis: Help in Creating Robots.txt.Refer Tool Based Analysis.*/
                boolean robotsDoesNotExisted = false;
                String preparRobotTxtContent = "empty";
                RobotsTxtGenerator robotsTxtGeneratorATE = null;
                if (session.getAttribute("robotsTxtGeneratorATE") != null) {
                    robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("robotsTxtGeneratorATE");
                } else if (session.getAttribute("emptyRobotsTxtATE") != null) {
                    robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("emptyRobotsTxtATE");
                }
                if (robotsTxtGeneratorATE != null) {
                    preparRobotTxtContent = robotsTxtGeneratorATE.getRobotTxtContent();
                }
                if (!preparRobotTxtContent.equals("empty")) {
                    if ((preparRobotTxtContent.equals("")) || (preparRobotTxtContent.trim().equals(""))) {
                        /* Robots.txt is not existed for url, issue existed for analysis: Help in Creating Robots.txt*/
                        robotsDoesNotExisted = true;
                        logger.debug("Issue related to analysis: Help in Creating Robots.txt.Refer Tool Based Analysis is exsited");
                    }
                }
                /*End of Help in Creating Robots.txt.*/

 /*To check issues related below analysis:
                        1)Help in Validating disallowed webpages 
                        2)Help in Validating allowed webpages
                        Refer Tool Based Analysis sheet.*/
                boolean directivesIssues = false;
                String finalRobotsTxt = "";
                if (session.getAttribute("finalRobotsTxt") != null) {
                    finalRobotsTxt = (String) session.getAttribute("finalRobotsTxt");
                }
                int numberofDisallow = 0;
                int numberofAllow = 0;

                String finalRobotsArray[] = finalRobotsTxt.split("\n");
                for (String directive : finalRobotsArray) {
                    if (!(directive.equals(""))) {
                        /*Analysis name : Help in Validating allowed webpages. */
                        boolean isAllowDirective = robotsTxtGeneratorService.checkIsTextPresent(directive, "allow");
                        if (isAllowDirective) {
                            numberofAllow++;
                        }
                        /*Analysis name : Help in Validating DisAllowed webpages. */
                        boolean isDisAllowDirective = robotsTxtGeneratorService.checkIsTextPresent(directive, "disallow");
                        if (isDisAllowDirective) {
                            numberofDisallow++;
                        }

                    }
                }
                if ((numberofAllow != 0) || (numberofDisallow != 0)) {
                    directivesIssues = true;
                }
                /*End of Help in Validating allowed webpages and Help in Validating DisAllowed webpages analysis.*/

                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                String toolIssues = "";
                String questionInfo = "";
                /*Show Ask Our Expert PopUp if atleast one issue is Existed*/
                if (directivesIssues) {
                    questionInfo = "Need help in fixing following issues with my website '" + robotsURL + "'.\n\n";
                    toolIssues = "<ul>";
                    if (numberofDisallow != 0) {
                        questionInfo += "- Validating links that are excluded in Robots.txt file.\n";
                        /* if(numberofDisallow >= 2){
                                            toolIssues += "<li>"+"You are currently blocking search engines from indexing <span style='color:red;'>"+numberofDisallow+"</span> links on your website."
                                            + " We can help you identify which links you may be inproperly blocking."+"</li>";
                                        }else if(numberofDisallow == 1){
                                            toolIssues += "<li>"+"You are currently blocking search engines from indexing <span style='color:red;'>"+numberofDisallow+"</span> link on your website."
                                            + " We can help you identify which link you may be inproperly blocking."+"</li>";
                                        } */
                        toolIssues += "<li>" + "You are currently blocking search engines from indexing <span style='color:red;'>" + numberofDisallow + "</span> link's on your website."
                                + " We can help you identify which link's you may be inproperly blocking." + "</li>";
                    }

                    if (numberofAllow != 0) {
                        questionInfo += "- Validating links that are included in Robots.txt file.\n";
                        /*if(numberofAllow >=2){
                                        toolIssues += "<li>"+"You are currently allowing search engines to index <span style='color:red;'>"+numberofAllow+"</span> links on your website."
                                                + "  We can help you identify which links search engines should not index."+"</li>";
                                        }else if(numberofAllow == 1){
                                            toolIssues += "<li>"+"You are currently allowing search engines to index <span style='color:red;'>"+numberofAllow+"</span> link on your website."
                                                + "  We can help you identify which link search engines should not index."+"</li>";
                                        }
                         */
                        toolIssues += "<li>" + "You are currently allowing search engines to index <span style='color:red;'>" + numberofAllow + "</span> link's on your website."
                                + "  We can help you identify which link's search engines should not index." + "</li>";
                    }
                    toolIssues += "</ul>";
                }
                if (robotsDoesNotExisted) {
                    questionInfo = "Need help in fixing following issues with my website '" + robotsURL + "'.\n\n";
                    toolIssues = "<ul>";
                    if (preparRobotTxtContent.trim().equals("")) {
                        questionInfo += "- Creating Robots.txt file from my website.\n";
                        toolIssues += "<li>" + "Your website does not currently have a robots.txt file. "
                                + "Seach engines currently have access to index all of the pages on your website.</li>";
                    }
                    toolIssues += "</ul>";
                }
                lxrmAskExpert.setDomain(robotsURL);
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(toolObj.getToolName());
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                                    if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), robotsURL, toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }

                return null;

            }/*End of ASK OUR Expert*/ else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                /*Navigation to user to show existed result from ask our expert cancel page.*/
                RobotsTxtGenerator robotsTxtGeneratorATE = null;
                if (session.getAttribute("robotsTxtGeneratorATE") != null) {
                    robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("robotsTxtGeneratorATE");
                } else {
                    robotsTxtGeneratorATE = new RobotsTxtGenerator();
                }

                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGeneratorATE);
                HashMap<String, String> robotNames = Common.loadRobotNames();
                mAndV.addObject("robotNames", robotNames);
                /*To make ajax call to show existed result from ask our exited page.*/
                mAndV.addObject("showExistedResult", true);
                String robotTxtTypeATE = "";
                if (session.getAttribute("robotTxtTypeATE") != null) {
                    robotTxtTypeATE = (String) session.getAttribute("robotTxtTypeATE");
                    if (robotTxtTypeATE.equals("importRobotsType")) {
                        String robotsURLATE = "";
                        if (session.getAttribute("userRobotURLATE") != null) {
                            robotsURLATE = (String) session.getAttribute("userRobotURLATE");
                            mAndV.addObject("robotsURLATE", robotsURLATE);
                        }
                    }

                }
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "report")) {
                int format = Integer.parseInt(request.getParameter("report"));
                if (format == 1 && WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    RobotsTxtGenerator robotsTxtGeneratorATE = null;
                    if (session.getAttribute("robotsTxtGeneratorATE") != null) {
                        robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("robotsTxtGeneratorATE");
                    } else {
                        robotsTxtGeneratorATE = new RobotsTxtGenerator();
                    }

                    ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGeneratorATE);
                    HashMap<String, String> robotNames = Common.loadRobotNames();
                    mAndV.addObject("robotNames", robotNames);
                    /*To make ajax call to show existed result from ask our exited page.*/
                    mAndV.addObject("showExistedResult", true);
                    String robotTxtTypeATE = "";
                    if (session.getAttribute("robotTxtTypeATE") != null) {
                        robotTxtTypeATE = (String) session.getAttribute("robotTxtTypeATE");
                        if (robotTxtTypeATE.equals("importRobotsType")) {
                            String robotsURLATE = "";
                            if (session.getAttribute("userRobotURLATE") != null) {
                                robotsURLATE = (String) session.getAttribute("userRobotURLATE");
                                mAndV.addObject("robotsURLATE", robotsURLATE);
                            }
                        }

                    }
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "showExistedResult")) {
                RobotsTxtGenerator robotsTxtGeneratorATE = null;
                if (session.getAttribute("robotsTxtGeneratorATE") != null) {
                    robotsTxtGeneratorATE = (RobotsTxtGenerator) session.getAttribute("robotsTxtGeneratorATE");
                } else {
                    robotsTxtGeneratorATE = new RobotsTxtGenerator();
                }
                String robotTxtTypeATE = "";
                String robotsURLATE = "";
                if (session.getAttribute("robotTxtTypeATE") != null) {
                    robotTxtTypeATE = (String) session.getAttribute("robotTxtTypeATE");
                    if (robotTxtTypeATE.equals("importRobotsType")) {
                        if (session.getAttribute("userRobotURLATE") != null) {
                            robotsURLATE = (String) session.getAttribute("userRobotURLATE");
                        }
                    }

                }
                List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings = new ArrayList<>();
                robotsTxtGenerator.setRobotsTxtUrl(robotsURLATE);
                HashMap<String, String> robotNames = Common.loadRobotNames();
                robotTxtCOntent = robotsTxtGeneratorATE.getRobotTxtContent();
                robotsTxtDirectiveSettings = robotsTxtGeneratorService.constructRobotsTxtList(robotTxtCOntent, robotNames);
                if (robotsTxtDirectiveSettings.isEmpty()) {
                    robotsTxtDirectiveSettings = robotsTxtGeneratorATE.getRobotsTxtDirectiveSettings();
                }
                RobotsTxtDirectiveSettings[] robotsArray = robotsTxtDirectiveSettings.toArray(new RobotsTxtDirectiveSettings[robotsTxtDirectiveSettings.size()]);
                if (robotTxtCOntent != null && !robotTxtCOntent.trim().equals("")) {
                    session.setAttribute("robotsTextGeneratorValidURl", finalURL);
                }
                robotsTxtGeneratorService.addInJson2(robotsTxtGeneratorATE.getSitemapUrl(), robotsArray, robotTxtCOntent, response, robotsTxtGeneratorATE);
                return null;
            }/*End of showExistedResult request.*/

            robotsTxtGenerator = new RobotsTxtGenerator();
            ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
            HashMap<String, String> robotNames = Common.loadRobotNames();
            mAndV.addObject("robotNames", robotNames);
            mAndV.addObject("showExistedResult", false);
            return mAndV;
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtGeneratorTool", robotsTxtGenerator);
        HashMap<String, String> robotNames = Common.loadRobotNames();
        mAndV.addObject("robotNames", robotNames);
        mAndV.addObject("showExistedResult", false);
        return mAndV;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        RobotsTxtGenerator robotsTxtGenerator = (RobotsTxtGenerator) session.getAttribute("robotsTxtGenerator");
        if (robotsTxtGenerator == null) {
            robotsTxtGenerator = new RobotsTxtGenerator();
            robotsTxtGenerator.setDirectoryOrFilePath("/");
        }
        return robotsTxtGenerator;
    }

}
