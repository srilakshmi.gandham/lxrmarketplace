package lxr.marketplace.robotstxtgenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import lxr.marketplace.util.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class RobotsTxtGeneratorService extends JdbcDaoSupport {

    static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";

    /*
			 * Modified on oct-7 2015
			 * "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; de-de) AppleWebKit/523.10.3 (KHTML, like Gecko) Version/3.0.4 Safari/523.10";*/
    public String fetchRobotTxtFileContent(RobotsTxtGenerator robotsTxtGenerator, String finalURL,
            List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings, HashMap<String, String> robotNames) {
        String robotsTxtContent = "";
        try {
            URL robotURL = new URL(finalURL);
            URLConnection connectionObj = robotURL.openConnection();
            connectionObj.setConnectTimeout(50000);
            connectionObj.setReadTimeout(30000);
            connectionObj.setRequestProperty("User-Agent", USER_AGENT);
            BufferedReader in = new BufferedReader(new InputStreamReader(connectionObj.getInputStream()));
            String inputLine = "";
            String userAgent = "";
            String specialUserAgent = "";
            String directiveName = "";
            int count = 1;
            int indexVal = 0;
            String oldSiteMapName = "";
            String siteMapName = "";
            if (in != null) {
                while ((inputLine = in.readLine()) != null) {
                    robotsTxtContent = robotsTxtContent.concat("\n" + inputLine);
                    if (!inputLine.startsWith("#")) {
                        boolean isUserAgent = checkIsUAPresent(inputLine, "User-agent");
                        if (isUserAgent) {
                            userAgent = "";
                            userAgent = getPropertyValue(inputLine).trim();
                            logger.debug("userAgent........" + userAgent);
                            if (userAgent.equals("*")) {
                                userAgent = "All";
                            } else {
                                Set<String> robotkeyNames = robotNames.keySet();
                                Set<String> tempSet = new HashSet<String>();
                                if (robotkeyNames != null && robotkeyNames.size() > 0) {
                                    for (String robotUA : robotkeyNames) {
                                        tempSet.add(robotUA.toLowerCase());
                                    }
                                }
                                if (!tempSet.contains(userAgent.toLowerCase())) {
                                    specialUserAgent = userAgent;
                                    userAgent = "Specify";
                                }
                            }
                        } else {
                            int directiveType = 1;
                            boolean isDirective = checkIsTextPresent(inputLine, "allow");
                            if (isDirective) {
                                directiveType = 1;
                            } else {
                                isDirective = checkIsTextPresent(inputLine, "disallow");
                                if (isDirective) {
                                    directiveType = 2;
                                }
                            }
                            if (isDirective) {
                                RobotsTxtDirectiveSettings robotsTxtSettings = new RobotsTxtDirectiveSettings();
                                robotsTxtSettings.setId(indexVal);
                                robotsTxtSettings.setAddDirectiveType(directiveType);
                                robotsTxtSettings.setUserAgentName(userAgent);
                                directiveName = getPropertyValue(inputLine).trim();
                                robotsTxtSettings.setDirectoryOrFilePath(directiveName);
                                indexVal = indexVal + 1;
                                if (!specialUserAgent.equals("")) {
                                    robotsTxtSettings.setSpecialUserAgentName(specialUserAgent);
                                }
                                robotsTxtDirectiveSettings.add(robotsTxtSettings);
                            }
                            boolean isSiteMap = checkIsTextPresent(inputLine, "sitemap");
                            if (isSiteMap) {
                                if (count > 1) {
                                    siteMapName = getPropertyValue(inputLine).trim();
                                    oldSiteMapName = oldSiteMapName + "\n" + siteMapName;
                                } else {
                                    oldSiteMapName = getPropertyValue(inputLine).trim();
                                }
                                count++;
                            }
                            robotsTxtGenerator.setSitemapUrl(oldSiteMapName);
                        }
                    }
                }
            }
            robotsTxtGenerator.setRobotsTxtDirectiveSettings(robotsTxtDirectiveSettings);
            in.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error("IOException in fetching existing RobotTxtFileContent: ", e);
        } catch (Exception e) {
            logger.error("Exception in fetchRobotTxtFileContent: ", e);
        }
        robotsTxtGenerator.setRobotTxtContent(robotsTxtContent);
        return robotsTxtContent;
    }

    public boolean checkIsUAPresent(String inputContent, String checkedText) {
        boolean isPresent = false;
        if (inputContent.trim().toLowerCase().contains(checkedText.toLowerCase())) {
            isPresent = true;
        }
        return isPresent;
    }

    public boolean checkIsTextPresent(String inputContent, String checkedText) {
        boolean isPresent = false;
        if (inputContent.trim().toLowerCase().startsWith(checkedText)) {
            isPresent = true;
        }
        return isPresent;
    }

    public String getPropertyValue(String inputContent) {
        String textValue = "";
        textValue = inputContent.substring(inputContent.indexOf(':') + 1, inputContent.length());
        return textValue;

    }

    public Map<String, String> createExistingRobotDataMap(RobotsTxtGenerator robotsTxtGenerator, List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings) {  //Preparing Map with existing robots.txt file content 
        Map<String, String> existingDataMap = new LinkedHashMap<String, String>();
        String totString = "";
        String addDir = "";
        String userAgent = "";
        for (RobotsTxtDirectiveSettings robotDirObj : robotsTxtDirectiveSettings) {
            userAgent = "";
            addDir = "";
            totString = "";
            if (robotDirObj.getAddDirectiveType() == 1) {
                addDir = "Allow: " + robotDirObj.getDirectoryOrFilePath().trim();
            } else if (robotDirObj.getAddDirectiveType() == 2) {
                addDir = "Disallow: " + robotDirObj.getDirectoryOrFilePath().trim();
            }
            if (robotDirObj.getUserAgentName() == "Specify") {
                userAgent = robotDirObj.getSpecialUserAgentName().trim().toLowerCase();
            } else {
                userAgent = robotDirObj.getUserAgentName().trim().toLowerCase();
            }
            try {
                if (existingDataMap != null && existingDataMap.size() > 0) {
                    if (existingDataMap.containsKey(userAgent)) {
                        logger.info("present key......");
                        String mapValue = existingDataMap.get(userAgent);
                        String[] mapValArr = mapValue.split("\n");
                        for (String mapArrObj : mapValArr) {
                            if (mapArrObj.equals(addDir)) {
                                totString = "";
                                break;
                            } else {
                                totString = "\n" + addDir;
                            }
                            logger.info("totString......" + totString);
                        }
                        mapValue = mapValue + totString;
                        logger.info("mapValue......" + mapValue);
                        existingDataMap.put(userAgent, mapValue);
                    } else {
                        totString = "\n\n" + "User-agent: " + userAgent + "\n" + addDir;
                        existingDataMap.put(userAgent, totString);
                    }
                } else {
                    totString = "User-agent: " + userAgent + "\n" + addDir;
                    existingDataMap.put(userAgent, totString);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("existingDataMap......." + existingDataMap.size() + "......." + existingDataMap);
        return existingDataMap;
    }

    public Map<String, String> createUserInputDataMap(JSONArray totRobotsArr) {  //Preparing Map with user entered Data
        Map<String, String> resultMap = new LinkedHashMap<String, String>();
        if (totRobotsArr != null) {
            for (int i = 0; i < totRobotsArr.length(); i++) {
                try {
                    JSONObject robotsObj = totRobotsArr.getJSONObject(i);
                    if (!(robotsObj.getString("userAg").trim().equals("") && robotsObj.getString("totUAStr").trim().equals(""))) {
                        logger.debug("user agent val is  " + i + ": " + robotsObj.getString("totUAStr"));
                        resultMap.put(robotsObj.getString("userAg"), robotsObj.getString("totUAStr"));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return resultMap;
    }

    public Map<String, String> createTotalRobotsResultMapVersion2(Map<String, String> finalResultMap,
            JSONArray totRobotsArr, RobotsTxtGenerator robotsTxtGenerator, List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings) {  // Creating finalmap by merging both user entered data Map and Existing data Map
        String totString, userAgentVal, directoryVal, totDirectoryOrFilePath, userInputTotUAVal, existingTotUAVal;
        if (finalResultMap != null && finalResultMap.size() > 0) {
            if (totRobotsArr != null) {
                for (int i = 0; i < totRobotsArr.length(); i++) {
                    try {
                        JSONObject robotsObj = totRobotsArr.getJSONObject(i);
                        totString = "";
                        userAgentVal = robotsObj.getString("userAg").trim();
                        logger.info("userAgentVal......." + userAgentVal);
                        userInputTotUAVal = robotsObj.getString("totUAStr").trim();
                        logger.info("userInputTotUAVal......." + userInputTotUAVal);
                        if (finalResultMap.containsKey(userAgentVal)) {
                            existingTotUAVal = finalResultMap.get(userAgentVal);
                            logger.info("existingTotUAVal......." + existingTotUAVal);
                            String[] existingDataArr = existingTotUAVal.split("\n");
                            String[] userInputArr = userInputTotUAVal.split("\n");
                            for (String userInputStr : userInputArr) {
                                totString = "";
                                if (!userInputStr.startsWith("User-agent")) {
                                    for (String existingDataStr : existingDataArr) {
                                        if (!existingDataStr.startsWith("User-agent")) {
                                            if (existingDataStr.equals(userInputStr)) {
                                                totString = "";
                                                break;
                                            } else {
                                                totString = "\n" + userInputStr;
                                            }
                                        }
                                    }
                                    existingTotUAVal = existingTotUAVal + totString;
                                    finalResultMap.put(userAgentVal, existingTotUAVal);
                                }
                            }
                        } else {
                            totString = "\n\n" + userInputTotUAVal;
                            finalResultMap.put(userAgentVal, totString);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } else {
            finalResultMap = createUserInputDataMap(totRobotsArr);
        }
        logger.info("........" + finalResultMap);
        return finalResultMap;
    }

    public Map<String, String> createTotalRobotsResultMap(Map<String, String> finalResultMap,
            List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings) {
        String userAgentName = "User-agent: ";
        String totString, userAgentVal, directoryVal, totDirectoryOrFilePath;
        if (robotsTxtDirectiveSettings != null && robotsTxtDirectiveSettings.size() > 0) {
            for (RobotsTxtDirectiveSettings robotSettingObj : robotsTxtDirectiveSettings) {
                totString = "";
                userAgentVal = robotSettingObj.getUserAgentName();
                directoryVal = robotSettingObj.getDirectoryOrFilePath();
                totDirectoryOrFilePath = "";
                if (robotSettingObj.getAddDirectiveType() == 1) {
                    totDirectoryOrFilePath = "Allow: " + directoryVal;
                } else if (robotSettingObj.getAddDirectiveType() == 2) {
                    totDirectoryOrFilePath = "Disallow: " + directoryVal;
                }
                if (robotSettingObj.getUserAgentName().trim().equals("Specify")) {
                    userAgentVal = robotSettingObj.getSpecialUserAgentName();
                }
                if (finalResultMap.containsKey(userAgentVal)) {
                    String mapValue = finalResultMap.get(userAgentVal);
                    String[] mapValArr = mapValue.split("\n");
                    for (String mapArrObj : mapValArr) {
                        if (mapArrObj.equals(totDirectoryOrFilePath)) {
                            totString = "";
                        } else {
                            //@TO DO ONLY DIRECTORY OR FILE PATH CHECKING
                            totString = "\n" + totDirectoryOrFilePath;
                        }
                    }
                    mapValue = mapValue + totString;
                    finalResultMap.put(userAgentVal, mapValue);

                } else {
                    totString = userAgentName + userAgentVal + "\n" + totDirectoryOrFilePath + "\n\n";
                    finalResultMap.put(userAgentVal, totString);
                }
            }
        }
        logger.info("........" + finalResultMap);
        return finalResultMap;

    }

    public String generateRobotsTxtContent(Map<String, String> finalResultMap, RobotsTxtGenerator robotsTxtGenerator) {
        String finalRobotsTxt = "";
        String finalSiteMaps = "";
        if (finalResultMap != null && finalResultMap.size() > 0) {
            for (Map.Entry<String, String> entry : finalResultMap.entrySet()) {
                finalRobotsTxt = finalRobotsTxt + entry.getValue();
            }
        }
        if (finalRobotsTxt.contains("User-agent: All")) {
            finalRobotsTxt = finalRobotsTxt.replace("User-agent: All", "User-agent: *");
        }
        if (robotsTxtGenerator.getSitemapUrl() != null && !robotsTxtGenerator.getSitemapUrl().trim().equals("")) {
            String[] siteMaps = robotsTxtGenerator.getSitemapUrl().split(",");
            for (String sitemap : siteMaps) {
                finalSiteMaps = finalSiteMaps + "\n" + "Sitemap: " + sitemap.trim();
            }
        }
        finalRobotsTxt = finalRobotsTxt + finalSiteMaps;
        return finalRobotsTxt;
    }

    public String createRobotsTxtFile(String finalRobotsTxt, String filePath) {
        try {

            File robotFile = new File(filePath + "/" + "robots.txt");
            if (robotFile.exists()) {
                robotFile.delete();
            }
            robotFile.createNewFile();
            FileWriter fw = new FileWriter(robotFile.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(finalRobotsTxt);
            bw.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "robots.txt";
    }

    public String createTemporaryFolder(String downloadfolder, long userID, long startTime) {
        /*public String createTemporaryFolder(String downloadfolder,long userID,Timestamp startTime){*/

        String uID = "" + userID;
        String startTimeVal = "" + startTime;
        File f = new File(downloadfolder + uID + "_" + startTimeVal);
        logger.info("The robot txt generator file is created at--> " + f + " for user id" + uID);
        if (f.isDirectory()) {
            deleteDirectory(f);
        }
        f.mkdir();
        return downloadfolder + uID + "_" + startTimeVal;
    }

    public boolean deleteDirectory(File f) {
        if (f.isDirectory()) {
            String[] children = f.list();
            for (int i = 0; i < children.length; i++) {
                File childPath = new File(f, children[i]);
                childPath.delete();
            }
        }
        return f.delete();
    }

    public void addInJson(String statsData, HttpServletResponse response) {
        net.sf.json.JSONArray arr = null;
        arr = new net.sf.json.JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, statsData);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public void addInJson2(String sitemapData, RobotsTxtDirectiveSettings[] robotsArray,
            String existingRobotsTxtContent, HttpServletResponse response, RobotsTxtGenerator robotsTxtGenerator) {
        String errorMsg = "";
        net.sf.json.JSONArray arr = null;
        arr = new net.sf.json.JSONArray();
        PrintWriter writer = null;
        if (existingRobotsTxtContent != null && existingRobotsTxtContent.trim().contains("This URL cannot be reviewed")) {
               errorMsg = existingRobotsTxtContent;
               existingRobotsTxtContent = null;
//               errorMsg = "Your URL is not working, please check the URL.";
            sitemapData = "";
        } 
        /*else if (existingRobotsTxtContent == null) {
            errorMsg = "Unable to fetch robots.txt from the Specified URL. Please create new one";
            robotsTxtGenerator.setSitemapUrl("");
            sitemapData = "";
        }*/
        try {
            writer = response.getWriter();
            arr.add(0, sitemapData);
            arr.add(1, robotsArray);
            arr.add(2, existingRobotsTxtContent);
            arr.add(3, errorMsg);
            if (robotsTxtGenerator != null) {
                arr.add(4, robotsTxtGenerator.getRobotsTxtUrl());
            }
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public String validateURL(String url) {

        if (!url.startsWith("http")) {
            url = "http://" + url;
            int responseCode = Common.getURLStatus(url);
            if (responseCode > 400) {
                url = "https://" + url;
                responseCode = Common.getURLStatus(url);
            }
        }
        return url;
    }

    /*Construct Robots Map from Robots.Txt Content to display for exited result.*/
    public List<RobotsTxtDirectiveSettings> constructRobotsTxtList(String robotsTxtContent, HashMap<String, String> robotNames) {
        List<RobotsTxtDirectiveSettings> robotsTxtDirectiveSettings = new ArrayList<>();
        try {
            String inputLine = "";
            String userAgent = "";
            String specialUserAgent = "";
            String directiveName = "";
            int count = 1;
            int indexVal = 0;
            String oldSiteMapName = "";
            String siteMapName = "";
            String robotsTxtArray[] = robotsTxtContent.split("\n");
            if (robotsTxtArray != null) {
                for (String directive : robotsTxtArray) {
                    inputLine = directive.replace("\n", "").replace("\r", "");
                    robotsTxtContent = robotsTxtContent.concat("\n" + inputLine);
                    if (!inputLine.startsWith("#")) {
                        boolean isUserAgent = checkIsUAPresent(inputLine, "User-agent");
                        if (isUserAgent) {
                            userAgent = "";
                            userAgent = getPropertyValue(inputLine).trim();
                            logger.info("userAgent........" + userAgent);
                            if (userAgent.equals("*")) {
                                userAgent = "All";
                            } else {
                                Set<String> robotkeyNames = robotNames.keySet();
                                Set<String> tempSet = new HashSet<String>();
                                if (robotkeyNames != null && robotkeyNames.size() > 0) {
                                    for (String robotUA : robotkeyNames) {
                                        tempSet.add(robotUA.toLowerCase());
                                    }
                                }
                                if (!tempSet.contains(userAgent.toLowerCase())) {
                                    specialUserAgent = userAgent;
                                    userAgent = "Specify";
                                }
                            }
                        } else {
                            int directiveType = 1;
                            boolean isDirective = checkIsTextPresent(inputLine, "allow");
                            if (isDirective) {
                                directiveType = 1;
                            } else {
                                isDirective = checkIsTextPresent(inputLine, "disallow");
                                if (isDirective) {
                                    directiveType = 2;
                                }
                            }
                            if (isDirective) {
                                RobotsTxtDirectiveSettings robotsTxtSettings = new RobotsTxtDirectiveSettings();
                                robotsTxtSettings.setId(indexVal);
                                robotsTxtSettings.setAddDirectiveType(directiveType);
                                robotsTxtSettings.setUserAgentName(userAgent);
                                directiveName = getPropertyValue(inputLine).trim();
                                robotsTxtSettings.setDirectoryOrFilePath(directiveName);
                                indexVal = indexVal + 1;
                                if (!specialUserAgent.equals("")) {
                                    robotsTxtSettings.setSpecialUserAgentName(specialUserAgent);
                                }
                                robotsTxtDirectiveSettings.add(robotsTxtSettings);
                            }
                            boolean isSiteMap = checkIsTextPresent(inputLine, "sitemap");
                            if (isSiteMap) {
                                if (count > 1) {
                                    siteMapName = getPropertyValue(inputLine).trim();
                                    oldSiteMapName = oldSiteMapName + "\n" + siteMapName;
                                } else {
                                    oldSiteMapName = getPropertyValue(inputLine).trim();
                                }
                                count++;
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Exception in constructRobotsTxtList" + e.getMessage());
        }

        return robotsTxtDirectiveSettings;
    }
}
