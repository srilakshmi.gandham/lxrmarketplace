/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.time.LocalDateTime;
import java.util.Map;

/**
 *
 * @author vidyasagar.korada
 */
/*To store pre sales tool usage and API count*/
public class PreSalesToolUsage {

    private long id;
    private int toolId;
    private String userEmail;
    private LocalDateTime createdDate;
    private int numberOfRequests;
    /*API Name, ResponseCount*/
    private Map<String,Integer> apiList;
  
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getToolId() {
        return toolId;
    }

    public void setToolId(int toolId) {
        this.toolId = toolId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public int getNumberOfRequests() {
        return numberOfRequests;
    }

    public void setNumberOfRequests(int numberOfRequests) {
        this.numberOfRequests = numberOfRequests;
    }

    public Map<String, Integer> getApiList() {
        return apiList;
    }

    public void setApiList(Map<String, Integer> apiList) {
        this.apiList = apiList;
    }
    
}
