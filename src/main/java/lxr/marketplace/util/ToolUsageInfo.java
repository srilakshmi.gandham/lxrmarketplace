package lxr.marketplace.util;

import java.io.Serializable;

public class ToolUsageInfo implements Serializable{
	private long toolId;
	private String toolName;
	private long usageCount;
	
	public long getToolId() {
		return toolId;
	}
	public void setToolId(long toolId) {
		this.toolId = toolId;
	}
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}
	public long getUsageCount() {
		return usageCount;
	}
	public void setUsageCount(long usageCount) {
		this.usageCount = usageCount;
	}
	
}
