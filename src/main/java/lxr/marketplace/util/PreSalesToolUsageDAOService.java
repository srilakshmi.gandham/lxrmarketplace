/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vidyasagar.korada
 */
@Repository
public class PreSalesToolUsageDAOService {

    private static final Logger LOGGER = Logger.getLogger(PreSalesToolUsageDAOService.class);

    public static final String SIMILARWEB = "SimilarWeb";
    public static final String SPYFU = "SpyFu";
    public static final String GOOGLECUSTOMSEARCH = "Google Custom Search";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean insertPreSalesToolUsage(PreSalesToolUsage preSalesToolUsage) {
        boolean isDataUpdated = Boolean.FALSE;
        String query = "INSERT INTO presales_tools_usage (tool_id, user_email, created_date, number_of_requests) VALUES( ?, ?, ?, ?)";
        /*Primary key of presales tools usage*/
        long preSalesToolUsageId = 0;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update((Connection connection) -> {
                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, preSalesToolUsage.getToolId());
                ps.setString(2, preSalesToolUsage.getUserEmail());
                ps.setTimestamp(3, Timestamp.valueOf(preSalesToolUsage.getCreatedDate()));
                ps.setInt(4, preSalesToolUsage.getNumberOfRequests());
                return ps;
            }, keyHolder);
        } catch (DataAccessException e) {
            LOGGER.error("DataAccessException in insertpreSalesToolUsage cause:: ", e);
        }
        /*Presales tool usage record status*/
        preSalesToolUsageId = (Long) keyHolder.getKey();
        isDataUpdated = preSalesToolUsageId > 0;
        if (isDataUpdated) {
            LOGGER.info("Data is inserted in insert PreSales Tool Usage, id:: " + preSalesToolUsage + ", toolId: " + preSalesToolUsage.getToolId() + ", ");
            /*Presales tools api usage record status*/
            isDataUpdated = insertPreSalesToolAPIUsage(preSalesToolUsageId, preSalesToolUsage.getApiList());
        }
        return isDataUpdated;
    }

    public boolean insertPreSalesToolAPIUsage(long preSalesToolUsageId, Map<String, Integer> apiList) {
        String query = "INSERT INTO presales_tools_api_usage (id, api_name, response_count) VALUES (?,?,?)";
        int[] updatedList = null;
        try {
            Object apiName[] = apiList.keySet().toArray();
            updatedList = this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement statement, int i) throws SQLException {
                    String name = (String) apiName[i];
                    LOGGER.info("Data to be updated in insert preSalesTool API Usage for apiName:: " + name + ", responseCount:: " + apiList.get(name));
                    statement.setLong(1, preSalesToolUsageId);
                    statement.setString(2, name);
                    statement.setInt(3, apiList.get(name));
                }

                @Override
                public int getBatchSize() {
                    return apiList.size();
                }
            });
        } catch (Exception e) {
            LOGGER.error("Exception in insertPreSalesToolAPIUsage cause::  ", e);
        }
        return (updatedList != null ? updatedList.length == apiList.size() : Boolean.FALSE);
    }

}