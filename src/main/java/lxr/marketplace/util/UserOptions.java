package lxr.marketplace.util;

public class UserOptions {
	private long uopId;
	private long userId;
	private long toolId;
	private String userDomain;
	private boolean schedulerEnabled;
	private int scheduledDay;

	public long getUopId() {
		return uopId;
	}
	public void setUopId(long uopId) {
		this.uopId = uopId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getToolId() {
		return toolId;
	}
	public void setToolId(long toolId) {
		this.toolId = toolId;
	}
	public String getUserDomain() {
		return userDomain;
	}
	public void setUserDomain(String userDomain) {
		this.userDomain = userDomain;
	}

	public void setSchedulerEnabled(boolean schedulerEnabled) {
		this.schedulerEnabled = schedulerEnabled;
	}
	public boolean isSchedulerEnabled() {
		return schedulerEnabled;
	}
	public void setScheduledDay(int scheduledDay) {
		this.scheduledDay = scheduledDay;
	}
	public int getScheduledDay() {
		return scheduledDay;
	}
	
	
}
