package lxr.marketplace.util;

import java.sql.ResultSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public final class UserOptionService {
	private static Logger logger = Logger.getLogger(UserOptionService.class);
 
    public static List<UserOptions> getListForDay(long toolId, int scheduledDay, JdbcTemplate jdbcTemplate) {
        String query = "select * from user_options where tool_id = " + toolId + " and scheduled_day = " +  scheduledDay + " and scheduler_isenabled = true";
        try {
        	List<UserOptions> userOptionsS = jdbcTemplate.query(query, (ResultSet rs, int rowNum) -> {
                    UserOptions userOptions = new UserOptions();
                    
                    userOptions.setUopId(rs.getLong("id"));
                    userOptions.setUserId(rs.getLong("user_id"));
                    userOptions.setToolId(rs.getLong("tool_id"));
                    userOptions.setUserDomain(rs.getString("user_domain"));
                    userOptions.setSchedulerEnabled(rs.getBoolean("scheduler_isenabled"));
                    userOptions.setScheduledDay(rs.getInt("scheduled_day"));
                    return userOptions;
                });
            return userOptionsS;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }
    
    public static UserOptions find(long toolId, long userId, JdbcTemplate jdbcTemplate){
    	String query = "select * from user_options where tool_id = "+toolId+" and user_id = "+userId;
    
    	try{
    		
    		UserOptions userOptions = (UserOptions) jdbcTemplate.queryForObject(query, (ResultSet rs, int rowNum) -> {
                    UserOptions userOptions1 = new UserOptions();
                        userOptions1.setUopId(rs.getLong("id"));
                        userOptions1.setUserId(rs.getLong("user_id"));
                        userOptions1.setToolId(rs.getLong("tool_id"));
                        userOptions1.setUserDomain(rs.getString("user_domain"));
                        userOptions1.setSchedulerEnabled(rs.getBoolean("scheduler_isenabled"));
                        userOptions1.setScheduledDay(rs.getInt("scheduled_day"));
                        return userOptions1;
                    });
    		return userOptions;
    	}catch (Exception e) {
            logger.error(e);
        }
        return null;
    }
    public static int save(final UserOptions userOptions, JdbcTemplate jdbcTemplate) {
    	final String query = "INSERT INTO user_options ( user_id, tool_id, scheduler_isenabled, scheduled_day, user_domain) VALUES (?, ?, ?, ?, ?)";
        Object[] valLis = new Object[5];

        valLis[0] = userOptions.getUserId();
        valLis[1] = userOptions.getToolId();
        valLis[2] = userOptions.isSchedulerEnabled();
        valLis[3] = userOptions.getScheduledDay();
        valLis[4] = userOptions.getUserDomain();
        
        try {
            return jdbcTemplate.update(query, valLis);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return 0;
    } 
    
    public static int update(final UserOptions userOptions, JdbcTemplate jdbcTemplate) {
    	
    	final String query = "update user_options set user_id =?, tool_id =?, scheduler_isenabled =?, scheduled_day =?, user_domain =? where id = ?";
        Object[] valLis = new Object[6];

        valLis[0] = userOptions.getUserId();
        valLis[1] = userOptions.getToolId();
        valLis[2] = userOptions.isSchedulerEnabled();
        valLis[3] = userOptions.getScheduledDay();
        valLis[4] = userOptions.getUserDomain();
        valLis[5] = userOptions.getUopId();
        
        try {
            return jdbcTemplate.update(query, valLis);
        } catch (Exception ex) {
           logger.error(ex);
        }
        return 0;
    }
}
