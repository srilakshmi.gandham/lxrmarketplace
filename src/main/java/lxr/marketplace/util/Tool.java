package lxr.marketplace.util;

import java.io.Serializable;
import java.util.Comparator;

public class Tool implements Serializable{

    private int toolId;
    private String toolName;
    private String toolDesc;
    private String imgPath;
    private String tagline;
    private long categoryid;
    private String toolLink;
    private double amount;
    private String buttonName;
    private String toolIssue;
    private String toolIconColor;
    private boolean isAndroidApp;
    private String androidAppURL;
    private boolean isIOSApp;
    private String iosAppURL;
    private boolean showInHome;
    private ToolUserInfo toolUserInfo;

    public int getToolId() {
        return toolId;
    }

    public void setToolId(int toolId) {
        this.toolId = toolId;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public String getToolDesc() {
        return toolDesc;
    }

    public void setToolDesc(String toolDesc) {
        this.toolDesc = toolDesc;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public long getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(long categoryid) {
        this.categoryid = categoryid;
    }

    public String getToolLink() {
        return toolLink;
    }

    public void setToolLink(String toolLink) {
        this.toolLink = toolLink;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getToolIssue() {
        return toolIssue;
    }

    public void setToolIssue(String toolIssue) {
        this.toolIssue = toolIssue;
    }

    public String getToolIconColor() {
        return toolIconColor;
    }

    public void setToolIconColor(String toolIconColor) {
        this.toolIconColor = toolIconColor;
    }

    public boolean isIsAndroidApp() {
        return isAndroidApp;
    }

    public void setIsAndroidApp(boolean isAndroidApp) {
        this.isAndroidApp = isAndroidApp;
    }

    public String getAndroidAppURL() {
        return androidAppURL;
    }

    public void setAndroidAppURL(String androidAppURL) {
        this.androidAppURL = androidAppURL;
    }

    public boolean isIsIOSApp() {
        return isIOSApp;
    }

    public void setIsIOSApp(boolean isIOSApp) {
        this.isIOSApp = isIOSApp;
    }

    public String getIosAppURL() {
        return iosAppURL;
    }

    public void setIosAppURL(String iosAppURL) {
        this.iosAppURL = iosAppURL;
    }

    public boolean isShowInHome() {
        return showInHome;
    }

    public void setShowInHome(boolean showInHome) {
        this.showInHome = showInHome;
    }

    public ToolUserInfo getToolUserInfo() {
        return toolUserInfo;
    }

    public void setToolUserInfo(ToolUserInfo toolUserInfo) {
        this.toolUserInfo = toolUserInfo;
    }
    
    public static Comparator<Tool> getToolComparator() {
        return toolComparator;
    }

    public static void setToolComparator(Comparator<Tool> toolComparator) {
        Tool.toolComparator = toolComparator;
    }

    public static Comparator<Tool> toolComparator = (Tool o1, Tool o2) -> {
        Integer tool1 = (int) o1.getToolId();
        Integer tool2 = (int) o2.getToolId();
        return tool2.compareTo(tool1);
    };

}
