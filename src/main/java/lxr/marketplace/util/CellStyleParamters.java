/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 *
 * @author vidyasagar.korada
 */
public class CellStyleParamters {
     private Font font;
    private HorizontalAlignment horizontalPosition;
    private VerticalAlignment verticalPostion;
    private boolean haveBorder;
    private short borderColorIndex;
    private String borderType;
    private short foreGroundColorIndex;
    private boolean fillPattern;

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public HorizontalAlignment getHorizontalPosition() {
        return horizontalPosition;
    }

    public void setHorizontalPosition(HorizontalAlignment horizontalPosition) {
        this.horizontalPosition = horizontalPosition;
    }

    public VerticalAlignment getVerticalPostion() {
        return verticalPostion;
    }

    public void setVerticalPostion(VerticalAlignment verticalPostion) {
        this.verticalPostion = verticalPostion;
    }

    public boolean isHaveBorder() {
        return haveBorder;
    }

    public void setHaveBorder(boolean haveBorder) {
        this.haveBorder = haveBorder;
    }

    public short getBorderColorIndex() {
        return borderColorIndex;
    }

    public void setBorderColorIndex(short borderColorIndex) {
        this.borderColorIndex = borderColorIndex;
    }

    public String getBorderType() {
        return borderType;
    }

    public void setBorderType(String borderType) {
        this.borderType = borderType;
    }

    public short getForeGroundColorIndex() {
        return foreGroundColorIndex;
    }

    public void setForeGroundColorIndex(short foreGroundColorIndex) {
        this.foreGroundColorIndex = foreGroundColorIndex;
    }

    public boolean isFillPattern() {
        return fillPattern;
    }

    public void setFillPattern(boolean fillPattern) {
        this.fillPattern = fillPattern;
    }
    
    
}
