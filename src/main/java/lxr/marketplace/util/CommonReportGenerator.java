package lxr.marketplace.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Types;
import javax.imageio.ImageIO;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
public class CommonReportGenerator {

    private static Logger logger = Logger.getLogger(CommonReportGenerator.class);

    private static String logoPath;

    public void setLogoPath(String logoPath) {
        CommonReportGenerator.logoPath = logoPath;
    }

    public static boolean addImage(HSSFWorkbook workBook, HSSFSheet sheet, short col1, int row1, short col2, int row2) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(logoPath);
        } catch (FileNotFoundException e) {
            logger.error(e);
            return false;
        }
        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        int b;

        try {
            while ((b = fis.read()) != -1) {
                imgBytes.write(b);
            }
            fis.close();
        } catch (IOException e) {
            logger.error("Image is not added into report", e);
            return false;
        }
//        sheet.addMergedRegion(new CellRangeAddress(row1, row2 - 1, col1, col2));
        HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        int index = workBook.addPicture(imgBytes.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG);
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        patriarch.createPicture(anchor, index);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);
        anchor.setDx1(25);
        return true;
    }

    /*XSSFWorkbook*/
    public static boolean appendImageToExcellCell(XSSFWorkbook workBook, XSSFSheet sheet, short col1, int row1, short col2, int row2) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(logoPath);
        } catch (FileNotFoundException e) {
            logger.error(e);
            return false;
        }
        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        int b;
        try {
            while ((b = fis.read()) != -1) {
                imgBytes.write(b);
            }
            fis.close();
        } catch (IOException e) {
            logger.error("Image is not added into report", e);
            return false;
        }
//        sheet.addMergedRegion(new CellRangeAddress(row1, row2 - 1, col1, col2 - 1));
        XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        int index = workBook.addPicture(imgBytes.toByteArray(), XSSFWorkbook.PICTURE_TYPE_JPEG);
        XSSFDrawing patriarch = sheet.createDrawingPatriarch();
        patriarch.createPicture(anchor, index);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
        anchor.setDx1(25);
        return true;
    }

    public static void addTitle(HSSFWorkbook workBook, HSSFSheet sheet, String[] headings, int rowInd) {
        HSSFRow curRow;
        HSSFCell curCel;

        //Heading Cell Style   
        HSSFFont font = workBook.createFont();
        font.setBold(true);

        HSSFCellStyle heaCelSty = workBook.createCellStyle();
        heaCelSty.setFont(font);
        heaCelSty.setFillBackgroundColor(HSSFColor.WHITE.index);
        heaCelSty.setFillPattern(FillPatternType.BIG_SPOTS);
        heaCelSty.setFillForegroundColor(HSSFColor.WHITE.index);
        heaCelSty.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        for (int i = 0; i < headings.length; i++) {
            curRow = sheet.createRow(i + rowInd);
            curCel = curRow.createCell(0);
            curCel.setCellValue(headings[i]);
            sheet.addMergedRegion(new CellRangeAddress(i + rowInd, i + rowInd, 0, 3));
            curCel.setCellStyle(heaCelSty);
        }
        curRow = sheet.createRow(rowInd + headings.length);
        for (int i = 0; i < 4; i++) {
            curCel = curRow.createCell(i);
            curCel.setCellStyle(heaCelSty);
        }
    }

    public static void createXLSFromDatabase(HSSFWorkbook workBook, HSSFSheet sheet, SqlRowSet sqlRowSet,
            int colWidth[], String[] headings, boolean addLogo) {

        HSSFRow curRow;
        HSSFCell curCel;

        int colCount;
        int rowInd = 0;
        SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

        HSSFFont font = workBook.createFont();
        font.setBold(true);

        //Double Data Cell Style
        HSSFCellStyle dblCelSty = workBook.createCellStyle();
        dblCelSty.setDataFormat((short) 2);

        //String cell style
        HSSFCellStyle stringCellStyle = workBook.createCellStyle();
        stringCellStyle.setWrapText(true);

        //Column Heading Cell Style
        HSSFCellStyle colHeaCelSty = workBook.createCellStyle();
        colHeaCelSty.setFont(font);
        colHeaCelSty.setWrapText(true);
        colHeaCelSty.setBorderTop(BorderStyle.MEDIUM);
        colHeaCelSty.setTopBorderColor(HSSFColor.GREY_25_PERCENT.index);

        addTitle(workBook, sheet, headings, rowInd);
        if (addLogo) {
            addImage(workBook, sheet, (short) 4, 0, (short) 5, 4);
            rowInd = rowInd + (headings.length >= 3 ? headings.length : 3) + 1;
        } else {
            rowInd = rowInd + headings.length + 1;
        }

        //Adding Header
        colCount = sqlRowSetMetDat.getColumnCount();
        curRow = sheet.createRow(rowInd++);
        for (int colInd = 1; colInd <= colCount; colInd++) {
            curCel = curRow.createCell(colInd - 1);
            curCel.setCellValue(sqlRowSetMetDat.getColumnLabel(colInd));
            curCel.setCellStyle(colHeaCelSty);
            sheet.setColumnWidth(colInd - 1, (int) (441.3793d + 256d * (colWidth[colInd] - 1d)));
        }

        //for freeze
        sheet.createFreezePane(0, rowInd);

        while (sqlRowSet.next()) {
            curRow = sheet.createRow(rowInd);
            for (int colInd = 1; colInd <= colCount; colInd++) {
                curCel = curRow.createCell(colInd - 1);
                int dataType = sqlRowSetMetDat.getColumnType(colInd);

                switch (dataType) {
                    case Types.DOUBLE:
                        Double dblVal = sqlRowSet.getDouble(sqlRowSetMetDat.getColumnName(colInd));
                        if (dblVal == null) {
                            curCel.setCellValue(" ");
                        } else if (dblVal > 0) {
                            curCel.setCellValue(dblVal);
                            curCel.setCellStyle(dblCelSty);
                        } else if (dblVal == 0) {
                            curCel.setCellValue(" ");
                        }
                        break;
                    case Types.BIGINT:
                        Long longVal = sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd));
                        if (longVal != null) {
                            curCel.setCellValue(longVal);
                        } else {
                            curCel.setCellValue("");
                        }
                        break;
                    default:
                        String value = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd));
                        if (value == null) {
                            value = "";
                        }
                        curCel.setCellValue(value);
                        curCel.setCellStyle(stringCellStyle);
                        break;
                }
            }
            rowInd++;
        }
    }

    /*Insert image in excell sheet from application hosting URL(Image saved in local)*/
    public static void addImageInExcellSheet(HSSFWorkbook workBook, HSSFSheet sheet,
            short col1, int row1, short col2, int row2, String logoPath, HSSFCellStyle adviceStyle) {
        try {
            InputStream inputStream = new FileInputStream(logoPath);
            byte[] imageBytes = IOUtils.toByteArray(inputStream);
            int index = workBook.addPicture(imageBytes, HSSFWorkbook.PICTURE_TYPE_PNG);
            int width = 45;
            int height = 12;
            HSSFClientAnchor anchor;
            double remainderWidth = (width % 69) / 68.0;
            int widthInCells = width / 68;
            double remainderHeight = (height % 18) / 18.0;
            int heightInCells = (height / 18);
            int rowEnd = row1 + heightInCells;
            int colEnd = 0 + widthInCells;
            anchor = new HSSFClientAnchor(0, 15, (int) (remainderWidth * 1023),
                    (int) (remainderHeight * 255), (short) 0,
                    (short) row1, (short) colEnd, (short) rowEnd);
            HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
            patriarch.createPicture(anchor, index);
            sheet.setColumnWidth(0, 4 * 256);
        } catch (IOException e) {
            logger.error("Exception and image is not added into report", e);
        }
    }

    /*Read image from URL and insert image in excell cell.*/
//    (HSSFWorkbook workBook, HSSFSheet sheet,
//            short col1, int row1, short col2, int row2, String logoPath, HSSFCellStyle adviceStyle)
    public static boolean insertImageInExcellCellFromURL(HSSFWorkbook workBook, HSSFSheet sheet, short col1, int row1, String imagePath) {
        boolean isImageInserted = true;
        try {
            int imageType = 0;
            String extension = imagePath.substring(imagePath.lastIndexOf(".") + 1, imagePath.length());
            extension = extension.toLowerCase();
            switch (extension) {
                case "png":
                    imageType = HSSFWorkbook.PICTURE_TYPE_PNG;
                    break;
                case "jpg":
                    imageType = HSSFWorkbook.PICTURE_TYPE_JPEG;
                    break;
                case "jpeg":
                    imageType = HSSFWorkbook.PICTURE_TYPE_JPEG;
                    break;
                case "bmp":
                    imageType = HSSFWorkbook.PICTURE_TYPE_DIB;
                    break;
                case "dib":
                    imageType = HSSFWorkbook.PICTURE_TYPE_DIB;
                    break;
                default:
                    imageType = HSSFWorkbook.PICTURE_TYPE_PNG;
                    break;
            }
            URL imageURL = new URL(imagePath);
            byte[] bytes = null;
            BufferedImage originalImage = ImageIO.read(imageURL);
            if (originalImage != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(originalImage, extension, baos);
                bytes = baos.toByteArray();
            }
            int pictureIdx = 0;
            if (bytes != null) {
                pictureIdx = workBook.addPicture(bytes, imageType);
            }
            CreationHelper helper = workBook.getCreationHelper();
            // Create the drawing patriarch. This is the top level container for all shapes. 
            Drawing drawing  = ((HSSFSheet) sheet).getDrawingPatriarch();;
            //add a picture shape
            ClientAnchor anchor = helper.createClientAnchor();
            //add a picture shape
//            anchor.setAnchorType(3);
            anchor.setDx1(0);
            anchor.setCol1(col1);
            anchor.setRow1(row1);
            // +1 since we want to include the
            anchor.setRow2(row1 + 1);
            anchor.setCol2(col1 + 1);
            anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
            Picture my_picture = drawing.createPicture(anchor, pictureIdx);
            if (my_picture != null) {
                my_picture.resize();
            }
        } catch (IOException e) {
            isImageInserted = false;
            logger.error("ImagePath:  " + imagePath);
            logger.error("IOException in inserting image cause: "+e.getMessage());
        } catch (Exception e) {
            isImageInserted = false;
            logger.error("Exception in inserting image into  hssf cell from URL  and image is not added into report cause: "+e.getMessage());
        }
        return isImageInserted;
    }
}
