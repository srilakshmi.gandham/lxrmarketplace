package lxr.marketplace.util.findPhrase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lxr.marketplace.util.stopwordsutility.StopWordsTrie;
import lxr.marketplace.util.stopwordsutility.StopWordsTrieNode;

public class VariableLengthPhraseFinder {

    private String data;
    private int totalWords;
    private double threshold;
    private double maxCorr = 0;
    private HashMap<String, PhraseStat> phraseStats1;
    private HashMap<String, PhraseStat> phraseStats2;
    private HashMap<String, PhraseStat> phraseStats3;
    private HashMap<String, PhraseStat> phraseStats4;
    private HashMap<Integer, String[]> centenseStructure;
    private final List<String> stopWordsList;
   

    public String getData() {
        return data;
    }

    public int getTotalWords() {
        return totalWords;
    }

    public double getThreshold() {
        return threshold;
    }

    public HashMap<String, PhraseStat> getPhraseStats1() {
        return phraseStats1;
    }

    public HashMap<String, PhraseStat> getPhraseStats2() {
        return phraseStats2;
    }

    public HashMap<String, PhraseStat> getPhraseStats3() {
        return phraseStats3;
    }

    public HashMap<String, PhraseStat> getPhraseStats4() {
        return phraseStats4;
    }

    public VariableLengthPhraseFinder(String data, List<String> stopWordsList) {
        super();
        StopWordsTrieNode headNode = StopWordsTrie.createStopWordList(stopWordsList);
        this.data = new StopWordsTrie().removeStopWords(data, headNode);
        totalWords = 0;
        this.stopWordsList = stopWordsList;
    }

    public List<Map<String, PhraseStat>> findMeaningfulPhrases() {
        if (data == null || data.trim().equals("")) {
            return null;
        }
        countPhrases();
        threshold = calculateThreshold();
        LinkedHashMap<String, PhraseStat> phrases1 = sortAndFilterPhrases(phraseStats1);

        removeTwoWordPhrases();
        LinkedHashMap<String, PhraseStat> phrases2 = sortAndFilterPhrases(phraseStats2);

        removePhrases(3);
        LinkedHashMap<String, PhraseStat> phrases3 = sortAndFilterPhrases(phraseStats3);

        removePhrases(4);
        LinkedHashMap<String, PhraseStat> phrases4 = sortAndFilterPhrases(phraseStats4);

        List<Map<String, PhraseStat>> allPhrases = new ArrayList<>();

        allPhrases.add(phrases1);
        allPhrases.add(phrases2);
        allPhrases.add(phrases3);
        allPhrases.add(phrases4);
        return allPhrases;
    }

    private void removePhrases(int n) {
        if (n > 4) {
            return;
        }
        Iterator<Entry<Integer, String[]>> it = centenseStructure.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, String[]> pairs = (Map.Entry<Integer, String[]>) it.next();
            String words[] = (String[]) pairs.getValue();
            for (int i = n - 1; i < words.length;) {
                String firstPhrase = "";
                String secondPhrase = "";
                boolean nullFound = false;
                for (int j = i; j > i - n && !nullFound; j--) {
                    if (words[j] == null) {
                        i = j + n;
                        nullFound = true;
                    } else if (j == i) {
                        secondPhrase = words[j];
                    } else {
                        firstPhrase = words[j] + " " + firstPhrase;
                    }
                }
                if (!nullFound) {
                    HashMap<String, PhraseStat> firstMap = null;
                    HashMap<String, PhraseStat> finalMap = null;
                    firstPhrase = firstPhrase.trim();
                    int noOfNWordPhrase = totalWords - (n - 1) * centenseStructure.size();
                    if (n == 3) {
                        firstMap = phraseStats2;
                        finalMap = phraseStats3;
                    } else if (n == 4) {
                        firstMap = phraseStats3;
                        finalMap = phraseStats4;
                    }
                    PhraseStat stat = finalMap.get(firstPhrase + " " + secondPhrase);
                    double a = firstMap.get(firstPhrase).count / (double) (noOfNWordPhrase - centenseStructure.size());
                    double b = phraseStats1.get(secondPhrase).count / (double) totalWords;
                    double ab = stat.count / (double) noOfNWordPhrase;
                    double corr = calculateCorrelation(a, b, ab);
                    stat.correlation = corr;
                    if (corr < threshold) {
                        stat.positions.stream().forEach((p) -> {
                            centenseStructure.get(p.centense)[p.word] = null;
                        });
                    } else {
                        if (stat.correlation > maxCorr) {
                            maxCorr = stat.correlation;
                        }
                        stat.passed = true;
                    }
                    i++;
                }
            }
        }
    }

    private void removeTwoWordPhrases() {
        Iterator<Entry<String, PhraseStat>> it = phraseStats2.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, PhraseStat> pairs = (Map.Entry<String, PhraseStat>) it.next();
            PhraseStat stat = (PhraseStat) pairs.getValue();
            String keyword = (String) pairs.getKey();
            if (stat.correlation < threshold) {
                stat.positions.stream().forEach((p) -> {
                    centenseStructure.get(p.centense)[p.word] = null;
                });
            } else if (keyword.length() > 5) {
                if (stat.correlation > maxCorr) {
                    maxCorr = stat.correlation;
                }
                stat.passed = true;
            }
        }
    }

    private void countPhrases() {
        phraseStats1 = new HashMap<>();
        phraseStats2 = new HashMap<>();
        phraseStats3 = new HashMap<>();
        phraseStats4 = new HashMap<>();
        centenseStructure = new HashMap<>();

        String regex = "[\\p{Z}\\s]*([\\.,;:?]+[\\p{Z}\\s]+|\\|[\\p{Z}\\s]*|\\.$)";

        String bracketedData = extractBracketedText(data);
        data = data.replaceAll("(\\[(.+?)\\])|(\\((.+?)\\))", " ") + " " + bracketedData;

        String[] centenses = data.split(regex);
        for (int j = 0; j < centenses.length; j++) {
            String centense = centenses[j].replaceAll("[\\p{Z}\\s]+", " ");
            String[] words = centense.trim().split(" ");
            for (int i = 0; i < words.length; i++) {
                boolean startsWithCap = false;
                if (words[i] != null && !words[i].equals("") && words[i].charAt(0) >= 65 && words[i].charAt(0) <= 90) {
                    startsWithCap = true;
                }
                words[i] = words[i].trim().toLowerCase();
                switch (i) {
                    default:
                        String phrase4 = words[i - 3] + " " + words[i - 2] + " " + words[i - 1] + " " + words[i];
                        if (phraseStats4.containsKey(phrase4)) {
                            PhraseStat ps = phraseStats4.get(phrase4);
                            ps.count++;
                            ps.positions.add(new Position(j, i - 3));
                        } else if (phraseStats1.get(words[i - 3]).startsWithCapital) {
                            phraseStats4.put(phrase4, new PhraseStat(j, i - 3, true));
                        } else {
                            phraseStats4.put(phrase4, new PhraseStat(j, i - 3, false));
                        }
                    case 2:
                        String phrase3 = words[i - 2] + " " + words[i - 1] + " " + words[i];
                        if (phraseStats3.containsKey(phrase3)) {
                            PhraseStat ps = phraseStats3.get(phrase3);
                            ps.count++;
                            ps.positions.add(new Position(j, i - 2));
                        } else if (phraseStats1.get(words[i - 2]).startsWithCapital) {
                            phraseStats3.put(phrase3, new PhraseStat(j, i - 2, true));
                        } else {
                            phraseStats3.put(phrase3, new PhraseStat(j, i - 2, false));
                        }
                    case 1:
                        String phrase2 = words[i - 1] + " " + words[i];
                        if (phraseStats2.containsKey(phrase2)) {
                            PhraseStat ps = phraseStats2.get(phrase2);
                            ps.count++;
                            ps.positions.add(new Position(j, i - 1));
                        } else if (phraseStats1.get(words[i - 1]).startsWithCapital) {
                            phraseStats2.put(phrase2, new PhraseStat(j, i - 1, true));
                        } else {
                            phraseStats2.put(phrase2, new PhraseStat(j, i - 1, false));
                        }
                    case 0:
                        if (phraseStats1.containsKey(words[i])) {
                            PhraseStat ps = phraseStats1.get(words[i]);
                            ps.count++;
                            ps.positions.add(new Position(j, i));
                            if (words[i].length() >= 4) {	//passing one word phrases with count minimum 2 and length minimum 5
                                ps.passed = true;
                            }
                        } else {
                            phraseStats1.put(words[i], new PhraseStat(j, i, startsWithCap));
                        }
                }
            }
            totalWords += words.length;
            centenseStructure.put(j, words);
        }
    }

    private double calculateCorrelation(double a, double b, double ab) {
        return ab - a * b;
    }


    private double calculateThreshold() {
        Iterator<Entry<String, PhraseStat>> it = phraseStats2.entrySet().iterator();
        int noOfTwoWordPhrases = totalWords - centenseStructure.size();
        double totalCorrelation = 0;
        while (it.hasNext()) {
            Map.Entry<String, PhraseStat> pairs = (Map.Entry<String, PhraseStat>) it.next();
            PhraseStat stat = (PhraseStat) pairs.getValue();
            String phrase = (String) pairs.getKey();
            String words[] = phrase.split(" ");
            if (words.length == 2) {
                double a = phraseStats1.get(words[0]).count / (double) totalWords;
                double b = phraseStats1.get(words[1]).count / (double) totalWords;
                double ab = stat.count / (double) noOfTwoWordPhrases;
                double corr = calculateCorrelation(a, b, ab);
                stat.correlation = corr;
                totalCorrelation += corr;
            }
        }
        return totalCorrelation / phraseStats2.size();
    }

    private LinkedHashMap<String, PhraseStat> sortAndFilterPhrases(HashMap<String, PhraseStat> map) {
        List<Entry<String, PhraseStat>> list = new LinkedList<>(map.entrySet());
            Collections.sort(list, (Map.Entry<String, PhraseStat> o1, Map.Entry<String, PhraseStat> o2) -> {
                PhraseStat stat1 = (PhraseStat) (o1.getValue());
                PhraseStat stat2 = (PhraseStat) (o2.getValue());
                if (stat1.correlation > stat2.correlation) {	//sort by correlation
                    return -1;
                } else if (stat1.correlation < stat2.correlation) {
                    return 1;
                } else //sort by count of phrase
                {
                    if (stat1.count > stat2.count) {
                        return -1;
                    } else if (stat1.count < stat2.count) {
                        return 1;
                    } else // take phrase started with capital letter to up
                    {
                        if (stat1.startsWithCapital && !stat2.startsWithCapital) {
                            return -1;
                        } else if (stat2.startsWithCapital && !stat1.startsWithCapital) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }
            });
        /*Changing the state of keyword is passed, If size of list is equal to one.*/
         PhraseStat singleStat = null;   
            if(list.size() == 1){
                singleStat = (PhraseStat) list.get(0).getValue();
                if(!singleStat.passed){
                    singleStat.passed = true;
                }
            }
              
        LinkedHashMap<String, PhraseStat> result = new LinkedHashMap<>();
        Pattern regEx1 = Pattern.compile("(\\s+[^a-zA-Z0-9]+$)|^[^a-zA-Z]+$");
        Pattern regEx2 = Pattern.compile("[^a-zA-Z0-9]+");
        list.stream().forEach((pairs) -> {
            PhraseStat stat = (PhraseStat) pairs.getValue();
            String phrase = (String) pairs.getKey();
            if (stat.passed) {
                Matcher matcher1 = regEx1.matcher(phrase);
                Matcher matcher2 = regEx2.matcher(phrase);
                if (matcher1.find() || (matcher2.find() && matcher2.start() == 0)) {
                    //removing phrases started or ended with special characters
                } else {
                    stat.rating = (int) ((100 * stat.correlation) / maxCorr) / (float) 10;
                    result.put((String) pairs.getKey(), stat);
                }
            }
        });
        return result;
    }

    private String extractBracketedText(String data) {
        StringBuilder sb = new StringBuilder();
        Pattern regEx = Pattern.compile("(\\[(.+?)\\])|(\\((.+?)\\))");
        Matcher matcher = regEx.matcher(data);
        while (matcher.find()) {
            String phrase = matcher.group();
            if (phrase != null) {
                sb.append(phrase.substring(1, phrase.length() - 1)).append(". ");
            }
        }
        return sb.toString();
    }
    
    public List<Map<String, PhraseStat>> findMeaningfulPhrasesAndDensity(String content) {
        if (data == null || data.trim().equals("")) {
            return null;
        }
        countPhrases();
        threshold = calculateThreshold();
        
        
        long totWordsCount = getTotalWordCount(content);
        
        LinkedHashMap<String, PhraseStat> phrases1 = sortAndFilterPhrasesAndDensity(phraseStats1, totWordsCount);

        removeTwoWordPhrases();
        LinkedHashMap<String, PhraseStat> phrases2 = sortAndFilterPhrasesAndDensity(phraseStats2, totWordsCount);

        removePhrases(3);
        LinkedHashMap<String, PhraseStat> phrases3 = sortAndFilterPhrasesAndDensity(phraseStats3, totWordsCount);

        removePhrases(4);
        LinkedHashMap<String, PhraseStat> phrases4 = sortAndFilterPhrasesAndDensity(phraseStats4, totWordsCount);

        List<Map<String, PhraseStat>> allPhrases = new ArrayList<>();

        allPhrases.add(phrases1);
        allPhrases.add(phrases2);
        allPhrases.add(phrases3);
        allPhrases.add(phrases4);
        return allPhrases;
    }
    
    private LinkedHashMap<String, PhraseStat> sortAndFilterPhrasesAndDensity(HashMap<String, PhraseStat> map, long totWordCount) {
        
        List<Entry<String, PhraseStat>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, (Map.Entry<String, PhraseStat> o1, Map.Entry<String, PhraseStat> o2) -> {
            PhraseStat stat1 = (PhraseStat) (o1.getValue());
            PhraseStat stat2 = (PhraseStat) (o2.getValue());
            if (stat1.correlation > stat2.correlation) {	//sort by correlation
                return -1;
            } else if (stat1.correlation < stat2.correlation) {
                return 1;
            } else //sort by count of phrase
            {
                if (stat1.count > stat2.count) {
                    return -1;
                } else if (stat1.count < stat2.count) {
                    return 1;
                } else // take phrase started with capital letter to up
                {
                    if (stat1.startsWithCapital && !stat2.startsWithCapital) {
                        return -1;
                    } else if (stat2.startsWithCapital && !stat1.startsWithCapital) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        });

        LinkedHashMap<String, PhraseStat> result = new LinkedHashMap<>();
        Pattern regEx1 = Pattern.compile("(\\s+[^a-zA-Z0-9]+$)|^[^a-zA-Z]+$");
        Pattern regEx2 = Pattern.compile("[^a-zA-Z0-9]+");
        list.stream().forEach((pairs) -> {
            PhraseStat stat = (PhraseStat) pairs.getValue();
            String phrase = (String) pairs.getKey();
            if (stat.passed) {
                Matcher matcher1 = regEx1.matcher(phrase);
                Matcher matcher2 = regEx2.matcher(phrase);
                if (matcher1.find() || (matcher2.find() && matcher2.start() == 0)) {
                    //removing phrases started or ended with special characters
                } else {
                    stat.density = (double) stat.count / totWordCount * 100;
                    result.put((String) pairs.getKey(), stat);
                }
            }
        });
        return result;
    }
    public long getTotalWordCount(String content) {
        int index = 0;
        long numWords = 0;
        char c1 = 0;
        try {
            if (content != null) {
                boolean prevwhitespace = true;
                while (index < content.length()) {
                    char c = content.charAt(index++);
                    if ((index + 0) < content.length()) {
                        c1 = content.charAt(index + 0);
                    }
                    boolean currwhitespace = Character.isWhitespace(c);
                    if (c == ',' && !(c1 == ' ')) {
                        numWords++;
                    }
                    if (prevwhitespace && !currwhitespace) {
                        numWords++;
                    }
                    prevwhitespace = currwhitespace;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return numWords;
    }
}
