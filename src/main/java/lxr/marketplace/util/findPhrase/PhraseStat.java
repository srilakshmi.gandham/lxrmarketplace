package lxr.marketplace.util.findPhrase;

import java.util.ArrayList;

public class PhraseStat {
	public double correlation;
	public int count;
	public ArrayList<Position> positions;
	boolean passed;
	public float rating;
	boolean startsWithCapital; 
         public double density;

    public double getCorrelation() {
        return correlation;
    }

    public void setCorrelation(double correlation) {
        this.correlation = correlation;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Position> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<Position> positions) {
        this.positions = positions;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isStartsWithCapital() {
        return startsWithCapital;
    }

    public void setStartsWithCapital(boolean startsWithCapital) {
        this.startsWithCapital = startsWithCapital;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }
	
	public PhraseStat(int centense, int wordPosition, boolean startsWithCapital){
		correlation = 0;
		count = 1;
		positions = new ArrayList<>();
		positions.add(new Position(centense, wordPosition));
		passed = false;
		this.startsWithCapital = startsWithCapital;
	}
}
