package lxr.marketplace.util.relevancycalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lxr.marketplace.util.stopwordsutility.StopWordsTrie;
import lxr.marketplace.util.stopwordsutility.StopWordsTrieNode;

public class RelevancyChecker {
	private String data;
	private String title;
	private HashMap <Integer, ArrayList<String>> searchPhrases;
	private StopWordsTrieNode headNode;
		
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public RelevancyChecker(String data, String title, List<String> stopWordsList) {
		super();
		this.headNode = StopWordsTrie.createStopWordList(stopWordsList);
		this.data = data;
		this.title = title;
	}
	
//	private int findRelevancy_v1(){
//				
//		float [] percentages = new float[searchPhrases.size()];
//		Iterator<Entry<Integer, ArrayList<String>>> it = searchPhrases.entrySet().iterator(); 
//		data = data.toLowerCase();
//		while (it.hasNext()) {
//	        Map.Entry<Integer, ArrayList<String>> pairs = (Map.Entry<Integer, ArrayList<String>>)it.next();
//	        ArrayList<String> phrases = (ArrayList<String>)pairs.getValue();
//	        float fraction = 1/(float)phrases.size();
//	        float percentage = 0;
//	        for(String phrase : phrases){
////	        	System.out.println(phrase.trim());
//	        	if(data.contains(phrase.trim().toLowerCase())){
//	        		percentage = percentage + fraction;
//	        	}
//	        }
//	        percentages[(Integer)pairs.getKey() - 1] = percentage * 100;
//	        System.out.println("Percentage Relevancy for " + pairs.getKey() + " words = " + percentage * 100 + "\n");
//	    }
//		return 0;
//	}

	private float findRelevancy_v2(){		
		ArrayList<String> oneWordList = searchPhrases.get(1);
		if(oneWordList == null || oneWordList.size() == 0){
			return 0;
		}
		int [] weight = new int[oneWordList.size()];
		int denominator = oneWordList.size();
		Iterator<Entry<Integer, ArrayList<String>>> it = searchPhrases.entrySet().iterator(); 
		data = data.toLowerCase();
		while (it.hasNext()) {
	        Map.Entry<Integer, ArrayList<String>> pairs = (Map.Entry<Integer, ArrayList<String>>)it.next();
	        ArrayList<String> phrases = (ArrayList<String>)pairs.getValue();
	        if(phrases != null){
		        for(String phrase : phrases){
		        	if(data.contains(phrase.trim().toLowerCase())){
		        		int phraseLength = (Integer)pairs.getKey();
		        		if( phraseLength > 1){
		        			denominator += phraseLength;
		        			String wordsInPhrase[] = phrase.split("(\\s)+");
		        			for(String word : wordsInPhrase){
		        				weight[oneWordList.indexOf(word)]++;
		        			}
		        		}else{
		        			weight[oneWordList.indexOf(phrase)] = 1;
		        		}
		        	}
		        }
	        }
	    }
		int numarator = 0;
		for(int i = 0; i < weight.length; i++){
			numarator += weight[i];
		}
		if(denominator == 0){
			return 0;
		}
		return (numarator *100) / (float)denominator;
	}
	
	public float findRelevency(){
		if(data == null || title == null || data.equals("") || title.equals("")){
			return 0;
		}
		StopWordsTrie swt = new StopWordsTrie();		
		data = swt.removeStopWords(swt.removeSpecialCharacters(data.toString()), headNode);
		searchPhrases = generatePhrasesFromTitle(title, true);
		if(searchPhrases.size() == 0){
			return 0;
		}		
		return findRelevancy_v2();
	}
	
	private HashMap <Integer, ArrayList<String>> generatePhrasesFromTitle(String title, boolean removeStopWords){
		StopWordsTrie swt = new StopWordsTrie();
		String []splitedPhrases = title.split("(\\s)*[^a-zA-Z0-9]+(\\s)+");
		HashMap <Integer, ArrayList<String>> searchPhrases = new HashMap<Integer, ArrayList<String>>();
		int maxWordInPhraseCount = 0;
		for(String phrase: splitedPhrases){
			if(removeStopWords){
				phrase = swt.removeStopWords(phrase, headNode);
			}
			String []words = phrase.split("\\s+");
			int tempCount = maxWordInPhraseCount;
			for(;tempCount < words.length && tempCount < 6; tempCount++){
				searchPhrases.put(tempCount+1, new ArrayList<String>());
			}
			maxWordInPhraseCount = tempCount;
			for(int i = 0; i < words.length && i < 6; i++){
				for(int j = 0; j < words.length - i; j++){
					String tempPhrase = "";
					for(int k = j; k <= j + i; k++){
						tempPhrase = tempPhrase + " " +words[k];
					}
					if(!searchPhrases.get(i+1).contains(tempPhrase.trim())){
						searchPhrases.get(i+1).add(tempPhrase.trim());					
					}
				}
			}
		}
		return searchPhrases;
	}
	
//	private void showSearchPhrases(HashMap <Integer, ArrayList<String>> searchPhrases){
//		Iterator<Entry<Integer, ArrayList<String>>> it = searchPhrases.entrySet().iterator(); 
//		while (it.hasNext()) {
//	        Map.Entry<Integer, ArrayList<String>> pairs = (Map.Entry<Integer, ArrayList<String>>)it.next();
//	        ArrayList<String> phrases = (ArrayList<String>)pairs.getValue();
//	        System.out.println("\nSearch Phrases of length "+pairs.getKey());
//	        for(String phrase : phrases){
//	        	System.out.println(phrase);
//	        }
//	    }
//	}
	
}
