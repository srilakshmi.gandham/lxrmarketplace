package lxr.marketplace.util;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.AttachmentService;
import lxr.marketplace.admin.automail.EmailTemplate;
import lxr.marketplace.admin.automail.EmailTrackingInfoService;
import lxr.marketplace.apiaccess.lxrmbeans.WhoIsInfo;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.aws.mail.AWSSendMail;
import lxr.marketplace.feedback.Feedback;
import lxr.marketplace.keywordrankchecker.KeywordTaskInfo;
import lxr.marketplace.user.Login;
import org.apache.commons.io.FilenameUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class SendMail {

    private static Logger logger = Logger.getLogger(SendMail.class);

    static Properties properties = null;
    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
    public static final long MAX_FILE_SIZE = 2097152;

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        SendMail.ctx = ctx;
    }

    public static void sendToolAPIMessageMail(String subjectLine, String bodyText) {
        sendMail((String) ctx.getBean("supportMail"), (String) ctx.getBean("supportMail"), subjectLine, bodyText);
    }

    public static void sendToolAPIMessageMailV2(String subjectLine, String bodyText, String fromAddr, String toAddr) {
        sendMail(fromAddr, toAddr, subjectLine, bodyText);
    }

    public static boolean sendMail(String fromAddr, String toAddr, String subjectLine, String bodyText) {
        boolean messageSent = false;
        if (toAddr != null && !toAddr.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart messageBodyPart;
            MimeMultipart multipart;
            try {
                Properties prop = System.getProperties();
                prop.setProperty("mail.smtp.host", "localhost");
                // Get the session for this property
                Session session1 = Session.getDefaultInstance(prop);
                // Create email objects
                message = new MimeMessage(session1);
                if (fromAddr == null || fromAddr.equals("") || fromAddr.trim().length() == 0) {
                    fromAddr = (String) ctx.getBean("supportMail");
                }
                frmAddress = new InternetAddress(fromAddr);
                message.setFrom(frmAddress);
                messageBodyPart = new MimeBodyPart();
                multipart = new MimeMultipart();

                messageBodyPart.setContent(bodyText, "text/html;charset=utf-8");
                multipart.addBodyPart(messageBodyPart);
//                if (toAddr == null || toAddr.equals("") || toAddr.trim().length() == 0) {
//                    toAddr = (String) ctx.getBean("supportMail");
//                }
                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setText(bodyText);
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException mex) {
                logger.error(mex);
            }
            if (messageSent) {
                logger.info("Mail Sent to " + toAddr);
            } else {
                logger.info("Mail Failed to Send");
            }
        }
        return messageSent;
    }

    //send mails with  cc
    public static boolean sendMail(String fromAddr, String toAddr, String toCC, String subject, String displayTxt) {
        boolean messageSent = false;
        if (toAddr != null && !toAddr.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                Properties prop = System.getProperties();
                prop.setProperty("mail.smtp.host", "localhost");

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(prop, null);
                // Create email objects
                message = new MimeMessage(session1);
                message.setFrom(new InternetAddress(fromAddr));
                multipart = new MimeMultipart();
                multipart.setSubType("related");
                mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setText(displayTxt);
                mimeBodyPart.setContent(displayTxt, "text/html;charset=UTF-8");
                multipart.addBodyPart(mimeBodyPart);

                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                if (!toCC.equals("")) {
                    InternetAddress[] bccAddress = {new InternetAddress(toCC)};
                    message.setRecipients(Message.RecipientType.CC, bccAddress);
                }
                message.setSubject(subject);
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail sent to " + toAddr);
            } else {
                logger.info("Mail sending failed to " + toAddr + " bcc " + toAddr);
            }
        }
        return messageSent;
    }

    //send mails with many cc
    public static boolean sendMail(String fromAddr, String[] expertsMails, String subject, String displayTxt) {
        boolean messageSent = false;
        if (expertsMails != null && expertsMails.length > 0) {
            InternetAddress[] toAddress = new InternetAddress[expertsMails.length];
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                Properties prop = System.getProperties();
                prop.setProperty("mail.smtp.host", "localhost");
                // Get the session for this property
                Session session1 = Session.getDefaultInstance(prop, null);
                // Create email objects
                message = new MimeMessage(session1);
                message.setFrom(new InternetAddress(fromAddr));
                multipart = new MimeMultipart();
                multipart.setSubType("related");
                mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setText(displayTxt);
                mimeBodyPart.setContent(displayTxt, "text/html;charset=UTF-8");
                multipart.addBodyPart(mimeBodyPart);

                int i = 0;
                for (String expertMail : expertsMails) {
                    toAddress[i] = new InternetAddress(expertMail);
                    i++;
                }

                message.setRecipients(Message.RecipientType.TO, toAddress);
                message.setSubject(subject);
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException e) {
                logger.error("Exception on sending mail to all:", e);
            }
            if (messageSent) {
                logger.info(" Mail sent to all");
            } else {
                logger.info("Mail send fail to all");
            }
        }
        return messageSent;
    }

    public void amazonProductReportMail(String filepath, String fileName, Login user, String toAddr, String fromAddrs,
            String subjectLine, String bodyText) {
        boolean messageSent = false;
        InternetAddress[] toAddress = new InternetAddress[1];
        InternetAddress frmAddress;
        Message message;
        MimeBodyPart mimeBodyPart;
        MimeMultipart multipart;

        try {
            if (properties == null) {
                properties = new Properties();
                // Set the mail server
                properties.put("mail.host", "localhost");
            }

            // Get the session for this property
            Session session1 = Session.getDefaultInstance(properties, null);
            // Create email objects
            message = new MimeMessage(session1);
            frmAddress = new InternetAddress(fromAddrs);
            message.setFrom(frmAddress);
            multipart = new MimeMultipart();
            multipart.setSubType("related");

            if (filepath + fileName != null && (filepath + fileName).length() > 0) {
                mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setText("Hi " + user.getName() + "," + "\n\nPlease find the attached Report for the Products Comparision, which is uploaded in the Amazon Price Analyzer."
                        + "\n\nAt NetElixir, we strive to simplify the lives of retail marketers by providing them with free web applications. Thank you again for utilizing our tools and services."
                        + "\n\nIf you require any further information or help, please contact us at support@lxrmarketplace.com."
                        + "\n\nSincerely," + "\n\nYour LXRMarketplace Team."
                        + "\nwww.lxrmarketplace.com"
                        + "\nPowerful. Simple. Free.");
                multipart.addBodyPart(mimeBodyPart);
                mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filepath + fileName)));
                mimeBodyPart.setFileName(fileName);
                multipart.addBodyPart(mimeBodyPart);
            }

            toAddress[0] = new InternetAddress(toAddr);
            message.setRecipients(Message.RecipientType.TO, toAddress);

            message.setSubject(subjectLine);
            message.setSentDate(new Date());
            message.setContent(multipart);
            message.saveChanges();

            // To send email using AWS
            messageSent = AWSSendMail.sendMailFromAwsVersion2(message);
        } catch (Exception e) {
            logger.error(e);
        }
        if (messageSent) {
            logger.info("Mail Sent");
        } else {
            logger.info("Mail Failed to Send");
        }
    }

    public synchronized void sendAdminMail(EmailTemplate emailTemplate, Login user, String fromAddress) {
        if (!user.isUnsubscribed() && user.getUserName() != null && !user.getUserName().isEmpty()) {
            String toAddr = user.getUserName();
            String fromAddr = fromAddress;
            String[] attachments;
            boolean mailsent;
            String subject = emailTemplate.getMailSubject();
            String bodyText = emailTemplate.getMailBody();
            String name = user.getName();
            EmailTrackingInfoService emailTrackingInfoservice = (EmailTrackingInfoService) ctx.getBean("emailTrackingInfoService");
            String appUrl = (String) ctx.getBean("domainName");
            bodyText = emailTrackingInfoservice.appendUserName(user, bodyText, emailTemplate);
            bodyText = emailTrackingInfoservice.appendTrackingParameters(appUrl, user, bodyText);
            AttachmentService attchmentService = (AttachmentService) ctx.getBean("attachmentService");
            attachments = attchmentService.getListOfFilesById(emailTemplate.getId());
            String attachmentspath = ctx.getBean("attachmentFolder") + "" + emailTemplate.getId();
            mailsent = sendMailWithAttachments(toAddr, fromAddr, subject, bodyText, name, attachmentspath, attachments);
            if (mailsent) {
                emailTrackingInfoservice.insertUserEvents(user);
            }
        }
    }

    public static synchronized boolean sendMailWithAttachments(String toAddr, String fromAddr,
            String subjectLine, String bodyText, String name, String attachpath, String[] attachments) {
        boolean messageSent = false;
        if (toAddr != null && !toAddr.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                if (properties == null) {
                    properties = new Properties();
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(fromAddr);
                message.setFrom(frmAddress);
                mimeBodyPart = new MimeBodyPart();
                multipart = new MimeMultipart();
                mimeBodyPart.setContent(bodyText, "text/html;charset=utf-8");
                multipart.addBodyPart(mimeBodyPart);
                if (attachments != null && attachments.length > 0) {
                    for (String attachment : attachments) {
                        mimeBodyPart = new MimeBodyPart();
                        mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachpath + "/" + attachment)));
                        mimeBodyPart.setFileName(attachment);
                        multipart.addBodyPart(mimeBodyPart);
                    }
                }
                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setText(bodyText);
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent to " + toAddr);
            } else {
                logger.info("Mail Failed to Send");
            }
        }
        return messageSent;
    }

    /*To send mail with multiple attachments, CC/BCC address.*/
    public static synchronized boolean sendMailWithAttachments(String toAddr, String fromAddr,
            String subjectLine, String bodyText, String attachpath, String[] attachments, String[] toCCAddress) {
        boolean messageSent = false;
        if (toAddr != null && !toAddr.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress[] toCCInternetAddress = null;
            if (toCCAddress != null) {
                toCCInternetAddress = new InternetAddress[toCCAddress.length];
            }
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                if (properties == null) {
                    properties = new Properties();
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(fromAddr);
                message.setFrom(frmAddress);
                mimeBodyPart = new MimeBodyPart();
                multipart = new MimeMultipart();
                mimeBodyPart.setContent(bodyText, "text/html;charset=utf-8");
                multipart.addBodyPart(mimeBodyPart);
                String filePath = null;
                File file = null;
                String fileType = null;
                if (attachments != null && attachments.length > 0) {
                    for (String attachment : attachments) {
                        mimeBodyPart = new MimeBodyPart();
                        file = new File(attachpath + "/" + attachment);
                        fileType = FilenameUtils.getExtension(attachpath + "/" + attachment);
                        if (file.length() > MAX_FILE_SIZE && !fileType.equalsIgnoreCase("zip")) {
                            attachment = Common.compressFile(attachpath + "/", attachment, true);
                            filePath = attachpath + "/" + attachment;
                        } else {
                            filePath = attachpath + "/" + attachment;
                        }
                        mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filePath)));
                        mimeBodyPart.setFileName(attachment);
                        multipart.addBodyPart(mimeBodyPart);
                    }
                }
                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);

                if (toCCAddress != null) {
                    int i = 0;
                    for (String copyMailAddress : toCCAddress) {
                        toCCInternetAddress[i] = new InternetAddress(copyMailAddress);
                        i++;
                    }
                    message.setRecipients(Message.RecipientType.CC, toCCInternetAddress);
                }

                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setText(bodyText);
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (Exception e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent to " + toAddr);
            } else {
                logger.info("Mail Failed to Send");
            }
        }
        return messageSent;
    }

    public void sendWeeklyTooleport(String filepath, String fileName, String toAddr, String fromAddrs,
            String subjectLine, String toolName, String userName, String seventhDate, String curDate) {
        boolean messageSent = false;
        if (toAddr != null && !toAddr.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                if (properties == null) {
                    properties = new Properties();
                    // Set the mail server
                    properties.put("mail.host", "localhost");
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(fromAddrs);
                message.setFrom(frmAddress);
                multipart = new MimeMultipart();
                multipart.setSubType("related");
                logger.info("In send mail the file name" + fileName + "  and file path" + filepath + "   from address" + fromAddrs);
                if (filepath + fileName != null && (filepath + fileName).length() > 0) {
                    mimeBodyPart = new MimeBodyPart();
                    String applicationUrl = (String) ctx.getBean("domainName");
                    String mailBody = "<div width=\"728px\">";
                    mailBody += "<div id=\"head\" style=\"padding : 5px; border:2px solid #FFaa55;\"> "
                            + "<div style=\"background-color: white;\">"
                            + "<a href=\"" + applicationUrl + "\"> "
                            + "<img src='" + applicationUrl + "/css/0/images/LXRMarketplace.png' "
                            + "alt='lxrmarketplace.com' /></a> "
                            + "</div></div>";
                    mailBody += "<div style=\"padding-top:40px;font-size:12px;\"><span >Dear " + userName + ",</span></div><br>";
                    mailBody += "<div style=\"font-size:12px; padding-bottom:20px;\"><span  style=\"padding-top:40px;\">"
                            + "Your Weekly Report for the week (" + seventhDate + " – " + curDate + ") is ready .</span><br><br>";
                    mailBody += "<span style=\"padding-top:25px;font-size:12px;\">At NetElixir, we strive to simplify the lives of retail marketers by "
                            + "providing them with free web applications. Thank you again for utilizing our tools and "
                            + "services.</span><br><br>";
                    mailBody += "<span>If you require any further information or help, please contact us at "
                            + "support@lxrmarketplace.com.</span></div>";
                    mailBody += "<div style=\"font-size:12px; font-color:black; background-color: #dddddd; padding:10px;\"><span style=\"padding-top:50px;\">Sincerely,<br><br>LXRMarketplace Team.<br>"
                            + "<a href='https://lxrmarketplace.com'>https://xrmarketplace.com</a><br>"
                            + "</span>";
                    mailBody += "</div></div>";
                    mimeBodyPart.setContent(mailBody, "text/html;charset=UTF-8");
                    multipart.addBodyPart(mimeBodyPart);
                    mimeBodyPart = new MimeBodyPart();
                    mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filepath + fileName)));
                    mimeBodyPart.setFileName(fileName);
                    multipart.addBodyPart(mimeBodyPart);
                }

                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                //message.setText(bodyText);
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent to " + toolName + "  " + toAddr);
            } else {
                logger.info("Mail sending Failed");
            }
        }
    }

    public boolean sendTooleport(HttpServletRequest request, String filepath, String fileName, String toAddr, String fromAddrs,
            String subjectLine, String toolName, String bccAddress) {
        boolean messageSent = Boolean.FALSE;
        if (toAddr != null && !toAddr.isEmpty()) {
            /*In adspy tool multiple emails will be send to user*/
            if (toolName.contains("Ad Spy")) {
                toolName = "Ad Spy";
            }
            HttpSession session = request.getSession();
            Login user = (Login) session.getAttribute("user");

            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            String userName;
            if (user != null && user.getName() != null && !user.getName().isEmpty()) {
                userName = user.getName();
            } else if (user != null && (user.getUserName() != null && !user.getUserName().isEmpty() && !user.getUserName().toLowerCase().equals("email"))) {
                userName = user.getUserName().split("@")[0];
            } else if (toAddr != null) {
                userName = toAddr.split("@")[0];
            } else {
                userName = "LXRMarketplace User";
            }

            try {
                if (properties == null) {
                    properties = new Properties();
                    // Set the mail server
                    properties.put("mail.host", "localhost");
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(fromAddrs);
                message.setFrom(frmAddress);
                multipart = new MimeMultipart();
                multipart.setSubType("related");

                if (filepath + fileName != null && (filepath + fileName).length() > 0) {
                    mimeBodyPart = new MimeBodyPart();
                    String applicationUrl = (String) ctx.getBean("domainName");
                    String mailBody = "<div width=\"728px\">";
                    mailBody += "<div id=\"head\" style=\"padding : 5px; border:2px solid #FFaa55;\"> "
                            + "<div style=\"background-color: white;\">"
                            + "<a href=\"" + applicationUrl + "\"> "
                            + "<img src='" + applicationUrl + "/css/0/images/LXRMarketplace.png' "
                            + "alt='lxrmarketplace.com' /></a> "
                            + "</div></div>";
                    mailBody += "<div style=\"padding-top:40px;\"><span style=\"font-size:15px;\">Dear " + userName + ",</span></div><br>";
                    mailBody += "<div style=\"font-size:14px; padding-bottom:20px;\"><span  style=\"padding-top:40px;\">Thank you for using " + toolName + " tool in LXRMarketplace. Please find the "
                            + "attached report for your tool usage.</span><br><br>";

                    mailBody += "<span style=\"padding-top:25px;\">At NetElixir, we strive to simplify the lives of retail marketers by "
                            + "providing them with free web applications. Thank you again for utilizing our tools and "
                            + "services.</span><br><br>";
                    mailBody += "<span>If you require any further information or help, please contact us at "
                            + "support@lxrmarketplace.com.</span></div>";
                    mailBody += "<div style=\"font-size:12px; font-color:black; background-color: #dddddd; padding:10px;\"><span style=\"padding-top:50px;\">Sincerely,<br><br>LXRMarketplace Team.<br>"
                            + "<a href='https://lxrmarketplace.com'>https://xrmarketplace.com</a><br>"
                            + "</span>";
                    mailBody += "</div></div>";
                    mimeBodyPart.setContent(mailBody, "text/html;charset=UTF-8");
                    multipart.addBodyPart(mimeBodyPart);
                    mimeBodyPart = new MimeBodyPart();
                    mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filepath + fileName)));
                    mimeBodyPart.setFileName(fileName);
                    multipart.addBodyPart(mimeBodyPart);
                }

                toAddress[0] = new InternetAddress(toAddr);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                if (bccAddress != null) {
                    InternetAddress[] bccAddressLIst = {new InternetAddress(bccAddress)};
                    message.setRecipients(Message.RecipientType.BCC, bccAddressLIst);
                }

                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent for tool: " + toolName + ", user email: " + toAddr);
            } else {
                logger.info("Mail sending Failed");
            }
        }
        return messageSent;
    }

    public boolean sendFeedBackMail(Feedback feedback, String toAdress, String[] attachments, long id) {
        boolean messageSent = false;
        if (toAdress != null && !toAdress.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart;
            try {
                if (properties == null) {
                    properties = new Properties();
                    // Set the mail server
                    properties.put("mail.host", "localhost");
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                String fromAddr = (String) ctx.getBean("supportMail");
                message.setFrom(new InternetAddress(fromAddr));
                multipart = new MimeMultipart();
                multipart.setSubType("related");
                mimeBodyPart = new MimeBodyPart();
                String phnNo = feedback.getPhoneNo() != null ? feedback.getPhoneNo() : "";
                String totPhoneString = "";
                if (phnNo != null && !phnNo.trim().equals("")) {
                    totPhoneString = "Phone: " + phnNo;
                }

                mimeBodyPart.setText(feedback.getMailBody() + "\n\n" + "User Details: " + "\nName: "
                        + feedback.getName() + "\nEmail Id: " + feedback.getEmail() + "\n" + totPhoneString);//+"\n\n\n"+feedback.getName()+"\n"+feedback.getPhoneNo() != null?feedback.getPhoneNo():"");
                multipart.addBodyPart(mimeBodyPart);
                mimeBodyPart = new MimeBodyPart();
                if (attachments != null && attachments.length > 0) {
                    for (String attachment : attachments) {
                        mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource((String) ctx.getBean("feedbackattachmentFolder") + id + "/" + attachment)));
                        mimeBodyPart.setFileName(attachment);
                        multipart.addBodyPart(mimeBodyPart);
                    }
                }

                toAddress[0] = new InternetAddress(toAdress);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                InternetAddress[] bccAddress = {new InternetAddress(feedback.getName() + "<" + feedback.getEmail() + ">")};
                message.setRecipients(Message.RecipientType.BCC, bccAddress);

                String subject = " User query " + " || " + feedback.getSubject();
                message.setSubject(subject);
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent");
            } else {
                logger.info("Mail sending Failed");
            }
        }
        return messageSent;
    }

    public static boolean sendAlertMailToUser(String domain, String email, List<KeywordTaskInfo> alertValueReachedKeywords, String userName, String tempCurDate) {
        boolean messageSent = false;
        if (email != null && !email.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart messageBodyPart;
            MimeMultipart multipart;
            String fromAddr = (String) ctx.getBean("supportMail");
            String subjectLine = "Weekly Keyword Rank Checker Alert";
            String gRank, bRank;
            try {
                if (properties == null) {
                    properties = new Properties();
                    // Set the mail server
                    properties.put("mail.host", "localhost");
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties, null);
                // Create email objects
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(fromAddr);
                message.setFrom(frmAddress);
                messageBodyPart = new MimeBodyPart();
                multipart = new MimeMultipart();

                String applicationUrl = (String) ctx.getBean("domainName");
                String bodyText = "<body  width=\"728px\">";
                bodyText += "<div id=\"head\" style=\"padding : 5px; border:2px solid #FFaa55;\"> "
                        + "<div style=\"background-color: white;\">"
                        + "<a href=\"" + applicationUrl + "\"> "
                        + "<img src='" + applicationUrl + "/css/0/images/LXRMarketplace.png' "
                        + "alt='lxrmarketplace.com' /></a> "
                        + "</div></div><br/>";

                bodyText += "<p>Dear " + userName + ","
                        + "<br/>"
                        + "<br/>This is a custom alert from LXRMarketplace.com. <br/>"
                        + "<br/>Your keyword has reached the specified listing position (Date: " + tempCurDate + ").<br/>";
                String header = "";
                for (KeywordTaskInfo kwdTask : alertValueReachedKeywords) {
                    if (kwdTask.getSerachEngine().trim().equalsIgnoreCase("Google,Bing")) {
                        header = "<th style=\"border: 1px solid #E9E9E9;text-align: center;\">Google Rank</th><th style=\"border: 1px solid #E9E9E9;text-align: center;\">Bing Rank</th>";
                    } else if (kwdTask.getSerachEngine().trim().equalsIgnoreCase("Google")) {
                        header = "<th style=\"border: 1px solid #E9E9E9;text-align: center;\">Google Rank</th>";
                    } else if (kwdTask.getSerachEngine().trim().equalsIgnoreCase("Bing")) {
                        header = "<th style=\"border: 1px solid #E9E9E9;text-align: center;\">Bing Rank</th>";
                    }
                }
                bodyText += "<br/><table style=\"border:1px solid #E9E9E9;border-collapse: collapse; \" cellspacing=\"5\" cellpadding=\"5\" ><tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\"><th style=\"border: 1px solid #E9E9E9;text-align: center;\">Keyword</th><th style=\"border: 1px solid #E9E9E9;text-align: center;\">Search Engine</th>" + header + "</tr>";

                for (KeywordTaskInfo kwdTaskInfo : alertValueReachedKeywords) {
                    String data = "";
                    switch (kwdTaskInfo.getGoogleRank()) {
                        case -1:
                            gRank = ">100";
                            break;
                        case 0:
                            gRank = "-";
                            break;
                        default:
                            gRank = "" + kwdTaskInfo.getGoogleRank();
                            break;
                    }
                    switch (kwdTaskInfo.getBingRank()) {
                        case -1:
                            bRank = ">100";
                            break;
                        case 0:
                            bRank = "-";
                            break;
                        default:
                            bRank = "" + kwdTaskInfo.getBingRank();
                            break;
                    }
                    if (kwdTaskInfo.getSerachEngine().trim().equalsIgnoreCase("Google,Bing")) {
                        data = "<td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + gRank + "</td><td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + bRank + "</td>";

                    } else if (kwdTaskInfo.getSerachEngine().trim().equalsIgnoreCase("Google")) {
                        data = "<td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + gRank + "</td>";
                    } else if (kwdTaskInfo.getSerachEngine().trim().equalsIgnoreCase("Bing")) {
                        data = "<td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + bRank + "</td>";
                    }
                    bodyText += "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\"><td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + kwdTaskInfo.getKeyword() + "</td><td style=\"border: 1px solid #E9E9E9;text-align: center;\">" + kwdTaskInfo.getSerachEngine() + "</td>" + data + "</tr>";
                }

                bodyText += " </table><br/> ";
                bodyText += "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                        + "<br/>Thank you!<br/>"
                        + "<br/>Your LXRMarketplace Team.<br/>"
                        + "<a href='https://lxrmarketplace.com'>https://xrmarketplace.com</a>"
                        + "<br/>Powerful. Simple. Free.</p>"
                        + "</body>";

                messageBodyPart.setContent(bodyText, "text/html;charset=utf-8");
                multipart.addBodyPart(messageBodyPart);
                toAddress[0] = new InternetAddress(email);
                message.setRecipients(Message.RecipientType.TO, toAddress);

                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setText(bodyText);
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException e) {
                logger.error(e);
            }
            if (messageSent) {
                logger.info("Mail Sent to " + email);
            } else {
                logger.info("Mail Failed to Send");
            }
        }
        return messageSent;
    }

    // SendMail With Attachments
    public static void sendMailWithAttachMent(String filePath, String fileName, String name, String email, String subjectLine, String bodyText, boolean sendBCC) {
        boolean messageSent = false;
        if (email != null && !email.isEmpty()) {
            InternetAddress[] toAddress = new InternetAddress[1];
            InternetAddress frmAddress;
            Message message;
            MimeBodyPart mimeBodyPart;
            MimeMultipart multipart = null;
            try {
                if (properties == null) {
                    properties = new Properties();
                    // Set the mail server
                    properties.put("mail.host", "localhost");
                }

                // Get the session for this property
                Session session1 = Session.getDefaultInstance(properties);
                // Create email objects
                String frmAddrss = (String) ctx.getBean("supportMail");
                message = new MimeMessage(session1);
                frmAddress = new InternetAddress(frmAddrss);
                message.setFrom(frmAddress);
                if (filePath + fileName != null && (filePath + fileName).length() > 0) {
                    mimeBodyPart = new MimeBodyPart();
                    multipart = new MimeMultipart();
                    mimeBodyPart.setContent(bodyText, "text/html;charset=UTF-8");
                    multipart.addBodyPart(mimeBodyPart);
                    mimeBodyPart = new MimeBodyPart();
                    mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filePath + fileName)));
                    mimeBodyPart.setFileName(fileName);
                    multipart.addBodyPart(mimeBodyPart);
                }
                toAddress[0] = new InternetAddress(email);
                message.setRecipients(Message.RecipientType.TO, toAddress);
                if (sendBCC) {
                    InternetAddress[] bccAddress = {new InternetAddress(frmAddrss)};
                    message.setRecipients(Message.RecipientType.BCC, bccAddress);
                }

                message.setSubject(subjectLine);
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

            } catch (MessagingException | BeansException e) {
                logger.error("Exception on sending invoice mail to user:", e);
            }
        }
        if (messageSent) {
            logger.info("Mail Sent to " + email);
        } else {
            logger.info("Mail Failed to Send");
        }
    }

    public static void mailToFeedbackUponSchedulerFires(String schedulerName, String subjectLine) {
        boolean messageSent = false;
        InternetAddress[] toAddress = new InternetAddress[1];
        InternetAddress frmAddress;
        Message message;
        MimeBodyPart messageBodyPart;
        MimeMultipart multipart;
        try {
            if (properties == null) {
                properties = new Properties();
                // Set the mail server
                properties.put("mail.host", "localhost");
            }

            // Get the session for this property
            Session session1 = Session.getDefaultInstance(properties);
            // Create email objects
            String frmAddrss = (String) ctx.getBean("supportMail");
            message = new MimeMessage(session1);
            frmAddress = new InternetAddress(frmAddrss);
            message.setFrom(frmAddress);
            messageBodyPart = new MimeBodyPart();
            multipart = new MimeMultipart();
            String applicationUrl = (String) ctx.getBean("domainName");
            String bodyText = "<body>";
            bodyText += "<div width=\"728px\">";
            bodyText += "<div id=\"head\" style=\"padding : 5px;\"> "
                    + "<div style=\"background-color: white;\">"
                    + "<a href=\""
                    + applicationUrl
                    + "\"> "
                    + "<img src='"
                    + applicationUrl
                    + "/css/0/images/LXRMarketplace.png' "
                    + "alt='lxrmarketplace.com'  style=\"padding:22px 0;\" /></a> "
                    + "</div></div>";
            bodyText += schedulerName;
            bodyText += "</body>";
            messageBodyPart.setContent(bodyText, "text/html;charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            toAddress[0] = new InternetAddress(frmAddrss);
            message.setRecipients(Message.RecipientType.TO, toAddress);

            message.setSubject(subjectLine);
            message.setSentDate(new Date());
            message.setText(bodyText);

            message.setContent(multipart);
            message.saveChanges();

            // To send email using AWS
            messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

        } catch (MessagingException mex) {
            logger.error("Exception in Sending mail", mex);
        } catch (BeansException e) {
            logger.error("Exception in Sending mail", e);
            messageSent = false;
        }
        if (messageSent) {
            logger.info(" Mail Sent to " + toAddress[0]);
        } else {
            logger.info("Mail Failed to Send");
        }
    }

    public static boolean sendDomainAgeCheckerMailToUser(String domain, String email, WhoIsInfo whoIsInfo, String userName, String tempCurDate) {
        boolean messageSent = true;
        InternetAddress[] toAddress = new InternetAddress[1];
        InternetAddress frmAddress;
        Message message;
        MimeBodyPart messageBodyPart;
        MimeMultipart multipart;
        String fromAddr = (String) ctx.getBean("supportMail");
        String subjectLine = "Domain Age Checker Tool Report";
        if (userName == null || userName.trim().equals("")) {
            userName = "LXRMarketplace User";
        }
        try {
            if (properties == null) {
                properties = new Properties();
                // Set the mail server
                properties.put("mail.host", "localhost");
            }

            // Get the session for this property
            Session session1 = Session.getDefaultInstance(properties, null);
            // Create email objects
            message = new MimeMessage(session1);
            frmAddress = new InternetAddress(fromAddr);
            message.setFrom(frmAddress);
            messageBodyPart = new MimeBodyPart();
            multipart = new MimeMultipart();
            String applicationUrl = (String) ctx.getBean("domainName");
            String bodyText = "<body  width=\"728px\">";
            bodyText += "<div id=\"head\" style=\"padding : 5px; border:2px solid #FFaa55;\"> "
                    + "<div style=\"background-color: white;\">"
                    + "<a href=\"" + applicationUrl + "\"> "
                    + "<img src='" + applicationUrl + "/css/0/images/LXRMarketplace.png' "
                    + "alt='lxrmarketplace.com' /></a> "
                    + "</div></div><br/>";

            bodyText += "<p>Dear " + userName + ","
                    + "<br/>"
                    + "<br/>Thank you for using Domain Age Checker tool in LXRMarketplace. Please find the result for your tool usage <br/>";

            bodyText += "<br/><table style=\"width:100%;line-height:22px; \" cellspacing=\"5\" cellpadding=\"5\" >"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td>"
                    + "<table style=\"border:1px solid #E9E9E9;border-collapse: collapse;width:99.8%;line-height:22px; \" cellspacing=\"5\" cellpadding=\"5\" >"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;color:#f60;font-weight: bold;height: 30px;font-size: 14px;\">"
                    + "<td style=\"border: 0px;\" colspan=\"2\">Domain Registration Details</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Registered Domain Name</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getRegisteredDomName()+ "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Estimated Domain Age</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getDomainAge() + "</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Domain Created Date</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getCreatedDate() + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Domain Updated Date</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getUpdatedDate() + "</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Domain expired Date</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getRegExpiration() + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</td>"
                    + "</tr>";
            bodyText += "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td>"
                    + "<table style=\"border:1px solid #E9E9E9;border-collapse: collapse;width:99.8%;line-height:22px; \" cellspacing=\"5\" cellpadding=\"5\" >"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;color:#f60;font-weight: bold;height: 30px;font-size: 14px;\">"
                    + "<td style=\"border: 0px;\" colspan=\"2\">Domain Registrant Details</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Domain Registrant name</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getRegistrantName() + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Registrant Email</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\"> " + whoIsInfo.getRegistrantEmail() + "</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Registrant Address</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getRegistrantAddress(whoIsInfo) + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;font-weight:bold;\">Registrant Phone</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getRegistrantPhone() + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</td>"
                    + "</tr>";
            bodyText += "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td>"
                    + "<table style=\"border:1px solid #E9E9E9;border-collapse: collapse;width:99.8%;line-height:22px; \" cellspacing=\"5\" cellpadding=\"5\" >"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;color:#f60;font-weight: bold;height: 30px;font-size: 14px;\">"
                    + "<td style=\"border: 0px;\" colspan=\"2\">Administrative Contact Details</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Name</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getAdminName() + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Email</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getAdminEmail() + "</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Address</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getAdministrativeAddress(whoIsInfo) + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Phone</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getAdminPhone() + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</td>"
                    + "</tr>";
            bodyText += "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td>"
                    + "<table style=\"border:1px solid #E9E9E9;border-collapse: collapse;width:99.8%;line-height:22px; \" cellspacing=\"5\" cellpadding=\"5\" >"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;color:#f60;font-weight: bold;height: 30px;font-size: 14px;\">"
                    + "<td style=\"border: 0px;\" colspan=\"2\">Technical Contact Details</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Name</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getTechName() + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Email</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getTechEmail() + "</td>"
                    + "</tr>"
                    + "<tr style=\"display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Address</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getTechnicalAddress(whoIsInfo) + "</td>"
                    + "</tr>"
                    + "<tr style=\"background-color:rgba(181,219,251,0.3);display: table-row;vertical-align: inherit;border-color: inherit;\">"
                    + "<td style=\"font-weight:bold;border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">Phone</td>"
                    + "<td style=\"border: 1px solid #E9E9E9;word-wrap:break-word;padding:4px;width:50%;\">" + whoIsInfo.getTechPhone() + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</td>"
                    + "</tr>";
            bodyText += "</table><br/> ";
            bodyText += "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                    + "<br/>Thank you!<br/>"
                    + "<br/>Your LXRMarketplace Team.<br/>"
                    + "<a href = \"https://lxrmarketplace.com\">https://lxrmarketplace.com</a>"
                    + "<br/>Powerful. Simple. Free.</p>"
                    + "</body>";

            messageBodyPart.setContent(bodyText, "text/html;charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            toAddress[0] = new InternetAddress(email);
            message.setRecipients(Message.RecipientType.TO, toAddress);
            message.setSubject(subjectLine);
            message.setSentDate(new Date());
            message.setText(bodyText);
            message.setContent(multipart);
            message.saveChanges();

            // To send email using AWS
            messageSent = AWSSendMail.sendMailFromAwsVersion2(message);

        } catch (MessagingException | BeansException e) {
            logger.info("Exception ", e);
        }
        if (messageSent) {
            logger.info("Mail Sent to " + email);
        } else {
            logger.info("Mail Failed to Send");
        }
        return messageSent;
    }

    public static void sendSiteMapToolReport(String filepath, String fileName, String toAddr, String fromAddrs,
            String subjectLine, String toolName, String userName) {
        boolean messageSent = false;
        InternetAddress[] toAddress = new InternetAddress[1];
        InternetAddress frmAddress;
        Message message;
        MimeBodyPart mimeBodyPart;
        MimeMultipart multipart;
        try {
            if (properties == null) {
                properties = new Properties();
                // Set the mail server
                properties.put("mail.host", "localhost");
            }

            // Get the session for this property
            Session session1 = Session.getDefaultInstance(properties, null);
            // Create email objects
            message = new MimeMessage(session1);
            frmAddress = new InternetAddress(fromAddrs);
            message.setFrom(frmAddress);
            multipart = new MimeMultipart();
            multipart.setSubType("related");

            if (filepath + fileName != null && (filepath + fileName).length() > 0) {
                mimeBodyPart = new MimeBodyPart();
                String applicationUrl = (String) ctx.getBean("domainName");
                String mailBody = "<div width=\"728px\">";
                mailBody += "<div id=\"head\" style=\"padding : 5px; border:2px solid #FFaa55;\"> "
                        + "<div style=\"background-color: white;\">"
                        + "<a href=\"" + applicationUrl + "\"> "
                        + "<img src='" + applicationUrl + "/css/0/images/LXRMarketplace.png' "
                        + "alt='lxrmarketplace.com' /></a> "
                        + "</div></div>";
                mailBody += "<div style=\"padding-top:40px;\"><span style=\"font-size:15px;\">Dear " + userName + ",</span></div><br>";
                mailBody += "<div style=\"font-size:14px; padding-bottom:20px;\"><span  style=\"padding-top:40px;\">Thank you for using " + toolName + " tool in LXRMarketplace. Please find the "
                        + "attached report for your tool usage.</span><br><br>";

                mailBody += "<span style=\"padding-top:25px;\">At NetElixir, we strive to simplify the lives of retail marketers by "
                        + "providing them with free web applications. Thank you again for utilizing our tools and "
                        + "services.</span><br><br>";
                mailBody += "<span>If you require any further information or help, please contact us at "
                        + "support@lxrmarketplace.com.</span></div>";
                mailBody += "<div style=\"font-size:12px; font-color:black; background-color: #dddddd; padding:10px;\"><span style=\"padding-top:50px;\">Sincerely,<br><br>LXRMarketplace Team.<br>"
                        + "<a href='https://lxrmarketplace.com'>https://xrmarketplace.com</a><br>"
                        + "</span>";
                mailBody += "</div></div>";
                mimeBodyPart.setContent(mailBody, "text/html;charset=UTF-8");
                multipart.addBodyPart(mimeBodyPart);
                mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(filepath + fileName)));
                mimeBodyPart.setFileName(fileName);
                multipart.addBodyPart(mimeBodyPart);
            }

            toAddress[0] = new InternetAddress(toAddr);
            message.setRecipients(Message.RecipientType.TO, toAddress);
            message.setSubject(subjectLine);
            message.setSentDate(new Date());
            message.setContent(multipart);
            message.saveChanges();

            // To send email using AWS
            messageSent = AWSSendMail.sendMailFromAwsVersion2(message);
        } catch (MessagingException | BeansException e) {
            logger.info("Exception ", e);
        }
        if (messageSent) {
            logger.info("Mail Sent to " + toolName + "  " + toAddr);
        } else {
            logger.info("Mail sending Failed");
        }
    }
    
     public static SendEmailOptions sendEmailToUser(SendEmailOptions sendEmailOptions) {
        boolean isEmailSended = Boolean.FALSE;
        String errorMessage = null;
        if (sendEmailOptions != null) {
            Message message = null;
            MimeMultipart multipart;
            Properties properties = null;
            Session session = null;
            MimeBodyPart bodyPart = null;
            List<String> addressList = null;
            try {
                /*Getting system properties*/
                properties = new Properties();

                /* Setting up mail server */
                properties.put("mail.host", "localhost");

                /* creating session object to get properties */
                session = Session.getDefaultInstance(properties, null);

                /*MimeMessage object.*/
                message = new MimeMessage(session);

                /* Set From Field: adding senders email to from field. */
                if (sendEmailOptions.getFromAddress() != null && !sendEmailOptions.getFromAddress().isEmpty()) {
                    try {
                        message.setFrom(new InternetAddress(sendEmailOptions.getFromAddress()));
                    } catch (AddressException ex) {
                        errorMessage = "Invalid From-Address email:: " + sendEmailOptions.getFromAddress();
                        logger.error("Invalid from address email and email:: " + sendEmailOptions.getFromAddress());
                    }
                } else {
                    errorMessage = "Email not found for From-Address";
                }

                /*Set To Field: adding recipient's email to from field.*/
                if (sendEmailOptions.getToAddressList() != null && !sendEmailOptions.getToAddressList().isEmpty()) {
                    addressList = Arrays.asList(sendEmailOptions.getToAddressList().split(","));
                    InternetAddress[] toAddr = addressList.stream()
                            .map(email -> {
                                try {
                                    return new InternetAddress(email);
                                } catch (AddressException ex) {
                                    logger.error("Invalid To-Address email and email:: " + sendEmailOptions.getFromAddress());
                                    return null;
                                }
                            }).filter(a -> a != null).toArray(size -> new InternetAddress[size]);
                    message.setRecipients(Message.RecipientType.TO, toAddr);
                } else {
                    errorMessage = errorMessage != null ? errorMessage.concat(", Email not found for To-Address") : "Email not found for To-Address";
                }

                /* Adding CC Address */
                if (sendEmailOptions.getCcAddressList() != null && !sendEmailOptions.getCcAddressList().isEmpty()) {
                    addressList = Arrays.asList(sendEmailOptions.getCcAddressList().split(","));
                    InternetAddress[] ccAddr = addressList.stream()
                            .map(email -> {
                                try {
                                    return new InternetAddress(email);
                                } catch (AddressException ex) {
                                    logger.error("Invalid CC-Address email and email:: " + sendEmailOptions.getFromAddress());
                                    return null;
                                }
                            }).filter(a -> a != null).toArray(size -> new InternetAddress[size]);
                    message.setRecipients(Message.RecipientType.CC, ccAddr);
                    sendEmailOptions.setCcAddressList(Common.getConcatedStringFromList(addressList, ","));
                }

                /*  Adding BCC Address */
                if (sendEmailOptions.getBccAddressList() != null && !sendEmailOptions.getBccAddressList().isEmpty()) {
                    addressList = Arrays.asList(sendEmailOptions.getCcAddressList().split(","));
                    InternetAddress[] bccAddr = addressList.stream()
                            .map(email -> {
                                try {
                                    return new InternetAddress(email);
                                } catch (AddressException ex) {
                                    logger.error("Invalid BCC-Address email and email:: " + sendEmailOptions.getFromAddress());
                                    return null;
                                }
                            }).filter(a -> a != null).toArray(size -> new InternetAddress[size]);
                    message.setRecipients(Message.RecipientType.BCC, bccAddr);
                    sendEmailOptions.setBccAddressList(Common.getConcatedStringFromList(addressList, ","));
                }

                /* Set Subject: subject of the email */
                if (sendEmailOptions.getSubject() != null && !sendEmailOptions.getSubject().isEmpty()) {
                    message.setSubject(sendEmailOptions.getSubject());
                    message.setSentDate(new Date());
                } else {
                    errorMessage = errorMessage != null ? errorMessage.concat(", Subject not found") : "Subject not found";
                }

                multipart = new MimeMultipart();
                multipart.setSubType("related");
                /*creating first MimeBodyPart object */
                if (sendEmailOptions.getBodyText() != null && !sendEmailOptions.getBodyText().isEmpty()) {
                    bodyPart = new MimeBodyPart();
                    bodyPart.setContent(sendEmailOptions.getBodyText(), "text/html;charset=utf-8");
                    multipart.addBodyPart(bodyPart);
                } else {
                    errorMessage = errorMessage != null ? errorMessage.concat(", Body Text not found") : "Body Text not found";
                }
                /* creating second MimeBodyPart object 
                i.e Attachments */
                File fileObj = null;
                List<String> fileList = null;
                if (sendEmailOptions.getAttachmentsFilesPath() != null && !sendEmailOptions.getAttachmentsFilesPath().isEmpty()) {
                    for (Map.Entry<String, String> attachEntity : sendEmailOptions.getAttachmentsFilesPath().entrySet()) {
                        fileList = Arrays.asList(attachEntity.getValue().split(","));
                        for (String fileName : fileList) {
                            if (fileName != null) {
                                /*Checking maximum size if  excedds compress it*/
                                if (attachEntity.getKey() != null) {
                                    fileObj = new File(attachEntity.getKey() + fileName);
                                    if (fileObj.length() > Common.MAX_FILE_SIZE) {
                                        fileName = Common.compressFile(attachEntity.getKey(), fileName, Boolean.TRUE);
                                    }
                                }
                                /*Attaching file to body part*/
//                                Path pathObj = Paths.get(fileName);
                                bodyPart = new MimeBodyPart();
                                bodyPart.setDataHandler(new DataHandler(new FileDataSource(attachEntity.getKey().concat(fileName))));
                                bodyPart.setFileName(fileName);
                                multipart.addBodyPart(bodyPart);
                            }
                        }
                    }
                }
                message.setSentDate(new Date());
                message.setContent(multipart);
                message.saveChanges();

                // To send email using AWS
                isEmailSended = AWSSendMail.sendMailFromAwsVersion2(message);
                sendEmailOptions.setMailDelivered(isEmailSended);
                if (isEmailSended) {
                    sendEmailOptions.setDeliveredAt(LocalDateTime.now());
                }
            } catch (MessagingException e) {
                errorMessage = errorMessage != null ? errorMessage.concat(", and cause for messaging exception :: " + e.getLocalizedMessage() + " ") : "Cause for messaging exception :: " + e.getLocalizedMessage();
                logger.error("MessagingException:: Mail sending failed, exception in sending email to user: " + sendEmailOptions.getToAddressList() + ", cause:: ", e);
            } catch (Exception e) {
                errorMessage = errorMessage != null ? errorMessage.concat(", and cause for exception :: " + e.getLocalizedMessage() + " ") : "Cause for exception :: " + e.getLocalizedMessage();
                logger.error("Exception:: Mail sending failed, exception in sending email to user: " + sendEmailOptions.getToAddressList() + ", cause:: ", e);
            }
            sendEmailOptions.setErrorMessage(errorMessage);
        }
        if (isEmailSended) {
            logger.info("Mail sent Successfully to " + (sendEmailOptions != null ? sendEmailOptions.getToAddressList() : ""));
        } else {
            logger.info("Sending mail is failed to " + (sendEmailOptions != null ? sendEmailOptions.getToAddressList() : ""));
        }
        return sendEmailOptions;
    }
}
