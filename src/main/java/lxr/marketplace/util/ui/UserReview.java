package lxr.marketplace.util.ui;

public class UserReview {
private String username;
private String feedbackdate;
private String title;
private String comments;
double rating;
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getFeedbackdate() {
	return feedbackdate;
}
public void setFeedbackdate(String date) {
	this.feedbackdate = date;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getComments() {
	return comments;
}
public void setComments(String comments) {
	this.comments = comments;
}
public double getRating() {
	return rating;
}
public void setRating(double rating) {
	this.rating = rating;
}

}
