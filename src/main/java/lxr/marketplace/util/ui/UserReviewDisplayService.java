package lxr.marketplace.util.ui;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class UserReviewDisplayService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(UserReviewDisplayService.class);

    public void getUserReviewXml(HttpServletResponse response, long toolid, HttpSession session) {
        String query = "select sum(if(rating  between 0.0 and  1.0,1,0)) as 'case1', "
                + "ifnull(sum(if(rating  between 1.1 and  2.0,1,0)),0) as 'case2', "
                + "ifnull(sum(if(rating between  2.1 and 3.0,1,0)),0) as 'case3', "
                + "ifnull(sum(if(rating between  3.1 and 4.0,1,0)),0) as 'case4' , "
                + "ifnull(sum(if(rating >= 4.1 ,1,0)),0) as 'case5' "
                + "from feedback  where tool_id= " + toolid;

        long[] data = getData(5, query);
        long total = 0;
        for (long i : data) {
            total += i;
        }
        addInJson(total, data, response);
        session.setAttribute("totalReviewedUsers", total);
    }

    public void getTotalReviewCount(long toolid, HttpSession session) {
        String query = "select sum(if(rating  between 0.0 and  1.0,1,0)) as 'case1', "
                + "ifnull(sum(if(rating  between 1.1 and  2.0,1,0)),0) as 'case2', "
                + "ifnull(sum(if(rating between  2.1 and 3.0,1,0)),0) as 'case3', "
                + "ifnull(sum(if(rating between  3.1 and 4.0,1,0)),0) as 'case4' , "
                + "ifnull(sum(if(rating >= 4.1 ,1,0)),0) as 'case5' "
                + "from feedback  where tool_id= " + toolid;

        long[] data = getData(5, query);
        long total = 0;
        for (long i : data) {
            total += i;
        }
        session.setAttribute("totalReviewedUsers", total);
    }

    public void getUserReviewsbyTool(HttpServletResponse response, int toolid, boolean all) {
        String limit = "";
        if (!all) {
            limit = "LIMIT 4";
        } else {
            limit = "LIMIT 10";
        }
        String query = "select name, a.feedback_date, a.title, a.comments, a.rating  from user , "
                + "(select   user_id, Date_Format(feedback_date, '%M %d, %Y') as feedback_date, title , comments, rating from feedback where "
                + "((title is not null and title <> '') or (comments is not null and comments <> ''))  and tool_id= " + toolid + " order by id desc, rating desc " + limit + ") a where a.user_id = id ";
        try {
            List<UserReview> userrev = getJdbcTemplate().query(query, new RowMapper<UserReview>() {
                public UserReview mapRow(ResultSet rs, int rowNum) throws SQLException {

                    UserReview ur = new UserReview();
                    if (rs.getString(1) != null) {
                        ur.setUsername(rs.getString(1));
                    } else {
                        ur.setUsername("Guest User");
                    }
                    ur.setFeedbackdate(rs.getString(2));
                    ur.setTitle(rs.getString(3));
                    ur.setComments(rs.getString(4));
                    ur.setRating(rs.getDouble(5));
                    return ur;
                }
            });

            addInJson1(userrev, response);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }

        addInJson1(null, response);
    }

    public void addInJson1(List<UserReview> data1, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(0, data1);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public void addInJson(long total, long[] Data, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, total);
            arr.add(1, Data);
            writer.print(arr);
		} catch (Exception e) {
                    logger.error(e.getMessage());
                    logger.error(e.getCause());
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    writer.print("{success: false}");
        }finally {
                    writer.flush();
                    writer.close();
        }
	}
        	
		public long[] getData(int noColumn, String query){
			long [] data = new long [noColumn];
			
			List b =  getJdbcTemplate().queryForList(query);
			
			String a = ((Object) b.get(0)).toString();
			logger.debug(a);
			int fromIndex=0, count = 0,endIndex = 0;
			String val;
			while(true && count < noColumn && endIndex >= 0){
				fromIndex = a.indexOf('=', fromIndex);
				endIndex = a.indexOf(',', fromIndex);
				if(fromIndex>=0 && endIndex >= 0){
					val = a.substring(++fromIndex, endIndex);
					data[count++] = Long.parseLong(val.equals("null")?"0":val);
				}else{
					val = a.substring(++fromIndex, a.length()-1);
					data[count++] = Long.parseLong(val.equals("null")?"0":val);
				}
				
			}		
			return data;		
		  
		}
		
public long getTotalReviewCountHome(long toolid){
                
            try{
                
		 String query="select sum(if(rating  between 0.0 and  1.0,1,0)) as 'case1', " +
			        "ifnull(sum(if(rating  between 1.1 and  2.0,1,0)),0) as 'case2', " +
			        "ifnull(sum(if(rating between  2.1 and 3.0,1,0)),0) as 'case3', " +
			        "ifnull(sum(if(rating between  3.1 and 4.0,1,0)),0) as 'case4' , " +
			        "ifnull(sum(if(rating >= 4.1 ,1,0)),0) as 'case5' " +
			        "from feedback  where tool_id= " +toolid;
		         long[] data = getData(5, query);
                           long total=0;
		         for(long i :data){
		        	 total+=i;
				 }
                          return total;
            }catch(Exception e){
                logger.error("Exception in getTotalReviewCountHome rating for tool in UserReviewDisplayService class in package"+e.getMessage());
            }
            return 0;
	}
}
