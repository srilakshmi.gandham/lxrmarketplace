package lxr.marketplace.util.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

public class UserReviewDisplayController extends SimpleFormController {

    UserReviewDisplayService userReviewDisplayService;

    public void setUserReviewDisplayService(UserReviewDisplayService userReviewDisplayService) {
        this.userReviewDisplayService = userReviewDisplayService;
    }

    public UserReviewDisplayController() {
        setCommandClass(UserReview.class);
        setCommandName("userreview");
    }

    private static Logger logger = Logger.getLogger(UserReviewDisplayController.class);

    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession();
        int toolId;
        if (WebUtils.hasSubmitParameter(request, "userreview")) {
            String review = request.getParameter("userreview");
            toolId = Integer.parseInt(request.getParameter("toolId"));
            if (review.equals("1")) {
                userReviewDisplayService.getUserReviewsbyTool(response, toolId, false);
                return null;
            } else if (review.equals("2")) {
                userReviewDisplayService.getUserReviewsbyTool(response, toolId, true);
                return null;
            }
        } else if (WebUtils.hasSubmitParameter(request, "toolId")) {
            toolId = Integer.parseInt(WebUtils.findParameterValue(request, "toolId"));
            userReviewDisplayService.getUserReviewXml(response, toolId, session);
            return null;

        }

        ModelAndView mAndV = new ModelAndView();
        return mAndV;

    }
}
