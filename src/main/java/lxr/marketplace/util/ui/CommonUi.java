package lxr.marketplace.util.ui;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
@Service
public class CommonUi {
    private static Logger logger = Logger.getLogger(CommonUi.class);
    private static double empty, halfrating;
    private double avgrating;
    Map<Integer, Double> toolRatings = new HashMap();
    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
    static UserReviewDisplayService userReviewDisplayService = (UserReviewDisplayService) ctx.getBean("userReviewDisplayService");

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        CommonUi.ctx = ctx;
    }
    public CommonUi() {
     
    }
    public CommonUi(HttpSession session) {
        toolRatings = (Map<Integer, Double>) session.getAttribute("toolRatings");
    }

    public CommonUi(HttpSession session, int toolid) {
        toolRatings = (Map<Integer, Double>) session.getAttribute("toolRatings");
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        String ratingstr = df.format(toolRatings.get(toolid));
        this.avgrating = Double.parseDouble(ratingstr);

        userReviewDisplayService.getTotalReviewCount(toolid, session);
    }
    
    
    public String getRating(int toolid, int type) {
        double rating = 0.0;
        if (toolRatings != null && toolRatings.get(toolid) != null) {
            rating = toolRatings.get(toolid);
        }

        return getRatingStarString(rating,toolid,type);
    }
    
    public static String getAverageRatingGraph(double toolRating,long toolId, int type){
        
        return getRatingStarString(toolRating,toolId,type);
    }
    
    public static String getRatingStarString(double toolRating,long toolId,int requestType){
        String ratingstars = "";
        halfrating = toolRating % 1;
        empty = 5 - toolRating;
        ratingstars += "<div class=\"stars-rating\">";
        for (int r = 1; r <= toolRating; r++) {
            ratingstars += "<span><img src=\"resources/images/stars/full-gray-star.png\"></span>";
//                    ratingstars += "<img src=\"images/home-image/star.png\">";
        }
        if (halfrating > 0.0 && halfrating < 0.25) {
//			ratingstars += "<span class=\"star halfstar1\"></span>";
            ratingstars += "<span><img src=\"resources/images/stars/one-third-star.png\"></span>";
        } else if (halfrating >= 0.25 && halfrating < 0.75) {
//			ratingstars += "<span class=\"star halfstar\"></span>";
            ratingstars += "<span><img src=\"resources/images/stars/half-star.png\"></span>";
        } else if (halfrating >= 0.75 && halfrating < 1) {
//			ratingstars += "<span class=\"star halfstar2\"></span>";
            ratingstars += "<span><img src=\"resources/images/stars/two-third-star.png\"></span>";
        }

        for (int e = 1; e <= empty; e++) {
//			ratingstars += "<span class=\"star emptystar\"></span>";
            ratingstars += "<span><img src=\"resources/images/stars/empty-star.png\"></span>";
        }
        ratingstars += "</div>";
        if (requestType == 1) {
            ratingstars += "<div class=\"user-cnt\"><span class=\"totalCount\">" + getTotalReviewCount(toolId) + "</span>";
            ratingstars += "<span class=\"reviewImage\"><img title=\"User Review\" id=\"reviewIcon\" src=\"images/tools/review-count.png\"></span></div>";
        }
        return ratingstars;
    }
    

    public double getAvgrating() {
        return avgrating;
    }

    public static long getTotalReviewCount(long toolId) {
        long totalCount = userReviewDisplayService.getTotalReviewCountHome(toolId);
        return totalCount;
    }

}
