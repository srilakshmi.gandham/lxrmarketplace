/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author vidyasagar.korada
 */
public class ExcellUtillReportServce {

    public static void appplyBorderToMergedCells(CellRangeAddress cellRangeAddress, Sheet sheet, CellStyleParamters styleParams) {
        switch (styleParams.getBorderType()) {
            case "all":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "top":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "bottom":
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "left":
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "right":
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "leftBottom":
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "leftTop":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "rightBottom":
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "rightTop":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            default:
                break;
        }
    }
    
     /*XSSFWorkbook*/
    public static boolean appendImageToExcellCell(Workbook workBook, Sheet sheet, short col1, int row1, short col2, int row2, String imgPath) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imgPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        int b;
        try {
            while ((b = fis.read()) != -1) {
                imgBytes.write(b);
            }
            fis.close();
        } catch (IOException e) {
            System.err.println("IOException in image convertion cause:: " + e.getMessage());
            return false;
        }
//        sheet.addMergedRegion(new CellRangeAddress(row1, row2 - 1, col1, col2 - 1));
        XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        int index = workBook.addPicture(imgBytes.toByteArray(), imgPath.endsWith(".jpg") ? XSSFWorkbook.PICTURE_TYPE_JPEG : XSSFWorkbook.PICTURE_TYPE_PNG);
        Drawing patriarch = sheet.createDrawingPatriarch();
        patriarch.createPicture(anchor, index);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
        anchor.setDx1(25);
        return true;
    }
    
     public static XSSFFont getRequeriedXSSFFont(Workbook workBook, short fontSize, boolean isBold, XSSFColor color,String fontStyle) {
        XSSFFont fontObj = (XSSFFont) workBook.createFont();
        fontObj.setBold(isBold);
        fontObj.setFontHeightInPoints(fontSize);
        if (color != null) {
            fontObj.setColor(color);
        }
        fontObj.setFontName(fontStyle);
//        fontObj.setFontName("Times New Roman");
        return fontObj;
    }
     
     public static CellStyle getRequriedXSSFCellStyle(Workbook workBook, CellStyleParamters styleParams) {
        CellStyle cellStyle = (XSSFCellStyle) workBook.createCellStyle();
        cellStyle.setFont(styleParams.getFont());
        cellStyle.setWrapText(Boolean.TRUE);
        cellStyle.setAlignment(styleParams.getHorizontalPosition());
        cellStyle.setVerticalAlignment(styleParams.getVerticalPostion());
        if (styleParams.getForeGroundColorIndex() > 0) {
            cellStyle.setFillForegroundColor(styleParams.getForeGroundColorIndex());
        }
        if (styleParams.isFillPattern()) {
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        if (styleParams.isHaveBorder()) {
            switch (styleParams.getBorderType()) {
                case "all":
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "top":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "bottom":
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "left":
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "right":
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "leftBottom":
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "leftTop":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "rightBottom":
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "rightTop":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    break;
                default:
                    break;
            }
        }
        return cellStyle;
    }
     
     
     public static int createMergedCellsAddCellValue(String cellValue, Sheet sheet, CellStyle cellStyle, int startRowCount, int endRowCount, short startCellCount, short endCellCount) {
        Row row = null;
        int cuurentCellcount = startCellCount;
        for (int rowNum = startRowCount; rowNum <= endRowCount; ++rowNum) {
            row = sheet.getRow(rowNum);
            if (row == null) {
                sheet.createRow(rowNum);
//                LOGGER.error("while check row " + rowNum + " was created");
            }
            for (int colNum = startCellCount; colNum <= endCellCount; colNum++) {
                Cell currentCell = null;
                if (row != null) {
                    currentCell = row.getCell(colNum);
                    cuurentCellcount++;
                    if (currentCell == null) {
                        currentCell = row.createCell(colNum);
//                        LOGGER.error("while check cell " + rowNum + ":" + colNum + " was created");
                    }
                    currentCell.setCellStyle(cellStyle);
                    if (colNum == startCellCount) {
                        currentCell.setCellValue(cellValue);
                    }
                }
            }
        }
        if (startCellCount != endCellCount) {
            sheet.addMergedRegion(new CellRangeAddress(startRowCount, endRowCount, startCellCount, endCellCount));
        }
        return cuurentCellcount;
    }
}
