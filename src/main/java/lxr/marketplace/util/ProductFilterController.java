package lxr.marketplace.util;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

public class ProductFilterController extends SimpleFormController {

    ToolService toolService;
    private static Logger logger = Logger.getLogger(ProductFilterController.class);

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public ProductFilterController() {
        setCommandClass(NTools.class);
        setCommandName("nTools");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession();
        if (WebUtils.hasSubmitParameter(request, "filter")) {
            String filter = WebUtils.findParameterValue(request, "filter");
            List<Tool> filteredTools = null;
            switch (filter) {
                case "all":
                    filteredTools = toolService.getTools();
                    break;
                case "sem":
                    filteredTools = toolService.getToolsByCategory(1);
                    break;
                case "seo":
                    filteredTools = toolService.getToolsByCategory(3);
                    break;
                case "cse":
                    filteredTools = toolService.getToolsByCategory(2);
                    break;
                case "mobileApps":
                    filteredTools = toolService.getToolsByCategory(6);
                    break;
                default:
                    break;
            }
            Map<Integer, Double> toolrating = (Map<Integer, Double>) session.getAttribute("toolRatings");
            addInJson(filteredTools, toolrating, response);
        }
        if (WebUtils.hasSubmitParameter(request, "param")) {

            String param = WebUtils.findParameterValue(request, "param");

            if (param.equals("seo")) {
                ModelAndView model = new ModelAndView(new RedirectView("/seo-tools.html"));
                return model;
            } else if (param.equals("sem")) {
                ModelAndView model = new ModelAndView(new RedirectView("/sem-tools.html"));
                return model;
            } else if (param.equalsIgnoreCase("E-Commerce")) {
                ModelAndView model = new ModelAndView(new RedirectView("/e-commerce-mobile-apps.html"));
                return model;
            }
            ModelAndView mAndV = new ModelAndView(getFormView(), "nTools", new NTools());
            mAndV.addObject("param", param);
            return mAndV;
        }

        ModelAndView mAndV = new ModelAndView(getFormView(), "nTools", new NTools());
        return mAndV;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        return new NTools();
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    public void addInJson(List<Tool> data1, Map<Integer, Double> data2, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        String array[] = convertToArray(data1, data2);
        try {
            writer = response.getWriter();
            arr.add(0, data1);
            arr.add(1, array);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public String[] convertToArray(List<Tool> data1, Map<Integer, Double> map) {
        String[] arr = new String[map.size()];

        for (int i = 0; i < data1.size(); i++) {
            int toolId = data1.get(i).getToolId();
            if (map.get(toolId) != null) {
                arr[i] = map.get(toolId).toString();
            }
        }
        return arr;
    }
}
