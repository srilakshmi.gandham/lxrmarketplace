/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.io.Serializable;

/**
 *
 * @author netelixir
 */
public class ToolUserInfo implements Serializable{

   
    private long toolRatedUsersCount;
    private long toolUsageCount;
    private double avgRating;
    private String avgRatingGraph;
    private String toolIssues;

    public long getToolRatedUsersCount() {
        return toolRatedUsersCount;
    }

    public void setToolRatedUsersCount(long toolRatedUsersCount) {
        this.toolRatedUsersCount = toolRatedUsersCount;
    }

    public long getToolUsageCount() {
        return toolUsageCount;
    }

    public void setToolUsageCount(long toolUsageCount) {
        this.toolUsageCount = toolUsageCount;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public String getAvgRatingGraph() {
        return avgRatingGraph;
    }

    public void setAvgRatingGraph(String avgRatingGraph) {
        this.avgRatingGraph = avgRatingGraph;
    }

    public String getToolIssues() {
        return toolIssues;
    }

    public void setToolIssues(String toolIssues) {
        this.toolIssues = toolIssues;
    }
    
}
