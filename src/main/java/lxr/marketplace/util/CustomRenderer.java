package lxr.marketplace.util;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.renderer.category.BarRenderer;

public class CustomRenderer extends BarRenderer {

    private final Paint[] colors;

    public CustomRenderer() {
        this.colors = new Paint[]{Color.red, Color.blue, Color.green, Color.yellow, Color.orange, Color.cyan, Color.magenta, Color.blue};
    }

    @Override
    public Paint getItemPaint(final int row, final int column) {

        switch (column) {
            case 0: {
                Color color = new Color(0, 172, 237);
                return color;
            }
            case 1: {
                Color color = new Color(59, 89, 182);
                return color;
            }
            case 2: {
                Color color = new Color(89, 188, 79);
                return color;
            }
            case 3: {
                Color color = new Color(196, 48, 43);
                return color;
            }
            case 4: {
                Color color = new Color(63, 114, 155);
                return color;
            }
            default:
                break;
        }
        return (this.colors[column % this.colors.length]);
    }
}
