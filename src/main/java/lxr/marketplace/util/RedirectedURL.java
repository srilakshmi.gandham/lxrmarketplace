package lxr.marketplace.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RedirectedURL {
  
    public static void main(String[] args) {
        try {
            List<ReURL> urls = readFile();
            urls.stream().forEach((url) -> {
                HttpURLConnection currentUrlConn = null;
                int responseCode = 0;
                try {
                    URL currentUrlObj = new URL(url.getS1());
                    if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                        currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                        currentUrlConn.setInstanceFollowRedirects(false);
                        currentUrlConn.setConnectTimeout(50000);
                        currentUrlConn.setReadTimeout(10000);
                        currentUrlConn.setRequestProperty("User-Agent", Common.USER_AGENT);
                        responseCode = currentUrlConn.getResponseCode();
                        if(responseCode == 302){
                            String redirectedLocation =  currentUrlConn.getHeaderField("Location");
                            url.setS2(redirectedLocation);
                            System.out.println(redirectedLocation);
                        }else{
                            url.setS2(""+responseCode);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static List<ReURL> readFile() throws IOException{
        List<ReURL> urls = new ArrayList<ReURL>(); 
        String filePath = "/home/netelixir/Desktop/urls.txt";
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        try {
            String url = br.readLine();

            while (url != null) {
                urls.add(new ReURL(url));
                url = br.readLine();
            }
        } catch(Exception e){ 
            e.printStackTrace();
        }finally {
            br.close();
        }
        return urls;
    }
    
    
}
class ReURL{
    String s1;
    String s2;
    ReURL(String s1){
        this.s1 = s1;
    }
    
    public void setS2(String s2){
        this.s2 = s2;
    }
    public String getS1(){
        return s1;
    }
    public String getS2(){
        return s2;
    }
}