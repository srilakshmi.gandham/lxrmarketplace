package lxr.marketplace.util.stopwordsutility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class InputGetter {

	public static void getText(String url, StringBuilder text, StringBuilder title){
		int noOfTime = 3, timeoutIteration = 0;
		int timeout = 60000, sllepOut = 5000;
		Connection jsoupCon;
		Document document = null;

		String userAgent ="Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)"; 
				/*
				 *Modified on oct-7 2015 
				 * "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
//		System.out.println("Content URL: " + url);
		do {
			timeoutIteration += 1;
			try {
				jsoupCon = Jsoup.connect(url);
				jsoupCon.timeout(timeout);
				jsoupCon.userAgent(userAgent);
				document = jsoupCon.get();
				break;
			} catch (IOException ex) {
//				System.out.println("Exception to get Connection 1 at iteration " + timeoutIteration);
				try {
					Thread.sleep(sllepOut * timeoutIteration);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} while (timeoutIteration < noOfTime);
//		System.out.println("TITLE: "+document.select("title").text());
//		System.out.println("\nBODY:\n"+document.body().text());
		text.append(document.select("body").text());
		Element titleE = document.select("title").first();
		title.append(titleE.text());
	}
	
	public static ArrayList<String> createStopWordList(){
		ArrayList<String> stopWords = new ArrayList<String>();
		File swFile = new File("/home/netelixir/Som/LXR Marketplace/stopwords.txt");
		try {
			FileReader swFileReader = new FileReader(swFile);
			BufferedReader swBufferedReader = new BufferedReader(swFileReader); 
			String s; 
			while((s = swBufferedReader.readLine()) != null){ 
				if(!s.trim().equals("")){
					stopWords.add(s.trim());
				}
			} 
			swFileReader.close(); 					
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stopWords;
	}
}
