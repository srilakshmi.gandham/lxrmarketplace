package lxr.marketplace.util.stopwordsutility;

import java.util.HashMap;

public class StopWordsTrieNode {
	public char letter;
	public HashMap <Character, StopWordsTrieNode> childs;
	public boolean isWord;
	
	public StopWordsTrieNode(char letter){
		this.letter = letter;
		childs = new HashMap <Character, StopWordsTrieNode>();
		isWord = false;
	}
}
