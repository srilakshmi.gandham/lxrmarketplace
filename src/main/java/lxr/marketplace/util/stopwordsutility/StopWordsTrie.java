package lxr.marketplace.util.stopwordsutility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StopWordsTrie {
	public static List<String> getMatchedStopwords(String data, List<String> stopWords){
		StopWordsTrieNode headNode = createStopWordList(stopWords);
		int startIndex = 0;
		data.replaceAll("[\\p{Z}\\s]+", " ");
		ArrayList<String> stops= new ArrayList<String>();
		
		while(startIndex < data.length()){	
			int tempIndex = data.indexOf((int)' ', startIndex);
			char[] word;
			if(tempIndex > 0){
				word = data.substring(startIndex, tempIndex).toCharArray();
				startIndex = tempIndex+1;
			}else{
				word = data.substring(startIndex).toCharArray();
				startIndex = data.length();
			}
			
			StopWordsTrieNode currNode = headNode;
			for(char ch : word){
				if(ch >= 65 && ch <= 90){
					ch = (char)(ch + 32);
				}
				currNode = currNode.childs.get(ch);
				if(currNode == null){
					break;
				}
			}
			if(currNode != null && currNode.isWord){
				String stopWord = new String(word).toLowerCase();
				if(!stops.contains(stopWord)){
					stops.add(stopWord);
				}
				
			}
		}
		
		return stops;
	}
	public static StopWordsTrieNode createStopWordListFromText(){
		StopWordsTrieNode headNode = new StopWordsTrieNode('\n');
		File swFile = new File("/home/netelixir/Som/LXR Marketplace/stopwords.txt");
		try {
			FileReader swFileReader = new FileReader(swFile);
			BufferedReader swBufferedReader = new BufferedReader(swFileReader); 
			String s; 
			while((s = swBufferedReader.readLine()) != null){ 
				
				if(!s.trim().equals("")){
					char[] word = s.toCharArray();
					StopWordsTrieNode currNode = headNode;
					for(char ch : word){
						if(currNode.childs.get(ch) == null){
							StopWordsTrieNode tempNode = new StopWordsTrieNode(ch);
							currNode.childs.put(ch, tempNode);
							currNode = tempNode;
						}else{
							currNode = currNode.childs.get(ch);
						}
					}
					currNode.isWord = true;
				}
			} 
			swFileReader.close(); 					
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return headNode;
	}
	
	public static StopWordsTrieNode createStopWordList(List<String> stopWords) {
		StopWordsTrieNode headNode = new StopWordsTrieNode('\n');
		for (String s : stopWords) {

			if (!s.trim().equals("")) {
				char[] word = s.toCharArray();
				StopWordsTrieNode currNode = headNode;
				for (char ch : word) {
					if (currNode.childs.get(ch) == null) {
						StopWordsTrieNode tempNode = new StopWordsTrieNode(ch);
						currNode.childs.put(ch, tempNode);
						currNode = tempNode;
					} else {
						currNode = currNode.childs.get(ch);
					}
				}
				currNode.isWord = true;
			}
		}
		return headNode;
	}
	
	
	public void printStopWordsFromTrie(StopWordsTrieNode headNode){
		
		if(headNode.isWord){
//			System.out.println(headNode.letter);
		}else{
//			System.out.print(headNode.letter);
		}
		Iterator<Entry<Character, StopWordsTrieNode>> it = headNode.childs.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Character, StopWordsTrieNode> pairs = (Map.Entry<Character, StopWordsTrieNode>)it.next();
	        StopWordsTrieNode childNode = (StopWordsTrieNode)pairs.getValue();
	        printStopWordsFromTrie(childNode);
	    }
	}
	public String removeSpecialCharacters(String data){
		data = data.replaceAll("([\\p{Z}\\s]*)[^a-zA-Z0-9]*([\\p{Z}\\s]+)", " ");
//		System.out.println("Data after removing special characters:\n"+data);
		return data;
	}
	public String removeStopWords(String data, StopWordsTrieNode headNode){
		int startIndex = 0;
		data.replaceAll("[\\p{Z}\\s]+", " ");
		StringBuffer sbf = new StringBuffer();
		
		while(startIndex < data.length()){	
			int tempIndex = data.indexOf((int)' ', startIndex);
			char[] word;
			if(tempIndex > 0){
				word = data.substring(startIndex, tempIndex).toCharArray();
				startIndex = tempIndex+1;
			}else{
				word = data.substring(startIndex).toCharArray();
				startIndex = data.length();
			}
			
			StopWordsTrieNode currNode = headNode;
			for(char ch : word){
				if(ch >= 65 && ch <= 90){
					ch = (char)(ch + 32);
				}
				currNode = currNode.childs.get(ch);
				if(currNode == null){
					break;
				}
			}
			if(currNode == null || !currNode.isWord){
				sbf.append(word);
				sbf.append(' ');
			}
		}
		
		return sbf.toString();
	}
}
