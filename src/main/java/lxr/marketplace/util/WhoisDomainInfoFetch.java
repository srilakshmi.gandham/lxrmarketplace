package lxr.marketplace.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lxr.marketplace.apiaccess.lxrmbeans.WhoIsInfo;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class WhoisDomainInfoFetch {

    private static Logger logger = Logger.getLogger(WhoisDomainInfoFetch.class);
    public static WhoIsInfo getRoboWhois(String domainName) {

        String finalCreatedDt = null, finalUpdatedDt = null, finalExpiresDt = null;
        String domainAgeYrsAndMnths = null;
        JSONObject administrativeContactInfo = null, technicalContactInfo = null, registrantInfo = null;
        String createdDate = null;
        Date createdDt = null, updatedDt = null;
        WhoIsInfo whoisInfo = new WhoIsInfo();
        
        JSONObject jsonObject = getJsonRoboWhois(domainName);
        whoisInfo.setDomainName(domainName);
        if (jsonObject != null) {
            try {
                JSONObject domainInfo = jsonObject.getJSONObject("WhoisRecord");
                /*logger.info(domainInfo);*/

                if (domainInfo != null && !domainInfo.equals("")) {
                    JSONObject registryData = getWhoisObj(domainInfo, "registryData");
                    registrantInfo = getWhoisObj(domainInfo, "registrant");
                    if (registrantInfo != null) {
                        administrativeContactInfo = getWhoisObj(domainInfo, "administrativeContact");
                        technicalContactInfo = getWhoisObj(domainInfo, "technicalContact");
                    } else if (registryData != null) {
                        registrantInfo = getWhoisObj(registryData, "registrant");
                        administrativeContactInfo = getWhoisObj(registryData, "administrativeContact");
                        technicalContactInfo = getWhoisObj(registryData, "technicalContact");
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT' yyyy");
                    SimpleDateFormat newSDF = new SimpleDateFormat("MMMM dd, yyyy");
                    SimpleDateFormat sdf5 = new SimpleDateFormat("dd/MM/yyyy");
                    whoisInfo.setRegisteredDomName(getWhoisParam(domainInfo, "domainName"));
                    if (registryData != null) {
                        try {
                            createdDate = getWhoisParam(registryData, "createdDate");
                            if (!createdDate.trim().equals("")) {
                                createdDt = parseDate(createdDate, sdf);
                                if (createdDt == null) {
                                    createdDt = parseDate(createdDate, sdf1);
                                    if (createdDt == null) {
                                        createdDt = parseDate(createdDate, sdf2);
                                    }
                                    if (createdDt == null) {
                                        createdDt = parseDate(createdDate, sdf3);
                                    }
                                    if (createdDt == null) {
                                        createdDt = parseDate(createdDate, sdf5);
                                    }
                                }
                                if (createdDt != null) {
                                    Calendar createdCal = Calendar.getInstance();
                                    createdCal.setTime(createdDt);
                                    try {
                                        finalCreatedDt = newSDF.format(createdDt);
                                    } catch (Exception e) {
                                        finalCreatedDt = sdf.format(createdDt);
                                    }
                                    domainAgeYrsAndMnths = Common.getDateDiffInYrsAndMnts(createdCal);

                                } else {
                                    finalCreatedDt = createdDate;
                                    String domainAge = getWhoisParam(domainInfo, "estimatedDomainAge");
                                    int num = Integer.parseInt(domainAge);
                                    int months = num / 30;
                                    int years = months / 12;
                                    int remainingmonthss = months % 12;
//											logger.info(years+" Years  "+remainingmonthss+"months");
                                    domainAgeYrsAndMnths = years + " Years  " + remainingmonthss + " Months";
                                }
                            }
                            String updatedDate = getWhoisParam(registryData, "updatedDate");
                            String expiresDate = getWhoisParam(registryData, "expiresDate");
                            if (!updatedDate.trim().equals("")) {
                                updatedDt = parseDate(updatedDate, sdf);
                                if (updatedDt == null) {
                                    updatedDt = parseDate(updatedDate, sdf1);
                                    if (updatedDt == null) {
                                        updatedDt = parseDate(updatedDate, sdf2);
                                    }
                                    if (updatedDt == null) {
                                        updatedDt = parseDate(updatedDate, sdf3);
                                    }
                                }
                                if (updatedDt != null) {
                                    try {
                                        finalUpdatedDt = newSDF.format(updatedDt);
                                    } catch (Exception e) {
                                        finalUpdatedDt = sdf.format(updatedDt);
                                    }
                                } else {
                                    finalUpdatedDt = updatedDate;
                                }

                            }
                            if (!expiresDate.trim().equals("")) {
                                Date expiresDt = parseDate(expiresDate, sdf);
                                if (expiresDt == null) {
                                    expiresDt = parseDate(expiresDate, sdf1);
                                    if (expiresDt == null) {
                                        expiresDt = parseDate(expiresDate, sdf2);
                                    }
                                    if (expiresDt == null) {
                                        expiresDt = parseDate(expiresDate, sdf3);
                                    }
                                }
                                if (expiresDt != null) {
                                    try {
                                        finalExpiresDt = newSDF.format(expiresDt);
                                    } catch (Exception e) {
                                        finalExpiresDt = sdf.format(expiresDt);
                                    }
                                } else {
                                    finalExpiresDt = expiresDate;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        whoisInfo.setDomainAge(domainAgeYrsAndMnths);
                        whoisInfo.setCreatedDate(finalCreatedDt);
                        whoisInfo.setUpdatedDate(finalUpdatedDt);
                        whoisInfo.setRegExpiration(finalExpiresDt);
                        whoisInfo.setRegistererName(getWhoisParam(registryData, "registrarName"));
                        whoisInfo.setEmailOfRegisterer(getWhoisParam(registryData, "contactEmail"));
                    }
                    // Registrant Details
                    if (registrantInfo != null) {
                        whoisInfo.setRegistrantName(getWhoisParam(registrantInfo, "name"));
                        whoisInfo.setRegistrantOrganization(getWhoisParam(registrantInfo, "organization"));
                        whoisInfo.setRegistrantStreet(getWhoisParam(registrantInfo, "street1"));
                        whoisInfo.setRegistrantCity(getWhoisParam(registrantInfo, "city"));
                        whoisInfo.setRegistrantState(getWhoisParam(registrantInfo, "state"));
                        whoisInfo.setRegistrantPostalCode(getWhoisParam(registrantInfo, "postalCode"));
                        whoisInfo.setRegistrantCountry(getWhoisParam(registrantInfo, "country"));
                        whoisInfo.setRegistrantEmail(getWhoisParam(registrantInfo, "email"));
                        whoisInfo.setRegistrantPhone(getWhoisParam(registrantInfo, "telephone"));
                    } else if (registrantInfo == null) {
                        whoisInfo.setRegistrantName("NA");
                        whoisInfo.setRegistrantOrganization("NA");
                        whoisInfo.setRegistrantStreet("NA");
                        whoisInfo.setRegistrantCity("NA");
                        whoisInfo.setRegistrantState("NA");
                        whoisInfo.setRegistrantPostalCode("NA");
                        whoisInfo.setRegistrantCountry("NA");
                        whoisInfo.setRegistrantEmail("NA");
                        whoisInfo.setRegistrantPhone("NA");
                    }
                    //Administrative Contact Details
                    if (administrativeContactInfo != null) {
                        whoisInfo.setAdminName(getWhoisParam(administrativeContactInfo, "name"));
                        whoisInfo.setAdminOrganization(getWhoisParam(administrativeContactInfo, "organization"));
                        whoisInfo.setAdminStreet(getWhoisParam(administrativeContactInfo, "street1"));
                        whoisInfo.setAdminCity(getWhoisParam(administrativeContactInfo, "city"));
                        whoisInfo.setAdminState(getWhoisParam(administrativeContactInfo, "state"));
                        whoisInfo.setAdminPostalCode(getWhoisParam(administrativeContactInfo, "postalCode"));
                        whoisInfo.setAdminCountry(getWhoisParam(administrativeContactInfo, "country"));
                        whoisInfo.setAdminEmail(getWhoisParam(administrativeContactInfo, "email"));
                        whoisInfo.setAdminPhone(getWhoisParam(administrativeContactInfo, "telephone"));
                    } else if (administrativeContactInfo == null) {
                        whoisInfo.setAdminName("NA");
                        whoisInfo.setAdminOrganization("NA");
                        whoisInfo.setAdminStreet("NA");
                        whoisInfo.setAdminCity("NA");
                        whoisInfo.setAdminState("NA");
                        whoisInfo.setAdminPostalCode("NA");
                        whoisInfo.setAdminCountry("NA");
                        whoisInfo.setAdminEmail("NA");
                        whoisInfo.setAdminPhone("NA");
                    }

                    //Technical Contact Details
                    if (technicalContactInfo != null) {
                        whoisInfo.setTechName(getWhoisParam(technicalContactInfo, "name"));
                        whoisInfo.setTechOrganization(getWhoisParam(technicalContactInfo, "organization"));
                        whoisInfo.setTechStreet(getWhoisParam(technicalContactInfo, "street1"));
                        whoisInfo.setTechCity(getWhoisParam(technicalContactInfo, "city"));
                        whoisInfo.setTechState(getWhoisParam(technicalContactInfo, "state"));
                        whoisInfo.setTechPostalCode(getWhoisParam(technicalContactInfo, "postalCode"));
                        whoisInfo.setTechCountry(getWhoisParam(technicalContactInfo, "country"));
                        whoisInfo.setTechEmail(getWhoisParam(technicalContactInfo, "email"));
                        whoisInfo.setTechPhone(getWhoisParam(technicalContactInfo, "telephone"));
                    } else if (technicalContactInfo == null) {
                        whoisInfo.setTechName("NA");
                        whoisInfo.setTechOrganization("NA");
                        whoisInfo.setTechStreet("NA");
                        whoisInfo.setTechCity("NA");
                        whoisInfo.setTechState("NA");
                        whoisInfo.setTechPostalCode("NA");
                        whoisInfo.setTechCountry("NA");
                        whoisInfo.setTechEmail("NA");
                        whoisInfo.setTechPhone("NA");
                    }
                }
            } catch (NumberFormatException ex) {
                logger.error("NumberFormatException in getRoboWhois" + ex.getMessage());
            } catch (Exception e) {
                logger.error("Excpetion in getRoboWhois" + e.getMessage());
            }
        }
        return whoisInfo;

    }

    public static JSONObject getJsonRoboWhois(String domainName) {
        
        String whoisUrl = "http://www.whoisxmlapi.com/whoisserver/WhoisService" ;
        String API_KEY="at_5evVhSX8Jnhv1iqH135Z53LzenX8L";
        String outputFormat = "json";
//        String queryUrl = whoisUrl + "?domainName="+domainName+"&username="+whoisAPIUserName+"&password="
//                           +whoisAPIPassword+"&outputFormat="+outputFormat;
           String queryUrl = whoisUrl + "?apiKey="+API_KEY+"&domainName="+domainName+"&outputFormat="+outputFormat ;
        /*logger.info("Fetching  data for url: "+domainName +"\nAhref  query: " + queryUrl);*/
        JSONObject jsonObject = Common.createJsonObjectFromURL(queryUrl);
        
        return jsonObject;
    }
    
    public static String getWhoisParam(JSONObject infoObj, String paramName) {
        String paramVal = null;
        try {
            paramVal = infoObj.getString(paramName);
        } catch (JSONException e) {
            logger.error("JSONException in getRoboWhois for" + e.getMessage() + "For paramName" + paramName);
        } catch (Exception e) {
            logger.error("Exception in getRoboWhois for" + e.getMessage() + "For paramName" + paramName);
        }
        if (paramVal == null) {
            paramVal = "NA";
        }
        return paramVal;
    }

    public static JSONObject getWhoisObj(JSONObject infoObj, String paramName) {
        JSONObject objName = null;
        try {
            objName = infoObj.getJSONObject(paramName);
        } catch (JSONException e) {
            logger.error("JSONException in getWhoisObj for" + e.getMessage() + "For paramName" + paramName);
        } catch (Exception e) {
            logger.error("Exception in getWhoisObj for" + e.getMessage() + "For paramName" + paramName);
        }
        return objName;
    }

    public static Date parseDate(String dateVal, SimpleDateFormat sdf) {
        Date parsedDateVal = null;
        try {
            parsedDateVal = sdf.parse(dateVal);
        } catch (ParseException e) {
            logger.error("ParseException in parseDate for" + e.getMessage() + "For dateVal" + dateVal);
        } catch (Exception e) {
            logger.error("Exception in parseDate for" + e.getMessage() + "For dateVal" + dateVal);
        }
        return parsedDateVal;
    }
}
