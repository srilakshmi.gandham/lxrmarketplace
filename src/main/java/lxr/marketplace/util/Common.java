package lxr.marketplace.util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.bitwalker.useragentutils.Browser;
import nl.bitwalker.useragentutils.OperatingSystem;
import nl.bitwalker.useragentutils.UserAgent;
import nl.bitwalker.useragentutils.Version;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import crawlercommons.robots.BaseRobotRules;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import lxr.marketplace.admin.automail.EmailTemplate;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.admin.automail.EmailTrackingInfoService;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.keywordcombination.domain.Keyword;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.SessionService;
import lxr.marketplace.sitemapbuilder.SiteMapBuilderService;
import lxr.marketplace.sitemapbuilder.SiteMapBuilderThread;
import lxr.marketplace.sitemapbuilder.SitemapUrl;
import lxr.marketplace.user.CookieService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.findPhrase.PhraseStat;
import lxr.marketplace.util.findPhrase.VariableLengthPhraseFinder;
import lxr.marketplace.util.search.BoyerMooreSearch;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSession;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.client.RestTemplate;

public final class Common {

    private static Logger logger = Logger.getLogger(Common.class);

    public static final String ROI_CALCULATOR = "ROI Calculator";
    public static final String KEYWORD_COMBINATOR = "Keyword Combinator";
    public static final String URL_AUDITOR = "URL Auditor";
    public static final String KEYWORD_ANALYZER = "Keyword Performance Analyzer";
    public static final String AMAZON_PRICE_ANALYZER = "Amazon Price Analyzer";
    public static final String SITE_MAP_BUILDER = "Site Map Buildet";
    public static final String COMPETITOR_ANALYSIS = "Competitor Analysis";
    public static final String SEO_WEBPAGE_ANALYSIS = "SEO Webpage Analysis";
    public static final String COMPETITOR_WEBPAGE_MONITOR = "Competitor Webpage Monitor";
    public static final String SITE_GRADER = "Site Grader";
    public static final String KEYWORD_RANK_CHECKER = "SEO Rank Checker ";
    public static final String INBOUND_LINK_CHECKER = "Inbound Link Checker";
    public static final String DOMAIN_AGE_CHECKER = "Domain Age Checker ";
    public static final String ROBOTS_TXT_GENERATOR = "Robots.txt Generator";
    public static final String ROBOTS_TXT_VALIDATOR = "Robots.txt ValidatOor";
    public static final String META_TAG_GENERATOR = "MetaTag Generator";
    public static final String DNS_LOOKUP = "DNS Lookup";
    public static final String SOCIAL_MEDIA_ANALYZER = "Social Media Analyzer";
    public static final String TOP_RANKED_WEBSITES_BY_KEYWORD = "Top Ranked Websites by Keyword";
    public static final String BROKEN_LINK_CHECKER = "Broken Link Checker";
    public static final String MOST_USED_KEYWORDS = "Most Used Keywords";
    public static final String MOST_USED_KEYWORDS_DENSITY = "KeywordsDensity";
    public static final String PAGE_SPEED_INSIGHTS = "PageSpeed Insights";
    public static final String SEO_PENALTY_CHECKER = "SEO Penalty Checker";
    public static final String STRUCTURED_DATA_GENERATOR = "Structure Data Generator";
    public static final String MICRO_INFLUENCER_GENERATOR = "Micro Influencer Generator";
    public static final String AMAZON_PRODUCT_LISTING_GRADER = "Amazon Product Listing Grader";
    public static final String HREFLANG_GENERATOR_CHECKER = "Hreflang Generator and Checker";
    public static final String IMAGE_COMPRESSION_TOOL = "Image Compression Tool";
    public static int ADVISOR_TOOL_LIMIT = 50;
    public static int BROKEN_LINK_CHECKER_TOOL_LIMIT = 1000;
    public static final int BACK_LINK_COUNT = 100;
    public static int TOOL_ACCESS_LIMIT = 100;
    public static int LOCAL_USER_TOOL_LIMIT = 100000;
    public static final String SITE_OTHER = "Site/Others";
    public static final long MAX_FILE_SIZE = 2097152;

    public static final int CONNECTION_TIME_OUT = 20000;
    public static final int SOCKET_TIME_OUT = 20000;
    public static final int IMPORTANT_READ_TIME_OUT = 25000;
    public static final int IMPORTANT_CONNECT_TIME_OUT = 30000;
    public static final int VERIFICATION_CODE_LENGTH = 6;
    public static final int READ_TIME_OUT = 8000;
    public static final int CONNECT_TIME_OUT = 15000;
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#,##,###,##0.00");

    public static String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
//    public static  String USER_AGENT = "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";
//    static final String USER_AGENT_MOBILE = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
    static EncryptAndDecryptUsingDES encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();

    private static final String PROXY_USERNAME = "netelixir";
    private static final String PROXY_PASSWORD = "netelixir16";
    private static final String PROXY_HOST_1 = "us-dc.proxymesh.com";
    private static final String PROXY_HOST_2 = "us-fl.proxymesh.com";
    private static final int PROXY_PORT = 31280;

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        Common.ctx = ctx;
    }

    public static String[] cartesianJoin(String[] one, String[] two) {
        String[] stringList = new String[0];
        for (String two1 : two) {
            stringList = join(stringList, concatSuffix(one, two1));
        }
        return stringList;
    }

    public static String[] concatSuffix(String[] one, String suffix) {
        String[] list = new String[one.length];
        for (int i = 0; i < one.length; i++) {
            list[i] = one[i] + ' ' + suffix;
        }
        return list;
    }

    public static String[] concatPrefix(String[] one, String prefix) {
        String[] list = new String[one.length];
        for (int i = 0; i < one.length; i++) {
            list[i] = prefix + ' ' + one[i];
        }
        return list;
    }

    public static String[] join(String[]... arrays) {
        // calculate size of target array
        int size = 0;
        for (String[] array : arrays) {
            size += array.length;
        }
        // create list of appropriate size
        java.util.List list = new java.util.ArrayList(size);
        // add arrays
        for (String[] array : arrays) {
            list.addAll(java.util.Arrays.asList(array));
        }
        // create and return final array
        return (String[]) list.toArray(new String[size]);

    }

    public static ArrayList<String> convertStringToArrayList(String string) {
        return convertStringToArrayList(string, "[;\\n]");
    }

    public static ArrayList<String> convertStringToArrayList(String string,
            String separator) {
        ArrayList<String> keywordList = new ArrayList<>();
//        Set<String> keywordList = new LinkedHashSet();
        if (string != null && string.length() > 0 && !string.trim().equals("")) {
            Pattern p = Pattern.compile(separator);
            String[] list = p.split(string);
            for (String list1 : list) {
                if (list1 != null && list1.trim().length() > 0) {
                    keywordList.add(list1.replaceAll("\\s", "").replaceAll(",", "").trim());
                }
            }
        }
//        ArrayList<String> filteredList = new ArrayList<>();
//        filteredList.addAll(keywordList.stream().collect(Collectors.toList()));
        return keywordList;
    }

    public static ArrayList<String> convertStringToArrayListV2(String string, String separator) {
        ArrayList<String> keywordList = new ArrayList<>();
        if (string != null && string.length() > 0 && !string.trim().equals("")) {
            Pattern p = Pattern.compile(separator);
            String[] list = p.split(string);
            for (String list1 : list) {
                if (list1 != null && list1.trim().length() > 0) {
                    keywordList.add(list1.replaceAll(",", " ").trim());
                }
            }
        }
        return keywordList;
    }

    public static String convertListToString(List<String> ListElements) {
        String finalString = "";
        finalString = ListElements.stream().map((eachString) -> eachString + "\n").reduce(finalString, String::concat);
        return finalString;
    }

    public static HashMap<String, String> loadGeoLocations() {
        HashMap<String, String> geoLocation = new LinkedHashMap<>();

        geoLocation.put("af", "Afghanistan");
        geoLocation.put("al", "Albania");
        geoLocation.put("dz", "Algeria");
        geoLocation.put("as", "American Samoa");
        geoLocation.put("ad", "Andorra");
        geoLocation.put("ao", "Angola");
        geoLocation.put("ai", "Anguilla");
        geoLocation.put("aq", "Antarctica");
        geoLocation.put("ag", "Antigua and Barbuda");
        geoLocation.put("ar", "Argentina");
        geoLocation.put("am", "Armenia");
        geoLocation.put("aw", "Aruba");
        geoLocation.put("au", "Australia");
        geoLocation.put("at", "Austria");
        geoLocation.put("az", "Azerbaijan");
        geoLocation.put("bs", "Bahamas");
        geoLocation.put("bh", "Bahrain");
        geoLocation.put("bd", "Bangladesh");
        geoLocation.put("bb", "Barbados");
        geoLocation.put("by", "Belarus");
        geoLocation.put("be", "Belgium");
        geoLocation.put("bz", "Belize");
        geoLocation.put("bj", "Benin");
        geoLocation.put("bm", "Bermuda");
        geoLocation.put("bt", "Bhutan");
        geoLocation.put("bo", "Bolivia");
        geoLocation.put("ba", "Bosnia and Herzegovina");
        geoLocation.put("bw", "Botswana");
        geoLocation.put("bv", "Bouvet Island");
        geoLocation.put("br", "Brazil");
        geoLocation.put("io", "British Indian Ocean Territory");
        geoLocation.put("bn", "Brunei Darussalam");
        geoLocation.put("bg", "Bulgaria");
        geoLocation.put("bf", "Burkina Faso");
        geoLocation.put("bi", "Burundi");
        geoLocation.put("kh", "Cambodia");
        geoLocation.put("cm", "Cameroon");
        geoLocation.put("ca", "Canada");
        geoLocation.put("cv", "Cape Verde");
        geoLocation.put("ky", "Cayman Islands");
        geoLocation.put("cf", "Central African Republic");
        geoLocation.put("td", "Chad");
        geoLocation.put("cl", "Chile");
        geoLocation.put("cn", "China");
        geoLocation.put("cx", "Christmas Island");
        geoLocation.put("cc", "Cocos (Keeling) Islands");
        geoLocation.put("co", "Colombia");
        geoLocation.put("km", "Comoros");
        geoLocation.put("cg", "Congo");
        geoLocation.put("cd", "Congo, the Democratic Republic of the");
        geoLocation.put("ck", "Cook Islands");
        geoLocation.put("cr", "Costa Rica");
        geoLocation.put("ci", "Cote D'ivoire");
        geoLocation.put("hr", "Croatia");
        geoLocation.put("cu", "Cuba");
        geoLocation.put("cy", "Cyprus");
        geoLocation.put("cz", "Czech Republic");
        geoLocation.put("dk", "Denmark");
        geoLocation.put("dj", "Djibouti");
        geoLocation.put("dm", "Dominica");
        geoLocation.put("do", "Dominican Republic");
        geoLocation.put("ec", "Ecuador");
        geoLocation.put("eg", "Egypt");
        geoLocation.put("sv", "El Salvador");
        geoLocation.put("gq", "Equatorial Guinea");
        geoLocation.put("er", "Eritrea");
        geoLocation.put("ee", "Estonia");
        geoLocation.put("et", "Ethiopia");
        geoLocation.put("fk", "Falkland Islands (Malvinas)");
        geoLocation.put("fo", "Faroe Islands");
        geoLocation.put("fj", "Fiji");
        geoLocation.put("fi", "Finland");
        geoLocation.put("fr", "France");
        geoLocation.put("gf", "French Guiana");
        geoLocation.put("pf", "French Polynesia");
        geoLocation.put("tf", "French Southern Territories");
        geoLocation.put("ga", "Gabon");
        geoLocation.put("gm", "Gambia");
        geoLocation.put("ge", "Georgia");
        geoLocation.put("de", "Germany");
        geoLocation.put("gh", "Ghana");
        geoLocation.put("gi", "Gibraltar");
        geoLocation.put("gr", "Greece");
        geoLocation.put("gl", "Greenland");
        geoLocation.put("gd", "Grenada");
        geoLocation.put("gp", "Guadeloupe");
        geoLocation.put("gu", "Guam");
        geoLocation.put("gt", "Guatemala");
        geoLocation.put("gn", "Guinea");
        geoLocation.put("gw", "Guinea-Bissau");
        geoLocation.put("gy", "Guyana");
        geoLocation.put("ht", "Haiti");
        geoLocation.put("hm", "Heard Island and Mcdonald Islands");
        geoLocation.put("va", "Holy See (Vatican City State)");
        geoLocation.put("hn", "Honduras");
        geoLocation.put("hk", "Hong Kong");
        geoLocation.put("hu", "Hungary");
        geoLocation.put("is", "Iceland");
        geoLocation.put("in", "India");
        geoLocation.put("id", "Indonesia");
        geoLocation.put("ir", "Iran, Islamic Republic of");
        geoLocation.put("iq", "Iraq");
        geoLocation.put("ie", "Ireland");
        geoLocation.put("il", "Israel");
        geoLocation.put("it", "Italy");
        geoLocation.put("jm", "Jamaica");
        geoLocation.put("jp", "Japan");
        geoLocation.put("jo", "Jordan");
        geoLocation.put("kz", "Kazakhstan");
        geoLocation.put("ke", "Kenya");
        geoLocation.put("ki", "Kiribati");
        geoLocation.put("kp", "Korea, Democratic People's Republic of");
        geoLocation.put("kr", "Korea, Republic of");
        geoLocation.put("kw", "Kuwait");
        geoLocation.put("kg", "Kyrgyzstan");
        geoLocation.put("la", "Lao People's Democratic Republic");
        geoLocation.put("lv", "Latvia");
        geoLocation.put("lb", "Lebanon");
        geoLocation.put("ls", "Lesotho");
        geoLocation.put("lr", "Liberia");
        geoLocation.put("ly", "Libyan Arab Jamahiriya");
        geoLocation.put("li", "Liechtenstein");
        geoLocation.put("lt", "Lithuania");
        geoLocation.put("lu", "Luxembourg");
        geoLocation.put("mo", "Macao");
        geoLocation.put("mk", "Macedonia, the Former Yugosalv Republic of");
        geoLocation.put("mg", "Madagascar");
        geoLocation.put("mw", "Malawi");
        geoLocation.put("my", "Malaysia");
        geoLocation.put("mv", "Maldives");
        geoLocation.put("ml", "Mali");
        geoLocation.put("mt", "Malta");
        geoLocation.put("mh", "Marshall Islands");
        geoLocation.put("mq", "Martinique");
        geoLocation.put("mr", "Mauritania");
        geoLocation.put("mu", "Mauritius");
        geoLocation.put("yt", "Mayotte");
        geoLocation.put("mx", "Mexico");
        geoLocation.put("fm", "Micronesia, Federated States of");
        geoLocation.put("md", "Moldova, Republic of");
        geoLocation.put("mc", "Monaco");
        geoLocation.put("mn", "Mongolia");
        geoLocation.put("ms", "Montserrat");
        geoLocation.put("ma", "Morocco");
        geoLocation.put("mz", "Mozambique");
        geoLocation.put("mm", "Myanmar");
        geoLocation.put("na", "Namibia");
        geoLocation.put("nr", "Nauru");
        geoLocation.put("np", "Nepal");
        geoLocation.put("nl", "Netherlands");
        geoLocation.put("an", "Netherlands Antilles");
        geoLocation.put("nc", "New Caledonia");
        geoLocation.put("nz", "New Zealand");
        geoLocation.put("ni", "Nicaragua");
        geoLocation.put("ne", "Niger");
        geoLocation.put("ng", "Nigeria");
        geoLocation.put("nu", "Niue");
        geoLocation.put("nf", "Norfolk Island");
        geoLocation.put("mp", "Northern Mariana Islands");
        geoLocation.put("no", "Norway");
        geoLocation.put("om", "Oman");
        geoLocation.put("pk", "Pakistan");
        geoLocation.put("pw", "Palau");
        geoLocation.put("ps", "Palestinian Territory, Occupied");
        geoLocation.put("pa", "Panama");
        geoLocation.put("pg", "Papua New Guinea");
        geoLocation.put("py", "Paraguay");
        geoLocation.put("pe", "Peru");
        geoLocation.put("ph", "Philippines");
        geoLocation.put("pn", "Pitcairn");
        geoLocation.put("pl", "Poland");
        geoLocation.put("pt", "Portugal");
        geoLocation.put("pr", "Puerto Rico");
        geoLocation.put("qa", "Qatar");
        geoLocation.put("re", "Reunion");
        geoLocation.put("ro", "Romania");
        geoLocation.put("ru", "Russian Federation");
        geoLocation.put("rw", "Rwanda");
        geoLocation.put("sh", "Saint Helena");
        geoLocation.put("kn", "Saint Kitts and Nevis");
        geoLocation.put("lc", "Saint Lucia");
        geoLocation.put("pm", "Saint Pierre and Miquelon");
        geoLocation.put("vc", "Saint Vincent and the Grenadines");
        geoLocation.put("ws", "Samoa");
        geoLocation.put("sm", "San Marino");
        geoLocation.put("st", "Sao Tome and Principe");
        geoLocation.put("sa", "Saudi Arabia");
        geoLocation.put("sn", "Senegal");
        geoLocation.put("cs", "Serbia and Montenegro");
        geoLocation.put("sc", "Seychelles");
        geoLocation.put("sl", "Sierra Leone");
        geoLocation.put("sg", "Singapore");
        geoLocation.put("sk", "Slovakia");
        geoLocation.put("si", "Slovenia");
        geoLocation.put("sb", "Solomon Islands");
        geoLocation.put("so", "Somalia");
        geoLocation.put("za", "South Africa");
        geoLocation.put("gs", "South Georgia and the South Sandwich Islands");
        geoLocation.put("es", "Spain");
        geoLocation.put("lk", "Sri Lanka");
        geoLocation.put("sd", "Sudan");
        geoLocation.put("sr", "Suriname");
        geoLocation.put("sj", "Svalbard and Jan Mayen");
        geoLocation.put("sz", "Swaziland");
        geoLocation.put("se", "Sweden");
        geoLocation.put("ch", "Switzerland");
        geoLocation.put("sy", "Syrian Arab Republic");
        geoLocation.put("tw", "Taiwan, Province of China");
        geoLocation.put("tj", "Tajikistan");
        geoLocation.put("tz", "Tanzania, United Republic of");
        geoLocation.put("th", "Thailand");
        geoLocation.put("tl", "Timor-Leste");
        geoLocation.put("tg", "Togo");
        geoLocation.put("tk", "Tokelau");
        geoLocation.put("to", "Tonga");
        geoLocation.put("tt", "Trinidad and Tobago");
        geoLocation.put("tn", "Tunisia");
        geoLocation.put("tr", "Turkey");
        geoLocation.put("tm", "Turkmenistan");
        geoLocation.put("tc", "Turks and Caicos Islands");
        geoLocation.put("tv", "Tuvalu");
        geoLocation.put("ug", "Uganda");
        geoLocation.put("ua", "Ukraine");
        geoLocation.put("ae", "United Arab Emirates");
        geoLocation.put("uk", "United Kingdom");
        geoLocation.put("us", "United States");
        geoLocation.put("um", "United States Minor Outlying Islands");
        geoLocation.put("uy", "Uruguay");
        geoLocation.put("uz", "Uzbekistan");
        geoLocation.put("vu", "Vanuatu");
        geoLocation.put("ve", "Venezuela");
        geoLocation.put("vn", "Viet Nam");
        geoLocation.put("vg", "Virgin Islands, British");
        geoLocation.put("vi", "Virgin Islands, US");
        geoLocation.put("wf", "Wallis and Futuna");
        geoLocation.put("eh", "Western Sahara");
        geoLocation.put("ye", "Yemen");
        geoLocation.put("zm", "Zambia");
        geoLocation.put("zw", "Zimbabwe");
        return geoLocation;

    }

    public static HashMap<String, String> loadcustomSearchLanguages() {
        HashMap<String, String> customSearchLanguages = new LinkedHashMap<>();

        customSearchLanguages.put("lang_ar", "Arabic");
        customSearchLanguages.put("lang_bg", "Bulgarian");
        customSearchLanguages.put("lang_ca", "Catalan");
        customSearchLanguages.put("lang_zh-CN", "Chinese (Simplified)");
        customSearchLanguages.put("lang_zh-TW", "Chinese (Traditional)");
        customSearchLanguages.put("lang_hr", "Croatian");
        customSearchLanguages.put("lang_cs", "Czech");
        customSearchLanguages.put("lang_da", "Danish");
        customSearchLanguages.put("lang_nl", "Dutch");
        customSearchLanguages.put("lang_en", "English");
        customSearchLanguages.put("lang_et", "Estonian");
        customSearchLanguages.put("lang_fi", "Finnish");
        customSearchLanguages.put("lang_fr", "French");
        customSearchLanguages.put("lang_de", "German");
        customSearchLanguages.put("lang_el", "Greek");
        customSearchLanguages.put("lang_iw", "Hebrew");
        customSearchLanguages.put("lang_hu", "Hungarian");
        customSearchLanguages.put("lang_is", "Icelandic");
        customSearchLanguages.put("lang_id", "Indonesian");
        customSearchLanguages.put("lang_it", "Italian");
        customSearchLanguages.put("lang_ja", "Japanese");
        customSearchLanguages.put("lang_ko", "Korean");
        customSearchLanguages.put("lang_lv", "Latvian");
        customSearchLanguages.put("lang_lt", "Lithuanian");
        customSearchLanguages.put("lang_no", "Norwegian");
        customSearchLanguages.put("lang_pl", "Polish");
        customSearchLanguages.put("lang_pt", "Portuguese");
        customSearchLanguages.put("lang_ro", "Romanian");
        customSearchLanguages.put("lang_ru", "Russian");
        customSearchLanguages.put("lang_sr", "Serbian");
        customSearchLanguages.put("lang_sk", "Slovak");
        customSearchLanguages.put("lang_sl", "Slovenian");
        customSearchLanguages.put("lang_es", "Spanish");
        customSearchLanguages.put("lang_sv", "Swedish");
        customSearchLanguages.put("lang_tr", "Turkish");

        return customSearchLanguages;
    }

    public static HashMap<String, String> loadRobotNames() {
        HashMap<String, String> robotNames = new LinkedHashMap<>();
        robotNames.put("All", "All");
        robotNames.put("NinjaBot", "NinjaBot");
        robotNames.put("Googlebot", "Googlebot");
        robotNames.put("Googlebot-Mobile", "Googlebot-Mobile");
        robotNames.put("Googlebot-Image", "Googlebot-Image");
        robotNames.put("Mediapartners-Google", "Mediapartners-Google");
        robotNames.put("Adsbot-Google", "Adsbot-Google");
        robotNames.put("Bingbot", "Bingbot");
        robotNames.put("Slurp", "Slurp");
        robotNames.put("msnbot", "msnbot");
        robotNames.put("msnbot-media", "msnbot-media");
        robotNames.put("Teoma", "Teoma");
        robotNames.put("twiceler", "twiceler");
        robotNames.put("Gigabot", "Gigabot");
        robotNames.put("Scrubby", "Scrubby");
        robotNames.put("Robozilla", "Robozilla");
        robotNames.put("ia_archiver", "ia_archiver");
        robotNames.put("baiduspider", "baiduspider");
        robotNames.put("naverbot", "naverbot");
        robotNames.put("yeti", "yeti");
        robotNames.put("yahoo-mmcrawler", "yahoo-mmcrawler");
        robotNames.put("psbot", "psbot");
        robotNames.put("asterias", "asterias");
        robotNames.put("yahoo-blogs", "yahoo-blogs");
        robotNames.put("Yandex", "Yandex");
        robotNames.put("Specify", "Specify");
        return robotNames;
    }

    public static HashMap<String, String> loadFinalBingMarkets() {
        HashMap<String, String> finalBingMarketVal = new LinkedHashMap<>();
        finalBingMarketVal.put("Arabic-Saudi Arabia", "ar-XA");
        finalBingMarketVal.put("Bulgarian-Bulgaria ", " bg-BG");
        finalBingMarketVal.put("Czech-Czech Republic", "cs-CZ");
        finalBingMarketVal.put("Danish-Denmark", "da-DK");
        finalBingMarketVal.put("German-Austria", "de-AT");
        finalBingMarketVal.put("German-Switzerland", "de-CH");
        finalBingMarketVal.put("German-Germany", "de-DE");
        finalBingMarketVal.put("Greek-Greece", "el-GR");
        finalBingMarketVal.put("English-Australia", "en-AU");
        finalBingMarketVal.put("English-Canada", "en-CA");
        finalBingMarketVal.put("English-United Kingdom", "en-GB");
        finalBingMarketVal.put("English-Indonesia", "en-ID");
        finalBingMarketVal.put("English-Ireland", "en-IE");
        finalBingMarketVal.put("English-India", "en-IN");
        finalBingMarketVal.put("English-Malaysia", "en-MY");
        finalBingMarketVal.put("English-New Zealand", "en-NZ");
        finalBingMarketVal.put("English-Philippines", "en-PH");
        finalBingMarketVal.put("English-Singapore", "en-SG");
        finalBingMarketVal.put("English-United States", "en-US");
        finalBingMarketVal.put("English-Saudi Arabia", "en-XA");
        finalBingMarketVal.put("English-South Africa", "en-ZA");
        finalBingMarketVal.put("Spanish-Argentina", "es-AR");
        finalBingMarketVal.put("Spanish-Chile", "es-CL");
        finalBingMarketVal.put("Spanish-Spain", "es-ES");
        finalBingMarketVal.put("Spanish-Mexico", "es-MX");
        finalBingMarketVal.put("Spanish-United States", "es-US");
        finalBingMarketVal.put("Estonian-Estonia", "et-EE");
        finalBingMarketVal.put("Finnish-Finland", "fi-FI");
        finalBingMarketVal.put("French-Belgium", "fr-BE");
        finalBingMarketVal.put("French-Canada", "fr-CA");
        finalBingMarketVal.put("French-Switzerland", "fr-CH");
        finalBingMarketVal.put("French-France", "fr-FR");
        finalBingMarketVal.put("Hebrew-Israel", "he-IL");
        finalBingMarketVal.put("Croatian-Croatia", "hr-HR");
        finalBingMarketVal.put("Hungarian-Hungary", "hu-HU");
        finalBingMarketVal.put("Italian-Italy", "it-IT");
        finalBingMarketVal.put("Japanese-Japan", "ja-JP");
        finalBingMarketVal.put("Korean-Korea, Democratic People's Republic of", "ko-KR");
        finalBingMarketVal.put("Lithuanian-Lithuania", "lt-LT");
        finalBingMarketVal.put("Latvian-Latvia", "lv-LV");
        finalBingMarketVal.put("Norwegian-Norway", "nb-NO");
        finalBingMarketVal.put("Dutch-Belgium", "nl-BE");
        finalBingMarketVal.put("Dutch-Netherlands", "nl-NL");
        finalBingMarketVal.put("Polish-Poland", "pl-PL");
        finalBingMarketVal.put("Portuguese-Brazil", "pt-BR");
        finalBingMarketVal.put("Portuguese-Portugal", "pt-PT");
        finalBingMarketVal.put("Romanian-Romania", "ro-RO");
        finalBingMarketVal.put("Russian-Russian Federation", "ru-RU");
        finalBingMarketVal.put("Slovak-Slovakia", "sk-SK");
        finalBingMarketVal.put("Slovenian-Slovenia", "sl-SL");
        finalBingMarketVal.put("Swedish-Sweden", "sv-SE");
        finalBingMarketVal.put("Turkish-Turkey", "tr-TR");
        finalBingMarketVal.put("Chinese (Simplified)-China ", "zh-CN");
        finalBingMarketVal.put("Chinese (Traditional)-China", "zh-CN");
        finalBingMarketVal.put("Chinese (Simplified)-Hong Kong ", "zh-HK");
        finalBingMarketVal.put("Chinese (Traditional)-Hong Kong", "zh-HK");
        finalBingMarketVal.put("Chinese (Simplified)-Taiwan, Province of China", "zh-TW");
        finalBingMarketVal.put("Chinese (Traditional)-Taiwan, Province of China", "zh-TW");
        finalBingMarketVal.put("Korean-Korea, Republic of", "ko-KR");
        finalBingMarketVal.put("Spanish-Argentina", "es-XL");
        finalBingMarketVal.put("Spanish-Bolivia", "es-XL");
        finalBingMarketVal.put("Spanish-Brazil", "es-XL");
        finalBingMarketVal.put("Spanish-Chile", "es-XL");
        finalBingMarketVal.put("Spanish-Colombia", "es-XL");
        finalBingMarketVal.put("Spanish-Costa Rica", "es-XL");
        finalBingMarketVal.put("Spanish-Cuba", "es-XL");
        finalBingMarketVal.put("Spanish-Dominican Republic", "es-XL");
        finalBingMarketVal.put("Spanish-Ecuador", "es-XL");
        finalBingMarketVal.put("Spanish-El Salvador", "es-XL");
        finalBingMarketVal.put("Spanish-Guatemala", "es-XL");
        finalBingMarketVal.put("Spanish-Haiti", "es-XL");
        finalBingMarketVal.put("Spanish-Honduras", "es-XL");
        finalBingMarketVal.put("Spanish-Mexico", "es-XL");
        finalBingMarketVal.put("Spanish-Nicaragua", "es-XL");
        finalBingMarketVal.put("Spanish-Panama", "es-XL");
        finalBingMarketVal.put("Spanish-Paraguay", "es-XL");
        finalBingMarketVal.put("Spanish-Peru", "es-XL");
        finalBingMarketVal.put("Spanish-Uruguay", "es-XL");
        finalBingMarketVal.put("Spanish-Venezuela", "es-XL");

        return finalBingMarketVal;
    }

    public static HashMap<Integer, String> loadErrorCodes() {
        HashMap<Integer, String> hMap = new LinkedHashMap<>();
        hMap.put(122, "Requested URL too long");
        hMap.put(200, "The server successfully returned the page");
        hMap.put(201, "Created    [new resource created]");
        hMap.put(
                202,
                "Accepted   [processing started but not yet completed - depends on other factors]");
        hMap.put(203, "Non-Authoritative Information");
        hMap.put(204, "No Content");
        hMap.put(205, "Reset Content");
        hMap.put(
                206,
                "Partial Content    [this is a message that a file has been partially downloaded. Used by tools like wget to enable resuming of interrupted downloads]");
        hMap.put(207,
                "Multi-Status    [WebDAV - an XML message with additional codes]");
        hMap.put(122, "Requested URL too long");
        hMap.put(300, " Multiple Choices   [more info needed]");
        hMap.put(301,
                "Redirect - permanent    [the 301 is always used for ANY redirect now]");
        hMap.put(
                302,
                "Page has been moved temporarily and the new URL is available. You should be sent there by the server.");
        hMap.put(
                303,
                "See Other   [elsewhere - with various parameters - can be used for language redirects]");
        hMap.put(304,
                "Not Modified - The requested page hasn't been modified since the last request");
        hMap.put(305, "Use Proxy   [some browsers handle this wrongly]");
        hMap.put(306, "Switch Proxy   [not used now]");
        hMap.put(307,
                "Temporary Redirect   [elsewhere - another version with different parameters]");
        hMap.put(400, "Bad request");
        hMap.put(401, "Unauthorized    [login+password required]");
        hMap.put(402, "Payment Required");
        hMap.put(
                403,
                "Forbidden - The server understood the request but is refusing to perform the request because of an unspecified reason");
        hMap.put(404, "Page Not Found");
        hMap.put(405, "Method Not Allowed");
        hMap.put(406, "Not Acceptable    [normally returned for an attack]");
        hMap.put(407, "Proxy Authentication Required");
        hMap.put(408, "Request Timeout");
        hMap.put(409, "Conflict");
        hMap.put(410, "Gone   [page has moved]");
        hMap.put(411, "Length Required");
        hMap.put(412, "Precondition Failed");
        hMap.put(413, "Request Entity Too Large");
        hMap.put(414, "Request-URL Too Long");
        hMap.put(415, "Unsupported Media Type");
        hMap.put(416, "Requested Range Not Satisfiable");
        hMap.put(417, "Expectation Failed");
        hMap.put(422, "Unprocessable entity");
        hMap.put(423, "Locked");
        hMap.put(424, "Failed dependency");
        hMap.put(
                500,
                "Internal Server Error - The request was unsuccessful due to an unexpected condition encountered by the server");
        hMap.put(501, "Not Implemented");
        hMap.put(502, "Bad Gateway");
        hMap.put(
                503,
                "Service Unavailable - The request was unsuccessful to the server being down or overloaded");
        hMap.put(504, "Gateway Timeout");
        hMap.put(505, "HTTP Version Not Supported");
        hMap.put(506, "Variant also negotiates");
        hMap.put(507, "Insufficient storage");
        hMap.put(510, "Not Extended - HTTP staus code Not Extended");
        hMap.put(
                512,
                "Not Supported   [might be seen on a server communicating with another server, for example when updating, but fails due to going through a proxy]");
        hMap.put(990, "Blocked by robots.txt file");
        hMap.put(999, "Undefined Error");
        return hMap;
    }

    public static ArrayList<Keyword> sort(ArrayList<Keyword> keywordList) {
        Keyword keyword[] = new Keyword[keywordList.size()];
        keyword = (Keyword[]) keywordList.toArray(keyword);

        Comparator<Keyword> c = new Comparator<Keyword>() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }

            @Override
            public int compare(Keyword o1, Keyword o2) {
                return (o1.getKeyword().compareTo(o2.getKeyword()));
            }
        };

        Arrays.sort(keyword, c);
        keywordList.clear();
        keywordList.addAll(Arrays.asList(keyword));
        return keywordList;
    }

    static public Session getUserInformation(HttpServletRequest request, Session session) {
        String userAgent = request.getHeader("User-Agent");
        UserAgent agent = UserAgent.parseUserAgentString(userAgent);
        Browser browser = agent.getBrowser();
        Version brversion = agent.getBrowserVersion();
        OperatingSystem os = agent.getOperatingSystem();
        session.setIp(request.getRemoteAddr());
        session.setOs(os.getDeviceType() + "-" + os.getName());
        session.setBrowser(browser.getManufacturer() + "-" + browser.getName());
        if (brversion != null) {
            session.setBrowserVersion(brversion.getVersion());
        }
        return session;
    }

    public static String changeDateStringFormat(String inDateString, String fromFormat, String toFormat) {
        DateFormat fromDateFormat = new SimpleDateFormat(fromFormat);
        DateFormat toDateFormat = new SimpleDateFormat(toFormat);
        try {
            return toDateFormat.format(fromDateFormat.parse(inDateString));
        } catch (ParseException e) {
            logger.error(e);
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    public static String changeDateStringFormatWithTime(String inDateString, String fromFormat, String toFormat) {
        DateFormat fromDateFormat = new SimpleDateFormat(fromFormat);
        DateFormat toDateFormat = new SimpleDateFormat(toFormat);
        Date fromDate = null;
        try {
            fromDate = fromDateFormat.parse(inDateString);
        } catch (ParseException e1) {
            logger.error(e1);
            return null;
        }

        fromDate.setHours(23);
        fromDate.setMinutes(59);
        fromDate.setSeconds(59);
        return toDateFormat.format(fromDate);
    }

    public static String createFileName(String fileName) {
        java.util.Date d = new java.util.Date();
        String format = new SimpleDateFormat("yyyy_MMM_dd_HHmmss_SSS").format(d);
        fileName = fileName + "_" + format;
        return fileName;
    }

    public static String removeSpaces(String s) {
        return s.replaceAll(" ", "");
    }

    public static String generateTableName(String baseTableName) {
        java.util.Date d = new java.util.Date();
        String format = new SimpleDateFormat("ddMMMyy_HHmmss_SSS").format(d);
        String tableName = baseTableName + "_" + format;
        return tableName;
    }

    public static int createTable(String tableName, String columnNames, JdbcTemplate jdbcTemplate) {
        String sql = new StringBuilder().append("CREATE TABLE ").append(tableName).append(" ( ").append(columnNames).append(" ) ").toString();
        try {
            return jdbcTemplate.update(sql);
        } catch (Exception e) {
            logger.error("Exception while createTable ", e);
            return 1;
        }
    }

    public static String compressFile(String filePath, String fileName, boolean dummy) {
        String file = "";
        try {
            File f = new File(filePath + fileName.substring(0, fileName.lastIndexOf(".")) + ".zip");
            FileOutputStream fos = new FileOutputStream(f);
            BufferedInputStream sourceStream;
            try (ZipOutputStream targetStream = new ZipOutputStream(fos)) {
                targetStream.setMethod(ZipOutputStream.DEFLATED);
                FileInputStream fis = new FileInputStream(filePath + fileName);
                sourceStream = new BufferedInputStream(fis);
                ZipEntry theEntry = new ZipEntry(fileName);
                targetStream.putNextEntry(theEntry);
                byte[] data = new byte[1024];
                int bCnt;
                while ((bCnt = sourceStream.read(data, 0, 1024)) != -1) {
                    targetStream.write(data, 0, bCnt);
                }
                targetStream.flush();
                targetStream.closeEntry();
            }
            sourceStream.close();
            file = fileName.substring(0, fileName.lastIndexOf(".")) + ".zip";
        } catch (Exception exce) {
            logger.error("Exception while compressing file ", exce);
            return null;
        }
        return file;
    }

    public static void deleteFiles(String location, String[] fileNames) {
        for (String fileName : fileNames) {
            File f = new File(location.concat(fileName));
            if (f.exists()) {
                f.delete();
                logger.info(fileName + " is deleted.");
            }
        }
    }

    public static void writeTexttoFile(File file, String htmlText) {
        Writer output = null;
        try {
            output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8")); //new FileWriter(file)
            output.write(htmlText);
            output.close();
        } catch (IOException e1) {
            logger.error(e1);
        }
    }

    public static void sendInstantMail(ToolService toolService, EmailTemplateService emailTemplateService, HttpSession session, int eventId) {
        Login user = (Login) session.getAttribute("user");
        Session userSession = (Session) session.getAttribute("userSession");
        Session guestUserSession = (Session) session.getAttribute("guestUserSession");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        long noTimesToolUsed = toolService.fisrtToolUsage(user.getId());//to find the usage of anytool
        EmailTrackingInfoService emailTrackingInfoService = (EmailTrackingInfoService) ctx.getBean("emailTrackingInfoService");
        if (userLoggedIn && noTimesToolUsed == 0 && guestUserSession != null && (userSession.getToolUsage() == guestUserSession.getToolUsage())) {
            //call send mail method
            List<EmailTemplate> emailTemplates = emailTemplateService.getTemplatesByInstantEvent(eventId);
            for (EmailTemplate emailTemplate : emailTemplates) {
                if (emailTemplate != null) {
                    emailTrackingInfoService.addEventtoDB(emailTemplate);//To add event in event_details table for tracking the event
                    SendMail sendmail = new SendMail();
                    String fromAddress;
                    switch (emailTemplate.getFromAddress()) {
                        case 1:
                            fromAddress = (String) ctx.getBean("teamMail");
                            break;
                        case 2:
                            fromAddress = (String) ctx.getBean("netelixirinfo");
                            break;
                        case 3:
                            fromAddress = (String) ctx.getBean("supportMail");
                            break;
                        case 4:
                            fromAddress = (String) ctx.getBean("lxrseoinfo");
                            break;
                        case 5:
                            fromAddress = (String) ctx.getBean("lxrseosupport");
                            break;
                        default:
                            fromAddress = "";
                            break;
                    }
                    if (fromAddress == null || fromAddress.trim().equals("")) {
                        fromAddress = (String) ctx.getBean("teamMail");
                    }
                    sendmail.sendAdminMail(emailTemplate, user, fromAddress);
                    emailTemplate.setLastMailSent(Calendar.getInstance());
                    emailTemplateService.updateLastMailSentDate(emailTemplate);//update the last mail sent date for welcome mail
                }
            }
        }
    }

    public static boolean sendReportThroughMail(HttpServletRequest request, String repFilePath, String repFileName, String toAddrs,
            String toolName) {
        String subjectLine = null;
        if (toolName != null && toolName.contains(" Report")) {
            subjectLine = toolName;
        } else if (toolName != null) {
            subjectLine = toolName + " Report";
        }
        File f = new File(repFilePath + repFileName);
        long size = f.length();
        if (size > MAX_FILE_SIZE) {
            repFileName = compressFile(repFilePath, repFileName, true);
        }
        String fromAddrs = (String) ctx.getBean("teamMail");
        /*Sending tool analysis report to user email after payment sucess. 
        For SEO Inbound link checker tool only BCC will include for tool analysis report
           This was implemented on Jul 28, 2018 as per QA team suggestion.
        String bccAddrs = toolName.equals(Common.INBOUND_LINK_CHECKER)? (String) ctx.getBean("teamMail"):null;*/

        return new SendMail().sendTooleport(request, repFilePath, repFileName, toAddrs, fromAddrs, subjectLine, toolName, toolName.equals(Common.INBOUND_LINK_CHECKER) ? (String) ctx.getBean("teamMail") : null);
    }

    public static void sendSiteMapReportThroughMail(String repFilePath, String repFileName, String toAddrs,
            String toolName, String userName) {
        String subjectLine = toolName + " Report";
        File f = new File(repFilePath + repFileName);
        long size = f.length();
        if (size > MAX_FILE_SIZE) {
            repFileName = compressFile(repFilePath, repFileName, true);
        }
        String fromAddrs = (String) ctx.getBean("teamMail");
        SendMail.sendSiteMapToolReport(repFilePath, repFileName, toAddrs, fromAddrs, subjectLine, toolName, userName);
    }

    //Seo rank checker weekly report
    public static void sendWeeklyReportThroughMail(String repFilePath, String repFileName, String toAddrs,
            String toolName, String userName, String seventhDate, String curDate) {
        String subjectLine = toolName + " Report";
        File f = new File(repFilePath + repFileName);
        long size = f.length();
        if (size > MAX_FILE_SIZE) {
            repFileName = compressFile(repFilePath, repFileName, true);
        }
        String fromAddrs = (String) ctx.getBean("teamMail");
        new SendMail().sendWeeklyTooleport(repFilePath, repFileName, toAddrs, fromAddrs, subjectLine, toolName, userName, seventhDate, curDate);
    }

    public static void downloadReport(HttpServletResponse response, String repFilePath, String repFileName, String contentType) {
        File f = new File(repFilePath + repFileName);
        boolean zipped = false;
        long size = f.length();
        String zippedFileName = "";
        if (size > MAX_FILE_SIZE) {
            zippedFileName = compressFile(repFilePath, repFileName, true);
            zipped = true;
        }
        response.setHeader("Cache-Control", "private, max-age=15");
        if (zipped) {
            response.setContentType("application/zip");
            f = new File(repFilePath + zippedFileName);
        } else if (contentType.equals("pdf")) {
            logger.info("inside of common" + contentType);
            response.setContentType("application/pdf");
        } else if (contentType.equals("zip")) {
            response.setContentType("application/zip");
        } else if (contentType.equals("tsv")) {
            response.setContentType("text/tab-separated-values");
        } else if (contentType.equals("xlsx")) {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        } else if (contentType.equals("csv")) {
            response.setContentType("text/csv");
        } else if (contentType.equals("ppt")) {
            response.setContentType("application/vnd.openxmlformats-officedocument.presentationml.presentation");
        } else if (contentType.equals("txt")) {
            response.setContentType("text/plain");
        } else if (contentType.equals("jpg")) {
            response.setContentType("image/jpg");
        } else if (contentType.equals("jpeg")) {
            response.setContentType("image/jpeg");
        } else {
            response.setContentType("application/vnd.ms-excel");
        }
        response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException to download request file: " + repFilePath + repFileName, e);
        } catch (Exception e) {
            logger.error("Exception to download request file: " + repFilePath + repFileName, e);
        }
        try {
            ServletOutputStream resOut = response.getOutputStream();
            IOUtils.copy(fis, resOut);
            resOut.close();

        } catch (IOException e) {
            logger.error("File is not being copied to output response due to IO Exception", e);
        } catch (Exception e) {
            logger.error("Exception to send request file: ", e);
        }
    }

    public static String createSiteMapTxt(String appPath, HttpServletRequest request, String filename, String userType) {
        HttpSession session = request.getSession(true);
        SiteMapBuilderThread smbtNew1 = null;
        if (userType != null) {
            if (session.getAttribute("lxrSeoSitemapUrls") != null) {
                smbtNew1 = (SiteMapBuilderThread) session.getAttribute("lxrSeoSitemapUrls");
            }
        } else if (session.getAttribute("sitemapurls") != null) {
            smbtNew1 = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
        }
        try {

            Writer output = null;
            File totalpath = new File(appPath + filename);
            output = new BufferedWriter(new FileWriter(totalpath));
            List<SitemapUrl> txtsortUrls = SiteMapBuilderService.sortURLSBasedOnPriority(new LinkedList<>());

            if (smbtNew1 != null) {
                for (SitemapUrl allanctags : txtsortUrls) {
                    if (filename.equals("urllist.txt")) {
                        if (allanctags.getSitemapUrl().trim() != null && !" ".equals(allanctags.getSitemapUrl()) && !"&nbsp;".equals(allanctags.getSitemapUrl())) {
                            if ((allanctags.getStatuscode() < 300) && (allanctags.getStatuscode() != 301)) {
                                if (allanctags.getSitemapUrl().trim() != null && !" ".equals(allanctags.getSitemapUrl()) && !"&nbsp;".equals(allanctags.getSitemapUrl())) {
                                    output.write(allanctags.getSitemapUrl() + "\r\n");
                                }
                            }
                        }
                    } else if (filename.equals("sitemap.err")) {
                        if (allanctags.getSitemapUrl().trim() != null && !" ".equals(allanctags.getSitemapUrl()) && !"&nbsp;".equals(allanctags.getSitemapUrl())) {
                            if (allanctags.getStatuscode() >= 300) {
                                output.write(allanctags.getSitemapUrl() + " , " + allanctags.getStatuscode() + " , " + allanctags.getStatus() + "\r\n");
                            }
                        }
                    }
                }
                output.close();
            }
        } catch (IOException ie) {
            logger.error(ie);
        }
        return filename;

    }

    public static void insertnewId(String newId, HttpSession session, LoginService loginService, long userId) {
        Session userSession = (Session) session.getAttribute("userSession");
        long sessionId = 0;
        String newMailId = "";
        if (userSession != null) {
            sessionId = userSession.getId();
        }
        if (session.getAttribute("email") != null) {
            newMailId = session.getAttribute("email").toString();
        }
        if (!newMailId.equals(newId)) {
            loginService.insertGuestUserEmailId(userId, sessionId, newId);
        }
    }

    public static void modifyDummyUserInToolorVideoUsage(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(true);
        Login user = (Login) session.getAttribute("user");
        LoginService loginService = (LoginService) ctx.getBean("loginService");
        SessionService sessionService = (SessionService) ctx.getBean("sessionService");
        if (user.getId() < 0) {
            Cookie cookie = null;
            CookieService cookieService = new CookieService();
            cookieService.setLoginService(loginService);
            try {
                cookie = cookieService.createCookie(response, Common.checkLocal(request), false);
            } catch (ServletException e) {
                logger.error("Exception in creating cookie after tool usage", e);
            }
            Long guestUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(cookie.getValue()));
            user = loginService.find(guestUserId);
            session.setAttribute("user", user);
            session.setAttribute("marketplaceCookie", cookie);

            Session userSession = (Session) session.getAttribute("userSession");
            userSession.setUserId(guestUserId);
            sessionService.insert(userSession);
        }
    }

    public static boolean checkLocal(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        boolean isLocal = false;
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(session.getServletContext());
        if (context.getServletContext().getAttribute("localIps") != null) {
            List<String> ips = (List<String>) context.getServletContext().getAttribute("localIps");
            if (ips.contains(ipAddress)) {
                isLocal = true;
            }
        }
        return isLocal;
    }

    public static String getUrlValidationForCmp(String currentUrl) {
        try {
            if (currentUrl != null && !currentUrl.trim().isEmpty()) {
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpsCurrentUrl = "";
                int statusCode = 0;
                if (currentUrl.startsWith("https://")) {
                    httpsCurrentUrl = currentUrl;
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                } else if (!currentUrl.startsWith("http://")) {
                    httpsCurrentUrl = "https://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpsCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                        httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                        statusCode = getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpsCurrentUrl;
                        }
                    }
                }
                statusCode = 0;
                redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);

                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString().trim();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString().trim();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpCurrentUrl;
                        } else {
                            return "invalid";
                        }
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            } else {
                return "invalid";
            }
        } catch (Exception e) {
            logger.error("Exception in getUrlValidationForCmp", e);
        }
        return "invalid";
    }

    public static String getUrlValidation(String currentUrl, HttpSession session) {
        try {
            if (currentUrl != null && !currentUrl.trim().isEmpty()) {
                currentUrl = currentUrl.trim();
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpsCurrentUrl = "";
                List<List<String>> cookieContainer = new ArrayList<>();
                int statusCode = 0;
                if (currentUrl.startsWith("https://")) {
                    httpsCurrentUrl = currentUrl;
                    statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer, session);
                } else if (!currentUrl.startsWith("http://")) {
                    httpsCurrentUrl = "https://" + currentUrl;
                    statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer, session);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpsCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                        httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                        statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer, session);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpsCurrentUrl;
                        }
                    }
                }
                statusCode = 0;
                redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer, session);
                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer, session);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "redirected";
                } else if (statusCode == 200) {
                    return httpCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer, session);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "redirected";
                        } else if (statusCode == 200) {
                            return httpCurrentUrl;
                        } else {
                            return "invalid";
                        }
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            } else {
                return "invalid";
            }
        } catch (Exception e) {
            logger.error("Exception in getUrlValidation", e);
        }
        return "invalid";
    }

    public static int getStatusCode(String currentUrl, StringBuilder redirectedUrl, List<List<String>> cookieContainer, HttpSession session) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        int iteration = 0;
        boolean isCertUpdated = Boolean.FALSE;
        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 3) {
            logger.info("currentUrl: " + currentUrl + ", responseCode: " + responseCode + ", iteration: " + iteration);
            try {
                URL currentUrlObj = new URL(currentUrl);
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(Boolean.FALSE);
                currentUrlConn.setConnectTimeout(IMPORTANT_CONNECT_TIME_OUT);
                currentUrlConn.setReadTimeout(IMPORTANT_READ_TIME_OUT);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                currentUrlConn.setRequestMethod("GET");
                currentUrlConn.setDoOutput(false);

                if (cookieContainer != null && cookieContainer.size() > 0
                        && cookieContainer.get(0) != null && cookieContainer.get(0).size() > 0) {
                    for (String cookie : cookieContainer.get(0)) {
                        currentUrlConn.addRequestProperty("Cookie", cookie);
                    }
                }
                responseCode = currentUrlConn.getResponseCode();

                List<String> tempCookies = currentUrlConn.getHeaderFields().get("Set-Cookie");
                if (tempCookies != null && tempCookies.size() > 0 && cookieContainer != null) {
                    if (cookieContainer.size() > 0) {
                        cookieContainer.set(0, tempCookies);
                    } else {
                        cookieContainer.add(tempCookies);
                    }
                }
                logger.info("currentUrl: " + currentUrl + ", responseCode: " + responseCode + ", iteration: " + iteration);
                if (responseCode / 100 == 3) {
                    if (iteration != 2) {
                        String redirectedLocation = currentUrlConn.getHeaderField("Location");
                        String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                        if (redirectedLocation != null) {
                            if (!(redirectedLocation.startsWith("http"))) {
                                if (redirectedLocation.startsWith("/")) {
                                    currentUrl = tempUrl + redirectedLocation;
                                } else if (!(redirectedLocation.startsWith("/"))) {
                                    if (currentUrl.endsWith("/")) {
                                        currentUrl = currentUrl + redirectedLocation;
                                    } else {
                                        currentUrl = currentUrl + "/" + redirectedLocation;
                                    }
                                }
                            } else {
                                currentUrl = redirectedLocation;
                            }
                        }
                    } else {
                        redirectedUrl.append("");
                    }
                } else if (responseCode == 200 && iteration != 0) {
                    if (redirectedUrl.toString().equals("")) {
                        redirectedUrl.append(currentUrl);
                    } else if (!redirectedUrl.toString().equals("")) {
                        redirectedUrl = new StringBuilder("");
                        redirectedUrl.append(currentUrl);
                    }
                }
            } catch (SSLException e) {
                logger.error("SSLException produced by failed SSL-related operation in getStatusCode when connection to " + currentUrl + ", and isCertUpdated:: " + isCertUpdated + ", Cause: " + e);
                /*To update the SSL certification for a website/ URL */
                if (!isCertUpdated) {
                    try {
                        isCertUpdated = Common.sslValidation(currentUrl);
                        logger.info("isCertUpdated to handle the SSLProtocolException for the URL:: " + currentUrl + ", and status is:: " + isCertUpdated);
                    } catch (Exception ex) {
                        isCertUpdated = Boolean.TRUE;
                        logger.error("Exception in creating SSL | PKIX certification: " + currentUrl + ", terminating the status Cause:: " + ex);
                    }
                }
            } catch (IOException e) {
                /*
                -> java.net.NoRouteToHostException: No route to host 
                -> java.net.SocketTimeoutException: Read timed out */
                logger.error("IOException in getStatusCode when connection to " + currentUrl + ", at iteration:: " + iteration + ", Cause: " + e);
                /*Skip the actually iterations of current thread to get status code
                iteration = iteration + 3;*/
            } catch (Exception e) {
                logger.error("Exception in getStatusCode when connection to " + currentUrl + ", at iteration:: " + iteration + ", Cause: " + e);
            }
            iteration++;
        }
        if (session != null) {
            session.setAttribute("urlStatusCode", responseCode);
        }
        return responseCode;
    }

    public static int getStatusCodeVersion2(String currentUrl, StringBuilder redirectedUrl) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(currentUrl);
            currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
            currentUrlConn.setInstanceFollowRedirects(false);
            currentUrlConn.setConnectTimeout(50000);
            currentUrlConn.setReadTimeout(50000);
            currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
            responseCode = currentUrlConn.getResponseCode();
            if (responseCode == 301 || responseCode == 302
                    || responseCode == 306 || responseCode == 307) {
                String redirectedLocation = currentUrlConn.getHeaderField("Location");
                logger.info("redirected location is :" + redirectedLocation);
                String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                if (redirectedLocation != null) {
                    if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                        if (redirectedLocation.startsWith("/")) {
                            redirectedLocation = tempUrl + redirectedLocation;
                            logger.debug("redirectedlocation starts  with / " + redirectedLocation);
                        } else if (!(redirectedLocation.startsWith("/"))) {
                            if (currentUrl.endsWith("/")) {
                                redirectedLocation = currentUrl + redirectedLocation;
                                logger.debug("current url  ends with / " + redirectedLocation);
                            } else {
                                redirectedLocation = currentUrl + "/" + redirectedLocation;
                                logger.debug("current url not ends with / " + redirectedLocation);
                            }
                        }
                    }
                    URL redirectedUrlObj = new URL(redirectedLocation);
                    try {
                        HttpURLConnection redirectedUrlConn = (HttpURLConnection) redirectedUrlObj.openConnection();
                        redirectedUrlConn.setInstanceFollowRedirects(false);
                        redirectedUrlConn.setConnectTimeout(50000);
                        redirectedUrlConn.setReadTimeout(50000);
                        redirectedUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                        responseCode = redirectedUrlConn.getResponseCode();
                        logger.debug("responseCode is :------------------- " + responseCode);
                        if (responseCode == 200) {
                            logger.debug("location is : " + redirectedUrlConn.getHeaderField("Location"));
                            redirectedUrl.append(redirectedLocation);
                        } else if (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {
                            String redirectedLocation1 = redirectedUrlConn.getHeaderField("Location");
                            logger.info("redirectedLocation1 is :  " + redirectedLocation1);
                            String tempURL1 = redirectedUrlObj.getProtocol() + "://" + redirectedUrlObj.getHost();
                            if (redirectedLocation1 != null) {
                                if (!(redirectedLocation1.startsWith(currentUrlObj.getProtocol()))) {
                                    if (redirectedLocation1.startsWith("/")) {
                                        redirectedLocation1 = tempURL1 + redirectedLocation1;
                                        logger.info("redirectedlocation1 starts  with / " + redirectedLocation1);
                                    } else if (!(redirectedLocation1.startsWith("/"))) {
                                        if (redirectedLocation.endsWith("/")) {
                                            redirectedLocation1 = redirectedLocation + redirectedLocation1;
                                            logger.debug("redirectedLocation url  ends with / " + redirectedLocation1);
                                        } else {
                                            redirectedLocation1 = redirectedLocation + "/" + redirectedLocation1;
                                            logger.debug("redirectedLocation url not ends with / " + redirectedLocation1);
                                        }
                                    }
                                }
                                URL tempRedirectedUrlObj = new URL(redirectedLocation1);
                                try {
                                    HttpURLConnection redirectedUrlConn2 = (HttpURLConnection) tempRedirectedUrlObj.openConnection();
                                    redirectedUrlConn2.setRequestProperty("User-Agent", USER_AGENT);
                                    redirectedUrlConn2.setConnectTimeout(10000);
                                    redirectedUrlConn2.setReadTimeout(5000);
                                    responseCode = redirectedUrlConn2.getResponseCode();
                                    if (responseCode == 200 || responseCode == 301 || responseCode == 302 || responseCode == 307) {
                                        redirectedUrl.append(redirectedLocation1);
                                    }
                                } catch (Exception e1) {
                                    logger.error("Error 1 in connecting " + redirectedLocation1 + " Cause: ", e1);
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Error 2 in connecting " + redirectedLocation + " Cause: ", e);
                    }
                }
            }
        } catch (Exception e2) {
            logger.error("Error 3 in connecting " + currentUrl + " Cause: ", e2);
        }
        return responseCode;
    }

    public static int getStatusCodeAndHTML(String currentUrl, StringBuilder redirectedUrl, StringBuilder pageHTML) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(currentUrl);
            if (!(currentUrlObj.equals("null") || currentUrlObj.equals(" "))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
                logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                if (responseCode == 301 || responseCode == 302
                        || responseCode == 306 || responseCode == 307) {
                    String redirectedLocation = currentUrlConn.getHeaderField("Location");
                    String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                    if (redirectedLocation != null) {
                        if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                            if (redirectedLocation.startsWith("/")) {
                                redirectedLocation = tempUrl + redirectedLocation;
                            } else if (!(redirectedLocation.startsWith("/"))) {
                                if (currentUrl.endsWith("/")) {
                                    redirectedLocation = currentUrl + redirectedLocation;
                                } else {
                                    redirectedLocation = currentUrl + "/" + redirectedLocation;
                                }
                            }
                        }
                        URL redirectedUrlObj = new URL(redirectedLocation);
                        try {
                            HttpURLConnection redirectedUrlConn = (HttpURLConnection) redirectedUrlObj.openConnection();
                            redirectedUrlConn.setInstanceFollowRedirects(false);
                            redirectedUrlConn.setConnectTimeout(50000);
                            redirectedUrlConn.setReadTimeout(10000);
                            redirectedUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                            responseCode = redirectedUrlConn.getResponseCode();
                            if (responseCode == 200) {
                                String line;
                                pageHTML.setLength(0);
                                BufferedReader reader = new BufferedReader(new InputStreamReader(redirectedUrlConn.getInputStream()));
                                while ((line = reader.readLine()) != null) {
                                    pageHTML.append(line);
                                }
                                redirectedUrl.append(redirectedLocation);

                            } else if (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {

                                String redirectedLocation1 = redirectedUrlConn.getHeaderField("Location");
                                String tempURL1 = redirectedUrlObj.getProtocol() + "://" + redirectedUrlObj.getHost();
                                if (redirectedLocation1 != null) {
                                    if (!(redirectedLocation1.startsWith(currentUrlObj.getProtocol()))) {
                                        if (redirectedLocation1.startsWith("/")) {
                                            redirectedLocation1 = tempURL1 + redirectedLocation1;
                                        } else if (!(redirectedLocation1.startsWith("/"))) {
                                            if (redirectedLocation.endsWith("/")) {
                                                redirectedLocation1 = redirectedLocation + redirectedLocation1;
                                            } else {
                                                redirectedLocation1 = redirectedLocation + "/" + redirectedLocation1;
                                            }
                                        }
                                    }
                                    URL tempRedirectedUrlObj = new URL(redirectedLocation1);
                                    try {
                                        HttpURLConnection redirectedUrlConn2 = (HttpURLConnection) tempRedirectedUrlObj.openConnection();
                                        redirectedUrlConn2.setRequestProperty("User-Agent", USER_AGENT);
                                        responseCode = redirectedUrlConn2.getResponseCode();
                                        if (responseCode == 200 || responseCode == 301 || responseCode == 302 || responseCode == 307) {
                                            redirectedUrl.append(redirectedLocation1);
                                            if (responseCode == 200) {
                                                getPageHtml(currentUrlConn, pageHTML);
                                            }
                                        }
                                    } catch (Exception e1) {
                                        logger.error("Exception in appending page url for redirected url in getStatusCodeAndHTML", e1);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.error("Exception in chechking status code for redirected url in getStatusCodeAndHTML", e);
                        }
                    }
                } else if (responseCode == 200) {
                    getPageHtml(currentUrlConn, pageHTML);
                }
            }
        } catch (Exception e2) {
            logger.error("Error  in connecting getStatusCodeAndHTML " + currentUrl + " Cause: ", e2);
        }
        return responseCode;
    }

    private static void getPageHtml(HttpURLConnection currentUrlConn, StringBuilder pageHTML) {
        BufferedReader reader = null;
        try {
            String line;
            String charecterSet = "UTF-8";
            int charecterIndex = currentUrlConn.getContentType().indexOf("charset=");
            if (charecterIndex != -1) {
                charecterSet = currentUrlConn.getContentType().substring(charecterIndex + 8);
            }
            try {
                if (currentUrlConn.getHeaderField("Content-Encoding") != null && currentUrlConn.getHeaderField("Content-Encoding").equals("gzip")) {
                    reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(currentUrlConn.getInputStream())));
                } else {
                    reader = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream(), charecterSet));
                }
            } catch (Exception e) {
                reader = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream(), "UTF-8"));
            }
            while ((line = reader.readLine()) != null) {
                pageHTML.append(line);
            }
        } catch (IOException ex) {
            logger.error("Exception while fetching the Page HTML for the Webpage ", ex);
        } finally {
            try {
                reader.close();
            } catch (IOException | NullPointerException ex) {
                logger.error("Exception while closing the BufferedReader ", ex);
            }
        }
    }

    /**
     *
     * @param currentUrl : URL to check
     * @param redirectedUrl: Reference passed from called, returns redirected
     * URL if it redirects, blank if there is too many redirection or any other
     * response code
     * @param pageHTML : pageHTML of current page or redirected page
     * @return response code of currentUrl
     */
    public static int getCurrentStatusCodeAndHTML(String currentUrl, StringBuilder redirectedUrl, StringBuilder pageHTML) {
        int responseCode = 0;
        int currentResponseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(currentUrl);
            if (!(currentUrlObj.equals("null") || currentUrlObj.equals(" "))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(CONNECTION_TIME_OUT);
                currentUrlConn.setReadTimeout(SOCKET_TIME_OUT);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
                currentResponseCode = responseCode;
                logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                if (responseCode == 301 || responseCode == 302
                        || responseCode == 306 || responseCode == 307) {
                    String redirectedLocation = currentUrlConn.getHeaderField("Location");
                    String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                    if (redirectedLocation != null) {
                        if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                            if (redirectedLocation.startsWith("/")) {
                                redirectedLocation = tempUrl + redirectedLocation;
                            } else if (!(redirectedLocation.startsWith("/"))) {
                                if (currentUrl.endsWith("/")) {
                                    redirectedLocation = currentUrl + redirectedLocation;
                                } else {
                                    redirectedLocation = currentUrl + "/" + redirectedLocation;
                                }
                            }
                        }
                        URL redirectedUrlObj = new URL(redirectedLocation);
                        try {
                            HttpURLConnection redirectedUrlConn = (HttpURLConnection) redirectedUrlObj.openConnection();
                            redirectedUrlConn.setInstanceFollowRedirects(false);
                            redirectedUrlConn.setConnectTimeout(CONNECTION_TIME_OUT);
                            redirectedUrlConn.setReadTimeout(SOCKET_TIME_OUT);
                            redirectedUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                            responseCode = redirectedUrlConn.getResponseCode();
                            if (responseCode == 200) {
                                String line;
                                pageHTML.setLength(0);
                                BufferedReader reader = new BufferedReader(new InputStreamReader(redirectedUrlConn.getInputStream()));
                                while ((line = reader.readLine()) != null) {
                                    pageHTML.append(line);
                                }
                                redirectedUrl.append(redirectedLocation);

                            } else if (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {

                                String redirectedLocation1 = redirectedUrlConn.getHeaderField("Location");
                                String tempURL1 = redirectedUrlObj.getProtocol() + "://" + redirectedUrlObj.getHost();
                                if (redirectedLocation1 != null) {
                                    if (!(redirectedLocation1.startsWith(currentUrlObj.getProtocol()))) {
                                        if (redirectedLocation1.startsWith("/")) {
                                            redirectedLocation1 = tempURL1 + redirectedLocation1;
                                        } else if (!(redirectedLocation1.startsWith("/"))) {
                                            if (redirectedLocation.endsWith("/")) {
                                                redirectedLocation1 = redirectedLocation + redirectedLocation1;
                                            } else {
                                                redirectedLocation1 = redirectedLocation + "/" + redirectedLocation1;
                                            }
                                        }
                                    }
                                    URL tempRedirectedUrlObj = new URL(redirectedLocation1);
                                    try {
                                        HttpURLConnection redirectedUrlConn2 = (HttpURLConnection) tempRedirectedUrlObj.openConnection();
                                        redirectedUrlConn2.setRequestProperty("User-Agent", USER_AGENT);
                                        redirectedUrlConn2.setConnectTimeout(CONNECTION_TIME_OUT);
                                        redirectedUrlConn2.setReadTimeout(SOCKET_TIME_OUT);
                                        responseCode = redirectedUrlConn2.getResponseCode();
                                        if (responseCode == 200 || responseCode == 301 || responseCode == 302 || responseCode == 307) {

                                            if (responseCode == 200) {
                                                redirectedUrl.append(redirectedLocation1);
                                                String line;
                                                pageHTML.setLength(0);
                                                BufferedReader reader = new BufferedReader(new InputStreamReader(redirectedUrlConn.getInputStream()));
                                                while ((line = reader.readLine()) != null) {
                                                    pageHTML.append(line);
                                                }
                                            } else {
                                                redirectedUrl.append("");
                                            }
                                        }
                                    } catch (SocketTimeoutException e) {
                                        logger.error("SocketTimeoutException while connecting to " + redirectedLocation1, e);
                                    } catch (ConnectException e) {
                                        logger.error("ConnectException while connecting to " + redirectedLocation1, e);
                                    } catch (Exception e) {
                                        logger.error(e);
                                    }

                                }
                            }
                        } catch (SocketTimeoutException e) {
                            logger.error("SocketTimeoutException in getCurrentStatusCodeAndHTML while connecting to " + redirectedLocation + ", and cause:: " + e.getLocalizedMessage());
                        } catch (ConnectException e) {
                            logger.error("ConnectException in getCurrentStatusCodeAndHTML while connecting to " + redirectedLocation + ", and cause:: " + e.getLocalizedMessage());
                        } catch (IOException e) {
                            logger.error("IOException in getCurrentStatusCodeAndHTML while connecting to " + redirectedLocation + ", and cause:: " + e.getLocalizedMessage());
                        } catch (Exception e) {
                            logger.error("Exception in getCurrentStatusCodeAndHTML while connecting to " + redirectedLocation + ", and cause:: " + e.getLocalizedMessage());
                        }
                    }
                } else if (responseCode == 200) {
                    String line;
                    pageHTML.setLength(0);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream()));
                    while ((line = reader.readLine()) != null) {
                        pageHTML.append(line);
                    }
                }
            }
        } catch (SocketTimeoutException e) {
            logger.error("SocketTimeoutException while connecting to " + currentUrl, e);
        } catch (ConnectException e) {
            logger.error("ConnectException while connecting to " + currentUrl, e);
        } catch (Exception e) {
            logger.error(e);
        }
        return currentResponseCode;
    }

    public static int getFirstURLStatusCode(String currentUrl) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        URL currentUrlObj = null;
        try {
            currentUrlObj = new URL(currentUrl);
        } catch (MalformedURLException e1) {
            logger.error("MalformedURLException in getFirstURLStatusCode for " + currentUrl, e1);
        }
        if (currentUrlObj != null && !currentUrlObj.equals("")) {
            try {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return responseCode;
    }

    public static int appendProtocalAndCheckStausCode(String url, StringBuilder redirectedUrl, StringBuilder finalModifiedUrl) {
        int statusCode = 0;
        String modifiedurl = "";
        if (url.startsWith("https")) {
            finalModifiedUrl.append(url);
            statusCode = getURLStatusAndLocation(url, redirectedUrl);
        } else if (!url.startsWith("http://")) {
            modifiedurl = "https://" + url;
            finalModifiedUrl.append(modifiedurl);
            statusCode = getURLStatusAndLocation(modifiedurl, redirectedUrl);
        }
        if (statusCode == 200 || statusCode == 301 || statusCode == 302 || statusCode == 307) {
            return statusCode;
        } else {
            if (url.contains("www.")) {
                url = url.replace("www.", "");
            }
            modifiedurl = "https://www." + url;
            finalModifiedUrl = new StringBuilder("");
            finalModifiedUrl.append(modifiedurl);
            statusCode = getURLStatusAndLocation(modifiedurl, redirectedUrl);
            if (statusCode == 200 || statusCode == 301 || statusCode == 302 || statusCode == 307) {
                return statusCode;
            } else {
                modifiedurl = "http://www." + url;
                finalModifiedUrl.append(modifiedurl);
                statusCode = getURLStatusAndLocation(url, redirectedUrl);
                return statusCode;
            }
        }
    }

    /*To handle PKIX path building failed*/
    public static boolean sslValidation(String queryWebSite) {
        boolean isSSlCreated = Boolean.FALSE;
        if (queryWebSite != null && !queryWebSite.trim().isEmpty()) {
            TrustManager[] trustAllCerts = null;
            try {
                trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
                };
            } catch (Exception e) {
                logger.error("Exception in sslValidation when creating trustAllCerts cause:: ", e);
            }

            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("SSL");
                if (sslContext != null) {
                    sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                }
            } catch (NoSuchAlgorithmException e) {
                logger.error("NoSuchAlgorithmException in sslValidation for URL:: " + queryWebSite + ", cause:: ", e);
            } catch (KeyManagementException e) {
                logger.error("KeyManagementException in sslValidation for URL:: " + queryWebSite + ", cause:: ", e);
            } catch (Exception e) {
                logger.error("Exception in sslValidation for URL:: " + queryWebSite + ", cause:: ", e);
            }
            try {
                if (sslContext != null) {
                    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

                    // Create all-trusting host name verifier
                    HostnameVerifier allHostsValid = (String hostname, SSLSession session) -> true;
                    // Install the all-trusting host verifier
                    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
                    /* End of the fix*/
                    isSSlCreated = Boolean.TRUE;
                }
            } catch (Exception e) {
                logger.error("Exception in sslValidation when creating and install the all-trusting host verifier for URL:: " + queryWebSite + ", cause:: ", e);
            }
        }
        return isSSlCreated;
    }

    public static long getDateDiff(Calendar firstDate, Calendar secondDate) {
        long diffInMilis = secondDate.getTime().getTime() - firstDate.getTime().getTime();
        long days = diffInMilis / (24 * 60 * 60 * 1000);
        return days;
    }

    public static String getDomainName(String url) throws URISyntaxException {
        try {
            if (!url.toLowerCase().startsWith("http")) {
                url = "http://" + url;
            }
            URI uri = new URI(url.trim().replace(" ", "%20"));
            return uri.getHost();
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public static int getURLStatus(String url) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(url);
            if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return responseCode;
    }

    public static int getURLStatusAndLocation(String url, StringBuilder redirectedUrl) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(url);
            if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
                if (responseCode == 301 || responseCode == 302
                        || responseCode == 306 || responseCode == 307) {
                    String redirectedLocation = currentUrlConn.getHeaderField("Location");
                    String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                    if (redirectedLocation != null) {
                        if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                            if (redirectedLocation.startsWith("/")) {
                                redirectedLocation = tempUrl + redirectedLocation;
                            }
                        }
                    }
                    redirectedUrl.append(redirectedLocation);
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return responseCode;
    }

    public static boolean checkPresenceOfRobotsTxt(String domain) {
        boolean robotTxtPresent = false;
        if (!domain.startsWith("http://") && !domain.startsWith("https://")) {
            domain = "http://" + domain;
        }
        String url = domain + (domain.endsWith("/") ? "" : "/") + "robots.txt";
        BufferedReader in = null;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(url);
            if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(true);
                currentUrlConn.setConnectTimeout(IMPORTANT_CONNECT_TIME_OUT);
                currentUrlConn.setReadTimeout(IMPORTANT_READ_TIME_OUT);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                in = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream()));
            }
        } catch (Exception e) {
            logger.error(e);
        }

        if (in != null) {
            StringBuilder response = new StringBuilder();
            String line;
            try {
                while ((line = in.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
            } catch (IOException e) {
                logger.error(e);
            }
            if (!response.toString().matches("\\s*")) {
                robotTxtPresent = true;
            }
        }

        return robotTxtPresent;
    }

    public static JSONObject createJsonObjectFromURL(String queryUrl) {
        URL url = null;
        JSONObject jsonObject = null;
        try {
            url = new URL(queryUrl);
        } catch (MalformedURLException e) {
            logger.error(e);
        }
        URLConnection urlConnection;
        try {
            urlConnection = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = br.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            jsonObject = new JSONObject(responseStrBuilder.toString());
        } catch (IOException e) {
            logger.error("IOException", e);
        } catch (JSONException e) {
            logger.error("JSONException in createJsonObjectFromURL cause:" + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception", e);
        }
        return jsonObject;
    }

    public static boolean checkWWWResolve(String url) {
        boolean wwwResolved = false;
        String mainurl = url;
        int statusCode1 = 0, statusCode2 = 0;
        URL fullurl = null, tempUrl = null;
        HttpURLConnection fullwwwUrlConn = null, tempUrlConn = null;
        try {
            String loc = null;
            if (!(url.contains("www")) && url.startsWith("http")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("http://", "http://www."));
            } else if (!(url.contains("www")) && url.startsWith("https")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("https://", "https://www."));
            } else if (url.contains("www")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("www.", ""));
            }
            logger.info("WWW Redirection Checking URLs: " + tempUrl + "\t" + fullurl);
            try {
                fullwwwUrlConn = (HttpURLConnection) fullurl.openConnection();
                fullwwwUrlConn.setInstanceFollowRedirects(false);
                fullwwwUrlConn.connect();
                statusCode1 = fullwwwUrlConn.getResponseCode();

                tempUrlConn = (HttpURLConnection) tempUrl.openConnection();
                tempUrlConn.setInstanceFollowRedirects(false);
                tempUrlConn.connect();
                statusCode2 = tempUrlConn.getResponseCode();
            } catch (IOException e1) {
                logger.error("IOException", e1);
            } catch (Exception e1) {
                logger.error("Exception", e1);
            }

            try {
                if (statusCode1 == 200
                        && (statusCode2 == 301 || statusCode2 == 302 || statusCode2 == 306 || statusCode2 == 307)) {
                    loc = tempUrlConn.getHeaderField("Location");
                    logger.info("tempurl  location is  :" + loc);
                    if (loc.equals(url + "/") || loc.equals(url)) {
                        wwwResolved = true;
                    }
                } else if ((statusCode1 == 301 || statusCode1 == 302 || statusCode1 == 306 || statusCode1 == 307)
                        && statusCode2 == 200) {
                    String loction = fullwwwUrlConn.getHeaderField("Location");
                    logger.info("loction of full url is  :" + loction);
                    if (loction.equals(tempUrlConn.getURL().toString())
                            || loction.equals(tempUrlConn.getURL().toString() + '/')) {
                        wwwResolved = true;
                    }
                }
            } catch (Exception e3) {
                logger.error("Exception", e3);
            }
        } catch (MalformedURLException me) {
            logger.error("MalformedURLException", me);
        } catch (Exception e3) {
            logger.error("Exception", e3);
        }
        return wwwResolved;
    }

    public static JSONArray sendPostURLwithJSON(String url, JSONArray postData) throws Exception {
        HttpURLConnection httpcon = (HttpURLConnection) ((new URL(url).openConnection()));
        httpcon.setDoOutput(true);
        httpcon.setRequestProperty("Content-Type", "application/json");
        httpcon.setRequestProperty("Accept", "application/json");
        httpcon.setRequestMethod("POST");
        httpcon.connect();

        byte[] outputBytes = postData.toString().getBytes("UTF-8");
        try (OutputStream os = httpcon.getOutputStream()) {
            os.write(outputBytes);
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        JSONArray array = new JSONArray(response.toString());
        return array;
    }

    public static int checkMobileCompatibility(String mobileDomainURL) {
        int mobileCompatible = 0;
        HttpURLConnection currentUrlConn = null;
        int responseCode = 0;
        URL currentUrlObj = null;
        try {
            currentUrlObj = new URL(mobileDomainURL);

            if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
            }
        } catch (UnknownHostException e) {
            logger.error("UnknownHostException for " + mobileDomainURL);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException for " + mobileDomainURL);
        } catch (IOException e) {
            logger.error("IOException for " + mobileDomainURL);
        }
        if (responseCode == 200) {
            mobileCompatible = 100;
        } else if (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {
            int redirectResponseCode = 0;
            String redirectedLocation = currentUrlConn.getHeaderField("Location");
            String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
            if (redirectedLocation != null) {
                if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                    if (redirectedLocation.startsWith("/")) {
                        redirectedLocation = tempUrl + redirectedLocation;
                    } else if (!(redirectedLocation.startsWith("/"))) {
                        if (mobileDomainURL.endsWith("/")) {
                            redirectedLocation = mobileDomainURL + redirectedLocation;
                        } else {
                            redirectedLocation = mobileDomainURL + "/" + redirectedLocation;
                        }
                    }
                }

                HttpURLConnection currentUrlConn2 = null;
                try {
                    URL currentUrlObj2 = new URL(redirectedLocation);
                    if (!(currentUrlObj2.equals(" ") || currentUrlObj2.equals("null"))) {
                        currentUrlConn2 = (HttpURLConnection) currentUrlObj2.openConnection();
                        currentUrlConn2.setInstanceFollowRedirects(false);
                        currentUrlConn2.setConnectTimeout(50000);
                        currentUrlConn2.setReadTimeout(10000);
                        currentUrlConn2.setRequestProperty("User-Agent", USER_AGENT);
                        redirectResponseCode = currentUrlConn2.getResponseCode();
                    }
                } catch (UnknownHostException e) {
                    logger.error("UnknownHostException for " + redirectedLocation);
                } catch (MalformedURLException e) {
                    logger.error("MalformedURLException for " + redirectedLocation);
                } catch (IOException e) {
                    logger.error("IOException for " + redirectedLocation);
                }
            }
            switch (redirectResponseCode) {
                case 200:
                    mobileCompatible = 100;
                    break;
                case 301:
                case 302:
                case 306:
                case 307:
                    mobileCompatible = 50;
                    break;
                default:
                    mobileCompatible = 0;
                    break;
            }
        }
        logger.debug("mobileCompatible score is :" + mobileCompatible);
        return mobileCompatible;
    }

    public static String[] createMobileDomain(String domain) throws UnknownHostException {
        String[] domainFormats = new String[6];
        int formatCount = 0;
        String domainHost = "";
        URL domainURL = null;
        if (!domain.startsWith("http")) {
            domain = "http://" + domain;
        }
        try {
            domainURL = new URL(domain);
        } catch (MalformedURLException e) {
            logger.error(e);
        }
        if (domainURL != null) {
            domainHost = domainURL.getHost();
            String tempDomain = domainHost.replace("www", "m");
            String dmnPrfxMobile = domainHost.replace("www", "Mobile");
            String dmnStartsMobile = domainHost.replace("www", "mobile");
            String dmnPrfxMobi = domainHost.substring(0, domainHost.lastIndexOf(".") + 1);

            if (domainHost.contains(".co.")) {
                int dot1 = domainHost.indexOf(".co.");
                dmnPrfxMobi = domainHost.substring(0, dot1 + 1);
            }
            if (!tempDomain.startsWith("m")) {
                tempDomain = "m." + tempDomain;
            }
            if (!dmnPrfxMobile.startsWith("Mobile")) {
                dmnPrfxMobile = "Mobile." + dmnPrfxMobile;
            }
            if (!dmnStartsMobile.startsWith("mobile")) {
                dmnStartsMobile = "mobile." + dmnStartsMobile;
            }
            domainFormats[formatCount++] = "http://" + tempDomain;
            domainFormats[formatCount++] = "http://" + dmnPrfxMobile;
            domainFormats[formatCount++] = "http://" + dmnStartsMobile;
            domainFormats[formatCount++] = "http://" + dmnPrfxMobi + "mobi";
            domainFormats[formatCount++] = "http://" + domainHost + "/mobile";
            domainFormats[formatCount++] = "http://" + domainHost + "/iphone";
        }
        return domainFormats;
    }

    public static int checkMobileCompatabilityScore(String domain) {
        String[] domainFormats = new String[6];
        int iteration = 0;
        int tempVal = 0;
        try {
            domainFormats = createMobileDomain(domain);
            while (iteration < domainFormats.length && tempVal < 100) {
                int val = checkMobileCompatibility(domainFormats[iteration]);
                if (val > tempVal) {
                    tempVal = val;
                }
                iteration++;
            }
        } catch (Exception e1) {
            logger.error("Exception as", e1);
        }
        return tempVal;
    }

    public static boolean checkSitemapPresenceByURL(String domain) {
        boolean sitemapPresent = false;

        if (!domain.startsWith("http")) {
            domain = "http://" + domain;
        }
        if (domain.endsWith("/")) {
            domain = domain.substring(0, domain.length() - 1);
        }
        String[] sitemapExtensions = {"sitemap.xml", "Sitemap.xml", "sitemap", "Sitemap", "sitemap.html", "Sitemap.html", "Sitemap_Index.xml", "sitemap_index.xml",};
        for (String sitemapExt : sitemapExtensions) {
            HttpURLConnection currentUrlConn = null;
            int responseCode = 0;
            try {
                URL currentUrlObj = new URL(domain + "/" + sitemapExt);
                if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                    currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                    currentUrlConn.setInstanceFollowRedirects(true);
                    currentUrlConn.setConnectTimeout(IMPORTANT_CONNECT_TIME_OUT);
                    currentUrlConn.setReadTimeout(IMPORTANT_READ_TIME_OUT);
                    currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                    responseCode = currentUrlConn.getResponseCode();
                }
                if (responseCode == 200 && currentUrlConn.getURL().toString().toLowerCase().contains("sitemap")) {
                    sitemapPresent = true;
                    break;
                }
            } catch (MalformedURLException e) {
                logger.error("MalformedURLException as", e);
            } catch (IOException e) {
                logger.error("IOException as", e);
            } catch (Exception e) {
                logger.error("Exception as", e);
            }
        }
        return sitemapPresent;
    }

    public static boolean checkSitemapPresenceByRobotsTxtMention(BaseRobotRules baseRobotRules) {
        boolean sitemapPresent = false;;
        HttpURLConnection currentUrlConn = null;
        if (baseRobotRules != null) {
            List<String> siteMaps = baseRobotRules.getSitemaps();
            int count = 0;
            while (siteMaps.size() > count && !sitemapPresent) {
                String sitemap = siteMaps.get(count);
                int responseCode = 0;
                try {
                    URL currentUrlObj = new URL(sitemap);
                    if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                        currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                        currentUrlConn.setInstanceFollowRedirects(true);
                        currentUrlConn.setConnectTimeout(50000);
                        currentUrlConn.setReadTimeout(10000);
                        currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                        responseCode = currentUrlConn.getResponseCode();
                    }
                } catch (MalformedURLException e) {
                    logger.error(e);
                } catch (IOException e) {
                    logger.error(e);
                }
                if (responseCode == 200) {
                    sitemapPresent = true;
                }
                count++;
            }
        }
        return sitemapPresent;
    }

    public static boolean checkErrorPagePresence(String domain) {
        //Assuming websites should have links back to their website from error page
        boolean errorPagePresent = false;
        String domainURL = domain;
        if (!domainURL.startsWith("http")) {
            domainURL = "http://" + domainURL;
        }
        try {
            domain = getDomainName(domainURL);
        } catch (URISyntaxException e1) {
            logger.error(e1);
            return false;
        }
        if (domainURL.endsWith("/")) {
            domainURL = domainURL.substring(0, domainURL.length() - 1);
        }

        int responseCode = 0;
        StringBuilder responseStrBuilder = null;
        if (responseCode != 404) {
            String randomString = "iqw6ugiuwqghe72323roy3r87632fhijhkf";
            String errorURL = domainURL + "/" + randomString;
            HttpURLConnection currentUrlConn = null;
            try {
                URL currentUrlObj = new URL(errorURL);
                if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                    currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                    currentUrlConn.setInstanceFollowRedirects(true);
                    currentUrlConn.setConnectTimeout(50000);
                    currentUrlConn.setReadTimeout(10000);
                    currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                    responseCode = currentUrlConn.getResponseCode();
                    if (responseCode == 404) {
                        BufferedReader br = new BufferedReader(new InputStreamReader((currentUrlConn.getErrorStream())));
                        String inputStr;
                        responseStrBuilder = new StringBuilder();
                        while ((inputStr = br.readLine()) != null) {
                            responseStrBuilder.append(inputStr);
                        }
                    }
                }
            } catch (MalformedURLException e) {
                logger.error(e);
            } catch (IOException e) {
                logger.error(e);
            }
        }
        if (responseStrBuilder != null && !responseStrBuilder.toString().matches("\\s*")) {
            Document doc = Jsoup.parse(responseStrBuilder.toString(), domainURL);
            if (doc != null) {
                Elements links = doc.select("a[href]");
                for (Element link : links) {
                    String childURL = link.attr("href");
                    if (!childURL.startsWith("#")) {
                        if (!childURL.trim().equals("")) {
                            childURL = link.attr("abs:href");
                            if (childURL.contains(domain)) {
                                errorPagePresent = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return errorPagePresent;
    }

    // currency symbols provided by analytics	
    public static HashMap<String, String> loadCurrencySymbols() {
        HashMap<String, String> currencySymbol = new LinkedHashMap<String, String>();
        currencySymbol.put("USD", "$");
        currencySymbol.put("AED", " ");
        currencySymbol.put("ARS", "$");
        currencySymbol.put("AUD", "$");
        currencySymbol.put("BGN", "лв");
        currencySymbol.put("BOB", "$b");
        currencySymbol.put("BRL", "R$");
        currencySymbol.put("CAD", "$");
        currencySymbol.put("CHF", "Fr");
        currencySymbol.put("CLP", "$");
        currencySymbol.put("CNY", "¥");
        currencySymbol.put("COP", "$");
        currencySymbol.put("CZK", "Kč");
        currencySymbol.put("DKK", "kr");
        currencySymbol.put("EGP", "£");
        currencySymbol.put("EUR", "€");
        currencySymbol.put("FRF", " ");
        currencySymbol.put("GBP", "£");
        currencySymbol.put("HKD", "£");
        currencySymbol.put("HRK", " ");
        currencySymbol.put("HUF", "Ft");
        currencySymbol.put("IDR", "Rp");
        currencySymbol.put("ILS", "₪");
        currencySymbol.put("INR", "Rs");
        currencySymbol.put("JPY", "¥");
        currencySymbol.put("KRW", "₩");
        currencySymbol.put("LTL", "Lt");
        currencySymbol.put("MAD", " ");
        currencySymbol.put("MXN", "$");
        currencySymbol.put("MYR", "RM");
        currencySymbol.put("NOK", "Kr");
        currencySymbol.put("NZD", " ");
        currencySymbol.put("PEN", "S/.");
        currencySymbol.put("PHP", "₱");
        currencySymbol.put("PKR", " ");
        currencySymbol.put("PLN", "zł");
        currencySymbol.put("RON", " ");
        currencySymbol.put("RSD", "Дин.");
        currencySymbol.put("RUB", "руб");
        currencySymbol.put("SAR", "﷼");
        currencySymbol.put("SEK", " ");
        currencySymbol.put("SGD", "$");
        currencySymbol.put("THB", "฿");
        currencySymbol.put("TRL", "₤");
        currencySymbol.put("TWD", "NT$");
        currencySymbol.put("UAH", "₴");
        currencySymbol.put("VEF", "Bs");
        currencySymbol.put("VND", "₫");
        currencySymbol.put("ZAR", " ");

        return currencySymbol;
    }

    public static String addWaterMark(String fileName, String downloadFolder) {
        try {
            PdfReader Read_PDF_To_Watermark = new PdfReader(downloadFolder + fileName);
            fileName = Common.createFileName("LXRSEO - Weekly_Campaign_Report_and_Tasks_Update");
            fileName = fileName + ".pdf";
            PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, new FileOutputStream("/disk2/lxrmarketplace/download/" + fileName + ""));
            PdfContentByte underContent1 = stamp.getUnderContent(1);

            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);

            PdfGState gs = new PdfGState();
            gs.setFillOpacity(0.8f);
            underContent1.beginText();
            underContent1.setGState(gs);
            underContent1.setFontAndSize(bf, 40);
            underContent1.setRGBColorFill(224, 224, 224);
            underContent1.showTextAligned(com.itextpdf.text.Element.ALIGN_CENTER, "www.lxrmarketplace.com", 330, 650, 0);
            underContent1.endText();

            stamp.close();
        } catch (IOException | DocumentException i1) {
            logger.error(i1);
        }

        return fileName;
    }

    public static String formattingDate(Timestamp tempDate) {
        final SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDt = sdf1.format(tempDate);
        return formattedDt;
    }

    public static String getDateDiffInYrsAndMnts(Calendar createdCal2) {
        Calendar currCal1 = Calendar.getInstance();
        int curYear = currCal1.get(Calendar.YEAR);
        int curMonth = currCal1.get(Calendar.MONTH);
        int curDay = currCal1.get(Calendar.DAY_OF_MONTH);

        int year = createdCal2.get(Calendar.YEAR);
        int month = createdCal2.get(Calendar.MONTH);
        int day = createdCal2.get(Calendar.DAY_OF_MONTH);

        int y = curYear - year;
        int m = 0;
        int d = 0;
        if (curMonth < month) {
            y--;
            if (curDay < day) {
                m = curMonth + 11 - month;
                d = curDay + 30 - day;
            } else {
                m = curMonth + 12 - month;
                d = curDay - day;
            }
        } else if (month == curMonth) {
            if (curDay < day) {
                y--;
                m = curMonth + 11 - month;
                d = curDay + 30 - day;
            } else {
                m = 0;
                d = curDay - day;
            }
        } else if (curDay < day) {
            m = curMonth - (month + 1);
            d = curDay + 30 - day;
        } else {
            m = curMonth - month;
            d = curDay - day;
        }
        String finalDt = y + " Years " + m + " Months " + d + " Days";
        return finalDt;
    }

    public static boolean readandSaveImageFromUrl(String inputurl, String path) {
        boolean imagestatus = false;
        try {
            URL url = new URL(inputurl);
            BufferedImage image = ImageIO.read(url);
            File f = new File(path);
            if (f.exists()) {
                f.deleteOnExit();
                saveImage(image, image.getType(), path);
            } else {
                saveImage(image, image.getType(), path);
            }

            imagestatus = true;
        } catch (IOException e) {
            imagestatus = false;
            logger.error("Exception in ReadandSaveImageFromUrl", e);
        }
        return imagestatus;

    }

    public static boolean resizeImageFromUrlandSave(String inputurl, String path) {
        int IMG_WIDTH;
        int IMG_HEIGHT;
        boolean imagestatus;
        try {
            URL url = new URL(inputurl);
            BufferedImage originalImage = ImageIO.read(url);
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            try {

                if ((originalImage.getHeight() >= 200) || (originalImage.getWidth() >= 200)) {
                    IMG_WIDTH = 200;
                    IMG_HEIGHT = 200;
                } else if ((originalImage.getHeight() >= 100) || (originalImage.getWidth() >= 100)) {
                    IMG_WIDTH = 100;
                    IMG_HEIGHT = 100;
                } else if (((originalImage.getHeight() >= 50) || (originalImage.getWidth() >= 50))) {
                    IMG_WIDTH = 50;
                    IMG_HEIGHT = 50;
                } else {
                    IMG_WIDTH = 50;
                    IMG_HEIGHT = 50;
                }
            } catch (Exception e) {
                IMG_WIDTH = 50;
                IMG_HEIGHT = 50;
                logger.error("exception in processing IMG_WIDTH and IMG_HEIGHT", e);
            }
            BufferedImage resizeImageJpg = resizeImage(originalImage, type, IMG_WIDTH, IMG_HEIGHT);
            File f = new File(path);
            if (f.exists()) {
                f.deleteOnExit();
                saveImage(resizeImageJpg, resizeImageJpg.getType(), path);
            } else {
                saveImage(resizeImageJpg, resizeImageJpg.getType(), path);
            }
            return true;
        } catch (Exception e) {
            imagestatus = false;
            logger.error("Exception in resizeImageFromUrlandSave in Common Class", e);
        }
        return imagestatus;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        return resizedImage;
    }

    public static void saveImage(BufferedImage resizeImageJpg, int format, String path) {
        try {
            switch (format) {
                case 5:
                    ImageIO.write(resizeImageJpg, "jpg", new File(path));
                    break;
                case 6:
                    ImageIO.write(resizeImageJpg, "png", new File(path));
                    break;
                default:
                    ImageIO.write(resizeImageJpg, "jpg", new File(path));
                    break;
            }
        } catch (IOException e) {
            logger.error("Exception in save image: ", e);
        }
    }

    public static String getImageFormat(String imageUrl) {
        String imageFormat;
        try {
            URL url = new URL(imageUrl);
            BufferedImage originalImage = ImageIO.read(url);
            switch (originalImage.getType()) {
                case 5:
                    imageFormat = "jpg";
                    break;
                case 6:
                    imageFormat = "png";
                    break;
                default:
                    imageFormat = "jpg";
                    break;
            }
        } catch (IOException e) {
            imageFormat = "jpg";
            logger.error("Exception in getImageFormat: ", e);
        }
        return imageFormat;
    }

    public static int checkImageUrl(String url) {
        int status = 0;
        try {
            HttpGet httpget = new HttpGet(url);
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse httpResponse = httpClient.execute(httpget);
            status = httpResponse.getStatusLine().getStatusCode();
        } catch (IOException e) {
            logger.error("Exception in checkImageUrl: ", e);
        }
        return status;
    }

    public static String getDateFormatFromGoogleandYouTube(String jsonDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        String dateInString = jsonDate;
        String finalDate = null;
        try {
            Date date = formatter.parse(dateInString);
            finalDate = sdf.format(date);
        } catch (ParseException e) {
            logger.error("Exception in getDateFormatFromGoogleYouTubeAPI: ", e);
        }
        return finalDate;
    }

    public static String getFormatedDateForTwitter(String twitterDate) {
        /*logger.info("In getDateFormatFromTwitterandFacebook");*/
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        String finalDate = null;
        try {
            finalDate = sdf.format(formatter.parse(twitterDate));
        } catch (ParseException e) {
            logger.error("Exception for getDisplayFormatFromTwitter", e);
        }
        return finalDate;
    }

    /*End of Common Class*/
    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * To Check JSONObject is Existed or Not
     *
     * @param dataArray
     * @return
     */
    public static JSONObject checkJSONObjectInJSONArrayResponse(JSONArray dataArray) {
        JSONObject userExistence = null;
        try {
            userExistence = dataArray.getJSONObject(0);
        } catch (JSONException e) {
            logger.error("JSONException in checkJSONObjectInJSONArrayResponse cause:" + e.getMessage());
        }
        return userExistence;
    }

    public static JSONObject checkJSONObjectInJSONObjectResponse(JSONObject infoObj, String paramName) {
        JSONObject objName = null;
        try {
            objName = infoObj.getJSONObject(paramName);
        } catch (JSONException e) {
            logger.error("JSONException in checkJSONObjectInJSONObjectResponse cause:" + e.getMessage());
        }
        return objName;
    }

    public static JSONArray checkJSONArrayInJSONObjectResponse(JSONObject infoObj, String paramName) {
        JSONArray objName = null;
        try {
            objName = infoObj.getJSONArray(paramName);
        } catch (JSONException e) {
            logger.error("JSONException in checkJSONArrayInJSONObjectResponse cause:" + e.getMessage());
        }
        return objName;
    }

    public static JSONArray checkJSONArrayInJSONArrayResponse(JSONArray infoArray, int index) {
        JSONArray objName = null;
        try {
            objName = infoArray.getJSONArray(index);
        } catch (JSONException e) {
            logger.error("JSONException in checkJSONArrayInJSONArrayResponse cause:" + e.getMessage());
        }
        return objName;
    }

    public static String checkStringInJSONObjectResponse(JSONObject infoObj, String paramName) {
        String paramVal = null;
        try {
            paramVal = infoObj.getString(paramName);
        } catch (JSONException e) {
            logger.error("JSONException in checkStringInJSONObjectResponse cause:" + e.getMessage());
        }
        return paramVal;
    }

    public static JSONObject getResponseFromAPI(String url) {
        JSONObject responseJson = null;
        try {
            HttpGet httpget = new HttpGet(url);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpget);
            BufferedReader streamReader = new BufferedReader(
                    new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            responseJson = new JSONObject(responseStrBuilder.toString());
        } catch (IOException | UnsupportedOperationException | JSONException e) {
            logger.error("Exception for getResponseFromAPI", e);
        }
        return responseJson;
    }

    /*To calculate AudienceEngaged for a Post for SocialMediaAnalyzer*/
    public static double getAudienceEngaged(long engagement, long followers) {
        double audienceEngaged = 0.0;
        try {
            double tempaudienceEngaged = (double) Common.round(((engagement * 100.0 / followers)), 6);
            audienceEngaged = Double.parseDouble(DECIMAL_FORMAT.format(tempaudienceEngaged));
        } catch (NumberFormatException e) {
            logger.error("Exception for AudienceEngaged: " + e.getMessage());
        }
        return audienceEngaged;
    }

    public static String convertTimeStampintoDate(Timestamp timestamp) {
        String date = null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            long milliSeconds = timestamp.getTime();
            Date currentDate = new Date(milliSeconds);
            date = df.format(currentDate);
        } catch (Exception e) {
            Date dateTemp = Calendar.getInstance().getTime();
            date = df.format(dateTemp);
            logger.error("Exception for convertTimeStampintoDate:" + e.getMessage());
        }
        return date;
    }

    public static String checkURLAndGetHTMLV2(String requestURL) {
        try {
            // Checking redirection to get URLs with too many redirection
            String currentUrl = requestURL;
            if (currentUrl != null || !(currentUrl.equals(""))) {
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                int statusCode = 0;
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    statusCode = getStatusCodeAndHTMLV2(httpCurrentUrl, redirectedUrl);
                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    statusCode = getStatusCodeAndHTMLV2(httpCurrentUrl, redirectedUrl);
                }
                logger.debug("Current URL: " + httpCurrentUrl + " Redirected URL: " + redirectedUrl + " Status Code" + statusCode);
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "too many redirection";
                } else if (statusCode == 200) {
                    return httpCurrentUrl;
                } else if (statusCode != 200) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCodeAndHTMLV2(httpCurrentUrl, redirectedUrl);
                        if (!redirectedUrl.toString().equals("")) {
                            return redirectedUrl.toString();
                        } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                            return "too many redirection";
                        } else if (statusCode == 200) {
                            return httpCurrentUrl;
                        }
                    }
                }
                statusCode = 0;
                redirectedUrl = new StringBuilder("");
                String httpsCurrentUrl = "";
                if (currentUrl.startsWith("https://")) {
                    httpsCurrentUrl = currentUrl;
                    statusCode = getStatusCodeAndHTMLV2(httpsCurrentUrl, redirectedUrl);
                } else if (!currentUrl.startsWith("http://")) {
                    httpsCurrentUrl = "https://" + currentUrl;
                    statusCode = getStatusCodeAndHTMLV2(httpsCurrentUrl, redirectedUrl);
                }
                if (!redirectedUrl.toString().equals("")) {
                    return redirectedUrl.toString();
                } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                    return "too many redirection";
                } else if (statusCode == 200) {
                    return httpsCurrentUrl;
                } else if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                    httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                    statusCode = getStatusCodeAndHTMLV2(httpsCurrentUrl, redirectedUrl);
                    if (!redirectedUrl.toString().equals("")) {
                        return redirectedUrl.toString();
                    } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                        return "too many redirection";
                    } else if (statusCode == 200) {
                        return httpsCurrentUrl;
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            } else {
                return "invalid";
            }
        } catch (Exception e) {
            logger.error("Exception in checkURLAndGetHTML", e);
        }
        return "";
    }

    public static int getStatusCodeAndHTMLV2(String currentUrl, StringBuilder redirectedUrl) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn = null;
        try {
            URL currentUrlObj = new URL(currentUrl);
            if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(50000);
                currentUrlConn.setReadTimeout(10000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                responseCode = currentUrlConn.getResponseCode();
                logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                if (responseCode == 301 || responseCode == 302
                        || responseCode == 306 || responseCode == 307) {
                    String redirectedLocation = currentUrlConn.getHeaderField("Location");
                    String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                    if (redirectedLocation != null) {
                        if (!(redirectedLocation.startsWith(currentUrlObj.getProtocol()))) {
                            if (redirectedLocation.startsWith("/")) {
                                redirectedLocation = tempUrl + redirectedLocation;
                            } else if (!(redirectedLocation.startsWith("/"))) {
                                if (currentUrl.endsWith("/")) {
                                    redirectedLocation = currentUrl + redirectedLocation;
                                } else {
                                    redirectedLocation = currentUrl + "/" + redirectedLocation;
                                }
                            }
                        }
                        URL redirectedUrlObj = new URL(redirectedLocation);
                        try {
                            HttpURLConnection redirectedUrlConn = (HttpURLConnection) redirectedUrlObj.openConnection();
                            redirectedUrlConn.setInstanceFollowRedirects(false);
                            redirectedUrlConn.setConnectTimeout(50000);
                            redirectedUrlConn.setReadTimeout(10000);
                            redirectedUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                            responseCode = redirectedUrlConn.getResponseCode();
                            if (responseCode == 200) {

                                redirectedUrl.append(redirectedLocation);

                            } else if (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307) {

                                String redirectedLocation1 = redirectedUrlConn.getHeaderField("Location");
                                String tempURL1 = redirectedUrlObj.getProtocol() + "://" + redirectedUrlObj.getHost();
                                if (redirectedLocation1 != null) {
                                    if (!(redirectedLocation1.startsWith(currentUrlObj.getProtocol()))) {
                                        if (redirectedLocation1.startsWith("/")) {
                                            redirectedLocation1 = tempURL1 + redirectedLocation1;
                                        } else if (!(redirectedLocation1.startsWith("/"))) {
                                            if (redirectedLocation.endsWith("/")) {
                                                redirectedLocation1 = redirectedLocation + redirectedLocation1;
                                            } else {
                                                redirectedLocation1 = redirectedLocation + "/" + redirectedLocation1;
                                            }
                                        }
                                    }
                                    URL tempRedirectedUrlObj = new URL(redirectedLocation1);
                                    try {
                                        HttpURLConnection redirectedUrlConn2 = (HttpURLConnection) tempRedirectedUrlObj.openConnection();
                                        redirectedUrlConn2.setRequestProperty("User-Agent", USER_AGENT);
                                        responseCode = redirectedUrlConn2.getResponseCode();
                                        if (responseCode == 200 || responseCode == 301 || responseCode == 302 || responseCode == 307) {
                                            redirectedUrl.append(redirectedLocation1);
                                        }
                                    } catch (Exception e1) {
                                        logger.error("Exception in appending page url for redirected url in getStatusCodeAndHTML", e1);
                                    }

                                }
                            }
                        } catch (IOException e) {
                            logger.error("Exception in chechking status code for redirected url in getStatusCodeAndHTML", e);
                        }
                    }
                }
            }
        } catch (IOException e2) {
            logger.error("Error  in connecting getStatusCodeAndHTML " + currentUrl + " Cause: ", e2);
        }
        return responseCode;
    }

    public static boolean checkDateBetweenTwoDates(Date todayDate, Date promoStartDate, Date promoEndDate) {

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTime(todayDate);
        todayCalendar.set(Calendar.MILLISECOND, 0);
        todayCalendar.set(Calendar.SECOND, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);

        Calendar promoStartCal = Calendar.getInstance();
        promoStartCal.setTime(promoStartDate);
        promoStartCal.set(Calendar.MILLISECOND, 0);
        promoStartCal.set(Calendar.SECOND, 0);
        promoStartCal.set(Calendar.MINUTE, 0);
        promoStartCal.set(Calendar.HOUR_OF_DAY, 0);

        Calendar promoEndCal = Calendar.getInstance();
        promoEndCal.setTime(promoEndDate);
        promoEndCal.set(Calendar.MILLISECOND, 0);
        promoEndCal.set(Calendar.SECOND, 0);
        promoEndCal.set(Calendar.MINUTE, 0);
        promoEndCal.set(Calendar.HOUR_OF_DAY, 0);

        /**
         * It returns true when PromoCode entered date is in between start date
         * and end date otherwise false *
         */
        return (todayCalendar.getTime().after(promoStartCal.getTime()) || todayCalendar.getTime().equals(promoStartCal.getTime()))
                && (todayCalendar.getTime().before(promoEndCal.getTime()) || todayCalendar.getTime().equals(promoEndCal.getTime()));

    }

    /*To acheive short answer from orginal answer content for Knowledge Base Questioon.*/
    public static String convertHtmlToText(String html) {
        String filterdHtml = null;
        try {
            filterdHtml = Jsoup.parse(html).text();
            return filterdHtml;
        } catch (Exception e) {
            filterdHtml = html;
            logger.error("Exception in convertHtmlToText", e);
        }
        return filterdHtml;
    }

    public static String convertTimeStampintoDisplayDate(Timestamp timestamp) {
        String date = null;
        try {
            long milliSeconds = timestamp.getTime();
            Date currentDate = new Date(milliSeconds);
            DateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm");
            date = df.format(currentDate);
        } catch (Exception e) {
            date = "-";
            logger.error("Exception in convertTimeStampintoDate is", e);
        }
        return date;
    }

    public static String convertTimeStampintoString(Timestamp timestamp) {
        String date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date(timestamp.getTime()));
        } catch (Exception e) {
            logger.error("Exception for  convertTimeStampintoString: ", e);
        }
        return date;
    }

    public static long stringDatetoLongMilli(SimpleDateFormat df, String date) {
        long milliseconds = 0;
        try {
            Date d = df.parse(date);
            milliseconds = d.getTime() / 1000;
        } catch (ParseException e) {
            logger.error("Exception for  stringDatetoLongMilli: ", e);
        }
        return milliseconds;
    }

    public static Document getDocumentFromHTML(String pageHTML, String url) {
        String httpDomainUrl = "";
        if (url.startsWith("http://") || url.startsWith("https://")) {
            httpDomainUrl = url;
        } else {
            httpDomainUrl = "http://" + url;
        }
        return Jsoup.parse(pageHTML, httpDomainUrl);
    }

    /*Calculates Text/Code Ratio Metric from a document object for a url.
        Classes: TopMostKeywordCheckerService */
    public static double getTextHtmlratio(Document doc) {
        String temp = calculateTextHtmlRatio(doc);
        temp = temp.replace('%', ' ');
        double textHtmlratio = Double.parseDouble((temp).trim());
        return textHtmlratio;
    }

    public static String calculateTextHtmlRatio(Document doc) {
        double textHtmlratio = 0.0;
        double textlen = 0.0;
        double htmllen = 0.0;
        double titlelen = 0.0;
        double ratio = 0.0;
        try {
            textlen = doc.select("body").text().length();
            titlelen = doc.select("title").text().length();
            htmllen = doc.select("body").toString().length();
            if (htmllen > 0.0) {
                ratio = ((textlen + titlelen) / htmllen) * 100;
            }
            textHtmlratio = Double.parseDouble(new DecimalFormat("#.##").format(ratio));
        } catch (NumberFormatException ne) {
            logger.error("NumberFormatException in calculateTextHtmlRatio", ne);
        } catch (Exception e) {
            logger.error("Exception in calculateTextHtmlRatio", e);
        }
        return textHtmlratio + " " + "%";
    }

    /*Return required number of elements selected from list of map objcets.
    maxSize: count to fetch number of elements from each map of list.
    total size : Total nomber of elements should result map contain. */
    public static Map<String, Integer> getMaxElementsFromList(List<Map<String, PhraseStat>> keywordsList, int maxSize, int totalSize) {
        Map<String, Integer> resultMap = new LinkedHashMap<>();
        boolean extraFlag = false;
        Map<String, PhraseStat> keywordsMap = null;
        int totalLoopCount = 0;
        while (resultMap.size() != totalSize && totalLoopCount <= totalSize) {
            outer:
            for (int listIndex = 0; listIndex < keywordsList.size(); listIndex++) {
                keywordsMap = keywordsList.get(listIndex);
                int counter = 0;
                for (Map.Entry<String, PhraseStat> wordsEntry : keywordsMap.entrySet()) {
                    counter++;
                    if (resultMap.containsKey(wordsEntry.getKey())) {
                        continue;
                    }
                    resultMap.put(wordsEntry.getKey(), wordsEntry.getValue().count);
                    if (resultMap.size() == totalSize) {
                        break outer;
                    }
                    if (maxSize == counter) {
                        /* This is for any one of them map contains more than expected records. */
                        if (keywordsMap.size() - maxSize > 0 && !extraFlag) {
                            extraFlag = true;
                        }
                        break;
                    }
                }
            }
            totalLoopCount++;
        }
        return resultMap;
    }

    /*Return required number of elements selected from list of map objcets.
    maxSize: count to fetch number of elements from each map of list.
    total size : Total nomber of elements should result map contain. */
    public static Map<String, Integer> getMaxElementsFromMap(List<Map<String, Integer>> keywordsList, int maxSize, int totalSize) {
        Map<String, Integer> resultMap = new LinkedHashMap<>();
        boolean extraFlag = false;
        /**
         * Changing the limit for first map object in list by n+1. To fetch top
         * n+1 for single keywords phrases and n elements from other keywords
         * phrases.
         */
        int roundMaxSize = maxSize;
        Map<String, Integer> keywordsMap = null;
        for (int listIndex = 0; listIndex < keywordsList.size(); listIndex++) {
            if (listIndex != 0 && roundMaxSize != maxSize) {
                roundMaxSize = roundMaxSize - 1;
            } else if (listIndex == 0) {
                roundMaxSize = roundMaxSize + 1;
            }
            keywordsMap = keywordsList.get(listIndex);
            int counter = 0;
            for (Map.Entry<String, Integer> wordsEntry : keywordsMap.entrySet()) {
                counter++;
                resultMap.put(wordsEntry.getKey(), wordsEntry.getValue());
                if (roundMaxSize == counter) {
                    // this is for any one of them map contains more than expected records
                    if (keywordsMap.size() - maxSize > 0 && !extraFlag) {
                        extraFlag = true;
                    }
                    break;
                }
            }
        }

        if (resultMap.size() != totalSize && extraFlag) {
            outer:
            for (int count = 0; count < keywordsList.size(); count++) {
                keywordsMap = keywordsList.get(count);
                for (Map.Entry<String, Integer> wordsEntry : keywordsMap.entrySet()) {
                    if (resultMap.containsKey(wordsEntry.getKey())) {
                        continue;
                    }
                    resultMap.put(wordsEntry.getKey(), wordsEntry.getValue());
                    if (resultMap.size() == totalSize) {
                        break outer;
                    }
                }
            }
        }
        return resultMap;
    }

    /*Return path from crawling URL. */
    public static String getPathFromUrl(String crawlinURL) {
        String subURL = "";
        try {
            URL url = new URL(crawlinURL);
            /*Checking for query parameters*/
            if (url.getQuery() != null) {
                subURL = url.getPath() + "?" + url.getQuery();
            } else {
                /*To return subDomain for a give URL*/
                subURL = url.getPath();
            }
            if (subURL.equals("/")) {
                subURL = "";
            }
        } catch (StringIndexOutOfBoundsException e) {
            /*If path is not existed for url causes to StringIndexOutOfBoundsException.*/
            logger.error("StringIndexOutOfBoundsException in getPathFromUrl" + e);
        } catch (MalformedURLException e) {
            /*To handle MalformedURLException.*/
            logger.error("MalformedURLException in getPathFromUrl" + e);
        }
        return subURL;
    }

    /*Returns the total word count from a body text(Document object) of a URL.*/
    public static long getTotalWordCount(String content) {
        int index = 0;
        long numWords = 0;
        char c1 = 0;
        try {
            if (content != null) {
                boolean prevwhitespace = true;
                while (index < content.length()) {
                    char c = content.charAt(index++);
                    if ((index + 0) < content.length()) {
                        c1 = content.charAt(index + 0);
                    }
                    boolean currwhitespace = Character.isWhitespace(c);
                    if (c == ',' && !(c1 == ' ')) {
                        numWords++;
                    }
                    if (prevwhitespace && !currwhitespace) {
                        numWords++;
                    }
                    prevwhitespace = currwhitespace;
                }
            }
        } catch (Exception ex) {
            logger.error("Exceptio in getTotalWordCount", ex);
        }
        return numWords;
    }

    /*To fetch 1,2,3,4 keywords pharses from a body text for a page.*/
    public static List<Map<String, PhraseStat>> getMeaningFullPhrases(String bodyText, List<String> stopWordsList) {
        List<Map<String, PhraseStat>> allPhrases = new ArrayList<>();
        if (bodyText != null && !bodyText.isEmpty()) {
            VariableLengthPhraseFinder variableLengthPhraseFinder = new VariableLengthPhraseFinder(bodyText, stopWordsList);
            allPhrases = variableLengthPhraseFinder.findMeaningfulPhrases();
        }
        return allPhrases;
    }

    /*Activation for LXRM User signup. */
    public static synchronized long generateActivationId() {
        long activationId = Calendar.getInstance().getTime().getTime();
        return activationId;
    }

    /*Auto generates password for inserting user record.*/
    public static String autoGeneratePassword() {
        String saltChars = "abcdefghijklmnpqrstuvwxyz";
        StringBuilder captchaStrBuilder = new StringBuilder();
        java.util.Random rnd = new java.util.Random();
        // build a random captchaLength chars salt
        while (captchaStrBuilder.length() < 6) {
            int index = (int) (rnd.nextFloat() * saltChars.length());
            captchaStrBuilder.append(saltChars.substring(index, index + 1));
        }
        return captchaStrBuilder.toString();
    }

    public static String between(String value, String a, String b) {
        // Return a substring between the two strings.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    }

    /*Remove html content from given string*/
    public static String trimHtmlTagsContent(String htmlString) {
        return Jsoup.parse(htmlString).text().trim();
    }

    /*Returns a paragraph object for inputs title and image.*/
    public static Paragraph addTitleAndImageToPDFParagraph(String channelName, String imagePath) {
        logger.debug("imagePath in addTitleAndImageToPDFParagraph is " + imagePath);
        Font elementDescFont = new Font(FontFactory.getFont("Arial", 12f, Font.BOLD, new BaseColor(38, 38, 38)));
        Paragraph heading = new Paragraph();
        try {
            Image logo = Image.getInstance(new URL(imagePath));
            logo.setSpacingBefore(10);
            logo.scaleToFit(10f, 10f);
            Chunk logoInfo = new Chunk(logo, 0, 0, true);
            Chunk channelInfo = new Chunk(" " + channelName, elementDescFont);
            heading.add(logoInfo);
            heading.add(channelInfo);
            heading.setAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            heading.setSpacingAfter(5f);
        } catch (BadElementException | IOException e) {
            logger.error("Exception in addTitleAndImageToPDFParagraph" + e);
        }
        return heading;
    }

    public static String removeProtocolForWebsites(String urlQuery) {
        String url = urlQuery;
        String trimUrl = "";
        try {
            if (url != null) {
                url = url.replaceAll("https:", "");
                url = url.replaceAll("http:", "");
                url = url.replaceAll("//", "");
            }
            if (url.lastIndexOf("/") == url.length() - 1) {
                trimUrl = url.substring(0, url.lastIndexOf("/"));
            } else {
                trimUrl = url;
            }
            return trimUrl;
        } catch (Exception e) {
            trimUrl = urlQuery;
            logger.error("Exception in trimWebsitesName" + e);
        }
        return trimUrl;
    }

    public synchronized static String generatePassword() {
        String saltChars = "abcdefghijklmnpqrstuvwxyz";
        StringBuilder captchaStrBuilder = new StringBuilder();
        java.util.Random rnd = new java.util.Random();
        // build a random captchaLength chars salt
        while (captchaStrBuilder.length() < 6) {
            int index = (int) (rnd.nextFloat() * saltChars.length());
            captchaStrBuilder.append(saltChars.substring(index, index + 1));
        }
        return captchaStrBuilder.toString();
    }

    /*Returns Host name from URL.
    Accessing in Classes: SiteMapBuilderService.
    Created Date: March 01,2017.
     */
    public static String getHostFromURL(String query) {
        String host = null;
        try {
            URL queryURL = new URL(query);
            host = queryURL.getHost();
        } catch (MalformedURLException e) {
            host = query;
            logger.error("Exception in getHostFromURL ", e);
        }
        return host;
    }

    public static String getDomainFromUrl(String finalURL) {
        String domainName = "";
        try {
            if (!finalURL.startsWith("http")) {
                finalURL = "http://".concat(finalURL);
            }
            URL urlObj = new URL(finalURL);
            domainName = urlObj.getHost();
            if (domainName.startsWith("www.")) {
                domainName = domainName.substring(4);
            }
            logger.info("Path: " + urlObj.getPath());
        } catch (MalformedURLException e) {
            logger.error("Malformed exception in url", e);
        }
        return domainName;
    }

    /*Returns count of images from a document object for a webpage.
    Accessing in Classes: PageSpeedInsightsService.
    Created Date: April 20,2017.
     */
    public static int getImageCountofDoc(String queryURL) {
        int totalImages = 0;
        Elements imagesElement = null;
        try {
            Document doc = Jsoup.connect(queryURL).get();
            imagesElement = doc.select("img");
            totalImages = imagesElement.size();
            imagesElement = doc.select("amp-img");
            totalImages = totalImages + imagesElement.size();
        } catch (Exception e) {
            logger.error("Exception in getImageCountofDoc: ", e);
        }
        return totalImages;
    }

    /* Checking file existance*/
    public static boolean checkFileExistance(String filePath) {
        try {
            new FileInputStream(new File(filePath));
            return true;
        } catch (FileNotFoundException fE) {
            logger.error("FileNotFoundException for checkFileExistance ", fE);
        } catch (Exception e) {
            logger.error("Exception for checkFileExistance ", e);
        }
        return false;
    }

    /*Fetch keyword count from doc object for a query URL*/
    public static int calculateKeywordCount(String urlString, String queryKeyword) {
        Document doc = null;
        Document docforFrames = null;
        int keyCount = 0;
        String frameSrc = null;
        String frameUrl = null;
        StringBuilder pageHTML = null;
        Elements frameElements = null;
        try {
            /*Fetching doc from source page*/
            pageHTML = new StringBuilder("");
            getStatusCodeAndHTML(urlString, new StringBuilder(""), pageHTML);
            if (!pageHTML.toString().equals("")) {
                doc = getDocumentFromHTML(pageHTML.toString(), urlString);
            }
            if (doc != null) {
                keyCount = getKeywordCountFromDoc(doc, queryKeyword);
                /*check if page have iframe and get keyword count on iframes content as well*/
                frameElements = doc.select("iframe");
                if (frameElements != null) {
                    URL url = new URL(urlString);
                    for (Element elem : frameElements) {
                        docforFrames = null;
                        frameSrc = null;
                        frameUrl = null;
                        frameSrc = elem.attr("src");
                        if (frameSrc != null) {
                            frameUrl = frameSrc;
                            if (!frameSrc.startsWith("http")) {
                                String baseUrl = new StringBuilder().append(url.getProtocol()).append("://").append(url.getHost()).toString() + "/";
                                frameUrl = baseUrl + frameSrc;
                            }
                            /*Fetching doc from frame page*/
                            pageHTML = new StringBuilder("");
                            getStatusCodeAndHTML(frameUrl, new StringBuilder(""), pageHTML);
                            if (!pageHTML.toString().equals("")) {
                                docforFrames = getDocumentFromHTML(pageHTML.toString(), urlString);
                            }
                            if (docforFrames != null) {
                                keyCount += getKeywordCountFromDoc(docforFrames, queryKeyword);
                            }
                        }
                    }
                }

                /*check if page have frame and get keyword count on frames content as well*/
                frameElements = doc.select("frame");
                if (frameElements != null) {
                    URL url = new URL(urlString);
                    for (Element elem : frameElements) {
                        docforFrames = null;
                        frameSrc = null;
                        frameUrl = null;
                        frameSrc = elem.attr("src");
                        if (frameSrc != null) {
                            String baseUrl = new StringBuilder().append(url.getProtocol()).append("://").append(url.getHost()).toString() + "/";
                            frameUrl = baseUrl + frameSrc;
                            /*Fetching doc from frame page*/
                            pageHTML = new StringBuilder("");
                            getStatusCodeAndHTML(frameUrl, new StringBuilder(""), pageHTML);
                            if (!pageHTML.toString().equals("")) {
                                docforFrames = getDocumentFromHTML(pageHTML.toString(), urlString);
                            }
                            if (docforFrames != null) {
                                keyCount += getKeywordCountFromDoc(docforFrames, queryKeyword);
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException to fetch keyword count from URL doc: ", e);
        } catch (Exception e) {
            logger.error("Exception to fetch keyword count from URL doc: ", e);
        }
        return keyCount;
    }

    public static int getKeywordCountFromDoc(Document document, String queryKeyword) {
        String data = null;
        int keywordCount = 0;
        BoyerMooreSearch boyerMooreSearch = null;
        data = document.html();
        boyerMooreSearch = new BoyerMooreSearch(queryKeyword, 1);
        keywordCount = boyerMooreSearch.find(data);
        return keywordCount;
    }

    public static String changeGreaterNum(long value) {
        String indexedVal = null;
        try {
            int exp = (int) (Math.log(value) / Math.log(1000));
            indexedVal = String.format("%.1f %c", value / Math.pow(1000, exp), "KMBT".charAt(exp - 1));
        } catch (Exception e) {
            logger.error("Exception for changeGreaterNum", e);
        }
        return indexedVal;
    }

    public static int jsoupRelatedcheckCurrentURLAndGetHTML(String analysisUrl, StringBuilder pageHTML) {
        USER_AGENT = "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";
        int StatusCode = checkCurrentURLAndGetHTML(analysisUrl, pageHTML);
        return StatusCode;
    }

    public static int checkCurrentURLAndGetHTML(String analysisUrl, StringBuilder pageHTML) {
        String currentUrl = analysisUrl;
        int statusCode = 0;
        if (!(currentUrl == null || currentUrl.equals(""))) {
            StringBuilder redirectedUrl = new StringBuilder("");
            String httpCurrentUrl = "";

            if (currentUrl.startsWith("http://") || currentUrl.startsWith("https://")) {
                httpCurrentUrl = currentUrl;
            } else {
                httpCurrentUrl = "http://" + currentUrl;
            }

            List<List<String>> cookieContainer = new ArrayList<>();
            statusCode = getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML, cookieContainer);
            logger.info("Status Code before redirection checking: " + statusCode + " currentUrl: " + currentUrl + " redirectedUrl: " + redirectedUrl.toString());

            if (statusCode / 100 == 3) {
                try {
                    URL urlredirected = new URL(redirectedUrl.toString());
                    URL urlCurrent = new URL(httpCurrentUrl);
                    if (urlredirected.getHost().equals(urlCurrent.getHost())) {
                        statusCode = getCurrentStatusCodeAndHTML(redirectedUrl.toString(), redirectedUrl, pageHTML, cookieContainer);
                    }
                } catch (MalformedURLException e) {
                    logger.error(e);
                }

            }
        }
        return statusCode;
    }

    public static int getCurrentStatusCodeAndHTML(String currentUrl, StringBuilder redirectedUrl,
            StringBuilder pageHTML, List<List<String>> cookieContainer) {

        int responseCode = 0;
        int currentResponseCode = 0;
        HttpURLConnection currentUrlConn = null;
        int iteration = 0;
        boolean updateSSLCeritficate = Boolean.FALSE;
        boolean isCertUpdated = Boolean.FALSE;
        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 5) {
            try {
                URL currentUrlObj = new URL(currentUrl);
                if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
                    currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                    currentUrlConn.setInstanceFollowRedirects(false);
                    currentUrlConn.setConnectTimeout(CONNECTION_TIME_OUT);
                    currentUrlConn.setReadTimeout(IMPORTANT_READ_TIME_OUT);
                    currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                    responseCode = currentUrlConn.getResponseCode();
                    List<String> tempCookies = currentUrlConn.getHeaderFields().get("Set-Cookie");
                    if (tempCookies != null && tempCookies.size() > 0 && cookieContainer != null) {
                        if (cookieContainer.size() > 0) {
                            cookieContainer.set(0, tempCookies);
                        } else {
                            cookieContainer.add(tempCookies);
                        }
                    }
                    if (iteration == 0) {
                        currentResponseCode = responseCode;
                    }
                    logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
                    if (responseCode / 100 == 3) {
                        if (iteration != 4) {
                            String redirectedLocation = currentUrlConn.getHeaderField("Location");
                            String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                            if (redirectedLocation != null) {
                                if (!(redirectedLocation.startsWith("http"))) {
                                    if (redirectedLocation.startsWith("/")) {
                                        currentUrl = tempUrl + redirectedLocation;
                                    } else if (!(redirectedLocation.startsWith("/"))) {
                                        if (currentUrl.endsWith("/")) {
                                            currentUrl = currentUrl + redirectedLocation;
                                        } else {
                                            currentUrl = currentUrl + "/" + redirectedLocation;
                                        }
                                    }
                                } else {
                                    currentUrl = redirectedLocation;
                                }
                            }
                        } else {
                            redirectedUrl.append("");
                        }
                    } else if (responseCode == 200) {
                        currentResponseCode = responseCode;
                        getPageHtml(currentUrlConn, pageHTML);
                        if (iteration != 0) {
                            redirectedUrl.append(currentUrl);
                        }
                    }
                }
            } catch (ConnectException e) {
                logger.error("ConnectException in getStatusCode when connection to " + currentUrl + ", Cause: " + e);
                /*Skip the actually iterations of current thread to get status code*/
                iteration = iteration + 3;
            } catch (UnknownHostException e) {
                logger.error("UnknownHostException in getStatusCode when connection to " + currentUrl + ", Cause: " + e);
                /*Skip the actually iterations of current thread to get status code*/
                iteration = iteration + 3;
            } catch (SSLHandshakeException | SSLKeyException | SSLPeerUnverifiedException | SSLProtocolException e) {
                logger.error("SSLException produced by failed SSL-related operation in getStatusCode when connection to " + currentUrl + ", Cause: " + e);
                updateSSLCeritficate = Boolean.TRUE;
            } catch (Exception e) {
                logger.error("Exception in connection to " + currentUrl + " Cause:: " + e + ", at iteration:: " + iteration);
                /*Issues for a URL i.e
                -> java.net.NoRouteToHostException: No route to host 
                -> java.net.SocketTimeoutException: Read timed out */
                if (e.toString().trim().contains("java.net.NoRouteToHostException")
                        || e.toString().trim().contains("java.net.SocketTimeoutException")) {
                    iteration = iteration + 3;
                    logger.info("Skip the actually iterations of current thread to get status code");
                } else if (e.toString().trim().contains("PKIX path building failed") || e.toString().trim().contains("unrecognized_name")
                        || e.toString().trim().contains("CertificateException") || e.toString().trim().contains("SSLProtocolException")
                        || e.toString().trim().contains("SSLHandshakeException")) {
//                        || e.toString().trim().contains("javax.net.ssl.SSLHandshakeException: Remote host closed connection during handshake")
//                        || e.toString().trim().contains("javax.net.ssl.SSLHandshakeException: Received fatal alert: handshake_failure")) {
                    logger.info("SSLException produced by failed SSL-related operation");
                    updateSSLCeritficate = Boolean.TRUE;
                }
            }
            /*To update the SSL certification for a website/ URL */
            if (!isCertUpdated && updateSSLCeritficate) {
                try {
                    isCertUpdated = Common.sslValidation(currentUrl);
                    logger.info("isCertUpdated to handle the SSLProtocolException for the URL:: " + currentUrl + ", and status is:: " + isCertUpdated);
                } catch (Exception ex) {
                    isCertUpdated = Boolean.TRUE;
                    logger.error("Exception in creating SSL | PKIX certification: " + currentUrl + ", terminating the status Cause:: " + ex);
                }
            } else {
                iteration = iteration + 3;
            }
            iteration++;
        }
        return currentResponseCode;
    }

    public static String saveFile(MultipartFile multipartFile, String path) {
        String filename = null;
        if (multipartFile != null && multipartFile.getSize() != 1 && !multipartFile.getOriginalFilename().equals("emptyFile")) {

            try {
                filename = multipartFile.getOriginalFilename();
                byte[] bytes = multipartFile.getBytes();
                if (bytes != null) {
                    File oldFile = new File(path + "/" + multipartFile.getOriginalFilename());
                    if (oldFile.exists()) {
                        oldFile.delete();
                    }
                    FileCopyUtils.copy(multipartFile.getBytes(), new File(path + "/" + filename));
                }

            } catch (IOException ex) {
                logger.error("Exception on file saving in disk2:", ex);
            }
        }
        return filename;
    }

    /*
    Removes query params or request params from URL and returns domain contains protocol.
    Ex: http://www.sears.com/en_intnl/dap/shopping-tourism.html
    Returns: http://www.sears.com*/
    public static String getDomainWithProtocol(String queryURL) {
        String robotstxtQueryURL = null;
        if (queryURL != null) {
            if (queryURL.endsWith("/")) {
                queryURL = queryURL.substring(0, queryURL.lastIndexOf("/"));
            }
            try {
                URL url = new URL(queryURL);
                if (url.getProtocol() != null) {
                    robotstxtQueryURL = url.getProtocol().concat("://");
                }
                if (url.getHost() != null) {
                    robotstxtQueryURL = robotstxtQueryURL != null ? robotstxtQueryURL.concat(url.getHost()) : url.getHost();
                }
            } catch (MalformedURLException e) {
                logger.error("Exception in getDomainWithProtocol: ", e);
            }
        }
        return robotstxtQueryURL;
    }

    public static String texthtmlratio(Document doc) {
        double textHtmlratio = 0.0;
        double textlen;
        double htmllen;
        double titlelen;
        double ratio = 0.0;
        try {
            textlen = doc.select("body").text().length();
            titlelen = doc.select("title").text().length();
            htmllen = doc.select("body").toString().length();
            if (htmllen > 0.0) {
                ratio = ((textlen + titlelen) / htmllen) * 100;
            }
            textHtmlratio = Double.parseDouble(new DecimalFormat("#.##").format(ratio));
        } catch (NumberFormatException ne) {
            logger.error(ne);
        }
        return textHtmlratio + " " + "%";
    }

    public static Response getOkHttpResponse(String url) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(IMPORTANT_CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                .readTimeout(IMPORTANT_READ_TIME_OUT, TimeUnit.MILLISECONDS);

        OkHttpClient okHttpClient = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();
        } catch (IOException ex) {
            logger.error("IOException on getOkHttpResponse for responsebody: " + response + ", cause:: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("Exception on getOkHttpResponse for responsebody: " + response + " ,cause:: " + ex.getMessage());
        }
        return response;
    }

    public static Response getOkHttpResponseWithAuthorization(String apiURL, String requestBody, String apiKey, String mediaTypeParse) {
        Response response = null;
        try {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(IMPORTANT_CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
                    .readTimeout(IMPORTANT_READ_TIME_OUT, TimeUnit.MILLISECONDS);

            OkHttpClient client = builder.build();
            MediaType mediaType = MediaType.parse(mediaTypeParse);
            RequestBody body = RequestBody.create(mediaType, requestBody);
            Request request = new Request.Builder()
                    .url(apiURL)
                    .post(body)
                    .addHeader("content-type", mediaTypeParse)
                    .addHeader("authorization", apiKey)
                    .build();

            response = client.newCall(request).execute();
        } catch (IOException ex) {
            logger.error("IOException on getOkHttpResponseWithAuthorization for responsebody: " + response + ", cause:: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("Exception on getOkHttpResponseWithAuthorization for responsebody: " + response + " ,cause:: " + ex.getMessage());
        }
        return response;
    }

    public static void createMergedCell(HSSFRow rows, HSSFCellStyle style, int startCellNo, int endCellNo) {
        HSSFCell cell;
        for (int i = startCellNo; i <= endCellNo; i++) {
            cell = rows.createCell(i);
            cell.setCellStyle(style);
        }
    }

    public static int getStatusCodeOfURL(String urlPath) {
        int code = 0;
        try {
            URL urlObj = new URL(urlPath);
            HttpURLConnection connectionObj = (HttpURLConnection) urlObj.openConnection();
            connectionObj.setRequestMethod("GET");
            connectionObj.setInstanceFollowRedirects(false);
            connectionObj.setConnectTimeout(IMPORTANT_CONNECT_TIME_OUT);
            connectionObj.setReadTimeout(IMPORTANT_READ_TIME_OUT);
            connectionObj.connect();
            code = connectionObj.getResponseCode();
        } catch (IOException e) {
            logger.error("Exception on getStatusCodeOfURL: ", e);
        }
        return code;
    }

    public static JSONObject getJSONFromAPIResponse(String endPointURL) {
        Response response = null;
        JSONObject jsonObject = null;
        String responseContent = null;
        try {
            response = Common.getOkHttpResponse(endPointURL);
            if (response != null) {
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        responseContent = body.string();
                        if (responseContent != null && !responseContent.trim().isEmpty()) {
                            jsonObject = new JSONObject(responseContent.trim());
                        }
                    }
                }
            }
        } catch (JSONException | IOException e) {
            if (jsonObject != null) {
                logger.error("JSON | IO Exception in getJSONFromAPIResponse for responsebody: " + jsonObject.toString() + ", cause:: " + e.getMessage());
            } else {
                logger.error("JSON | IO Exception in getJSONFromAPIResponse response::  " + response + ",  cause:: " + e.getMessage());
            }
        } catch (Exception e) {
            if (jsonObject != null) {
                logger.error("Exception in getJSONFromAPIResponse for responsebody: " + jsonObject.toString() + ", cause:: " + e.getMessage());
            } else {
                logger.error("Exception in getJSONFromAPIResponse cause:: " + e.getMessage());
            }
        } finally {
            if (response != null && response.code() == HttpStatus.SC_OK) {
                response.body().close();
            }
        }
        return jsonObject;
    }

    /*Disable log of response in case of exception occurs*/
    public static JSONObject getJSONFromAPIResponseV2(String endPointURL) {
        Response response = null;
        JSONObject jsonObject = null;
        String responseContent = null;
        try {
            response = Common.getOkHttpResponse(endPointURL);
            if (response != null) {
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        responseContent = body.string();
                        if (responseContent != null && !responseContent.trim().isEmpty()) {
                            jsonObject = new JSONObject(responseContent.trim());
                        }
                    }
                }
            }
        } catch (JSONException | IOException e) {
            if (jsonObject != null) {
                logger.debug("JSON | IO Exception in getJSONFromAPIResponse for responsebody: " + jsonObject.toString() + ", cause:: " + e.getMessage());
            } else {
                logger.debug("JSON | IO Exception in getJSONFromAPIResponse response::  " + response + ",  cause:: " + e.getMessage());
            }
        } catch (Exception e) {
            if (jsonObject != null) {
                logger.debug("Exception in getJSONFromAPIResponse for responsebody: " + jsonObject.toString() + ", cause:: " + e.getMessage());
            } else {
                logger.debug("Exception in getJSONFromAPIResponse cause:: " + e.getMessage());
            }
        } finally {
            if (response != null && response.code() == HttpStatus.SC_OK) {
                response.body().close();
            }
        }
        return jsonObject;
    }

    public static JSONObject getJSONFromAPIResponse(String endPointURL, boolean waitForSuccessRepsonse, String errorKey) {
        Response response = null;
        JSONObject jsonObject = null;
        int count = 0;
        int responseCode = 0;
        String tempRespone = null;
        while (count <= 4) {
            try {
                response = Common.getOkHttpResponse(endPointURL);
                if (response != null) {
                    try (ResponseBody body = response.body()) {
                        if (body != null) {
                            tempRespone = body.string();
                        }
                    }
//                    responseCode = response.code();
//                    tempRespone = response.body().string();
                }
                if (tempRespone != null && !tempRespone.trim().isEmpty()) {
                    jsonObject = new JSONObject(tempRespone.trim());
                }
            } catch (JSONException | IOException e) {
                if (tempRespone != null) {
                    logger.error("JSON | IO Exception in getJSONFromAPIResponse with waitForSuccessRepsonse for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
                } else {
                    logger.debug("JSON | IO Exception in getJSONFromAPIResponse with waitForSuccessRepsonse cause:: " + e.getMessage());
                }
            } catch (Exception e) {
                if (tempRespone != null) {
                    logger.error("Exception in getJSONFromAPIResponse with waitForSuccessRepsonse for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
                } else {
                    logger.debug("Exception in getJSONFromAPIResponse with waitForSuccessRepsonse cause:: " + e.getMessage());
                }
            } finally {
                if (response != null) {
                    response.body().close();
                }

                /*Making thread sleep to make on more request after sleep time completion 
                if waitForSuccessRepsonse: TRUE.*/
                if (waitForSuccessRepsonse && ((responseCode > HttpStatus.SC_OK) || (jsonObject != null && jsonObject.has(errorKey)) || (jsonObject == null))) {
                    /*Sending Mail to support team*/
                    logger.info("responseCode: " + responseCode + ", for endPointURL:" + endPointURL);
                    jsonObject = null;
                    count++;
                    try {
//                        SendMail.sendToolAPIMessageMailV2("API Response Error", ("Preparing JSON from OKHHTP Response Message: ".concat(tempRespone) + ", endpointURL:: " + endPointURL + ", responseCode:: " + responseCode), (String) ctx.getBean("supportMail"), "vidyasagar.korada@netelixir.com");
                        Thread.sleep(1000 * 50);
                    } catch (InterruptedException e) {
                        logger.error("InterruptedException in making thread sleep in getJSONFromAPIResponse with waitForSuccessRepsonse cause:: ", e);
                    }
                } else {
                    count = 5;
                }
            }
        }
        return jsonObject;
    }

    public static JSONArray getJSONArrayFromAPIResponse(String endPointURL) {
        Response response = null;
        JSONArray jsonArray = null;
        String tempRespone = null;
        try {
            response = Common.getOkHttpResponse(endPointURL);
            if (response != null) {
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        tempRespone = body.string();
                    }
                }
            }
            if (tempRespone != null && !tempRespone.trim().isEmpty()) {
                jsonArray = new JSONArray(tempRespone.trim());
            }
        } catch (JSONException | IOException e) {
            if (tempRespone != null) {
                logger.error("JSON | IO Exception in getJSONFromAPIResponse convertinto JSONArray for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
            } else {
                logger.debug("JSON | IO Exception in getJSONFromAPIResponse convertinto JSONArrayse cause:: " + e.getMessage());
            }
        } catch (Exception e) {
            if (tempRespone != null) {
                logger.error("Exception in getJSONFromAPIResponse convertinto JSONArray for responsebody: " + tempRespone + ", cause:: " + e.getMessage());
            } else {
                logger.debug("Exception in getJSONFromAPIResponse convertinto JSONArray cause:: " + e.getMessage());
            }
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return jsonArray;
    }

    public static String saveLogoInDisk(MultipartFile multipartFile, String saveFileLocation, String logoName, float actualWidth, float actualHeight, String imageFormat) {
        BufferedImage originalImage = null;
        float width, height;
        try {
            originalImage = ImageIO.read(multipartFile.getInputStream());
            if (originalImage != null) {
                width = originalImage.getWidth();
                height = originalImage.getHeight();
                float finalWidth, finalHeight;
                /*logoName = logoName == null?saveFileLocation.concat(multipartFile.getOriginalFilename()):saveFileLocation.concat(logoName);
            net.coobird.thumbnailator.Thumbnails.of(multipartFile.getInputStream()).size((int) finalWidth, (int) finalHeight)
                    .toFile(logoName);*/
                if ((float) (actualHeight / actualWidth) < (height / width)) {
                    finalHeight = actualHeight;
                    finalWidth = (int) ((width / height) * finalHeight);
                } else {
                    finalWidth = actualWidth;
                    finalHeight = (int) ((height / width) * finalWidth);
                }
                checkFileExistance(saveFileLocation.concat(logoName), true);
                net.coobird.thumbnailator.Thumbnails.of(multipartFile.getInputStream()).size((int) finalWidth, (int) finalHeight)
                        .outputFormat(imageFormat).toFile(saveFileLocation.concat(logoName));
            }
        } catch (IOException e) {
            originalImage = null;
            logger.error("IOException in saveLogoInDisk cause: ", e);
        }
        return originalImage != null ? logoName.concat("." + imageFormat) : null;
    }

    public static boolean checkFileExistance(String filePath, boolean deleteFile) {
        /*Checking whether file is present or not*/
        File fileObj = new File(filePath);
        return fileObj.exists() && deleteFile ? fileObj.delete() : fileObj.exists();
    }

    public static String getDomainNameFromSite(String site) {
        String domainName = "";
        try {
            if (!site.startsWith("http")) {
                site = "http://".concat(site);
            }
            URL urlObj = new URL(site);
            domainName = urlObj.getHost();
            if (domainName.startsWith("www.")) {
                domainName = domainName.substring(4);
            }
        } catch (MalformedURLException e) {
            logger.error("Malformed exception in url cause:", e);
        }
        return domainName.toLowerCase();
    }

    public static String convertDateInToAPIRequestFormat(Date dateObj) {
        SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM");
        return dateFormate.format(dateObj);
    }

    public static String decryptedPassword(String password) {
        if (password != null) {
            String decryptedPassword = encryptAndDecryptUsingDES.decrypt(password);
            if (decryptedPassword != null) {
                password = decryptedPassword;
            }
        }
        return password;
    }

    public static String encryptPassword(String password) {
        if (password != null) {
            String encryptPassword = encryptAndDecryptUsingDES.encrypt(password);
            if (encryptPassword != null) {
                password = encryptPassword;
            }
        }
        return password;
    }

    /*This service can be used to connect any API endpoint URL using proxy mesh API,
    it helps to make continous hits using random public IP's */
    public static HttpMethod connectAPIURLEndpointUsingProxyServer(String searchQuery, int proxyHostNo) {
        HttpMethod method = null;
        String selectedProxyHost = null;
        URL currentUrlObj = null;
        if (searchQuery != null) {
            try {
                currentUrlObj = new URL(searchQuery);
            } catch (MalformedURLException ex) {
                logger.error("MalformedURLException in connectGoogleCustomSearchUsingProxyServer cause: ", ex);
            }
        }
        if (currentUrlObj != null) {
            selectedProxyHost = proxyHostNo == 0 ? PROXY_HOST_1 : PROXY_HOST_2;
            try {
                method = new GetMethod(currentUrlObj.toString());
                org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
                client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
                client.getParams().setSoTimeout(1000 * 1);
                client.getParams().setConnectionManagerTimeout(1000 * 1);
                HostConfiguration config = client.getHostConfiguration();
                config.setProxy(selectedProxyHost != null ? selectedProxyHost : String.valueOf(1), PROXY_PORT);
                client.getState().setProxyCredentials(new AuthScope(selectedProxyHost, PROXY_PORT), new UsernamePasswordCredentials(PROXY_USERNAME, PROXY_PASSWORD));
                client.executeMethod(method);
            } catch (IOException e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("IOException in connectGoogleCustomSearchUsingProxyServer cause: ", e);
            } catch (Exception e) {
                logger.info("Proxy Connection Failed: To get google custom search page results.");
                logger.error("Exception in connectGoogleCustomSearchUsingProxyServer cause: ", e);
            }
        }
        return method;
    }

    /*Connects to api endpoint url using proxy
    if proxy failed it connects to api endpoint url usinh OKHTTP client service*/
    public static JSONObject getJSONObejctFromProxyMethodResponse(String endPointURL) {
        HttpMethod method = null;
        JSONObject jsonObject = null;
        method = Common.connectAPIURLEndpointUsingProxyServer(endPointURL, 1);
        if (method != null && method.getStatusCode() == HttpStatus.SC_OK) {
            try {
                jsonObject = new JSONObject(method.getResponseBodyAsString());
            } catch (IOException | JSONException ex) {
                logger.error("IOException|JSONException in getSpyFuAPIResponse cause: ", ex);
            }
        } else {
            jsonObject = Common.getJSONFromAPIResponse(endPointURL);
        }
        return jsonObject;
    }

    public static <T extends Object> T postForObject(String url, Object request, Class<T> responseType) {
        RestTemplate template = new RestTemplate();
        return template.postForObject(url, request, responseType);
    }

    public static Date convertLocaleDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static String getConcatedStringFromList(List<String> response, String SEPARATOR) {
        String viewableFormat = response.stream().collect(Collectors.joining(SEPARATOR));
        return viewableFormat;
    }

    public static String generateUserEmailVerificationCode(int codeLength) {
        String generatedString = RandomStringUtils.randomAlphanumeric(codeLength);
        return generatedString;
    }

}
