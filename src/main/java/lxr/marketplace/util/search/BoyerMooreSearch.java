package lxr.marketplace.util.search;

public class BoyerMooreSearch {
	int MAXCHAR = 65536;// no of characters in characterset of the text and pattern
	int MAXPATT = 100;// max length of a pattern
	int bc[] = new int[MAXCHAR];
	int gs[] = new int[MAXPATT];
	int m;
	int choice;
	String patt;

	public BoyerMooreSearch(String p, int ch) {
		choice = ch;
		switch (choice) {
		case 1: // case insensitive considering double space error
			p = p.toLowerCase();
		case 2: // case sensitive considering double space error
			p = p.trim();
		case 3: // exact match
			patt = p;
			break;
		case 4: // case insensitive not considering double space error
			patt = p.toLowerCase();
			break;
		}
		m = patt.length();
		GoodSufxShift();
		BadCharShift();
	}

	public void BadCharShift() {
		int k;
		for (k = 0; k < MAXCHAR; k++)
			bc[k] = m;
		for (k = 0; k < m - 1; k++)
			bc[patt.charAt(k)] = m - k - 1;
	}

	public int[] suffixes(int[] suff) {
		int f = 0, g, i;
		suff[m - 1] = m;
		g = m - 1;
		for (i = m - 2; i >= 0; --i) {
			if (i > g && suff[i + m - 1 - f] < i - g) {
				suff[i] = suff[i + m - 1 - f];
			} else {
				if (i < g)
					g = i;
				f = i;
				while (g >= 0 && patt.charAt(g) == patt.charAt(g + m - 1 - f)) {
					--g;
				}
				suff[i] = f - g;
			}
		}
		return suff;
	}

	public void GoodSufxShift() {
		int i, j;
		int suff[] = new int[m];
		suff = suffixes(suff);
		for (i = 0; i < m; ++i) {
			gs[i] = m;
		}
		j = 0;
		for (i = m - 1; i >= 0; --i) {
			if (suff[i] == i + 1) {
				for (; j < m - 1 - i; ++j) {
					if (gs[j] == m) {
						gs[j] = m - 1 - i;
					}
				}
			}
		}
		for (i = 0; i <= m - 2; ++i) {
			gs[m - 1 - suff[i]] = m - 1 - i;
		}
	}

	public int find(String text) {
		int n = text.length();
		int count = 0;
		if (m > n)
			return 0;
		int i = 0, j = 0;
		while (j <= n - m) {
			if (choice == 1) {
				int k = 0;
				for (i = m - 1; i >= 0; --i) {
					char a = patt.charAt(i);
					char b = text.charAt(i + j - k);
					if (a == b || a == (b + 'a' - 'A')) {
						if (a == ' ' && b == ' ') {
							while (text.charAt(i + j - (++k)) == ' ') {
								//increaments k till number of spaces in text 
							}
							k--;
						}
					} else {
						break;
					}
				}
			} else if (choice == 2) {
				int k = 0;
				for (i = m - 1; i >= 0; --i) {
					char a = patt.charAt(i);
					char b = text.charAt(i + j - k);
					if (a == b) {
						if (a == ' ' && b == ' ') {
							while (text.charAt(i + j - (++k)) == ' ') {
								// increaments k till number of spaces in text
							}
							k--;
						}
					} else {
						break;
					}
				}
			} else if (choice == 3) {
				for (i = m - 1; i >= 0
						&& (patt.charAt(i) == text.charAt(i + j)); --i);
			} else if (choice == 4) {
				for (i = m - 1; i >= 0
						&& (patt.charAt(i) == text.charAt(i + j) || patt.charAt(i) == (text.charAt(i + j) + 'a' - 'A')); --i);
			}
			if (i < 0) {
				++count;
				j += gs[0];
			} else {
				char ch = text.charAt(i + j);
				if (choice == 1 || choice == 4) {
					j += Math.max(gs[i],
							bc[('A' <= ch && ch <= 'Z') ? (ch - ('A' - 'a'))
									: ch] - m + 1 + i);
				} else {
					j += Math.max(gs[i], bc[ch] - m + 1 + i);
				}
			}
		}
		return count;
	}
}
