/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.Errors;

/**
 *
 * @author vidyasagar.korada
 */
public class ValidationUtil {
    public static List<String> fromBindingErrors(Errors errors) {
        List<String> validErrors = new ArrayList<>();
        errors.getAllErrors().stream().forEach((objectError) -> {
            validErrors.add(objectError.getDefaultMessage());
        });
        return validErrors;
    }
}
