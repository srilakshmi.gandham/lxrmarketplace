/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.payment.PaymentModel;
import lxr.marketplace.services.Services;
import lxr.marketplace.user.EmailValidationModel;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.Signup;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna/ NE16T1213 - Sagar.K
 */
@Service
public class EmailBodyCreator {

    private static Logger logger = Logger.getLogger(EmailBodyCreator.class);

    static ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        EmailBodyCreator.ctx = ctx;
    }

    private static final String APPLICATION_URL = (String) ctx.getBean("domainName");
    private final static String ANCHOR_STYLE = "style=\"color: #0077E8; text-decoration: none;cursor: pointer;\"";
    public final static String FOOTER_ONE = "<div style='padding:5px 0 5px 10px;'>Copyright © NetElixir. All rights reserved";
    public final static String FOOTER_TWO = "<div style='padding:5px 0 5px 10px;text-align:center;'>This email was sent by lxrmarketplace.com. 3 Independence Way, Suite #203, Princeton, NJ 08540.</div>";

    public static String subjectOnWelcome = "Welcome to LXRMarketplace - Online Marketing Tools for Retailers";

    public static String getCommonFooterForMailContent(boolean blogInfo) {
        String commonFooterText = null;
        commonFooterText = "Sincerely,";
        commonFooterText += "<br/>LXRMarketplace Team.<br/>";
        commonFooterText += "<a href = \"https://lxrmarketplace.com\">https://lxrmarketplace.com</a>";
        commonFooterText += "<br/>Powerful. Simple. Free.";
        if (blogInfo) {
            commonFooterText += "<p>P.S: If you want industry updates, how-to guides, and more information about our tools, "
                    + "you can always stay up to date with our "
                    + "<strong><a href = \"https://lxrmarketplace.com/blog/\">LXRMarketplace Blog.</a></strong> "
                    + "Subscribe today and join the digital conversation!</p>";
        }
        return commonFooterText;

    }

    public static String bodyForSignup(Signup signup) {
        String bodyText = null;
        bodyText = "<body>";
        bodyText += "<p>Hi " + signup.getName() + ",";
        bodyText += "<p>Welcome to LXRMarketplace! We’re delighted that you’ve joined our fanatically analytical "
                + "and energetic community of digital marketers. </p>";
        /*With reference to mail from product manager
        1. Password Update
            No Password to be included in any email.
            a. Welcome mail should be sent only to the signed up users and should not contain password.
            b. No mail to be sent for users signing in using social channels
        if (signup.isSocialSignUp() && signup.getUserSocialProfile() != null && signup.getUserSocialProfile().getSocialChannel() != null) {
            bodyText += "<p>You choose to login using " + signup.getUserSocialProfile().getSocialChannel() + " this would be hassle free as you are automatically registered by us. </p>";
        }
        bodyText += "<span>Please review your login credentials below:</span>";
        bodyText += "<ul style='margin-top: 0%;margin-left: -2%;'>"
                + "<li>Username: " + signup.getEmail() + "</li>"
                + "<li>Password: " + signup.getPassword() + "</li>"
                + "</ul>";*/
        bodyText += "<p>You’ll get unlimited access to FREE digital marketing tools "
                + "that take the guesswork out of daily tasks. That means extra time for you at the end of each week.</p>";
        bodyText += "<p>If you have any questions or need any additional information, please contact us at support@lxrmarketplace.com.</p>";
        bodyText += "</br>";
        bodyText += "</br>";
        bodyText += getCommonFooterForMailContent(Boolean.TRUE);
        bodyText += "</body>";
        return bodyText;
    }

    public static String bodyForSendMailOnActivation(String toAddr) {
        String userName = toAddr.split("@")[0];
        String bodyText = "<body>"
                + "<p>Hi " + userName + ","
                + "<br/>"
                + "<br/>Welcome to LXRMarketplace, designed to help online retailers save time and increase productivity. <br/>"
                + "<br/>Register to our website to experience a whole new platform of online advertising tools.<br/>"
                + "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                + "<br/>Sincerely,<br/>"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"https://lxrmarketplace.com\">https://lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String subjectOnReminderMail = "Activate your LXRMarketplace account now!";

    public static String bodyForReminderMail(long activationId, String url, String name, String userName, String password) {
        String bodyText = "";
        if (activationId != 0) {
            bodyText = "<body>";
            bodyText += "<p>Hi "
                    + name
                    + ",<br/>"
                    + "<br/>Life is hectic! <br/>"
                    + "<br/>We understand how busy you are, and we are here to help!"
                    + "<br/>"
                    + "<br/>You are just one step away from being able to access our powerful, simple & FREE online marketing applications to QUICKLY improve your campaigns success!<br/>"
                    + "<br/>Please click the link below to activate your account that will make life just a little bit easier:<br/>";
            bodyText += "<br/><b>Link : <a href=' " + url
                    + "/useractivation.html?activation=" + activationId + "'> "
                    + url + "/useractivation.html?activation=" + activationId
                    + "</a></b><br/>";
            bodyText += "<br/>After activation use the following credentials to Sign in:<br/>";
            bodyText += "<br/>Username: "
                    + userName
                    + "<br/>Password: "
                    + password;
            bodyText += "<br/><br/>If you require any further information or help, please contact us at support@lxrmarketplace.com </br>";
            bodyText += "<br/><br/><br/>Best Wishes,";
            bodyText += "<br/><br/>Your LXRMarketplace Team.";
            bodyText += "<br/><a href = \"https://lxrmarketplace.com\">https://lxrmarketplace.com</a>";
            bodyText += "<br/>Powerful. Simple. Free.</p>";
            bodyText += "</body>";
        }
        return bodyText;
    }

    public static String subjectOnForgotPassword = "Password Update";

    public static String bodyForForgotPassword(Login user) {
        String bodyText;
        bodyText = "<body>";
        bodyText += "<p>Hi "
                + user.getName()
                + ",</p>"
                + "<p>Welcome to LXRMarketplace, designed to help online retailers save time and increase productivity. </p>"
                + "<p>Your Account details are:<br/>" + "<p>Username: "
                + user.getUserName() + "<br/>Password: " + user.getPassword() + " (You can reset the password after login.)</p>";
        bodyText += "<p>If you require any further information or help, please contact us at support@lxrmarketplace.com</p>";
        bodyText += "<p>Sincerely,</p>";
        bodyText += "<p>Your LXRMarketplace Team.";
        bodyText += "<br/><a href = \"https://lxrmarketplace.com\">https://lxrmarketplace.com</a>";
        bodyText += "<br/>Powerful. Simple. Free.</p>";
        bodyText += "</body>";
        return bodyText;
    }
    public static String subjectOnAskTheExpertFeedBackMail = "Your Feedback Matters!";

    public static String bodyForAskTheExpertFeedBackMail(String name, String encriptedUserId, String encriptedQuestionId, String question) {
        String finalURL = APPLICATION_URL + "/ask-expert-feedback.html?user-id=" + encriptedUserId + "&question-id=" + encriptedQuestionId + "&";
        String bodyText
                = "<div style='border: 1px solid #e6e6e6; background-color: #f9f9f9;padding-bottom: 10px;'>"
                + "<div style='padding : 6px'>"
                + "<a href=\""
                + APPLICATION_URL + "/"
                + "\"> "
                + "<img src='"
                + APPLICATION_URL
                + "/css/0/images/LXRMarketplace.png'"
                + "alt='lxrmarketplace.com'   /></a> "
                + "</div>"
                + "<div style='padding: 10px;margin-bottom: 2%;'>"
                + "<div style='font-weight: bold;font-size: 14px;font-family: arial,sans-serif;line-height: 20px;'>"
                + "Hi " + name.substring(0, 1).toUpperCase() + name.substring(1) + "!"
                + "</div>"
                + "<p style='color: #565656;line-height: 22px;font-size: 14px;font-family: arial,sans-serif;'>Please take a second to provide your feedback on the answer to your question.</p>"
                + "<p style='font-size: 17px; color: #565656;line-height: 16px;font-family: arial,sans-serif;'>" + question + ".</p>"
                + "<p style='font-size: 14px; color: #565656;margin-bottom: 4%;font-family: arial,sans-serif;'>How would you rate the answer provided by our expert?</p>"
                + "<a href=" + finalURL + "rating=2 style='font-family: arial,sans-serif;margin: 4% 3% 3% 0%; border: 0px; padding: 10px; font-size: 16px; color: #fff; text-decoration: none; background-color:#1179FF;'>Poor</a>"
                + "<a href=" + finalURL + "rating=3 style='font-family: arial,sans-serif;margin: 4% 3% 3% 0%; border: 0px; padding: 10px; font-size: 16px; color: #fff; text-decoration: none; background-color:#1179FF'>Fair</a>"
                + "<a href=" + finalURL + "rating=4 style='font-family: arial,sans-serif;margin: 4% 3% 3% 0%; border: 0px; padding: 10px; font-size: 16px; color: #fff; text-decoration: none; background-color:#1179FF'>Good</a>"
                + "<a href=" + finalURL + "rating=5 style='font-family: arial,sans-serif;margin: 4% 3% 3% 0%; border: 0px; padding: 10px; font-size: 16px; color: #fff; text-decoration: none; background-color:#1179FF'>Excellent</a>"
                + "</div>"
                + "</div>"
                + "<div style='background-color: #F2F2F2; text-align:center; color: #808080; font-size:12px;'>"
                + FOOTER_ONE
                + FOOTER_TWO
                + "</div>";

        return bodyText;
    }

    private final static String SUBHEADINGSTYLE = "style='color: #6b7b97; font-family:ProximaNova-Bold;'";

    public static String subjectOnExpertAnswerToUser = "[LXRMarketplace] Answer from our Experts";
    public static String subjectOnExpertAnswerTestMail = "[Test] Ask The Expert Answer";

    public static String bodyForExpertAnswerTestMail(String name, String question, String answerTemplate) {

        String bodyText = "<div style='font-family:ProximaNova-Regular'>"
                + "<h3> Hi " + name + "</h3>"
                + "<h2 " + SUBHEADINGSTYLE + ">Question:</h2>"
                + "<span>"
                + question
                + "</span>"
                + "<h2 " + SUBHEADINGSTYLE + ">Answer:</h2>"
                + "<span>"
                + appendApplicationHostURL(answerTemplate)
                + "</span>"
                + "<div style='background-color: #F2F2F2; text-align:center; color: #808080; font-size:12px;'>"/*Start of Footer.*/
                + FOOTER_ONE
                + FOOTER_TWO
                + "</div>"
                + "</div>";

        return bodyText;
    }

    public static String subjectOnExpertQuestionAssigned = " [Ask The Expert] Question assigned to you for Answer";

    public static String bodyForExpertQuestionAssigned(String assigneeName, String assidnedName, String question, String tool) {
        String bodyText;
        bodyText = "<div style='font-family:ProximaNova-Regular'>"
                + "<h3> Hi " + assigneeName + "</h3>"
                + "<p>A question has been assigned to you for answer by " + assidnedName + ".</p>"
                + "<p><b>Question:</b> " + question + "</p>"
                + "<p><b>Tool:</b> " + tool + "</p>"
                + "<p>Please <a href=\"" + APPLICATION_URL + "/\">click here</a> to answer the question.</p>"
                + "<p>Please note that SLA for answering the question is Two Business Days.</p>"
                + "<div style='background-color: #F2F2F2; text-align:center; color: #808080; font-size:12px;'>"/*Start of Footer.*/
                + FOOTER_ONE
                + FOOTER_TWO
                + "</div>"
                + "</div>";
        return bodyText;
    }

    public static String subjectOnExpertQuestionReview = " [Ask The Expert] Question Assigned To You for Review";

    public static String bodyForExpertQuestionReview(String reviewerName, String assidnedName, String question, String tool) {
        String bodyText;
        bodyText = "<div style='font-family:ProximaNova-Regular'>"
                + "<h3> Hi " + reviewerName + "</h3>"
                + "<p>A question has been assigned to you for review by " + assidnedName + ".</p>"
                + "<p><b>Question:</b> " + question + "</p>"
                + "<p><b>Tool:</b> " + tool + "</p>"
                + "<p>Please <a href=\"" + APPLICATION_URL + "/\">click here</a> to answer the question.</p>"
                + "<p>Please note that SLA for answering the question is Two Business Days.</p>"
                + "<div style='background-color: #F2F2F2; text-align:center; color: #808080; font-size:12px;'>"/*Start of Footer.*/
                + FOOTER_ONE
                + FOOTER_TWO
                + "</div>"
                + "</div>";

        return bodyText;
    }

    public static String subjectForHelpSubmitingTeam = "LXRMarketplace Get Help";

    public static String bodyForHelpSubmitingTeam(String userName, String email, String description, String pageUrl) {
        String bodyText = "<body>";
        bodyText += "Hi Team, <br/><br/>";
        bodyText += "Email: " + email + "<br/>";
        bodyText += "PageUrl: " + pageUrl + "<br/><br/>";
        bodyText += "Description: " + description + "<br/><br/>"
                + "Thank you!  <br/>"
                + "Your LXRMarketplace Team. <br/>"
                + "<a href=\"" + APPLICATION_URL + "\">www.lxrmarketplace.com</a>.  <br/>"
                + "Powerful. Simple. Free.<br/>"
                + "</body>";
        return bodyText;
    }

    public static String subjectForHelpSubmitingUser = "Thank You For Contacting LXRMarketplace!";

    public static String bodyForHelpSubmitingUser(String userName) {
        String bodyText = "<body>";
        bodyText += "Hi " + userName + ", <br/><br/>";
        bodyText += "We’ve received your message and would like to take a moment to say thank you!<br/><br/>"
                + "Someone from our team will be reaching out to you shortly regarding your inquiry."
                + " If you need urgent assistance, please reach out to us at <a " + ANCHOR_STYLE + " href=\"mailto:support@lxrmarketplace.com?subject=LXRMarketplace Customer Support\"> "
                + "support@lxrmarketplace.com</a><br/><br/>"
                + "Thank you!  <br/>"
                + "Your LXRMarketplace Team. <br/>"
                + "<a href=\"" + APPLICATION_URL + "\">www.lxrmarketplace.com</a>.  <br/>"
                + "Powerful. Simple. Free.<br/>"
                + "</body>";
        return bodyText;
    }

    public static String subjectForSendUserDetails = "Thank You For Contacting LXRMarketplace!";

    public static String bodyForSendUserDetails(String userName, String password, String name) {
        String bodyText = "<body>";
        bodyText += "Hi " + name + ", <br/><br/>";
        bodyText += "UserName: " + userName + "<br/>";
        bodyText += "Password: " + password + "<br/><br/>"
                + "Thank you!  <br/>"
                + "Your LXRMarketplace Team. <br/>"
                + "<a href=\"" + APPLICATION_URL + "\">www.lxrmarketplace.com</a>.  <br/>"
                + "Powerful. Simple. Free.<br/>"
                + "</body>";
        return bodyText;
    }

    public static String subjectForAskExpertQuestionToOurTeam = "[Ask The Expert] New Question To Be Answered";

    public static String bodyForAskExpertQuestionToOurTeam(String question, String toolName) {
        String bodyText = "<body>";
        bodyText += "Hi Expert Team, <br/><br/>"
                + "There is a new Ask The Expert question added to the panel.<a href=\"" + APPLICATION_URL + "\">Click here</a> to view the new question.<br/><br/>"
                + "<b>Question:</b> " + question + "<br/><br/>"
                + "<b>Tool:</b> " + toolName + "<br/><br/>"
                + "Please verify the question and assign the same to relevant Expert.<br/><br/>"
                + "Please note that SLA for answering the question is Two Business Days.<br/><br/>"
                + "Thank you!  <br/>"
                + "Your LXRMarketplace Team. <br/>"
                + "<a href=\"" + APPLICATION_URL + "\">www.lxrmarketplace.com</a>.  <br/>"
                + "Powerful. Simple. Free.<br/>"
                + "</body>";
        return bodyText;
    }

    public static String subjectForEbookToUser = "FREE EBOOK: Top 10 SEO Tactics That Work";

    public static String bodyForEbookToUser(String name, String attachmentName) {
        String bodyText = "<body>";
        bodyText += "Hi " + name + ", <br/><br/>";
        bodyText += "Thank you for subscribing to LXRMarketplace.<br/><br/>";
        bodyText += "Here is the link to "
                + "<a href=\"" + APPLICATION_URL + "/" + attachmentName + "\">FREE EBOOK: Top 10 SEO Tactics That Work.</a><br/><br/>";
        bodyText += "Thank you!  <br/>"
                + "Your LXRMarketplace Team. <br/>"
                + "<a href=\"" + APPLICATION_URL + "\">www.lxrmarketplace.com</a>.  <br/>"
                + "Powerful. Simple. Free.<br/>"
                + "</body>";
        return bodyText;
    }

    public static String appendApplicationHostURL(String answerTemplate) {
        logger.info("ATE Template image url: " + APPLICATION_URL + "/ate_template_images/");
        if (answerTemplate.contains("/ate_template_images/")) {
            answerTemplate = answerTemplate.replaceAll("/ate_template_images/", APPLICATION_URL + "/ate_template_images/");
        }
        return answerTemplate;
    }
    public static String subjectOnFeedBackRatingAndComments = " [Ask The Expert] FeedBack From User";

    public static String bodyForFeedBackReport(String askExpertFeedbackRating, String askExpertFeedbackComment, String userEmailId, String question, String answer, String toolName) {
        String bodyText;

        bodyText = "<div style='font-family:ProximaNova-Regular'>"
                + "<h3> Hi Expert/Reviewer, </h3>"
                + "<p><b>User MailId:</b> " + userEmailId + "</p>"
                + "<p><b>Question Rating:</b> " + askExpertFeedbackRating + "</p>";
        if (!askExpertFeedbackComment.trim().equals("")) {
            bodyText += "<p><b>Comment:</b> " + askExpertFeedbackComment + "</p>";
        }
        bodyText += "<p><b>Question:</b> " + question + "</p>"
                + "<p><b>Answer:</b> " + answer + "</p>"
                + "<p><b>Tool Name:</b> " + toolName + "</p>"
                + "<div style='background-color: #F2F2F2; text-align:center; color: #808080; font-size:12px;'>"/*Start of Footer.*/
                + FOOTER_ONE
                + FOOTER_TWO
                + "</div>"
                + "</div>";
        return bodyText;
    }
    public static String subjectOnBlogSubscription = "LXRMarketplace Blog Subscription";

    public static String bodyForBlogSubscription() {
        String bodyText = "<body>"
                + "<p>Dear User,"
                + "<br/>"
                + "<br/>Thank you for subscribing to the LXRMarketplace Blog!<br/>"
                + "<br/>We are so excited to give you the pointers, updates, and info you will need as you travel along your path to SEO success.<br/>"
                + "<br/>And don’t hesitate to check out the LXRMarketplace website for our free 20+ SEO tools <a href=\"" + APPLICATION_URL + "/\">here!</a><br/>"
                + "<br/>Sincerely,"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String subjectOnSEOTipSubscription = "LXRMarketplace SEO Tip of the Week";

    public static String bodyForSEOTipSubscription() {
        String bodyText = "<body>"
                + "<p>Dear User,"
                + "<br/>"
                + "<br/>Thank you for subscribing to our SEO Tip of the Week! <br/>"
                + "<br/>You will recieve SEO tip every week from our SEO experts.<br/>"
                + "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                + "<br/>Sincerely,<br/>"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String subjectOnLxrPluginForSignUp = "Thank You from LXRPlugin!";

    public static String bodyOnLxrPluginForSignUp(String name, String emailId, String password) {
        String credentailsText = "";
        if (!"".equals(password)) {
            credentailsText = "<p>You can access your LXRPluigin account using following credentials:</p><b>Username:</b> " + emailId + " <br/><b>Password:</b> " + password;

        }
        String bodyText = "<div>"
                + "<div style='text-align:center;border:1px solid gray; border-bottom:none !important;'>"
                + "<h2>Dear " + name + "</h2>"
                + "<h3>You have successfully created a LXRPlugin account</h3>"
                + credentailsText
                + "<p>Start generating actionable insights from your Google AdWords account within seconds!</p>"
                + "<p>You can download your free trial of <b>LXRPlugin</b> <a " + ANCHOR_STYLE + " href=\"" + APPLICATION_URL + "\"><b>here.</b></a></p><br><br/>"
                + "</div>"
                + "<div style='background-color: #FFAB40; border:1px solid gray; padding-left:20px;'>"
                + "<p><b>Need any help?</b></p>"
                + "<p>Our expert would be happy to answer any questions you may have, whether you need help installing the <b>LXRPlugin</b> or generating insights regarding your AdWords campaign. "
                + "Feel free to contact us "
                + "<a " + ANCHOR_STYLE + " href=\"mailto:support@lxrplugin.com?Subject=LXRPlugin Customer Support\">by email</a> or "
                + "<a " + ANCHOR_STYLE + " href=\"" + APPLICATION_URL + "#connect\">submit a support request</a> and we will get back to you shortly.</p> "
                + "</div><br></br>"
                + "<div style='width:99.5%;background-color: #CCCCCC; border:2px solid #84A5DA; color: black; font-size:11px;'>"
                + FOOTER_ONE + emailId + FOOTER_TWO
                + "</div>"
                + "</div>";

        return bodyText;
    }
    public static String subjectOnLxrPluginForMonthlyReport = "LXRPlugin Tool Adoption Monthly Report";

    public static String bodyOnLxrPluginForMonthlyReport() {
        String bodyText = "<body>"
                + "<p>Dear Anser,"
                + "<br/>"
                + "<br/>Please find the attachment of last month LXRPlugin tool adoption monthly report. <br/>"
                + "<br/>Sincerely,"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String subjectOnLxrPluginWeeklyReport(String accName) {
        return "LXRPlugin for " + accName;
    }

    public static String bodyOnLxrPluginForWeeklyReport1(String accName) {
        String bodyText = "<body>"
                + "<p>Dear Team,"
                + "<br/>"
                + "<br/>Good Job in adopting the LXRPlugin tool. You have reached your target of adopting the tool (50%) "
                + "for <b>" + accName + "</b>. Explore the other reports and get benefited.  <br/>"
                + "<br/>Sincerely,"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String bodyOnLxrPluginForWeeklyReport2(int toolUsage, String accName) {
        String bodyText = "<body>"
                + "<p>Dear Team,"
                + "<br/>"
                + "<br/>Your current adoption of LXRPlugin stands at <b>" + toolUsage + "%</b>. This is less than the planned "
                + "target of 50%. Make sure to use the LXRPlugin tool to effectively manage the client account <b>" + accName + "</b>. <br/>"
                + "<br/>Sincerely,"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }
    public static String subjectOnCompetitorWebpageManitor = "Competitor Webpage Monitor Tool";

    public static String bodyForCompetitorWebpageManitorURLStatus(String url) {
        String bodyText = "<body>"
                + "<p>Dear User,"
                + "<br/>"
                + "<br/>The website ( \" <a href = ' " + url + " '>" + url + "</a> \" ) that you have been monitoring is down . <br/>"
                + "So, the change history data cannot be maintained for that duration . <br/>";
        bodyText += "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                + "<br/>Thank you!<br/>"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";
        return bodyText;
    }

    public static String bodyForCompetitorWebpageManitorCmpInfo(long tempInfoId) {
        String bodyText = "<body>"
                + "<p>Dear User,"
                + "<br/>"
                + "<br/>We have detected some changes to your monitored URL via the SEO Competitor Webpage Monitor on LXRMarketplace. Please click on the link below to access your results.<br/>";
        bodyText += "<br/><b>Link : <a href=' " + APPLICATION_URL
                + "/competitorwebpagemonitortool.html?info_id=" + tempInfoId + "'> "
                + APPLICATION_URL + "/competitorwebpagemonitortool.html?info_id=" + tempInfoId
                + "</a></b><br/>"
                + "<br/>Please note that you need to sign in on the website in order to view these changes."
                + "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                + "<br/>Thank you!<br/>"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";

        return bodyText;
    }

    public static String bodyForCompetitorWebpageManitor(String cmpURL) {
        String bodyText = "<body>"
                + "<p>Dear User,"
                + "<br/>"
                + "<br/>Thank you for using the SEO Competitor Webpage Monitor for " + cmpURL + " ! <br/>"
                + "<br/>We will automatically monitor the webpage URL you provided and send you email alerts whenever we detect any changes on that webpage.<br/>"
                + "<br/>After receiving the alert, you will need to sign in to LXRMarketplace to view these changes.<br/>"
                + "<br/>If you require any further information or help, please contact us at support@lxrmarketplace.com<br/>"
                + "<br/>Sincerely,<br/>"
                + "<br/>Your LXRMarketplace Team.<br/>"
                + "<a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a>"
                + "<br/>Powerful. Simple. Free.</p>"
                + "</body>";

        return bodyText;
    }
    public static String subjectOnPaymnetSuccess = "LXRMarketplace - Payment Successful";

    public static String bodyForPaymnetSuccess(String name, String tool, long toolId, Date transactionDate) {

        SimpleDateFormat sdfPeriod = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(transactionDate);

        String bodyText = "<body> <div width=\"728px\">";
        bodyText += "<div id=\"head\" style=\"padding : 5px;\"> "
                + "<div style=\"background-color: white;\">"
                + "<a href=\"" + APPLICATION_URL + "\"> "
                + "<img src='" + APPLICATION_URL + "/css/0/images/LXRMarketplace.png' "
                + "alt='lxrmarketplace.com'  style=\"padding:22px 0;\" /></a> "
                + "</div></div>";
        if (toolId == 0) {
            bodyText += "<p>Dear " + name + ",</p>"
                    + "<p>Thank you for your submitted question. Please find your receipt for your " + sdfPeriod.format(cal.getTime()) + " payment attached.</p>"
                    + "<p>Our digital marketing experts are currently reviewing your question, and they will respond to this email address with an answer within the next 48 hours.</p>"
                    + "<p>If you have any questions or need customer support regarding this transaction, please email us at support@lxrmarketplace.com </p>";
        } else {
            bodyText += "<p>Dear "
                    + name + ", "
                    + "<br/>"
                    + "<br/>"
                    + "Thank you for choosing our " + tool + ". Your payment is successful."
                    + "<br/>"
                    + "<br/>"
                    + "Please find the invoice attached."
                    + "<br/>"
                    + "<br/>"
                    + "Your report should have arrived in a separate mail."
                    + "<br/>"
                    + "<br/>"
                    + "If you have any questions or need customer support, please email us at support@lxrmarketplace.com"
                    + "<br/>"
                    + "<br/>";
        }
        bodyText = bodyText.concat(getCommonFooterForMailContent(Boolean.FALSE));
        bodyText += "</body>";

        return bodyText;
    }

    public static String bodyForPaymnetSuccessV2(PaymentModel paymentModel, String supportMail) {
        String bodyText = "<body> <div width=\"728px\">";
        bodyText += "<div id=\"head\" style=\"padding : 5px;\"> "
                + "<div style=\"background-color: white;\">"
                + "<a href=\"" + APPLICATION_URL + "\"> "
                + "<img src='" + APPLICATION_URL + "/css/0/images/LXRMarketplace.png' "
                + "alt='lxrmarketplace.com'  style=\"padding:22px 0;\" /></a> "
                + "</div></div>";
        if (paymentModel.getPayerUserId() <= 0) {
            /*For dashly apps payment inovice mail content*/
            bodyText += "<p>Dear "
                    + paymentModel.getBillingUserName() + ", "
                    + "<br/>"
                    + "<br/>"
                    + "Thank you for choosing our " + paymentModel.getToolName() + ". Your payment is successful."
                    + "<br/>"
                    + "<br/>"
                    + "Please find the invoice attached."
                    + "<br/>"
                    + "<br/>"
                    + "If you have any questions or need customer support, please email us at " + supportMail
                    + "<br/>"
                    + "<br/>";
        } else {
            bodyText += "<p>Dear "
                    + paymentModel.getBillingUserName() + ", "
                    + "<br/>"
                    + "<br/>"
                    + "Thank you for choosing our " + paymentModel.getToolName() + ". Your payment is successful."
                    + "<br/>"
                    + "<br/>"
                    + "Please find the invoice attached."
                    + "<br/>"
                    + "<br/>"
                    + "Your report should have arrived in a separate mail."
                    + "<br/>"
                    + "<br/>"
                    + "If you have any questions or need customer support, please email us at " + supportMail
                    + "<br/>"
                    + "<br/>";
        }
        bodyText = bodyText.concat(getCommonFooterForMailContent(Boolean.FALSE));
//        bodyText += "<br/>Sincerely,";
//        bodyText += "<br/>The LXRMarketplace Team.";
//        bodyText += "<br/><a href = \"www.lxrmarketplace.com\">www.lxrmarketplace.com</a><br/>";
        bodyText += "</body>";
        return bodyText;
    }

    public static String subjectOnClientServices = "4-Point SEM Consulting Package";

    public static String bodyForClientServices(Services service) {
        String phnNo = "", monSemSpend = "", url = "", title = "";
        if (service.getPhoneNo() != null && !"".equals(service.getPhoneNo())) {
            phnNo = "\n Phone Number : " + service.getPhoneNo();
        }
        if (service.getMonSemSpend() != null && !"".equals(service.getMonSemSpend())) {
            monSemSpend = "\n Approximate Monthly SEM Spend : " + service.getMonSemSpend();
        }
        if (service.getUrl() != null && !"".equals(service.getUrl())) {
            url = "\n Website URL : " + service.getUrl();
        }
        if (service.getTitle() != null && !"".equals(service.getTitle())) {
            title = "\n Title : " + service.getTitle();
        }

        String bodyText = "Hi Team,\n\nBelow are the details of the User for 4-Point SEM Consulting Package:"
                + "\n\n Name : " + service.getName() + "\n Company : " + service.getCompany() + "\n Email : " + service.getEmail()
                + phnNo + monSemSpend + url + title
                + "\n\nLXRMarketplace Team.\nwww.lxrmarketplace.com";
        return bodyText;
    }

    public static String bodyForClient() {
        String bodyText = "Hi LXRMarketplace User,\n\nThank you for registering in 4-Point SEM Consulting Package."
                + "\n\nOne of our SEM Consultants will contact you shortly."
                + "\n\nIf you require any further information or help, please contact us at team@lxrmarketplace.com."
                + "\n\nSincerely," + "\nYour LXRMarketplace Team."
                + "\nwww.lxrmarketplace.com"
                + "\nNetElixir Inc. • 609.356.5112";
        return bodyText;
    }

    public static String subjectOnEmailVerification = "Email Verification";

    public static String bodyForEmailVerification(EmailValidationModel emailValidationModel) {
        String bodyText = "";
        bodyText = "<body>";
        bodyText += "<p> Hi " + (emailValidationModel.getEmailUserName() != null ? emailValidationModel.getEmailUserName() : "LXRMarketplace User") + ",";
        bodyText += "<p> Welcome to LXRMarketplace! We’re delighted that you’ve joined our fanatically analytical "
                + "and energetic community of digital marketers. </p>";
        bodyText += "<p> Thank you for signing up for LXRMarketplace. </p>";
        bodyText += "<p> Please enter this code to verify your email: ";
        bodyText += "<ul style='margin-top: 0%;margin-left: -2%;'>"
                + "<li> Email: " + emailValidationModel.getEmailToBeVerified() + "</li>"
                + "<li> Verification Code: " + emailValidationModel.getVerificationCode() + "</li>"
                + "</ul>"
                + "</p>";
        bodyText += "<p>If you have any questions or need customer support, please email us at support@lxrmarketplace.com.</p>";
        bodyText += "</br>";
        bodyText += "</br>";
        bodyText += getCommonFooterForMailContent(Boolean.TRUE);
        bodyText += "</body>";
        return bodyText;
    }

    public static String custiomizeSubject = " - Payment Successful";

    /*When user is auto signup.
    i.e Email will send to user When new user utilizes the service of ATE*/
    public static String bodyForAutoSignup(Signup signup) {
        String bodyText = null;
        bodyText = "<body>";
        bodyText += "<p>Hi " + signup.getName() + ",";
        bodyText += "<p>Welcome to LXRMarketplace! We’re delighted that you’ve joined our fanatically analytical "
                + "and energetic community of digital marketers. </p>";
        bodyText += "<p>As you've chosen \"Ask The Expert\" feature this would be hassle free us to automatically registered by us.</p>";
        bodyText += "<span>Please review your login credentials below:</span>";
        bodyText += "<ul style='margin-top: 0%;margin-left: -2%;'>"
                + "<li>Username: " + signup.getEmail() + "</li>"
                + "<li>Password: " + signup.getPassword() + " (You can reset the password after login.)</li>"
                + "</ul>";
        bodyText += "<p>Once you log in, you’ll get unlimited access to FREE digital marketing tools "
                + "that take the guesswork out of daily tasks. That means extra time for you at the end of each week.</p>";
        bodyText += "<p>If you have any questions or need any additional information, please contact us at support@lxrmarketplace.com.</p>";
        bodyText += "</br>";
        bodyText += "</br>";
        bodyText += getCommonFooterForMailContent(Boolean.TRUE);
        bodyText += "</body>";
        return bodyText;
    }

}
