package lxr.marketplace.util.encryption;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.apache.log4j.Logger;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import org.springframework.stereotype.Service;

@Service
public class EncryptAndDecryptUsingDES {

//    public static void main(String[] args) {
//        String encrypted = "123";
//        EncryptAndDecryptUsingDES decoder = new EncryptAndDecryptUsingDES();
//        System.out.println(encrypted + " -> " + decoder.decrypt(encrypted));
//    }
    
    private SecretKey key;
    private AlgorithmParameterSpec paramSpec;
    private static final int iterationCount = 10;
    private static Logger logger = Logger.getLogger(EncryptAndDecryptUsingDES.class);
    // 8-byte Salt
    private static final byte[] salt = {
        (byte) 0xB2, (byte) 0x12, (byte) 0xD5, (byte) 0xB2,
        (byte) 0x44, (byte) 0x21, (byte) 0xC3, (byte) 0xC3};

    public EncryptAndDecryptUsingDES() {

        try {

            String passPhrase = "NX-LOXERS";

            // create a user-chosen password that can be used with
            // password-based encryption (PBE)
            // provide password, salt, iteration count for generating PBEKey of
            // fixed-key-size PBE ciphers
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            // create a secret (symmetric) key using PBE with MD5 and DES
            key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            // construct a parameter set for password-based encryption as
            // defined in the PKCS #5 standard
            paramSpec = new PBEParameterSpec(salt, iterationCount);

        } catch (InvalidKeySpecException e) {
            logger.error("Invalid Key Spec:" + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.error("No Such Algorithm:" + e.getMessage());
        }
    }

    public String encrypt(String str) {

        try {
            Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            // encode the string into a sequence of bytes using the named
            // charset 
            // storing the result into a new byte array. 
            byte[] utf8 = str.getBytes("UTF8");
            byte[] enc = ecipher.doFinal(utf8);
            // encode to base64 
            enc = BASE64EncoderStream.encode(enc);
            return new String(enc);

        } catch (InvalidAlgorithmParameterException e) {
            logger.error("Invalid Alogorithm Parameter:" + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.error("No Such Algorithm:" + e.getMessage());
        } catch (NoSuchPaddingException e) {
            logger.error("No Such Padding:" + e.getMessage());
        } catch (InvalidKeyException e) {
            logger.error("Invalid Key:" + e.getMessage());
        } catch (IllegalBlockSizeException e) {
            logger.error("Invalid BlockSize:" + e.getMessage());
        } catch (BadPaddingException e) {
            logger.error("Invalid Padding:" + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            logger.error("Invalid Encoding:" + e.getMessage());
        }
        return null;
    }

    public String decrypt(String str) {
        try {
            Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            // decode with base64 to get bytes 
            byte[] dec = BASE64DecoderStream.decode(str.getBytes());
            byte[] utf8 = dcipher.doFinal(dec);
            // create new string based on the specified charset 
            return new String(utf8, "UTF8");

        } catch (InvalidAlgorithmParameterException e) {
            logger.error("Invalid Alogorithm Parameter:" + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.error("No Such Algorithm:" + e.getMessage());
        } catch (NoSuchPaddingException e) {
            logger.error("No Such Padding:" + e.getMessage());
        } catch (InvalidKeyException e) {
            logger.error("Invalid Key:" + e.getMessage());
        } catch (IllegalBlockSizeException e) {
            logger.error("Invalid BlockSize:" + e.getMessage());
        } catch (BadPaddingException e) {
            logger.error("Invalid Padding:" + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            logger.error("Invalid Encoding:" + e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.error("ArrayIndexOutOfBoundsException: " + e.getMessage());
        }
        return null;
    }
}
