package lxr.marketplace.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails;
import lxr.marketplace.tool.advisor.ToolsUsageCountDTO;
import lxr.marketplace.util.ui.CommonUi;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

@Service
public class ToolService {

    private final Logger logger = Logger.getLogger(ToolService.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Tool> getTools() {
        String query = "select * from tool where is_deleted = 0 and show_in_home = 1 order by id desc";
        try {
            List<Tool> tools = jdbcTemplate.query(query, (ResultSet rs, int rowNum) -> {
                Tool tl = new Tool();
                tl.setToolId(rs.getInt(1));
                tl.setToolName(rs.getString(2));
                tl.setToolDesc(rs.getString(3));
                tl.setImgPath(rs.getString(4));
                tl.setTagline(rs.getString(5));
                tl.setCategoryid(rs.getLong(6));
                tl.setToolLink(rs.getString(7));
                return tl;
            });

            return tools;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public Map<Integer, Tool> populateTools() {
        final Map<Integer, Tool> toolsMap = new LinkedHashMap<>();
        String query = "select t.*, tu.tool_usage_count, tf.tool_rated_users_count, tf.tool_avg_rating, ts.sorting_order_id from "
                + "(select * from tool where is_deleted = 0 and cat_id !=2)t "
                + "left join "
                + "(select tu.tool_id 'tool_id',sum(tu.count) 'tool_usage_count' from tool_usage tu, session s where "
                + "tu.session_id = s.id and s.user_id not in (select id from user where is_admin = 1 and is_local = 1) "
                + "group by tu.tool_id)tu on t.id = tu.tool_id "
                + "left join "
                + "(select tool_id,count(tool_id) 'tool_rated_users_count',round(avg(rating),1) 'tool_avg_rating' from "
                + "feedback group by tool_id)tf on t.id = tf.tool_id "
                + "left join "
                + "(select tool_id, sorting_order_id from lxrm_tool_sorting)ts on t.id = ts.tool_id  "
                + "order by ts.sorting_order_id asc";

        jdbcTemplate.query(query, (ResultSet rs, int rowNum) -> {
            Tool tool = new Tool();
            int toolId = rs.getInt("id");
            tool.setToolId(toolId);
            tool.setToolName(rs.getString("name"));
            tool.setToolDesc(rs.getString("description"));
            tool.setImgPath(rs.getString("img_path"));
            tool.setTagline(rs.getString("tagline"));
            tool.setCategoryid(rs.getLong("cat_id"));
            tool.setToolLink(rs.getString("tool_link"));
            tool.setToolIconColor(rs.getString("tool_icon_color"));
            tool.setShowInHome(rs.getBoolean("show_in_home"));
            if (rs.getString("android_app_url") != null) {
                tool.setAndroidAppURL(rs.getString("android_app_url"));
                tool.setIsAndroidApp(true);
            } else {
                tool.setIsAndroidApp(false);
            }
            if (rs.getString("ios_app_url") != null) {
                tool.setIosAppURL(rs.getString("ios_app_url"));
                tool.setIsIOSApp(true);
            } else {
                tool.setIsIOSApp(false);
            }
            ToolUserInfo toolUserInfo = new ToolUserInfo();
            if (rs.getString("tool_usage_count") != null) {
                toolUserInfo.setToolUsageCount(rs.getLong("tool_usage_count"));
            } else {
                toolUserInfo.setToolUsageCount(0);
            }
            if (rs.getString("tool_rated_users_count") != null) {
                toolUserInfo.setToolRatedUsersCount(rs.getLong("tool_rated_users_count"));
            } else {
                toolUserInfo.setToolRatedUsersCount(0);
            }
            if (rs.getString("tool_avg_rating") != null) {
                toolUserInfo.setAvgRating(rs.getDouble("tool_avg_rating"));
                toolUserInfo.setAvgRatingGraph(CommonUi.getAverageRatingGraph(toolUserInfo.getAvgRating(), tool.getToolId(), 2));
            } else {
                toolUserInfo.setAvgRating(0);
            }
            tool.setToolUserInfo(toolUserInfo);

            toolsMap.put(toolId, tool);

            return tool;
        });
        return toolsMap;
    }

    /*Returns tools map object for Existing User Case
        1.Recent launched tool.
        2.Top two most used tools by user sort by tool usage count.
        3.Most used tools based on tool usage count.
     */
    public Map<Integer, Tool> populateToolsOnUserBasis(long userId, Map<Integer, Tool> newUserSortedTools) {

        List<Integer> topmostUsedToolIds = fetchingTopMostUsedTwoToolIds(userId);
        if (topmostUsedToolIds == null || topmostUsedToolIds.isEmpty()) {
            return null;
        } else {
            Map<Integer, Tool> reUserSortedTools = new LinkedHashMap<>();
            boolean isSeoTools = false, isAddedMostUsedTools = false;
            for (Integer toolId : newUserSortedTools.keySet()) {
                if (isSeoTools && !isAddedMostUsedTools) {
                    for (Integer i : topmostUsedToolIds) {
                        Tool tool = newUserSortedTools.get(i);
                        reUserSortedTools.put(i, tool);
                        isAddedMostUsedTools = true;
                    }
                }
                Tool tool = newUserSortedTools.get(toolId);
                reUserSortedTools.put(toolId, tool);
                if (tool.getCategoryid() == 3) {
                    isSeoTools = true;
                }
            }
            return reUserSortedTools;
        }
    }

    private List<Integer> fetchingTopMostUsedTwoToolIds(long userId) {

        String query = "select x.tool_id from "
                + "(select tu.tool_id, sum(tu.count) 'tool_usage_count' from tool_usage tu, session s, tool t "
                + "where s.id  = tu.session_id and t.id = tu.tool_id and t.show_in_home = 1 and t.is_deleted = 0 "
                + "and s.user_id= " + userId + " group by tu.tool_id order by tool_usage_count desc limit 2)x";
        SqlRowSet sqlRowset = jdbcTemplate.queryForRowSet(query);
        boolean isContainsData = sqlRowset.isBeforeFirst();
        List<Integer> topmostUsedToolIds = null;
        if (isContainsData) {
            topmostUsedToolIds = new ArrayList<>();
            while (sqlRowset.next()) {
                topmostUsedToolIds.add(sqlRowset.getInt("tool_id"));
            }
        }
        return topmostUsedToolIds;
    }

    public Map<Integer, List<Integer>> populateRelatedTools() {
        final Map<Integer, List<Integer>> tools = new HashMap<>();
        String query = "select tg.tool_id,t.id  from tools_grouping tg, tool t where tg.correlated_tool_id = t.id "
                + " order by tg.tools_order";
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(query);
        if (sqlRowSet != null) {
            while (sqlRowSet.next()) {
                if (tools.containsKey(sqlRowSet.getInt(1))) {
                    tools.get(sqlRowSet.getInt(1)).add(sqlRowSet.getInt(2));
                } else {
                    List<Integer> toolList = new ArrayList<>();
                    toolList.add(sqlRowSet.getInt(2));
                    tools.put(sqlRowSet.getInt(1), toolList);
                }
            }
        }
        return tools;
    }

    public List<Tool> getToolsByCategory(long catId) {
        String query = "select * from tool where cat_id = " + catId + " and is_deleted = 0 and show_in_home = 1 order by id desc";
        try {
            List<Tool> tools = jdbcTemplate.query(query, (ResultSet rs, int rowNum) -> {
                Tool tl = new Tool();
                tl.setToolId(rs.getInt(1));
                tl.setToolName(rs.getString(2));
                tl.setToolDesc(rs.getString(3));
                tl.setImgPath(rs.getString(4));
                tl.setTagline(rs.getString(5));
                tl.setCategoryid(rs.getLong(6));
                tl.setToolLink(rs.getString(7));
                return tl;
            });

            return tools;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public List<Integer> getToolsIdsForToolAdvisor(String toolIds, int limit) {
        List<Integer> toolIdsList = null;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -6);
        String date = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        String query = "(select id from tool where cat_id = 3 and is_deleted = 0 and show_in_home = 1 "
                + "and id not in(" + toolIds + ") and tool_date >='" + date + "' order by id desc limit " + limit + ")";
        SqlRowSet sqlRowset = jdbcTemplate.queryForRowSet(query);
        boolean isContainsData = sqlRowset.isBeforeFirst();
        if (isContainsData) {
            toolIdsList = new ArrayList<>();
            while (sqlRowset.next()) {
                toolIdsList.add(sqlRowset.getInt("id"));
            }
        }

        return toolIdsList;
    }

    public int fisrtToolUsage(long userId) {
        int noTimesToolUsed = 0;
        String query = "select count(tool_id) from tool_usage,session where "
                + "tool_usage.session_id = session.id and session.user_id=? ";
        try {
            noTimesToolUsed = jdbcTemplate.queryForObject(query, new Object[]{userId}, Integer.class);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return noTimesToolUsed;
    }

    public int findToolUsage(long userId, long tool_Id) {
        int noTimesToolUsed = 0;
        String query = "select count(" + tool_Id + ") from tool_usage,session where "
                + "tool_usage.session_id = session.id and session.user_id=? ";
        try {
            noTimesToolUsed = jdbcTemplate.queryForObject(query, new Object[]{userId}, Integer.class);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return noTimesToolUsed;
    }

    public Map<Integer, Double> getToolRatings() {
        String query = "select tool_id,avg(rating) from feedback where tool_id>0 and rating is not null group by tool_id order by tool_id desc";
        Map tool;
        try {
            Map<Integer, Double> toolratings = new HashMap<>();
            List<Map<String, Object>> b = jdbcTemplate.queryForList(query);
            for (int i = 0; i < b.size(); i++) {
                tool = b.get(i);
                toolratings.put(Integer.parseInt(tool.get("tool_id").toString()), Double.parseDouble(tool.get("avg(rating)").toString()));
            }
            return toolratings;
        } catch (DataAccessException | NumberFormatException e) {
            logger.error(e);
        }

        return null;

    }

    public Tool getToolInfoFromToolTab(int toolId, final String buttonName, final double amount) {
        final String query = "select * from tool where id= ? and is_deleted = ?";
        try {
            Tool toolInfo = jdbcTemplate.query(query, new Object[]{toolId, 0}, (ResultSet rs) -> {
                Tool toolObj = new Tool();
                try {
                    if (rs.next()) {
                        toolObj.setCategoryid(rs.getLong("cat_id"));
                        toolObj.setToolId(rs.getInt("id"));
                        toolObj.setToolLink(rs.getString("tool_link"));
                        toolObj.setImgPath(rs.getString("img_path"));
                        toolObj.setToolName(rs.getString("name"));
                        toolObj.setToolDesc(rs.getString("description"));
                        toolObj.setTagline(rs.getString("tagline"));
                        toolObj.setAmount(amount);
                        toolObj.setButtonName(buttonName);

                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return toolObj;
            });
            return toolInfo;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public int getActiveUserAquisitionPopupId() {
        int popupId = 0;
        logger.info("In checking the user info in lxrm_user_aquisition_popup table");
        String query = "SELECT popup_id FROM lxrm_user_aquisition_popup WHERE is_active = 1";
        try {
            popupId = jdbcTemplate.queryForObject(query, Integer.class);
        } catch (Exception e) {
            logger.error("Exception when checking the user info in lxrm_user_aquisition_popup table: " + e.getMessage());
        }
        return popupId;
    }

    public long insertDataInDownloadReportDetailsTab(final Object[] valObjArray) {
        final String query = "insert into download_report_details (report_name,tool_id,user_id,created_date,url) values (?,?,?,?,?)";
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {
                jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                    statement.setString(1, (String) valObjArray[0]);
                    statement.setInt(2, (Integer) valObjArray[1]);
                    statement.setLong(3, (Long) valObjArray[2]);
                    statement.setTimestamp(4, (Timestamp) valObjArray[3]);
                    statement.setString(5, (String) valObjArray[4]);
                    return statement;
                }, keyHolder);
            }
            logger.info("Credit Card is saved in lxrmarketplace database for user with id = " + (Long) valObjArray[2]);
            return (Long) keyHolder.getKey();
        } catch (Exception ex) {
            logger.error("Exception on inserting downlaod report details:", ex);
            return 0;
        }
    }

    /*Preparing the list of previous download reports for tool wise.*/
    public List<PaidDownloadedReportDetails> fetchPaidDownladReports(long userId, int toolId) {
        final String query = "select * from download_report_details where user_id = ? and tool_id = ? order by created_date desc";
        try {
            //Permission for guest users is 0 change it
            List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = jdbcTemplate.query(query, new Object[]{userId, toolId}, (ResultSet rs, int rowNum) -> {
                PaidDownloadedReportDetails paidDownloadedReportDetails = new PaidDownloadedReportDetails();
                paidDownloadedReportDetails.setReportName(rs.getString("report_name"));
                paidDownloadedReportDetails.setReportId(rs.getLong("r_id"));
                paidDownloadedReportDetails.setUrl(rs.getString("url"));
                paidDownloadedReportDetails.setToolId(rs.getInt("tool_id"));
                paidDownloadedReportDetails.setUserId(rs.getLong("user_id"));
                String tempDate = Common.convertTimeStampintoDisplayDate(rs.getTimestamp("created_date"));
                if (tempDate != null) {
                    paidDownloadedReportDetails.setCreatedTime(tempDate);
                } else {
                    paidDownloadedReportDetails.setCreatedTime("-");
                }
                return paidDownloadedReportDetails;
            });
            return paidDownloadedReportDetailsList;
        } catch (Exception ex) {
            logger.error("Exception in fetchPaidDownladReports", ex);
        }
        return null;
    }

    public void deletePaidDownladReports(long reportId) {
        final String query = "DELETE FROM download_report_details WHERE r_id=?";
        try {
            jdbcTemplate.update(query, reportId);
        } catch (Exception rE) {
            logger.error("Exception for deletePaidDownladReports: ", rE);
        }
    }

    public Map<Integer, Tool> getToolsBySorting(String sortByInfo) {
        final Map<Integer, Tool> tools = new LinkedHashMap<>();
        String query = null;
        if (sortByInfo.equalsIgnoreCase("byRating")) {
            query = "SELECT t.id, t.name, t.description, t.img_path, t.is_free, t.tagline, t.cat_id, t.desc_link, "
                    + "t.tool_link, avg(f.rating) rating FROM feedback f, tool t "
                    + "where f.tool_id=t.id and t.is_deleted=0 and t.show_in_home = 1 "
                    + "group by f.tool_id order by rating desc";
        } else if (sortByInfo.equalsIgnoreCase("byReview")) {
            query = "select t.id, t.name, t.description, t.img_path, t.is_free, t.tagline, t.cat_id, t.desc_link, "
                    + "t.tool_link, count(1) tool_count from feedback f, tool t "
                    + "where  t.id=f.tool_id and t.is_deleted=0  and t.show_in_home = 1 "
                    + "group by tool_id order by tool_count desc";
        } else if (sortByInfo.equalsIgnoreCase("byMostUsage")) {

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String today = sdf.format(calendar.getTime());
            calendar.add(Calendar.MONTH, -6);
            String sixMonthsBefore = sdf.format(calendar.getTime());

            query = "select tl.id, tl.name, tl.description, tl.img_path, tl.is_free, tl.tagline, tl.cat_id, tl.desc_link, "
                    + "tl.tool_link from tool tl "
                    + "left join "
                    + "(select t.*, sum(count) as tool_usage_count from tool t, tool_usage tu, session s "
                    + "where s.id = tu.session_id and t.id = tu.tool_id and DATE_FORMAT (s.start_time, '%Y-%m-%d') "
                    + "BETWEEN DATE_FORMAT('" + sixMonthsBefore + "', '%Y-%m-%d') AND DATE_FORMAT('" + today + "', '%Y-%m-%d') "
                    + "group by tu.tool_id) toolinfo on tl.id = toolinfo.id where tl.show_in_home = 1 and tl.is_deleted = 0 "
                    + "order by toolinfo.tool_usage_count desc";

        } else if (sortByInfo.equalsIgnoreCase("byNewToolFirst")) {
            query = "select * from tool where is_deleted = 0 and show_in_home = 1 order by id desc ";
        }
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(query);
        if (sqlRowSet != null) {
            while (sqlRowSet.next()) {
                Tool tl = new Tool();
                tl.setToolId(sqlRowSet.getInt(1));
                tl.setToolName(sqlRowSet.getString(2));
                tl.setToolDesc(sqlRowSet.getString(3));
                tl.setImgPath(sqlRowSet.getString(4));
                tl.setTagline(sqlRowSet.getString(5));
                tl.setCategoryid(sqlRowSet.getLong(6));
                tl.setToolLink(sqlRowSet.getString(7));
                tools.put(tl.getToolId(), tl);
            }
        }
        return tools;
    }

    public void fetchingToolUsageForAdvisor(long userId, ToolsUsageCountDTO toolsUsageCountDTO) {
        String query = "select "
                + "sum(case when u.tool_id =8 then u.count else 0 end ) as 'seo_webpage_analysis', "
                + "sum(case when u.tool_id =24 then u.count else 0 end ) as 'robots_txt_generator', "
                + "sum(case when u.tool_id =6 then u.count else 0 end ) as 'site_map_builder', "
                + "sum(case when u.tool_id =25 then u.count else 0 end ) as 'robots_txt_validator', "
                + "sum(case when u.tool_id =35 then u.count else 0 end ) as 'page_speed_insights', "
                + "sum(case when u.tool_id =27 then u.count else 0 end ) as 'meta_tag_genarator', "
                + "sum(case when u.tool_id =20 then u.count else 0 end ) as 'inbound_link_checker', "
                + "sum(case when u.tool_id =31 then u.count else 0 end ) as 'broken_link_checker', "
                + "sum(case when u.tool_id =34 then u.count else 0 end ) as 'most_used_keywords', "
                + "sum(case when u.tool_id =7 then u.count else 0 end ) as 'competitor_analysis' "
                + "from tool_usage u,session s where u.session_id = s.id and s.user_id=" + userId;

        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(query);
        boolean isContainData = sqlRowSet.isBeforeFirst();
        if (isContainData) {
            while (sqlRowSet.next()) {
                toolsUsageCountDTO.setSeoWebpageAnalysis(sqlRowSet.getInt("seo_webpage_analysis"));
                toolsUsageCountDTO.setRobotsTxtGenerator(sqlRowSet.getInt("robots_txt_generator"));
                toolsUsageCountDTO.setSiteMapBuilder(sqlRowSet.getInt("site_map_builder"));
                toolsUsageCountDTO.setRobotsTxtValidator(sqlRowSet.getInt("robots_txt_validator"));
                toolsUsageCountDTO.setPageSpeedInsights(sqlRowSet.getInt("page_speed_insights"));
                toolsUsageCountDTO.setMetaTagGenerator(sqlRowSet.getInt("meta_tag_genarator"));
                toolsUsageCountDTO.setInboundLinkChecker(sqlRowSet.getInt("inbound_link_checker"));
                toolsUsageCountDTO.setBrokenLinkChecker(sqlRowSet.getInt("broken_link_checker"));
                toolsUsageCountDTO.setMostUsedKeywords(sqlRowSet.getInt("most_used_keywords"));
                toolsUsageCountDTO.setCompetitorAnalysis(sqlRowSet.getInt("competitor_analysis"));
            }
        }
    }

    /**
     * Returns the below features: Users count used till to date. Websites
     * analyzed count till to date. LXRMarketplace tools count till to date.
     *
     * @return: Map object, it contains LXRM usage stats
     */
    public Map<String, Long> getLXRMUserStatstics() {
        Map<String, Long> userStastics = new HashMap<>();

        String query = "SELECT * FROM (SELECT COUNT(*) 'total_lxrm_users' FROM  user WHERE signup_type is null "
                + "or signup_type = 0) as total_lxrm_users, "
                + "(select COUNT(*) as 'total_tools' from tool where is_deleted = 0 and show_in_home=1 ) as total_tools, "
                + "(select sum(count) as 'total_websites_analyzed' from tool_usage where tool_id in "
                + "(select id from tool where is_deleted = 0 and show_in_home=1)) as total_websites_analyzed";
        try {
            SqlRowSet lxrmStasticis = jdbcTemplate.queryForRowSet(query);
            if (lxrmStasticis != null) {
                while (lxrmStasticis.next()) {
                    userStastics.put("totalLXRMUsers", lxrmStasticis.getLong("total_lxrm_users"));
                    userStastics.put("totalTools", lxrmStasticis.getLong("total_tools"));
                    userStastics.put("totalWebsitesAnalyzed", lxrmStasticis.getLong("total_websites_analyzed"));
                }
            }
        } catch (DataAccessException e) {
            logger.error("Exception for preparing getLxrmUserStatstics: ", e);
        }
        return userStastics;
    }

    public boolean saveHompageToolsOrderByUsage() {
        boolean isHomepageToolsInseted = false;
        String query = "insert into lxrm_tool_sorting(sorting_order_id, tool_id) "
                + "(select  @curRank := @curRank + 1 AS rank, id 'tool_id' from tool, "
                + "(SELECT @curRank := 0) q where is_deleted=0 and show_in_home=1 order by tool_date desc limit 1) "
                + "union "
                + "(select  @curRank := @curRank + 1 AS rank, x.id 'tool_id' from  (SELECT @curRank := 0) q, "
                + "(select t.id , CASE WHEN tr.total_tool_usage >0 THEN tr.total_tool_usage  ELSE 0 END AS total_tool_usage from "
                + "(select t.id , (SELECT @curRank := 1) q from tool t where t.is_deleted = 0 and t.show_in_home = 1 and "
                + "t.tool_date !=(select tool_date from tool order by tool_date desc limit 1))t "
                + "left join  "
                + "(select tu.tool_id,sum(tu.count) 'total_tool_usage'  from tool_usage tu,session s where s.id = tu.session_id "
                + "and s.user_id not in (select id from user where is_admin = 1 and is_local = 1) group by tool_id) tr "
                + "on  t.id = tr.tool_id order by total_tool_usage desc)x)";
        try {
            this.jdbcTemplate.execute("truncate table lxrm_tool_sorting");
            this.jdbcTemplate.update(query);
            isHomepageToolsInseted = true;
        } catch (DataAccessException e) {
            logger.error("Exception for inserting tools in lxrm_tool_sorting table throw scheduler", e);
            isHomepageToolsInseted = false;
        }
        return isHomepageToolsInseted;
    }
}
