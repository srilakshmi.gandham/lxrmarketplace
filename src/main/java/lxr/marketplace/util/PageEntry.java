package lxr.marketplace.util;

import java.util.Calendar;

public class PageEntry {
	private long sessionId;
	private String requestUri;
	private String requestQuery;
	private Calendar entryTime;
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public Calendar getEntryTime() {
		return entryTime;
	}
	public void setEntryTime(Calendar entryTime) {
		this.entryTime = entryTime;
	}
	public String getRequestUri() {
		return requestUri;
	}
	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}
	public String getRequestQuery() {
		return requestQuery;
	}
	public void setRequestQuery(String requestQuery) {
		this.requestQuery = requestQuery;
	}
	public PageEntry(String requestUri, String requestQuery, Calendar entryTime) {
		super();
		this.requestUri = requestUri;
		this.requestQuery = requestQuery;
		this.entryTime = entryTime;
	}	
	public PageEntry(String requestUri, Calendar entryTime) {
		super();
		this.requestUri = requestUri;
		this.entryTime = entryTime;
	}
}
