/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;
/**
 *
 * @author vidyasagar.korada
 */
public class SendEmailOptions implements Serializable{
    
    private String subject;
    private String bodyText;
    
    private String fromAddress;
    private String toAddressList;
    private String ccAddressList;
    private String bccAddressList;
    /*Key: File Path*/
    /*Value: File Names i.e : File #1, File #2....*/
    private Map<String,String> attachmentsFilesPath;
    
    private boolean mailDelivered;
    private String  errorMessage;
    private LocalDateTime deliveredAt;
    
    private String hostAppName;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddressList() {
        return toAddressList;
    }

    public void setToAddressList(String toAddressList) {
        this.toAddressList = toAddressList;
    }

    public String getCcAddressList() {
        return ccAddressList;
    }

    public void setCcAddressList(String ccAddressList) {
        this.ccAddressList = ccAddressList;
    }

    public String getBccAddressList() {
        return bccAddressList;
    }

    public void setBccAddressList(String bccAddressList) {
        this.bccAddressList = bccAddressList;
    }

    public Map<String, String> getAttachmentsFilesPath() {
        return attachmentsFilesPath;
    }

    public void setAttachmentsFilesPath(Map<String, String> attachmentsFilesPath) {
        this.attachmentsFilesPath = attachmentsFilesPath;
    }

    public boolean isMailDelivered() {
        return mailDelivered;
    }

    public void setMailDelivered(boolean mailDelivered) {
        this.mailDelivered = mailDelivered;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public LocalDateTime getDeliveredAt() {
        return deliveredAt;
    }

    public void setDeliveredAt(LocalDateTime deliveredAt) {
        this.deliveredAt = deliveredAt;
    }

}