package lxr.marketplace.util.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

public class MarketplaceHTMLTextParser {
	private static Logger logger = Logger.getLogger(MarketplaceHTMLTextParser.class);
	private StringBuffer textBuffer = null;
    private String newWord ="<span class=\"new\">";
//	public static void main(String[] args) {
//		MarketplaceHTMLTextParser parser = new MarketplaceHTMLTextParser();
////		Document doc = null;
////		try {
////			doc = parser
////					.getConnection("http://www.overnightprints.com/businesscards");
////		} catch (InterruptedException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////		String newBodyText = parser.parse(doc);
//
//		String oldBodyText = "";
//		String newBodyText = "";
//		FileInputStream inputStream1 = null;
//		FileInputStream inputStream2 = null;
//		try {
//			inputStream1 = new FileInputStream("/home/netelixir/Desktop/file1.txt");
//			oldBodyText = IOUtils.toString(inputStream1);
//			inputStream2 = new FileInputStream("/home/netelixir/Desktop/file2.txt");
//			newBodyText = IOUtils.toString(inputStream2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//
//			try {
//				inputStream1.close();
//				inputStream2.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		String[] newLines = newBodyText.split("[\\.\\:\\!\\?]\\s+");
//		String[] oldLines = oldBodyText.split("[\\.\\:\\!\\?]\\s+");
//		parser.findTextDiff(newLines, oldLines);
//	}

	public void findTextDiff(String[] newLines, String[] oldLines,ArrayList<String> textDiff,int wordCount[]) {
		int nlCount = 0;
		int olCount = 0;
		int removedWordsCount = 0;
		int addedWordsCount = 0;
		int totalWordCount = 0;
		//ArrayList<String> textDiff = new ArrayList<String>();
		while (nlCount < newLines.length && olCount < oldLines.length) {
			StringBuffer diff = new StringBuffer();
			int noWords[]  = new int[3];
			int matched = findLineDiff(oldLines[olCount], newLines[nlCount],diff,noWords);
			if (matched == 2) {
				nlCount++;
				olCount++;
				totalWordCount += noWords[2];
			} else if (matched == 1) {
				textDiff.add(diff.toString()+"<br>");
				removedWordsCount += noWords[0];
				addedWordsCount += noWords[1];
				totalWordCount += noWords[2];
				nlCount++;
				olCount++;

			} else {
				int tempNlCount = nlCount + 1;
				int tempMatched = 0;
				StringBuffer tempDiff = new StringBuffer();
				int tempNoWords[]  = new int[3];
				while(tempNlCount < newLines.length){
					tempDiff = new StringBuffer();
					tempMatched = findLineDiff(oldLines[olCount], newLines[tempNlCount], tempDiff, tempNoWords);
					if(tempMatched == 1 || tempMatched == 2){
						break;
					}else{
						tempNlCount++;
					}
				}
				if(tempMatched == 1 || tempMatched == 2){
					for(int k = nlCount; k < tempNlCount; k++){
						textDiff.add(newWord+newLines[k]+"</span><br>");
						String[] newWords = newLines[k].split("[\\p{Z}\\s]+");
						for(String newWord : newWords){
							if(!newWord.equals("")){
								addedWordsCount ++;
								totalWordCount ++;
							}
						}
					}
					if(tempMatched == 1){
						textDiff.add(tempDiff.toString()+"<br>");
						removedWordsCount += tempNoWords[0];
						addedWordsCount += tempNoWords[1];
						totalWordCount += tempNoWords[2]; 
					}else{
						totalWordCount += tempNoWords[2]; 
					}
					olCount ++;
					nlCount = tempNlCount+1;
				}else{
					if(!oldLines[olCount].equals("")){
						textDiff.add("<strike>"+oldLines[olCount]+"</strike><br>");
						String [] oldWords = oldLines[olCount].split("[\\p{Z}\\s]+");
						for(String oldWord : oldWords){
							if(!oldWord.equals("")){
								removedWordsCount ++;
							}
						}
						
						olCount++;
					}else{
						olCount++;	
					}
				}
			}			
		}
		
		if(olCount == oldLines.length && nlCount < newLines.length){
			for(int k = nlCount; k < newLines.length; k++){
				textDiff.add(newWord+newLines[k]+"</span><br>");
				String[] newWords = newLines[k].split("[\\p{Z}\\s]+");
				for(String newWord : newWords){
					if(!newWord.equals("")){
						addedWordsCount ++;
						totalWordCount ++;
					}
				}
			}
		}else if(olCount < oldLines.length && nlCount == newLines.length){
			for(int k = olCount; k < oldLines.length; k++){
				textDiff.add("<strike>"+oldLines[k]+"</strike><br>");
				String [] oldWords = oldLines[k].split("[\\p{Z}\\s]+");
				for(String oldWord : oldWords){
					if(!oldWord.equals("")){
						removedWordsCount ++;
					}
				}
			}
		}
		
		wordCount[0] = addedWordsCount;
		wordCount[1] = removedWordsCount;
		wordCount[2] = totalWordCount;
		logger.info("#new words = " + addedWordsCount+" #removed words = " + removedWordsCount+" #totalWordCount ="+totalWordCount);
		// = createHtml(textDiff);
	}

	public int findLineDiff(String oldL, String newL, StringBuffer out, int noWords[]) {
		String[] words1 = oldL.split("[\\p{Z}\\s]+");
		String[] words2 = newL.split("[\\p{Z}\\s]+");
		if(words1.length == 0){
			if(words2.length != 0){
				out.append(newWord+newL+"</span>");
	            noWords[0] = 0;
	            noWords[1] = words2.length;
	            noWords[2] = words2.length;
	            return 1;
			}else{
				return 2;
			}
		}
		int i = 0;
		int j = 0;
		int misMatchCount = 0;
		int newWordCount = 0;
		int totalWords = 0;
		while (i < words1.length && j < words2.length) {
			if (words1[i].equalsIgnoreCase(words2[j])) {
				if(!words1[i].equals("")){
					out.append(words1[i] + " ");
					totalWords++;
				}
				i++;
				j++;
			} else {
				int tempj = j + 1;
				boolean found = false;
				while (tempj < words2.length) {
					if (words1[i].equalsIgnoreCase(words2[tempj])) {
						found = true;
						break;
					} else {
						tempj++;
					}
				}
				if (found) {
					out.append(newWord);
					for (int k = j; k < tempj; k++) {
						if(!words2[k].equals("")){
							out.append(words2[k] + " ");
							newWordCount++;
							totalWords++;
						}
					}
					if(!words1[i].equals("")){
						out.append("</span>" + words1[i]+" ");
						totalWords++;
					}else{
						out.append("</span>");
					}
					i++;
					j = tempj + 1;
				} else {
					if(!words1[i].equals("")){
						out.append("<strike>" + words1[i] + " </strike>");
						misMatchCount++;
						i++;
					}else{
						i++;						
					}
				}
			}
		}
		if (i == words1.length && j < words2.length) {
			out.append(newWord);
			for (int k = j; k < words2.length; k++) {
				if(!words2[k].equals("")){
					out.append(words2[k] + " ");
					newWordCount++;
					totalWords++;
				}
			}
			out.append("</span>");
		} else if (i < words1.length && j == words2.length) {
			out.append("<strike>");
			for (int k = i; k < words1.length; k++) {
				if(!words1[k].equals("")){
					out.append(words1[k] + " ");
					misMatchCount++;
				}
			}
			out.append("</strike>");
		}
		if (misMatchCount == 0 && newWordCount == 0) {
			noWords[2] = totalWords;
			return 2;
		} else if (misMatchCount * 100 / words1.length < 60) {
			noWords[0]  = misMatchCount;
			noWords[1] = newWordCount;
			noWords[2] = totalWords;
			return 1; // matched
		} else {
			return 0;
		}
	}

	public String parse(Document doc) {
		textBuffer = new StringBuffer();
		List<Node> nodes = doc.childNodes();
		for (Node child : nodes) {
			parseNode(child);
		}
		String content = textBuffer.toString();
		return content;
		// writeTexttoFile(textBuffer.toString(),
		// "/home/netelixir/Som/LXR Marketplace/testOutput.txt");
		// String []lines = textBuffer.toString().split("[\\.\\:\\!\\?] ");
		// for(String line : lines){
		// System.out.println(line);
		// }
		// return lines;
	}

	private void parseNode(Node node) {
		for (Node child : node.childNodes()) {
			if (child instanceof TextNode) {
				String text = ((TextNode) child).text().replaceAll("[\\p{Z}\\s]+"," ");
				Pattern regEx = Pattern.compile("^([\\p{Z}\\s]*)$");
				Matcher matcher = regEx.matcher(text);
				if (!matcher.find()) {
					Pattern regEx2 = Pattern.compile("([\\.\\:\\!\\?])$");
					Matcher matcher2 = regEx2.matcher(text);
					if (matcher2.find()) {
						text += " ";

					} else {
						text += ". ";
					}
					// System.out.println(text);
//					if(text.contains(" Ramven")){
//						text = text.replaceAll(""+(char)169,"©");
//						char[] c = text.toCharArray();
//						for(int i=0;i<c.length;i++){
//							System.out.println((int)c[i]+" "+c[i]);
//						}
//					}
					text = text.replaceAll("<","&lt;").replaceAll(">", "&gt;");
					textBuffer.append(text);
				}

			}
			parseNode(child);
		}
	}

	// private void parseNode(Element node){
	// Elements childElements = node.children();
	// if(childElements.size() < 1){
	// return;
	// }
	// for(Element child : childElements){
	// parseNode(child);
	// }
	// }

	private Document getConnection(String url) throws InterruptedException {
		int timeoutIteration = 0;
		int noOfTime = 3;
		int timeout = 60000;
		int sllepOut = 5000;
		Connection jsoupCon;
		String userAgent ="Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)"; 
				/*
				 * Modified on oct-7 2015
				 * "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
		Document doc = null;
		do {
			timeoutIteration += 1;
			try {
				jsoupCon = Jsoup.connect(url);
				jsoupCon.timeout(timeout);
				jsoupCon.userAgent(userAgent);
				doc = jsoupCon.get();
				break;
			} catch (IOException ex) {
				System.out
						.println("Exception to get Connection 1 at iteration "
								+ timeoutIteration);
				Thread.sleep(sllepOut * timeoutIteration);
			}
		} while (timeoutIteration < noOfTime);

		return doc;
	}

	
	public String createHtml(ArrayList<String>textDiff){
//		try {
//	        BufferedWriter out = new BufferedWriter(new FileWriter("/home/netelixir/Desktop/output.html"));
//	            out.write("<html>"+"\n");
//	            out.write("<style>"+"\n");
//	            out.write(".new"+"\n");
//	            out.write("{"+"\n");
//	            out.write("font-family:\"Times New Roman\";"+"\n");
//	            out.write("background-color:#00ff00;"+"\n");
//	            out.write("}"+"\n");
//	            out.write("</style>"+"\n");
//	            out.write("<body>"+"\n");
//	            for (String line : textDiff) {
//	            	out.write(line+"<br><br>\n");
//	    		}
//	            
//	            
//	            out.write("</body>"+"\n");
//	            out.write("</html>"+"\n");
//	            out.close();
//	        } catch (IOException e) {}
	        
	        String diffText = "";
			diffText = "<html>"+"\n"
			         +"<style>"+"\n"
			         +".new"+"\n"
			         +"{"+"\n"
			         +"font-family:\"Times New Roman\";"+"\n"
			         +"background-color:#00ff00;"+"\n"
			         +"}"+"\n"
			         +"</style>"+"\n"
			         +"<body>"+"\n";
			for (String line : textDiff) {
				diffText=line+"<br><br>\n";
    		}
			diffText = "</body>"+"\n"
			         + "</html>"+"\n";
			logger.info(diffText);
			return diffText;
			
	}
}
