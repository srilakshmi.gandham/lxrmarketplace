package lxr.marketplace.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

public class CommonThreadPoolExecuter {

    private static Logger logger = Logger.getLogger(CommonThreadPoolExecuter.class);

    int poolSize = 2;
    int maxPoolSize = 10;
    long keepAliveTime = 10;

    ThreadPoolExecutor threadPool = null;

    LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    public CommonThreadPoolExecuter(int poolSize, int maxPoolSize, long keepAliveTime) {
        threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, queue);
        this.poolSize = poolSize;
        this.maxPoolSize = maxPoolSize;
        this.keepAliveTime = keepAliveTime;
    }

    public CommonThreadPoolExecuter() {
        threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, queue);
    }

    public void runTask(Runnable task) {
        threadPool.execute(task);
    }

    public void shutDown() {
        threadPool.shutdown();
    }

    public boolean isTerminated() {
        return threadPool.isTerminated();
    }
}
