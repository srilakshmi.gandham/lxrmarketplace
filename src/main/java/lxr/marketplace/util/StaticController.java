/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.util;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author vemanna
 */
@Controller
public class StaticController {
    
    @RequestMapping(value = "/terms-of-service.html")
    public String termsOfService(){
        return "views/termsofService";
    }
    
    /*Returns the RobotTxt Generator Tool help page*/
   @RequestMapping(value = "/robots-txt-generator-help.html")
    public String handleRobotTxtHelp() {
        return "views/robotsTextGenerator/robotsTxtGeneratorHelp";
    }
    
    @RequestMapping(value = "/about-us.html")
    public String aboutUs(){
        return "views/aboutUs";
    }
    
    @RequestMapping(value = "/privacy-policy.html")
    public String privacyPolicy(){
        return "views/privacyPolicy";
    }
    
    /*Added on July 23,2018*/
    @RequestMapping(value = "/privacy-policy-14sep2016.html")
    public String archivedPrivacyPolicy(){
        return "views/privacyPolicyArchived";
    }
    
    @RequestMapping(value = "dashly-for-magento.html")
    public String magento(){
        return "/views/mobileApps/dashlyForMagento";
    }
    @RequestMapping(value = "dashly-for-bigcommerce.html")
    public String bigCommerce(){
        return "/views/mobileApps/dashlyForBigcommerce";
    }
    @RequestMapping(value = "dashly-for-prestaShop.html")
    public String prestaShop(){
        return "/views/mobileApps/dashlyForPrestaShop";
    }
    @RequestMapping(value = "dashly-for-woocommerce.html")
    public String wooCommerce(){
        return "/views/mobileApps/dashlyForWoocommerce";
    }
   /* @RequestMapping(value = "woocommerce-product-feed.html")
    public String productFeed(){
        return "/views/mobileApps/wooCommerceProductFeed";
    }*/
    @RequestMapping(value = "magento-product-feed.html")
    public String productFeed(){
        return "/views/mobileApps/magentoProductFeed";
    }
    @RequestMapping(value = "presales-tools.html")
    public String presalesTools(){
        return "/views/preSalesToolsShowcase";
    }
}
