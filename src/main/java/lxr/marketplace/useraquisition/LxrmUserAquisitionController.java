/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.useraquisition;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anil
 */
@Controller
public class LxrmUserAquisitionController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private LxrmUserAquisitionService lxrmUserAquisitionService;
    @Autowired
    private String supportMail;
    @Autowired
    private SignupService signupService;

    @RequestMapping(value = "/lxrm-send-user-request.html", method = RequestMethod.POST)
    @ResponseBody
    public String sendEbook(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("emailId") String emailId, @RequestParam("name") String name,
            HttpSession session, Model model) throws IOException {
        Login user = null;
        user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        long userId = 0;
        if (!userLoggedIn) {
            Signup existingUser = signupService.getUserDetails(emailId, 0);
            if (existingUser == null) {
//                checking wheather the user is guest user or not
                existingUser = signupService.getUserDetails(emailId, 1);
                if (existingUser == null) {
                    user.setId(-1);
                    session.setAttribute("user", user);
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    user = (Login) session.getAttribute("user");
                    userId = user.getId();
                }
                if (user.getActivationDate() == null) {
                    Calendar cal = Calendar.getInstance();
                    user.setActivationDate(cal);
                    loginService.updateActDate(userId, new Timestamp(cal.getTimeInMillis()),1);
                    String subject = EmailBodyCreator.subjectOnWelcome;
                    String bodyText = EmailBodyCreator.bodyForSendMailOnActivation(emailId);
                    SendMail.sendMail(supportMail, emailId, subject, bodyText);
                }
//                Again updating with new emailid of the guest user
                String guestUserEmail = "GuestUser" + userId + "<" + emailId + ">";
                loginService.updateEmail(request, userId, guestUserEmail);
                if (name.equals("")) {
                    user.setName(emailId.split("@")[0]);
                } else {
                    user.setName(name);
                }
                user.setUserName(emailId);
            } else {
                if (name.equals("")) {
                    user.setName(existingUser.getName());
                } else {
                    user.setName(name);
                }
                user.setUserName(existingUser.getEmail());
            }
        }
        session.setAttribute("user", user);
        String popupName = null ;
        String mailAttachmentName = null ;

        SqlRowSet sqlRowSet = lxrmUserAquisitionService.getPopupinfo(1);
        if (sqlRowSet.isBeforeFirst()) {
            while (sqlRowSet.next()) {
                popupName = sqlRowSet.getString("popup_name");
                mailAttachmentName = sqlRowSet.getString("mail_attachment_name");
            }
        }
        boolean status = lxrmUserAquisitionService.checkUserDownlodStatus(popupName, emailId);
        if (!status) {
            lxrmUserAquisitionService.insertUserData(user.getName(), user.getUserName(), popupName);
        }
        String subject = EmailBodyCreator.subjectForEbookToUser;
        String displayTxt = EmailBodyCreator.bodyForEbookToUser(user.getName(), mailAttachmentName);
        boolean sendMail = SendMail.sendMail(supportMail, user.getUserName(), subject, displayTxt);
        if (sendMail) {
            return "success";
        } else {
            return "error";
        }

    }
}
