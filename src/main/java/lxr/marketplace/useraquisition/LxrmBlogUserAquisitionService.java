/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.useraquisition;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
@RequestMapping("/blogUserAquisition.html")
public class LxrmBlogUserAquisitionService {

    @Autowired
    private String supportMail;
    @Autowired
    LxrmUserAquisitionService lxrmUserAquisitionService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    String blogUserAquisation(HttpServletRequest request, HttpServletResponse response, @RequestParam(defaultValue = "") String name,
            @RequestParam(defaultValue = "") String email, @RequestParam(defaultValue = "") String aquisitionType) {
        String message = "";
        if (email != null && !"".equals(email)) {
            boolean isUserFound = lxrmUserAquisitionService.checkUserDownlodStatus(aquisitionType, email);
            if (!isUserFound) {
                boolean isInserted = lxrmUserAquisitionService.insertUserData(name, email, aquisitionType);
                if (isInserted) {
                    String subject = "", bodyText = "";
                    if (aquisitionType.equals("Blog")) {
                        subject = EmailBodyCreator.subjectOnBlogSubscription;
                        bodyText = EmailBodyCreator.bodyForBlogSubscription();
                    } else if (aquisitionType.equals("SEO Tip")) {
                        subject = EmailBodyCreator.subjectOnSEOTipSubscription;
                        bodyText = EmailBodyCreator.bodyForSEOTipSubscription();
                    }
                    SendMail.sendMail(supportMail, email, subject, bodyText);
                    message = "success";
                } else {
                    message = "fail";
                }
            } else {
                message = "existing-user";
            }
        } else {
            message = "Please enter email.";
        }

        return message;

    }

}
