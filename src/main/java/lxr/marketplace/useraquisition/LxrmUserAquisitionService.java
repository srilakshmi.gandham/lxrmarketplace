/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.useraquisition;

import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class LxrmUserAquisitionService {
    
    private static Logger logger = Logger.getLogger(LxrmUserAquisitionService.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    
//    To insert the user data in lxrm_user_aquisition table
    public boolean insertUserData(String userName, String emailId, String aquisitionType)
    {
        boolean insertStatus = false;
        String query = "insert into lxrm_user_aquisition (name, email, activation_date, aquisition_type) values(?,?,?,?)";
        Calendar cal = Calendar.getInstance();
        Object[] params = {userName, emailId, cal, aquisitionType};
        try{
            jdbcTemplate.update(query, params);
            insertStatus = true;
        }catch(DataAccessException e){
            logger.error("Exception on inserting data in blog user aquisation: ",e);
        }
        return insertStatus;
    }
    
//    To check wheather the user is already subscribed or not.
    public boolean checkUserDownlodStatus(String aquisitionType, String emailId)
    {
        boolean userStatus = false;
        logger.info("In checking the user info in lxrm_user_aquisition table");
        String query = "SELECT count(*) FROM lxrm_user_aquisition WHERE email = ? and aquisition_type = ?";
        Object[] params = {emailId, aquisitionType};
        try{
            int count = jdbcTemplate.queryForObject(query, params, Integer.class);
            if(count > 0){
                userStatus = true;
            }
        }catch(Exception e){
            logger.info("Exception when checking the user info in lxrm_user_aquisition table: "+e.getMessage());
        }
        return userStatus;
    }
    
//    To get the user aquisition active popup info.
    public SqlRowSet getPopupinfo(int popupId){
        SqlRowSet sqlRowSet = null;
        logger.info("In checking the user info in lxrm_user_aquisition_popup table");
        String query = "SELECT popup_name, mail_attachment_name FROM lxrm_user_aquisition_popup WHERE popup_id="+popupId;
        try{
            sqlRowSet = jdbcTemplate.queryForRowSet(query);
        }catch(Exception e){
            logger.info("Exception when checking the user info in lxrm_user_aquisition_popup table: "+e.getMessage());
        }
        return sqlRowSet;
    }
    public int getActiveUserAquisitionPopupId()
    {
        int popupId = 0;
        logger.info("In checking the user info in lxrm_user_aquisition_popup table");
        String query = "SELECT popup_id FROM lxrm_user_aquisition_popup WHERE is_active = 1";
        try{
            popupId = jdbcTemplate.queryForObject(query, Integer.class);
        }catch(Exception e){
            logger.info("Exception when checking the user info in lxrm_user_aquisition_popup table: "+e.getMessage());
        }
        return popupId;
    }
}
