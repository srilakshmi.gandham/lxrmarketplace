/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.microInfluencer;

import java.util.List;

/**
 *
 * @author sagar
 */

public class MicroInfluencerGenerator {
    private String searchQuery;
    private List<MicroInfluencer> influencersList;
    
    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public List<MicroInfluencer> getInfluencersList() {
        return influencersList;
    }

    public void setInfluencersList(List<MicroInfluencer> influencersList) {
        this.influencersList = influencersList;
    }
    
    
}
