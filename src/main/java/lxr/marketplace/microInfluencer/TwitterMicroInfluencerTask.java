/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.microInfluencer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.apiaccess.APIErrorHandlingService;
import lxr.marketplace.socialmedia.APIKeys;
import lxr.marketplace.util.Common;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author sagar
 */
public class TwitterMicroInfluencerTask implements Callable<List<MicroInfluencer>> {

    private static final Logger LOGGER = Logger.getLogger(TwitterMicroInfluencerTask.class);

    private final int page;
    private String searchQuery;
    private CountDownLatch latchObj;
    private static volatile boolean limitExceed = false;
    private List<Integer> noResponseCount;

    public TwitterMicroInfluencerTask(String searchQuery, int page, CountDownLatch latchObj, List<Integer> noResposneCount) {
        this.page = page;
        this.searchQuery = searchQuery;
        this.latchObj = latchObj;
        this.noResponseCount = noResposneCount;
    }

    @Override
    public List<MicroInfluencer> call() {
        List<MicroInfluencer> influencerList = null;
        influencerList = fetchInfluencersListFromAPIPage();
        latchObj.countDown();
        return influencerList;
    }

    List<MicroInfluencer> fetchInfluencersListFromAPIPage() {
        List<MicroInfluencer> influencerList = new ArrayList<>();
        OAuthConsumer consumer = new CommonsHttpOAuthConsumer(APIKeys.twitterConsumerKey, APIKeys.twitterConsumerSecret);
        consumer.setTokenWithSecret(APIKeys.twitterAccessToken, APIKeys.twitterAccessTokenSecret);
        HttpGet httpget = new HttpGet("https://api.twitter.com/1.1/users/search.json?q=" + searchQuery + "&page=" + page + "&count=20");
        LOGGER.debug("Request URL to API : " + httpget.getURI());
        int apiLimitCount = 0;
        HttpClient client = null;
        HttpResponse response = null;
        MicroInfluencer influencer = null;
        try {
            consumer.sign(httpget);
            client = HttpClientBuilder.create().build();
            response = (HttpResponse) client.execute(httpget);
            ArrayList<Header> arrayList = new ArrayList<>(Arrays.asList(response.getAllHeaders()));
            Header headerObj = arrayList.stream().filter(h -> (h.getValue() != null && h.getName().equalsIgnoreCase("x-rate-limit-remaining"))).findAny().orElse(null);
            if (headerObj != null) {
                apiLimitCount = Integer.parseInt(headerObj.getValue());
                LOGGER.debug("Remaning limt: " + apiLimitCount);
                if (apiLimitCount < 1) {
                    limitExceed = true;
                    LOGGER.info("Limit Exceed at page count: " + page + ", and remaining limt: " + apiLimitCount);
                }
            }

            if (response.getStatusLine().getStatusCode() == 200 && !limitExceed) {
                String result = EntityUtils.toString(response.getEntity());
                if (result != null && !result.equals("") && !result.equals("[]")) {
                    JSONArray arrayObj = new JSONArray(result);
                    LOGGER.debug("Page list: " + arrayObj.length());
                    JSONObject jsonObject = null;
                    for (int count = 0; count < arrayObj.length(); count++) {
                        jsonObject = arrayObj.getJSONObject(count);
                        if (!jsonObject.getString("name").equals("") && !jsonObject.getString("screen_name").equals("") && jsonObject.getLong("followers_count") != 0) {
                            influencer = new MicroInfluencer();
                            influencer.setId(jsonObject.getString("id_str"));
                            influencer.setName(jsonObject.getString("name"));
                            influencer.setScreenName(jsonObject.getString("screen_name"));
                            influencer.setDescription(jsonObject.getString("description"));
                            influencer.setFollowers(jsonObject.getLong("followers_count"));
                            influencer.setFollowing(jsonObject.getLong("friends_count"));//Following
                            influencer.setPosts(jsonObject.getLong("statuses_count"));//posts
                            influencer.setLikes(jsonObject.getLong("favourites_count"));//Likes
                            influencer.setUserSince(MicroInfluencerService.getUserCreateDate(jsonObject.getString("created_at")));
                            influencer.setProfileImageURL(Common.getStatusCodeOfURL(jsonObject.getString("profile_image_url_https")) == 200 ? jsonObject.getString("profile_image_url_https") : null);
                            if (jsonObject.has("location")) {
                                influencer.setLocation(jsonObject.getString("location"));
                            }
                            if (jsonObject.has("url")) {
                                influencer.setWebsite(jsonObject.getString("url"));
                            }
                            //                    influencer.setListedIn(jsonObject.getLong("listed_count"));
                            influencerList.add(influencer);
                        }
                    }
                }
                LOGGER.debug("Page count: " + page + ", API remaiing limt: " + apiLimitCount);
            }
        } catch (IOException | NumberFormatException | OAuthCommunicationException | OAuthExpectationFailedException | OAuthMessageSignerException | ParseException | JSONException e) {
            LOGGER.error("Exception in findInflunecersInTwitter cause: " + e.getMessage());
        }
        if ((response != null && response.getStatusLine().getStatusCode() > 300) || influencerList.isEmpty()) {
            /*Controller will enter into this block->
                When api returns empty response i.e no data/accounts existed fro search query in this indexed page.*/
            LOGGER.info("Response failed or empty page for page count: " + page + ", API remaining limt: " + apiLimitCount);
            noResponseCount.add(page);
            try {
                if (response != null) {
                    String result = EntityUtils.toString(response.getEntity());
                    APIErrorHandlingService.raiseTwitterAPIError(result, Common.MICRO_INFLUENCER_GENERATOR, 40, null);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        LOGGER.error("InterruptedException in checkGoogleUserExistence cause: " + ex.getMessage());
                    }
                }
            } catch (IOException | ParseException e) {
                LOGGER.error("Exception in checking for Error object in response cause: ", e);
            }
        }
        return influencerList;
    }
}
