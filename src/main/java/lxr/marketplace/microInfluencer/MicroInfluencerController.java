/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.microInfluencer;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

/**
 *
 * @author NE16T1213-Sagar
 */
@Controller
@RequestMapping("/micro-influencer-generator.html")
public class MicroInfluencerController {

    public static final Logger LOGGER = Logger.getLogger(MicroInfluencerController.class);
    @Autowired
    private MicroInfluencerService influencerService;

    @Autowired
    private String downloadFolder;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;


    /*Returns to tool page*/
    @RequestMapping(method = RequestMethod.GET)
    public String getMicroInfluencerToolPage(HttpSession session, HttpServletRequest request) {
        if ((WebUtils.hasSubmitParameter(request, "loginRefresh")) || WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            session.setAttribute("loginrefresh", "loginrefresh");
        } else {
            session.removeAttribute("repsonseInflunecerSet");
            session.removeAttribute("isInfluencerThreadCompleted");
            session.removeAttribute("MicroInfluencerGenerator");
        }
        return "views/microInfluencer/microInfluencer";
    }

    /*Validate user inputs and process user query*/
    @RequestMapping(params = {"request=analyze"}, method = RequestMethod.POST)
    @ResponseBody
    public List<MicroInfluencer> validateUserInputs(@RequestBody MicroInfluencerGenerator influencerGeneratorObj, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        List<MicroInfluencer> apiResponseList = null;
        MicroInfluencerGenerator processedInfluencerObj = null;
        /*Searching processced list from session for respective search query to handle back to success page event*/
        if (session.getAttribute("MicroInfluencerGenerator") != null) {
            processedInfluencerObj = (MicroInfluencerGenerator) session.getAttribute("MicroInfluencerGenerator");
            apiResponseList = (influencerGeneratorObj.getSearchQuery().equalsIgnoreCase((processedInfluencerObj.getSearchQuery()))
                    ? (processedInfluencerObj.getInfluencersList()) : null);
        }
        if (apiResponseList == null) {
            /*Removing session objects to handle to find existing query processing*/
            session.removeAttribute("repsonseInflunecerSet");
            session.removeAttribute("isInfluencerThreadCompleted");
            session.removeAttribute("MicroInfluencerGenerator");
            Tool toolObj = new Tool();
            toolObj.setToolId(40);
            toolObj.setToolName(Common.MICRO_INFLUENCER_GENERATOR);
            toolObj.setToolLink("micro-influencer-generator.html");
            session.setAttribute("toolObj", toolObj);
            apiResponseList = influencerService.processInfluencersList(influencerGeneratorObj, session);
        }

        if (apiResponseList != null) {
            influencerGeneratorObj.setInfluencersList(apiResponseList);
            session.setAttribute("MicroInfluencerGenerator", influencerGeneratorObj);
        }
        /*Saving guest user data*/
        Common.modifyDummyUserInToolorVideoUsage(request, response);
        /*Updatng too usage count.*/
        if (session.getAttribute("userSession") != null) {
            Session userSession = (Session) session.getAttribute("userSession");
            userSession.addToolUsage(40);
        }
        /*Auto sending tool analysis report after analysis for loggedin user only */
        Login user = (Login) session.getAttribute("user");
        if (user != null && (user.getUserName() != null && !user.getUserName().trim().isEmpty() && !user.getUserName().toLowerCase().equals("email")) && (apiResponseList != null && !apiResponseList.isEmpty())) {
            String email = user.getUserName().trim();
            if (email.contains("GuestUser")) {
                email = email.substring(("GuestUser" + user.getId() + "<").length(), email.lastIndexOf(">"));
            }
            influencerService.sendMailMicroInfluencerReport(request, influencerGeneratorObj, user.getId(), email, 0, 0, downloadFolder, false);
        }
         session.setAttribute("isInfluencerThreadCompleted", true);
        return apiResponseList;
    }

    @RequestMapping(params = {"request=updatedList"}, method = RequestMethod.GET)
    @ResponseBody
    public Object[] getUpdatedList(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        Object[] responseArray = new Object[3];
        Set<MicroInfluencer> responseInflunecerSet = null;
        boolean isInfluencerThreadCompleted = false;
        if (session.getAttribute("isInfluencerThreadCompleted") != null) {
            isInfluencerThreadCompleted = (boolean) session.getAttribute("isInfluencerThreadCompleted");
        }
        responseArray[0] = isInfluencerThreadCompleted;
        if (session.getAttribute("repsonseInflunecerSet") != null) {
            responseInflunecerSet = (Set<MicroInfluencer>) session.getAttribute("repsonseInflunecerSet");
            responseArray[1] = responseInflunecerSet.toArray(new MicroInfluencer[responseInflunecerSet.size()]);
        }
        if (session.getAttribute("userLoggedIn") != null) {
            boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (!userLoggedIn && session.getAttribute(Common.MICRO_INFLUENCER_GENERATOR) != null) {
                responseArray[2] = "LimitExceedded";
            }
        }
        return responseArray;
    }

    /*processing download request tool analysis  report*/
    @RequestMapping(params = {"request=download"}, method = RequestMethod.POST)
    public String processDownloadInfluencerReport(HttpServletResponse response, HttpSession session,
            @RequestParam("minFollowers") long minFollowers, @RequestParam("maxFollowers") long maxFollowers) {
        try {
            String fileName = null;
            Login user = (Login) session.getAttribute("user");
            if (session.getAttribute("MicroInfluencerGenerator") != null) {
                MicroInfluencerGenerator processedInfluencerObj = (MicroInfluencerGenerator) session.getAttribute("MicroInfluencerGenerator");
                List<MicroInfluencer> processedList = processedInfluencerObj.getInfluencersList();
                if (maxFollowers != 0) {
                    processedList = processedList.stream().filter(Influencer -> Influencer.getFollowers() >= minFollowers && Influencer.getFollowers() <= maxFollowers).collect(Collectors.toList());
                }
                fileName = influencerService.generateInfluencerXLSReport(processedList, processedInfluencerObj.getSearchQuery(), downloadFolder, user.getId());
            }
            LOGGER.info("Downloadble tool analysis report: " + fileName);
            if (fileName != null && !fileName.trim().equals("")) {
                Common.downloadReport(response, downloadFolder, fileName, "xls");
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Download reports request: ", e);
        }
        return null;
    }

    /*
     * Preparing tool analysis report to send mail to user.
     */
    @RequestMapping(params = {"request=sendMail"}, method = RequestMethod.POST)
    @ResponseBody
    public boolean processSendMailInfluencerReport(HttpServletRequest request, HttpSession session, @RequestParam("email") String userEmail, @RequestParam("minFollowers") long minFollowers, @RequestParam("maxFollowers") long maxFollowers) {
        Login user = (Login) session.getAttribute("user");
        return (session.getAttribute("MicroInfluencerGenerator") != null ? influencerService.sendMailMicroInfluencerReport(request, (MicroInfluencerGenerator) session.getAttribute("MicroInfluencerGenerator"), user != null ? user.getId() : 0, userEmail, minFollowers, maxFollowers, downloadFolder, true) : false);
    }

    /*Save the query in lxrm website tracking*/
    @RequestMapping(params = {"ate=ate"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            Model model, @RequestParam("domain") String domain) {
        String toolIssues = null;
        String questionInfo = null;
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain);
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.MICRO_INFLUENCER_GENERATOR);
        lxrmAskExpertService.addInJson(lxrmAskExpert, response);
        return null;
    }

    /*Returns to tool page when user came from came from ATE confirmation page*/
    @RequestMapping(params = {"backToSuccessPage"})
    public String backToSuccessPage(HttpSession session, Model model) {
        return "views/microInfluencer/microInfluencer";
    }
}
