/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.microInfluencer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class MicroInfluencerService {

    private static final Logger LOGGER = Logger.getLogger(MicroInfluencerService.class);
    private final int poolSize = 8;
    private final int maxPoolSize = 12;
    private final long keepAliveTime = 360;

    public List<MicroInfluencer> processInfluencersList(MicroInfluencerGenerator microInfluencerGenerator, HttpSession sessionObj) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(20));
        Set<MicroInfluencer> influencerSet = new LinkedHashSet<>();
        boolean isInfluencerThreadCompleted = false;
        String apiRequestFormat = getAPIQueryString(microInfluencerGenerator.getSearchQuery().trim());
//        String apiRequestFormat = microInfluencerGenerator.getSearchQuery().trim();
        LOGGER.debug("Encoded query:" + apiRequestFormat);
        int firstPageIndex = 1;
        int lastPageIndex = 2;
        Callable<List<MicroInfluencer>> influencerTask = null;
        Future<List<MicroInfluencer>> influencerFutureObj = null;
        List<Integer> noResponseCount = new ArrayList();
        CountDownLatch latchObj = null;
        sessionObj.setAttribute("isInfluencerThreadRunning", true);
        while (!isInfluencerThreadCompleted) {
            try {
                if (influencerSet.size() < 1000) {
                    latchObj = new CountDownLatch(((lastPageIndex - firstPageIndex) + 1));
                    for (int pageCount = firstPageIndex; pageCount <= lastPageIndex; pageCount++) {
                        influencerTask = new TwitterMicroInfluencerTask(apiRequestFormat, pageCount, latchObj, noResponseCount);
                        influencerFutureObj = threadPoolExecutor.submit(influencerTask);
                        influencerSet.addAll(influencerFutureObj.get());
                        sessionObj.setAttribute("repsonseInflunecerSet", influencerSet);
                        LOGGER.info("Updated influencers size: " + influencerSet.size() + ", firstPageIndex:" + firstPageIndex + ", lastPageIndex:" + lastPageIndex + ", Page count:" + pageCount);
                    }
                    /*To make main task to be wait untill all threads completed*/
                    latchObj.await();
                    /*checking response from API for first 30 pages if count is more than 3 termnating thread pool
                OR fetching data from next 15 pages.*/
                    if (noResponseCount.size() >= 1) {
                        isInfluencerThreadCompleted = true;
                    } else {
                        firstPageIndex = lastPageIndex + 1;
                        lastPageIndex = (firstPageIndex < 48) ? (firstPageIndex + 4) : (firstPageIndex + 2);
                    }
                }else{
                    isInfluencerThreadCompleted = true;
                }
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("InterruptedException when adding influencer object to list from thread: ", e);
            } finally {
                if (isInfluencerThreadCompleted || influencerSet.size() >= 1000 || firstPageIndex >= 50) {
                    threadPoolExecutor.shutdownNow();
                    while (!threadPoolExecutor.isShutdown()) {
                    }
                    isInfluencerThreadCompleted = true;
//                    sessionObj.setAttribute("isInfluencerThreadCompleted", isInfluencerThreadCompleted);
                    sessionObj.removeAttribute("isInfluencerThreadRunning");
                }
            }
        }
        LOGGER.info("Influencers found for search query: " + influencerSet.size());
        List<MicroInfluencer> influnecersList = influencerSet.stream().sorted(Comparator.comparing(MicroInfluencer::getFollowers).reversed()).collect(Collectors.toList());
        return influnecersList;
    }

    public static String getAPIQueryString(String query) {
        String encodeQuery = null;
        try {
            encodeQuery = URLEncoder.encode(query, "UTF-8");
//            encodeQuery = encodeQuery.replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Exception converting in getAPIQueryString: ", e);
        }
        if (encodeQuery != null) {
            StringBuilder builderObj = new StringBuilder(encodeQuery.length());
            char focus;
            for (int i = 0; i < encodeQuery.length(); i++) {
                focus = encodeQuery.charAt(i);
                if (focus == '*') {
                    builderObj.append("%2A");
                } else if (focus == '+') {
                    builderObj.append("%20");
                } else if (focus == '%' && (i + 1) < encodeQuery.length()
                        && encodeQuery.charAt(i + 1) == '7' && encodeQuery.charAt(i + 2) == 'E') {
                    builderObj.append('~');
                    i += 2;
                } else {
                    builderObj.append(focus);
                }
            }
            encodeQuery = builderObj.toString();
        }
        return encodeQuery;
    }

    public String generateInfluencerXLSReport(List<MicroInfluencer> influencerList, String searchQuery, String saveFilePath, long userId) {
        String fileName = null;
        try {
            HSSFWorkbook workBook = new HSSFWorkbook();

            HSSFSheet sheet = workBook.createSheet("Micro Influencer Generator Report");
            sheet.setDisplayRowColHeadings(false);
            sheet.setDisplayGridlines(false);

            CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);

            HSSFRow row;
            HSSFCell cell = null;
            Runtime runtime = Runtime.getRuntime();
            runtime.freeMemory();

            HSSFCellStyle cellStyleObj = null;
            HSSFCellStyle cellNormalStyleObj = null;

            HSSFFont fontNormalObj = null;
            HSSFFont fontBoldObj = null;

            fontNormalObj = getFontObj(workBook, false, (short) 10, HSSFColor.BLACK.index);
            fontBoldObj = getFontObj(workBook, true, (short) 10, HSSFColor.BLACK.index);

            cellStyleObj = getCellStylObj(workBook, getFontObj(workBook, true, (short) 20, HSSFColor.ORANGE.index), false, HorizontalAlignment.CENTER, (short) 0);
            int rowCout = 0;

            //For logo and Reoprt name
            row = sheet.createRow(rowCout);
            row.setHeight((short) 800);
            cell = row.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 1, 4));
            Common.createMergedCell(row, cellStyleObj, 1, 4);
            cell.setCellValue("Micro Influencer Generator Report");
            cell.setCellStyle(cellStyleObj);
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));

            cellStyleObj = getCellStylObj(workBook, fontBoldObj, false, HorizontalAlignment.LEFT, (short) 0);
            cellNormalStyleObj = getCellStylObj(workBook, fontNormalObj, false, HorizontalAlignment.RIGHT, (short) 0);

            cell = row.createCell(5);
            cell.setCellValue("Reviewed on: ");
            cell.setCellStyle(cellNormalStyleObj);

            cellNormalStyleObj = getCellStylObj(workBook, fontNormalObj, false, HorizontalAlignment.GENERAL, (short) 0);

            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            timeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = timeFormat.format(startDat);
            cell = row.createCell(6);
            sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 6, 7));
            Common.createMergedCell(row, cellStyleObj, 6, 7);
            cell.setCellValue(curDate);
            cell.setCellStyle(cellStyleObj);
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 6));

            row = sheet.createRow(++rowCout);
            row.setHeight((short) 300);

            row = sheet.createRow(++rowCout);
            row.setHeight((short) 600);

            cell = row.createCell(1);
//            sheet.addMergedRegion(new CellRangeAddress(rowCout,rowCout, 0, 1));
//            Common.createMergedCell(row, cellStyleObj, 0, 1);
            cell.setCellValue("Search Query: ");
            cell.setCellStyle(cellNormalStyleObj);

            cell = row.createCell(2);
//            sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 2, 3));
            Common.createMergedCell(row, cellStyleObj, 2, 3);
            cell.setCellValue(searchQuery);
            cell.setCellStyle(cellStyleObj);

            row = sheet.createRow(++rowCout);
            row.setHeight((short) 600);
            cell = row.createCell(1);
//           sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 0, 1));
            Common.createMergedCell(row, cellStyleObj, 0, 3);
            cell.setCellValue("Micro influencers found on twitter: ");
            cell.setCellStyle(cellNormalStyleObj);

            cell = row.createCell(2);
//            sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 4, 5));
            Common.createMergedCell(row, cellStyleObj, 4, 5);
            cell.setCellValue((influencerList != null) ? String.valueOf(influencerList.size()) : "NA");
            cell.setCellStyle(cellStyleObj);

            cellStyleObj = getCellStylObj(workBook, getFontObj(workBook, true, (short) 10, HSSFColor.WHITE.index), true, HorizontalAlignment.CENTER, getPaletteObj(workBook, HSSFColor.BLUE.index, 66, 133, 244));
            row = sheet.createRow(++rowCout);
            row.setHeight((short) 300);

            row = sheet.createRow(++rowCout);
            row.setHeight((short) 600);

            int colCount = 0;
            cell = row.createCell(colCount);
            cell.setCellValue("S.No.");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("User Name");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Full Name");
            cell.setCellStyle(cellStyleObj);

//            cell = row.createCell(++colCount);
//            cell.setCellValue("Profile Picture");
//            cell.setCellStyle(cellStyleObj);
            cell = row.createCell(++colCount);
            cell.setCellValue("Followers");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Tweets");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Following");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Likes");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Location");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("WebSite");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("User Since");
            cell.setCellStyle(cellStyleObj);

            cell = row.createCell(++colCount);
            cell.setCellValue("Description");
            cell.setCellStyle(cellStyleObj);

            sheet.createFreezePane(0, 6);

            cellStyleObj = getCellStylObj(workBook, fontNormalObj, true, HorizontalAlignment.RIGHT, (short) 0);
            cellNormalStyleObj = getCellStylObj(workBook, fontNormalObj, true, HorizontalAlignment.GENERAL, (short) 0);
//            HSSFCellStyle cellImageStyleObj = getCellStylObj(workBook, fontNormalObj, true, HSSFCellStyle.ALIGN_JUSTIFY, (short) 0);
            HSSFCellStyle cellEmptyStyleObj = getCellStylObj(workBook, fontBoldObj, true, HorizontalAlignment.CENTER, (short) 0);

            sheet.setColumnWidth(0, 3500);
            sheet.setColumnWidth(1, 7000);
            sheet.setColumnWidth(2, 7000);
//            sheet.setColumnWidth(3, 3000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
            sheet.setColumnWidth(5, 5000);
            sheet.setColumnWidth(6, 5000);
            sheet.setColumnWidth(7, 5000);
            sheet.setColumnWidth(8, 8000);
            sheet.setColumnWidth(9, 5000);
            sheet.setColumnWidth(10, 25000);
            String metricValue = null;
//            boolean isImageInserted = false;
            if (influencerList != null && !influencerList.isEmpty()) {
                try {
                    int x = ++rowCout, k = 0;
                    for (int j = 0; j < influencerList.size(); j++) {
                        k = k + 1;
                        row = sheet.createRow(x);
                        row.setHeightInPoints(40);
                        for (int i = 0; i <= colCount; i++) {
                            cell = row.createCell(i);
                            switch (i) {
                                case 0:
                                    cell.setCellValue(k);
                                    cell.setCellStyle(cellNormalStyleObj);
                                    break;
                                case 1:
                                    cell.setCellValue("@" + influencerList.get(j).getScreenName() != null ? influencerList.get(j).getScreenName().trim() : "-");
                                    cell.setCellStyle(cellNormalStyleObj);
                                    break;
                                case 2:
                                    cell.setCellValue(influencerList.get(j).getName() != null ? influencerList.get(j).getName().trim() : "-");
                                    cell.setCellStyle(cellNormalStyleObj);
                                    break;
//                                case 3:
//                                    metricValue = influencerList.get(j).getProfileImageURL();
//                                    if (metricValue != null && (metricValue.toLowerCase().endsWith(".png")
//                                            || metricValue.toLowerCase().endsWith(".jpeg")
//                                            || metricValue.toLowerCase().endsWith(".jpg"))) {
//                                        isImageInserted = CommonReportGenerator.insertImageInExcellCellFromURL(workBook, sheet, (short) i, x, influencerList.get(j).getProfileImageURL());
//                                        cell.setCellStyle(cellImageStyleObj);
//                                    }
//                                    if (!isImageInserted) {
//                                        cell.setCellValue("Unable to add image here");
//                                        cell.setCellStyle(cellImageStyleObj);
//                                    }
//                                    break;
                                case 3:
                                    cell.setCellValue(influencerList.get(j).getFollowers());
                                    cell.setCellStyle(cellStyleObj);
                                    break;
                                case 4:
                                    cell.setCellValue(influencerList.get(j).getPosts());
                                    cell.setCellStyle(cellStyleObj);
                                    break;
                                case 5:
                                    cell.setCellValue(influencerList.get(j).getFollowing());
                                    cell.setCellStyle(cellStyleObj);
                                    break;
                                case 6:
                                    cell.setCellValue(influencerList.get(j).getLikes());
                                    cell.setCellStyle(cellStyleObj);
                                    break;
                                case 7:
                                    metricValue = influencerList.get(j).getLocation();
                                    if (metricValue != null && !metricValue.trim().equals("") && !metricValue.trim().equalsIgnoreCase("null")) {
                                        cell.setCellValue(influencerList.get(j).getLocation());
                                        cell.setCellStyle(cellNormalStyleObj);
                                    } else {
                                        cell.setCellValue("-");
                                        cell.setCellStyle(cellEmptyStyleObj);
                                    }
                                    break;
                                case 8:
                                    metricValue = influencerList.get(j).getWebsite();
                                    if (metricValue != null && !metricValue.trim().equals("") && !metricValue.trim().equalsIgnoreCase("null")) {
                                        cell.setCellValue(influencerList.get(j).getWebsite());
                                        cell.setCellStyle(cellNormalStyleObj);
                                    } else {
                                        cell.setCellValue("-");
                                        cell.setCellStyle(cellEmptyStyleObj);
                                    }
                                    break;
                                case 9:
                                    metricValue = influencerList.get(j).getUserSince();
                                    if (metricValue != null) {
                                        cell.setCellValue(metricValue);
                                        cell.setCellStyle(cellNormalStyleObj);
                                    } else {
                                        cell.setCellValue("-");
                                        cell.setCellStyle(cellEmptyStyleObj);
                                    }
                                    break;
                                case 10:
                                    metricValue = influencerList.get(j).getDescription();
                                    if (metricValue != null && !metricValue.trim().equals("") && !metricValue.trim().equalsIgnoreCase("null")) {
                                        cell.setCellValue(metricValue);
                                        cell.setCellStyle(cellNormalStyleObj);
                                    } else {
                                        cell.setCellValue("-");
                                        cell.setCellStyle(cellEmptyStyleObj);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        ++x;
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception inserting influencer data into cell: ", e);
                }
            }
            fileName = Common.createFileName(Common.removeSpaces("LXRMarketplace_Micro_Influencer_Generator_Report_" + userId)) + ".xls";
            LOGGER.info("FileName is: " + fileName);
            File file = new File(saveFilePath + fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (IOException e) {
            LOGGER.error("Exception in generating tool report: ", e);
        }
        return fileName;
    }

    private HSSFCellStyle getCellStylObj(HSSFWorkbook workBook, Font fontObj, boolean isBorder, HorizontalAlignment textPosition, short paletteColor) {
        HSSFCellStyle cellObj = null;
        cellObj = workBook.createCellStyle();
        cellObj.setFont(fontObj);
        cellObj.setWrapText(true);
//        if (textPosition != 5) {
        cellObj.setAlignment(textPosition);
//        }
        cellObj.setVerticalAlignment(VerticalAlignment.CENTER);
        if (paletteColor != 0) {
            cellObj.setFillForegroundColor(paletteColor);
            cellObj.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        if (isBorder) {
            cellObj.setBorderBottom(BorderStyle.THIN);
            cellObj.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellObj.setBorderLeft(BorderStyle.THIN);
            cellObj.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellObj.setBorderRight(BorderStyle.THIN);
            cellObj.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellObj.setBorderTop(BorderStyle.THIN);
            cellObj.setTopBorderColor(IndexedColors.BLACK.getIndex());
        }
        return cellObj;
    }

    public HSSFFont getFontObj(HSSFWorkbook workBook, boolean isBold, short fontSize, short fontColor) {
        HSSFFont fontObj = workBook.createFont();
        if (isBold) {
            fontObj.setBold(true);
        }
        if (fontSize != 0) {
            fontObj.setFontHeightInPoints((short) fontSize);
        }
        if (fontColor != 0) {
            fontObj.setColor(fontColor);
        }
        return fontObj;
    }

    public short getPaletteObj(HSSFWorkbook workBook, short colorIndex, int red, int green, int blue) {
        HSSFPalette paletteObj;
        HSSFColor hssfColor = null;
        paletteObj = workBook.getCustomPalette();
        paletteObj.setColorAtIndex(colorIndex, (byte) red, (byte) green, (byte) blue);
        hssfColor = paletteObj.getColor(colorIndex);
        return hssfColor.getIndex();
    }

    public static String getUserCreateDate(String apiResponseDate) {
        if (apiResponseDate != null && apiResponseDate.contains(" ")) {
            String[] splitedDate = apiResponseDate.split(" ");
            String viewDate = splitedDate[1] + " " + splitedDate[2] + ", " + splitedDate[splitedDate.length - 1] + ".";
            return viewDate;
        }
        return null;
    }

    public boolean sendMailMicroInfluencerReport(HttpServletRequest request, MicroInfluencerGenerator processedInfluencerObj,
            long userID, String userEmail, long minFollowers, long maxFollowers, String downloadFolder, boolean isRangeApplied) {
        boolean mailStatus = false;
        try {
            String fileName = null;
            if (processedInfluencerObj != null) {
                List<MicroInfluencer> processedList = processedInfluencerObj.getInfluencersList();
//                processedList = processedList.stream().filter(Influencer -> Influencer.getFollowers() >= microInfoObj.getMinFollowers() && Influencer.getFollowers() <= microInfoObj.getMaxFollowers()).collect(Collectors.toList());
                if (isRangeApplied) {
                    processedList = processedList.stream().filter(Influencer -> Influencer.getFollowers() >= minFollowers && Influencer.getFollowers() <= maxFollowers).collect(Collectors.toList());
                }
                fileName = generateInfluencerXLSReport(processedList, processedInfluencerObj.getSearchQuery(), downloadFolder, userID);
            }
            if (fileName != null) {
                LOGGER.info("Sending report through mail for user");
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, fileName, userEmail, Common.MICRO_INFLUENCER_GENERATOR);
                if (!mailStatus) {
                    LOGGER.info("Sending report through mail for user email: " + userEmail);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Send Mail of MicroInfluencerReport for email: ", e);
        }
        return mailStatus;
    }
}
