/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.microInfluencer;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sagar
 */
public class MicroInfluencer implements Serializable{
    
    /*Common for both Twitter and Instagram*/
    private String id; 
    private String name;
    private String screenName;
    private String description;
    private String website;
    private String userSince;
    private String profileImageURL;
    /*Twitter:The number of followers this account currently has. */
    private long followers;
    /*Twitter: The number of users this account is following*/
    private long following;
    /*Twitter: The number of Tweets (including retweets) issued by the user.
     statuses_count is key in API response.*/
    private long posts;
    
    /*Specific to Twitter channel.*/
    /*The number of Tweets this user has liked in the account’s lifetime.
    favourites_count is key in API response.*/
    private long likes;
    /*The number of public lists that this user is a member of. 
    private long listedIn;*/
    /*The BCP 47 code for the user’s self-declared user interface language.
    private String language;*/
    /*The user-defined location for this account’s profile.*/
    private String location;
    
    /*Tool Related
    private String channelName;*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getUserSince() {
        return userSince;
    }

    public void setUserSince(String userSince) {
        this.userSince = userSince;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public long getFollowing() {
        return following;
    }

    public void setFollowing(long following) {
        this.following = following;
    }

    public long getPosts() {
        return posts;
    }

    public void setPosts(long posts) {
        this.posts = posts;
    }

//    public long getListedIn() {
//        return listedIn;
//    }
//
//    public void setListedIn(long listedIn) {
//        this.listedIn = listedIn;
//    }

//    public String getLanguage() {
//        return language;
//    }
//
//    public void setLanguage(String language) {
//        this.language = language;
//    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }
    
    
    
    @Override
    public boolean equals(Object object) {
        boolean isEqual=false;
        if (object != null && object instanceof MicroInfluencer) {
            MicroInfluencer infu = (MicroInfluencer) object;
            return this.id.equalsIgnoreCase(infu.getId());
        }
            return isEqual;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

}
