/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.penaltychecker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import lxr.marketplace.apiaccess.alexa.AlexaDailyTraffic;
import lxr.marketplace.apiaccess.alexa.AlexaServiceV1;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class PenaltyCheckerService {

    private static Logger logger = Logger.getLogger(PenaltyCheckerService.class);

    private JdbcTemplate jdbcTemplate;
    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    private AlexaServiceV1 alexaAPIService;

    public ArrayList<HashMap<String, String>> fetchWebsiteTrafficData(String domain, Date startDate, Date endDate) {
        ArrayList<HashMap<String, String>> dailyReach = new ArrayList<>();

        Calendar eDate = Calendar.getInstance();
        eDate.setTime(endDate);

        Calendar tempStartDate = Calendar.getInstance();
        tempStartDate.setTime(startDate);
        Calendar tempEndDate = Calendar.getInstance();
        tempEndDate.setTime(startDate);
        tempEndDate.add(Calendar.DATE, 31);

        int diffInDays = 0;
        while (tempEndDate.getTime().compareTo(eDate.getTime()) < 0) {
            dailyReach.addAll(fetchWebsiteTrafficDataFor31Days(domain, tempStartDate, 31));
            logger.debug(tempStartDate.getTime() + " - " + tempEndDate.getTime());
            diffInDays = (int) ((eDate.getTime().getTime() - tempEndDate.getTime().getTime()) / (1000 * 60 * 60 * 24));
            logger.debug("diffInDays: " + diffInDays);
            tempStartDate.setTime(tempEndDate.getTime());
            tempStartDate.add(Calendar.DATE, 1);
            tempEndDate.add(Calendar.DATE, 31);
        }
        if (diffInDays == 0) {
            diffInDays = (int) ((eDate.getTime().getTime() - tempStartDate.getTime().getTime()) / (1000 * 60 * 60 * 24));
            tempStartDate.setTime(startDate);
        }
        dailyReach.addAll(fetchWebsiteTrafficDataFor31Days(domain, tempStartDate, diffInDays));

//        Converting daily visits into weely wise visits
        int tempWeek = 0;
        long weekVisits = 0;
        ArrayList<HashMap<String, String>> weeklyVisitsList = new ArrayList<>();
        HashMap<String, String> weeklyHashMap = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        Calendar cal = Calendar.getInstance();
        for (int i = 0; i < dailyReach.size(); i++) {
            try {
                HashMap<String, String> tempHashmap = dailyReach.get(i);
                String tempDate = tempHashmap.get("date");
                Date date = sdf.parse(tempDate);
                cal.setTime(date);
                int week = cal.get(Calendar.WEEK_OF_YEAR);
                if (tempWeek == week) {
                    weekVisits = Math.round(Double.parseDouble(tempHashmap.get("dailyreach"))) + weekVisits;
                } else {
                    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                    if (weeklyHashMap != null) {
                        weeklyHashMap.put("dailyreach", "" + weekVisits);
                        weeklyVisitsList.add(weeklyHashMap);
                        cal.add(Calendar.DAY_OF_MONTH, 7);
                    }
                    weeklyHashMap = new HashMap<>();
                    weeklyHashMap.put("date", sdf.format(cal.getTime()));
                    tempWeek = week;
                    weekVisits = Math.round(Double.parseDouble(tempHashmap.get("dailyreach")));
                }

            } catch (ParseException ex) {
                logger.error("Exception while converting daily visits into weely wise visits", ex);
            }
        }
        if (weeklyHashMap != null && weeklyHashMap.size() > 0) {
            cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
            cal.add(Calendar.DAY_OF_MONTH, 7);
            weeklyHashMap.put("dailyreach", "" + weekVisits);
//            weeklyHashMap.put("date", sdf.format(cal.getTime()));
            weeklyVisitsList.add(weeklyHashMap);
        }
        return weeklyVisitsList;
    }

    public ArrayList<HashMap<String, String>> fetchWebsiteTrafficDataFor31Days(String domain, Calendar startDate, int range) {
        List<AlexaDailyTraffic> trafficHistory = alexaAPIService.fetchTrafficHistory(domain, startDate, range);
        Calendar endCal = Calendar.getInstance();
        endCal.add(Calendar.DAY_OF_YEAR, -2);
        ArrayList<HashMap<String, String>> dailyPageViewsAndReach = new ArrayList<>();
        SimpleDateFormat displayFormat = new SimpleDateFormat("MMM dd, yyyy");
        if (trafficHistory != null && trafficHistory.size() > 0) {
            trafficHistory.stream().map((td) -> {
                HashMap<String, String> map = new HashMap<>();
                String displayDate = displayFormat.format(td.getCal().getTime());
                map.put("date", displayDate);
                map.put("dailyreach", String.valueOf(td.getReachPerMill()));
                return map;
            }).forEachOrdered((map) -> {
                dailyPageViewsAndReach.add(map);
            });
        } else {
//            If we are not getting data from alexa we are considering that data as 0 for that date range.
            for (int i = 0; i < range; i++) {
                HashMap<String, String> map = new HashMap<>();
                String displayDate = displayFormat.format(startDate.getTime());
                map.put("date", displayDate);
                map.put("dailyreach", "0");
                dailyPageViewsAndReach.add(map);
                startDate.add(Calendar.DATE, 1);
            }
        }
        return dailyPageViewsAndReach;
    }

    public List<GoogleUpdateInfo> getGoogleUpdatesData(String startDate, String endDate) {
        SqlRowSet sqlRowSet = null;
        String query = "SELECT * FROM google_updates_info WHERE DATE_FORMAT(updated_date, '%Y-%m-%d') "
                + "BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d')";
        Object[] params = {startDate, endDate};
        List<GoogleUpdateInfo> googleUpdateInfoList = new ArrayList<>();
        GoogleUpdateInfo googleUpdateInfo = null;
        try {
            sqlRowSet = jdbcTemplate.queryForRowSet(query, params);
            String updateTip = "";
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    googleUpdateInfo = new GoogleUpdateInfo();
                    googleUpdateInfo.setUpdateId(sqlRowSet.getInt("update_id"));
                    googleUpdateInfo.setUpdateType(sqlRowSet.getString("update_type"));
                    googleUpdateInfo.setDescription(sqlRowSet.getString("description"));
                    googleUpdateInfo.setUpdatedDate(sqlRowSet.getString("updated_date"));
                    updateTip = "<p><span class='lxrm-bold'>Google Update</span>: " + sqlRowSet.getString("updated_date") + "</p><p><span class='lxrm-bold'>Updated Date</span>: " + sqlRowSet.getString("update_type") + "</p><p><span class='lxrm-bold'>Description</span>: " + sqlRowSet.getString("description") + "</p>";
                    googleUpdateInfo.setToolTip(updateTip);
                    googleUpdateInfoList.add(googleUpdateInfo);
                }
            }
        } catch (DataAccessException e) {
            logger.error("Exception when checking the user info in google_updates_info table: ", e);
        }
        return googleUpdateInfoList;
    }
}
