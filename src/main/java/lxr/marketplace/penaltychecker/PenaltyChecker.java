/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.penaltychecker;

/**
 *
 * @author anil
 */
public class PenaltyChecker {

    private String domain;
    private String startDate;
    private String endDate;
    private String isAnalyticsAccount;
    private String analyticsFetchType;
    private String device;
    private String profileId;

    public static final String FETCH_BY_WEEK = "week";
    public static final String FETCH_BY_DAY = "day";

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsAnalyticsAccount() {
        return isAnalyticsAccount;
    }

    public void setIsAnalyticsAccount(String isAnalyticsAccount) {
        this.isAnalyticsAccount = isAnalyticsAccount;
    }

    public String getAnalyticsFetchType() {
        return analyticsFetchType;
    }

    public void setAnalyticsFetchType(String analyticsFetchType) {
        this.analyticsFetchType = analyticsFetchType;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

}
