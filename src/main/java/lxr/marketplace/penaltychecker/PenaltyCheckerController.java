/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.penaltychecker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.services.analytics.model.Profile;
import com.google.api.services.analytics.model.Profiles;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.apiaccess.lxrmbeans.AnalyticsData;
import lxr.marketplace.apiaccess.GoogleAnalyticsAPIService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anil
 */
@Controller
@RequestMapping("/penalty-checker-tool.html")
public class PenaltyCheckerController {

    private static Logger logger = Logger.getLogger(PenaltyCheckerController.class);

    @Autowired
    ToolService toolService;
    @Autowired
    EmailTemplateService emailTemplateService;

    @Autowired
    private PenaltyCheckerService penaltyCheckerService;
    @Autowired
    private GoogleAnalyticsAPIService googleAnalyticsAPIService;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private MessageSource messageSource;

    JSONObject userInputJson = null;

    @RequestMapping(method = RequestMethod.GET)
    public String submitDefault(Model model) {
        String authorizationUrl = googleAnalyticsAPIService.generateAuthorizationUrl();
        model.addAttribute("authorizationUrl", authorizationUrl);
        model.addAttribute(new PenaltyChecker());
        return "views/penaltyChecker/penaltyChecker";
    }

    @RequestMapping(params = {"fetchData=traffic-history"}, method = RequestMethod.GET)
    @ResponseBody
    public Object[] getTrafficData(Model model, @ModelAttribute("penaltyChecker") PenaltyChecker penaltyChecker,
            @RequestParam("domain") String domain, HttpSession session, HttpServletRequest request,
            HttpServletResponse response) throws ParseException, JSONException, JsonProcessingException {
        try {
            Login user = (Login) session.getAttribute("user");
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(36);
                toolObj.setToolName(Common.SEO_PENALTY_CHECKER);
                toolObj.setToolLink("penalty-checker-tool.html");
                session.setAttribute("toolObj", toolObj);

                Object[] data = new Object[10];

                if (domain != null && !domain.trim().equals("")) {

                    String finalUrl = Common.getUrlValidationForCmp(domain.trim());
                    if (finalUrl.equals("invalid") || finalUrl.equals("redirected")) {
                        String backLinkUrl = "invalid";
                        session.setAttribute("backLinkUrl", backLinkUrl);
//                        String notWorkingUrl = "Your URL is not working, please check the URL.";
                        String notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
//                        session.setAttribute("notWorkingUrl", notWorkingUrl);
                        data[0] = notWorkingUrl;
                    } else {

                        Calendar startDateCal = Calendar.getInstance();
                        Calendar endDateCal = Calendar.getInstance();
                        endDateCal.add(Calendar.DAY_OF_YEAR, -1);

                        startDateCal.add(endDateCal.YEAR, -2);
                        startDateCal.add(startDateCal.DAY_OF_YEAR, -1);

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat weekFormat = new SimpleDateFormat("YYYYww");
                        SimpleDateFormat updatesFormat = new SimpleDateFormat("MMM dd,yyyy");
//                        String gStartDate = sdf.format(startDateCal.getTime());
//                        String gEndDate = sdf.format(endDateCal.getTime());
                        penaltyChecker.setStartDate(sdf.format(startDateCal.getTime()));
                        penaltyChecker.setEndDate(sdf.format(endDateCal.getTime()));
                        if (penaltyChecker.getDomain() != null && !penaltyChecker.getDomain().trim().isEmpty()) {
                            penaltyChecker.setDomain(penaltyChecker.getDomain().trim());
                        }
                        ArrayList<HashMap<String, String>> dailyReachList = penaltyCheckerService.fetchWebsiteTrafficData(penaltyChecker.getDomain(), startDateCal.getTime(), endDateCal.getTime());

                        // For getting google updates data from our DB
                        List<GoogleUpdateInfo> googleUpdateInfoList = penaltyCheckerService.getGoogleUpdatesData(penaltyChecker.getStartDate(), penaltyChecker.getEndDate());

//                        if (dailyReachList.size() > 0) {
//                            String tempDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                            if (updatesFormat.parse(tempDate).after(endDateCal.getTime())) {
//                                dailyReachList.get(dailyReachList.size() - 1).put("date", updatesFormat.format(endDateCal.getTime()));
//                                gStartDate = dailyReachList.get(0).get("date");
//                                gEndDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                            } else {
//                                gStartDate = dailyReachList.get(0).get("date");
//                                gEndDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                            }
//                        }
                        // This is for list of different type of updates
                        Set<String> updatesList = new LinkedHashSet<>();
                        googleUpdateInfoList.stream().forEach((googleUpdateInfo) -> {
                            updatesList.add(googleUpdateInfo.getUpdateType());
                        });

                        // For getting max value of the page views in that given date range
                        int maxValue = 0;
                        int totalTraffic = 0;

                        for (HashMap<String, String> map : dailyReachList) {
                            for (Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey().equals("date")) {
                                    int dailyReach = Math.round(Float.parseFloat(map.get("dailyreach")));
                                    totalTraffic = totalTraffic + (int) Float.parseFloat(map.get("dailyreach"));
                                    for (int i = 0; i < googleUpdateInfoList.size(); i++) {
                                        String updateDate = weekFormat.format(sdf.parse(googleUpdateInfoList.get(i).getUpdatedDate()));
                                        String trafficDate = weekFormat.format(updatesFormat.parse(entry.getValue()));
                                        if (updateDate.equals(trafficDate)) {
                                            googleUpdateInfoList.get(i).setOrganicTraffic(dailyReach);
                                        }
                                    }
                                    if (dailyReach > maxValue) {
                                        maxValue = dailyReach;
                                    }
                                }
                            }
                        }

                        data[0] = dailyReachList;
                        data[1] = penaltyChecker.getDomain();
                        data[2] = penaltyChecker.getStartDate();
                        data[3] = penaltyChecker.getEndDate();
                        data[4] = totalTraffic;
                        data[5] = googleUpdateInfoList;
                        data[6] = updatesList;
                        data[7] = maxValue;
                        data[8] = null;
                        data[9] = penaltyChecker;

                        Session userSession = (Session) session.getAttribute("userSession");
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                        userSession.addToolUsage(toolObj.getToolId());

//                    if the request is coming from tool page then only add the user inputs to JSONObject
//                    if the request is coming from mail then do not add the user inputs to JSONObject
                        boolean requestFromMail = false;
                        if (session.getAttribute("requestFromMail") != null) {
                            requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                            session.removeAttribute("requestFromMail");
                        }
                        if (!requestFromMail && !user.isAdmin()) {
                            userInputJson = new JSONObject();
                            userInputJson.put("2", false);
                        }
                    }
                }
                session.setAttribute("isAnalytics", false);
                session.setAttribute("penaltyDailyReachInfo", data);
                return data;
            }
        } catch (ParseException | NumberFormatException e) {
            logger.error("Exception in while getting the traffic data from alexa data (getTrafficData method)", e);
        }
        return null;
    }

    @RequestMapping(params = {"gAnalytics=gAnalytics"}, method = RequestMethod.GET)
    public String getAnalyticsData(Model model, HttpSession session, HttpServletRequest request) {
        try {
            Login user = (Login) session.getAttribute("user");
            if (user != null) {
                String authorizationCode = request.getParameter("code");
                GoogleTokenResponse analyticsResponse = googleAnalyticsAPIService.createAnalyticsSetup(authorizationCode);
                if (analyticsResponse != null && analyticsResponse.getRefreshToken() != null && analyticsResponse.getAccessToken() != null) {
                    List<Profiles> analyticsProfilesList = new ArrayList<>();
                    HashMap<String, String> analyticsWebPropertiesList = new HashMap<>();
                    List<String> accountNames = googleAnalyticsAPIService.fetchAllAnalyticsProfiles(analyticsResponse, analyticsProfilesList, analyticsWebPropertiesList);

                    if (analyticsProfilesList.size() > 0) {
                        model.addAttribute("analyticsProfilesList", analyticsProfilesList);
                        model.addAttribute("analyticsAccountNames", accountNames);
                    }

                    session.setAttribute("analyticsProfilesList", analyticsProfilesList);
                    session.setAttribute("analyticsAccountNames", accountNames);
                    session.setAttribute("analyticsWebPropertiesList", analyticsWebPropertiesList);
                    session.setAttribute("analyticsRefreshToken", analyticsResponse.getRefreshToken());
                    session.setAttribute("analyticsAccessToken", analyticsResponse.getAccessToken());
                }
            }
        } catch (Exception e) {
            logger.error("Exception in while getting the Analytics data (getAnalyticsData method)", e);
        }
        String authorizationUrl = googleAnalyticsAPIService.generateAuthorizationUrl();
        model.addAttribute("authorizationUrl", authorizationUrl);
        model.addAttribute(new PenaltyChecker());
        return "views/penaltyChecker/penaltyChecker";
    }

    @RequestMapping(value = "/profileId", method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAnalyticsProfileData(HttpSession session, HttpServletRequest request, HttpServletResponse response,
            Model model, @ModelAttribute("penaltyChecker") PenaltyChecker penaltyChecker) throws ParseException, JSONException {
        Login user = (Login) session.getAttribute("user");
        try {
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(36);
                toolObj.setToolName(Common.SEO_PENALTY_CHECKER);
                toolObj.setToolLink("penalty-checker-tool.html");
                session.setAttribute("toolObj", toolObj);

                /* After providing analytics access when user chooses Analytics Profile from Dashboard popup*/
                logger.info("fetching data for selected profile Id" + penaltyChecker.getProfileId());
//                String analyticsDomain = "";

                // Access Token and Refresh Token
                String analyticsRefreshToken = "";
                String analyticsAccessToken = "";
                if (session.getAttribute("analyticsRefreshToken") != null && session.getAttribute("analyticsAccessToken") != null) {
                    analyticsRefreshToken = (String) session.getAttribute("analyticsRefreshToken");
                    analyticsAccessToken = (String) session.getAttribute("analyticsAccessToken");
                }

                /*    Analytics Profile Data Fetching - new version   */
                List<Profiles> analyticsProfilesList = (List<Profiles>) session.getAttribute("analyticsProfilesList");
                if (analyticsProfilesList != null && analyticsProfilesList.size() > 0) {
                    for (Profiles profiles : analyticsProfilesList) {
                        for (Profile prof : profiles.getItems()) {
                            if (prof.getId().equalsIgnoreCase(penaltyChecker.getProfileId())) {
                                penaltyChecker.setDomain(prof.getWebsiteUrl());
                                break;
                            }
                        }
                    }
                }

                long diffInMilis;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat updatesFormat = new SimpleDateFormat("MMM dd,yyyy");

                if (penaltyChecker.getStartDate() == null || penaltyChecker.getStartDate().equals("")
                        || penaltyChecker.getEndDate() == null || penaltyChecker.getEndDate().equals("")) {
                    Calendar startDateCal = Calendar.getInstance();
                    Calendar endDateCal = Calendar.getInstance();
                    endDateCal.add(Calendar.DAY_OF_YEAR, -1);
                    startDateCal.add(Calendar.YEAR, -2);
                    startDateCal.add(startDateCal.DAY_OF_YEAR, -1);
//                    String gStartDate = sdf.format(startDateCal.getTime());
//                    String gEndDate = sdf.format(endDateCal.getTime());

                    diffInMilis = endDateCal.getTime().getTime() - startDateCal.getTime().getTime();
                    Integer diffInDays = (int) (diffInMilis / (24 * 60 * 60 * 1000));

                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.add(Calendar.DAY_OF_YEAR, -diffInDays);
                    cal2.add(Calendar.DAY_OF_YEAR, -1);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                    penaltyChecker.setStartDate(sdf1.format(cal1.getTime()));
                    penaltyChecker.setEndDate(sdf1.format(cal2.getTime()));

                    penaltyChecker.setAnalyticsFetchType("week");
                    penaltyChecker.setDevice("");
                }
                penaltyChecker.setIsAnalyticsAccount("1");

                logger.debug("DateRange: " + penaltyChecker.getStartDate() + " : " + penaltyChecker.getEndDate());

                /*Fetch Analytics Data for the selected profile id with in a date range*/
                Map<String, AnalyticsData> analyticsDataSet = googleAnalyticsAPIService
                        .fetchLastnDaysAnalyticsData(analyticsAccessToken, analyticsRefreshToken, penaltyChecker.getProfileId(), penaltyChecker.getStartDate(), penaltyChecker.getEndDate(), penaltyChecker.getAnalyticsFetchType(), penaltyChecker.getDevice());

                Object[] data = new Object[10];

                SimpleDateFormat fetchFormat;
                if (penaltyChecker.getAnalyticsFetchType().equals(PenaltyChecker.FETCH_BY_WEEK)) {
                    fetchFormat = new SimpleDateFormat("YYYYww");
                } else {
                    fetchFormat = new SimpleDateFormat("yyyyMMdd");
                }

                // fetching the organic visits from the Analytics data
                ArrayList<HashMap<String, String>> dailyReachList = new ArrayList<>();
                ArrayList<HashMap<String, String>> newUsersList = new ArrayList<>();
                for (Map.Entry<String, AnalyticsData> entry : analyticsDataSet.entrySet()) {
                    HashMap<String, String> dailyReachMap = new HashMap<>();
                    HashMap<String, String> newUsersMap = new HashMap<>();
                    String dateValue;
                    // Converting string to date and again date to string
                    if (fetchFormat.parse(entry.getKey()).getTime() < sdf.parse(penaltyChecker.getStartDate()).getTime()) {
                        dateValue = penaltyChecker.getStartDate();
                    } else if (fetchFormat.parse(entry.getKey()).getTime() > sdf.parse(penaltyChecker.getEndDate()).getTime()) {
                        dateValue = penaltyChecker.getEndDate();
                    } else {
                        dateValue = sdf.format(fetchFormat.parse(entry.getKey()));
                    }

                    dailyReachMap.put("date", dateValue);
                    dailyReachMap.put("dailyreach", String.valueOf(entry.getValue().getOrganicVisits()));
                    dailyReachList.add(dailyReachMap);

                    newUsersMap.put("date", dateValue);
                    newUsersMap.put("newUsers", String.valueOf(entry.getValue().getNewUsers()));
                    newUsersList.add(newUsersMap);
                }
//                Collections.reverse(dailyReachList);

                //  For getting google updates data from our DB
                List<GoogleUpdateInfo> googleUpdateInfoList = penaltyCheckerService.getGoogleUpdatesData(penaltyChecker.getStartDate(), penaltyChecker.getEndDate());

                //  This is for list of different type of updates
                Set<String> updatesList = new LinkedHashSet<>();
                googleUpdateInfoList.stream().forEach((GoogleUpdateInfo googleUpdateInfo) -> {
                    updatesList.add(googleUpdateInfo.getUpdateType());
                });

//                if (dailyReachList.size() > 0) {
//                    String tempDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                    if (sdf.parse(tempDate).after(endDateCal.getTime())) {
//                        dailyReachList.get(dailyReachList.size() - 1).put("date", updatesFormat.format(endDateCal.getTime()));
//                        gStartDate = dailyReachList.get(0).get("date");
//                        gEndDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                    } else {
//                        gStartDate = dailyReachList.get(0).get("date");
//                        gEndDate = dailyReachList.get(dailyReachList.size() - 1).get("date");
//                    }
//                }
                //  For getting max value and Total Traffic of the organic visits in that given date range
                int maxValue = 0;
                int totalTraffic = 0;

                for (HashMap<String, String> map : dailyReachList) {
                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        if (entry.getKey().equals("date")) {
                            int dailyReach = Math.round(Float.parseFloat(map.get("dailyreach")));
                            totalTraffic = totalTraffic + (int) Float.parseFloat(map.get("dailyreach"));
                            String trafficDate = fetchFormat.format(sdf.parse(entry.getValue()));
                            for (int i = 0; i < googleUpdateInfoList.size(); i++) {
                                // Converting string to date and again date to string
                                String updateDate = fetchFormat.format(sdf.parse(googleUpdateInfoList.get(i).getUpdatedDate()));
                                if (updateDate.equals(trafficDate)) {
                                    googleUpdateInfoList.get(i).setOrganicTraffic(dailyReach);
                                }
                            }
                            if (dailyReach > maxValue) {
                                maxValue = dailyReach;
                            }
                        }
                    }
                }
                data[0] = dailyReachList;
                data[1] = penaltyChecker.getDomain();
                data[2] = penaltyChecker.getStartDate();
                data[3] = penaltyChecker.getEndDate();
                data[4] = totalTraffic;
                data[5] = googleUpdateInfoList;
                data[6] = updatesList;
                data[7] = maxValue;
                data[8] = newUsersList;
                data[9] = penaltyChecker;

                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());

                //  if the request is coming from tool page then only add the user inputs to JSONObject
                // if the request is coming from mail then do not add the user inputs to JSONObject
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    userInputJson.put("2", true);
                }
                session.setAttribute("isAnalytics", true);
                session.setAttribute("penaltyDailyReachInfo", data);
                return data;
            }
        } catch (ParseException | NumberFormatException | JSONException e) {
            logger.error("While fetching analytics data using profile-id ", e);
        }
        return null;
    }

    @RequestMapping(params = {"ate=penaltyCheckerATE"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            Model model, @RequestParam("domain") String domain) {
        Session userSession = (Session) session.getAttribute("userSession");
        Login user = (Login) session.getAttribute("user");
        String toolIssues = "Want to get full audit of your website on how you are penalized by Google algorithm updates?";
        String questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
        questionInfo += "- Auditing on-page SEO factors for my website.";
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain.trim());
        if (userInputJson != null) {
            lxrmAskExpert.setOtherInputs(userInputJson.toString());
        }
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.SEO_PENALTY_CHECKER);
        lxrmAskExpertService.addInJson(lxrmAskExpert, response);
        // if the request is not from  mail then we should add this input to the session object
        if (userInputJson != null) {
            String toolUrl = "/penalty-checker-tool.html";
            userSession.addUserAnalysisInfo(user.getId(), 36, domain, toolIssues,
                    questionInfo, userInputJson.toString(), toolUrl);
            userInputJson = null;
        }
        return null;
    }

    @RequestMapping(params = {"backToSuccessPage"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] backToSuccessPage(Model model, HttpSession session, HttpServletRequest request) {
        Object[] data = null;
        if (session.getAttribute("penaltyDailyReachInfo") != null) {
            data = (Object[]) session.getAttribute("penaltyDailyReachInfo");
        }
        return data;
    }

    @RequestMapping(params = {"clear"}, method = RequestMethod.POST)
    public String clearPage(Model model, HttpSession session, HttpServletRequest request) {
        String authorizationUrl = googleAnalyticsAPIService.generateAuthorizationUrl();
        model.addAttribute("authorizationUrl", authorizationUrl);
        model.addAttribute(new PenaltyChecker());
        session.removeAttribute("penaltyDailyReachInfo");
        return "views/penaltyChecker/penaltyChecker";
    }
}
