/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.penaltychecker;

/**
 *
 * @author anil
 */
public class GoogleUpdateInfo {
    
    private int updateId;
    private String updateType;
    private String description;
    private String updatedDate;
    private String toolTip;
    private long organicTraffic;
    
    public int getUpdateId() {
        return updateId;
    }

    public void setUpdateId(int updateId) {
        this.updateId = updateId;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
    
    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public long getOrganicTraffic() {
        return organicTraffic;
    }

    public void setOrganicTraffic(long organicTraffic) {
        this.organicTraffic = organicTraffic;
    }


    
}
