package lxr.marketplace.competitoranalysis;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CompetitorAnalysisResult implements Serializable{

    String url;
    String totalWordCount;
    long imageCount;
    String pageSize;
    String flash;
    String framesCount;
    long linkCount;
    String bodytext;
    String titleTag;
    String descriptionTag;
    String keywordsTag;
    String altTags;
    String linkText;
    String textbyhtmlratio;
    Map<Integer, List<String>> totalHeadingsList;
    String favicon;
    String wwwresolve;
    long containAlttext;
    String Language;
    String loadtime;

    public CompetitorAnalysisResult(){
    }
    public long getContainAlttext() {
        return containAlttext;
    }

    public void setContainAlttext(long containAlttext) {
        this.containAlttext = containAlttext;
    }

    public String getFavicon() {
        return favicon;
    }

    public String getWwwresolve() {
        return wwwresolve;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }

    public void setWwwresolve(String wwwresolve) {
        this.wwwresolve = wwwresolve;
    }

    public String getLoadtime() {
        return loadtime;
    }

    public void setLoadtime(String loadtime) {
        this.loadtime = loadtime;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getTextbyhtmlratio() {
        return textbyhtmlratio;
    }

    public void setTextbyhtmlratio(String textbyhtmlratio) {
        this.textbyhtmlratio = textbyhtmlratio;
    }

    public String getBodytext() {
        return bodytext;
    }

    public void setBodytext(String bodytext) {
        this.bodytext = bodytext;
    }

    public String getTitleTag() {
        return titleTag;
    }

    public void setTitleTag(String titleTag) {
        this.titleTag = titleTag;
    }

    public String getDescriptionTag() {
        return descriptionTag;
    }

    public void setDescriptionTag(String descriptionTag) {
        this.descriptionTag = descriptionTag;
    }

    public String getKeywordsTag() {
        return keywordsTag;
    }

    public void setKeywordsTag(String keywordsTag) {
        this.keywordsTag = keywordsTag;
    }

    public String getAltTags() {
        return altTags;
    }

    public void setAltTags(String altTags) {
        this.altTags = altTags;
    }

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getTotalWordCount() {
        return totalWordCount;
    }

    public void setTotalWordCount(String totalWordCount) {
        this.totalWordCount = totalWordCount;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getFlash() {
        return flash;
    }

    public void setFlash(String flash) {
        this.flash = flash;
    }

    public String getFramesCount() {
        return framesCount;
    }

    public void setFramesCount(String framesCount) {
        this.framesCount = framesCount;
    }

    public long getImageCount() {
        return imageCount;
    }

    public void setImageCount(long imageCount) {
        this.imageCount = imageCount;
    }

    public long getLinkCount() {
        return linkCount;
    }

    public void setLinkCount(long linkCount) {
        this.linkCount = linkCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<Integer, List<String>> getTotalHeadingsList() {
        return totalHeadingsList;
    }

    public void setTotalHeadingsList(Map<Integer, List<String>> totalHeadingsList) {
        this.totalHeadingsList = totalHeadingsList;
    }
    
    
}
