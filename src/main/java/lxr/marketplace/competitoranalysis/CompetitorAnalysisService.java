package lxr.marketplace.competitoranalysis;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import lxr.marketplace.util.search.BoyerMooreSearch;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class CompetitorAnalysisService {

    private static Logger logger = Logger.getLogger(CompetitorAnalysisService.class);

    long acount = 0, icount = 0, bytes;
    String tmpredirectedurl = "";
    ApplicationContext context = ApplicationContextProvider.getApplicationContext();

    public String getTotalWordCount(String url, Document doc, String content) {
        int index = 0;
        long numWords = 0;
        char c1 = 0;
        try {
            if (content != null) {
                boolean prevwhitespace = true;
                while (index < content.length()) {
                    char c = content.charAt(index++);
                    if ((index + 0) < content.length()) {
                        c1 = content.charAt(index + 0);
                    }
                    boolean currwhitespace = Character.isWhitespace(c);
                    if (c == ',' && !(c1 == ' ')) {
                        numWords++;
                    }
                    if (prevwhitespace && !currwhitespace) {
                        numWords++;
                    }
                    prevwhitespace = currwhitespace;
                }
            }
        } catch (Exception ex) {
            logger.error("Exception on fetching total word count:", ex);
        }
        String NumWords = formatValue(numWords);
        return NumWords;
    }

    public String getPageSize(Document doc) {
        if (doc.toString().length() != 0) {
            bytes = doc.toString().length() / 1024;
        }
        String Bytes = formatValue(bytes);
        return Bytes;
    }

    public String formatValue(long val) {
        DecimalFormat df = new DecimalFormat();
        df.getDecimalFormatSymbols().setGroupingSeparator(',');
        df.setGroupingSize(3);
        String value = df.format(val);
        return value;
    }

    public long imageCount(String url, Document doc, Elements imgtags) {
        icount = imgtags.size();
        return icount;
    }

    public long containAltText(String url, Document doc, Elements imgtags) {
        int textcount = 0;
        if (imgtags != null) {
            textcount = imgtags.stream().map((imgtag) -> imgtag.attr("alt")).filter((alttext) -> (!" ".equals(alttext.trim()) && alttext.length() > 0 && !(alttext.trim().equals(" ")) && !(alttext.trim().equals("")) && !(alttext.trim() == null))).map((_item) -> 1).reduce(textcount, Integer::sum);
        }
        return textcount;
    }

    public long linkCount(String url, Document doc, Elements anchors) {
        acount = anchors.size();
        return acount;
    }

    public String isFrame(Document doc) {
        Elements iframes;
        Elements frames;
        String framesused;
        iframes = doc.select("iframe");
        frames = doc.select("frame");
        if (iframes.size() > 0 || frames.size() > 0) {
            framesused = "Frames are used";
        } else {
            framesused = "Frames are not used";
        }
        return framesused;
    }

    public String isFlash(Document doc) {
        Elements flashtags;
        Elements flashtags1;
        Elements iframeflash;
        String flash;
        flashtags = doc.select("object");
        flashtags1 = doc.select("embed");
        iframeflash = doc.select("iframe[src~=(youtube\\.com|vimeo\\.com)]");
        if (flashtags.size() > 0 || flashtags1.size() > 0 || iframeflash.size() > 0) {
            flash = "Flash is used";
        } else {
            flash = "Flash is not used";
        }
        return flash;
    }

    public double bodyTextkeywordCount(String url, Document doc, String keyword, String content) {
        double keyCount = 0;
        try {
            BoyerMooreSearch search = new BoyerMooreSearch(keyword, 1);
            keyCount = search.find(content);
        } catch (Exception e) {
            logger.error(e);
        }
        return keyCount;
    }

    public String keywordDensityInpercentage(String url, Document doc, String keyword, String text) {
        double keyworddensity = 0;
        long keywordDensity;
        double numofoccrencesofaphrase = bodyTextkeywordCount(url, doc,
                keyword, text);
        String TotalWords = getTotalWordCount(url, doc, text);
        double totalwords = Double.parseDouble((TotalWords.replace(",", "")));
        double wordsofaphrase = wordsOfaPhrase(url, keyword);
        if (totalwords != 0) {
            keyworddensity = Math
                    .ceil(((numofoccrencesofaphrase * 100) / (totalwords - ((wordsofaphrase - 1) * numofoccrencesofaphrase))));
        }
        keywordDensity = (long) keyworddensity;
        return keywordDensity + " " + "%";
    }

    public double wordsOfaPhrase(String url, String searchQuery) {
        double wordsofphrase = 0;
        int index1 = 0;
        try {
            boolean prevwhitespace = true;
            while (index1 < searchQuery.length()) {
                char c = searchQuery.charAt(index1++);
                char c1 = 0;
                if ((index1 + 0) < searchQuery.length()) {
                    c1 = searchQuery.charAt(index1 + 0);
                }
                boolean currwhitespace = Character.isWhitespace(c);
                if (c == ',' && !(c1 == ' ')) {
                    wordsofphrase++;
                }
                if (prevwhitespace && !currwhitespace) {
                    wordsofphrase++;
                }
                prevwhitespace = currwhitespace;
            }
        } catch (Exception ex) {
            logger.error(ex);
            //System.exit(1);
        }
        return wordsofphrase;
    }

    public String KeywordinTitle(String url, Document doc, String keyword, String content) {
        String title;
        double numofoccrencesofaphrase = bodyTextkeywordCount(url, doc,
                keyword, content);
        if (numofoccrencesofaphrase != 0) {
            title = "Yes";
        } else {
            title = "No";
        }
        return title;
    }

    public Map<Integer, List<String>> genearteHeadingTagsList(Document doc) {
        Map<Integer, List<String>> TotalHeadings = new HashMap<>();
        if (doc.select("h1") != null) {
            TotalHeadings.put(1, getTextFromElements(doc.select("h1")));
        } else {
            TotalHeadings.put(1, new ArrayList<>());
        }
        if (doc.select("h2") != null) {
            TotalHeadings.put(2, getTextFromElements(doc.select("h2")));
        } else {
            TotalHeadings.put(2, new ArrayList<>());
        }
        if (doc.select("h3") != null) {
            TotalHeadings.put(3, getTextFromElements(doc.select("h3")));
        } else {
            TotalHeadings.put(3, new ArrayList<>());
        }
        if (doc.select("h4") != null) {
            TotalHeadings.put(4, getTextFromElements(doc.select("h4")));
        } else {
            TotalHeadings.put(4, new ArrayList<>());
        }
        if (doc.select("h5") != null) {
            TotalHeadings.put(5, getTextFromElements(doc.select("h5")));
        } else {
            TotalHeadings.put(5, new ArrayList<>());
        }
        if (doc.select("h6") != null) {
            TotalHeadings.put(6, getTextFromElements(doc.select("h6")));
        } else {
            TotalHeadings.put(6, new ArrayList<>());
        }
        return TotalHeadings;
    }

    public List<String> getTextFromElements(Elements elementObj) {
        List<String> textList = null;
        if (elementObj != null) {
            textList = new ArrayList<>();
            for (Element ele : elementObj) {
                if (ele != null) {
                    textList.add(ele.text());
                }
            }
        }
        return textList;
    }

    public String getLanguage(Document doc) {
        Elements htmltag = doc.getElementsByAttribute("lang");
        Element templang1 = htmltag.first();
        String lang;
        if (templang1 != null) {
            lang = templang1.attr("lang");
        } else {
            lang = "Language missing";
        }
        return lang;
    }

    public String findFavIcon(String url, Document doc) {
        String Url = " ";
        String keyword = ".ico";
        String favoriteicon = " ";
        int sc = 0;
        try {
            String imports = doc.select("link[href]").toString();
            URL url1 = new URL(url);
            String domain = url1.getProtocol() + "://" + url1.getHost();
            if (!(url.endsWith("/"))) {
                Url = url + "/favicon.ico";
            } else if (url.endsWith("/")) {
                Url = url + "favicon.ico";
            }
            Connection.Response resp;
            try {
                resp = Jsoup.connect(Url).userAgent(Common.USER_AGENT)
                        .timeout(100000).ignoreHttpErrors(true).execute();
                sc = resp.statusCode();
            } catch (Exception e) {
                logger.error(e);
            }
            int fav = 0;
            if (imports.contains(domain) && sc > 200) {
                try {
                    BoyerMooreSearch search = new BoyerMooreSearch(keyword, 1);
                    fav = search.find(imports);
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            if (sc == 200 || fav > 0) {
                favoriteicon = "Favicon is present.";
            } else {
                favoriteicon = "Favicon is not present.";
            }
        } catch (Exception e1) {
            logger.error(e1);
        }
        return favoriteicon;
    }

    public String checkingwwwResolve(String url) {
        String mainurl = url;
        String reslovemsg = " ";
        int statuscode = 0;
        URL fullurl;
        URL tempUrl;
        int responseCode = 0;
        HttpURLConnection fullwwwUrlConn = null, tempUrlConn = null;
        try {
            String loc;
            if (!(url.contains("www")) && url.startsWith("http")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("http://", "http://www."));
            } else if (!(url.contains("www")) && url.startsWith("https")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("https://", "https://www."));
            } else if (url.contains("www")) {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url.replace("www.", ""));
            } else {
                fullurl = new URL(mainurl);
                tempUrl = new URL(url);
            }
            try {
                fullwwwUrlConn = (HttpURLConnection) fullurl.openConnection();
                fullwwwUrlConn.setInstanceFollowRedirects(false);
                fullwwwUrlConn.connect();
                statuscode = fullwwwUrlConn.getResponseCode();
                tempUrlConn = (HttpURLConnection) tempUrl.openConnection();
                tempUrlConn.setInstanceFollowRedirects(false);
                try {
                    tempUrlConn.connect();
                } catch (UnknownHostException ue) {
                    logger.error(ue);
                }
                responseCode = tempUrlConn.getResponseCode();
            } catch (IOException e1) {
                logger.error("IOException in checkingwwwResolve", e1);
            }
            try {
                if (statuscode == 200 && (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307)) {
                    loc = tempUrlConn.getHeaderField("Location");
                    if (loc.equals(url + "/") || loc.equals(url)) {
                        reslovemsg = "The website with and without www redirects to the same page.";
                    } else {
                        reslovemsg = "It is not redirected to same site.";
                    }
                } else if (responseCode > 400 || responseCode == 0) {
                    reslovemsg = "It is not redirected to same site.";
                } else if ((statuscode == 301 || statuscode == 302 || statuscode == 306 || statuscode == 307) && tempUrlConn.getResponseCode() == 200) {
                    String loction = fullwwwUrlConn.getHeaderField("Location");
                    if (loction.equals(tempUrlConn.getURL().toString())
                            || loction.equals(tempUrlConn.getURL().toString() + '/')) {
                        reslovemsg = "The website with and without www redirects to the same page.";
                    } else {
                        reslovemsg = "It is not redirected to same site.";
                    }
                } else if (statuscode == 200 && responseCode == 200) {
                    reslovemsg = "It is not redirected to same site.";
                } else if ((statuscode == 301 || statuscode == 302 || statuscode == 306 || statuscode == 307) && (responseCode == 301 || responseCode == 302 || responseCode == 306 || responseCode == 307)) {
                    reslovemsg = "It is not redirected to same site.";
                }
            } catch (Exception e3) {
                logger.error("Exception in checkingwwwResolve", e3);
            }
        } catch (MalformedURLException me) {
            logger.error("MalformedURLException in checkingwwwResolve", me);
        }
        return reslovemsg;
    }

    public boolean checkProperRedirection(URL primaryUrl, URL redirectedUrl) throws IOException {
        tmpredirectedurl = redirectedUrl.getHost();
        String primaryHost = primaryUrl.getHost().replace("www.", "");
        String redirectedHost = redirectedUrl.getHost().replace("www.", "");
        return !primaryHost.equals(redirectedHost);
    }

    public String getTempRedirectedurl() throws IOException {
        return tmpredirectedurl;
    }

    public String createExcelFile(HttpSession session, String targetFilePath) {
        CompetitorAnalysisTool competitorAnalysisTool = (CompetitorAnalysisTool) session.getAttribute("competitorAnalysistool");
        CompetitorAnalysisResult companalysisResult = (CompetitorAnalysisResult) session.getAttribute("yoursite");
        List<CompetitorAnalysisResult> companalysisResultList = (List<CompetitorAnalysisResult>) session.getAttribute("compsite");

        HSSFRow curRow, curRow1;
        HSSFCell curCel;
//        Map<Integer, Elements> headIngs = new HashMap<>();
        Map<Integer, List<String>> headIngsList = new HashMap<>();
        String filename = null;
        try {
            int colCount = 2 + companalysisResultList.size(), rowInd = 0;
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFCellStyle heaCelSty, colHeaCelSty, dblCelSty, headingCellSty, mainheadCellSty, fieldtextSty, densitySty, textalignSty;
            HSSFFont font, subheadingfont, headfont;
            //for bold text
            font = workBook.createFont();
            font.setBold(true);
            //for main heading & orange color
            headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);
            //for sub headings & orange color
            subheadingfont = workBook.createFont();
            subheadingfont.setBold(true);
            subheadingfont.setColor(HSSFColor.ORANGE.index);
            //color & not bold & left alignment
            headingCellSty = workBook.createCellStyle();
            headingCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            headingCellSty.setFillForegroundColor(HSSFColor.LEMON_CHIFFON.index);
            headingCellSty.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headingCellSty.setAlignment(HorizontalAlignment.LEFT);
            headingCellSty.setWrapText(true);
            //for main heading
            mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);
            //for text wrapping  & no color
            densitySty = workBook.createCellStyle();
            densitySty.setWrapText(true);
            densitySty.setVerticalAlignment(VerticalAlignment.CENTER);
            //for center alignment  and color
            textalignSty = workBook.createCellStyle();
            textalignSty.setVerticalAlignment(VerticalAlignment.CENTER);
            textalignSty.setFillForegroundColor(HSSFColor.LEMON_CHIFFON.index);   //251, 248, 239
            textalignSty.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            textalignSty.setAlignment(HorizontalAlignment.CENTER);
            //for color & bold
            fieldtextSty = workBook.createCellStyle();
            fieldtextSty.setFillForegroundColor(HSSFColor.LEMON_CHIFFON.index);   //251, 248, 239
            fieldtextSty.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            fieldtextSty.setVerticalAlignment(VerticalAlignment.CENTER);
            fieldtextSty.setFont(font);
            int colWidth[] = {0, 18, 60, 60, 40, 22};
            // Double Data Cell Style
            dblCelSty = workBook.createCellStyle();
            dblCelSty.setAlignment(HorizontalAlignment.CENTER);
            dblCelSty.setVerticalAlignment(VerticalAlignment.CENTER);
            // for sub headings Cell Style
            heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(subheadingfont);
            heaCelSty.setAlignment(HorizontalAlignment.CENTER);
            heaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);
            // Column Heading Cell Style
            colHeaCelSty = workBook.createCellStyle();
            colHeaCelSty.setFont(font);
            colHeaCelSty.setWrapText(true);
            colHeaCelSty.setAlignment(HorizontalAlignment.LEFT);
            colHeaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFSheet sheet = workBook.createSheet("Competitor Analysis Report");
            sheet.setDefaultRowHeight((short) 500);
            boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
            curRow1 = sheet.createRow(0);
            curCel = curRow1.createCell(1);
            curCel.setCellValue("Competitor Analysis Report");
            sheet.addMergedRegion(new CellRangeAddress(rowInd, rowInd, 1, companalysisResultList.size() + 1));
            mainheadCellSty.setFont(headfont);
            curCel.setCellStyle(mainheadCellSty);
            curRow1.setHeight((short) 1000);
            if (logoAdded) {
                rowInd = 0;
            }
            curRow = sheet.createRow(++rowInd);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(rowInd, rowInd, 0, companalysisResultList.size() + 1));
            curCel.setCellValue("Competitive Search Engine Friendly Metrics");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(heaCelSty);
            // for merging cells

            curRow = sheet.createRow(rowInd + 1);
            for (int colInd = 1; colInd <= colCount; colInd++) {
                curCel = curRow.createCell(colInd - 1);
                switch (colInd) {
                    case 1:
                        curCel.setCellValue("Metrics");
                        break;
                    case 2:
                        curCel.setCellValue("Your Website");
                        break;
                    case 3:
                        curCel.setCellValue("Competitor Website");
                        break;
                    case 4:
                        curCel.setCellValue("Competitor Website");
                        break;
                    default:
                        break;
                }
                curCel.setCellStyle(colHeaCelSty);
                curCel.setCellStyle(heaCelSty);
                heaCelSty.setFont(subheadingfont);
                sheet.setColumnWidth(colInd - 1, (int) (441.3793d + 256d * (colWidth[colInd] - 1d)));
            }
            /*int totalsize = companalysisResult.getTotalHeadings().get(1).size() + companalysisResult.getTotalHeadings().get(2).size()
                    + companalysisResult.getTotalHeadings().get(3).size() + companalysisResult.getTotalHeadings().get(4).size()
                    + companalysisResult.getTotalHeadings().get(5).size() + companalysisResult.getTotalHeadings().get(6).size();
            int totalcompsize = companalysisResult1.getTotalHeadings().get(1).size() + companalysisResult1.getTotalHeadings().get(2).size()
                    + companalysisResult1.getTotalHeadings().get(3).size() + companalysisResult1.getTotalHeadings().get(4).size()
                    + companalysisResult1.getTotalHeadings().get(5).size() + companalysisResult1.getTotalHeadings().get(6).size();*/
            int totalsize = companalysisResult.getTotalHeadingsList().values().stream().mapToInt(list -> list.size()).sum();
            int totalcompsize = 0;
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                totalcompsize = totalcompsize + competitorAnalysisResult.getTotalHeadingsList().values().stream().mapToInt(list -> list.size()).sum();
            }
            int headsize = totalsize > totalcompsize ? totalsize : totalcompsize;
            int totalRows = 35 + headsize;
            int tempRowInd = rowInd + 1;
            List<HSSFRow> rowList = new ArrayList<>();
            for (int i = 0; i < totalRows; i++) {
                rowList.add(sheet.createRow(tempRowInd + 1 + i));
            }
            int v = 0;
            curRow = rowList.get(v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("\t" + "Domain Name");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Page Size");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.pagesize", null, Locale.UK));
            curCel.setCellStyle(densitySty);
            curRow.setHeight((short) 800);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Word Count");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.wordcount", null, Locale.UK));
            curCel.setCellStyle(headingCellSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Image Count");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            long alttext = companalysisResult.getContainAlttext();
            if (alttext > 0 && alttext == companalysisResult.getImageCount()) {
                curCel.setCellValue(context.getMessage("message.positive", null, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
            } else if (alttext > 0 && alttext != companalysisResult.getImageCount()) {
                Object[] imagecnt = new Long[1];
                imagecnt[0] = alttext;
                curCel.setCellValue(context.getMessage("message.negative", imagecnt, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
            } else if (alttext == 0 && companalysisResult.getImageCount() != 0) {
                curCel.setCellValue(context.getMessage("message.positive", null, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
            }
            curRow.setHeight((short) 800);
            curCel.setCellStyle(densitySty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Link Count");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.linkcount", null, Locale.UK));
            curCel.setCellStyle(headingCellSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Flash");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            String flash = companalysisResult.getFlash();
            if (flash.trim().equals("Flash is used")) {
                curCel.setCellValue(context.getMessage("message.negativeflash", null, Locale.UK));
            } else if ((flash.trim().equals("Flash is not used"))) {
                curCel.setCellValue(context.getMessage("message.positiveflash", null, Locale.UK));
            }
            curCel.setCellStyle(densitySty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Frames");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            String frame = companalysisResult.getFramesCount();
            if (frame.trim().equals("Frames are used")) {
                curCel.setCellValue(context.getMessage("message.negativeframes", null, Locale.UK));
            } else if ((frame.trim().equals("Frames are not used"))) {
                curCel.setCellValue(context.getMessage("message.positiveframes", null, Locale.UK));
            }
            curCel.setCellStyle(headingCellSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Text / Html Ratio");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.texthtmlratio", null, Locale.UK) + "\n" + context.getMessage("message.texthtmlrecom1", null, Locale.UK) + "\n"
                    + context.getMessage("message.texthtmlrecom2", null, Locale.UK) + "\n" + context.getMessage("message.texthtmlrecom3", null, Locale.UK));
            curRow.setHeight((short) 1000);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Favicon");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.favicon", null, Locale.UK));
            curRow.setHeight((short) 800);
            curCel.setCellStyle(headingCellSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Language");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            String language = companalysisResult.getLanguage();
            if (language.trim().equals("Language missing")) {
                curCel.setCellValue(context.getMessage("message.negativelanguage", null, Locale.UK));
            } else if (!(language.trim().equals("Language missing"))) {
                curCel.setCellValue(context.getMessage("message.positivelanguage", null, Locale.UK));
            }
            curCel.setCellStyle(densitySty);
            curRow.setHeight((short) 800);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("Load Time");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.loadtime", null, Locale.UK));
            curCel.setCellStyle(headingCellSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum() + 1, 0, 0));
            curCel.setCellValue("WWW Resolve");
            curCel.setCellStyle(colHeaCelSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.wwwresolve", null, Locale.UK));
            curCel.setCellStyle(densitySty);
            curRow.setHeight((short) 1000);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Headings (H1,H2...H6)");
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 0, companalysisResultList.size() + 1));
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H1");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H2");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H3");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H4");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H5");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = rowList.get(++v);
            curCel = curRow.createCell(0);
            curCel.setCellValue("H6");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            int temprowind1 = 1 + v + headsize;
            for (int s = ++v; s <= temprowind1; s++) {
                curRow = rowList.get(s);
                curCel = curRow.createCell(0);
                curCel.setCellValue(" ");
                curCel.setCellStyle(colHeaCelSty);
                curCel.setCellStyle(headingCellSty);
            }

            curRow = rowList.get(temprowind1);
            curCel = curRow.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 1, companalysisResultList.size() + 1));
            curCel.setCellValue(context.getMessage("message.headings", null, Locale.UK));
            curCel.setCellStyle(headingCellSty);
            curRow.setHeight((short) 1200);

            for (int x = 0; x <= companalysisResultList.size(); x++) {
                CompetitorAnalysisResult result = new CompetitorAnalysisResult();
                int col = 0;
                if (x == 0) {
                    result = companalysisResult;
                    col = 1;
                } else if (x == 1) {
                    result = companalysisResultList.get(0);
                    col = 2;
                } else if (x == 2) {
                    result = companalysisResultList.get(1);
                    col = 3;
                }
                int z = 0, w = 0;
                curRow = rowList.get(z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getUrl());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getPageSize() + "  " + "Kb");
                curCel.setCellStyle(dblCelSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getTotalWordCount());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getImageCount());
                curCel.setCellStyle(dblCelSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getLinkCount());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getFlash());
                curCel.setCellStyle(dblCelSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getFramesCount());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getTextbyhtmlratio());
                curCel.setCellStyle(dblCelSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getFavicon());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getLanguage());
                curCel.setCellStyle(dblCelSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getLoadtime());
                curCel.setCellStyle(textalignSty);

                curRow = rowList.get(++w + ++z);
                curCel = curRow.createCell(col);
                curCel.setCellValue(result.getWwwresolve());
                curCel.setCellStyle(dblCelSty);

                int c = 1 + (++w + ++z);
//                headIngs = result.getTotalHeadings();
                headIngsList = result.getTotalHeadingsList();
                for (int u = c; u < v; u++) {
                    curRow = rowList.get(u);
                    curCel = curRow.createCell(col);
//                    curCel.setCellValue(headIngs.get(u - c + 1).size());
                    if ((u - c + 1) <= headIngsList.size()) {
                        curCel.setCellValue(headIngsList.get(u - c + 1).size());
                    } else {
                        curCel.setCellValue(" ");
                    }
                    curCel.setCellStyle(textalignSty);
                }
                int counter = 1;
                int counter2 = 0;
                for (int h = 0; h < headsize; h++) {
                    if (counter < 7 && counter2 >= headIngsList.get(counter).size()) {
                        counter2 = 0;
                        counter++;
                        while (counter < 7 && headIngsList.get(counter).isEmpty()) {
                            counter++;
                        }
                    }
                    if (counter == 7) {
                        curRow = rowList.get(h + v);
                        curCel = curRow.createCell(col);
                        curCel.setCellStyle(headingCellSty);
                    } else {
                        curRow = rowList.get(h + v);
                        curCel = curRow.createCell(col);
                        curCel.setCellValue("->" + "[" + "H" + counter + "]" + " " + headIngsList.get(counter).get(counter2++));
                        curCel.setCellStyle(headingCellSty);
                    }
                }
            }
            int secrowind = temprowind1 + 8;
            curRow = sheet.createRow(secrowind);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Competitive Keyword Density for " + "\"" + competitorAnalysisTool.getSearchPhrase() + "\"");
            sheet.addMergedRegion(new CellRangeAddress(secrowind, secrowind, 0, companalysisResultList.size() + 1));
            curCel.setCellStyle(heaCelSty);
            curRow = sheet.createRow(secrowind + 1);
            for (int colInd = 1; colInd <= colCount; colInd++) {
                curCel = curRow.createCell(colInd - 1);
                switch (colInd) {
                    case 1:
                        curCel.setCellValue("Keyword Density in");
                        break;
                    case 2:
                        curCel.setCellValue("Your Website");
                        break;
                    case 3:
                        curCel.setCellValue("Competitor Website");
                        break;
                    case 4:
                        curCel.setCellValue("Competitor Website");
                        break;
                    default:
                        break;
                }
                curCel.setCellStyle(colHeaCelSty);
                curCel.setCellStyle(heaCelSty);
                sheet.setColumnWidth(colInd - 1, (int) (441.3793d + 256d * (colWidth[colInd] - 1d)));
            }
            List<HSSFRow> secondrowList = new ArrayList<>();
            int secRows = secrowind + 6;
            int tempsecrowind = secrowind + 1;
            for (int k = 0; k < secRows; k++) {
                secondrowList.add(sheet.createRow(tempsecrowind + 1 + k));
            }
            int s = 0;
            curRow = secondrowList.get(s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Domain Name");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Title Tag");
            curCel.setCellStyle(colHeaCelSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Description Tag");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Keywords Tag");
            curCel.setCellStyle(colHeaCelSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Body Text");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Alt tags");
            curCel.setCellStyle(colHeaCelSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            curCel.setCellValue("Link text");
            curCel.setCellStyle(colHeaCelSty);
            curCel.setCellStyle(fieldtextSty);

            curRow = secondrowList.get(++s);
            curCel = curRow.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(curRow.getRowNum(), curRow.getRowNum(), 0, 2));
            curCel.setCellValue(context.getMessage("message.keyworddensity", null, Locale.UK));
            curRow.setHeight((short) 800);
            curCel.setCellStyle(densitySty);

            for (int x = 0; x <= companalysisResultList.size(); x++) {
                CompetitorAnalysisResult densityresult = new CompetitorAnalysisResult();
                int col = 0;
                if (x == 0) {
                    densityresult = companalysisResult;
                    col = 1;
                } else if (x == 1) {
                    densityresult = companalysisResultList.get(0);
                    col = 2;
                } else if (x == 2) {
                    densityresult = companalysisResultList.get(1);
                    col = 3;
                }
                int d = 0;
                curRow = secondrowList.get(d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getUrl());
                curCel.setCellStyle(textalignSty);;

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getTitleTag());
                curCel.setCellStyle(dblCelSty);

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getDescriptionTag());
                curCel.setCellStyle(textalignSty);

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getKeywordsTag());
                curCel.setCellStyle(dblCelSty);

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getBodytext());
                curCel.setCellStyle(textalignSty);

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getAltTags());
                curCel.setCellStyle(dblCelSty);

                curRow = secondrowList.get(++d);
                curCel = curRow.createCell(col);
                curCel.setCellValue(densityresult.getLinkText());
                curCel.setCellStyle(textalignSty);
            }
            String repName = "LXRMarketplace_Competitor_Analysis_Report";
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".xls";
            String filePath = targetFilePath + filename;
            File file = new File(filePath);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (NoSuchMessageException | IOException e) {
            logger.error(e);
        }
        return filename;
    }

    public PdfPCell createPdfCell(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 11f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        return newCell;
    }

    public PdfPCell createPdfCellHeadings(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 11, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        PdfPCell newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderColorLeft(BaseColor.GRAY);
        newCell.setRowspan(2);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        newCell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        return newCell;
    }

    public PdfPCell createPdfCell1(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 11, com.itextpdf.text.Font.BOLD, new BaseColor(225, 110, 0)));
        PdfPCell newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        return newCell;
    }

    public String createPDFFile(HttpSession session, String targetFilePath) throws DocumentException {

        com.itextpdf.text.Font reportNameFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, com.itextpdf.text.Font.BOLD, new BaseColor(255, 110, 0)));
        com.itextpdf.text.Font blackFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));

        CompetitorAnalysisTool competitorAnalysisTool = (CompetitorAnalysisTool) session.getAttribute("competitorAnalysistool");
        CompetitorAnalysisResult companalysisResult = (CompetitorAnalysisResult) session.getAttribute("yoursite");
        List<CompetitorAnalysisResult> companalysisResultList = (List<CompetitorAnalysisResult>) session.getAttribute("compsite");
        String filename;
        try {
            String repName = "LXRMarketplace_Competitor_Analysis_Report";
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            String filePath = targetFilePath + filename;
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Competitor Analysis Report", reportNameFont);
            mainHeading.add(tempReportName);
            Chunk normalHeading = new Chunk("Review of", blackFont);
            Chunk domain = new Chunk(" " + "\"" + companalysisResult.getUrl() + "\" ", blackFont);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpName.setBorder(Rectangle.NO_BORDER);
            rpName.setPaddingLeft(120f);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            domName.setPaddingLeft(120f);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            domHeading.add(normalHeading);
            domHeading.add(domain);

            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Date: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            dateName.setPaddingLeft(120f);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(27f);
            rpName.setPaddingBottom(27f);
            rpName.setPaddingTop(6f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);

            float[] resultTabWidths = new float[companalysisResultList.size() + 2];
            if (companalysisResultList.size() > 1) {
                resultTabWidths[0] = 15f;
                resultTabWidths[1] = 28f;
                resultTabWidths[2] = 28f;
                resultTabWidths[3] = 28f;
            } else {
//                resultTabWidths = {15f, 40f, 40f};
                resultTabWidths[0] = 15f;
                resultTabWidths[1] = 40f;
                resultTabWidths[2] = 40f;
            }
            PdfPTable resultTab = new PdfPTable(resultTabWidths);
            resultTab.setWidthPercentage(99.9f);
            pdfDoc.add(Chunk.NEWLINE);

            PdfPCell competitiveHeader = createPdfCell1("Competitive Search Engine Friendly Metrics");
            competitiveHeader.setColspan(companalysisResultList.size() + 2);
            resultTab.addCell(competitiveHeader);
            PdfPCell pagetextHeader = createPdfCell1("Metrics");
            resultTab.addCell(pagetextHeader);
            PdfPCell yourSiteHeader = createPdfCell1("Your Website");
            resultTab.addCell(yourSiteHeader);
            PdfPCell competitorSiteHeader = createPdfCell1("Competitor Website");
            for (int i = 0; i < companalysisResultList.size(); i++) {
                resultTab.addCell(competitorSiteHeader);
            }
            PdfPCell domainNameHeader = createPdfCellHeadings("Domain Name");
            domainNameHeader.setRowspan(1);
            resultTab.addCell(domainNameHeader);
            CompetitorAnalysisResult result = null;
            PdfPCell yourSite = createPdfCell("" + companalysisResult.getUrl());
            resultTab.addCell(yourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell yourCompetitorSite = createPdfCell("" + competitorAnalysisResult.getUrl());
                resultTab.addCell(yourCompetitorSite);
            }
            PdfPCell pageSize = createPdfCellHeadings("Page Size");
            pageSize.setRowspan(2);
            resultTab.addCell(pageSize);
            PdfPCell pSYourSite = createPdfCell("" + companalysisResult.getPageSize() + "  " + "Kb");
            resultTab.addCell(pSYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell pSCompetitorSite = createPdfCell("" + competitorAnalysisResult.getPageSize() + "  " + "Kb");
                resultTab.addCell(pSCompetitorSite);
            }
            PdfPCell psTagLine = createPdfCell("" + context.getMessage("message.pagesize", null, Locale.UK));
            psTagLine.setColspan(companalysisResultList.size() + 1);
            psTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(psTagLine);
            PdfPCell wordCount = createPdfCellHeadings("Word Count");
            resultTab.addCell(wordCount);
            PdfPCell wCYourSite = createPdfCell("" + companalysisResult.getTotalWordCount());
            resultTab.addCell(wCYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell wcYourCompetitor = createPdfCell("" + competitorAnalysisResult.getTotalWordCount());
                resultTab.addCell(wcYourCompetitor);
            }
            PdfPCell wCTagLine = createPdfCell("" + context.getMessage("message.wordcount", null, Locale.UK));
            wCTagLine.setColspan(companalysisResultList.size() + 1);
            wCTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(wCTagLine);
            PdfPCell imageCount = createPdfCellHeadings("Image Count");
            resultTab.addCell(imageCount);
            PdfPCell iCYourSite = createPdfCell("" + companalysisResult.getImageCount());
            resultTab.addCell(iCYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell icYourCompetitor = createPdfCell("" + competitorAnalysisResult.getImageCount());
                resultTab.addCell(icYourCompetitor);
            }
            long alttext = companalysisResult.getContainAlttext();
            if (alttext > 0 && alttext == companalysisResult.getImageCount()) {

                PdfPCell iCTagLine = createPdfCell("" + context.getMessage("message.positive", null, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
                iCTagLine.setColspan(companalysisResultList.size() + 1);
                iCTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(iCTagLine);

            } else if (alttext > 0 && alttext != companalysisResult.getImageCount()) {
                Object[] imagecnt = new Long[1];
                imagecnt[0] = alttext;

                PdfPCell iCTagLine2 = createPdfCell("" + context.getMessage("message.negative", imagecnt, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
                iCTagLine2.setColspan(companalysisResultList.size() + 1);
                iCTagLine2.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(iCTagLine2);

            } else if (alttext == 0 && companalysisResult.getImageCount() != 0) {

                PdfPCell iCTagLine3 = createPdfCell("" + context.getMessage("message.positive", null, Locale.UK) + " " + context.getMessage("message.imagecount", null, Locale.UK));
                iCTagLine3.setColspan(companalysisResultList.size() + 1);
                iCTagLine3.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(iCTagLine3);

            } else {
                PdfPCell iCTagLine = createPdfCell(context.getMessage("message.imagecount", null, Locale.UK));
                iCTagLine.setColspan(companalysisResultList.size() + 1);
                iCTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(iCTagLine);
            }

            PdfPCell linkCount = createPdfCellHeadings("Link Count");
            resultTab.addCell(linkCount);
            PdfPCell lCYourSite = createPdfCell("" + companalysisResult.getLinkCount());
            resultTab.addCell(lCYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell lCYourCompetitor = createPdfCell("" + competitorAnalysisResult.getLinkCount());
                resultTab.addCell(lCYourCompetitor);
            }
            PdfPCell lCTagLine = createPdfCell("" + context.getMessage("message.linkcount", null, Locale.UK));
            lCTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            lCTagLine.setColspan(companalysisResultList.size() + 1);
            resultTab.addCell(lCTagLine);
            PdfPCell flashHeader = createPdfCellHeadings("Flash");
            resultTab.addCell(flashHeader);
            PdfPCell flashStatusyourStie = createPdfCell("" + companalysisResult.getFlash());
            resultTab.addCell(flashStatusyourStie);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell flashStatusYourCompetitor = createPdfCell("" + competitorAnalysisResult.getFlash());
                resultTab.addCell(flashStatusYourCompetitor);
            }
            String flashTagLine = companalysisResult.getFlash();
            if (flashTagLine.trim().equals("Flash is used")) {
                PdfPCell flashStatus = createPdfCell("" + context.getMessage("message.negativeflash", null, Locale.UK));
                flashStatus.setColspan(companalysisResultList.size() + 1);
                flashStatus.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(flashStatus);
            } else if ((flashTagLine.trim().equals("Flash is not used"))) {
                PdfPCell flashStatus = createPdfCell("" + context.getMessage("message.positiveflash", null, Locale.UK));
                flashStatus.setColspan(companalysisResultList.size() + 1);
                flashStatus.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(flashStatus);
            }
            PdfPCell framesHeader = createPdfCellHeadings("Frames");
            resultTab.addCell(framesHeader);
            PdfPCell framesForYourSite = createPdfCell("" + companalysisResult.getFramesCount());
            resultTab.addCell(framesForYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell framesForYourcompetitor = createPdfCell("" + competitorAnalysisResult.getFramesCount());
                resultTab.addCell(framesForYourcompetitor);
            }
            String frame = companalysisResult.getFramesCount();
            if (frame.trim().equals("Frames are used")) {
                PdfPCell framesTagLine = createPdfCell("" + context.getMessage("message.negativeframes", null, Locale.UK));
                framesTagLine.setColspan(companalysisResultList.size() + 1);
                framesTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(framesTagLine);

            } else if ((frame.trim().equals("Frames are not used"))) {
                PdfPCell framesTagLine = createPdfCell("" + context.getMessage("message.positiveframes", null, Locale.UK));
                framesTagLine.setColspan(companalysisResultList.size() + 1);
                framesTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(framesTagLine);

            }
            PdfPCell textHtemlRatioHeader = createPdfCellHeadings("Text / Html Ratio");
            resultTab.addCell(textHtemlRatioHeader);
            PdfPCell tHRForYourSite = createPdfCell("" + companalysisResult.getTextbyhtmlratio());
            resultTab.addCell(tHRForYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell tHRForYourcompetitor = createPdfCell("" + competitorAnalysisResult.getTextbyhtmlratio());
                resultTab.addCell(tHRForYourcompetitor);
            }
            PdfPCell tHRTagLine = createPdfCell("" + context.getMessage("message.texthtmlratio", null, Locale.UK) + "\n" + context.getMessage("message.texthtmlrecom1", null, Locale.UK) + "\n" + context.getMessage("message.texthtmlrecom2", null, Locale.UK) + "\n" + context.getMessage("message.texthtmlrecom3", null, Locale.UK));
            tHRTagLine.setColspan(companalysisResultList.size() + 1);
            tHRTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(tHRTagLine);

            float[] resultTabWidths1 = new float[companalysisResultList.size() + 2];
            if (companalysisResultList.size() > 1) {
                resultTabWidths1[0] = 15f;
                resultTabWidths1[1] = 28f;
                resultTabWidths1[2] = 28f;
                resultTabWidths1[3] = 28f;
            } else {
                resultTabWidths1[0] = 15f;
                resultTabWidths1[1] = 40f;
                resultTabWidths1[2] = 40f;
            }
            PdfPTable headingTagTable = new PdfPTable(resultTabWidths1);
            headingTagTable.setWidthPercentage(99.9f);
            PdfPCell headingTagHeader = createPdfCellHeadings("Headings(H1,H2...H6)");
            headingTagHeader.setBorderColorTop(BaseColor.GRAY);
            headingTagHeader.setBorderWidthRight(0.1f);
            headingTagHeader.setBorderWidthBottom(0.1f);
            headingTagHeader.setBorderWidthLeft(0.1f);
            headingTagHeader.setBorderWidthTop(0.1f);
            headingTagHeader.setColspan(companalysisResultList.size() + 2);
            headingTagHeader.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            headingTagTable.addCell(headingTagHeader);

            for (int i = 1; i <= companalysisResult.getTotalHeadingsList().size(); i++) {
                for (int k = 0; k <= companalysisResultList.size(); k++) {
                    if (k == 0) {
                        result = companalysisResult;
                    } else if (k == 1) {
                        result = companalysisResultList.get(0);
                    } else if (k == 2) {
                        result = companalysisResultList.get(1);
                    }

                    if (k == 0 && result != null) {
                        PdfPCell headings = createPdfCell("H" + i);
                        headings.setBorderWidthRight(0.1f);
                        headings.setBorderWidthBottom(0.0f);
                        headings.setBorderWidthLeft(0.1f);
                        headings.setBorderWidthTop(0f);
                        headingTagTable.addCell(headings);
                        PdfPCell compheadingvalues = createPdfCell("" + result.getTotalHeadingsList().get(i).size());
                        compheadingvalues.setBorderWidthRight(0.1f);
                        compheadingvalues.setBorderWidthBottom(0.0f);
                        compheadingvalues.setBorderWidthLeft(0f);
                        compheadingvalues.setBorderWidthTop(0f);
                        headingTagTable.addCell(compheadingvalues);

                    } else if (result != null) {
                        PdfPCell compheadingvalues = createPdfCell("" + result.getTotalHeadingsList().get(i).size());
                        compheadingvalues.setBorderWidthRight(0.1f);
                        compheadingvalues.setBorderWidthBottom(0f);
                        compheadingvalues.setBorderWidthLeft(0f);
                        compheadingvalues.setBorderWidthTop(0f);
                        headingTagTable.addCell(compheadingvalues);
                    }
                }
            }

            for (int i = 1; i <= companalysisResult.getTotalHeadingsList().size(); i++) {

                PdfPCell empty = createPdfCell("");
                empty.setBorder(Rectangle.NO_BORDER);
                empty.setBorderWidthRight(0.0f);
                empty.setBorderWidthBottom(0.0f);
                empty.setBorderWidthLeft(0.1f);
                empty.setBorderWidthTop(0f);
                headingTagTable.addCell(empty);
                boolean isYourDomain = false;
                for (int k = 0; k <= companalysisResultList.size(); k++) {
                    if (k == 0) {
                        result = companalysisResult;
                        isYourDomain = true;
                    } else if (k == 1) {
                        result = companalysisResultList.get(0);
                        isYourDomain = false;
                    } else if (k == 2) {
                        result = companalysisResultList.get(1);
                        isYourDomain = false;
                    }
                    String headingText = "";
                    if (result != null) {
                        for (int j = 0; j < result.getTotalHeadingsList().get(i).size(); j++) {
                            if (headingText.equals("")) {
                                headingText = "->[h" + i + "]" + result.getTotalHeadingsList().get(i).get(j);
                            } else {
                                headingText = headingText + "\n" + " ->[h" + i + "]" + result.getTotalHeadingsList().get(i).get(j);
                            }
                        }
                    }

                    PdfPCell yourHeadingLine1 = createPdfCell(headingText);
                    if (isYourDomain) {
                        yourHeadingLine1.setBorderWidthRight(0.1f);
                        yourHeadingLine1.setBorderWidthBottom(0.0f);
                        yourHeadingLine1.setBorderWidthLeft(0.1f);
                        yourHeadingLine1.setBorderWidthTop(0f);
                    } else {
                        yourHeadingLine1.setBorderWidthRight(0.1f);
                        yourHeadingLine1.setBorderWidthBottom(0.0f);
                        yourHeadingLine1.setBorderWidthLeft(0.0f);
                        yourHeadingLine1.setBorderWidthTop(0f);
                    }
                    headingTagTable.addCell(yourHeadingLine1);
                }
            }
            PdfPCell headingEmptyCell = createPdfCell("");
            headingEmptyCell.setBorderWidthRight(0f);
            headingEmptyCell.setBorderWidthBottom(0f);
            headingEmptyCell.setBorderWidthLeft(0.1f);
            headingEmptyCell.setBorderWidthTop(0f);
            headingTagTable.addCell(headingEmptyCell);

            PdfPCell headingTageLine = createPdfCell(context.getMessage("message.headings", null, Locale.UK));
            headingTageLine.setColspan(companalysisResultList.size() + 1);
            headingTageLine.setBorderWidthRight(0.1f);
            headingTageLine.setBorderWidthBottom(0.0f);
            headingTageLine.setBorderWidthLeft(0.1f);
            headingTageLine.setBorderWidthTop(0.1f);
            headingTageLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            headingTagTable.addCell(headingTageLine);

            PdfPCell jointable = new PdfPCell(headingTagTable);
            jointable.setBorder(Rectangle.BOTTOM);
            jointable.setBorderWidthRight(0.1f);
            jointable.setBorderWidthBottom(0.1f);
            jointable.setBorderWidthLeft(0.1f);
            jointable.setBorderWidthTop(0.1f);
            jointable.setColspan(companalysisResultList.size() + 2);
//            resultTab.addCell(jointable);

            PdfPCell FaviconHeader = createPdfCellHeadings("Favicon");
            resultTab.addCell(FaviconHeader);

            PdfPCell FaviconYourSite = createPdfCell("" + companalysisResult.getFavicon());
            resultTab.addCell(FaviconYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell FaviconYourCompetitor = createPdfCell("" + competitorAnalysisResult.getFavicon());
                resultTab.addCell(FaviconYourCompetitor);
            }
            PdfPCell FaviconTageLine = createPdfCell("" + context.getMessage("message.favicon", null, Locale.UK));
            FaviconTageLine.setColspan(companalysisResultList.size() + 1);
            FaviconTageLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(FaviconTageLine);
            PdfPCell languageHeader = createPdfCellHeadings("Language");
            resultTab.addCell(languageHeader);
            PdfPCell languageYourSite = createPdfCell("" + companalysisResult.getLanguage());
            resultTab.addCell(languageYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell languageYourCompetitor = createPdfCell("" + competitorAnalysisResult.getLanguage());
                resultTab.addCell(languageYourCompetitor);
            }
            String language = companalysisResult.getLanguage();
            if (language.trim().equals("Language missing")) {
                PdfPCell langTageLine = createPdfCell(context.getMessage("message.negativelanguage", null, Locale.UK));
                langTageLine.setColspan(companalysisResultList.size() + 1);
                langTageLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(langTageLine);
            } else if (!(language.trim().equals("Language missing"))) {
                PdfPCell langTageLine = createPdfCell("" + context.getMessage("message.positivelanguage", null, Locale.UK));
                langTageLine.setColspan(companalysisResultList.size() + 1);
                langTageLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                resultTab.addCell(langTageLine);
            }
            PdfPCell loadTimeHeader = createPdfCellHeadings("LoadTime");
            resultTab.addCell(loadTimeHeader);
            PdfPCell loadTimeYourSite = createPdfCell("" + companalysisResult.getLoadtime());
            resultTab.addCell(loadTimeYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell loadTimeYourCompetitor = createPdfCell("" + competitorAnalysisResult.getLoadtime());
                resultTab.addCell(loadTimeYourCompetitor);
            }
            PdfPCell lTTagLine = createPdfCell("" + context.getMessage("message.loadtime", null, Locale.UK));
            lTTagLine.setColspan(companalysisResultList.size() + 1);
            lTTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(lTTagLine);
            PdfPCell resloveHeader = createPdfCellHeadings("WWW Resolve");
            resultTab.addCell(resloveHeader);
            PdfPCell resloveYourStie = createPdfCell("" + companalysisResult.getWwwresolve());
            resultTab.addCell(resloveYourStie);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell resloveYourComp = createPdfCell("" + competitorAnalysisResult.getWwwresolve());
                resultTab.addCell(resloveYourComp);
            }
            PdfPCell wRTagLine = createPdfCell("" + context.getMessage("message.wwwresolve", null, Locale.UK));
            wRTagLine.setColspan(companalysisResultList.size() + 1);
            wRTagLine.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(wRTagLine);
            PdfPCell wRTagLine1 = createPdfCell("");
            wRTagLine1.setColspan(companalysisResultList.size() + 2);
            wRTagLine1.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            resultTab.addCell(wRTagLine1);

            resultTab.addCell(jointable);
            pdfDoc.add(resultTab);

            float[] resultTabWidths2 = new float[companalysisResultList.size() + 2];
            if (companalysisResultList.size() > 1) {
                resultTabWidths2[0] = 15f;
                resultTabWidths2[1] = 28f;
                resultTabWidths2[2] = 28f;
                resultTabWidths2[3] = 28f;
            } else {
//                resultTabWidths = {15f, 40f, 40f};
                resultTabWidths2[0] = 15f;
                resultTabWidths2[1] = 40f;
                resultTabWidths2[2] = 40f;
            }
            PdfPTable googleResult = new PdfPTable(resultTabWidths2);
            googleResult.setWidthPercentage(99.9f);
            pdfDoc.add(Chunk.NEWLINE);
            PdfPCell competitiveKDHeader = createPdfCell1("Competitive Keyword Density for " + "\"" + competitorAnalysisTool.getSearchPhrase() + "\"");
            competitiveKDHeader.setColspan(companalysisResultList.size() + 2);
            googleResult.addCell(competitiveKDHeader);
            PdfPCell keywordDensity = createPdfCell1("Keyword Density in");
            keywordDensity.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(keywordDensity);
            googleResult.addCell(yourSiteHeader);
            for (int i = 0; i < companalysisResultList.size(); i++) {
                googleResult.addCell(competitorSiteHeader);
            }
            PdfPCell domainNameHead = createPdfCellHeadings("Domain Name");
            domainNameHead.setRowspan(1);
            domainNameHead.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(domainNameHead);
            googleResult.addCell(yourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell yourCompetitorSite = createPdfCell("" + competitorAnalysisResult.getUrl());
                googleResult.addCell(yourCompetitorSite);
            }
            PdfPCell littleTag = createPdfCellHeadings("Title Tag");
            littleTag.setRowspan(1);
            littleTag.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(littleTag);
            PdfPCell littleTagYourSite = createPdfCell("" + companalysisResult.getTitleTag());
            googleResult.addCell(littleTagYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell littleTagYourComp = createPdfCell("" + competitorAnalysisResult.getTitleTag());
                googleResult.addCell(littleTagYourComp);
            }
            PdfPCell descriptionTag = createPdfCellHeadings("Description Tag");
            descriptionTag.setRowspan(1);
            descriptionTag.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(descriptionTag);
            PdfPCell descripTagYourSite = createPdfCell("" + companalysisResult.getDescriptionTag());
            googleResult.addCell(descripTagYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell ldescripTagYourComp = createPdfCell("" + competitorAnalysisResult.getDescriptionTag());
                googleResult.addCell(ldescripTagYourComp);
            }
            PdfPCell keywordsTag = createPdfCellHeadings("Keywords Tag");
            keywordsTag.setRowspan(1);
            keywordsTag.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(keywordsTag);
            PdfPCell KeywordsTagYourSite = createPdfCell("" + companalysisResult.getKeywordsTag());
            googleResult.addCell(KeywordsTagYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell keywordsTagYourComp = createPdfCell("" + competitorAnalysisResult.getKeywordsTag());
                googleResult.addCell(keywordsTagYourComp);
            }
            PdfPCell bodyText = createPdfCellHeadings("Body Text");
            bodyText.setRowspan(1);
            bodyText.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(bodyText);
            PdfPCell bodyTextYourSite = createPdfCell("" + companalysisResult.getBodytext());
            googleResult.addCell(bodyTextYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell bodyTextYourComp = createPdfCell("" + competitorAnalysisResult.getBodytext());
                googleResult.addCell(bodyTextYourComp);
            }
            PdfPCell altText = createPdfCellHeadings("Alt Text");
            altText.setRowspan(1);
            altText.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(altText);
            PdfPCell altTextYourSite = createPdfCell("" + companalysisResult.getAltTags());
            googleResult.addCell(altTextYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell altTextYourComp = createPdfCell("" + competitorAnalysisResult.getAltTags());
                googleResult.addCell(altTextYourComp);
            }
            PdfPCell linkText = createPdfCellHeadings("Link Text");
            altText.setRowspan(1);
            linkText.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            googleResult.addCell(linkText);
            PdfPCell linkTextYourSite = createPdfCell("" + companalysisResult.getLinkText());
            googleResult.addCell(linkTextYourSite);
            for (CompetitorAnalysisResult competitorAnalysisResult : companalysisResultList) {
                PdfPCell linkTextYourComp = createPdfCell("" + competitorAnalysisResult.getLinkText());
                googleResult.addCell(linkTextYourComp);
            }
            PdfPCell tagLineKeydensity = createPdfCell("" + context.getMessage("message.keyworddensity", null, Locale.UK));
            tagLineKeydensity.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            tagLineKeydensity.setColspan(companalysisResultList.size() + 2);
            googleResult.addCell(tagLineKeydensity);

            pdfDoc.newPage();
            pdfDoc.add(googleResult);
            pdfDoc.close();
            return filename;

        } catch (DocumentException | IOException | NoSuchMessageException e) {
            logger.error("Exception in download Reports request ", e);
        }
        return null;
    }

    public Map<String, Object> getCompetitorData(CompetitorAnalysisTool competitorAnalysisTool) {
        String requestedURL = context.getMessage("lxrm.apps.rest.hostingURL", null, Locale.UK).concat(context.getMessage("competitorAnalysisServiceUrl", null, Locale.UK));
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("yourUrl", competitorAnalysisTool.getYourUrl());
        body.add("searchPhrase", competitorAnalysisTool.getSearchPhrase());
        body.add("domainUrlList[0]", competitorAnalysisTool.getDomainUrlList().get(0));
        if (competitorAnalysisTool.getDomainUrlList().get(1) != null && !competitorAnalysisTool.getDomainUrlList().get(1).equals("")) {
            body.add("domainUrlList[1]", competitorAnalysisTool.getDomainUrlList().get(1));
        }

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

//        headers.add("LXRM_SEO_BACKLINKS", ServiceAuthEncryption.crypt(domainName));
//        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<Map<String, Object>> response = restTemplate.exchange(requestedURL, HttpMethod.GET, entity, new ParameterizedTypeReference<Map<String, Object>>() {});
        ResponseEntity<Map<String, Object>> response = restTemplate.exchange(requestedURL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Map<String, Object>>() {
        });
        Map<String, Object> competitorData = null;
        if (response.getBody() != null && !response.getBody().equals("")) {
            competitorData = response.getBody();
        }
        return competitorData;
    }

}