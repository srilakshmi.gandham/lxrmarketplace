package lxr.marketplace.competitoranalysis;

import java.util.ArrayList;
import java.util.List;

public class CompetitorAnalysisTool {

    private String yourUrl;
    private List<String> domainUrlList = new ArrayList<>();
    private String competitorUrl;
    private String searchPhrase;

    public String getYourUrl() {
        if (yourUrl != null) {
            yourUrl = yourUrl.trim();
        }
        return yourUrl;
    }

    public void setYourUrl(String yourUrl) {
        this.yourUrl = yourUrl;
    }

    public List<String> getDomainUrlList() {
        return domainUrlList;
    }

    public void setDomainUrlList(List<String> domainUrlList) {
        this.domainUrlList = domainUrlList;
    }

    public String getCompetitorUrl() {
        return competitorUrl;
    }

    public void setCompetitorUrl(String competitorUrl) {
        this.competitorUrl = competitorUrl;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

}
