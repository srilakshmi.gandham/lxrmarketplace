package lxr.marketplace.competitoranalysis;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
//import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
//import lxr.marketplace.util.ToolService;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class CompetitorAnalysisToolController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(CompetitorAnalysisToolController.class);

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    String downloadFolder;
    int yoursitesc = 0;
    int compsitesc = 0;

    JSONObject userInputJson = null;

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public CompetitorAnalysisToolController() {
        setCommandClass(CompetitorAnalysisTool.class);
        setCommandName("competitorAnalysisTool");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        CompetitorAnalysisTool competitoranalysis = (CompetitorAnalysisTool) command;
        CompetitorAnalysisService compservice = new CompetitorAnalysisService();
        if (competitoranalysis != null) {
            if (competitoranalysis.getSearchPhrase() != null && !competitoranalysis.getSearchPhrase().trim().isEmpty()) {
                competitoranalysis.setSearchPhrase(competitoranalysis.getSearchPhrase().trim());
            }
            if (competitoranalysis.getYourUrl()!= null && !competitoranalysis.getYourUrl().trim().isEmpty()) {
                competitoranalysis.setYourUrl(competitoranalysis.getYourUrl().trim());
            }
            competitoranalysis.setDomainUrlList(competitoranalysis.getDomainUrlList().stream().map(String :: trim).collect(Collectors.toList()));
            session.setAttribute("competitorAnalysistool", competitoranalysis);
        }
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(7);
            toolObj.setToolName("SEO Competitor Analysis Tool");
            toolObj.setToolLink("seo-competitor-analysis-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.COMPETITOR_ANALYSIS);
            session.removeAttribute("notWorkingUrl");
            if (WebUtils.hasSubmitParameter(request, "getResult") && (competitoranalysis != null && competitoranalysis.getYourUrl() != null)) {
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "competitorAnalysisTool", competitoranalysis);
                    String notWorkingUrl = null;
                    notWorkingUrl = (String) session.getAttribute("notWorkingUrl");
                    if (notWorkingUrl == null) {
                        mAndV.addObject("status", "success");
                    }
                    return mAndV;
                }
                session.removeAttribute("notWorkingUrl");
                session.removeAttribute("yoursite");
                session.removeAttribute("compsite");
                ModelAndView mAndV = new ModelAndView(getFormView(), "competitorAnalysisTool", competitoranalysis);
                Map<String, Object> competitorsDataMap = compservice.getCompetitorData(competitoranalysis);
                if ((int) competitorsDataMap.get("success") == 0) {
                    mAndV.addObject("status", "NoResult");
                    session.setAttribute("notWorkingUrl", (String) competitorsDataMap.get("message"));
                    return mAndV;
                } else {
                    ObjectMapper objectMapper = new ObjectMapper();
                    CompetitorAnalysisResult competitorAnalysisResult = objectMapper.readValue(objectMapper.writeValueAsString(competitorsDataMap.get("userSiteAnalysis")), new TypeReference<CompetitorAnalysisResult>() {
                    });
                    List<CompetitorAnalysisResult> competitorAnalysisResultList = objectMapper.readValue(objectMapper.writeValueAsString(competitorsDataMap.get("competitrSiteAnalysis")), new TypeReference<List<CompetitorAnalysisResult>>() {
                    });
                    session.setAttribute("yoursite", competitorAnalysisResult);
                    session.setAttribute("compsite", competitorAnalysisResultList);
                    Session userSession = (Session) session.getAttribute("userSession");
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    userSession.addToolUsage(7);
                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }

                    // if the request is coming from tool page then only add the user inputs to JSONObject
                    //if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                        session.removeAttribute("requestFromMail");
                    }
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson = new JSONObject();
                        //By using this we can get tool URL and query parameters
                        userInputJson.put("2", competitoranalysis.getDomainUrlList());
                        userInputJson.put("3", competitoranalysis.getSearchPhrase());
                        userInputJson.put("4", true);
                    }
//                    if (competitorsDataMap.size() > 0) {
//                        mAndV.addObject("connectionerror", " Unable to connect.");
//                        return mAndV;
//                    } else {
//                        mAndV.addObject("status", "success");
//                        return mAndV;
//                    }
                    mAndV.addObject("status", "success");
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                try {
                    String downFileName;
                    String downParam = request.getParameter("report-type");
                    if (downParam.equalsIgnoreCase("xls")) {
                        downFileName = compservice.createExcelFile(session, downloadFolder);
                        Common.downloadReport(response, downloadFolder, downFileName, downParam);
                    } else if (downParam.equalsIgnoreCase("pdf")) {
                        downFileName = compservice.createPDFFile(session, downloadFolder);
                        Common.downloadReport(response, downloadFolder, downFileName, downParam);
                    } else if (downParam.equalsIgnoreCase("'sendmail'")) {
                        String toAddrs = request.getParameter("email");
                        downFileName = compservice.createPDFFile(session, downloadFolder);
                        Common.sendReportThroughMail(request, downloadFolder, downFileName, toAddrs, Common.COMPETITOR_ANALYSIS);
                        return null;
                    }
                } catch (DocumentException e) {
                    logger.error("Exception in while downloading file", e);
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "competitorAnalysisTool", competitoranalysis);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                Session userSession = (Session) session.getAttribute("userSession");
                String domain = request.getParameter("domain").trim();
                String toolIssues = "";
                String questionInfo = "";
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
                questionInfo += "- " + "Auditing competitor webpages for relevent keywords." + "\n";
                toolIssues += "Do you want a full audit of how your competitors are targeting your keywords?";
                lxrmAskExpert.setDomain(domain.trim());
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(Common.COMPETITOR_ANALYSIS);
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);
                //if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), domain, toolIssues, questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }
                return null;
            }
            ModelAndView mAndV = new ModelAndView(getFormView(), "competitorAnalysisTool", competitoranalysis);
            competitoranalysis = new CompetitorAnalysisTool();
            List<String> competitorList = new ArrayList();
            competitorList.add("");
            competitorList.add("");
            competitoranalysis.setDomainUrlList(competitorList);
            mAndV.addObject(competitoranalysis);
            mAndV.addObject("status", "NoResult");
            return mAndV;
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "competitorAnalysisTool", competitoranalysis);
        mAndV.addObject("status", "NoResult");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        CompetitorAnalysisTool competitorAnalysisTool = (CompetitorAnalysisTool) session.getAttribute("competitorAnalysistool");
        if (competitorAnalysisTool == null) {
            List<String> competitorList = new ArrayList();
            competitorList.add("");
            competitorList.add("");
            competitorAnalysisTool = new CompetitorAnalysisTool();
            competitorAnalysisTool.setDomainUrlList(competitorList);
//            competitorAnalysisTool.setRedirectUrlCheckbox(true);
        }
        return competitorAnalysisTool;
    }

}
