package lxr.marketplace.competitoranalysis;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class YourSiteThread implements Callable<CompetitorAnalysisResult> {

    private static Logger logger = Logger.getLogger(YourSiteThread.class);
//    CompetitorAnalysisTool cpa;
//    CompetitorAnalysisResult cpresult;
    String url = null;
    String searchqy;
    boolean jsoupconnerror = false;

    public boolean isJsoupconnerror() {
        return jsoupconnerror;
    }

    public void setJsoupconnerror(boolean jsoupconnerror) {
        this.jsoupconnerror = jsoupconnerror;
    }

    String bodycontent = null;
    String titlecontent = null;
    Elements metalinks;
    String description;
    String keywords = null;
    String metatagname = null;
    Elements imgtags;
    Elements anchors;
    private static final int TIME_OUT = 30000;
    private static final int SLEEP_OUT = 10000;
    private CountDownLatch latchObj;

    public YourSiteThread(String url, String searchqy, CountDownLatch latchObj) {
        this.url = url;
        this.searchqy = searchqy;
        this.latchObj = latchObj;
//        cpresult = new CompetitorAnalysisResult();
    }
    
    @Override
    public CompetitorAnalysisResult call() throws Exception {
        CompetitorAnalysisResult competitorAnalysisResult = new CompetitorAnalysisResult();
        Document ourdoc = null;
        String altcontent = " ";
        String linkcontnet = " ";
        long timeoutIteration = 0;
        long noOfTime = 3;
        Connection jsoupCon = null;
        double startTime = 0.0;
        double endTime = 0.0;
        do {
            timeoutIteration += 1;
            try {
                startTime = Calendar.getInstance().getTimeInMillis();
                jsoupCon = Jsoup.connect(url);
                jsoupCon.timeout(TIME_OUT);
                jsoupCon.userAgent(Common.USER_AGENT);
                ourdoc = jsoupCon.get();
                break;
            } catch (IOException ex) {
                logger.error("Exception on jsoup connection:", ex);
                jsoupconnerror = true;
                try {
                    Thread.sleep(SLEEP_OUT * timeoutIteration);
                } catch (InterruptedException e) {
                    logger.error("Exception on sleep thread:", e);
                }
            }
        } while (timeoutIteration < noOfTime);
        if (ourdoc != null) {
            endTime = Calendar.getInstance().getTimeInMillis();
            double difference;
            if (endTime > startTime) {
                difference = endTime - startTime;
            } else {
                difference = startTime - endTime;
            }
            double diff = difference / 1000;
            double loadtime = Double.parseDouble(new DecimalFormat("#.##").format(diff));
            String loadTime = loadtime + "Seconds";
            competitorAnalysisResult.setLoadtime(loadTime);
            bodycontent = ourdoc.getElementsByTag("body").text();
            imgtags = ourdoc.select("img");
            anchors = ourdoc.select("a");
            if (anchors.size() > 0) {
                linkcontnet = anchors.stream().map((anchor) -> " " + anchor.text()).reduce(linkcontnet, String::concat);
            }
            if (imgtags.size() > 0) {
                altcontent = imgtags.stream().map((imgTag) -> " " + imgTag.attr("alt")).reduce(altcontent, String::concat);
            }
            metalinks = ourdoc.select("meta");
            if (metalinks.size() > 0) {
                metalinks.stream().map((metalink) -> {
                    metatagname = metalink.attr("name");
                    return metalink;
                }).map((metalink) -> {
                    if (metatagname.equalsIgnoreCase("description")) {
                        description = metalink.attr("content");
                    }
                    return metalink;
                }).filter((metalink) -> (metatagname.equalsIgnoreCase("keywords"))).forEach((metalink) -> {
                    keywords = metalink.attr("content");
                });
            }
            titlecontent = ourdoc.select("title").text();
            CompetitorAnalysisService compservice = new CompetitorAnalysisService();
            competitorAnalysisResult.setUrl(url);
            competitorAnalysisResult.setTotalWordCount(compservice.getTotalWordCount(url, ourdoc, bodycontent));
            competitorAnalysisResult.setPageSize(compservice.getPageSize(ourdoc));
            competitorAnalysisResult.setImageCount(compservice.imageCount(url, ourdoc, imgtags));
            competitorAnalysisResult.setLinkCount(compservice.linkCount(url, ourdoc, anchors));
            competitorAnalysisResult.setFramesCount(compservice.isFrame(ourdoc));
            competitorAnalysisResult.setFlash(compservice.isFlash(ourdoc));
            competitorAnalysisResult.setBodytext(compservice.keywordDensityInpercentage(url, ourdoc, searchqy, bodycontent));
            competitorAnalysisResult.setTitleTag(compservice.KeywordinTitle(url, ourdoc, searchqy, titlecontent));
            competitorAnalysisResult.setDescriptionTag(compservice.keywordDensityInpercentage(url, ourdoc, searchqy, description));
            competitorAnalysisResult.setKeywordsTag(compservice.keywordDensityInpercentage(url, ourdoc, searchqy, keywords));
            competitorAnalysisResult.setAltTags(compservice.keywordDensityInpercentage(url, ourdoc, searchqy, altcontent));
            competitorAnalysisResult.setLinkText(compservice.keywordDensityInpercentage(url, ourdoc, searchqy, linkcontnet));
            competitorAnalysisResult.setTextbyhtmlratio(Common.texthtmlratio(ourdoc));
            competitorAnalysisResult.setLanguage(compservice.getLanguage(ourdoc));
            competitorAnalysisResult.setTotalHeadingsList(compservice.genearteHeadingTagsList(ourdoc));
            competitorAnalysisResult.setFavicon(compservice.findFavIcon(url, ourdoc));
            competitorAnalysisResult.setWwwresolve(compservice.checkingwwwResolve(url));
            competitorAnalysisResult.setContainAlttext((compservice.containAltText(url, ourdoc, imgtags)));
        }
        latchObj.countDown();
        return competitorAnalysisResult;
    }
}
