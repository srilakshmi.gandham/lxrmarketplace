/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.user;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author vidyasagar.korada
 */
public class UserSocialProfile implements Serializable{
    
    public static String GMAIL="Google";
    public static String FACEBOOK="Facebook";
    
    private long userId;
    private String profileId;
    private String socialChannel;
    private String oauthAccessToken;
    private LocalDateTime profielCapturedTime;

    public static String getGMAIL() {
        return GMAIL;
    }

    public static void setGMAIL(String GMAIL) {
        UserSocialProfile.GMAIL = GMAIL;
    }

    public static String getFACEBOOK() {
        return FACEBOOK;
    }

    public static void setFACEBOOK(String FACEBOOK) {
        UserSocialProfile.FACEBOOK = FACEBOOK;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getSocialChannel() {
        return socialChannel;
    }

    public void setSocialChannel(String socialChannel) {
        this.socialChannel = socialChannel;
    }

    public String getOauthAccessToken() {
        return oauthAccessToken;
    }

    public void setOauthAccessToken(String oauthAccessToken) {
        this.oauthAccessToken = oauthAccessToken;
    }

    public LocalDateTime getProfielCapturedTime() {
        return profielCapturedTime;
    }

    public void setProfielCapturedTime(LocalDateTime profielCapturedTime) {
        this.profielCapturedTime = profielCapturedTime;
    }

    @Override
    public String toString() {
        return "UserSocialProfile{" + "userId=" + userId + ", profileId=" + profileId + ", socialChannel=" + socialChannel + ", oauthAccessToken=" + oauthAccessToken + ", profielCapturedTime=" + profielCapturedTime + '}';
    }

    
    
    
}
