/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.user;

import java.time.LocalDateTime;

/**
 *
 * @author vidyasagar.korada
 */
public class EmailValidationModel {
    private String emailUserName;
    private String emailToBeVerified;
    private double emailVerificationScore;
    private String verificationCode;
    private LocalDateTime createdDate;
    private boolean emailVerified;
    /*
    Returns true or false depending  on whether or not the requested 
    email address is a disposable email address. (e.g. "user123@mailinator.com")
    */
    private boolean disposable;

    public String getEmailToBeVerified() {
        return emailToBeVerified;
    }

    public void setEmailToBeVerified(String emailToBeVerified) {
        this.emailToBeVerified = emailToBeVerified;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public double getEmailVerificationScore() {
        return emailVerificationScore;
    }

    public void setEmailVerificationScore(double emailVerificationScore) {
        this.emailVerificationScore = emailVerificationScore;
    }

    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    public boolean isDisposable() {
        return disposable;
    }

    public void setDisposable(boolean disposable) {
        this.disposable = disposable;
    }
    
    

    @Override
    public String toString() {
        return "EmailValidationModel{" + "emailToBeVerified=" + emailToBeVerified + ", emailVerificationScore=" + emailVerificationScore + ", verificationCode=" + verificationCode + ", createdDate=" + createdDate + ", emailVerified=" + emailVerified + '}';
    }
    


    
}
