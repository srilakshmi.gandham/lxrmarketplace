package lxr.marketplace.user;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.admin.expertuser.ExpertUserService;
import lxr.marketplace.feedback.FeedbackService;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.SessionService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class LoginController extends SimpleFormController {

    private final static Logger LOGGER = Logger.getLogger(LoginController.class);

    private LoginService loginService;
    private SessionService sessionService;
    private CookieService cookieService;
    private FeedbackService feedbackService;
    @Autowired
    private EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    @Autowired
    private ExpertUserService expertUserService;
    
    @Autowired
    private MessageSource messageSource;

    public void setFeedbackService(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    public void setCookieService(CookieService cookieService) {
        this.cookieService = cookieService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public EncryptAndDecryptUsingDES getEncryptAndDecryptUsingDES() {
        return encryptAndDecryptUsingDES;
    }

    public void setEncryptAndDecryptUsingDES(EncryptAndDecryptUsingDES encryptAndDecryptUsingDES) {
        this.encryptAndDecryptUsingDES = encryptAndDecryptUsingDES;
    }

    public LoginController() {
        setCommandClass(Login.class);
        setCommandName("login");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Login login = (Login) command;
        Login user = null;
        if (session.getAttribute("user") != null) {
            user = (Login) session.getAttribute("user");
        }
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (WebUtils.hasSubmitParameter(request, "guestuseremail")) {
            String email = request.getParameter("email");
            session.setAttribute("email", email);
            Login prev_user = (Login) session.getAttribute("user");
            Session userSession = (Session) session.getAttribute("userSession");
            long sesid = 0;
            long userid = 0;
            if (userSession != null) {
                sesid = userSession.getId();
            }
            if (prev_user != null) {
                userid = prev_user.getId();
            }
            loginService.insertGuestUserEmailId(userid, sesid, email);
        }

        if (WebUtils.hasSubmitParameter(request, "popuplogin")) {
            if (!userLoggedIn) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String encryptedPassword = encryptAndDecryptUsingDES.encrypt(request.getParameter("password"));
                Login prev_user = (Login) session.getAttribute("user");
                List<Login> loginDetails = loginService.find(username, password, encryptedPassword);
                user = new Login();
                if (loginDetails != null && loginDetails.size() > 0) {
                    user = loginDetails.get(0);
                    user.setUserName(username.toLowerCase());
                    user.setPassword(password);
                }

                session.removeAttribute("sendMailId");

                if (user.getPassword().equals(password) && !user.isDeleted()) {

                    session.removeAttribute("userLoggedIn");
                    session.removeAttribute("getUserMostUsedTools");

                    session.setAttribute("sendMailId", user.getUserName().toLowerCase());
                    session.setAttribute("user", user);
                    session.setAttribute("userLoggedIn", true);
                    session.setAttribute("getUserMostUsedTools", true);
                    if (user.getPrivacyPolicyAgreedTime() == null) {
                        loginService.updateActDate(user.getId(), new Timestamp(Calendar.getInstance().getTimeInMillis()), 2);
                    }
                    if (!user.isEncrypted()) {
                        loginService.updateEncryptedPassword(encryptedPassword, user.getId());
                    }
                    if (user.isAdmin()) {
                        guestUserMapping(prev_user, request, response);
                        ExpertUser expertUser = expertUserService.findExpertUser(user.getUserName());
                        if (expertUser != null) {
                            session.setAttribute("expertUser", expertUser);
                        }
                        loginService.addInJsonforLogin("success", user, response);
                        return null;
                    } else {
                        Session userSession = (Session) session.getAttribute("userSession");
                        session.setAttribute("guestUserSession", userSession);
                        guestUserMapping(prev_user, request, response);
                        if (prev_user.getId() < 0) {
                            sessionService.insert(userSession);
                        }
                        session.setAttribute("userSession", userSession);
                        loginService.addInJsonforLogin("success", user, response);
                        return null;
                    }
                } else if (user.isDeleted()) {
//                    loginService.addInJsonforLogin("Your email account is not valid. To restore your account, please contact support at support@lxrmarketplace.com", user, response);
                    loginService.addInJsonforLogin(messageSource.getMessage("lxrm.deleted.user.login.error", null, Locale.US), user, response);
                    return null;
                } else {
                    loginService.addInJsonforLogin("The email or password you entered is incorrect.", user, response);
                    return null;
                }
            } else {
                if (user != null) {
                    loginService.addInJsonforLogin("success", user, response);
                }
                return null;
            }

        } else if (WebUtils.hasSubmitParameter(request, "signout")) {
            Session userSession = (Session) session.getAttribute("userSession");
            Cookie cookie = cookieService.getIsLoggedInCookie(response, request);
            if (cookie != null) {
                cookieService.deleteIsLoggedInCookie(request, response);
            }
            if (userSession != null) {
                Calendar endTime = Calendar.getInstance();
                userSession.setEndTime(endTime);
            }
            session.invalidate();
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            loginService.addInJsonforLogin(url, null, response);

        } else {
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            loginService.addInJsonforLogin(url, null, response);
        }
        return null;
    }

    public void guestUserMapping(Login prev_user, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        HttpSession session = request.getSession(true);
        Session userSession = (Session) session.getAttribute("userSession");
        Login user = (Login) session.getAttribute("user");

        if (user.getId() != userSession.getUserId()) {
            feedbackService.changeFileName(userSession.getUserId(), user.getId());
            if (userSession.getUserId() > 0) {
                LOGGER.debug("PREV USER NAME: " + prev_user.getName());
                long sessionCount = sessionService.checkUser(userSession.getUserId());

                if (sessionCount <= 1 && prev_user.getName() == null) {
                    LOGGER.debug("in guestusermapping() method user have multiple sessions ........");
                    loginService.changeDeletedField(userSession.getUserId(), true, Common.checkLocal(request));
                }
            }
            userSession.setUserId(user.getId());
            if (!user.isAdmin()) {
                cookieService.changeCookieValue(request, response, user.getId());
            }
            sessionService.updateUserId(userSession);
        }
        if (request.getParameter("isLoginRemember") != null) {
            boolean isLoginRemember = Boolean.valueOf(request.getParameter("isLoginRemember"));
            long cookieUserId = 0;
            if (isLoginRemember) {
                Cookie loginRememberCookie = cookieService.getIsLoggedInCookie(response, request);
                if (loginRememberCookie != null) {
                    cookieUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(loginRememberCookie.getValue()));
                } else if (loginRememberCookie != null || cookieUserId <= 0) {
                    cookieService.createIsLoggedInCookie(response, user.getId());
                }
            }
        }

        userSession.setLoggedIn(true);
        session.setAttribute("userSession", userSession);
    }
}
