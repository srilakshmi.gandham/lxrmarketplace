package lxr.marketplace.user;

import java.io.IOException;
import java.io.PrintWriter;
import lxr.marketplace.util.SendMail;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.Calendar;
import java.util.List;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class SignupService extends JdbcDaoSupport {
    
    private static Logger logger = Logger.getLogger(SignupService.class);
    
    LoginService loginService;
    String supportMail;
    EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public void setEncryptAndDecryptUsingDES(EncryptAndDecryptUsingDES encryptAndDecryptUsingDES) {
        this.encryptAndDecryptUsingDES = encryptAndDecryptUsingDES;
    }

    public Signup caseInsensitiveFind(String email) {
        Signup signup = null;
        SqlRowSet sqlRowSet = null;
        String query = null;
        query = "select * from user where email = LOWER('" + email + "')";
        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    signup = new Signup();
                    signup.setId(sqlRowSet.getInt("id"));
                    signup.setName(sqlRowSet.getString("name"));
                    signup.setEmail(sqlRowSet.getString("email"));
                    signup.setPassword(sqlRowSet.getString("password"));
                    signup.setActive(sqlRowSet.getBoolean("is_active"));
                    signup.setAdmin(sqlRowSet.getBoolean("is_admin"));
                    signup.setActivationId(sqlRowSet.getLong("activation_id"));
                    Calendar reg_cal = Calendar.getInstance();
                    Calendar act_cal = Calendar.getInstance();
                    reg_cal.setTimeInMillis(sqlRowSet.getTimestamp("reg_date").getTime());
                    signup.setRegistratonTime(reg_cal);
                    if (sqlRowSet.getTimestamp("act_date") != null) {
                        act_cal.setTimeInMillis(sqlRowSet.getTimestamp("act_date").getTime());
                        signup.setActivationTime(act_cal);
                    }
                    signup.setDomainName(sqlRowSet.getString("domain_name"));
                    signup.setSignUpType(sqlRowSet.getInt("signup_type"));
                    signup.setDeleted(sqlRowSet.getBoolean("deleted"));
                    signup.setSocialSignUp(sqlRowSet.getBoolean("is_social_signup"));
                    signup.setUnsubscribe(sqlRowSet.getBoolean("unsubscribe"));
                }
            }
        } catch (Exception e) {
            logger.error("Exception when fetching user data in user table: ", e);
        }
        return signup;
    }

    public void updateLxrmPassword(String email, String password) {
        String query = "update user set password=? where email=?";
        try {
            getJdbcTemplate().update(query, new Object[]{password, email});
        } catch (Exception ex) {
            logger.error("Exception in updateLxrmPassword cause:: "+ex.getLocalizedMessage());
        }
    }

    public Signup find(long activationId) {
        String query = "select email, is_active from user where activation_id=?";
        try {
            Signup signup = (Signup) getJdbcTemplate().queryForObject(query, new Object[]{activationId}, (ResultSet rs, int rowNum) -> {
                Signup snUp = new Signup();
                snUp.setEmail(rs.getString(1));
                snUp.setActive(rs.getBoolean(2));
                return snUp;
            });

            return signup;
        } catch (Exception ex) {
            logger.error("Exception in find cause:: "+ex.getLocalizedMessage());
        }
        return null;
    }

    public List<Signup> listAll() {
        String query = "select id, name, email, password, is_active, is_admin, activation_id, reg_date, act_date from user";
        try {
            List<Signup> signups = getJdbcTemplate().query(query, new BeanPropertyRowMapper(Signup.class));

            return signups;
        }  catch (Exception ex) {
            logger.error("Exception in listAll cause:: "+ex.getLocalizedMessage());
        }

        return null;
    }

    public void save(final Signup signup) {
        final String query = "insert into user (name, email, password, is_active, activation_id, reg_date,domain_name,is_local, is_encrypted) values (?, ?, ?, ?, ?, ?, ?, ?)";
        final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            synchronized (this) {
                getJdbcTemplate().update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(query);
                    statement.setString(1, signup.getName());
                    statement.setString(2, signup.getEmail().toLowerCase());
                    statement.setString(3, signup.getPassword());
                    statement.setBoolean(4, false);
                    statement.setLong(5, signup.getActivationId());
                    statement.setTimestamp(6, regTime);
                    //logger.info(signup.getDomainName() + "Domain Name");
                    statement.setString(7, signup.getDomainName());
                    if (signup.getEmail().contains("netelixir") || signup.getEmail().contains("NETELIXIR")) {
                        statement.setBoolean(8, true);
                    } else {
                        statement.setBoolean(8, false);
                    }
                    statement.setBoolean(9, signup.isEncrypted());
                    return statement;
                });
            }
        } catch (Exception ex) {
            logger.error("Exception on signup save cause:: "+ ex.getLocalizedMessage());
        }
    }

    public long insertNewUser(final Signup signup, final boolean islocal) {
        long deletedUserId = loginService.findDeletedUser();
        long lxrmUserId;
        if (deletedUserId != 0) {
            updateDeletedUserByNew(deletedUserId, signup, islocal);
            return deletedUserId;
        } else {
            final String query = "insert into user (name, email, password, is_active, is_social_signup, activation_id, "
                    + "reg_date, act_date, privacy_policy_agreed_time, domain_name, is_local, is_encrypted) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            KeyHolder keyHolder = new GeneratedKeyHolder();
            try {
                synchronized (this) {
                    getJdbcTemplate().update((Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"id"});
                        if (signup.getName() != null && !signup.getName().isEmpty()) {
                            try {
                                statement.setBytes(1, signup.getName().trim().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(1, signup.getName());
                                logger.error("Exception on signup name: ", e);
                            }
                        } else {
                            statement.setString(1, null);
                        }

                        if (signup.getEmail() != null && !signup.getEmail().isEmpty()) {
                            try {
                                statement.setBytes(2, signup.getEmail().trim().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(2, signup.getEmail());
                                logger.error("UnsupportedEncodingException when converting user email to bytes cause:: ", e);
                            }
                        } else {
                            statement.setString(2, null);
                        }

                        if (signup.getPassword() != null && !signup.getPassword().isEmpty()) {
                            try {
                                statement.setBytes(3, signup.getPassword().trim().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(3, signup.getPassword());
                                logger.error("Exception on signup user email: ", e);
                            }
                        } else {
                            statement.setString(3, null);
                        }
                        statement.setBoolean(4, Boolean.TRUE);
                        statement.setBoolean(5, signup.isSocialSignUp());
                        statement.setLong(6, signup.getActivationId());
                        statement.setTimestamp(7, regTime);
                        statement.setTimestamp(8, regTime);
                        statement.setTimestamp(9, regTime);

                        if (signup.getDomainName() != null && !signup.getDomainName().isEmpty()) {
                            try {
                                statement.setBytes(10, signup.getDomainName().trim().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(10, signup.getDomainName());
                                logger.error("Exception on signup user email: ", e);
                            }
                        } else {
                            statement.setString(10, null);
                        }
                        statement.setBoolean(11, islocal);
                        statement.setBoolean(12, signup.isEncrypted());
                        return statement;
                    }, keyHolder);
                }
            } catch (Exception ex) {
                logger.error("Exception in siginsertNewUsernup cause:: ", ex);
            }
            lxrmUserId = (Long) keyHolder.getKey();
        }
        if (lxrmUserId > 0 && signup.isSocialSignUp() && (signup.getUserSocialProfile() != null && !isUserSocialProfileCaptured(signup.getId(), signup.getUserSocialProfile().getSocialChannel(), signup.getUserSocialProfile().getProfileId()))) {
            signup.getUserSocialProfile().setUserId(lxrmUserId);
            saveUserSocialProfileData(signup.getUserSocialProfile());
        }
        return lxrmUserId;
    }

    private void saveUserSocialProfileData(final UserSocialProfile userSocialProfile) {
        final String query = "insert into user_social_profile (user_id, profile_id, social_channel,access_token,captured_date) values (?, ?, ?, ?, ?)";
        try {
            synchronized (this) {
                getJdbcTemplate().update((Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) con.prepareStatement(query);
                    statement.setLong(1, userSocialProfile.getUserId());
                    statement.setString(2, userSocialProfile.getProfileId());
                    statement.setString(3, userSocialProfile.getSocialChannel());
                    statement.setString(4, ((userSocialProfile.getOauthAccessToken() != null && userSocialProfile.getOauthAccessToken().length() < 299) ? userSocialProfile.getOauthAccessToken() : null));
                    statement.setTimestamp(5, Timestamp.valueOf(userSocialProfile.getProfielCapturedTime()));
                    return statement;
                });
            }
            logger.info("Successfully saved User Social Profile Data for lxrm user id:: " + userSocialProfile.getUserId());
        } catch (DataAccessException ex) {
            logger.error("DataAccessException in saveUserSocialProfileData for the lxrmUserID:  " + userSocialProfile.getUserId() + ", cause:: " + ex);
        }
    }

//    public long insertNewUser(final Signup signup, final boolean islocal, final int signupType) {
//        Long deletedUserId = loginService.findDeletedUser();
//        if (deletedUserId != 0) {
//            updateDeletedUserByNew(deletedUserId, signup, islocal, signupType);
//            return deletedUserId;
//        } else {
//            final String query = "insert into user (name, email, password, is_active, activation_id, "
//                    + "reg_date,act_date,domain_name,is_local,signup_type) values (?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
//            final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
//            KeyHolder keyHolder = new GeneratedKeyHolder();
//            try {
//                synchronized (this) {
//                    getJdbcTemplate().update((Connection con) -> {
//                        PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"id"});
//
//                        if (signup.getName() == null || signup.getName().equals("")) {
//                            statement.setString(1, "");
//                        } else {
//                            try {
//                                statement.setBytes(1, signup.getName().getBytes("UTF-8"));
//                            } catch (UnsupportedEncodingException e) {
//                                statement.setString(1, signup.getName());
//                                logger.error(e);
//                            }
//                        }
//
//                        if (signup.getEmail().toLowerCase() == null || signup.getEmail().toLowerCase().equals("")) {
//                            statement.setString(2, "");
//                        } else {
//                            try {
//                                statement.setBytes(2, signup.getEmail().toLowerCase().getBytes("UTF-8"));
//                            } catch (UnsupportedEncodingException e) {
//                                statement.setString(2, signup.getEmail().toLowerCase());
//                                logger.error(e);
//                            }
//                        }
//
//                        if (signup.getPassword() == null || signup.getPassword().equals("")) {
//                            statement.setString(3, "");
//                        } else {
//                            try {
//                                statement.setBytes(3, signup.getPassword().getBytes("UTF-8"));
//                            } catch (UnsupportedEncodingException e) {
//                                statement.setString(3, signup.getPassword());
//                                logger.error(e);
//                            }
//                        }
//
//                        if (signup.getDomainName() == null || signup.getDomainName().equals("")) {
//                            statement.setString(8, "");
//                        } else {
//                            try {
//                                statement.setBytes(8, signup.getDomainName().getBytes("UTF-8"));
//                            } catch (UnsupportedEncodingException e) {
//                                statement.setString(8, signup.getDomainName());
//                                logger.error(e);
//                            }
//                        }
//
//                        statement.setBoolean(4, true);
//                        statement.setLong(5, signup.getActivationId());
//                        statement.setTimestamp(6, regTime);
//                        statement.setTimestamp(7, regTime);
//
//                        if (signup.getEmail().contains("netelixir") || islocal || signup.getEmail().contains("NETELIXIR")) {
//                            statement.setBoolean(9, true);
//                        } else {
//                            statement.setBoolean(9, false);
//                        }
//                        statement.setInt(10, signupType);
//                        return statement;
//                    }, keyHolder);
//                }
//            } catch (Exception ex) {
//                logger.error(ex);
//            }
//
//            return (Long) keyHolder.getKey();
//        }
//    }
    public boolean updateGuestUser(final Signup signup, boolean local) {
        boolean userInfoUpdated = false;
        final Timestamp actTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String query = "update user set name = ?, email = ?, password = ?, is_active = ?, is_social_signup = ?, activation_id = ?, act_date = ?, privacy_policy_agreed_time = ?, "
                + " domain_name = ?, is_local = ?, unsubscribe = ?, is_encrypted = ?  where id = ?";
        try {
            getJdbcTemplate().update(query,
                    new Object[]{
                        signup.getName(),
                        signup.getEmail().toLowerCase(),
                        signup.getPassword(),
                        Boolean.TRUE,
                        signup.isSocialSignUp(),
                        signup.getActivationId(),
                        actTime,
                        actTime,
                        signup.getDomainName(),
                        local,
                        Boolean.FALSE,
                        signup.isEncrypted(),
                        signup.getId()
                    });
            userInfoUpdated = Boolean.TRUE;
        } catch (DataAccessException ex) {
            logger.error("DataAccessException on updating guest user: ", ex);
        }
        if (userInfoUpdated && signup.isSocialSignUp() && (signup.getUserSocialProfile() != null && !isUserSocialProfileCaptured(signup.getId(), signup.getUserSocialProfile().getSocialChannel(), signup.getUserSocialProfile().getProfileId()))) {
            signup.getUserSocialProfile().setUserId(signup.getId());
            saveUserSocialProfileData(signup.getUserSocialProfile());
        }
        return userInfoUpdated;
    }

    public void updateGuestUser(final Signup signup, boolean local, int signupType) {

        final Timestamp actTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String query = "update user set name = ?, email = ?, password = ?, is_active = ?, activation_id = ?, act_date = ?, privacy_policy_agreed_time = ?,  "
                + " domain_name = ?, is_local = ?, unsubscribe = ?, signup_type = ? where id = ?";
        try {
            if (signup.getEmail().contains("netelixir") || signup.getEmail().contains("NETELIXIR")) {
                local = true;
            }
            getJdbcTemplate().update(query, new Object[]{
                signup.getName(),
                signup.getEmail().toLowerCase(),
                signup.getPassword(),
                true,
                signup.getActivationId(),
                actTime,
                actTime,
                signup.getDomainName(),
                local, false, signupType,
                signup.getId()
            });
        } catch (Exception ex) {
            logger.error("Exception on updating guest user: ", ex);
        }
    }

    public void updateDeletedUserByNew(long deletedUserId, Signup signup, boolean local) {
        final String query = "update user set name = ?, email = ?, password = ?, is_active = ?, activation_id = ?, "
                + "reg_date = ?, act_date = ?, domain_name = ?, is_local = ?, deleted = ?, unsubscribe = ? where id = ?";

        final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            if (signup.getEmail().contains("netelixir") || signup.getEmail().contains("NETELIXIR")) {
                local = true;
            }
            getJdbcTemplate().update(query, new Object[]{signup.getName(),
                signup.getEmail(),
                signup.getPassword(),
                true,
                signup.getActivationId(),
                regTime, regTime,
                signup.getDomainName(),
                local, false, false,
                deletedUserId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateDeletedUserByNew(long deletedUserId, Signup signup, boolean local, int signupType) {
        final String query = "update user set name = ?, email = ?, password = ?, is_active = ?, activation_id = ?, "
                + "reg_date = ?, act_date = ?, domain_name = ?, is_local = ?, deleted = ?, unsubscribe = ?, signup_type = ? where id = ?";

        final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            if (signup.getEmail().contains("netelixir") || signup.getEmail().contains("NETELIXIR")) {
                local = true;
            }
            getJdbcTemplate().update(query, new Object[]{signup.getName(),
                signup.getEmail(),
                signup.getPassword(),
                true,
                signup.getActivationId(),
                regTime, regTime,
                signup.getDomainName(),
                local, false, false, signupType,
                deletedUserId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void update(String email) {
        String query = "update user set is_active=true, act_date = ? where email=?";
        final Timestamp actTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            getJdbcTemplate().update(query, new Object[]{actTime, email});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void delete(int id) {
        String query = "delete from user where id = ?";

        try {
            getJdbcTemplate().update(query, new Object[]{id});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public synchronized long generateActivationId() {
        long activationId = Calendar.getInstance().getTime().getTime();
        return activationId;
    }

    public synchronized void sendMail(Signup signup, String url) {
        if (signup.getEmail() != null && !signup.getEmail().isEmpty()) {
            String subject = EmailBodyCreator.subjectOnWelcome;
            String bodyText = EmailBodyCreator.bodyForSignup(signup);
            SendMail.sendMail(supportMail, signup.getEmail(), subject, bodyText);
        }
    }

    public Login getUser(long id) {
        Login user = loginService.find(id);
        user.setPassword(Common.decryptedPassword(user.getPassword()));
        return user;
    }

    public void updateSignupDetailsInUserTab(Signup signup, long userId) {
        final String query = "update user set name = ?,password = ? where id = " + userId;

        try {
            getJdbcTemplate().update(query, new Object[]{
                signup.getName(), signup.getPassword(),});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public Signup getUserDetails(String email, int searchType) {
        Signup signup = null;
        SqlRowSet sqlRowSet;
        String query = null;
        if (searchType == 0) {
            query = "select id, name, email, password, is_active, is_admin, activation_id, reg_date, act_date,domain_name, signup_type, deleted, unsubscribe, is_social_signup"
                    + " from user where email = '" + email + "'";
        } else if (searchType == 1) {
            query = "select id, name, email, password, is_active, is_admin, activation_id, reg_date, act_date,domain_name, signup_type, deleted, unsubscribe, is_social_signup"
                    + " from user where email like '%<" + email + ">%'";
        }
        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    signup = new Signup();
                    signup.setId(sqlRowSet.getInt("id"));
                    signup.setName(sqlRowSet.getString("name"));
                    signup.setEmail(sqlRowSet.getString("email"));
                    signup.setPassword(sqlRowSet.getString("password"));
                    signup.setActive(sqlRowSet.getBoolean("is_active"));
                    signup.setAdmin(sqlRowSet.getBoolean("is_admin"));
                    signup.setActivationId(sqlRowSet.getLong("activation_id"));
                    Calendar reg_cal = Calendar.getInstance();
                    Calendar act_cal = Calendar.getInstance();
                    reg_cal.setTimeInMillis(sqlRowSet.getTimestamp("reg_date").getTime());
                    signup.setRegistratonTime(reg_cal);
                    if (sqlRowSet.getTimestamp("act_date") != null) {
                        act_cal.setTimeInMillis(sqlRowSet.getTimestamp("act_date").getTime());
                        signup.setActivationTime(act_cal);
                    }

                    signup.setDomainName(sqlRowSet.getString("domain_name"));
                    signup.setSignUpType(sqlRowSet.getInt("signup_type"));
                    signup.setDeleted(sqlRowSet.getBoolean("deleted"));
                    signup.setSocialSignUp(sqlRowSet.getBoolean("is_social_signup"));
                    signup.setUnsubscribe(sqlRowSet.getBoolean("unsubscribe"));
                }
            }
        } catch (Exception e) {
            logger.error("Exception when fetching user data in user table: ", e);
        }
        return signup;
    }

    public boolean updateUserInfo(Login user, boolean islocal) {
        boolean updateStatus = false;
        if (user.getUserName().contains("netelixir") || user.getUserName().contains("NETELIXIR")) {
            islocal = true;
        }
        String query = "update user SET email=?, name = ?, password = ?, act_date = ?, is_local=?, is_encrypted = ? where id = ?";
        Object[] params = {user.getUserName(), user.getName(), user.getPassword(), user.getActivationDate(),
            islocal, user.isEncrypted(), user.getId()};
        try {
            getJdbcTemplate().update(query, params);
            updateStatus = true;
        } catch (Exception e) {
            logger.info("Exception when update user info in user table");
        }
        return updateStatus;
    }

    public void sendLXRMCaptchaJson(String lxrmCaptcha, HttpServletResponse response) {
        JSONArray arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(lxrmCaptcha);
            //arr.put(changeStatus);
            writer.print(arr);
        } catch (IOException e) {
            logger.error("Exception in sendLXRMCaptchaJson:", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public Map<Integer, Map<Integer, String>> getUserPersonaInof() {
        String query = "select * from lxrm_user_persona_category_info";
        Map<Integer, Map<Integer, String>> userPersona = new LinkedHashMap<>();
        SqlRowSet sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        if (sqlRowSet.isBeforeFirst()) {
            Map<Integer, String> userDesignations = new LinkedHashMap<>();
            Map<Integer, String> howToKnowAboutLXRM = new LinkedHashMap<>();
            while (sqlRowSet.next()) {
                int personaCategoryId = sqlRowSet.getInt("persona_category_id");
                if (personaCategoryId == 1) {
                    userDesignations.put(sqlRowSet.getInt("persona_category_info_id"), sqlRowSet.getString("persona_category_info_name"));
                } else if (personaCategoryId == 2) {
                    howToKnowAboutLXRM.put(sqlRowSet.getInt("persona_category_info_id"), sqlRowSet.getString("persona_category_info_name"));
                }
            }
            userPersona.put(1, userDesignations);
            userPersona.put(2, howToKnowAboutLXRM);
        }
        return userPersona;
    }

    void saveUserPersona(long userId, int howToKnowAboutLXRM, int userDesignations) {
        String query = "INSERT INTO lxrm_user_persona_info (user_id, persona_category_info_id) values(?,?)";
        getJdbcTemplate().update(query, userId, howToKnowAboutLXRM);
        getJdbcTemplate().update(query, userId, userDesignations);
    }

    /*Returns User Id of LXRMarketplace.
    Serach for user based on Email
    If user exists in below cases 
    Guest user: updates the user info and update activiation date. 
    New User: Insert new record in user table.
    Send user credentials trough and welcome mail to user email address for both cases.
     */
    public long searchUserInMarketplace(Signup signupObj, String lxrmHostingURL) {
        /*Searching for user existance*/
        Signup existingUser = null;
        long lxrmUserId = 0;
        String autoGeneratedPassword = null;
        String encryptPassword = null;
        if (signupObj != null) {
            if (signupObj.getEmail() != null && !signupObj.getEmail().isEmpty()) {
                /* Searching for login user  for emailId in ( Table: user )*/
                existingUser = getUserDetails(signupObj.getEmail(), 0);
                if (existingUser != null) {
                    lxrmUserId = existingUser.getId();
                } else if (existingUser == null) {
                    /*No user found with Login*/
 /* Searching as guest user  for emailId in ( Table: user )*/
                    existingUser = getUserDetails(signupObj.getEmail(), 1);
                    if (existingUser != null) {
                        lxrmUserId = existingUser.getId();
                    }
                }
            } else if (signupObj.isSocialSignUp() && signupObj.getUserSocialProfile() != null) {
                if (signupObj.getUserSocialProfile().getProfileId() != null && signupObj.getUserSocialProfile().getSocialChannel() != null) {
                    /*Checking in User social profile
                    This case may raise when email not able to capture from Social channel 
                    using profile id of a channel preparing LXRM User Id to prepare user LXRM user profile*/
                    UserSocialProfile existedSocialProfile = getUserSocialProfile(signupObj.getUserSocialProfile().getProfileId(), signupObj.getUserSocialProfile().getSocialChannel());
                    lxrmUserId = (existedSocialProfile != null ? existedSocialProfile.getUserId() : 0);
                    logger.info("Profile ID:: " + signupObj.getUserSocialProfile().getProfileId() + ", Channel Name:: " + signupObj.getUserSocialProfile().getSocialChannel() + ", lxrmUserId:: " + lxrmUserId);
                }
            }

            if (existingUser == null && lxrmUserId == 0) {
                /* EmailId is not existed in lxrmarketplace as user/guest user. 
                 * Creating a user in lxrmarketplace with emailId.*/
                existingUser = new Signup();
                if (signupObj.getEmail() != null && !signupObj.getEmail().isEmpty() && !signupObj.getEmail().equalsIgnoreCase("Email")) {
                    existingUser.setEmail(signupObj.getEmail());
                }
                existingUser.setName(signupObj.getName() != null ? signupObj.getName() : (signupObj.getEmail() != null ? signupObj.getEmail().split("@")[0] : null));
                if (!signupObj.isSocialSignUp()) {
                    autoGeneratedPassword = Common.autoGeneratePassword().trim();
                    encryptPassword = encryptAndDecryptUsingDES.encrypt(autoGeneratedPassword);
                    /*Saving the password in encryped format*/
                    existingUser.setPassword(encryptPassword != null ? encryptPassword : autoGeneratedPassword);
                    existingUser.setEncrypted(encryptPassword != null ? Boolean.TRUE : Boolean.FALSE);
                }
                existingUser.setActive(Boolean.TRUE);
                existingUser.setAdmin(Boolean.FALSE);
                existingUser.setActivationId(Common.generateActivationId());
                if (signupObj.getContactNumber() != null) {
                    existingUser.setContactNumber(signupObj.getContactNumber());
                }
                existingUser.setSocialSignUp(signupObj.isSocialSignUp());
                if (existingUser.isSocialSignUp()) {
                    existingUser.setUserSocialProfile(signupObj.getUserSocialProfile());
                }

                signupObj.setLocalUser(((signupObj.getEmail() != null && signupObj.getEmail().toLowerCase().contains(("netelixir"))) ? Boolean.TRUE : Boolean.FALSE));
                existingUser.setLocalUser(signupObj.isLocalUser());
                /*Inserting new record in user table.*/
                lxrmUserId = insertNewUser(existingUser, signupObj.isLocalUser());
                if (lxrmUserId > 0) {
                    /*Sending the password in user readble format*/
                    if (autoGeneratedPassword != null) {
                        existingUser.setPassword(autoGeneratedPassword);
                    }
                    if (existingUser.getEmail() != null && !existingUser.getEmail().isEmpty() && !signupObj.isSocialSignUp()) {
                        /*Sending activation mail for user.*/
                        sendMail(existingUser, lxrmHostingURL);
                    }
                }
            } else if (lxrmUserId != 0 && existingUser != null && (existingUser.getEmail().contains("GuestUser"))) {
                /* EmailId is existed in lxrmarketplace as guest user.
                   Updating the guest user status as permenent user.*/
                if (signupObj.getEmail() != null && !signupObj.getEmail().isEmpty() && !signupObj.getEmail().equalsIgnoreCase("Email")) {
                    existingUser.setEmail(signupObj.getEmail());
                }
                if (existingUser.getName() == null || (existingUser.getName() != null && existingUser.getName().isEmpty())) {
                    existingUser.setName(signupObj.getName() != null ? signupObj.getName() : (signupObj.getEmail() != null ? signupObj.getEmail().split("@")[0] : null));
                }
                if (!signupObj.isSocialSignUp()) {
                    autoGeneratedPassword = Common.autoGeneratePassword().trim();
                    encryptPassword = encryptAndDecryptUsingDES.encrypt(autoGeneratedPassword);
                    /*Saving the password in encryped format*/
                    existingUser.setPassword(encryptPassword != null ? encryptPassword : autoGeneratedPassword);
                    existingUser.setEncrypted(encryptPassword != null ? Boolean.TRUE : Boolean.FALSE);
                }
                existingUser.setAdmin(Boolean.FALSE);
                existingUser.setActive(Boolean.TRUE);
                existingUser.setActivationId(Common.generateActivationId());
                existingUser.setUserSocialProfile(signupObj.getUserSocialProfile());
                existingUser.setSocialSignUp(signupObj.isSocialSignUp());
                signupObj.setLocalUser(((signupObj.getEmail() != null && signupObj.getEmail().toLowerCase().contains(("netelixir"))) ? Boolean.TRUE : Boolean.FALSE));
                existingUser.setLocalUser(signupObj.isLocalUser());
                if (updateGuestUser(existingUser, signupObj.isLocalUser())) {
                    /*Sending the password in user readble format*/
                    if (autoGeneratedPassword != null) {
                        existingUser.setPassword(autoGeneratedPassword);
                    }
                    if (existingUser.getEmail() != null && !existingUser.getEmail().isEmpty() && !signupObj.isSocialSignUp()) {
                        sendMail(existingUser, lxrmHostingURL);
                    }
                }
            }
        }
        return lxrmUserId;
    }

    /* Send activation mail to registered user in lxrmarketplace .*/
    public void sendActivationMailofLXRMUser(Signup lxrmUser) {
        if (lxrmUser.getEmail() != null) {
            String subject = EmailBodyCreator.subjectOnWelcome;
            String bodyText = EmailBodyCreator.bodyForSignup(lxrmUser);
            SendMail.sendMail(supportMail, lxrmUser.getEmail(), subject, bodyText);
        }
    }

    public boolean isUserPersonaCaptured(long lxrmUserId) {
        String query = "SELECT count(*) FROM lxrm_user_persona_info WHERE user_id = ?";
        int count = getJdbcTemplate().queryForObject(query, new Object[]{lxrmUserId}, Integer.class);
        return count > 0;
    }

    private boolean isUserSocialProfileCaptured(long lxrmUserId, String socialChannel, String profileId) {
        String query = "SELECT count(*) FROM user_social_profile WHERE user_id = ? and social_channel = ? and profile_id = ?";
        int count = getJdbcTemplate().queryForObject(query, new Object[]{lxrmUserId, socialChannel, profileId}, Integer.class);
        return count > 0;
    }

    public UserSocialProfile getUserSocialProfile(String socialProfilId, String socialChannel) {
        String query = "SELECT * FROM user_social_profile WHERE profile_id = ? and social_channel = ?";
        UserSocialProfile existedUserSocialProfile = null;
        try {
            existedUserSocialProfile = this.getJdbcTemplate().queryForObject(query, new Object[]{socialProfilId.trim(), socialChannel.trim()}, (ResultSet resultSet, int rowNum) -> {
                UserSocialProfile socialProfile = new UserSocialProfile();
                socialProfile.setUserId(resultSet.getLong("user_id"));
                socialProfile.setProfileId(resultSet.getString("profile_id"));
                socialProfile.setSocialChannel(resultSet.getString("social_channel"));
                Timestamp capturedDate = resultSet.getTimestamp("captured_date");
                socialProfile.setProfielCapturedTime(capturedDate.toLocalDateTime());
                return socialProfile;
            });
        } catch (Exception e) {
            logger.error("Exception in getUserSocialProfile casue:: " + e.getMessage());
        }
        logger.info("Query to get UserUsageStats :: " + query + ", existedUserSocialProfile:: " + existedUserSocialProfile);
        return existedUserSocialProfile;
    }

}
