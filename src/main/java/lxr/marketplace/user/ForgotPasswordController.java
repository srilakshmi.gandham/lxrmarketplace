package lxr.marketplace.user;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ForgotPasswordController {

    private static Logger logger = Logger.getLogger(ForgotPasswordController.class);
    private ForgotPasswordService forgotPasswordService;
    @Autowired
    String supportMail;
    @Autowired
    private EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    @Autowired
    public ForgotPasswordController(ForgotPasswordService forgotPasswordService) {
        this.forgotPasswordService = forgotPasswordService;
    }

    @RequestMapping(value = "/forgot-password.html", method = RequestMethod.GET)
    public String forgotPasswordPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn) {
            try {
                String applicationUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
                response.sendRedirect(applicationUrl + "/");
            } catch (IOException ex) {
                logger.error("");
            }
        }
        return "views/forgotPassword";
    }

    @RequestMapping(value = "/forgot-password.html", method = RequestMethod.POST)
    @ResponseBody
    public Object[] passwordSendToMail(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("forgotpassword") ForgotPassword forgotpassword) {
        Object[] data = new Object[1];
        Login user = forgotPasswordService.findUserForForgotPassword(forgotpassword.getUserName());
        if ((user != null && (user.isDeleted() || user.isUnsubscribed()))){
            data[0] = "contactSupportTeam";
        }else if (user != null && !user.isSocialLogin()) {
            String password = Common.generatePassword();
            user.setPassword(password);
            forgotPasswordService.updateLxrmPassword(user.getUserName(), encryptAndDecryptUsingDES.encrypt(password));
            String subject = EmailBodyCreator.subjectOnForgotPassword;
            String bodyText = EmailBodyCreator.bodyForForgotPassword(user);
            boolean sentStatus = SendMail.sendMail(supportMail, user.getUserName(), subject, bodyText);
            if (sentStatus) {
                data[0] = "mailSent";
            } else {
                data[0] = "mailSendFailed";
            }
        } else if(user != null && user.isSocialLogin()){
            data[0] = "socialChannelLogin";
        } else if(user == null){
            data[0] = "forgotPasswordErrorMsg";
        }
        return data;
    }

    @RequestMapping(value = "change-password.html", method = RequestMethod.GET)
    public String changePasswordPage(HttpServletRequest request,
            HttpServletResponse response, HttpSession session) {
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        String viewName = "views/changePassword";
        if (!userLoggedIn) {
            try {
                String applicationUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
                response.sendRedirect(applicationUrl + "/");
            } catch (IOException ex) {
                logger.info("IOException in changePasswordPage cause:: "+ex);
                viewName = "views/lxrMarketplace";
            }
        }
        return viewName;
    }

    @RequestMapping(value = "change-password.html", params = {"changePassword"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] changePassword(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Object[] data = new Object[1];
        Login userInfo = (Login) session.getAttribute("user");
        String newPassword = request.getParameter("newPassword");
        boolean updateStatus = forgotPasswordService.changePassword(userInfo.getId(), encryptAndDecryptUsingDES.encrypt(newPassword));
        data[0] = updateStatus;
        userInfo.setPassword(newPassword);
        session.setAttribute("user", userInfo);
//        session.setAttribute("success", "success");
        return data;
    }
}
