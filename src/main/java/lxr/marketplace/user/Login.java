package lxr.marketplace.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;

public class Login implements Serializable{

    private long id;
    private String name;
    private String userName;
    private String password;
    private String domainName;
    private boolean isActive;
    private boolean isAdmin;
    private boolean unsubscribed;
    private boolean islocal;
    private Calendar activationDate;
    private Timestamp privacyPolicyAgreedTime;
    private boolean deleted;
    private boolean encrypted;
    private UserUsageStats userUsageStats;
    private boolean socialLogin;

    public boolean isIslocal() {
        return islocal;
    }

    public void setIslocal(boolean islocal) {
        this.islocal = islocal;
    }

    public boolean isUnsubscribed() {
        return unsubscribed;
    }

    public void setUnsubscribed(boolean unsubscribed) {
        this.unsubscribed = unsubscribed;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return (userName == null) ? "Email" : userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return (password == null) ? "Password" : password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Calendar getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Calendar activationDate) {
        this.activationDate = activationDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Timestamp getPrivacyPolicyAgreedTime() {
        return privacyPolicyAgreedTime;
    }

    public void setPrivacyPolicyAgreedTime(Timestamp privacyPolicyAgreedTime) {
        this.privacyPolicyAgreedTime = privacyPolicyAgreedTime;
    }
    

    public boolean isEncrypted() {
        return encrypted;
    }

    public void setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
    }

    public boolean equals(Object login) {
        if (login != null && login instanceof Login) {
            Login l = (Login) login;
            return this.id == l.getId();
        } else {
            return false;
        }
    }
    
    @Override
    public int hashCode() {
        return BigInteger.valueOf(id).intValue();
    }

    public UserUsageStats getUserUsageStats() {
        return userUsageStats;
    }

    public void setUserUsageStats(UserUsageStats userUsageStats) {
        this.userUsageStats = userUsageStats;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isSocialLogin() {
        return socialLogin;
    }

    public void setSocialLogin(boolean socialLogin) {
        this.socialLogin = socialLogin;
    }
    
    

    @Override
    public String toString() {
        return "Login{" + "id=" + id + ", name=" + name + ", userName=" + userName + ", password=" + password + ", domainName=" + domainName + ", isActive=" + isActive + ", isAdmin=" + isAdmin + ", unsubscribed=" + unsubscribed + ", islocal=" + islocal + ", activationDate=" + activationDate + ", privacyPolicyAgreedTime=" + privacyPolicyAgreedTime + ", deleted=" + deleted + ", encrypted=" + encrypted + ", userUsageStats=" + userUsageStats + '}';
    }
    
    
}
