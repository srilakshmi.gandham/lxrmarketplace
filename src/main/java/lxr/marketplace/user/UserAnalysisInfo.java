/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.user;

import java.sql.Timestamp;

/**
 *
 * @author anil
 */
public class UserAnalysisInfo {
    private long userId;
    private long toolId;
    private String websiteName;
    private String analysisIssues;
    private String suggestedQuestion;
    private String userInputs;
    private Timestamp analysisTime;
    private String toolUrl;

    public String getToolUrl() {
        return toolUrl;
    }

    public void setToolUrl(String toolUrl) {
        this.toolUrl = toolUrl;
    }

    public Timestamp getAnalysisTime() {
        return analysisTime;
    }

    public void setAnalysisTime(Timestamp analysisTime) {
        this.analysisTime = analysisTime;
    }

    public String getUserInputs() {
        return userInputs;
    }

    public void setUserInputs(String userInputs) {
        this.userInputs = userInputs;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getToolId() {
        return toolId;
    }

    public void setToolId(long toolId) {
        this.toolId = toolId;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getAnalysisIssues() {
        return analysisIssues;
    }

    public void setAnalysisIssues(String analysisIssues) {
        this.analysisIssues = analysisIssues;
    }

    public String getSuggestedQuestion() {
        return suggestedQuestion;
    }

    public void setSuggestedQuestion(String suggestedQuestion) {
        this.suggestedQuestion = suggestedQuestion;
    }
}
