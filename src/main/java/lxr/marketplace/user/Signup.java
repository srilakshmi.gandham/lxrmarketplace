package lxr.marketplace.user;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class Signup implements Serializable{

    private long id;
    @NotNull
    @NotEmpty
    @Size(min = 4, max = 100)
    private String name;
    @NotNull
    @NotEmpty
    @Email
    @Size(min = 4, max = 100)
    private String email;
    private String domainName;
    @NotNull
    @NotEmpty
    @Size(min = 4, max = 100)
    private String password;
    @NotNull
    @NotEmpty
    @Size(min = 4, max = 100)
    private String repassword;
    
    private boolean termsAndConditions;

    private boolean active;
    private boolean admin;
    private long activationId;
    private Calendar regTime;
    private Calendar actTime;
    private int signUpType;
    private boolean deleted;
    private String captcha;
    private boolean socialSignUp;
    
    private UserSocialProfile userSocialProfile;
    
    private boolean localUser;
    
    private Timestamp privacyPolicyAgreedTime;
    
    private String contactNumber;
    
    private String emailValidationCode;
    private boolean unsubscribe;
    private boolean encrypted;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return (name == null) ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public boolean isTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(boolean termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public long getActivationId() {
        return activationId;
    }

    public void setActivationId(long activationId) {
        this.activationId = activationId;
    }

    public Calendar getRegistratonTime() {
        return regTime;
    }

    public void setRegistratonTime(Calendar regTime) {
        this.regTime = regTime;
    }

    public Calendar getActivationTime() {
        return actTime;
    }

    public void setActivationTime(Calendar actTime) {
        this.actTime = actTime;
    }

    public int getSignUpType() {
        return signUpType;
    }

    public void setSignUpType(int signUpType) {
        this.signUpType = signUpType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Timestamp getPrivacyPolicyAgreedTime() {
        return privacyPolicyAgreedTime;
    }

    public void setPrivacyPolicyAgreedTime(Timestamp privacyPolicyAgreedTime) {
        this.privacyPolicyAgreedTime = privacyPolicyAgreedTime;
    }

    public boolean isSocialSignUp() {
        return socialSignUp;
    }

    public void setSocialSignUp(boolean socialSignUp) {
        this.socialSignUp = socialSignUp;
    }

    public UserSocialProfile getUserSocialProfile() {
        return userSocialProfile;
    }

    public void setUserSocialProfile(UserSocialProfile userSocialProfile) {
        this.userSocialProfile = userSocialProfile;
    }

    public boolean isLocalUser() {
        return localUser;
    }

    public void setLocalUser(boolean localUser) {
        this.localUser = localUser;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailValidationCode() {
        return emailValidationCode;
    }

    public void setEmailValidationCode(String emailValidationCode) {
        this.emailValidationCode = emailValidationCode;
    }

//    public String getEmailVerificationScore() {
//        return emailVerificationScore;
//    }
//
//    public void setEmailVerificationScore(String emailVerificationScore) {
//        this.emailVerificationScore = emailVerificationScore;
//    }

    public boolean isUnsubscribe() {
        return unsubscribe;
    }

    public void setUnsubscribe(boolean unsubscribe) {
        this.unsubscribe = unsubscribe;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public void setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
    }

    @Override
    public String toString() {
        return "Signup{" + "id=" + id + ", name=" + name + ", email=" + email + ", domainName=" + domainName + ", password=" + password + ", repassword=" + repassword + ", termsAndConditions=" + termsAndConditions + ", active=" + active + ", admin=" + admin + ", activationId=" + activationId + ", signUpType=" + signUpType + ", deleted=" + deleted + ", captcha=" + captcha + ", socialSignUp=" + socialSignUp + ", userSocialProfile=" + userSocialProfile + ", localUser=" + localUser + ", privacyPolicyAgreedTime=" + privacyPolicyAgreedTime + ", contactNumber=" + contactNumber + ", emailValidationCode=" + emailValidationCode + ", unsubscribe=" + unsubscribe + ", encrypted=" + encrypted + '}';
    }

    
    
}
