package lxr.marketplace.user;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ForgotPasswordService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(ForgotPasswordService.class);

    public Login findUserForForgotPassword(String userName) {
        String query = "select id, password, is_active, name, is_admin, domain_name, unsubscribe, is_local, act_date, privacy_policy_agreed_time, deleted, is_encrypted, is_social_signup, email from user where LOWER(email) = ?";
        Login forgotUserLoginObj = null;
        List<Login> lxrmUserLoginList = null;
        try {
            lxrmUserLoginList = getJdbcTemplate().query(query, new Object[]{userName.toLowerCase()}, (ResultSet rs, int rowNum) -> {
                Login login = new Login();
                login.setId(rs.getLong(1));
                login.setPassword(Common.decryptedPassword(rs.getString(2)));
//                login.setPassword(rs.getString(2));
                login.setActive(rs.getBoolean(3));
                login.setName(rs.getString(4));
                login.setAdmin(rs.getBoolean(5));
                login.setDomainName(rs.getString(6));
                login.setUnsubscribed(rs.getBoolean(7));
                login.setIslocal(rs.getBoolean(8));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                login.setActivationDate(actDate);
                login.setPrivacyPolicyAgreedTime(rs.getTimestamp(10));
                login.setDeleted(rs.getBoolean(11));
                login.setEncrypted(rs.getBoolean(12));
                login.setSocialLogin(rs.getBoolean(13));
                login.setUserName(rs.getString(14));
                return login;
            });
        } catch (DataAccessException e) {
            logger.error("DataAccessException in findUserForForgotPassword cause:: ", e);
        } catch (Exception e) {
            logger.error("Exception in findUserForForgotPassword cause:: ", e);
        }
        forgotUserLoginObj = (lxrmUserLoginList != null && !lxrmUserLoginList.isEmpty()) ? lxrmUserLoginList.get(0) : null;
        if(forgotUserLoginObj == null){
            logger.info("Query to fetch user login obj for forgot password for email:: "+userName+",  and query:: "+query);
        }
        return forgotUserLoginObj;
    }

    public boolean changePassword(long userId, String password) {
        boolean updateStatus = false;
        String updateQuery = "UPDATE user SET PASSWORD = ?, is_encrypted = 1 WHERE id = ?";
        try {
            getJdbcTemplate().update(updateQuery, new Object[]{password, userId});
            updateStatus = true;
        } catch (Exception ex) {
            logger.error("Exception when changing the password: ", ex);
        }
        return updateStatus;
    }

    public void updateLxrmPassword(String email, String password) {
        String query = "update user set password = ?, is_encrypted = 1 where email=?";
        try {
            getJdbcTemplate().update(query, new Object[]{password, email});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }
}
