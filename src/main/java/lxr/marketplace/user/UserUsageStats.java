/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.user;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author vidyasagar.korada
 */
public class UserUsageStats implements Serializable{
    
    private long userId;
    private int countofToolsUsed;
    private int countOfWebSitesAnalysis;
    private List<Integer> toolsUsedList;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getCountofToolsUsed() {
        return countofToolsUsed;
    }

    public void setCountofToolsUsed(int countofToolsUsed) {
        this.countofToolsUsed = countofToolsUsed;
    }

    public int getCountOfWebSitesAnalysis() {
        return countOfWebSitesAnalysis;
    }

    public void setCountOfWebSitesAnalysis(int countOfWebSitesAnalysis) {
        this.countOfWebSitesAnalysis = countOfWebSitesAnalysis;
    }

    public List<Integer> getToolsUsedList() {
        return toolsUsedList;
    }

    public void setToolsUsedList(List<Integer> toolsUsedList) {
        this.toolsUsedList = toolsUsedList;
    }

    @Override
    public String toString() {
        return "UserUsageStats{" + "userId=" + userId + ", countofToolsUsed=" + countofToolsUsed + ", countOfWebSitesAnalysis=" + countOfWebSitesAnalysis + ", toolsUsedList=" + toolsUsedList + '}';
    }

    
}
