package lxr.marketplace.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

public class UserActivationController extends SimpleFormController{

	private static Logger logger = Logger.getLogger(UserActivationController.class);
	private SignupService signupService;
	private LoginService loginService;
	
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public void setSignupService(SignupService signupService) {
		this.signupService = signupService;
	}

	public UserActivationController() {
		setCommandClass(Signup.class);
		setCommandName("lxrmarketplaceactivation");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		if (WebUtils.hasSubmitParameter(request, "activation")) {
			long activationId = Long.parseLong(WebUtils.findParameterValue(request, "activation"));
			//signupService = new SignupService();
			Login login = loginService.findActiveUser(activationId);
			ModelAndView mAndV = null;  
			if(login.isActive()){
				signupService.update(login.getUserName());
				session.setAttribute("active", login);
				mAndV = new ModelAndView(getSuccessView(), "signUp", login);
			}else{
				signupService.update(login.getUserName());
				session.setAttribute("signUp", login);
				mAndV = new ModelAndView(getSuccessView(), "signUp", login);
			}
			String scheme = request.getScheme();
			String server = request.getServerName();
			int port = request.getServerPort();
			String url = scheme + "://" + server + ":" + port;
			response.sendRedirect(url+"/lxrmarketplace.html");
			return mAndV;
		}else if (WebUtils.hasSubmitParameter(request, "unsubscribe")) {
			long userId = Long.parseLong(WebUtils.findParameterValue(request, "lm_uid"));
			String email = WebUtils.findParameterValue(request, "email");
			Login unsubscribeuser = loginService.find(userId);
			if(session.getAttribute("unsubuserId")!= null){
		     session.removeAttribute("unsubuserId");		
			}
			if(session.getAttribute("usermail")!= null){  
			     session.removeAttribute("usermail");		
			}
			if(unsubscribeuser != null){
				session.setAttribute("usermail", unsubscribeuser.getUserName());
			}else{
				session.setAttribute("usermail", email);
			}
			session.setAttribute("unsubuserId", userId);
			ModelAndView mAndV = new ModelAndView("view/Unsubscribe");
			return mAndV;
		}
		return null;
	}
	
}
