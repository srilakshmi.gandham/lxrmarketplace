package lxr.marketplace.user;

import lxr.marketplace.captchaVerification.RecaptchaVerification;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.SocialChannelsUserOAuthService;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.SessionService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SignupController {

    private final static Logger LOGGER = Logger.getLogger(SignupController.class);

    @Autowired
    private SignupService signupService;
    @Autowired
    private CookieService cookieService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    @Autowired
    RecaptchaVerification recaptchaVerification;

    @Autowired
    private String supportMail;

    @Autowired
    private EmailVerfication emailVerfication;

    @Autowired
    private SocialChannelsUserOAuthService socialChannelUserOAuthService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    LoginService loginService;

    @Value("${validate.user.email}")
    private boolean validateUserEmail;

    private final String SIGNUP_RETURN_VIEW = "/views/signup";
    private final String VERIFICATION_RETURN_VIEW = "/views/emailVerification";

    @RequestMapping(value = "/signup.html")
    public String initView(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        boolean isUserLogin = Boolean.FALSE;
        if (session.getAttribute("userLoggedIn") != null) {
            isUserLogin = (boolean) session.getAttribute("userLoggedIn");
        }
        if (!isUserLogin) {
            boolean isBackToSignup = Boolean.FALSE;
            if (session.getAttribute("isBackToSignup") != null) {
                model.addAttribute("signup", new Signup());
                isBackToSignup = ((Boolean) session.getAttribute("isBackToSignup"));
            }
            if (!isBackToSignup) {
                session.removeAttribute("signUpObjToBeVerified");
                model.addAttribute("signup", new Signup());
                model.addAttribute("signUpError", Boolean.FALSE);
            } else {
                model.addAttribute("signUpError", Boolean.TRUE);
            }
        }
        return isUserLogin ? "views/lxrMarketplace" : SIGNUP_RETURN_VIEW;
    }

    @RequestMapping(value = "/signup.html", method = RequestMethod.POST)
    public String doSignup(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            Model model, @Validated Signup signup, BindingResult bindingResult) {
        Login user = (Login) session.getAttribute("user");
        String returnView = null;
        LOGGER.info("Captcha verification Started user captcha is :: " + request.getParameter("g-recaptcha-response"));
        boolean isCaptchaVeirfied = recaptchaVerification.verifyCaptcha(request.getParameter("g-recaptcha-response"));
        LOGGER.info("Captcha verification status:: " + isCaptchaVeirfied);
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Please enter all details.");
            model.addAttribute("signUpError", Boolean.TRUE);
            returnView = SIGNUP_RETURN_VIEW;
        } else if (!isCaptchaVeirfied) {
            model.addAttribute("varyfiedResponse", "Your captcha has not been verified successfully");
            model.addAttribute("signUpError", Boolean.TRUE);
            returnView = SIGNUP_RETURN_VIEW;
        } else if (signup.getName().trim().length() == 0 || signup.getEmail().trim().length() == 0
                || signup.getPassword().trim().length() == 0 || signup.getRepassword().trim().length() == 0
                || !signup.isTermsAndConditions()) {
            if (signup.getName().trim().length() == 0) {
                bindingResult.rejectValue("name", "error.name", "Please enter user name");
            }
            if (signup.getEmail().trim().length() == 0) {
                bindingResult.rejectValue("email", "error.email", "Please enter email address");
            }
            if (signup.getPassword().trim().length() == 0) {
                bindingResult.rejectValue("password", "error.password", "Please enter password");
            }
            if (signup.getRepassword().trim().length() == 0) {
                bindingResult.rejectValue("repassword", "error.repassword", "Please enter confirm password");
            }
            if (!signup.isTermsAndConditions()) {
                bindingResult.rejectValue("termsAndConditions", "error.termsAndConditions", "Please accept Terms of Service and Privacy Policy");
            }
            model.addAttribute("signUpError", Boolean.TRUE);
            returnView = SIGNUP_RETURN_VIEW;
        } else if (!signup.getPassword().equals(signup.getRepassword())) {
            model.addAttribute("error", "Password and Confirm Password should be same.");
            model.addAttribute("signUpError", Boolean.TRUE);
            returnView = SIGNUP_RETURN_VIEW;
        } else {
            Signup existingUser = signupService.caseInsensitiveFind(signup.getEmail().trim());
            LOGGER.info("The existingUser while signup:: " + existingUser);
            if (existingUser != null
                    && ((existingUser.getEmail() != null && existingUser.getPassword() != null && !existingUser.isDeleted())
                    || (existingUser.isSocialSignUp() && !existingUser.isDeleted()))) {
                /* Found account with this email and account not deleted */
                model.addAttribute("error", "An account already exists with this email address.");
                model.addAttribute("signUpError", Boolean.TRUE);
                returnView = SIGNUP_RETURN_VIEW;
            } else if (existingUser != null
                    && ((existingUser.getEmail() != null && existingUser.getPassword() != null && existingUser.isDeleted())
                    || (existingUser.isSocialSignUp() && existingUser.isDeleted()))) {
                /* Found account with this email and account deleted */
                model.addAttribute("error", "You don't have permission to login / signup with this email. To restore your account,  please contact support team at support@lxrmarketplace.com");
                model.addAttribute("signUpError", Boolean.TRUE);
                returnView = SIGNUP_RETURN_VIEW;
            } else {
                EmailValidationModel emailValidationModel = null;
                if (validateUserEmail) {
                    emailValidationModel = emailVerfication.validateAndGenerateScoreForEmail(signup.getEmail().trim());
                }
                LOGGER.info("EmailValidationModel:: " + emailValidationModel);
                if (emailValidationModel != null && !emailValidationModel.isDisposable() && emailValidationModel.isEmailVerified() && emailValidationModel.getEmailToBeVerified().contains("@gmail.com")) {
                    Object[] responseArray = processUserSignUp(request, response, session, signup, user);
                    LOGGER.info("Response to signup user with email verification code and responseArray:: " + Arrays.toString(responseArray) + ", signUpObj :: " + signup.toString());
                    if (responseArray != null && ((Boolean) responseArray[0])) {
                        session.removeAttribute("signUpObjToBeVerified");
                        try {
                            response.sendRedirect(((responseArray[1] != null) ? ((String) responseArray[1]) : getHost(request) + "/"));
                            return null;
                        } catch (IOException e) {
                            LOGGER.error("IOException in response send rediect cause:: ", e);
                        }
                    } else {
                        LOGGER.info("No response to signup user with email from signup");
                        model.addAttribute("error", "Failed to validate the email, Please try signup again.");
                        model.addAttribute("signUpError", Boolean.TRUE);
                        model.addAttribute("signup", new Signup());
                        returnView = SIGNUP_RETURN_VIEW;
                    }
                } else if (emailValidationModel != null && emailValidationModel.isDisposable()) {
                    LOGGER.info("The user email for signup is Disposable and error message:: " + ("\' " + signup.getEmail().trim().concat("\' email address is invalid, please enter a valid email address.")));
                    model.addAttribute("error", "\' " + signup.getEmail().trim().concat("\' email address is invalid, please enter a valid email address."));
                    model.addAttribute("signUpError", Boolean.TRUE);
                    returnView = SIGNUP_RETURN_VIEW;
                } else {
                    LOGGER.info("User email is failed to verify with verification score for email:: " + signup.getEmail().trim() + ", and to be veified using verification code");
                    emailValidationModel = getEmailValidationModel(signup.getName(), signup.getEmail());
                    if (emailValidationModel != null) {
                        signup.setEmailValidationCode(emailValidationModel.getVerificationCode());
                        session.setAttribute("emailValidationModelToBeVerified", emailValidationModel);
                        session.setAttribute("signUpObjToBeVerified", signup);
                        try {
                            response.sendRedirect("/emailVerification.html");
                            return null;
                        } catch (IOException e) {
                            LOGGER.error("IOException in response send rediect cause:: ", e);
                        }
                    } else {
                        model.addAttribute("error", "Failed to validate the email, Please try signup again.");
                        model.addAttribute("signUpError", Boolean.TRUE);
                        model.addAttribute("signup", new Signup());
                        returnView = SIGNUP_RETURN_VIEW;
                    }
                }
            }
        }
        return returnView != null ? returnView : SIGNUP_RETURN_VIEW;
    }

    public Object[] processUserSignUp(HttpServletRequest request, HttpServletResponse response, HttpSession session, Signup signUpObj, Login loginObj) {
        Object[] responseObj = new Object[2];
        Login loggedInUserObj = null;
        long newUserId = 0;
        Session userSession = null;
        if (session.getAttribute("userSession") != null) {
            userSession = (Session) session.getAttribute("userSession");
        } else if (userSession == null) {
            userSession = new Session();
            userSession.setStartTime(Calendar.getInstance());
        }
        Signup existingUser = null;
        if (signUpObj != null) {
            signUpObj.setPassword(encryptAndDecryptUsingDES.encrypt(signUpObj.getPassword()));
            existingUser = signupService.caseInsensitiveFind(signUpObj.getEmail());
            signUpObj.setEncrypted(Boolean.TRUE);
        }
        LOGGER.info("In processUserSignUp for signup obj loginObj :: " + (loginObj != null ? loginObj.toString() : null) + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
        if (existingUser != null) {
            if (existingUser.getEmail() != null && existingUser.getPassword() == null) {
                LOGGER.info("In processUserSignUp the email is existed and password empty loginObj :: " + (loginObj != null ? loginObj : null) + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
                // User has already acccount but password is empty
                if (signUpObj != null) {
                    signupService.updateLxrmPassword(signUpObj.getEmail(), signUpObj.getPassword());
                }
                loggedInUserObj = signupService.getUser(existingUser.getId());
                //Setting user id for session and inseting session into db
                if (loggedInUserObj != null && userSession != null) {
                    if (signUpObj != null) {
                        signUpObj.setId(loggedInUserObj.getId());
                    }
                    //After successfully signup, changing the cookie
                    cookieService.changeCookieValue(request, response, loggedInUserObj.getId());
                    userSession.setUserId(loggedInUserObj.getId());
                    userSession.setLoggedIn(Boolean.TRUE);
                    userSession.setId(sessionService.insert(userSession));
                }
            }
        } else {
            boolean islocal = Common.checkLocal(request);
            //For guest users with id as -1
            if (loginObj != null && loginObj.getId() < 0) {
                LOGGER.info("In processUserSignUp the loginObj as guest user with id as -1 loginObj :: " + loginObj + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
                //Doing signing up new user
                if (signUpObj != null) {
                    signUpObj.setActivationId(Common.generateActivationId());
                }
                islocal = (signUpObj != null && signUpObj.getEmail() != null && !signUpObj.getEmail().trim().isEmpty()) ? signUpObj.getEmail().toLowerCase().contains("netelixir") : false;
                newUserId = signupService.insertNewUser(signUpObj, islocal);
                loggedInUserObj = signupService.getUser(newUserId);
                //Setting user id into user session and inserting usersession
                if (loggedInUserObj != null && userSession != null) {
                    if (signUpObj != null) {
                        signUpObj.setId(loggedInUserObj.getId());
                    }
                    //After successfully signup, changing the cookie
                    cookieService.changeCookieValue(request, response, loggedInUserObj.getId());
                    userSession.setUserId(loggedInUserObj.getId());
                    userSession.setLoggedIn(Boolean.TRUE);
                    userSession.setId(sessionService.insert(userSession));
                }
            } else if (loginObj != null && ((loginObj.getName() != null && loginObj.getName().isEmpty()) || loginObj.getName() == null)) {
                // previous user was a guest users 
                LOGGER.info("In processUserSignUp the previous user was a guest users  loginObj :: " + loginObj + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
                if (signUpObj != null) {
                    signUpObj.setId(loginObj.getId());
                    signUpObj.setActivationId(Common.generateActivationId());
                }
                islocal = (signUpObj != null && signUpObj.getEmail() != null && !signUpObj.getEmail().trim().isEmpty()) ? signUpObj.getEmail().toLowerCase().contains("netelixir") : false;
                boolean isUserUpdated = signupService.updateGuestUser(signUpObj, islocal);
                if (isUserUpdated) {
                    loggedInUserObj = signupService.getUser(loginObj.getId());
                    if (signUpObj != null) {
                        signUpObj.setId(loggedInUserObj.getId());
                    }
                }
            } else {
                LOGGER.info("In processUserSignUp insertNewUser  loginObj :: " + (loginObj != null ? loginObj.toString() : null) + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
                //previous user is a signed up user
                //Signing up new user
                if (signUpObj != null) {
                    signUpObj.setActivationId(Common.generateActivationId());
                }
                islocal = (signUpObj != null && signUpObj.getEmail() != null && !signUpObj.getEmail().trim().isEmpty()) ? signUpObj.getEmail().toLowerCase().contains("netelixir") : false;
                newUserId = signupService.insertNewUser(signUpObj, islocal);
                loggedInUserObj = signupService.getUser(newUserId);
                if (loggedInUserObj != null && userSession != null) {
                    if (signUpObj != null) {
                        signUpObj.setId(loggedInUserObj.getId());
                    }
                    userSession.setUserId(loggedInUserObj.getId());
                    userSession.setLoggedIn(Boolean.TRUE);
                    if (userSession.getId() > 0) {
                        sessionService.updateUserId(userSession);
                    } else {
                        userSession.setId(sessionService.insert(userSession));
                    }
                    //After successfully signup, changing the cookie
                    cookieService.changeCookieValue(request, response, loggedInUserObj.getId());
                }
            }
        }

        LOGGER.info("In processUserSignUp user signup/login is comepleted  loginObj :: " + (loggedInUserObj != null ? loggedInUserObj.toString() : null) + ", and signUpObj:: " + (signUpObj != null ? signUpObj : null));
        //Redirecting to success page or thank you page 
        if ((signUpObj != null && signUpObj.getId() > 0) && (loggedInUserObj != null && loggedInUserObj.getId() > 0)) {
            signUpObj.setPassword(loggedInUserObj.getPassword());
            //Sending mail to user
            signupService.sendMail(signUpObj, getHost(request));
            responseObj[0] = Boolean.TRUE;
            session.setAttribute("user", loggedInUserObj);
            session.setAttribute("complete", signUpObj);
            session.setAttribute("userSession", userSession);
            session.setAttribute("userLoggedIn", Boolean.TRUE);
            session.setAttribute("sendMailId", loggedInUserObj.getUserName());
            session.setAttribute("guestUserSession", userSession);
            String successPageURL = null;
            if (session.getAttribute("successpage") != null && (!((String) session.getAttribute("successpage")).contains("/signup.html"))) {
                successPageURL = (String) session.getAttribute("successpage");
                LOGGER.info("IN processUserSignUp successPageURL:: " + successPageURL);
            } else if (!signupService.isUserPersonaCaptured(loggedInUserObj.getId()) || (successPageURL != null && successPageURL.endsWith("/signup.html"))) {
                LOGGER.info("IN processUserSignUp successPageURL:: " + successPageURL);
                successPageURL = getHost(request) + "/help-us.html";
            }
            if (successPageURL == null) {
                successPageURL = getHost(request) + "/";
            }
            /*
                #1: Will navigate back to tool page/ tool result page in case when user login/signup from the tool pages.
                #2: Will navigate to help us page if user login/signup from signup page and no help us profile info found.
             */
            LOGGER.info("The next page URL user should navigate to:: " + successPageURL);
            responseObj[1] = successPageURL;
        }
        return responseObj;
    }

    @RequestMapping(value = "/validate-verification-code.html", method = RequestMethod.POST)
    public String validateEmailVerificationCode(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("emailValidationCode") String emailValidationCode,
            @RequestParam("userEmailToBeVerified") String userEmailToBeVerified,
            @RequestParam("userNameToBeVerified") String userNameToBeVerified, Model model) {
        LOGGER.info("In validateEmailVerificationCode emailValidationCode:: " + emailValidationCode + ", userEmailToBeVerified:: " + userEmailToBeVerified + ", userNameToBeVerified:: " + userNameToBeVerified);
        userEmailToBeVerified = userEmailToBeVerified != null ? userEmailToBeVerified.trim() : null;
        emailValidationCode = emailValidationCode != null ? emailValidationCode.trim() : null;
        String returnView = null;
        EmailValidationModel emailValidationModel = null;
        Signup signUpObjToBeVerified = null;
        String errorMessage = null;
        if (session.getAttribute("signUpObjToBeVerified") != null) {
            signUpObjToBeVerified = (Signup) session.getAttribute("signUpObjToBeVerified");
        }
        if (session.getAttribute("emailValidationModelToBeVerified") != null) {
            emailValidationModel = (EmailValidationModel) session.getAttribute("emailValidationModelToBeVerified");
        }
        if (signUpObjToBeVerified != null) {
            if ((signUpObjToBeVerified.getEmail() != null && signUpObjToBeVerified.getEmail().trim().equals(userEmailToBeVerified))) {
                if (emailValidationModel != null
                        && (emailValidationModel.getEmailToBeVerified() != null && emailValidationModel.getEmailToBeVerified().equals(userEmailToBeVerified))
                        && (emailValidationModel.getVerificationCode() != null && emailValidationModel.getVerificationCode().equals(emailValidationCode))) {
                    Object[] responseArray = processUserSignUp(request, response, session, signUpObjToBeVerified, ((Login) session.getAttribute("user")));
                    LOGGER.info("Response to signup user with email verification code and responseArray:: " + Arrays.toString(responseArray) + ", signUpObjToBeVerified :: " + signUpObjToBeVerified.toString());
                    if (responseArray != null && ((Boolean) responseArray[0])) {
                        errorMessage = null;
                        session.removeAttribute("signUpObjToBeVerified");
                        returnView = ((responseArray[1] != null) ? ((String) responseArray[1]) : getHost(request) + "/");
                    } else {
                        errorMessage = "Failed to register you account, Please try signup again.";
                        returnView = SIGNUP_RETURN_VIEW;
                    }
                } else {
                    LOGGER.info("Invalid email verification code for signup signUpObjToBeVerified:: " + signUpObjToBeVerified + ", EmailValidationModel:: " + emailValidationModel);
                    model.addAttribute("veificationEror", "Please enter a valid verification code");
                    model.addAttribute("userEmailToBeVerified", signUpObjToBeVerified.getEmail().trim());
                    model.addAttribute("userNameToBeVerified", signUpObjToBeVerified.getName().trim());
                    returnView = VERIFICATION_RETURN_VIEW;
                }
            } else {
                LOGGER.info("Email is different/invalid founded from session to verify  :: " + signUpObjToBeVerified + ", EmailValidationModel:: " + emailValidationModel);
                model.addAttribute("veificationEror", "\'" + userEmailToBeVerified + "\' email address is invalid for verification");
                model.addAttribute("userEmailToBeVerified", signUpObjToBeVerified.getEmail().trim());
                model.addAttribute("userNameToBeVerified", signUpObjToBeVerified.getName().trim());
                returnView = VERIFICATION_RETURN_VIEW;
            }
        } else {
            LOGGER.info("No signUpObjToBeVerified found from session :: " + signUpObjToBeVerified);
            errorMessage = "Failed to register you account, Please try signup again.";
        }
        try {
            if (errorMessage == null && returnView != null && !returnView.equals(VERIFICATION_RETURN_VIEW)) {
                /*Signup Success*/
                response.sendRedirect(returnView);
                return null;
            } else if (errorMessage != null && (returnView == null || (returnView.equals(SIGNUP_RETURN_VIEW)))) {
                /*Signup Failed back to signup page*/
                session.setAttribute("isBackToSignup", Boolean.TRUE);
                response.sendRedirect("/signup.html");
                return null;
            }
        } catch (IOException e) {
            LOGGER.error("IOException in response send rediect cause:: ", e);
        }
        return VERIFICATION_RETURN_VIEW;
    }

    @RequestMapping(value = "/emailVerification.html")
    public String navigateToEmailVerification(HttpSession session, Model model) {
        boolean isUserLogin = Boolean.FALSE;
        if (session.getAttribute("userLoggedIn") != null) {
            isUserLogin = (boolean) session.getAttribute("userLoggedIn");
        }
        Signup signupObj = null;
        if (session.getAttribute("signUpObjToBeVerified") != null) {
            signupObj = (Signup) session.getAttribute("signUpObjToBeVerified");
            model.addAttribute("userEmailToBeVerified", signupObj.getEmail());
            model.addAttribute("userNameToBeVerified", signupObj.getName());
        }
        return isUserLogin ? "views/lxrMarketplace" : "/views/emailVerification";
    }

    @RequestMapping(value = "/back-to-signup.html")
    public String backToSignUpView(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        boolean isUserLogin = Boolean.FALSE;
        if (session.getAttribute("userLoggedIn") != null) {
            isUserLogin = (boolean) session.getAttribute("userLoggedIn");
        }
        if (!isUserLogin) {
            model.addAttribute("signup", new Signup());
            model.addAttribute("signUpError", Boolean.TRUE);
            try {
                session.setAttribute("isBackToSignup", Boolean.TRUE);
                response.sendRedirect("/signup.html");
                return null;
            } catch (IOException e) {
                LOGGER.error("IOException in backToSignUpView send rediect cause:: ", e);
            }
        }
        return isUserLogin ? "views/lxrMarketplace" : SIGNUP_RETURN_VIEW;
    }

    @RequestMapping(value = "/help-us.html")
    public String initViewForHelpus(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        boolean isUserLogin = Boolean.FALSE;
        if (session.getAttribute("userLoggedIn") != null) {
            isUserLogin = (boolean) session.getAttribute("userLoggedIn");
        }
        if (isUserLogin) {
            Map<Integer, Map<Integer, String>> userPersona = signupService.getUserPersonaInof();
            session.setAttribute("userDesignations", userPersona.get(1));
            session.setAttribute("howToKnowAboutLXRM", userPersona.get(2));
            if (session.getAttribute("user") != null) {
                Login lxrmUser = (Login) session.getAttribute("user");
                if (lxrmUser != null) {
                    if (lxrmUser.getUserName() != null && !lxrmUser.getUserName().isEmpty() && !lxrmUser.getUserName().equalsIgnoreCase("Email")) {
                        model.addAttribute("loginUserEmail", lxrmUser.getUserName());
                        model.addAttribute("isUserEmailAvailable", Boolean.TRUE);
                    } else {
                        model.addAttribute("isUserEmailAvailable", Boolean.FALSE);
                    }
                }
            }
        }
        return isUserLogin ? "views/helpUsToImprove" : "views/lxrMarketplace";
    }

    @RequestMapping(value = "/help-us.html", method = RequestMethod.POST)
    @ResponseBody
    public String saveHelpUsImprove(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("howToKnowAboutLXRM") int howToKnowAboutLXRM, @RequestParam("userDesignations") int userDesignations) throws IOException {
        Login user = null;
        if (session.getAttribute("user") != null) {
            user = (Login) session.getAttribute("user");
        }
        if (user != null && user.getId() > 0) {
            signupService.saveUserPersona(user.getId(), howToKnowAboutLXRM, userDesignations);
            return "success";
        } else {
            return "error";
        }

    }

    @RequestMapping(value = "/save-success-page.json", method = RequestMethod.POST)
    @ResponseBody
    public String saveSuccessPagePath(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        if (request.getParameter("successpage") != null) {
            String successpage = request.getParameter("successpage");
            String input = request.getParameter("input");
            String column = request.getParameter("column");
            String report = request.getParameter("report");
            String reportType = request.getParameter("report-type");
            String currentPage = request.getParameter("navigationPage");
            if (input != null && successpage != null) {
                successpage += "&input=" + input;
            }
            if (column != null && successpage != null) {
                successpage += "&column=" + column;
            }
            if (report != null && successpage != null) {
                if (successpage.contains("/seo-sitemap-builder-tool.html")) {
                    if (successpage.contains("/seo-sitemap-builder-tool.html?getResult=getResult")) {
                        successpage += "&report=1";
                    } else {
                        successpage += "?report=1";
                    }
                } else {
                    successpage += "&report=1";
                }
            }
            if (reportType != null && successpage != null) {
                successpage += "&report-type=" + reportType;
            }

            if (successpage != null && successpage.contains("?")) {
                successpage += "&loginRefresh=1";
            } else {
                successpage += "?loginRefresh=1";
            }
            /*To handle request specfically for inbound link checker tool to save different payment options*/
            if (successpage != null && successpage.contains("/seo-inbound-link-checker-tool.html?getResult")) {
                String optedPrice = request.getParameter("price");
                if (optedPrice != null) {
                    successpage += "&price=" + optedPrice;
                }
                String optedBackLinkCount = request.getParameter("count");
                if (optedBackLinkCount != null) {
                    successpage += "&count=" + optedBackLinkCount;
                }

            }
            if (currentPage != null && currentPage.equals("toolPage")) {
                session.setAttribute("successpage", successpage);
//            } else if (currentPage != null && currentPage.equals("homepage") && (successpage != null && successpage.contains("limit-exceeded"))) {
            } else if (currentPage != null && currentPage.equals("homepage")) {
                session.setAttribute("successpage", successpage);
            }
        }
        return "/signup.html";
    }

    @RequestMapping(value = "/thanks.html")
    public String initViewForThanks(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        return "views/thanks";
    }

    @ModelAttribute("howToKnowAboutLXRM")
    public Map<Integer, String> getHowToKnowAboutLXRM(HttpSession session) {
        Map<Integer, String> knowAboutLXRM = (Map<Integer, String>) session.getAttribute("howToKnowAboutLXRM");
        return knowAboutLXRM;
    }

    @ModelAttribute("userDesignations")
    public Map<Integer, String> getUserDesigNations(HttpSession session) {
        Map<Integer, String> userDesignations = (Map<Integer, String>) session.getAttribute("userDesignations");
        return userDesignations;
    }

    private String getHost(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }

    @RequestMapping(value = "/lxrm-facebook-login-oauth.html", method = RequestMethod.GET)
    public String handleUserLoginFromFaceBook(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        String oAuthAccessStatus = request.getParameter("error");
        String oAuthAccessCode = request.getParameter("code");
        Signup fbUserProfileSignup = null;
        String errorMessage = null;
        boolean isLoginSuccess = Boolean.FALSE;
        if (oAuthAccessCode != null && oAuthAccessStatus == null) {
            fbUserProfileSignup = socialChannelUserOAuthService.getUserSocialProfileFromFaceBook(oAuthAccessCode);
            if (fbUserProfileSignup != null) {
                LOGGER.info("fbUserProfileSignup success :: " + fbUserProfileSignup);
                Object[] responseObj = saveSocialChannelUserProfile(request, response, session, fbUserProfileSignup);
                LOGGER.info("Facebook responseObj for saving user profile:: " + Arrays.toString(responseObj));
                if (responseObj != null) {
                    if (responseObj[1] != null) {
                        isLoginSuccess = (boolean) responseObj[1];
                    }
                    if (!isLoginSuccess && (responseObj[4] != null)) {
                        errorMessage = (String) responseObj[4];
                    }
                    if (isLoginSuccess && (responseObj[3] != null)) {
                        try {
                            LOGGER.info("fbUserProfileSignup isLoginSuccess and success page:: " + responseObj[3]);
                            response.sendRedirect((String) responseObj[3]);
                            return null;
                        } catch (IOException e) {
                            LOGGER.error("IOException in handleUserLoginFromFaceBook cause:: " + e);
                        }
                    }

                }
            }
        } else if (oAuthAccessStatus != null && oAuthAccessStatus.equals("access_denied") || oAuthAccessCode == null) {
            String backPageURL = null;
            if (session.getAttribute("successpage") != null) {
                backPageURL = (String) session.getAttribute("successpage");
            }
            if (backPageURL != null) {
                try {
                    LOGGER.info("User may opted NotNow/Cancel navigating backPageURL :: " + backPageURL);
                    response.sendRedirect(backPageURL);
                    return null;
                } catch (IOException e) {
                    LOGGER.error("IOException in navigating backPageURL :: " + backPageURL);
                }
            }
        }

        if (!isLoginSuccess && errorMessage != null) {
            model.addAttribute("socialSignupError", errorMessage);
            model.addAttribute("signUpError", Boolean.FALSE);
        } else {
            LOGGER.info("Error when user login/signup with:: " + messageSource.getMessage("lxrm.social.channel.login.error", null, Locale.US));
            model.addAttribute("error", messageSource.getMessage("lxrm.social.channel.login.error", null, Locale.US));
            model.addAttribute("signUpError", Boolean.FALSE);
        }
        model.addAttribute("signup", new Signup());
        return SIGNUP_RETURN_VIEW;
    }

    @RequestMapping(value = "/lxrm-google-login-oauth.html", method = RequestMethod.GET)
    public String handleUserLoginFromGoogle(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
        String oAuthAccessStatus = request.getParameter("error");
        String oAuthAccessCode = request.getParameter("code");
        Signup gmailUserProfileSignup = null;
        String errorMessage = null;
        boolean isLoginSuccess = Boolean.FALSE;
        LOGGER.info("In handleUserLoginFromGoogle oAuthAccessStatus:: " + oAuthAccessStatus + ", oAuthAccessCode:: " + oAuthAccessCode);
        if (oAuthAccessStatus == null && oAuthAccessCode != null) {
            gmailUserProfileSignup = socialChannelUserOAuthService.getUserSocialProfileFromGoogle(oAuthAccessCode);
            if (gmailUserProfileSignup != null) {
                LOGGER.info("gmailUserProfileSignup success :: " + gmailUserProfileSignup);
                Object[] responseObj = saveSocialChannelUserProfile(request, response, session, gmailUserProfileSignup);
                if (responseObj != null) {
                    LOGGER.info("Google responseObj for saving user profile:: " + Arrays.toString(responseObj));
                    if (responseObj[1] != null) {
                        isLoginSuccess = (boolean) responseObj[1];
                    }
                    if (!isLoginSuccess && (responseObj[4] != null)) {
                        errorMessage = (String) responseObj[4];
                    }
                    if (isLoginSuccess && (responseObj[3] != null)) {
                        try {
                            response.sendRedirect((String) responseObj[3]);
                            return null;
                        } catch (IOException e) {
                            LOGGER.error("IOException in handleUserLoginFromGoogle cause:: " + e);
                        }
                    }

                }
            }
        } else if (oAuthAccessStatus != null && oAuthAccessStatus.equals("access_denied") || oAuthAccessCode == null) {
            String backPageURL = null;
            if (session.getAttribute("successpage") != null) {
                backPageURL = (String) session.getAttribute("successpage");
            }
            if (backPageURL != null) {
                try {
                    LOGGER.info("User may opted NotNow/Cancel navigating backPageURL :: " + backPageURL);
                    response.sendRedirect(backPageURL);
                    return null;
                } catch (IOException e) {
                    LOGGER.error("IOException in navigating backPageURL :: " + backPageURL);
                }
            }
        }

        if (!isLoginSuccess && errorMessage != null) {
            model.addAttribute("socialSignupError", errorMessage);
            model.addAttribute("signUpError", Boolean.FALSE);
        } else {
            LOGGER.info("Error when user login/signup with:: " + messageSource.getMessage("lxrm.social.channel.login.error", null, Locale.US));
            model.addAttribute("error", messageSource.getMessage("lxrm.social.channel.login.error", null, Locale.US));
            model.addAttribute("signUpError", Boolean.FALSE);
        }
        model.addAttribute("signup", new Signup());
        return SIGNUP_RETURN_VIEW;
    }

    public Object[] saveSocialChannelUserProfile(HttpServletRequest request, HttpServletResponse response, HttpSession session, Signup socialProfileSignup) {
        Object[] responseObj = new Object[6];
        long lxrmUserId = signupService.searchUserInMarketplace(socialProfileSignup, getHost(request));
        String successPageURL = null;
        if (lxrmUserId > 0) {
            Login loginObj = signupService.getUser(lxrmUserId);
            if (loginObj != null && !loginObj.isDeleted()) {
                if (((loginObj.getUserName() == null || loginObj.getUserName().trim().isEmpty() || loginObj.getUserName().equalsIgnoreCase("email")) && (socialProfileSignup.getEmail() != null && !socialProfileSignup.getEmail().trim().isEmpty()))) {
                    loginService.updateUserEmailAddress(lxrmUserId, socialProfileSignup.getEmail());
                    loginObj.setUserName(socialProfileSignup.getEmail());
                }
                //Setting user id into user session and inserting
                Session userSession = (Session) session.getAttribute("userSession");
                if (userSession == null) {
                    userSession = new Session();
                    userSession.setStartTime(Calendar.getInstance());
                }
                userSession.setUserId(loginObj.getId());
                userSession.setLoggedIn(Boolean.TRUE);
                if (userSession.getId() == 0) {
                    userSession.setId(sessionService.insert(userSession));
                } else {
                    sessionService.updateUserId(userSession);
                }
                session.setAttribute("userSession", userSession);
                session.setAttribute("userLoggedIn", Boolean.TRUE);
                session.setAttribute("sendMailId", loginObj.getUserName());
                //Changing cookie value
                cookieService.changeCookieValue(request, response, loginObj.getId());
                session.setAttribute("user", loginObj);
                responseObj[1] = Boolean.TRUE;
                if (session.getAttribute("successpage") != null && (!((String) session.getAttribute("successpage")).contains("/signup.html"))) {
                    successPageURL = (String) session.getAttribute("successpage");
                }
                /*
                with reference to discussion with product manger/QA team Jan 06,2019
                User persona not required for user login/signup with social channels
                else if (!signupService.isUserPersonaCaptured(lxrmUserId) || (successPageURL != null && successPageURL.endsWith("/signup.html"))) {
                    successPageURL = getHost(request) + "/help-us.html";
                }
                #1: Will navigate back to tool page/ tool result page in case when user login/signup from the tool pages.
                #2: Will navigate to help us page if user login/signup from signup page and no help us profile info found.
                 */
                LOGGER.info("The next page URL user should navigate to:: " + successPageURL + ", for social channel:: " + ((socialProfileSignup != null && socialProfileSignup.getUserSocialProfile() != null) ? socialProfileSignup.getUserSocialProfile().getSocialChannel() : null));
                if (successPageURL == null) {
                    successPageURL = getHost(request) + "/";
                }
                responseObj[3] = successPageURL;
            } else {
//                Object[] emails = new String[1];
                String email = ((loginObj != null && (loginObj.getUserName() != null && !loginObj.getUserName().isEmpty() && !loginObj.getUserName().equals("Email"))) ? loginObj.getUserName() : "with this " + ((socialProfileSignup != null && socialProfileSignup.getUserSocialProfile() != null) ? socialProfileSignup.getUserSocialProfile().getSocialChannel().toLowerCase() + " account" : "with this account"));
                /*If email is not existed then displaying the respective social channel name
                i.e 
                #1 email = "example@gmail.com"*
                #2 email = "Facebook"*/
                email = email.contains("@") ? " with this email \'" + email + "\'" : email;
                String errorMessage = "You are not allowed to login " + email + ",  If you have any queries or need support, please email us at \'support@lxrmarketplace.com\' (OR) Signup with the another email.";
                //                String errorMessage = messageSource.getMessage("lxrm.user.deleted.error", emails, Locale.US);
                responseObj[1] = Boolean.FALSE;
                responseObj[3] = null;
                responseObj[4] = errorMessage;
            }
        }
        responseObj[0] = lxrmUserId;
        return responseObj;
    }

    /*
    * Generate code
    * Send Email.
    * Save the object in session.
     */
    @RequestMapping(value = "/generate-verification-code.html", params = "request=generateCode", method = RequestMethod.POST)
    @ResponseBody
    public Object[] generateEmailVerificationCode(HttpServletRequest request, HttpServletResponse response, HttpSession httpSessionObj,
            @RequestParam("userName") String userName, @RequestParam("userEmail") String userEmail) {
        userEmail = userEmail.trim();
        Object[] responseObj = new Object[2];
        boolean sentStatus = Boolean.FALSE;
        String messageToUser = null;
        Signup signUpObj = null;
        if (httpSessionObj.getAttribute("signUpObjToBeVerified") != null) {
            signUpObj = (Signup) httpSessionObj.getAttribute("signUpObjToBeVerified");
        }
        if (userEmail != null && !userEmail.isEmpty()) {
            if (signUpObj != null && (signUpObj.getEmail() != null && signUpObj.getEmail().equals(userEmail))) {
                EmailValidationModel emailValidationModel = getEmailValidationModel(userName, userEmail);
                if (emailValidationModel != null) {
                    sentStatus = Boolean.TRUE;
                    signUpObj.setEmailValidationCode(emailValidationModel.getVerificationCode());
                    httpSessionObj.setAttribute("emailValidationModelToBeVerified", emailValidationModel);
                    httpSessionObj.setAttribute("signUpObjToBeVerified", signUpObj);
                    messageToUser = "New verification code has been sent to your email address \'" + signUpObj.getEmail() + "\' successfully";
                }
            } else {
                messageToUser = "\'" + userEmail + "\' email address is invalid to send new verification code";
            }
        } else {
            messageToUser = "\'" + userEmail + "\' email address is invalid to send new verification code";
        }

        responseObj[0] = sentStatus;
        responseObj[1] = messageToUser;
        return responseObj;
    }

    public EmailValidationModel getEmailValidationModel(String userName, String userEmail) {
        EmailValidationModel emailValidationModel = new EmailValidationModel();
        emailValidationModel.setEmailUserName(userName.trim());
        emailValidationModel.setEmailToBeVerified(userEmail.trim());
        emailValidationModel.setVerificationCode(Common.generateUserEmailVerificationCode(Common.VERIFICATION_CODE_LENGTH).trim());
        emailValidationModel.setCreatedDate(LocalDateTime.now());
        String subject = EmailBodyCreator.subjectOnEmailVerification;
        String bodyText = EmailBodyCreator.bodyForEmailVerification(emailValidationModel);
        boolean sentStatus = SendMail.sendMail(supportMail, userEmail, subject, bodyText);
        if (!sentStatus) {
            emailValidationModel = null;
        }
        LOGGER.info("In getEmailValidationModel and emailValidationModel:: " + emailValidationModel.toString() + ", and mail status:: " + sentStatus);
        return emailValidationModel;
    }

}
