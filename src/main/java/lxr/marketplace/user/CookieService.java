package lxr.marketplace.user;

import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class CookieService extends HttpServlet {

    private final Logger logger = Logger.getLogger(CookieService.class);
    private static final long serialVersionUID = 1L;
    private final String cookieName = "MarketplaceUser";
    private final String isLoggedInCookie = "isLoggedIn";
    private LoginService loginService;
    private final EncryptAndDecryptUsingDES encryptAndDecryptUsingDES;

    public CookieService() {
        super();
        encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public Cookie createCookie(HttpServletResponse response, boolean islocal, boolean dummy) throws ServletException {
        Cookie cookie = new Cookie(cookieName, cookieName);
        cookie.setComment("");
        cookie.setHttpOnly(false);
        Long newGuestUserId = -1l;
        if (dummy) {
            cookie.setValue(encryptAndDecryptUsingDES.encrypt(newGuestUserId.toString()));
        } else {
            newGuestUserId = loginService.insertGuestUser(islocal);
            cookie.setValue(encryptAndDecryptUsingDES.encrypt(newGuestUserId.toString()));
        }
        cookie.setPath("/");
        cookie.setMaxAge(60 * 60 * 24 * 365);
        response.addCookie(cookie);
        logger.debug("New Cookie and user table entry created with id = " + newGuestUserId);
        return cookie;
    }

    public Cookie getCookie(HttpServletResponse response, HttpServletRequest request) {
        Cookie cookie = null;
        HttpSession session = request.getSession(true);
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        Cookie[] allCookie = request.getCookies();
        boolean cookieFound = false;
        if (allCookie != null) {
            for (int i = 0; i < allCookie.length && !cookieFound; i++) {
                if (allCookie[i].getName().equals(cookieName)) {
                    cookie = allCookie[i];
                    cookieFound = true;
                }
            }
        }
        if (cookieFound && cookie != null) {
            long cookieId;
            try {
                cookieId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(cookie.getValue()));
            } catch (Exception e) {
                cookieId = -2;    //if cookie contains any other string then taking it as -2
            }
            if (cookieId < -1) {   //dummy cookie
                cookie = userCookieCreation(response, session, ipAddress, false);
            }
        } else {
            cookie = userCookieCreation(response, session, ipAddress, true);
        }
        return cookie;
    }

    private Cookie userCookieCreation(HttpServletResponse response, HttpSession session, String ipAddress, boolean dummy) {
        Cookie cookie = null;
        try {
            boolean islocal = false;
            WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
            if (context.getServletContext().getAttribute("localIps") != null) {
                List<String> ips = (List<String>) context.getServletContext().getAttribute("localIps");
                if (ips.contains(ipAddress)) {
                    islocal = true;
                }
            }
            cookie = createCookie(response, islocal, dummy);
        } catch (ServletException e) {
            logger.error("Cookie creation Faield due to Servlet Exception", e);
        }
        return cookie;
    }

    private Cookie userSpecificCookieCreation(HttpServletResponse response, HttpSession session, long userId) {
        Cookie cookie = new Cookie(cookieName, cookieName);
        cookie.setComment("");
        cookie.setHttpOnly(false);
        cookie.setValue(encryptAndDecryptUsingDES.encrypt(userId + ""));

        cookie.setPath("/");
        cookie.setMaxAge(60 * 60 * 24 * 365);
        response.addCookie(cookie);
        logger.debug("Adding cookie to response " + userId);
        return cookie;
    }

    public void changeCookieValue(HttpServletRequest request, HttpServletResponse response, Long val) {
        HttpSession session = request.getSession(true);
        Cookie cookie = (Cookie) session.getAttribute("marketplaceCookie");
        logger.debug("Cookie Value Changed from " + encryptAndDecryptUsingDES.decrypt(cookie.getValue()) + " to " + val);
        cookie.setValue(encryptAndDecryptUsingDES.encrypt(val.toString()));
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    public void deleteCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] allCookie = request.getCookies();
        if (allCookie != null) {
            for (Cookie allCookie1 : allCookie) {
                if (allCookie1.getName().equals(cookieName)) {
                    allCookie1.setMaxAge(0);
                    response.addCookie(allCookie1);
                }
            }
        }
    }

    public Cookie createIsLoggedInCookie(HttpServletResponse response, long userId) throws ServletException {
        Cookie cookie = new Cookie(isLoggedInCookie, isLoggedInCookie);
        cookie.setComment("");
        cookie.setHttpOnly(false);
        Long cookieId = (long) new Random().nextInt(90000) + 10000;
        cookie.setValue(encryptAndDecryptUsingDES.encrypt(cookieId.toString()));
        cookie.setPath("/");
        cookie.setMaxAge(60 * 60 * 24 * 365);
        response.addCookie(cookie);
        return cookie;
    }

    public Cookie getIsLoggedInCookie(HttpServletResponse response, HttpServletRequest request) {
        Cookie cookie = null;
        Cookie[] allCookie = request.getCookies();
        boolean cookieFound = false;
        if (allCookie != null) {
            for (int i = 0; i < allCookie.length && !cookieFound; i++) {
                if (allCookie[i].getName().equals(isLoggedInCookie)) {
                    cookie = allCookie[i];
                    cookieFound = true;
                    return cookie;
                }
            }
        }
        return null;
    }

    public void deleteIsLoggedInCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] allCookie = request.getCookies();
        if (allCookie != null) {
            for (Cookie allCookie1 : allCookie) {
                if (allCookie1.getName().equals(isLoggedInCookie)) {
                    allCookie1.setMaxAge(0);
                    response.addCookie(allCookie1);
                }
            }
        }
    }
}
