package lxr.marketplace.user;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.spiderview.SpiderViewResult;
import lxr.marketplace.util.findPhrase.PhraseStat;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class MobileUserService extends JdbcDaoSupport{
	
	public Long findUser(String email,String toolname) {
        String query = "select id from mobile_user where email = ? and mobile_tool_name = ?";
		logger.info("in findUser "+query+"email is : "+email);
		try {
			Long userId = getJdbcTemplate().query(query,new Object[] {email,toolname},new ResultSetExtractor<Long>(){
				public Long extractData(ResultSet rs){
					Long uId = null;
					try{
						if(rs.next()){
							uId= new Long(rs.getLong("id"));
						} 
   				 	}catch (SQLException e) {
   				 		e.printStackTrace();
   				 	}
   			        return uId;
   				}
			});
   		    return userId;			
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    }
		return null;        
   	}
	
	public boolean userHasCampaign(Login user){
		 String query = "select count(sc.camp_id) from((select camp_id from seo_user_campaign_map where user_id="+user.getId()+"" +
		  		" and is_guest_user = 0 )c join seo_campaign sc on c.camp_id=sc.camp_id)where sc.is_deleted=0 ";
		 int n = getJdbcTemplate().queryForInt(query);
		 if(n == 0){
			 return false;
		 }else{
			 return true;
		 }
	}
	
    public long insertInMobileUserTab(final String name,final String emailId,final Timestamp start_time,final String toolName) {

    		final String query = "insert into mobile_user(name,email,start_time,deleted,mobile_tool_name) values (?,?,?,?,?)";
	        KeyHolder keyHolder = new GeneratedKeyHolder();
	        try {
	            synchronized(this) {
	                getJdbcTemplate().update(new PreparedStatementCreator() {
	                    public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
	                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[] {"id"});
	                        statement.setString(1, name);
	                        statement.setString(2, emailId);
	                        statement.setTimestamp(3, start_time);
	                        statement.setBoolean(4, false);
	                        statement.setString(5, toolName);
	                        return statement;
	                    }

	                }, keyHolder);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return (Long) keyHolder.getKey();

    }
    
    public void addErrorToResp(String errMsg,HttpServletResponse response) {
                JSONObject jsonObject = new JSONObject();
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			jsonObject.put("error", errMsg);
			writer.print(jsonObject);
			logger.debug("in side of addErrorToResp method"+writer.toString());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			writer.flush();
			writer.close();
		}
	 }
        public void addErrorToJsonResp(int errMsg,HttpServletResponse response) {
                JSONObject jsonObject = new JSONObject();
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			jsonObject.put("error", errMsg);
			writer.print(jsonObject);
			logger.debug("in side of addErrorToResp method"+writer.toString());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			writer.flush();
			writer.close();
		}
	 }
    public void addDataToJsonResp(SpiderViewResult svresult, HttpServletResponse response) {
            PrintWriter writer = null;
            try {
                JSONObject ja = new JSONObject();
                ja.put("error", 1);
                JSONObject titleTagJson = new JSONObject();
                titleTagJson.put("title", "");
                titleTagJson.put("totalCharacters", 0);
                 titleTagJson.put("pageContentPercentage", 0);
                JSONArray titleStopWordArray = new JSONArray();
                for(int i=0;i<svresult.getTitleTag().size();i++)
                {
                    if(i == 0){
                        titleTagJson.put("title", svresult.getTitleTag().get(i));
                    }else if(i==1){
                        titleTagJson.put("totalCharacters", svresult.getTitleTag().get(i));
                    }else if(i==2){
                        titleTagJson.put("pageContentPercentage", svresult.getTitleTag().get(i));
                    }else{
                        titleStopWordArray.add(svresult.getTitleTag().get(i));
                    }
                }
                titleTagJson.put("stopWords", titleStopWordArray);
                ja.put("titleTag", titleTagJson);
                
                JSONObject metaTagJson = new JSONObject();
                JSONArray metaStopWordArray = new JSONArray();
                 metaTagJson.put("metaDescription","");
                 metaTagJson.put("totalCharacters", 0);
                 metaTagJson.put("pageContentPercentage", 0);
                for(int i=0;i<svresult.getMetaDescriptionTag().size();i++)
                {
                    if(i == 0){
                        metaTagJson.put("metaDescription", svresult.getMetaDescriptionTag().get(i));
                    }else if(i==1){
                        metaTagJson.put("totalCharacters", svresult.getMetaDescriptionTag().get(i));
                    }else if(i==2){
                        metaTagJson.put("pageContentPercentage", svresult.getMetaDescriptionTag().get(i));
                    }else{
                        titleStopWordArray.add(svresult.getMetaDescriptionTag().get(i));
                    }
                }
                metaTagJson.put("stopWords", metaStopWordArray);
                ja.put("metaDescriptionTag", metaTagJson);
                
                if(svresult.getTotalWordCount() !=0){
                    ja.put("totalWordCount", svresult.getTotalWordCount());
                }else{
                    ja.put("totalWordCount",0);
                }
                
                
                if(svresult.getTextRatio() !=0){
                    ja.put("textRatio", svresult.getTextRatio());
                }else{
                    ja.put("textRatio",0);
                }
                
                

                JSONObject pageSearchEngineJson = new JSONObject();
                pageSearchEngineJson.put("titleDescription","");
                pageSearchEngineJson.put("description","");
                pageSearchEngineJson.put("url","");
                for(int i=0;i<svresult.getPageDisplayedInSerachResults().size();i++)
                {
                    if(i == 0){
                        pageSearchEngineJson.put("titleDescription", svresult.getPageDisplayedInSerachResults().get(i));
                    }else if(i==1){
                        pageSearchEngineJson.put("description", svresult.getPageDisplayedInSerachResults().get(i));
                    }else if(i==2){
                        pageSearchEngineJson.put("url", svresult.getPageDisplayedInSerachResults().get(i));
                    }
                }
                ja.put("pageDisplayedInSerachResults", pageSearchEngineJson);
                
                
                JSONArray keyWordsAnchorJson = new JSONArray();
                
                for (Map.Entry<String, Integer> entrySet : svresult.getKeywordsFoundInAnchorTags().entrySet()) {
                    JSONObject keyWordsJson = new JSONObject();
                    keyWordsJson.put("keyword", entrySet.getKey());
                    keyWordsJson.put("value", entrySet.getValue());
                    keyWordsAnchorJson.add(keyWordsJson);
                }
                ja.put("keywordsFoundInAnchorTags", keyWordsAnchorJson);
                /*int keywordIncVal = 0;*/
                JSONArray keyWordsAltAttributesJson = new JSONArray();
                try{
                        for (Map.Entry<String, Integer> entrySet : svresult.getKeywordsFoundInAltAttributes().entrySet()) {
                        JSONObject keyWordsAltAttrObj = new JSONObject();
                            /*int limit = keywordIncVal++;*/
                                         if( svresult.getKeywordsFoundInAltAttributes().size() >= 10){
                                                keyWordsAltAttrObj.put("imgAttrName", entrySet.getKey());
                                                keyWordsAltAttrObj.put("count", entrySet.getValue());
                                                keyWordsAltAttributesJson.add(keyWordsAltAttrObj);
                                         }else if(svresult.getKeywordsFoundInAltAttributes().size() < 10){
                                                keyWordsAltAttrObj.put("imgAttrName", entrySet.getKey());
                                                keyWordsAltAttrObj.put("count", entrySet.getValue());
                                                keyWordsAltAttributesJson.add(keyWordsAltAttrObj);
                                         }
                        }
                }catch(Exception e){
                    logger.error("Exception in constructing keyWordsAltAttributesJson JSON",e);
                }
                ja.put("keywordsFoundInAltAttributes", keyWordsAltAttributesJson);
                
                
                Map<Integer, Map<String, Integer>> totLinks = svresult.getAnchorLinks();
                int uniquelinks=0;
                Iterator<Map.Entry<String, Integer>> totalurl = null;
                Map.Entry<String, Integer> totnoofurls = null ; 
                if(totLinks.size() > 0){
                    totalurl = totLinks.get(1).entrySet().iterator();
                    while(totalurl.hasNext()){
                        totnoofurls = totalurl.next();
                        if(totnoofurls != null){
                            uniquelinks=totLinks.get(2).size() +totLinks.get(3).size() + totLinks.get(4).size();
                        }
                    }
                }else{ /**/
                        JSONArray tempAnchorJson = new JSONArray();   
                        ja.put("Internal Links", tempAnchorJson);
                        ja.put("External Links", tempAnchorJson);
                        ja.put("Social Links", tempAnchorJson);
                }
                String nameOfWords = null;
                int incrVal = 0;
                  
                for(int i=2;i<= totLinks.size();i++){
                    JSONArray tempAnchorJson = new JSONArray();
                    if(i==2){
                        nameOfWords = "Internal Links";
                         incrVal = 0;
                    }
                    if(i==3){
                        nameOfWords = "External Links";
                        incrVal = 0;
                    }
                    if(i== 4){
                        /*nameOfWords = "Internal Anchor Links";*/
                        nameOfWords = "Social Links";
                        incrVal = 0;
                    }
                    Map<String, Integer> anchorLinks = totLinks.get(i);
                        
                    for (Entry<String, Integer> entrySet : anchorLinks.entrySet()) {
                            JSONObject jsonObj = new JSONObject();
                            //int limit = incrVal++;
                            if(uniquelinks >= 20){
                                /*To remove limit
                                if(limit < 5){*/
                                    jsonObj.put("link", entrySet.getKey());
                                    jsonObj.put("count", entrySet.getValue());
                                    tempAnchorJson.add(jsonObj);
                                /*}*/
                            }else if(uniquelinks < 20){
                                    jsonObj.put("link", entrySet.getKey());
                                    jsonObj.put("count", entrySet.getValue());
                                tempAnchorJson.add(jsonObj);
                            }
                        
                    }
                    ja.put(nameOfWords, tempAnchorJson);
                }
                
                List<Map<String, PhraseStat>> relevantKeywordsAndKeyphrases = svresult.getKeywordPhrses();
                /*Default Value with empty size*/
                JSONArray jArrayEmpty = new JSONArray();
                ja.put("singleKeywordPhrases", jArrayEmpty);
                ja.put("twoWordKeyphrases", jArrayEmpty);
                ja.put("threeWordKeyphrases", jArrayEmpty);
                ja.put("fourWordKeyphrases", jArrayEmpty);
                
                
                if(relevantKeywordsAndKeyphrases!=null){
                            for(int i=0; i<=relevantKeywordsAndKeyphrases.size()-1;i++){
                             if(i==0){
                                 Map<String, PhraseStat> singleKeyword = relevantKeywordsAndKeyphrases.get(i);
                                 int incVal = 0;
                                 JSONArray jArray = new JSONArray();
                                 for (Map.Entry<String, PhraseStat> entrySet : singleKeyword.entrySet()) {

                                     JSONObject jObject = new JSONObject();
                                     PhraseStat pstat = entrySet.getValue();
                                     int limit = incVal++; 
                                     if(relevantKeywordsAndKeyphrases.get(i).size() >= 5){
                                         if(limit < 5){
                                             jObject.put("key", entrySet.getKey());
                                             jObject.put("count", pstat.count);
                                             jObject.put("density",calculateKeywordDensity(pstat.count, svresult.getTotalWordCount()));
                                             jArray.add(jObject);
                                         }
                                     }else if(relevantKeywordsAndKeyphrases.size() < 5){
                                             jObject.put("key", entrySet.getKey());
                                             jObject.put("count", pstat.count);
                                             jObject.put("density",calculateKeywordDensity(pstat.count, svresult.getTotalWordCount()));
                                             jArray.add(jObject);
                                     }
                                 }
                                 ja.put("singleKeywordPhrases", jArray);
                             }
                             else{
                                 String wordsType =" ";
                                 int incVal= 0;
                                 if(i == 1){
                                     wordsType = "twoWordKeyphrases";
                                     incVal = 0;
                                 }
                                 if(i == 2){
                                    wordsType = "threeWordKeyphrases";
                                    incVal = 0;
                                 }
                                 if(i == 3){
                                     wordsType = "fourWordKeyphrases";
                                     incVal = 0;
                                 }
                                     Map<String, PhraseStat> multipleKeyword = relevantKeywordsAndKeyphrases.get(i);
                                     JSONArray jsonArray = new JSONArray();
                                     for (Map.Entry<String, PhraseStat> entrySet : multipleKeyword.entrySet()) {
                                         JSONObject jObject = new JSONObject();
                                         int limit = incVal++; 
                                         PhraseStat pstat = entrySet.getValue();
                                         if(relevantKeywordsAndKeyphrases.get(i).size() >= 5){
                                             if(limit < 5){
                                                 jObject.put("key", entrySet.getKey());
                                                 jObject.put("rating", pstat.rating);
                                                 jObject.put("count", pstat.count);
                                                 jObject.put("density",calculateKeywordDensity(pstat.count, svresult.getTotalWordCount()));
                                                 jsonArray.add(jObject);
                                             }
                                         }else if(relevantKeywordsAndKeyphrases.size() < 5){
                                                 jObject.put("key", entrySet.getKey());
                                                 jObject.put("rating", pstat.rating);
                                                 jObject.put("count", pstat.count);
                                                 jObject.put("density",calculateKeywordDensity(pstat.count, svresult.getTotalWordCount()));
                                                 jsonArray.add(jObject);
                                         }
                                     }
                                     ja.put(wordsType, jsonArray);
                                 }
                         } 
                }
                
                String keyname=null;
                Map<Integer, Elements> headingAndPhraseElements = svresult.getHeadingAndPhraseElements();
                
                JSONArray tagHeadingEmpty = new JSONArray();
                ja.put("headingAndPhraseElements", tagHeadingEmpty);
                
                if(headingAndPhraseElements!=null){
                    JSONArray tagHeadingArray = new JSONArray();
                            for (Entry<Integer, Elements> entrySet : headingAndPhraseElements.entrySet()) {
                            Integer key = entrySet.getKey();
                            JSONObject headingjson = new JSONObject();
                            Elements handpvalue = entrySet.getValue();
                            int i=entrySet.getKey();
                            keyname="h"+i+" "+"-"+" "+"tag(s)"; 
                            if(i==7) {
                                keyname="Strong - tag(s)";
                            } 
                            if(i==8){
                                keyname="b - tag(s)";
                            } 
                            if(i==9){
                              keyname="em - tag(s)";
                            }  
                            if(i==10) {
                              keyname="i - tag(s)";
                            }
                            if(i==11){
                              keyname="acronym - tag(s)";
                            }
                            if(i==12) {
                              keyname="dfn - tag(s)";
                            }
                            if(i==13){
                               keyname="abbr - tag(s)"; 
                            }
                            if(handpvalue.size()>0)
                            {
                                Pattern p = Pattern.compile( "^[\\p{Z}\\s]*$");
                                String text = "";
                                String text1 = "";
                                JSONArray textArray = new JSONArray();
                                for(Element hp:handpvalue)
                                {
                                    text = hp.text();
                                    if(text!= null){
                                        Matcher m = p.matcher(text);
                                        if(hp.hasText() && !m.find()){
                                            text = text.replaceAll("[\\p{Z}\\s]+", "");
                                            if(!text.trim().equals("")){
                                                if(text.length() > 100){
                                                    String tempText = text.substring(0,100)+"...."; 
                                                    text = tempText;
                                                }
                                            }
                                        }
                                    }
                                    if(text1.equals(""))
                                    {
                                        text1 = text;
                                    }
                                    else{
                                        text1 = text1+", "+text;
                                    }
        //                            textArray.add(text);
                                }
                                headingjson.put("tagName", keyname);
                                headingjson.put("tagInfo", text1);
                            }
                            else{
                                headingjson.put("tagName", keyname);
                                headingjson.put("tagInfo", "");
                            }
                            tagHeadingArray.add(headingjson);
                        }
                            ja.put("headingAndPhraseElements", tagHeadingArray);
                }
//                logger.info("The Json Response ---> "+ja);
                writer = response.getWriter();
                writer.print(ja);
            } catch (Exception e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                logger.error(e);
            } finally {
                writer.flush();
                writer.close();
            }
	 }

public double calculateKeywordDensity(int count,long totalwords){
            double keywordDensity;
            try{
                double tempaudienceEngaged = (double)count / totalwords * 100;
                DecimalFormat df = new DecimalFormat("#,##,###,##0.00");
                keywordDensity=Double.parseDouble(df.format(tempaudienceEngaged));
            }catch(Exception e){
                keywordDensity=0.0;
                System.out.println("Exception in calculateKeywordDensity is "+e);
            }
            return keywordDensity;
        }
}
