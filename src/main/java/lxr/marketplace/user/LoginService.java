package lxr.marketplace.user;

import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.util.Common;
import net.sf.json.JSONArray;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class LoginService extends JdbcDaoSupport {

    String query = null;

    public List<Login> find(String userName, String password, String encryptedPassword) {
        query = "select id, password, is_active, name,is_admin,domain_name, unsubscribe,is_local,act_date,privacy_policy_agreed_time,deleted,is_encrypted, is_social_signup, email from user where LOWER(email) = ? and (password = ? || password = ?)";
        try {
            List<Login> loginDetails = getJdbcTemplate().query(query, new Object[]{userName.toLowerCase(), password, encryptedPassword}, (ResultSet rs, int rowNum) -> {
                Login login = new Login();

                login.setId(rs.getLong(1));
                login.setPassword(Common.decryptedPassword(rs.getString(2)));
//                login.setPassword(rs.getString(2));
                login.setActive(rs.getBoolean(3));
                login.setName(rs.getString(4));
                login.setAdmin(rs.getBoolean(5));
                login.setDomainName(rs.getString(6));
                login.setUnsubscribed(rs.getBoolean(7));
                login.setIslocal(rs.getBoolean(8));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                login.setActivationDate(actDate);
                login.setPrivacyPolicyAgreedTime(rs.getTimestamp(10));
                login.setDeleted(rs.getBoolean(11));
                login.setEncrypted(rs.getBoolean(12));
                login.setSocialLogin(rs.getBoolean(13));
                login.setUserName(rs.getString(14));
                if (login.getId() != 0) {
                    login.setUserUsageStats(getUserUsageStats(login.getId()));
                }
                return login;
            });

            return loginDetails;
        } catch (DataAccessException e) {
            logger.error("DataAccessException on login: ", e);
        } catch (Exception e) {
            logger.error("Exception on login: ", e);
        }

        return null;
    }

    public void updateUnSubscriptionList(final List<Long> userIds) {
        query = "update user set unsubscribe=1 where id = ?";
        logger.debug("in updateUnSubscriptionList() " + userIds.size());
        getJdbcTemplate().batchUpdate(query, new org.springframework.jdbc.core.BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                logger.debug("Inside batch preparation: " + userIds.get(i));
                ps.setLong(1, (long) userIds.get(i));
            }

            @Override
            public int getBatchSize() {
                return userIds.size();
            }
        });
    }

    public Login find(long id) {
        query = "select id, password, is_active, name,email,is_admin, unsubscribe,is_local,act_date,is_encrypted, deleted, is_social_signup   from user where id = ?";
        try {
            Login login = (Login) getJdbcTemplate().queryForObject(query, new Object[]{id}, (ResultSet rs, int rowNum) -> {
                Login lg = new Login();
                lg.setId(rs.getLong(1));
                lg.setPassword(Common.decryptedPassword(rs.getString(2)));
//                lg.setPassword(rs.getString(2));
                lg.setActive(rs.getBoolean(3));
                lg.setName(rs.getString(4));
                lg.setUserName(rs.getString(5));
                lg.setAdmin(rs.getBoolean(6));
                lg.setUnsubscribed(rs.getBoolean(7));
                lg.setIslocal(rs.getBoolean(8));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                lg.setActivationDate(actDate);
                lg.setEncrypted(rs.getBoolean(10));
                lg.setDeleted(rs.getBoolean(11));
                lg.setSocialLogin(rs.getBoolean(12));
                return lg;
            });
            if (login != null && login.getId() != 0) {
                login.setUserUsageStats(getUserUsageStats(login.getId()));
            }
            return login;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public Login findUser(long id, JdbcTemplate jdbcTemplate) {
        query = "select * from user where id = " + id;

        try {
            Login login = (Login) jdbcTemplate.queryForObject(query, (final ResultSet rs, final int rowNum) -> {
                final Login lg = new Login();
                lg.setId(rs.getLong("id"));
                lg.setName(rs.getString("name"));
                lg.setUserName(rs.getString("email"));
                lg.setPassword(Common.decryptedPassword(rs.getString("password")));
                lg.setActive(rs.getBoolean("is_active"));
                lg.setAdmin(rs.getBoolean("is_admin"));
                lg.setUnsubscribed(rs.getBoolean("unsubscribe"));
                lg.setIslocal(rs.getBoolean("is_local"));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                lg.setActivationDate(actDate);
                lg.setEncrypted(rs.getBoolean("is_encrypted"));
                lg.setSocialLogin(rs.getBoolean("is_social_signup"));
                return lg;
            });
            if (login != null && login.getId() != 0) {
                login.setUserUsageStats(getUserUsageStats(login.getId()));
            }
            return login;
        } catch (Exception e) {
            logger.error(e);

        }

        return null;
    }

    public boolean findUnsubscribedUser(long userId) {
        query = "select count(user_id) from unsubscribe_reasons where user_id=" + userId;
        int n = getJdbcTemplate().queryForObject(query, Integer.class);
        return n != 0;
    }

    public List<Long> findEmailInUserTab(String email) {
        query = "select id from user where email like '%" + email + "%'";
        try {
            //Permission for guest users is 0 change it
            List<Long> userIdList = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                Long uid = rs.getLong("id");
                return uid;
            });
            return userIdList;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public Login findActiveUser(long activationId) {
        query = "select email, is_active from user where activation_id=?";
        try {
            Login login = (Login) getJdbcTemplate().queryForObject(query, new Object[]{activationId}, (ResultSet rs, int rowNum) -> {
                Login snUp = new Login();
                snUp.setUserName(rs.getString(1));
                snUp.setActive(rs.getBoolean(2));
                return snUp;
            });

            return login;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public List<Login> getUsersList() {
        query = "select * from user where is_active=? and is_admin =? and unsubscribe=? and email is not null ";
        try {
            List<Login> logins = getJdbcTemplate().query(query, new Object[]{1, 0, 0}, (ResultSet rs, int rowNum) -> {
                Login login = new Login();
                login.setId(rs.getLong("id"));
                login.setName(rs.getString("name"));
                login.setUserName(rs.getString("email"));
                login.setPassword(Common.decryptedPassword(rs.getString("password")));
                login.setActive(rs.getBoolean("is_active"));
                login.setAdmin(rs.getBoolean("is_admin"));
                login.setUnsubscribed(rs.getBoolean("unsubscribe"));
                login.setIslocal(rs.getBoolean("is_local"));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                login.setActivationDate(actDate);
                login.setEncrypted(rs.getBoolean("is_encrypted"));
                login.setSocialLogin(rs.getBoolean("is_social_signup"));
                return login;
            });
            return logins;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public List<Login> getAllUsersExceptLocalUser() {
        query = "select * from user where is_active=1 and is_admin =0 and is_local=0 and unsubscribe=0 and email is not null ";
        List<Login> logins = getUsersByQuery(query);
        return logins;
    }

    public List<Login> getUsersByQuery(String query) {

        try {
            List<Login> logins = getJdbcTemplate().query(query, (ResultSet rs, int rowNum) -> {
                Login login = new Login();
                login.setId(rs.getLong("id"));
                login.setName(rs.getString("name"));
                login.setUserName(rs.getString("email"));
                login.setPassword(Common.decryptedPassword(rs.getString("password")));
                login.setActive(rs.getBoolean("is_active"));
                login.setAdmin(rs.getBoolean("is_admin"));
                login.setUnsubscribed(rs.getBoolean("unsubscribe"));
                login.setIslocal(rs.getBoolean("is_local"));
                Calendar actDate = null;
                Timestamp actDateTime = rs.getTimestamp(9);
                if (actDateTime != null) {
                    actDate = Calendar.getInstance();
                    actDate.setTimeInMillis(actDateTime.getTime());
                }
                login.setActivationDate(actDate);
                login.setEncrypted(rs.getBoolean("is_encrypted"));
                login.setSocialLogin(rs.getBoolean("is_social_signup"));
                return login;
            });
            return logins;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public long insertGuestUser(final boolean islocal) {
        Long deletedUserId = findDeletedUser();
        if (deletedUserId != 0) {
            changeDeletedField(deletedUserId, false, islocal);
            return deletedUserId;
        } else {
            final Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            query = "insert into user (is_active, is_admin, reg_date, unsubscribe,is_local) values (?,?,?,?,?)";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            try {
                synchronized (this) {
                    getJdbcTemplate().update((java.sql.Connection con) -> {
                        PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"id"});
                        statement.setBoolean(1, true);
                        statement.setBoolean(2, false);
                        statement.setTimestamp(3, regTime);
                        statement.setBoolean(4, false);
                        statement.setBoolean(5, islocal);
                        return statement;
                    }, keyHolder);
                }
            } catch (Exception ex) {
                logger.error(ex);
            }

            return (Long) keyHolder.getKey();
        }
    }

    public long findDeletedUser() {
        query = "select min(id) from user where deleted=1 and id not in (select user_id from session group by user_id) ";
        long deletedUserId = 0;
        try {
            deletedUserId = getJdbcTemplate().queryForObject(query, Long.class);
        } catch (NullPointerException e) {
        }
        return deletedUserId;
    }

    public void changeDeletedField(long userId, boolean val, boolean islocal) {
        query = "update user set deleted = ?, reg_date = ?, act_date = ?, is_active = ?, "
                + "is_admin = ?,  unsubscribe = ?, is_local = ? where id = ?";
        Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        try {
            getJdbcTemplate().update(query, new Object[]{val, regTime, null, true, false,
                false, islocal, userId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateRegDate(long userId, Timestamp regTime) {
        query = "update user set reg_date = ? where id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{regTime, userId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateUnSubscription(long userId) {
        query = "update user set unsubscribe=1 where id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{userId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void insertIntoUnsubscribeReasonsTab(long userId) {
        final String sql = "insert into unsubscribe_reasons(user_id, unsub_reason ,unsub_date) values (?, ? , ?)";
        final Timestamp unsubDate = new Timestamp(Calendar.getInstance().getTimeInMillis());

        Object[] unsubList = new Object[3];
        unsubList[0] = userId;
        unsubList[1] = "User unsubscribes from the mail";
        unsubList[2] = unsubDate;
        try {
            getJdbcTemplate().update(sql, unsubList);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateActDate(long userId, Timestamp actTime, int dateType) {
        if (dateType == 1) {
            query = "update user set act_date = ? where id = ?";
        } else if (dateType == 2) {
            query = "update user set privacy_policy_agreed_time = ? where id = ?";
        }
        try {
            getJdbcTemplate().update(query, new Object[]{actTime, userId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateEncryptedPassword(String password, long userId) {
        String query = "update user set is_encrypted = 1, password = ? where id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{password, userId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void updateEmail(HttpServletRequest request, long userId, String email) {
        boolean islocal;
        if (Common.checkLocal(request) || email.contains("netelixir") || email.contains("NETELIXIR")) {
            islocal = true;
            query = "update user set email=?, is_local=? where id= ? ";
            try {
                getJdbcTemplate().update(query, new Object[]{email, islocal, userId});
            } catch (Exception ex) {
                logger.error(ex);
            }
        } else {
            query = "update user set email = ?  where id = ?";
            try {
                getJdbcTemplate().update(query, new Object[]{email, userId});
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
    }

    public void addInJsonforLogin(String stringData, Login loginUserObj, HttpServletResponse response) {
        JSONArray arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, stringData);
            arr.add(1, loginUserObj);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public Map<Long, String> fetchTemplatesData(HttpSession session) {

        Map template;
        String query1 = "select m.temp_name as template_name,date_format(e.sent_date ,'%d-%b-%y') as sent_date ,e.event_detail_id "
                + " as event_detail_id  from mail_templates m, event_details e where e.mail_template_id= m.id and m.category in(2,3) order by e.sent_date desc ";

        String query2 = "select m.temp_name as template_name,e.event_detail_id "
                + " as event_detail_id  from mail_templates m, event_details e where e.mail_template_id= m.id and m.category in(1,4) order by e.sent_date desc ";

        try {
            Map<Long, String> templates = new HashMap<>();
            Map<Long, String> templates1 = new HashMap<>();
            List<Map<String, Object>> b = getJdbcTemplate().queryForList(query1);
            List<Map<String, Object>> c = getJdbcTemplate().queryForList(query2);
            for (int i = 0; i < b.size(); i++) {
                template = b.get(i);
                templates.put(Long.parseLong(template.get("event_detail_id").
                        toString()), template.get("sent_date").toString() + "     "
                        + template.get("template_name").toString());
            }
            for (int i = 0; i < c.size(); i++) {
                template = c.get(i);
                templates1.put(Long.parseLong(template.get("event_detail_id").
                        toString()), template.get("template_name").toString());
            }
            if (session.getAttribute("templates") != null && session.getAttribute("templates1") != null) {
                session.removeAttribute("templates");
                session.removeAttribute("templates1");
            }
            session.setAttribute("templates", templates);
            session.setAttribute("templates1", templates1);
            return templates;
        } catch (DataAccessException | NumberFormatException e) {
            logger.error(e);
        }
        return null;
    }

    public List fetchFiltersForUsersByIp() {
        List<Map<String, Object>> localipAdressDb;
        List<String> localipAdress = new ArrayList<>();
        Map ipsDb;
        String query1 = "select * from localuser_filter where filter_option=1 and  restriction_type = 2 and is_deleted = 0";

        try {
            localipAdressDb = getJdbcTemplate().queryForList(query1);
            for (int i = 0; i < localipAdressDb.size(); i++) {
                ipsDb = localipAdressDb.get(i);
                localipAdress.add((String) ipsDb.get("filter"));
            }
            return localipAdress;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public List fetchFiltersForUsersByDomain() {

        List<Map<String, Object>> localdomainsDb;
        List<String> localdomains = new ArrayList<>();
        Map domainDb;
        String query2 = "select * from localuser_filter where filter_option=2 and restriction_type = 2";
        try {
            localdomainsDb = getJdbcTemplate().queryForList(query2);
            for (int i = 0; i < localdomainsDb.size(); i++) {
                domainDb = localdomainsDb.get(i);
                localdomains.add((String) domainDb.get("filter"));
            }
            return localdomains;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public long insertGuestUserEmailId(final long userId, final long sessionId, final String emailId) {

        query = "insert into guestuser_emails(user_id,session_id,email_id) values (?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"id"});
                    statement.setLong(1, userId);
                    statement.setLong(2, sessionId);
                    statement.setString(3, emailId);
                    return statement;
                }, keyHolder);
            }
        } catch (DataAccessException ex) {
            logger.error(ex);
        }

        return (Long) keyHolder.getKey();
    }

    /*To get user usgae stats*/
    public UserUsageStats getUserUsageStats(long lxrmUserId) {
        String sqlQuery = "select count(*) tools_used, sum(cnt) how_many_times_used, group_concat(tool_id) toolIds, id user_id "
                + "from (select sum(count) cnt,tool_id,u.id,u.name from tool_usage t "
                + "inner join session s on t.session_id=s.id "
                + "inner join user u on u.id=s.user_id and deleted=0 and u.id=" + lxrmUserId + " "
                + "inner join tool tl on t.tool_id=tl.id and tl.show_in_home = 1 AND tl.is_deleted = 0 "
                + "group by tool_id) f group by id";
        logger.info("Query to get UserUsageStats :: " + sqlQuery);
        UserUsageStats userUsageStats = null;
        try {
            userUsageStats = getJdbcTemplate().queryForObject(sqlQuery, (final ResultSet rs, final int rowNum) -> {
                final UserUsageStats usageStats = new UserUsageStats();
                usageStats.setUserId(rs.getLong("user_id"));
//                usageStats.setToolsUsedList(rs.getString("toolIds") != null ? Arrays.asList(rs.getString("toolIds").split(",")).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList()) : null);
                usageStats.setCountofToolsUsed(rs.getInt("tools_used"));
                usageStats.setCountOfWebSitesAnalysis(rs.getInt("how_many_times_used"));
                return usageStats;
            });
        } catch (DataAccessException e) {
            logger.error("Exception in getUserUsageStats cause:: " + e.getMessage());
        }
        if (userUsageStats == null) {
            userUsageStats = new UserUsageStats();
        }
        return userUsageStats;
    }
    
    /*Calling from signup service
    When email address of existing user is empty updating the email address*/
    public void updateUserEmailAddress(long userId, String email) {
        String sqlQuery = "update user set email=?, is_local=? where id= ? ";
        try {
            getJdbcTemplate().update(sqlQuery, new Object[]{email, (email.toLowerCase().contains("netelixir")), userId});
        } catch (DataAccessException ex) {
           logger.error("DataAccessException in updateUserEmailAddress cause:: ",ex);
        }

    }

}
