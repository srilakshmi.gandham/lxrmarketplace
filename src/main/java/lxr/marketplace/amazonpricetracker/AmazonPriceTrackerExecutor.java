package lxr.marketplace.amazonpricetracker;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.UserOptionService;
import lxr.marketplace.util.UserOptions;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author ramana
 */
public class AmazonPriceTrackerExecutor implements ApplicationContextAware {

    private static Logger logger = Logger.getLogger(AmazonPriceTrackerExecutor.class);
    private static AmazonPriceTrackerExecutor instance;
    private final LinkedList<Task> taskQue;
    private ApplicationContext applicationContext;
    private TaskProducer tasPro;
    private TaskConsumer tasCon;

    private AmazonPriceTrackerExecutor() {
        taskQue = new LinkedList();
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void start() {
        Thread con;
        Thread pro;

        tasPro = new TaskProducer();
        //tasPro.setApplicationContext(applicationContext);
        //tasPro.setTaskQueue(taskQue);

        tasCon = new TaskConsumer();
        //tasCon.setApplicationContext(applicationContext);
        //tasCon.setTaskQueue(taskQue);

        pro = new Thread(tasPro, "Task Producer");
        con = new Thread(tasCon, "Task Consumer");

        pro.start();
        con.start();

    }

    public static synchronized AmazonPriceTrackerExecutor getInstance() {
        if (instance == null) {
            instance = new AmazonPriceTrackerExecutor();
        }
        return instance;
    }

    public void shutDown() {
        tasPro.setTerminated(true);
        tasCon.setTerminated(true);
    }
    // - -- - - - - - - inner classes TaskProducer- - - - - - - 

    private class TaskProducer implements Runnable {

        private boolean terminated = false;
        private long timeOut = 5000;

        public void setTerminated(boolean terminated) {
            this.terminated = terminated;
        }

        public boolean isTerminated() {
            return terminated;
        }

        public void run() {
            List<Task> taskLis;
            TaskService taskService = (TaskService) applicationContext.getBean("taskService");
            try {
                while (!terminated) {
                    taskLis = taskService.getListForProcessing();
                    if (taskLis != null && taskLis.size() > 0) {
                        synchronized (taskQue) {
                            for (Task task : taskLis) {
                                taskQue.add(task);
                            }
                            taskQue.notify();
                        }
                    }
                    Thread.currentThread().sleep(timeOut);
                }
            } catch (Exception ex) {
                logger.error("TaskProducer: " + ex.getMessage());
                ex.printStackTrace();
                logger.error(ex.getStackTrace().toString());
            }
        }
    }
    // - - - -- -  - - - - - - - - 
    // - - - --  - - - - - - - TaskConsumer - - - - - - -

    private class TaskConsumer implements Runnable {

        private long timeOut;
        private ThreadPoolExecutor threadPool = null;
        private int poolSize = 2;
        private int maxPoolSize = 100;
        private long keepAliveTime = 10;
        private boolean terminated = false;
        private LinkedBlockingQueue<Runnable> thrQue = new LinkedBlockingQueue<Runnable>();

        public void setTimeOut(long timeOut) {
            this.timeOut = timeOut;
        }

        public void setTerminated(boolean terminated) {
            this.terminated = terminated;
        }

        public boolean isTerminated() {
            return terminated;
        }

        public void run() {
            try {
            	 TaskService taskService = (TaskService) applicationContext.getBean("taskService");
                threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, thrQue);
                while (!terminated) {
                    //logger.info("Inside TaskConsumer Run");
                    Task task;
                    String targetFile ="";
                    synchronized (taskQue) {
                        if (taskQue.isEmpty()) {
                            try {
                                taskQue.wait();
                            } catch (InterruptedException ex) {
                                logger.error(ex.getMessage());
                            }
                        }
                        task = taskQue.removeFirst();
                    }
                   // if (task.getMarketplacePriceTable() == null || task.getMarketplacePriceTable() == "") {
                        AmazonProductPriceAnalysierTask amzProPriAnaTask = new AmazonProductPriceAnalysierTask();
                        amzProPriAnaTask.setTask(task);
                        amzProPriAnaTask.setApplicationContext(applicationContext);
                        threadPool.execute(amzProPriAnaTask);
                        
                        JdbcTemplate jdbcTemplate = (JdbcTemplate)applicationContext.getBean("jdbcTemplate");
                        UserOptions userOptions = UserOptionService.find(5l, task.getUserId(), jdbcTemplate);
                        Date d = new Date();
                        if(userOptions!= null && userOptions.isSchedulerEnabled() && (d.getDay())+1 ==  userOptions.getScheduledDay()){
	                        Task reptask = new Task();
	                        reptask = taskService.find(task.getTaskid());
	                        Thread.sleep(20000);
							while(!reptask.getStatus().equals("Completed"))
							{		
								//logger.info("Task "+task.getTaskid()+" is not completed going to sleep for 3 secs ..........");
								try{	
									Thread.sleep(3000);
								} catch (InterruptedException e) {
			                        logger.error(e.getMessage());
			                    }
								reptask = taskService.find(task.getTaskid());//task.getTaskid());					
							}
	
							ReportGeneratorTask repGenTask = new ReportGeneratorTask();
	                        repGenTask.setTask(reptask);
	                        repGenTask.setApplicationContext(applicationContext);
	                        threadPool.execute(repGenTask);
	                        
	                        //Task task = new TaskService(jdbcTemplate).find(newtask_id);
	        				while (task.getResultFile() == null) {
	        					//logger.info("Result file for task "+task.getTaskid()+" is not created yet!! going to sleep for 2 Secs ..........");
	        					try{
	        						Thread.sleep(2000);
	        					} catch (InterruptedException e) {
				                    logger.error(e.getMessage());
				                }
	        					task = new TaskService(jdbcTemplate).find(task.getTaskid());
	        				}
	        				if (task.getResultFile() != null) {
	        					targetFile = task.getResultFile();
	        					//logger.info(targetFile + "targetFile name");
	        					Login user = new LoginService().findUser(task.getUserId(), jdbcTemplate);
	        					String toAddr = user.getUserName();
	        					String subject = "Amazon Product Report for your Products Uploaded";
	        					String bodyText = "";
	        					String name = user.getName();
	        					String fromAddrs =(String) applicationContext.getBean("teamMail");//sqlRowSet.getString("email");
	        					SendMail sendmail = new SendMail();
	        					String downloadFolder = (String) applicationContext.getBean("downloadFolder");
	        					sendmail.amazonProductReportMail(downloadFolder,targetFile,user,toAddr,fromAddrs,subject,bodyText);
	        				}
	                        
                        }
                }
            } catch (Exception ex) {
                logger.error("TaskConsumer: " + ex.getMessage());
                ex.printStackTrace();
                logger.error(ex.getStackTrace().toString());
            }
        }
    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - -
}
