package lxr.marketplace.amazonpricetracker;

import java.io.File;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.UserOptionService;
import lxr.marketplace.util.UserOptions;
import lxr.marketplace.amazonpricetracker.TaskService;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

public class AmazonPriceTrackerController extends SimpleFormController{
		private static Logger logger = Logger.getLogger(AmazonPriceTrackerController.class);
		String uploadFolder;
		String downloadFolder;
		private final String headerFileName = "X-File-Name";
		JdbcTemplate jdbcTemplate; 
		
		public static void setLogger(Logger logger) {
			AmazonPriceTrackerController.logger = logger;
		}
		public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
		       this.jdbcTemplate = jdbcTemplate;  
		}

		public AmazonPriceTrackerController() {
			setCommandClass(AmazonPriceTrackerTool.class);
			setCommandName("amazonPriceTrackerTool");
		}

		public void setUploadFolder(String uploadFolder) {
	        this.uploadFolder = uploadFolder;
	    }

		public void setDownloadFolder(String downloadFolder) {
	        this.downloadFolder = downloadFolder;
	    }

		protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
			
			HttpSession session = request.getSession();
			Login user = (Login) session.getAttribute("user");
			AmazonPriceTrackerService amazonPriceTrackerService = new AmazonPriceTrackerService();
			amazonPriceTrackerService.setJdbcTemplate(jdbcTemplate);
			AmazonPriceTrackerTool amazonPriceTrackerTool = (AmazonPriceTrackerTool) command;
			JSONArray arr = null;
			arr = new JSONArray();
			session.setAttribute("json", arr);
			String inputMethod;
			String[] products = null;
			String fileName = "";

			String domainName= "";
			String strPath ="";
			if(session.getAttribute("fileName")!=null){
				fileName = session.getAttribute("fileName").toString();
			}
			if(user!=null){
				session.setAttribute("currentTool", Common.AMAZON_PRICE_ANALYZER);
				if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
					logger.info("Uploading of file started at : " + Calendar.getInstance().getTime());	
					if(!fileName.equals("")){
						File f=new File(uploadFolder.concat(fileName));
				        if(f.exists()){
				        	f.delete();
				        	logger.info("previously uploaded file is deleted");
				        }
					}
					fileName = amazonPriceTrackerService.saveFile(uploadFolder, request, response);
					session.setAttribute("fileName",fileName);
					return null;	
				}else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
					AmazonPriceTrackerTool amazonPriceTrackerToolold = (AmazonPriceTrackerTool) session
							.getAttribute("amazonPriceTrackerTool");
					if (amazonPriceTrackerToolold != null) {
						session.removeAttribute("amazonPriceTrackerTool");
						session.setAttribute("amazonPriceTrackerTool", null);
						logger.info("Cleared the data");
					}
					session.removeAttribute("json");
	                arr = new JSONArray(); 
	                session.setAttribute("json",arr);
	  
					return null;
				}else if (WebUtils.hasSubmitParameter(request, "getResult")) {
					//Session userSession = (Session) session.getAttribute("userSession");
					//userSession.addToolUsage(5l);
				
	        		amazonPriceTrackerService.adInUserOptionTable(user, amazonPriceTrackerTool);        		
					if(WebUtils.hasSubmitParameter(request, "input")){
						inputMethod = WebUtils.findParameterValue(request, "input");
						logger.info("Input Method: "+inputMethod);
						session.setAttribute("amazonPriceTrackerTool", amazonPriceTrackerTool);
						if(inputMethod.equals("manual")){
							products = amazonPriceTrackerTool.getProductList().split("\n");						
						}else if(inputMethod.equals("fileUpload")){
							ArrayList<String> arrlist = new ArrayList<String>();
							arrlist = amazonPriceTrackerService.getProductList(fileName, uploadFolder);			
						   products = arrlist.toArray(new String[arrlist.size()]);
						     	if(products[0].contains("morecolumns")|| products[0].contains("empty")||products[0].contains("empty1")){	
								arr.add(products[0]);
								session.setAttribute("json",arr);
								ModelAndView mAndV = new ModelAndView(getFormView(),"amazonPriceTrackerTool", amazonPriceTrackerTool);
								return mAndV;								
						   }
						}
						    session.setAttribute("domain",amazonPriceTrackerTool.getDomain());
						    Session userSession = (Session) session.getAttribute("userSession");//for user reports
						    userSession.addToolUsage(5);
						    
						    products = amazonPriceTrackerService.removeDuplicates(products);
						    amazonPriceTrackerService.insertProductsIntoTable(products, user.getId());
							long task_id = amazonPriceTrackerService.addTask(user.getId(), amazonPriceTrackerTool.getDomain(), downloadFolder);	
							session.setAttribute("currenttask_id",task_id);
							List<Result> results = new ArrayList<Result>();		
		                    TaskService taskser = new TaskService(jdbcTemplate);
		                    Task task = new Task();
		        			task = taskser.find(task_id);
							while(!task.getStatus().equals("Completed")){																
								task = taskser.find(task_id);;
								Thread.sleep(20000);
								logger.info("status of task with id "+task_id+" : "+task.getStatus());
							}
					        results = amazonPriceTrackerService.getResultsFromTable(task.getTaskid());
					      /*  int prodcount =0 ;
					        for(Result i :results){
						        if(i.getRemark_id()==1){
						        	prodcount++;
						        }
					        }
					        if(prodcount == results.size()){
					        	arr.add("noproduct");
					        	session.setAttribute("json", arr);
					        	ModelAndView mAndV = new ModelAndView(getFormView(),"amazonPriceTrackerTool", amazonPriceTrackerTool);
					        	return mAndV;	
					        }	*/						
							session.setAttribute("results", results);
							ModelAndView mAndV = new ModelAndView(getSuccessView(),"amazonPriceTrackerTool", amazonPriceTrackerTool);
							return mAndV;			
						
					}else{
						ModelAndView mAndV = new ModelAndView(getFormView(),
								"amazonPriceTrackerTool", amazonPriceTrackerTool);
						return mAndV;
					}
				} else if (WebUtils.hasSubmitParameter(request, "download")) {
					logger.info("downloading report ...");
				
					Login userdwnld = (Login) session.getAttribute("user");
					Task currnetTask = (Task) session.getAttribute("currenttask");
					long newtask_id = (Long) (session.getAttribute("currenttask_id"));
					Calendar time_stamp = null;
					long user_id = 0;
					String marketPlacePriceTable = "";
					String targetFile = "";
					
					Task task = new TaskService(jdbcTemplate).find(newtask_id);
					Date date = new Date();
					if(!amazonPriceTrackerTool.isToMail() ||amazonPriceTrackerTool.getMailingDay() != date.getDay()+1){
						int poolSize = 2;
				        int maxPoolSize = 100;
				        long keepAliveTime = 10;
				        LinkedBlockingQueue<Runnable> thrQue = new LinkedBlockingQueue<Runnable>();
				        
						ThreadPoolExecutor threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, thrQue);
						ReportGeneratorTask repGenTask = new ReportGeneratorTask();
		                repGenTask.setTask(task);
		                repGenTask.setApplicationContext(getWebApplicationContext());
		                threadPool.execute(repGenTask);
						
					}
					while (task.getResultFile() == null) {
						Thread.sleep(800);
						task = new TaskService(jdbcTemplate).find(newtask_id);
					}
					if (task.getResultFile() != null) {
						targetFile = task.getResultFile();
						logger.info("report created : "+targetFile);
					}
					time_stamp = task.getcreated_time_stamp();
					user_id = task.getUserId();
					try {
						boolean flag = false;
						File file1 = new File(downloadFolder + targetFile);
						// Compress the file if the file size is mote than 2MB
						strPath = targetFile;
						response.setHeader("Cache-Control", "private, max-age=15");
						String repName = targetFile;
						long size1 = file1.length();
						if (size1 > Common.MAX_FILE_SIZE) {
							strPath = Common.compressFile(downloadFolder, targetFile, true);													
							flag = true;
						}
						if (flag) {
							response.setContentType("Application/zip");
							response.setHeader(
									"Content-disposition",
									"attachment; filename ="
											+ repName.substring(0,
													repName.lastIndexOf("."))
											+ ".zip");
						} else {
							response.setContentType("Application/x-download");
							response.setHeader("Content-disposition",
									"attachment; filename =" + repName);
						}
	
						try {
							// Get it from file system
							FileInputStream fis = new FileInputStream(new File(
									downloadFolder + strPath));
							ServletOutputStream sout = response.getOutputStream();
							byte buffer[] = new byte[fis.available()];
							fis.read(buffer, 0, fis.available());
							sout.write(buffer);
							fis.close();
							sout.flush();
							sout.close();
							logger.info("File downloaded successfully");
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (Exception ex) {
						ex.printStackTrace();	
					}
					ModelAndView mAndV = new ModelAndView(getSuccessView(), "amazonPriceTrackerTool", amazonPriceTrackerTool);
					return mAndV;
	
				} else if (WebUtils.hasSubmitParameter(request,"back")){
					 ModelAndView mAndV = new ModelAndView(getFormView(),"amazonPriceTrackerTool", amazonPriceTrackerTool);
						return mAndV;
					}else {
					ModelAndView mAndV = new ModelAndView(getFormView(), "amazonPriceTrackerTool", amazonPriceTrackerTool);
					return mAndV;
				}
			}else{
				ModelAndView mAndV = new ModelAndView("amazonpricetracker/AmazonPriceTracker","amazonPriceTrackerTool", null);
				return mAndV;		
			}
		}
		
		protected boolean isFormSubmission(HttpServletRequest request) {
			return true;
		}
		 
		 protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
				HttpSession session = request.getSession();
				AmazonPriceTrackerTool amazonPriceTrackerTool = (AmazonPriceTrackerTool) session.getAttribute("amazonPriceTrackerTool");
				if(amazonPriceTrackerTool== null){
					amazonPriceTrackerTool = new AmazonPriceTrackerTool();
					amazonPriceTrackerTool.setMailingDay(2);
					Login user = (Login) session.getAttribute("user");
					//if(user.getDomainName() != null){
					//amazonPriceTrackerTool.setDomain(user.getDomainName());
					//}
				}
				return amazonPriceTrackerTool;
		}
}
