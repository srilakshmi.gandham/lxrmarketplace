package lxr.marketplace.amazonpricetracker;

public class SellerProductPrice {
	private String seller;
    private double productPrice;
    
	public SellerProductPrice(String domain_name) {
		// TODO Auto-generated constructor stub
		seller = domain_name;
	}
	public SellerProductPrice() {
		// TODO Auto-generated constructor stub
	}
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	
    

}
