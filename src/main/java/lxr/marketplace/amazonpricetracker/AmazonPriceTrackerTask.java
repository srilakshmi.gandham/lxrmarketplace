package lxr.marketplace.amazonpricetracker;

import org.springframework.context.ApplicationContext;



public interface AmazonPriceTrackerTask extends Runnable{
    public Task getTask();
    public void setTask(Task t);
    public ApplicationContext getApplicationContext();
    public void setApplicationContext(ApplicationContext applicationContext);
}
