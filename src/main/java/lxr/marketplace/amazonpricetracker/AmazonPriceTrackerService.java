package lxr.marketplace.amazonpricetracker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.UserOptionService;
import lxr.marketplace.util.UserOptions;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.FileItemFactory;
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.springframework.jdbc.core.JdbcTemplate;

import com.Ostermiller.util.CSVParser;
import org.apache.tomcat.util.http.fileupload.IOUtils;

public class AmazonPriceTrackerService{
	private static Logger logger = Logger.getLogger(AmazonPriceTrackerService.class);

	final String headerFileName = "X-File-Name";
	@Resource(name = "jdbcTemplate")
	private static JdbcTemplate jdbcTemplate;
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}	
	
	public static JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
	public String saveFile(String realPath, HttpServletRequest request,
			HttpServletResponse response) {
		String fileName = "", newFilename = "";
		InputStream is = null;
		FileOutputStream fos = null;
		if (request.getHeader(headerFileName) != null) {
			fileName = request.getHeader(headerFileName);
			logger.info("uploaded file name: " + fileName);
			if (fileName.trim().contains(" ")) {
				fileName = fileName.replace(" ", "%");
			}
			//logger.info("after removing spaces filename" + fileName);
			int ind = fileName.lastIndexOf(".");
			newFilename = fileName.substring(0, ind);
			String ext = fileName.substring((ind + 1), fileName.length());
			newFilename += "_" + Calendar.getInstance().getTime().getTime()
					+ "." + ext;
			try {
				is = request.getInputStream();
				File f = new File(realPath + newFilename);
				fos = new FileOutputStream(f);
				IOUtils.copy(is, fos);
				response.setContentType("application/json");
				response.setStatus(response.SC_OK);
			} catch (Exception e) {
				logger.error(e.getMessage());
				response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
			} finally {
				try {
					fos.close();
					is.close();
				} catch (IOException ignored) {
				}
			}
		} else if (request.getParameter("fileName") != null) {
			fileName = request.getParameter("fileName");
			logger.info("Uploaded File Name: " + fileName);
			if (fileName.trim().contains(" ")) {
				fileName = fileName.replace(" ", "%");
			}
			//logger.info("after removing spaces filename : " + fileName);
			int ind = fileName.lastIndexOf(".");
			newFilename = fileName.substring(0, ind);
			String ext = fileName.substring((ind + 1), fileName.length());
			newFilename += "_" + Calendar.getInstance().getTime().getTime()
					+ "." + ext;

			response.setContentType("text/html");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "No-cache");
			response.setDateHeader("Expires", 0);
			final long permitedSize = 314572800;
			long fileSize;
			try {
				boolean isMultipart = ServletFileUpload
						.isMultipartContent(request);
				if (isMultipart) {
					FileItemFactory factory = new DiskFileItemFactory();
					ServletFileUpload upload = new ServletFileUpload(factory);
					List items = upload.parseRequest(request);
					for (int i = 0; i < items.size(); i++) {
						FileItem fileItem = (FileItem) items.get(i);
						if (fileItem.isFormField()) {
							String any_parameter = fileItem.getString();
						} else {
							fileName = fileItem.getName();
							fileSize = fileItem.getSize();
							if (fileSize <= permitedSize) {
								File uploadedFile = new File(realPath
										+ newFilename);
								fileItem.write(uploadedFile);
							}
							else
							{
								logger.info("file is too big");
							}
						}
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		return newFilename;
	}
	public String [] removeDuplicates(String [] inProducts){
		List<String> prodList = new ArrayList<String>(); 
		
		for(String prod : inProducts){
			prod = prod.trim();			
			if(!prod.equals("")){	
				prodList.add(prod);
			}
		}
		//HashSet <String> h = new HashSet<String>();
		Set<String> h = new HashSet<String>();
		List<String> newList = new ArrayList<String>();
       
		for(String prod : prodList){
			if(h.add(prod))
				newList.add(prod);
		}
		prodList.clear();
		prodList.addAll(newList);
		return prodList.toArray(new String [prodList.size()]);
	}
	public void insertProductsIntoTable(String[] products, long user_id){
		ProductService productService = new ProductService(jdbcTemplate);
		List<Product> prevProducts = productService.getAllProductList(user_id);
		String tempProd;
		boolean isPresent;
		boolean [] toDelete = new boolean [prevProducts.size()];

		for(String prod : products){
			prod = prod.trim();
			isPresent = false;
			for(int i = 0; i < prevProducts.size(); i++){
				Product prevProd = prevProducts.get(i);
				tempProd = prevProd.getProduct_name();
				if(tempProd.equals(prod)){
					isPresent = true;
					toDelete[i] = true;
					break;
				}
			}	
			if(!isPresent && !prod.equals("")){
				Product product = new Product();
				product.setUser_id(user_id);
				product.setProduct_name(prod);
				product.setIs_Deleted(false);
				productService.save(product);
			}
		}
		for(int i = 0; i < prevProducts.size(); i++){
			if(!toDelete[i]){
				Product prevProd = prevProducts.get(i);
				prevProd.setIs_Deleted(true);
				productService.update(prevProd);
			}else{
				Product prevProd = prevProducts.get(i);
				if(prevProd.isIs_Deleted()){
					prevProd.setIs_Deleted(false);
					productService.update(prevProd);
				}
			}
		}
	}
	public void deletePreviousResultFiles(long userId, String downloadFolder){
		TaskService taskService = new TaskService(jdbcTemplate);
		List<Task> prevTasks = taskService.prevTaskForUser(userId);
		Calendar currCal = Calendar.getInstance();
		Calendar prevCal;
		ArrayList<String> filesTobeDeleted = new ArrayList<String>();
		List<Task> tasksTobeUpdated = new ArrayList<Task>();
		for(Task prevTask : prevTasks){
			prevCal = prevTask.getUpdatedtimeStamp();
			if((currCal.getTimeInMillis() - prevCal.getTimeInMillis()) > 60*60*1000){
				if(prevTask.getResultFile() != null){
					filesTobeDeleted.add(prevTask.getResultFile());
					tasksTobeUpdated.add(prevTask);
				}
			}
		}		
		String [] fileNames = filesTobeDeleted.toArray(new String[filesTobeDeleted.size()]);
		for(Task prevTaskToUpdate : tasksTobeUpdated){
			prevTaskToUpdate.setResultFile("Deleted");
			taskService.updateResultFile(prevTaskToUpdate);
		}
		Common.deleteFiles(downloadFolder, fileNames);
		logger.info("Previous files to be deleted: " + filesTobeDeleted.toString());
	}
	
	public long addTask(long userId, String domainName, String downloadFolder){
		deletePreviousResultFiles(userId, downloadFolder);			
		Task task = new Task();
		task.setStatus("Ready");
		task.setCreated_time_stamp(Calendar.getInstance());
		task.setUpdatedtimeStamp(Calendar.getInstance());
		task.setUserId(userId);
		task.setDomain_name(domainName);
		TaskService taskService = new TaskService(jdbcTemplate);	
		long i = taskService.save(task);
		return i;
	}
	public void adInUserOptionTable(Login user, AmazonPriceTrackerTool amazonPriceTrackerTool){
		
		UserOptions prevUserOptions = new UserOptions();
		prevUserOptions = UserOptionService.find(5, user.getId(), jdbcTemplate);
		
		UserOptions userOptions = new UserOptions();
		userOptions.setUserId(user.getId());
		userOptions.setToolId(5);
		userOptions.setUserDomain(amazonPriceTrackerTool.getDomain());
		userOptions.setSchedulerEnabled(amazonPriceTrackerTool.isToMail());
		userOptions.setScheduledDay(amazonPriceTrackerTool.getMailingDay());
		if(prevUserOptions != null){
			userOptions.setUopId(prevUserOptions.getUopId());
			UserOptionService.update(userOptions, jdbcTemplate);
		}else{
			UserOptionService.save(userOptions, jdbcTemplate);
		}
	}
	public List <Result> getResultsFromTable(long id){
		logger.info("service");
		ResultService result=new ResultService(jdbcTemplate);
		return result.find(id);
	}
	
	public ArrayList<String> getProductList(String fileName, String uploadFolder) {
		  
		String filePath = uploadFolder.concat(fileName);
		ArrayList<String> list = null;
		FileInputStream fis = null;
		FileInputStream tempFis = null;
		FileReader r1 = null;
		try {
			File f = new File(filePath);
			fis = new FileInputStream(f);

			list = new ArrayList<String>();
			if (filePath.endsWith(".xls") && !(filePath.endsWith(".xlsx"))) {
				list = getListFromXls(fis);
			} else if (filePath.endsWith(".xlsx")) {
				list = getListFromXlsx(filePath);
			} else if (filePath.endsWith(".csv")) {
				list = getListFromCsv(fis);
			} else if (filePath.endsWith(".tsv")) {
				list = getListFromTsv(fis);
			}
			return list;
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public ArrayList<String> getListFromXls(FileInputStream fis) {
		ArrayList<String> list = new ArrayList<String>();
		POIFSFileSystem fs = null;
		try {
			fs = new POIFSFileSystem(fis);
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheetAt(0);
		   
				int firstRowNum=0;
				int lastRowNum=0;
				firstRowNum = sheet.getFirstRowNum();
				if(sheet.getLastRowNum() >= 19){
					lastRowNum = 19;
				}else {
					lastRowNum = sheet.getLastRowNum();
				}
				if(lastRowNum == 0){
					list.add("empty");
					return list;
				}
				//logger.info("lastRowNum"+lastRowNum);
				for (int rowIndex = firstRowNum; rowIndex <= lastRowNum; rowIndex++) {
					HSSFRow row = sheet.getRow(rowIndex);
					
					 if(row.getLastCellNum()>1){
						list.add("morecolumns");
						return list;
						
					}
					if(row != null){
						HSSFCell cell = row.getCell(0);
						
						if (cell != null ) {
							if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
								list.add(Double.toString(cell.getNumericCellValue()));
							} else if (cell.getCellType() == cell.CELL_TYPE_STRING) {
								list.add(cell.getRichStringCellValue().getString()
										.toString());
							}
						}
					}
					
				}
				return list;
			}
		 catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	
	public ArrayList<String> getListFromCsv(FileInputStream fis) throws UnsupportedEncodingException {
	//	BufferReader in = new BufferReader(new InputStream());
		ArrayList<String> list = new ArrayList<String>();
		Reader r = new InputStreamReader(fis, "ISO-8859-1");
		CSVParser csvp = new CSVParser(r);
		try {			
			String[][] values = csvp.getAllValues();
			if (values == null || values.length == 0) {
				list.add("empty");
				return list;
			}
				int len = values.length;
				if(len >= 20){
					len = 20;
				}else{
				  len = values.length;
				}
				if(values[0].length > 1){
					list.add("morecolumns");
					return list;
				}else if(values[0][0].equals(""))
				{
					list.add("empty1");
					return list;
				}				
				int firstRowNum = 0;				
				String TM = ""+(char)226+(char)132+(char)162;
				for (int i = firstRowNum; i < len; i++) {
					String dataString = values[i][0];
					dataString = dataString.replaceAll(TM, "�").replaceAll("Â®", "®").replaceAll("Â©", "©").replace((char)153,'�');
					list.add(dataString);
				}
				return list;
			}
		catch (IOException e) {
			logger.error(e.getMessage());
		}
		return list;
	}
	
	
	public ArrayList<String> getListFromXlsx(String filePath) {
		ArrayList<String> list = new ArrayList<String>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(filePath);
			XSSFSheet sheet = workbook.getSheetAt(0);
			
				int firstRowNum = 0;
				firstRowNum = sheet.getFirstRowNum();
				int lastRowNum = sheet.getLastRowNum();
				if(lastRowNum >= 19){
					lastRowNum = 19;
				}else{
				  lastRowNum = sheet.getLastRowNum();
				}
				if(lastRowNum == 0){
					list.add("empty");
					return list;
				}
												
				for (int rowIndex = firstRowNum; rowIndex <= lastRowNum; rowIndex++) {
					XSSFRow row = sheet.getRow(rowIndex);
					if(row.getLastCellNum()>1){
						list.add("morecolumns");
						return list;
						
					}
					if(row!=null){
					XSSFCell cell = row.getCell(0);
						if (cell != null) {
							if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
								list.add(Double.toString(cell.getNumericCellValue()));
							} else if (cell.getCellType() == cell.CELL_TYPE_STRING) {
								list.add(cell.getRichStringCellValue().getString()
										.toString());
							}
						}
					}
				}
				return list;
			}
		catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<String> getListFromTsv(FileInputStream fis) throws IOException {
		ArrayList<String> list = new ArrayList<String>();

		Reader r = new InputStreamReader(fis, "ISO-8859-1");
		CSVParser csvp = new CSVParser(r);
		
		try {
			String[][] values = csvp.getAllValues();
			if (values == null || values.length == 0) {
				list.add("empty");
				return list;
			}

				int length = values.length;
				if(length >= 20){
					length = 20;
				}else{
					length = values.length;
				}
				if(values[0].length > 1){
					list.add("morecolumns");
					return list;
				}else if(values[0][0].equals(""))
				{
					list.add("empty1");
					return list;
				}
				int firstRowNum = 0;
				
					//length -= 1;
				String TM = ""+(char)226+(char)132+(char)162;
				for (int i = firstRowNum; i < length; i++) {
					String dataString = values[i][0];
					String[] data = dataString.split("\t");
					String prod = data[0].replaceAll(TM, "™").replaceAll("Â®", "®").replaceAll("Â©", "©").replace((char)153,'™');
					list.add(prod);
				}
				return list;
			}
		 catch (IOException e) {
			logger.error(e.getMessage());
		}
		return list;
	}
	
	public static Map<Long, String> populateRecomendations(){
		String query = "select * from amazonPA_reco order by id asc";
		//logger.info(getJdbcTemplate()+"jdbctemplate************************");
		Map recomendation;
	       try{
				Map<Long,String> recomendations = new HashMap<Long,String>();
				List<Map<String,Object>> b =  getJdbcTemplate().queryForList(query);
				for(int i=0;i<b.size();i++){
					recomendation = b.get(i);
					recomendations.put(Long.parseLong(recomendation.get("id").toString()),recomendation.get("reco").toString());
		   }
				//System.out.println(recomendations);
	            return recomendations;
	        } catch(Exception e) {
	            e.printStackTrace();
	        }

	        return null;
			
		}
}

