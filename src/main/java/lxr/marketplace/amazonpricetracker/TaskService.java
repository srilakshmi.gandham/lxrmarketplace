package lxr.marketplace.amazonpricetracker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.core.RowMapper;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class TaskService {
	private static Logger logger = Logger.getLogger(TaskService.class);
	@Resource(name = "jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	public TaskService(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}
	public JdbcTemplate getJdbcTemplate() {
        return this.jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    

    public  Task find(long task_id) {
        String query = "select * from task where task_id = "+task_id ;
        try {
            Task task = (Task) getJdbcTemplate().queryForObject(query,
                    new RowMapper() {

                        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Task task = new Task();
                            task.setId(rs.getLong("task_id"));
                            task.setUserId(rs.getLong("user_id"));
                            task.setDomain_name(rs.getString("domain_name"));
                 
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(rs.getTimestamp("created_time_stamp").getTime());
                            task.setCreated_time_stamp(cal);
                            cal.setTimeInMillis(rs.getTimestamp("updated_time_stamp").getTime());
                            task.setUpdatedtimeStamp(cal);
                            task.setStatus(rs.getString("status"));
                            task.setResultFile(rs.getString("result_file"));
                            return task;
                        }
                    });
            return task;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }

 /*   public List<Task> getList() {
        String query = "select id, user_id, source_file, time_stamp, product_table, marketplace_price_table, target_file,status, remarks, marketplaces from task";
        try {
            List<Task> tasks = getJdbcTemplate().query(query, new RowMapper<Task>() {

                public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Task task = new Task();

                    task.setId(rs.getLong("id"));
                    task.setUserId(rs.getLong("user_id"));
                    task.setSourceFile(rs.getString("source_file"));
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(rs.getTimestamp("time_stamp").getTime());
                    task.setTimeStamp(cal);
                    task.setProductTable(rs.getString("product_table"));
                    task.setMarketplacePriceTable(rs.getString("marketplace_price_table"));
                    task.setTargetFile(rs.getString("target_file"));
                    task.setStatus(rs.getString("status"));
                    task.setRemarks(rs.getString("remarks"));
                    task.setMarketPlaces(rs.getLong("marketplaces"));

                    return task;
                }
            });
            return tasks;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Task> getUserTaskList(long userId) {
        String query = "select id, user_id, source_file, time_stamp, product_table, marketplace_price_table, target_file,status, remarks, marketplaces from task where user_id = ?  order by time_stamp desc";
        try {
            List<Task> tasks = getJdbcTemplate().query(query, new Object[]{userId}, new RowMapper<Task>() {

                public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Task task = new Task();

                    task.setId(rs.getLong("id"));
                    task.setUserId(rs.getLong("user_id"));
                    task.setSourceFile(rs.getString("source_file"));
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(rs.getTimestamp("time_stamp").getTime());
                    task.setTimeStamp(cal);
                    task.setProductTable(rs.getString("product_table"));
                    task.setMarketplacePriceTable(rs.getString("marketplace_price_table"));
                    task.setTargetFile(rs.getString("target_file"));
                    task.setStatus(rs.getString("status"));
                    task.setRemarks(rs.getString("remarks"));
                    task.setMarketPlaces(rs.getLong("marketplaces"));

                    return task;
                }
            });
            return tasks;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
*/
    public List<Task> getListForProcessing() {
        String locQuery = "update task set status = '' where status != ? and status != ? and status != ?";
        String unLocQuery = "update task set status = 'Processing', updated_time_stamp =? where status = ? ";
        String query = "select task_id, user_id, domain_name, created_time_stamp, updated_time_stamp, status, result_file "
                + " from task where status = ''";
        int rowCnt;
        try {
            synchronized (this) {
                rowCnt = getJdbcTemplate().update(locQuery, new Object[]{"Completed", "Processing", "Failed"});
                if (rowCnt > 0) {
                    List<Task> tasks = getJdbcTemplate().query(query, new RowMapper<Task>() {

                        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Task task = new Task();

                            task.setId(rs.getLong("task_id"));
                            
                            task.setUserId(rs.getLong("user_id"));
                            task.setDomain_name(rs.getString("domain_name"));
                            task.setResultFile(rs.getString("result_file"));
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(rs.getTimestamp("created_time_stamp").getTime());
                            task.setCreated_time_stamp(cal);
                            cal.setTimeInMillis(rs.getTimestamp("updated_time_stamp").getTime());
                            task.setUpdatedtimeStamp(cal);
                            task.setStatus(rs.getString("status"));
                            
                            return task;
                        }
                    });
                    rowCnt = getJdbcTemplate().update(unLocQuery, new Object[]{Calendar.getInstance(), ""});
                    return tasks;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public int save(final Task task) {
        final String query = "insert into task (user_id, domain_name, created_time_stamp, updated_time_stamp, status, result_file) values (?, ?, ?, ?, ?, ?)";
        Object[] valLis = new Object[6];
        Object obj ;
        valLis[0] = task.getUserId();
        valLis[1] = task.getDomain_name();
        
        Calendar cal = task.getcreated_time_stamp();
        Date d = cal.getTime();
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String str = "\'"+df.format(d)+"\'";
        logger.info("created task time "+str);
 
        valLis[2] = new java.sql.Timestamp(task.getcreated_time_stamp().getTimeInMillis());
        valLis[3] = new java.sql.Timestamp(task.getUpdatedtimeStamp().getTimeInMillis());
        valLis[4] = task.getStatus();
        valLis[5] = task.getResultFile();
        try {
            int i= getJdbcTemplate().update(query, valLis);
            return getJdbcTemplate().queryForInt("select task_id from task where created_time_stamp = "+str);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return 0;
    }

    public TaskService() {
		super();
	}

	public int updateStatus(Task task) {
        String query = "update task set user_id = ?, updated_time_stamp =?, status=? where task_id=?";
        Object[] valLis = new Object[4];
        valLis[0] = task.getUserId();
        valLis[1] = new java.sql.Timestamp(task.getUpdatedtimeStamp().getTimeInMillis());
        valLis[2] = task.getStatus();
        valLis[3] = task.getTaskid();
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
	public int updateResultFile(Task task) {
        String query = "update task set user_id = ?, result_file=? where task_id=?";
        Object[] valLis = new Object[3];
        valLis[0] = task.getUserId();
        valLis[1] = task.getResultFile();
        valLis[2] = task.getTaskid();
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
    public int delete(long id, String uploadFolder, String downloadFolder) {
        String query = "delete from task where id = ?";
        Task task = null;
        String name = null;

        /*task = find(id);
        if (task != null && task.getId() > 0) {
            // Deleting source file
            name = task.getSourceFile();
            if (name != null && !"".equals(name.trim())) {
                name = uploadFolder + name;
                Commons.deleteFile(name);
            }

            // Deleting product table
            name = task.getProductTable();
            if (name != null && !"".equals(name.trim())) {
                DBUtil.dropTable(name, jdbcTemplate);
            }

            // Deleting report table
            name = task.getMarketplacePriceTable();
            if (name != null && !"".equals(name.trim())) {
                DBUtil.dropTable(name, jdbcTemplate);
            }

            // Deleting target file
            name = task.getTargetFile();
            if (name != null && !"".equals(name.trim())) {
                name = downloadFolder + name;
                Commons.deleteFile(name);
            }

            try {
                return getJdbcTemplate().update(query, new Object[]{id});
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }*/
        return 0;
    }
    public List<Task> prevTaskForUser(long userId) {
        String query = "select task_id, user_id, domain_name, created_time_stamp, updated_time_stamp, status, result_file "
                + " from task where user_id = " + userId +" and result_file != \'Deleted\'";
        try {
            synchronized (this) {
                    List<Task> tasks = getJdbcTemplate().query(query, new RowMapper<Task>() {

                        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Task task = new Task();

                            task.setId(rs.getLong("task_id"));                           
                            task.setUserId(rs.getLong("user_id"));
                            task.setDomain_name(rs.getString("domain_name"));
                            task.setResultFile(rs.getString("result_file"));
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(rs.getTimestamp("created_time_stamp").getTime());
                            task.setCreated_time_stamp(cal);
                            cal.setTimeInMillis(rs.getTimestamp("updated_time_stamp").getTime());
                            task.setUpdatedtimeStamp(cal);
                            task.setStatus(rs.getString("status"));
                            
                            return task;
                        }
                    });
                    return tasks;
                }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
