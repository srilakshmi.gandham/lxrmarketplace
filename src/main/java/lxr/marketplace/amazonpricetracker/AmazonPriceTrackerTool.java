package lxr.marketplace.amazonpricetracker;

import org.apache.log4j.Logger;

public class AmazonPriceTrackerTool {
	private static Logger logger = Logger.getLogger(AmazonPriceTrackerTool.class);
	String productList;
	String uploadFileName;
	String domain;
	//String newdomain;
	boolean toMail;
	int mailingDay;

	
	/*public String getNewdomain() {
		return newdomain;
	}

	public void setNewdomain(String newdomain) {
		this.newdomain = newdomain;
	}
    */
	public int getMailingDay() {
		return mailingDay;
	}

	public void setMailingDay(int mailingDay) {
		this.mailingDay = mailingDay;
	}

	public boolean isToMail() {
		return toMail;
	}

	public void setToMail(boolean toMail) {
		this.toMail = toMail;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getProductList() {
		return (productList==null)?"":productList;
	}

	public void setProductList(String productList) {
		String TM = ""+(char)226+(char)132+(char)162;
		productList = productList.replaceAll(TM, "™").replaceAll("Â®", "®").replaceAll("Â©", "©").replace((char)153,'™');
		this.productList = productList;
	}

}
