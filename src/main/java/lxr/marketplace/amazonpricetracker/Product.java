package lxr.marketplace.amazonpricetracker;

public class Product {
	 	private long product_id;
	 	private long user_id;
	    private String product_name;
	    private boolean is_Deleted;
	    
		public long getProduct_id() {
			return product_id;
		}
		public void setProduct_id(long product_id) {
			this.product_id = product_id;
		}
		public long getUser_id() {
			return user_id;
		}
		public void setUser_id(long user_id) {
			this.user_id = user_id;
		}
		public String getProduct_name() {
			return product_name;
		}
		public void setProduct_name(String product_name) {
			this.product_name = product_name;
		}
		public boolean isIs_Deleted() {
			return is_Deleted;
		}
		public void setIs_Deleted(boolean is_Deleted) {
			this.is_Deleted = is_Deleted;
		}
	    
}
