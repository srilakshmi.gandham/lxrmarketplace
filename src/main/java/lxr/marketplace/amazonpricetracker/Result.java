package lxr.marketplace.amazonpricetracker;

import java.util.Calendar;

public class Result {
	private long result_id;
	private long user_id;
	private long task_id;
	private long product_id;
	private String product_name;
	private double client_price;
	private double competitor_price;
	private String competitor_name;
	private int remark_id;
	private Calendar result_date;
	
	public long getResult_id() {
		return result_id;
	}
	public void setResult_id(long result_id) {
		this.result_id = result_id;
	}
	public long getTask_id() {
		return task_id;
	}
	public void setTask_id(long task_id) {
		this.task_id = task_id;
	}
	public Calendar getResult_date() {
		return result_date;
	}
	public void setResult_date(Calendar result_date) {
		this.result_date = result_date;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public int getRemark_id() {
		return remark_id;
	}
	public void setRemark_id(int remark_id) {
		this.remark_id = remark_id;
	}
	public long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public double getClient_price() {
		return client_price;
	}
	public void setClient_price(double client_price) {
		this.client_price = client_price;
	}
	public double getCompetitor_price() {
		return competitor_price;
	}
	public void setCompetitor_price(double competitor_price) {
		this.competitor_price = competitor_price;
	}
	public String getCompetitor_name() {
		return competitor_name;
	}
	public void setCompetitor_name(String competitor_name) {
		this.competitor_name = competitor_name;
	}
	

}
