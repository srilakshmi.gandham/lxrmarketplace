package lxr.marketplace.amazonpricetracker;

public class AmazonProductPrice {
	
	Product product;
    //Marketplace marketplace;
    SellerProductPrice client;
    SellerProductPrice competitor;    
   // private String url;
    
   public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
    
   /* public Marketplace getMarketplace() {
        return marketplace;
    }
    public void setMarketplace(Marketplace marketplace) {
        this.marketplace = marketplace;
    }*/
    
    public SellerProductPrice getClient() {
        return client;
    }
    public void setClient(SellerProductPrice client) {
        this.client = client;
    }

    public SellerProductPrice getCompetitor() {
        return competitor;
    }
    public void setCompetitor(SellerProductPrice competitor) {
        this.competitor = competitor;
    }
    
   /* public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }*/

}
