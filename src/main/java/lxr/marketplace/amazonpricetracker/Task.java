package lxr.marketplace.amazonpricetracker;

import java.io.Serializable;
import java.util.Calendar;

public class Task implements Serializable{
	
	    private long taskid;
	    private long userId;
	    private String domain_name;
	    private String resultFile;
	    private Calendar created_time_stamp;
	    private Calendar updatedtimeStamp;
	    private String status;
		public long getId() {
			return taskid;
		}
		public void setId(long taskid) {
			this.taskid = taskid;
		}
		public long getTaskid() {
			return taskid;
		}
		public Calendar getCreated_time_stamp() {
			return created_time_stamp;
		}
		public long getUserId() {
			return userId;
		}
		public void setUserId(long userId) {
			this.userId = userId;
		}
		public String getDomain_name() {
			return domain_name;
		}
		public void setDomain_name(String domain_name) {
			this.domain_name = domain_name;
		}
		public String getResultFile() {
			return resultFile;
		}
		public void setResultFile(String resultFile) {
			this.resultFile = resultFile;
		}
		public Calendar getcreated_time_stamp() {
			return created_time_stamp;
		}
		public void setCreated_time_stamp(Calendar created_time_stamp) {
			this.created_time_stamp = created_time_stamp;
		}
		public Calendar getUpdatedtimeStamp() {
			return updatedtimeStamp;
		}
		public void setUpdatedtimeStamp(Calendar updatedtimeStamp) {
			this.updatedtimeStamp = updatedtimeStamp;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
	    
	    
	    

}
