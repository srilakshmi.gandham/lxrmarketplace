package lxr.marketplace.amazonpricetracker;


public interface AmazonPriceTracker extends Runnable, AmazonPriceTrackerTask{
    public AmazonProductPrice getAmazonProductPrice();
    public void setAmazonProductPrice(AmazonProductPrice amPrdPrice);
	   
}

