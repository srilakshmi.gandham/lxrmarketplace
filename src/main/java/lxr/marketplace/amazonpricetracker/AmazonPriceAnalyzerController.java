package lxr.marketplace.amazonpricetracker;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ToolService;
import lxr.marketplace.util.UserOptionService;
import lxr.marketplace.util.UserOptions;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class AmazonPriceAnalyzerController extends SimpleFormController{
		private static Logger logger = Logger.getLogger(AmazonPriceAnalyzerController.class);
		String uploadFolder;
		String downloadFolder;
		private final String headerFileName = "X-File-Name";
		JdbcTemplate jdbcTemplate; 
		EmailTemplateService emailTemplateService;
		ToolService  toolService;
		LoginService loginService;
		TaskService taskService;
		
		public void setTaskService(TaskService taskService) {
			this.taskService = taskService;
		}
		public ToolService getToolService() {
			return toolService;
		}
		public void setToolService(ToolService toolService){
			this.toolService = toolService;
		}
		public EmailTemplateService getEmailTemplateService() {
			return emailTemplateService;
		}
		public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
			this.emailTemplateService = emailTemplateService;
		}
		public static void setLogger(Logger logger) {
			AmazonPriceAnalyzerController.logger = logger;
		}
		public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
		       this.jdbcTemplate = jdbcTemplate;  
		}

		public AmazonPriceAnalyzerController() {
			setCommandClass(AmazonPriceTrackerTool.class);
			setCommandName("amazonPriceAnalyzerTool");
		}
		public void setLoginService(LoginService loginService) {
			this.loginService = loginService;
		}
		public void setUploadFolder(String uploadFolder) {
	        this.uploadFolder = uploadFolder;
	    }

		public void setDownloadFolder(String downloadFolder) {
	        this.downloadFolder = downloadFolder;
	    }

		protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
			
			HttpSession session = request.getSession();
			Login user = (Login) session.getAttribute("user");
			boolean userLoggedIn = (Boolean)session.getAttribute("userLoggedIn");
			AmazonPriceTrackerService amazonPriceTrackerService = new AmazonPriceTrackerService();
			amazonPriceTrackerService.setJdbcTemplate(jdbcTemplate);
			AmazonPriceTrackerTool amazonPriceAnalyzerTool = (AmazonPriceTrackerTool) command;
			JSONArray arr = null;
			arr = new JSONArray();
			session.setAttribute("json", arr);
			String inputMethod;
			String[] products = null;
			String fileName = "";

			String domainName= "";
//			if(true){
//			String url = request.getScheme() + "://" + request.getServerName()
//			+ ":" + request.getServerPort();
//			response.sendRedirect(url + "/lxrmarketplace.html");
//			return null;
//			}
			if(session.getAttribute("fileName")!=null){
				fileName = session.getAttribute("fileName").toString();
			}
			UserOptions userOptions = UserOptionService.find(5l, user.getId(), jdbcTemplate);
			if(userLoggedIn || !userLoggedIn && 
					(!user.isActive() || (user.isActive() && 
							(userOptions == null || !userOptions.isSchedulerEnabled())))){
				session.setAttribute("currentTool", Common.AMAZON_PRICE_ANALYZER);
				if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
					logger.info("Uploading of file started at : " + Calendar.getInstance().getTime());	
					if(!fileName.equals("")){
						File f=new File(uploadFolder.concat(fileName));
				        if(f.exists()){
				        	f.delete();
				        	logger.info("previously uploaded file is deleted");
				        }
					}
					fileName = amazonPriceTrackerService.saveFile(uploadFolder, request, response);
					session.setAttribute("fileName",fileName);
					return null;	
				}else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
					AmazonPriceTrackerTool amazonPriceAnalyzerToolold = (AmazonPriceTrackerTool) session
							.getAttribute("amazonPriceAnalyzerTool");
					if (amazonPriceAnalyzerToolold != null) {
						session.removeAttribute("amazonPriceAnalyzerTool");
						session.setAttribute("amazonPriceAnalyzerTool", null);
						logger.info("Cleared the data");
					}
					session.removeAttribute("json");
	                arr = new JSONArray(); 
	                session.setAttribute("json",arr);
	  
					return null;
				}else if (WebUtils.hasSubmitParameter(request, "getResult")) {
					if(WebUtils.hasSubmitParameter(request, "loginRefresh")){
						ModelAndView mAndV = new ModelAndView(getSuccessView(),"amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
						return mAndV;
					}
					if(userLoggedIn){
						amazonPriceTrackerService.adInUserOptionTable(user, amazonPriceAnalyzerTool); 
					}
					if(WebUtils.hasSubmitParameter(request, "input")){
						inputMethod = WebUtils.findParameterValue(request, "input");
						logger.info("Input Method: "+inputMethod);
						session.setAttribute("amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
						if(inputMethod.equals("manual")){
							products = amazonPriceAnalyzerTool.getProductList().split("\n");						
						}else if(inputMethod.equals("fileUpload")){
							ArrayList<String> arrlist = new ArrayList<String>();
							arrlist = amazonPriceTrackerService.getProductList(fileName, uploadFolder);			
						   products = arrlist.toArray(new String[arrlist.size()]);
						     	if(products[0].contains("morecolumns")|| products[0].contains("empty")||products[0].contains("empty1")){	
								arr.add(products[0]);
								session.setAttribute("json",arr);
								ModelAndView mAndV = new ModelAndView(getFormView(),"amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
								return mAndV;								
						   }
						}
						    session.setAttribute("domain",amazonPriceAnalyzerTool.getDomain());
						    Session userSession = (Session) session.getAttribute("userSession");//for user reports
						    Common.modifyDummyUserInToolorVideoUsage(request, response);
						    userSession.addToolUsage(5);
						    
						    products = amazonPriceTrackerService.removeDuplicates(products);
						    amazonPriceTrackerService.insertProductsIntoTable(products, user.getId());
							long task_id = amazonPriceTrackerService.addTask(user.getId(), amazonPriceAnalyzerTool.getDomain(), downloadFolder);	
							session.setAttribute("currenttask_id",task_id);
							List<Result> results = new ArrayList<Result>();		
		                    Task task = new Task();

		                    int prodNumber = products.length;
		                    Thread.sleep(prodNumber * 5000);
		        			task = taskService.find(task_id);
							while(!task.getStatus().equals("Completed")){	
								//logger.info("Task "+task_id+" is not completed going to sleep ..........");
								try{
									Thread.sleep((prodNumber > 2 ? prodNumber--:2)*1000);
								} catch (InterruptedException e) {
			                        logger.error(e.getMessage());
			                    }
								task = taskService.find(task_id);							
							}
					        results = amazonPriceTrackerService.getResultsFromTable(task.getTaskid());		
					        for(Result i :results){
					        	i.setProduct_name(i.getProduct_name().replaceAll("™", "&trade;") );
					        }
							session.setAttribute("results", results);
							ModelAndView mAndV = new ModelAndView(getSuccessView(),"amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
							return mAndV;			
						
					}else{
						ModelAndView mAndV = new ModelAndView(getFormView(),
								"amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
						return mAndV;
					}
				}
				else if (WebUtils.hasSubmitParameter(request, "download")) {
					logger.info("downloading report ...");
				    long newtask_id = (Long) (session.getAttribute("currenttask_id"));
					String targetFile = "";
					Task task = taskService.find(newtask_id);
					Date date = new Date();
					if(!amazonPriceAnalyzerTool.isToMail() ||amazonPriceAnalyzerTool.getMailingDay() != date.getDay()+1){
						int poolSize = 2;
				        int maxPoolSize = 100;
				        long keepAliveTime = 10;
				        LinkedBlockingQueue<Runnable> thrQue = new LinkedBlockingQueue<Runnable>();
				        
						ThreadPoolExecutor threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, thrQue);
						ReportGeneratorTask repGenTask = new ReportGeneratorTask();
		                repGenTask.setTask(task);
		                repGenTask.setApplicationContext(getWebApplicationContext());
		                threadPool.execute(repGenTask);
						
					}
					while (task.getResultFile() == null) {
						logger.info("Result file for "+task.getTaskid()+" is not created yet!! going to sleep for 1 Secs ........");
						try{
							Thread.sleep(1000);
						} catch (InterruptedException e) {
	                        logger.error(e.getMessage());
	                    }
						task = taskService.find(newtask_id);
					}
					if (task.getResultFile() != null) {
						targetFile = task.getResultFile();
						logger.info("report created : "+targetFile);
					}
					try {
						String fileType="";
						String downParam = request.getParameter("download");
						if(downParam.equalsIgnoreCase("'download'")){
							Common.downloadReport(response, downloadFolder,targetFile,fileType);
						}else if(downParam.equalsIgnoreCase("'sendmail'")){
							logger.info("Sending report through mail");
							String toAddrs = request.getParameter("email").toString();
							String toolName = "Amazon Price Analyzer";//TODO: get tool name from database
							Common.sendReportThroughMail(request, downloadFolder, targetFile, toAddrs, toolName);
							return null;
						}
					
					} catch (Exception ex) {
						ex.printStackTrace();	
					}
					ModelAndView mAndV = new ModelAndView(getSuccessView(), "amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
					return mAndV;
				    
				} else if (WebUtils.hasSubmitParameter(request,"back")){
					 ModelAndView mAndV = new ModelAndView(getFormView(),"amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
						return mAndV;
					}else {
					ModelAndView mAndV = new ModelAndView(getFormView(), "amazonPriceAnalyzerTool", amazonPriceAnalyzerTool);
					return mAndV;
				}
			}else{
				ModelAndView mAndV = new ModelAndView("amazonpriceanalyzer/AmazonPriceAnalyzer","amazonPriceAnalyzerTool", null);
				mAndV.addObject("alert","Please login to use this tool.");
				return mAndV;		
			}
		}
		
		protected boolean isFormSubmission(HttpServletRequest request) {
			return true;
		}
		 
		 protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
				HttpSession session = request.getSession();
				AmazonPriceTrackerTool amazonPriceAnalyzerTool = (AmazonPriceTrackerTool) session.getAttribute("amazonPriceAnalyzerTool");
				if(amazonPriceAnalyzerTool== null){
					amazonPriceAnalyzerTool= new AmazonPriceTrackerTool();
					amazonPriceAnalyzerTool.setMailingDay(2);
					//Login user = (Login) session.getAttribute("user");
					//if(user.getDomainName() != null){
					//amazonPriceTrackerTool.setDomain(user.getDomainName());
					//}
				}
				return amazonPriceAnalyzerTool;
		}
}
