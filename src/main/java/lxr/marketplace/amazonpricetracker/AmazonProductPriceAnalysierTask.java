package lxr.marketplace.amazonpricetracker;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;

public class AmazonProductPriceAnalysierTask implements AmazonPriceTrackerTask {
	  private static Logger logger = Logger.getLogger(AmazonProductPriceAnalysierTask.class);
	    Task task;
	    ApplicationContext applicationContext;
	 //   AmazonProductPrice amazonProductPrice;
	    private long timeOut;
	    private ThreadPoolExecutor amaThreadPool = null;
	    private LinkedBlockingQueue<Runnable> amaThrQue = new LinkedBlockingQueue<Runnable>();
	    private int poolSize = 1; //50;
	    private int maxPoolSize = 1; //100;
	    private long keepAliveTime = 360;
	    private boolean terminated = false;

	    public void setTimeOut(long timeOut) {
	        this.timeOut = timeOut;
	    }

	    public void setTerminated(boolean terminated) {
	        this.terminated = terminated;
	    }

	    public boolean isTerminated() {
	        return terminated;
	    }

	    public Task getTask() {
	        return task;
	    }

	    public void setTask(Task task) {
	        this.task = task;
	    }

	    public AmazonProductPrice getMarketplaceProductPrice() {
	        throw new UnsupportedOperationException("Not supported yet.");
	    }

	    ///public void setAmazonProductPrice(AmazonProductPrice amazonProductPrice) {
	    //    this.amazonProductPrice = amazonProductPrice;
	   // }

	    public ApplicationContext getApplicationContext() {
	        return applicationContext;
	    }

	    public void setApplicationContext(ApplicationContext applicationContext) {
	        this.applicationContext = applicationContext;
	    }

	    public void run() {
	        SendMail sendMail = null;
	        LoginService loginService = null;
	        try {
	            logger.info("**********Crawling products started**************");
	            AmazonPriceTrackerService amptSer = (AmazonPriceTrackerService)applicationContext.getBean("amazonPriceTrackerService");
	            
	            ProductService proSer = (ProductService) applicationContext.getBean("productService");
	            List<Product> products = proSer.getProductList(task.getUserId());

	            JdbcTemplate jdbcTemplate = (JdbcTemplate) applicationContext.getBean("jdbcTemplate");

	           if (products != null || products.size() > 0) {
	              amaThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, amaThrQue);
	                 for (Product product : products) {
	                	 AmazonPriceTracker amazonPrzTracker = null;

	                	 AmazonProductPrice amPrdPrice = new AmazonProductPrice();
	                	 amPrdPrice.setProduct(product);
	                     amPrdPrice.setClient(new SellerProductPrice(task.getDomain_name()));
	                     amazonPrzTracker = new AmazonProductPriceTracker();
	                      
	                     amazonPrzTracker.setTask(task);
	                     amazonPrzTracker.setApplicationContext(applicationContext);
	                     amazonPrzTracker.setAmazonProductPrice(amPrdPrice);
	                       amaThreadPool.execute(amazonPrzTracker);
	                 }
	               
	                amaThreadPool.shutdown();
	                int prodNumber = products.size();
	                Thread.sleep(prodNumber * 5000);
	                while (amaThreadPool.isTerminated() != true ){
	                    	//logger.info("Threadpool for task "+task.getId()+" is not terminated, going to sleep ........");
	                    	try{
	                    		Thread.sleep((prodNumber > 2 ? prodNumber--:2)*1000);
		                    } catch (InterruptedException e) {
		                        logger.error(e.getMessage());
		                    }
	                    
	                } 
	           }
	            logger.info("********** crawling completed**************");
	            task.setStatus("Completed");
	            task.setUpdatedtimeStamp(Calendar.getInstance());
	            } catch (Exception e) {
		            logger.error(e.toString());
		            task.setStatus("Failed");
		            task.setUpdatedtimeStamp(Calendar.getInstance());
	            } finally {
	            try {
	                TaskService taskService = (TaskService) applicationContext.getBean("taskService");
	                taskService.updateStatus(task);
	            } catch (Exception ex) {
	                ex.printStackTrace();
	            }

	        }

	    }

	    public JdbcTemplate getJdbcTemplate() {
	        throw new UnsupportedOperationException("Not supported yet.");
	    }

	    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	        throw new UnsupportedOperationException("Not supported yet.");
	    }
}
