package lxr.marketplace.amazonpricetracker;

import org.springframework.jdbc.core.RowMapper;

import java.util.Calendar;
import java.util.List;
import java.sql.SQLException;
import java.sql.ResultSet;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class ResultService {
	 private static Logger logger = Logger.getLogger(ResultService.class);
	 	@Resource(name = "jdbcTemplate")
	    JdbcTemplate jdbcTemplate;
	    public JdbcTemplate getJdbcTemplate() {
	        return this.jdbcTemplate;
	    }

	    public ResultService(){
	    	
	    }
	    ResultService(JdbcTemplate jdbcTemplate){
	    	this.jdbcTemplate = jdbcTemplate;
	    }



	    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	        this.jdbcTemplate = jdbcTemplate;
	    }
	    public List<Result> find(long task_id) {
	    	logger.info("inside result service.......");
	        String query = "select * from result where task_id = " + task_id + " order by product_id";
	        try {
	        	//logger.info("task_ id"+task_id+"   ..."+jdbcTemplate);
	        	logger.info("find in result table query: "+query);
	        	List<Result> results = getJdbcTemplate().query(query, new RowMapper<Result>() {
                      
                    public Result mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Result result = new Result();
	           
	                            //result.setId(rs.getLong("id"));
	                            result.setResult_id(rs.getLong("result_id"));
	                            result.setUser_id(rs.getLong("user_id"));
	                            result.setTask_id(rs.getLong("task_id"));
	                            result.setProduct_id(rs.getLong("product_id"));
	                            result.setProduct_name(rs.getString("product_name"));
	                            result.setClient_price(rs.getDouble("client_price"));
	                            result.setCompetitor_name(rs.getString("competitor_name"));
	                            result.setCompetitor_price(rs.getDouble("competitor_price"));
	                            result.setRemark_id(rs.getInt("remark_id"));
	                            
	                            Calendar cal = Calendar.getInstance();
	                            cal.setTimeInMillis(rs.getTimestamp("result_date").getTime());
	                            result.setResult_date(cal);
	                            return result;
	                            
	                        }
	                    });
	            return results;
	        } catch (Exception e) {
	            logger.error(e.getMessage());
	            logger.error(e.getCause());
	        }
	        return null;
	    }	   
	
	    public int save(final Result result) {
	        final String query = "insert into result (user_id, task_id, product_id, product_name, client_price, competitor_name, competitor_price, remark_id, result_date) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	        Object[] valLis = new Object[9];

	        valLis[0] = result.getUser_id();
	        valLis[1] = result.getTask_id();
	        valLis[2] = result.getProduct_id();
	        valLis[3] = result.getProduct_name();
	        valLis[4] = result.getClient_price();
	        valLis[5] = result.getCompetitor_name();
	        valLis[6] = result.getCompetitor_price();
	        valLis[7] = result.getRemark_id();
	        valLis[8] = new java.sql.Timestamp(result.getResult_date().getTimeInMillis());
	        try {
	            return getJdbcTemplate().update(query, valLis);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return 0;
	    }
	    
	    
	    public int update( Result result) {
	        String query = "update task set user_id = ?, source_file =?, time_stamp =?, product_table=?, marketplace_price_table=?, target_file=?,status=?, remarks=?, marketplaces=? where id=?";
	        Object[] valLis = new Object[9];

	        valLis[0] = result.getUser_id();
	        valLis[1] = result.getTask_id();
	        valLis[2] = result.getProduct_id();
	        valLis[3] = result.getProduct_name();
	        valLis[4] = result.getClient_price();
	        valLis[5] = result.getCompetitor_name();
	        valLis[6] = result.getCompetitor_price();
	        valLis[7] = recommend(result.getClient_price(),result.getCompetitor_price());//result.getRemark_id();
	        valLis[8] = new java.sql.Timestamp(result.getResult_date().getTimeInMillis());
	        try {
	            return getJdbcTemplate().update(query, valLis);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return 0;
	    }
	    
	    public int recommend (double a, double b){
	    	if(a != 0 && b>0){
	    	return (int)Math.round(((((b-a)/a)*100)));
	    	}else{
	    		return 0;
	    	}
	    	
	    }	    
}
