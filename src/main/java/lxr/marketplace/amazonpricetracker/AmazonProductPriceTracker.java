package lxr.marketplace.amazonpricetracker;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import java.util.Collections;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import java.util.Iterator;
import java.net.URLEncoder;
import org.springframework.context.ApplicationContext;

public class AmazonProductPriceTracker implements AmazonPriceTracker {

	private static Logger logger = Logger
			.getLogger(AmazonProductPriceTracker.class);
	private Task task;
	private ApplicationContext applicationContext;
	private AmazonProductPrice amazonProductPrice;
	private int timeout = 60000;
	private int sllepOut = 5000;
	private String baseURl = "http://www.amazon.com";
	private String searchDept = "aps"; // search-alias
	private String searchCategory = "search-alias";
	private String searchPar = "field-keywords";
	private String userAgent ="Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
			
			/*Modified on oct-7 2015
			 * "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
	static NumberFormat numFor = NumberFormat.getInstance();
	Document aznProdRpt = null;
	long timeoutIteration = 0;
	long noOfTime = 10;

	public Task getTask() {

		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public AmazonProductPrice getAmazonProductPrice() {
		return amazonProductPrice;

	}

	public void setAmazonProductPrice(AmazonProductPrice amazonProductPrice) {
		this.amazonProductPrice = amazonProductPrice;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public void run() {

		SellerProductPrice client;
		String prodSearchTerm = "";
		Connection jsoupCon, jsoupCon1;
		String domain;
		double clientCost = 0.00;
		double clientShpCost = 0.00;
		double competitorCost = 0;
		double competitorShpCost = 0.00;
		double clientTotalPrice = clientCost + clientShpCost;
		double competitorTotalPrice = competitorCost + competitorShpCost;
		String competitorName = "";
		String urlDef = "";
		boolean productMatch = false;
		boolean isAmazon = false;
		boolean clientP  = false;
		boolean competitorP  = false;
		boolean foundResult =true;
		try {
			client = amazonProductPrice.getClient();
			domain = client.getSeller().replace(".com", "");
			
			prodSearchTerm = amazonProductPrice.getProduct().getProduct_name();
			
			// placing all the objects in this arrayList if seller present
			String finalBaseUrl = baseURl + "/s";
			String queryUrl = "url=" + searchCategory + "=" + searchDept + "&"
					+ searchPar + "=\""
					+ URLEncoder.encode(prodSearchTerm, "UTF-8")+"\"";

			urlDef = (finalBaseUrl.concat("?")).concat(queryUrl);
			logger.info("Search URL: " + urlDef);
			do {
				timeoutIteration += 1;
				try {
					jsoupCon = Jsoup.connect(urlDef);
					jsoupCon.timeout(timeout);
					jsoupCon.userAgent(userAgent);
					aznProdRpt = jsoupCon.get();
					break;
				} catch (IOException ex) {
					logger.info("Exception to get Connection 1 at iteration " + timeoutIteration);
					Thread.sleep(sllepOut * timeoutIteration);
				}
			} while (timeoutIteration < noOfTime);
			int checkGrid = 0;	
			Elements  aznProdRptEls = aznProdRpt.select("div[id$=atfResults] .result"); // for list view
			if (aznProdRptEls!= null && aznProdRptEls.size() == 0) {
				aznProdRptEls = aznProdRpt.select("div[id$=atfResults] .fstRow");		
			}
			if (aznProdRptEls != null && aznProdRptEls.size() == 0) {
				aznProdRptEls = aznProdRpt.select("div[class^=fkmrResults] .result"); // for grid view
				checkGrid = 1;
			}
			String domain2;
			if(domain.contains("&")){
				domain2 = domain.replaceAll("&","&amp;");
			}else{
				domain2 = domain;
			}
			
			ArrayList allListArray = new ArrayList(); 
			int alIterator = 0;
			
			if (aznProdRptEls != null && aznProdRptEls.size() > 0) {
				int i = 0; 
				String productTitle;
				Elements checkDigitNew;
				Elements checkBuyNew;
				String parentPrice = ""; 	// In Buynew's link page if client price is in range then we take
											// price from parent page price displayed
				
				List<String> firsthref = new ArrayList<String>();
				List<String> outProdNamefirsthref = new ArrayList<String>();
				List<String> amazonsClients = new ArrayList<String>();
				List<String> outProdNameamazonsClients = new ArrayList<String>();
				int index = 0;
				int index1 = 0;
				for (int j = 0; j < aznProdRptEls.size() ; j++) {
					 Element aznProdRptEl = aznProdRptEls.get(j);
					 String clssnm = "result_" + i;
					 if (aznProdRptEl.attr("id").equalsIgnoreCase(clssnm)) {
													
						Elements amzdata = aznProdRptEl.select(".data");
							
							if(amzdata!= null && amzdata.size() != 0){		//If "data" div present in first screen	
								try{
									productTitle = (aznProdRptEl.select(".data .title .title").text());
									productTitle = productTitle.replace((char)160, ' ').replace((char)153,'�');	
								}catch(NullPointerException npe){
									productTitle = "";
									logger.error("The element '.data .title .title' is unavailable.");
									npe.printStackTrace();
								}
								checkDigitNew = aznProdRptEl.select(".data .usedNewPrice");
								if (checkDigitNew != null && checkDigitNew.size() > 0) {
									Element onlyNews = checkDigitNew.select(".subPrice a").first();
									if (onlyNews != null && onlyNews.text().contains("new")) {
										
										String newLink = onlyNews.attr("href").contains("&condition=new")?
												onlyNews.attr("href"):onlyNews.attr("href").concat("&condition=new");
										firsthref.add(index, newLink);
										outProdNamefirsthref.add(index, productTitle);
										index++;
									}
								} else {
									checkBuyNew = aznProdRptEl.select(".data .newPrice");
									if (checkBuyNew!= null && checkDigitNew!= null && checkBuyNew.size() > 0 && checkDigitNew.size() <= 0) { // if directClientLinkPresent(Buynow)
										Element buyNews = checkBuyNew.select("a").first(); // not used
										if (buyNews != null&& ((buyNews.text().toLowerCase()).contains("buy"))) {
											try{
												parentPrice = checkBuyNew.select("span[class=price]").text();
											}catch(NullPointerException npe){
												parentPrice = null;
												logger.error("The element '.data .newPrice span[class=price]' is not found");
												npe.printStackTrace();
											}
											amazonsClients.add(index1, buyNews.attr("href"));
											outProdNameamazonsClients.add(index1, productTitle);
											index1++;
										}
									}
		
								}
							}else if(aznProdRptEl.select(".rsltL") != null && aznProdRptEl.select(".rsltL").size() != 0){		// if ul element present with class = "rsltL"
								try{
								productTitle = (aznProdRptEl.select(".newaps a .lrg").text());
								}catch(NullPointerException npe){
									productTitle = "";
									logger.error("The element '.newaps a .lrg' is not found");
									npe.printStackTrace();
								}
								productTitle = productTitle.replace((char)160, ' ').replace((char)153,'�');	
								Elements rsltLLis =  aznProdRptEl.select(".rsltL li");
								for(Element li : rsltLLis){
									if(li.attr("class").equals("med grey mkp") || li.attr("class").equals("medgreymkp")){
										Elements anchors = li.select("a");
										if(anchors!=null && anchors.get(0).text().matches("(\\d+)( )(new)")){
											String newLink = anchors.get(0).attr("href");											
											firsthref.add(index, newLink);
											outProdNamefirsthref.add(index, productTitle);
											index++;
										}else if(anchors!= null && anchors.get(0).text().matches("(\\d+)( )(Used & new)")){
											
											String link = anchors.get(0).attr("href");
											String newLink = link.contains("&condition=new")?link:link.concat("&condition=new");
											firsthref.add(index, newLink);
											outProdNamefirsthref.add(index, productTitle);
											index++;
										}
									}
								}
							}
							
						//}
						i++;
						if (i == 3 && checkGrid == 1){
							i = 0;
						}
					}
					/*if(aznProdRptEls.size()+temp == i){
						
						//check for pagination in the search page
						Elements nextPage = aznProdRpt.select("div[id$=pagn] span[class=pagnNext] .pagnNext");
						String nextHref;
						if(nextPage.size() >0){
							Element firstLink = nextPage.get(0);
							nextHref = firstLink.attr("href");
							logger.info("Link: " + nextHref);
							logger.info(baseURl.concat(nextHref) + "url of next link");
							jsoupCon1 =  Jsoup.connect(baseURl.concat(nextHref));
							jsoupCon1.timeout(timeout);
							jsoupCon1.userAgent(userAgent);
							aznProdRpt = jsoupCon1.get();
							
							aznProdRptEls = (aznProdRpt.select("#fkmr-results0 div")).select("div[id^=result_]");
							checkGrid = 0;
							aznProdRptEls = aznProdRpt.select("div[id$=tfResults] .result"); // for list view
							if (aznProdRptEls.size() == 0) {
									aznProdRptEls = aznProdRpt.select("div[class^=fkmrResults] .result"); // for grid view
									checkGrid = 1;
							}
							j = 0;
							temp  = i;
						}
						pagecount++;
					}*/
					
				}

				if (firsthref!= null && firsthref.size() > 0) {
					int hshIte = 0;
					// HashMap: ArrayLists are placed which internally have its
					// all links respectively
					HashMap<Integer, ArrayList> categoryMap = new HashMap<Integer, ArrayList>();
					ArrayList categWise;
					Document amazon = null;
					for (int k = 0; k < firsthref.size(); k++) {
						
						Object e = firsthref.get(k);
						timeoutIteration = 0;
						do {
							try {
								jsoupCon1 = Jsoup.connect(e.toString());
								jsoupCon1.timeout(timeout);
								jsoupCon1.userAgent(userAgent);
								 amazon = jsoupCon1.get();
								break;
							} catch (IOException ex) {
								logger.info("Exception to get Connection 2 at iteration " + timeoutIteration+"\nLink: "+e.toString());
								Thread.sleep(sllepOut * timeoutIteration);
							}
							timeoutIteration++;
						} while (timeoutIteration < noOfTime/2);
						if(amazon == null){
							continue;
						}
						String prodTitle = "";
						try{
							prodTitle = (amazon.select("#productheader .producttitle ").text());
							prodTitle = prodTitle.replace((char)160, ' ').replace((char)153,'�');
						}catch (NullPointerException npe){
							prodTitle="";
							//logger.info("Exception to find producttitle in URL : "+e.toString()+"");
							logger.info("Exception to find producttitle, The element '#productheader .producttitle' is not found");
							npe.printStackTrace();
						}

						if(prodSearchTerm.equalsIgnoreCase(prodTitle) 
								|| prodSearchTerm.equalsIgnoreCase(outProdNamefirsthref.get(k))){
							foundResult = false;
							productMatch = true;
							categWise = new ArrayList();
							categWise.add(0, e.toString());
							Elements pagnation = amazon.select(".pages a");
							if (pagnation!= null && pagnation.size() > 0) { // if pagination exists
								Element op = pagnation.get(pagnation.size() - 2);
								int pgno = Integer.parseInt(op.text());
								int j = 1;
								int m = 15;
								while (pgno > 1) {
									categWise.add(j,e.toString().concat("&startIndex=" + m));
									j++;
									m = m + 15;
									pgno--;
								}
							}
							categoryMap.put(hshIte, categWise);
							hshIte++;
						
						}
					}
						
					Iterator entries = categoryMap.keySet().iterator();

					while (entries.hasNext()) { // reading each arrayList
						Object hshKey = entries.next();
						ArrayList FirstList = (ArrayList) categoryMap.get((Integer) hshKey);
						Iterator listIt = FirstList.iterator();
						
						Elements aznElements;
						Document gleProdSrch = null;
						String iterConn = "";
						String finalUrl = "";
						while (listIt.hasNext()) {
							iterConn = (String) listIt.next();
							timeoutIteration=0;
							do{
								try {
									jsoupCon = Jsoup.connect(iterConn);
									jsoupCon.timeout(timeout);
									jsoupCon.userAgent(userAgent);
									gleProdSrch = jsoupCon.get();
									break;
								} catch (IOException ex) {
									logger.info("Exception to get Connection 3 at iteration " + timeoutIteration+"\nLink: "+iterConn);
									Thread.sleep(sllepOut * timeoutIteration);
								}
								timeoutIteration++;
							} while (timeoutIteration < noOfTime/2);
							
							try{
								aznElements = gleProdSrch.select(".resultsset").get(0).select(".result tr");
							}catch(NullPointerException npe){
								aznElements = null;
								logger.error(".resultsset or .result tr not present in website");
								npe.printStackTrace();
							}
							if(aznElements!= null){
							for (Element aznEl : aznElements) { // Reading each seller Information
								if (aznEl.select("div[class=condition]").text().equalsIgnoreCase("New")){												
									String[] prcShpCstSellrArray = new String[5];
									Elements tds = aznEl.select("td");
									for (Element td : tds) {
										Elements prcstColl = td.select("span[class=price]");
										if (prcstColl.size() > 0) {
											String priceInit = td.select("span[class=price]").text();
											String shipCostInit = td.select("span[class=price_shipping]").text();
											priceInit = priceInit.substring(priceInit.indexOf("$") + 1);
											shipCostInit = shipCostInit.contains(",")?shipCostInit.replace(",", "."):shipCostInit;
											
											double price = Double.parseDouble(numFor.parse(priceInit)+ "");										
											double shipCost;
											try{
												shipCost= (shipCostInit.length() > 2) ? (Double.parseDouble((shipCostInit)
															.substring(shipCostInit.indexOf("$") + 1))): 0.00;
											}catch(Exception e){
												shipCost = 0;
											}
											prcShpCstSellrArray[0] = String.valueOf(price);
											prcShpCstSellrArray[4] = String.valueOf(price + shipCost);
											prcShpCstSellrArray[1] = String.valueOf(shipCost);
										}

										Elements sellerColl;
										sellerColl = td.select(".sellerInformation a img");
										if (sellerColl.size() == 0) {
											sellerColl = td.select(".seller a");
											if (sellerColl.size() == 0) {
												sellerColl = td.select(".sellerInformation img");
											}
										}
										for (Element seller : sellerColl) {
											if (seller.attr("alt").length() > 0) {
												prcShpCstSellrArray[2] = seller.attr("alt");								
											} else {
												prcShpCstSellrArray[2] = (seller.text()).replace((char)160,' ');
											}
										}
										if(prcShpCstSellrArray[2]!= null){
											if(prcShpCstSellrArray[2].equalsIgnoreCase(domain)){
										    	clientP = true;
										    }else if(prcShpCstSellrArray[2].toLowerCase().contains("amazon")){
										    	isAmazon = true;
										    }else {
										    	competitorP = true;
										    }
										  }
										prcShpCstSellrArray[3] = finalUrl;
									}
									allListArray.add(alIterator, prcShpCstSellrArray);
									alIterator++;
								}
							}
							foundResult=true;
							}
						}

					}	
				}
				
				if (amazonsClients.size() > 0) {
					for (int j = 0; j < amazonsClients.size(); j++) {
						Object e = amazonsClients.get(j);
						Document amazon = null;
						timeoutIteration=0;
						do{
							try{
								Connection jsoupCon2 = Jsoup.connect(e.toString());		
								jsoupCon2.timeout(timeout);
								jsoupCon2.userAgent(userAgent);	
								amazon = jsoupCon2.get();
								break;
							} catch (IOException ex) {
								logger.info("Exception to get Connection 4 at iteration " + timeoutIteration+"\nLink: "+e.toString());
								Thread.sleep(sllepOut * timeoutIteration);
							}
							timeoutIteration++;
						} while (timeoutIteration < noOfTime/2);
						String mainTitletext = (amazon.select(".buying #btAsinTitle").text()).replace((char)160,' ').replace((char)153,'�');
						if(mainTitletext!= null && prodSearchTerm.equalsIgnoreCase(mainTitletext)||prodSearchTerm.equalsIgnoreCase(outProdNameamazonsClients.get(j))){
							String[] prcShpCstSellrArray = new String[5];
							foundResult = false;
							productMatch = true;
							String priceInit = amazon.select(".priceLarge").html();							
							String soldbyFromBuyBox = (amazon.select ("#BBAvailPlusMerchID .tiny b").text()).replace((char)160,' ').replace((char)153,'�');
							String soldby = "";
							if(!soldbyFromBuyBox.equals(""))
								soldby = soldbyFromBuyBox;
							else{
								Elements handleBuy = amazon.select("#handleBuy .buying");					
								for(Element eachSoldBy : handleBuy){
									if((eachSoldBy.text()).toLowerCase().contains("sold by")){
										soldby = eachSoldBy.select("b a").text().replace((char)160,' ').replace((char)153,'�');
										break;
									}
								}
							}
							if(!(soldby.equals(""))){
								String shippingPrice = amazon.select("#buyBoxContent .plusShippingText").text();
								String finalshippingPrice = "0.0";
								if(shippingPrice!= null && shippingPrice.indexOf("$") > 0){
									String test = shippingPrice.substring(shippingPrice.indexOf("$") + 1).replaceAll(",", "");
									finalshippingPrice = test.substring(0,(test.indexOf((char)160)));
								}
								String[] testRange = priceInit.split("[$]");
								if (testRange.length > 2) {
									priceInit = parentPrice;
								}
								if(soldby.equalsIgnoreCase(domain)|| soldby.equalsIgnoreCase(domain2)){
									clientP = true;
									clientCost = Double.parseDouble((priceInit).substring(priceInit.indexOf("$") + 1).replaceAll(",", ""));
									clientShpCost = (finalshippingPrice.length() > 2) ? (Double.parseDouble(finalshippingPrice)): 0.00;
									prcShpCstSellrArray[0] = ""+clientCost;
									prcShpCstSellrArray[1] = ""+clientShpCost;
									prcShpCstSellrArray[2] = soldby;
									prcShpCstSellrArray[3] = e.toString();
									prcShpCstSellrArray[4] = ""+(clientCost+clientShpCost);
								}else{
									competitorP = true;
									competitorCost = Double.parseDouble((priceInit).substring(priceInit.indexOf("$") + 1).replaceAll(",", ""));
									competitorShpCost = (finalshippingPrice.length() > 2) ? (Double.parseDouble(finalshippingPrice)): 0.00;
									competitorName = soldby;
									prcShpCstSellrArray[0] = ""+competitorCost;
									prcShpCstSellrArray[1] = ""+competitorShpCost;
									prcShpCstSellrArray[2] = soldby;
									prcShpCstSellrArray[3] = e.toString();
									prcShpCstSellrArray[4] = ""+(competitorCost+competitorShpCost);
									
								}
								allListArray.add(alIterator, prcShpCstSellrArray);
								alIterator++;
						  	}else{
						  		isAmazon = true;
						  	}
							foundResult = true;
						}
					}
				}
			}
			if (allListArray.size() > 0) {				
				//Removing competitors for which price is not showing in amazon
				for(int i = 0; i < allListArray.size(); i++){
					String[] ads = (String[])allListArray.get(i);
					if(ads[4] == null || ads[2].toLowerCase().contains("amazon")){
						allListArray.remove(i);
						i--;
					}
				}
				
				Collections.sort(allListArray, new Comparator<String[]>() {

					public int compare(String[] strings, String[] otherStrings) {
						Double a1 = Double.parseDouble(strings[4]
								.substring((strings[4].indexOf("$")) + 1));
						Double a2 = Double.parseDouble(otherStrings[4]
								.substring((otherStrings[4].indexOf("$")) + 1));
						return a1.compareTo(a2);
					}
				});
				// for getting client details and setting to Seller obj
				for (Object a : allListArray) {
					String[] ary = (String[]) a;
					if (ary[2].equalsIgnoreCase(domain)) {
						clientCost = Double.parseDouble(ary[0].substring((ary[0].indexOf("$")) + 1));
						clientShpCost = Double.parseDouble(ary[1].substring((ary[1].indexOf("$")) + 1));
						//clientURl = ary[3];
						break;
					}
				}
				// getting competitor details and setting competitor obj
				for (Object e : allListArray) {
					String[] temp = (String[]) e;
					if (!temp[2].equalsIgnoreCase(domain)){
						competitorCost = Double.parseDouble(temp[0]
												.substring((temp[0].indexOf("$")) + 1));
						competitorShpCost = (temp[1].length() > 0) ? (Double.parseDouble(temp[1]
												.substring((temp[1]
														.indexOf("$")) + 1)))
										: 0.00;
						competitorName = temp[2];
						break;
					}
				}
			}
			clientTotalPrice = clientCost + clientShpCost;
			competitorTotalPrice = competitorCost + competitorShpCost;
			
			/*         Creating result object            */
			Result result = new Result();
			result.setClient_price(clientTotalPrice);
			result.setCompetitor_name(competitorName);
			result.setCompetitor_price(competitorTotalPrice);
			result.setProduct_name(prodSearchTerm);
			result.setTask_id(task.getId());
			result.setUser_id(task.getUserId());
			result.setProduct_id(amazonProductPrice.getProduct()
					.getProduct_id());
			result.setResult_date(Calendar.getInstance());

			logger.info("Client: " + clientTotalPrice + " Competitor: "+ competitorTotalPrice);
			
			/*              setting remarks                     */
			
			if (clientTotalPrice <= 0 && competitorTotalPrice <= 0) {
				if(productMatch){
					if(!(clientP) && competitorP)
					    result.setRemark_id(2);
					else if(!(clientP) && !competitorP )
						result.setRemark_id(3);	
					else if(clientP && competitorP)
						result.setRemark_id(5);
					else if(clientP && !competitorP)
						result.setRemark_id(6);
				}else if(!foundResult){
					result.setRemark_id(17);
				}
				else {
					result.setRemark_id(16);
				}
			} else if (clientTotalPrice <= 0 && competitorTotalPrice>0) {
				if(!clientP && competitorP)
				   result.setRemark_id(1); 
				else if(clientP && competitorP)
				   result.setRemark_id(4); 
			} else if (competitorTotalPrice <= 0 && clientTotalPrice>0) {
				if(clientP && !competitorP)
				   result.setRemark_id(7);
				else if(clientP && competitorP)
				   result.setRemark_id(8);
			} else {
				double diff = ((competitorTotalPrice - clientTotalPrice) / clientTotalPrice) * 100;
				if (diff < -15) {
					result.setRemark_id(9); // client is on very high price
				} else if (diff >= -15 && diff < -5) {
					result.setRemark_id(10);
				} else if (diff >= -5 && diff < 0) {
					result.setRemark_id(11);
				} else if (diff > 15) {
					result.setRemark_id(12); // client is on very low price
				} else if (diff <= 15 && diff > 5) {
					result.setRemark_id(13);
				} else if (diff <= 5 && diff > 0) {
					result.setRemark_id(14);
				} else {
					result.setRemark_id(15); // price is same with competitor 
				}
			}
			ResultService resultService = (ResultService) applicationContext.getBean("resultService");
			resultService.save(result);

		} catch (Exception e) {
			logger.error("URL: " + urlDef);
			e.printStackTrace();
		}
	}
}
