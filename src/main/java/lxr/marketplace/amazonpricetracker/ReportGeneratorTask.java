package lxr.marketplace.amazonpricetracker;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lxr.marketplace.util.CommonReportGenerator;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

public class ReportGeneratorTask implements AmazonPriceTrackerTask {
	private static Logger logger = Logger.getLogger(ReportGeneratorTask.class);
    private Task task;
    private ApplicationContext applicationContext;
    private String domainName;
    
    public ReportGeneratorTask() {
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void run() {
    	 String downloadFolder = (String) applicationContext.getBean("downloadFolder");
        String targetFile = Common.generateTableName("LXRMarketplace_Amazon_Price_Analyzer_Report").concat(".xls");
      
        JdbcTemplate jdbcTemplate = (JdbcTemplate) applicationContext.getBean("jdbcTemplate");;
        SqlRowSet sqlRowSet;

        String sql = "select product_name as 'Product Name',client_price as 'Your Price'," +
        		"competitor_name as 'Competitor Name',competitor_price as 'Competitor Price'," +
        		"(select reco from amazonPA_reco where id=remark_id) as 'Comments' from result " +
        		"where task_id = " + task.getTaskid() +" order by product_id ";
        sqlRowSet = jdbcTemplate.queryForRowSet(sql);
        Calendar cal = (Calendar) task.getCreated_time_stamp();
        domainName = task.getDomain_name();
        Date d = new Date(cal.getTime().getTime());
        String format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss").format(d);
        try {
        	 int flag=0;
        	TaskService taskService = (TaskService) applicationContext.getBean("taskService"); 
            sqlRowSet = jdbcTemplate.queryForRowSet(sql);

            domainName = task.getDomain_name();         
          flag = generateExcelDocument(sqlRowSet, downloadFolder+targetFile, format);
          if(flag > 0){
        	  task.setResultFile(targetFile);
        	  logger.info("Amazon Price Tracker Report created.");
          }else{
          		task.setResultFile("");
          		logger.error("Result file is empty");
          }
//           if(createXLSReport(sqlRowSet, downloadFolder, targetFile)){                     
//            	task.setResultFile(targetFile);
//            	logger.info("Amazon Price Tracker Report created.");
//          }else{
//            	task.setResultFile("");
//          }
            taskService.updateResultFile(task);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public boolean createXLSReport( SqlRowSet sqlRowSet, String downloadFolder, String targetFile){
    	
        Calendar cal = (Calendar) task.getCreated_time_stamp();
        Date d = new Date(cal.getTime().getTime());
        String format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss").format(d);
    	int colWidth[] = {0,40,22,14,15,45,10,8,10,5,8,8,9,8,8,8,8,10,10,15,50};
        String[] headings = new String[3];
        headings[0] = "Report: Price Analysis for Amazon";
        headings[1] = "Client: "+domainName;
        headings[2] = "Date: "+format;
        
		HSSFWorkbook workBook = new HSSFWorkbook();
        HSSFSheet sheet = workBook.createSheet("Price Analysis for Amazon");
       // CommonReportGenerator.createXLSFromDatabase(workBook, sheet, sqlRowSet, colWidth, headings, true);
    	try {
        	FileOutputStream filOutPutStr = new FileOutputStream(downloadFolder + targetFile);
			workBook.write(filOutPutStr);
	        filOutPutStr.flush();
	        filOutPutStr.close();
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
			return false;
		}catch(IOException e){			
			e.printStackTrace();
			return false;
		}  
		return true;
    }
    
    public int generateExcelDocument(SqlRowSet sqlRowSet, String targetFilePath, String dateRange) throws Exception {
    	logger.info("Amazon Price Tracker Report .");
        int colCount;
        int rowInd = 0;
        FileOutputStream filOutPutStr;

        SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

        HSSFWorkbook workBook = new HSSFWorkbook();
        HSSFSheet sheet = workBook.createSheet("Price Analysis for Amazon");
        boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short)0, 0, (short)1, 4);
        if(logoAdded){
        	rowInd = 5;
		}

        HSSFRow curRow;
        HSSFCell curCel;
        int colWidth[] = {0,40,22,14,15,45};
        HSSFCellStyle heaCelSty;
        HSSFCellStyle colHeaCelSty;
        HSSFCellStyle dblCelSty;
        HSSFFont font;
        
        
           //Double Data Cell Style
        dblCelSty = workBook.createCellStyle();
        dblCelSty.setDataFormat((short) 2);

          //Heading Cell Style
        heaCelSty = workBook.createCellStyle();
        font = workBook.createFont();
        font.setBold(true);
        heaCelSty.setFont(font);

        //Column Heading Cell Style
        colHeaCelSty = workBook.createCellStyle();
        font = workBook.createFont();
        font.setBold(true);
        colHeaCelSty.setFont(font);
        colHeaCelSty.setWrapText(true);

        //Report Heading1
        curRow = sheet.createRow(rowInd++);
        curCel = curRow.createCell(0);
        curCel.setCellValue("Price Analysis for Amazon");
        curCel.setCellStyle(heaCelSty);
        

      //Report Heading2
        curRow = sheet.createRow(rowInd++);
        curCel = curRow.createCell(0);
        curCel.setCellValue("Client :");
        curCel.setCellStyle(heaCelSty);
        curCel = curRow.createCell(1);
        curCel.setCellValue(domainName);
        curCel.setCellStyle(heaCelSty);


        //Report Heading3
        curRow = sheet.createRow(rowInd++);
        curCel = curRow.createCell(0);
        curCel.setCellValue("Date :");
        curCel.setCellStyle(heaCelSty);
        curCel = curRow.createCell(1);
        curCel.setCellValue(dateRange);
        curCel.setCellStyle(heaCelSty);
        
        

        colCount = sqlRowSetMetDat.getColumnCount();
        curRow = sheet.createRow(rowInd++);
        for (int colInd = 1; colInd <= colCount; colInd++) {
            curCel = curRow.createCell(colInd - 1);
            curCel.setCellValue(sqlRowSetMetDat.getColumnLabel(colInd));
            curCel.setCellStyle(colHeaCelSty);
              sheet.setColumnWidth( colInd-1,(int)(441.3793d+256d*(colWidth[colInd]-1d)) );
        }

        //for freeze
        sheet.createFreezePane(0, rowInd);

        double clientPrice;
        double competitorPrice;
        while (sqlRowSet.next()) {
            curRow = sheet.createRow(rowInd);
            for (int colInd = 1; colInd <= colCount; colInd++) {
            	clientPrice=sqlRowSet.getDouble(sqlRowSetMetDat.getColumnName(2));           	
            	 competitorPrice=sqlRowSet.getDouble(sqlRowSetMetDat.getColumnName(4));
                curCel = curRow.createCell(colInd - 1);
                if (sqlRowSetMetDat.getColumnType(colInd) == Types.DOUBLE) {
                    double dblVal = sqlRowSet.getDouble(sqlRowSetMetDat.getColumnName(colInd));
                    if (dblVal > 0) {
                        curCel.setCellValue(dblVal);
                        curCel.setCellStyle(dblCelSty);

                    }else if(dblVal == 0){
                        curCel.setCellValue(" ");
                        curCel.setCellStyle(dblCelSty);
                    }

                }else {
            	 int recom = recommend(clientPrice, competitorPrice);
                 String recommendation ="";
                 if(clientPrice != 0.0 && competitorPrice != 0.0){  
				       if(recom < 0){
				    	  recommendation = "Your price is "+ Math.abs(recom) +"% higher than your lowest priced Competitor";					      
				       }else if(recom > 0){
				    	 recommendation = "Your price is " + recom + "% lower than your Lowest priced Competitor";
				       }else if(recom == 0){
				    	  recommendation = "Your price is almost same as your lowest priced Competitor";
				       }					      
				       curCel.setCellValue(recommendation);
			       }
                 
                else {
                     curCel.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)));
             		}
            }
            }
            rowInd++;
        }
        filOutPutStr = new FileOutputStream(targetFilePath);
        workBook.write(filOutPutStr);
        filOutPutStr.flush();
        filOutPutStr.close();
        return rowInd;
    }
    
    public int recommend (double a, double b){
    	if(a != 0 && b>0){
    	return (int)Math.round(((((b-a)/a)*100)));
    	}else{
    		return 0;
    	}

    }
    
}

