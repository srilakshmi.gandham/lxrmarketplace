package lxr.marketplace.amazonpricetracker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class ProductService {
	
	private static Logger logger = Logger.getLogger(ProductService.class);
    JdbcTemplate jdbcTemplate;
    
    ProductService(JdbcTemplate jdbcTemplate){
    	this.jdbcTemplate = jdbcTemplate;
    }
    
    public ProductService() {

	}

	public JdbcTemplate getJdbcTemplate() {
        return this.jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    
    public int save(final Product product) {
    	final String query = "INSERT INTO product ( user_id, product_name, is_Deleted) VALUES (?, ?, ?)";
        Object[] valLis = new Object[3];

        valLis[0] = product.getUser_id();
        valLis[1] = product.getProduct_name();
        valLis[2] = product.isIs_Deleted();
        
        try {
        	//System.out.println(getJdbcTemplate());
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }   
    public List<Product> getProductList(long user_id) {
        String query = "select * from product where user_id = "+user_id+" and is_Deleted = false";
        try {
        	 List<Product> products = getJdbcTemplate().query(query, new RowMapper<Product>() {

                        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Product product = new Product();

                            product.setProduct_id(rs.getLong("product_id"));
                            product.setUser_id(rs.getLong("user_id"));
                            product.setProduct_name(rs.getString("product_name"));
                            product.setIs_Deleted(rs.getBoolean("is_Deleted"));

                            return product;
                        }
                    });
            return products;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }
    public List<Product> getAllProductList(long user_id) {
        String query = "select * from product where user_id = "+user_id;
        try {
        	 List<Product> products = getJdbcTemplate().query(query, new RowMapper<Product>() {

                        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Product product = new Product();

                            product.setProduct_id(rs.getLong("product_id"));
                            product.setUser_id(rs.getLong("user_id"));
                            product.setProduct_name(rs.getString("product_name"));
                            product.setIs_Deleted(rs.getBoolean("is_Deleted"));

                            return product;
                        }
                    });
            return products;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }
    
    public int update(Product product) {
        String query = "update product set user_id =?, product_name =?, is_Deleted=? where product_id=?";
        Object[] valLis = new Object[4];

        valLis[0] = product.getUser_id();
        valLis[1] = product.getProduct_name();
        valLis[2] = product.isIs_Deleted();
        valLis[3] = product.getProduct_id();
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
}
