/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import lxr.marketplace.util.CellStyleParamters;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Comparator;
import java.util.Locale;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
import org.apache.poi.ss.util.CellUtil;
import org.springframework.context.MessageSource;

/**
 *
 * @author vidyasagar.korada(NE16T1213)
 */
@Service
public class PageSpeedInsightReportService {

    private static final Logger LOGGER = Logger.getLogger(PageSpeedInsightReportService.class);

    @Autowired
    private PageSpeedInsightsService pageSpeedInsightsService;

    @Autowired
    private MessageSource messageSource;

    @Value("${PageSpeedInsights.logoPath}")
    private String logoPath;
    @Value("${PageSpeedInsights.NORMAL_TEXT_SIZE}")
    private short NORMAL_TEXT_SIZE;
    @Value("${PageSpeedInsights.SUBHEADING_TEXT_SIZE}")
    private short SUBHEADING_TEXT_SIZE;

    @Value("${XSSFCELL.MAX_CHARCHTER_SIZE}")
    private int MAX_CHARCHTER_SIZE;

    private static final XSSFColor TITLE_FONT_INDEX = new XSSFColor(new Color(245, 129, 42));
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("##.#");
    private static final String LAB_DATA_RULES_IDS[] = {"first-contentful-paint", "first-meaningful-paint", "speed-index", "first-cpu-idle", "interactive", "max-potential-fid"};

    /*
    Intialize the variables
    Create the sheets
    Add the QueryURL
    Add the Time Stamp
    Add the score
    Add the real word field data
    Add the lab data
    Add the Opportunity
    Add the Diagnostics
    Add the Passed Rules
    Write into File out stream*/
    public String generatePageSpeedInsightsAuditReport(List<PageSpeedInsights> pageSpeedInsightsList, String downloadFolder, String fileName) {
        PageSpeedInsights pageSpeedInsights = null;
        if (pageSpeedInsightsList != null && !pageSpeedInsightsList.isEmpty()) {
            pageSpeedInsights = pageSpeedInsightsList.stream().filter(psi -> psi.getStatusCode() == HttpStatus.SC_OK).findAny().get();
        }
        File file = null;
        String filePath = null;
        XSSFWorkbook workBookObj = null;
        XSSFSheet sheetObj = null;
        Row rowObj = null;
        Cell cellObj = null;
        CellRangeAddress cellRangeAddress = null;
        CellStyle mainheadCellSty = null, subColHeaderStyle = null;
        CellStyleParamters styleParams = null;
        workBookObj = new XSSFWorkbook();
        sheetObj = workBookObj.createSheet(WordUtils.capitalize(pageSpeedInsightsService.getMobileUserAgent()));
        sheetObj.setDisplayRowColHeadings(false);
        sheetObj.setDisplayGridlines(false);

        /*Creating sheet # 1 for mobile device*/
        if (pageSpeedInsights != null) {

            Runtime runtime = Runtime.getRuntime();
            runtime.freeMemory();

            int rowCount = 0;


            /*Row #1: Adding LXRM logo image to sheet.*/
//            addImage(workBookObj, sheetObj, 0,0, 1, 1, logoPath, true);
            appendImageToExcellCell(workBookObj, sheetObj, (short) 0, 0, (short) 1, 1, logoPath + "LXRMarketplace.png");


            /*Adding the sheet title*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 29, true, TITLE_FONT_INDEX));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);

            /*getting tsyle object*/
            mainheadCellSty = getRequriedXSSFCellStyle(workBookObj, styleParams);

            rowObj = sheetObj.createRow(rowCount);
            rowObj.setHeight((short) 800);
            cellObj = rowObj.createCell(1);
            cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.SHEET_TITLE", null, Locale.US));
            sheetObj.addMergedRegion(new CellRangeAddress(0, 0, 1, 10));
            cellObj.setCellStyle(mainheadCellSty);

            /*Analysis time*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 13, Boolean.TRUE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            subColHeaderStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 600);

            cellObj = rowObj.createCell(12);
            cellObj.setCellValue("Analysis time: " + pageSpeedInsights.getAnalysisUTCTimestamp());
            sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 18));
            cellObj.setCellStyle(subColHeaderStyle);

            /*Row #2: Adding the revied URL*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            styleParams.setHaveBorder(Boolean.TRUE);
            styleParams.setBorderColorIndex(IndexedColors.BLACK.index);
            styleParams.setBorderType("all");
            /*getting tsyle object*/
            subColHeaderStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 800);

            cellObj = rowObj.createCell(0);
            cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 0, ((pageSpeedInsights.getFinalUrl().length() < 190) ? 18 : 23));
            sheetObj.addMergedRegion(cellRangeAddress);
            cellObj.setCellValue(pageSpeedInsights.getFinalUrl());
            cellObj.setCellStyle(subColHeaderStyle);
            appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
            /*Incremnt row count*/
            ++rowCount;
            ++rowCount;
            ++rowCount;
            /*Mobile Adding the Device Categorie*/
            pageSpeedInsights = pageSpeedInsightsList.stream().filter(psi -> psi.getDeviceCategory().equals(pageSpeedInsightsService.getMobileUserAgent())).findAny().get();
            generateDeviceCategorieSheet(workBookObj, sheetObj, rowCount, pageSpeedInsights);

            sheetObj = workBookObj.createSheet(WordUtils.capitalize(pageSpeedInsightsService.getDesktopUserAgent()));
            sheetObj.setDisplayRowColHeadings(false);
            sheetObj.setDisplayGridlines(false);

            /*Adding the Device Categorie*/
            pageSpeedInsights = pageSpeedInsightsList.stream().filter(psi -> psi.getDeviceCategory().equals(pageSpeedInsightsService.getDesktopUserAgent())).findAny().get();
            generateDeviceCategorieSheet(workBookObj, sheetObj, rowCount, pageSpeedInsights);
        }/*EOF of pageSpeedInsights object*/

 /*Saving the file*/
        filePath = fileName + ".xlsx";
        file = new File(downloadFolder + filePath);
        try {
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBookObj.write(stream);
                stream.close();
            }
        } catch (IOException e) {
            LOGGER.error("Exception in wrting into streams cause:: " + e);
        }
        LOGGER.info("FilePath to get PSI generated report is :: " + filePath);
        return filePath;
    }

    private void generateDeviceCategorieSheet(Workbook workBookObj, Sheet sheetObj, int rowCount, PageSpeedInsights pageSpeedInsights) {
        Row rowObj = null;
        Cell cellObj = null;
        CellStyle mainheadCellSty = null, subColHeaderStyle = null, valueCellStyle = null, valueRightCellStyle = null, tempCellStyle = null;
        CellStyleParamters styleParams = null;
        CellRangeAddress cellRangeAddress = null;
        int tempRowCount = 0;
        if (pageSpeedInsights != null) {
            /*Preparing Common style objects*/

 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 27, Boolean.TRUE, TITLE_FONT_INDEX));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            mainheadCellSty = getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, SUBHEADING_TEXT_SIZE, Boolean.TRUE, TITLE_FONT_INDEX));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            subColHeaderStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.JUSTIFY);
            /*getting tsyle object*/
            valueCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            valueRightCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            CellStyle valueCenterCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

            Map<String, CellStyle> commonStyleObj = new HashedMap<>();
            commonStyleObj.put("subColHeaderStyle", subColHeaderStyle);
            commonStyleObj.put("valueCellStyle", valueCellStyle);
            commonStyleObj.put("valueRightCellStyle", valueRightCellStyle);

            /*Adding the Device Categorie in sheet*/
            rowObj = sheetObj.createRow(++rowCount);
            rowCount = createMergedRowsAndCellsAddCellValue("  " + pageSpeedInsights.getDeviceCategory().toUpperCase() + " :- ", sheetObj, mainheadCellSty, rowCount, rowCount + 1, (short) 0, (short) 3);
            if (pageSpeedInsights.getStatusCode() == HttpStatus.SC_OK) {
                /*Score section*/
 /*Preparmg style params for light house text*/
                styleParams = new CellStyleParamters();
                styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                styleParams.setVerticalPostion(VerticalAlignment.BOTTOM);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                /*Creating 5 rows & first 5 coloumns and merging them*/
                tempRowCount = rowCount;
                rowCount = createMergedRowsAndCellsAddCellValue(messageSource.getMessage("PageSpeedInsights.lighthouseScore", null, Locale.US) + " :-", sheetObj, tempCellStyle, rowCount, rowCount + 4, (short) 0, (short) 4);
                /*Preparmg style params for score*/
                styleParams = new CellStyleParamters();
                styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 80, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getScoreColor(((double) pageSpeedInsights.getPerformanceScore()), null))));
                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                /*Creating next 3 coloumns and merging them in previously created rows */
                createMergedCellsAddCellValue(String.valueOf(pageSpeedInsights.getPerformanceScore()), sheetObj, tempCellStyle, tempRowCount, tempRowCount + 4, (short) 5, (short) 8);

//                rowObj = sheetObj.createRow(++rowCount);

                /*Field data section*/
 /*Field data Label*/
                sheetObj.createRow(++rowCount);
                rowCount = createMergedRowsAndCellsAddCellValue("  " + messageSource.getMessage("PageSpeedInsights.FIELD_DATA", null, Locale.US) + " :-", sheetObj, subColHeaderStyle, rowCount, rowCount + 1, (short) 0, (short) 3);
                if (!pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory().equals(pageSpeedInsightsService.getNoData())) {
                    /*Field Data Prefix*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.FIELD_DATA_PREFIX", null, Locale.US) + " ");
                    sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 7));
                    cellObj.setCellStyle(valueRightCellStyle);
                    /*Field Data categorie*/
                    styleParams = new CellStyleParamters();
//                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory().toLowerCase()))));
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    /*getting style object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                    if (pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory().length() <= 4) {
                        cellObj = rowObj.createCell(8);
                        cellObj.setCellValue(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory().toLowerCase()));
                        cellObj.setCellStyle(tempCellStyle);
                        cellObj = rowObj.createCell(9);
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 32));
                        cellObj.setCellStyle(valueCenterCellStyle);
                        cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.FIELD_DATA_POSTFIX", null, Locale.US));
                        CellUtil.setAlignment(cellObj, HorizontalAlignment.LEFT);
                    } else {
                        cellObj = rowObj.createCell(8);
                        cellObj.setCellValue(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getFieldDataCategory().toLowerCase()));
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, (short) 8, (short) 9));
                        cellObj.setCellStyle(tempCellStyle);
                        cellObj = rowObj.createCell(10);
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 32));
                        cellObj.setCellStyle(valueCenterCellStyle);
                        cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.FIELD_DATA_POSTFIX", null, Locale.US));

                    }
                    /*EOf FCP title and desc*/
                    tempRowCount = ++rowCount;
                    rowObj = sheetObj.createRow(tempRowCount);
                    rowObj.setHeight((short) 500);

                    addImageInXSSFCell(workBookObj, sheetObj, rowObj, 0, tempRowCount, logoPath + pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPColor().toLowerCase() + ".png");

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.TRUE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.firstContentfulPaint", null, Locale.US) + " (FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 1, 7);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    /*FCP score*/
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPColor())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(8);
//                    cellObj.setCellValue(DECIMAL_FORMAT.format(pageSpeedInsightsService.convertMilliSecondIntoSeconds(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPPercentile())) + " s");
                    cellObj.setCellValue(convertMilliSecondIntoSeconds(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPPercentile()) + " s");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 8, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF title and score for fcp */

 /*FID title*/
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.TRUE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    addImageInXSSFCell(workBookObj, sheetObj, rowObj, 11, tempRowCount, logoPath + pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDColor().toLowerCase() + ".png");

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.firstInputDelay", null, Locale.US) + " (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 12, 19);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*FID score*/
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDColor())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(20);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDPercentile() + " ms");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 20, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF title and score for fcp */

 /*Fast message*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getFast())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPdistributions().get(pageSpeedInsightsService.getFast()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " fast (< 1 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getFast())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDdistributions().get(pageSpeedInsightsService.getFast()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + "  fast (< 50 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF of Fast*/

 /*Average*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getAverage())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPdistributions().get(pageSpeedInsightsService.getAverage()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " average (1 s ~ 2.5 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getAverage())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDdistributions().get(pageSpeedInsightsService.getAverage()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + "  average (50 ms ~ 250 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF Average*/
 /*Slow*/

                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getSlow())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPdistributions().get(pageSpeedInsightsService.getSlow()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " slow (>2.5 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getSlow())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getFieldDataFIDdistributions().get(pageSpeedInsightsService.getSlow()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " slow (>250 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                } else {
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);
                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.NO_DATA_MSG", null, Locale.US) + " page.");
                    sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 25));
                    cellObj.setCellStyle(valueCellStyle);
                }
                /*EOF of Field Data Content*/

                sheetObj.createRow(++rowCount);
                rowObj = sheetObj.createRow(++rowCount);
                rowObj.setHeight((short) 500);
                cellObj = rowObj.createCell(0);
                cellObj.setCellValue("  " + messageSource.getMessage("PageSpeedInsights.ORIGIN_SUMMARY", null, Locale.US) + " :-");
                sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 5));
                cellObj.setCellStyle(subColHeaderStyle);

                if (!pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory().equals(pageSpeedInsightsService.getNoData())) {
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);
                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.ORIGIN_SUMMARY_PREFIX", null, Locale.US));
                    sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 5));
                    cellObj.setCellStyle(valueRightCellStyle);
                    /*Preparmg style params*/
                    styleParams = new CellStyleParamters();
//                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory().toLowerCase()))));
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                    if (pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory().length() <= 4) {
                        cellObj = rowObj.createCell(6);
                        cellObj.setCellValue(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory().toLowerCase()));
                        cellObj.setCellStyle(tempCellStyle);
                        cellObj = rowObj.createCell(7);
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 32));
                        cellObj.setCellStyle(valueCenterCellStyle);
                        cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.ORIGIN_SUMMARY_POSTFIX", null, Locale.US));
                    } else {
                        cellObj = rowObj.createCell(6);
                        cellObj.setCellValue(WordUtils.capitalize(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryCategory().toLowerCase()));
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, (short) 6, (short) 7));
                        cellObj.setCellStyle(tempCellStyle);
                        cellObj = rowObj.createCell(8);
                        sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 32));
                        cellObj.setCellStyle(valueCenterCellStyle);
                        cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.ORIGIN_SUMMARY_POSTFIX", null, Locale.US));

                    }
                    /*Orgin Data categorie*/

 /*FCP title*/
                    tempRowCount = ++rowCount;
                    rowObj = sheetObj.createRow(tempRowCount);
                    rowObj.setHeight((short) 500);

                    addImageInXSSFCell(workBookObj, sheetObj, rowObj, 0, tempRowCount, logoPath + pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPColor().toLowerCase() + ".png");

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.TRUE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.firstContentfulPaint", null, Locale.US) + " (FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 1, 7);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    /*FCP score*/
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getFieldDataFCPColor())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(8);
//                    cellObj.setCellValue(DECIMAL_FORMAT.format(pageSpeedInsightsService.convertMilliSecondIntoSeconds(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPPercentile())) + " s");
                    cellObj.setCellValue(convertMilliSecondIntoSeconds(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPPercentile()) + " s");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 8, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF title and score for fcp */

 /*FID title*/
                    addImageInXSSFCell(workBookObj, sheetObj, rowObj, 11, tempRowCount, logoPath + pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDColor().toLowerCase() + ".png");
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.TRUE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.firstInputDelay", null, Locale.US) + " (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 12, 19);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    /*FID score*/
                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDColor())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightTop");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(20);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDPercentile() + " ms");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 20, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF title and score for fcp */

 /*Fast message*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getFast())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPdistributions().get(pageSpeedInsightsService.getFast()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " fast (< 1 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getFast())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDdistributions().get(pageSpeedInsightsService.getFast()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + "  fast (< 50 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                    /*EOF of Fast*/

 /*Average*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getAverage())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPdistributions().get(pageSpeedInsightsService.getAverage()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " average (1 s ~ 2.5 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getAverage())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("left");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDdistributions().get(pageSpeedInsightsService.getAverage()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("right");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + "  average (50 ms ~ 250 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    /*EOF Average*/
 /*Slow*/
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getSlow())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFCPdistributions().get(pageSpeedInsightsService.getSlow()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " slow (>2.5 s) First Contentful Paint(FCP)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 2, 9);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getSlow())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("leftBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(12);
                    cellObj.setCellValue(pageSpeedInsights.getRealWorldFieldData().getOriginSummaryFIDdistributions().get(pageSpeedInsightsService.getSlow()) + "%");
                    cellObj.setCellStyle(tempCellStyle);

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                    styleParams.setHaveBorder(Boolean.TRUE);
                    styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                    styleParams.setBorderType("rightBottom");
                    /*getting tsyle object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    cellObj = rowObj.createCell(13);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.GRAPH_MSG", null, Locale.US) + " slow (>250 ms) First Input Delay (FID)");
                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 13, 21);
                    sheetObj.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(tempCellStyle);
                    appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                } else {
                    sheetObj.createRow(++rowCount);
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);
                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.NO_DATA_MSG", null, Locale.US) + " origin.");
                    sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 26));
                    cellObj.setCellStyle(valueCellStyle);
                }

                /*EOF of origin summary data*/
 /*Lab Data*/
                List<PageSpeedAuditRule> labDataRuleList = pageSpeedInsights.getAuditRules().stream().filter(rule -> (rule != null && rule.isLabDataFieldRule())).collect(Collectors.toList());
                if (labDataRuleList != null && !labDataRuleList.isEmpty()) {
                    LOGGER.debug("labDataRuleCount:: " + labDataRuleList.size());
                    for (int i = 0; i <= 1; i++) {
                        rowObj = sheetObj.createRow(++rowCount);
                        rowObj.setHeight((short) 100);
                    }
                    rowObj = sheetObj.createRow(++rowCount);
                    rowObj.setHeight((short) 500);
                    /*Lab data title*/
                    cellObj = rowObj.createCell(0);
                    rowObj.setHeight((short) 500);
                    cellObj.setCellValue("  " + messageSource.getMessage("PageSpeedInsights.LAB_DATA", null, Locale.US) + " :-");
                    sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                    cellObj.setCellStyle(subColHeaderStyle);
                    int count = 0;
                    int firstTableIndex = 1, secondTableIndex = 12, lastColumIndex = 0;
                    for (int i = 0; i < LAB_DATA_RULES_IDS.length; i++) {
                        if (i % 2 == 0) {
                            rowObj = sheetObj.createRow(++rowCount);
                            rowObj.setHeight((short) 500);
                        }
                        for (PageSpeedAuditRule ruleObj : labDataRuleList) {
                            if (ruleObj != null && ruleObj.getId().equalsIgnoreCase(LAB_DATA_RULES_IDS[i])) {
                                /*Rule title*/
                                if (i % 2 == 0) {
                                    cellObj = rowObj.createCell(firstTableIndex);
                                } else {
                                    cellObj = rowObj.createCell(secondTableIndex);
                                }
                                cellObj.setCellValue(ruleObj.getTitle());
                                ++count;
                                if (i % 2 == 0) {
                                    lastColumIndex = firstTableIndex + 6;
                                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, firstTableIndex, lastColumIndex);//1 to 7
                                } else {
                                    lastColumIndex = secondTableIndex + 7;
                                    cellRangeAddress = new CellRangeAddress(rowCount, rowCount, secondTableIndex, lastColumIndex);//12 to 19
                                }
                                sheetObj.addMergedRegion(cellRangeAddress);
                                styleParams = new CellStyleParamters();
                                styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                                styleParams.setHaveBorder(Boolean.TRUE);
                                styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                                switch (count) {
                                    case 1:
                                    case 3:
                                        styleParams.setBorderType("leftTop");
                                        break;
                                    case 9:
                                    case 11:
                                        styleParams.setBorderType("leftBottom");
                                        break;
                                    default:
                                        styleParams.setBorderType("left");
                                        break;
                                }
                                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                                cellObj.setCellStyle(tempCellStyle);
                                appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);

                                /*Rule score*/
                                cellObj = rowObj.createCell(lastColumIndex + 1);
                                cellObj.setCellValue(ruleObj.getDisplayValue());
                                ++count;
                                cellRangeAddress = new CellRangeAddress(rowCount, rowCount, lastColumIndex + 1, lastColumIndex + 2);//8 to 9
                                sheetObj.addMergedRegion(cellRangeAddress);
                                styleParams = new CellStyleParamters();
                                styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(ruleObj.getScoreColor())));
                                styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
                                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                                styleParams.setHaveBorder(Boolean.TRUE);
                                styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
                                switch (count) {
                                    case 2:
                                    case 4:
                                        styleParams.setBorderType("rightTop");
                                        break;
                                    case 10:
                                    case 12:
                                        styleParams.setBorderType("rightBottom");
                                        break;
                                    default:
                                        styleParams.setBorderType("right");
                                        break;
                                }
                                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                                cellObj.setCellStyle(tempCellStyle);
                                appplyBorderToMergedCells(cellRangeAddress, sheetObj, workBookObj, styleParams);
                                lastColumIndex = 0;
                                firstTableIndex = 1;
                                secondTableIndex = 12;
                            }
                        }
                    }
                    /*EOF labdata*/
 /*Adding the rules to sheet*/
                    sheetObj.createRow(++rowCount);
                }
                String categorieTag = null;
                if (pageSpeedInsights.getAuditRules() != null) {
                    List<PageSpeedAuditRule> pageSpeedAuditRuleList = pageSpeedInsights.getAuditRules().stream().filter(rule -> (!rule.isLabDataFieldRule() && rule.getAuditCategorie() != null && rule.getAuditCategorie().equals(pageSpeedInsightsService.getOpportunities()))).sorted(Comparator.comparingInt(PageSpeedAuditRule::getRulePriority).reversed()).collect(Collectors.toList());
                    Collections.sort(pageSpeedAuditRuleList, (PageSpeedAuditRule p1, PageSpeedAuditRule p2) -> Double.compare(p2.getOverallSavingsMs(), p1.getOverallSavingsMs()));
                    if (pageSpeedAuditRuleList != null && !pageSpeedAuditRuleList.isEmpty()) {
                        categorieTag = pageSpeedInsights.getLoadOpportunitiesDescReport();
                        if (categorieTag == null) {
                            categorieTag = messageSource.getMessage("PageSpeedInsights.Opportunities.Def.Report", null, Locale.US);
                        }
                        rowCount = addRuleCategorieSecion(workBookObj, sheetObj, pageSpeedInsightsService.getOpportunities(), categorieTag, pageSpeedAuditRuleList, ++rowCount, commonStyleObj);
                        sheetObj.createRow(++rowCount);
                    }
                    pageSpeedAuditRuleList = pageSpeedInsights.getAuditRules().stream().filter(rule -> (!rule.isLabDataFieldRule() && rule.getAuditCategorie() != null && rule.getAuditCategorie().equals(pageSpeedInsightsService.getDiagnostics()))).sorted(Comparator.comparingInt(PageSpeedAuditRule::getRulePriority).reversed()).collect(Collectors.toList());
                    if (pageSpeedAuditRuleList != null && !pageSpeedAuditRuleList.isEmpty()) {
                        categorieTag = pageSpeedInsights.getDiagnosticsDescReport();
                        if (categorieTag == null) {
                            categorieTag = messageSource.getMessage("PageSpeedInsights.Diagnostics.Def.Report", null, Locale.US);
                        }
                        rowCount = addRuleCategorieSecion(workBookObj, sheetObj, pageSpeedInsightsService.getDiagnostics(), categorieTag, pageSpeedAuditRuleList, ++rowCount, commonStyleObj);
                        sheetObj.createRow(++rowCount);
                    }
                    pageSpeedAuditRuleList = pageSpeedInsights.getAuditRules().stream().filter(rule -> (!rule.isLabDataFieldRule() && rule.getAuditCategorie() != null && rule.getAuditCategorie().equals(pageSpeedInsightsService.getPassed()))).sorted(Comparator.comparingInt(PageSpeedAuditRule::getScore).reversed()).collect(Collectors.toList());
                    if (pageSpeedAuditRuleList != null && !pageSpeedAuditRuleList.isEmpty()) {
                        addRuleCategorieSecion(workBookObj, sheetObj, pageSpeedInsightsService.getPassed(), null, pageSpeedAuditRuleList, ++rowCount, commonStyleObj);
                    }
                }
            } else {
                /*Displaying error message*/
 /*Preparmg style params*/
                String errorMessage = null;
                if (pageSpeedInsights.getFinalUrl() != null && pageSpeedInsights.getFinalUrl().trim().contains("URL cannot be reviewed")) {
                    errorMessage = messageSource.getMessage("URL.report.error", null, Locale.US);
                } else {
                    errorMessage = pageSpeedInsights.getFinalUrl();
                }
                rowObj = sheetObj.createRow(++rowCount);
                rowObj.setHeight((short) 800);
                styleParams = new CellStyleParamters();
                styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 11, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                cellObj = rowObj.createCell(1);
                cellObj.setCellValue(errorMessage);
                sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 18));
                cellObj.setCellStyle(tempCellStyle);
            }

        }
    }

    private int addRuleCategorieSecion(Workbook workBookObj, Sheet sheet, String ruleCategorie, String ruleTag, List<PageSpeedAuditRule> pageSpeedAuditRuleList, int currentRowCount, Map<String, CellStyle> commonStyleObj) {
        int innerRowCount = currentRowCount;
        Row rowObj = null;
        Cell cellObj = null;
        CellStyle subColHeaderStyle = null, valueCellStyle = null, tempCellStyle = null;
        if (commonStyleObj != null) {
            if (commonStyleObj.containsKey("subColHeaderStyle")) {
                subColHeaderStyle = commonStyleObj.get("subColHeaderStyle");
            }
            if (commonStyleObj.containsKey("valueCellStyle")) {
                valueCellStyle = commonStyleObj.get("valueCellStyle");
            }
        }
        CellStyleParamters styleParams = null;
        CellStyle alignTopCellStyle = null;
        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
        alignTopCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

//        CellRangeAddress cellRangeAddress = null;
        /*Rule Categorie title*/
        rowObj = sheet.createRow(innerRowCount);
        rowObj.setHeight((short) 900);

        /*Title*/
        cellObj = rowObj.createCell(0);
        cellObj.setCellValue("  " + ruleCategorie);
        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 0, 2));
        cellObj.setCellStyle(subColHeaderStyle);

        /*Tagline*/
        cellObj = rowObj.createCell(3);
        if (ruleTag != null && !ruleCategorie.equals(pageSpeedInsightsService.getPassed())) {
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
            sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 3, 36));
            cellObj.setCellStyle(tempCellStyle);
            cellObj.setCellValue(" - " + ruleTag);
        } else {
            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 18, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getNoData())));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
            cellObj.setCellValue("(" + pageSpeedAuditRuleList.size() + ")");
            sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 3, 4));
            cellObj.setCellStyle(tempCellStyle);
        }
        boolean isErrorRule = Boolean.FALSE;
        XSSFFont ruleTitleFont = getRequeriedXSSFFont(workBookObj, (short) 13.5, Boolean.TRUE, new XSSFColor(Color.BLACK));
        int tempRowCount = 0;
        /*Iterating Rules list*/
        for (PageSpeedAuditRule ruleObj : pageSpeedAuditRuleList) {

            isErrorRule = (ruleObj.getErrorMessage() != null && !ruleObj.getErrorMessage().trim().isEmpty()) ? Boolean.TRUE : Boolean.FALSE;

            /*Rule Title*/
            tempRowCount = ++innerRowCount;
            rowObj = sheet.createRow(tempRowCount);
            rowObj.setHeight((short) 600);

            if (isErrorRule) {

                styleParams = new CellStyleParamters();
                styleParams.setFont(ruleTitleFont);
                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                cellObj = rowObj.createCell(1);
                cellObj.setCellValue(ruleObj.getTitle());
                sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 8));
                cellObj.setCellStyle(tempCellStyle);
            } else {
                addImageInXSSFCell(workBookObj, sheet, rowObj, 0, tempRowCount, logoPath + ruleObj.getScoreColor().toLowerCase() + ".png");
                switch (ruleCategorie) {
                    case "Opportunities":
                        styleParams = new CellStyleParamters();
                        styleParams.setFont(ruleTitleFont);
                        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                        /*getting tsyle object*/
                        tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                        cellObj = rowObj.createCell(1);
                        cellObj.setCellValue(ruleObj.getTitle());
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 7));
                        cellObj.setCellStyle(tempCellStyle);

//                        if (ruleObj.getOverallSavingsMs() > 0) {
                        /*ESTIMATED_SAVINGS*/
                        rowObj = sheet.createRow(++innerRowCount);
                        rowObj.setHeight((short) 500);

                        styleParams = new CellStyleParamters();
                        styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                        /*getting tsyle object*/
                        tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                        cellObj = rowObj.createCell(1);
                        cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.ESTIMATED_SAVINGS", null, Locale.US) + ": ");
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 3));
                        cellObj.setCellStyle(tempCellStyle);

                        styleParams = new CellStyleParamters();
                        styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(ruleObj.getScoreColor())));
                        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                        /*getting tsyle object*/
                        tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                        cellObj = rowObj.createCell(4);
//                            cellObj.setCellValue(convertMicroSecondIntoSeconds(ruleObj.getOverallSavingsMs()) + " s");
                        cellObj.setCellValue(convertMilliSecondIntoSeconds(ruleObj.getOverallSavingsMs()) + " s");
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 4, 8));
                        cellObj.setCellStyle(tempCellStyle);
                        /*EOF ESTIMATED_SAVINGS*/
//                        }
                        break;
                    case "Diagnostics":
                        styleParams = new CellStyleParamters();
                        styleParams.setFont(ruleTitleFont);
                        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                        /*getting tsyle object*/
                        tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                        cellObj = rowObj.createCell(1);
                        cellObj.setCellValue(ruleObj.getTitle());
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 8));
                        cellObj.setCellStyle(tempCellStyle);

                        if (ruleObj.getOverallSavingsMs() > 0 || ruleObj.getDisplayValue() != null) {
                            styleParams = new CellStyleParamters();
                            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(ruleObj.getScoreColor())));
                            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                            /*getting tsyle object*/
                            tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                            cellObj = rowObj.createCell(9);
//                            cellObj.setCellValue(" - " + (ruleObj.getOverallSavingsMs() > 0 ? (convertMilliSecondIntoSeconds((long) ruleObj.getOverallSavingsMs()) + " s") : ruleObj.getDisplayValue()));
                            cellObj.setCellValue(ruleObj.getDisplayValue());
                            sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 9, 14));
                            cellObj.setCellStyle(tempCellStyle);
                        }
                        break;
                    case "PassedAudits":
                        styleParams = new CellStyleParamters();
                        styleParams.setFont(ruleTitleFont);
                        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                        /*getting tsyle object*/
                        tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                        cellObj = rowObj.createCell(1);
                        cellObj.setCellValue(ruleObj.getTitle());
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 8));
                        cellObj.setCellStyle(tempCellStyle);

                        if (ruleObj.getDisplayValue() != null) {
                            styleParams = new CellStyleParamters();
                            styleParams.setFont(getRequeriedXSSFFont(workBookObj, (short) 16, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getFast())));
                            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                            /*getting tsyle object*/
                            tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                            cellObj = rowObj.createCell(9);
                            cellObj.setCellValue(" - " + ruleObj.getDisplayValue());
                            sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 9, 14));
                            cellObj.setCellStyle(tempCellStyle);
                        }
                }
            }
            /*EOF of title*/
            if (!isErrorRule) {
                if (ruleObj.getWarnings() != null && !ruleObj.getWarnings().isEmpty()) {
                    /*Rule warning title*/
                    rowObj = sheet.createRow(++innerRowCount);
                    rowObj.setHeight((short) 600);
                    cellObj = rowObj.createCell(1);
                    cellObj.setCellValue("Warning");
                    sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 5));
                    cellObj.setCellStyle(subColHeaderStyle);
                    /*EOF of title*/

                    styleParams = new CellStyleParamters();
                    styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, getColorIndex(pageSpeedInsightsService.getAverage())));
                    styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                    styleParams.setVerticalPostion(VerticalAlignment.JUSTIFY);
                    /*getting style object*/
                    tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);

                    for (String warning : ruleObj.getWarnings()) {

                        /*Rule warning title*/
                        rowObj = sheet.createRow(++innerRowCount);
                        rowObj.setHeight((short) 900);

                        cellObj = rowObj.createCell(1);
                        cellObj.setCellValue(warning);
//                        sheet.addMergedRegion(warning.length() < 300 ? new CellRangeAddress(innerRowCount, innerRowCount, 1, 15) : new CellRangeAddress(innerRowCount, innerRowCount, 1, 18));
                        sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 18));
                        cellObj.setCellStyle(tempCellStyle);
                        /*EOF of title*/
                    }
                }/*EOF of Warning*/

 /*Rule Descripiton*/
                if (ruleObj.getDescription() != null) {
                    rowObj = sheet.createRow(++innerRowCount);
                    rowObj.setHeight((short) 800);
                    cellObj = rowObj.createCell(1);
//                    sheet.addMergedRegion(ruleObj.getDescription().length() < 300 ? new CellRangeAddress(innerRowCount, innerRowCount, 1, 15) : new CellRangeAddress(innerRowCount, innerRowCount, 1, 25));
                    sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 26));
                    cellObj.setCellStyle(alignTopCellStyle);
                    cellObj.setCellValue(ruleObj.getDescription());
                }

                /*EOF of Descripiton*/
                if (ruleObj.getStackPackInfo() != null) {

                    /*StackPack info*/
                    tempRowCount = ++innerRowCount;

                    rowObj = sheet.createRow(tempRowCount);
                    rowObj.setHeight((short) 800);
                    /*Stackpack image*/
                    addImageInXSSFCell(workBookObj, sheet, rowObj, 1, tempRowCount, logoPath + "wordpress.png");

                    /*Stackpack Desc*/
                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue(ruleObj.getStackPackInfo());
//                    sheet.addMergedRegion(ruleObj.getStackPackInfo().length() < 300 ? new CellRangeAddress(innerRowCount, innerRowCount, 1, 15) : new CellRangeAddress(innerRowCount, innerRowCount, 1, 21));
                    sheet.addMergedRegion(new CellRangeAddress(tempRowCount, tempRowCount, 2, 18));
                    cellObj.setCellStyle(valueCellStyle);
                    /*EOF of StackPack Info Descripiton*/
                }

                if (ruleObj.getTableHeadersRow() != null && !ruleObj.getTableHeadersRow().isEmpty()) {
                    /*Adding the table section of rule to sheet*/
                    innerRowCount = addTableSectionofRuleToSheet(workBookObj, sheet, ruleObj, ++innerRowCount);
                    sheet.createRow(++innerRowCount);
                } else if (ruleObj.getCriticalRequestChain() != null && !ruleObj.getCriticalRequestChain().isEmpty()) {
                    innerRowCount = addCriticalRequestChainSection(workBookObj, sheet, ruleObj, ++innerRowCount);
                    sheet.createRow(++innerRowCount);
                }

            } else {
                /*Error Title*/
                rowObj = sheet.createRow(++innerRowCount);
                rowObj.setHeight((short) 500);

                styleParams = new CellStyleParamters();
                styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.TRUE, getColorIndex(pageSpeedInsightsService.getSlow())));
                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                /*Error title*/
                cellObj = rowObj.createCell(1);
                cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.ERROR", null, Locale.US));
                cellObj.setCellStyle(tempCellStyle);
                /*EOF of title*/

                styleParams = new CellStyleParamters();
                styleParams.setFont(getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
                styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
                styleParams.setVerticalPostion(VerticalAlignment.CENTER);
                /*getting tsyle object*/
                tempCellStyle = getRequriedXSSFCellStyle(workBookObj, styleParams);
                /*Error description*/
                cellObj = rowObj.createCell(2);
                cellObj.setCellValue(ruleObj.getErrorMessage());
                sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 2, 18));
                cellObj.setCellStyle(tempCellStyle);

                if (ruleObj.getDescription() != null) {
                    rowObj = sheet.createRow(++innerRowCount);
                    rowObj.setHeight((short) 800);
                    cellObj = rowObj.createCell(1);
                    sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 26));
                    cellObj.setCellStyle(alignTopCellStyle);
                    cellObj.setCellValue(ruleObj.getDescription());
                }
                /*EOF of description*/
            }
        }/*EOF of iteration*/
        return ++innerRowCount;
    }

    private int addTableSectionofRuleToSheet(Workbook workBook, Sheet sheet, PageSpeedAuditRule ruleObj, int currentRowCount) {
        ArrayList<AuditHeadings> tableHeadersRow = ruleObj.getTableHeadersRow();
        int innerRowCount = currentRowCount;
        sheet.createRow(++innerRowCount);
        Row rowObj = null;
        Cell cellObj = null;
        CellStyle childColLeftStyle = null, childRightStyle = null, headerColStyle = null, childCenterStyle = null, headerRightColStyle = null;
        CellStyleParamters styleParams = null;
        CellRangeAddress cellRangeAddress = null;
        /*Rule Categorie title*/
        rowObj = sheet.createRow(innerRowCount);
        rowObj.setHeight((short) 500);

        /*Style related to table header row text*/
        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBook, (short) 11, Boolean.TRUE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
        styleParams.setHaveBorder(Boolean.TRUE);
        styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
        styleParams.setBorderType("all");
        headerColStyle = getRequriedXSSFCellStyle(workBook, styleParams);

        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBook, (short) 11, Boolean.TRUE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
        styleParams.setHaveBorder(Boolean.TRUE);
        styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
        styleParams.setBorderType("all");
        headerRightColStyle = getRequriedXSSFCellStyle(workBook, styleParams);

        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBook, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
        styleParams.setVerticalPostion(VerticalAlignment.TOP);
        styleParams.setHaveBorder(Boolean.TRUE);
        styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
        styleParams.setBorderType("all");
        childColLeftStyle = getRequriedXSSFCellStyle(workBook, styleParams);

        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBook, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
        styleParams.setHaveBorder(Boolean.TRUE);
        styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
        styleParams.setBorderType("all");
        childRightStyle = getRequriedXSSFCellStyle(workBook, styleParams);

        styleParams = new CellStyleParamters();
        styleParams.setFont(getRequeriedXSSFFont(workBook, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK)));
        styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
        styleParams.setVerticalPostion(VerticalAlignment.CENTER);
        styleParams.setHaveBorder(Boolean.TRUE);
        styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
        styleParams.setBorderType("all");
        childCenterStyle = getRequriedXSSFCellStyle(workBook, styleParams);

        /*Adding table header*/
        int startColCount = 1;
        int endColCount = 0;

        for (AuditHeadings auditHeadings : tableHeadersRow) {
            if (!auditHeadings.getItemType().equalsIgnoreCase("thumbnail")) {
                startColCount = (startColCount == 0) ? startColCount : (++endColCount);
                cellObj = rowObj.createCell(startColCount);
                cellObj.setCellValue((auditHeadings.getText() != null && !auditHeadings.getText().equals(pageSpeedInsightsService.getNone())) ? auditHeadings.getText() : "     ");
                if (auditHeadings.getItemType().equalsIgnoreCase("link") || auditHeadings.getItemType().equalsIgnoreCase("url")
                        || auditHeadings.getItemType().equalsIgnoreCase("text") || auditHeadings.getItemType().equalsIgnoreCase("code")) {
//                if (auditHeadings.getText().equalsIgnoreCase("url") || auditHeadings.getText().equalsIgnoreCase("entity")) {
                    endColCount = startColCount + 9;
                    cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, startColCount, endColCount);
                } else {
                    endColCount = startColCount + 1;
                    cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, startColCount, endColCount);
                }
                sheet.addMergedRegion(cellRangeAddress);
                if (auditHeadings.getItemType().equalsIgnoreCase("link") || auditHeadings.getItemType().equalsIgnoreCase("url")
                        || auditHeadings.getItemType().equalsIgnoreCase("text") || auditHeadings.getItemType().equalsIgnoreCase("code")) {
                    cellObj.setCellStyle(headerColStyle);
                } else {
                    cellObj.setCellStyle(headerRightColStyle);
                }
                appplyBorderToMergedCells(cellRangeAddress, sheet, workBook, styleParams);
                ++startColCount;
            }
        }

        startColCount = 1;
        endColCount = 0;
        String alignType = "left";
        if (true) {
            int headerColumIndex = -1;
            for (AuditItems auditItems : ruleObj.getTableChildRow()) {
                rowObj = sheet.createRow(++innerRowCount);
                rowObj.setHeight((short) 700);
                for (AuditHeadings auditHeadings : tableHeadersRow) {
                    try {
                        if (auditHeadings != null) {
                            if (!auditHeadings.getItemType().equalsIgnoreCase("thumbnail")) {
                                headerColumIndex = auditHeadings.getHeaderColumnIndex();
//                                LOGGER.error("headerColumIndex:: " + headerColumIndex);
                                startColCount = (startColCount == 0) ? startColCount : (++endColCount);
                                cellObj = rowObj.createCell(startColCount);
                                try {
                                    switch (headerColumIndex) {
                                        case 0:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn0(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn0() != null ? (NumberUtils.isCreatable(auditItems.getColumn0()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        case 1:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn1(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn1() != null ? (NumberUtils.isCreatable(auditItems.getColumn1()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        case 2:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn2(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn2() != null ? (NumberUtils.isCreatable(auditItems.getColumn2()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        case 3:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn3(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn3() != null ? (NumberUtils.isCreatable(auditItems.getColumn3()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        case 4:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn4(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn4() != null ? (NumberUtils.isCreatable(auditItems.getColumn4()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        case 5:
                                            cellObj.setCellValue(getBodyContentOfRow(auditItems.getColumn5(), auditHeadings.getItemType()));
                                            if (auditHeadings.getKey().equalsIgnoreCase("cacheLifetimeMs")) {
                                                alignType = "right";
                                            } else {
                                                alignType = auditItems.getColumn5() != null ? (NumberUtils.isCreatable(auditItems.getColumn5()) ? "right" : "left") : "center";
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                } catch (Exception e) {
                                    LOGGER.error("Exeption in adding colum content for coloum: " + headerColumIndex + ", for rule:: " + ruleObj.getTitle());
                                }
                                if (auditHeadings.getItemType().equalsIgnoreCase("link") || auditHeadings.getItemType().equalsIgnoreCase("url")
                                        || auditHeadings.getItemType().equalsIgnoreCase("text") || auditHeadings.getItemType().equalsIgnoreCase("code")) {
                                    endColCount = startColCount + 9;
                                    cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, startColCount, endColCount);
                                } else {
                                    endColCount = startColCount + 1;
                                    cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, startColCount, endColCount);
                                }
                                sheet.addMergedRegion(cellRangeAddress);
                                switch (alignType) {
                                    case "right":
                                        cellObj.setCellStyle(childRightStyle);
                                        break;
                                    case "center":
                                        cellObj.setCellStyle(childCenterStyle);
                                        break;
                                    default:
                                        cellObj.setCellStyle(childColLeftStyle);
                                        break;
                                }
                                appplyBorderToMergedCells(cellRangeAddress, sheet, workBook, styleParams);
                                ++startColCount;
                                alignType = "left";
                            }
                        }
                        headerColumIndex = -1;
                    } catch (Exception e) {
                        LOGGER.error("Exeption in adding colum content for coloum: " + headerColumIndex + ", for rule:: " + ruleObj.getTitle());
                    }
                }
                startColCount = 1;
                endColCount = 0;
            }
        }
        return innerRowCount;
    }

    private String getBodyContentOfRow(String columnValue, String itemType) {
        String cellValue = "";
        if (columnValue != null) {
            columnValue = columnValue.trim();
            columnValue = getTextFitIntoCell(columnValue);
            itemType = itemType.trim();
            if (NumberUtils.isCreatable(columnValue)) {
                if (itemType != null) {
                    cellValue = addUnitToValueInCell(columnValue, itemType).trim();
                } else {
                    cellValue = columnValue;
                }
            } else {
                if (columnValue.contains("<\"") && columnValue.contains("\">")) {
                    columnValue = columnValue.trim();
                    columnValue = columnValue.replaceAll("<\"", "");
                    columnValue = columnValue.replaceAll("\">", "");
                    cellValue = columnValue;
                } else {
                    cellValue = columnValue;
                }
            }
        } else {
            cellValue = "-";
        }
        return cellValue;
    }

    private int addCriticalRequestChainSection(Workbook workBook, Sheet sheet, PageSpeedAuditRule ruleObj, int currentRowCount) {
        int innerRowCount = 0;
        try {
            innerRowCount = currentRowCount;
            Row rowObj = null;
            Cell cellObj = null;
            CellStyle childColLeftBorderStyle = null, childColRightBorderStyle = null, tempCellStyle = null;
            CellStyleParamters styleParams = null;
            CellRangeAddress cellRangeAddress = null;
            /*Rule Categorie title*/
            rowObj = sheet.createRow(innerRowCount);
            rowObj.setHeight((short) 500);

            /*Style related to table header row text*/
            styleParams = new CellStyleParamters();
            XSSFFont fontObj = getRequeriedXSSFFont(workBook, (short) 14, Boolean.FALSE, getColorIndex(pageSpeedInsightsService.getNone()));
            fontObj.setItalic(Boolean.TRUE);
            styleParams.setFont(fontObj);
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            tempCellStyle = getRequriedXSSFCellStyle(workBook, styleParams);

            cellObj = rowObj.createCell(1);
            cellObj.setCellValue(messageSource.getMessage("PageSpeedInsights.INITIAL_NAVIGATION", null, Locale.US));
            sheet.addMergedRegion(new CellRangeAddress(innerRowCount, innerRowCount, 1, 3));
            cellObj.setCellStyle(tempCellStyle);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBook, (short) 12, Boolean.TRUE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.TOP);
            styleParams.setHaveBorder(Boolean.TRUE);
            styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
            styleParams.setBorderType("leftBottom");
            childColLeftBorderStyle = getRequriedXSSFCellStyle(workBook, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBook, (short) 12, Boolean.TRUE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.TOP);
            styleParams.setHaveBorder(Boolean.TRUE);
            styleParams.setBorderColorIndex(new XSSFColor(Color.BLACK).getIndex());
            styleParams.setBorderType("left");
            tempCellStyle = getRequriedXSSFCellStyle(workBook, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(getRequeriedXSSFFont(workBook, (short) 12, Boolean.FALSE, new XSSFColor(Color.BLACK)));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.TOP);
            childColRightBorderStyle = getRequriedXSSFCellStyle(workBook, styleParams);

            /*Adding root domain first && follwed by child resource URLs*/
            for (CriticalRequestChain chainObj : ruleObj.getCriticalRequestChain()) {
                if (chainObj.getUrlType().equalsIgnoreCase(CriticalRequestChain.HOST_URL)) {
                    rowObj = sheet.createRow(++innerRowCount);
                    rowObj.setHeight((short) 700);
                    cellObj = rowObj.createCell(2);
                    cellObj.setCellValue("----------");
                    cellObj.setCellStyle(childColLeftBorderStyle);

                    cellObj = rowObj.createCell(3);
                    cellObj.setCellValue(getTextFitIntoCell(chainObj.getUrl()));
                    cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, 3, 13);
                    sheet.addMergedRegion(cellRangeAddress);
                    cellObj.setCellStyle(childColRightBorderStyle);

                    /*cellObj = rowObj.createCell(14);
                cellObj.setCellValue("--> ".concat(convertIntoMilliSeconds(chainObj.getStartTime(), chainObj.getEndTime()).concat(", ").concat(convertInToKibiByte(chainObj.getTransferSize()))));
                cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, 14, 16);
                sheet.addMergedRegion(cellRangeAddress);
                cellObj.setCellStyle(cellBoldStyle);*/
                }
                if (chainObj.getUrlType().equalsIgnoreCase(CriticalRequestChain.CHILDERN_URL)) {
                    try {
                        rowObj = sheet.createRow(++innerRowCount);
                        rowObj.setHeight((short) 700);
                        cellObj = rowObj.createCell(3);
                        cellObj.setCellValue("----------");
                        cellObj.setCellStyle(tempCellStyle);

                        cellObj = rowObj.createCell(4);
                        cellObj.setCellValue(getTextFitIntoCell(chainObj.getUrl()));
                        cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, 4, 13);
                        sheet.addMergedRegion(cellRangeAddress);
                        cellObj.setCellStyle(childColRightBorderStyle);

                        cellObj = rowObj.createCell(14);
                        cellObj.setCellValue("--> ".concat(convertIntoMilliSeconds(chainObj.getStartTime(), chainObj.getEndTime()).concat(", ").concat(convertInToKibiByte(chainObj.getTransferSize()))));
//                cellObj.setCellValue("-- ".concat(convertIntoMilliSeconds(chainObj.getStartTime(),chainObj.getEndTime()).concat(", ").concat(convertInToKibiByte(chainObj.getTransferSize()))));
                        cellRangeAddress = new CellRangeAddress(innerRowCount, innerRowCount, 14, 16);
                        sheet.addMergedRegion(cellRangeAddress);
                        cellObj.setCellStyle(childColRightBorderStyle);
                    } catch (Exception e) {
                        LOGGER.error("Exception in adding criticla request child chain data to cell cause:: ", e);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in addCriticalRequestChainSection cause:: ", e);
        }
        return innerRowCount;
    }

//    private String getCriticalRequestChainText(CriticalRequestChain criticalRequestChain) {
//        String cellValue = criticalRequestChain.getUrl().concat("   ====>   ").concat(convertIntoMilliSeconds(criticalRequestChain.getResponseReceivedTime())).concat(", ").concat(convertInToKibiByte(criticalRequestChain.getTransferSize()));
//        return cellValue;
//    }
    private String convertIntoMilliSeconds(double startTime, double endTime) {
        return endTime > 0 ? DECIMAL_FORMAT.format(Math.ceil((endTime - startTime) * 1000)) + " ms" : DECIMAL_FORMAT.format(Math.ceil((startTime / 1000))) + " ms";
    }

    private String convertInToKibiByte(double transferSize) {
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        return decimalFormat.format(((transferSize) / pageSpeedInsightsService.getKibibyes())).concat(" KB");
    }

    private CellStyle getRequriedXSSFCellStyle(Workbook workBook, CellStyleParamters styleParams) {
        CellStyle cellStyle = (XSSFCellStyle) workBook.createCellStyle();
        cellStyle.setFont(styleParams.getFont());
        cellStyle.setWrapText(Boolean.TRUE);
        cellStyle.setAlignment(styleParams.getHorizontalPosition());
        cellStyle.setVerticalAlignment(styleParams.getVerticalPostion());
        if (styleParams.getForeGroundColorIndex() > 0) {
            cellStyle.setFillForegroundColor(styleParams.getForeGroundColorIndex());
        }
        if (styleParams.isFillPattern()) {
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        if (styleParams.isHaveBorder()) {
            switch (styleParams.getBorderType()) {
                case "all":
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "top":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "bottom":
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "left":
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "right":
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "leftBottom":
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "leftTop":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderLeft(BorderStyle.THIN);
                    cellStyle.setLeftBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "rightBottom":
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderBottom(BorderStyle.THIN);
                    cellStyle.setBottomBorderColor(styleParams.getBorderColorIndex());
                    break;
                case "rightTop":
                    cellStyle.setBorderTop(BorderStyle.THIN);
                    cellStyle.setTopBorderColor(styleParams.getBorderColorIndex());
                    cellStyle.setBorderRight(BorderStyle.THIN);
                    cellStyle.setRightBorderColor(styleParams.getBorderColorIndex());
                    break;
                default:
                    break;
            }
        }
        return cellStyle;
    }

    /*This methods create both N rows && cells and append text also merge rows, cells
    I.e XSSFSheet, XSSFRow, XSSFCell*/
    private int createMergedRowsAndCellsAddCellValue(String cellValue, Sheet sheet, CellStyle cellStyle, int startRowCount, int endRowCount, short startCellCount, short endCellCount) {
        Row row = null;
        Cell cell;
        int currnetRow = startRowCount;
        for (int i = startRowCount; i <= endRowCount; ++i) {
            row = sheet.createRow(i);
            ++currnetRow;
            for (int j = startCellCount; j <= endCellCount; j++) {
                cell = row.createCell(j);
                cell.setCellStyle(cellStyle);
                if (i == startRowCount && j == startCellCount) {
                    cell.setCellValue(cellValue);
                }
            }
        }
        sheet.addMergedRegion(new CellRangeAddress(startRowCount, endRowCount, startCellCount, endCellCount));
        return currnetRow;
    }

    /*This methods create cells in created rows and appends text also merge cells
    I.e XSSFSheet, XSSFRow, XSSFCell*/
    private int createMergedCellsAddCellValue(String cellValue, Sheet sheet, CellStyle cellStyle, int startRowCount, int endRowCount, short startCellCount, short endCellCount) {
        Row row = null;
        int cuurentCellcount = startCellCount;
        for (int rowNum = startRowCount; rowNum <= endRowCount; ++rowNum) {
            row = sheet.getRow(rowNum);
            if (row == null) {
                sheet.createRow(rowNum);
//                LOGGER.error("while check row " + rowNum + " was created");
            }
            for (int colNum = startCellCount; colNum <= endCellCount; colNum++) {
                Cell currentCell = null;
                if (row != null) {
                    currentCell = row.getCell(colNum);
                    cuurentCellcount++;
                    if (currentCell == null) {
                        currentCell = row.createCell(colNum);
//                        LOGGER.error("while check cell " + rowNum + ":" + colNum + " was created");
                    }
                    currentCell.setCellStyle(cellStyle);
                    if (colNum == startCellCount) {
                        currentCell.setCellValue(cellValue);
                    }
                }
            }
        }
        if (startCellCount != endCellCount) {
            sheet.addMergedRegion(new CellRangeAddress(startRowCount, endRowCount, startCellCount, endCellCount));
        }
        return cuurentCellcount;
    }

    private void appplyBorderToMergedCells(CellRangeAddress cellRangeAddress, Sheet sheet, Workbook workBook, CellStyleParamters styleParams) {
        switch (styleParams.getBorderType()) {
            case "all":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "top":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "bottom":
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "left":
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "right":
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "leftBottom":
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "leftTop":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "rightBottom":
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            case "rightTop":
                RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
                RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
                break;
            default:
                break;
        }
    }

    private XSSFFont getRequeriedXSSFFont(Workbook workBook, short fontSize, boolean isBold, XSSFColor color) {
        XSSFFont fontObj = (XSSFFont) workBook.createFont();
        fontObj.setBold(isBold);
        fontObj.setFontHeightInPoints(fontSize);
        if (color != null) {
            fontObj.setColor(color);
        }
        fontObj.setFontName(messageSource.getMessage("PageSpeedInsights.FONT_STYLE", null, Locale.US));
//        fontObj.setFontName("Times New Roman");
        return fontObj;
    }

    /*  slow: hsl(3, 88%, 60%), rgb(243, 74, 65)
        average: hsl(34, 96%, 58%), rgb(251, 164, 47)
        fast: hsl(126, 54%, 60%), rgb(97, 208, 108), 
        No data: hsl(0, 0%, 75%),rgb(190, 190, 190)
     */
    private XSSFColor getColorIndex(String colorType) {
        XSSFColor xssfColor = null;
        switch (colorType) {
            case "fast":
                xssfColor = new XSSFColor(new java.awt.Color(97, 208, 108));
                break;
            case "average":
                xssfColor = new XSSFColor(new java.awt.Color(251, 164, 47));
                break;
            case "slow":
                xssfColor = new XSSFColor(new java.awt.Color(243, 74, 65));
                break;
            default:
                xssfColor = new XSSFColor(new java.awt.Color(117, 117, 117));
                break;
        }
        return xssfColor;
    }

    private String addUnitToValueInCell(String cellValue, String unitType) {
        String unit = "";
        if (unitType.equalsIgnoreCase("bytes")) {
            try {
                unit = String.valueOf(Math.ceil(Double.parseDouble(cellValue) / pageSpeedInsightsService.getKibibyes())) + " KB";
            } catch (Exception e) {
                LOGGER.info("The cellValue::" + cellValue + ", unitType::" + unitType);
                LOGGER.error("Exception in in calcuting bytes:: " + e.getMessage());
            }
        } else if (unitType.equalsIgnoreCase("ms") || unitType.equalsIgnoreCase("timespanMs")) {
            try {
                unit = String.valueOf(Math.ceil(Double.parseDouble(cellValue))) + " ms";
            } catch (Exception e) {
                LOGGER.info("The cellValue::" + cellValue + ", unitType::" + unitType);
                LOGGER.error("Exception in in calcuting ms::" + e.getMessage());

            }
        } else {
            LOGGER.debug("The cellValue::" + cellValue + ", unitType::" + unitType);
            unit = cellValue;
        }
        return unit;
    }

    /*XSSFWorkbook*/
    public boolean appendImageToExcellCell(Workbook workBook, Sheet sheet, short col1, int row1, short col2, int row2, String imgPath) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imgPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        int b;
        try {
            while ((b = fis.read()) != -1) {
                imgBytes.write(b);
            }
            fis.close();
        } catch (IOException e) {
            System.err.println("IOException in image convertion cause:: " + e.getMessage());
            return false;
        }
//        sheet.addMergedRegion(new CellRangeAddress(row1, row2 - 1, col1, col2 - 1));
        XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        int index = workBook.addPicture(imgBytes.toByteArray(), imgPath.endsWith(".jpg") ? XSSFWorkbook.PICTURE_TYPE_JPEG : XSSFWorkbook.PICTURE_TYPE_PNG);
        Drawing patriarch = sheet.createDrawingPatriarch();
        patriarch.createPicture(anchor, index);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_DONT_RESIZE);
        anchor.setDx1(25);
        return true;
    }

    public static void addImageInXSSFCell(Workbook workBook, Sheet sheet, Row rowObj, int startColIndex, int rowStartIndex, String imagePath) {
        try {
            rowObj.createCell(startColIndex);
//            sheet.setColumnWidth(0, (0 + 1) * 5 * 256);
            //load the picture
//            LOGGER.error("imagePath:: " + imagePath);
            int pictureIdx;
            try ( //load the picture
                    InputStream inputStream = new FileInputStream(imagePath)) {
                byte[] bytes = IOUtils.toByteArray(inputStream);
                pictureIdx = workBook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            }

            //create an anchor with upper left cell A1
            CreationHelper helper = workBook.getCreationHelper();
            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setCol1(startColIndex); //Column A
            anchor.setRow1(rowStartIndex); //Row 1

            //create a picture anchored to A1
            Drawing drawing = sheet.createDrawingPatriarch();
            Picture pict = drawing.createPicture(anchor, pictureIdx);

            //resize the pictutre to original size
            pict.resize();

            //get the picture width
            int pictWidthPx = pict.getImageDimension().width;
//            LOGGER.error(pictWidthPx);

            float cellWidthPx = sheet.getColumnWidthInPixels(startColIndex);
//            LOGGER.error(cellWidthPx);

            //calculate the center position
            int centerPosPx = Math.round(cellWidthPx / 2f - (float) pictWidthPx / 2f);
//            LOGGER.error(centerPosPx);

            //determine the new first anchor column dependent of the center position 
            //and the remaining pixels as Dx
            int anchorCol1 = startColIndex;
            if (Math.round(sheet.getColumnWidthInPixels(startColIndex)) < centerPosPx) {
                centerPosPx -= Math.round(sheet.getColumnWidthInPixels(startColIndex));
                anchorCol1 = startColIndex + 1;
            }
            //set the new upper left anchor position
            anchor.setCol1(anchorCol1);
            //set the remaining pixels up to the center position as Dx in unit EMU
            anchor.setDx1(centerPosPx * Units.EMU_PER_PIXEL);

            //resize the pictutre to original size again
            //this will determine the new bottom rigth anchor position
            pict.resize();
        } catch (IOException ioex) {
            System.err.println("IOException:: " + ioex);
        }
    }

    public String convertMilliSecondIntoSeconds(double milliSeconds) {
        LOGGER.debug("milliSeconds:: " + milliSeconds);
        double oneMilliSecond = 1000.0;
        double seconds = 0;
        if (milliSeconds > 0) {
            seconds = ((milliSeconds / oneMilliSecond));
            LOGGER.debug("converted from mili to seconds bfre round:: " + seconds);
        } else {
            seconds = milliSeconds;
        }
        LOGGER.debug("milli-->seconds:: " + seconds);
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(1);
        LOGGER.debug("decimalFormat::  " + decimalFormat.format(seconds));
        return decimalFormat.format(seconds);
    }

    private String getTextFitIntoCell(String valueOfCell) {
        String temp = valueOfCell.length() >= MAX_CHARCHTER_SIZE ? valueOfCell.substring(0, MAX_CHARCHTER_SIZE - 2) : valueOfCell;
        return temp;
    }
}
