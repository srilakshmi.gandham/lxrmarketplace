/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author vidyasagar.korada
 */
public class PageSpeedInsightsScoreTask implements Callable<Map<String, Integer>>{

    private final String endPointURL;
    private CountDownLatch latchObj;
    private final PageSpeedInsightsService pageSpeedInsightsService;

    public PageSpeedInsightsScoreTask(String endPointURL, CountDownLatch latchObj, PageSpeedInsightsService pageSpeedInsightsService) {
        this.endPointURL = endPointURL;
        this.latchObj = latchObj;
        this.pageSpeedInsightsService = pageSpeedInsightsService;
    }

    @Override
    public Map<String, Integer> call() {
        /*Pull data from API for*/
        Map<String, Integer> deviceLightHouseScore = null;
        deviceLightHouseScore = this.pageSpeedInsightsService.getGoogleLightScoreForDevice(this.endPointURL);
        latchObj.countDown();
        return deviceLightHouseScore;
    }
}
