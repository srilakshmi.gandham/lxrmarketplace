/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author vidyasagar.korada
 */
public class StackPacks implements Serializable{
        private String id;
    private String title;
    private String iconDataURL;
    private Map<String,String> auditDescriptions;
    private Map<String,String> auditDescriptionsHTML;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIconDataURL() {
        return iconDataURL;
    }

    public void setIconDataURL(String iconDataURL) {
        this.iconDataURL = iconDataURL;
    }

    public Map<String, String> getAuditDescriptions() {
        return auditDescriptions;
    }

    public void setAuditDescriptions(Map<String, String> auditDescriptions) {
        this.auditDescriptions = auditDescriptions;
    }

    public Map<String, String> getAuditDescriptionsHTML() {
        return auditDescriptionsHTML;
    }

    public void setAuditDescriptionsHTML(Map<String, String> auditDescriptionsHTML) {
        this.auditDescriptionsHTML = auditDescriptionsHTML;
    }
  
    
}
