/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;

/**
 *
 * @author vidyasagar.korada
 */
public class CriticalRequestChain implements Serializable{
    
      public static String HOST_URL= "host"; //Parent URL
    public static String CHILDERN_URL = "children"; //Childern URL(S)
    
    
    private String url;
    private String urlType;
    private String urlId;
    private double responseReceivedTime;
    private double transferSize;
    private double endTime;
    private double startTime;

    public static String getHOST_URL() {
        return HOST_URL;
    }

    public static void setHOST_URL(String HOST_URL) {
        CriticalRequestChain.HOST_URL = HOST_URL;
    }

    public static String getCHILDERN_URL() {
        return CHILDERN_URL;
    }

    public static void setCHILDERN_URL(String CHILDERN_URL) {
        CriticalRequestChain.CHILDERN_URL = CHILDERN_URL;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getUrlId() {
        return urlId;
    }

    public void setUrlId(String urlId) {
        this.urlId = urlId;
    }

    public double getResponseReceivedTime() {
        return responseReceivedTime;
    }

    public void setResponseReceivedTime(double responseReceivedTime) {
        this.responseReceivedTime = responseReceivedTime;
    }

    public double getTransferSize() {
        return transferSize;
    }

    public void setTransferSize(double transferSize) {
        this.transferSize = transferSize;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }
    
    
    
}
