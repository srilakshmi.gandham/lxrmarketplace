/**
 * @author NE16T1213-Sagar
 */
package lxr.marketplace.pagespeedinsights;

import java.util.List;
import org.apache.log4j.Logger;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lxr.marketplace.util.Common;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lxr.marketplace.mobile.user.PageSpeedRestModel;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class PageSpeedInsightsService {

    private static final Logger LOGGER = Logger.getLogger(PageSpeedInsightsService.class);

    @Autowired
    private MessageSource messageSource;

    @Value("${PageSpeedInsights.Google.API.Key}")
    /*Google V5 API key*/
    private String API_KEY;

    @Value("${PageSpeedInsights.Google.Endpoint.URL}")
    /*Google V5 API endpoint URL*/
    private String API_ENDPOINT_URL;

    @Value("${PageSpeedInsights.desktopUserAgent}")
    private String desktopUserAgent;

    @Value("${PageSpeedInsights.mobileUserAgent}")
    private String mobileUserAgent;

    @Value("${PageSpeedInsights.apiFormat}")
    private String apiFormat;
    @Value("${PageSpeedInsights.userFormat}")
    private String userFormat;

    @Value("${PageSpeedInsights.Fast}")
    private String fast;
    @Value("${PageSpeedInsights.Average}")
    private String average;
    @Value("${PageSpeedInsights.Slow}")
    private String slow;
    @Value("${PageSpeedInsights.None}")
    private String none;
    @Value("${PageSpeedInsights.AverageScore}")
    private int averageScore;
    @Value("${PageSpeedInsights.SlowScore}")
    private int slowScore;
//    private static final int NO_SCORE = -1;
    /*Common for all for data not found / label not found in header row*/
    @Value("${PageSpeedInsights.Nodata}")
    private String noData;
    @Value("${PageSpeedInsights.Diagnostics}")
    private String diagnostics;
    @Value("${PageSpeedInsights.Opportunities}")
    private String opportunities;
    @Value("${PageSpeedInsights.Passed}")
    private String passed;

    @Value("${PageSpeedInsights.KIBIBYTES}")
    private long kibibyes;

    @Value("${PageSpeedInsights.lighthouseCategorys}")
    private String lighthouseCategorys;

    private static final String LOADING_EXPERIENCE = "loadingExperience";

    private static final String ORIGIN_LOADING_EXPERIENCE = "originLoadingExperience";

    private static final String LIGHTHOUSERESULT = "lighthouseResult";

    private static final String FIRST_CONTENTFUL_PAINT_MS = "FIRST_CONTENTFUL_PAINT_MS";

    private static final String FIRST_INPUT_DELAY_MS = "FIRST_INPUT_DELAY_MS";

    private static final String PERCENTILE = "percentile";

    private static final String DISTRIBUTIONS = "distributions";

    private static final String OVERALL_CATEGORY = "overall_category";

    private static final String METRICS = "metrics";

    private static final String FCP = "FCP";

    private static final String FID = "FID";

    private static final String[] AUDIT_RULE_CATEGORIES = {"metrics", "load-opportunities", "diagnostics"};

    private static final String SCREENSHOT_THUMBNAILS = "screenshot-thumbnails";

    private static final String FINAL_SCREENSHOT = "final-screenshot";
    /*External  adding audit rule to diagnostics list, 
        will not found in categories list. 
        For this type query URL:
        https://searchengineland.com/google-pagespeeds-insights-tool-gets-a-major-update-with-more-data-from-lighthouse-307968*/
    private static final String[] AUDIT_RULE_EXTERNAL = {"critical-request-chains"};

//    private static final String LAB_DATA_RULES_IDS[] = {"first-contentful-paint", "first-meaningful-paint", "speed-index", "first-cpu-idle", "interactive", "max-potential-fid"};
    public String getDesktopUserAgent() {
        return desktopUserAgent;
    }

    public String getMobileUserAgent() {
        return mobileUserAgent;
    }

    public String getApiFormat() {
        return apiFormat;
    }

    public String getUserFormat() {
        return userFormat;
    }

    public String getFast() {
        return fast;
    }

    public String getAverage() {
        return average;
    }

    public String getSlow() {
        return slow;
    }

    public String getNone() {
        return none;
    }

    public int getAverageScore() {
        return averageScore;
    }

    public int getSlowScore() {
        return slowScore;
    }

    public String getNoData() {
        return noData;
    }

    public String getDiagnostics() {
        return diagnostics;
    }

    public String getOpportunities() {
        return opportunities;
    }

    public String getPassed() {
        return passed;
    }

    public long getKibibyes() {
        return kibibyes;
    }

    public Map<String, Object> analyzePageSpeedMetrics(String queryWebPageURL) {
        Map<String, Object> pageSpeedData = null;

        PageSpeedRestModel pageSpeedRestModel = new PageSpeedRestModel();
        pageSpeedRestModel.setQueryWebPageURL(queryWebPageURL.trim());
        pageSpeedRestModel.setDevices(desktopUserAgent.concat(",").concat(mobileUserAgent));
        pageSpeedRestModel.setCategories(lighthouseCategorys);
        String requestedURL = messageSource.getMessage("lxrm.apps.rest.hostingURL", null, Locale.US).trim().concat(messageSource.getMessage("pageSpeedInsightsServiceUrl", null, Locale.US).trim());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("queryWebPageURL", pageSpeedRestModel.getQueryWebPageURL());
        body.add("devices", pageSpeedRestModel.getDevices());
        body.add("categories", pageSpeedRestModel.getCategories());

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Map<String, Object>> response = null;
        try {
            response = restTemplate.exchange(requestedURL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Map<String, Object>>() {
            });
            if (response.getBody() != null && !response.getBody().toString().equals("")) {
                pageSpeedData = response.getBody();
            }
        } catch (Exception e) {
            LOGGER.error("Exception in analyzePageSpeedMetrics cause:: " + e.getMessage());
        }
        return pageSpeedData;
    }

    public boolean checkForErrorsInResponse(final List<PageSpeedInsights> resultPageSpeedList) {
        return resultPageSpeedList.stream().filter(insight -> (insight != null && insight.getStatusCode() >= 400)).count() >= 2;
    }

    /*Returns Google PageSpeed Insights API EndPoint URL*/
    public String generateGooglePSIAPIEndPointURL(String encodedQueryURL, String device, String categoryListURL) {
        /*Google API EndPoint URL + Encoded URL+ Cateogry Type*/
        String endPointURL = API_ENDPOINT_URL + "&url=" + encodedQueryURL + categoryListURL;
        if (device.equals(desktopUserAgent)) {
            endPointURL += "&strategy=" + desktopUserAgent;
        }
        if (device.equals(mobileUserAgent)) {
            endPointURL += "&strategy=" + mobileUserAgent;
        }
        endPointURL += "&key=" + API_KEY;
        LOGGER.debug("Device: " + device + ", Google PSI endpoint URL: " + endPointURL);
        return endPointURL;
    }

    public String getCategoryString(List<String> categoryList) {
        /*Google API EndPoint URL + Encoded URL+ Cateogry Type*/
        String categoryURL = "&category=";
        for (String categoryName : categoryList) {
            categoryURL = categoryURL + categoryName.toLowerCase();
        }
        return categoryURL;
    }

    public String getEncodedURLString(String queryPathURL) {
        String encodedURL = null;
        try {
            /*Encode the url's for Ecommerce type  to prevent 404 status code for working url.*/
            encodedURL = URLEncoder.encode(queryPathURL, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            encodedURL = queryPathURL;
            LOGGER.error("UnsupportedEncodingException in getCategoryString for queryPathURL: ", e);
        }
        return encodedURL;
    }

    public PageSpeedInsights generatePageSpeedInsighstForURLDevice(String endpointURL) {
        PageSpeedInsights pageSpeedInsightsModel = new PageSpeedInsights();
        JSONObject responseJSON = Common.getJSONFromAPIResponse(endpointURL);
        JSONObject innerJSON = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        JSONArray innerJSONArray = null;
        /*Checking for status : Success or Error*/
        if (responseJSON != null && !responseJSON.has("error")) {
            /*responseJSON contains Key: lighthouseResult*/
            if (responseJSON.has(LIGHTHOUSERESULT)) {
                try {
                    /*Pulling Requested/ Final URL */
                    innerJSON = responseJSON.getJSONObject(LIGHTHOUSERESULT);
                    if (innerJSON != null) {
                        if (innerJSON.has("requestedUrl")) {
                            pageSpeedInsightsModel.setRequestedUrl(innerJSON.getString("requestedUrl"));
                        }
                        if (innerJSON.has("finalUrl")) {
                            pageSpeedInsightsModel.setFinalUrl(innerJSON.getString("finalUrl"));
                        }
                        /*Device Type*/
                        if (innerJSON.has("configSettings")) {
                            subInnerOneJSON = innerJSON.getJSONObject("configSettings");
                            if (subInnerOneJSON != null) {
                                if (subInnerOneJSON.has("emulatedFormFactor")) {
                                    pageSpeedInsightsModel.setStatusCode(HttpStatus.SC_OK);
                                    pageSpeedInsightsModel.setDeviceCategory(subInnerOneJSON.getString("emulatedFormFactor"));
                                }
                            }
                        }

                        /*Performance Score Type*/
                        if (innerJSON.has("categories")) {
                            subInnerOneJSON = innerJSON.getJSONObject("categories");
                            if (subInnerOneJSON != null) {
                                if (subInnerOneJSON.has("performance")) {
                                    subInnerTwoJSON = subInnerOneJSON.getJSONObject("performance");
                                    if (subInnerTwoJSON != null && subInnerTwoJSON.has("score")) {
                                        pageSpeedInsightsModel.setPerformanceScore(roundUpTheValue(subInnerTwoJSON.getDouble("score")));
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in Preapring stack packs metrics  from API resposne cause: " + e);
                }
                if (pageSpeedInsightsModel.getStatusCode() == HttpStatus.SC_OK) {
                    if (innerJSON != null) {
                        try {
                            /*Preapring Stack Packs metric*/
                            if (innerJSON.has("stackPacks")) {
                                innerJSONArray = innerJSON.getJSONArray("stackPacks");
                                if (innerJSONArray != null && innerJSONArray.length() >= 1) {
                                    subInnerOneJSON = innerJSONArray.getJSONObject(0);
                                    if (subInnerOneJSON != null) {
                                        StackPacks stackPacks = new StackPacks();
                                        if (subInnerOneJSON.has("id")) {
                                            stackPacks.setId(subInnerOneJSON.getString("id"));
                                        }
                                        if (subInnerOneJSON.has("title")) {
                                            stackPacks.setTitle(subInnerOneJSON.getString("title"));
                                        }
                                        if (subInnerOneJSON.has("iconDataURL")) {
                                            stackPacks.setIconDataURL(subInnerOneJSON.getString("iconDataURL"));
                                        }
                                        /*Audit Rules Description from stack packs*/
                                        if (subInnerOneJSON.has("descriptions")) {
                                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("descriptions");
                                            if (subInnerTwoJSON != null) {
                                                stackPacks.setAuditDescriptions(getStackPacksFromAuditRulesForPage(subInnerTwoJSON, Boolean.FALSE));
                                                stackPacks.setAuditDescriptionsHTML(getStackPacksFromAuditRulesForPage(subInnerTwoJSON, Boolean.TRUE));
                                            }
                                        }
                                        pageSpeedInsightsModel.setStackPacks(stackPacks);
                                    }

                                }
                            }
                        } catch (JSONException e) {
                            LOGGER.error("JSONException in Preapring stack packs metrics  from API resposne cause: " + e);
                        }
                        try {
                            /*Preapring Audit Ruels/Final/ Screenshot thumbnails metrics*/
                            Map<String, List<String>> auditTitleMap = null;
                            if (innerJSON.has("categories")) {
                                auditTitleMap = getAuditRulesIdsfromCategories(innerJSON.getJSONObject("categories"));
                            }
                            if (innerJSON.has("audits")) {
                                LOGGER.info("Audit Rules for Device::  " + pageSpeedInsightsModel.getDeviceCategory());
                                pageSpeedInsightsModel.setAuditRules(getGooglePSIAuditRulesForPage(innerJSON.getJSONObject("audits"), auditTitleMap != null ? auditTitleMap : null, pageSpeedInsightsModel.getStackPacks() != null ? pageSpeedInsightsModel.getStackPacks() : null));
                                pageSpeedInsightsModel.setScreenShots(getDeviceScreenShotsForPage(innerJSON.getJSONObject("audits")));
                                pageSpeedInsightsModel.setFinalScreenshotDesc(getHTMlFormatDescription(getDeviceScreenShotsInfo(innerJSON.getJSONObject("audits"), FINAL_SCREENSHOT)));
                                pageSpeedInsightsModel.setScreenshotThumbnailDesc(getHTMlFormatDescription(getDeviceScreenShotsInfo(innerJSON.getJSONObject("audits"), SCREENSHOT_THUMBNAILS)));
                            }
                        } catch (JSONException e) {
                            LOGGER.error("JSONException in Preapring Audit Ruels/Final/ Screenshot thumbnails metrics  from API resposne cause: " + e);
                        }

                        /*adding desc*/
                        if (innerJSON.has("categoryGroups")) {
                            try {
                                subInnerOneJSON = innerJSON.getJSONObject("categoryGroups");
                                if (subInnerOneJSON != null) {
                                    if (subInnerOneJSON.has("load-opportunities")) {
                                        subInnerTwoJSON = subInnerOneJSON.getJSONObject("load-opportunities");
                                        if (subInnerTwoJSON != null && subInnerTwoJSON.has("description")) {
                                            pageSpeedInsightsModel.setLoadOpportunitiesDesc(getHTMlFormatDescription(subInnerTwoJSON.getString("description")));
                                            pageSpeedInsightsModel.setLoadOpportunitiesDescReport((subInnerTwoJSON.getString("description")));
                                        }
                                    }
                                    if (subInnerOneJSON.has("diagnostics")) {
                                        subInnerTwoJSON = subInnerOneJSON.getJSONObject("diagnostics");
                                        if (subInnerTwoJSON != null && subInnerTwoJSON.has("description")) {
                                            pageSpeedInsightsModel.setDiagnosticsDesc(getHTMlFormatDescription(subInnerTwoJSON.getString("description")));
                                            pageSpeedInsightsModel.setDiagnosticsDescReport((subInnerTwoJSON.getString("description")));

                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                LOGGER.error("JSONException in preparing desc about load-opportunities and diagnostics causee:: ", e);
                            }

                        }
                    }
                }
                innerJSON = null;
            }
            if (pageSpeedInsightsModel.getStatusCode() == HttpStatus.SC_OK) {
                try {
                    /*Pulling TimeStamp of URL */
                    if (responseJSON.has("analysisUTCTimestamp")) {
                        String analysisUTCTimestamp = responseJSON.getString("analysisUTCTimestamp");
                        pageSpeedInsightsModel.setAnalysisUTCTimestamp(Common.changeDateStringFormat(analysisUTCTimestamp, apiFormat, userFormat));
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in preparing stackpacks/ images cause: " + e);
                }
                /*Preparing Field Data && Origin Summary*/
                RealWorldFieldData realWorldFieldData = new RealWorldFieldData();
                RealWorldFieldData realWorldFielTemp;
                if (responseJSON.has(ORIGIN_LOADING_EXPERIENCE)) {
                    realWorldFielTemp = getRealWorldFieldDataForPage(responseJSON, ORIGIN_LOADING_EXPERIENCE);
                    if (realWorldFielTemp != null) {
                        realWorldFieldData.setOriginSummaryCategory(realWorldFielTemp.getOriginSummaryCategory());
                        realWorldFieldData.setOriginSummaryFCPPercentile(realWorldFielTemp.getOriginSummaryFCPPercentile());
                        realWorldFieldData.setOriginSummaryFCPColor(realWorldFielTemp.getOriginSummaryFCPColor());
                        realWorldFieldData.setOriginSummaryFCPdistributions(realWorldFielTemp.getOriginSummaryFCPdistributions());
                        realWorldFieldData.setOriginSummaryFIDPercentile(realWorldFielTemp.getOriginSummaryFIDPercentile());
                        realWorldFieldData.setOriginSummaryFIDColor(realWorldFielTemp.getOriginSummaryFIDColor());
                        realWorldFieldData.setOriginSummaryFIDdistributions(realWorldFielTemp.getOriginSummaryFIDdistributions());
                    }
                } else {
                    realWorldFieldData.setOriginSummaryCategory(noData);
                }
                if (responseJSON.has(LOADING_EXPERIENCE)) {
                    realWorldFielTemp = getRealWorldFieldDataForPage(responseJSON, LOADING_EXPERIENCE);
                    if (realWorldFielTemp != null) {
                        realWorldFieldData.setFieldDataCategory(realWorldFielTemp.getFieldDataCategory());
                        realWorldFieldData.setFieldDataFCPPercentile(realWorldFielTemp.getFieldDataFCPPercentile());
                        realWorldFieldData.setFieldDataFCPColor(realWorldFielTemp.getFieldDataFCPColor());
                        realWorldFieldData.setFieldDataFCPdistributions(realWorldFielTemp.getFieldDataFCPdistributions());
                        realWorldFieldData.setFieldDataFIDPercentile(realWorldFielTemp.getFieldDataFIDPercentile());
                        realWorldFieldData.setFieldDataFIDColor(realWorldFielTemp.getFieldDataFIDColor());
                        realWorldFieldData.setFieldDataFIDdistributions(realWorldFielTemp.getFieldDataFIDdistributions());
                    }
                } else {
                    realWorldFieldData.setFieldDataCategory(noData);
                }
                pageSpeedInsightsModel.setRealWorldFieldData(realWorldFieldData);

            }
            LOGGER.info("pageSpeedInsightsModel completed for device:" + pageSpeedInsightsModel.getDeviceCategory());
        } else if (responseJSON != null && responseJSON.has("error")) {
            /*Error case*/
            try {
                innerJSON = responseJSON.getJSONObject("error");
                if (innerJSON != null) {
                    if (endpointURL.contains("&strategy=" + desktopUserAgent)) {
                        pageSpeedInsightsModel.setDeviceCategory(desktopUserAgent);
                    } else if (endpointURL.contains("&strategy=" + mobileUserAgent)) {
                        pageSpeedInsightsModel.setDeviceCategory(mobileUserAgent);
                    }
                    LOGGER.info("Error from GPSI ::: " + innerJSON);
                    /*Seting the error code*/
                    if (innerJSON.has("code")) {
                        Object objType = innerJSON.get("code");
                        if (objType != null) {
                            if (objType instanceof Integer) {
                                pageSpeedInsightsModel.setStatusCode(innerJSON.getInt("code"));
                            } else {
                                pageSpeedInsightsModel.setStatusCode(Integer.valueOf(innerJSON.getString("code")));
                            }
                        }
                    }
                    /*Seting the error message*/
                    if (innerJSON.has("message")) {
                        pageSpeedInsightsModel.setFinalUrl(innerJSON.getString("message"));
                    }

                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in checking error for Status code from API resposne cause: " + e);
            }
        } else if (responseJSON == null) {
            if (endpointURL.contains("&strategy=" + desktopUserAgent)) {
                pageSpeedInsightsModel.setDeviceCategory(desktopUserAgent);
            } else if (endpointURL.contains("&strategy=" + mobileUserAgent)) {
                pageSpeedInsightsModel.setDeviceCategory(mobileUserAgent);
            }
            pageSpeedInsightsModel.setStatusCode(HttpStatus.SC_BAD_REQUEST);
            pageSpeedInsightsModel.setFinalUrl(messageSource.getMessage("URL.error", null, Locale.US));
        }

        return pageSpeedInsightsModel;
    }

    private List<ScreenShot> getDeviceScreenShotsForPage(JSONObject auditRules) {
        List<ScreenShot> deviceScreenShots = new ArrayList<>();
        JSONObject subInnerThreeJSON = null;
        JSONObject subInnerFourJSON = null;
        JSONObject subInnerFiveJSON = null;
        JSONArray innerJSONArray = null;
        try {
            if (auditRules != null) {
                /*preparing the SCREENSHOT_THUMBNAILS of a device*/
                if (auditRules.has(SCREENSHOT_THUMBNAILS)) {
                    subInnerThreeJSON = auditRules.getJSONObject(SCREENSHOT_THUMBNAILS);
                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("details")) {
                        subInnerFourJSON = subInnerThreeJSON.getJSONObject("details");
                        if (subInnerFourJSON != null && subInnerFourJSON.has("items")) {
                            innerJSONArray = subInnerFourJSON.getJSONArray("items");
                            if (innerJSONArray != null) {
                                for (int i = 0; i < innerJSONArray.length(); i++) {
                                    subInnerFiveJSON = innerJSONArray.getJSONObject(i);
                                    if (subInnerFiveJSON != null) {
                                        /*Added the SCREENSHOT_THUMBNAILS of device*/
                                        deviceScreenShots.add(getScreenShotObject(subInnerFiveJSON, SCREENSHOT_THUMBNAILS));
                                    }
                                }
                            }
                        }
                    }
                }
                /*preparing the FINAL_SCREENSHOT of a device*/
                if (auditRules.has(FINAL_SCREENSHOT)) {
                    subInnerThreeJSON = auditRules.getJSONObject(FINAL_SCREENSHOT);
                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("details")) {
                        subInnerFourJSON = subInnerThreeJSON.getJSONObject("details");
                        if (subInnerFourJSON != null) {
                            /*Added the FINAL_SCREENSHOT of device*/
                            deviceScreenShots.add(getScreenShotObject(subInnerFiveJSON, FINAL_SCREENSHOT));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getScreenShotsOfURLForDevice caus: " + e);
        }
        return deviceScreenShots;
    }

    public ScreenShot getScreenShotObject(JSONObject responseJSON, String screeShotType) {
        ScreenShot screenImage = null;
        try {
            screenImage = new ScreenShot();
            screenImage.setImageType(screeShotType);
            if (responseJSON.has("timing")) {
                screenImage.setLoadingSeconds(responseJSON.getInt("timing"));
            } else {
                screenImage.setLoadingSeconds(0);
            }
            if (responseJSON.has("timestamp")) {
                screenImage.setTimestamp(responseJSON.getLong("timestamp"));
            } else {
                screenImage.setTimestamp(0);
            }
            if (responseJSON.has("data")) {
                screenImage.setImageURL(responseJSON.getString("data"));
            } else {
                screenImage.setImageURL(none);
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getScreenShotObject cause:: ", e);
        }
        LOGGER.debug("screenImage::" + screenImage);
        return screenImage;
    }

    private String getDeviceScreenShotsInfo(JSONObject auditRules, String typeOfImage) {
        String descInfo = null;
        JSONObject subInnerThreeJSON = null;
        if (auditRules != null) {
            try {
                if (auditRules.has(SCREENSHOT_THUMBNAILS) && typeOfImage.equals(SCREENSHOT_THUMBNAILS)) {
                    subInnerThreeJSON = auditRules.getJSONObject(SCREENSHOT_THUMBNAILS);
                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("description")) {
                        descInfo = subInnerThreeJSON.getString("description");
                    }
                } else if (auditRules.has(FINAL_SCREENSHOT) && typeOfImage.equals(FINAL_SCREENSHOT)) {
                    subInnerThreeJSON = auditRules.getJSONObject(FINAL_SCREENSHOT);
                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("description")) {
                        descInfo = subInnerThreeJSON.getString("description");
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getDeviceScreenShotsInfo cause: " + e);
            }
        }
        return descInfo;
    }

    private Map<String, String> getStackPacksFromAuditRulesForPage(JSONObject descriptionsJSON, boolean htmlFormat) {
        Map<String, String> finalMap = new HashMap<>();
        Map<String, Object> filteredMap = new HashMap<>();
        Map<String, Object> notNestingMap = new HashMap<>();

        Iterator<String> keysItr = null;
        if (descriptionsJSON != null) {
            keysItr = descriptionsJSON.keys();
            while (keysItr.hasNext()) {
                String key = null;
                Object value = null;
                try {
                    key = keysItr.next();
                    value = descriptionsJSON.get(key);
                    if (value != null && isValueValid(value.toString())) {
                        notNestingMap.put(key, value);
                    }
                    if (value != null && value instanceof JSONArray) {
                        value = convertJSONArrayToList((JSONArray) value, htmlFormat);
                    } else if (value != null && value instanceof JSONObject) {
                        value = getStackPacksFromAuditRulesForPage((JSONObject) value, htmlFormat);
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getStackPacksFromAuditRulesForPage cause:: ", e);
                }
                if (value != null && value instanceof List) {
                    if (!((List<Object>) value).isEmpty()) {
                        filteredMap.put(key, value);
                    }
                } else if (value != null && isValueValid(value.toString())) {
                    filteredMap.put(key, value);
                }
            }
            finalMap = convertMapObjectIntoString(filteredMap, htmlFormat);
        }
//        finalMap.forEach((k, v) -> LOGGER.debug((k + ":" + v)));
        return finalMap;
    }

    private int roundUpTheValue(double apiFormatedValue) {
        int finalFormatedValue;
        apiFormatedValue = apiFormatedValue * 100;
        if (apiFormatedValue <= 50) {
            finalFormatedValue = (int) Math.floor(apiFormatedValue);
        } else {
            finalFormatedValue = (int) Math.ceil(apiFormatedValue);
        }
        return finalFormatedValue;
    }

    private List<Object> convertJSONArrayToList(JSONArray array, boolean htmlFormat) {
        List<Object> list = new ArrayList<>();
        Object value = null;
        for (int i = 0; i < array.length(); i++) {
            if (value != null) {
                try {
                    value = array.get(i);
                } catch (JSONException e) {
                    LOGGER.error("JSONException in convertJSONArrayToList cause:: " + e.getMessage());
                }
                if (value instanceof JSONArray) {
                    value = convertJSONArrayToList((JSONArray) value, htmlFormat);
                } else if (value instanceof JSONObject) {
                    value = getStackPacksFromAuditRulesForPage((JSONObject) value, htmlFormat);
                }
                if (value instanceof List) {
                    if (!((List<Object>) value).isEmpty()) {
                        list.add(value);
                    }
                } else if (isValueValid(value.toString())) {
                    list.add(value);
                }
            }
        }
        list.removeAll(Arrays.asList(null, "", ",", " "));
        return list;
    }

    private Map<String, String> convertMapObjectIntoString(Map<String, Object> resultMap, boolean htmlFormat) {
        Map<String, String> clearedMap = new HashMap<>();
        String value = null;
        for (Map.Entry entry : resultMap.entrySet()) {
            value = entry.getValue().toString();
            if (value != null) {
                LOGGER.info("Bfroe format:: " + value);
                value = htmlFormat ? getHTMlFormatDescription(value) : value;
                LOGGER.info("after format:: " + value);
                value = value
                        .replaceAll("\\[", "")
                        .replaceAll("\\]", "")
                        .replaceAll("\\{", "")
                        .replaceAll("\\}", "")
                        //                        .replaceAll("\"", "")
                        //                        .replaceAll("=", "-")
                        //                        .replaceAll("\"", "")
                        .trim();
                if (isValueValid(value)) {
                    clearedMap.put(entry.getKey().toString(), value);
                }
            }
        }
        return clearedMap;
    }

    private static boolean isValueValid(String value) {
        return !(value.trim().equals("{}") || value.trim().equals("[]") || value.trim().isEmpty()
                || value.trim().equals(","));
    }

    private RealWorldFieldData getRealWorldFieldDataForPage(JSONObject responseJSON, String realWorldFieldDataType) {
        /*Preparing Field Data*/
        RealWorldFieldData realWorldFieldData = new RealWorldFieldData();
        JSONObject innerJSON = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        boolean metricsNotExisted = false;
        if (responseJSON.has(realWorldFieldDataType)) {
            try {
                innerJSON = responseJSON.getJSONObject(realWorldFieldDataType);
                if (innerJSON != null) {
                    if (innerJSON.has(OVERALL_CATEGORY)) {
                        if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                            realWorldFieldData.setOriginSummaryCategory(innerJSON.getString(OVERALL_CATEGORY).toLowerCase());
                        } else if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                            realWorldFieldData.setFieldDataCategory(innerJSON.getString(OVERALL_CATEGORY).toLowerCase());
                        }
                    } else {
                        metricsNotExisted = true;
                    }
                    if (innerJSON.has(METRICS)) {
                        subInnerOneJSON = innerJSON.getJSONObject(METRICS);
                        if (subInnerOneJSON != null) {
                            if (subInnerOneJSON.has(FIRST_CONTENTFUL_PAINT_MS)) {
                                subInnerTwoJSON = subInnerOneJSON.getJSONObject(FIRST_CONTENTFUL_PAINT_MS);
                                if (subInnerTwoJSON != null) {
                                    if (subInnerTwoJSON.has(PERCENTILE)) {
                                        if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setOriginSummaryFCPPercentile(subInnerTwoJSON.getInt(PERCENTILE));
                                            realWorldFieldData.setOriginSummaryFCPColor(getPSIColor(realWorldFieldData.getOriginSummaryFCPPercentile(), FCP));
                                        } else if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setFieldDataFCPPercentile(subInnerTwoJSON.getInt(PERCENTILE));
                                            realWorldFieldData.setFieldDataFCPColor(getPSIColor(realWorldFieldData.getFieldDataFCPPercentile(), FCP));
                                        }

                                    }
                                    if (subInnerTwoJSON.has(DISTRIBUTIONS)) {
                                        if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setOriginSummaryFCPdistributions(getDistributionsMap(subInnerTwoJSON.getJSONArray(DISTRIBUTIONS), FCP));
                                        } else if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setFieldDataFCPdistributions(getDistributionsMap(subInnerTwoJSON.getJSONArray(DISTRIBUTIONS), FCP));
                                        }

                                    }
                                }
                            }
                            if (subInnerOneJSON.has(FIRST_INPUT_DELAY_MS)) {
                                subInnerTwoJSON = subInnerOneJSON.getJSONObject(FIRST_INPUT_DELAY_MS);
                                if (subInnerTwoJSON != null) {
                                    if (subInnerTwoJSON.has(PERCENTILE)) {
                                        if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setOriginSummaryFIDPercentile(subInnerTwoJSON.getInt(PERCENTILE));
                                            realWorldFieldData.setOriginSummaryFIDColor(getPSIColor(realWorldFieldData.getOriginSummaryFIDPercentile(), FID));
                                        } else if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setFieldDataFIDPercentile(subInnerTwoJSON.getInt(PERCENTILE));
                                            realWorldFieldData.setFieldDataFIDColor(getPSIColor(realWorldFieldData.getFieldDataFIDPercentile(), FID));

                                        }
                                    }
                                    if (subInnerTwoJSON.has(DISTRIBUTIONS)) {
                                        if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setOriginSummaryFIDdistributions(getDistributionsMap(subInnerTwoJSON.getJSONArray(DISTRIBUTIONS), FID));
                                        } else if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                                            realWorldFieldData.setFieldDataFIDdistributions(getDistributionsMap(subInnerTwoJSON.getJSONArray(DISTRIBUTIONS), FID));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        metricsNotExisted = true;
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getRealWorldFieldDataForPage cause: ", e);
            }
        }

        if (metricsNotExisted) {
            if (realWorldFieldDataType.equals(LOADING_EXPERIENCE)) {
                realWorldFieldData.setFieldDataCategory(noData);
            } else if (realWorldFieldDataType.equals(ORIGIN_LOADING_EXPERIENCE)) {
                realWorldFieldData.setOriginSummaryCategory(noData);
            }
        }
        return realWorldFieldData;
    }

    private LinkedHashMap<String, Integer> getDistributionsMap(JSONArray distributionsArray, String fieldDatatype) {
        LinkedHashMap<String, Integer> distributionsMap = new LinkedHashMap<>();
        JSONObject innerJSON = null;
        int min = 0, max = 0, proportion = 0;
        for (int i = 0; i < distributionsArray.length(); i++) {
            try {
                innerJSON = distributionsArray.getJSONObject(i);
                if (innerJSON != null) {
                    if (innerJSON.has("min")) {
                        min = innerJSON.getInt("min");
                    }
                    if (innerJSON.has("max")) {
                        max = innerJSON.getInt("max");
                    } else {
                        /*If max not found */
                        max = min + 1;
                    }
                    if (innerJSON.has("proportion")) {
                        proportion = roundUpTheValue(innerJSON.getDouble("proportion"));
                    }
                    if (fieldDatatype.equals(FCP)) {
                        if (min >= 0 && max <= 1000) {
                            distributionsMap.put(fast, proportion);
                        }
                        if (min >= 1000 && max <= 3000) {
                            distributionsMap.put(average, proportion);
                        }
                        if (min >= 3000) {
                            distributionsMap.put(slow, proportion);
                        }
                    }
                    if (fieldDatatype.equals(FID)) {
                        if (min >= 0 && max <= 100) {
                            distributionsMap.put(fast, proportion);
                        }
                        if (min >= 100 && max <= 300) {
                            distributionsMap.put(average, proportion);
                        }
                        if (min >= 300) {
                            distributionsMap.put(slow, proportion);
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getDistributionsMap cuase: ", e);
            }
            min = 0;
            max = 0;
            proportion = 0;
        }

        /*Setting defult values*/
        if (!distributionsMap.containsKey(fast)) {
            distributionsMap.put(fast, 0);
        }
        if (!distributionsMap.containsKey(average)) {
            distributionsMap.put(average, 0);
        }
        if (!distributionsMap.containsKey(slow)) {
            distributionsMap.put(slow, 0);
        }
        return distributionsMap;
    }

    private ArrayList<PageSpeedAuditRule> getGooglePSIAuditRulesForPage(JSONObject auditJSON, Map<String, List<String>> auditTitleMap, StackPacks stackPacksRules) {
        ArrayList<PageSpeedAuditRule> ruleList = new ArrayList<>();
        JSONObject innerJSON = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        /*Note: To decide rule categoire auditCategorie && auditType will be used*/
 /*Categorie: metrics, load-opportunities, diagnostics*/
        String auditCategorie = null;
        /*type: opportunities, table*/
        String auditType = null;

        List<String> auditRuleList = null;

        Map<String, String> stackPacksRulesMap = null;
        Map<String, String> stackPacksRulesMapHTML = null;

        if (stackPacksRules != null) {
            stackPacksRulesMap = stackPacksRules.getAuditDescriptions();
            stackPacksRulesMapHTML = stackPacksRules.getAuditDescriptionsHTML();
        }

        PageSpeedAuditRule pageSpeedAuditRule = null;
        /*
        >=0: score of rule
        -1: score is null for rule from API;
        Audits currently not structured as something to "pass" or "fail". */
        double ruleScore = -1;
        double overallSavingsMs = 0;
        double overallSavingsBytes = 0;

        Object objType = null;
        if (auditJSON != null && auditTitleMap != null) {
            for (Map.Entry<String, List<String>> entry : auditTitleMap.entrySet()) {
                auditCategorie = null;
                auditCategorie = entry.getKey();
                auditRuleList = entry.getValue();
//                LOGGER.debug("auditCategorie::======================== " + auditCategorie);
                for (String auditRuleId : auditRuleList) {
                    if (auditJSON.has(auditRuleId)) {
                        try {
                            innerJSON = auditJSON.getJSONObject(auditRuleId);
                            if (innerJSON != null) {
                                pageSpeedAuditRule = new PageSpeedAuditRule();

                                if (innerJSON.has("id")) {
                                    pageSpeedAuditRule.setId(innerJSON.getString("id"));
                                }
                                if (innerJSON.has("title")) {
                                    pageSpeedAuditRule.setTitle(innerJSON.getString("title"));
                                }
                                if (innerJSON.has("description")) {
                                    pageSpeedAuditRule.setDescription(innerJSON.getString("description"));
                                    pageSpeedAuditRule.setDescriptionHTML(getHTMlFormatDescription(innerJSON.getString("description")));
                                }
                                if (innerJSON.has("score")) {
                                    if (innerJSON.isNull("score")) {
                                        ruleScore = -1;
                                        pageSpeedAuditRule.setScore((int) ruleScore);
//                                        LOGGER.debug("score is null::: id::: " + pageSpeedAuditRule.getTitle() + ", score::" + pageSpeedAuditRule.getScore());
                                    } else {
                                        objType = innerJSON.get("score");
                                        if (objType != null) {
                                            if (objType instanceof Integer) {
                                                ruleScore = innerJSON.getInt("score");
                                                pageSpeedAuditRule.setScore(getScoreForAuditRule(ruleScore));
                                            } else if (objType instanceof Double) {
                                                ruleScore = innerJSON.getDouble("score");
                                                pageSpeedAuditRule.setScore(getScoreForAuditRule(ruleScore));
                                            }
                                        }
                                    }
                                }

                                if (innerJSON.has("warnings")) {
                                    pageSpeedAuditRule.setWarnings(getWarningsList(innerJSON.getJSONArray("warnings"), Boolean.FALSE));
                                    pageSpeedAuditRule.setWarningsHTML(getWarningsList(innerJSON.getJSONArray("warnings"), Boolean.TRUE));
                                }

                                if (innerJSON.has("displayValue")) {
                                    pageSpeedAuditRule.setDisplayValue(innerJSON.getString("displayValue"));
                                }

                                if (innerJSON.has("scoreDisplayMode")) {
                                    pageSpeedAuditRule.setScoreDisplayMode(innerJSON.getString("scoreDisplayMode"));
                                }

                                if (innerJSON.has("errorMessage")) {
                                    pageSpeedAuditRule.setErrorMessage(innerJSON.getString("errorMessage"));
                                    pageSpeedAuditRule.setErrorMessageHTML(getHTMlFormatDescription(innerJSON.getString("errorMessage")));
                                }
                                pageSpeedAuditRule.setStackPackInfo(((stackPacksRulesMap != null && stackPacksRulesMap.containsKey(auditRuleId)) ? stackPacksRulesMap.get(auditRuleId) : null));
                                pageSpeedAuditRule.setStackPackInfoHTML(((stackPacksRulesMapHTML != null && stackPacksRulesMapHTML.containsKey(auditRuleId)) ? stackPacksRulesMapHTML.get(auditRuleId) : null));
                                try {
                                    /*Preparing overallSavingsMs, overallSavingsBytes, auidtType */
                                    if (innerJSON.has("details")) {
                                        /*key: details(JSON Object)*/
                                        subInnerOneJSON = innerJSON.getJSONObject("details");
//                                    } else if (!innerJSON.has("details") && ((pageSpeedAuditRule.getErrorMessage() == null) || (pageSpeedAuditRule.getErrorMessage() != null && !pageSpeedAuditRule.getErrorMessage().trim().isEmpty()))) {
                                    } else if (!innerJSON.has("details") && (pageSpeedAuditRule.getErrorMessage() == null)) {
                                        /*This rule belongs to lab data not be considere for Opportunities/Diagnostics/Passed Audits */
                                        pageSpeedAuditRule.setLabDataFieldRule(Boolean.TRUE);
//                                        LOGGER.debug("Lab data rule:: " + pageSpeedAuditRule.getTitle() + ", score:: " + pageSpeedAuditRule.getScore() + ", id:: " + pageSpeedAuditRule.getId());
                                    }
                                    if (subInnerOneJSON != null) {
                                        if (subInnerOneJSON.has("overallSavingsMs")) {
                                            overallSavingsMs = subInnerOneJSON.getDouble("overallSavingsMs");
                                        }
                                        if (subInnerOneJSON.has("overallSavingsBytes")) {
                                            overallSavingsBytes = subInnerOneJSON.getDouble("overallSavingsBytes");
                                        }
                                        if (pageSpeedAuditRule.getOverallSavingsBytes() == 0 && pageSpeedAuditRule.getOverallSavingsMs() == 0 && subInnerOneJSON.has("summary")) {
                                            subInnerTwoJSON = subInnerOneJSON.getJSONObject("summary");
                                            if (subInnerTwoJSON != null) {
                                                if (subInnerTwoJSON.has("wastedBytes")) {
                                                    overallSavingsBytes = subInnerTwoJSON.getDouble("wastedBytes");
                                                }
                                                if (subInnerTwoJSON.has("wastedMs")) {
                                                    overallSavingsMs = subInnerTwoJSON.getDouble("wastedMs");
                                                }
                                            }
                                        }
                                        pageSpeedAuditRule.setOverallSavingsMs(overallSavingsMs);
                                        pageSpeedAuditRule.setOverallSavingsBytes(overallSavingsBytes);
                                        if (subInnerOneJSON.has("type")) {
                                            auditType = subInnerOneJSON.getString("type");
                                            pageSpeedAuditRule.setAuditType(auditType);
                                        }
                                        if (auditRuleId.equalsIgnoreCase(AUDIT_RULE_EXTERNAL[0])) {
                                            if (subInnerOneJSON.has("chains")) {
                                                subInnerOneJSON = subInnerOneJSON.getJSONObject("chains");
                                                if (subInnerOneJSON != null) {
                                                    pageSpeedAuditRule.setCriticalRequestChain(getCriticalReuqestChainUrls(subInnerOneJSON));
                                                }
                                            }
                                        } else {
//                                            LOGGER.debug("auditCategorie:: " + auditCategorie + ",      auditType:: " + auditType);
                                            /*Preparing URLs/table data for auidt rule*/
                                            Object[] auditRuleTableData = getRuleTableData(subInnerOneJSON);
                                            if (auditRuleTableData != null && auditRuleTableData.length >= 2) {
                                                pageSpeedAuditRule.setTableHeadersRow((ArrayList<AuditHeadings>) auditRuleTableData[0]);
                                                pageSpeedAuditRule.setTableChildRow((ArrayList<AuditItems>) auditRuleTableData[1]);
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    LOGGER.error("JSONException in preparing audit rule table data cause:: " + e);

                                }
                                /*Rule Categorie: Opportunitys || Diagnostics || Passed */
                                String temp = getAuditRuleCategorie(pageSpeedAuditRule.getScore(), ((pageSpeedAuditRule.getOverallSavingsBytes() > 0 || pageSpeedAuditRule.getOverallSavingsMs() > 0) ? Boolean.TRUE : Boolean.FALSE), auditCategorie, pageSpeedAuditRule.getAuditType(), pageSpeedAuditRule.getTitle());
                                if (temp != null) {
                                    pageSpeedAuditRule.setAuditCategorie(temp);
                                } else {
//                                    pageSpeedAuditRule.setAuditCategorie(diagnostics);
                                    LOGGER.debug("No audit categrie for title:: " + pageSpeedAuditRule.getTitle());
                                }

                                /*Rule Color: Fast/ Average/ SLow*/
                                pageSpeedAuditRule.setScoreColor(getScoreColor(pageSpeedAuditRule.getScore(), pageSpeedAuditRule.getTitle()));
                                /*Rule Priority*/
                                switch (pageSpeedAuditRule.getScoreColor()) {
                                    case "slow":
                                        pageSpeedAuditRule.setRulePriority((short) 3);
                                        break;
                                    case "average":
                                        pageSpeedAuditRule.setRulePriority((short) 2);
                                        break;
                                    case "fast":
                                        pageSpeedAuditRule.setRulePriority((short) 1);
                                        break;
                                    default:
                                        pageSpeedAuditRule.setRulePriority((short) 0);
                                        break;
                                }

                                ruleList.add(pageSpeedAuditRule);
                            }
                        } catch (JSONException e) {
                            LOGGER.error("JSONException in preparing auditRule Object cause:: " + e);
                        }
                    }
                    auditType = null;
                    auditRuleList = null;
                    pageSpeedAuditRule = null;
                    ruleScore = -2;
                    overallSavingsMs = 0;
                    overallSavingsBytes = 0;
                    innerJSON = null;
                    subInnerOneJSON = null;
                }
                auditType = null;
                auditRuleList = null;
                pageSpeedAuditRule = null;
                ruleScore = -2;
                overallSavingsMs = 0;
                overallSavingsBytes = 0;
                innerJSON = null;
                subInnerOneJSON = null;
            }
        }
        return ruleList;
    }

    /*Prepare List of audit rules id(s) to pull from auditJSON*/
    private Map<String, List<String>> getAuditRulesIdsfromCategories(JSONObject categoriesJSON) {
        List<String> metricsId = new ArrayList<>();
        List<String> loadOppturnityId = new ArrayList<>();
        List<String> diagnosticsId = new ArrayList<>();
        Map<String, List<String>> auditCategorieMap = new HashMap<>();
        List<String> auditCategorieList = Arrays.asList(AUDIT_RULE_CATEGORIES);
        JSONObject innerJSON = null;
        JSONArray innerJSONArray = null;
        JSONObject subInnerOneJSON = null;
        String categorie = null;
        int index = -1;
        if (categoriesJSON != null && categoriesJSON.has("performance")) {
            try {
                innerJSON = categoriesJSON.getJSONObject("performance");
                if (innerJSON != null && innerJSON.has("auditRefs")) {
                    innerJSONArray = innerJSON.getJSONArray("auditRefs");
                    if (innerJSONArray != null && innerJSONArray.length() >= 1) {
                        for (int i = 0; i < innerJSONArray.length(); i++) {
                            subInnerOneJSON = innerJSONArray.getJSONObject(i);
                            if (subInnerOneJSON != null) {
                                if (subInnerOneJSON.has("group")) {
                                    categorie = subInnerOneJSON.getString("group");
                                    if (categorie != null && auditCategorieList.contains(categorie)) {
                                        index = auditCategorieList.indexOf(categorie);
//                                        LOGGER.debug("Rule name::        " + subInnerOneJSON.getString("id") + ", categorie:: " + categorie);
                                        switch (index) {
                                            case 0:
                                                if (subInnerOneJSON.has("id")) {
                                                    metricsId.add(subInnerOneJSON.getString("id"));
                                                }
                                                break;
                                            case 1:
                                                if (subInnerOneJSON.has("id")) {
                                                    loadOppturnityId.add(subInnerOneJSON.getString("id"));
                                                }
                                                break;
                                            case 2:
                                                if (subInnerOneJSON.has("id")) {
                                                    diagnosticsId.add(subInnerOneJSON.getString("id"));
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                            index = -1;
                            categorie = null;
                            subInnerOneJSON = null;
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in Prepare List of audit rules id(s) to pull from categoriesJSON cause:: " + e);
            }
        }
        auditCategorieMap.put(AUDIT_RULE_CATEGORIES[0], metricsId);
        auditCategorieMap.put(AUDIT_RULE_CATEGORIES[1], loadOppturnityId);
        if (!diagnosticsId.contains(AUDIT_RULE_EXTERNAL[0])) {
            diagnosticsId.add(AUDIT_RULE_EXTERNAL[0]);
        }
        auditCategorieMap.put(AUDIT_RULE_CATEGORIES[2], diagnosticsId);
        return auditCategorieMap;
    }

    private ArrayList<String> getWarningsList(JSONArray warningArray, boolean htmlContent) {
        ArrayList<String> warningsList = new ArrayList<>();
        if (warningArray != null && warningArray.length() >= 1) {
            for (int i = 0; i < warningArray.length(); i++) {
                try {
                    warningsList.add(htmlContent ? getHTMlFormatDescription(warningArray.getString(i)) : warningArray.getString(i));
                } catch (JSONException e) {
                    LOGGER.error("JSONException in preparing warning list cause:: " + e);
                }
            }
        }
        return warningsList;
    }

    public String getScoreColor(double score, String title) {
        String scoreColor = null;
        if (score <= -1) {
            scoreColor = none;
//            LOGGER.debug("Score color::"+NONE);
        } else if (score <= slowScore) {
            scoreColor = slow;
        } else if (score > slowScore && score <= averageScore) {
            scoreColor = average;
        } else if (score > averageScore) {
            scoreColor = fast;
        }
        LOGGER.debug("score:" + score + ", scoreColor: " + scoreColor);
        return scoreColor;
    }

    public String getPSIColor(int seconds, String type) {
        String scoreColor = null;
        switch (type) {
            case "FCP":
                if (seconds < 1000) {
                    scoreColor = fast;
                } else if (seconds >= 1000 && seconds < 2500) {
                    scoreColor = average;
                } else if (seconds >= 2500) {
                    scoreColor = slow;
                }
                break;
            case "FID":
                if (seconds < 50) {
                    scoreColor = fast;
                } else if (seconds >= 50 && seconds < 250) {
                    scoreColor = average;
                } else if (seconds >= 250) {
                    scoreColor = slow;
                }
                break;
            default:
                break;
        }
        return scoreColor;
    }

    /* Categorie based on below parmeters
    Passed: Score = ( 1 ||  >= averageScore)
    Opportunities: Categorie = load-opportunities && type = opportunity && (overallSavingsMs || overallSavingsMs >= 1)
    Diagnostics: Categorie = diagnostics && type = table && (overallSavingsMs &&  overallSavingsMs keys not found)
     */
    private String getAuditRuleCategorie(int score, boolean isSavingBytesOrTime, String categorie, String type, String title) {
        String ruleCategorie = null;
        LOGGER.debug("score" + score + ", Title:: " + title + ",: auditCategorie:" + categorie + ",:: getAuditType:" + type);
        if (score > averageScore) {
            ruleCategorie = passed;
            LOGGER.debug("Passed tite:: " + title);

        } else if ((((categorie != null && categorie.equalsIgnoreCase(AUDIT_RULE_CATEGORIES[2]))) || (type != null && type.equalsIgnoreCase("table"))) || score == -1) {
            ruleCategorie = diagnostics;
            LOGGER.debug("DIAGNOSTICS tite:: " + title);
        } else if (isSavingBytesOrTime || (((categorie != null && categorie.equalsIgnoreCase(AUDIT_RULE_CATEGORIES[1]))) || (type != null && type.equalsIgnoreCase("opportunity")))) {
            ruleCategorie = opportunities;
            LOGGER.debug("OPPORTUNITIES" + opportunities + ", tite:: " + title);

        }
        return ruleCategorie;
    }

    private int getScoreForAuditRule(double score) {
//        LOGGER.debug("score:: " + score);
        int finalFormatedValue;
        if (score != -1) {
            score = score * 100;
            finalFormatedValue = (int) Math.ceil(score);
        } else {
            finalFormatedValue = (int) score;
        }
//        LOGGER.debug("finalFormatedValue:: " + finalFormatedValue);
        return finalFormatedValue;
    }

    private Object[] getRuleTableData(JSONObject detailsJSON) {
        Object[] objectsList = new Object[2];
        ArrayList<AuditHeadings> auditHeadingsList = new ArrayList<>();
        ArrayList<AuditItems> auditItemsList = new ArrayList<>();
        JSONObject innerJSON = null;
        JSONArray innerArray = null;
        AuditHeadings auditHeadings = null;
        AuditItems auditItems = null;
        if (detailsJSON != null) {
            try {
                /*Collecting table header row and column information*/
                if (detailsJSON.has("headings")) {
                    innerArray = detailsJSON.getJSONArray("headings");
                    if (innerArray != null && innerArray.length() != 0) {
                        for (int count = 0; count < innerArray.length(); count++) {
                            innerJSON = innerArray.getJSONObject(count);
                            if (innerJSON != null) {
                                auditHeadings = new AuditHeadings();

                                auditHeadings.setHeaderColumnIndex(count);
                                /*Column title: (possible keys: label/text)*/
                                if (innerJSON.has("text")) {
                                    auditHeadings.setText(innerJSON.getString("text"));
                                } else if (innerJSON.has("label")) {
                                    auditHeadings.setText(innerJSON.getString("label"));
                                }
                                /*Column value type: (possible keys: itemType/valueType)*/
                                if (innerJSON.has("itemType")) {
                                    auditHeadings.setItemType(innerJSON.getString("itemType"));
                                } else if (innerJSON.has("valueType")) {
                                    auditHeadings.setItemType(innerJSON.getString("valueType"));
                                }
                                /*Column key: (possible keys: key)*/
                                if (innerJSON.has("key")) {
                                    auditHeadings.setKey(innerJSON.getString("key"));
                                }
                                /*Column displayUnit: (possible keys: itemType/valueType)*/
                                if (innerJSON.has("displayUnit")) {
                                    auditHeadings.setDisplayUnit(innerJSON.getString("displayUnit"));
                                }

                                /*If text/labelare not found for any one of header row column
                                then it mean no label for that column 
                                example URL: https://www.amazon.in/
                                Audit rule: Serve images in next-gen formats*/
                                if (auditHeadings.getText() == null) {
                                    auditHeadings.setText(none);
                                }
                                auditHeadingsList.add(auditHeadings);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONEXception in preparing getRuleTableData cause:: " + e);
            }
            Object objtype = null;
            int parentRowColoumnIndex = -1;
            String parentRowColoumnKey = null;
            try {
                /*Collecting table child rows objects*/
                if (detailsJSON.has("items")) {
                    innerArray = detailsJSON.getJSONArray("items");
                    if (innerArray != null && innerArray.length() != 0) {
                        for (int count = 0; count < innerArray.length(); count++) {
                            innerJSON = innerArray.getJSONObject(count);
                            if (innerJSON != null) {
                                auditItems = new AuditItems();
                                /*Preparing child row object for respective cloumns based on column index*/
                                for (AuditHeadings ah : auditHeadingsList) {
                                    if (ah != null) {
                                        parentRowColoumnIndex = ah.getHeaderColumnIndex();
                                        parentRowColoumnKey = ah.getKey();
                                        if (innerJSON.has(parentRowColoumnKey)) {
                                            try {
                                                switch (parentRowColoumnIndex) {
                                                    case 0:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn0(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    case 1:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn1(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    case 2:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn2(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    case 3:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn3(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    case 4:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn4(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    case 5:
                                                        objtype = innerJSON.get(parentRowColoumnKey);
                                                        if (objtype != null && parentRowColoumnKey != null) {
                                                            auditItems.setColumn5(getValueFromOject(objtype, innerJSON, parentRowColoumnKey));
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            } catch (Exception e) {
                                                LOGGER.error("JSONEXception in preparing child rows object cause:: " + e);
                                            }
                                        }
                                        parentRowColoumnIndex = -1;
                                        parentRowColoumnKey = null;
                                    }
                                    objtype = null;

                                }
                                auditItemsList.add(auditItems);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONEXception in preparing getRuleTableData cause:: " + e);
            }
        }
        objectsList[0] = auditHeadingsList;
        objectsList[1] = auditItemsList;
        return objectsList;
    }

    private String getValueFromOject(Object objtype, JSONObject innerJSON, String parentRowColoumnKey) {
        String value = null;
        try {
            if (objtype instanceof Integer) {
                if (parentRowColoumnKey.equalsIgnoreCase("cacheLifetimeMs")) {
                    value = getCacheTTLValueV2(innerJSON.getInt(parentRowColoumnKey));
                } else {
                    value = String.valueOf(innerJSON.getInt(parentRowColoumnKey));
                }
            } else if (objtype instanceof Long) {
                if (parentRowColoumnKey.equalsIgnoreCase("cacheLifetimeMs")) {
                    value = getCacheTTLValueV2(innerJSON.getLong(parentRowColoumnKey));
                } else {
                    value = String.valueOf(innerJSON.getLong(parentRowColoumnKey));
                }
            } else if (objtype instanceof Double) {
                if (parentRowColoumnKey.equalsIgnoreCase("cacheLifetimeMs")) {
                    value = getCacheTTLValueV2((long) innerJSON.getDouble(parentRowColoumnKey));
                } else {
                    value = String.valueOf(innerJSON.getDouble(parentRowColoumnKey));
                }
            } else if (objtype instanceof JSONObject && parentRowColoumnKey.equalsIgnoreCase("entity")) {
                JSONObject innerObj = null;
                if (innerJSON.has(parentRowColoumnKey)) {
                    innerObj = innerJSON.getJSONObject(parentRowColoumnKey);
                    if (innerObj != null) {
                        if (innerObj.has("text")) {
                            value = innerObj.getString("text");
                        }
                        if (innerObj.has("url")) {
//                            value = value != null ? value + " (" + innerObj.getString("url") + ")" : innerObj.getString("url");
                            value = value != null ? value + " (<\"" + innerObj.getString("url") + "\">)" : innerObj.getString("url");
                        }
                    }
                }
            } else if (objtype instanceof JSONObject && parentRowColoumnKey.equalsIgnoreCase("element")) {
                JSONObject innerObj = null;
                if (innerJSON.has(parentRowColoumnKey)) {
                    innerObj = innerJSON.getJSONObject(parentRowColoumnKey);
                    if (innerObj != null) {
                        if (innerObj.has("value")) {
                            value = innerObj.getString("value");
                        }
                    }
                }
            } else if (objtype instanceof String) {
                value = (innerJSON.getString(parentRowColoumnKey));
            }

        } catch (JSONException e) {
            LOGGER.error("JSONException in getValueFromOject cause:: ", e);
        }
        return value;
    }

    private ArrayList<CriticalRequestChain> getCriticalReuqestChainUrls(JSONObject chainJSON) {
        ArrayList<CriticalRequestChain> chainList = null;
        Iterator keys = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        JSONObject subInnerThreeJSON = null;
        JSONObject subInnerFourJSON = null;
        String currentDynamicKey = null;
        if (chainJSON != null) {
            keys = chainJSON.keys();
            /*Creating list*/
            chainList = new ArrayList<>();
            if (keys != null && keys.hasNext()) {
                currentDynamicKey = (String) keys.next();
                try {
                    subInnerOneJSON = chainJSON.getJSONObject(currentDynamicKey);
                    if (subInnerOneJSON != null && subInnerOneJSON.has("request")) {
                        subInnerTwoJSON = subInnerOneJSON.getJSONObject("request");
                        if (subInnerTwoJSON != null) {
                            /*Adding host url object to list*/
                            chainList.add(getCriticalRequestChainObject(subInnerTwoJSON, CriticalRequestChain.HOST_URL, currentDynamicKey));
                        }
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getting root elemnet of critical chain cause:: ", e);
                } catch (Exception e) {
                    LOGGER.error("Exception in creating getting root elemnet of critical chain cause:: ", e);
                }
                /*Preparing child objects*/
                try {
                    subInnerOneJSON = chainJSON.getJSONObject(currentDynamicKey);
                    if (subInnerOneJSON != null && subInnerOneJSON.has("children")) {
                        subInnerTwoJSON = subInnerOneJSON.getJSONObject("children");
                        if (subInnerTwoJSON != null) {
                            keys = subInnerTwoJSON.keys();
                            if (keys != null) {
                                while (keys.hasNext()) {
                                    currentDynamicKey = (String) keys.next();
//                                        LOGGER.debug("currentDynamicKey:: " + currentDynamicKey);
                                    subInnerThreeJSON = subInnerTwoJSON.getJSONObject(currentDynamicKey);
                                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("request")) {
                                        try {
                                            subInnerFourJSON = subInnerThreeJSON.getJSONObject("request");
                                            if (subInnerFourJSON != null) {
                                                chainList.add(getCriticalRequestChainObject(subInnerFourJSON, CriticalRequestChain.CHILDERN_URL, currentDynamicKey));
                                            }
                                        } catch (JSONException e) {
                                            LOGGER.error("JSONException in getting child elemnets of critical chain cause:: ", e);
                                        } catch (Exception e) {
                                            LOGGER.error("Exception in creating child elemnets of critical chain cause:: ", e);
                                        }
                                    }
                                    /*Looking for sub child objects*/
                                    if (subInnerThreeJSON != null && subInnerThreeJSON.has("children")) {
                                        List<CriticalRequestChain> tempList = getCriticalChainSubChildList(subInnerThreeJSON.getJSONObject("children"));
                                        if (tempList != null) {
                                            chainList.addAll(tempList);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getting child elemnets root id of critical chain cause:: ", e);
                } catch (Exception e) {
                    LOGGER.error("Exception in creating child elemnets root id  of critical chain cause:: ", e);
                }
            }
        }
        return chainList;
    }

    private ArrayList<CriticalRequestChain> getCriticalChainSubChildList(JSONObject chainJSON) {
        ArrayList<CriticalRequestChain> subChildList = null;
        JSONObject subInnerThreeJSON = null;
        JSONObject subInnerFourJSON = null;
        String currentDynamicKey = null;
        Iterator keys = null;
        if (chainJSON != null) {
            try {
                keys = chainJSON.keys();
                if (keys != null) {
                    subChildList = new ArrayList<>();
                    while (keys.hasNext()) {
                        currentDynamicKey = (String) keys.next();
                        LOGGER.debug("in sub list currentDynamicKey:: " + currentDynamicKey);
                        subInnerThreeJSON = chainJSON.getJSONObject(currentDynamicKey);
                        if (subInnerThreeJSON != null && subInnerThreeJSON.has("request")) {
                            subInnerFourJSON = subInnerThreeJSON.getJSONObject("request");
                            if (subInnerFourJSON != null) {
                                /*Adding child object*/
                                subChildList.add(getCriticalRequestChainObject(subInnerFourJSON, CriticalRequestChain.CHILDERN_URL, currentDynamicKey));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("JSONException in getting sub child elemnets root key id  of critical chain cause:: ", e);
            } catch (Exception e) {
                LOGGER.error("Exception in creating sub child elemnets root key id of critical chain cause:: ", e);
            }
        }
        if (subChildList != null) {
            LOGGER.debug("Length of subchild list:: " + subChildList.size());
        }
        return subChildList;
    }

    private CriticalRequestChain getCriticalRequestChainObject(JSONObject responseJSON, String urlType, String currentDynamicKey) {
        CriticalRequestChain requestChain = null;
        try {
            requestChain = new CriticalRequestChain();
            requestChain.setUrlType(urlType);
            requestChain.setUrlId(currentDynamicKey);
            if (responseJSON.has("url")) {
                requestChain.setUrl(responseJSON.getString("url"));
            }
            if (responseJSON.has("transferSize")) {
                requestChain.setTransferSize((responseJSON.getDouble("transferSize") / kibibyes));
            }
            if (responseJSON.has("startTime")) {
                requestChain.setStartTime(responseJSON.getDouble("startTime"));
            }
            if (responseJSON.has("endTime")) {
                requestChain.setEndTime(responseJSON.getDouble("endTime"));
            }
            if (responseJSON.has("responseReceivedTime")) {
                requestChain.setResponseReceivedTime(responseJSON.getDouble("responseReceivedTime"));
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException in getting sub child elemnets of critical chain cause:: ", e);
        }
        return requestChain;
    }

    private String getHTMlFormatDescription(String apiDescription) {
        String htmlDescriptionFormat = apiDescription;
        Matcher matcher = Pattern.compile("\\[([^]]+)\\]\\(([^)]+)\\)").matcher(apiDescription);
        if (matcher != null) {
            while (matcher.find()) {
                if (matcher.group(2).startsWith("http")) {
                    String text = matcher.group(1);
                    String link = matcher.group(2);
//                    System.out.println("text:: " + text + ",  link::" + link);
                    String replaceWith = "<a target=\"_blank\" class=\"hyperLink\" href=" + "\"" + link + "\"" + ">" + text + "</a>";
                    String target = "[" + text + "](" + link + ")";
                    htmlDescriptionFormat = htmlDescriptionFormat.replace(target, replaceWith);
                }
            }
        }
        LOGGER.debug("Original ApiResponse:: " + apiDescription);
        LOGGER.debug("HTML Formated ApiResponse:: " + htmlDescriptionFormat);
        return htmlDescriptionFormat;
    }

    public static String getCacheTTLValueV2(long milliseconds) {
        String displayValue = null;
        if (milliseconds > 0) {
            long days = TimeUnit.MILLISECONDS.toDays(milliseconds);
            long years = days / 365;
            days %= 365;
            /*long months = days / 30;
            days %= 30;
            long wk = days / 7;
            days %= 7;*/
            long hours = TimeUnit.MILLISECONDS.toHours(milliseconds) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));
            long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
            long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));
            long ms = TimeUnit.MILLISECONDS.toMillis(milliseconds) - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(milliseconds));
//System.out.println(String.format("%d Years %d Months %d Days %d Hours %d Minutes %d Seconds %d Milliseconds", years, months, days, hours, minutes, seconds, ms));
//System.out.println(String.format("%d Years %d Months %d Weeks %d Days %d Hours %d Minutes %d Seconds %d Milliseconds", years, months, wk, days, hours, minutes, seconds, ms));
            displayValue = "";
            if (years != 0) {
//                displayValue += (years != 1) ? (years + " years ") : (years + " year ");
                displayValue += (years + " y");
            }
            if (days != 0) {
                displayValue += (days + " d ");
            }
            if (hours != 0) {
                displayValue += (hours + " h ");
            }
            if (minutes != 0) {
                displayValue += (minutes + " m ");
            }
            if (seconds != 0 || milliseconds < 1000) {
                displayValue += (seconds + " s ");
            }

        } else {
            displayValue = "None";
        }
        return displayValue;
    }

    /*Tool adivisor*/
    public List<String> getQueryEndPointURLs(String queryWebPageURL, List<String> deviceList, List<String> categoryList) {
        String endPointURL = null;
        List<String> endPointList = new ArrayList<>();
        String encodedQueryURL = getEncodedURLString(queryWebPageURL);
        String categoryQueryURL = getCategoryString(categoryList);
        for (String device : deviceList) {
            endPointURL = generateGooglePSIAPIEndPointURL(encodedQueryURL, device.toLowerCase(), categoryQueryURL);
            endPointList.add(endPointURL);
        }
        return endPointList;
    }

    public Map<String, Integer> getGoogleLightScoreForPageURL(String queryWebPageURL) {
        Map<String, Integer> pageLightScore = new HashMap<>();     
        List<String> endPointURLList = getQueryEndPointURLs(queryWebPageURL,Arrays.asList(desktopUserAgent,mobileUserAgent),Arrays.asList(lighthouseCategorys));
        if (endPointURLList != null && !endPointURLList.isEmpty()) {
            ExecutorService executorService = Executors.newFixedThreadPool(endPointURLList.size());
            CountDownLatch latchObj = new CountDownLatch(endPointURLList.size());
            List<Future<Map<String,Integer>>> pageSpeedInsightsFutureList = new ArrayList<>();
            if (!endPointURLList.isEmpty()) {
                endPointURLList.forEach((apiEndPointURL) -> {
                    pageSpeedInsightsFutureList.add(executorService.submit(new PageSpeedInsightsScoreTask(apiEndPointURL, latchObj, this)));
                });
            }
            try {
                latchObj.await();
            } catch (InterruptedException ex) {
                LOGGER.error("Interrupted | Execution exception latch object count cause:", ex);
            }
            if (!pageSpeedInsightsFutureList.isEmpty()) {
                pageSpeedInsightsFutureList.forEach((pageSpeedInsights) -> {
                    if (pageSpeedInsights != null) {
                        try {
                            Map<String, Integer> tempPageLightScore = pageSpeedInsights.get();
                            if(tempPageLightScore.containsKey(desktopUserAgent)){
                                pageLightScore.put(desktopUserAgent, (int)tempPageLightScore.get(desktopUserAgent));
                            }
                            if(tempPageLightScore.containsKey(mobileUserAgent)){
                                pageLightScore.put(mobileUserAgent, (int)tempPageLightScore.get(mobileUserAgent));
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            LOGGER.error("Interrupted | Execution exception pulling pagespeed insights object from future obj cause:", e);
                        }
                    }
                });
            }
            executorService.shutdown();
        }
        LOGGER.info("Tool adivisor pageSpeed light out score for queryURL:: "+queryWebPageURL+", and response list size:: "+pageLightScore.size());
        return pageLightScore;
    }

    public Map<String, Integer> getGoogleLightScoreForDevice(String endpointURL) {
        int lightHousescore = 0;
        Map<String, Integer> deviceScore = new HashMap<>();
        JSONObject responseJSON = Common.getJSONFromAPIResponse(endpointURL);
        JSONObject innerJSON = null;
        JSONObject subInnerOneJSON = null;
        JSONObject subInnerTwoJSON = null;
        /*Checking for status : Success or Error*/
        if (responseJSON != null && !responseJSON.has("error")) {
            /*responseJSON contains Key: lighthouseResult*/
            if (responseJSON.has(LIGHTHOUSERESULT)) {
                try {
                    /*Pulling Requested/ Final URL */
                    innerJSON = responseJSON.getJSONObject(LIGHTHOUSERESULT);
                    if (innerJSON != null) {
                        /*Performance Score Type*/
                        if (innerJSON.has("categories")) {
                            subInnerOneJSON = innerJSON.getJSONObject("categories");
                            if (subInnerOneJSON != null) {
                                if (subInnerOneJSON.has("performance")) {
                                    subInnerTwoJSON = subInnerOneJSON.getJSONObject("performance");
                                    if (subInnerTwoJSON != null && subInnerTwoJSON.has("score")) {
                                        lightHousescore = roundUpTheValue(subInnerTwoJSON.getDouble("score"));
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    LOGGER.error("JSONException in getGoogleLightScoreForDevice  from API resposne cause: " + e);
                }
            }
        }
        if (endpointURL.contains("&strategy=" + desktopUserAgent)) {
            deviceScore.put(desktopUserAgent, lightHousescore);
            LOGGER.info("In getGoogleLightScoreForDevice for desktop score:: "+desktopUserAgent);
        } else if (endpointURL.contains("&strategy=" + mobileUserAgent)) {
            deviceScore.put(mobileUserAgent, lightHousescore);
            LOGGER.info("In getGoogleLightScoreForDevice for mobile score:: "+mobileUserAgent);
        }
        return deviceScore;
    }
}