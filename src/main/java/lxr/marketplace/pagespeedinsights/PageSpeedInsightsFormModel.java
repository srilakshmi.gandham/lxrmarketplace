/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.util.List;

/**
 *
 * @author vidyasagar.korada
 */
public class PageSpeedInsightsFormModel{
     private String queryWebPageURL;
     private boolean newAnalysis;
     private List<PageSpeedInsights> pageSpeedInsightsList;

    public String getQueryWebPageURL() {
        return queryWebPageURL;
    }

    public void setQueryWebPageURL(String queryWebPageURL) {
        this.queryWebPageURL = queryWebPageURL;
    }

    public List<PageSpeedInsights> getPageSpeedInsightsList() {
        return pageSpeedInsightsList;
    }

    public void setPageSpeedInsightsList(List<PageSpeedInsights> pageSpeedInsightsList) {
        this.pageSpeedInsightsList = pageSpeedInsightsList;
    }

    public boolean isNewAnalysis() {
        return newAnalysis;
    }

    public void setNewAnalysis(boolean newAnalysis) {
        this.newAnalysis = newAnalysis;
    }

     
}
