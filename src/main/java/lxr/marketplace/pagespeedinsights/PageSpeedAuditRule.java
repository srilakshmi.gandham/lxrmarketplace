/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author vidyasagar.korada
 */
public class PageSpeedAuditRule implements Serializable{

    private String id;
    private String title;
    private String description;
    private String scoreDisplayMode;
    private String displayValue;
    private String auditType;
    private ArrayList<String> warnings;
    private ArrayList<String> warningsHTML;
    private ArrayList<AuditHeadings> tableHeadersRow;
    private ArrayList<AuditItems> tableChildRow;
    private ArrayList<CriticalRequestChain> criticalRequestChain;

    private double overallSavingsMs;
    private double overallSavingsBytes;
    private int score;
    private short  rulePriority;

    private String stackPackInfo;
    private String auditCategorie;

    private String scoreColor;

    private String errorMessage;

    /*TRUE: If details object are not present in audit rule object 
    will consider as lab data audit rule*/
    private boolean labDataFieldRule;

    private String descriptionHTML;
    private String stackPackInfoHTML;
    private String errorMessageHTML;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScoreDisplayMode() {
        return scoreDisplayMode;
    }

    public void setScoreDisplayMode(String scoreDisplayMode) {
        this.scoreDisplayMode = scoreDisplayMode;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getAuditType() {
        return auditType;
    }

    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    public ArrayList<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(ArrayList<String> warnings) {
        this.warnings = warnings;
    }

    public ArrayList<AuditHeadings> getTableHeadersRow() {
        return tableHeadersRow;
    }

    public void setTableHeadersRow(ArrayList<AuditHeadings> tableHeadersRow) {
        this.tableHeadersRow = tableHeadersRow;
    }

    public ArrayList<AuditItems> getTableChildRow() {
        return tableChildRow;
    }

    public void setTableChildRow(ArrayList<AuditItems> tableChildRow) {
        this.tableChildRow = tableChildRow;
    }

    public ArrayList<CriticalRequestChain> getCriticalRequestChain() {
        return criticalRequestChain;
    }

    public void setCriticalRequestChain(ArrayList<CriticalRequestChain> criticalRequestChain) {
        this.criticalRequestChain = criticalRequestChain;
    }

    public double getOverallSavingsMs() {
        return overallSavingsMs;
    }

    public void setOverallSavingsMs(double overallSavingsMs) {
        this.overallSavingsMs = overallSavingsMs;
    }

    public double getOverallSavingsBytes() {
        return overallSavingsBytes;
    }

    public void setOverallSavingsBytes(double overallSavingsBytes) {
        this.overallSavingsBytes = overallSavingsBytes;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getStackPackInfo() {
        return stackPackInfo;
    }

    public void setStackPackInfo(String stackPackInfo) {
        this.stackPackInfo = stackPackInfo;
    }

    public String getAuditCategorie() {
        return auditCategorie;
    }

    public void setAuditCategorie(String auditCategorie) {
        this.auditCategorie = auditCategorie;
    }

    public String getScoreColor() {
        return scoreColor;
    }

    public void setScoreColor(String scoreColor) {
        this.scoreColor = scoreColor;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isLabDataFieldRule() {
        return labDataFieldRule;
    }

    public void setLabDataFieldRule(boolean labDataFieldRule) {
        this.labDataFieldRule = labDataFieldRule;
    }

    public String getDescriptionHTML() {
        return descriptionHTML;
    }

    public void setDescriptionHTML(String descriptionHTML) {
        this.descriptionHTML = descriptionHTML;
    }

    public String getStackPackInfoHTML() {
        return stackPackInfoHTML;
    }

    public void setStackPackInfoHTML(String stackPackInfoHTML) {
        this.stackPackInfoHTML = stackPackInfoHTML;
    }

    public String getErrorMessageHTML() {
        return errorMessageHTML;
    }

    public void setErrorMessageHTML(String errorMessageHTML) {
        this.errorMessageHTML = errorMessageHTML;
    }

    public ArrayList<String> getWarningsHTML() {
        return warningsHTML;
    }

    public void setWarningsHTML(ArrayList<String> warningsHTML) {
        this.warningsHTML = warningsHTML;
    }
    
    public short getRulePriority() {
        return rulePriority;
    }

    public void setRulePriority(short rulePriority) {
        this.rulePriority = rulePriority;
    }
}
