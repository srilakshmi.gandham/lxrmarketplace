/**
 * @author NE16T1213-Sagar
 */
package lxr.marketplace.pagespeedinsights;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/page-speed-insights-tool.html")
public class PageSpeedInsightsController {

    private static final Logger LOGGER = Logger.getLogger(PageSpeedInsightsController.class);

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @Autowired
    private String downloadFolder;

    @Autowired
    private PageSpeedInsightsService pageSpeedInsightsService;

    @Autowired
    private PageSpeedInsightReportService pageSpeedInsightReportService;

    private Login user;

    JSONObject userInputJson = null;

    @Value("${PageSpeedInsights.view}")
    private String FORM_VIEW;

    /*@return : 
     * 1: Navigation to tool page. 
     * 2: Navigation to tool result page fpr the requests i.e ('backToSuccessPage'|| 'loginrefresh') ex. from ask the expert confirmation page. 
     */
    @RequestMapping(method = RequestMethod.GET)
    public String navigationToolPage(HttpSession session, HttpServletRequest request) {
        if ((WebUtils.hasSubmitParameter(request, "loginRefresh")) || WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            session.setAttribute("loginrefresh", "loginrefresh");
        } else {
            session.removeAttribute("pageSpeedInsightsFormSessionObj");
        }
        return FORM_VIEW;
    }

    /*@return : 
     * Result Page: Analaysis the given URL to fetch page speed insights. 
     */
    @RequestMapping(params = {"request=analyze"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object[] processingPageSpeedInsightsQueryURL(@RequestBody PageSpeedInsightsFormModel pageSpeedInsightsFormModel, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        /*
        [0]: True: Result Fetching Completed, False: Exception in processing result
        [1]: True: No erros in both desktop and mobile devices, False: Errors in respective devices.
        [2]: Contains Mobile Device PSI Data (OR) Error message
        [3]: Contains Desktop Device PSI Data
         */
        Object pageSpeedResponse[] = new Object[6];
        PageSpeedInsightsFormModel processcedPageSpeedInsightsFormModel = null;
        List<PageSpeedInsights> pageSpeedInsightsResultList = null;

        /*Searching processced list from session for respective search query to handle back to success page event*/
        if (session.getAttribute("pageSpeedInsightsFormSessionObj") != null && !pageSpeedInsightsFormModel.isNewAnalysis()) {
            processcedPageSpeedInsightsFormModel = (PageSpeedInsightsFormModel) session.getAttribute("pageSpeedInsightsFormSessionObj");
            pageSpeedInsightsResultList = (processcedPageSpeedInsightsFormModel.getQueryWebPageURL().equalsIgnoreCase((pageSpeedInsightsFormModel.getQueryWebPageURL()))
                    ? (processcedPageSpeedInsightsFormModel.getPageSpeedInsightsList()) : null);
            pageSpeedResponse[0] = Boolean.TRUE;
        }

        if (pageSpeedInsightsResultList == null) {
            /*Removing session objects to handle to find existing query processing*/
            session.removeAttribute("pageSpeedInsightsFormSessionObj");

            Tool toolObj = new Tool();
            toolObj.setToolId(35);
            toolObj.setToolName(Common.PAGE_SPEED_INSIGHTS);
            toolObj.setToolLink("page-speed-insights-tool.html");
            session.setAttribute("toolObj", toolObj);

            try {
                Map<String, Object> pageSpeedData = null;
                if (pageSpeedInsightsFormModel != null && (pageSpeedInsightsFormModel.getQueryWebPageURL() != null && !pageSpeedInsightsFormModel.getQueryWebPageURL().trim().isEmpty())) {
                    pageSpeedData = pageSpeedInsightsService.analyzePageSpeedMetrics(pageSpeedInsightsFormModel.getQueryWebPageURL());
                }
                if (pageSpeedData != null && !pageSpeedData.isEmpty() && pageSpeedInsightsFormModel != null) {
                    if (pageSpeedData.containsKey("Success")) {
                        /*isDataPulled*/
                        pageSpeedResponse[0] = (Boolean) pageSpeedData.get("Success");
                    }
                    if (pageSpeedData.containsKey(pageSpeedInsightsFormModel.getQueryWebPageURL())) {
                        ObjectMapper objectMapper = new ObjectMapper();
                        pageSpeedInsightsResultList = objectMapper.readValue(objectMapper.writeValueAsString(pageSpeedData.get(pageSpeedInsightsFormModel.getQueryWebPageURL())), new TypeReference<List<PageSpeedInsights>>() {
                        });
                    }
                    if (pageSpeedData.containsKey("Error")) {
                        pageSpeedResponse[1] = Boolean.FALSE;
                        pageSpeedResponse[2] = (String) pageSpeedData.get("Error");
                    }
                }
            } catch (IOException e) {
                LOGGER.error("IOException | JSONException in processingPageSpeedInsightsQueryURL cause:: ", e);
            }
        }

        /*Session Handling*/
        if (pageSpeedInsightsResultList != null && !pageSpeedInsightsResultList.isEmpty() && pageSpeedInsightsFormModel != null) {
            /*Count of 400 status code of devices*/
            pageSpeedResponse[1] = pageSpeedInsightsService.checkForErrorsInResponse(pageSpeedInsightsResultList);
            /*Mobile*/
            PageSpeedInsights insightObj = null;
            insightObj = pageSpeedInsightsResultList.stream().filter((insight -> (insight != null && insight.getDeviceCategory() != null && insight.getDeviceCategory().equalsIgnoreCase(pageSpeedInsightsService.getMobileUserAgent())))).findAny().orElse(null);
            pageSpeedResponse[2] = insightObj;
            /*Desktop*/
            insightObj = pageSpeedInsightsResultList.stream().filter((insight -> (insight != null && insight.getDeviceCategory().equalsIgnoreCase(pageSpeedInsightsService.getDesktopUserAgent())))).findAny().orElse(null);
            pageSpeedResponse[3] = insightObj;
            pageSpeedResponse[4] = pageSpeedInsightsResultList;
            pageSpeedInsightsFormModel.setPageSpeedInsightsList(pageSpeedInsightsResultList);
            session.setAttribute("pageSpeedInsightsFormSessionObj", pageSpeedInsightsFormModel);
        } else {
            pageSpeedResponse[0] = Boolean.FALSE;
            pageSpeedResponse[1] = "Unable to process your webpage URL, Please try again after some time";
        }

        if (pageSpeedInsightsFormModel != null && pageSpeedInsightsFormModel.isNewAnalysis()) {
            /*Saving guest user data*/
            Common.modifyDummyUserInToolorVideoUsage(request, response);
            /*Updatng too usage count.*/
            if (session.getAttribute("userSession") != null) {
                Session userSession = (Session) session.getAttribute("userSession");
                userSession.addToolUsage(35);
            }
        }
        return pageSpeedResponse;
    }

    /*
     * @return : Navigation to tool result page, when success full login by user through 
       PopUp/SignUP or login in header section.
     */
    @RequestMapping(params = {"loginRefresh=1"}, method = RequestMethod.GET)
    public String navigationToResultPageUserLogin(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("pageSpeedInsights") PageSpeedInsights pageSpeedInsights, HttpSession session, Model model) {
        String requestParam = request.getParameter("backToSuccessPage");
        session.setAttribute("loginrefresh", "loginrefresh");
        List<PageSpeedInsights> pageSpeedInsightsResult = null;
        if ((session.getAttribute("pageSpeedInsightsDashBoard") != null)
                || ((requestParam != null) && (requestParam.equalsIgnoreCase("'backToSuccessPage'")))) {
            pageSpeedInsightsResult = (List<PageSpeedInsights>) session.getAttribute("pageSpeedInsightsDashBoard");
            if (pageSpeedInsightsResult != null) {
                model.addAttribute(pageSpeedInsights);
                model.addAttribute("pageSpeedSuccess", true);
                pageSpeedInsights.setRequestedUrl((String) session.getAttribute("validPageSpeedUrl"));
                return FORM_VIEW;
            }
        }
        model.addAttribute("pageSpeedSuccess", false);
        return FORM_VIEW;
    }

    /*
     *Request to handle to generate tool report in PDF.
     */
    @RequestMapping(params = {"request=download"}, method = RequestMethod.POST)
    public String createPageSpeedInsightsExcelReport(HttpServletRequest request, HttpServletResponse response, String pageSpeedInsightsResult, HttpSession session) {
        long userId = 0;
        if (session.getAttribute("user") != null) {
            user = (Login) session.getAttribute("user");
        }
        if (user != null) {
            userId = user.getId();
        }
        String insightFileName = null;
        List<PageSpeedInsights> pageSpeedInsightsResultList = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            pageSpeedInsightsResultList = objectMapper.readValue(pageSpeedInsightsResult, new TypeReference<List<PageSpeedInsights>>() {
            });
        } catch (IOException e) {
            LOGGER.error("Exception in preparing psi list using object mapper cause:: ", e);
        }
        if (pageSpeedInsightsResultList != null && !pageSpeedInsightsResultList.isEmpty()) {
            String fileName = Common.createFileName("LXRMarketplace_PageSpeed_Insights_Report_" + userId);
            LOGGER.info("PSI report fileName is ==" + fileName);
            insightFileName = pageSpeedInsightReportService.generatePageSpeedInsightsAuditReport(pageSpeedInsightsResultList, downloadFolder, fileName);
            if (insightFileName != null && !insightFileName.trim().equals("")) {
                LOGGER.info("File name of downloadable report  is:: " + insightFileName);
                Common.downloadReport(response, downloadFolder, insightFileName, ".xlsx");
            }
        }
        return null;
    }

    /*
     *Request to handle to  send mail PDF tool report.
     */
    @RequestMapping(params = {"request=sendMail"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] sendMailPageSpeedExcelReport(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("pageSpeedInsightsResult") String pageSpeedInsightsResult, @RequestParam("userEmail") String userEmail, HttpSession session) {
        long userId = 0;
        if (session.getAttribute("user") != null) {
            user = (Login) session.getAttribute("user");
        }
        if (user != null) {
            userId = user.getId();
        }
        boolean mailStatus = false;
        Object[] responseData = new Object[1];
        if (userEmail != null && !userEmail.trim().equals("") && userEmail.contains("@")) {
            String insightFileName = null;
            List<PageSpeedInsights> pageSpeedInsightsResultList = null;
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                pageSpeedInsightsResultList = objectMapper.readValue(pageSpeedInsightsResult, new TypeReference<List<PageSpeedInsights>>() {
                });
            } catch (IOException e) {
                LOGGER.error("Exception in preparing psi list using object mapper cause:: ", e);
            }
            if (pageSpeedInsightsResultList != null && !pageSpeedInsightsResultList.isEmpty()) {
                String fileName = Common.createFileName("LXRMarketplace_PageSpeed_Insights_Report_" + userId);
                LOGGER.info("Email PSI report fileName is ==" + fileName);
                insightFileName = pageSpeedInsightReportService.generatePageSpeedInsightsAuditReport(pageSpeedInsightsResultList, downloadFolder, fileName);
            }
            if (insightFileName != null && !insightFileName.trim().equals("")) {
                LOGGER.info("File name of send mail report  is:: " + insightFileName);
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, insightFileName, userEmail, "PageSpeed Insights");
            }
        }
        responseData[0] = mailStatus;
        return responseData;
    }

    /*
     * Preparing ASK the expert analysis for user. 
     */
    @RequestMapping(params = {"requestMethod=pageSpeedATE"}, method = RequestMethod.POST)
    public void processATEofMostUsedKeywords(HttpServletResponse response, HttpSession session,
            @RequestParam("userATEDomain") String userATEDomain) {
        Session userSession = (Session) session.getAttribute("userSession");
        user = (Login) session.getAttribute("user");
        String toolIssues = "", questionInfo = "";
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(userATEDomain.trim());
        if (userInputJson != null) {
            lxrmAskExpert.setOtherInputs(userInputJson.toString());
        }
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.PAGE_SPEED_INSIGHTS);
        lxrmAskExpertService.addInJson(lxrmAskExpert, response);
//                if the request is not from  mail then we should add this input to the session object
        if (userInputJson != null) {
            String toolUrl = "/page-speed-insights-tool.html";
            userSession.addUserAnalysisInfo(user.getId(), 35, userATEDomain, toolIssues,
                    questionInfo, userInputJson.toString(), toolUrl);
            userInputJson = null;
        }
    }
}
