/**
 * @author NE16T1213
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;
import java.util.List;

public class PageSpeedInsights implements Serializable{
   
    private int statusCode;
    private String requestedUrl;
    private String finalUrl;
    private int performanceScore;
    private String analysisUTCTimestamp;
    private String deviceCategory;
    private String finalScreenshotDesc;
    private String screenshotThumbnailDesc;
    private RealWorldFieldData realWorldFieldData;
    private List<ScreenShot> screenShots;
    private List<PageSpeedAuditRule> auditRules;
    private StackPacks stackPacks;
    
    private String loadOpportunitiesDesc;
    private String loadOpportunitiesDescReport;
    private String diagnosticsDesc;
    private String diagnosticsDescReport;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getRequestedUrl() {
        return requestedUrl;
    }

    public void setRequestedUrl(String requestedUrl) {
        this.requestedUrl = requestedUrl;
    }

    public String getFinalUrl() {
        return finalUrl;
    }

    public void setFinalUrl(String finalUrl) {
        this.finalUrl = finalUrl;
    }

    public int getPerformanceScore() {
        return performanceScore;
    }

    public void setPerformanceScore(int performanceScore) {
        this.performanceScore = performanceScore;
    }

    public String getAnalysisUTCTimestamp() {
        return analysisUTCTimestamp;
    }

    public void setAnalysisUTCTimestamp(String analysisUTCTimestamp) {
        this.analysisUTCTimestamp = analysisUTCTimestamp;
    }

    public String getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(String deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public String getFinalScreenshotDesc() {
        return finalScreenshotDesc;
    }

    public void setFinalScreenshotDesc(String finalScreenshotDesc) {
        this.finalScreenshotDesc = finalScreenshotDesc;
    }

    public String getScreenshotThumbnailDesc() {
        return screenshotThumbnailDesc;
    }

    public void setScreenshotThumbnailDesc(String screenshotThumbnailDesc) {
        this.screenshotThumbnailDesc = screenshotThumbnailDesc;
    }

    public RealWorldFieldData getRealWorldFieldData() {
        return realWorldFieldData;
    }

    public void setRealWorldFieldData(RealWorldFieldData realWorldFieldData) {
        this.realWorldFieldData = realWorldFieldData;
    }

    public List<ScreenShot> getScreenShots() {
        return screenShots;
    }

    public void setScreenShots(List<ScreenShot> screenShots) {
        this.screenShots = screenShots;
    }

    public List<PageSpeedAuditRule> getAuditRules() {
        return auditRules;
    }

    public void setAuditRules(List<PageSpeedAuditRule> auditRules) {
        this.auditRules = auditRules;
    }

    public StackPacks getStackPacks() {
        return stackPacks;
    }

    public void setStackPacks(StackPacks stackPacks) {
        this.stackPacks = stackPacks;
    }

    public String getLoadOpportunitiesDesc() {
        return loadOpportunitiesDesc;
    }

    public void setLoadOpportunitiesDesc(String loadOpportunitiesDesc) {
        this.loadOpportunitiesDesc = loadOpportunitiesDesc;
    }

    public String getDiagnosticsDesc() {
        return diagnosticsDesc;
    }

    public void setDiagnosticsDesc(String diagnosticsDesc) {
        this.diagnosticsDesc = diagnosticsDesc;
    }

    public String getLoadOpportunitiesDescReport() {
        return loadOpportunitiesDescReport;
    }

    public void setLoadOpportunitiesDescReport(String loadOpportunitiesDescReport) {
        this.loadOpportunitiesDescReport = loadOpportunitiesDescReport;
    }

    public String getDiagnosticsDescReport() {
        return diagnosticsDescReport;
    }

    public void setDiagnosticsDescReport(String diagnosticsDescReport) {
        this.diagnosticsDescReport = diagnosticsDescReport;
    }

 
  
}
