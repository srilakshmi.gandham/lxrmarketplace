/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author vidyasagar.korada
 */
public class PageSpeedInsightsTask implements Callable<PageSpeedInsights> {

    private String endPointURL;
    private CountDownLatch latchObj;
    private PageSpeedInsightsService pageSpeedInsightsService;

    public PageSpeedInsightsTask(String endPointURL, CountDownLatch latchObj, PageSpeedInsightsService pageSpeedInsightsService) {
        this.endPointURL = endPointURL;
        this.latchObj = latchObj;
        this.pageSpeedInsightsService = pageSpeedInsightsService;
    }

    @Override
    public PageSpeedInsights call() {
        PageSpeedInsights pageSpeedInsights = this.pageSpeedInsightsService.generatePageSpeedInsighstForURLDevice(this.endPointURL);
        latchObj.countDown();
        return pageSpeedInsights;
    }
}
