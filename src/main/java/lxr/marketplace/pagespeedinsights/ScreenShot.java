/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;

/**
 *
 * @author vidyasagar.korada
 */

/*
screenshot-thumbnails: to get images when web page is loading.
final-screenshot: to get image of loaded webpage.
Both are keys in API response*/
public class ScreenShot implements Serializable{

    private String imageURL;
    private long timestamp;
    private int loadingSeconds;
    private String imageType;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getLoadingSeconds() {
        return loadingSeconds;
    }

    public void setLoadingSeconds(int loadingSeconds) {
        this.loadingSeconds = loadingSeconds;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    @Override
    public String toString() {
        return "ScreenShot{" + "timestamp=" + timestamp + ", loadingSeconds=" + loadingSeconds + ", imageType=" + imageType + '}';
    }

 
    
    

}
