/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;

/**
 *
 * @author vidyasagar.korada
 */
public class AuditHeadings implements Serializable{
    
     /*This is used to map respective child column value*/
    private String key;
    private String itemType;
    /*text/label*/
    private String text;
    private String displayUnit;
    /*This is used to map between 
    header row row coloumn 
    and child row coloumn*/
    int headerColumnIndex;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDisplayUnit() {
        return displayUnit;
    }

    public void setDisplayUnit(String displayUnit) {
        this.displayUnit = displayUnit;
    }

    public int getHeaderColumnIndex() {
        return headerColumnIndex;
    }

    public void setHeaderColumnIndex(int headerColumnIndex) {
        this.headerColumnIndex = headerColumnIndex;
    }
    
    
    
    
}
