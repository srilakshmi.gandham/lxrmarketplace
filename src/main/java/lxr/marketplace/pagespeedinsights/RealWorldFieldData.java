/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.pagespeedinsights;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 *
 * @author vidyasagar.korada
 */
public class RealWorldFieldData implements Serializable{
   /*
    Possible values : Slow/ Average/ Fast / No data
    If key: loadingExperience not found the result will be NoData
    */
    private String fieldDataCategory;
    
    private int fieldDataFCPPercentile;
    private String fieldDataFCPColor;
    
    private int fieldDataFIDPercentile;
    private String fieldDataFIDColor;
    
    private LinkedHashMap<String,Integer> fieldDataFCPdistributions;
    private LinkedHashMap<String,Integer> fieldDataFIDdistributions;
    
    /*
    Possible values : Slow/ Average/ Fast / No data
    If key: originLoadingExperience not found the result will be NoData
    */
    private String originSummaryCategory;
    
    private int originSummaryFCPPercentile;
    private String originSummaryFCPColor;
    
    private int originSummaryFIDPercentile;
    private String originSummaryFIDColor;
    
    private LinkedHashMap<String,Integer> originSummaryFCPdistributions;
    private LinkedHashMap<String,Integer> originSummaryFIDdistributions;

    public String getFieldDataCategory() {
        return fieldDataCategory;
    }

    public void setFieldDataCategory(String fieldDataCategory) {
        this.fieldDataCategory = fieldDataCategory;
    }

    public int getFieldDataFCPPercentile() {
        return fieldDataFCPPercentile;
    }

    public void setFieldDataFCPPercentile(int fieldDataFCPPercentile) {
        this.fieldDataFCPPercentile = fieldDataFCPPercentile;
    }

    public String getFieldDataFCPColor() {
        return fieldDataFCPColor;
    }

    public void setFieldDataFCPColor(String fieldDataFCPColor) {
        this.fieldDataFCPColor = fieldDataFCPColor;
    }

    public int getFieldDataFIDPercentile() {
        return fieldDataFIDPercentile;
    }

    public void setFieldDataFIDPercentile(int fieldDataFIDPercentile) {
        this.fieldDataFIDPercentile = fieldDataFIDPercentile;
    }

    public String getFieldDataFIDColor() {
        return fieldDataFIDColor;
    }

    public void setFieldDataFIDColor(String fieldDataFIDColor) {
        this.fieldDataFIDColor = fieldDataFIDColor;
    }

    public LinkedHashMap<String, Integer> getFieldDataFCPdistributions() {
        return fieldDataFCPdistributions;
    }

    public void setFieldDataFCPdistributions(LinkedHashMap<String, Integer> fieldDataFCPdistributions) {
        this.fieldDataFCPdistributions = fieldDataFCPdistributions;
    }

    public LinkedHashMap<String, Integer> getFieldDataFIDdistributions() {
        return fieldDataFIDdistributions;
    }

    public void setFieldDataFIDdistributions(LinkedHashMap<String, Integer> fieldDataFIDdistributions) {
        this.fieldDataFIDdistributions = fieldDataFIDdistributions;
    }

    public String getOriginSummaryCategory() {
        return originSummaryCategory;
    }

    public void setOriginSummaryCategory(String originSummaryCategory) {
        this.originSummaryCategory = originSummaryCategory;
    }

    public int getOriginSummaryFCPPercentile() {
        return originSummaryFCPPercentile;
    }

    public void setOriginSummaryFCPPercentile(int originSummaryFCPPercentile) {
        this.originSummaryFCPPercentile = originSummaryFCPPercentile;
    }

    public String getOriginSummaryFCPColor() {
        return originSummaryFCPColor;
    }

    public void setOriginSummaryFCPColor(String originSummaryFCPColor) {
        this.originSummaryFCPColor = originSummaryFCPColor;
    }

    public int getOriginSummaryFIDPercentile() {
        return originSummaryFIDPercentile;
    }

    public void setOriginSummaryFIDPercentile(int originSummaryFIDPercentile) {
        this.originSummaryFIDPercentile = originSummaryFIDPercentile;
    }

    public String getOriginSummaryFIDColor() {
        return originSummaryFIDColor;
    }

    public void setOriginSummaryFIDColor(String originSummaryFIDColor) {
        this.originSummaryFIDColor = originSummaryFIDColor;
    }

    public LinkedHashMap<String, Integer> getOriginSummaryFCPdistributions() {
        return originSummaryFCPdistributions;
    }

    public void setOriginSummaryFCPdistributions(LinkedHashMap<String, Integer> originSummaryFCPdistributions) {
        this.originSummaryFCPdistributions = originSummaryFCPdistributions;
    }

    public LinkedHashMap<String, Integer> getOriginSummaryFIDdistributions() {
        return originSummaryFIDdistributions;
    }

    public void setOriginSummaryFIDdistributions(LinkedHashMap<String, Integer> originSummaryFIDdistributions) {
        this.originSummaryFIDdistributions = originSummaryFIDdistributions;
    }

    
    
}
