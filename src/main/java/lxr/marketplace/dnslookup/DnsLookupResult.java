/**
 * 
 */
package lxr.marketplace.dnslookup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.xbill.DNS.Record;

/**
 * @author netelixir
 * 
 */
public class DnsLookupResult implements Serializable{

	/** Array of A records */
	private Record[] aRecords;

	/** Array of C Name records */
	private Record[] cNameRecords;

	/** Array of MX records */
	private Record[] mxRecords;

	/** Array of NS records */
	private Record[] nsRecords;

	/** Array of SOA records */
	private Record[] soaRecords;

	/** Array of AAAA records*/
	private Record[] aaaaRecords;
	
	/** Array of TEXT records*/
	private Record[] txtRecords;
	
	/** Array of D Name records*/
	private Record[] dRecords;
	
	/** List of individual text records*/
	private List<String> text = new ArrayList<String>();

	/** Get A Records*/
	public Record[] getaRecords() {
		return aRecords;
	}

	/** Set A Records*/
	public void setaRecords(Record[] aRecords) {
		this.aRecords = aRecords;
	}

	/** Get C Name Records*/
	public Record[] getcNameRecords() {
		return cNameRecords;
	}

	/** Set C Name Records*/
	public void setcNameRecords(Record[] cNameRecords) {
		this.cNameRecords = cNameRecords;
	}

	/** Get MX Records*/
	public Record[] getMxRecords() {
		return mxRecords;
	}

	/** Set MX Records*/
	public void setMxRecords(Record[] mxRecords) {
		this.mxRecords = mxRecords;
	}

	/** Get NS Records*/
	public Record[] getNsRecords() {
		return nsRecords;
	}

	/** Set NS Records*/
	public void setNsRecords(Record[] nsRecords) {
		this.nsRecords = nsRecords;
	}

	/** Get SOA Records*/
	public Record[] getSoaRecords() {
		return soaRecords;
	}

	/** Set SOA Records*/
	public void setSoaRecords(Record[] soaRecords) {
		this.soaRecords = soaRecords;
	}

	/** Get AAAA Records*/
	public Record[] getAaaaRecords() {
		return aaaaRecords;
	}

	/** Set AAAA Records*/
	public void setAaaaRecords(Record[] aaaaRecords) {
		this.aaaaRecords = aaaaRecords;
	}

	/** Get Text Records*/
	public Record[] getTxtRecords() {
		return txtRecords;
	}

	/** Set Text Records*/
	public void setTxtRecords(Record[] txtRecords) {
		this.txtRecords = txtRecords;
	}

	/** Get List of Text Records*/
	public List<String> getText() {
		return text;
	}

	/** Set List of Text Records*/
	public void setText(List<String> text) {
		this.text = text;
	}

	/** Get D Name Records*/
	public Record[] getdRecords() {
		return dRecords;
	}

	/** Set D Name Records*/
	public void setdRecords(Record[] dRecords) {
		this.dRecords = dRecords;
	}

    @Override
    public String toString() {
        return "DnsLookupResult{" + "aRecords=" + aRecords.toString() + ", cNameRecords=" + cNameRecords + ", mxRecords=" + mxRecords + ", nsRecords=" + nsRecords + ", soaRecords=" + soaRecords + ", aaaaRecords=" + aaaaRecords + ", txtRecords=" + txtRecords + ", dRecords=" + dRecords + ", text=" + text + '}';
    }
        
        
        
}
