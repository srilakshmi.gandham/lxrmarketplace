package lxr.marketplace.dnslookup;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

//* Defines that this class is a spring bean */
@Controller
@RequestMapping("/dns-lookup-tool.html")
public class DnsLookupToolController {

    private static Logger logger = Logger.getLogger(DnsLookupToolController.class);

    /**
     * downloadFolder variable for Download Report *
     */
    private String downloadFolder;

    /**
     * Service class Reference *
     */
    @Autowired
    private DnsLookupService dnsLookupService;

    @Autowired
    private MessageSource messageSource;

    /**
     * Tells the application context to inject an instance of dnsLookupService
     * here
     *
     * @param dnsLookupService
     * @param downloadFolder
     */
    @Autowired
    public DnsLookupToolController(DnsLookupService dnsLookupService,
            String downloadFolder) {
        this.dnsLookupService = dnsLookupService;
        this.downloadFolder = downloadFolder;
    }

    /**
     * This method will response only get Request from User
     *
     * @param request
     * @param response
     * @param dnsLookupTool
     * @param model
     * @param session
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("dnsLookupTool") DnsLookupTool dnsLookupTool, ModelMap model, HttpSession session) {

        model.addAttribute("dnsLookupTool", dnsLookupTool);
        DnsLookupResult dnsResult = null;
        String ntwrk = null;
        session.removeAttribute("notWorkingUrl");
        if ((WebUtils.hasSubmitParameter(request, "getResult") && WebUtils.hasSubmitParameter(request, "loginRefresh"))
                || WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            session.setAttribute("loginrefresh", "loginrefresh");
            session.getAttribute("dnsResult");
            dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
            dnsLookupTool.setUrl((String) session.getAttribute("domainUrl"));
            model.addAttribute(dnsLookupTool);
            model.addAttribute("dnsResult", dnsResult);
            ntwrk = (String) session.getAttribute("notWorkingUrlDNS");
            if (ntwrk == null) {
                model.addAttribute("status", "success");
            }
        }

        return "views/dnsLookup/dnsLookup";
    }

    /**
     * In this method by default return Tool Page and based on the condition
     * return Success page also for Post Request
     *
     * @param request
     * @param response
     * @param model
     * @param dnsLookupTool
     * @param result
     * @param session
     * @param query
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, HttpServletResponse response, ModelMap model,
            @ModelAttribute("dnsLookupTool") DnsLookupTool dnsLookupTool, BindingResult result, HttpSession session,
            @RequestParam(defaultValue = "") String query) {

        String dnsURL = null;
        Session userSession = null;
        session = request.getSession();
        Login user = null;
        if (session.getAttribute("user") != null) {
            user = (Login) session.getAttribute("user");
        }
        DnsLookupService dnsService = null;
        DnsLookupResult dnsResult = null;
        logger.debug("inside of onSubmit" + user);
        if (dnsLookupTool != null) {
            if (dnsLookupTool.getUrl() != null && !dnsLookupTool.getUrl().trim().isEmpty()) {
                dnsLookupTool.setUrl(dnsLookupTool.getUrl().trim());
            }
            session.setAttribute("dnsLookupTool", dnsLookupTool);
        }

        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(28);
            toolObj.setToolName("DNS Lookup Tool");
            toolObj.setToolLink("dns-lookup-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.DNS_LOOKUP);
            if (WebUtils.hasSubmitParameter(request, "getResult") && dnsLookupTool != null) {
                dnsURL = Common.getUrlValidation(dnsLookupTool.getUrl().trim(), session);
                session.setAttribute("url", dnsURL.trim());
                logger.debug("Get Result..");

                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    logger.debug("loginrefresh..........");
                    session.setAttribute("loginrefresh", "loginrefresh");
                    session.getAttribute("dnsResult");
                    dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
                    model.addAttribute("dnsResult", dnsResult);
                    model.addAttribute("status", "success");
                    return "views/dnsLookup/dnsLookup";
                }
                session.removeAttribute("notWorkingUrl");
                if (dnsURL.equals("invalid") || dnsURL.equals("redirected")) {
//                    String notWorkingUrl = new String("Your URL is not working, please check the URL.");
                    String notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                    model.addAttribute("notWorkingUrl", notWorkingUrl);
                    session.setAttribute("notWorkingUrlDNS", notWorkingUrl);
                    session.setAttribute("domainUrl", dnsLookupTool.getUrl());
                    return "views/dnsLookup/dnsLookup";
                } else {
                    dnsService = new DnsLookupService();
                    if (session.getAttribute("userSession") != null) {
                        userSession = (Session) session.getAttribute("userSession");
                    }
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }
                    if (userSession != null) {
                        userSession.addToolUsage(toolObj.getToolId());
                        userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), dnsLookupTool.getUrl(), "", "", "", "/" + toolObj.getToolLink());
                    }
                    /*User website tracking when session completes*/
                    dnsResult = dnsService.getResult(dnsURL);
                    dnsLookupTool.setUrl(dnsURL);
                    session.setAttribute("dnsResult", dnsResult);
                    model.addAttribute("dnsResult", dnsResult);
                    model.addAttribute("status", "success");
                    session.setAttribute("domainUrl", dnsLookupTool.getUrl());
                    response.setCharacterEncoding("UTF-8");
                    return "views/dnsLookup/dnsLookup";

                }
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                try {
                    dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
                    model.addAttribute(dnsResult);
                    String downParam = request.getParameter("download");
                    String dwnfileName = dnsLookupService.createPdfFile(session, downloadFolder, user);
                    if (dwnfileName != null && !dwnfileName.trim().equals("")) {
                        if (downParam.equalsIgnoreCase("download")) {
                            Common.downloadReport(response, downloadFolder, dwnfileName, "pdf");
                        } else if (downParam.equalsIgnoreCase("sendmail")) {
                            logger.debug("Sending report through mail");
                            dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
                            model.addAttribute("dnsResult", dnsResult);
                            String toAddrs = request.getParameter("email");
                            Common.sendReportThroughMail(request, downloadFolder, dwnfileName, toAddrs, "DNS Lookup");
                            logger.debug("mail sent");
                            return null;
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception while downloading DNSLookup report", e);
                }
            }
        }
        return "views/dnsLookup/dnsLookup";
    }
}
