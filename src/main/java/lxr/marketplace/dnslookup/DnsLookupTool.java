/**
 * 
 */
package lxr.marketplace.dnslookup;

/**
 * @author netelixir
 * 
 */
public class DnsLookupTool {

	/** Title */
	private String title;
	
	/** Description */
	private String description;
	
	/** Keyword */
	private String keyword;
	
	/** Result */
	private String result;
	
	/** Url */
	private String url;
	
	/** Author */
	private String author;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeyword() { 
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getResult() { 
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
