/**
 *
 */
package lxr.marketplace.dnslookup;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpSession;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;

import org.springframework.context.ApplicationContext;
import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.DNAMERecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.NSRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.SOARecord;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import org.apache.log4j.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * @author netelixir
 *
 */
@Service
public class DnsLookupService {

    private static Logger logger = Logger.getLogger(DnsLookupService.class);

    ApplicationContext context = ApplicationContextProvider.getApplicationContext();
    URL dnsURL = null;

    /**
     * GetResults For DNS Lookup
     *
     * @param url
     * @return
     */
    public DnsLookupResult getResult(String url) {

        DnsLookupResult dnsResult = new DnsLookupResult();
        fetchAResults(dnsResult, url);
        fetchCnameResults(dnsResult, url);
        fetchMxRecords(dnsResult, url);
        fetchNsRecords(dnsResult, url);
        fetchSoaRecords(dnsResult, url);
        fetchA4Results(dnsResult, url);
        fetchTextResults(dnsResult, url);
        fetchDResults(dnsResult, url);
        return dnsResult;
    }

    /**
     * Fetch A Record Result
     *
     * @param dnsResult
     * @param urlA
     * @return
     */
    public Record[] fetchAResults(DnsLookupResult dnsResult, String urlA) {

        Record[] aRecords = null;
        try {
            dnsURL = new URL(urlA);
            urlA = dnsURL.getHost();
            aRecords = new Lookup(urlA, Type.A).run();
            if (aRecords != null) {
                dnsResult.setaRecords(aRecords);

            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfARecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfARecords", e);

        }
        return aRecords;
    }

    /**
     * Fetch C Name Record Result
     *
     * @param dnsResult
     * @param urlC
     * @return
     */
    public Record[] fetchCnameResults(DnsLookupResult dnsResult, String urlC) {

        Record[] cNameRecords = null;
        try {

            dnsURL = new URL(urlC);
            urlC = dnsURL.getHost();
            cNameRecords = new Lookup(urlC, Type.CNAME).run();
            if (cNameRecords != null) {
                dnsResult.setcNameRecords(cNameRecords);
            }

        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfCNameRecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfCNameRecords", e);

        }
        return cNameRecords;
    }

    /**
     * Fetch MX Record Result
     *
     * @param dnsResult
     * @param urlMx
     * @return
     */
    public Record[] fetchMxRecords(DnsLookupResult dnsResult, String urlMx) {

        Record[] mxRecords = null;
        try {
            dnsURL = new URL(urlMx);
            urlMx = dnsURL.getHost();
            mxRecords = new Lookup(urlMx, Type.MX).run();
            if (mxRecords != null) {
                dnsResult.setMxRecords(mxRecords);
            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfMxRecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfMxRecords", e);

        }
        return mxRecords;
    }

    /**
     * Fetch NS Record Result
     *
     * @param dnsResult
     * @param urlNs
     * @return
     */
    public Record[] fetchNsRecords(DnsLookupResult dnsResult, String urlNs) {

        Record[] nsRecords = null;
        try {
            dnsURL = new URL(urlNs);
            urlNs = dnsURL.getHost();
            nsRecords = new Lookup(urlNs, Type.NS).run();
            if (nsRecords != null) {
                dnsResult.setNsRecords(nsRecords);
            }

        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfNSRecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfNSRecords", e);

        }
        return nsRecords;
    }

    /**
     * Fetch SOA Record Result
     *
     * @param dnsResult
     * @param urlSoa
     * @return
     */
    public Record[] fetchSoaRecords(DnsLookupResult dnsResult, String urlSoa) {

        Record[] soaRecords = null;
        try {
            dnsURL = new URL(urlSoa);
            urlSoa = dnsURL.getHost();
            soaRecords = new Lookup(urlSoa, Type.SOA).run();
            if (soaRecords != null) {
                dnsResult.setSoaRecords(soaRecords);
            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfSOARecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfSOARecords", e);

        }
        return soaRecords;
    }

    /**
     * Fetch AAAA Record Result
     *
     * @param dnsResult
     * @param urlAAAA
     * @return
     */
    public Record[] fetchA4Results(DnsLookupResult dnsResult, String urlAAAA) {

        Record[] a4Records = null;
        try {
            dnsURL = new URL(urlAAAA);
            urlAAAA = dnsURL.getHost();
            a4Records = new Lookup(urlAAAA, Type.AAAA).run();
            if (a4Records != null) {
                dnsResult.setAaaaRecords(a4Records);
            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfAAAARecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfAAAARecords", e);

        }
        return a4Records;
    }

    /**
     * Fetch Text Record Result
     *
     * @param dnsResult
     * @param urlTxt
     * @return
     */
    @SuppressWarnings("unchecked")
    public Record[] fetchTextResults(DnsLookupResult dnsResult, String urlTxt) {

        Record[] txtRecords = null;
        try {
            dnsURL = new URL(urlTxt);
            urlTxt = dnsURL.getHost();

            txtRecords = new Lookup(urlTxt, Type.TXT).run();
            if (txtRecords != null) {
                dnsResult.setTxtRecords(txtRecords);
                int ith = 0;
                TXTRecord tRecord = (TXTRecord) txtRecords[ith];
                logger.debug("Text:  " + tRecord.getStrings());
                dnsResult.setText(tRecord.getStrings());
            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfTxtRecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfTxtRecords", e);

        }
        return txtRecords;
    }

    /**
     * Fetch D Name Record Result
     *
     * @param dnsResult
     * @param urlD
     * @return
     */
    public Record[] fetchDResults(DnsLookupResult dnsResult, String urlD) {

        Record[] dRecords = null;
        try {
            dnsURL = new URL(urlD);
            urlD = dnsURL.getHost();

            dRecords = new Lookup(urlD, Type.DNAME).run();
            if (dRecords != null) {
                dnsResult.setTxtRecords(dRecords);
            }
        } catch (TextParseException e) {
            // TODO Auto-generated catch block
            logger.error("TextParseExceptionOfDNameRecords", e);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("MalformedURLExceptionOfDNameRecords", e);

        }
        return dRecords;
    }

    /**
     * Creating PDF for DNS Lookup Tool
     *
     */
    public String createPdfFile(HttpSession session, String targetFilePath, Login user) {
        DnsLookupResult dnsResult = (DnsLookupResult) session.getAttribute("dnsResult");
        Record[] aRecords = dnsResult.getaRecords();
        Record[] cNameRecords = dnsResult.getcNameRecords();
        Record[] dRecords = dnsResult.getdRecords();
        Record[] mxRecords = dnsResult.getMxRecords();
        Record[] nsRecords = dnsResult.getNsRecords();
        Record[] txtRecords = dnsResult.getTxtRecords();
        Record[] soaRecords = dnsResult.getSoaRecords();
        Record[] aaaaRecords = dnsResult.getAaaaRecords();

        String url = (String) session.getAttribute("url");

        try {
//            DnsLookupService dnsLookupService = new DnsLookupService();
////            context = dnsLookupService.context;

            Font reportNameFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
                    Font.BOLD, new BaseColor(255, 110, 0)));
            Font orangeFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(255, 110, 0)));
            Font blackFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0)));
            Font smallFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 10.5f, Font.NORMAL, new BaseColor(0, 0, 0)));
            Font smallFontmsg = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 8, Font.NORMAL, new BaseColor(0, 0, 0)));
            Font normalFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 10.8f, Font.NORMAL));
            Font normalHeadingFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 10, Font.BOLD));
            Font normalOrangeFont = new Font(FontFactory.getFont(
                    FontFactory.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(255, 110, 0)));

            String repName = "LXRMarketplace_DNS_Lookup_Report" + user.getId();
            String reptName = Common.removeSpaces(repName);
            String filename = Common.createFileName(reptName) + ".pdf";
            String filePath = targetFilePath + filename;

            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            logger.info(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();

            Chunk tempReportName = new Chunk("DNS Lookup Tool Report", reportNameFont);
            Chunk normalHeading = new Chunk("Review of", normalFont);
            Chunk domain = new Chunk(" " + "\"" + (String) session.getAttribute("url") + "\" ", blackFont);

            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};

            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);

            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);

            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            domHeading.add(normalHeading);
            domHeading.add(domain);

            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis());
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm");
            String curDate = dtf.format(startDat);

            PdfPCell dateName = new PdfPCell(new Phrase("Date : " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();

            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(10f);
            rpName.setPaddingBottom(10f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            imgTable.setSpacingBefore(10f);
            imgTable.setSpacingAfter(35f);
            String headersDef = "With this tool you can quickly get all the DNS records returned from a DNS server for a specified domain. "
                    + "Free DNS Lookup tool makes it easy to view all kinds of Domain Name System (DNS) records. "
                    + "Now you can quickly and easily analyze your website’s DNS records with our free tool and set up DNS records properly. ";
            addTitlePage(pdfDoc, "DNS Lookup : ", headersDef, orangeFont, normalFont);

            float[] widths = {25f, 25f, 25f, 25f, 25f};
            float[] a4widths = {75f, 25f};
            float[] hwidths = {125f};
            PdfPTable aTable = null;
            PdfPTable ahTable = null;

            PdfPTable cTable = null;
            PdfPTable chTable = null;

            PdfPTable dTable = null;
            PdfPTable dhTable = null;

            PdfPTable mxTable = null;
            PdfPTable mxhTable = null;

            PdfPTable nsTable = null;
            PdfPTable nshTable = null;

            PdfPTable txtTable = null;
            PdfPTable txthTable = null;

            PdfPTable saoTable = null;
            PdfPTable saohTable = null;

            PdfPTable a4Table = null;
            PdfPTable a4hTable = null;

            dnsURL = new URL(url);
            url = dnsURL.getHost();

            Phrase rHeading = new Phrase();
            Chunk recordHeading = new Chunk("A Records :", normalOrangeFont);
            rHeading.add(recordHeading);
//			pdfDoc.add(recordHeading);

            if (aRecords != null) {
                if (aRecords.length > 0) {

                    ahTable = new PdfPTable(hwidths);
                    ahTable.setWidthPercentage(100f);

                    aTable = new PdfPTable(widths);
                    aTable.setWidthPercentage(100f);
//				aRecords = new Lookup(url, Type.A).run();

                    PdfPCell cell1 = new PdfPCell(new Paragraph("Name ", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("Address", normalHeadingFont));
                    PdfPCell cell3 = new PdfPCell(new Paragraph("Type ", normalHeadingFont));
                    PdfPCell cell4 = new PdfPCell(new Paragraph("Class ", normalHeadingFont));
                    PdfPCell cell5 = new PdfPCell(new Paragraph("TTL ", normalHeadingFont));

                    ahTable.addCell(rHeading);
                    ahTable.completeRow();
                    aTable.addCell(cell1);
                    aTable.addCell(cell2);
                    aTable.addCell(cell3);
                    aTable.addCell(cell4);
                    aTable.addCell(cell5);
                    aTable.completeRow();

                    for (int j = 0; j < aRecords.length; j++) {
                        ARecord aRecord = (ARecord) aRecords[j];

                        PdfPCell cell6 = new PdfPCell(new Paragraph(aRecord.getName().toString().replaceAll("\\.(?!.*\\.)", " "), smallFont));
                        PdfPCell cell7 = new PdfPCell(new Paragraph(aRecord.getAddress().getHostAddress(), smallFont));
                        PdfPCell cell8 = new PdfPCell(new Paragraph(aRecord.getType() + "", smallFont));
                        PdfPCell cell9 = new PdfPCell(new Paragraph(aRecord.getClass().getModifiers() + "", smallFont));
                        PdfPCell cell10 = new PdfPCell(new Paragraph(aRecord.getTTL() + "s", smallFont));

                        aTable.addCell(cell6);
                        aTable.addCell(cell7);
                        aTable.addCell(cell8);
                        aTable.addCell(cell9);
                        aTable.addCell(cell10);
                        aTable.completeRow();
                        aTable.setSpacingAfter(20f);
                    }
                    pdfDoc.add(ahTable);
                    pdfDoc.add(aTable);
                }
            } else {

                float[] aWidths = {100f};
                aTable = new PdfPTable(aWidths);
                aTable.setWidthPercentage(100f);

                ahTable = new PdfPTable(hwidths);
                ahTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for A Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                ahTable.addCell(rHeading);
                ahTable.completeRow();

                aTable.addCell(cell1);
                aTable.setSpacingAfter(20f);

                pdfDoc.add(ahTable);
                pdfDoc.add(aTable);

            }

            Phrase crHeading = new Phrase();
            Chunk crecordHeading = new Chunk("C Name Records :", normalOrangeFont);
            crHeading.add(crecordHeading);

            if (cNameRecords != null) {
                if (cNameRecords.length > 0) {
                    float[] cWidths = {30f, 50f};
                    cTable = new PdfPTable(cWidths);
                    cTable.setWidthPercentage(100f);

                    chTable = new PdfPTable(hwidths);
                    chTable.setWidthPercentage(100f);

//				cNameRecords = new Lookup(url, Type.CNAME).run();
                    chTable.addCell(crHeading);
                    chTable.completeRow();

                    for (int j = 0; j < cNameRecords.length; j++) {
                        CNAMERecord cRecord = (CNAMERecord) cNameRecords[j];

                        PdfPCell cell1 = new PdfPCell(new Paragraph("CName", normalHeadingFont));
                        PdfPCell cell6 = new PdfPCell(new Paragraph(cRecord.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));

                        PdfPCell cell2 = new PdfPCell(new Paragraph("Name  ", normalHeadingFont));
                        PdfPCell cell7 = new PdfPCell(new Paragraph(cRecord.getName().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));

                        PdfPCell cell3 = new PdfPCell(new Paragraph("Type", normalHeadingFont));
                        PdfPCell cell8 = new PdfPCell(new Paragraph(cRecord.getType() + " ", smallFont));

                        PdfPCell cell4 = new PdfPCell(new Paragraph("Class  ", normalHeadingFont));
                        PdfPCell cell9 = new PdfPCell(new Paragraph(cRecord.getDClass() + " ", smallFont));

                        PdfPCell cell5 = new PdfPCell(new Paragraph("TTL  ", normalHeadingFont));
                        PdfPCell cell10 = new PdfPCell(new Paragraph(cRecord.getTTL() + "s", smallFont));

                        cTable.addCell(cell1);
                        cTable.addCell(cell6);
                        cTable.completeRow();

                        cTable.addCell(cell2);
                        cTable.addCell(cell7);
                        cTable.completeRow();

                        cTable.addCell(cell3);
                        cTable.addCell(cell8);
                        cTable.completeRow();

                        cTable.addCell(cell4);
                        cTable.addCell(cell9);
                        cTable.completeRow();

                        cTable.addCell(cell5);
                        cTable.addCell(cell10);
                        cTable.completeRow();
                        cTable.setSpacingAfter(20f);
                    }

                    pdfDoc.add(chTable);
                    pdfDoc.add(cTable);
                }
            } else {
                float[] cWidths = {100f};
                cTable = new PdfPTable(cWidths);
                cTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for CNAME Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));

                chTable = new PdfPTable(hwidths);
                chTable.setWidthPercentage(100f);

//				cNameRecords = new Lookup(url, Type.CNAME).run();
                chTable.addCell(crHeading);
                chTable.completeRow();

                cTable.addCell(cell1);
                cTable.setSpacingAfter(20f);

                pdfDoc.add(chTable);
                pdfDoc.add(cTable);

            }

            Phrase drHeading = new Phrase();
            Chunk dHeading = new Chunk("D Name Records :", normalOrangeFont);
            drHeading.add(dHeading);

            if (dRecords != null) {
                if (dRecords.length > 0) {
                    dTable = new PdfPTable(widths);
                    dTable.setWidthPercentage(100f);

                    dhTable = new PdfPTable(hwidths);
                    dhTable.setWidthPercentage(100f);

                    PdfPCell cell1 = new PdfPCell(new Paragraph("DName", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("TTL   ", normalHeadingFont));

                    dhTable.addCell(drHeading);
                    dhTable.completeRow();

                    dTable.addCell(cell1);
                    dTable.addCell(cell2);
                    dTable.completeRow();

//					dRecords = new Lookup(url, Type.DNAME).run();
                    for (int j = 0; j < dRecords.length; j++) {
                        DNAMERecord dRecord = (DNAMERecord) dRecords[j];

                        PdfPCell cell3 = new PdfPCell(new Paragraph(dRecord.getName() + " ", smallFont));
                        PdfPCell cell4 = new PdfPCell(new Paragraph(dRecord.getTTL() + "s", smallFont));

                        dTable.addCell(cell3);
                        dTable.addCell(cell4);
                        dTable.completeRow();
                        dTable.setSpacingAfter(20f);

                    }
                    pdfDoc.add(dhTable);
                    pdfDoc.add(dTable);

                }
            } else {
                float[] dWidths = {100f};
                dTable = new PdfPTable(dWidths);
                dTable.setWidthPercentage(100f);

                dhTable = new PdfPTable(hwidths);
                dhTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for DNAME Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                dhTable.addCell(drHeading);
                dhTable.completeRow();

                dTable.addCell(cell1);
                dTable.setSpacingAfter(20f);

                pdfDoc.add(dhTable);
                pdfDoc.add(dTable);

            }

            Phrase mxrHeading = new Phrase();
            Chunk mxHeading = new Chunk("MX Records :", normalOrangeFont);
            mxrHeading.add(mxHeading);

            if (mxRecords != null) {
                if (mxRecords.length > 0) {

                    float[] mxwidths = {30f, 25f, 25f, 25f, 25f, 25f};
                    mxTable = new PdfPTable(mxwidths);
                    mxTable.setWidthPercentage(100f);

                    mxhTable = new PdfPTable(hwidths);
                    mxhTable.setWidthPercentage(100f);

                    PdfPCell cell1 = new PdfPCell(new Paragraph("Host", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("Preference Exchange", normalHeadingFont));
                    PdfPCell cell3 = new PdfPCell(new Paragraph("Name   ", normalHeadingFont));
                    PdfPCell cell4 = new PdfPCell(new Paragraph("Type  ", normalHeadingFont));
                    PdfPCell cell5 = new PdfPCell(new Paragraph("Class   ", normalHeadingFont));
                    PdfPCell cell6 = new PdfPCell(new Paragraph("TTL    ", normalHeadingFont));

                    mxhTable.addCell(mxrHeading);
                    mxhTable.completeRow();

                    mxTable.addCell(cell1);
                    mxTable.addCell(cell2);
                    mxTable.addCell(cell3);
                    mxTable.addCell(cell4);
                    mxTable.addCell(cell5);
                    mxTable.addCell(cell6);
                    mxTable.completeRow();

//				mxRecords = new Lookup(url, Type.MX).run();
                    for (int j = 0; j < mxRecords.length; j++) {
                        MXRecord mxRecord = (MXRecord) mxRecords[j];

                        PdfPCell cell7 = new PdfPCell(new Paragraph(mxRecord.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));
                        PdfPCell cell8 = new PdfPCell(new Paragraph(mxRecord.getPriority() + " ", smallFont));
                        PdfPCell cell9 = new PdfPCell(new Paragraph(mxRecord.getName().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));
                        PdfPCell cell10 = new PdfPCell(new Paragraph(mxRecord.getType() + " ", smallFont));
                        PdfPCell cell11 = new PdfPCell(new Paragraph(mxRecord.getDClass() + " ", smallFont));
                        PdfPCell cell12 = new PdfPCell(new Paragraph(mxRecord.getTTL() + "s", smallFont));

                        mxTable.addCell(cell7);
                        mxTable.addCell(cell8);
                        mxTable.addCell(cell9);
                        mxTable.addCell(cell10);
                        mxTable.addCell(cell11);
                        mxTable.addCell(cell12);
                        mxTable.completeRow();
                        mxTable.setSpacingAfter(20f);
                    }
                    pdfDoc.add(mxhTable);
                    pdfDoc.add(mxTable);

                }

            } else {
                float[] mxWidths = {100f};
                mxTable = new PdfPTable(mxWidths);
                mxTable.setWidthPercentage(100f);

                mxhTable = new PdfPTable(hwidths);
                mxhTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for MX Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                mxhTable.addCell(mxrHeading);
                mxhTable.completeRow();
                mxTable.addCell(cell1);
                mxTable.setSpacingAfter(20f);

                pdfDoc.add(mxhTable);
                pdfDoc.add(mxTable);

            }

            Phrase nsrHeading = new Phrase();
            Chunk nsHeading = new Chunk("NS Records :", normalOrangeFont);
            nsrHeading.add(nsHeading);

            if (nsRecords != null) {
                if (nsRecords.length > 0) {

                    nsTable = new PdfPTable(widths);
                    nsTable.setWidthPercentage(100f);

                    nshTable = new PdfPTable(hwidths);
                    nshTable.setWidthPercentage(100f);

                    PdfPCell cell1 = new PdfPCell(new Paragraph("Nsd Name", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("Name    ", normalHeadingFont));
                    PdfPCell cell3 = new PdfPCell(new Paragraph("Type   ", normalHeadingFont));
                    PdfPCell cell4 = new PdfPCell(new Paragraph("Class    ", normalHeadingFont));
                    PdfPCell cell5 = new PdfPCell(new Paragraph("TTL      ", normalHeadingFont));

                    nshTable.addCell(nsrHeading);
                    nshTable.completeRow();

                    nsTable.addCell(cell1);
                    nsTable.addCell(cell2);
                    nsTable.addCell(cell3);
                    nsTable.addCell(cell4);
                    nsTable.addCell(cell5);

//				nsRecords = new Lookup(url, Type.NS).run();
                    for (int j = 0; j < nsRecords.length; j++) {
                        NSRecord nsRecord = (NSRecord) nsRecords[j];

                        PdfPCell cell6 = new PdfPCell(new Paragraph(nsRecord.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));
                        PdfPCell cell7 = new PdfPCell(new Paragraph(nsRecord.getName().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));
                        PdfPCell cell8 = new PdfPCell(new Paragraph(nsRecord.getType() + " ", smallFont));
                        PdfPCell cell9 = new PdfPCell(new Paragraph(nsRecord.getDClass() + " ", smallFont));
                        PdfPCell cell10 = new PdfPCell(new Paragraph(nsRecord.getTTL() + "s", smallFont));

                        nsTable.addCell(cell6);
                        nsTable.addCell(cell7);
                        nsTable.addCell(cell8);
                        nsTable.addCell(cell9);
                        nsTable.addCell(cell10);
                        nsTable.completeRow();
                        nsTable.setSpacingAfter(20f);
                    }
                    pdfDoc.add(nshTable);
                    pdfDoc.add(nsTable);

                }
            } else {
                float[] nsWidths = {100f};
                nsTable = new PdfPTable(nsWidths);
                nsTable.setWidthPercentage(100f);

                nshTable = new PdfPTable(hwidths);
                nshTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for NS Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                nshTable.addCell(nsrHeading);
                nshTable.completeRow();
                nsTable.addCell(cell1);
                nsTable.setSpacingAfter(20f);
                pdfDoc.add(nshTable);
                pdfDoc.add(nsTable);

            }

            Phrase txtrHeading = new Phrase();
            Chunk txtHeading = new Chunk("Text Records :", normalOrangeFont);
            txtrHeading.add(txtHeading);

            if (txtRecords != null) {
                if (txtRecords.length > 0) {

                    float[] txtwidths = {50, 50f};
                    txtTable = new PdfPTable(txtwidths);
                    txtTable.setWidthPercentage(100f);

                    txthTable = new PdfPTable(hwidths);
                    txthTable.setWidthPercentage(100f);

                    /*PdfPCell cell1 = new PdfPCell(new Paragraph("Nsd Name" ,normalHeadingFont));*/
                    PdfPCell cell1 = new PdfPCell(new Paragraph("Text ", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("TTL ", normalHeadingFont));
                    txtTable.addCell(cell1);
                    txtTable.addCell(cell2);
                    txtTable.completeRow();

//					txtRecords = new Lookup(url, Type.TXT).run();
                    for (int j = 0; j < txtRecords.length; j++) {
                        TXTRecord txtRecord = (TXTRecord) txtRecords[j];

                        PdfPCell cell3 = new PdfPCell(new Paragraph(dnsResult.getText().toString().replaceAll("\\.(?!.*\\.)", " "), smallFont));
                        PdfPCell cell4 = new PdfPCell(new Paragraph(txtRecord.getTTL() + "s ", smallFont));

                        txthTable.addCell(txtrHeading);
                        txthTable.completeRow();
                        txtTable.addCell(cell3);
                        txtTable.addCell(cell4);
                        txtTable.completeRow();
                        txtTable.setSpacingAfter(20f);
                    }
                    pdfDoc.add(txthTable);
                    pdfDoc.add(txtTable);

                }
            } else {
                float[] txtWidths = {100f};
                txtTable = new PdfPTable(txtWidths);
                txtTable.setWidthPercentage(100f);

                txthTable = new PdfPTable(hwidths);
                txthTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for TXT Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                txthTable.addCell(txtrHeading);
                txthTable.completeRow();
                txtTable.addCell(cell1);
                txtTable.setSpacingAfter(20f);
                pdfDoc.add(txthTable);
                pdfDoc.add(txtTable);

            }

            Phrase soarHeading = new Phrase();
            Chunk soaHeading = new Chunk("SOA Records :", normalOrangeFont);
            soarHeading.add(soaHeading);

            if (soaRecords != null) {
                if (soaRecords.length > 0) {
                    float[] soaWidths = {30f, 50f};
                    saoTable = new PdfPTable(soaWidths);
                    saoTable.setWidthPercentage(100f);

                    saohTable = new PdfPTable(hwidths);
                    saohTable.setWidthPercentage(100f);

                    saohTable.addCell(soarHeading);
                    saohTable.completeRow();

//				soaRecords = new Lookup(url, Type.SOA).run();
                    for (int j = 0; j < soaRecords.length; j++) {
                        SOARecord soaRecord = (SOARecord) soaRecords[j];

                        PdfPCell cell1 = new PdfPCell(new Paragraph("Host", normalHeadingFont));
                        PdfPCell cell12 = new PdfPCell(new Paragraph(soaRecord.getHost().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));

                        PdfPCell cell2 = new PdfPCell(new Paragraph("Admin", normalHeadingFont));
                        PdfPCell cell13 = new PdfPCell(new Paragraph(soaRecord.getAdmin().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));

                        PdfPCell cell3 = new PdfPCell(new Paragraph("Serial", normalHeadingFont));
                        PdfPCell cell14 = new PdfPCell(new Paragraph(soaRecord.getSerial() + " ", smallFont));

                        PdfPCell cell4 = new PdfPCell(new Paragraph("Refresh", normalHeadingFont));
                        PdfPCell cell15 = new PdfPCell(new Paragraph(soaRecord.getRefresh() + "s", smallFont));

                        PdfPCell cell5 = new PdfPCell(new Paragraph("Retry", normalHeadingFont));
                        PdfPCell cell16 = new PdfPCell(new Paragraph(soaRecord.getRetry() + "s", smallFont));

                        PdfPCell cell6 = new PdfPCell(new Paragraph("Expire", normalHeadingFont));
                        PdfPCell cell17 = new PdfPCell(new Paragraph(soaRecord.getExpire() + "s", smallFont));

                        PdfPCell cell7 = new PdfPCell(new Paragraph("Minimum", normalHeadingFont));
                        PdfPCell cell18 = new PdfPCell(new Paragraph(soaRecord.getMinimum() + "s", smallFont));

                        PdfPCell cell8 = new PdfPCell(new Paragraph("Name", normalHeadingFont));
                        PdfPCell cell19 = new PdfPCell(new Paragraph(soaRecord.getName().toString().replaceAll("\\.(?!.*\\.)", ""), smallFont));

                        PdfPCell cell9 = new PdfPCell(new Paragraph("Type    ", normalHeadingFont));
                        PdfPCell cell20 = new PdfPCell(new Paragraph(soaRecord.getType() + "", smallFont));

                        PdfPCell cell10 = new PdfPCell(new Paragraph("Class     ", normalHeadingFont));
                        PdfPCell cell21 = new PdfPCell(new Paragraph(soaRecord.getDClass() + " ", smallFont));

                        PdfPCell cell11 = new PdfPCell(new Paragraph("TTL        ", normalHeadingFont));
                        PdfPCell cell22 = new PdfPCell(new Paragraph(soaRecord.getTTL() + "s", smallFont));

                        saoTable.addCell(cell1);
                        saoTable.addCell(cell12);
                        saoTable.completeRow();

                        saoTable.addCell(cell2);
                        saoTable.addCell(cell13);
                        saoTable.completeRow();

                        saoTable.addCell(cell3);
                        saoTable.addCell(cell14);
                        saoTable.completeRow();

                        saoTable.addCell(cell4);
                        saoTable.addCell(cell15);
                        saoTable.completeRow();

                        saoTable.addCell(cell5);
                        saoTable.addCell(cell16);
                        saoTable.completeRow();

                        saoTable.addCell(cell6);
                        saoTable.addCell(cell17);
                        saoTable.completeRow();

                        saoTable.addCell(cell7);
                        saoTable.addCell(cell18);
                        saoTable.completeRow();

                        saoTable.addCell(cell8);
                        saoTable.addCell(cell19);
                        saoTable.completeRow();

                        saoTable.addCell(cell9);
                        saoTable.addCell(cell20);
                        saoTable.completeRow();

                        saoTable.addCell(cell10);
                        saoTable.addCell(cell21);
                        saoTable.completeRow();

                        saoTable.addCell(cell11);
                        saoTable.addCell(cell22);
                        saoTable.completeRow();

                        saoTable.setSpacingAfter(20f);
                    }
                    pdfDoc.add(saohTable);
                    pdfDoc.add(saoTable);
                }
            } else {
                float[] soaWidths = {100f};
                saoTable = new PdfPTable(soaWidths);
                saoTable.setWidthPercentage(100f);

                saohTable = new PdfPTable(hwidths);
                saohTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for SOA Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                saohTable.addCell(soarHeading);
                saohTable.completeRow();
                saoTable.addCell(cell1);
                saoTable.setSpacingAfter(20f);

                pdfDoc.add(saohTable);
                pdfDoc.add(saoTable);

            }

            Phrase a4rHeading = new Phrase();
            Chunk a4Heading = new Chunk("AAAA Records :", normalOrangeFont);
            a4rHeading.add(a4Heading);

            if (aaaaRecords != null) {
                if (aaaaRecords.length > 0) {
                    a4Table = new PdfPTable(a4widths);
                    a4Table.setWidthPercentage(100f);

                    a4hTable = new PdfPTable(hwidths);
                    a4hTable.setWidthPercentage(100f);

                    PdfPCell cell1 = new PdfPCell(new Paragraph("IP", normalHeadingFont));
                    PdfPCell cell2 = new PdfPCell(new Paragraph("TTL ", normalHeadingFont));

                    a4hTable.addCell(a4rHeading);
                    a4hTable.completeRow();

                    a4Table.addCell(cell1);
                    a4Table.addCell(cell2);
                    a4Table.completeRow();
                    int coloumCount = 0;
//					aaaaRecords = new Lookup(url, Type.AAAA).run();
                    for (int j = 0; j < aaaaRecords.length; j++) {
                        AAAARecord aaaaRecord = (AAAARecord) aaaaRecords[j];
                        PdfPCell cell3 = new PdfPCell(new Paragraph(aaaaRecord.getAddress().getHostAddress(), smallFont));
                        PdfPCell cell4 = new PdfPCell(new Paragraph(aaaaRecord.getTTL() + "s ", smallFont));
                        /*logger.info("aaaaRecord adrress  canonical "+aaaaRecord.getAddress().getCanonicalHostName());
                                                logger.info("aaaaRecord adrress  host adress"+aaaaRecord.getAddress().getHostAddress());
                                                logger.info("aaaaRecord adrress  getHostName "+aaaaRecord.getAddress().getHostName());
                                                logger.info("aaaaRecord getTTL()"+aaaaRecord.getTTL());
                                                logger.info("aaaaRecord getDClass()"+aaaaRecord.getDClass());
                                                logger.info("aaaaRecord getAdditionalName()"+aaaaRecord.getAdditionalName());
                                                logger.info("aaaaRecord getRRsetType()()"+aaaaRecord.getRRsetType());*/
                        a4Table.addCell(cell3);
                        a4Table.addCell(cell4);
                        a4Table.completeRow();
                        a4Table.setSpacingAfter(20f);
                        coloumCount++;
                    }
                    pdfDoc.add(a4hTable);
                    pdfDoc.add(a4Table);

                }
            } else {
                float[] a4Widths = {100f};
                a4Table = new PdfPTable(a4Widths);
                a4Table.setWidthPercentage(100f);

                a4hTable = new PdfPTable(hwidths);
                a4hTable.setWidthPercentage(100f);

                Chunk warning = new Chunk("Warning : ", smallFontmsg);
                Chunk errorMsg = new Chunk("No record found for AAAA Records", smallFontmsg);

                PdfPCell cell1 = new PdfPCell(new Paragraph(warning + "" + errorMsg, smallFont));
                a4hTable.addCell(a4rHeading);
                a4hTable.completeRow();
                a4Table.addCell(cell1);
                a4Table.setSpacingAfter(20f);
                pdfDoc.add(a4hTable);
                pdfDoc.add(a4Table);
                pdfDoc.add(Chunk.NEWLINE);

            }

            pdfDoc.close();
            return filename;

        } catch (DocumentException | IOException e) {
            logger.error("DocumentException | IOException in createPdfFile cuase: ", e);
        } catch (Exception e) {
            logger.error("Exception in createPdfFile cuase: ", e);
        }
        return null;
    }

    /**
     * Adding Title Page
     *
     * @param pdfDoc
     * @param heading
     * @param description
     * @param normalFont
     * @param orangeFont
     * @throws com.itextpdf.text.DocumentException
     */
    public void addTitlePage(com.itextpdf.text.Document pdfDoc, String heading,
            String description, Font orangeFont, Font normalFont)
            throws DocumentException {

        Paragraph paragraph = new Paragraph();
        paragraph.add(new Paragraph(heading, orangeFont));
        paragraph.setSpacingBefore(10f);
        paragraph.setSpacingAfter(30f);
        paragraph.add(new Paragraph(description, normalFont));
        paragraph.setSpacingBefore(13f);
        paragraph.setSpacingAfter(13f);
        pdfDoc.add(paragraph);
    }

    public JSONObject getDNSResultJSON(DnsLookupResult dnsLookupResult) {
        JSONObject completeResultJSON = new JSONObject();
        try {
            completeResultJSON.put("aRecords", getARecords(dnsLookupResult.getaRecords()));
            completeResultJSON.put("cNameRecords", getCNameRecords(dnsLookupResult.getcNameRecords()));
            completeResultJSON.put("dRecords", getDRecords(dnsLookupResult.getdRecords()));
            completeResultJSON.put("mxRecords", getMXRecords(dnsLookupResult.getMxRecords()));
            completeResultJSON.put("nsRecords", getNSRecords(dnsLookupResult.getNsRecords()));
            completeResultJSON.put("txtRecords", getTextRecords(dnsLookupResult.getTxtRecords(), dnsLookupResult));
            completeResultJSON.put("soaRecords", getSOARecords(dnsLookupResult.getSoaRecords()));
            completeResultJSON.put("aaaaRecords", getAAAARecords(dnsLookupResult.getAaaaRecords()));
        } catch (Exception e) {
            logger.error("Exception in getDNSResultJSON cause: ", e);
        }
        return completeResultJSON;
    }

    public JSONArray getARecords(Record[] aRecords) {
        JSONArray aRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (aRecords != null && aRecords.length > 0) {
                for (Record aRecord : aRecords) {
                    ARecord aRecordObj = (ARecord) aRecord;
                    if (aRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("Name", aRecordObj.getName().toString().replaceAll("\\.(?!.*\\.)", " "));
                        innerObject.put("Address", aRecordObj.getAddress().getHostAddress());
                        innerObject.put("Type", aRecordObj.getType());
                        innerObject.put("Class", aRecordObj.getClass().getModifiers());
                        innerObject.put("TTL", aRecordObj.getTTL() + "s");
                        aRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getARecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for A Records");
            aRecordsJSON.add(innerObject);
        }
        return aRecordsJSON;
    }

    public JSONArray getCNameRecords(Record[] cNameRecords) {
        JSONArray cRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (cNameRecords != null && cNameRecords.length > 0) {
                for (Record cNameRecord : cNameRecords) {
                    CNAMERecord cRecord = (CNAMERecord) cNameRecord;
                    if (cRecord != null) {
                        innerObject = new JSONObject();
                        innerObject.put("CName", cRecord.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Name", cRecord.getName().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Type", cRecord.getType());
                        innerObject.put("Class", cRecord.getDClass());
                        innerObject.put("TTL", cRecord.getTTL() + "s");
                        cRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getCNameRecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for CNAME Records");
            cRecordsJSON.add(innerObject);
        }
        return cRecordsJSON;
    }

    public JSONArray getDRecords(Record[] dRecords) {
        JSONArray dRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (dRecords != null && dRecords.length > 0) {
                for (Record dRecord : dRecords) {
                    DNAMERecord dRecordObj = (DNAMERecord) dRecord;
                    if (dRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("DName", dRecord.getName());
                        innerObject.put("TTL", dRecord.getTTL() + "s");
                        dRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getDRecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for DNAME Records");
            dRecordsJSON.add(innerObject);
        }
        return dRecordsJSON;
    }

    public JSONArray getMXRecords(Record[] mxRecords) {
        JSONArray mxRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (mxRecords != null && mxRecords.length > 0) {
                for (Record mxRecord : mxRecords) {
                    MXRecord mxRecordObj = (MXRecord) mxRecord;
                    if (mxRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("Host", mxRecordObj.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Preference Exchange", mxRecordObj.getPriority());
                        innerObject.put("Name", mxRecordObj.getName().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Type", mxRecordObj.getType());
                        innerObject.put("Class", mxRecordObj.getDClass());
                        innerObject.put("TTL", mxRecordObj.getTTL() + "s");
                        mxRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getMXRecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for MX Records");
            mxRecordsJSON.add(innerObject);
        }
        return mxRecordsJSON;
    }

    public JSONArray getNSRecords(Record[] nsRecords) {
        JSONArray nsRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (nsRecords != null && nsRecords.length > 0) {
                for (Record nsRecord : nsRecords) {
                    NSRecord nsRecordObj = (NSRecord) nsRecord;
                    if (nsRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("Nsd Name", nsRecordObj.getTarget().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Name", nsRecordObj.getName().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Type", nsRecordObj.getType());
                        innerObject.put("Class", nsRecordObj.getDClass());
                        innerObject.put("TTL", nsRecordObj.getTTL() + "s");
                        nsRecordsJSON.add(innerObject);
                    }
                }
            } 
        } catch (Exception e) {
            logger.error("Exception in getNSRecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for NS Records");
            nsRecordsJSON.add(innerObject);
        }
        return nsRecordsJSON;
    }

    public JSONArray getTextRecords(Record[] txtRecords, DnsLookupResult dnsResult) {
        JSONArray txtRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (txtRecords != null && txtRecords.length > 0) {
                for (Record txtRecord : txtRecords) {
                    TXTRecord txtRecordObj = (TXTRecord) txtRecord;
                    if (txtRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("Text", dnsResult.getText().toString().replaceAll("\\.(?!.*\\.)", " "));
                        innerObject.put("TTL", txtRecord.getTTL() + "s");
                        txtRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getTextRecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for TXT Records");
            txtRecordsJSON.add(innerObject);
        }
        return txtRecordsJSON;
    }

    public JSONArray getSOARecords(Record[] soaRecords) {
        JSONArray soaRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (soaRecords != null && soaRecords.length > 0) {
                for (Record soaRecord : soaRecords) {
                    SOARecord soaRecordObj = (SOARecord) soaRecord;
                    if (soaRecordObj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("Host", soaRecordObj.getHost().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Admin", soaRecordObj.getAdmin().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Serial", soaRecordObj.getSerial());
                        innerObject.put("Refresh", soaRecordObj.getRefresh() + "s");
                        innerObject.put("Retry", soaRecordObj.getRetry() + "s");
                        innerObject.put("Expire", soaRecordObj.getExpire() + "s");
                        innerObject.put("Minimum", soaRecordObj.getMinimum() + "s");
                        innerObject.put("Name", soaRecordObj.getName().toString().replaceAll("\\.(?!.*\\.)", ""));
                        innerObject.put("Type", soaRecordObj.getType());
                        innerObject.put("Class", soaRecordObj.getDClass());
                        innerObject.put("TTL", soaRecordObj.getTTL() + "s");
                        soaRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getSOARecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for SOA Records");
            soaRecordsJSON.add(innerObject);
        }
        return soaRecordsJSON;
    }

    public JSONArray getAAAARecords(Record[] aaaaRecords) {
        JSONArray aaaaRecordsJSON = new JSONArray();
        JSONObject innerObject = null;
        try {
            if (aaaaRecords != null && aaaaRecords.length > 0) {
                for (Record aaaaRecord : aaaaRecords) {
                    AAAARecord aaaaRecordOBj = (AAAARecord) aaaaRecord;
                    if (aaaaRecordOBj != null) {
                        innerObject = new JSONObject();
                        innerObject.put("IP", aaaaRecordOBj.getAddress().getHostAddress());
                        innerObject.put("TTL", aaaaRecordOBj.getTTL() + "s");
                        aaaaRecordsJSON.add(innerObject);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getAAAARecords cause: ", e);
        }
        if (innerObject == null) {
            innerObject = new JSONObject();
            innerObject.put("Warning", "No record found for AAAA Records");
            aaaaRecordsJSON.add(innerObject);
        }
        return aaaaRecordsJSON;
    }
}
