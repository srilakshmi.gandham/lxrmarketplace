/**
 * 
 */
package lxr.marketplace.dnslookup;

/**
 * @author netelixir
 * 
 */
public final class Functions {
	private Functions() {
		//
	}

	public static String replaceEndDot(String string, String pattern,
			String replacement) {
		return string.replaceAll(pattern, replacement);
	}

}
