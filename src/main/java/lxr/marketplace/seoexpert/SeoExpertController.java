/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.seoexpert;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author netelixir
 */
@Controller
@RequestMapping("/seo-expert.html")
public class SeoExpertController {

    @RequestMapping(method = RequestMethod.GET) 
    public String getSeoExpert()
    {
        return "view/seoexpert/seoExpert";
    }
}
