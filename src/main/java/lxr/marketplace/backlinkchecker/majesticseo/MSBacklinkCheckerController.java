package lxr.marketplace.backlinkchecker.majesticseo;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.apiaccess.MajesticSeoService;
import lxr.marketplace.apiaccess.lxrmbeans.BackLinkInfo;
import lxr.marketplace.apiaccess.lxrmbeans.DomainInfo;
import lxr.marketplace.user.Login;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class MSBacklinkCheckerController extends SimpleFormController {
	private static Logger logger = Logger.getLogger(MSBacklinkCheckerController.class);
	public MSBacklinkCheckerController() {
		setCommandClass(MSBacklinkCheckerTool.class);
		setCommandName("backlinkCheckerTool");
	}
	
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		MSBacklinkCheckerTool backlinkCheckerTool = (MSBacklinkCheckerTool)command;
		HttpSession session = request.getSession();
		Login user = (Login) session.getAttribute("user");
		
		if (user != null) {
			if (WebUtils.hasSubmitParameter(request, "getResult")) {
				if(session.getAttribute("blcBacklinks") != null){
					ArrayList<BackLinkInfo> backLinks = (ArrayList<BackLinkInfo>) session.getAttribute("blcBacklinks");
					ModelAndView mAndV = new ModelAndView(getSuccessView(), "backlinkCheckerTool", backlinkCheckerTool);
					mAndV.addObject("backLinks", backLinks);
					return mAndV;
				}
				MajesticSeoService majesticSeo = new MajesticSeoService();
				ArrayList<BackLinkInfo> backLinks = new ArrayList<BackLinkInfo>();
				ArrayList<DomainInfo> refDomains = new ArrayList<DomainInfo>();
				
				majesticSeo.getBackLinkData(backlinkCheckerTool.getDomainName(), backLinks, refDomains);
				session.setAttribute("blcBacklinks", backLinks);
				session.setAttribute("blcRefDomains", refDomains);
				
//				System.out.println("#TopBackLink :"+backLinks.size()+" #RefDomain: "+refDomains.size());
//				for (BackLinkInfo backLink : backLinks) {
//					System.out.print("Backlink URL: "+backLink.getSourceURL());
//					System.out.println(" | Target URL: "+backLink.getTargetURL());
//				}
//				for (DomainInfo refDomain : refDomains) {
//					System.out.print("Domain: " + refDomain.getDomain());
//					System.out.print(" | Alexa Rank: " + refDomain.getAlexaRank());
//					System.out.println(" | External Backlinks: " + refDomain.getExtBackLinkCount());		
//				}	
//				logger.info("backlinkCheckerTool " + backlinkCheckerTool.getDomainName());
				
				logger.info("Showing results for backlink checker... #TopBackLink :"
						+ backLinks.size() + " #RefDomain: " + refDomains.size());
				ModelAndView mAndV = new ModelAndView(getSuccessView(), "backlinkCheckerTool", backlinkCheckerTool);
				mAndV.addObject("backLinks", backLinks);
				return mAndV;
			}else if (WebUtils.hasSubmitParameter(request, "pagination") && 
					session.getAttribute("blcBacklinks") != null) {
				
				List<BackLinkInfo> backLinks = (ArrayList<BackLinkInfo>) session.getAttribute("blcBacklinks");
				
				int  begin = 0;
				int end = backLinks.size() < 10 ? backLinks.size() - 1 : 9;
				try{
					begin = Integer.parseInt(request.getParameter("begin"));
					end = Integer.parseInt(request.getParameter("end"));
				}catch(Exception e){
					logger.error("Parsing Error in pagination begin value");
				}
				logger.info("pagination -------------"+begin+"  "+end);
				JSONArray arr = null;
				arr = new JSONArray();
				PrintWriter writer = null;
				List<BackLinkInfo> data = backLinks.subList(begin, end + 1);
				try {
					writer = response.getWriter();
					arr.add(0, data);
					writer.print(arr);
				} catch (Exception e) {
					logger.error(e.getMessage());
					logger.error(e.getCause());
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					writer.print("{success: false}");
				} finally {
					writer.flush();
					writer.close();
				}
				return null;
			}else if (WebUtils.hasSubmitParameter(request, "sort") && 
					session.getAttribute("blcBacklinks") != null) {
				String param = request.getParameter("param");
				final String sort = request.getParameter("sort");
				List<BackLinkInfo> backLinks = (ArrayList<BackLinkInfo>) session.getAttribute("blcBacklinks");
				if(param.equals("sourceUrl")){
					Collections.sort(backLinks, new Comparator<BackLinkInfo>(){
	                     public int compare(BackLinkInfo f1, BackLinkInfo f2){
	                    	 if(sort.contains("sortUp")){
	                    		 return f1.getSourceURL().compareTo(f2.getSourceURL());
	                    	 }else{
	                    		 return f2.getSourceURL().compareTo(f1.getSourceURL());
	                    	 }
	                     }
	                });
				}else if(param.equals("anchorText")){
					Collections.sort(backLinks, new Comparator<BackLinkInfo>(){
	                     public int compare(BackLinkInfo f1, BackLinkInfo f2){
	                    	 if(sort.contains("sortUp")){
	                    		 return f1.getAnchorText().compareTo(f2.getAnchorText());
	                    	 }else{
	                    		 return f2.getAnchorText().compareTo(f1.getAnchorText());
	                         }
	                     }
	                });
				}else if(param.equals("sCitationFlow")){
					Collections.sort(backLinks, new Comparator<BackLinkInfo>(){
	                     public int compare(BackLinkInfo f1, BackLinkInfo f2){
	                    	 if(sort.contains("sortUp")){
	                    		 return Double.compare(f1.getSourceCitationFlow(), f2.getSourceCitationFlow());
	                    	 }else{
	                    		 return Double.compare(f2.getSourceCitationFlow(), f1.getSourceCitationFlow());
	                         }
	                     }
	                });
				}else if(param.equals("sTrustFlow")){
					Collections.sort(backLinks, new Comparator<BackLinkInfo>(){
	                     public int compare(BackLinkInfo f1, BackLinkInfo f2){
	                    	 if(sort.contains("sortUp")){
	                    		 return Double.compare(f1.getSourceTrustFlow(), f2.getSourceTrustFlow());
	                    	 }else{
	                    		 return Double.compare(f2.getSourceTrustFlow(), f1.getSourceTrustFlow());
	                         }
	                     }
	                });
				}
				int  begin = 0;
				int end = backLinks.size() < 10 ? backLinks.size() - 1 : 9;
				try{
					begin = Integer.parseInt(request.getParameter("begin"));
					end = Integer.parseInt(request.getParameter("end"));
				}catch(Exception e){
					logger.error("Parsing Error in pagination begin value");
				}
					logger.info("pagination -------------"+begin+"  "+end);
				JSONArray arr = null;
				arr = new JSONArray();
				PrintWriter writer = null;
				List<BackLinkInfo> data = backLinks.subList(begin, end + 1);
				try {
					writer = response.getWriter();
					arr.add(0, data);
					writer.print(arr);
				} catch (Exception e) {
					logger.error(e.getMessage());
					logger.error(e.getCause());
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					writer.print("{success: false}");
				} finally {
					writer.flush();
					writer.close();
				}
				return null;			
			}
		}		
		ModelAndView mAndV = new ModelAndView(getFormView(), "backlinkCheckerTool", backlinkCheckerTool);
		return mAndV;
	}
	
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}

	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		HttpSession session = request.getSession();
		MSBacklinkCheckerTool backlinkCheckerTool = (MSBacklinkCheckerTool) session
				.getAttribute("backlinkCheckerTool");
		if (backlinkCheckerTool == null) {
			backlinkCheckerTool = new MSBacklinkCheckerTool();
			session.setAttribute("backlinkCheckerTool", backlinkCheckerTool);
		}
		return backlinkCheckerTool;
	}
}
