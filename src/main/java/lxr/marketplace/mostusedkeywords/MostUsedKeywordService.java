package lxr.marketplace.mostusedkeywords;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import crawlercommons.robots.BaseRobotRules;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.toList;
import javax.servlet.http.HttpSession;
import lxr.marketplace.crawler.LxrmUrlFecthThread;
import lxr.marketplace.crawler.LxrmUrlStats;
import lxr.marketplace.crawler.LxrmUrlStatsService;
import lxr.marketplace.seoninja.data.LXRSEOURLFetchThread;
import lxr.marketplace.seoninja.data.NinjaURL;
import lxr.marketplace.seoninja.data.NinjaURLCrawlerUtil;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.itextpdf.text.*;
import static java.util.stream.Collectors.groupingBy;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * @author sagar
 */
@Service
public class MostUsedKeywordService {

    private static final Logger logger = Logger.getLogger(MostUsedKeywordService.class);
    private final int maxPoolSize = 100;
    private final long keepAliveTime = 600;
    private final LinkedBlockingQueue<Runnable> blockingQue = new LinkedBlockingQueue<>();

    @Autowired
    private LxrmUrlStatsService lxrmUrlStatsService;

    public List<MostUsedKeyWordsResult> crawlDomainToFetchMostUsedKeywords(NinjaWebStats webStats, List<String> stopWordsList, HttpSession session, int crawlingUrlLimit) {
        /*Limit of URl's to be analyzed for a domain.*/
//        int urlLimit = crawlingUrlLimit;
        int poolSize = 30;
        boolean keywordsUrlCrawlingStatus = false;
        List<MostUsedKeyWordsResult> mostUsedKeyWords = new LinkedList<>();
        logger.info("Site Crawling started for most used keywords" + webStats.getDomain() + " ...");
        ThreadPoolExecutor siteGraderThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, blockingQue);
        NinjaURL domainUrl = new NinjaURL(webStats.getDomain());
        NinjaURL redirectedDomainUrl = null;
        Document domainDoc = null;
        Set<NinjaURL> crawlingURLs = new LinkedHashSet<>(); // shared set of urls being processed
        Set<NinjaURL> urlQueue = new LinkedHashSet<>(); //shared set of urls in queue to be processed
        String statusMessage = NinjaURLCrawlerUtil.checkURL(domainUrl);
        try {
            if (statusMessage.equals("too many redirection") || statusMessage.equals("invalid")
                    || domainUrl.getRedirectedURL().equals("")) {
                logger.info("No data fetched for USED KEYWORDS. closing...");
                return mostUsedKeyWords;
            }
            LxrmUrlStats homapageStats = new LxrmUrlStats(domainUrl.getRedirectedURL());
            homapageStats.setLinkDepthId(0);

            /**
             * for non redirected URLs also there can be redirected URL in
             * domain URL object with same value as primary URL.
             */
            if (!domainUrl.getUrl().equals(domainUrl.getRedirectedURL())) {
                redirectedDomainUrl = new NinjaURL(domainUrl.getRedirectedURL(), NinjaURL.PROCESSING);

                int urlStatusCode = Common.getURLStatus(domainUrl.getRedirectedURL());
                logger.info("Redirected Domain Url: " + domainUrl.getRedirectedURL() + " Status Code: " + urlStatusCode);
                redirectedDomainUrl.setUrlStatus(urlStatusCode);
                homapageStats.setStatusCode(urlStatusCode);
                redirectedDomainUrl.setRedirectedURL(domainUrl.getRedirectedURL());
                domainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                crawlingURLs.add(redirectedDomainUrl);
                domainDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedDomainUrl);
            } else {
                crawlingURLs.add(domainUrl);
                logger.info("Createing JsoupConnection....");
                domainDoc = NinjaURLCrawlerUtil.createJsoupConnection(domainUrl);
                homapageStats.setStatusCode(domainUrl.getUrlStatus());
            }
            BaseRobotRules baseRobotRules = null;
            baseRobotRules = NinjaURLCrawlerUtil.fetchRobotRules("Googlebot", webStats.getDomain());
//		NinjaURLCrawlerThread.setBaseRobotRules(baseRobotRules);
            if (baseRobotRules != null) {
                LXRSEOURLFetchThread.setBaseRobotRules(baseRobotRules);
            }
            List<NinjaURL> internalURLs = new LinkedList<>();
            List<LxrmUrlStats> lxrmUrlStatsList = new LinkedList<>();
            session.setAttribute("mostUsedKeywordsCrawlingULrs", lxrmUrlStatsList);

            NinjaURL useDomainURL = new NinjaURL(homapageStats.getUrl(), NinjaURL.COMPLETED);
            useDomainURL.setBaseUrl(homapageStats.getUrl());
            internalURLs.add(useDomainURL);
            lxrmUrlStatsService.fetchStats(domainDoc, homapageStats, internalURLs, webStats.getDomain(), baseRobotRules);

            /*Processing keyword density for home page url.*/
            MostUsedKeyWordsResult homePageKeyWordsResult = new MostUsedKeyWordsResult(homapageStats.getUrl());
            lxrmUrlStatsService.fetchKeywordMetric(homePageKeyWordsResult, domainDoc, stopWordsList);
            homapageStats.setMostUsedKeyWordsResult(homePageKeyWordsResult);

            lxrmUrlStatsService.addURLStatsInaList(lxrmUrlStatsList, homapageStats);
            synchronized (urlQueue) {
                urlQueue.addAll(internalURLs);
                urlQueue.notifyAll();
            }
            if (redirectedDomainUrl != null) {
                synchronized (redirectedDomainUrl) {
                    redirectedDomainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                    redirectedDomainUrl.notifyAll();
                }
            } else {
                synchronized (domainUrl) {
                    domainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                    domainUrl.notifyAll();
                }
            }

            boolean terminated = false;
            while (!terminated) {
                try {
                    synchronized (urlQueue) {
                        if (!urlQueue.isEmpty()) {
                            synchronized (crawlingURLs) {
                                for (NinjaURL url : urlQueue) {
                                    if (crawlingURLs.size() < crawlingUrlLimit) {
                                        boolean added = crawlingURLs.add(url);
                                        if (added) {
                                            url.setCrawlStatus(NinjaURL.ADDED);
                                            LxrmUrlFecthThread urlThread = new LxrmUrlFecthThread(url, lxrmUrlStatsService, crawlingURLs, urlQueue, webStats, lxrmUrlStatsList, Common.MOST_USED_KEYWORDS_DENSITY, stopWordsList);
                                            siteGraderThreadPool.execute(urlThread);
                                            /*logger.info("The size of crawlingURLs is "+crawlingURLs.size());*/
                                        }
                                    } else {
                                        break;
                                    }
                                }
                                crawlingURLs.notifyAll();
                            }
                        }
                        urlQueue.clear();
                        urlQueue.notifyAll();
                    }
                    int active = siteGraderThreadPool.getActiveCount();
                    int queued = siteGraderThreadPool.getQueue().size();
                    /*try {
                        session.setAttribute("keywordsURLCount", (active + queued));
                    } catch (Exception e) {
                        logger.error("Unable to set blcTotUrlCnt in session ", e);
                    }*/
                    if ((active + queued) > 0) {
                        synchronized (crawlingURLs) {
                            if ((crawlingURLs.size() >= crawlingUrlLimit && !siteGraderThreadPool.isShutdown())) {
                                logger.info("Shutting down most used keyword  threadpool for '" + webStats.getDomain() + "'");
                                siteGraderThreadPool.shutdown();
                                //it won't take new tasks
                            }
                            crawlingURLs.notifyAll();
                        }
                        Thread.sleep(2000);
                    } else {
                        terminated = true;
                        keywordsUrlCrawlingStatus = true;
                        logger.debug("Terminating most used keyword threadpool for '" + webStats.getDomain() + "' and the value of UrlCrawlingStatus2 in service class " + keywordsUrlCrawlingStatus);
                        session.setAttribute("keywordsUrlCrawlingStatus", keywordsUrlCrawlingStatus);
                        if (!siteGraderThreadPool.isShutdown()) {
                            siteGraderThreadPool.shutdownNow();
                        }
                    }
                } catch (InterruptedException ex) {
                    logger.error("Most used keyword threadpool execution interrupted..", ex);
                } catch (Exception ex) {
                    logger.error("Exception: Most used keyword analysis threadpool..", ex);
                }
            }
            logger.info("Completed scanning of urls for query URL: " + webStats.getDomain() + ", with size: " + crawlingURLs.size());

            /**
             * Analyzing the link depth for keyword density URL.
             */
            session.removeAttribute("keywordsLinkDepth");
            int linkDepth;
            if (!lxrmUrlStatsList.isEmpty()) {
                linkDepth = lxrmUrlStatsList.stream().map(LxrmUrlStats::getLinkDepthId).reduce(Integer::max).get();
                session.setAttribute("keywordsLinkDepth", linkDepth);
            }
            /**
             * Extracting most used keyword objects from a list of LxrmUrlStats
             * objects in two approach.Both have same functionality.
             */
            mostUsedKeyWords = lxrmUrlStatsList.stream().filter(urStats -> urStats.getMostUsedKeyWordsResult() != null).map(LxrmUrlStats::getMostUsedKeyWordsResult).collect(Collectors.toList());
            return mostUsedKeyWords;
        } catch (Exception e) {
            logger.error("Error: crawling Domain For Urls Genration............ ", e);
        }
        return mostUsedKeyWords;
    }

    public void prepareDashBoardReport(List<MostUsedKeyWordsResult> mostUsedKeywordsResult, MostUsedKeywords mostUsedKeyword) {
        if (!mostUsedKeywordsResult.isEmpty()) {
            /**
             * Calculating average keywords from sum of total keywords for all
             * URL'S of web site.
             */
            int averageKeywords = (int) (mostUsedKeywordsResult.stream().mapToInt(keyword -> keyword.getKeywordsCount()).sum()) / mostUsedKeywordsResult.size();
            /**
             * Calculating average text/code ratio from sum of total text/code
             * ratio for all URL'S of web site.
             */
            float averageTextCodeRatio = (float) (mostUsedKeywordsResult.stream().mapToDouble(keyword -> keyword.getTextCodeRatio()).sum()) / mostUsedKeywordsResult.size();

            mostUsedKeyword.setAverageKeywords(averageKeywords);
            mostUsedKeyword.setAverageTextCodeRatio(getDecimalFormat(averageTextCodeRatio));

            /* Searching the minimum and maximum keywords from result list for a web site.*/
            mostUsedKeyword.setMinimumKeywords(mostUsedKeywordsResult.stream().min(Comparator.comparing(item -> item.getKeywordsCount())).get().getKeywordsCount());
            mostUsedKeyword.setMaximiumKeywords(mostUsedKeywordsResult.stream().max(Comparator.comparing(item -> item.getKeywordsCount())).get().getKeywordsCount());

            /* Searching the minimum and maximum keywords from result list for a web site.*/
            mostUsedKeyword.setMinimumTextCodeRatio(mostUsedKeywordsResult.stream().min(Comparator.comparing(item -> item.getTextCodeRatio())).get().getTextCodeRatio());
            mostUsedKeyword.setMaximiumTextCodeRatio(mostUsedKeywordsResult.stream().max(Comparator.comparing(item -> item.getTextCodeRatio())).get().getTextCodeRatio());

            /* To store type of phrases keyword for all URL'S of web site.*/
            Map<String, Integer> singlePharse = new LinkedHashMap<>();
            Map<String, Integer> doublePharse = new LinkedHashMap<>();
            Map<String, Integer> multiplePharse = new LinkedHashMap<>();

            /* Collecting all total keywords map object of MostUsedKeyWordsResult into single Map object. */
            List<Map<String, Integer>> completeKeywordsList = mostUsedKeywordsResult.stream().map(mukr -> mukr.getTotalKeywords()).collect(toList());

            /* Merging all keyword pharses from completeKeywordsList map object into single Map object. */
            Map<String, Integer> totalPharses = completeKeywordsList.stream().flatMap(m -> m.entrySet().stream())
                    .collect(groupingBy(Map.Entry::getKey, summingInt(Map.Entry::getValue)));

            /* sorting all keywords based on  reptitve count with irespective pharses. */
            Map<String, Integer> sortedKeywordPharses = new LinkedHashMap<>();
            totalPharses.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .forEachOrdered(x -> sortedKeywordPharses.put(x.getKey(), x.getValue()));

            /* Separting the type of keywords depend upon pharses. */
            sortedKeywordPharses.forEach((key, value) -> {
                List<String> items = Arrays.asList(key.split(" "));
                switch (items.size()) {
                    case 1:
                        singlePharse.put(key, value);
                        break;
                    case 2:
                        doublePharse.put(key, value);
                        break;
                    case 3:
                        multiplePharse.put(key, value);
                        break;
                }
            });
            List<Map<String, Integer>> topKeywordList = new LinkedList<>();
            topKeywordList.add(singlePharse);
            topKeywordList.add(doublePharse);
            topKeywordList.add(multiplePharse);

            /* Selecting top 10 keywords based on reptetive count from total keywords. */
            Map<String, Integer> topKeywordsofDomain = Common.getMaxElementsFromMap(topKeywordList, 3, 10);
            mostUsedKeyword.setTopKeywordsofDomain(topKeywordsofDomain);
        }
    }

    public float getDecimalFormat(float averageCount) {
        float averageMetric = 0;
        try {
            DecimalFormat df = new DecimalFormat("#,##,###,##0.00");
            averageMetric = Float.parseFloat(df.format(averageCount));
        } catch (Exception e) {
            logger.error("Exception in getDecimalFormat" + e.getMessage());
        }
        return averageMetric;
    }

    public String createMUSDKeywordsXLSFile(String downloadfolder, HttpSession session, Login user) {
        try {
            List<MostUsedKeyWordsResult> mostUsedKeyWordsResult = null;
            if (session.getAttribute("mostUsedKeywordResult") != null) {
                mostUsedKeyWordsResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
            } else {
                mostUsedKeyWordsResult = new LinkedList<>();
            }
            String validMostUsedKeywordsUrl = " ";
            if (session.getAttribute("validMostUsedKeywordsUrl") != null) {
                validMostUsedKeywordsUrl = (String) session.getAttribute("validMostUsedKeywordsUrl");
            }
            MostUsedKeywords mostUsedKeywords = new MostUsedKeywords();
            if (session.getAttribute("dashBoardResult") != null) {
                mostUsedKeywords = (MostUsedKeywords) session.getAttribute("dashBoardResult");
            }
            List<MostUsedKeyWordsResult> keywordsStats = mostUsedKeyWordsResult;
            HSSFCellStyle mainheadCellSty;
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFFont headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            HSSFFont font = workBook.createFont();
            font.setBold(true);
            //for main heading
            mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);
            HSSFSheet sheet = workBook.createSheet("Most Used Keywords Report");
            sheet.setDisplayGridlines(false);

            boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
            /*To set coloum width based on requried coloums.*/
//            int colWidth[] = new int[]{10, 40, 20, 20, 60};
            HSSFFont font1 = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            HSSFCellStyle subColHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            /* For logo and Reoprt name.*/
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 7));
            createMergedCell(rows, mainheadCellSty, 1, 7);
            cell.setCellValue("Most Used Keywords Report");
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 1000);

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(false);
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);

            heaCelSty.setBorderBottom(BorderStyle.THIN);
            heaCelSty.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            heaCelSty.setBorderRight(BorderStyle.THIN);
            heaCelSty.setRightBorderColor(IndexedColors.BLACK.getIndex());
            heaCelSty.setBorderLeft(BorderStyle.THIN);
            heaCelSty.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            heaCelSty.setBorderTop(BorderStyle.THIN);
            heaCelSty.setTopBorderColor(IndexedColors.BLACK.getIndex());

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            font1 = workBook.createFont();
            font1.setBold(true);
            columnHeaderStyle.setFont(font1);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.LEFT);

            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            subColHeaderStyle.setFont(font1);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.LEFT);
            subColHeaderStyle.setBorderBottom(BorderStyle.THIN);
            subColHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderRight(BorderStyle.THIN);
            subColHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderLeft(BorderStyle.THIN);
            subColHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderTop(BorderStyle.THIN);
            subColHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftColHeaderStyle = workBook.createCellStyle();
            leftColHeaderStyle.setFont(font1);
            leftColHeaderStyle.setWrapText(true);
            leftColHeaderStyle.setAlignment(HorizontalAlignment.RIGHT);

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            /*dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));*/
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMMM_dd_yy"));
            logger.info("Generating Report in .xls format");

            /*For Domain Name label.*/
            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 3));
            createMergedCell(rows, subColHeaderStyle, 0, 3);
            cell.setCellValue("Domain Name: " + validMostUsedKeywordsUrl);
            cell.setCellStyle(subColHeaderStyle);

            //            rows = sheet.createRow(2);
            cell = rows.createCell(5);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 13));
            cell.setCellValue("Reviewed on: " + curDate);
            cell.setCellStyle(leftColHeaderStyle);

            /*For Link Depth label.*/
            rows = sheet.createRow(2);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 3));
            createMergedCell(rows, subColHeaderStyle, 0, 3);
            cell.setCellValue("Link Depth: " + mostUsedKeywords.getLinkDepth());
            cell.setCellStyle(subColHeaderStyle);

            /*For Pages Scanned label.*/
            rows = sheet.createRow(3);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 3));
            createMergedCell(rows, subColHeaderStyle, 0, 3);
            cell.setCellValue("Pages Scanned: " + keywordsStats.size());
            cell.setCellStyle(subColHeaderStyle);

            HSSFFont averageCountFont = workBook.createFont();
            averageCountFont.setFontHeightInPoints((short) 24);
            averageCountFont.setBold(true);

            HSSFCellStyle averageCountStyle = workBook.createCellStyle();
            averageCountStyle.setFont(averageCountFont);
            averageCountStyle.setWrapText(true);
            averageCountStyle.setAlignment(HorizontalAlignment.CENTER);
            averageCountStyle.setBorderRight(BorderStyle.THIN);
            averageCountStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            averageCountStyle.setBorderTop(BorderStyle.THIN);
            averageCountStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFFont averageHeaderFont = workBook.createFont();
            averageHeaderFont.setFontHeightInPoints((short) 12);
            averageHeaderFont.setBold(true);

            HSSFCellStyle aveargeHeaderStyle = workBook.createCellStyle();
            aveargeHeaderStyle.setFont(averageHeaderFont);
            aveargeHeaderStyle.setWrapText(true);
            aveargeHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            aveargeHeaderStyle.setBorderBottom(BorderStyle.THIN);
            aveargeHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            aveargeHeaderStyle.setBorderRight(BorderStyle.THIN);
            aveargeHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            HSSFFont minMaxHeaderFont = workBook.createFont();
            minMaxHeaderFont.setFontHeightInPoints((short) 8);
            minMaxHeaderFont.setBold(true);

            HSSFCellStyle minHeaderStyle = workBook.createCellStyle();
            minHeaderStyle.setFont(minMaxHeaderFont);
            minHeaderStyle.setWrapText(true);
            minHeaderStyle.setAlignment(HorizontalAlignment.RIGHT);
            minHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFCellStyle maxHeaderStyle = workBook.createCellStyle();
            maxHeaderStyle.setFont(minMaxHeaderFont);
            maxHeaderStyle.setWrapText(true);
            maxHeaderStyle.setAlignment(HorizontalAlignment.RIGHT);
            maxHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFFont minFontStyle = workBook.createFont();
//            minFontStyle.setColor(HSSFColor.RED.index);
            minFontStyle.setFontHeightInPoints((short) 18);

            HSSFFont maxFontStyle = workBook.createFont();
//            maxFontStyle.setColor(HSSFColor.GREEN.index);
            maxFontStyle.setFontHeightInPoints((short) 18);

            HSSFCellStyle minValueStyle = workBook.createCellStyle();
            minValueStyle.setAlignment(HorizontalAlignment.LEFT);
            minValueStyle.setFont(minFontStyle);

            HSSFCellStyle maxValueStyle = workBook.createCellStyle();
            maxValueStyle.setAlignment(HorizontalAlignment.LEFT);
            maxValueStyle.setFont(maxFontStyle);
            maxValueStyle.setBorderRight(BorderStyle.THIN);
            maxValueStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            int mergingRowNo;
            int rowNo = 6;
            rows = sheet.createRow(rowNo);
            mergingRowNo = rowNo + 1;
            /*In Excell Row No: 7,8(Merging rows 7,8) Average keywords Metric Value.*/
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, mergingRowNo, 0, 3));
            cell = rows.createCell(0);
            createMergedCell(rows, averageCountStyle, 0, 3);
            cell.setCellValue(mostUsedKeywords.getAverageKeywords());
            cell.setCellStyle(averageCountStyle);
            /*In Excell Row No: 7,8(Merging rows 7,8) Most used keywords header.*/
            rows = sheet.getRow(rowNo);
            rows.setHeight((short) 500);
            cell = rows.createCell(5);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, mergingRowNo, 5, 13));
            createMergedCell(rows, subColHeaderStyle, 5, 13);
            HSSFCellStyle mostUsedHeaderStyle = subColHeaderStyle;
            mostUsedHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            cell.setCellStyle(mostUsedHeaderStyle);
            cell.setCellValue("Most Used Keywords:");

            /*In Excell Row No: 9, Average keywords Min and Max Metric Value.*/
            rowNo = rowNo + 2;
            int mostKeywordsRow = rowNo;
            rows = sheet.createRow(rowNo);/*rowNo : 8*/
            rows.setHeight((short) 500);

            /*Minimum keywords label*/
            cell = rows.createCell(0);
            cell.setCellValue("Min:");
            cell.setCellStyle(minHeaderStyle);

            /*Minimum keywords count*/
            cell = rows.createCell(1);
            cell.setCellValue(mostUsedKeywords.getMinimumKeywords());
            cell.setCellStyle(minValueStyle);

            /*Maximum keywords label*/
            cell = rows.createCell(2);
            cell.setCellValue("Max:");
            cell.setCellStyle(maxHeaderStyle);

            /*Maximum keywords count*/
            cell = rows.createCell(3);
            cell.setCellValue(mostUsedKeywords.getMaximiumKeywords());
            cell.setCellStyle(maxValueStyle);

            /*In Excell Row No: 10, Average keywords Metric Header.*/
            rowNo = rowNo + 1;
            rows = sheet.createRow(rowNo);/*rowNo : 9*/
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, rowNo, 0, 3));
            createMergedCell(rows, aveargeHeaderStyle, 0, 3);
            cell.setCellValue("Average No.of Keywords");

            cell.setCellStyle(aveargeHeaderStyle);

            /*In Excell Row No: 11,Empty Row.*/
            rowNo = rowNo + 1;
            rows = sheet.createRow(rowNo);/*rowNo : 10*/
            rows.setHeight((short) 500);

            /*In Excell Row No: 12, Average Text/Code Ratio Metric Value.*/
            rowNo = rowNo + 1;
            mergingRowNo = rowNo + 1;
            rows = sheet.createRow(rowNo);/*rowNo : 11*/
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, mergingRowNo, 0, 3));
            createMergedCell(rows, averageCountStyle, 0, 3);
            cell.setCellValue(mostUsedKeywords.getAverageTextCodeRatio() + "%");
            cell.setCellStyle(averageCountStyle);

            /*In Excell Row No: 13, Average Text/Code Ratio Min and Max Metric Header.*/
            rowNo = rowNo + 2;
            rows = sheet.createRow(rowNo);/*rowNo : 12*/
            rows.setHeight((short) 500);
            /*Minimum TextCode label*/
            cell = rows.createCell(0);
            cell.setCellValue("Min:");
            cell.setCellStyle(minHeaderStyle);
            /*Minimum TextCode count*/
            cell = rows.createCell(1);
            cell.setCellValue(mostUsedKeywords.getMinimumTextCodeRatio() + "%");
            cell.setCellStyle(minValueStyle);
            /*Maximum TextCode label*/
            cell = rows.createCell(2);
            cell.setCellValue("Max:");
            cell.setCellStyle(maxHeaderStyle);
            /*Maximum TextCode label*/
            cell = rows.createCell(3);
            cell.setCellValue(mostUsedKeywords.getMaximiumTextCodeRatio() + "%");
            cell.setCellStyle(maxValueStyle);

            /*In Excell Row No: 14, Average Text/Code Ratio Metric Header.*/
            rowNo = rowNo + 1;
            rows = sheet.createRow(rowNo);//13
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, rowNo, 0, 3));
            createMergedCell(rows, aveargeHeaderStyle, 0, 3);
            cell.setCellValue("Average Text/Code Ratio");
            cell.setCellStyle(aveargeHeaderStyle);

            HSSFFont mostKeywordFontStyle = workBook.createFont();
            mostKeywordFontStyle.setFontHeightInPoints((short) 10.5);

            HSSFCellStyle mostKeywordCellStyle = workBook.createCellStyle();
            mostKeywordCellStyle.setFont(mostKeywordFontStyle);
            mostKeywordCellStyle.setWrapText(true);
            mostKeywordCellStyle.setAlignment(HorizontalAlignment.LEFT);
            mostKeywordCellStyle.setBorderBottom(BorderStyle.THIN);
            mostKeywordCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            mostKeywordCellStyle.setBorderRight(BorderStyle.THIN);
            mostKeywordCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            mostKeywordCellStyle.setBorderLeft(BorderStyle.THIN);
            mostKeywordCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            mostKeywordCellStyle.setBorderTop(BorderStyle.THIN);
            mostKeywordCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            /*For Most used keywords section*/
            Map<String, Integer> topUsedKeywords = mostUsedKeywords.getTopKeywordsofDomain();
            List<String> topKeywords = new ArrayList<>(topUsedKeywords.keySet());
            String topKeyword;
            int reptetiveCount;
            if (topUsedKeywords.size() > 0) {
                /*Row starting value.*/
                int rowIterate = mostKeywordsRow;
                HSSFRow topKeywordRow = null;
                int keywordCount = 0;
                /*Starting cell value.*/
                int columcount = 5;
                while (columcount <= 13) {
                    keywordColoum:
                    for (int keywordIndex = keywordCount; keywordIndex < topKeywords.size();) {
                        topKeywordRow = sheet.getRow(rowIterate);
                        if (topKeywordRow == null) {
                            topKeywordRow = sheet.createRow(rowIterate);
                            topKeywordRow.setHeight((short) 500);
                        }
                        cell = topKeywordRow.createCell(columcount);
                        topKeyword = topKeywords.get(keywordIndex);
                        reptetiveCount = topUsedKeywords.get(topKeyword);
                        sheet.addMergedRegion(new CellRangeAddress(rowIterate, rowIterate, columcount, columcount + 2));
                        /* columcount+n Ending cell value.*/
                        createMergedCell(topKeywordRow, mostKeywordCellStyle, columcount, columcount + 2);
                        cell.setCellValue(topKeyword + "-" + reptetiveCount);
                        cell.setCellStyle(mostKeywordCellStyle);
                        keywordIndex++;
                        columcount = columcount + 3;
                        if ((keywordIndex == 3) || (keywordIndex == 6) || (keywordIndex == 9)) {
                            keywordCount = keywordIndex;
                            columcount = 5;
                            break keywordColoum;
                        }
                    }
                    /*Incrementing Row value.*/
                    rowIterate = rowIterate + 1;
                    /*Cursor will move to Next row.*/
                    if (rowIterate >= 12) {
                        break;
                    }
                }
            }

            /*In Excell Row No: 16, Empty row.*/
            rowNo = rowNo + 2;
            rows = sheet.createRow(rowNo + 2);
            rows.setHeight((short) 500);

            /*Result list starting row, the row number should increment 
            by 1 for varaible X to start itertaing list.*/
            rowNo = rowNo + 1;
            rows = sheet.createRow(rowNo);//16
            rows.setHeight((short) 500);

            cell = rows.createCell(0);
            cell.setCellValue("S.NO");
            cell.setCellStyle(columnHeaderStyle);
            heaCelSty.setWrapText(true);

            cell = rows.createCell(1);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, rowNo, 1, 5));
            createMergedCell(rows, columnHeaderStyle, 1, 5);
            cell.setCellValue("URL");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(6);
            cell.setCellValue("Text/Code Ratio");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(7);
            cell.setCellValue("Total Keywords");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(8);
            sheet.addMergedRegion(new CellRangeAddress(rowNo, rowNo, 8, 13));
            createMergedCell(rows, columnHeaderStyle, 8, 13);
            cell.setCellValue("Most Used Keywords");
            cell.setCellStyle(columnHeaderStyle);

            HSSFCellStyle textRatioStyle = workBook.createCellStyle();
            textRatioStyle.setAlignment(HorizontalAlignment.RIGHT);
            textRatioStyle.setFont(font);
            textRatioStyle.setWrapText(true);
            textRatioStyle.setBorderBottom(BorderStyle.THIN);
            textRatioStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            logger.info("The sheet is before if " + sheet);
            sheet.createFreezePane(0, rowNo + 1);
            if (keywordsStats.size() > 0) {
                logger.info("UrlStats size is :" + keywordsStats.size());
                try {
                    /*listIterate: Row starting value for first value of map. 
                      rowNumber: value for S.No*/
                    int listIterate = rowNo + 1, rowNumber = 0;
                    for (int j = 1; j < keywordsStats.size() + 1; j++) {
                        logger.debug("urlStats.get(j-1).getUrl()" + keywordsStats.get(j - 1).getSubDomain());
                        if (true) {
                            ++rowNumber;
                            rows = sheet.createRow(listIterate);
                            setHieghtToTheRow(keywordsStats.get(j - 1), rows);
                            for (int columcount = 0; columcount <= 4; columcount++) {
                                switch (columcount) {
                                    case 0:
                                        cell = rows.createCell(columcount);
                                        cell.setCellValue(rowNumber);
                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 1:
                                        cell = rows.createCell(columcount);
                                        sheet.addMergedRegion(new CellRangeAddress(listIterate, listIterate, 1, 5));
                                        createMergedCell(rows, columnHeaderStyle, 1, 5);
                                        cell.setCellStyle(heaCelSty);
                                        cell.setCellValue(keywordsStats.get(j - 1).getSubDomain());
                                        /*if (j == 1) {
                                            cell.setCellValue(keywordsStats.get(j - 1).getSubDomain());
                                        } else if (j != 1) {
                                            cell.setCellValue(validMostUsedKeywordsUrl + keywordsStats.get(j - 1).getSubDomain());
                                        }*/
                                        break;
                                    case 2:
                                        cell = rows.createCell(6);
                                        cell.setCellValue(((keywordsStats.get(j - 1).getTextCodeRatio())) + "%");
                                        cell.setCellStyle(textRatioStyle);
                                        break;
                                    case 3:
                                        cell = rows.createCell(7);
                                        cell.setCellValue(keywordsStats.get(j - 1).getKeywordsCount());
                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 4:
                                        try {
                                            cell = rows.createCell(8);
                                            sheet.addMergedRegion(new CellRangeAddress(listIterate, listIterate, 8, 13));
                                            createMergedCell(rows, columnHeaderStyle, 8, 13);
                                            Set<String> keywords = (keywordsStats.get(j - 1).getTopKeyWords());
                                            String[] mostKeywords = keywords.toArray(new String[keywords.size()]);
                                            String removeBraces = "";
                                            if (mostKeywords.length != 0) {
                                                removeBraces = Arrays.toString(mostKeywords);
                                                removeBraces = removeBraces.replace("[", " ").replace("]", " ");
                                                cell.setCellValue(removeBraces.trim() + ".");
                                            } else if (mostKeywords.length == 0) {
                                                removeBraces = "No keywords available";
                                                cell.setCellValue(removeBraces.trim() + ".");
                                            }

                                            cell.setCellStyle(heaCelSty);
                                        } catch (Exception e) {
                                            logger.error("Exception in converting most used keywords into array" + e.getMessage());
                                            cell = rows.createCell(columcount);
                                            cell.setCellValue(" ");
                                            cell.setCellStyle(heaCelSty);
                                        }
                                        break;
                                    default:
                                        break;
                                }

                            }
                            listIterate = listIterate + 1;
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception in creating UrlStats PDF" + e.getMessage());
                }
            }
            if ((keywordsStats.isEmpty())) {

                rows = sheet.createRow(14);
                cell = rows.createCell(0);
                rows.setHeight((short) 500);
                sheet.addMergedRegion(new CellRangeAddress(13, 13, 0, 3));
                cell.setCellValue("No Records Found");
                cell.setCellStyle(columnHeaderStyle);
//                cell.setCellStyle(subColHeaderStyle);

                columnHeaderStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                columnHeaderStyle.setFont(font1);
                columnHeaderStyle.setWrapText(true);
//            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
//            sheet.createFreezePane(7, 0);
//                rows = sheet.createRow(7);
//                sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 3));
//                rows.setHeight((short) 500);  
//                cell = rows.createCell(0);
//                cell.setCellValue("No records Found");
//                cell.setCellStyle(columnHeaderStyle);
//                cell.setCellStyle(heaCelSty);
            }
//            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(0, 3000);
            sheet.setColumnWidth(1, 3000);
            sheet.setColumnWidth(2, 3000);
            sheet.setColumnWidth(3, 3000);
//            for (int i = 0; i <= 4; i++) {
//                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
//            }
            logger.info(" Report generated in .xls format");

            String repName = "LXRMarketplace_Most_Used_Keywords__Report_" + user.getId();
            String reptName = Common.removeSpaces(repName);
            String fileName = Common.createFileName(reptName) + ".xls";
            logger.info(" The reptName is ==" + reptName + "====fileName is ==" + fileName);
            File file = new File(downloadfolder + fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
            return fileName;

        } catch (Exception e) {
            logger.error("Exception in createBLCURLStatsXLSFile casue:",e);
        }
        return null;
    }

    public PdfPCell createPdfCell(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public PdfPCell createPdfCellValues(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public String createMUSDKeywordsPDFFile(String downloadFolder, HttpSession session, Login user) throws FileNotFoundException, BadElementException, IOException {

        List<MostUsedKeyWordsResult> mostUsedKeyWordsResult = null;
        if (session.getAttribute("mostUsedKeywordResult") != null) {
            mostUsedKeyWordsResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
        } else {
            mostUsedKeyWordsResult = new LinkedList<>();
        }
        String validMostUsedKeywordsUrl = " ";
        if (session.getAttribute("validMostUsedKeywordsUrl") != null) {
            validMostUsedKeywordsUrl = (String) session.getAttribute("validMostUsedKeywordsUrl");
        }
        MostUsedKeywords mostUsedKeywords = new MostUsedKeywords();
        if (session.getAttribute("dashBoardResult") != null) {
            mostUsedKeywords = (MostUsedKeywords) session.getAttribute("dashBoardResult");
        }
        List<MostUsedKeyWordsResult> keywordsStats = mostUsedKeyWordsResult;

        System.out.println("in side of createPdfFile.................. ");

        com.itextpdf.text.Font reportNameFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, com.itextpdf.text.Font.BOLD, new BaseColor(255, 110, 0)));
        com.itextpdf.text.Font blackFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 13f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));
//        com.itextpdf.text.Font normalFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 13f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));

        String filename = "";
        try {
            String repName = "LXRMarketplace_Most_Used_Keyword_Report" + user.getId();
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            String filePath = downloadFolder + filename;
            logger.info("in side of createPdfFile. filePath................. " + filePath);
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            System.out.println(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Most Used Keywords Report", reportNameFont);
            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            rpName.setBorder(Rectangle.NO_BORDER);
            rpName.setPaddingRight(35f);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Reviewed on: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            dateName.setBorder(Rectangle.NO_BORDER);
            dateName.setPaddingRight(14f);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(27f);
            rpName.setPaddingBottom(27f);
            rpName.setPaddingTop(6f);
            rpName.setPaddingRight(30f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);
            /*For Domain Name label.*/
            Phrase domainName = new Phrase(new Chunk("Domain Name: " + validMostUsedKeywordsUrl, blackFont));
            /*For Link Depth label.*/
            Phrase linkDepth = new Phrase(new Chunk("Link Depth: " + mostUsedKeywords.getLinkDepth(), blackFont));
            /*For Pages Scanned label.*/
            Phrase pagesScanned = new Phrase(new Chunk("Pages Scanned: " + keywordsStats.size(), blackFont));
            pdfDoc.add(domainName);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(linkDepth);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(pagesScanned);
            pdfDoc.add(Chunk.NEWLINE);
            com.itextpdf.text.Font elementDescFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 9f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
            com.itextpdf.text.Font NoteParaFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));

//            com.itextpdf.text.Font elementDescFontRed = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 18f, com.itextpdf.text.Font.NORMAL, new BaseColor(255, 0, 0)));
//            com.itextpdf.text.Font elementDescFontGreen = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 18f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 139, 69)));
            com.itextpdf.text.Font elementDescFontRed = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 18f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));
            com.itextpdf.text.Font elementDescFontGreen = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 18f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));
            com.itextpdf.text.Font elementDescFontBlack = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 23f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));
            com.itextpdf.text.Font elementDescFontBlackSize = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 13f, com.itextpdf.text.Font.BOLD, BaseColor.BLACK));

            /* Average keywords | Text/Code Ratio with Min and Max Headers|values.*/
            float[] resultTabWidth = {50f, 50f};
            PdfPTable MinMaxTableLeft = new PdfPTable(resultTabWidth);
            MinMaxTableLeft.setWidthPercentage(50f);

            PdfPCell totalAverageKeywords = new PdfPCell(new Phrase(" " + mostUsedKeywords.getAverageKeywords(), elementDescFontBlack));
            totalAverageKeywords.setBorder(Rectangle.NO_BORDER);
            totalAverageKeywords.setColspan(2);
            totalAverageKeywords.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableLeft.addCell(totalAverageKeywords);

            Paragraph minKeywords = new Paragraph();
            Phrase minHeader = new Phrase("Min:", elementDescFont);
            Phrase minvalueKeywords = new Phrase("" + mostUsedKeywords.getMinimumKeywords(), elementDescFontRed);
            minKeywords.add(minHeader);
            minKeywords.add(minvalueKeywords);

            Paragraph maxKeywords = new Paragraph();
            Phrase maxHeader = new Phrase("Max:", elementDescFont);
            Phrase maxValue = new Phrase("" + mostUsedKeywords.getMaximiumKeywords(), elementDescFontGreen);
            maxKeywords.add(maxHeader);
            maxKeywords.add(maxValue);

            PdfPCell minKeywordsCell = new PdfPCell(new Phrase(minKeywords));
            minKeywordsCell.setBorder(Rectangle.NO_BORDER);
            minKeywordsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableLeft.addCell(minKeywordsCell);
            PdfPCell maxKeywordsCell = new PdfPCell(new Phrase(maxKeywords));
            maxKeywordsCell.setBorder(Rectangle.NO_BORDER);
            maxKeywordsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableLeft.addCell(maxKeywordsCell);
            PdfPCell avgNumKeywordsHeader = new PdfPCell(new Phrase("Average No.of Keywords", elementDescFontBlackSize));
            avgNumKeywordsHeader.setColspan(2);
            avgNumKeywordsHeader.setPaddingTop(10f);
            avgNumKeywordsHeader.setPaddingBottom(10f);
            avgNumKeywordsHeader.setBorder(Rectangle.NO_BORDER);
            avgNumKeywordsHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableLeft.addCell(avgNumKeywordsHeader);

            float[] TabWidth = {50f, 50f};
            PdfPTable MinMaxTableRight = new PdfPTable(TabWidth);
            MinMaxTableRight.setWidthPercentage(52f);

            PdfPCell totalAverageTextCodeRatio = new PdfPCell(new Phrase(mostUsedKeywords.getAverageTextCodeRatio() + "%", elementDescFontBlack));
            totalAverageTextCodeRatio.setBorder(Rectangle.NO_BORDER);
            totalAverageTextCodeRatio.setColspan(2);
            totalAverageTextCodeRatio.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableRight.addCell(totalAverageTextCodeRatio);

            Paragraph minTextCodeRatio = new Paragraph();
            Phrase minValueTextCode = new Phrase("" + mostUsedKeywords.getMinimumTextCodeRatio() + "%", elementDescFontRed);
            minTextCodeRatio.add(minHeader);
            minTextCodeRatio.add(minValueTextCode);

            Paragraph maxTextCodeRatio = new Paragraph();
            Phrase maxValueTextCode = new Phrase("" + mostUsedKeywords.getMaximiumTextCodeRatio() + "%", elementDescFontGreen);
            maxTextCodeRatio.add(maxHeader);
            maxTextCodeRatio.add(maxValueTextCode);

            PdfPCell minTextCodeRatioCell = new PdfPCell(new Phrase(minTextCodeRatio));
            minTextCodeRatioCell.setBorder(Rectangle.NO_BORDER);
            minTextCodeRatioCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableRight.addCell(minTextCodeRatioCell);

            PdfPCell maxTextCodeRatioCell = new PdfPCell(new Phrase(maxTextCodeRatio));
            maxTextCodeRatioCell.setBorder(Rectangle.NO_BORDER);
            maxTextCodeRatioCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableRight.addCell(maxTextCodeRatioCell);

            PdfPCell avgNumTextCodeHeader = new PdfPCell(new Phrase("Average Text/Code Ratio", elementDescFontBlackSize));
            avgNumTextCodeHeader.setColspan(2);
            avgNumTextCodeHeader.setPaddingTop(10f);
            avgNumTextCodeHeader.setPaddingBottom(10f);
            avgNumTextCodeHeader.setBorder(Rectangle.NO_BORDER);
            avgNumTextCodeHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
            MinMaxTableRight.addCell(avgNumTextCodeHeader);
            MinMaxTableRight.completeRow();

            float[] resultTabWid = {100f};
            PdfPTable tableForBorder = new PdfPTable(resultTabWid);
            tableForBorder.setWidthPercentage(40f);
            PdfPCell MinMaxTableLefttab = new PdfPCell(MinMaxTableLeft);
            MinMaxTableLefttab.setBorderColor(BaseColor.GRAY);
            MinMaxTableLefttab.setBorderColorTop(BaseColor.GRAY);
            tableForBorder.addCell(MinMaxTableLefttab);
            PdfPCell middleemptyCell = createPdfCell(" ");
            middleemptyCell.setBorder(Rectangle.NO_BORDER);
            tableForBorder.addCell(middleemptyCell);
            PdfPCell MinMaxTableRighttab = new PdfPCell(MinMaxTableRight);
            MinMaxTableRighttab.setBorderColor(BaseColor.GRAY);
            tableForBorder.addCell(MinMaxTableRighttab);

            /*For Most used keywords Heading*/
            float[] resultTabWidths = {33.3f, 33.3f, 33.3f};
            PdfPTable googleResultTab = new PdfPTable(resultTabWidths);
            googleResultTab.setWidthPercentage(99.9f);


            /*For Most used keywords section*/
            Map<String, Integer> topUsedKeywords = mostUsedKeywords.getTopKeywordsofDomain();
            List<String> topKeywords = new ArrayList<>(topUsedKeywords.keySet());

            int reptetiveCount;
            if (topUsedKeywords.size() > 0) {
                PdfPCell mostusedKeywordsHeader = new PdfPCell(new Phrase("Most Used Keywords:", blackFont));
                mostusedKeywordsHeader.setBorderColor(BaseColor.GRAY);
                mostusedKeywordsHeader.setBorderWidth(0.1f);
                mostusedKeywordsHeader.setPadding(6f);
                mostusedKeywordsHeader.setColspan(3);
                googleResultTab.addCell(mostusedKeywordsHeader);

                for (String keyword : topKeywords) {
                    reptetiveCount = topUsedKeywords.get(keyword);
                    PdfPCell mostusedKeywords = new PdfPCell(new Phrase(keyword + "-" + reptetiveCount, NoteParaFont));
                    mostusedKeywords.setBorderColor(BaseColor.GRAY);
                    mostusedKeywords.setBorderWidth(0.1f);
                    mostusedKeywords.setPadding(6f);
                    googleResultTab.addCell(mostusedKeywords);
                }
                /*topKeywordssize is 7,8 for that below follows.*/
                if (topUsedKeywords.size() % 3 == 2) {
                    PdfPCell foremptyCell = new PdfPCell(new Phrase(""));
                    foremptyCell.setBorder(Rectangle.NO_BORDER);
                    googleResultTab.addCell(foremptyCell);

                } else if (topUsedKeywords.size() % 3 == 1) {
                    PdfPCell foremptyCell = new PdfPCell(new Phrase(""));
                    foremptyCell.setBorder(Rectangle.NO_BORDER);
                    googleResultTab.addCell(foremptyCell);
                    googleResultTab.addCell(foremptyCell);
                }
            }
            googleResultTab.setWidthPercentage(40);
            googleResultTab.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfDoc.add(Chunk.NEWLINE);

            /*Mergging multiple tables in one table(JoinTable)*/
            float[] TabWidths = {45f, 10f, 45f};
            PdfPTable joinTable = new PdfPTable(TabWidths);
            joinTable.setWidthPercentage(100f);
            PdfPCell emptyCell = createPdfCell(" ");
            emptyCell.setBorder(Rectangle.NO_BORDER);
            emptyCell.setColspan(3);
            joinTable.addCell(emptyCell);
            PdfPCell minMaxTableLefttab = new PdfPCell(tableForBorder);
            minMaxTableLefttab.setBorderColor(BaseColor.GRAY);
            minMaxTableLefttab.setBorder(Rectangle.NO_BORDER);
            joinTable.addCell(minMaxTableLefttab);
            PdfPCell midemptyCell = createPdfCell(" ");
            midemptyCell.setBorder(Rectangle.NO_BORDER);
            joinTable.addCell(midemptyCell);
            PdfPCell mustUsedKeywordsTable = new PdfPCell(googleResultTab);
            mustUsedKeywordsTable.setBorder(Rectangle.NO_BORDER);
            mustUsedKeywordsTable.setPaddingTop(20f);
            mustUsedKeywordsTable.setPaddingBottom(30f);
            joinTable.addCell(mustUsedKeywordsTable);
            pdfDoc.add(joinTable);
            pdfDoc.add(Chunk.NEWLINE);

            float[] resultWidths = {5f, 35f, 15f, 15f, 35f};
            PdfPTable googleResult = new PdfPTable(resultWidths);
            googleResult.setWidthPercentage(99.9f);
            /*Table Headers*/
            PdfPCell sNoHeader = createPdfCell("S.No");
            PdfPCell urlHeader = createPdfCell("URL");
            PdfPCell textCodeRationHeader = createPdfCell("Text/Code Ratio");
            PdfPCell totalKeywordsHeader = createPdfCell("Total Keywords");
            PdfPCell mostUsedKeywordsHeader = createPdfCell("Most Used Keywords");

            googleResult.addCell(sNoHeader);
            googleResult.addCell(urlHeader);
            googleResult.addCell(textCodeRationHeader);
            googleResult.addCell(totalKeywordsHeader);
            googleResult.addCell(mostUsedKeywordsHeader);
            /*Table Headers Values*/
            if (keywordsStats.size() > 0) {
                logger.info("UrlStats size is :" + keywordsStats.size());

                for (int j = 1; j < keywordsStats.size() + 1; j++) {
                    logger.debug("urlStats.get(j-1).getUrl()" + keywordsStats.get(j - 1).getSubDomain());

                    for (int i = 0; i <= 4; i++) {
                        if (i == 0) {

                            PdfPCell sno = createPdfCellValues(j + "");
                            googleResult.addCell(sno);
                        } else if (i == 1) {
                            /*if (j == 1) {
                                PdfPCell domain = createPdfCellValues(keywordsStats.get(j - 1).getSubDomain());
                                googleResult.addCell(domain);
                            } else if (j != 1) {
                                PdfPCell domain = createPdfCellValues(validMostUsedKeywordsUrl + keywordsStats.get(j - 1).getSubDomain());
                                googleResult.addCell(domain);
                            }*/
                            PdfPCell domain = createPdfCellValues(keywordsStats.get(j - 1).getSubDomain());
                            googleResult.addCell(domain);
                        } else if (i == 2) {

                            PdfPCell textRatio = createPdfCellValues((keywordsStats.get(j - 1).getTextCodeRatio()) + "%");
                            textRatio.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                            googleResult.addCell(textRatio);
                        } else if (i == 3) {
                            PdfPCell keywordsStatus = createPdfCellValues(("" + keywordsStats.get(j - 1).getKeywordsCount()));
                            keywordsStatus.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
                            googleResult.addCell(keywordsStatus);
                        } else if (i == 4) {
                            try {
                                Set<String> keywords = (keywordsStats.get(j - 1).getTopKeyWords());
                                String[] mostKeywords = keywords.toArray(new String[keywords.size()]);
                                String removeBraces = "";
                                if (mostKeywords.length != 0) {
                                    removeBraces = Arrays.toString(mostKeywords);
                                    removeBraces = removeBraces.replace("[", " ").replace("]", " ");
                                    PdfPCell trimBraces = createPdfCellValues(removeBraces.trim() + ".");
                                    googleResult.addCell(trimBraces);
                                } else {
                                    removeBraces = "No keywords available";
                                    PdfPCell trimBraces = createPdfCellValues(removeBraces.trim() + ".");
                                    googleResult.addCell(trimBraces);
                                }

                            } catch (Exception e) {
                                logger.error("Exception while constructing row for table in PDF: ", e);
                            }
                        }

                    }

                }

                pdfDoc.add(googleResult);
                pdfDoc.close();
            }
        } catch (DocumentException e) {
            System.out.println(e.getMessage());
        }

        return filename;
    }

    /*To set height based on url content length*/
    private void setHieghtToTheRow(MostUsedKeyWordsResult urlStats, HSSFRow rows) {
        logger.debug("Description" + urlStats.getSubDomain().length());
        if (urlStats.getSubDomain().length() > 150) {
            rows.setHeight((short) 1700);
        } else {
            rows.setHeight((short) 700);
        }
    }

    public void createMergedCell(HSSFRow rows, HSSFCellStyle style, int startCellNo, int endCellNo) {
        HSSFCell cell;
        for (int i = startCellNo; i <= endCellNo; i++) {
            cell = rows.createCell(i);
            cell.setCellStyle(style);
        }
    }

}
