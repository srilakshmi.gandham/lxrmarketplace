/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mostusedkeywords;

import java.util.Map;

/**
 *
 * @author sagar
 */
public class MostUsedKeywords {

    private String userDomain;

    private int averageKeywords;
    private float averageTextCodeRatio;

    private int minimumKeywords;
    private int maximiumKeywords;

    private double minimumTextCodeRatio;
    private double maximiumTextCodeRatio;

    private Map<String, Integer> topKeywordsofDomain;
    
    private int linkDepth;

    public String getUserDomain() {
        return userDomain;
    }

    public void setUserDomain(String userDomain) {
        this.userDomain = userDomain;
    }

    public int getAverageKeywords() {
        return averageKeywords;
    }

    public void setAverageKeywords(int averageKeywords) {
        this.averageKeywords = averageKeywords;
    }

    public float getAverageTextCodeRatio() {
        return averageTextCodeRatio;
    }

    public void setAverageTextCodeRatio(float averageTextCodeRatio) {
        this.averageTextCodeRatio = averageTextCodeRatio;
    }

    public int getMinimumKeywords() {
        return minimumKeywords;
    }

    public void setMinimumKeywords(int minimumKeywords) {
        this.minimumKeywords = minimumKeywords;
    }

    public int getMaximiumKeywords() {
        return maximiumKeywords;
    }

    public void setMaximiumKeywords(int maximiumKeywords) {
        this.maximiumKeywords = maximiumKeywords;
    }

    public double getMinimumTextCodeRatio() {
        return minimumTextCodeRatio;
    }

    public void setMinimumTextCodeRatio(double minimumTextCodeRatio) {
        this.minimumTextCodeRatio = minimumTextCodeRatio;
    }

    public double getMaximiumTextCodeRatio() {
        return maximiumTextCodeRatio;
    }

    public void setMaximiumTextCodeRatio(double maximiumTextCodeRatio) {
        this.maximiumTextCodeRatio = maximiumTextCodeRatio;
    }

    public Map<String, Integer> getTopKeywordsofDomain() {
        return topKeywordsofDomain;
    }

    public void setTopKeywordsofDomain(Map<String, Integer> topKeywordsofDomain) {
        this.topKeywordsofDomain = topKeywordsofDomain;
    }

    public int getLinkDepth() {
        return linkDepth;
    }

    public void setLinkDepth(int linkDepth) {
        this.linkDepth = linkDepth;
    }

}
