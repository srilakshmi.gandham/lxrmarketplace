/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 *
 * @author NE16T1213-Sagar
 */
package lxr.marketplace.mostusedkeywords;

import java.util.Map;
import java.util.Set;


public class MostUsedKeyWordsResult {

private String subDomain;
private int keywordsCount;
private double textCodeRatio;
private Set<String> topKeyWords;
private Map<String,Integer> totalKeywords;



public MostUsedKeyWordsResult(String subDomain) {
		super();
		this.subDomain = subDomain;
//                totalKeywords = 0;
//                textCodeRatio = 0.0;
//                topKeyWords = new HashSet<>();
	}
    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public int getKeywordsCount() {
        return keywordsCount;
    }

    public void setKeywordsCount(int keywordsCount) {
        this.keywordsCount = keywordsCount;
    }

    public Map<String, Integer> getTotalKeywords() {
        return totalKeywords;
    }

    public void setTotalKeywords(Map<String, Integer> totalKeywords) {
        this.totalKeywords = totalKeywords;
    }

  
    
    public double getTextCodeRatio() {
        return textCodeRatio;
    }

    public void setTextCodeRatio(double textCodeRatio) {
        this.textCodeRatio = textCodeRatio;
    }

    public Set<String> getTopKeyWords() {
        return topKeyWords;
    }

    public void setTopKeyWords(Set<String> topKeyWords) {
        this.topKeyWords = topKeyWords;
    }


}
