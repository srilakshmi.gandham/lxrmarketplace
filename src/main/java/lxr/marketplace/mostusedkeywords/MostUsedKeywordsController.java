/**
 * @author NE16T1213-Eswar
 */
package lxr.marketplace.mostusedkeywords;

import com.itextpdf.text.BadElementException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.crawler.LxrmUrlStats;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Controller
@RequestMapping("/most-used-keywords-tool.html")
public class MostUsedKeywordsController {

    private static final Logger logger = Logger.getLogger(MostUsedKeywordsController.class);

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    /* @Autowired
    private ToolService toolService;

    @Autowired
    private EmailTemplateService emailTemplateService;*/
    @Autowired
    /* downloadFolder variable for Download Report */
    private String downloadFolder;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    /* Service class Reference */
    private MostUsedKeywordService mostUsedKeywordService;

    private Login user;

    JSONObject userInputJson = null;

    private int toolAccessLimit;

    private static final String FORM_VIEW = "views/mostUsedKeywords/mostUsedKeywords";
//    private static final String FORM_VIEW = "view/mostUsedkeywords/mostUsedkeywords";

    /*@return : 
     * 1: Navigation to tool page. 
     * 2: Navigation to tool result page when user comes back(CANCEL) from ask the expert confirmation page. 
     */
    @RequestMapping(method = RequestMethod.GET)
    public String navigationToolPage(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {

        String requestParam = request.getParameter("backToSuccessPage");
        if ((requestParam != null) && (requestParam.equalsIgnoreCase("'backToSuccessPage'"))) {
            mostUsedKeywords.setUserDomain((String) session.getAttribute("validMostUsedKeywordsUrl"));
            List<MostUsedKeyWordsResult> mostUsedKeywordResult = new ArrayList<>();
            if (session.getAttribute("mostUsedKeywordResult") != null) {
                mostUsedKeywordResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
            }
            /*Preparing dash board results from most used keywords metrics result list.*/
            mostUsedKeywordService.prepareDashBoardReport(mostUsedKeywordResult, mostUsedKeywords);
            model.addAttribute("dashBoardView", mostUsedKeywords);
            model.addAttribute("keywordSuccess", true);
        } else {
            session.removeAttribute("mostUsedKeywordsTool");
            session.removeAttribute("mostUsedKeywordsCrawlingULrs");
            session.removeAttribute("mostKeywordsFinishedUrls");
            session.removeAttribute("mostKeywordsInvalidUrl");
            session.removeAttribute("validMostUsedKeywordsUrl");
            session.removeAttribute("mostUsedKeywordResult");
            mostUsedKeywords.setUserDomain("");
            model.addAttribute(mostUsedKeywords);
            model.addAttribute("keywordSuccess", false);
        }
        return FORM_VIEW;
    }

    /*
     * @param mostKeywordsURL : Processing to fetch most used keywords for input URL.
     * @return : Naviagtes to tool page. 
     */
    @RequestMapping(params = {"requestMethod=getResult"}, method = RequestMethod.POST)
    @ResponseBody
    public String processingUserRequestForAnalysis(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model,
            @RequestParam("toolQueryURL") String mostKeywordsURL) {
        try {
            user = (Login) session.getAttribute("user");
            if (user != null) {
                Tool toolObj = new Tool();
                toolObj.setToolId(34);
                toolObj.setToolName(Common.MOST_USED_KEYWORDS);
                toolObj.setToolLink("most-used-keywords-tool.html");
                session.setAttribute("toolObj", toolObj);
                session.setAttribute("currentTool", Common.MOST_USED_KEYWORDS);

                session.removeAttribute("mostUsedKeywordsTool");
                session.removeAttribute("mostUsedKeywordsCrawlingULrs");
                session.removeAttribute("mostKeywordsFinishedUrls");
                session.removeAttribute("mostKeywordsInvalidUrl");
                session.removeAttribute("keywordsUrlCrawlingStatus");

                /* Applying the tool access limit filter for local user. */
                toolAccessLimit = (user.isIslocal() && !user.isDeleted()) ? Common.LOCAL_USER_TOOL_LIMIT : Common.TOOL_ACCESS_LIMIT;

                session.setAttribute("mostUsedKeywordsTool", mostUsedKeywords);
                List<MostUsedKeyWordsResult> mostUsedKeywordResult = new ArrayList<>();
                session.setAttribute("mostKeywordsFinishedUrls", mostUsedKeywordResult);
                String validQueryUrl = null;
                if (mostUsedKeywords != null && mostUsedKeywords.getUserDomain() != null && !mostUsedKeywords.getUserDomain().trim().isEmpty()) {
                    validQueryUrl = Common.getUrlValidation(mostUsedKeywords.getUserDomain().trim(), session);
                } else if (mostKeywordsURL != null && !mostKeywordsURL.trim().isEmpty()) {
                    validQueryUrl = Common.getUrlValidation(mostKeywordsURL.trim(), session);
                } else {
                    validQueryUrl = "invalid";
                }
                session.removeAttribute("validMostUsedKeywordsUrl");
                if ((validQueryUrl != null && mostUsedKeywords != null)
                        && (validQueryUrl.equals("invalid") || validQueryUrl.equals("redirected"))) {
                    String mostKeywordsInvalidUrl = messageSource.getMessage("URL.error", null, Locale.US);
                    session.setAttribute("mostKeywordsInvalidUrl", mostKeywordsInvalidUrl);
                    session.setAttribute("validMostUsedKeywordsUrl", mostKeywordsURL);
                    session.removeAttribute("mostUsedKeywordResult");
                    logger.info("Query URL for most used keywords analysis: " + mostUsedKeywords.getUserDomain() + "is" + validQueryUrl);
//                    session.removeAttribute("mostUsedKeywordsTool");
//                    session.removeAttribute("mostUsedKeywordsCrawlingULrs");
//                    session.removeAttribute("mostKeywordsFinishedUrls");

                    /*String notWorkingUrl = "Your URL is not working, please check the URL.";
                    session.setAttribute("notWorkingUrl", notWorkingUrl);
                    model.addAttribute("notWorkingUrl", notWorkingUrl);
                    model.addAttribute("keywordSuccess", false);
                    return "view/mostUsedkeywords/mostUsedkeywords";*/
                } else if (validQueryUrl != null) {
                    if (!validQueryUrl.startsWith("http")) {
                        validQueryUrl = "http://" + validQueryUrl;
                    }
                    if (validQueryUrl.lastIndexOf("/") == validQueryUrl.length() - 1) {
                        validQueryUrl = validQueryUrl.substring(0, validQueryUrl.length() - 1);
                    }
                    if (mostUsedKeywords != null) {
                        mostUsedKeywords.setUserDomain(validQueryUrl);
                        session.setAttribute("validMostUsedKeywordsUrl", validQueryUrl);
                    }

                    List<String> stopWordsList = new ArrayList<>();
                    WebApplicationContext context1 = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
                    if (context1.getServletContext().getAttribute("stopwords") != null) {
                        stopWordsList = (List<String>) context1.getServletContext().getAttribute("stopwords");
                    }
                    /*Remvoing all used session objects for new report/result/analysis.*/
                    session.removeAttribute("mostUsedKeywordResult");
                    session.removeAttribute("keywordsURLCount");
                    session.removeAttribute("mostUsedKeywordsReport");
                    session.removeAttribute("mostUsedKeywordsCrawlingULrs");

                    URL keywordUrl = null;
                    String validDensityUrl = null;
                    NinjaWebStats ninjaWebStats = null;
                    try {
                        keywordUrl = new URL(validQueryUrl);
                        validDensityUrl = keywordUrl.getHost();
                    } catch (MalformedURLException e) {
                        validDensityUrl = validQueryUrl;
                        logger.error("Exception : Validating user query URL: ", e);
                    }
                    ninjaWebStats = new NinjaWebStats(validDensityUrl);
                    mostUsedKeywordResult = mostUsedKeywordService.crawlDomainToFetchMostUsedKeywords(ninjaWebStats, stopWordsList, session, toolAccessLimit);
                    if (mostUsedKeywordResult.isEmpty()) {
                        String mostKeywordsInvalidUrl = messageSource.getMessage("URL.error", null, Locale.US);
                        session.setAttribute("mostKeywordsInvalidUrl", mostKeywordsInvalidUrl);
                        model.addAttribute("mostKeywordsInvalidUrl", mostKeywordsInvalidUrl);
                        model.addAttribute("keywordSuccess", false);
                        return FORM_VIEW;
                    }
                    session.setAttribute("mostUsedKeywordResult", mostUsedKeywordResult);
                    model.addAttribute("keywordSuccess", true);
                    model.addAttribute(mostUsedKeywords);
                    return FORM_VIEW;
                }
            }
        } catch (Exception e) {
            logger.error("Exception: Processing request most used keywords analysis for query URL cause:: ", e);
        }
        model.addAttribute("keywordSuccess", false);
        model.addAttribute(mostUsedKeywords);
        return FORM_VIEW;
    }

    /**
     *
     * @param request
     * @param response
     * @param mostUsedKeyword
     * @param session
     * @param model
     * @return Navigation to tool result page after finishing of analyzing
     * URL's.
     */
    @RequestMapping(params = {"requestMethod=getToolData"}, method = RequestMethod.POST)
    public String returningResultPage(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeyword, HttpSession session, Model model) {
        try {
            user = (Login) session.getAttribute("user");
            session.removeAttribute("dashBoardResult");
            model.addAttribute("keywordSuccess", false);
            if (session.getAttribute("mostUsedKeywordResult") != null) {
                List<MostUsedKeyWordsResult> mostUsedKeywordResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
                mostUsedKeywordService.prepareDashBoardReport(mostUsedKeywordResult, mostUsedKeyword);
                int keywordsLinkDepth;
                if (session.getAttribute("keywordsLinkDepth") != null) {
                    keywordsLinkDepth = (int) session.getAttribute("keywordsLinkDepth");
                    mostUsedKeyword.setLinkDepth(keywordsLinkDepth);
                }
                mostUsedKeyword.setUserDomain((String) session.getAttribute("validMostUsedKeywordsUrl"));
                model.addAttribute("dashBoardView", mostUsedKeyword);
                session.setAttribute("dashBoardResult", mostUsedKeyword);
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(34);
                if (user.getId() == -1) {
                    user = (Login) session.getAttribute("user");
                }

                /*                if the request is coming from tool page then only add the user inputs to JSONObject
                if the request is coming from mail then do not add the user inputs to JSONObject*/
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && user != null && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                }

                model.addAttribute("keywordSuccess", true);
            }
        } catch (Exception e) {
            logger.error("Exception: Navigating to tool page: ", e);
        }
        return FORM_VIEW;
    }

    /*
     * @param request : To get status of URL's to be  processed.
     * @return : Set of URL's are analyzed.
     */
    @RequestMapping(params = {"requestMethod=getAnalyizedURLs"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] updatingTheCountOfURLAnalysied(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {
        Object[] responseData = new Object[3];
        try {
            /*String crawlingStatus = "false";*/
            String mostKeywordsInvalidUrl = "valid";
            List<LxrmUrlStats> lxrmUrlStatsList = new ArrayList<>();
            boolean keywordsUrlCrawlingStatus = false;
            if (session.getAttribute("keywordsUrlCrawlingStatus") != null) {
                keywordsUrlCrawlingStatus = (Boolean) session.getAttribute("keywordsUrlCrawlingStatus");
            }
            if (session.getAttribute("mostKeywordsInvalidUrl") != null) {
                mostKeywordsInvalidUrl = (String) session.getAttribute("mostKeywordsInvalidUrl");
            }
            /*if (keywordsUrlCrawlingStatus) {
                crawlingStatus = "true";
            }*/
            if (session.getAttribute("mostUsedKeywordsCrawlingULrs") != null) {
                lxrmUrlStatsList = (List<LxrmUrlStats>) session.getAttribute("mostUsedKeywordsCrawlingULrs");
            }
            responseData[0] = mostKeywordsInvalidUrl;
            responseData[1] = lxrmUrlStatsList.size();
            responseData[2] = keywordsUrlCrawlingStatus;
        } catch (Exception e) {
            logger.error("Exception: Updating the counting Noof URL's analysied: ", e);
        }
        return responseData;
    }

    /*
     * @return : Navigation to tool result page, when success full login by user through 
       PopUp/SignUP or login in header section.
     */
    @RequestMapping(params = {"requestMethod=getToolData", "loginRefresh=1"}, method = RequestMethod.GET)
    public String navigationToResultPageUserLogin(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {
        model.addAttribute("keywordSuccess", false);
        try {
            logger.debug("WebUtils Submit parameters i.e getResult,loginrefresh,backLink");
            session.setAttribute("loginrefresh", "loginrefresh");
            mostUsedKeywords.setUserDomain((String) session.getAttribute("validMostUsedKeywordsUrl"));
            List<MostUsedKeyWordsResult> mostUsedKeywordResult = new ArrayList<>();
            if (session.getAttribute("mostUsedKeywordResult") != null) {
                mostUsedKeywordResult = (List<MostUsedKeyWordsResult>) session.getAttribute("mostUsedKeywordResult");
            }
            /*Preparing dash board results from most used keywords metrics result list.*/
            mostUsedKeywordService.prepareDashBoardReport(mostUsedKeywordResult, mostUsedKeywords);
            model.addAttribute("dashBoardView", mostUsedKeywords);
            model.addAttribute("keywordSuccess", true);
        } catch (Exception e) {
            logger.error("Exception: Navigating tool result page after login successfully" + e);
        }
        return FORM_VIEW;
    }

    /* Navigation to tool page for invalid URL.*/
    @RequestMapping(params = {"requestMethod=invalidQueryURL"}, method = RequestMethod.POST)
    public String navigationToToolPageForInvalid(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {
        try {
            String mostKeywordsInvalidUrl = messageSource.getMessage("URL.error", null, Locale.US);
            session.setAttribute("mostKeywordsInvalidUrl", mostKeywordsInvalidUrl);
            model.addAttribute("mostKeywordsInvalidUrl", mostKeywordsInvalidUrl);
            model.addAttribute("keywordSuccess", false);
//            session.removeAttribute("mostUsedKeywordsTool");
            session.removeAttribute("mostUsedKeywordResult");
            session.removeAttribute("mostUsedKeywordsCrawlingULrs");
            session.removeAttribute("mostKeywordsFinishedUrls");
            session.removeAttribute("keywordsLinkDepth");
        } catch (Exception e) {
            logger.error("Exception: Navigating to tool page for invalid query URL", e);
        }
        return FORM_VIEW;
    }

    /* Navigation to tool page reseting the input fields and clearing all the session objects. */
    @RequestMapping(params = {"requestMethod=clearAll"}, method = RequestMethod.POST)
    public String removingSessionObjects(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {
        /*Remvoing all used session objects for new report/result/analysis.*/
        session.removeAttribute("mostKeywordsInvalidUrl");
        session.removeAttribute("keywordsUrlCrawlingStatus");
        session.removeAttribute("mostUsedKeywordsCrawlingULrs");
        session.removeAttribute("validMostUsedKeywordsUrl");
        session.removeAttribute("mostUsedKeywordResult");
        session.removeAttribute("keywordsLinkDepth");
        session.removeAttribute("mostUsedKeywordsReport");
        session.removeAttribute("dashBoardResult");
        model.addAttribute("keywordSuccess", false);
        model.addAttribute("clearAll", "clearAll");
        return FORM_VIEW;
    }

    /*
     * @return : returning most used keywords tool report in excell format.
     */
    @RequestMapping(params = {"download=download"}, method = RequestMethod.POST)
    public String generatingMostKeywordsExcelReport(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model) {
        //logger.info("In download Reports request of ");
        user = (Login) session.getAttribute("user");
        try {
            String dwnfileName = null;
            String params = request.getParameter("report-type");
            if (params.equalsIgnoreCase("xls") && session.getAttribute("mostUsedKeywordsXLSReport") == null) {
                dwnfileName = mostUsedKeywordService.createMUSDKeywordsXLSFile(downloadFolder, session, user);
                /*session.setAttribute("mostUsedKeywordsXLSReport", dwnfileName);*/
            } else if (params.equalsIgnoreCase("pdf") && session.getAttribute("mostUsedKeywordsPDFReport") == null) {
                dwnfileName = mostUsedKeywordService.createMUSDKeywordsPDFFile(downloadFolder, session, user);
                /*session.setAttribute("mostUsedKeywordsPDFReport", dwnfileName);*/
            }
            /*else if (params.equalsIgnoreCase("xls") && session.getAttribute("mostUsedKeywordsXLSReport") != null) {
                dwnfileName = (String) session.getAttribute("mostUsedKeywordsXLSReport");
            } else if (params.equalsIgnoreCase("pdf") && session.getAttribute("mostUsedKeywordsPDFReport") != null) {
                dwnfileName = (String) session.getAttribute("mostUsedKeywordsPDFReport");
            }*/
            logger.info("Downloadble Most Used Keyword report: " + dwnfileName);
            if (dwnfileName != null && !dwnfileName.trim().equals("")) {
                Common.downloadReport(response, downloadFolder, dwnfileName, params);
            }
            model.addAttribute("status", "success");
            model.addAttribute("keywordSuccess", true);
            mostUsedKeywords.setUserDomain((String) session.getAttribute("validMostUsedKeywordsUrl"));
        } catch (BadElementException | IOException e) {
            logger.error("Exception:Download reports request: ", e);
        }
        return FORM_VIEW;
    }

    /*
     * Preparing most used keywords tool report to send mail to user.
     */
    @RequestMapping(params = {"download=sendmail"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] sendMailMostKeywordsExcelReport(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model,
            @RequestParam("email") String userEmail) {
        user = (Login) session.getAttribute("user");
        boolean mailStatus = false;
        Object[] responseData = new Object[1];
        try {
            String dwnfileName = null;
            if (session.getAttribute("mostUsedKeywordsReport") != null) {
                dwnfileName = (String) session.getAttribute("mostUsedKeywordsReport");
            } else {
                dwnfileName = mostUsedKeywordService.createMUSDKeywordsPDFFile(downloadFolder, session, user);
                session.setAttribute("mostUsedKeywordsReport", dwnfileName);
            }
            if (dwnfileName != null) {
                logger.info("Sending report through mail for user");
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, dwnfileName, userEmail, "Most Used Keywords");
                if (!mailStatus) {
                    logger.info("Sending report through mail for user email: " + userEmail);
                }
            }
            responseData[0] = mailStatus;
        } catch (Exception e) {
            logger.error("Exception:Send report to email request: ", e);
        }
        return responseData;
    }

    /*
     * Preparing ASK the expert analysis for user. 
     */
    @RequestMapping(params = {"requestMethod=keyordsATE"}, method = RequestMethod.POST)
    public void processATEofMostUsedKeywords(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("mostUsedKeywords") MostUsedKeywords mostUsedKeywords, HttpSession session, Model model,
            @RequestParam("userATEDomain") String userATEDomain) throws JSONException {
        Session userSession = (Session) session.getAttribute("userSession");
        user = (Login) session.getAttribute("user");
        String toolIssues = "", questionInfo = "";
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        questionInfo = "Need help in fixing following issues with my website '" + userATEDomain + "'.\n\n";
        questionInfo += "- " + "Auditing on-page SEO factors for my website." + "\n";
        toolIssues += "Want to get full audit of on-page SEO factors for your website?";
        lxrmAskExpert.setDomain(userATEDomain.trim());
        if (userInputJson != null) {
            lxrmAskExpert.setOtherInputs(userInputJson.toString());
        }
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.MOST_USED_KEYWORDS);
        lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                if the request is not from  mail then we should add this input to the session object
        if (userInputJson != null) {
            String toolUrl = "/most-used-keywords-tool.html";
            userSession.addUserAnalysisInfo(user.getId(), 34, userATEDomain, toolIssues,
                    questionInfo, userInputJson.toString(), toolUrl);
            userInputJson = null;
        }
    }
}
