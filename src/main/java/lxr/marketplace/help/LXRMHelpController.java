/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.help;

import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anil
 */
@Controller
@RequestMapping("/")
public class LXRMHelpController {

    private static Logger logger = Logger.getLogger(LXRMHelpController.class);

    @Autowired
    private LXRMHelpService lxrmHelpService;
    @Autowired
    private SignupService signupService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private String supportMail;
    @Autowired
    private String productSupport;

    @RequestMapping(value = "lxrm-help.html", method = RequestMethod.GET)
    public @ResponseBody
    Object[] help(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("lxrmHelp") LXRMHelp lxrmHelp,
            HttpSession session, @RequestParam(defaultValue = "") String userName,
            @RequestParam(defaultValue = "") String email, @RequestParam("description") String description,
            @RequestParam("pageUrl") String pageUrl) {
        Object[] data = new Object[1];
        Login user = null;
        long userId = 0;
        user = (Login) session.getAttribute("user");
        userId = user.getId();
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn) {
            email = user.getUserName();
            userId = user.getId();
        } else {
            Signup existingUser = signupService.caseInsensitiveFind(email);
            if (existingUser != null) {
                userId = existingUser.getId();
            } else {
                if (userId <= 0) {
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    user = (Login) session.getAttribute("user");
                    userId = user.getId();
                }
                if (user.getActivationDate() == null) {
                    Calendar cal = Calendar.getInstance();
                    user.setActivationDate(cal);
                    loginService.updateActDate(userId, new Timestamp(cal.getTimeInMillis()),1);
                    String subject = EmailBodyCreator.subjectOnWelcome;
                    String bodyText = EmailBodyCreator.bodyForSendMailOnActivation(email);
                    SendMail.sendMail(supportMail, email, subject, bodyText);
                }
                String guestUserEmail = "GuestUser" + userId + "<" + email + ">";
                loginService.updateEmail(request, userId, guestUserEmail);
            }
            user.setUserName(email);
            session.setAttribute("user", user);
        }

        lxrmHelpService.insertHelpData(userId, email, description, pageUrl);

        String subject = EmailBodyCreator.subjectForHelpSubmitingTeam;
        String displayTxt = EmailBodyCreator.bodyForHelpSubmitingTeam(userName, email, description, pageUrl);
        SendMail.sendMail(supportMail, supportMail, productSupport, subject, displayTxt);

        String teamSubject = EmailBodyCreator.subjectForHelpSubmitingUser;
        String teamDisplayTxt = EmailBodyCreator.bodyForHelpSubmitingUser(userName);
        SendMail.sendMail(supportMail, email, teamSubject, teamDisplayTxt);
        data[0] = "success";
        return data;
    }
}
