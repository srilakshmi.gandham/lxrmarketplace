/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.help;

import java.sql.Timestamp;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class LXRMHelpService {
    private static Logger logger = Logger.getLogger(LXRMHelpService.class);
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public void insertHelpData(long userId, String email, String helpDesc, String pageUrl) {
        final String query = "insert into lxrm_help(user_id, email,help_text,page_url,date_time) values (?,?,?,?,?) ";
        Timestamp creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        logger.info("Inserting help text "+ query);
        try {
            synchronized (this) {
                Object[] args = {userId, email, helpDesc, pageUrl, creationTime};
                this.jdbcTemplate.update(query, args);
                logger.info("lxrm_help inserted; userId: " + userId + " pageUrl: " + pageUrl);
            }
        } catch (Exception ex) {
            logger.error("Exception in inserting helpdata",ex);
        }
    }
}
