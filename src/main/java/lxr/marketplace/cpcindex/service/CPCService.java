package lxr.marketplace.cpcindex.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class CPCService extends JdbcDaoSupport{
	
	private double cpc;
	private String date;
	private double adwordsWtCpc;
	private double bingWtCpc;
	private byte count;
	
	
	public byte getCount() {
		return count;
	}

	public void setCount(byte count) {
		this.count = count;
	}

	public double getAdwordsWtCpc() {
		return adwordsWtCpc;
	}

	public void setAdwordsWtCpc(double adwordsWtCpc) {
		this.adwordsWtCpc = adwordsWtCpc;
	}

	public double getBingWtCpc() {
		return bingWtCpc;
	}

	public void setBingWtCpc(double bingWtCpc) {
		this.bingWtCpc = bingWtCpc;
	}

	public double getCpc() {
		return cpc;
	}

	public void setCpc(double cpc) {
		this.cpc = cpc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	String query = null;
/*
 * This method returns the google weighted cpc for the specific date 
 * @date : expects the date
 */
	    public CPCService getAdwordsWtCpc(String date) {
	       // query = "select adwords_wtcpc, date from lxr_mkpl_cpc_index where date = ?";
	        query = "select date,adwords_wtcpc from lxr_mkpl_cpc_index where date = (select max(date) from lxr_mkpl_cpc_index where adwords_wtcpc is not null)";
	        
	        try {
	        	CPCService cpcService = (CPCService) getJdbcTemplate().queryForObject(query, new Object[] {},
	                new RowMapper() {
	        		//int i=0;
	        		CPCService cpcSer;
	                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	                    	cpcSer = new CPCService();
	                    	cpcSer.setCpc(rs.getDouble(2));
	                    	cpcSer.setDate(rs.getString(1));
	                    	//cpcSer.setCount((byte)i++);
	                        return cpcSer;
	                    }
	            });
	            return cpcService;
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	        return null;
	    }

	    /*
	     * returns the data in the form of CPCSevice object to build the chart
	     */
	    public List<CPCService> getXmlData(String fromDate,String toDate) {
	        query = "select adwords_wtcpc,bing_wtcpc,date  from lxr_mkpl_cpc_index where date between ? and ? ";
	        try {
	        	List<CPCService> cpcService = getJdbcTemplate().query(query, new Object[]{fromDate,toDate}, new RowMapper<CPCService>() {
	                    public CPCService mapRow(ResultSet rs, int rowNum) throws SQLException {
	                    	CPCService cpcSer = new CPCService();
	                    	cpcSer.setAdwordsWtCpc(rs.getDouble(1));
	                    	cpcSer.setBingWtCpc(rs.getDouble(2));
	                    	cpcSer.setDate(rs.getString(3));
	                        return cpcSer;
	                    }
	            });
	            return cpcService;
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	        return null;
	    }
	    
	    /*
	     * Returns the xml in the form of String for Fussion charts
	     */
	    public StringBuilder getXml(List<CPCService> cpcXmlData){
	    	try{
	    	//int max = cpcXmlData.size();
	    	StringBuilder finalxml = new StringBuilder();
	    	StringBuilder catxml = new StringBuilder();
	    	StringBuilder adwordsxml = new StringBuilder();
	    	StringBuilder bingxml = new StringBuilder();
	    	//finalxml.append("<graph caption='Adwords CPC v/s Bing CPC' divlinecolor='F47E00' numdivlines='4' showAreaBorder='1' areaBorderColor='000000' numberPrefix='$' showNames='1' numVDivLines='29' vDivLineAlpha='30' formatNumberScale='1' rotateNames='1' decimalPrecision='2' yAxisMinvalue='0.8'>");
	    	finalxml.append("<graph caption='NetElixir Holiday Retail CPC Index'  hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='"+(cpcXmlData.size()-2)+"' yaxisminvalue='0.8'  rotateNames='1'>");
	    	adwordsxml.append("<dataset seriesname='Adwords CPC' color='FF5904' showValues='0' areaAlpha='50' showAreaBorder='1' areaBorderThickness='2' areaBorderColor='FF0000'>");
	    	bingxml.append("<dataset seriesname='Bing CPC' color='3F99E8' showValues='0' areaAlpha='50' showAreaBorder='1' areaBorderThickness='2' areaBorderColor='006600'>");
	    	catxml.append("<categories>");
	    	Date frDate = null;
	    	SimpleDateFormat sdf = null;
	    	SimpleDateFormat sdfDestination = null;
	    	double adword_wtcpc = 0.0;
	    	double bing_wtcpc = 0.0;
	    	for(CPCService cpcservice :cpcXmlData){
	    		sdf = new SimpleDateFormat("yyyy-MM-dd");
				frDate = sdf.parse(cpcservice.getDate());	    		
				sdfDestination = new SimpleDateFormat("MM/dd/yyyy");
				//String fromdate = sdfDestination.format(frDate);
	    		catxml.append("<category name='"+sdfDestination.format(frDate)+"'/>");
	    		adword_wtcpc = cpcservice.getAdwordsWtCpc();
	    		bing_wtcpc = cpcservice.getBingWtCpc();
	    		if(adword_wtcpc>0){
	    			adwordsxml.append("<set value ='"+adword_wtcpc+"'/>");
		    		bingxml.append("<set value ='"+bing_wtcpc+"'/>");
	    		}else{
	    		adwordsxml.append("<set value =''/>");
	    		bingxml.append("<set value =''/>");
	    		}
	    
	    	}
	    	adwordsxml.append("</dataset>");
	    	bingxml.append("</dataset>");
	    	catxml.append("</categories>");
	    	return finalxml.append(catxml).append(adwordsxml).append(bingxml).append("</graph>");
	    	//return adwordsxml.append(bingxml).toString();
	    }catch(Exception ex){
	    	ex.printStackTrace();
	    	return null;
	    }
}
	     public int updateData(String query) {
	         try {
	             return getJdbcTemplate().update(query);
	         } catch (Exception ex) {
	             ex.printStackTrace();
	         }
	         return 0;
	     }
}
