package lxr.marketplace.cpcindex.controller;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.cpcindex.service.CPCService;
import lxr.marketplace.user.Login;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

public class CPCController extends SimpleFormController{
	
	private static Logger logger = Logger.getLogger(CPCController.class);
	CPCService cpcService; 
	
	public CPCService getCpcService() {
		return cpcService;
	}
	
	public void setCpcService(CPCService cpcService) {
		this.cpcService = cpcService;
	}
	
	public CPCController() {
		setCommandClass(CPCService.class);
		setCommandName("cpc");
	}
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		HttpSession session = request.getSession(true);
		CPCService cpcServiceCmd = (CPCService) command;
		CPCService CPCSer = (CPCService)session.getAttribute("googleCPC");
		
		ModelAndView mAndV = null;
		
	//	Calendar cal = Calendar.getInstance(); 
	//	cal.add(Calendar.HOUR,-5);
      //  Date d = new Date(cal.getTime().getTime());
    //    String toDay = new SimpleDateFormat("yyyy-MM-dd").format(d);
    //    String toDate = new SimpleDateFormat("MM/dd/yyyy").format(d);
    //    String dispFormat = new SimpleDateFormat("MMM d,  yyyy").format(d);
        
        Login user = (Login) session.getAttribute("user");
        
		if(WebUtils.hasSubmitParameter(request,"loadCharts")){
			if(user!=null){	
			logger.info("loading Google wt Charts started.......");
			StringBuilder xmlData = new StringBuilder();
			List<CPCService> cpcXmlData = null;
			cpcXmlData = cpcService.getXmlData("2011-11-25", "2011-12-25");
			xmlData = cpcService.getXml(cpcXmlData);
			session.setAttribute("cpcCompXmlData", xmlData);
			session.setAttribute("fromDate", "11/25/2011");
			//session.setAttribute("toDate", toDate);
			session.setAttribute("toDate", "12/25/2011");
			mAndV = new ModelAndView("CPCIndex/CPCIndexChart", "CPC", cpcServiceCmd);		
			return mAndV;
			}else{
				mAndV = new ModelAndView("CPCIndex/CPCIndexChartDesc", "CPC", cpcServiceCmd);		
				return mAndV;
			}
		}else if(WebUtils.hasSubmitParameter(request,"loadChartsDesc")){
			mAndV = new ModelAndView("CPCIndex/CPCIndexChartDesc", "CPC", cpcServiceCmd);		
			return mAndV;
		}else if (WebUtils.hasSubmitParameter(request,"insertData")){			
			mAndV = new ModelAndView("CPCIndex/LXRMpWtdCPC", "CPC", cpcServiceCmd);		
			return mAndV;
		}
		else{
			if(CPCSer == null){
			logger.info("loading Google wt cpc Started.......");
			Date frDate = null;
			CPCSer = cpcService.getAdwordsWtCpc("");
			if(CPCSer!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			frDate = sdf.parse(CPCSer.getDate());			
			SimpleDateFormat sdfDestination = new SimpleDateFormat("MMM d,  yyyy");
			String dispFormat = sdfDestination.format(frDate);	
			CPCSer.setDate(dispFormat);
			}
			session.setAttribute("googleCPC", CPCSer);
			logger.info("loading Google wt cpc Completed.......");
			mAndV = new ModelAndView(getFormView(), "CPC", cpcServiceCmd);		
			return mAndV;
		}else{
			mAndV = new ModelAndView(getFormView(), "CPC", cpcServiceCmd);		
			return mAndV;
		}
		}
}
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}
}
