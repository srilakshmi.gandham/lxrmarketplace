/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.captchaVerification;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author NE16T1213/Sagar.K
 */
@Service
public class RecaptchaVerification {

    private static final Logger LOGGER = Logger.getLogger(RecaptchaVerification.class);

    @Value("${google.captcha.api.endpoint}")
    private String verificationurl;
    @Value("${google.captcha.api.key}")
    private String secretkey;
    @Value("${lxrm.user.agent}")
    private String userAgent;

    public boolean verifyCaptcha(String gRecaptchaResponse) {
        boolean isCaptchaVeirfied = Boolean.FALSE;
        try {
            if (gRecaptchaResponse != null && !gRecaptchaResponse.trim().isEmpty()) {
                URL urlObj = new URL(verificationurl);
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) urlObj.openConnection();
                // add reuqest header
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setRequestProperty("User-Agent", userAgent);
                httpsURLConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                String postParams = "secret=" + secretkey + "&response=" + gRecaptchaResponse;
                // Send post request
                httpsURLConnection.setDoOutput(Boolean.TRUE);
                try (DataOutputStream dataOutputStream = new DataOutputStream(httpsURLConnection.getOutputStream())) {
                    dataOutputStream.writeBytes(postParams);
                    dataOutputStream.flush();
                }
                isCaptchaVeirfied = (httpsURLConnection.getResponseCode() == 200);
                LOGGER.info("HttpsURLConnection ResponseCode:: " + httpsURLConnection.getResponseCode());
            }
        } catch (IOException e) {
            LOGGER.info("The gRecaptchaResponse:: "+gRecaptchaResponse);
            LOGGER.error("IOException in verifyCaptcha cause::",e);
        }
        return isCaptchaVeirfied;
    }
}
