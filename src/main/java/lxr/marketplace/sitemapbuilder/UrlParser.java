package lxr.marketplace.sitemapbuilder;

import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;
import lxr.marketplace.robots.crawler.BaseRobotRules;
import org.apache.log4j.Logger;

public class UrlParser implements Runnable {

    private static Logger logger = Logger.getLogger(UrlParser.class);
    
    private final Set<SitemapUrl> s2;
    private final SitemapUrl tempUrl1;
    private ThreadPoolExecutor sitemapthreadPool = null;
    private long[] linkdepth = null;
    private final int limit = 50000;
    private BaseRobotRules baseRobotRules = null;
    public UrlParser(Set<SitemapUrl> s2, SitemapUrl tempUrl1, ThreadPoolExecutor sitemapthreadPool, long[] linkdepth,BaseRobotRules baseRobotRules) {
        super();
        this.s2 = s2;
        this.tempUrl1 = tempUrl1;
        this.sitemapthreadPool = sitemapthreadPool;
        this.linkdepth = linkdepth;
        this.baseRobotRules = baseRobotRules;
    }

    @Override
    public void run() {
        try {

            SiteMapBuilderService smbservice = new SiteMapBuilderService();
            if (!tempUrl1.getSitemapUrl().endsWith(".pdf")) {
                Set<SitemapUrl> primaryUrls = smbservice.getUrlsList(tempUrl1,baseRobotRules);
                if (primaryUrls != null && primaryUrls.size() > 0) {
                    for (SitemapUrl secondaryurl : primaryUrls) {
                        if (secondaryurl.getSitemapUrl() != null
                                && !secondaryurl.getSitemapUrl().trim().equals("")
                                && !secondaryurl.getSitemapUrl().trim().equals("&nbsp;")) {
                            boolean found = false;
                            for (SitemapUrl secondaryurl1 : s2) {
                                if (secondaryurl1.getSitemapUrl() != null
                                        && !secondaryurl1.getSitemapUrl().trim().equals("")
                                        && !secondaryurl1.getSitemapUrl().trim().equals("&nbsp;")) {
                                    if (secondaryurl1.getSitemapUrl().equals(secondaryurl.getSitemapUrl())) {
//                                        logger.info("secondaryurl1: "+secondaryurl1.getSitemapUrl());
//                                        logger.info("secondaryurl: "+secondaryurl.getSitemapUrl());
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (!found) {
                                synchronized (s2) {
                                    if (s2.size() < limit) {
                                        if (secondaryurl.getSitemapUrl() != null
                                                && !secondaryurl.getSitemapUrl().trim().equals("")
                                                && !secondaryurl.getSitemapUrl().trim().equals("&nbsp;")) {
                                            s2.add(secondaryurl);

                                            UrlParser up = new UrlParser(s2, secondaryurl, sitemapthreadPool, linkdepth,baseRobotRules);
                                            if (linkdepth[0] < secondaryurl.getDepthId()) {
                                                linkdepth[0] = secondaryurl.getDepthId();
                                            }
                                            sitemapthreadPool.execute(up);
                                            s2.notifyAll();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception in crawling URL, selected from SitemapUrl Set : ", e);
        }
    }

}
