package lxr.marketplace.sitemapbuilder;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails;
import lxr.marketplace.robots.crawler.BaseRobotRules;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.json.JSONObject;
import org.springframework.context.MessageSource;

@SuppressWarnings("deprecation")
public class SiteMapBuilderToolController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(SiteMapBuilderToolController.class);

    private LxrmAskExpertService lxrmAskExpertService;
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    LoginService loginService;
    private ThreadPoolExecutor sitemapconPool = null;
    boolean terminated = false;
    String downloadFolder;
    long usrdt = Calendar.getInstance().getTimeInMillis();
    private final LinkedBlockingQueue<Runnable> sitemapconQue = new LinkedBlockingQueue<>();
    private String supportMail;

    JSONObject userInputJson = null;

    private MessageSource messageSource;

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public LxrmAskExpertService getLxrmAskExpertService() {
        return lxrmAskExpertService;
    }

    public void setLxrmAskExpertService(LxrmAskExpertService lxrmAskExpertService) {
        this.lxrmAskExpertService = lxrmAskExpertService;
    }

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public SiteMapBuilderToolController() {
        setCommandClass(SiteMapBuilderTool.class);
        setCommandName("siteMapBuilderTool");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        String dwnfileName = "";
        String downloadFilePath;
        String fileName;
        String sourceSiteMapPath;
        JSONArray arr = new JSONArray();
        PrintWriter writer;
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        String userType = null;
        boolean userLoggedIn = false;
        BaseRobotRules baseRobotRules = null;
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        }
        String sendMailId = (String) session.getAttribute("sendMailId");
        String userName = "";

        if (user != null && user.getName() != null) {
            userName = user.getName();
        }

        SiteMapBuilderTool sitemapBuilder = (SiteMapBuilderTool) command;
        if (sitemapBuilder != null) {
            session.setAttribute("sitemapBuilder", sitemapBuilder);
        }
        if (request.getRequestURL().toString().contains("LXRSEO/Tools")) {
            userType = "ninja";
        }
        if (user != null) {

            Tool toolObj = new Tool();
            toolObj.setToolId(6);
            toolObj.setToolName("SEO Sitemap Builder");
            toolObj.setToolLink("seo-sitemap-builder-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.SITE_MAP_BUILDER);

            if (userType == null || (userLoggedIn)) {
                if (WebUtils.hasSubmitParameter(request, "getResult")) {

                    SiteMapBuilderThread smbtNew = null;
                    int poolSize;
                    int maxPoolSize;
                    long keepAliveTime;
                    if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                        session.setAttribute("loginrefresh", "loginrefresh");
                        /*Making Guest user access to login user access */
                        String reqParam = request.getParameter("report");
                        try {
                            ModelAndView mAndV;
                            if ((reqParam == null)) {
                                session.removeAttribute("loginrefresh");
                                logger.debug("Making Guest user access to login user access");
                                sitemapBuilder = new SiteMapBuilderTool();
                                sitemapBuilder.setLastmod(2);
                                sitemapBuilder.setSepriority(1.0);
                                session.removeAttribute("sitemapurls");
                                if (session.getAttribute("sitemapBuilderProcessing") != null) {
                                    sitemapBuilder = (SiteMapBuilderTool) session.getAttribute("sitemapBuilderProcessing");
                                }
                                baseRobotRules = SiteMapBuilderService.fetchRobotRules("Googlebot", sitemapBuilder.getInputUrl());
                                SiteMapBuilderThread smbtEx = new SiteMapBuilderThread(sitemapBuilder, downloadFolder, userName, userLoggedIn, user.getId(), toolService, baseRobotRules);
                                session.removeAttribute("ld");//to get linkdepth            
                                session.removeAttribute("ps");//to get pages scanned.
                                session.removeAttribute("pl");// to get pages left.
                                session.removeAttribute("tp");
                                session.setAttribute("sitemapurls", smbtEx);
                                session.removeAttribute("userInstance");
                                mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                                String threadStatus = "";
                                if (session.getAttribute("userInstance") != null) {
                                    threadStatus = (String) session.getAttribute("userInstance");
                                }
                                if (threadStatus.equalsIgnoreCase("existed")) {
                                    mAndV.addObject("status", "success");
                                } else {
                                    mAndV.addObject("processStatus", false);
                                    mAndV.addObject("status", "GetResult");
                                }
                                session.removeAttribute("sitemapBuilder");
                                session.removeAttribute("sitemapBuilderProcessing");
                                return mAndV;
                            }
                        } catch (Exception e) {
                            logger.error("Exception Making Guest user access to login user access" + e.getMessage());
                        }
                        String notWorkingUrl = null;
                        notWorkingUrl = (String) session.getAttribute("notWorkingUrl");
                        if (session.getAttribute("sitemapurls") != null) {
                            smbtNew = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
                        }
                        if (smbtNew != null) {
                            session.setAttribute("ld", smbtNew.linkdepth[0]);//to get linkdepth            
                            session.setAttribute("ps", smbtNew.s2.size());//to get pages scanned.
                            session.setAttribute("pl", smbtNew.getPagesleft());// to get pages left.
                            session.setAttribute("tp", smbtNew.s2.size());
                        }
                        if (userType != null) {
                            logger.debug("userType-----------------------" + userType);
                            ModelAndView mAndV = new ModelAndView("SEONinja/Tools/SiteMapBuilderTool", "siteMapBuilderTool", sitemapBuilder);
                            if (notWorkingUrl == null) {
                                mAndV.addObject("status", "success");
                            }
                            mAndV.addObject("notWorkingUrl", notWorkingUrl);
                            mAndV.addObject("processStatus", false);
                            return mAndV;
                        } else {
                            if (sitemapBuilder == null) {
                                if (session.getAttribute("sitemapBuilderProcessing") != null) {
                                    sitemapBuilder = (SiteMapBuilderTool) session.getAttribute("sitemapBuilderProcessing");
                                }
                            }
                            ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                            if (notWorkingUrl == null) {
                                mAndV.addObject("status", "success");
                            }
                            mAndV.addObject("processStatus", false);
                            mAndV.addObject("siteMapBuilderTool", sitemapBuilder);
                            return mAndV;
                        }
                    }
                    String mainUrl = null;
                    if (sitemapBuilder != null && sitemapBuilder.getInputUrl() != null && !sitemapBuilder.getInputUrl().trim().isEmpty()) {
                        mainUrl = sitemapBuilder.getInputUrl().trim();
                    }
                    logger.debug("Site Map Report builder  for MainUrl:   " + mainUrl);
                    mainUrl = Common.getUrlValidation(mainUrl, session);
                    removeSessionObjects(session);
                    if (mainUrl != null && (mainUrl.equals("invalid") || mainUrl.equals("redirected"))) {
                        String notWorkingUrl;
                        if (userType != null) {
                            ModelAndView mAndV = new ModelAndView("SEONinja/Tools/SiteMapBuilderTool", "siteMapBuilderTool", sitemapBuilder);
                            notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                            mAndV.addObject("notWorkingUrl", notWorkingUrl);
                            session.setAttribute("notWorkingUrl", notWorkingUrl);
                            mAndV.addObject("processStatus", false);
                            return mAndV;
                        } else {
                            ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                            notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                            mAndV.addObject("notWorkingUrl", notWorkingUrl);
                            session.setAttribute("notWorkingUrl", notWorkingUrl);
                            mAndV.addObject("processStatus", false);
                            return mAndV;
                        }
                    } else if (sitemapBuilder != null) {
                        logger.info("After concating: " + mainUrl);
                        session.removeAttribute("sitemapurls");
                        sitemapBuilder.setInputUrl(mainUrl);
                        if (sitemapBuilder.getEmail() != null && !sitemapBuilder.getEmail().trim().isEmpty()) {
                            sitemapBuilder.setEmail(sitemapBuilder.getInputUrl().trim());
                        }
                        session.setAttribute("SiteMapUrl", mainUrl);
                        try {

                            SiteMapBuilderThread smbtconTemp;
                            if (userType != null) {
                                baseRobotRules = SiteMapBuilderService.fetchRobotRules("Googlebot", sitemapBuilder.getInputUrl());
                                SiteMapBuilderThread lxrseoSmbtCon = new SiteMapBuilderThread(sitemapBuilder, downloadFolder, userName, userLoggedIn, user.getId(), toolService, baseRobotRules);
                                if (session.getAttribute("lxrSeoSitemapUrls") != null) {
                                    session.removeAttribute("lxrSeoSitemapUrls");
                                }
                                session.setAttribute("lxrSeoSitemapUrls", lxrseoSmbtCon);
                                poolSize = 20;
                                maxPoolSize = 100;
                                keepAliveTime = 360;
                                terminated = false;
                                sitemapconPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, sitemapconQue);
                                smbtconTemp = lxrseoSmbtCon;
                            } else {
                                baseRobotRules = SiteMapBuilderService.fetchRobotRules("Googlebot", sitemapBuilder.getInputUrl());
                                SiteMapBuilderThread smbtcon = new SiteMapBuilderThread(sitemapBuilder, downloadFolder, userName, userLoggedIn, user.getId(), toolService, baseRobotRules);
                                if (session.getAttribute("sitemapurls") != null) {
                                    session.removeAttribute("sitemapurls");
                                }
                                session.setAttribute("sitemapurls", smbtcon);
                                poolSize = 20;
                                maxPoolSize = 100;
                                keepAliveTime = 360;
                                terminated = false;
                                smbtconTemp = smbtcon;
                                sitemapconPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, sitemapconQue);
                            }
                            sitemapconPool.execute(smbtconTemp);
                            session.setAttribute("userInstance", "existed");
                            /*Using to prevent storing the site map zip file into DB.*/
                            session.removeAttribute("isSiteMapDownload");

                            if (userLoggedIn) {
                                session.setAttribute("isSiteMapDownload", true);
                            }
                            try {
                                String toAddrs = sitemapBuilder.getEmail();
                                if (!userLoggedIn) {
                                    if (sendMailId == null) {
                                        if (user.getName() == null) {
                                            if (user.getActivationDate() == null) {
                                                Calendar cal = Calendar.getInstance();
                                                user.setActivationDate(cal);
                                                loginService.updateActDate(user.getId(), new Timestamp(cal.getTimeInMillis()), 1);
                                                String subject = EmailBodyCreator.subjectOnWelcome;
                                                String bodyText = EmailBodyCreator.bodyForSendMailOnActivation(toAddrs);
                                                SendMail.sendMail(supportMail, toAddrs, subject, bodyText);
                                            }
                                            user.setUserName(toAddrs);
                                            if (user.getId() == -1) {
                                                Common.modifyDummyUserInToolorVideoUsage(request, response);
                                                user = (Login) session.getAttribute("user");
                                            }
                                            String guestUserEmail = "GuestUser" + user.getId() + "<" + toAddrs + ">";
                                            loginService.updateEmail(request, user.getId(), guestUserEmail);
                                        }
                                        session.setAttribute("sendMailId", toAddrs);
                                    }
                                }
                                if (sendMailId != null) {
                                    if (!toAddrs.equalsIgnoreCase(sendMailId)) {
                                        session.setAttribute("siteMapMailId", toAddrs);
                                    }
                                }
                                sitemapBuilder.setEmail(toAddrs);
                            } catch (Exception e) {
                                logger.error("Exception in updating guest user mail Id", e);
                            }
                        } catch (Exception e) {
                            logger.error("Exception in getResult of SiteMapBulider Contoller", e);
                        }

                        session.setAttribute("sitemapBuilderProcessing", sitemapBuilder);

                        if (userType != null) {
                            ModelAndView mAndV = new ModelAndView("SEONinja/Tools/SiteMapBuilderTool", "siteMapBuilderTool", sitemapBuilder);
                            mAndV.addObject("processStatus", false);
                            mAndV.addObject("getResSucess", true);
                            return mAndV;
                        } else {
                            ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                            mAndV.addObject("status", "success");
                            mAndV.addObject("processStatus", false);
                            mAndV.addObject("siteMapBuilderTool", sitemapBuilder);
                            return mAndV;
                        }
                    }
                } else if (WebUtils.hasSubmitParameter(request, "download")) {
                    String fileType = "zip";
                    try {
                        String downParam = request.getParameter("download");
                        SiteMapBuilderTool sitemapBuilderDwnld;
                        String siteMapQueryURL = null;
                        if (session.getAttribute("sitemapBuilder") != null) {
                            sitemapBuilderDwnld = (SiteMapBuilderTool) session.getAttribute("sitemapBuilder");
                            siteMapQueryURL = sitemapBuilderDwnld.getInputUrl();
                        } else {
                            sitemapBuilderDwnld = new SiteMapBuilderTool();
                        }

                        if (siteMapQueryURL == null && session.getAttribute("SiteMapUrl") != null) {
                            siteMapQueryURL = (String) session.getAttribute("SiteMapUrl");
                        }

                        Timestamp createdTime;
                        if (downParam.equalsIgnoreCase("'download'")) {
                            if (session.getAttribute("downloadFileName") != null && session.getAttribute("downloadFilePath") != null) {
                                dwnfileName = (String) session.getAttribute("downloadFileName");
                                downloadFilePath = (String) session.getAttribute("downloadFilePath");
                                logger.info("User downloading SiteMap downloadFolder: " + downloadFilePath + ", dwnfileName: " + dwnfileName + ", user-ID: " + user.getId());
                                Common.downloadReport(response, downloadFilePath, dwnfileName, fileType);
                            }
                        }
                        if (session.getAttribute("isSiteMapDownload") == null && dwnfileName != null && siteMapQueryURL != null) {
                            createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                            Object[] vals = {dwnfileName, 6, user.getId(), createdTime, siteMapQueryURL};
                            toolService.insertDataInDownloadReportDetailsTab(vals);
                            session.setAttribute("isSiteMapDownload", true);
                        }

                    } catch (Exception e) {
                        logger.error("Exception while downloading SiteMap: ", e);
                    }
                } else if (WebUtils.hasSubmitParameter(request, "getdata")) {
                    writer = response.getWriter();
                    SiteMapBuilderThread smbtNew = null;
                    if (userType != null) {
                        if (session.getAttribute("lxrSeoSitemapUrls") != null) {
                            smbtNew = (SiteMapBuilderThread) session.getAttribute("lxrSeoSitemapUrls");
                        }
                    } else if (session.getAttribute("sitemapurls") != null) {
                        smbtNew = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
                    }
//                    if (smbtNew == null) {
//                        baseRobotRules = SiteMapBuilderService.fetchRobotRules("Googlebot"," ");
//                        smbtNew = new SiteMapBuilderThread(new SiteMapBuilderTool(), downloadFolder, userName, userLoggedIn, user.getId(), toolService,baseRobotRules);
//                    }
                    if (smbtNew != null) {
                        if (!(smbtNew.getSiteMapZipFile() == null)) {
                            session.setAttribute("downloadFileName", smbtNew.getSiteMapZipFile());
                        }
                        if (!(smbtNew.getSaveSiteMapLocation() == null)) {
                            session.setAttribute("downloadFilePath", smbtNew.getSaveSiteMapLocation());
                        }
                        if (smbtNew.isProcesscompleted()) {
                            Session userSession = (Session) session.getAttribute("userSession");
                            if (user.getId() == -1) {
                                Common.modifyDummyUserInToolorVideoUsage(request, response);
                                user = (Login) session.getAttribute("user");
                            }

                            boolean requestFromMail = false;
                            if (session.getAttribute("requestFromMail") != null) {
                                requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                                session.removeAttribute("requestFromMail");
                            }
                            if (!requestFromMail && user != null && !user.isAdmin()) {
                                userInputJson = new JSONObject();
                                userInputJson.put("2", sitemapBuilder.getFrequency());
                                userInputJson.put("3", sitemapBuilder.getLastmod());
                                userInputJson.put("4", sitemapBuilder.getUserdate());
                                userInputJson.put("5", sitemapBuilder.getSepriority());
                                userInputJson.put("6", sitemapBuilder.getEmail());
                            }
                            userSession.addToolUsage(toolObj.getToolId());
                        }
                    }
                    if (smbtNew != null) {
                        arr.add(smbtNew.isTerminated());//to check thread is terminated or not
                        arr.add(smbtNew.isProcesscompleted());//process of generation report completed or not
                        arr.add(smbtNew.linkdepth[0]);//to get linkdepth            
                        arr.add(smbtNew.getPagescanned());//to get pages scanned.
                        arr.add(smbtNew.getPagesleft());// to get pages left.
                        arr.add(smbtNew.s2.size());//to get totalpages.
                        writer.print(arr);
                        writer.flush();
                        writer.close();
                    }
                    return null;
                } else if (WebUtils.hasSubmitParameter(request, "prvDownloadFile")) {
                    if (userLoggedIn) {
                        logger.info("Request to download previous site map reports: " + request.getParameter("prvDownloadFile"));
                        fileName = request.getParameter("prvDownloadFile");
                        List<PaidDownloadedReportDetails> updatedPaidDownloadedReportDetails;
                        if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                            updatedPaidDownloadedReportDetails = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                            sourceSiteMapPath = SiteMapBuilderService.getPreviousDownLoadReportPath(updatedPaidDownloadedReportDetails, fileName, user.getId());
                            if (sourceSiteMapPath != null && Common.checkFileExistance(downloadFolder + sourceSiteMapPath + "/" + fileName)) {
                                Common.downloadReport(response, downloadFolder + sourceSiteMapPath + "/", fileName, "zip");
                                return null;
                            } else {
                                PaidDownloadedReportDetails reportToBeDeleted = null;
                                /*Deleting report from paid download reports and updating new list in session.*/
                                List<PaidDownloadedReportDetails> fileteredPaidReports = updatedPaidDownloadedReportDetails.stream().filter(report -> report.getReportName().equalsIgnoreCase(fileName)).collect(Collectors.toList());
                                if (fileteredPaidReports != null && !fileteredPaidReports.isEmpty()) {
                                    reportToBeDeleted = fileteredPaidReports.get(0);
                                }
                                if (reportToBeDeleted != null) {
                                    toolService.deletePaidDownladReports(reportToBeDeleted.getReportId());
                                    updatedPaidDownloadedReportDetails = toolService.fetchPaidDownladReports(user.getId(), 6);
                                    session.setAttribute("paidDownloadedReportDetailsList", updatedPaidDownloadedReportDetails);
                                }
                                ModelAndView mAndV = null;
                                SiteMapBuilderThread smbtEx;
                                if (session.getAttribute("sitemapurls") != null) {
                                    smbtEx = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
                                    if (smbtEx != null) {
                                        session.setAttribute("ld", smbtEx.linkdepth[0]);//to get linkdepth            
                                        session.setAttribute("ps", smbtEx.s2.size());//to get pages scanned.
                                        session.setAttribute("pl", smbtEx.getPagesleft());// to get pages left.
                                        session.setAttribute("tp", smbtEx.s2.size());
//                        session.setAttribute("siteMapThreadTerminated", smbtEx.isTerminated());//to check thread is terminated or not
//                        session.setAttribute("siteMapProcessCompleted", smbtEx.isProcesscompleted());
                                    }
                                    if (session.getAttribute("sitemapBuilderProcessing") != null) {
                                        SiteMapBuilderTool sessionSitemapBuilder = (SiteMapBuilderTool) session.getAttribute("sitemapBuilderProcessing");
                                        mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sessionSitemapBuilder);
                                    }
                                } else {
                                    //smbtEx = new SiteMapBuilderThread(sitemapBuilder,downloadFolder);
                                    session.removeAttribute("userInstance");
                                    sitemapBuilder = new SiteMapBuilderTool();
                                    sitemapBuilder.setLastmod(2);
                                    sitemapBuilder.setSepriority(1.0);
                                    if (userLoggedIn) {
                                        sitemapBuilder.setEmail(user.getUserName());
                                    }
                                    mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                                }
                                if (mAndV == null) {
                                    mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                                }
                                String threadStatus = "";
                                if (session.getAttribute("userInstance") != null) {
                                    threadStatus = (String) session.getAttribute("userInstance");
                                }
                                if (threadStatus.equalsIgnoreCase("existed")) {
                                    mAndV.addObject("status", "success");
                                } else {
                                    mAndV.addObject("processStatus", false);
                                    mAndV.addObject("status", "GetResult");
                                }
                                mAndV.addObject("fileExistence", "Sorry the requested file is no longer existed.");
                                return mAndV;
                            }
                        }
                    }
                } else if (WebUtils.hasSubmitParameter(request, "back")) {
                    SiteMapBuilderTool siteMapBuilderTool = (SiteMapBuilderTool) session.getAttribute("siteMapBuilder");
                    removeSessionObjects(session);
                    if (siteMapBuilderTool == null) {
                        siteMapBuilderTool = new SiteMapBuilderTool();
                        siteMapBuilderTool.setLastmod(2);
                        siteMapBuilderTool.setSepriority(1.0);
                        siteMapBuilderTool.setInputUrl("");
                    }
                    if (userType != null) {
                        ModelAndView mAndV = new ModelAndView("SEONinja/Tools/SiteMapBuilderTool", "siteMapBuilderTool", sitemapBuilder);
                        mAndV.addObject("processStatus", false);
                        return mAndV;
                    } else {
                        ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                        mAndV.addObject("processStatus", false);
                        return mAndV;
                    }

                } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                    SiteMapBuilderTool siteMapBuilderTool = (SiteMapBuilderTool) session.getAttribute("sitemapBuilder");
                    removeSessionObjects(session);
                    siteMapBuilderTool.setLastmod(2);
                    siteMapBuilderTool.setSepriority(1.0);
//                    session.removeAttribute("siteMapThreadTerminated");
//                    session.removeAttribute("siteMapProcessCompleted");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                    mAndV.addObject("clearAll", "clearAll");
                    mAndV.addObject("processStatus", false);
                    return mAndV;

                } else if (WebUtils.hasSubmitParameter(request, "limit-exceeded")) {
                    String limitParam = request.getParameter("limit-exceeded");
                    ModelAndView mAndV;
                    if (limitParam != null) {
                        session.removeAttribute("loginrefresh");
                        logger.info("Making Guest user access to login user access");
                        sitemapBuilder = new SiteMapBuilderTool();
                        sitemapBuilder.setLastmod(2);
                        sitemapBuilder.setSepriority(1.0);
                        session.removeAttribute("sitemapurls");
                        if (sitemapBuilder.getInputUrl() != null) {
                            baseRobotRules = SiteMapBuilderService.fetchRobotRules("Googlebot", sitemapBuilder.getInputUrl());
                        } else {
                            baseRobotRules = new BaseRobotRules() {
                                @Override
                                public boolean isAllowed(String url) {
                                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                }

                                @Override
                                public boolean isAllowAll() {
                                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                }

                                @Override
                                public boolean isAllowNone() {
                                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                }
                            };
                        }
                        SiteMapBuilderThread smbtEx = new SiteMapBuilderThread(sitemapBuilder, downloadFolder, userName, userLoggedIn, user.getId(), toolService, baseRobotRules);
                        session.removeAttribute("ld");//to get linkdepth            
                        session.removeAttribute("ps");//to get pages scanned.
                        session.removeAttribute("pl");// to get pages left.
                        session.removeAttribute("tp");
                        session.removeAttribute("siteMapThreadTerminated");
                        session.removeAttribute("siteMapProcessCompleted");
                        session.setAttribute("sitemapurls", smbtEx);
                        session.removeAttribute("userInstance");
                        mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                        String threadStatus = "";
                        if (session.getAttribute("userInstance") != null) {
                            threadStatus = (String) session.getAttribute("userInstance");
                        }
                        if (threadStatus.equalsIgnoreCase("existed")) {
                            mAndV.addObject("status", "success");
                        } else {
                            mAndV.addObject("processStatus", false);
                            mAndV.addObject("status", "GetResult");
                        }
                        session.removeAttribute("sitemapBuilder");
                        session.removeAttribute("sitemapBuilderProcessing");
                        return mAndV;
                    }
                } else if (WebUtils.hasSubmitParameter(request, "siteMapATE")) {
                    Session userSession = (Session) session.getAttribute("userSession");
                    /*To  Handle Ask Our Expert request*/
                    String siteMapURL = request.getParameter("siteMapURL");
                    SiteMapBuilderThread sMBThread = null;
                    if (userType != null) {
                        if (session.getAttribute("lxrSeoSitemapUrls") != null) {
                            sMBThread = (SiteMapBuilderThread) session.getAttribute("lxrSeoSitemapUrls");
                        }
                    } else if (session.getAttribute("sitemapurls") != null) {
                        sMBThread = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
                    }

                    /*Refer the Sheet Tool Based Analysis Analysis Name:Issues with SiteMap creation*/
                    String numberOfErrorUrls = "";
                    long countOfErrorUrls = 0;
                    boolean issueStatus = false;
                    if (sMBThread != null) {
                        List<SitemapUrl> txtsortUrls = SiteMapBuilderService.sortURLSBasedOnPriority(new LinkedList<>());
                        countOfErrorUrls = txtsortUrls.stream()
                                .filter((uRL -> ((uRL != null) && (!uRL.getSitemapUrl().equals(" "))
                                && (!uRL.getSitemapUrl().equals("&nbsp;"))
                                && uRL.getStatuscode() >= 400)))
                                .count();
                        if (countOfErrorUrls >= 0) {
                            issueStatus = true;
                            numberOfErrorUrls = "<span style='color:red'>" + countOfErrorUrls + "</span> "
                                    + "link's are not working properly and are not added to your website's sitemap. Search engines can not crawl and index these links.";
                        }
                    }/*End of sMBThread object not null condition*/

 /*Refer the Sheet Tool Based Analysis Analysis Name:Help in Submitting SiteMaps*/
                    String siteMapHelp = "We can help you submit your sitemap to Google and Bing.";
                    LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                    String toolIssues = "";
                    String questionInfo = "";
                    if (issueStatus) {
                        questionInfo = "Need help in fixing following issues with my website '" + siteMapURL + "'.\n\n";
                        toolIssues = "<ul>";
                        if (countOfErrorUrls > 0) {
                            questionInfo += "- Fixing missing links in sitemap.\n";
                            toolIssues += "<li>" + numberOfErrorUrls + "</li>";
                        }
                        questionInfo += "- Submitting my sitemap to Google and Bing.\n";
                        toolIssues += "<li>" + siteMapHelp + "</li>";
                        toolIssues += "</ul>";
                    }
                    lxrmAskExpert.setDomain(siteMapURL);
                    if (userInputJson != null) {
                        lxrmAskExpert.setOtherInputs(userInputJson.toString());
                    }
                    lxrmAskExpert.setQuestion(questionInfo);
                    lxrmAskExpert.setIssues(toolIssues);
                    lxrmAskExpert.setExpertInfo(questionInfo);
                    lxrmAskExpert.setToolName(toolObj.getToolName());
                    lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                    if the request is not from  mail then we should add this input to the session object
                    if (userInputJson != null) {
                        String toolUrl = "/" + toolObj.getToolLink();
                        userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), siteMapURL, toolIssues,
                                questionInfo, userInputJson.toString(), toolUrl);
                        userInputJson = null;
                    }

                    return null;
                }/*End of ATE else*/


                ModelAndView mAndV = null;
                SiteMapBuilderThread smbtEx;
                if (session.getAttribute("sitemapurls") != null) {
                    smbtEx = (SiteMapBuilderThread) session.getAttribute("sitemapurls");
                    if (smbtEx != null) {
                        session.setAttribute("ld", smbtEx.linkdepth[0]);//to get linkdepth            
                        session.setAttribute("ps", smbtEx.s2.size());//to get pages scanned.
                        session.setAttribute("pl", smbtEx.getPagesleft());// to get pages left.
                        session.setAttribute("tp", smbtEx.s2.size());
//                        session.setAttribute("siteMapThreadTerminated", smbtEx.isTerminated());//to check thread is terminated or not
//                        session.setAttribute("siteMapProcessCompleted", smbtEx.isProcesscompleted());
                    }
                    if (session.getAttribute("sitemapBuilderProcessing") != null) {
                        SiteMapBuilderTool sessionSitemapBuilder = (SiteMapBuilderTool) session.getAttribute("sitemapBuilderProcessing");
                        mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sessionSitemapBuilder);
                        mAndV.addObject("siteMapBuilderTool", sessionSitemapBuilder);
                    }
                } else {
                    //smbtEx = new SiteMapBuilderThread(sitemapBuilder,downloadFolder);
                    session.removeAttribute("userInstance");
                    sitemapBuilder = new SiteMapBuilderTool();
                    sitemapBuilder.setLastmod(2);
                    sitemapBuilder.setSepriority(1.0);
                    session.removeAttribute("notWorkingUrl");
                    if (userLoggedIn) {
                        sitemapBuilder.setEmail(user.getUserName());
                    }
                    mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                }
                if (mAndV == null) {
                    mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
                }
                String threadStatus = "";
                if (session.getAttribute("userInstance") != null) {
                    threadStatus = (String) session.getAttribute("userInstance");
                }
                if (threadStatus.equalsIgnoreCase("existed")) {
                    mAndV.addObject("status", "success");
                } else {
                    mAndV.addObject("processStatus", false);
                    session.removeAttribute("notWorkingUrl");
                    mAndV.addObject("status", "GetResult");
                }
                return mAndV;

            }
        } //user equals to null below part will execute
        if (userLoggedIn && user != null && sitemapBuilder != null) {
            sitemapBuilder.setEmail(user.getUserName());
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "siteMapBuilderTool", sitemapBuilder);
        session.removeAttribute("notWorkingUrl");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        SiteMapBuilderTool siteMapBuilderTool;
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        session.removeAttribute("paidDownloadedReportDetailsList");
        if (user != null && userLoggedIn) {
            List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = toolService.fetchPaidDownladReports(user.getId(), 6);
            session.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
        }
        if (session.getAttribute("sitemapBuilderProcessing") != null) {
            siteMapBuilderTool = (SiteMapBuilderTool) session.getAttribute("sitemapBuilderProcessing");
        } else {
            siteMapBuilderTool = (SiteMapBuilderTool) session.getAttribute("sitemapBuilder");
        }
        if (siteMapBuilderTool == null) {
            siteMapBuilderTool = new SiteMapBuilderTool();
            siteMapBuilderTool.setLastmod(2);
            siteMapBuilderTool.setSepriority(1.0);
            if (userLoggedIn && user != null) {
                siteMapBuilderTool.setEmail(user.getUserName());
            }
        }
        return siteMapBuilderTool;

    }

    public void removeSessionObjects(HttpSession session) {
        try {
            session.removeAttribute("ld");
            session.removeAttribute("ps");
            session.removeAttribute("pl");
            session.removeAttribute("tp");
            session.removeAttribute("sitemapurls");
            session.removeAttribute("loginrefresh");
            session.removeAttribute("finalUrl");
            session.removeAttribute("notWorkingUrl");
            session.removeAttribute("downloadFileName");
            session.removeAttribute("downloadFilePath");
            session.removeAttribute("isSiteMapDownload");
            session.removeAttribute("siteMapMailId");
            session.removeAttribute("siteMapBuilderTool");
            session.setAttribute("siteMapBuilderTool", null);
//            session.removeAttribute("siteMapThreadTerminated");
//            session.removeAttribute("siteMapProcessCompleted");
        } catch (Exception e) {
            logger.error("Exception in remove session objects", e);
        }
    }

}
