package lxr.marketplace.sitemapbuilder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lxr.marketplace.robots.crawler.BaseRobotRules;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ToolService;

import org.apache.log4j.Logger;

public class SiteMapBuilderThread implements Runnable {

    private static Logger logger = Logger.getLogger(SiteMapBuilderThread.class);

    public Set<SitemapUrl> s2;
    public long[] linkdepth;
    SiteMapBuilderTool smb;
    SitemapUrl smbUrl;
    String domain;
    String userName;
    private BaseRobotRules baseRobotRules = null;

    private final ToolService toolService;

    private ThreadPoolExecutor sitemapthreadPool = null;
    private final int poolSize = 15;
    private final int maxPoolSize = 25;
    private final long keepAliveTime = 1000;
    private boolean terminated = false;
    private boolean processcompleted = false;
    private final LinkedBlockingQueue<Runnable> sitemapQue = new LinkedBlockingQueue<>();
    private final int limit = 50000;
    private String siteMapZipFile;
    private final String downloadFolder;
    static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";

    private boolean isUserLoggedIn = false;
    private final long userId;
    private final static int SITEMAP_SIZE = 25000;
    private List<String> saveSiteMapFiles = null;
    private List<String> phasesSiteMapFiles = null;
    private List<SitemapUrl> sortingURLS = null;
    private String saveSiteMapLocation = null;
    int counter = 1;
    int targetSize = 0;
    int sourceSize = 0;

    public String getSaveSiteMapLocation() {
        return saveSiteMapLocation;
    }

    public void setSaveSiteMapLocation(String saveSiteMapLocation) {
        this.saveSiteMapLocation = saveSiteMapLocation;
    }
    private List<String> xmlFileList = null;

    public SiteMapBuilderThread(SiteMapBuilderTool smb, String downloadFolder, String userName, boolean isUserLoggedIn, long userId, ToolService toolService,BaseRobotRules baseRobotRules) {
        this.smb = smb;
        this.s2 = Collections.synchronizedSet(new HashSet<SitemapUrl>());
        this.smbUrl = new SitemapUrl();
        this.linkdepth = new long[1];
        this.downloadFolder = downloadFolder;
        this.userName = userName;
        this.isUserLoggedIn = isUserLoggedIn;
        this.userId = userId;
        this.toolService = toolService;
        this.baseRobotRules = baseRobotRules;
    }

    public long getPagesleft() {
        if (sitemapthreadPool != null) {
            return sitemapthreadPool.getActiveCount() + sitemapthreadPool.getQueue().size();
        }
        return 0;
    }

    public long getPagescanned() {
        if (sitemapthreadPool != null && s2 != null) {
            return s2.size() - sitemapthreadPool.getActiveCount()
                    - sitemapthreadPool.getQueue().size();
        }
        return 0;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public boolean isProcesscompleted() {
        return processcompleted;
    }

    public void setProcesscompleted(boolean processcompleted) {
        this.processcompleted = processcompleted;
    }

    public String getSiteMapZipFile() {
        return siteMapZipFile;
    }

    public void setSiteMapZipFile(String siteMapZipFile) {
        this.siteMapZipFile = siteMapZipFile;
    }

    public List<String> getSaveSiteMapFiles() {
        return saveSiteMapFiles;
    }

    public void setSaveSiteMapFiles(List<String> saveSiteMapFiles) {
        this.saveSiteMapFiles = saveSiteMapFiles;
    }

    @Override
    public void run() {
        try {

            String alink = smb.getInputUrl();
            smbUrl.setSitemapUrl(alink);
            smbUrl.setDepthId(0);
            smbUrl.setPriorityval(1.0);
            sitemapthreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, sitemapQue);
            s2.add(smbUrl);

            try {
                UrlParser up = new UrlParser(s2, smbUrl, sitemapthreadPool, linkdepth, baseRobotRules);
                sitemapthreadPool.execute(up);
            } catch (NoSuchElementException e) {
                logger.error("NoSuchElementException in ", e);
            }
            /*Collection of all SiteMap related file save location .index,.xml,.html,.err,.txt to genarte ZIp File.*/
            saveSiteMapFiles = new LinkedList<>();

            /*Creating a folder to save SiteMap related files and ZIp File.*/
            saveSiteMapLocation = SiteMapBuilderService.getSiteMapFileSaveLocation(smb.getInputUrl(), downloadFolder);

            while (!terminated) {
                try {
                    int active = sitemapthreadPool.getActiveCount();
                    int qued = sitemapthreadPool.getQueue().size();
                    targetSize = SITEMAP_SIZE * counter;
                    if (s2.size() > SITEMAP_SIZE * counter) {
                        sortingURLS = new ArrayList<>(s2);
                        sortingURLS = sortingURLS.subList(sourceSize, targetSize);
                        sortingURLS = SiteMapBuilderService.sortURLSBasedOnPriority(sortingURLS);
                        logger.info("Generating site map files for sourceSize: " + sourceSize + ", targetSize: " + targetSize + ", counter: " + counter + ", total URLS size: " + sortingURLS.size() + ", and s2 size: " + s2.size());
                        phasesSiteMapFiles = SiteMapBuilderService.genrateSiteMapFiles(saveSiteMapLocation, sortingURLS, smb, counter);
                        saveSiteMapFiles.addAll(phasesSiteMapFiles);
                        sourceSize = targetSize;/* sourceSize Value 0 to 75k. targetSize Value 25k to 10k.*/
                        counter++;/* Counter Value 1 to 4.*/
                    }
                    logger.info("active:" + active + ", qued: " + qued + ", terminated:" + terminated + ", s2.size():" + s2.size());
                    /*Checking the URL's list size isexceeds or not and active threads count else shutdown crawler*/
                    if ((active + qued) > 0 && (s2.size() <= limit)) {
                        Thread.sleep(5000);
                    } else {
                        logger.info("while shutdown:");
                        sitemapthreadPool.shutdownNow();
                        terminated = true;
                    }
                } catch (InterruptedException ex) {
                    logger.error("InterruptedException in SiteMapBuilderThread run cause: ", ex);
                } catch (Exception ex) {
                    logger.error("Exception in SiteMapBuilderThread run cause: ", ex);
                }
            }
            logger.info("ShutDown the crawling thread and NoOf Site Map URLS: " + s2.size() + ", and limit: " + limit);
            /*Checking for all site map urls is written into files or not.
            Writting the remaning  sitemap URLS into files.*/
            if (sourceSize != s2.size()) {
                sortingURLS = new ArrayList<>(s2);
                sortingURLS = sortingURLS.subList(sourceSize, s2.size());
                sortingURLS = SiteMapBuilderService.sortURLSBasedOnPriority(sortingURLS);
                logger.info("Generating site map files for remaining sitemap  URL's from sourceSize: " + sourceSize + ", s2.size(): " + s2.size() + ", total Sorting URLS: " + sortingURLS.size() + " and prepared sitemap files count:" + saveSiteMapFiles.size());
                if (counter == 1) {
                    counter = 0;
                }
                if (sortingURLS != null) {
                    phasesSiteMapFiles = SiteMapBuilderService.genrateSiteMapFiles(saveSiteMapLocation, sortingURLS, smb, counter);
                }
                saveSiteMapFiles.addAll(phasesSiteMapFiles);
            }
            /*SiteMap_index will generate for above above 25,000 urls.*/
            String filePath = null;
            if (counter >= 2) {
                /*collecting the list of  multiple SiteMap.xml files and preparing sitemap_index.xml.*/
                xmlFileList = saveSiteMapFiles.stream().filter(fileName -> fileName.contains(".xml")).collect(Collectors.toList());
                filePath = BuildSiteMapXML.generateSiteMapIndex(smb.getInputUrl(), xmlFileList, saveSiteMapLocation);
                if (filePath != null && Common.checkFileExistance(saveSiteMapLocation + filePath)) {
                    saveSiteMapFiles.add(filePath);
                }
            }

            /*Constructing Site Map Error File.*/
            filePath = null;
            long errorURLSCount = sortingURLS.stream().filter(errorSortURL -> ((errorSortURL.getSitemapUrl() != null) && (!errorSortURL.getSitemapUrl().equals(" ")) && (!errorSortURL.getSitemapUrl().equals("&nbsp;")) && (errorSortURL.getStatuscode() >= 400))).count();
            if (errorURLSCount != 0) {
                filePath = new BuildSiteMapErrorTextFiles(saveSiteMapLocation).createSiteMapTxtError("sitemap", sortingURLS, 0, ".err");
                logger.info("Total error urls found:: "+errorURLSCount+", and generated file path:: "+filePath);
                if (Common.checkFileExistance(saveSiteMapLocation + filePath)) {
                    saveSiteMapFiles.add(filePath);
                }
            }

            /*Constructing Site Map ReadMe.txt File.*/
            filePath = null;
            if (xmlFileList == null) {
                filePath = new BuildSiteMapErrorTextFiles(saveSiteMapLocation).createReadMeFile(saveSiteMapLocation, smb.getInputUrl(), true);
            } else {
                filePath = new BuildSiteMapErrorTextFiles(saveSiteMapLocation).createReadMeFile(saveSiteMapLocation, smb.getInputUrl(), xmlFileList.isEmpty());
            }

            if (Common.checkFileExistance(saveSiteMapLocation + filePath)) {
                saveSiteMapFiles.add(filePath);
            }


            /*Creating Site Map Zip File for all SiteMap.xml,SiteMap.html,SiteMap.err,SiteMap.txt,*/
            siteMapZipFile = SiteMapBuilderService.generateSiteMapZipFile(smb.getInputUrl(), saveSiteMapLocation, saveSiteMapFiles);

            logger.info("Preparing SiteMapZipFile is Completed, location: " + saveSiteMapLocation + ", siteMapZipFile: " + siteMapZipFile + ", Query URL: " + smb.getInputUrl());
            if (isUserLoggedIn) {
                /*Saving site map zip file path.*/
                try {
                    Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                    Object[] vals = {siteMapZipFile, 6, userId, createdTime, smb.getInputUrl()};
                    toolService.insertDataInDownloadReportDetailsTab(vals);
                } catch (Exception e) {
                    logger.error("Saving the site map zip file casus Exception:", e);
                }
            }
            if (userName.equals("")) {
                userName = smb.getEmail().split("@")[0];
            }
            try {
                Common.sendSiteMapReportThroughMail(saveSiteMapLocation, siteMapZipFile, smb.getEmail().trim(), "SEO Sitemap Builder", userName);
                /*logger.info("Mail sent to Site Map Query URL: " + smb.getInputUrl());*/
            } catch (Exception e) {
                logger.error("Exeption in sending mail of sitemap tool zip file for input url is failed: ", e);
            }
            setSaveSiteMapLocation(saveSiteMapLocation);
            processcompleted = true;
            logger.info("Reached END and processcompleted:: "+processcompleted+", for URL:: "+smb.getInputUrl());
        } catch (Exception ex) {
            logger.error("Exception in run block", ex);
        }

    }

}
