package lxr.marketplace.sitemapbuilder;

public class SiteMapBuilderTool {
	private String inputUrl;
	private int frequency;
	private int lastmod;
	private String userdate;
	private double sepriority;
        private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        
        
	
	
	public int getLastmod() {
		return lastmod;
	}

	public void setLastmod(int lastmod) {
		this.lastmod = lastmod;
	}

	public double getSepriority() {
		return sepriority;
	}

	public void setSepriority(double sepriority) {
		this.sepriority = sepriority;
	}

	public String getUserdate() {
		return userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

	public String getInputUrl() {
		return inputUrl;
	}

	public void setInputUrl(String inputUrl) {
		this.inputUrl = inputUrl;
	}

	public int getFrequency() {
			return frequency;
		}

		public void setFrequency(int frequency) {
			this.frequency = frequency;
		}

}