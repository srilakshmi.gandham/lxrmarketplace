package lxr.marketplace.sitemapbuilder;

import java.io.IOException;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.log4j.Logger;
//import crawlercommons.robots.BaseRobotRules;
//import crawlercommons.robots.SimpleRobotRulesParser;
import lxr.marketplace.robots.crawler.SimpleRobotRulesParser;
import lxr.marketplace.robots.crawler.BaseRobotRules;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails;
import lxr.marketplace.util.Common;

public class SiteMapBuilderService {

    private static Logger logger = Logger.getLogger(SiteMapBuilderService.class);
    //"Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)"
    static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";

    private static final String FAKE_ROBOTS_URL = "http://domain.com";

    public Set<SitemapUrl> getUrlsList(SitemapUrl smbUrl, BaseRobotRules baseRobotRules) {
        URL url1 = null;
        Document doc = null;
        Document iframedoc2;
        Elements links = null;
        Elements iframe = null;
        Elements frameelemts = null;
        String iframeurl;
        String iframeurl1;
        String iframeurl2;
        String frameurl = "";
        String frameurl1 = "";
        String frameurl2 = "";
        double prival = 0;
        double prival2 = 0;
        double prival3 = 0;
        double changedpv = 0;
        double frameprival = 0;
        double changedpvframe = 0;
        double changedpvframeanc = 0;
        double changedframeprival1 = 0;

        Set<SitemapUrl> totlinks = new HashSet<>();
        try {
            DecimalFormat twoDForm = new DecimalFormat("#.#");
            url1 = new URL(smbUrl.getSitemapUrl());
            String domain = url1.getProtocol() + "://" + url1.getHost().toLowerCase();
            try {
                HttpURLConnection currentUrlConn = null;
                URL currentUrlObj = new URL(smbUrl.getSitemapUrl());
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(false);
                currentUrlConn.setConnectTimeout(200000);
                currentUrlConn.setReadTimeout(200000);
                currentUrlConn.setRequestProperty("User-Agent", USER_AGENT);
                smbUrl.setStatuscode(currentUrlConn.getResponseCode());
                smbUrl.setStatus(currentUrlConn.getResponseMessage());
            } catch (Exception e) {
                smbUrl.setStatuscode(500);
                smbUrl.setStatus("Internal Server Error.");
            }
            if (smbUrl.getStatuscode() < 400) {
                try {
                    doc = Jsoup.connect(smbUrl.getSitemapUrl()).get();
                } catch (Exception e) {
                    StringBuilder pageHTML = new StringBuilder("");
                    StringBuilder redirectedUrl = new StringBuilder("");
                    String url = smbUrl.getSitemapUrl();
                    //                checkCurrentURLAndGetHTML(url, pageHTML);
                    Common.getCurrentStatusCodeAndHTML(url, redirectedUrl, pageHTML);
                    if (!pageHTML.toString().equals("")) {
                        String httpDomainUrl = "";
                        if (url.startsWith("http://") || url.startsWith("https://")) {
                            httpDomainUrl = url;
                        } else {
                            httpDomainUrl = "http://" + url;
                        }
                        logger.debug("Fetching doc object by connecting page manully using input stream:" + httpDomainUrl);
                        doc = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
                    }
                }

                if (doc != null) {
                    links = doc.select("a");
                    iframe = doc.select("iframe");
                    frameelemts = doc.select("frame");
                }
                if (links != null) {
                    String imagePattern = "([^\\s]+(\\.(?i)(jpeg|js|jpg|png|gif|bmp))$)";
                    Pattern img = Pattern.compile(imagePattern);
                    Matcher image = null;
                    for (Element link : links) {
                        String childUrl = link.attr("href");
                        if (childUrl.startsWith("#")) {
                            childUrl = link.attr("abs:href").replace(childUrl, "");
                        } else if (!childUrl.equals("/")) {
                            childUrl = link.attr("abs:href");
                        }
                        if (childUrl.trim().toLowerCase().startsWith((domain))
                                || childUrl.trim().toLowerCase().startsWith((url1.getHost()))) {
                            SitemapUrl procUrl = new SitemapUrl();
                            procUrl.setSitemapUrl(childUrl);
                            procUrl.setDepthId(smbUrl.getDepthId() + 1);
                            double pv = smbUrl.getPriorityval();
                            if (pv != 0.0) {
                                prival2 = smbUrl.getPriorityval() - 0.2;
                                changedpv = Double
                                        .valueOf(twoDForm.format(prival2));
                            }
                            procUrl.setPriorityval(changedpv);
                            if (procUrl.getSitemapUrl() != null && !procUrl.getSitemapUrl().trim().equals("")
                                    && !procUrl.getSitemapUrl().trim().equals("&nbsp;")
                                    && (procUrl.getSitemapUrl().trim().toLowerCase().startsWith(domain)
                                    || procUrl.getSitemapUrl().trim().toLowerCase().startsWith(url1.getHost().toLowerCase()))) {
                                image = img.matcher(procUrl.getSitemapUrl().trim().toLowerCase());
                                if (!image.find()) {
                                    totlinks.add(procUrl);
                                }
                            }
                        }
                    }
                    if (iframe != null) {
                        for (Element e : iframe) {
                            String frameSrc = e.getElementsByTag("iframe").attr("src");
                            if (frameSrc.startsWith("http")) {
                                iframeurl = frameSrc;
                            } else if (!frameSrc.startsWith("http") && !frameSrc.startsWith("/")) {
                                iframeurl1 = new StringBuilder()
                                        .append(url1.getProtocol()).append("://")
                                        .append(url1.getHost()).toString()
                                        + "/";
                                iframeurl = iframeurl1 + frameSrc;
                            } else {
                                iframeurl2 = new StringBuilder()
                                        .append(url1.getProtocol()).append("://")
                                        .append(url1.getHost()).toString();
                                iframeurl = iframeurl2 + frameSrc;
                            }
                            SitemapUrl procUrl2 = new SitemapUrl();
                            procUrl2.setSitemapUrl(iframeurl);
                            procUrl2.setDepthId(smbUrl.getDepthId() + 1);
                            double pv1 = smbUrl.getPriorityval();
                            if (pv1 != 0.0) {
                                prival = smbUrl.getPriorityval() - 0.2;
                                changedpvframe = Double.valueOf(twoDForm.format(prival));
                            }
                            procUrl2.setPriorityval(prival);
                            if (procUrl2.getSitemapUrl() != null
                                    && !procUrl2.getSitemapUrl().trim().equals("")
                                    && !procUrl2.getSitemapUrl().trim().equals("&nbsp;")
                                    && (procUrl2.getSitemapUrl().trim().toLowerCase().startsWith(domain)
                                    || procUrl2.getSitemapUrl().trim().toLowerCase().startsWith(url1.getHost().toLowerCase()))) {
                                image = img.matcher(procUrl2.getSitemapUrl().trim().toLowerCase());
                                if (!image.find()) {
                                    totlinks.add(procUrl2);
                                }
                            }
                            try {
                                iframedoc2 = Jsoup.connect(iframeurl).get();
                                Elements iframeanc = iframedoc2.select("a");
                                if (iframeanc != null) {
                                    for (Element anchs : iframeanc) {
                                        String childanchs = anchs.attr("abs:href");
                                        if (!childanchs.equals("/")) {
                                            SitemapUrl procUrl3 = new SitemapUrl();
                                            procUrl3.setSitemapUrl(childanchs);
                                            procUrl3.setDepthId(smbUrl.getDepthId() + 1);
                                            double pv2 = smbUrl.getPriorityval();
                                            if (pv2 != 0.0) {
                                                prival3 = smbUrl.getPriorityval() - 0.2;
                                                changedpvframeanc = Double
                                                        .valueOf(twoDForm
                                                                .format(prival3));
                                            }
                                            procUrl3.setPriorityval(prival3);
                                            if (procUrl3.getSitemapUrl() != null && !procUrl3.getSitemapUrl().trim().equals("")
                                                    && !procUrl3.getSitemapUrl().trim().equals("&nbsp;")
                                                    && (procUrl3.getSitemapUrl().trim().toLowerCase().startsWith(domain)
                                                    || procUrl3.getSitemapUrl().trim().toLowerCase().startsWith(url1.getHost().toLowerCase()))) {
                                                image = img.matcher(procUrl3.getSitemapUrl().trim().toLowerCase());
                                                if (!image.find()) {
                                                    totlinks.add(procUrl3);
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (MalformedURLException mfe1) {
                                logger.error("MalformedURLException connecting to iframe of getUrlsList: " + mfe1.getLocalizedMessage());
                            } catch (IOException ie1) {
                                logger.error("IOException connecting to iframe of getUrlsList:" + ie1.getLocalizedMessage());
                            } catch (Exception e1) {
                                logger.error("Exception in connecting to iframe getUrlsList: " + e1.getLocalizedMessage());
                            }
                        }
                    }

                    if (frameelemts != null) {
                        for (Element frameelem : frameelemts) {
                            if (frameelem != null) {
                                String framename = frameelem.getElementsByTag("frame").attr("src");
                                if (framename.startsWith("http")) {
                                    frameurl = framename;
                                } else if (!framename.startsWith("http") && (!framename.startsWith("/"))) {
                                    frameurl1 = new StringBuilder()
                                            .append(url1.getProtocol()).append("://")
                                            .append(url1.getHost()).toString()
                                            + "/";
                                    frameurl = frameurl1 + framename;
                                } else {
                                    frameurl2 = new StringBuilder()
                                            .append(url1.getProtocol()).append("://")
                                            .append(url1.getHost()).toString();
                                    frameurl = frameurl2 + framename;
                                }
                                SitemapUrl frameprocUrl = new SitemapUrl();
                                frameprocUrl.setSitemapUrl(frameurl);
                                frameprocUrl.setDepthId(smbUrl.getDepthId() + 1);
                                double pval = smbUrl.getPriorityval();
                                if (pval != 0.0) {
                                    frameprival = smbUrl.getPriorityval() - 0.2;
                                    changedframeprival1 = Double.valueOf(twoDForm.format(frameprival));
                                }
                                frameprocUrl.setPriorityval(frameprival);

                                if (frameprocUrl.getSitemapUrl() != null && !frameprocUrl.getSitemapUrl().trim().equals("")
                                        && !frameprocUrl.getSitemapUrl().trim().equals("&nbsp;")
                                        && (frameprocUrl.getSitemapUrl().trim().toLowerCase().startsWith(domain)
                                        || frameprocUrl.getSitemapUrl().trim().toLowerCase().startsWith(url1.getHost().toLowerCase()))) {
                                    image = img.matcher(frameprocUrl.getSitemapUrl().trim().toLowerCase());
                                    if (!image.find()) {
                                        totlinks.add(frameprocUrl);
                                    }
                                }

                            }
                        }
                    }
                }
            }
        } catch (NumberFormatException | MalformedURLException mfe) {
            logger.error("MalformedURLException | NumberFormatException for getUrlsList for URL: " + smbUrl + ", cause: " + mfe.getLocalizedMessage());
        } catch (Exception e) {
            logger.error("Exception for getUrlsList for URL: " + smbUrl + ", cause: " + e.getLocalizedMessage());
        }

//        String domName = url1.getHost();
//        BaseRobotRules baseRobotRules = fetchRobotRules("Googlebot", domName);
        logger.debug("Size of totlinks bfre filtering robots: " + totlinks.size() + ", for URL: " + smbUrl.getSitemapUrl());
        if (baseRobotRules != null) {
            totlinks = filterCrawlingURLs(totlinks, baseRobotRules);
        } else {
            logger.info("Robot URLS not found for URL: " + smbUrl.getSitemapUrl());
        }
        logger.debug("Size of totlinks after filtering robots: " + totlinks.size() + ", for URL: " + smbUrl.getSitemapUrl());
        return totlinks;
    }

    public static Set<SitemapUrl> filterCrawlingURLs(Set<SitemapUrl> urls, BaseRobotRules baseRobotRules) {
        /*Filetering the robots URL's version 2: */
        String[] abondonedExts = new String[]{".pdf", ".docx", ".doc", ".png", ".jpeg", ".avi",
            ".jpg", ".xls", ".xlsx", ".csv", ".tsv", ".exe"};
        Iterator<SitemapUrl> siteMapIterator = urls.iterator();
        while (siteMapIterator.hasNext()) {
            SitemapUrl url = siteMapIterator.next();
            for (String extension : abondonedExts) {
                if ((baseRobotRules != null && !baseRobotRules.isAllowed(url.getSitemapUrl())) || url.getSitemapUrl().endsWith(extension)) {
                    siteMapIterator.remove();
                    break;
                }
            }
        }
        return urls;
        /*Filetering the robots URL's version 1: */
 /*Iterator<SitemapUrl> i = urls.iterator();
        while (i.hasNext()) {
            SitemapUrl url = i.next();
            if ((baseRobotRules != null && !baseRobotRules.isAllowed(url.getSitemapUrl()))) {
                logger.debug(url.getSitemapUrl() + "is NOT ALLOWED by ROBOTS.TXT RULES.");
                i.remove();
            }
        }
        return urls;*/
    }

    public static BaseRobotRules fetchRobotRules(String crawlerName, String domain) {
        if (!domain.startsWith("http://") && !domain.startsWith("https://")) {
            domain = "http://" + domain;
        }
        String url = domain + (domain.endsWith("/") ? "" : "/") + "robots.txt";
        try {
            URL robotURL = new URL(url);
            URLConnection connectionObj = robotURL.openConnection();
            connectionObj.setConnectTimeout(50000);
            connectionObj.setReadTimeout(30000);
            connectionObj.setRequestProperty("User-Agent", USER_AGENT);
            BufferedReader in = new BufferedReader(new InputStreamReader(connectionObj.getInputStream()));
            byte[] bytes = org.apache.tika.io.IOUtils.toByteArray(in);
            SimpleRobotRulesParser robotParser = new SimpleRobotRulesParser();
            BaseRobotRules baseRobotRules = robotParser.parseContent(FAKE_ROBOTS_URL, bytes, "text/plain", crawlerName);
            return baseRobotRules;
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException in fetching robot rules cause: ", e);
            return null;
        } catch (IOException e) {
            logger.error("IOException in fetching robot rules cause: ", e);
            return null;
        } catch (Exception e) {
            logger.error("Exception in fetching robot rules cause: ", e);
            return null;
        }
    }

    public static List<SitemapUrl> sortURLSBasedOnPriority(List<SitemapUrl> sortingURL) {
        List<SitemapUrl> toturlslist = new ArrayList<>();
        //logger.info("Sorting the URL'S  from siteCrawlingThread S2 size: " + sortingURL.size());
        sortingURL.stream().filter((arrlistvals) -> ((!arrlistvals.isUpdatedInFile()) && (arrlistvals.getSitemapUrl().trim() != null
                || !" ".equals(arrlistvals.getSitemapUrl())
                || !"&nbsp;".equals(arrlistvals.getSitemapUrl())))).map((arrlistvals) -> {
            toturlslist.add(arrlistvals);
            return arrlistvals;
        }).forEach((arrlistvals) -> {
            arrlistvals.setUpdatedInFile(true);
//                    siteCrawlingThread.s2.add(arrlistvals);
        });

        Collections.sort(toturlslist, (SitemapUrl privalue1, SitemapUrl privalue2) -> {
            int diff = 0;
            double values = (privalue1.getPriorityval() - privalue2.getPriorityval());
            if (values > 0) {
                diff = -1;
            } else if (values < 0) {
                diff = 1;
            }
            return diff;
        });
        return toturlslist;
    }

    public static List<String> genrateSiteMapFiles(String fileSaveLocation, List<SitemapUrl> sortingURLS, SiteMapBuilderTool sitemapBuilder, int phaseIndex) {
        List<String> siteMapfiles = new LinkedList<>();
        try {
            logger.info("Genrateing SiteMap Files for index: " + phaseIndex + ", for sortingURLS size:: " + sortingURLS.size() + ", sitemapBuilder :: " + (sitemapBuilder.toString()));
            BuildSiteMapXML siteMapXML = new BuildSiteMapXML(fileSaveLocation, sortingURLS, sitemapBuilder, phaseIndex);
            BuildSiteMapHtml siteMapHTML = new BuildSiteMapHtml(fileSaveLocation, sortingURLS, phaseIndex);
            BuildSiteMapErrorTextFiles siteMapText = new BuildSiteMapErrorTextFiles(fileSaveLocation, sortingURLS, "sitemapurllist", phaseIndex);
            //BuildSiteMapErrorTextFiles siteMapError = new BuildSiteMapErrorTextFiles(fileSaveLocation, siteCrawlingThread, downloadThreadMap, "sitemap.err");

            Thread sMXMLThread = new Thread(siteMapXML);
            Thread sMHTMLThread = new Thread(siteMapHTML);
            Thread sMTextThread = new Thread(siteMapText);
            //Thread sMErrorThread = new Thread(siteMapError);

            sMXMLThread.start();
            sMHTMLThread.start();
            sMTextThread.start();
            //sMErrorThread.start();
            try {
                sMXMLThread.join();
                sMHTMLThread.join();
                sMTextThread.join();
            } catch (Exception e) {
                logger.error("Exception for threads joining: ", e);
            }
            /*Adding created files  to List, execluding empty files.*/
            if (!siteMapXML.getDwnXMLfileName().contains("empty")) {
                if (Common.checkFileExistance(fileSaveLocation + siteMapXML.getDwnXMLfileName())) {
                    siteMapfiles.add(siteMapXML.getDwnXMLfileName());
                }
            }
            if (!siteMapHTML.getDwnHTMLfileName().contains("empty")) {
                if (Common.checkFileExistance(fileSaveLocation + siteMapHTML.getDwnHTMLfileName())) {
                    siteMapfiles.add(siteMapHTML.getDwnHTMLfileName());
                }
            }
            if (!siteMapText.getDwnfileName().contains("empty")) {
                if (Common.checkFileExistance(fileSaveLocation + siteMapText.getDwnfileName())) {
                    siteMapfiles.add(siteMapText.getDwnfileName());
                }
            }

        } catch (Exception e) {
            logger.error("Exception in genartion of site map files: ", e);
        }
        logger.info("List of Sitemap files is generated:: " + siteMapfiles.size() + ", for URLS size::" + sortingURLS.size() + ", at index:: " + phaseIndex);
        return siteMapfiles;
    }

    public static String generateSiteMapZipFile(String queryURL, String saveSiteMapFile, List<String> siteMapFiles) {
        String userFileName = null;
        try {
            URL fileName = new URL(queryURL);
            /*userFileName = fileName.getHost() + "_" + (new SimpleDateFormat("MMMM_dd_yyyy").format(Calendar.getInstance().getTime()));*/
            userFileName = fileName.getHost();
        } catch (Exception e) {
            userFileName = queryURL;
            logger.error("Exception in making userFileName ", e);
        }
        logger.info("Files in SiteMap Zip: " + siteMapFiles);
        String zipFile = userFileName + "_sitemap.zip";
//        String[] sourceFiles = {dwnfileName1, dwnfileName2, dwnfileName3, dwnfileName4};
        byte[] buffer = new byte[3072];
        try {
            FileOutputStream fout = new FileOutputStream(saveSiteMapFile + zipFile);
            try (ZipOutputStream zout = new ZipOutputStream(fout)) {
                FileInputStream fin;
                for (int i = 0; i < siteMapFiles.size(); i++) {
                    //create object of FileInputStream for source file
                    fin = new FileInputStream(saveSiteMapFile + siteMapFiles.get(i));
                    if (fin != null) {
                        if (siteMapFiles.get(i).length() == -1) {
                            logger.info("!!File " + siteMapFiles.get(i) + " emty!!");
                        }
                        int byteData = fin.read();
                        fin.close();
                        if (byteData != -1) {
                            zout.putNextEntry(new ZipEntry(siteMapFiles.get(i)));
                        }
                        int length;
                        fin = new FileInputStream(saveSiteMapFile + siteMapFiles.get(i));
                        while ((length = fin.read(buffer)) > 0) {
                            zout.write(buffer, 0, length);
                        }
                        zout.closeEntry();
                        fin.close();
                    }
                }
                //close the ZipOutputStream
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in Site Map Builder while preparing ZIP file", e);
        } catch (IOException ioe) {
            logger.error("IOException :" + ioe);
        } catch (Exception e) {
            logger.error("Exception :" + e);
        }

        return zipFile;
    }

    /*Creating a folderto save the SiteMap Files and ZipFile and returns the path.*/
    public static String getSiteMapFileSaveLocation(String userQueryURL, String lxrmDownloadPath) {
        String siteMapFileLocation = null;
        SimpleDateFormat siteMapDateFormat = new SimpleDateFormat("yyyy_MMM_dd_HHmmss_SSS");
        try {
            URL queryURL = new URL(userQueryURL);
            siteMapFileLocation = "LXRM_SiteMap_" + siteMapDateFormat.format(Calendar.getInstance().getTimeInMillis()) + "_" + queryURL.getHost();
        } catch (Exception e) {
            siteMapFileLocation = "LXRM_SiteMap_" + siteMapDateFormat.format(Calendar.getInstance().getTimeInMillis()) + "_" + userQueryURL;
            logger.error("Exception in making userFileName ", e);
        }
        siteMapFileLocation = lxrmDownloadPath + siteMapFileLocation + "/";
        logger.info("The site map file location: " + siteMapFileLocation);
        File f = new File(siteMapFileLocation);
        f.mkdir();
        return siteMapFileLocation;
    }

    /*Returns the path to fetch the SiteMap ZipFile from previous user download reports.*/
    public static String getPreviousDownLoadReportPath(List<PaidDownloadedReportDetails> paidDownloadedReportDetails, String requestFileName, long userID) {
        String filePath = null;
        Date fileDate = null;
        SimpleDateFormat fileDateParser = new SimpleDateFormat("MMMM dd, yyyy HH:mm");
        try {
            PaidDownloadedReportDetails storedReportDeatils = paidDownloadedReportDetails.stream()
                    .filter(report -> ((report.getReportName().equalsIgnoreCase(requestFileName)) && (report.getToolId() == 6) && (report.getUserId() == userID))).findAny().orElse(null);
            if (storedReportDeatils != null) {
                fileDate = fileDateParser.parse(storedReportDeatils.getCreatedTime());
                fileDateParser = new SimpleDateFormat("yyyy_MMM_dd");
                filePath = "LXRM_SiteMap_" + fileDateParser.format(fileDate) + "_" + Common.getHostFromURL(storedReportDeatils.getUrl());
            }
        } catch (Exception e) {
            logger.error("Exception in getPreviousDownLoadReportPath", e);
        }
        return filePath;
    }

}
