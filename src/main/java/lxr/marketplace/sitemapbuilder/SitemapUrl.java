package lxr.marketplace.sitemapbuilder;

import java.io.Serializable;

public class SitemapUrl implements Serializable {

    private String sitemapUrl;
    private String status;
    private long depthId;
    private long statuscode;
    private double priorityval;
    private String lastmodifieddate;
    private boolean updatedInFile;

    public String getLastmodifieddate() {
        return lastmodifieddate;
    }

    public void setLastmodifieddate(String lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    public double getPriorityval() {
        return priorityval;
    }

    public void setPriorityval(double priorityval) {
        this.priorityval = priorityval;
    }

    public String getSitemapUrl() {
        return sitemapUrl;
    }

    public void setSitemapUrl(String sitemapUrl) {
        this.sitemapUrl = sitemapUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getDepthId() {
        return depthId;
    }

    public void setDepthId(long depthId) {
        this.depthId = depthId;
    }

    public long getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(long statuscode) {
        this.statuscode = statuscode;
    }

    public boolean equals(Object o) {
        if ((o instanceof SitemapUrl) && ((this.sitemapUrl).equals(((SitemapUrl) o).getSitemapUrl()))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isUpdatedInFile() {
        return updatedInFile;
    }

    public void setUpdatedInFile(boolean updatedInFile) {
        this.updatedInFile = updatedInFile;
    }

}
