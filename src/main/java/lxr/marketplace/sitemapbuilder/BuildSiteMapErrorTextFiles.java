/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.sitemapbuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.List;
import org.apache.log4j.Logger;

public class BuildSiteMapErrorTextFiles implements Runnable {
    
    private static final Logger logger = Logger.getLogger(BuildSiteMapErrorTextFiles.class);
    
    private String savePath = null;
    
    private String dwnfileName = null;
    private String fileTypeName = null;
    private List<SitemapUrl> textSortURLS = null;
    private int phaseCount = 0;
    
    public BuildSiteMapErrorTextFiles(String saveErrorOrReadMefile) {
        this.savePath = saveErrorOrReadMefile;
    }
    
    public BuildSiteMapErrorTextFiles(String saveErrorHTMlfile, List<SitemapUrl> sortingURLS, String fileTypeName, int phaseCount) {
        this.savePath = saveErrorHTMlfile;
        this.textSortURLS = sortingURLS;
        this.fileTypeName = fileTypeName;
        this.phaseCount = phaseCount;
    }
    
    public String getDwnfileName() {
        return dwnfileName;
    }
    
    public void setDwnfileName(String dwnfileName) {
        this.dwnfileName = dwnfileName;
    }
    
    @Override
    public void run() {
        dwnfileName = createSiteMapTxtError(fileTypeName, textSortURLS, phaseCount, ".txt");
    }
    
    public String createSiteMapTxtError(String fileTypeName, List<SitemapUrl> textSortURL, int phaseCount, String fileExtension) {
        logger.info("*** Started creating SiteMapText/Error for URL Size:: " + this.textSortURLS.size()+", ****");
        try {
//            logger.info("BuildSiteMapErrorTextFiles: " + fileTypeName);
            Writer output = null;
            if (phaseCount != 0) {
                fileTypeName = fileTypeName + "-" + phaseCount + fileExtension;
            } else {
                fileTypeName = fileTypeName + fileExtension;
            }
            long urlStatusCount = textSortURL.stream().filter(siteURL -> ((siteURL.getSitemapUrl() != null) && (!siteURL.getSitemapUrl().equals("")) && (!siteURL.getSitemapUrl().equals("&nbsp;")))).filter(siteURL -> (((siteURL.getStatuscode() == 302) || (siteURL.getStatuscode() == 200)))).count();
            if (urlStatusCount != 0) {
                File totalpath = new File(savePath + fileTypeName);
                output = new BufferedWriter(new FileWriter(totalpath));
//            List<SitemapUrl> txtsortUrls = SiteMapBuilderToolController.newSortingurls(sMBThread);
                for (SitemapUrl allanctags : textSortURL) {
                    if (fileTypeName.contains("sitemapurllist")) {
                        
                        if ((allanctags.getSitemapUrl() != null) && (!allanctags.getSitemapUrl().equals(" ")) && (!allanctags.getSitemapUrl().equals("&nbsp;"))) {
                            if (((allanctags.getStatuscode() == 302) || (allanctags.getStatuscode() == 200))) {
                                
                                if ((!allanctags.getSitemapUrl().equals(" ")) && (!allanctags.getSitemapUrl().equals("&nbsp;"))) {
                                    output.write(allanctags.getSitemapUrl() + "\r\n");
                                }
                            }
                        }
                    } else if (fileTypeName.contains("sitemap")) {
                        
                        if ((allanctags.getSitemapUrl() != null) && (!allanctags.getSitemapUrl().equals(" ")) && (!allanctags.getSitemapUrl().equals("&nbsp;"))) {
                            if (allanctags.getStatuscode() >= 400) {
                                output.write(allanctags.getSitemapUrl() + " , " + allanctags.getStatuscode() + " , " + allanctags.getStatus() + "\r\n");
                            }
                        }
                    }
                }
                output.close();
            } else {
                fileTypeName = "emptyTextFile";
            }
        } catch (Exception ie) {
            logger.error("Exception on creating createSiteMapTxtError: ", ie);
        }
        logger.info("*** Compeleted generating Text/Error File for Sorting URLS: " + textSortURL.size() + ", and  Text/Error File Path is: " + fileTypeName +" *****");
        return fileTypeName;
    }
    
    public String createReadMeFile(String saveReadMeFile, String querySiteMapURL, boolean includeSiteMapIndex) {
        Writer output;
        try {
            if (querySiteMapURL.endsWith("/")) {
                querySiteMapURL = querySiteMapURL.substring(0, querySiteMapURL.length() - 1);
            }
            File totalpath = new File(savePath + "/ReadMe.txt");
            output = new BufferedWriter(new FileWriter(totalpath));
            output.write("SiteMap Builder Tool: \r\n");
            output.write("\r\n");
            output.write("Now you have XML and html sitemap files for your website.\r\n");
            output.write("\r\n");
            output.write("Upload this file to your website so that it can be accessed with URL as\r\n");
            output.write("\r\n");
            output.write(querySiteMapURL + "/sitemap.xml  [This URL is for the Search Engine Crawlers like Google].\r\n");
            output.write("\r\n");
            output.write(querySiteMapURL + "/sitemap.html [This URL is for the end users visiting the website. This URL has to be linked anywhere in your website such as the footer].\r\n");
            output.write("\r\n");
            output.write("\r\n");
            output.write("Add the following text in last line of your robots.txt file, Robots.txt [Path] (" + querySiteMapURL + "/robots.txt).\r\n");
            output.write("\r\n");
            output.write("Sitemap:" + querySiteMapURL + "/sitemap.xml \r\n");
            output.write("\r\n");
            output.write("SiteMap.Zip contains the following list of files:\r\n");
            output.write("\r\n");
            output.write("1. Sitemap.xml contains the list of  web pages of your site to tell Google and other search engines about the organization of your site content.\r\n");
            output.write("\r\n");
            output.write("2. Sitemap.html contains the list of  web pages helps visitors to easily navigate to your website.\r\n");
            output.write("\r\n");
            output.write("3. Sitemap.txt contains the list of URL’S present on your website for your reference.\r\n");
            output.write("\r\n");
            output.write("4. Sitemap.error contains the list of  web pages you were trying to reach could not be found on your web site server.\r\n");
            output.write("\r\n");
            if (!includeSiteMapIndex) {
                output.write("5. Sitemap_index.xml contains list all your sitemaps and submit this single file to Google rather than submitting individual sitemaps.\r\n");
                output.write("\r\n");
                output.write("\r\n");
                output.write("[Note]\r\n A sitemap file can't contain more than 50,000 URLs and must be no larger than 10 MB uncompressed, breaking  up large sitemaps into a smaller sitemaps to prevent your server from being overloaded if Google requests your sitemap frequently.\r\n");
            }
            output.write("\r\n");
            output.write("\r\n");
            output.write("Wouldn't you like a FREE and easy way to create a new robots.txt file use our tool [Robots.txt Generator](https://lxrmarketplace.com/robots-txt-generator-tool.html)");
            output.close();
        } catch (Exception e) {
            logger.error("Exception in createReadMeFile", e);
        }
        return "ReadMe.txt";
    }
    
}
