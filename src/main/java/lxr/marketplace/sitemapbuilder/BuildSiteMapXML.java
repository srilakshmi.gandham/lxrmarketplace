/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.sitemapbuilder;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author netelixir1
 */
public class BuildSiteMapXML implements Runnable {

    private static Logger logger = Logger.getLogger(BuildSiteMapXML.class);

    private final String appPath;
    private final SiteMapBuilderTool sitemapBuilder;

    private String dwnXMLfileName;
    private List<SitemapUrl> xmlsortUrls = null;
    private final int phaseCount;

    public String getDwnXMLfileName() {
        return dwnXMLfileName;
    }

    public void setDwnXMLfileName(String dwnXMLfileName) {
        this.dwnXMLfileName = dwnXMLfileName;
    }

    public BuildSiteMapXML(String downloadFolder, List<SitemapUrl> sortingURLS, SiteMapBuilderTool sitemapBuilder, int phaseCount) {
        this.appPath = downloadFolder;
        this.xmlsortUrls = sortingURLS;
        this.sitemapBuilder = sitemapBuilder;
        this.phaseCount = phaseCount;
    }

    @Override
    public void run() {
        dwnXMLfileName = buildSiteMapXML(this.phaseCount);
    }

    public String buildSiteMapXML(int phaseCount) {
        logger.info("*** Started creating SiteMapXML for URL Size:: " + this.xmlsortUrls.size() + ", ****");
        String XMLFileName = (phaseCount != 0) ? "sitemap-" + phaseCount + ".xml" : "sitemap.xml";
        int lastmodf = 0;
        double priority = 0;
        int changefrq = 0;
        long urlStatusCount = 0;
        String userDate = null;
        String lastmodifieddate = null;
        URL currentUrlObj = null;
        Element urlElement = null;
        Element locelElement = null;
        Element lastmodElement = null;
        HttpURLConnection currentUrlConn = null;
        String secondDouble = null;
        String tempDate = null;
        Date dateObj = null;
        SimpleDateFormat simpleDateFormatOne = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat simpleDateFormatTwo = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormatThree = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        //List<SitemapUrl> xmlsortUrls = SiteMapBuilderToolController.newSortingurls(sMBThread);
        priority = sitemapBuilder.getSepriority();
        changefrq = sitemapBuilder.getFrequency();
        lastmodf = sitemapBuilder.getLastmod();
        userDate = sitemapBuilder.getUserdate();
        if (userDate != null) {
            try {
                Date normalDt = simpleDateFormatOne.parse(userDate);
                tempDate = simpleDateFormatTwo.format(normalDt);
            } catch (ParseException e) {
                logger.error("Exception in buildSiteMapXML: ", e);
            }
            userDate = tempDate;
        }
        tempDate = null;
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            // root elements
            Document documentObj = documentBuilder.newDocument();
            Element rootElement = documentObj.createElement("urlset");
            documentObj.appendChild(rootElement);

            // set attribute to urlset element
            Attr attr = documentObj.createAttribute("xmlns");
            attr.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");
            rootElement.setAttributeNode(attr);

            Attr attrOne = documentObj.createAttribute("xmlns:xsi");
            attrOne.setValue("http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttributeNode(attrOne);

            Attr attrTwo = documentObj.createAttribute("xsi:schemaLocation");
            attrTwo.setValue("http://www.sitemaps.org/schemas/sitemap/0.9  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
            rootElement.setAttributeNode(attrTwo);

            urlStatusCount = this.xmlsortUrls.stream().filter(siteURL -> ((siteURL.getSitemapUrl() != null) && (!siteURL.getSitemapUrl().equals("")) && (!siteURL.getSitemapUrl().equals("&nbsp;")))).filter(siteURL -> (((siteURL.getStatuscode() == 302) || (siteURL.getStatuscode() == 200)))).count();
            if (urlStatusCount != 0) {
                for (SitemapUrl sitemapUrlObj : this.xmlsortUrls) {
                    try {
                        logger.debug("The URL :: " + sitemapUrlObj.getSitemapUrl() + ", and statusCode:" + sitemapUrlObj.getStatuscode());
                        if ((sitemapUrlObj.getSitemapUrl() != null) && (!sitemapUrlObj.getSitemapUrl().trim().isEmpty())) {
                            if (((sitemapUrlObj.getStatuscode() == 302) || (sitemapUrlObj.getStatuscode() == 200))) {
                                urlElement = documentObj.createElement("url");
                                locelElement = documentObj.createElement("loc");
                                locelElement.setTextContent(sitemapUrlObj.getSitemapUrl());
                                urlElement.appendChild(locelElement);
                                if (lastmodf == 2) {
                                    try {
                                        currentUrlObj = new URL(sitemapUrlObj.getSitemapUrl());
                                        currentUrlConn = null;
                                        currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                                        currentUrlConn.setInstanceFollowRedirects(false);
                                        currentUrlConn.setConnectTimeout(Common.IMPORTANT_CONNECT_TIME_OUT);
                                        currentUrlConn.setReadTimeout(Common.IMPORTANT_READ_TIME_OUT);
                                        currentUrlConn.setRequestProperty("User-Agent", Common.USER_AGENT);
                                    } catch (IOException e) {
                                        logger.error("Exception in connection to HeaderField Last-Modified for URL:: " + sitemapUrlObj.getSitemapUrl() + " Cause:: " + e.getMessage());
                                    }
                                    if (currentUrlConn != null) {
                                        lastmodifieddate = currentUrlConn.getHeaderField("Last-Modified");
                                    }
                                    if (lastmodifieddate != null) {
                                        try {
                                            dateObj = simpleDateFormatThree.parse(lastmodifieddate);
                                            tempDate = simpleDateFormatTwo.format(dateObj);
                                            if (tempDate != null) {
                                                sitemapUrlObj.setLastmodifieddate(tempDate);
                                            }
                                        } catch (ParseException e) {
                                            logger.error("Exception in buildSiteMapXML method for ParseException of BuildSiteMapXML: ", e);
                                        }
                                    }
                                    if (sitemapUrlObj.getLastmodifieddate() != null) {
                                        lastmodElement = documentObj.createElement("lastmod");
                                        lastmodElement.setTextContent(sitemapUrlObj.getLastmodifieddate());
                                        urlElement.appendChild(lastmodElement);
                                    }
                                } else if (lastmodf == 3) {
                                    lastmodElement = documentObj.createElement("lastmod");
                                    lastmodElement.setTextContent(userDate);
                                    urlElement.appendChild(lastmodElement);
                                }
                                if (changefrq != 1) {
                                    Element changefreq = documentObj.createElement("changefreq");
                                    switch (changefrq) {
                                        case 2:
                                            changefreq.setTextContent("always");
                                            break;
                                        case 3:
                                            changefreq.setTextContent("hourly");
                                            break;
                                        case 4:
                                            changefreq.setTextContent("daily");
                                            break;
                                        case 5:
                                            changefreq.setTextContent("weekly");
                                            break;
                                        case 6:
                                            changefreq.setTextContent("monthly");
                                            break;
                                        case 7:
                                            changefreq.setTextContent("never");
                                            break;
                                        default:
                                            break;
                                    }
                                    urlElement.appendChild(changefreq);
                                }
                                if (priority == 2.0) {
                                    secondDouble = Double.toString(sitemapUrlObj.getPriorityval());
                                    Element priority1 = documentObj.createElement("priority");
                                    priority1.setTextContent(secondDouble);
                                    urlElement.appendChild(priority1);
                                }
                                rootElement.appendChild(urlElement);
                            }
                            // write the content into xml file
                            TransformerFactory transformerFactory = TransformerFactory.newInstance();
                            Transformer transformer = transformerFactory.newTransformer();
                            //for indent of XML
                            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

                            DOMSource source = new DOMSource(documentObj);
                            StreamResult result = new StreamResult(new File(appPath + XMLFileName));
                            transformer.transform(source, result);
                        }
                    } catch (Exception e) {
                        logger.error("Exception adding URL:: " + sitemapUrlObj.getSitemapUrl() + ", adding to sitemap.xml cause:: ", e);
                    }
                    lastmodifieddate = null;
                    currentUrlObj = null;
                    urlElement = null;
                    locelElement = null;
                    lastmodElement = null;
                    currentUrlConn = null;
                    secondDouble = null;
                    tempDate = null;
                    dateObj = null;
                }
            } else {
                XMLFileName = "emptyXML";
            }
        } catch (ParserConfigurationException e) {
            logger.error("ParserConfigurationException when creating sitemap.xml cause:: " + e);
        }
        logger.info("***** Compeleted total sorting URLS: " + xmlsortUrls.size() + ", in SiteMap.xml File is: " + XMLFileName);
        return XMLFileName;
    }

    public static String generateSiteMapIndex(String queryURL, List<String> siteMapFiles, String saveIndexFile) {
        String indexXMLFile = null;
        indexXMLFile = "sitemap_index.xml";
        try {
            Element sitemapElem = null;
            Element sitemapIndexElem = null;
            Element locElem = null;
            Attr xmlAttribute = null;

            /*Instance of doc object.*/
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            /*Root Element*/
            org.w3c.dom.Document xmlDoc = docBuilder.newDocument();

            /*Writing sitemapindex child element into doc object.*/
            sitemapIndexElem = xmlDoc.createElement("sitemapindex");
            xmlDoc.appendChild(sitemapIndexElem);

            /*Appending xmlns child attribute to sitemapindex element.*/
            xmlAttribute = xmlDoc.createAttribute("xmlns");
            xmlAttribute.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");
            sitemapIndexElem.setAttributeNode(xmlAttribute);

            for (String indexElem : siteMapFiles) {
                /*Writing sitemap child element into sitemapindex parent element.*/
                sitemapElem = xmlDoc.createElement("sitemap");

                locElem = xmlDoc.createElement("loc");
                // logger.info("The sitemap file: "+indexElem);
                if (queryURL.endsWith("/")) {
                    locElem.setTextContent(queryURL + indexElem);
                } else {
                    locElem.setTextContent(queryURL + "/" + indexElem);
                }

                sitemapElem.appendChild(locElem);

                /*lastmod = xmlDoc.createElement("lastmod");
                lastmod.setTextContent("Last Updated");
                sitemapElem.appendChild(lastmod);*/
                sitemapIndexElem.appendChild(sitemapElem);

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                //for indent of XML
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

                DOMSource source = new DOMSource(xmlDoc);
                StreamResult result = new StreamResult(new File(saveIndexFile + indexXMLFile));
                transformer.transform(source, result);
            }
        } catch (ParserConfigurationException pce) {
            logger.error("ParserConfigurationExceptio in generateSiteMapIndex: ", pce);
        } catch (TransformerException tfe) {
            logger.error("TransformerException in generateSiteMapIndex:", tfe);
        } catch (DOMException | IllegalArgumentException e) {
            logger.error("DOMException in generateSiteMapIndex:", e);
        } catch (Exception e) {
            logger.error("Exception in generateSiteMapIndex:", e);
        }
        return indexXMLFile;
    }
}
