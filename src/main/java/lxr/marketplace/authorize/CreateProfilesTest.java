package lxr.marketplace.authorize;

/*
 * Before working with this sample code, please be sure to read the accompanying Readme.txt file.
 * It contains important information regarding the appropriate use of and conditions for this sample
 * code. Also, please pay particular attention to the comments included in each individual code file,
 * as they will assist you in the unique and correct implementation of this code on your specific platform.
 *
 * Copyright 2008 Authorize.Net Corp.
 */

//import java.net.MalformedURLException;
//import java.net.URL;
import lxr.marketplace.authnet.rebill.*;
//import net.authorize.data.creditcard.CreditCard;

public class CreateProfilesTest {

	public static void main(String[] args){
		
		System.out.println(SoapAPIUtilities.getExampleLabel("Create Profiles Test"));
		ServiceSoap soap = SoapAPIUtilities.getServiceSoap();

		CustomerPaymentProfileType new_payment_profile = new CustomerPaymentProfileType();
		
		PaymentType new_payment = new PaymentType();
//		BankAccountType new_bank = new BankAccountType();
//		new_bank.setAccountNumber("4111111");
		
		CreditCardType new_card = new CreditCardType();
//		CreditCardMaskedType new_card1 = new CreditCardMaskedType();
		
		new_card.setCardNumber("1234567867356576");
		
		try{
			javax.xml.datatype.XMLGregorianCalendar cal = javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar();
			cal.setMonth(6);
			cal.setYear(2019);
			new_card.setExpirationDate(cal);
			// System.out.println(new_card.getExpirationDate().toXMLFormat());
		}catch(javax.xml.datatype.DatatypeConfigurationException dce){
			System.out.println(dce.getMessage());
		}
		new_card.setCardCode("484");
		new_payment.setCreditCard(new_card);
		//new_payment.setBankAccount(new_bank);
		
		new_payment_profile.setPayment(new_payment);

		CustomerProfileType m_new_cust = new CustomerProfileType();
		m_new_cust.setEmail("here@there.com");
		m_new_cust.setDescription("Example customer: " + Long.toString(System.currentTimeMillis()));

		ArrayOfCustomerPaymentProfileType pay_list = new ArrayOfCustomerPaymentProfileType();
		pay_list.getCustomerPaymentProfileType().add(new_payment_profile);
		
		m_new_cust.setPaymentProfiles(pay_list);
		
		CreateCustomerProfileResponseType response = soap.createCustomerProfile(SoapAPIUtilities.getMerchantAuthentication(),m_new_cust,ValidationModeEnum.TEST_MODE);
		if(response != null){
//			response.getCustomerPaymentProfileIdList().getLong().get(0);
			System.out.println("Response Code: " + response.getResultCode().value());
			for(int i = 0; i < response.getMessages().getMessagesTypeMessage().size(); i++){
//				if(response.getResultCode().value().equals("Error")){ //NOT SUCCESS
//					System.out.println("eror meassage............"+response.getMessages().getMessagesTypeMessage().get(i).getText());
//				}
//				System.out.println("Message: " + response.getMessages().getMessagesTypeMessage().get(i).getText());
			}
			long new_cust_id = response.getCustomerProfileId();
			if(new_cust_id > 0){
				System.out.println("New ID = " + new_cust_id);
			}
			if (response.getCustomerPaymentProfileIdList() != null){
				for(long paymentProfileId : response.getCustomerPaymentProfileIdList().getLong()){
					System.out.println("With payment profile ID = " + paymentProfileId);
				}
			}
//			if(new_cust_id > 0 && tempPaymentProfileId > 0 ){
//				savedCreditCard.setCustProfileId(new_cust_id);
//				savedCreditCard.setPaymentProfileId(tempPaymentProfileId);
//				savedCreditCard.setType(lxrCreditCard.getType());
				int num = new_card.getCardNumber().length();
				int xCount = num-4;
				String tempCardNum="";
				for(int i=1;i<=xCount;i++){
					tempCardNum+="X";
				}
				
				tempCardNum+=new_card.getCardNumber().substring(xCount);
				System.out.println("tempCardNum-------"+tempCardNum);
//			}
		
		}
		else{
			System.out.println("Null response from server");
		}
		
	}
	
}
