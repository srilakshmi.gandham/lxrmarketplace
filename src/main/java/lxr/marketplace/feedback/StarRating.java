package lxr.marketplace.feedback;

import java.util.Calendar;

public class StarRating {
	int toolId;
	String comments;
	String title;
	private double rating;
	private Calendar feedbackDate; 
	
	public Calendar getFeedbackDate() {
		return feedbackDate;
	}
	public void setFeedbackDate(Calendar feedbackDate) {
		this.feedbackDate = feedbackDate;
	}
	public String getTitle() {
		return (title==null)?"":title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComments() {
		return (comments==null)?"":comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public double getRating() {
		return rating;
	}
	public int getToolId() {
		return toolId;
	}
	public void setToolId(int toolId) {
		this.toolId = toolId;
	}
	
}
