package lxr.marketplace.feedback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.user.Login;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.dao.DataAccessException;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class FeedbackService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(FeedbackService.class);
    String feedbackattachmentFolder;
    final String headerFileName = "X-File-Name";

    public String getFeedbackattachmentFolder() {
        return feedbackattachmentFolder;
    }

    public void setFeedbackattachmentFolder(String feedbackattachmentFolder) {
        this.feedbackattachmentFolder = feedbackattachmentFolder;
    }

    public void savefeedback(final StarRating sr, final long userId) {

        final String sql = "insert into feedback(user_id, tool_id ,rating ,title,comments,feedback_date) values (?, ? , ?, ?, ?, ?)";

        Object[] ratingList = new Object[6];
        ratingList[0] = userId;
        ratingList[1] = sr.getToolId();
        ratingList[2] = sr.getRating();
        ratingList[3] = sr.getTitle();
        ratingList[4] = sr.getComments();
        ratingList[5] = new java.sql.Timestamp(sr.getFeedbackDate().getTimeInMillis());

        try {
            getJdbcTemplate().update(sql, ratingList);
        } catch (DataAccessException ex) {
            logger.error("Exception in savefeedback", ex);
        }
    }

    public void createTemporaryFolder(long uId) {
        File f = new File(feedbackattachmentFolder + uId);
        if (f.isDirectory()) {
            deleteDirectory(f);
        }
        f.mkdir();
    }

    public boolean deleteDirectory(File f) {
        if (f.isDirectory()) {
            String[] children = f.list();
            for (int i = 0; i < children.length; i++) {
                File childPath = new File(f, children[i]);
                childPath.delete();
            }
        }
        return f.delete();
    }

    public void addFileInTemporary(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(true);
        Login user = (Login) session.getAttribute("user");
        File f = new File(feedbackattachmentFolder + user.getId());
        if (!f.isDirectory()) {
            createTemporaryFolder(user.getId());
        }
        saveFile(feedbackattachmentFolder + user.getId() + "/", request, response);
    }

    public String saveFile(String realPath, HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession(true);
        Login user = (Login) session.getAttribute("user");
        String fileName = "";
        InputStream is = null;
        FileOutputStream fos = null;
        String[] files = getListOfFiles(user.getId());
        boolean alreadyExist = false;
        if (request.getHeader(headerFileName) != null) {
            fileName = request.getHeader(headerFileName);
            logger.info("uploaded file name: " + fileName);
            if (fileName.trim().contains(" ")) {
                fileName = fileName.replace(" ", "%");
            }
            fileName = decodeFileName(fileName);
            for (String file : files) {
                if (file.equals(fileName)) {
                    alreadyExist = true;
                }
            }
            if (!alreadyExist) {
                try {
                    is = request.getInputStream();
                    File f = new File(realPath + fileName);
                    fos = new FileOutputStream(f);
                    IOUtils.copy(is, fos);
                    response.setContentType("application/json");
                    response.setStatus(HttpServletResponse.SC_OK);
                } catch (Exception e) {
                    logger.error(e);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                } finally {
                    try {
                        fos.close();
                        is.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        } else if (request.getParameter("fileName") != null) {
            fileName = request.getParameter("fileName");
            logger.info("Uploaded File Name: " + fileName);

            response.setContentType("text/html");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            final long permitedSize = 314572800;
            long fileSize;
            try {
                boolean isMultipart = ServletFileUpload
                        .isMultipartContent(request);
                if (isMultipart) {
                    FileItemFactory factory = new DiskFileItemFactory();
                    ServletFileUpload upload = new ServletFileUpload(factory);
                    List items = upload.parseRequest(request);
                    for (int i = 0; i < items.size(); i++) {
                        FileItem fileItem = (FileItem) items.get(i);
                        if (fileItem.isFormField()) {
                            //String any_parameter = fileItem.getString();
                        } else {
                            fileName = fileItem.getName();
                            fileName = decodeFileName(fileName);
                            for (String file : files) {
                                if (file.equals(fileName)) {
                                    alreadyExist = true;
                                }
                            }
                            if (!alreadyExist) {
                                fileSize = fileItem.getSize();
                                if (fileSize <= permitedSize) {
                                    File uploadedFile = new File(realPath + fileName);
                                    fileItem.write(uploadedFile);
                                } else {
                                    logger.info("file is too big");
                                    String message = "File size of '" + fileName + "' is more than the maximum file size limit";
                                    addInJson(message, response);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in  save file ", e);
            }
        }
        if (alreadyExist) {
            String message = "A file with name '" + fileName + "' already attached to this template mail.";
            addInJson(message, response);
        }
        return fileName;
    }

    public String[] getListOfFiles(long id) {
        String[] attachments = null;
        File source = new File(feedbackattachmentFolder + id);
        if (source.isDirectory()) {
            attachments = source.list();
        }
        return attachments;
    }

    public void addInJson(String message, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, message);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public String decodeFileName(String fileName) {
        String newfile = null;
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        fileName = fileName.replaceAll("'", "\\\\'");
        try {
            newfile = (String) engine.eval("decodeURIComponent('" + fileName + "')");
            //engine.eval("print(encodeURIComponent("+fileName+"))");
            //logger.info(engine.eval("print(encodeURIComponent("+fileName+"))"));
        } catch (ScriptException e) {
            logger.error("Exception in decodeFileName", e);
        }
        return newfile;
    }

    public boolean deleteTemporaryFolder(long id) {
        File f = new File(feedbackattachmentFolder + id);
        if (f.isDirectory()) {
            String[] children = f.list();
            for (int i = 0; i < children.length; i++) {
                File childPath = new File(f, children[i]);
                childPath.delete();
            }
        }
        return f.delete();
    }

    public void deleteFileFromTemporary(String fileName, long id) {
        String newfile = decodeFileName(fileName);
        File f = new File(feedbackattachmentFolder + id + "/" + newfile);
        if (f.exists()) {
            f.delete();
            logger.info("Attached file " + newfile + " is deleted.");
        } else {
            logger.debug("There is no file to remove from temporary folder with name " + newfile);
        }
    }

    public void changeFileName(long prevId, long userId) {
        File f = new File(feedbackattachmentFolder + prevId);
        if (f.exists()) {
            f.renameTo(f = new File(feedbackattachmentFolder + userId));
            logger.info("File Renamed to ");
        } else {
            logger.info("There is no file to rename");
        }
    }

    public void addInJson(String[] attachedFiles, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            if (attachedFiles != null) {
                int i = 0;
                for (String attachedFile : attachedFiles) {
                    arr.add(i++, attachedFile);
                }
                writer.print(arr);
            } else {
                writer.print("{success: false}");
            }
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public void insertFeedbackforUnsubscription(String comments, long userId) {
        final String sql = "insert into feedback(user_id, tool_id ,comments,feedback_date) values (?, ? , ?, ?)";
        final Timestamp feeedbackTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        Object[] ratingList = new Object[4];
        ratingList[0] = userId;
        ratingList[1] = 0;
        ratingList[2] = comments;
        ratingList[3] = feeedbackTime;

        try {
            getJdbcTemplate().update(sql, ratingList);
        } catch (Exception ex) {
            logger.error("Exception in insertFeedbackforUnsubscription ", ex);
        }

    }

    public void jsonAvearageRating(String avearageRating, String starRating, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, avearageRating);
            arr.add(1, starRating);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public long getFeedBackId(final long userId, final long toolId) {
        long feedBackId = 0;
        SqlRowSet sqlRowSet = null;
        String query = "select id from feedback where tool_id = ? and  user_id = ? order by time(feedback_date) desc,feedback_date";
        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{toolId, userId});
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    feedBackId = sqlRowSet.getLong("id");
                    break;
                }
            }
        } catch (Exception e) {
            logger.error("Exception in getFeedBackId", e);

        }
        return feedBackId;
    }

    public void updateUserExistedFeedBack(final double rating, final String title, final String comments, final java.sql.Timestamp feedBackTime, final long feedBackId) {
        String query = "update feedback set rating = ?, title = ?,comments = ?,feedback_date = ?  where id = ?";
        getJdbcTemplate().update(query, rating, title, comments, feedBackTime, feedBackId);
    }
}
