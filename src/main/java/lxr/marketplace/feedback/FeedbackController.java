package lxr.marketplace.feedback;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.AttachmentService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class FeedbackController extends SimpleFormController {

	private static Logger logger = Logger.getLogger(FeedbackController.class);
	FeedbackService feedbackService;
	AttachmentService attachmentService;
	String supportMail;
	LoginService loginService;
	
	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public void setSupportMail(String supportMail) {
		this.supportMail = supportMail;
	}

	public AttachmentService getAttachmentService() {
		return attachmentService;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public FeedbackController() {
		setCommandClass(Feedback.class);
		setCommandName("feedback");
	}

	public void setFeedbackService(FeedbackService feedbackService) {
		this.feedbackService = feedbackService;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		HttpSession session = request.getSession(true);
		Feedback feedback = (Feedback) command;
		Login user = (Login) session.getAttribute("user");
		if (user != null) {
			session.setAttribute("feedback",feedback);
                        session.removeAttribute("messageSent");
			if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
				feedbackService.addFileInTemporary(request, response);
                                return null;
			} else if(WebUtils.hasSubmitParameter(request, "feedbackSubmit")) {
				if(feedback.getSubject()!= null || feedback.getMailBody()!= null ){
					String toAdress = supportMail;
					String[] attachments= feedbackService.getListOfFiles(user.getId());
					SendMail sendmail = new SendMail();
					boolean messageSent =sendmail.sendFeedBackMail(feedback,toAdress,attachments, user.getId());
					if(messageSent){
                                            session.setAttribute("messageSent", messageSent);
                                            if(attachments !=null && attachments.length>=1)
                                            {
                                                feedbackService.deleteFileFromTemporary(attachments[0], user.getId());
                                            }
					}
				}
                                ModelAndView mAndV = new ModelAndView(getFormView(), "feedback",new Feedback());
                                mAndV.addObject("feedBackMail","sent");
                                return null;
			}else if(WebUtils.hasSubmitParameter(request, "getAllFiles")) {
				String [] attachedFiles = feedbackService.getListOfFiles(user.getId());
				feedbackService.addInJson(attachedFiles, response);
				return null;
			}else if(WebUtils.hasSubmitParameter(request, "removeFile")) {
				String fileName = WebUtils.findParameterValue(request, "removeFile");	
				feedbackService.deleteFileFromTemporary(fileName, user.getId());
				String [] attachedFiles = feedbackService.getListOfFiles(user.getId());
				feedbackService.addInJson(attachedFiles, response);
				return null;
			} else if(WebUtils.hasSubmitParameter(request, "feedbackClear")) {
				feedback = (Feedback) session.getAttribute("feedback");
				if (feedback != null) {
					feedbackService.deleteTemporaryFolder(user.getId());
					session.removeAttribute("feedback");
					session.setAttribute("feedback", null);
					session.removeAttribute("messageSent");
					logger.info("Cleared the data");
				}
                                ModelAndView mAndV = new ModelAndView(getFormView(), "feedback", new Feedback());
                                mAndV.addObject("feedBackMail","fail");
                                mAndV.addObject("clearAll","clearAll");
                                return mAndV;
			}else if(WebUtils.hasSubmitParameter(request, "unsubscribe")) {
				boolean recordPresent = false;
				long guestId = 0;
				String comments = request.getParameter("comments");
				long userId = Long.parseLong(session.getAttribute("unsubuserId").toString());
				
				feedbackService.insertFeedbackforUnsubscription(comments,userId);
				if(userId > 0){
					loginService.updateUnSubscription(userId);
				}else{
					if(session.getAttribute("usermail")!= null){
						String guestMail = (String)session.getAttribute("usermail");
						List<Long> guestUserIdList = loginService.findEmailInUserTab(guestMail);
						if(guestUserIdList !=null && guestUserIdList.size() > 0){
							logger.debug("guest Id is...."+guestUserIdList.get(0));
							guestId = guestUserIdList.get(0);
							loginService.updateUnSubscriptionList(guestUserIdList);
							recordPresent = loginService.findUnsubscribedUser(guestId);
						}
					}
				}
				if(userId > 0){
					recordPresent = loginService.findUnsubscribedUser(userId);
				}
				if(!recordPresent){
					if(userId > 0){
						loginService.insertIntoUnsubscribeReasonsTab(userId);
					}else if(guestId  > 0){
						loginService.insertIntoUnsubscribeReasonsTab(guestId);
					}
				}
				logger.info("updated unsubsrciption in user table");
				return null;
			}

		}
                feedback = new Feedback();
                feedback = (Feedback) session.getAttribute("feedback");
                if (feedback != null) {
                        feedbackService.deleteTemporaryFolder(user.getId());
                        session.removeAttribute("feedback");
                        session.setAttribute("feedback", null);
                        session.removeAttribute("messageSent");
                }
		ModelAndView mAndV = new ModelAndView(getFormView(), "feedback",
				feedback);
                mAndV.addObject("feedBackMail","fail");
		return mAndV;

	}

        @Override
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}

        @Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		HttpSession session = request.getSession();
		Feedback feedbackForm = (Feedback) session.getAttribute("feedback");
		if (feedbackForm == null) {
			feedbackForm = new Feedback();
	}
		return feedbackForm;
	}
}
