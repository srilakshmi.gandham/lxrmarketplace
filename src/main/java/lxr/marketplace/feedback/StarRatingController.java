package lxr.marketplace.feedback;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.home.HomePageService;

import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import lxr.marketplace.util.ui.CommonUi;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class StarRatingController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(StarRatingController.class);
    FeedbackService feedbackService;
    private ToolService toolService;
    private HomePageService homePageService;

    public StarRatingController() {
        setCommandClass(StarRating.class);
        setCommandName("starRating");
    }

    public void setFeedbackService(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    @Autowired
    public void setHomePageService(HomePageService homePageService) {
        this.homePageService = homePageService;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        StarRating starRating = (StarRating) command;
        String avgrating = null;
        Login user = (Login) session.getAttribute("user");
        if (user.getId() < 0) {
            //convert the user to guest user on keyword rank checker usage.
            Common.modifyDummyUserInToolorVideoUsage(request, response);
            user = (Login) session.getAttribute("user");
        }
        if (WebUtils.hasSubmitParameter(request, "feedbackdata")) {
            String submit = request.getParameter("feedbackdata");
            if (submit != null && submit.equalsIgnoreCase("submit")) {
                String title = Common.trimHtmlTagsContent(starRating.getTitle());
                String comments = Common.trimHtmlTagsContent(starRating.getComments());
                starRating.setTitle(title);
                starRating.setComments(comments);
                starRating.setFeedbackDate(Calendar.getInstance());
                /*Seraching for user existed rating.*/
                long feedBackId = feedbackService.getFeedBackId(user.getId(), starRating.getToolId());
                if (feedBackId == 0) {
                    feedbackService.savefeedback(starRating, user.getId());
                } else {
                    feedbackService.updateUserExistedFeedBack(starRating.getRating(), title, comments, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()), feedBackId);
                }
                Map<Integer, Double> toolRatings = toolService.getToolRatings();
                avgrating = Double.toString(toolRatings.get(starRating.getToolId()));
                session.setAttribute("toolRatings", toolRatings);
                CommonUi sRating = new CommonUi(session, starRating.getToolId());
                sRating.getRating(starRating.getToolId(), 2);
                if (session.getAttribute("sortbyInfo") != null) {
                    session.removeAttribute("totalTools");
                    Map<Integer, Tool> reSortTools = new LinkedHashMap<>();
                    String sortByInfo = (String) session.getAttribute("sortbyInfo");
                    Map<Integer, Tool> totalTools = toolService.getToolsBySorting(sortByInfo);
                    for (Map.Entry<Integer, Tool> entry : totalTools.entrySet()) {
                        Tool tools = entry.getValue();
                        reSortTools.put(tools.getToolId(), tools);
                    }
                    session.setAttribute("totalTools", totalTools);
                }
                feedbackService.jsonAvearageRating(avgrating, sRating.getRating(starRating.getToolId(), 2), response);
                return null;
            }
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "starRating",
                starRating);
        return mAndV;

    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        StarRating FeedbackForm = (StarRating) session.getAttribute("starRating");
        if (FeedbackForm == null) {
            FeedbackForm = new StarRating();
        }
        return FeedbackForm;

    }

}
