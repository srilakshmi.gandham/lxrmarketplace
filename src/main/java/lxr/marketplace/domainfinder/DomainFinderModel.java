/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.domainfinder;

import java.io.Serializable;
import org.springframework.stereotype.Component;

/**
 *
 * @author NE16T1213/ Sagar.K
 */
@Component
public class DomainFinderModel implements Serializable {

    /*Company Name*/
    private String brandKeyword;
    private String domainURL;
    private Integer keywordId;
    private int queryStatus;

    public DomainFinderModel() {
    }

    public DomainFinderModel(DomainFinderModel cloneObj) {
        this.brandKeyword = cloneObj.getBrandKeyword();
        this.domainURL = cloneObj.getDomainURL();
    }

    public String getBrandKeyword() {
        return brandKeyword;
    }

    public void setBrandKeyword(String brandKeyword) {
        this.brandKeyword = brandKeyword;
    }

    public String getDomainURL() {
        return domainURL;
    }

    public void setDomainURL(String domainURL) {
        this.domainURL = domainURL;
    }

    public Integer getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Integer keywordId) {
        this.keywordId = keywordId;
    }

    public int getQueryStatus() {
        return queryStatus;
    }

    public void setQueryStatus(int queryStatus) {
        this.queryStatus = queryStatus;
    }

}
