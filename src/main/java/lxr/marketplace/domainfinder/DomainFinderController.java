/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.domainfinder;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.GoogleCustomSearch;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.PreSalesToolUsage;
import lxr.marketplace.util.PreSalesToolUsageDAOService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author NE16T1213/Sagar k
 */
@Controller
@RequestMapping("/domain-finder.html")
public class DomainFinderController {

    private static final Logger LOGGER = Logger.getLogger(DomainFinderController.class);

    @Autowired
    private DomainFinderService domainFinderService;
    @Autowired
    private String downloadFolder;
    @Autowired
    private String uploadFolder;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PreSalesToolUsageDAOService preSalesToolUsageDAOService;

    @Value("${PrseSales.Offline.URLLimit}")
    private int offlineURLLimit;

    @Value("${Google.API.Threashold}")
    private int googleAPIThreashold;

    @RequestMapping(value = "/domain-finder.html", method = RequestMethod.GET)
    public String getDomainFinder(HttpSession session) {
        resetToolSessionObjects(session);
        return "/views/domainFinder/domainFinder";
    }

    @RequestMapping(params = {"request=analyze"}, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object[] handleDomainFinderRequestQuery(@RequestParam("brandKeywordsList") String brandKeywordsList, @RequestParam("brandKeywordsFeed") MultipartFile brandKeywordsFeed,
            @RequestParam("offlineReportEmail") String offlineReportEmail, @RequestParam("toolUsageUserEmail") String toolUsageUserEmail, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        resetToolSessionObjects(session);
        Object[] responseObj = new Object[4];
        boolean isMetricsPulled = false;
        String errorMessage = null;
        List<String> searchQueryList = null;
        List<DomainFinderModel> domainFinderList = null;
        boolean isValidInput = true;

        if (brandKeywordsList != null && !brandKeywordsList.isEmpty() && brandKeywordsFeed != null && brandKeywordsFeed.getSize() != 1 && !brandKeywordsFeed.getOriginalFilename().equals("emptyFile")) {
            isValidInput = false;
        }
        if (isValidInput) {
            if (brandKeywordsList != null && !brandKeywordsList.isEmpty()) {
                searchQueryList = Common.convertStringToArrayListV2(brandKeywordsList, "\n");
            } else if (brandKeywordsFeed != null && brandKeywordsFeed.getSize() != 1 && !brandKeywordsFeed.getOriginalFilename().equals("emptyFile")) {
                String fileName = null;
                if (brandKeywordsFeed.getOriginalFilename() != null) {
                    fileName = brandKeywordsFeed.getOriginalFilename().substring(brandKeywordsFeed.getOriginalFilename().lastIndexOf('.') + 1).toLowerCase();
                }
                if ((fileName != null) && (fileName.equalsIgnoreCase("xls") || fileName.equalsIgnoreCase("xlsx"))) {
                    fileName = Common.saveFile(brandKeywordsFeed, uploadFolder);
                    if (fileName != null) {
                        searchQueryList = domainFinderService.readKeywordsFromFile(uploadFolder.concat(fileName));
                    }
                }
            }
        }
        if (searchQueryList != null && !searchQueryList.isEmpty() && searchQueryList.size() <= googleAPIThreashold) {
            resetToolSessionObjects(session);
            domainFinderList = domainFinderService.processAndGenarteBrandKeywords(searchQueryList, session);
        }
        session.setAttribute("isDomainFinderThreadTerminated", true);
        if (domainFinderList != null && !domainFinderList.isEmpty()) {
            session.setAttribute("processedBrandKeywords", domainFinderList.size());
            isMetricsPulled = true;
            session.setAttribute("resultDomainFinderList", domainFinderList);
            responseObj[2] = domainFinderList;
            if (domainFinderList.size() > offlineURLLimit && !offlineReportEmail.equals("")) {
                LOGGER.info("Send tool analysis report to user email:: " + offlineReportEmail);
                domainFinderService.sendMailDomainFinderExcelReport(request, response, session, offlineReportEmail, downloadFolder);
            }

            /*To track tool usage*/
            if (toolUsageUserEmail != null && !toolUsageUserEmail.trim().equals("") && searchQueryList != null) {
                PreSalesToolUsage preSalesToolUsage = new PreSalesToolUsage();
                preSalesToolUsage.setToolId(46);
                preSalesToolUsage.setUserEmail(toolUsageUserEmail);
                preSalesToolUsage.setNumberOfRequests(searchQueryList.size());
                preSalesToolUsage.setCreatedDate(LocalDateTime.now());
                long responseCount = domainFinderList.stream().filter(domainFinder->(domainFinder.getDomainURL()!=null && !domainFinder.getDomainURL().equals(GoogleCustomSearch.GOOGLE_USAGE_LIMIT_ERROR))).count();
                Map<String, Integer> apiUsageList = new LinkedHashMap<>();
                /*Adding Similar web api usage*/
                apiUsageList.put(PreSalesToolUsageDAOService.GOOGLECUSTOMSEARCH, (int)responseCount);
                preSalesToolUsage.setApiList(apiUsageList);
                boolean isToolUsageTracked = preSalesToolUsageDAOService.insertPreSalesToolUsage(preSalesToolUsage);
                LOGGER.info("Tool usage tracking status::" + isToolUsageTracked + ", for requestCount:: " + searchQueryList.size() + ", for responseCount:: " + responseCount);
            }
        } else {
            session.setAttribute("processedBrandKeywords", 0);
            errorMessage = messageSource.getMessage("PreSales.tool.error", null, Locale.US);
        }
        responseObj[0] = isMetricsPulled;
        responseObj[1] = errorMessage;
        return responseObj;
    }

    @RequestMapping(params = {"request=validateKeywordsSize"}, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int validateQueryURLSSize(@RequestParam("brandKeywordsFeed") MultipartFile brandKeywordsFeeds, HttpSession session, HttpServletResponse response) {
        String fileName = null;
        List<String> searchQueryList = null;
        if (brandKeywordsFeeds != null && brandKeywordsFeeds.getSize() != 1 && !brandKeywordsFeeds.getOriginalFilename().equals("emptyFile")) {
            if (brandKeywordsFeeds.getOriginalFilename() != null) {
                fileName = brandKeywordsFeeds.getOriginalFilename().substring(brandKeywordsFeeds.getOriginalFilename().lastIndexOf('.') + 1).toLowerCase();
            }
            if ((fileName != null) && (fileName.equalsIgnoreCase("xls") || fileName.equalsIgnoreCase("xlsx"))) {
                fileName = Common.saveFile(brandKeywordsFeeds, uploadFolder);
                if (fileName != null) {
                    searchQueryList = domainFinderService.readKeywordsFromFile(uploadFolder.concat(fileName));
                }
            } else {
                fileName = null;
            }
        }
        return (fileName != null && searchQueryList != null) ? searchQueryList.size() : -1;
    }

    @RequestMapping(params = {"request=analyzedCount"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object[] getProcessedKeywordsCount(HttpSession session, HttpServletResponse response) {
        Object[] responseObj = new Object[2];
        int processedURLsCount = 0;
        boolean isThreadTerminated = false;
        if (session.getAttribute("isDomainFinderThreadTerminated") != null) {
            isThreadTerminated = (boolean) session.getAttribute("isDomainFinderThreadTerminated");
        }
        if (session.getAttribute("processedBrandKeywords") != null) {
            processedURLsCount = (int) session.getAttribute("processedBrandKeywords");
            LOGGER.debug("processedURLsCount:" + processedURLsCount);
        }
        responseObj[0] = isThreadTerminated;
        responseObj[1] = processedURLsCount;
        LOGGER.debug("Finished --> Updated count of analyzed processedBrandKeywords and isThreadTerminated: " + isThreadTerminated + ", processedURLsCount: " + processedURLsCount);
        return responseObj;
    }

    @RequestMapping(params = {"request=download"}, method = RequestMethod.POST)
    public String processDomainFinderAnalyisReport(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        try {
            String fileName = null;
            if (session.getAttribute("resultDomainFinderList") != null) {
                fileName = domainFinderService.generateDomainFinderAnalysisReport((List<DomainFinderModel>) session.getAttribute("resultDomainFinderList"), downloadFolder);
                session.setAttribute("domainFinderAnalysisReport", fileName);
            }
            LOGGER.info("Downloadble Domain Finder report: " + fileName);
            if (fileName != null && !fileName.trim().equals("")) {
                Common.downloadReport(response, downloadFolder, fileName, "xls");
                return null;
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Download reports request: ", e);
        }
        return "/views/domainFinder/domainFinder";
    }

    /*
     * Preparing Domain Finder report to send mail for user.
     */
    @RequestMapping(params = {"request=sendmail"}, method = RequestMethod.POST)
    @ResponseBody
    public boolean sendMailDomainFinderAnalyisReport(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam("email") String userEmail) {
        return domainFinderService.sendMailDomainFinderExcelReport(request, response, session, userEmail, downloadFolder);
    }

    public void resetToolSessionObjects(HttpSession session) {
        session.removeAttribute("resultDomainFinderList");
        session.removeAttribute("domainFinderAnalysisReport");
        session.removeAttribute("isDomainFinderThreadTerminated");
        session.removeAttribute("processedBrandKeywords");
    }
}
