/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.domainfinder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.GoogleCustomSearch;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.commons.collections4.ListUtils;
import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author NE16T1213/Sagar k
 */
@Service
public class DomainFinderService {

    private static final Logger LOGGER = Logger.getLogger(DomainFinderService.class);

    @Autowired
    private GoogleCustomSearch googleCustomSearch;

    @Value("${Google.Console.CustSearchApiKey}")
    private String googleCustSearchApiKey;

    @Value("${Google.Console.CustomSearchEngnId}")
    private String googleCustomSearchEngnId;

    protected List<DomainFinderModel> processAndGenarteBrandKeywords(List<String> queryBrandKeywords, HttpSession session) {
        LOGGER.info("Total analysis to be done  for:: " + queryBrandKeywords.size() + " company names");
        int corePoolSize = 55;
        int maxPoolSize = 100;
        long keepAliveTime = 2000;
        int count = 1;
        List<DomainFinderModel> domainFinderResultList = new LinkedList<>();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        List<Future<DomainFinderModel>> domainFinderFuterList = null;
        List<List<String>> splitedList = ListUtils.partition(queryBrandKeywords, 40);
        for (List<String> subList : splitedList) {
            domainFinderFuterList = new ArrayList<>();
            for (String query : subList) {
                domainFinderFuterList.add(threadPoolExecutor.submit(new DomainFinderTask(query, count, this)));
                count++;
            }
            if (!domainFinderFuterList.isEmpty()) {
                domainFinderFuterList.forEach((domainFinderFuter) -> {
                    try {
                        domainFinderResultList.add(domainFinderFuter.get());
                        session.setAttribute("processedBrandKeywords", domainFinderResultList.size());
                        if (domainFinderResultList.size() % 150 == 0) {
                            LOGGER.info("Completed analysis for :" + domainFinderResultList.size() + ", yet to be analyzed:" + (queryBrandKeywords.size() - domainFinderResultList.size()) + ", for total : " + queryBrandKeywords.size());
                        }
                    } catch (InterruptedException | ExecutionException ex) {
                        LOGGER.error("Excpetion in getting from future obj cause: ", ex);
                    }
                });
            }
            /*if (subListCount != 0 && subList.size() == 40) {
                try {
                    LOGGER.info("Completed analysis for :" + count + ", yet to be analyzed:" + (queryBrandKeywords.size() - count) + ", for total : " + queryBrandKeywords.size());
                    LOGGER.info("Making thread sleep for 20 seconds  and analyzed count: " + count);
                    Thread.sleep(1000 * 20);
                } catch (InterruptedException e) {
                    LOGGER.error("Exception in thread sleep cause:", e);
                }
            }
            subListCount++;*/
        }
        domainFinderResultList.sort((domainOne, domainTwo) -> domainOne.getKeywordId().compareTo(domainTwo.getKeywordId()));
        LOGGER.info("Completed analysis for company names count:: " + domainFinderResultList.size() + ", is porcessed allquires:: " + (queryBrandKeywords.size() == domainFinderResultList.size()));
        session.setAttribute("isDomainFinderThreadTerminated", true);
        return domainFinderResultList;
    }


    /*Looking for domain/ URL using Google Search Page API
    Below are cases domain URL value:
    1: URL: Result from google custom search page.
    2: null: If founded url is not domain its related to page URL then value will be null.
    3: Google fetch limit exceeded: When google custom search api limmit is reached.*/
    protected String getGoogleSearchDataForCompanyName(String searchQuery) {
        String domainURL = null, queryString = null;
        /*Making API call using Key#1*/
        queryString = constructGoogleSearchQuery(searchQuery, googleCustSearchApiKey);
        domainURL = googleCustomSearch.getGoogleSearchDataForaBrandKeyword(queryString);
        /*If API limit reached then making API call using Key#2
        if (domainURL != null && domainURL.equalsIgnoreCase(GoogleCustomSearch.GOOGLE_USAGE_LIMIT_ERROR)) {
            queryString = constructGoogleSearchQuery(searchQuery, googleCustSearchApiSecondKey);
            domainURL = googleCustomSearch.getGoogleSearchDataForaBrandKeyword(queryString);
        }*/
        if (domainURL != null && !domainURL.equalsIgnoreCase(GoogleCustomSearch.GOOGLE_USAGE_LIMIT_ERROR)) {
            /*Checking the URL is page URL or Domain URL*/
            String tempDomainURL = isDomainORPageURL(domainURL);
            if (tempDomainURL != null && tempDomainURL.equalsIgnoreCase("DOMAIN")) {
                domainURL = Common.getDomainWithProtocol(domainURL.trim());
            } else {
                /*If URL is categriozed as page then returning null otherwise it is considered as result*/
                domainURL = null;
            }
        }
        return domainURL;
    }

    /*Checking the URL is page URL or Domain URL 
        To handle below case:
        Query Keyword: b12, HomeWagon
        Search URL from proxy is:https://ods.od.nih.gov/factsheets/VitaminB12-HealthProfessional/
        Actual (or) looking URL is: https://www.b12.io/*/
    protected String isDomainORPageURL(String queryURL) {
        String urlType = null;
        if (queryURL != null) {
            if (queryURL.endsWith("/")) {
                queryURL = queryURL.substring(0, queryURL.lastIndexOf("/"));
            }
            try {
                /*Ref Link: https://docs.oracle.com/javase/tutorial/networking/urls/urlInfo.html*/
                URL urlObj = new URL(queryURL);
                if ((urlObj.getPath() != null && !urlObj.getPath().isEmpty()) || (urlObj.getFile() != null && !urlObj.getFile().isEmpty())) {
                    urlType = "PAGE";
//                } else if (urlObj.getProtocol() != null && urlObj.getHost() != null) {
                } else if (urlObj.getHost() != null) {
                    urlType = "DOMAIN";
                }
            } catch (MalformedURLException e) {
                LOGGER.error("Excception in isDomainORPageURL cause: ", e);
            }
        }
        return urlType;
    }

    protected String getDomainName(String url) {
        String domainName = Common.getDomainFromUrl(url);
        String[] domainsSplit = null;
        try {
            if (domainName.endsWith("/")) {
                domainName = domainName.substring(0, domainName.lastIndexOf("/"));
            }
            if (url != null) {
                domainsSplit = domainName.split("\\.", 0);
            }
            if (domainsSplit != null && domainsSplit.length != 0) {
                domainName = domainsSplit[0];
            } else {
                domainName = url;
            }
        } catch (Exception e) {
            domainName = null;
            LOGGER.error("Exeception in root domain from URL cause: ", e);
        }
        return domainName;
    }

    protected List<String> readKeywordsFromFile(String fileName) {
        List<String> keywords = null;
        Workbook workbook = null;
        Sheet sheet = null;
        Row rowObj = null;
        try {
            // Creating a Workbook from an Excel file (.xls or .xlsx)
            workbook = WorkbookFactory.create(new File(fileName));
            sheet = null;
            if (workbook != null) {
                sheet = workbook.getSheetAt(0);
            }
            if (sheet != null) {
                rowObj = sheet.getRow(0);
                if (rowObj != null) {
                    if (rowObj.getCell(0) != null && rowObj.getCell(0).getStringCellValue().trim().equalsIgnoreCase("Company Name")) {
                        keywords = new LinkedList<>();
                    }
                }
            }
//        } catch (IOException | InvalidFormatException e) {
        } catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
            LOGGER.error("IOException | InvalidFormatException in readKeywordsFromFile cause: ", e);
        }
        if (sheet != null && keywords != null) {
            for (Row row : sheet) {
                if (row != null) {
                    Cell cell = row.getCell(0);
//                    for (Cell cell : row) {
                    if (cell != null && cell.getCellTypeEnum() == CellType.STRING) {
                        if (!cell.getStringCellValue().trim().equals("") && !cell.getStringCellValue().trim().equalsIgnoreCase("Company Name")) {
                            keywords.add(cell.getStringCellValue().trim());
                        }
                    }
//                    }
                }
            }
        }
        return keywords;
    }

    protected String generateDomainFinderAnalysisReport(List<DomainFinderModel> domainFinderList, String downloadfolder) {
        String fileName = null;
        try {
            HSSFCellStyle mainheadCellSty;
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFFont headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            HSSFFont font = workBook.createFont();
            font.setBold(true);
            //for main heading
            mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            HSSFSheet sheet = workBook.createSheet("Domain Finder Analysis Report");
            sheet.setDisplayRowColHeadings(false);
            sheet.setDisplayGridlines(false);
            CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
            HSSFFont font1 = null;
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = null;
            HSSFCellStyle subColHeaderStyle = null;
            CreationHelper createHelper = workBook.getCreationHelper();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            //For logo and Reoprt name
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            cell.setCellValue("Domain Finder Analysis Report");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 2));
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 800);

            subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            cell = rows.createCell(3);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 3, 6));
            cell.setCellValue("Reviewed on: " + curDate);
            cell.setCellStyle(subColHeaderStyle);

            int rowCout = 1;
            //Report Headings
            rows = sheet.createRow(++rowCout);
            rows.setHeight((short) 150);

            rows = sheet.createRow(++rowCout);
            rows.setHeight((short) 150);

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            font1 = workBook.createFont();
            font1.setBold(true);
            columnHeaderStyle.setFont(font1);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            subColHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            subColHeaderStyle.setBorderBottom(BorderStyle.THIN);
            subColHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderRight(BorderStyle.THIN);
            subColHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderLeft(BorderStyle.THIN);
            subColHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            subColHeaderStyle.setBorderTop(BorderStyle.THIN);
            subColHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle valueCellStyle = workBook.createCellStyle();
            valueCellStyle.setFont(font);
            valueCellStyle.setWrapText(true);
            valueCellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            valueCellStyle.setAlignment(HorizontalAlignment.LEFT);
            valueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            valueCellStyle.setBorderBottom(BorderStyle.THIN);
            valueCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderRight(BorderStyle.THIN);
            valueCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderLeft(BorderStyle.THIN);
            valueCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            valueCellStyle.setBorderTop(BorderStyle.THIN);
            valueCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle valueRightCellStyle = workBook.createCellStyle();
            valueRightCellStyle.setFont(font);
            valueRightCellStyle.setWrapText(true);
            valueRightCellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            valueRightCellStyle.setAlignment(HorizontalAlignment.RIGHT);
            valueRightCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            valueRightCellStyle.setBorderBottom(BorderStyle.THIN);
            valueRightCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            valueRightCellStyle.setBorderRight(BorderStyle.THIN);
            valueRightCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            valueRightCellStyle.setBorderLeft(BorderStyle.THIN);
            valueRightCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            valueRightCellStyle.setBorderTop(BorderStyle.THIN);
            valueRightCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle metricCategoryStyle = backgroundColorStyles(workBook, "blue");
            HSSFCellStyle errorValueCellStyle = backgroundColorStyles(workBook, "red");

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            /*dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));*/
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMMM_dd_yy"));
            LOGGER.debug("Generating Report in .xls format");

            rows = sheet.createRow(++rowCout);
            rows.setHeight((short) 150);

            rows = sheet.createRow(++rowCout);
            rows.setHeight((short) 150);

            rows = sheet.createRow(++rowCout);
            rows.setHeight((short) 600);
            cell = rows.createCell(0);
            cell.setCellValue("S.No.");
            cell.setCellStyle(metricCategoryStyle);
            heaCelSty.setWrapText(true);

            cell = rows.createCell(1);
            cell.setCellValue("Company Name");
            cell.setCellStyle(metricCategoryStyle);
            heaCelSty.setWrapText(true);

            cell = rows.createCell(2);
            cell.setCellValue("Domain/URL");
            cell.setCellStyle(metricCategoryStyle);

            sheet.createFreezePane(0, rowCout + 1);
            if (domainFinderList != null && !domainFinderList.isEmpty()) {
                try {
                    int x = ++rowCout, k = 0;
                    for (int j = 0; j < domainFinderList.size(); j++) {
                        k = k + 1;
                        rows = sheet.createRow(x);
                        rows.setHeight((short) 600);
                        for (int i = 0; i <= 3; i++) {
                            cell = rows.createCell(i);
                            switch (i) {
                                case 0:
                                    cell.setCellValue(k);
                                    cell.setCellStyle(subColHeaderStyle);
                                    break;
                                case 1:
                                    cell.setCellValue(domainFinderList.get(j).getBrandKeyword());
                                    cell.setCellStyle(valueCellStyle);
                                    break;
                                case 2:
                                    cell.setCellValue((domainFinderList.get(j).getDomainURL() != null) ? domainFinderList.get(j).getDomainURL() : "No Matching Domain / URL Found");
                                    cell.setCellStyle((domainFinderList.get(j).getDomainURL() != null && !domainFinderList.get(j).getDomainURL().equalsIgnoreCase(GoogleCustomSearch.GOOGLE_USAGE_LIMIT_ERROR)) ? valueCellStyle : errorValueCellStyle);
                                    break;
                                default:
                                    break;
                            }
                        }
                        x = x + 1;
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception in creating domain finder record excel", e);
                }
            }
            if (domainFinderList == null || domainFinderList.isEmpty()) {
                rows = sheet.createRow(++rowCout);
                cell = rows.createCell(0);
                rows.setHeight((short) 500);
                sheet.addMergedRegion(new CellRangeAddress(rowCout, rowCout, 0, 3));
                cell.setCellValue("No records found");
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
                cell.setCellStyle(columnHeaderStyle);

                columnHeaderStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                columnHeaderStyle.setFont(font1);
                columnHeaderStyle.setWrapText(true);
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            }

            sheet.setColumnWidth(0, 3500);
            sheet.setColumnWidth(1, 12000);
            sheet.setColumnWidth(2, 14000);
            sheet.setColumnWidth(3, 14000);
            LOGGER.info("Domain Finder Analysis generated in .xls format");
            fileName = Common.createFileName("LXRMarketplace_Domain_Finder_Report") + ".xls";
            try (FileOutputStream stream = new FileOutputStream(new File(downloadfolder + fileName))) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (IOException e) {
            LOGGER.error("IOException in generateDomainFimderAnalysisReport: ", e);
        }
        return fileName;
    }

    private HSSFCellStyle backgroundColorStyles(HSSFWorkbook workBook, String color) {
        HSSFCellStyle cellStyle = null;
        HSSFFont font1;
        HSSFPalette palette;
        HSSFColor hssfColor = null;
        switch (color) {
            case "blue":
                cellStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                font1.setFontHeightInPoints((short) 11);
                font1.setColor(IndexedColors.WHITE.getIndex());
                cellStyle.setFont(font1);
                cellStyle.setWrapText(true);
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                palette = workBook.getCustomPalette();
                palette.setColorAtIndex(HSSFColor.AQUA.index, (byte) 34, (byte) 166, (byte) 177);
                hssfColor = palette.getColor(HSSFColor.AQUA.index);
                cellStyle.setFillForegroundColor(hssfColor.getIndex());
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                cellStyle.setBorderBottom(BorderStyle.THIN);
                cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderLeft(BorderStyle.THIN);
                cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderRight(BorderStyle.THIN);
                cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderTop(BorderStyle.THIN);
                cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
                break;
            case "gray":
                cellStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                font1.setFontHeightInPoints((short) 11);
                font1.setColor(IndexedColors.WHITE.getIndex());
                cellStyle.setFont(font1);
                cellStyle.setWrapText(true);
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                palette = workBook.getCustomPalette();
                palette.setColorAtIndex(HSSFColor.PLUM.index, (byte) 81, (byte) 81, (byte) 81);
                hssfColor = palette.getColor(HSSFColor.PLUM.index);
                cellStyle.setFillForegroundColor(hssfColor.getIndex());
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                cellStyle.setBorderBottom(BorderStyle.THIN);
                cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderLeft(BorderStyle.THIN);
                cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderRight(BorderStyle.THIN);
                cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderTop(BorderStyle.THIN);
                cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
                break;
            case "red":
                cellStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                font1.setFontHeightInPoints((short) 11);
                font1.setColor(IndexedColors.RED.getIndex());
                cellStyle.setFont(font1);
                cellStyle.setWrapText(true);
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                cellStyle.setBorderBottom(BorderStyle.THIN);
                cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderRight(BorderStyle.THIN);
                cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderLeft(BorderStyle.THIN);
                cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
                cellStyle.setBorderTop(BorderStyle.THIN);
                cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
                break;
            default:
                break;
        }

        return cellStyle;
    }

    protected boolean sendMailDomainFinderExcelReport(HttpServletRequest request, HttpServletResponse response, HttpSession session, String userEmail, String downloadFolder) {
        boolean mailStatus = false;
        try {
            String fileName = null;
            if (session.getAttribute("domainFinderAnalysisReport") != null) {
                fileName = (String) session.getAttribute("domainFinderAnalysisReport");
            } else if (session.getAttribute("resultDomainFinderList") != null) {
                fileName = generateDomainFinderAnalysisReport((List<DomainFinderModel>) session.getAttribute("resultDomainFinderList"), downloadFolder);
                session.setAttribute("domainFinderAnalysisReport", fileName);
            }
            if (fileName != null) {
                LOGGER.info("Sending report through mail for user with file name: " + fileName);
                mailStatus = Common.sendReportThroughMail(request, downloadFolder, fileName, userEmail, "Domain Finder");
                if (mailStatus) {
                    LOGGER.info("Sending report through mail for user email: " + userEmail);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception:Send report to email: " + userEmail + ", request: " + e.getMessage());
        }
        return mailStatus;
    }

    protected String constructGoogleSearchQuery(String searchQuery, String googleCustSearchApiKey) {
        String queryString = null;
        String geoGraphicLocation = "us", language = "lang_en";
        try {
            queryString = "key=" + googleCustSearchApiKey + "&cx=" + googleCustomSearchEngnId
                    + "&googlehost=google.com&gl=" + geoGraphicLocation + "&lr=" + language + "&ie=utf8&oe=utf8&filter=0&q=" + URLEncoder.encode(searchQuery, "UTF-8");
            LOGGER.debug("Query URL for GoogleSearch Data For a Keyword: " + queryString);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("UnsupportedEncodingException in encoding search query cause: ", e);
        }
        return queryString;
    }

}
