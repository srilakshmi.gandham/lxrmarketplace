/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.domainfinder;

import java.util.concurrent.Callable;
import org.apache.log4j.Logger;

/**
 *
 * @author NE16T1213/Sagar k
 */
public class DomainFinderTask implements Callable<DomainFinderModel> {

    private static final Logger LOGGER = Logger.getLogger(DomainFinderTask.class);
    private final DomainFinderService domainFinderService;
    private String companyName;
    private int index;

    public DomainFinderTask(String companyName, int index, DomainFinderService domainFinderService) {
        this.companyName = companyName;
        this.domainFinderService = domainFinderService;
        this.index = index;
    }

    @Override
    public DomainFinderModel call() {
        DomainFinderModel finder = new DomainFinderModel();
        finder.setBrandKeyword(this.companyName);
        finder.setKeywordId(this.index);
        finder.setDomainURL(getDomainForCompanyName(this.companyName, this.domainFinderService));
        return finder;
    }

    public String getDomainForCompanyName(String companyName, DomainFinderService domainFinderService) {
        return domainFinderService.getGoogleSearchDataForCompanyName(companyName);
    }
}
