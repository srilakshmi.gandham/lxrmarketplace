package lxr.marketplace.session;

import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.AutomailService;
import lxr.marketplace.admin.automail.EmailTemplate;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

@SuppressWarnings("deprecation")
public class SessionController extends SimpleFormController{
	
	private static final Logger LOGGER = Logger.getLogger(SessionController.class);
        
	SessionService sessionService;	
	@Resource(name = "jdbcTemplate")
	JdbcTemplate jdbcTemplate; 
	private LoginService loginService;
	private EmailTemplateService emailTemplateService;
	private ToolService  toolService;
	private AutomailService  automailService;
	
	public AutomailService getAutomailService() {
		return automailService;
	}
	public void setAutomailService(AutomailService automailService) {
		this.automailService = automailService;
	}
	public ToolService getToolService() {
		return toolService;
	}
	public void setToolService(ToolService  toolService){
		this.toolService = toolService;
	}
	public EmailTemplateService getEmailTemplateService() {
		return emailTemplateService;
	}
	public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
		this.emailTemplateService = emailTemplateService;
	}
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}	
	public void setSessionService(SessionService sessionService){
		this.sessionService = sessionService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	@Override
	//@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		Session userSession = (Session)session.getAttribute("userSession");
		String action = request.getParameter("action");
		int noTimesToolUsed=0;
		if(action!=null && action.equalsIgnoreCase("roiCalculator")){
			//noTimesToolUsed=toolService.fisrtToolUsage(userSession.getUserId(),3l);//to find the usage of particulartool
			Common.modifyDummyUserInToolorVideoUsage(request, response);
			noTimesToolUsed=toolService.fisrtToolUsage(userSession.getUserId());//to find the usage of anytool
			if(noTimesToolUsed == 0 && userSession.getToolUsage()== null){
				  //call send mail method
				List<EmailTemplate> emailTemplates = emailTemplateService.getTemplatesByInstantEvent(3);
				for(EmailTemplate emailTemplate: emailTemplates){
					if(emailTemplate!= null){
						SendMail sendmail = new SendMail();
						String fromAddress = automailService.ftechFromAddress(emailTemplate);
						if(fromAddress==null || fromAddress.trim().equals("")){
							/* Commented with reference to mail from Pavan Sir
                                                    fromAddress = "LXRMarketplace Team<team@lxrmarketplace.com>";*/
                                                    fromAddress = "LXRMarketplace Support<support@lxrmarketplace.com>";
                                                    
						}
				        sendmail.sendAdminMail(emailTemplate,loginService.findUser(userSession.getUserId(),jdbcTemplate),fromAddress);
						emailTemplate.setLastMailSent(Calendar.getInstance());
						emailTemplateService.updateLastMailSentDate(emailTemplate);
				        
					}
				}
				}
			
			userSession.addToolUsage(3);
		}
		if(userSession != null){
			userSession.setEndTime(Calendar.getInstance());
			//sessionService.update(userSession);
		}else{
			String url = request.getScheme() + "://" + request.getServerName()
			+ ":" + request.getServerPort();
	response.sendRedirect(url + "/lxrmarketplace.html");
	return null;
		}
		return null;
 	}
	
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}
	
	/*protected Object formBackingObject(HttpServletRequest request)
	throws Exception {
		HttpSession session = request.getSession();
		re
	}*/
}
