package lxr.marketplace.session;

import java.io.Serializable;

public class ToolUsage implements Serializable{
	long id;
	long sessionId;
	long toolId;
	int count;
	String description;
	long ipSessionId; 
	

	public long getIpSessionId() {
		return ipSessionId;
	}
	public void setIpSessionId(long ipSessionId) {
		this.ipSessionId = ipSessionId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	
	public long getToolId() {
		return toolId;
	}
	public void setToolId(long toolId) {
		this.toolId = toolId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	

    @Override
    public String toString() {
        return "ToolUsage{" + "id=" + id + ", sessionId=" + sessionId + ", toolId=" + toolId + ", count=" + count + ", description=" + description + ", ipSessionId=" + ipSessionId + '}';
    }
        
}
