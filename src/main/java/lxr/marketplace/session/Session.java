package lxr.marketplace.session;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import lxr.marketplace.admin.automail.VideoUsage;
import lxr.marketplace.reportdownload.UserReportDownloadStats;
import lxr.marketplace.user.UserAnalysisInfo;
import lxr.marketplace.util.PageEntry;

public class Session {

    long id;
    long userId;
    Calendar startTime;
    Calendar endTime;
    double duration;
    String ip;
    String os;
    String browser;
    String browserVersion;
    HashMap<Integer, ToolUsage> listToolUsage;
    HashMap<Long, VideoUsage> listVideoUsage;
    private Map<Integer, UserReportDownloadStats> listReportDownload;
    private List<PageEntry> pageEntryList = new ArrayList<>();
    
    private boolean loggedIn;
    private List<UserAnalysisInfo> userAnalysisInfoList;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public List<PageEntry> getPageEntryList() {
        return pageEntryList;
    }

    public void setPageEntryList(List<PageEntry> pageEntryList) {
        this.pageEntryList = pageEntryList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public Calendar getEndTime() {
        return endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public HashMap<Integer, ToolUsage> getToolUsage() {
        return listToolUsage;
    }

    public void setToolUsage(HashMap<Integer, ToolUsage> listToolUsage) {
        this.listToolUsage = listToolUsage;
    }

    public HashMap<Long, VideoUsage> getListVideoUsage() {
        return listVideoUsage;
    }

    public void setListVideoUsage(HashMap<Long, VideoUsage> listVideoUsage) {
        this.listVideoUsage = listVideoUsage;
    }

    public Map<Integer, UserReportDownloadStats> getListReportDownload() {
        return listReportDownload;
    }

    public void setListReportDownload(
            Map<Integer, UserReportDownloadStats> listReportDownload) {
        this.listReportDownload = listReportDownload;
    }

    

    public List<UserAnalysisInfo> getUserAnalysisInfoList() {
        return userAnalysisInfoList;
    }

    public void setUserAnalysisInfoList(List<UserAnalysisInfo> userAnalysisInfoList) {
        this.userAnalysisInfoList = userAnalysisInfoList;
    }
	
    public void addToolUsage(int toolId) {
        ToolUsage toolUsage;
        if (listToolUsage == null) {
            listToolUsage = new HashMap<>();
        }
        toolUsage = (ToolUsage) listToolUsage.get(toolId);
        if (toolUsage == null) {
            toolUsage = new ToolUsage();
            toolUsage.setToolId(toolId);
            toolUsage.setSessionId(this.getId());
            listToolUsage.put(toolId, toolUsage);
        }
        toolUsage.setCount(toolUsage.getCount() + 1);
    }

    public void addVideoUsage(long videoId) {
        VideoUsage videoUsage;
        if (listVideoUsage == null) {
            listVideoUsage = new HashMap<>();
        }
        videoUsage = (VideoUsage) listVideoUsage.get(videoId);
        if (videoUsage == null) {
            videoUsage = new VideoUsage();
            videoUsage.setVideoId(videoId);
            videoUsage.setSessionId(this.getId());
            listVideoUsage.put(videoId, videoUsage);
        }
        videoUsage.setCount(videoUsage.getCount() + 1);
    }

    public void addReportDownload(int reportId) {
        if (listReportDownload == null) {
            listReportDownload = new HashMap<>();
        }
        UserReportDownloadStats repDown = (UserReportDownloadStats) listReportDownload.get(reportId);
        if (repDown == null) {
            repDown = new UserReportDownloadStats(reportId);
            repDown.setSessionId(this.getId());
            listReportDownload.put(reportId, repDown);
        }
        repDown.addReportDownload();
    }

   
    
//    User tracking information stored in session
    public void addUserAnalysisInfo(long userId, int toolId, String websiteName, String analysisIssues, 
            String suggestedQuestion, String userOtherInputJson, String toolAnalysisUrl){
        Timestamp currentTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        if(userAnalysisInfoList == null){
            userAnalysisInfoList = new ArrayList();
        }
        UserAnalysisInfo userAnalysisInfo = new UserAnalysisInfo();
        userAnalysisInfo.setUserId(userId);
        userAnalysisInfo.setToolId(toolId);
        userAnalysisInfo.setWebsiteName(websiteName);
        userAnalysisInfo.setAnalysisIssues(analysisIssues);
        userAnalysisInfo.setSuggestedQuestion(suggestedQuestion);
        userAnalysisInfo.setUserInputs(userOtherInputJson);
        userAnalysisInfo.setAnalysisTime(currentTime);
        userAnalysisInfo.setToolUrl(toolAnalysisUrl);
        userAnalysisInfoList.add(userAnalysisInfo);
    }

    @Override
    public String toString() {
        return "Session{" + "id=" + id + ", userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime + ", duration=" + duration + ", ip=" + ip + ", os=" + os + ", browser=" + browser + ", browserVersion=" + browserVersion + ", listToolUsage=" + listToolUsage + ", listVideoUsage=" + listVideoUsage + ", listReportDownload=" + listReportDownload + ", pageEntryList=" + pageEntryList + ", loggedIn=" + loggedIn + ", userAnalysisInfoList=" + userAnalysisInfoList + '}';
    }
    
    
}
