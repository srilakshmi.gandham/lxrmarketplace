package lxr.marketplace.session;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lxr.marketplace.admin.automail.VideoUsage;
import lxr.marketplace.apiaccess.GeoBytesIpLocationAPIService;
import lxr.marketplace.apiaccess.lxrmbeans.GeoBytesIpInfo;
import lxr.marketplace.reportdownload.UserReportDownloadStats;
import lxr.marketplace.user.UserAnalysisInfo;
import lxr.marketplace.util.PageEntry;
import org.apache.log4j.Logger;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class SessionService extends JdbcDaoSupport {

    private final static Logger LOGGER = Logger.getLogger(SessionService.class);

    public Session find(long id) {
        final String query = "SELECT id, user_id, start_time, end_time, duration FROM session WHERE id = ? ";
        try {
            Session session = (Session) getJdbcTemplate().queryForObject(query,
                    new Object[]{id}, new RowMapper<Object>() {
                Session session;

                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    session = new Session();
                    session.setId(rs.getLong(1));
                    session.setUserId(rs.getLong(2));
                    Calendar startTime = Calendar.getInstance();
                    startTime.setTimeInMillis(rs.getTimestamp(3).getTime());
                    session.setStartTime(startTime);
                    if (rs.getTimestamp(4) != null) {
                        Calendar endTime = Calendar.getInstance();
                        endTime.setTimeInMillis(rs.getTimestamp(4).getTime());
                        session.setStartTime(endTime);
                    }
                    session.setDuration(rs.getDouble(5));
                    return session;
                }
            });
            return session;
        } catch (Exception e) {
            LOGGER.error(e);

        }
        return null;
    }

    public List<Session> listAll() {
        String query = "SELECT id, user_id, start_time, end_time, duration FROM session ";
        try {
            List<Session> sessions = getJdbcTemplate().query(query,
                    new BeanPropertyRowMapper<Session>(Session.class));
            return sessions;
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return null;
    }

    public long insert(final Session session) {
       long sessionId = 0;
        final String query = "INSERT INTO session (user_id, start_time, ip, os, browser, browser_version) VALUES (?, ?, ?, ?, ?, ?) ";
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {
                getJdbcTemplate().update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                    statement.setLong(1, session.getUserId());
                    statement.setTimestamp(2, new Timestamp(session.getStartTime().getTimeInMillis()));
                    statement.setString(3, session.getIp());
                    statement.setString(4, session.getOs());
                    statement.setString(5, session.getBrowser());
                    statement.setString(6, session.getBrowserVersion());
                    return statement;
                }, keyHolder);
                session.setId(keyHolder.getKey().longValue());
            }
            sessionId = (Long) keyHolder.getKey();
        } catch (Exception ex) {
            LOGGER.error("Exception in inserting newly created session cause:: ", ex);
        }
        return sessionId;
    }

    public void update(final Session session) {
        if (session != null) {
            if (session.getId() != 0) {
                String query = "update session set start_time = ?, end_time = ?, duration = ?, is_logged_in = ?  where id = ?";
                try {
                    getJdbcTemplate().update(query,
                            new Object[]{
                                new Timestamp(session.getStartTime().getTimeInMillis()),
                                new Timestamp(session.getEndTime().getTimeInMillis()),
                                session.getDuration(),
                                session.isLoggedIn(),
                                session.getId()});
                } catch (Exception ex) {
                    LOGGER.error("Exception in update method" + ex.getMessage());
                }
                insertChild(session.getToolUsage());
                insertVideoUsage(session.getListVideoUsage());
                inserPageTracking(session.getPageEntryList(), session.getId());
                insertReportDownloadStats(session.getListReportDownload());
                if (session.getUserAnalysisInfoList() != null) {
                    insertUserAnalysisInfo(session.getUserAnalysisInfoList());
                }
            }
        }
    }

    public void updateUserId(final Session session) {
        String query = "update session set user_id = ? where id = ?";
        try {
            getJdbcTemplate().update(query,
                    new Object[]{
                        session.getUserId(),
                        session.getId()});
        } catch (Exception ex) {
            LOGGER.error("Exception in updateUserId method" + ex.getMessage());
        }
    }

    public void delete(int id) {
        String query = "delete from session where id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{id});
        } catch (Exception ex) {
            LOGGER.error("Exception in delete method", ex);
        }
    }

    public void insertChild(HashMap<Integer, ToolUsage> listToolUsage) {
        final String sql = "INSERT INTO tool_usage (session_id, tool_id, count) VALUES (?, ?, ?) ";
        final ToolUsage[] toolUsageArray;
        logger.debug("Updating user tools usage data :: "+listToolUsage);
        if (listToolUsage != null && !listToolUsage.isEmpty()) {
            toolUsageArray = (ToolUsage[]) listToolUsage.values().toArray(new ToolUsage[listToolUsage.size()]);
            try {
                getJdbcTemplate().batchUpdate(sql,
                        new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i)
                            throws SQLException {
                        ToolUsage toolUsage = toolUsageArray[i];
                        ps.setLong(1, toolUsage.getSessionId());
                        ps.setLong(2, toolUsage.getToolId());
                        ps.setInt(3, toolUsage.getCount());
                    }

                    @Override
                    public int getBatchSize() {
                        return toolUsageArray.length;
                    }
                });
            } catch (Exception e) {
                logger.error("Exception in insertChild cause:: " + e.getMessage());
            }
            LOGGER.debug("inserted tool usage data into database");
        }
    }

    //Insert new Ip information
    public void insertIntoLxrmSessionByIpTab(final Session session, final List<ToolUsage> newIdList) {
        final String sql = "INSERT INTO lxrm_session_by_ip(raw_ip,conv_ip,tool_id,count) values(?,INET_ATON(?),?,?);";
        LOGGER.debug("insertIntoLxrmSessionByIpTab query........." + sql);
        LOGGER.debug("Ip to track session by ip:: " + session.getIp() + ", and Session obj........." + session);
        LOGGER.debug("List of tool Ids:: " + newIdList);
        getJdbcTemplate().batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i)
                    throws SQLException {
                ToolUsage toolUsage = newIdList.get(i);
                ps.setString(1, session.getIp());
                ps.setString(2, session.getIp());
                ps.setLong(3, toolUsage.getToolId());
                ps.setInt(4, toolUsage.getCount());
            }

            @Override
            public int getBatchSize() {
                return newIdList.size();
            }
        });
        LOGGER.debug("inserted tool usage data into LxrmSessionByIpTab");
    }

    //Updating existed IP information
    public void updateLxrmSessionByIpTab(final Session session, final List<ToolUsage> matchedIdList) {

        final String sql = "update lxrm_session_by_ip set count = ? where id = ?";

        getJdbcTemplate().batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i)
                    throws SQLException {
                ToolUsage toolUsage = matchedIdList.get(i);
                ps.setInt(1, toolUsage.getCount());
                ps.setLong(2, toolUsage.getIpSessionId());
            }

            @Override
            public int getBatchSize() {
                return matchedIdList.size();
            }
        });
        LOGGER.debug("updated tool usage data into LxrmSessionByIpTab");
    }

    public Long findIpUsingToolId(String ip_addr, long tool_id) {
        final String query = "select id from lxrm_session_by_ip where raw_ip=? and tool_id =?";
        try {
            Long ip_Id = getJdbcTemplate().query(query, new Object[]{ip_addr, tool_id}, (ResultSet rs) -> {
                Long id = null;
                try {
                    if (rs.next()) {
                        id = rs.getLong("id");
                    }
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
                return id;
            });
            return ip_Id;
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
        return null;
    }

    public SqlRowSet findIpInLxrmSessionByIpTab(String ip_addr) {
        SqlRowSet ipInfoRowSet = null;
        String query = "select * from lxrm_session_by_ip where raw_ip='" + ip_addr + "'";
        LOGGER.debug("findIpInLxrmSessionByIpTab query " + query);
        ipInfoRowSet = getJdbcTemplate().queryForRowSet(query);
        return ipInfoRowSet;
    }

    public boolean findIpInCountryTab(String ip_addr) {
        String query = "select count(*) from ip_country where raw_ip='" + ip_addr + "'";
        LOGGER.debug("findIpInCountryTab query ........." + query);
        int n = getJdbcTemplate().queryForObject(query, Integer.class);
        return n != 0;
    }

    public void checkIpAndManipulateData(final Session session) {
        LOGGER.debug("in side of checkIpAndManipulateData....");
        SqlRowSet ipInfoRowSet = null;
        List<ToolUsage> matchedIdList = new ArrayList<>();
        List<ToolUsage> newIdList = new ArrayList<>();
        if (session != null) {
            HashMap<Integer, ToolUsage> listToolUsage = session.getToolUsage();
            ipInfoRowSet = findIpInLxrmSessionByIpTab(session.getIp());
            boolean matched = false;
            if (listToolUsage != null && listToolUsage.size() > 0) {

                boolean containData = ipInfoRowSet.isBeforeFirst();
                if (containData) {
                    for (ToolUsage toolUsage : listToolUsage.values()) {
                        matched = false;
                        ipInfoRowSet.beforeFirst();
                        while (ipInfoRowSet.next()) {
                            if (toolUsage.getToolId() == ipInfoRowSet.getLong("tool_id")) {
                                toolUsage.setIpSessionId(ipInfoRowSet.getInt("id"));
                                toolUsage.setCount(ipInfoRowSet.getInt("count") + toolUsage.getCount());
                                matched = true;
                                matchedIdList.add(toolUsage);
                                break;
                            }
                        }
                        if (!matched) {
                            newIdList.add(toolUsage);
                        }
                    }
                } else {
                    listToolUsage.values().stream().forEach((toolUsage) -> {
                        newIdList.add(toolUsage);
                    });
                }

            }
            if (newIdList.size() > 0) {
                insertIntoLxrmSessionByIpTab(session, newIdList);
            }
            if (matchedIdList.size() > 0) {
                updateLxrmSessionByIpTab(session, matchedIdList);
            }
        }
    }

    public void CheckAndInsertInIpCountryTab(Session session, GeoBytesIpLocationAPIService geoBytesIpLocationAPIService) {
        boolean ipPresent = findIpInCountryTab(session.getIp());
        if (!ipPresent) {
            try {
                GeoBytesIpInfo geoBytesIpInfo = new GeoBytesIpInfo(session.getIp());
                geoBytesIpInfo = geoBytesIpLocationAPIService.getCountryFromIpinfodb(session.getIp(), geoBytesIpInfo);
                insertInIpCountryTab(session.getIp(), geoBytesIpInfo);
            } catch (InterruptedException | IOException | ParseException e) {
                LOGGER.error(e);
            }
        }
    }

    public void insertInIpCountryTab(String ipAddress, GeoBytesIpInfo geoBytesIpInfo) {
        final String query = "insert into ip_country(raw_ip,country,region_name,city_name) values(?,?,?,?)";
        Object[] valLis = new Object[4];

        valLis[0] = geoBytesIpInfo.getIp();
        valLis[1] = geoBytesIpInfo.getCountry();
        valLis[2] = geoBytesIpInfo.getRegion();
        valLis[3] = geoBytesIpInfo.getCity();
        try {
            getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }

    public void insertListInIpCountryTab(final List<GeoBytesIpInfo> geoBytesIpInfoList) {
        final String query = "insert into ip_country(raw_ip,country,region_name,city_name) values(?,?,?,?)";
        LOGGER.debug("inside of  insertListInIpCountryTab " + query);
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                GeoBytesIpInfo geoBytesIpInfo = geoBytesIpInfoList.get(i);
                ps.setString(1, geoBytesIpInfo.getIp());
                if (geoBytesIpInfo.getCountry() != null) {
                    ps.setString(2, geoBytesIpInfo.getCountry());
                } else {
                    ps.setString(2, "");
                }
                if (geoBytesIpInfo.getRegion() != null) {
                    ps.setString(3, geoBytesIpInfo.getRegion());
                } else {
                    ps.setString(3, "");
                }
                if (geoBytesIpInfo.getCity() != null) {
                    ps.setString(4, geoBytesIpInfo.getCity());
                } else {
                    ps.setString(4, "");
                }
            }

            @Override
            public int getBatchSize() {
                return geoBytesIpInfoList.size();
            }
        });
    }

    public void insertVideoUsage(final HashMap<Long, VideoUsage> listVideoUsage) {
        final String sql = "INSERT INTO video_usage (session_id, video_id, count) VALUES (?, ?, ?) ";
        final VideoUsage[] videoUsageArray;

        if (listVideoUsage != null && !listVideoUsage.isEmpty()) {
            videoUsageArray = (VideoUsage[]) listVideoUsage.values().toArray(new VideoUsage[listVideoUsage.size()]);
            try {
                getJdbcTemplate().batchUpdate(sql,
                        new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i)
                            throws SQLException {
                        VideoUsage videoUsage = videoUsageArray[i];
                        ps.setLong(1, videoUsage.getSessionId());
                        ps.setLong(2, videoUsage.getVideoId());
                        ps.setInt(3, videoUsage.getCount());
                    }

                    @Override
                    public int getBatchSize() {
                        return videoUsageArray.length;
                    }
                });
            } catch (Exception e) {
                logger.error("Exception in insertVideoUsage cause:: " + e.getMessage());
            }
            LOGGER.debug("inserted video usage data into database");
        }
    }

    public void insertReportDownloadStats(final Map<Integer, UserReportDownloadStats> listReportDownload) {
        final String sql = "INSERT INTO lxr_report_downloads (session_id, report_id, count) VALUES (?, ?, ?) ";
        final UserReportDownloadStats[] userReportDownloadStatsArray;

        if (listReportDownload != null && !listReportDownload.isEmpty()) {
            userReportDownloadStatsArray = (UserReportDownloadStats[]) listReportDownload.values()
                    .toArray(new UserReportDownloadStats[listReportDownload.size()]);
            try {
                getJdbcTemplate().batchUpdate(sql,
                        new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i)
                            throws SQLException {
                        UserReportDownloadStats userReportDownloadStats = userReportDownloadStatsArray[i];
                        ps.setLong(1, userReportDownloadStats.getSessionId());
                        ps.setLong(2, userReportDownloadStats.getReportId());
                        ps.setInt(3, userReportDownloadStats.getCount());
                    }

                    @Override
                    public int getBatchSize() {
                        return userReportDownloadStatsArray.length;
                    }
                });
            } catch (Exception e) {
                logger.error("Exception in insertReportDownloadStats cause:: " + e.getMessage());
            }
            LOGGER.debug("inserted report download data into database");
        }
    }

    private void inserPageTracking(final List<PageEntry> pageEntryList, final long sessionId) {
        final String sql = "INSERT INTO user_webpage_tracker (session_id, request_uri, request_query, entry_time) VALUES (?, ?, ?, ?) ";
        if (pageEntryList != null && !pageEntryList.isEmpty()) {
            try {
                getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i)
                            throws SQLException {
                        PageEntry pageEntry = pageEntryList.get(i);
                        ps.setLong(1, sessionId);
                        ps.setString(2, pageEntry.getRequestUri());
                        ps.setString(3, pageEntry.getRequestQuery());
                        ps.setTimestamp(4, new Timestamp(pageEntry.getEntryTime().getTimeInMillis()));
                    }

                    @Override
                    public int getBatchSize() {
                        return pageEntryList.size();
                    }
                });
            } catch (Exception e) {
                logger.error("Exception in inserPageTracking cause:: " + e.getMessage());
            }
        }
    }

    public void deleteKeywordAnalyzerTable(String tablename) {
        LOGGER.debug("deleting db table for keyword analyzer with name: " + tablename);
        getJdbcTemplate().execute("drop table " + tablename);
    }

    public int checkUser(long id) {
        if (id > 0) {
            int noSession = 0;
            String query = "select COUNT(id) from session where user_id= ? ";
            try {
                noSession = getJdbcTemplate().queryForObject(query, new Object[]{id}, Integer.class);
                LOGGER.debug("Number of sessions for user " + id + " are " + noSession);
                return noSession;
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        }
        return (Integer) null;
    }

    public String[] fetchingCountryAndRegion(Session session, GeoBytesIpLocationAPIService geoBytesIpLocationAPIService) {

        try {
            GeoBytesIpInfo geoBytesIpInfo = new GeoBytesIpInfo(session.getIp());
            geoBytesIpInfo = geoBytesIpLocationAPIService.getCountryFromIpinfodb(session.getIp(), geoBytesIpInfo);
            LOGGER.debug("User IP: " + session.getIp() + " , Country: " + geoBytesIpInfo.getCountry() + " , Region: " + geoBytesIpInfo.getRegion());
            return new String[]{geoBytesIpInfo.getCountry(), geoBytesIpInfo.getRegion()};
        } catch (InterruptedException | IOException | ParseException e) {
            LOGGER.error("Exception in fetching Country and Region: /n", e);
        }
        return null;
    }

    public void insertUserAnalysisInfo(List<UserAnalysisInfo> userAnalysisInfoList) {

        final String query = "INSERT INTO lxrm_websites_tracking (lxrm_user_id, lxrm_tool_id, website, created_date, "
                + "issues, suggested_question, user_input, tool_url) values(?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            if (userAnalysisInfoList != null && !userAnalysisInfoList.isEmpty()) {
                getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        // TODO Auto-generated method stub
                        UserAnalysisInfo userAnalysisInfo = userAnalysisInfoList.get(i);
                        ps.setLong(1, userAnalysisInfo.getUserId());
                        ps.setLong(2, userAnalysisInfo.getToolId());
                        ps.setString(3, userAnalysisInfo.getWebsiteName());
                        ps.setTimestamp(4, userAnalysisInfo.getAnalysisTime());
                        ps.setString(5, userAnalysisInfo.getAnalysisIssues());
                        ps.setString(6, userAnalysisInfo.getSuggestedQuestion());
                        ps.setString(7, userAnalysisInfo.getUserInputs());
                        ps.setString(8, userAnalysisInfo.getToolUrl());
                    }

                    @Override
                    public int getBatchSize() {
                        return userAnalysisInfoList.size();
                    }
                });
            }
        } catch (Exception e) {
            logger.error("Exception in insertUserAnalysisInfo cause:: ", e);
        }
    }
}
