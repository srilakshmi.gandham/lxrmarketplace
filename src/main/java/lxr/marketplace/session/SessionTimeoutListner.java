package lxr.marketplace.session;

import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lxr.marketplace.apiaccess.GeoBytesIpLocationAPIService;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.feedback.FeedbackService;
import lxr.marketplace.keywordrankchecker.KeywordRankCheckerService;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.ToolService;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SessionTimeoutListner implements HttpSessionListener {

    private static final Logger LOGGER = Logger.getLogger(SessionTimeoutListner.class);
    
    private static int activeSessions;
    private SessionService sessionService;
    private ToolService toolService;
    private FeedbackService feedbackService;
    private KeywordRankCheckerService keywordRankCheckerService;
    private GeoBytesIpLocationAPIService geoBytesIpLocationAPIService;
    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.ctx = ctx;
    }

    public static int getActiveSession() {
        return activeSessions;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        activeSessions++;
        HttpSession session = httpSessionEvent.getSession();
        Session userSession = new Session();
        Calendar startTime = Calendar.getInstance();
        userSession.setStartTime(startTime);
        LOGGER.debug("sessionCreated - added one session into counter at " + userSession.getStartTime().getTime());
        if (toolService == null) {
            ApplicationContext ctx = WebApplicationContextUtils
                    .getWebApplicationContext(session.getServletContext());
            toolService = (ToolService) ctx.getBean("toolService");
        }
        Map<Integer, Double> toolRatings = toolService.getToolRatings();
        session.setAttribute("toolRatings", toolRatings);
        session.setAttribute("userSession", userSession);
        session.setAttribute("userLoggedIn", false);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        activeSessions--;
        HttpSession httpSession = httpSessionEvent.getSession();
        ApplicationContext ctx = WebApplicationContextUtils
                .getWebApplicationContext(httpSession.getServletContext());
        if (httpSession.getAttribute("email") != null) {
            httpSession.removeAttribute("email");
        }
        if (sessionService == null) {
            sessionService = (SessionService) ctx.getBean("sessionService");
        }
        Session userSession = (Session) httpSession.getAttribute("userSession");
        if (userSession != null && userSession.getUserId() > 0) {
            if (userSession.getEndTime() == null) {
                Calendar endTime = Calendar.getInstance();
                userSession.setEndTime(endTime);
            }
            Map<Integer, ToolUsage> toolUsageInfo = (Map<Integer, ToolUsage>) userSession.getToolUsage();
            if (toolUsageInfo != null && toolUsageInfo.get(10) != null) {
                keywordRankCheckerService = (KeywordRankCheckerService) ctx.getBean("keywordRankCheckerService");
                Long rankUserId = keywordRankCheckerService.fetchRankUserIdFromRankUserGuest(userSession.getUserId());
                if (rankUserId != null && rankUserId > 0) {
                    keywordRankCheckerService.deleteGuestUserDataByRankUserId(rankUserId);
                }
            }
            userSession.setDuration((userSession.getEndTime().getTimeInMillis()
                    - userSession.getStartTime().getTimeInMillis()) / 1000);
            if (httpSession.getAttribute("keywordAnalyzerTempTable") != null) {
                sessionService.deleteKeywordAnalyzerTable(httpSession.getAttribute("keywordAnalyzerTempTable").toString());
                httpSession.removeAttribute("keywordAnalyzerTempTable");
            }
            Login user = (Login) httpSession.getAttribute("user");
            feedbackService = (FeedbackService) ctx.getBean("feedbackService");
            geoBytesIpLocationAPIService = (GeoBytesIpLocationAPIService) ctx.getBean("geoBytesIpLocationAPIService");
            feedbackService.deleteTemporaryFolder(user.getId());
            sessionService.update(userSession);
            sessionService.checkIpAndManipulateData(userSession);
            sessionService.CheckAndInsertInIpCountryTab(userSession, geoBytesIpLocationAPIService);
        }
    }
}
