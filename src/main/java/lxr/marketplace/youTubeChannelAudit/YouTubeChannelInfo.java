/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.youTubeChannelAudit;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author vidyasagar.korada
 */
public class YouTubeChannelInfo {

    /*Root Key in JSON::  items->id*/
    private String channelId;
    /*CommonParent key: snippet*/
 /*Key in JSON::  title*/
    private String title;
    /*Key in JSON:: description*/
    private String description;
    /*Key in JSON::  publishedAt*/
    private String publishedAt;
    /*Key in JSON:: thumbnails->default->width->url*/
    private String thumbnailsURL;

    private String country;
    private long subscribersCount;
    private int videosCount;
    private long videosViewsCount;
    private long videosCommentsCount;
    private int playListCount;

    private List<ChannelVideoInfo> videoInfoList;

    private LocalDateTime analysisDate;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getThumbnailsURL() {
        return thumbnailsURL;
    }

    public void setThumbnailsURL(String thumbnailsURL) {
        this.thumbnailsURL = thumbnailsURL;
    }

    public int getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(int videosCount) {
        this.videosCount = videosCount;
    }

    public long getVideosViewsCount() {
        return videosViewsCount;
    }

    public void setVideosViewsCount(long videosViewsCount) {
        this.videosViewsCount = videosViewsCount;
    }

    public int getPlayListCount() {
        return playListCount;
    }

    public void setPlayListCount(int playListCount) {
        this.playListCount = playListCount;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getVideosCommentsCount() {
        return videosCommentsCount;
    }

    public void setVideosCommentsCount(long videosCommentsCount) {
        this.videosCommentsCount = videosCommentsCount;
    }

    public List<ChannelVideoInfo> getVideoInfoList() {
        return videoInfoList;
    }

    public void setVideoInfoList(List<ChannelVideoInfo> videoInfoList) {
        this.videoInfoList = videoInfoList;
    }

    public LocalDateTime getAnalysisDate() {
        return analysisDate;
    }

    public void setAnalysisDate(LocalDateTime analysisDate) {
        this.analysisDate = analysisDate;
    }

    public long getSubscribersCount() {
        return subscribersCount;
    }

    public void setSubscribersCount(long subscribersCount) {
        this.subscribersCount = subscribersCount;
    }

    @Override
    public String toString() {
        return "YouTubeChannelInfo{" + "channelId=" + channelId + ", title=" + title + ", description=" + description + ", publishedAt=" + publishedAt + ", thumbnailsURL=" + thumbnailsURL + ", country=" + country + ", subscribersCount=" + subscribersCount + ", videosCount=" + videosCount + ", videosViewsCount=" + videosViewsCount + ", videosCommentsCount=" + videosCommentsCount + ", playListCount=" + playListCount + ", videoInfoList=" + videoInfoList + '}';
    }

}
