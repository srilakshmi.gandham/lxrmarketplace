/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.youTubeChannelAudit;

import java.util.List;

/**
 *
 * @author vidyasagar.korada
 */
public class ChannelPlayListInfo {
    
    private String playListId;
    private String playListName;
    private int totalVideos;
    private int playListVideosViews;
    private List<ChannelVideoInfo> channelVideos;

    public String getPlayListId() {
        return playListId;
    }

    public void setPlayListId(String playListId) {
        this.playListId = playListId;
    }

    public String getPlayListName() {
        return playListName;
    }

    public void setPlayListName(String playListName) {
        this.playListName = playListName;
    }

    public int getTotalVideos() {
        return totalVideos;
    }

    public void setTotalVideos(int totalVideos) {
        this.totalVideos = totalVideos;
    }

    public int getPlayListVideosViews() {
        return playListVideosViews;
    }

    public void setPlayListVideosViews(int playListVideosViews) {
        this.playListVideosViews = playListVideosViews;
    }

    public List<ChannelVideoInfo> getChannelVideos() {
        return channelVideos;
    }

    public void setChannelVideos(List<ChannelVideoInfo> channelVideos) {
        this.channelVideos = channelVideos;
    }
    
    
            
    
}
