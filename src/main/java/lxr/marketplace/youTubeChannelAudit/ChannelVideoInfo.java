/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.youTubeChannelAudit;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author vidyasagar.korada
 */
public class ChannelVideoInfo {

    private String videoId;
    private String videoThumbnailURL;

    private String videoURL;
    private String videoTitle;
    private String videoDescription;
    private List<String> tags;
    private String videoPublishedAt;

    private int likesCount;
    private int dislikesCount;
    private int viewsCount;
    private int commentsCount;
    private int favoriteCount;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getDislikesCount() {
        return dislikesCount;
    }

    public void setDislikesCount(int dislikesCount) {
        this.dislikesCount = dislikesCount;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getVideoPublishedAt() {
        return videoPublishedAt;
    }

    public void setVideoPublishedAt(String videoPublishedAt) {
        this.videoPublishedAt = videoPublishedAt;
    }

    public String getVideoThumbnailURL() {
        return videoThumbnailURL;
    }

    public void setVideoThumbnailURL(String videoThumbnailURL) {
        this.videoThumbnailURL = videoThumbnailURL;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

//    @Override
//    public String toString() {
//        return "ChannelVideoInfo{" + "videoId=" + videoId + ", videoThumbnailURL=" + videoThumbnailURL + ", videoURL=" + videoURL + ", videoTitle=" + videoTitle + ", videoDescription=" + videoDescription + ", tags=" + tags + ", videoPublishedAt=" + videoPublishedAt + ", likesCount=" + likesCount + ", dislikesCount=" + dislikesCount + ", viewsCount=" + viewsCount + ", commentsCount=" + commentsCount + ", favoriteCount=" + favoriteCount + '}';
//    }
    
}
