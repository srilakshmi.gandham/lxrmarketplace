/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.youTubeChannelAudit;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import lxr.marketplace.util.CellStyleParamters;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ExcellUtillReportServce;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author vidyasagar.korada
 */
public class YouTubeChannelAuditReportService {

    private static final Logger LOGGER = Logger.getLogger(YouTubeChannelAuditReportService.class);

    private static final String API_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String USER_FORMAT = "MMMM dd, yyyy HH:mm 'GMT'";
    private static final String SEPARATOR = ", ";
    private static final String logoPath = "/disk2/lxrmarketplace/images/LXRMarketplace.png";

    private static final XSSFColor TITLE_FONT_INDEX = new XSSFColor(new Color(245, 129, 42));

    private static String FONT_STYLE = "Calibri";
    private static short NORMAL_TEXT_SIZE = 12;
    private static short SUBHEADING_TEXT_SIZE = 18;

    private static final String SHEET_TITLE = "YouTube Channel Audit Report";
    private static final String SHEET_LABEL = "YouTube Channel Audit";

    public String generateYouTubeChannelAuditReports(YouTubeChannelInfo youTubeChannelInfo, String downloadFolder, String fileName) {

        File file = null;
        String filePath = null;
        XSSFWorkbook workBookObj = null;
        XSSFSheet sheetObj = null;
        Row rowObj = null;
        Cell cellObj = null;
        CellRangeAddress cellRangeAddress = null;
        CellStyle mainheadCellSty = null, subColHeaderStyle = null, valueCellStyle = null;
        CellStyleParamters styleParams = null;
        workBookObj = new XSSFWorkbook();
        sheetObj = workBookObj.createSheet(WordUtils.capitalize(SHEET_LABEL));
        sheetObj.setDisplayRowColHeadings(false);
        sheetObj.setDisplayGridlines(false);

        /*Creating sheet # 1 for mobile device*/
        if (youTubeChannelInfo != null) {

            Runtime runtime = Runtime.getRuntime();
            runtime.freeMemory();

            int rowCount = 0;

            /*Row #1: Adding LXRM logo image to sheet.*/
            ExcellUtillReportServce.appendImageToExcellCell(workBookObj, sheetObj, (short) 0, 0, (short) 1, 1, logoPath);

            /*Adding the sheet title*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, (short) 29, true, TITLE_FONT_INDEX, FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);

            /*getting tsyle object*/
            mainheadCellSty = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            rowObj = sheetObj.createRow(rowCount);
            rowObj.setHeight((short) 800);
            cellObj = rowObj.createCell(1);
            cellObj.setCellValue(SHEET_TITLE);
            sheetObj.addMergedRegion(new CellRangeAddress(0, 0, 1, 10));
            cellObj.setCellStyle(mainheadCellSty);

            /*Analysis time*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, (short) 13, Boolean.TRUE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            subColHeaderStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 600);

            cellObj = rowObj.createCell(12);
            cellObj.setCellValue("Analysis time: " + youTubeChannelInfo.getAnalysisDate());
            sheetObj.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 18));
            cellObj.setCellStyle(subColHeaderStyle);

            /*Row #2: Adding the revied URL*/
 /*Preparmg style params*/
            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, (short) 14, Boolean.FALSE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            styleParams.setHaveBorder(Boolean.TRUE);
            styleParams.setBorderColorIndex(IndexedColors.BLACK.index);
            styleParams.setBorderType("all");
            /*getting tsyle object*/
            subColHeaderStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);
            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 800);

            cellObj = rowObj.createCell(0);
            cellRangeAddress = new CellRangeAddress(rowCount, rowCount, 0, 5);
            sheetObj.addMergedRegion(cellRangeAddress);
            cellObj.setCellValue(youTubeChannelInfo.getTitle());
            cellObj.setCellStyle(subColHeaderStyle);
            ExcellUtillReportServce.appplyBorderToMergedCells(cellRangeAddress, sheetObj, styleParams);
            /*Incremnt row count*/
            ++rowCount;
            ++rowCount;
            ++rowCount;

            generateVideoAuditSheet(workBookObj, sheetObj, rowCount, youTubeChannelInfo);

        }/*EOF of pageSpeedInsights object*/

 /*Saving the file*/
        filePath = fileName + ".xlsx";
        file = new File(downloadFolder + filePath);
        try {
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBookObj.write(stream);
                stream.close();
            }
        } catch (IOException e) {
            LOGGER.error("Exception in wrting into streams cause:: " + e);
        }
        LOGGER.info("FilePath to get PSI generated report is :: " + filePath);
        return filePath;
    }

   private void generateVideoAuditSheet(Workbook workBookObj, Sheet sheetObj, int rowCount, YouTubeChannelInfo youTubeChannelInfo) {
        Row rowObj = null;
        Cell cellObj = null;
        CellStyle subColHeaderStyle = null, valueCellStyle = null, valueRightCellStyle = null, tempCellStyle = null;
        CellStyleParamters styleParams = null;
        if (youTubeChannelInfo != null) {
            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.TOP);
            /*getting tsyle object*/
            subColHeaderStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.TRUE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            valueCellStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            /*Field Data Prefix*/
            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Videos");
            cellObj.setCellStyle(subColHeaderStyle);

            cellObj = rowObj.createCell(2);
            cellObj.setCellValue(": " + youTubeChannelInfo.getVideosCount());
            cellObj.setCellStyle(valueCellStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Subscribers");
            cellObj.setCellStyle(subColHeaderStyle);

            cellObj = rowObj.createCell(2);
            cellObj.setCellValue(": " + youTubeChannelInfo.getSubscribersCount());
            cellObj.setCellStyle(valueCellStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Views");
            cellObj.setCellStyle(subColHeaderStyle);

            cellObj = rowObj.createCell(2);
            cellObj.setCellValue(": " + youTubeChannelInfo.getVideosViewsCount());
            cellObj.setCellStyle(valueCellStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Comments Count");
            cellObj.setCellStyle(subColHeaderStyle);

            cellObj = rowObj.createCell(2);
            cellObj.setCellValue(": " + youTubeChannelInfo.getVideosCommentsCount());
            cellObj.setCellStyle(valueCellStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Joined");
            cellObj.setCellStyle(subColHeaderStyle);

            cellObj = rowObj.createCell(2);
            cellObj.setCellValue(": " + Common.changeDateStringFormat(youTubeChannelInfo.getPublishedAt(), API_FORMAT, USER_FORMAT));
            cellObj.setCellStyle(valueCellStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 1000);

            cellObj = rowObj.createCell(0);
            cellObj.setCellValue("Description");
            cellObj.setCellStyle(subColHeaderStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);
           
            cellObj = rowObj.createCell(0);
            cellObj.setCellValue(Common.trimHtmlTagsContent(youTubeChannelInfo.getDescription()));
            cellObj.setCellStyle(subColHeaderStyle);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            rowObj = sheetObj.createRow(++rowCount);
            rowObj.setHeight((short) 500);

            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.TRUE, new XSSFColor(Color.WHITE), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            styleParams.setForeGroundColorIndex(IndexedColors.ORANGE.getIndex());
            styleParams.setFillPattern(Boolean.TRUE);
            /*getting tsyle object*/
            subColHeaderStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.LEFT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            valueCellStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.RIGHT);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            /*getting tsyle object*/
            valueRightCellStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            styleParams = new CellStyleParamters();
            styleParams.setFont(ExcellUtillReportServce.getRequeriedXSSFFont(workBookObj, NORMAL_TEXT_SIZE, Boolean.FALSE, new XSSFColor(Color.BLACK), FONT_STYLE));
            styleParams.setHorizontalPosition(HorizontalAlignment.CENTER);
            styleParams.setVerticalPostion(VerticalAlignment.CENTER);
            styleParams.setForeGroundColorIndex(IndexedColors.ORANGE.getIndex());
            styleParams.setFillPattern(Boolean.TRUE);
            /*getting tsyle object*/
            tempCellStyle = ExcellUtillReportServce.getRequriedXSSFCellStyle(workBookObj, styleParams);

            if (youTubeChannelInfo.getVideoInfoList() != null && !youTubeChannelInfo.getVideoInfoList().isEmpty()) {

                rowObj = sheetObj.createRow(++rowCount);
                rowObj.setHeight((short) 500);

                cellObj = rowObj.createCell(0);
                cellObj.setCellValue("S.No.");
                cellObj.setCellStyle(tempCellStyle);

                cellObj = rowObj.createCell(1);
                cellObj.setCellValue("Title");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(2);
                cellObj.setCellValue("Created/Uploaded Date");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(3);
                cellObj.setCellValue("Likes");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(4);
                cellObj.setCellValue("Dislikes");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(5);
                cellObj.setCellValue("Views");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(6);
                cellObj.setCellValue("Comments");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(7);
                cellObj.setCellValue("Favorites");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(8);
                cellObj.setCellValue("Keywords");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(9);
                cellObj.setCellValue("Description");
                cellObj.setCellStyle(subColHeaderStyle);

                cellObj = rowObj.createCell(10);
                cellObj.setCellValue("URL");
                cellObj.setCellStyle(subColHeaderStyle);

                int x = ++rowCount, k = 0;
                for (int j = 0; j < youTubeChannelInfo.getVideoInfoList().size(); j++) {
                    try {
                        ++k;
                        rowObj = sheetObj.createRow(x);
                        rowObj.setHeight((short) 2500);
                        for (int i = 0; i <= 10; i++) {
                            cellObj = rowObj.createCell(i);
                            switch (i) {
                                case 0:
                                    cellObj.setCellValue(k);
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 1:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getVideoTitle() != null ? Common.trimHtmlTagsContent(youTubeChannelInfo.getVideoInfoList().get(j).getVideoTitle()) : "NA");
                                    cellObj.setCellStyle(valueCellStyle);
                                    break;
                                case 2:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getVideoPublishedAt() != null ? Common.changeDateStringFormat(youTubeChannelInfo.getVideoInfoList().get(j).getVideoPublishedAt(), API_FORMAT, USER_FORMAT) : "NA");
                                    cellObj.setCellStyle(valueCellStyle);
                                    break;
                                case 3:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getLikesCount());
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 4:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getDislikesCount());
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 5:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getViewsCount());
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 6:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getCommentsCount());
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 7:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getFavoriteCount());
                                    cellObj.setCellStyle(valueRightCellStyle);
                                    break;
                                case 8:
                                    cellObj.setCellValue((youTubeChannelInfo.getVideoInfoList().get(j).getTags() != null && !youTubeChannelInfo.getVideoInfoList().get(j).getTags().isEmpty()) ? Common.getConcatedStringFromList(youTubeChannelInfo.getVideoInfoList().get(j).getTags(), SEPARATOR) : "NA");
                                    cellObj.setCellStyle(valueCellStyle);
                                    break;
                                case 9:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getVideoDescription() != null ? Common.trimHtmlTagsContent(youTubeChannelInfo.getVideoInfoList().get(j).getVideoDescription()) : "NA");
                                    cellObj.setCellStyle(valueCellStyle);
                                    break;
                                case 10:
                                    cellObj.setCellValue(youTubeChannelInfo.getVideoInfoList().get(j).getVideoURL() != null ? youTubeChannelInfo.getVideoInfoList().get(j).getVideoURL() : "NA");
                                    cellObj.setCellStyle(valueCellStyle);
                                    break;
                            }
                        }
                        ++x;
                    } catch (Exception e) {
                       LOGGER.error("Exception in adding cell value to report cause:: ",e);
                    }
                }
            }

        }
    }

}
