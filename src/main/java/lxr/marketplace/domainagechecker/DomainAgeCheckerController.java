package lxr.marketplace.domainagechecker;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.apiaccess.lxrmbeans.WhoIsInfo;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.WhoisDomainInfoFetch;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/domain-age-checker-tool.html")
public class DomainAgeCheckerController {

    private static Logger logger = Logger.getLogger(DomainAgeCheckerController.class);
    private EmailTemplateService emailTemplateService;
//    @Autowired
//    private ToolService toolService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    public DomainAgeCheckerController(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }
    @RequestMapping(method = RequestMethod.GET)
    public String getDACHomePage(HttpServletRequest request, HttpSession session, Model model) {
        DomainAgeChecker domainAgeChecker = null;
        session.removeAttribute("notWorkingUrl");
        if (session.getAttribute("domainAgeChecker") != null
                && WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            domainAgeChecker = (DomainAgeChecker) session.getAttribute("domainAgeChecker");
        } else {
            domainAgeChecker = new DomainAgeChecker();
        }
        model.addAttribute(domainAgeChecker);
        return "views/domainAgeChecker/domainAgeChecker";
    }

    @RequestMapping(params = {"getResult"})
    public String getResult(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model,
            @ModelAttribute("domainAgeChecker") DomainAgeChecker domainAgeChecker, @RequestParam("domain") String domain) {
        Login user = (Login) session.getAttribute("user");
        if (domainAgeChecker != null) {
            domainAgeChecker.setDomain(domain);
            session.setAttribute("domainAgeChecker", domainAgeChecker);
        }
        int statusCode = 0;
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(21);
            toolObj.setToolName("Domain Age Checker");
            toolObj.setToolLink("domain-age-checker-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.DOMAIN_AGE_CHECKER);
            if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                session.setAttribute("loginrefresh", "loginrefresh");
                if (session.getAttribute("notWorkingUrl") == null) {
                    model.addAttribute("getSuccess", "success");
                }
            } else {
                Session userSession = (Session) session.getAttribute("userSession");
                WhoIsInfo whoIsInfo = null;
                String notWorkingUrl = null;
                StringBuilder redirectedUrl = new StringBuilder("");
                StringBuilder finalModifiedUrl = new StringBuilder("");
                try {
                    statusCode = Common.appendProtocalAndCheckStausCode(domainAgeChecker.getDomain().trim(), redirectedUrl, finalModifiedUrl);
                } catch (Exception e) {
                    logger.debug("Exception while checking url status in Domain Age Checker", e);
                }
                if (statusCode == 0 || statusCode >= 400) {
                    notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
//                    notWorkingUrl = new String("Your URL is not working, please check the URL.");
//                    session.setAttribute("notWorkingUrl", notWorkingUrl);
                    model.addAttribute("notWorkingUrl", notWorkingUrl);
                } else {
                    try {
                        if (!redirectedUrl.toString().trim().equals("")) {
                            session.setAttribute("redirectedUrl", redirectedUrl.toString());
                        } else {
                            session.setAttribute("redirectedUrl", "");
                        }
                        String finalModUrl = finalModifiedUrl.toString();

                        session.setAttribute("Url", finalModUrl);
                        URL tempUrl = new URL(finalModUrl);
                        String domName = tempUrl.getHost();
                        whoIsInfo = WhoisDomainInfoFetch.getRoboWhois(domName);
                        session.setAttribute("whoIsInfo", whoIsInfo);
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                        userSession.addToolUsage(toolObj.getToolId());
                        if (user.getId() == -1) {
                            user = (Login) session.getAttribute("user");
                        }
                        userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), domainAgeChecker.getDomain(), "", "", "", "/" + toolObj.getToolLink());
                        if (whoIsInfo != null) {
                            whoIsInfo.setHttpStatus(statusCode);
                            model.addAttribute("getSuccess", "success");
                        } else {
                            notWorkingUrl = new String("Your URL is not working, please check the URL.");
                            model.addAttribute("notWorkingUrl", notWorkingUrl);
                            session.setAttribute("notWorkingUrl", notWorkingUrl);
                        }
                    } catch (MalformedURLException ex) {
                        logger.error("Exception while converting string to URL Object", ex);
                    }
                }
            }
        }
        model.addAttribute(domainAgeChecker);
        return "views/domainAgeChecker/domainAgeChecker";
    }

    @RequestMapping(params = {"download"})
    @ResponseBody
    public Object[] downloadOrSendReport(HttpServletRequest request, HttpSession session, Model model) {
        Login user = (Login) session.getAttribute("user");
        Object[] data = new Object[1];
        if (user != null) {
            try {
                WhoIsInfo whoIsInfo = null;
                String downParam = request.getParameter("download");
                if (downParam.equalsIgnoreCase("sendmail")) {
                    logger.info("Sending report through mail");
                    String toAddrs = request.getParameter("email");
                    if (session.getAttribute("whoIsInfo") != null) {
                        whoIsInfo = (WhoIsInfo) session.getAttribute("whoIsInfo");
                        if (whoIsInfo != null) {
                            SendMail.sendDomainAgeCheckerMailToUser(whoIsInfo.getDomainName(), toAddrs, whoIsInfo, user.getName(), "");
                            data[0] = "success";
                        } else {
                            logger.info("Mailing to user is failed due to whoIsInfo is null");
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Exception while domain age checker tool report download/send mail ", e);
            }
        }
        return data;
    }
}
