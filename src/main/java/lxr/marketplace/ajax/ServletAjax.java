package lxr.marketplace.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import lxr.marketplace.cpcindex.service.CPCService;

/**
 * Servlet implementation class ServletAjax
 */
@WebServlet("/ServletAjax")
public class ServletAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAjax() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs = (HttpSession) request.getSession();		
		String dataset = request.getParameter("dataset");
		String key = request.getParameter("key");
		String key1 = request.getParameter("key1");
		String key2 = request.getParameter("key2");
		String key3 = request.getParameter("key3");
		response.setContentType("text/html;charset=UTF-8");
		response.setContentType("text/xml");
		PrintWriter out = response.getWriter();
		 ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(hs.getServletContext());
		 if(dataset.equalsIgnoreCase("loadCPCChart")){
			 try{			
			    CPCService cpcService= (CPCService)ctx.getBean("CPCService");
				StringBuilder xmlData = new StringBuilder();
				List<CPCService> cpcXmlData = null;
				Date frDate = null;
				Date toDate = null;
				
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				frDate = sdf.parse(key);
				toDate = sdf.parse(key1);
				
				
				SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
				 
				//parse the date into another format
				String fromdate = sdfDestination.format(frDate);
				 String toDate1 = sdfDestination.format(toDate);
				/*Calendar calf = Calendar.getInstance();
	            Calendar calt = Calendar.getInstance();
	            calf.setTime(frDate);
	            calt.setTime(toDate);
	            
	              // Calendar cal = Calendar.getInstance(); 
	              // Date d = new Date(cal.getTime().getTime());
	               String toDate1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calt.getTime().getTime()));
	               String fromdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calf.getTime().getTime()));
	               
	               //com.netelixir.api.common.DateConverter dc = new DateConverter(calf);
	               //fromDate = dc.getMonth()+ "-"+dc.getDate()+ "-" + dc.getYear();
	               //dc = new DateConverter(calt);
	               //toDate = dc.getMonth()+ "-"+dc.getDate()+ "-" + dc.getYear(); */

	            //   String toDate1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calt.getTime().getTime()));
	            //   String fromdate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(calf.getTime().getTime()));
				
				//String fromDate = 
				//String toDate = 
			//	cpcXmlData = cpcService.getXmlData("2011-11-25", "2011-11-27");
				 cpcXmlData = cpcService.getXmlData(fromdate, toDate1);
				xmlData = cpcService.getXml(cpcXmlData);
				hs.setAttribute("cpcCompXmlData", xmlData); 
				hs.setAttribute("fromDate", key);
				hs.setAttribute("toDate", key1);
				//out.println("chartDisplay();");
				out.println("xmlCpcData=\"" + xmlData.toString() + "\";");
				out.println("loadCharts('CompChart', xmlCpcData);");
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		}else if(dataset.equalsIgnoreCase("insertData")){
			try{
			//String code = request.getParameter("code");
				StringBuilder html = new StringBuilder();
			if(key3.equals("VOGAJWAcpc25225")){
			int flag =0;
			//ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(hs.getServletContext());
			CPCService cpcService= (CPCService)ctx.getBean("CPCService");
			double adwords_wtcpc = Double.parseDouble(key1);
			double bing_wtcpc = Double.parseDouble(key2);			
			Date frDate = null;
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			frDate = sdf.parse(key);
			//Calendar calf = Calendar.getInstance();
            //calf.setTime(frDate);
           // calf.add(Calendar.HOUR, -5);
			SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
			//String fromdate = sdfDestination.format(new Date(calf.getTime().getTime()));
			String fromdate = sdfDestination.format(frDate);
			
			String query = "update  lxr_mkpl_cpc_index set adwords_wtcpc = "+adwords_wtcpc+",bing_wtcpc = "+bing_wtcpc+" where date = '"+fromdate+"'";
			//String query = "insert into lxr_mkpl_cpc_index (date,adwords_wtcpc,bing_wtcpc) values ('"+fromdate+"',"+adwords_wtcpc+","+bing_wtcpc+")";
			
			
			flag = cpcService.updateData(query);
			if(flag > 0){
				 html.append("<label style='color:green'>Data inserted successfully</label>");
				//out.println("document.getElementById('errorDiv').innerHTML="+"Data inserted successfully");
				 out.println("document.getElementById(\"errorDiv\").innerHTML=\"" + html + "\"");
			}else{
				 html.append("<label style='color:red'>Data not inserted please try again.</label>");
				//out.println("document.getElementById('errorDiv').innerHTML="+"Data not inserted please try again");
				out.println("document.getElementById(\"errorDiv\").innerHTML=\"" + html + "\"");
			}
			}else{
				 html.append("<label style='color:red'>Your security code is invalid</label>");
				//out.println("document.getElementById('errorDiv').innerHTML="+"Your security code is invalid");
				 out.println("document.getElementById(\"errorDiv\").innerHTML=\"" + html + "\"");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
  }
}
