package lxr.marketplace.promotions;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class PromotionalService extends JdbcDaoSupport{
    
    
    private static Logger logger = Logger.getLogger(PromotionalService.class);
    
    
//    public boolean insertIntoUserPartnerMap(long userId, long partnerId){
//        boolean inserted = false;
//        String query = "INSERT INTO user_partner_map (user_id, partner_id) VALUES (?, ?)";
//        Object[] valLis = new Object[2];
//
//        valLis[0] = userId;
//        valLis[1] = partnerId;
//        try {
//            getJdbcTemplate().update(query, valLis);
//            inserted = true;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return inserted;
//    }
//    
//    public boolean checkPresenceInUserPartnerMap(long userId, long partnerId){
//        boolean present = false;
//        String query = "SELECT count(*) FROM user_partner_map WHERE user_id = " + userId + " AND partner_id = " +partnerId;
//        try {
//            int count = getJdbcTemplate().queryForInt(query);
//            if(count > 0){
//                present = true;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
////        logger.debug("User Promotion already present: " + present);
//        return present;
//    }
//    
//    public Long fetchPartnerIdByName(String partnerName){
//        String query = "SELECT partner_id FROM promotion_partner WHERE partner_name = '"+partnerName+"'";
//        Long partnerId = null;
//        try{
//            partnerId = getJdbcTemplate().queryForLong(query);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return partnerId;
//    }
//    
//    public List<PromotionalOffer> toolPromotionAvailablilityForUser(long userId, long toolId, Calendar toDay){
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String todayString = sdf.format(toDay.getTime());
//        logger.debug("today's Date: " + todayString);
//        String query = "SELECT * FROM promotional_offers WHERE partner_id IN (SELECT partner_id " +
//        		"FROM user_partner_map WHERE user_id = " + userId + ") AND is_active = true AND date_format" +
//        				"(end_date, '%Y-%m-%d') >= '"+todayString+"' AND tool_id = " + toolId; 
//        logger.debug("Promotion Availability Query: " + query);
//        List<PromotionalOffer> offers = fetchPromotionsByQuery(query);
//        if(offers == null || offers.size() <= 0){   //Provide LXRMarketplace basic promotions
//            query = "SELECT * FROM promotional_offers WHERE partner_id  = 1 AND is_active = true AND date_format" +
//                            "(end_date, '%Y-%m-%d') >= '"+todayString+"' AND tool_id = " + toolId; 
//            offers = fetchPromotionsByQuery(query);
//        }
//        return offers;
//    }
//    
//    public List<PromotionalOffer> fetchPromotionsByPartnerName(String partnerName){
//        String query = "SELECT * FROM promotional_offers WHERE partner_id IN (SELECT partner_id FROM " +
//        		"promotion_partner WHERE partner_name = '" + partnerName + "')";
//        return fetchPromotionsByQuery(query);
//    }
//    
//    public List<PromotionalOffer> fetchActivePromotionsByPartnerId(long partnerId, Calendar toDay){
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String todayString = sdf.format(toDay.getTime());
//        String query = "SELECT * FROM promotional_offers WHERE partner_id = " +partnerId+"  AND date_format" +
//                        "(end_date, '%Y-%m-%d') >= '"+todayString+"' AND is_active = 1";
//        return fetchPromotionsByQuery(query);
//    }
//    
//    public PromotionalOffer fetchPromotionByPromoId(long promotionId){
//        String query = "SELECT * FROM promotional_offers WHERE promotion_id="+promotionId;
//        List<PromotionalOffer> promotionalOffers = fetchPromotionsByQuery(query);
//        if(promotionalOffers == null || promotionalOffers.size() == 0){
//            return null;
//        }else{
//            return promotionalOffers.get(0);
//        }
//    }
//
//    private List<PromotionalOffer> fetchPromotionsByQuery(String query){
//        try {
//            List<PromotionalOffer> promoOffers = getJdbcTemplate().query(query, new RowMapper<PromotionalOffer>() { 
//                public PromotionalOffer mapRow(ResultSet rs, int rowNum) throws SQLException {              
//                    PromotionalOffer promoOffer = new PromotionalOffer();
//                    promoOffer.setPromotionId(rs.getLong("promotion_id"));
//                    promoOffer.setPartnerId(rs.getLong("partner_id"));
//                    promoOffer.setToolId(rs.getLong("tool_id"));
//                    promoOffer.setPromotionName(rs.getString("promotion_name"));
//                    
//                    Calendar startCal = Calendar.getInstance();
//                    Timestamp startTimestamp = rs.getTimestamp("start_date");
//                    if(startTimestamp != null){
//                        startCal.setTimeInMillis(startTimestamp.getTime());
//                    }
//                    promoOffer.setStartDate(startCal);
//                    
//                    Calendar endCal = Calendar.getInstance();
//                    Timestamp endTimestamp = rs.getTimestamp("end_date");
//                    if(endTimestamp != null){
//                        endCal.setTimeInMillis(endTimestamp.getTime());
//                    }
//                    promoOffer.setEndDate(endCal);
//                    
//                    promoOffer.setActive(rs.getBoolean("is_active"));
//                    promoOffer.setPrice(rs.getDouble("price"));
//                    promoOffer.setNoOfTimes(rs.getInt("no_of_times"));
//                    promoOffer.setNoOfMonths(rs.getInt("no_of_months"));
//                    promoOffer.setDisplayString(rs.getString("display_string"));
//                    
//                    return promoOffer;
//                }
//            });
//
//            return promoOffers;
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//            logger.error(e.getCause());
//        }
//        return null;
//    }
}
