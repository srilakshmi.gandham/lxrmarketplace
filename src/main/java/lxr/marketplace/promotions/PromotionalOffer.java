package lxr.marketplace.promotions;

import java.util.Calendar;

public class PromotionalOffer {
    private long promotionId;
    private long partnerId; 
    private long toolId;
    private String promotionName;
    private String displayString;
    private Calendar startDate;
    private Calendar endDate;
    private boolean isActive;
    private double price;
    private int noOfTimes;
    private int noOfMonths;
    
    public PromotionalOffer(){
        
    }

//    public PromotionalOffer(long promotionId, long partnerId, long toolId,
//            String promotionName, String displayString, Calendar startDate,
//            Calendar endDate, boolean isActive, double price, int noOfTimes,
//            int noOfMonths) {
//        super();
//        this.promotionId = promotionId;
//        this.partnerId = partnerId;
//        this.toolId = toolId;
//        this.promotionName = promotionName;
//        this.displayString = displayString;
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.isActive = isActive;
//        this.price = price;
//        this.noOfTimes = noOfTimes;
//        this.noOfMonths = noOfMonths;
//    }

    public long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(long promotionId) {
        this.promotionId = promotionId;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    public long getToolId() {
        return toolId;
    }

    public void setToolId(long toolId) {
        this.toolId = toolId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getDisplayString() {
        return displayString;
    }

    public void setDisplayString(String displayString) {
        this.displayString = displayString;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNoOfTimes() {
        return noOfTimes;
    }

    public void setNoOfTimes(int noOfTimes) {
        this.noOfTimes = noOfTimes;
    }

    public int getNoOfMonths() {
        return noOfMonths;
    }

    public void setNoOfMonths(int noOfMonths) {
        this.noOfMonths = noOfMonths;
    }          
}
