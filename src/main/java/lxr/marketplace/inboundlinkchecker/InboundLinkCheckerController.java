package lxr.marketplace.inboundlinkchecker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import net.sf.json.JSONObject;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

@SuppressWarnings("deprecation")
public class InboundLinkCheckerController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(InboundLinkCheckerController.class);

    private EmailTemplateService emailTemplateService;
    private ToolService toolService;
    private String downloadFolder;
    private String googleCustomSearchApiKeyLxrseo;
    private InboundLinkCheckerService inboundLinkCheckerService;
    private MarketplacePaymentService marketplacePaymentService;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @Autowired
    private MessageSource messageSource;

    JSONObject userInputJson = null;

    public MarketplacePaymentService getMarketplacePaymentService() {
        return marketplacePaymentService;
    }

    public void setMarketplacePaymentService(
            MarketplacePaymentService marketplacePaymentService) {
        this.marketplacePaymentService = marketplacePaymentService;
    }

    public InboundLinkCheckerService getInboundLinkCheckerService() {
        return inboundLinkCheckerService;
    }

    public void setInboundLinkCheckerService(
            InboundLinkCheckerService inboundLinkCheckerService) {
        this.inboundLinkCheckerService = inboundLinkCheckerService;
    }

    public String getGoogleCustomSearchApiKeyLxrseo() {
        return googleCustomSearchApiKeyLxrseo;
    }

    public void setGoogleCustomSearchApiKeyLxrseo(
            String googleCustomSearchApiKeyLxrseo) {
        this.googleCustomSearchApiKeyLxrseo = googleCustomSearchApiKeyLxrseo;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(
            EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public InboundLinkCheckerController() {
        setCommandClass(InboundLinkChecker.class);
        setCommandName("inboundLinkCheckerTool");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {

        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");

        InboundLinkChecker inboundLinkCheckerTool = (InboundLinkChecker) command;
        if (inboundLinkCheckerTool != null) {
            session.setAttribute("inboundLinkCheckerTool", inboundLinkCheckerTool);
        }

        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(20);
            toolObj.setToolName("Inbound Link Checker");
            toolObj.setToolLink("seo-inbound-link-checker-tool.html");
            session.setAttribute("toolObj", toolObj);

            boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            Session userSession = (Session) session.getAttribute("userSession");
            session.setAttribute("currentTool", Common.INBOUND_LINK_CHECKER);

            if (WebUtils.hasSubmitParameter(request, "getResult")) {
                session.removeAttribute("toolObj");
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                    String domName = "";
                    if (session.getAttribute("blcBacklinks") != null
                            && session.getAttribute("ahrefBackLinkDomainInfo") != null
                            && session.getAttribute("domName") != null) {
                        ArrayList<AhrefBacklink> backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                        Map<String, String> backLinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                        domName = (String) session.getAttribute("domName");
                        mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                        if (backLinks == null || backLinks.isEmpty()) {
                            mAndV.addObject("backLinks", 0);
                        } else {
                            String totBacklinks = "" + backLinks.size();
                            mAndV.addObject("backLinks", totBacklinks);
                            if (session.getAttribute("notWorkingUrl") == null) {
                                mAndV.addObject("getSuccess", "success");
                            }
                        }
                        org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                        mAndV.addObject("priceJSON", priceJSON);
                        mAndV.addObject("getSuccess", "success");
//                        mAndV.addObject("showUrltoUser", "20 most recent backlinks for '" + domName + "'");
                    }
                    return mAndV;
                }
                session.removeAttribute("notWorkingUrl");

                JSONObject backLinksData = null;
                if (inboundLinkCheckerTool != null && !inboundLinkCheckerTool.getUrl().trim().isEmpty()) {
                    backLinksData = inboundLinkCheckerService.backLinkDataService("LXRMarketplace", inboundLinkCheckerTool.getUrl().trim(), (messageSource.getMessage("lxrm.apps.rest.hostingURL", null, Locale.US) + messageSource.getMessage("inboundLinkCheckerServiceURL", null, Locale.US)));
                }

                ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                if (backLinksData != null && backLinksData.has("error")) {
                    mAndV.addObject("notWorkingUrl", backLinksData.getString("error"));
                    session.setAttribute("notWorkingUrl", backLinksData.getString("error"));
                    session.removeAttribute("ahrefBacklinkCheckerTool");
                    session.removeAttribute("backlinksPrice");
//                    Map<String, String> domainList = new HashMap<>();
//                    mAndV.addObject("domainList", domainList);
                    return mAndV;
                } else if (backLinksData != null) {
                    List<AhrefBacklink> backLinks = null;
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
                    Map<String, String> backLinksPrice = null;
                    if (!backLinksData.isEmpty()) {
                        ObjectMapper objectMapper = new ObjectMapper();
                        if (backLinksData.has("metrics")) {
                            ahrefBackLinkDomainInfo = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("metrics")),
                                    new TypeReference<AhrefBackLinkDomainInfo>() {
                            });
                        }
                        if (backLinksData.has("backlinksInfo")) {
                            backLinks = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("backlinksInfo")),
                                    new TypeReference<List<AhrefBacklink>>() {
                            });
                        }
                        if (backLinksData.has("backlinksPrice")) {
                            backLinksPrice = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("backlinksPrice")),
                                    new TypeReference<LinkedHashMap<String, String>>() {
                            });
                        }
                    }
                    if (backLinks != null) {
                        backLinks = backLinks.stream().sorted((o2, o1) -> o1.getNoFollowUrl().compareTo(o2.getNoFollowUrl()))
                                .collect(Collectors.toList());
//                        String showUrltoUser = null;
                        if (ahrefBackLinkDomainInfo != null) {
                            if (inboundLinkCheckerTool != null) {
                                inboundLinkCheckerTool.setUrl(ahrefBackLinkDomainInfo.getDomainName());
                            }
                            session.setAttribute("domName", ahrefBackLinkDomainInfo.getDomainName());
                            session.setAttribute("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
//                            showUrltoUser = ahrefBackLinkDomainInfo.getDomainName();
                        }
                        session.setAttribute("blcBacklinks", backLinks);
                        session.setAttribute("backlinksPrice", backLinksPrice);
                        Common.modifyDummyUserInToolorVideoUsage(request, response);
                        if (user.getId() == -1) {
                            user = (Login) session.getAttribute("user");
                        }
                        userSession.addToolUsage(toolObj.getToolId());
                        // if the request is coming from tool page then only add the user inputs to JSONObject
                        // if the request is coming from mail then do not add the user inputs to JSONObject
                        boolean requestFromMail = false;
                        if (session.getAttribute("requestFromMail") != null) {
                            requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                            session.removeAttribute("requestFromMail");
                        }
                        if (!requestFromMail && user != null && !user.isAdmin()) {
                            userInputJson = new JSONObject();
                        }
//                        if (showUrltoUser != null && ahrefBackLinkDomainInfo != null) {
//                            mAndV.addObject("showUrltoUser", ahrefBackLinkDomainInfo.getAhrefsbackLinks() > BACKLINK_COUNT
//                                    ? (BACKLINK_COUNT + " most recent backlinks for '" + showUrltoUser + "'") : ("All backlinks for '" + showUrltoUser + "'"));
//                        }
                        mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                        mAndV.addObject("backLinks", backLinks.size());
                        mAndV.addObject("getSuccess", "success");
                        org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                        mAndV.addObject("priceJSON", priceJSON);
                    }

                    return mAndV;
                }

            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                session.removeAttribute("valsObjArray");
                session.removeAttribute("downloadType");

                String queryDomain = session.getAttribute("domName") != null ? (String) session.getAttribute("domName") : inboundLinkCheckerTool.getUrl().trim();
                if (userLoggedIn) {
                    List<AhrefBacklink> backLinks = null;
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
                    Map<String, String> backLinksPrice = null;
                    try {
                        //Evulating request param to get back links count
                        long requestedBacklinksCount = Long.parseLong(request.getParameter("count"));
                        if (requestedBacklinksCount <= Common.BACK_LINK_COUNT && session.getAttribute("blcBacklinks") != null && session.getAttribute("ahrefBackLinkDomainInfo") != null) {
                            ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                            backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                        } else {
                            //Fetching back links from service based on user opted price
                            JSONObject backLinksData = inboundLinkCheckerService.backLinkDataService(request.getParameter("count"), queryDomain, (messageSource.getMessage("lxrm.apps.rest.hostingURL", null, Locale.US) + messageSource.getMessage("inboundLinkCheckerServiceURL", null, Locale.US)));
                            if (backLinksData != null) {
                                if (backLinksData.has("error")) {
                                    ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                                    mAndV.addObject("notWorkingUrl", backLinksData.getString("error"));
                                    session.setAttribute("notWorkingUrl", backLinksData.getString("error"));
                                    session.removeAttribute("ahrefBacklinkCheckerTool");
                                    session.removeAttribute("backlinksPrice");
//                    Map<String, String> domainList = new HashMap<>();
//                    mAndV.addObject("domainList", domainList);
                                    return mAndV;
                                }
                                ObjectMapper objectMapper = new ObjectMapper();
                                ahrefBackLinkDomainInfo = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("metrics")), new TypeReference<AhrefBackLinkDomainInfo>() {
                                });
                                backLinks = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("backlinksInfo")), new TypeReference<List<AhrefBacklink>>() {
                                });
                                session.setAttribute("domName", ahrefBackLinkDomainInfo.getDomainName());
                                session.setAttribute("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                            }
                        }
                        //Generating report based on fetched back links
                        String generatedReportName = null;
                        if (backLinks != null) {
                            backLinks = backLinks.stream().sorted((o2, o1) -> o1.getNoFollowUrl().compareTo(o2.getNoFollowUrl()))
                                    .collect(Collectors.toList());
                            generatedReportName = inboundLinkCheckerService.createBackLinkXLSFile(queryDomain, downloadFolder, backLinks, ahrefBackLinkDomainInfo, user.getId(), InboundLinkCheckerService.REPORT_FILE_NAME, InboundLinkCheckerService.REPORT_TITLE);
                        }
                        if ((generatedReportName != null && !generatedReportName.trim().equals(""))) {
                            //Generating report for free back links no payment is required
                            if (backLinks != null && backLinks.size() <= Common.BACK_LINK_COUNT) {
                                Common.downloadReport(response, downloadFolder, generatedReportName, "xls");
                                return null;
                            } else if ((backLinks != null) && backLinks.size() > Common.BACK_LINK_COUNT) {
                                //Forwarding request to  payment page for 500, 1000 or 100000 backlinks to genarte report
                                double amount = inboundLinkCheckerService.getPayableFormatAmount(request.getParameter("price").trim());
                                Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                                Object[] vals = {generatedReportName, 20, user.getId(), createdTime, queryDomain, amount, "", Common.INBOUND_LINK_CHECKER, "/seo-inbound-link-checker-tool.html", String.valueOf(backLinks.size()) + "  Backlinks"};
                                String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
                                logger.info("The Fianl redirect url for payment:: " + (url + "/lxrm-payment.html"));
                                response.sendRedirect(url + "/lxrm-payment.html");
                                session.setAttribute("valsObjArray", vals);
                                return null;
                            } else {
                                ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                                mAndV.addObject("getSuccess", "success");
                                return mAndV;
                            }
                        }
                    } catch (IOException e) {
                        logger.error("Exception on fetching backlinks when click download button", e);
                    }

                    backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                    ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                    mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                    if (backLinks == null || backLinks.isEmpty()) {
                        mAndV.addObject("backLinks", 0);
                    } else {
                        mAndV.addObject("backLinks", backLinks.size());
                        mAndV.addObject("getSuccess", "success");
                    }
                    backLinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                    org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                    mAndV.addObject("priceJSON", priceJSON);
                    mAndV.addObject("getSuccess", "success");
                    if (queryDomain != null) {
//                        mAndV.addObject("showUrltoUser", "20 most recent backlinks for '" + queryDomain + "'");
                    }
                    return mAndV;
                } else {
                    ArrayList<AhrefBacklink> backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                    mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                    if (backLinks == null || backLinks.isEmpty()) {
                        mAndV.addObject("backLinks", 0);
                    } else {
                        mAndV.addObject("backLinks", backLinks.size());
                        mAndV.addObject("getSuccess", "success");
                    }
                    Map<String, String> backLinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                    org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                    mAndV.addObject("priceJSON", priceJSON);
                    mAndV.addObject("getSuccess", "success");
                    if (queryDomain != null) {
//                        mAndV.addObject("showUrltoUser", "20 most recent backlinks for '" + queryDomain + "'");
                    }
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "downloadFile")) {
                session.removeAttribute("downloadType");
                if (userLoggedIn) {
                    String downParam = request.getParameter("downloadFile");
                    Object[] vals = (Object[]) session.getAttribute("valsObjArray");
                    if ((String) vals[0] != null) {
                        if (downParam.equalsIgnoreCase("'downloadFile'")) {
                            Common.downloadReport(response, downloadFolder, (String) vals[0], downParam);
                        } else if (downParam.equalsIgnoreCase("'sendmail'")) {
                            String toAddrs = user.getUserName();
                            String toolName = Common.INBOUND_LINK_CHECKER;
                            Common.sendReportThroughMail(request, downloadFolder, (String) vals[0], toAddrs, toolName);
                            ModelAndView mAndV = new ModelAndView(new RedirectView("/lxrm-thanks.html?ms='ms'"), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                            return mAndV;
                        }
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "prvDownloadFile")) {
                session.removeAttribute("toolObj");
                session.removeAttribute("downloadType");
                if (userLoggedIn) {
                    String fileName = request.getParameter("prvDownloadFile");
                    if (fileName != null && Common.checkFileExistance(downloadFolder + fileName)) {
                        Common.downloadReport(response, downloadFolder, fileName, fileName.split("\\.")[1]);
                        return null;
                    } else {
                        List<PaidDownloadedReportDetails> updatedPaidDownloadedReportDetails;
                        PaidDownloadedReportDetails reportToBeDeleted;
                        /*Deleting report from paid download reports and updating new list in session.*/
                        if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                            updatedPaidDownloadedReportDetails = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                            reportToBeDeleted = updatedPaidDownloadedReportDetails.stream().filter(report -> report.getReportName().equalsIgnoreCase(fileName)).collect(Collectors.toList()).get(0);
                            if (reportToBeDeleted != null) {
                                toolService.deletePaidDownladReports(reportToBeDeleted.getReportId());
                                updatedPaidDownloadedReportDetails = toolService.fetchPaidDownladReports(user.getId(), 20);
                                session.setAttribute("paidDownloadedReportDetailsList", updatedPaidDownloadedReportDetails);
                            }
                        }
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                if (session.getAttribute("blcBacklinks") != null) {
                    ArrayList<AhrefBacklink> backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                    String domName = "";
                    if (session.getAttribute("domName") != null) {
                        domName = (String) session.getAttribute("domName");
                    }
                    mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                    if (backLinks == null || backLinks.isEmpty()) {
                        mAndV.addObject("backLinks", 0);
                    } else {
                        mAndV.addObject("backLinks", backLinks.size());
                    }
                    mAndV.addObject("getSuccess", "success");

                    Map<String, String> backLinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                    org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                    mAndV.addObject("priceJSON", priceJSON);
//                    mAndV.addObject("showUrltoUser", "20 most recent backlinks for '" + domName + "'");
                }
                mAndV.addObject("fileExistence", "Sorry the requested file is no longer existed.");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                /*Cancel the payment process*/
                ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
                if (session.getAttribute("blcBacklinks") != null) {
                    ArrayList<AhrefBacklink> backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = (AhrefBackLinkDomainInfo) session.getAttribute("ahrefBackLinkDomainInfo");
                    String domName = "";
                    if (session.getAttribute("domName") != null) {
                        domName = (String) session.getAttribute("domName");
                    }
                    mAndV.addObject("ahrefBackLinkDomainInfo", ahrefBackLinkDomainInfo);
                    if (backLinks == null || backLinks.isEmpty()) {
                        mAndV.addObject("backLinks", 0);
                    } else {
                        mAndV.addObject("backLinks", backLinks.size());
                    }
                    mAndV.addObject("getSuccess", "success");

                    Map<String, String> backLinksPrice = (Map<String, String>) session.getAttribute("backlinksPrice");
                    org.json.JSONObject priceJSON = new org.json.JSONObject(backLinksPrice);
                    mAndV.addObject("priceJSON", priceJSON);
//                    mAndV.addObject("showUrltoUser", "20 most recent backlinks for '" + domName + "'");
                }
                session.removeAttribute("notWorkingUrl");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                /* Ask The Expert Block Start */
                String domain = request.getParameter("domain");
                List<AhrefBacklink> backLinks = null;
                Map<String, Long> counts = null;
                Map<Boolean, Long> countDoFollowLinks = null;
                backLinks = (ArrayList<AhrefBacklink>) session.getAttribute("blcBacklinks");
                if (backLinks != null) {
                    counts = backLinks.stream().collect(Collectors.groupingBy(e -> e.getAnchorText(), Collectors.counting()));
                }
                String numberOfBackLinksText = "";
                String questionInfo = "";
                boolean issueStatus = false;
                long badBackLinksCount = 0;
//                    Getting the no. of bad backlinks
                if (backLinks != null && !backLinks.isEmpty()) {
                    badBackLinksCount = backLinks.stream().filter(item -> item.getRating() <= 5).count();
                    if (badBackLinksCount > 0) {
                        issueStatus = true;
                        numberOfBackLinksText = "There are currently <span style='color:red'>" + badBackLinksCount + "</span> bad backlinks that could be harmful to your webpage. "
                                + "Identifying and removing bad backlinks can help give your site more credibility with search engines.";
                    }
                }
//                    Getting the duplicate anchor text which is > 20%
                String duplicateAnchorText = "";
                if (backLinks != null && counts != null && !counts.isEmpty()) {
                    for (Map.Entry<String, Long> entry : counts.entrySet()) {
                        int val = (int) (long) entry.getValue();

                        float backLinkPercentage = (val * 100) / backLinks.size();
                        if (backLinkPercentage >= 20.0) {
                            if (!duplicateAnchorText.equals("")) {
                                duplicateAnchorText = duplicateAnchorText + ", " + entry.getKey();
                            } else {
                                duplicateAnchorText = entry.getKey();
                            }
                        }
                    }
                }
                String anchorTagText = "";
                if (!duplicateAnchorText.equals("")) {
                    issueStatus = true;
                    anchorTagText = "Many of your backlinks repeatedly use the same words or phrases as the anchor text. "
                            + "'" + duplicateAnchorText + "'" + " appears frequently in your backlinks. "
                            + "Adjusting backlinks to add variety can help to increase your SEO.";
                }

//                    Getting the no. of DoFollow Links which is rating <= 15%
                if (backLinks != null) {
                    countDoFollowLinks = backLinks.stream().collect(Collectors.groupingBy(e -> e.isNoFollow(), Collectors.counting()));
                }
                String doFollowText = "";
                float doFollowLinkPercentage = 0;
                if (backLinks != null && countDoFollowLinks != null && !countDoFollowLinks.isEmpty()) {
                    for (Map.Entry<Boolean, Long> entry : countDoFollowLinks.entrySet()) {
                        if (!entry.getKey()) {
                            int val = (int) (long) entry.getValue();
                            int size = backLinks.size();
                            doFollowLinkPercentage = (val * 100) / size;
                            if (doFollowLinkPercentage <= 15.0) {
                                issueStatus = true;
                                doFollowText = "Your webpage has <span style='color:red'>" + doFollowLinkPercentage + "%</span> DoFollow links. "
                                        + "More DoFollow links on your site can positively affect the overall ranking of your webpage. ";
                            }
                        }
                    }
                }
                String toolIssues = "";
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                if (issueStatus) {
                    questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
                    toolIssues = "<ul>";
                    if (badBackLinksCount > 0) {
                        questionInfo += "- " + "Fixing " + badBackLinksCount + " of bad backlinks." + "\n";
                        toolIssues += "<li>" + numberOfBackLinksText + "</li>";
                    }
                    if (!duplicateAnchorText.equals("")) {
                        questionInfo += "- " + "Little variation in anchor texts of backlinks. " + duplicateAnchorText + " repeating more than 20% of the times." + "\n";
                        toolIssues += "<li>" + anchorTagText + "</li>";
                    }
                    if (doFollowLinkPercentage <= 15.0 && !doFollowText.equals("")) {
                        questionInfo += "- " + "Increase do follow backlinks." + "\n";
                        toolIssues += "<li>" + doFollowText + "</li>";
                    }
                    toolIssues += "</ul>";
                }
                lxrmAskExpert.setDomain(domain);
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(Common.INBOUND_LINK_CHECKER);
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);
//                    if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), domain, toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }
                return null;
            }
            /* Ask The Expert Block End */
            inboundLinkCheckerTool = new InboundLinkChecker();
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "inboundLinkCheckerTool", inboundLinkCheckerTool);
        session.removeAttribute("notWorkingUrl");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        InboundLinkChecker inboundLinkCheckerTool = (InboundLinkChecker) session.getAttribute("inboundLinkCheckerTool");
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        session.removeAttribute("paidDownloadedReportDetailsList");
        if (user != null && userLoggedIn) {
            List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = toolService.fetchPaidDownladReports(user.getId(), 20);
            session.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
        }
        if (inboundLinkCheckerTool == null) {
            inboundLinkCheckerTool = new InboundLinkChecker();
        }
        return inboundLinkCheckerTool;
    }
}
