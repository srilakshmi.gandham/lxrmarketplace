/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.inboundlinkchecker;

import java.util.ArrayList;
import java.util.List;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefRefDomains;
import org.apache.log4j.Logger;

/**
 *
 * @author santosh
 */
public class BackLinksInsertingTask implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(BackLinksInsertingTask.class);

    private final InboundLinkCheckerService inboundLinkCheckerService;
    private final List<AhrefBacklink> backlinks;
    private final List<AhrefRefDomains> referringDomains;
    private final AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo;
    private final boolean saveBackLinkData;
    private long domainId;

    public BackLinksInsertingTask(InboundLinkCheckerService inboundLinkCheckerService, AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo, ArrayList<AhrefBacklink> backlinks, ArrayList<AhrefRefDomains> referringDomains, boolean saveBackLinkData, long domainId) {
        this.inboundLinkCheckerService = inboundLinkCheckerService;
        this.ahrefBackLinkDomainInfo = ahrefBackLinkDomainInfo;
        this.backlinks = backlinks;
        this.referringDomains = referringDomains;
        this.domainId = domainId;
        this.saveBackLinkData = saveBackLinkData;
    }

    @Override
    public void run() {
        inboundLinkCheckerService.updateBackLinkDomLevelData(ahrefBackLinkDomainInfo, domainId);
        if (saveBackLinkData && (backlinks != null && !backlinks.isEmpty())) {
            inboundLinkCheckerService.savebacklinksData(domainId, backlinks);
        } else {
            LOGGER.info("Backlinks data not saved and saveBackLinkData:: " + saveBackLinkData + ", and backlinks:: " + (backlinks != null ? backlinks.size() : 0) + ", for domainId:: " + domainId);
        }
        if (referringDomains != null && !referringDomains.isEmpty()) {
            inboundLinkCheckerService.saveReferringDomainData(domainId, referringDomains);
        } else {
            LOGGER.info("ReferringDomains data not saved and referringDomains:: " + (referringDomains != null ? referringDomains.size() : 0) + ", for domainId:: " + domainId);
        }

    }

}
