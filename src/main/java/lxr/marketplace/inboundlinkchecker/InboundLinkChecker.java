package lxr.marketplace.inboundlinkchecker;

public class InboundLinkChecker {
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
