package lxr.marketplace.inboundlinkchecker;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import lxr.marketplace.apiaccess.AhrefAPIService;
import lxr.marketplace.apiaccess.GoogleCustomSearch;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefRefDomains;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.CommonReportGenerator;
import lxr.marketplace.util.ServiceAuthEncryption;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.client.RestTemplate;

public class InboundLinkCheckerService extends JdbcDaoSupport {

    private static final Logger LOGGER = Logger.getLogger(InboundLinkCheckerService.class);

    ApplicationContext context = ApplicationContextProvider.getApplicationContext();

    private AhrefAPIService ahrefAPIService;
    private GoogleCustomSearch googleCustomSearch;
    private String googleCustomSearchApiKeyLxrm;
    private MessageSource messageSource;
    private final DecimalFormat paymentFormat = new DecimalFormat("##,##,##,###.00");

    private final String[] columnNamesOfReport = {"S.NO", "REFERRING URL", "REFERRING URL RATING", "ANCHOR TEXT", "LAST CHECKED"};
    private final String[] domainNameInfoMetrics = {"Number of Backlinks", "Referring Domains", "Total No Follow Links", "Governmental Links", "Educational Links", "Indexed Pages in Google"};
    private final SimpleDateFormat reportGeneratedDateFormat = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
    public static final String REPORT_FILE_NAME = "LXRMarketplace_Inbound_Link_Checker_Report";
    public static final String REPORT_TITLE = "Inbound Link Checker Report";

    public void setGoogleCustomSearchApiKeyLxrm(String googleCustomSearchApiKeyLxrm) {
        this.googleCustomSearchApiKeyLxrm = googleCustomSearchApiKeyLxrm;
    }

    public void setAhrefAPIService(AhrefAPIService ahrefAPIService) {
        this.ahrefAPIService = ahrefAPIService;
    }

    public void setGoogleCustomSearch(GoogleCustomSearch googleCustomSearch) {
        this.googleCustomSearch = googleCustomSearch;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public AhrefBackLinkDomainInfo getbackLinkData(String domainName, ArrayList<AhrefBacklink> backLinks, long count) {
        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = ahrefAPIService.getDomainLevelData(domainName);
        if (ahrefBackLinkDomainInfo != null) {
            long googleIndexedPages = googleCustomSearch.countIndexedPagesInGoogle(googleCustomSearchApiKeyLxrm, domainName);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar backLinkFetchedDate = Calendar.getInstance();
            ahrefBackLinkDomainInfo.setBackLinkFetchedDate(sdf.format(backLinkFetchedDate.getTime()));
            ahrefBackLinkDomainInfo.setDomainName(domainName);
            ahrefBackLinkDomainInfo.setFetchedBackLinks(count);
            ahrefBackLinkDomainInfo.setGoogleIndexedPages(googleIndexedPages);
            ahrefAPIService.getBackLinkData(domainName, backLinks, 0, count);
        }
        return ahrefBackLinkDomainInfo;
    }

    public String getReferringDomainData(String domainName, ArrayList<AhrefRefDomains> referringDomains, int referringDomainCount) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar refDomainFetchedDate = Calendar.getInstance();
        ahrefAPIService.getReferringDomainsData(domainName, referringDomains, referringDomainCount);
        return dateFormat.format(refDomainFetchedDate.getTime());
    }

    public synchronized long insertBackLinkDomLevelData(String domName) {
        String query = " INSERT INTO inboundlinks_domains(domain_name) values(?)";
        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();

        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(query, new String[]{"id"});
                ps.setString(1, domName);
                return ps;
            }
        }, generatedKeyHolder);
        return generatedKeyHolder.getKey().longValue();
    }

    public synchronized void updateBackLinkDomLevelData(AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo, long domainId) {

        String query = "UPDATE inboundlinks_domains SET fetched_date = ? ,fetched_backlinks = ? ,refdomain_fetched_date = ?,ahrefs_backlinks = ? ,ref_pages = ?, "
                + " nofollow_links = ?,dofollow_links = ?,canonical = ?,gov_links = ?,edu_links = ?,rss_links = ?,internal_links = ? ,external_links = ?, "
                + " ref_domains = ? , html_pages = ?,google_Indexed_pages = ?  WHERE domain_id = ? ";
        getJdbcTemplate().update(query, new Object[]{ahrefBackLinkDomainInfo.getBackLinkFetchedDate(), ahrefBackLinkDomainInfo.getFetchedBackLinks(), ahrefBackLinkDomainInfo.getRefDomainFetchedDate(),
            ahrefBackLinkDomainInfo.getAhrefsbackLinks(), ahrefBackLinkDomainInfo.getRefPages(), ahrefBackLinkDomainInfo.getNoFollowLinks(),
            ahrefBackLinkDomainInfo.getDoFollowLinks(), ahrefBackLinkDomainInfo.getCanonical(), ahrefBackLinkDomainInfo.getGovLinks(),
            ahrefBackLinkDomainInfo.getEduLinks(), ahrefBackLinkDomainInfo.getRssLinks(), ahrefBackLinkDomainInfo.getInternalLinks(),
            ahrefBackLinkDomainInfo.getExternalLinks(), ahrefBackLinkDomainInfo.getRefdomains(), ahrefBackLinkDomainInfo.getHtmlPages(), ahrefBackLinkDomainInfo.getGoogleIndexedPages(),
            domainId});
    }

    public void savebacklinksData(long domainId, List<AhrefBacklink> backLinks) {
        deletePreviousBackLinkData(domainId);
        insertBackLinkData(domainId, backLinks);
    }

    public void deletePreviousBackLinkData(long domainId) {

        final String query = "DELETE FROM inbound_links WHERE domain_id = ?";
        getJdbcTemplate().update(query, new Object[]{domainId});
    }

    public synchronized void insertBackLinkData(long domainId, List<AhrefBacklink> backLinks) {
        try {
            final String query = "INSERT INTO inbound_links(domain_id, source_url,target_url, ahrefs_rank, domain_rating, "
                    + " internal_links, external_links, title, lastvisited, alt_text, anchor_text, anchor_type, nofollow) "
                    + " values(?,?,?,?,?,?,?,?,?,?,?,?,?)";

            final int batchSize = backLinks != null ? backLinks.size() : 0;
            SimpleDateFormat ahrefsFormat = new SimpleDateFormat("MMMM dd, yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            int[][] updateCounts;
            updateCounts = getJdbcTemplate().batchUpdate(query, backLinks, batchSize, new ParameterizedPreparedStatementSetter<AhrefBacklink>() {
                @Override
                public void setValues(PreparedStatement ps, AhrefBacklink ahrefBacklink) {
                    try {
                        ps.setLong(1, domainId);
                        if (ahrefBacklink.getSourceURL() == null || ahrefBacklink.getSourceURL().equals("")) {
                            ps.setString(2, "");
                        } else {
                            try {
                                ps.setBytes(2, URLDecoder.decode(ahrefBacklink.getSourceURL(), "UTF-8").getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException ex) {
                                ps.setString(2, ahrefBacklink.getSourceURL());
                                LOGGER.error("UnsupportedEncodingException in saving backlinks for backlink SourceURL:: " + ahrefBacklink.getSourceURL() + ", and cause:: " + ex.getMessage());
                            } catch (Exception ex) {
                                ps.setString(2, ahrefBacklink.getSourceURL());
                                LOGGER.error("Exception in saving backlinks for backlink SourceURL:: " + ahrefBacklink.getSourceURL() + ", and cause:: " + ex.getMessage());
                            }
                        }

                        if (ahrefBacklink.getTargetURL() == null || ahrefBacklink.getTargetURL().equals("")) {
                            ps.setString(3, "");
                        } else {
                            try {
                                ps.setBytes(3, URLDecoder.decode(ahrefBacklink.getTargetURL(), "UTF-8").getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException ex) {
                                ps.setString(3, ahrefBacklink.getTargetURL());
                                LOGGER.error("UnsupportedEncodingException in saving backlinks for backlink TargetURL:: " + ahrefBacklink.getTargetURL() + ", and cause:: " + ex.getMessage());
                            } catch (Exception ex) {
                                ps.setString(3, ahrefBacklink.getTargetURL());
                                LOGGER.error("Exception in saving backlinks for backlink TargetURL:: " + ahrefBacklink.getTargetURL() + ", and cause:: " + ex.getMessage());
                            }
                        }
                        ps.setFloat(4, ahrefBacklink.getRating());
                        ps.setFloat(5, ahrefBacklink.getDomainRating());
                        ps.setLong(6, ahrefBacklink.getInternalLinks());
                        ps.setLong(7, ahrefBacklink.getExternalLinks());
                        ps.setString(8, (ahrefBacklink.getTitle() != null ? getFixedContent(ahrefBacklink.getTitle()) : ""));
                        Date lastVisitedDate = null;
                        try {
                            lastVisitedDate = ahrefsFormat.parse(ahrefBacklink.getLastVisitedDateString());
                        } catch (ParseException e) {
                            LOGGER.error("ParseException when converting  LastVisitedDateString key into Date object for value :: " + ahrefBacklink.getLastVisitedDateString() + " , and cause:: " + e.getLocalizedMessage());
                        }
                        ps.setString(9, lastVisitedDate != null ? sdf.format(lastVisitedDate.getTime()) : null);
                        ps.setString(10, (ahrefBacklink.getAltText() != null ? getFixedContent(ahrefBacklink.getAltText()) : ""));
                        if (ahrefBacklink.getAnchorText() == null || ahrefBacklink.getAnchorText().equals("")) {
                            ps.setString(11, "");
                        } else {
                            try {
                                ps.setBytes(11, getFixedContent(ahrefBacklink.getAnchorText()).getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException ex) {
                                ps.setString(11, getFixedContent(ahrefBacklink.getAnchorText()));
                                LOGGER.error("UnsupportedEncodingException in saving backlinks for backlink AnchorText:: " + ahrefBacklink.getAnchorText() + ", and cause:: " + ex.getMessage());
                            } catch (Exception ex) {
                                ps.setString(11, getFixedContent(ahrefBacklink.getAnchorText()));
                                LOGGER.error("Exception in saving backlinks for backlink AnchorText:: " + ahrefBacklink.getAnchorText() + ", and cause:: " + ex.getMessage());
                            }
                        }
                        ps.setString(12, ahrefBacklink.getAnchorType());
                        ps.setBoolean(13, ahrefBacklink.isNoFollow());
                    } catch (Exception e) {
                        LOGGER.error("Exception in insertBackLinkData when saving each backlink for total size:: " + backLinks.size() + ", and for domainId:: " + domainId);
                    }
                }
            });
            LOGGER.info("Saved backlinks count:: " + (updateCounts != null ? updateCounts.length : 0) + ", for domain Id:: " + domainId);
        } catch (Exception e) {
            LOGGER.error("Exception in insertBackLinkData for domain Id:: " + domainId + ", cause :: " + e.getLocalizedMessage());
        }
    }

    private String getFixedContent(String queryText) {
        return queryText.length() > 254 ? queryText.substring(0, 253) : queryText;
    }

    public void saveReferringDomainData(long domainId, List<AhrefRefDomains> refDomains) {
        deletePreviousReferringDomainData(domainId);
        insertReferringDomainData(domainId, refDomains);
    }

    public void deletePreviousReferringDomainData(long domainId) {

        final String query = "DELETE FROM inbound_refdomains WHERE domain_id = ?";
        getJdbcTemplate().update(query, new Object[]{domainId});
    }

    public synchronized void insertReferringDomainData(long domainId, List<AhrefRefDomains> refDomains) {

        final String query = "INSERT INTO inbound_refdomains(domain_id, refdomain,backlinks, refpages, first_seen, "
                + " last_visited, domain_rating) values(?,?,?,?,?,?,?)";
        final int batchSize = refDomains.size();
        SimpleDateFormat ahrefsFormat = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        getJdbcTemplate().batchUpdate(query, refDomains, batchSize, new ParameterizedPreparedStatementSetter<AhrefRefDomains>() {
            @Override
            public void setValues(PreparedStatement ps, AhrefRefDomains referringDomain) {
                try {
                    ps.setLong(1, domainId);
                    if (referringDomain.getRefdomain() == null || referringDomain.getRefdomain().equals("")) {
                        ps.setString(2, "");
                    } else {
                        try {
                            ps.setBytes(2, URLDecoder.decode(referringDomain.getRefdomain(), "UTF-8").getBytes("UTF-8"));
                        } catch (UnsupportedEncodingException ex) {
                            ps.setString(2, referringDomain.getRefdomain());
                            LOGGER.error("UnsupportedEncodingException in saving reffering domain for reffering domain Refdomain:: " + referringDomain.getRefdomain() + ", and cause:: " + ex.getMessage());
                        } catch (Exception ex) {
                            ps.setString(2, referringDomain.getRefdomain());
                            LOGGER.error("Exception in saving reffering domain for reffering domain Refdomain:: " + referringDomain.getRefdomain() + ", and cause:: " + ex.getMessage());
                        }
                    }
                    ps.setFloat(3, referringDomain.getBacklinks());
                    ps.setInt(4, referringDomain.getRefpages());
                    Date tempDate = null;
                    try {
                        tempDate = ahrefsFormat.parse(referringDomain.getFirstSeen());
                    } catch (ParseException e) {
                        LOGGER.error("ParseException when converting referringDomain FirstSeen key into Date object for value :: " + referringDomain.getFirstSeen() + " , and cause:: " + e.getLocalizedMessage());
                    }
                    if (tempDate != null) {
                        ps.setString(5, sdf.format(tempDate.getTime()));
                    } else {
                        ps.setString(5, null);
                    }
                    tempDate = null;
                    try {
                        tempDate = ahrefsFormat.parse(referringDomain.getLastVisited());
                    } catch (ParseException e) {
                        LOGGER.error("ParseException when converting referringDomain LastVisited key into Date object for value :: " + referringDomain.getLastVisited() + " , and cause:: " + e.getLocalizedMessage());
                    }
                    if (tempDate != null) {
                        ps.setString(6, sdf.format(tempDate.getTime()));
                    } else {
                        ps.setString(6, null);
                    }
                    ps.setFloat(7, referringDomain.getDomainRating());
                } catch (Exception e) {
                    LOGGER.error("Exception in insertReferringDomainData for domain Id:: " + domainId + ", cause :: " + e.getLocalizedMessage());
                }
            }
        });

    }

    public AhrefBackLinkDomainInfo fetchDomainLevelData(String domainName) {
        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
        String query = "SELECT * FROM inboundlinks_domains WHERE domain_name = ?";
        try {
            ahrefBackLinkDomainInfo = getJdbcTemplate().queryForObject(query, new Object[]{domainName}, new RowMapper<AhrefBackLinkDomainInfo>() {
                @Override
                public AhrefBackLinkDomainInfo mapRow(ResultSet rs, int i) throws SQLException {
                    AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = new AhrefBackLinkDomainInfo();
                    ahrefBackLinkDomainInfo.setDomainId(rs.getLong("domain_id"));
                    ahrefBackLinkDomainInfo.setDomainName(rs.getString("domain_name"));
                    ahrefBackLinkDomainInfo.setFetchedBackLinks(rs.getLong("fetched_backlinks"));
                    ahrefBackLinkDomainInfo.setBackLinkFetchedDate(rs.getString("fetched_date"));
                    ahrefBackLinkDomainInfo.setRefDomainFetchedDate(rs.getString("refdomain_fetched_date"));
                    ahrefBackLinkDomainInfo.setAhrefsbackLinks(rs.getLong("ahrefs_backlinks"));
                    ahrefBackLinkDomainInfo.setRefPages(rs.getLong("ref_pages"));
                    ahrefBackLinkDomainInfo.setNoFollowLinks(rs.getLong("nofollow_links"));
                    ahrefBackLinkDomainInfo.setDoFollowLinks(rs.getLong("dofollow_links"));
                    ahrefBackLinkDomainInfo.setCanonical(rs.getLong("canonical"));
                    ahrefBackLinkDomainInfo.setGovLinks(rs.getLong("gov_links"));
                    ahrefBackLinkDomainInfo.setEduLinks(rs.getLong("edu_links"));
                    ahrefBackLinkDomainInfo.setRssLinks(rs.getLong("rss_links"));
                    ahrefBackLinkDomainInfo.setInternalLinks(rs.getLong("internal_links"));
                    ahrefBackLinkDomainInfo.setExternalLinks(rs.getLong("external_links"));
                    ahrefBackLinkDomainInfo.setRefdomains(rs.getLong("ref_domains"));
                    ahrefBackLinkDomainInfo.setHtmlPages(rs.getLong("html_pages"));
                    ahrefBackLinkDomainInfo.setGoogleIndexedPages(rs.getLong("google_Indexed_pages"));

                    return ahrefBackLinkDomainInfo;
                }
            });

        } catch (DataAccessException e) {
            return null;
        }
        return ahrefBackLinkDomainInfo;
    }

    public ArrayList<AhrefBacklink> fetchBackLinkData(long domainId, long count) {
        ArrayList<AhrefBacklink> ahrefBacklinksList = new ArrayList<>();
        String query = "SELECT * FROM inbound_links WHERE domain_id = ?  LIMIT 0 , ?";
        SimpleDateFormat ahrefsFormat = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        getJdbcTemplate().query(query, new Object[]{domainId, count}, new RowMapper<ArrayList<AhrefBacklink>>() {
            @Override
            public ArrayList<AhrefBacklink> mapRow(ResultSet rs, int i) throws SQLException {
                AhrefBacklink ahrefBacklink = new AhrefBacklink();
                ahrefBacklink.setSourceURL(rs.getString("source_url"));
                ahrefBacklink.setTargetURL(rs.getString("target_url"));
                ahrefBacklink.setRating(rs.getFloat("ahrefs_rank"));
                ahrefBacklink.setDomainRating(rs.getFloat("domain_rating"));
                ahrefBacklink.setInternalLinks(rs.getLong("internal_links"));
                ahrefBacklink.setExternalLinks(rs.getLong("external_links"));
                ahrefBacklink.setTitle(rs.getString("title"));
                Date lastVisitedDate = null;
                try {
                    lastVisitedDate = sdf.parse(rs.getString("lastvisited"));
                } catch (ParseException e) {
                    LOGGER.error("error inconversion", e);
                }
                ahrefBacklink.setLastVisitedDateString(ahrefsFormat.format(lastVisitedDate));
                ahrefBacklink.setAltText(rs.getString("alt_text"));
                ahrefBacklink.setAnchorText(rs.getString("anchor_text"));
                ahrefBacklink.setAnchorType(rs.getString("anchor_type"));
                ahrefBacklink.setNoFollow(rs.getBoolean("nofollow"));
                if (rs.getBoolean("nofollow")) {
                    ahrefBacklink.setNoFollowUrl("No Follow");
                } else {
                    ahrefBacklink.setNoFollowUrl("Do Follow");
                }
                ahrefBacklinksList.add(ahrefBacklink);
                return ahrefBacklinksList;
            }
        });
        return ahrefBacklinksList;
    }

    public ArrayList<AhrefRefDomains> fetchReferringDomainData(long domainId) {
        ArrayList<AhrefRefDomains> ahrefRefDomainsList = new ArrayList<>();
        String query = "SELECT * FROM inbound_refdomains WHERE domain_id = ?";
        SimpleDateFormat ahrefsFormat = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        getJdbcTemplate().query(query, new Object[]{domainId}, new RowMapper<ArrayList<AhrefRefDomains>>() {
            @Override
            public ArrayList<AhrefRefDomains> mapRow(ResultSet rs, int i) throws SQLException {

                AhrefRefDomains ahrefRefDomains = new AhrefRefDomains();
                ahrefRefDomains.setRefdomain(rs.getString("refdomain"));
                ahrefRefDomains.setBacklinks(rs.getInt("backlinks"));
                ahrefRefDomains.setRefpages(rs.getInt("refpages"));
                ahrefRefDomains.setDomainRating(rs.getFloat("domain_rating"));
                Date tempDate = null;
                try {
                    tempDate = sdf.parse(rs.getString("first_seen"));
                } catch (ParseException e) {
                    LOGGER.error("error inconversion", e);
                }
                ahrefRefDomains.setFirstSeen(ahrefsFormat.format(tempDate));
                try {
                    tempDate = sdf.parse(rs.getString("last_visited"));
                } catch (ParseException e) {
                    LOGGER.error("error inconversion", e);
                }
                ahrefRefDomains.setLastVisited(ahrefsFormat.format(tempDate));
                ahrefRefDomainsList.add(ahrefRefDomains);
                return ahrefRefDomainsList;
            }
        });
        return ahrefRefDomainsList;
    }

    public JSONObject backLinkDataService(String type, String domainName, String hostingURL) {
        String requestUrl = hostingURL + "?domain=" + domainName + "&type=" + type;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("LXRM_SEO_BACKLINKS", ServiceAuthEncryption.crypt(domainName));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JSONObject> response = restTemplate.exchange(requestUrl, HttpMethod.POST, entity, JSONObject.class);
        JSONObject backLinksData = null;
        if (response.getBody() != null && !response.getBody().isEmpty()) {
            backLinksData = response.getBody();
        }
        return backLinksData;
    }

    /*Generates Price List based on backlinks count.
    Price Slaps:
    2.99$ for Count <= 100 
    9.99$ for Count >= 101 && <=500
    14.99$ for Count >= 501 && <=1000
    Total BackLinks * 0.01 for Count >= 1001 
     */
    public LinkedHashMap<String, String> getPriceListForBackLinks(long totalBackLinksCount) {
        LinkedHashMap<String, String> priceList = new LinkedHashMap<>();
        if (totalBackLinksCount > 0 || totalBackLinksCount <= 100) {
            priceList.put(String.valueOf(Common.BACK_LINK_COUNT), "0.0");
        }
        if ((totalBackLinksCount > 100 || totalBackLinksCount <= 500) && (totalBackLinksCount > 100)) {
            priceList.put("500", "9.99");
        }
        if ((totalBackLinksCount > 500 || totalBackLinksCount <= 1000) && (totalBackLinksCount > 500)) {
            priceList.put("1000", "14.99");
        }
        if (totalBackLinksCount > 1000) {
            if (totalBackLinksCount > 100000) {
                priceList.put("100000", "999.99");
                /*To calculate amount for above 100000 backlinks
                    if ((totalBackLinksCount > 100000 || totalBackLinksCount <= 500000) && (totalBackLinksCount > 100000)) {
                            totalPrice = ((totalBackLinksCount - 100000) * 0.01) + 999.99;
                    }
                 */
            } else {
                double totalBackLinksPrice = ((totalBackLinksCount - 1000) * 0.01) + 14.98;
                priceList.put(String.valueOf(totalBackLinksCount), (paymentFormat.format(totalBackLinksPrice)));
            }
        }
        return priceList;
    }

    /*Retruns amount in payable format converting from string*/
    public double getPayableFormatAmount(String requestedAmount) {
        double amount = 0.0;
        try {
            amount = paymentFormat.parse(requestedAmount).doubleValue();
        } catch (ParseException e) {
            amount = 0.0;
            logger.error("Exception in getPayableFormatAmount: " + e.getMessage());
        }
        return amount;
    }

    /*
    public AhrefBackLinkDomainInfo getPreparedBackLinks(List<AhrefBacklink> backLinks, Map<String, Double> backLinksPrice, String applicationURL, String queryDomain, StringBuilder status, String backLinkQuota) {
        JSONObject resposeJsonObject = backLinkDataService(applicationURL, backLinkQuota, queryDomain, status);
        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = new AhrefBackLinkDomainInfo();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            if (resposeJsonObject != null) {
                ahrefBackLinkDomainInfo = objectMapper.readValue(objectMapper.writeValueAsString(resposeJsonObject.get("metrics")), new TypeReference<AhrefBackLinkDomainInfo>() {
                });
                backLinks = objectMapper.readValue(objectMapper.writeValueAsString(resposeJsonObject.get("backlinksInfo")), new TypeReference<List<AhrefBacklink>>() {
                });
                backLinksPrice = objectMapper.readValue(objectMapper.writeValueAsString(resposeJsonObject.get("backlinksPrice")), new TypeReference<LinkedHashMap<String, Double>>() {
                });
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(InboundLinkCheckerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ahrefBackLinkDomainInfo;
    }*/
    public String createBackLinkXLSFile(String finalDomName, String downloadfolder, 
            List<AhrefBacklink> backLinks, AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo, 
            long userId,String reportFileName,String reportTitle) {
        try {

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet("No Follow and DoFollow URL(s)");
            /*Hideing all cell borders*/
            sheet.setDisplayGridlines(false);
            HSSFCellStyle headCelStyle;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);
            // Heading Cell Style
            headCelStyle = workBook.createCellStyle();
            HSSFFont font = workBook.createFont();
            font.setBold(true);
            headCelStyle.setFont(font);
            headCelStyle.setWrapText(true);

            HSSFCellStyle snoCelSty = workBook.createCellStyle();
            snoCelSty.setFont(font);
            snoCelSty.setWrapText(true);
            snoCelSty.setVerticalAlignment(VerticalAlignment.CENTER);
            snoCelSty.setAlignment(HorizontalAlignment.CENTER);

            HSSFCellStyle mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            HSSFFont headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            HSSFFont boldFontStyle = workBook.createFont();
            boldFontStyle.setBold(true);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(boldFontStyle);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
//            sheet.createFreezePane(0, 1);

            //Style for Date
            CellStyle dateCellStyle = workBook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMMM dd, yyyy"));
            dateCellStyle.setAlignment(HorizontalAlignment.LEFT);
            dateCellStyle.setBorderBottom(BorderStyle.THIN);
            dateCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            dateCellStyle.setBorderRight(BorderStyle.THIN);
            dateCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            dateCellStyle.setBorderLeft(BorderStyle.THIN);
            dateCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            dateCellStyle.setBorderTop(BorderStyle.THIN);
            dateCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFFont headerColoumFont = workBook.createFont();
            headerColoumFont.setBold(true);
            headerColoumFont.setColor(HSSFColor.WHITE.index);

            HSSFPalette palette = null;
            HSSFColor hssfColor = null;

            HSSFCellStyle headerColoumStyle = workBook.createCellStyle();
            headerColoumStyle.setFont(headerColoumFont);
            headerColoumStyle.setWrapText(true);
            palette = workBook.getCustomPalette();
            palette.setColorAtIndex(HSSFColor.BLACK.index, (byte) 0, (byte) 0, (byte) 0);
            hssfColor = palette.getColor(HSSFColor.BLACK.index);
            headerColoumStyle.setFillForegroundColor(hssfColor.getIndex());
            headerColoumStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerColoumStyle.setAlignment(HorizontalAlignment.CENTER);
            headerColoumStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFFont normalFont = workBook.createFont();
            normalFont.setFontHeightInPoints((short) 10);
            normalFont.setFontName(HSSFFont.FONT_ARIAL);

            HSSFCellStyle noDataStyle = workBook.createCellStyle();
            noDataStyle.setFont(normalFont);
            noDataStyle.setWrapText(true);
            noDataStyle.setAlignment(HorizontalAlignment.CENTER);
            noDataStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            noDataStyle.setBorderBottom(BorderStyle.THIN);
            noDataStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            noDataStyle.setBorderRight(BorderStyle.THIN);
            noDataStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            noDataStyle.setBorderLeft(BorderStyle.THIN);
            noDataStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle topBorderCellStyle = workBook.createCellStyle();
            topBorderCellStyle.setFont(boldFontStyle);
            topBorderCellStyle.setWrapText(true);
            topBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            topBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            topBorderCellStyle.setBorderTop(BorderStyle.THIN);
            topBorderCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle bottomBorderCellStyle = workBook.createCellStyle();
            bottomBorderCellStyle.setFont(normalFont);
            bottomBorderCellStyle.setWrapText(true);
            bottomBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            bottomBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            bottomBorderCellStyle.setBorderBottom(BorderStyle.THIN);
            bottomBorderCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftBorderCellStyle = workBook.createCellStyle();
            leftBorderCellStyle.setFont(boldFontStyle);
            leftBorderCellStyle.setWrapText(true);
            leftBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            leftBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            leftBorderCellStyle.setBorderLeft(BorderStyle.THIN);
            leftBorderCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftTopBorderCellStyle = workBook.createCellStyle();
            leftTopBorderCellStyle.setFont(boldFontStyle);
            leftTopBorderCellStyle.setWrapText(true);
            leftTopBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            leftTopBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            leftTopBorderCellStyle.setBorderLeft(BorderStyle.THIN);
            leftTopBorderCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            leftTopBorderCellStyle.setBorderTop(BorderStyle.THIN);
            leftTopBorderCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftBottomBorderCellStyle = workBook.createCellStyle();
            leftBottomBorderCellStyle.setFont(normalFont);
            leftBottomBorderCellStyle.setWrapText(true);
            leftBottomBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            leftBottomBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            leftBottomBorderCellStyle.setBorderLeft(BorderStyle.THIN);
            leftBottomBorderCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            leftBottomBorderCellStyle.setBorderBottom(BorderStyle.THIN);
            leftBottomBorderCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle rightBorderCellStyle = workBook.createCellStyle();
            rightBorderCellStyle.setFont(boldFontStyle);
            rightBorderCellStyle.setWrapText(true);
            rightBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            rightBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            rightBorderCellStyle.setBorderRight(BorderStyle.THIN);
            rightBorderCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle rightTopBorderCellStyle = workBook.createCellStyle();
            rightTopBorderCellStyle.setFont(boldFontStyle);
            rightTopBorderCellStyle.setWrapText(true);
            rightTopBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            rightTopBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            rightTopBorderCellStyle.setBorderRight(BorderStyle.THIN);
            rightTopBorderCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightTopBorderCellStyle.setBorderTop(BorderStyle.THIN);
            rightTopBorderCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle rightBottomBorderCellStyle = workBook.createCellStyle();
            rightBottomBorderCellStyle.setFont(normalFont);
            rightBottomBorderCellStyle.setWrapText(true);
            rightBottomBorderCellStyle.setAlignment(HorizontalAlignment.CENTER);
            rightBottomBorderCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            rightBottomBorderCellStyle.setBorderRight(BorderStyle.THIN);
            rightBottomBorderCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightBottomBorderCellStyle.setBorderBottom(BorderStyle.THIN);
            rightBottomBorderCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftBorderNormalCellStyle = workBook.createCellStyle();
            leftBorderNormalCellStyle.setFont(normalFont);
            leftBorderNormalCellStyle.setWrapText(true);
            leftBorderNormalCellStyle.setAlignment(HorizontalAlignment.CENTER);
            leftBorderNormalCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            leftBorderNormalCellStyle.setBorderLeft(BorderStyle.THIN);
            leftBorderNormalCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle rightBorderNormalCellStyle = workBook.createCellStyle();
            rightBorderNormalCellStyle.setFont(normalFont);
            rightBorderNormalCellStyle.setWrapText(true);
            rightBorderNormalCellStyle.setAlignment(HorizontalAlignment.CENTER);
            rightBorderNormalCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            rightBorderNormalCellStyle.setBorderRight(BorderStyle.THIN);
            rightBorderNormalCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle metricTitleCellStyle = workBook.createCellStyle();
            metricTitleCellStyle.setFont(normalFont);
            metricTitleCellStyle.setWrapText(true);
            metricTitleCellStyle.setAlignment(HorizontalAlignment.CENTER);
            metricTitleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFCellStyle metricValueCellStyle = workBook.createCellStyle();
            metricValueCellStyle.setFont(boldFontStyle);
            metricValueCellStyle.setWrapText(true);
            metricValueCellStyle.setAlignment(HorizontalAlignment.CENTER);
            metricValueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            int rowCount = 0;
            HSSFRow commonRowObject = null;
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis());
            reportGeneratedDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reportGeneratedDateFormat.format(startDat);
            /*Row for report title*/
            rows = sheet.createRow(rowCount);
            commonRowObject = sheet.getRow(0);
            commonRowObject.setHeight((short) 900);
            sheet.setColumnWidth(0, (short) (sheet.getColumnWidth(0) + 256));
            /*Creating cell for report title*/
            cell = rows.createCell(1);
            cell.setCellValue(reportTitle);
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            /*Creating cell for review of*/
            cell = rows.createCell(2);
            cell.setCellValue("Review of: " + finalDomName + "\n" + "Review On: " + curDate);
            cell.setCellStyle(columnHeaderStyle);

            rowCount++;
            /*Eof report deatils: Title, Review on & Of*/
            rowCount++;
            /*Empty Row*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            rowCount++;
            /*Start of domain info and creating row for placing domain info values*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*Number of Backlinks value*/
            cell = rows.createCell(1);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getAhrefsbackLinks()));
            cell.setCellStyle(leftTopBorderCellStyle);
            /*Referring Domains Value*/
            cell = rows.createCell(2);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getRefdomains()));
            cell.setCellStyle(topBorderCellStyle);

            /*Total No Follow Links Value*/
            cell = rows.createCell(3);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getNoFollowLinks()));
            cell.setCellStyle(rightTopBorderCellStyle);

            rowCount++;
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*Number of Backlinks Title*/
            cell = rows.createCell(1);
            cell.setCellValue(domainNameInfoMetrics[0]);
            cell.setCellStyle(leftBorderNormalCellStyle);

            /*Referring Domains Title*/
            cell = rows.createCell(2);
            cell.setCellValue(domainNameInfoMetrics[1]);
            cell.setCellStyle(metricTitleCellStyle);

            /*Total No Follow Links Title*/
            cell = rows.createCell(3);
            cell.setCellValue(domainNameInfoMetrics[2]);
            cell.setCellStyle(rightBorderNormalCellStyle);
            /*Phase 2*/
            rowCount++;
            /*Start of domain info and creating row for placing domain info values*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*Governmental Links Value*/
            cell = rows.createCell(1);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getGovLinks()));
            cell.setCellStyle(leftBorderCellStyle);

            /*Educational Links Value*/
            cell = rows.createCell(2);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getEduLinks()));
            cell.setCellStyle(metricValueCellStyle);

            /*Indexed Pages in Google Value*/
            cell = rows.createCell(3);
            cell.setCellValue(String.valueOf(ahrefBackLinkDomainInfo.getGoogleIndexedPages()));
            cell.setCellStyle(rightBorderCellStyle);
            /*Eof domain info  placing values row*/

            rowCount++;
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*Governmental Links title*/
            cell = rows.createCell(1);
            cell.setCellValue(domainNameInfoMetrics[3]);
            cell.setCellStyle(leftBottomBorderCellStyle);

            /*Educational Links title*/
            cell = rows.createCell(2);
            cell.setCellValue(domainNameInfoMetrics[4]);
            cell.setCellStyle(bottomBorderCellStyle);

            /*Indexed Pages in Google title*/
            cell = rows.createCell(3);
            cell.setCellValue(domainNameInfoMetrics[5]);
            cell.setCellStyle(rightBottomBorderCellStyle);
            /*Eof domain metric titles row and cell content*/

            rowCount++;
            /*Empty Row*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            HSSFCellStyle backLinkValueCellStyle = workBook.createCellStyle();
            backLinkValueCellStyle.setFont(normalFont);
            backLinkValueCellStyle.setWrapText(true);
            backLinkValueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            backLinkValueCellStyle.setAlignment(HorizontalAlignment.LEFT);
            backLinkValueCellStyle.setBorderBottom(BorderStyle.THIN);
            backLinkValueCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            backLinkValueCellStyle.setBorderRight(BorderStyle.THIN);
            backLinkValueCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            backLinkValueCellStyle.setBorderLeft(BorderStyle.THIN);
            backLinkValueCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            backLinkValueCellStyle.setBorderTop(BorderStyle.THIN);
            backLinkValueCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle backLinkCenterCellStyle = workBook.createCellStyle();
            backLinkCenterCellStyle.setFont(normalFont);
            backLinkCenterCellStyle.setWrapText(true);
            backLinkCenterCellStyle.setAlignment(HorizontalAlignment.CENTER);
            backLinkValueCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            backLinkCenterCellStyle.setBorderBottom(BorderStyle.THIN);
            backLinkCenterCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            backLinkCenterCellStyle.setBorderRight(BorderStyle.THIN);
            backLinkCenterCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            backLinkCenterCellStyle.setBorderLeft(BorderStyle.THIN);
            backLinkCenterCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            backLinkCenterCellStyle.setBorderTop(BorderStyle.THIN);
            backLinkCenterCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());


            /*Cell style for normal text*/
            Font doFollowTitleFont = workBook.createFont();
            doFollowTitleFont.setFontHeightInPoints((short) 10);
            doFollowTitleFont.setFontName(HSSFFont.FONT_ARIAL);
            doFollowTitleFont.setBold(true);
            doFollowTitleFont.setColor(HSSFColor.GREEN.index);

            palette = workBook.getCustomPalette();
            palette.setColorAtIndex(HSSFColor.RED.index, (byte) 255, (byte) 69, (byte) 69);
            hssfColor = palette.getColor(HSSFColor.RED.index);
            Font noFollowTitleFont = workBook.createFont();
            noFollowTitleFont.setFontHeightInPoints((short) 10);
            noFollowTitleFont.setFontName(HSSFFont.FONT_ARIAL);
            noFollowTitleFont.setBold(true);
            noFollowTitleFont.setColor(hssfColor.getIndex());

            HSSFCellStyle noFollowCellStyle = workBook.createCellStyle();
            noFollowCellStyle.setFont(noFollowTitleFont);
            noFollowCellStyle.setWrapText(true);
            noFollowCellStyle.setAlignment(HorizontalAlignment.CENTER);
            noFollowCellStyle.setBorderRight(BorderStyle.THIN);
            noFollowCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            noFollowCellStyle.setBorderLeft(BorderStyle.THIN);
            noFollowCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle doFollowCellStyle = workBook.createCellStyle();
            doFollowCellStyle.setFont(doFollowTitleFont);
            doFollowCellStyle.setWrapText(true);
            doFollowCellStyle.setAlignment(HorizontalAlignment.CENTER);
            doFollowCellStyle.setBorderRight(BorderStyle.THIN);
            doFollowCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            doFollowCellStyle.setBorderLeft(BorderStyle.THIN);
            doFollowCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle backLinkHelpContnetCellStyle = workBook.createCellStyle();
            backLinkHelpContnetCellStyle.setFont(normalFont);
            backLinkHelpContnetCellStyle.setWrapText(true);
            backLinkHelpContnetCellStyle.setAlignment(HorizontalAlignment.CENTER);
            backLinkHelpContnetCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            backLinkHelpContnetCellStyle.setBorderBottom(BorderStyle.THIN);
            backLinkHelpContnetCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            backLinkHelpContnetCellStyle.setBorderRight(BorderStyle.THIN);
            backLinkHelpContnetCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            backLinkHelpContnetCellStyle.setBorderLeft(BorderStyle.THIN);
            backLinkHelpContnetCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            rowCount++;
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 600);
            for (int count = 0; count < columnNamesOfReport.length; count++) {
                cell = rows.createCell(count);
                cell.setCellValue(columnNamesOfReport[count]);
                if (count == 0) {
                    sheet.setColumnWidth(0, (short) (sheet.getColumnWidth(0) + 256));
                }
                cell.setCellStyle(headerColoumStyle);
            }

            rowCount++;
            sheet.createFreezePane(0, rowCount);
            if (backLinks != null && !backLinks.isEmpty()) {
                boolean isDofollow = true;
                boolean isNofollow = true;
                int sNo = 1;
//                try {
                for (int j = 1; j < backLinks.size() + 1; j++) {
                    if (backLinks.get(j - 1).isNoFollow()) {
                        if (isNofollow) {
                            isNofollow = false;
                            rows = sheet.createRow(rowCount);
                            rows.setHeight((short) 400);
                            cell = rows.createCell(0);
                            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                            mergeHSSFCell(rows, noDataStyle, 0, 4);
                            cell.setCellValue("NO FOLLOW URL(s)");
                            cell.setCellStyle(noFollowCellStyle);
                            rowCount++;
                            rows = sheet.createRow(rowCount);
                            rows.setHeight((short) 400);
                            cell = rows.createCell(0);
                            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                            mergeHSSFCell(rows, noDataStyle, 0, 4);
                            cell.setCellValue(messageSource.getMessage("noFollowHelpContent", null, Locale.US));
                            cell.setCellStyle(backLinkHelpContnetCellStyle);
                            rowCount++;
                        }
                        rows = sheet.createRow(rowCount);
                        rowCount++;
                        for (int i = 0; i < columnNamesOfReport.length; i++) {
                            switch (i) {
                                case 0:
                                    cell = rows.createCell(i);
//                                    cell.setCellValue(j);
                                    cell.setCellValue(sNo);
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 1:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getSourceURL());
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 2:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getRating());
                                    cell.setCellStyle(backLinkCenterCellStyle);
                                    break;
                                case 3:
                                    cell = rows.createCell(i);
                                    if (!backLinks.get(j - 1).getAnchorText().equals("")) {
                                        cell.setCellValue(backLinks.get(j - 1).getAnchorText());
                                    } else {
                                        cell.setCellValue("-");
                                    }
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 4:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getLastVisitedDateString());
                                    cell.setCellStyle(dateCellStyle);
                                    break;
                                default:
                                    break;
                            }

                        }
                        sNo++;
                    }
                }
                for (int j = 1; j < backLinks.size() + 1; j++) {
                    if (!backLinks.get(j - 1).isNoFollow()) {
                        if (isDofollow) {
                            isDofollow = false;
                            rows = sheet.createRow(rowCount);
                            rows.setHeight((short) 400);
                            cell = rows.createCell(0);
                            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                            mergeHSSFCell(rows, noDataStyle, 0, 4);
                            cell.setCellValue("DO FOLLOW URL(s)");
                            cell.setCellStyle(doFollowCellStyle);

                            rowCount++;

                            rows = sheet.createRow(rowCount);
                            rows.setHeight((short) 400);
                            cell = rows.createCell(0);
                            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                            mergeHSSFCell(rows, noDataStyle, 0, 4);
                            cell.setCellValue(messageSource.getMessage("doFollowHelpContent", null, Locale.US));
                            cell.setCellStyle(backLinkHelpContnetCellStyle);

                            rowCount++;
                        }
                        rows = sheet.createRow(rowCount);
                        rowCount++;
                        for (int i = 0; i < columnNamesOfReport.length; i++) {
                            switch (i) {
                                case 0:
                                    cell = rows.createCell(i);
//                                    cell.setCellValue(j);
                                    cell.setCellValue(sNo);
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 1:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getSourceURL());
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 2:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getRating());
                                    cell.setCellStyle(backLinkCenterCellStyle);
                                    break;
                                case 3:
                                    cell = rows.createCell(i);
                                    if (!backLinks.get(j - 1).getAnchorText().equals("")) {
                                        cell.setCellValue(backLinks.get(j - 1).getAnchorText());
                                    } else {
                                        cell.setCellValue("-");
                                    }
                                    cell.setCellStyle(backLinkValueCellStyle);
                                    break;
                                case 4:
                                    cell = rows.createCell(i);
                                    cell.setCellValue(backLinks.get(j - 1).getLastVisitedDateString());
                                    cell.setCellStyle(dateCellStyle);
                                    break;
                                default:
                                    break;
                            }

                        }
                        sNo++;
                    }
                }
//                } catch (Exception e) {
//                    LOGGER.error("Exception in creating Excel object for Inbound link checker Report, Error", e);
//                }
            } else {
                rows = sheet.createRow(rowCount);
                cell = rows.createCell(0);
                rows.setHeight((short) 600);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 4));
                mergeHSSFCell(rows, noDataStyle, 0, 4);
                cell.setCellValue("No Data Available.");
                cell.setCellStyle(noDataStyle);
            }
//            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(0, 3500);
            sheet.setColumnWidth(1, 16500);
            sheet.setColumnWidth(2, 10500);
            sheet.setColumnWidth(3, 8500);
            sheet.setColumnWidth(4, 4500);
            sheet.setColumnWidth(5, 6000);
            String reportName = userId != 0 ? Common.removeSpaces(reportFileName + "_" + userId) : Common.removeSpaces(reportFileName);
            String fileName = null;
            if (reportName != null) {
                fileName = Common.createFileName(reportName) + ".xls";
            }
            File file = null;
            FileOutputStream stream = null;
            if (fileName != null) {
                file = new File(downloadfolder + fileName);
                stream = new FileOutputStream(file);
                workBook.write(stream);
            }
            stream.flush();
            stream.close();
            return fileName;
        } catch (Exception e) {
            LOGGER.error("Exception in creating Inbound link checker Report, Error");
            e.printStackTrace();

        }
        return null;
    }

    public PdfPCell createPdfCell(String cellContent, com.itextpdf.text.Font textFontStyle) {
        PdfPCell newCell = new PdfPCell(new Phrase(cellContent, textFontStyle));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public PdfPCell createPdfCellHeading(String cellContent, boolean isBorderRequried, com.itextpdf.text.Font textFontStyle) {
        PdfPCell metricCell = new PdfPCell(new Phrase(cellContent, textFontStyle));
        if (isBorderRequried) {
            metricCell.setBorderColor(BaseColor.GRAY);
            metricCell.setBorderWidth(0.1f);
            metricCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            metricCell.setPadding(6f);
            metricCell.setBackgroundColor(new BaseColor(0, 0, 0));
        } else {
            metricCell.setBorder(Rectangle.NO_BORDER);
            metricCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        }
        metricCell.setVerticalAlignment(Element.ALIGN_LEFT);
        return metricCell;
    }

    public String generateBackLinksPDFReport(String finalDomName, String downloadFolder, List<AhrefBacklink> backLinks, 
            AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo, long userId,String reportFileName,String reportTitle) {

        BaseColor metricGreenFontColor = new BaseColor(100, 178, 100);
        BaseColor metricRedFontColor = new BaseColor(255, 69, 69);
        BaseColor whiteBaseColor = new BaseColor(255, 252, 252);
        BaseColor blackBaseColor = new BaseColor(0, 0, 0);

        com.itextpdf.text.Font reportNameFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, com.itextpdf.text.Font.BOLD, new BaseColor(255, 110, 0)));
        com.itextpdf.text.Font normalFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.NORMAL, blackBaseColor));
        com.itextpdf.text.Font boldFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, blackBaseColor));
        com.itextpdf.text.Font tableHeaderFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, whiteBaseColor));
        com.itextpdf.text.Font metricGreenTitleFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, metricGreenFontColor));
        com.itextpdf.text.Font metricRedTitleFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, metricRedFontColor));
        String filename = "";
        try {
            String repName = reportFileName + "_" + userId;
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            String filePath = downloadFolder + filename;
            File file = new File(filePath);
            FileOutputStream stream = null;
            stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            Phrase mainHeading = null;
            Chunk tempReportName = null, labelTitle = null, labelValue = null;
            PdfPCell reportMetricCell = null;
            PdfPCell logo = null;
            Phrase reportPharse = null;
            PdfPTable imgTable = null;
            Image lXRMLogoImage = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            if (lXRMLogoImage != null) {
                lXRMLogoImage.scaleToFit(90f, 50f);
                Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
                lXRMLogoImage.setAnnotation(anno);
                mainHeading = new Phrase();
                tempReportName = new Chunk(reportTitle, reportNameFont);
                mainHeading.add(tempReportName);
                /*Start of Review of and Review on*/
                labelTitle = new Chunk("Review of: ", normalFont);
                labelValue = new Chunk(" " + "\"" + finalDomName + "\" ", boldFont);

                float[] headingWidths = {55f, 45f};
                imgTable = new PdfPTable(headingWidths);
                imgTable.setWidthPercentage(99.9f);
                logo = new PdfPCell(new Phrase(new Chunk(lXRMLogoImage, 0, 0, true)));
            }

            /*Creating header section*/
            float[] headingTabWidths = {99.9f};
            PdfPTable headingTable = new PdfPTable(headingTabWidths);
            headingTable.setWidthPercentage(99.9f);

            reportMetricCell = new PdfPCell(mainHeading);
            reportMetricCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            reportMetricCell.setBorder(Rectangle.NO_BORDER);
            reportMetricCell.setPaddingLeft(120f);

            headingTable.addCell(reportMetricCell);
            headingTable.completeRow();

            reportPharse = new Phrase();
            reportPharse.add(labelTitle);
            reportPharse.add(labelValue);

            reportMetricCell = new PdfPCell(reportPharse);
            reportMetricCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            reportMetricCell.setBorder(Rectangle.NO_BORDER);
            reportMetricCell.setPaddingLeft(120f);

            headingTable.addCell(reportMetricCell);
            headingTable.completeRow();

            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            reportGeneratedDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reportGeneratedDateFormat.format(startDat);

            labelTitle = new Chunk("Review On: ", normalFont);
            labelValue = new Chunk(" " + "\"" + curDate + "\" ", boldFont);

            reportPharse = new Phrase();
            reportPharse.add(labelTitle);
            reportPharse.add(labelValue);

            reportMetricCell = new PdfPCell(reportPharse);
            reportMetricCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            reportMetricCell.setBorder(Rectangle.NO_BORDER);
            reportMetricCell.setPaddingLeft(120f);

            headingTable.addCell(reportMetricCell);
            headingTable.completeRow();

            PdfPCell textForheading = new PdfPCell(headingTable);
            textForheading.setBorder(Rectangle.NO_BORDER);
            if (logo != null) {
                logo.setBorder(Rectangle.NO_BORDER);
                logo.setPaddingBottom(27f);
            }
            reportMetricCell.setPaddingBottom(27f);
            reportMetricCell.setPaddingTop(6f);
            if (imgTable != null) {
                imgTable.addCell(logo);
                imgTable.addCell(textForheading);
            }

            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);
            /*End of Review of and Review on*/

 /*Addin domain info to page.*/
 /*Creating and intilization domain info table.*/
            float[] domainInfoTabWidths = {15f, 15f, 20f, 15f, 15f, 15f};
            PdfPTable domainInfoTab = new PdfPTable(domainInfoTabWidths);
            domainInfoTab.setWidthPercentage(99.9f);

            /*Creating each metric cell*/
            PdfPCell meticValueCell = null;

            /*Number of Backlinks value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getAhrefsbackLinks()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            /*Referring Domains Value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getRefdomains()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            /*Total No Follow Links Value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getNoFollowLinks()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            /*Governmental Links Value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getGovLinks()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            /*Educational Links Value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getEduLinks()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            /*Indexed Pages in Google Value*/
            meticValueCell = createPdfCellHeading(String.valueOf(ahrefBackLinkDomainInfo.getGoogleIndexedPages()), false, boldFont);
            domainInfoTab.addCell(meticValueCell);

            domainInfoTab.completeRow();

            for (String domainNameInfoMetric : domainNameInfoMetrics) {
                meticValueCell = createPdfCellHeading(domainNameInfoMetric, false, normalFont);
                domainInfoTab.addCell(meticValueCell);
            }

            /*Adding domain info table to Doc object.*/
            pdfDoc.add(domainInfoTab);

            Paragraph commonParaObject = null;
            Paragraph innerParaObject = null;
            PdfPCell commonCellObject = null;
            Chunk contentChunk = null;

            commonParaObject = new Paragraph();
            commonParaObject.setSpacingAfter(1f);
            commonParaObject.setSpacingBefore(1f);
            pdfDoc.add(commonParaObject);

            /*Creating DoFollow title*/
            float[] resultTabWidths = {5f, 35f, 15f, 28f, 16f};
            PdfPTable googleResultTab = new PdfPTable(resultTabWidths);
            googleResultTab.setWidthPercentage(99.9f);
            pdfDoc.add(Chunk.NEWLINE);

            //Report Headings
            for (int count = 0; count < columnNamesOfReport.length; count++) {
                meticValueCell = createPdfCellHeading(columnNamesOfReport[count], true, tableHeaderFont);
                googleResultTab.addCell(meticValueCell);
            }
            googleResultTab.completeRow();

            try {
                //Report Heading values
                if (backLinks != null && !backLinks.isEmpty()) {

                    boolean isDofollow = true;
                    boolean isNofollow = true;
                    int sNo = 1;
                    for (int j = 1; j < backLinks.size() + 1; j++) {
                        if (backLinks.get(j - 1).isNoFollow()) {
                            if (isNofollow) {
                                isNofollow = false;
                                commonParaObject = new Paragraph();
                                innerParaObject = new Paragraph();
                                innerParaObject.add(Chunk.NEWLINE);
                                contentChunk = new Chunk("NO FOLLOW URL(s)", metricRedTitleFont);
                                innerParaObject.add(contentChunk);
                                commonParaObject.add(innerParaObject);
                                innerParaObject = new Paragraph();
                                innerParaObject.add(Chunk.NEWLINE);
                                innerParaObject.add(Chunk.NEWLINE);
                                contentChunk = new Chunk(messageSource.getMessage("noFollowHelpContent", null, Locale.US), normalFont);
                                innerParaObject.add(contentChunk);
                                innerParaObject.add(Chunk.NEWLINE);
                                innerParaObject.add(Chunk.NEWLINE);
                                commonParaObject.add(innerParaObject);
                                commonCellObject = new PdfPCell(commonParaObject);
                                commonCellObject.setColspan(5);
                                googleResultTab.addCell(commonCellObject);
                                googleResultTab.completeRow();
                            }
                            for (int i = 0; i <= 4; i++) {
                                switch (i) {
                                    case 0:
//                                        PdfPCell sno = createPdfCell(j + "", normalFont);
                                        PdfPCell sno = createPdfCell(sNo + "", normalFont);
                                        googleResultTab.addCell(sno);
                                        break;
                                    case 1:
                                        PdfPCell referringUrl = createPdfCell(backLinks.get(j - 1).getSourceURL() + "" + "", normalFont);
                                        referringUrl.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                                        googleResultTab.addCell(referringUrl);
                                        break;
                                    case 2:
                                        PdfPCell referringUrlRating = createPdfCell(backLinks.get(j - 1).getRating() + "", normalFont);
                                        referringUrlRating.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                                        googleResultTab.addCell(referringUrlRating);
                                        break;
                                    case 3:
                                        String anchorText = "";
                                        if (!backLinks.get(j - 1).getAnchorText().equals("")) {
                                            anchorText = backLinks.get(j - 1).getAnchorText();
                                        } else {
                                            anchorText = "-";
                                        }
                                        PdfPCell anchor = createPdfCell(anchorText, normalFont);
                                        googleResultTab.addCell(anchor);
                                        break;
                                    case 4:
                                        PdfPCell lastChecked = createPdfCell(backLinks.get(j - 1).getLastVisitedDateString() + "" + "", normalFont);
                                        googleResultTab.addCell(lastChecked);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            sNo++;
                        }
                        googleResultTab.completeRow();
                    }
                    for (int j = 1; j < backLinks.size() + 1; j++) {
                        if (!backLinks.get(j - 1).isNoFollow()) {
                            if (isDofollow) {
                                isDofollow = false;
                                commonParaObject = new Paragraph();
                                innerParaObject = new Paragraph();
                                innerParaObject.add(Chunk.NEWLINE);
//                                if (sNo != 1) {
//                                    innerParaObject.add(Chunk.NEWLINE);
//                                }
                                contentChunk = new Chunk("DO FOLLOW URL(s)", metricGreenTitleFont);
                                innerParaObject.add(contentChunk);
                                commonParaObject.add(innerParaObject);
                                innerParaObject = new Paragraph();
                                innerParaObject.add(Chunk.NEWLINE);
                                innerParaObject.add(Chunk.NEWLINE);
                                contentChunk = new Chunk(messageSource.getMessage("doFollowHelpContent", null, Locale.US), normalFont);
                                innerParaObject.add(contentChunk);
                                innerParaObject.add(Chunk.NEWLINE);
                                innerParaObject.add(Chunk.NEWLINE);
                                commonParaObject.add(innerParaObject);
                                commonCellObject = new PdfPCell(commonParaObject);
                                commonCellObject.setColspan(5);
                                googleResultTab.addCell(commonCellObject);
                                googleResultTab.completeRow();
                            }
                            for (int i = 0; i <= 4; i++) {
                                switch (i) {
                                    case 0:
                                        PdfPCell sno = createPdfCell(sNo + "", normalFont);
//                                        PdfPCell sno = createPdfCell(j + "", normalFont);
                                        googleResultTab.addCell(sno);
                                        break;
                                    case 1:
                                        PdfPCell referringUrl = createPdfCell(backLinks.get(j - 1).getSourceURL() + "" + "", normalFont);
                                        referringUrl.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
                                        googleResultTab.addCell(referringUrl);
                                        break;
                                    case 2:
                                        PdfPCell referringUrlRating = createPdfCell(backLinks.get(j - 1).getRating() + "", normalFont);
                                        referringUrlRating.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                                        googleResultTab.addCell(referringUrlRating);
                                        break;
                                    case 3:
                                        String anchorText = "";
                                        if (!backLinks.get(j - 1).getAnchorText().equals("")) {
                                            anchorText = backLinks.get(j - 1).getAnchorText();
                                        } else {
                                            anchorText = "-";
                                        }
                                        PdfPCell anchor = createPdfCell(anchorText, normalFont);
                                        googleResultTab.addCell(anchor);
                                        break;
                                    case 4:
                                        PdfPCell lastChecked = createPdfCell(backLinks.get(j - 1).getLastVisitedDateString() + "" + "", normalFont);
                                        googleResultTab.addCell(lastChecked);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            sNo++;
                        }
                        googleResultTab.completeRow();
                    }
                } else {
                    try {
                        PdfPCell noResult = new PdfPCell(new Phrase("No results found", normalFont));
                        noResult.setBorderColor(BaseColor.GRAY);
                        noResult.setBorderWidth(0.1f);
                        noResult.setHorizontalAlignment(Element.ALIGN_CENTER);
                        noResult.setColspan(5);
                        noResult.setPadding(6f);
                        googleResultTab.addCell(noResult);
                        googleResultTab.completeRow();
                    } catch (Exception e) {
                        LOGGER.error(e);
                    }
                }
                pdfDoc.add(googleResultTab);
                pdfDoc.close();
            } catch (DocumentException e) {
                LOGGER.error(e);
            }
        } catch (DocumentException e) {
            LOGGER.error("DocumentException when generateBackLinksPDFReport: ", e);
        } catch (FileNotFoundException e) {
            LOGGER.error("FileNotFoundException when generateBackLinksPDFReport: ", e);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(InboundLinkCheckerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return filename;
    }

    /* Merges multiple columns of specified range for row.*/
    public void mergeHSSFCell(HSSFRow rows, HSSFCellStyle style, int startCellNo, int endCellNo) {
        HSSFCell cell;
        for (int i = startCellNo; i <= endCellNo; i++) {
            cell = rows.createCell(i);
            cell.setCellStyle(style);
        }
    }

    public Paragraph genertaeParagraph(String text, com.itextpdf.text.Font textStyle) {
        Paragraph paragraphObject = new Paragraph();
        paragraphObject.add(new Chunk(text, textStyle));
        paragraphObject.setSpacingAfter(5f);
        return paragraphObject;
    }

    private static LinkedHashMap sortByValues(LinkedHashMap pricePlan) {
        List list = new LinkedList(pricePlan.entrySet());
        /*Custom Comparator*/
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        LinkedHashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

}
