package lxr.marketplace.urlvalidator.domain;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class UrlValidator {

    private StringBuffer manaullyURL;
    private String keyword;
    private String selFile;
    private String destUrlCol;
    boolean headerRow;
    private ArrayList<UrlStatus> urlStatusList;
    private List columnList;
    private String fileField;
    private int selectedColumIndex;

    private int workingURLCount;
    private int notWorkingURLCount;
    private int totalURLCount;

    private int permanentRedirectingCount;
    private int temporaryRedirectingCount;
    private int noURLStatusCount;
    
    MultipartFile uploadedFile;

    public int getSelectedColumIndex() {
        return selectedColumIndex;
    }

    public void setSelectedColumIndex(int selectedColumIndex) {
        this.selectedColumIndex = selectedColumIndex;
    }

    public boolean isHeaderRow() {
        return headerRow;
    }

    public void setHeaderRow(boolean headerRow) {
        this.headerRow = headerRow;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword.trim();
    }

    public String getKeyword() {
        return (keyword == null) ? "" : keyword;
    }

    public void setUrlStatusList(ArrayList<UrlStatus> urlStatusList) {
        this.urlStatusList = urlStatusList;
    }

    public ArrayList<UrlStatus> getUrlStatusList() {
        return urlStatusList;
    }

    public void setSelFile(String selFile) {
        this.selFile = selFile;
    }

    public String getSelFile() {
        return (selFile == null) ? "" : selFile;
    }

    public void setDestUrlCol(String destUrlCol) {
        this.destUrlCol = destUrlCol;
    }

    public String getDestUrlCol() {
        return destUrlCol;
    }

    public void setColumnList(ArrayList<String> columnList) {
        this.columnList = columnList;
    }

    public List getColumnList() {
        return columnList;
    }

    public void setFileField(String fileField) {
        this.fileField = fileField;
    }

    public String getFileField() {
        return (fileField == null) ? "" : fileField;
    }

    public int getWorkingURLCount() {
        return workingURLCount;
    }

    public void setWorkingURLCount(int workingURLCount) {
        this.workingURLCount = workingURLCount;
    }

    public int getNotWorkingURLCount() {
        return notWorkingURLCount;
    }

    public void setNotWorkingURLCount(int notWorkingURLCount) {
        this.notWorkingURLCount = notWorkingURLCount;
    }

    public int getTotalURLCount() {
        return totalURLCount;
    }

    public void setTotalURLCount(int totalURLCount) {
        this.totalURLCount = totalURLCount;
    }

    public StringBuffer getManaullyURL() {
        return manaullyURL;
    }

    public void setManaullyURL(StringBuffer manaullyURL) {
        this.manaullyURL = manaullyURL;
    }

    public int getPermanentRedirectingCount() {
        return permanentRedirectingCount;
    }

    public void setPermanentRedirectingCount(int permanentRedirectingCount) {
        this.permanentRedirectingCount = permanentRedirectingCount;
    }

    public int getTemporaryRedirectingCount() {
        return temporaryRedirectingCount;
    }

    public void setTemporaryRedirectingCount(int temporaryRedirectingCount) {
        this.temporaryRedirectingCount = temporaryRedirectingCount;
    }

    public MultipartFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(MultipartFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public int getNoURLStatusCount() {
        return noURLStatusCount;
    }

    public void setNoURLStatusCount(int noURLStatusCount) {
        this.noURLStatusCount = noURLStatusCount;
    }

    
}
