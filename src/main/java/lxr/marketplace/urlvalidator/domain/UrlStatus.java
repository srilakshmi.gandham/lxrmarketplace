package lxr.marketplace.urlvalidator.domain;

import java.io.Serializable;
import java.util.LinkedHashMap;

public class UrlStatus implements Serializable{

    private String inputUrl;
    private String status;
    private int statusCode;
    private int keywordCount;
    private String remark;

    private int finalStatusCode;
    private String destinationURL;
    private LinkedHashMap<String,Integer> chainedRedirectsMap;
    private float responseTime;
    

//	private String destUrlCol;
    public void setInputUrl(String inputUrl) {
        this.inputUrl = inputUrl;
    }

    public String getInputUrl() {
        return inputUrl;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return (status == null) ? "" : status;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setKeywordCount(int keywordCount) {
        this.keywordCount = keywordCount;
    }

    public int getKeywordCount() {
        return keywordCount;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public int getFinalStatusCode() {
        return finalStatusCode;
    }

    public void setFinalStatusCode(int finalStatusCode) {
        this.finalStatusCode = finalStatusCode;
    }

    public String getDestinationURL() {
        return destinationURL;
    }

    public void setDestinationURL(String destinationURL) {
        this.destinationURL = destinationURL;
    }

    public LinkedHashMap<String, Integer> getChainedRedirectsMap() {
        return chainedRedirectsMap;
    }

    public void setChainedRedirectsMap(LinkedHashMap<String, Integer> chainedRedirectsMap) {
        this.chainedRedirectsMap = chainedRedirectsMap;
    }

    
    public float getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(float responseTime) {
        this.responseTime = responseTime;
    }

    @Override
    public String toString() {
        return "UrlStatus{" + "inputUrl=" + inputUrl + ", status=" + status + ", statusCode=" + statusCode + ", keywordCount=" + keywordCount + ", remark=" + remark + ", finalStatusCode=" + finalStatusCode + ", destinationURL=" + destinationURL + ", chainedRedirectsMap=" + chainedRedirectsMap + ", responseTime=" + responseTime + '}';
    }

   
}