package lxr.marketplace.urlvalidator.service;

import lxr.marketplace.urlvalidator.domain.UrlStatus;
import lxr.marketplace.urlvalidator.domain.UrlValidator;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import com.Ostermiller.util.CSVParser;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lxr.marketplace.util.CommonReportGenerator;
import lxr.marketplace.util.CommonThreadPoolExecuter;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;

@Service
public class UrlValidatorService {

    private static Logger logger = Logger.getLogger(UrlValidatorService.class);
    final String headerFileName = "X-File-Name";
    final int READ_ONLY_FILE = 0;
    HttpURLConnection connection = null, connection1 = null;
    String[] colHeaders = {"URL", "", "", "HTTP Status", "# of Redirects", "Final Destination", "Final HTTP Status", "Keyword Count"};
    String[] csvColHeaders = {"URL", "HTTP Status", "# of Redirects", "Final Destination", "Final HTTP Status", "Keyword Count"};
    public static final String WORKING = "Working";
    public static final String WORKING_TEMPORARY_REDIRECTION = "Working - Temporary Redirection";
    public static final String WORKING_PERMANENT_REDIRECTION = "Working - Permanent Redirection";
    public static final String FAILED_TO_CHECK = "Unable to check Status Code";
    public static final int FAILED_STATUS_CODE = 0;
    public static final String CONNECTION_TIME_OUT = "ConnectionTimeOut";
    public static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;https://lxrmarketplace.com/,support@lxrmarketplace.com)";

    private final Format fileSaveFormat = new SimpleDateFormat("yyyy_MMM_dd_HHmmss_SSS");
    private final SimpleDateFormat reviewOnFormat = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
    private PrintStream printStream;
    private FileOutputStream stream;

//    private int workingUrlCount;
//    private int nonWorkingUrlCount;

    /* called by controller to get UrlStatusList when URLs are entered manually */
    public ArrayList<UrlStatus> getUrlStatus(UrlValidator urlValidator, int toolAcessLimit) {
        ArrayList<UrlStatus> filtereURLStats = null;
        ArrayList<UrlStatus> urlQuriesList = null;
        urlQuriesList = getFinalUrlList(urlValidator.getManaullyURL(), toolAcessLimit);
        String keyword = urlValidator.getKeyword();
        try {
            CommonThreadPoolExecuter threadPoolExecuter = new CommonThreadPoolExecuter(30, 100, 600);
            if (urlQuriesList != null) {
                int length = urlQuriesList.size();
                if (length > toolAcessLimit) {
                    length = toolAcessLimit;
                }
                for (int i = 0; i < length; i++) {
                    UrlStatus url = urlQuriesList.get(i);
                    UrlValidatorThread urlValidatorThread = new UrlValidatorThread(url, keyword);
                    threadPoolExecuter.runTask(urlValidatorThread);
                }
                threadPoolExecuter.shutDown();
                do {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage());
                    }
                } while (threadPoolExecuter.isTerminated() != true);
            }
            if (urlQuriesList != null) {
                filtereURLStats = (ArrayList<UrlStatus>) urlQuriesList.stream().filter(urlStatus -> (!urlStatus.getInputUrl().trim().equals(""))).collect(Collectors.toList());
            }
        } catch (Exception e) {
            logger.error("Exception in getUrlList when collecting or intializing the urlList cause:: ", e);
        }
        return filtereURLStats;
    }

    /*
	 * called when URLs are entered manually. URLs entered by user are separated
	 * by ',' ';' and line break and UrlSatus object is created for each URL and
	 * finally ArrayList of all these objects is returned*
     */
    public ArrayList<UrlStatus> getFinalUrlList(StringBuffer urls, int toolAcessLimit) {
        ArrayList<String> urlList = convertStringToArrayList(urls, "\n");
        ArrayList<UrlStatus> finalUrlsList = new ArrayList<>();
        //for (String urlString : urlList) {
        int length = urlList.size();
        if (length > toolAcessLimit) {
            length = toolAcessLimit;
        }
        for (int i = 0; i < length; i++) {
            String urlString = urlList.get(i);
            /*Commented to validat the given input
            String finalUrl = getFinalUrl(urlString,session);*/
            UrlStatus urlObj = new UrlStatus();
            if (urlString != null) {
                urlObj.setInputUrl(urlString.trim());
            }
            finalUrlsList.add(urlObj);
        }
        return finalUrlsList;
    }

    /*
	 * Called when URLs are entered manually as well as when file is uploaded It
	 * calculates working, non-working and total URL's count
     */
    public void getCounts(ArrayList<UrlStatus> ulrList, UrlValidator urlDashBoard) {
        urlDashBoard.setTotalURLCount(ulrList.size());
        urlDashBoard.setWorkingURLCount((int) ulrList.stream().filter(l -> l.getStatusCode() == 200).count());
        urlDashBoard.setPermanentRedirectingCount((int) ulrList.stream().filter(l -> l.getStatusCode() == 301).count());
        urlDashBoard.setTemporaryRedirectingCount((int) ulrList.stream().filter(l -> l.getStatusCode() == 302).count());
        urlDashBoard.setNotWorkingURLCount((int) ulrList.stream().filter(l -> l.getStatusCode() >= 400 && l.getStatusCode() <= 500).count());
        urlDashBoard.setNoURLStatusCount((int) ulrList.stream().filter(l -> l.getStatusCode() == FAILED_STATUS_CODE).count());
    }

    /*
	 * Called by controller to save the uploaded file on disk. this reads first
	 * row of input file and save the column names in JSon object to display the
	 * popup to select column for destination URL.
     */
    public String saveFile(String realPath, HttpServletRequest request,
            HttpServletResponse response) {
        String fileName = "", newFilename = "";
        InputStream is = null;
        FileOutputStream fos = null;
        if (request.getHeader(headerFileName) != null) {
            fileName = request.getHeader(headerFileName);
            if (fileName.trim().contains(" ")) {
                fileName = fileName.replace(" ", "%");
            }
            int ind = fileName.lastIndexOf(".");
            newFilename = fileName.substring(0, ind);
            String ext = fileName.substring((ind + 1), fileName.length());
            newFilename += "_" + Calendar.getInstance().getTime().getTime() + "." + ext;
            try {
                is = request.getInputStream();
                File f = new File(realPath + newFilename);
                fos = new FileOutputStream(f);
                IOUtils.copy(is, fos);
                response.setContentType("application/json");
                response.setStatus(HttpServletResponse.SC_OK);
            } catch (Exception e) {
                logger.error(e.getMessage());
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } finally {
                try {
                    fos.close();
                    is.close();
                } catch (IOException ignored) {
                }
            }
        } else if (request.getParameter("fileName") != null) {
            fileName = request.getParameter("fileName");

            if (fileName.trim().contains(" ")) {
                fileName = fileName.replace(" ", "%");
            }
            int ind = fileName.lastIndexOf(".");
            newFilename = fileName.substring(0, ind);
            String ext = fileName.substring((ind + 1), fileName.length());
            newFilename += "_" + Calendar.getInstance().getTime().getTime() + "." + ext;

            response.setContentType("text/html");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            final long permitedSize = 3145728;
            long fileSize;
            try {
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    FileItemFactory factory = new DiskFileItemFactory();
                    ServletFileUpload upload = new ServletFileUpload(factory);
                    List items = upload.parseRequest(request);
                    for (int i = 0; i < items.size(); i++) {
                        FileItem fileItem = (FileItem) items.get(i);
                        if (fileItem.isFormField()) {
                            String any_parameter = fileItem.getString();
                        } else {
                            fileName = fileItem.getName();
                            fileSize = fileItem.getSize();
                            if (fileSize <= permitedSize) {
                                File uploadedFile = new File(realPath + newFilename);
                                fileItem.write(uploadedFile);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Exception for saveFile", e);
            }
        }
        return newFilename;
    }

    public void readFile(ArrayList<String> columns,
            HttpServletRequest request, HttpServletResponse response, int toolAccessLimit) {
        JSONArray arr = null;
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr = new JSONArray();
            for (int i = 0; i < columns.size(); i++) {
                String colName = columns.get(i);
                logger.debug("colName is " + colName);
//				Commented Based on Dec 10 2015 for Bug No:
//                                if(colName.length()>50){
//					colName = colName.substring(0, 50);
//				}
                arr.add(colName);
            }
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();

        }
    }

    /*
	 * called to get the list of columns when file is uploaded and called to get
	 * list of URLs in specified column when URLs from uploaded file needs to be
	 * validated
     */
    public ArrayList<String> getListByOption(String filePath, String options,
            int selColInd, boolean headerRow, int toolAccessLimit) {
        ArrayList<String> list;
        FileInputStream fis = null;
        try {
            File f = new File(filePath);
            fis = new FileInputStream(f);
            list = new ArrayList<>();
            if (filePath.endsWith(".xls") && !(filePath.endsWith(".xlsx"))
                    || (filePath.endsWith(".XLS") && !(filePath.endsWith(".XLSX")))) {
                list = getListFromXls(fis, options, selColInd, headerRow, toolAccessLimit);
            } else if (filePath.endsWith(".xlsx") || filePath.endsWith(".XLSX")) {
                list = getListFromXlsx(filePath, options, selColInd, headerRow, toolAccessLimit);
            } else if (filePath.endsWith(".csv") || filePath.endsWith(".CSV")) {
                list = getListFromCsv(fis, options, selColInd, headerRow, toolAccessLimit);
            } else if (filePath.endsWith(".tsv") || filePath.endsWith(".TSV")) {
                list = getListFromTsv(fis, options, selColInd, headerRow, toolAccessLimit);
            }
            return list;
        } catch (IOException e) {
            logger.error("Exception in getListByOption", e);
//			e.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
//				e.printStackTrace();
            }
        }
        return null;
    }

    /* called by controller to get UrlStatusList from uploaded file */
    public ArrayList<UrlStatus> getUrlList(String selFile, String selCol,
            String keyword, boolean headerRow, int toolAccessLimit) {
        ArrayList<UrlStatus> filtereURLStats = null;
        CommonThreadPoolExecuter threadPoolExecuter = new CommonThreadPoolExecuter(30, 100, 600);
        ArrayList<String> urlFilesList = null;
        ArrayList<UrlStatus> urlQuriesList = new ArrayList<>();
        urlFilesList = getListByOption(selFile, "urlList", Integer.parseInt(selCol), headerRow, toolAccessLimit);
        int urlCount = 0;
        if (urlFilesList != null) {
            for (String inputUrl : urlFilesList) {
                /*String finalUrl = getFinalUrl(inputUrl);
            logger.debug("finalUrl si "+finalUrl);*/
                UrlStatus urlStatus = new UrlStatus();
                urlStatus.setInputUrl(inputUrl.trim());
                urlQuriesList.add(urlStatus);
                urlCount++;
                if (urlCount > toolAccessLimit) {
                    break;
                }
            }
        }
        try {
            urlQuriesList.stream().map((url) -> new UrlValidatorThread(url, keyword)).forEach((urlValidatorThread) -> {
                /*Thread t = new Thread(urlValidatorThread);
            thrList.add(t);*/
                threadPoolExecuter.runTask(urlValidatorThread);
            });
            threadPoolExecuter.shutDown();
            do {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                }
            } while (threadPoolExecuter.isTerminated() != true);
        } catch (Exception e) {
            logger.error("Exception in getUrlList when collecting or intializing the urlList cause:: ", e);
        }
        filtereURLStats = (ArrayList<UrlStatus>) urlQuriesList.stream().filter(urlStatus -> (!urlStatus.getInputUrl().trim().equals(""))).collect(Collectors.toList());
        return filtereURLStats;
    }

    /*
	 * takes url entred by user either manually or through file and checks if it
	 * is in proper format. (starts with http:// or http://www.). If not
	 * converts it in proper format
     */
    public String getFinalUrl(String urlString) {
        /*String finalUrl = Common.getUrlValidation(urlString ,session);
		if(finalUrl.equals("invalid") || finalUrl.equals("redirected")){
			finalUrl = urlString;
		}*/
        String finalUrl = null;
        try {
            boolean appendHttp = false, appendWww = false;
            /*if (urlString.endsWith("?") || urlString.endsWith("\"")|| urlString.endsWith("/")) {
                        int slashIndex=0,backwrdSlashIndx=0;
                        int questionIndex=0;
                        slashIndex=urlString.lastIndexOf("\"");
                        backwrdSlashIndx=urlString.lastIndexOf('/');
                        questionIndex=urlString.lastIndexOf("?");
                        if(slashIndex!=-1 && slashIndex!=urlString.length()){
                            urlString=urlString.substring(0,urlString.length()-1);
                        }
                        if(backwrdSlashIndx!=-1 && backwrdSlashIndx!=urlString.length()){
                            urlString=urlString.substring(0,urlString.length()-1);
                        }
                        if(questionIndex!=-1 && questionIndex!=urlString.length()){
                            urlString=urlString.substring(0,urlString.length()-1);
                        }
                    }*/
            if (!(urlString.startsWith("http://") || urlString
                    .startsWith("https://"))) {
                appendHttp = true;
            }
            if (appendHttp
                    && !(urlString.startsWith("http://www.") || urlString
                    .startsWith("https://www."))) {
                if (!(urlString.startsWith("www.") || urlString.startsWith("www."))) {
                    appendHttp = true;
                    appendWww = true;
                } else {
                    appendWww = false;
                }
            }
            if (appendWww) {
                urlString = "www." + urlString;
            }
            if (appendHttp) {
                urlString = "http://" + urlString;
            }
            return finalUrl = urlString;
        } catch (Exception e) {
            logger.error("Exception for getFinalUrl", e);
        }
        return finalUrl;
    }

    /*
	 * called by controller to create a file in excel(.xls) format for
	 * downloading when URLs are entered manually
     */
    public String createExcelFile(ArrayList<UrlStatus> urlStatusList,
            String filePath, String searchTerm, UrlValidator urlValidatorOld) {
        try {
            HSSFCell cell;
            HSSFRow rows;
            HSSFFont font;
            HSSFFont dashBoardFont;
            HSSFWorkbook workBook = new HSSFWorkbook();

            HSSFFont headfont = workBook.createFont();
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            font = workBook.createFont();
            font.setBold(true);

            dashBoardFont = workBook.createFont();
            dashBoardFont.setBold(false);

            /*Title Row Style.*/
            HSSFCellStyle mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            /*DashBoard Row Style.*/
            HSSFCellStyle subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.LEFT);

            HSSFCellStyle notApplicableCelltyle = workBook.createCellStyle();
            notApplicableCelltyle.setFont(font);
            notApplicableCelltyle.setWrapText(true);
            notApplicableCelltyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            notApplicableCelltyle.setAlignment(HorizontalAlignment.CENTER);
            notApplicableCelltyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFCellStyle dashboardStyle = workBook.createCellStyle();
            dashboardStyle.setFont(dashBoardFont);
            dashboardStyle.setWrapText(true);
            dashboardStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            dashboardStyle.setAlignment(HorizontalAlignment.LEFT);

            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            font = workBook.createFont();
            font.setBold(false);

            HSSFCellStyle heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);
            heaCelSty.setAlignment(HorizontalAlignment.LEFT);
            heaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFCellStyle valueCelStyle = workBook.createCellStyle();
            valueCelStyle.setFont(font);
            valueCelStyle.setWrapText(true);
            valueCelStyle.setAlignment(HorizontalAlignment.RIGHT);
            heaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);

            Runtime r = Runtime.getRuntime();
            r.freeMemory();

            HSSFSheet sheet = workBook.createSheet("URL Auditor Report");
            CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);

            int rowCount = 0;
            /*Creating Title for sheet*/
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            cell.setCellValue("URL Auditor Report");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 1000);

            /*Creating TimeStamp of sheet*/
            cell = rows.createCell(5);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            reviewOnFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reviewOnFormat.format(startDat);
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, 5, 7));
            cell.setCellValue("Reviewed on: " + curDate);
            cell.setCellStyle(notApplicableCelltyle);

            rowCount = rowCount + 2;
            if (urlValidatorOld.getNotWorkingURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("NONWORKING URL(s): " + urlValidatorOld.getNotWorkingURLCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }
            if (urlValidatorOld.getTemporaryRedirectingCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("TEMPORARY REDIRECTION URL(s): " + urlValidatorOld.getTemporaryRedirectingCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getPermanentRedirectingCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("PERMANENT REDIRECTION URL(s): " + urlValidatorOld.getPermanentRedirectingCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getWorkingURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("WORKING URL(s): " + urlValidatorOld.getWorkingURLCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getNoURLStatusCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);

                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("NO STATUS URL(s): " + urlValidatorOld.getNoURLStatusCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getTotalURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("TOTAL URL(s): " + urlValidatorOld.getTotalURLCount());
                cell.setCellStyle(dashboardStyle);
                rowCount = rowCount + 1;
            }

            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);
            rowCount = rowCount + 1;
            int numberOfRedirections = getCountofRedirections(urlStatusList);
            /*Table headers*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*URL coloum header*/
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 2));
            cell.setCellValue(colHeaders[0]);
            cell.setCellStyle(columnHeaderStyle);

            int colCount = 3;
            for (; colCount < colHeaders.length; colCount++) {
                cell = rows.createCell(colCount);
                cell.setCellStyle(columnHeaderStyle);
                if (colCount == 7 && !(searchTerm.trim().equals(""))) {
                    cell.setCellValue(colHeaders[colCount] + " (" + searchTerm + ")");
                } else {
                    cell.setCellValue(colHeaders[colCount]);
                }
            }
            if (numberOfRedirections != 0) {
                for (int redirectionCount = 1; redirectionCount <= numberOfRedirections; redirectionCount++) {
                    cell = rows.createCell(colCount);
                    cell.setCellStyle(columnHeaderStyle);
                    if (colCount % 2 == 0) {
                        cell.setCellValue("Redirect Location(" + redirectionCount + ")");
                    }
                    colCount++;
                    cell = rows.createCell(colCount);
                    cell.setCellStyle(columnHeaderStyle);
                    if (colCount % 2 != 0) {
                        cell.setCellValue("Redirect Status(" + redirectionCount + ")");
                    }
                    colCount++;
                }
            }
//            rowCount=rowCount+1;
            UrlStatus urlStatus = null;
            colCount = 0;
            HSSFRow row;
            Map<String, Integer> chainedRedirections;
            int freezeRowCount = rowCount + 1;
            int tempRowCount = 0;
            for (int i = 0; i < urlStatusList.size(); i++) {
                try {
                    urlStatus = urlStatusList.get(i);
                    tempRowCount = rowCount + 1;
                    row = sheet.createRow(tempRowCount);
                    row.setHeight((short) 500);
                    chainedRedirections = urlStatus.getChainedRedirectsMap();
                    for (; colCount < colHeaders.length; colCount++) {
                        if (colCount == 0) {
                            cell = row.createCell(colCount);
                            sheet.addMergedRegion(new CellRangeAddress(tempRowCount, tempRowCount, 0, 2));
                        } else {
                            cell = row.createCell(colCount + 2);
                        }
                        switch (colCount) {
                            case 0:
                                cell.setCellValue(urlStatus.getInputUrl());

                                cell.setCellStyle(heaCelSty);
                                break;
                            case 1:
                                cell = row.createCell(colCount + 2);
                                cell.setCellValue(urlStatus.getStatusCode());
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 2:
                                cell.setCellValue(urlStatus.getChainedRedirectsMap() != null ? urlStatus.getChainedRedirectsMap().size() : 0);
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 3:
                                if ((urlStatus.getStatusCode() < 400) && !(urlStatus.getDestinationURL() != null && urlStatus.getDestinationURL().contains("Connection"))) {
                                    cell.setCellValue(urlStatus.getDestinationURL());
                                    cell.setCellStyle(heaCelSty);
                                } else if ((urlStatus.getStatusCode() >= 400 && urlStatus.getStatusCode() <= 600) || urlStatus.getStatusCode() == 0) {
                                    cell.setCellValue("-");
                                    cell.setCellStyle(notApplicableCelltyle);
                                } else if (urlStatus.getDestinationURL().contains("Connection")) {
                                    cell.setCellValue(urlStatus.getDestinationURL());
                                }
                                break;
                            case 4:
                                cell.setCellValue(urlStatus.getFinalStatusCode());
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 5:
                                cell.setCellValue(urlStatus.getKeywordCount());
                                cell.setCellStyle(valueCelStyle);
                                break;
                        }

                    }
                    sheet.createFreezePane(0, freezeRowCount);
//                    colCount = colHeaders.length+1;

                    for (Map.Entry<String, Integer> chainedKey : chainedRedirections.entrySet()) {
                        cell = row.createCell(colCount);
                        if (colCount % 2 == 0) {
                            cell.setCellValue(chainedKey.getKey());
                            cell.setCellStyle(heaCelSty);
                        }
                        colCount++;
                        cell = row.createCell(colCount);
                        if (colCount % 2 != 0) {
                            cell.setCellValue(chainedKey.getValue());
                            cell.setCellStyle(valueCelStyle);
                        }
                        colCount++;
                    }
                } catch (Exception e) {
                    logger.error("Exception in adding the row at current row index:: " + rowCount + ", urlStatus:: " + (urlStatus != null ? urlStatus.toString() : null) + ", and cause:: " + e.getLocalizedMessage());
                }
                rowCount++;
                colCount = 0;
            }

            int colWidth[] = new int[]{10, 10, 10, 13, 13, 45, 17, 17, 45, 20, 45, 20, 45, 20, 45, 20};
            for (int i = 1; i <= colHeaders.length + numberOfRedirections + 2; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            String s = fileSaveFormat.format(new Date());
            String fileName = filePath + "LXRMarketplace_URLAuditor_" + s + ".xls";
            File file = new File(fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
            return fileName;
        } catch (Exception e) {
            logger.error("Exception for createExcelFile", e);
        }
        return null;
    }

    /*
	 * called by controller to create a file in excel(.xlsx) format for
	 * downloading when URLs are entered manually
     */
    public String generateExcelesFile(ArrayList<UrlStatus> urlStatusList,
            String filePath, String searchTerm, UrlValidator urlValidatorOld) {
        try {
            XSSFCell cell;
            XSSFRow rows;
            XSSFFont font;
            XSSFFont dashboardFont;
            XSSFWorkbook workBook = new XSSFWorkbook();

            XSSFFont headfont = workBook.createFont();
            headfont.setFontHeightInPoints((short) 13);
            headfont.setColor(HSSFColor.ORANGE.index);

            font = workBook.createFont();
            font.setBold(true);

            dashboardFont = workBook.createFont();
            dashboardFont.setBold(false);

            /*Title Row Style.*/
            XSSFCellStyle mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            /*DashBoard Row Style.*/
            XSSFCellStyle subColHeaderStyle = workBook.createCellStyle();
            subColHeaderStyle.setFont(font);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.LEFT);

            XSSFCellStyle notApplicableCelltyle = workBook.createCellStyle();
            notApplicableCelltyle.setFont(font);
            notApplicableCelltyle.setWrapText(true);
            notApplicableCelltyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            notApplicableCelltyle.setAlignment(HorizontalAlignment.CENTER);
            notApplicableCelltyle.setVerticalAlignment(VerticalAlignment.CENTER);

            /*DashBoard Row Style.*/
            XSSFCellStyle dashBoardStyle = workBook.createCellStyle();
            dashBoardStyle.setFont(dashboardFont);
            dashBoardStyle.setWrapText(true);
            dashBoardStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            dashBoardStyle.setAlignment(HorizontalAlignment.LEFT);

            XSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            font = workBook.createFont();
            font.setBold(false);

            XSSFCellStyle heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);
            heaCelSty.setAlignment(HorizontalAlignment.LEFT);
            heaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFCellStyle valueCelStyle = workBook.createCellStyle();
            valueCelStyle.setFont(font);
            valueCelStyle.setWrapText(true);
            valueCelStyle.setAlignment(HorizontalAlignment.RIGHT);
            heaCelSty.setVerticalAlignment(VerticalAlignment.CENTER);

            Runtime r = Runtime.getRuntime();
            r.freeMemory();

            XSSFSheet sheet = workBook.createSheet("URL Auditor Report");
            CommonReportGenerator.appendImageToExcellCell(workBook, sheet, (short) 0, 0, (short) 1, 1);

            int rowCount = 0;
            /*Creating Title for sheet*/
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            cell.setCellValue("URL Auditor Report");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 1000);

            /*Creating TimeStamp of sheet*/
            cell = rows.createCell(5);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            reviewOnFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reviewOnFormat.format(startDat);
            cell.setCellValue("Reviewed on: " + curDate);
            cell.setCellStyle(notApplicableCelltyle);

            rowCount = rowCount + 2;
            if (urlValidatorOld.getNotWorkingURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("NONWORKING URL(s): " + urlValidatorOld.getNotWorkingURLCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getTemporaryRedirectingCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("TEMPORARY REDIRECTION URL(s): " + urlValidatorOld.getTemporaryRedirectingCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getPermanentRedirectingCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("PERMANENT REDIRECTION URL(s): " + urlValidatorOld.getPermanentRedirectingCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getWorkingURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("WORKING URL(s): " + urlValidatorOld.getWorkingURLCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getNoURLStatusCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);

                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("NO STATUS URL(s): " + urlValidatorOld.getNoURLStatusCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            if (urlValidatorOld.getTotalURLCount() >= 1) {
                rows = sheet.createRow(rowCount);
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
                cell.setCellValue("TOTAL URL(s): " + urlValidatorOld.getTotalURLCount());
                cell.setCellStyle(dashBoardStyle);
                rowCount = rowCount + 1;
            }

            rowCount = rowCount + 1;
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);
            rowCount = rowCount + 1;
            int numberOfRedirections = getCountofRedirections(urlStatusList);
            /*Table headers*/
            rows = sheet.createRow(rowCount);
            rows.setHeight((short) 500);

            /*URL coloum header*/
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 2));
            cell.setCellValue(colHeaders[0]);
            cell.setCellStyle(columnHeaderStyle);

            int colCount = 3;
            for (; colCount < colHeaders.length; colCount++) {
                cell = rows.createCell(colCount);
                cell.setCellStyle(columnHeaderStyle);
                if (colCount == 7 && !(searchTerm.trim().equals(""))) {
                    cell.setCellValue(colHeaders[colCount] + " (" + searchTerm + ")");
                } else {
                    cell.setCellValue(colHeaders[colCount]);
                }
            }
            if (numberOfRedirections != 0) {
                for (int redirectionCount = 1; redirectionCount <= numberOfRedirections; redirectionCount++) {
                    cell = rows.createCell(colCount);
                    cell.setCellStyle(columnHeaderStyle);
                    if (colCount % 2 == 0) {
                        cell.setCellValue("Redirect Location(" + redirectionCount + ")");
                    }
                    colCount++;
                    cell = rows.createCell(colCount);
                    cell.setCellStyle(columnHeaderStyle);
                    if (colCount % 2 != 0) {
                        cell.setCellValue("Redirect Status(" + redirectionCount + ")");
                    }
                    colCount++;
                }
            }

            UrlStatus urlStatus = null;
            colCount = 0;
            XSSFRow row;
            Map<String, Integer> chainedRedirections;
            int tempRowCount = 0;
            int freezeRowCount = rowCount + 1;
            for (int i = 0; i < urlStatusList.size(); i++) {
                try {
                    urlStatus = urlStatusList.get(i);
                    tempRowCount = rowCount + 1;
                    row = sheet.createRow(tempRowCount);
                    row.setHeight((short) 500);
                    chainedRedirections = urlStatus.getChainedRedirectsMap();
                    for (; colCount < colHeaders.length; colCount++) {
                        if (colCount == 0) {
                            cell = row.createCell(colCount);
                            sheet.addMergedRegion(new CellRangeAddress(tempRowCount, tempRowCount, 0, 2));
                        } else {
                            cell = row.createCell(colCount + 2);
                        }
                        switch (colCount) {
                            case 0:
                                cell.setCellValue(urlStatus.getInputUrl());

                                cell.setCellStyle(heaCelSty);
                                break;
                            case 1:
                                cell = row.createCell(colCount + 2);
                                cell.setCellValue(urlStatus.getStatusCode());
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 2:
                                cell.setCellValue(urlStatus.getChainedRedirectsMap() != null ? urlStatus.getChainedRedirectsMap().size() : 0);
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 3:
//                            cell.setCellValue(urlStatus.getDestinationURL());
                                if ((urlStatus.getStatusCode() < 400) && !(urlStatus.getDestinationURL() != null && urlStatus.getDestinationURL().contains("Connection"))) {
                                    cell.setCellValue(urlStatus.getDestinationURL());
                                    cell.setCellStyle(heaCelSty);
                                } else if ((urlStatus.getStatusCode() >= 400 && urlStatus.getStatusCode() <= 600) || urlStatus.getStatusCode() == 0) {
                                    cell.setCellValue("-");
                                    cell.setCellStyle(notApplicableCelltyle);
                                } else if (urlStatus.getDestinationURL().contains("Connection")) {
                                    cell.setCellValue(urlStatus.getDestinationURL());
                                }
                                break;
                            case 4:
                                cell.setCellValue(urlStatus.getFinalStatusCode());
                                cell.setCellStyle(valueCelStyle);
                                break;
                            case 5:
                                cell.setCellValue(urlStatus.getKeywordCount());
                                cell.setCellStyle(valueCelStyle);
                                break;
                        }

                    }
                    sheet.createFreezePane(0, freezeRowCount);
//                    colCount = colHeaders.length+1;

                    for (Map.Entry<String, Integer> chainedKey : chainedRedirections.entrySet()) {
                        cell = row.createCell(colCount);
                        if (colCount % 2 == 0) {
                            cell.setCellValue(chainedKey.getKey());
                            cell.setCellStyle(heaCelSty);
                        }
                        colCount++;
                        cell = row.createCell(colCount);
                        if (colCount % 2 != 0) {
                            cell.setCellValue(chainedKey.getValue());
                            cell.setCellStyle(valueCelStyle);
                        }
                        colCount++;
                    }
                } catch (Exception e) {
                    logger.error("Exception in adding the row at Exceles files at current row index:: " + rowCount + ", urlStatus:: " + (urlStatus != null ? urlStatus.toString() : null) + ", and cause:: " + e.getLocalizedMessage());
                }
                rowCount++;
                colCount = 0;
            }
            int colWidth[] = new int[]{10, 10, 10, 13, 13, 45, 17, 17, 45, 20, 45, 20, 45, 20, 45, 20};
            for (int i = 1; i <= colHeaders.length + numberOfRedirections + 2; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            String s = fileSaveFormat.format(new Date());
            String fileName = filePath + "LXRMarketplace_URLAuditor_" + s + ".xlsx";
            File file = new File(fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
            return fileName;
        } catch (Exception e) {
            logger.error("Exception for generateExcelesFile", e);
        }
        return null;
    }

    /**
     * called by controller to create a file in text(.csv) format for
     * downloading when URLs are entered manually
     *
     * @param urlStatusList
     * @param filePath
     * @param searchTerm
     * @param urlValidatorOld
     * @return fileName
     */
    public String createCsvFile(ArrayList<UrlStatus> urlStatusList,
            String filePath, String searchTerm, UrlValidator urlValidatorOld) {
        String fileNmae = "";
        try {
            String dateFormat = fileSaveFormat.format(new Date());
            String fileName = filePath + "LXRMarketplace_URLAuditor_" + dateFormat + ".csv";
            stream = new FileOutputStream(fileName);
            printStream = new PrintStream(stream);
            /*Report title*/
            printStream.print("URL Auditor Report" + "\t");
            printStream.println();

            /*Creating TimeStamp of sheet*/
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            reviewOnFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reviewOnFormat.format(startDat);

            printStream.print("Reviewed on: " + curDate + "\t");
            printStream.println();

            if (urlValidatorOld.getNotWorkingURLCount() >= 1) {
                printStream.print("NONWORKING URL(s):  " + urlValidatorOld.getNotWorkingURLCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getTemporaryRedirectingCount() >= 1) {
                printStream.print("TEMPORARY REDIRECTION URL(s):  " + urlValidatorOld.getTemporaryRedirectingCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getPermanentRedirectingCount() >= 1) {
                printStream.print("PERMANENT REDIRECTION URL(s):  " + urlValidatorOld.getPermanentRedirectingCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getWorkingURLCount() >= 1) {
                printStream.print("WORKING URL(s):  " + urlValidatorOld.getWorkingURLCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getNoURLStatusCount() >= 1) {
                printStream.print("NO STATUS URL(s):  " + urlValidatorOld.getNoURLStatusCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getTotalURLCount() >= 1) {
                printStream.print("TOTAL URL(s):  " + urlValidatorOld.getTotalURLCount() + "\t");
                printStream.println();
            }

            int numberOfRedirections = getCountofRedirections(urlStatusList);
            for (String csvColHeader : csvColHeaders) {
                if (csvColHeader.equals("Keyword Count") && !(searchTerm.trim().equals(""))) {
                    printStream.print(csvColHeader + " (" + searchTerm + ")" + ",");
                    break;
                } else {
                    printStream.print(csvColHeader + ",");
                }
            }

            if (numberOfRedirections != 0) {
                for (int redirectionCount = 1; redirectionCount <= numberOfRedirections; redirectionCount++) {
                    printStream.print("Redirect Location(" + redirectionCount + ")" + ",");
                    printStream.print("Redirect Status(" + redirectionCount + ")" + ",");
                }
            }
            printStream.println();
            UrlStatus urlStatus = null;
            for (int i = 0; i < urlStatusList.size(); i++) {
                try {
                    urlStatus = urlStatusList.get(i);
                    printStream.println();
                    printStream.print(urlStatus.getInputUrl() + ",");
                    printStream.print(urlStatus.getStatusCode() + ",");
                    printStream.print((urlStatus.getChainedRedirectsMap() != null ? urlStatus.getChainedRedirectsMap().size() : 0) + ",");
//                printStream.print(urlStatus.getDestinationURL() + ",");
                    if ((urlStatus.getStatusCode() < 400) && !(urlStatus.getDestinationURL() != null && urlStatus.getDestinationURL().contains("Connection"))) {
                        printStream.print(urlStatus.getDestinationURL() + ",");
                    } else if ((urlStatus.getStatusCode() >= 400 && urlStatus.getStatusCode() <= 600) || urlStatus.getStatusCode() == 0) {
                        printStream.print("-,");
                    } else if (urlStatus.getDestinationURL().contains("Connection")) {
                        printStream.print(urlStatus.getDestinationURL());
                    }
                    printStream.print(urlStatus.getFinalStatusCode() + ",");
                    printStream.print(urlStatus.getKeywordCount() + ",");
                    for (Map.Entry<String, Integer> chainedKey : urlStatus.getChainedRedirectsMap().entrySet()) {
                        printStream.print(chainedKey.getKey() + ",");
                        printStream.print(chainedKey.getValue() + ",");
                    }
                } catch (Exception e) {
                    logger.error("Exception in adding the row content to createCsvFile at rowindex:: " + i + ", and urlStatus:: " + (urlStatus != null ? urlStatus.toString() : null) + ", and cause:: " + e.getLocalizedMessage());
                }
            }
            return fileName;
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in createCsvFile and cause:: " + e.getLocalizedMessage());
        } finally {
            printStream.flush();
            printStream.close();
        }
        return fileNmae;
    }

    /**
     * called by controller to create a file in text(.tsv) format for
     * downloading when URLs are entered manually
     *
     * @param urlStatusList
     * @param filePath
     * @param searchTerm
     * @param urlValidatorOld
     * @return fileName
     */
    public String createTsvFile(ArrayList<UrlStatus> urlStatusList,
            String filePath, String searchTerm, UrlValidator urlValidatorOld) {
        String fileNmae = "";
        try {
            String dateString = fileSaveFormat.format(new Date());
            String fileName = filePath + "LXRMarketplace_URLAuditor_" + dateString + ".tsv";
            stream = new FileOutputStream(fileName);
            printStream = new PrintStream(stream);
            /*Report title*/
            printStream.print("URL Auditor Report" + "\t");
            printStream.println();

            /*Creating TimeStamp of sheet*/
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            reviewOnFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = reviewOnFormat.format(startDat);

            printStream.print("Reviewed on: " + curDate + "\t");
            printStream.println();

            if (urlValidatorOld.getNotWorkingURLCount() >= 1) {
                printStream.print("NONWORKING URL(s):  " + urlValidatorOld.getNotWorkingURLCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getTemporaryRedirectingCount() >= 1) {
                printStream.print("TEMPORARY REDIRECTION URL(s):  " + urlValidatorOld.getTemporaryRedirectingCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getPermanentRedirectingCount() >= 1) {
                printStream.print("PERMANENT REDIRECTION URL(s):  " + urlValidatorOld.getPermanentRedirectingCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getWorkingURLCount() >= 1) {
                printStream.print("WORKING URL(s):  " + urlValidatorOld.getWorkingURLCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getNoURLStatusCount() >= 1) {
                printStream.print("NO STATUS URL(s):  " + urlValidatorOld.getNoURLStatusCount() + "\t");
                printStream.println();
            }

            if (urlValidatorOld.getTotalURLCount() >= 1) {
                printStream.print("TOTAL URL(s):  " + urlValidatorOld.getTotalURLCount() + "\t");
                printStream.println();
            }

            int numberOfRedirections = getCountofRedirections(urlStatusList);
            for (String csvColHeader : csvColHeaders) {
                if (csvColHeader.equals("Keyword Count") && !(searchTerm.trim().equals(""))) {
                    printStream.print(csvColHeader + " (" + searchTerm + ")" + ",");
                    break;
                } else {
                    printStream.print(csvColHeader + ",");
                }
            }

            if (numberOfRedirections != 0) {
                for (int redirectionCount = 1; redirectionCount <= numberOfRedirections; redirectionCount++) {
                    printStream.print("Redirect Location(" + redirectionCount + ")" + ",");
                    printStream.print("Redirect Status(" + redirectionCount + ")" + ",");
                }
            }
            printStream.println();
            UrlStatus urlStatus = null;
            for (int i = 0; i < urlStatusList.size(); i++) {
                try {
                    urlStatus = urlStatusList.get(i);
                    printStream.println();
                    printStream.print(urlStatus.getInputUrl() + ",");
                    printStream.print(urlStatus.getStatusCode() + ",");
                    printStream.print((urlStatus.getChainedRedirectsMap() != null ? urlStatus.getChainedRedirectsMap().size() : 0) + ",");
//                printStream.print(urlStatus.getDestinationURL() + ",");
                    if ((urlStatus.getStatusCode() < 400) && !(urlStatus.getDestinationURL() != null && urlStatus.getDestinationURL().contains("Connection"))) {
                        printStream.print(urlStatus.getDestinationURL() + ",");
                    } else if ((urlStatus.getStatusCode() >= 400 && urlStatus.getStatusCode() <= 600) || urlStatus.getStatusCode() == 0) {
                        printStream.print("-,");
                    } else if (urlStatus.getDestinationURL().contains("Connection")) {
                        printStream.print(urlStatus.getDestinationURL());
                    }
                    printStream.print(urlStatus.getFinalStatusCode() + ",");
                    printStream.print(urlStatus.getKeywordCount() + ",");
                    for (Map.Entry<String, Integer> chainedKey : urlStatus.getChainedRedirectsMap().entrySet()) {
                        printStream.print(chainedKey.getKey() + ",");
                        printStream.print(chainedKey.getValue() + ",");
                    }
                } catch (Exception e) {
                    logger.error("Exception in adding the row content to createTsvFile at rowindex:: " + i + ", and urlStatus:: " + (urlStatus != null ? urlStatus.toString() : null) + ", and cause:: " + e.getLocalizedMessage());
                }
            }
            return fileName;
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in createTsvFile and cause:: " + e.getLocalizedMessage());
        } finally {
            printStream.flush();
            printStream.close();
        }
        return fileNmae;
    }

    /*
	 * called by controller to download the results in same file which user has
	 * uploaded for testing
     */
    public String appendToExistingFile(ArrayList<UrlStatus> urlStatusList,
            String fileName, String selCol, boolean headerRow, String searchTerm, String downloadFolder) {
        String finalFile = "";
        int selColInd = Integer.parseInt(selCol);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileName);
            String newFilename = getFinalFileName(fileName, downloadFolder);
            //File file = new File(newFilename);
            FileOutputStream stream = new FileOutputStream(newFilename);
            if (fileName.endsWith(".xls") && !(fileName.endsWith(".xlsx"))) {
                HSSFWorkbook workBook = writeExcelFileData(fis, urlStatusList,
                        selColInd, headerRow, searchTerm);
                workBook.write(stream);
            } else if (fileName.endsWith(".xlsx")) {
                XSSFWorkbook workBook = writeNewExcelFileData(fileName,
                        urlStatusList, selColInd, headerRow, searchTerm);
                workBook.write(stream);
            } else if (fileName.endsWith(".csv")) {
                writeCsvFileData(fis, stream, urlStatusList, selColInd,
                        headerRow, searchTerm);
            } else if (fileName.endsWith(".tsv")) {
                writeTsvFileData(fis, stream, urlStatusList, selColInd,
                        headerRow, searchTerm);
            }
            stream.flush();
            stream.close();
            return newFilename;
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return finalFile;
    }

    /* called to append the columns in header of uploaded file (for downloading) */
    public int createHeaderRow(HSSFSheet sheet, int firstRowNum,
            HSSFCellStyle columnHeaderStyle, boolean headerRow, String searchTerm) {
        HSSFRow row = sheet.getRow(firstRowNum);
//        sheet.autoSizeColumn(100000);
        int lastCellInd = row.getLastCellNum();
        if (headerRow) {
            int ind = lastCellInd;
            HSSFCell cell = row.createCell(ind);
            cell.setCellValue("Status");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            cell.setCellValue("Status Code");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            cell.setCellValue("Remark");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            if (!(searchTerm.trim().equals(""))) {
                cell.setCellValue("Keyword Count (" + searchTerm + ")");
            } else {
                cell.setCellValue("Keyword Count");
            }
            cell.setCellStyle(columnHeaderStyle);
        }
        return lastCellInd;
    }

    /* called to append the columns in header of uploaded file (for downloading) */
    public int createHeaderRowNewExcel(XSSFSheet sheet, int firstRowNum,
            XSSFCellStyle columnHeaderStyle, boolean headerRow, String searchTerm) {
        XSSFRow row = sheet.getRow(firstRowNum);
        // sheet.getCTWorksheet().getDimension().
        int lastCellInd = row.getLastCellNum();
        if (headerRow) {
            int ind = lastCellInd;
            XSSFCell cell = row.createCell(ind);
            cell.setCellValue("Status");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            cell.setCellValue("Status Code");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            cell.setCellValue("Remark");
            cell.setCellStyle(columnHeaderStyle);
            ind++;
            cell = row.createCell(ind);
            if (!(searchTerm.trim().equals(""))) {
                cell.setCellValue("Keyword Count (" + searchTerm + ")");
            } else {
                cell.setCellValue("Keyword Count");
            }
            cell.setCellStyle(columnHeaderStyle);
        }
        return lastCellInd;
    }

    /*
	 * called to append the columns data in uploaded file in excel format(.xls)
	 * (for downloading)
     */
    public HSSFWorkbook writeExcelFileData(FileInputStream fis,
            ArrayList<UrlStatus> urlStatusList, int selColInd, boolean headerRow, String searchTerm) {
        HSSFWorkbook workBook = null;
        POIFSFileSystem fs = null;
        try {
            fs = new POIFSFileSystem(fis);
            workBook = new HSSFWorkbook(fs);
            HSSFSheet sheet = workBook.getSheetAt(0);
//            sheet.autoSizeColumn(100000);
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            HSSFFont font = workBook.createFont();
            font.setBold(true);
            columnHeaderStyle.setFont(font);
            HSSFCellStyle sstatusColStyle = workBook.createCellStyle();

            int firstRowNum = sheet.getFirstRowNum();
            int lastRorNum = sheet.getLastRowNum();
            int listInd = 0;
            int lastCellInd = createHeaderRow(sheet, firstRowNum,
                    columnHeaderStyle, headerRow, searchTerm);
            if (lastRorNum > 100) {
                lastRorNum = 100;
            }
            if (headerRow) {
                firstRowNum++;
                //lastRorNum += 1;
                if (lastRorNum > 101) {
                    lastRorNum = 101;
                }
            } else if (lastRorNum > 99) {
                lastRorNum = 99;
            }

            for (int rowIndex = firstRowNum; rowIndex <= lastRorNum; rowIndex++) {
                HSSFRow row = sheet.getRow(rowIndex);
                if (row != null) {
                    HSSFCell cell = row.getCell(selColInd);
                    row.getLastCellNum();
                    if (cell != null && !(cell.getStringCellValue().trim().equals(""))) {
                        if (listInd < urlStatusList.size()) {
                            UrlStatus urlStatus = urlStatusList.get(listInd);
                            listInd++;
                            cell.setCellValue(urlStatus.getInputUrl());
                            int ind = lastCellInd;
                            cell = row.createCell(ind);
                            ind++;
                            cell.setCellValue(urlStatus.getStatus());
                            cell = row.createCell(ind);
                            ind++;
                            cell.setCellValue(urlStatus.getStatusCode());
                            cell = row.createCell(ind);
                            ind++;
                            cell.setCellValue(urlStatus.getRemark());
                            cell = row.createCell(ind);
                            ind++;
                            cell.setCellValue(urlStatus.getKeywordCount());
                        }
                    }
                }
            }
            return workBook;
        } catch (IOException e) {
            logger.error("Exception in writeExcelFileData", e);
        }
        return null;
    }

    /*
	 * called to append the columns data in uploaded file in excel format(.xls)
	 * (for downloading)
     */
    public XSSFWorkbook writeNewExcelFileData(String fileName,
            ArrayList<UrlStatus> urlStatusList, int selColInd, boolean headerRow, String searchTerm) {
        XSSFWorkbook workBook = null;
        try {
            workBook = new XSSFWorkbook(fileName);
            XSSFSheet sheet = workBook.getSheetAt(0);
            XSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            XSSFFont font = workBook.createFont();
            font.setBold(true);
            columnHeaderStyle.setFont(font);
            int firstRowNum = sheet.getFirstRowNum();
            int lastRorNum = sheet.getLastRowNum();
            int listInd = 0;
            int lastCellInd = createHeaderRowNewExcel(sheet, firstRowNum,
                    columnHeaderStyle, headerRow, searchTerm);
            if (lastRorNum > 100) {
                lastRorNum = 100;
            }
            if (headerRow) {
                firstRowNum++;
                //lastRorNum += 1;
                if (lastRorNum > 101) {
                    lastRorNum = 101;
                }
            } else if (lastRorNum > 99) {
                lastRorNum = 99;
            }
            for (int rowIndex = firstRowNum; rowIndex <= lastRorNum; rowIndex++) {
                XSSFRow row = sheet.getRow(rowIndex);
                if (row != null) {
                    XSSFCell cell = row.getCell(selColInd);
                    row.getLastCellNum();
                    if (cell != null && !(cell.getRawValue().trim().equals(""))) {
                        UrlStatus urlStatus = urlStatusList.get(listInd);
                        listInd++;
                        cell.setCellValue(urlStatus.getInputUrl());
                        int ind = lastCellInd;
                        cell = row.createCell(ind);
                        ind++;
                        cell.setCellValue(urlStatus.getStatus());
                        cell = row.createCell(ind);
                        ind++;
                        cell.setCellValue(urlStatus.getStatusCode());
                        cell = row.createCell(ind);
                        ind++;
                        cell.setCellValue(urlStatus.getRemark());
                        cell = row.createCell(ind);
                        ind++;
                        cell.setCellValue(urlStatus.getKeywordCount());
                    }
                }
            }
            return workBook;
        } catch (IOException e) {
            logger.error("Exception in writeNewExcelFileData", e);
        }
        return null;
    }

    /*
	 * called to append the columns data in uploaded file in excel format(.xls)
	 * (for downloading)
     */
    public void writeCsvFileData(FileInputStream fis, FileOutputStream stream,
            ArrayList<UrlStatus> urlStatusList, int selColInd, boolean headerRow, String searchTerm) {
        PrintStream ps = null;
        try {
            // FileInputStream fis = new FileInputStream(fileName);
            CSVParser csvp = new CSVParser(fis);
            String[][] values = csvp.getAllValues();
            int listInd = 0;
            ps = new PrintStream(stream);
            String[][] newValues = new String[values.length][values[0].length + 4];
            for (int i = 0; i < values.length; i++) {
                for (int j = 0; j < values[i].length; j++) {
                    newValues[i][j] = values[i][j];
                }
            }
            int firstRowNum = 0;
            int rowNum = 100;
            if (headerRow) {
                rowNum = 101;
                int lastColInd = newValues[0].length - 4;
                newValues[0][lastColInd++] = "Status";
                newValues[0][lastColInd++] = "Status Code";
                newValues[0][lastColInd++] = "Remark";
                if (!(searchTerm.trim().equals(""))) {
                    newValues[0][lastColInd++] = "Keyword Count (" + searchTerm + ")";
                } else {
                    newValues[0][lastColInd++] = "Keyword Count";
                }
                firstRowNum = 1;
            }
            int length = newValues.length;

            for (int i = firstRowNum; i < length; i++) {
                if (i >= rowNum) {
                    newValues[i][newValues[i].length - 4] = "";
                    newValues[i][newValues[i].length - 3] = "";
                    newValues[i][newValues[i].length - 2] = "";
                    newValues[i][newValues[i].length - 1] = "";
                } else if (urlStatusList.size() > listInd) {
                    UrlStatus urlStatus = urlStatusList.get(listInd);
                    listInd++;
                    newValues[i][newValues[i].length - 4] = urlStatus.getStatus();
                    newValues[i][newValues[i].length - 3] = Integer.toString(urlStatus.getStatusCode());
                    newValues[i][newValues[i].length - 2] = urlStatus.getRemark();
                    newValues[i][newValues[i].length - 1] = Integer.toString(urlStatus.getKeywordCount());
                }
            }
            for (String[] newValue : newValues) {
                for (String newValue1 : newValue) {
                    ps.print(newValue1 + ",");
                }
                ps.println();
            }
        } catch (IOException e) {
            logger.error("Exception for writeCsvFileData:", e);
        } finally {
            ps.close();
        }
    }

    /*
	 * called to append the columns data in uploaded file in excel format(.xls)
	 * (for downloading)
     */
    public void writeTsvFileData(FileInputStream fis, FileOutputStream stream,
            ArrayList<UrlStatus> urlStatusList, int selColInd, boolean headerRow, String searchTerm) {
        PrintStream ps = null;
        try {
            // FileInputStream fis = new FileInputStream(fileName);
            CSVParser csvp = new CSVParser(fis);
            String[][] values = csvp.getAllValues();
            int listInd = 0;
            ps = new PrintStream(stream);
            String[][] newValues = new String[values.length][values[0].length + 4];
            for (int i = 0; i < values.length; i++) {
                System.arraycopy(values[i], 0, newValues[i], 0, values[i].length);
            }
            int firstRowNum = 0;
            int rowNum = 100;
            if (headerRow) {
                rowNum = 101;
                int lastColInd = newValues[0].length - 4;
                newValues[0][lastColInd++] = "Status";
                newValues[0][lastColInd++] = "Status Code";
                newValues[0][lastColInd++] = "Remark";
                if (!(searchTerm.trim().equals(""))) {
                    newValues[0][lastColInd++] = "Keyword Count (" + searchTerm + ")";
                } else {
                    newValues[0][lastColInd++] = "Keyword Count";
                }
                firstRowNum = 1;
            }

            for (int i = firstRowNum; i < newValues.length; i++) {
                if (i >= rowNum) {
                    newValues[i][newValues[i].length - 4] = "";
                    newValues[i][newValues[i].length - 3] = "";
                    newValues[i][newValues[i].length - 2] = "";
                    newValues[i][newValues[i].length - 1] = "";
                } else {
                    UrlStatus urlStatus = urlStatusList.get(listInd);
                    listInd++;
                    newValues[i][newValues[i].length - 4] = urlStatus.getStatus();
                    newValues[i][newValues[i].length - 3] = Integer.toString(urlStatus.getStatusCode());
                    newValues[i][newValues[i].length - 2] = urlStatus.getRemark();
                    newValues[i][newValues[i].length - 1] = Integer.toString(urlStatus.getKeywordCount());
                }

            }
            for (int rowInd = 0; rowInd < newValues.length; rowInd++) {
                for (int colInd = 0; colInd < newValues[rowInd].length; colInd++) {
                    ps.print(newValues[rowInd][colInd] + "\t");
                }
                ps.println();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            ps.close();
        }
    }

    /* called to get the filename for downloading */
    public String getFinalFileName(String fileName, String downloadFolder) {
        int ind = fileName.lastIndexOf("/");
        int ind1 = fileName.lastIndexOf(".");
        String newFilename = fileName.substring(0, ind);
        String fName = fileName.substring(ind + 1, ind1);
        newFilename = downloadFolder + fName;
        String ext = fileName.substring((ind1 + 1), fileName.length());
        newFilename = newFilename + "." + ext;
        return newFilename;
    }

    /*
	 * called for excel file (.xls)for getting the list of columns in first line
	 * or list of urls from specified column. if options is "columnList then it
	 * will return list of columns data in first row if options is "urlList" it
	 * will return list of urs from column number specified by selColInd
	 * argument
     */
    public ArrayList<String> getListFromXls(FileInputStream fis,
            String options, int selColInd, boolean headerRow, int toolAccessLimit) {
        ArrayList<String> list = new ArrayList<>();
        POIFSFileSystem fs = null;
        try {
            fs = new POIFSFileSystem(fis);
            HSSFWorkbook workbook = new HSSFWorkbook(fs);
            HSSFSheet sheet = workbook.getSheetAt(0);
            if (options.equalsIgnoreCase("columnList")) {
                int number = sheet.getFirstRowNum();
                HSSFRow row = sheet.getRow(number);
                if (row != null) {
                    short lastCellNum = row.getLastCellNum();
                    for (int i = 0; i < lastCellNum; i++) {
                        HSSFCell cell = row.getCell(i);
                        if (cell != null) {
                            if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                list.add(Double.toString(cell.getNumericCellValue()));
                            } else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                list.add(cell.getRichStringCellValue().getString());
                            }
                        }
                        /*Empty coloum data should not accept
                        else {
                            list.add("");
                        }*/
                    }
                }
            } else if (options.equalsIgnoreCase("urlList")) {
                int lastRorNum = sheet.getLastRowNum();
                if (lastRorNum > 100) {
                    /*lastRorNum = 100;*/
                    lastRorNum = toolAccessLimit;
                }
                int firstRowNum = 0;
                if (headerRow) {
                    firstRowNum = sheet.getFirstRowNum() + 1;
                    if (lastRorNum > 101) {
                        lastRorNum = toolAccessLimit + 1;
                    }
                } else {
                    firstRowNum = sheet.getFirstRowNum();
                    if (lastRorNum > 99) {
                        lastRorNum = toolAccessLimit - 1;
                    }
                }

                HSSFRow row;
                HSSFCell cell;
                for (int rowIndex = firstRowNum; rowIndex <= lastRorNum; rowIndex++) {
                    row = sheet.getRow(rowIndex);
                    if (row != null) {
                        cell = row.getCell(selColInd);
                        if (cell != null) {
                            if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                list.add(Double.toString(cell.getNumericCellValue()));
                            } else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                list.add(cell.getRichStringCellValue().getString());
                            }
                        } else if (cell == null) {
                            for (int i = 0; i < row.getLastCellNum(); i++) {
                                cell = row.getCell(i);
                                if (cell != null) {
                                    if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                                        list.add(Double.toString(cell.getNumericCellValue()));
                                    } else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                        list.add(cell.getRichStringCellValue().getString());
                                    }
                                }
                            }
                        }/*Else if */
                    }

                }
            }
        } catch (Exception e) {
            logger.error("Exception for getListFromXls", e);
        }
        return list;
    }

    /*
	 * called for text file (.csv)for getting the list of columns in first line
	 * or list of urls from specified column. if options is "columnList then it
	 * will return list of columns data in first row if options is "urlList" it
	 * will return list of urs from column number specified by selColInd
	 * argument
     */
    public ArrayList<String> getListFromCsv(FileInputStream fis,
            String options, int selColInd, boolean headerRow, int toolAccessLimit) {
        ArrayList<String> list = new ArrayList<>();
        CSVParser csvp = new CSVParser(fis);
        try {
            String[][] values = csvp.getAllValues();
            if (options.equalsIgnoreCase("columnList")) {
                String[] columns = values[0];
                list.addAll(Arrays.asList(columns));
                list.removeAll(Arrays.asList("", null));
            } else if (options.equalsIgnoreCase("urlList")) {
                int len = values.length;
                if (len > toolAccessLimit) {
                    len = toolAccessLimit;
                }

                int firstRowNum = 0;
                if (headerRow) {
                    len = values.length;
                    firstRowNum = 1;
                    if (len > toolAccessLimit) {
                        len = toolAccessLimit;
                    }
                    //len -= 1;
                }
                String dataString;
                //for (int i = firstRowNum; i < values.length; i++) {
                for (int i = firstRowNum; i < len - 1; i++) {
                    dataString = values[i][selColInd];
                    //String[] data = dataString.split(",");
                    list.add(dataString);
                    list.removeAll(Arrays.asList("", null));
                }
            }
        } catch (IOException e) {
            logger.error("Exception for getListFromCsv", e);
        }
        return list;
    }

    /*
	 * called for text file (.tsv)for getting the list of columns in first line
	 * or list of urls from specified column. if options is "columnList then it
	 * will return list of columns data in first row if options is "urlList" it
	 * will return list of urs from column number specified by selColInd
	 * argument
     */
    public ArrayList<String> getListFromTsv(FileInputStream fis,
            String options, int selColInd, boolean headerRow, int toolAccessLimit) {
        ArrayList<String> list = new ArrayList<>();
        CSVParser csvp = new CSVParser(fis);
        try {
            String[][] values = csvp.getAllValues();
            if (options.equalsIgnoreCase("columnList")) {
                String columnsString = values[0][0];
                String[] columns = columnsString.split("\t");
                list.addAll(Arrays.asList(columns));
                list.removeAll(Arrays.asList("", null));
            } else if (options.equalsIgnoreCase("urlList")) {
                int length = values.length;
                if (length > toolAccessLimit) {
                    length = toolAccessLimit;
                }
                int firstRowNum = 0;
                if (headerRow) {
                    length = values.length;
                    firstRowNum = 1;
                    if (length > toolAccessLimit) {
                        length = toolAccessLimit;
                    }
                    //length -= 1;
                }
                String dataString;
                String[] data;

                for (int i = firstRowNum; i < length - 1; i++) {
                    dataString = values[i][0];
                    data = dataString.split("\t");
                    if (data.length >= selColInd) {
                        list.add(data[selColInd]);
                    }
                }
                list.removeAll(Arrays.asList("", null));
            }
        } catch (IOException e) {
            logger.error("Exception for getListFromTsv", e);
        }
        return list;
    }

    /*
	 * called for new excel file (.xlsx)for getting the list of columns in first
	 * line or list of urls from specified column. if options is "columnList
	 * then it will return list of columns data in first row if options is
	 * "urlList" it will return list of urs from column number specified by
	 * selColInd argument
     */
    public ArrayList<String> getListFromXlsx(String filePath, String options,
            int selColInd, boolean headerRow, int toolAccessLimit) {
        ArrayList<String> list = new ArrayList<>();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(filePath));
            XSSFSheet sheet = workbook.getSheetAt(0);
            if (options.equalsIgnoreCase("columnList")) {
                int number = sheet.getFirstRowNum();
                XSSFRow row = sheet.getRow(number);
                if (row != null) {
                    short lastCellNum = row.getLastCellNum();
                    for (int i = 0; i < lastCellNum; i++) {
                        XSSFCell cell = row.getCell(i);
                        if (cell != null) {
                            if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                                list.add(Double.toString(cell.getNumericCellValue()));
                            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                                list.add(cell.getRichStringCellValue().toString());
                            }
                        }
                        /*Empty coloum data should not accept
                        else {
                            list.add("");
                        }*/
                    }
                }
            } else if (options.equalsIgnoreCase("urlList")) {
                int firstRowNum = 0;
                int lastRorNum = sheet.getLastRowNum();
                if (lastRorNum > 100) {
                    lastRorNum = toolAccessLimit;
                }
                if (headerRow) {
                    firstRowNum = sheet.getFirstRowNum() + 1;
                    if (lastRorNum > 101) {
                        lastRorNum = toolAccessLimit + 1;
                    }
                } else {
                    firstRowNum = sheet.getFirstRowNum();
                    if (lastRorNum > 99) {
                        lastRorNum = toolAccessLimit - 1;
                    }
                }
                XSSFRow row;
                XSSFCell cell;
                for (int rowIndex = firstRowNum; rowIndex <= lastRorNum; rowIndex++) {
                    row = sheet.getRow(rowIndex);
                    if (row != null) {
                        cell = row.getCell(selColInd);
                        if (cell != null) {
                            if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                                list.add(Double.toString(cell.getNumericCellValue()));
                            } else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                                list.add(cell.getRichStringCellValue().getString());
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception for getListFromXlsx:", e);
        }
        return list;
    }

    /*Get count of redirections in list of objects*/
    public int getCountofRedirections(ArrayList<UrlStatus> queryURLList) {
        int numberOfRedirections = 0, maxSize;
        /*To find number of redirection count.*/
        for (UrlStatus urlStatus : queryURLList) {
            maxSize = urlStatus.getChainedRedirectsMap().size();
            if (numberOfRedirections < maxSize) {
                numberOfRedirections = maxSize;
            }
        }
        return numberOfRedirections;
    }

    public ArrayList<String> convertStringToArrayList(StringBuffer string) {
        return convertStringToArrayList(string, "[;\\n]");
    }

    public ArrayList<String> convertStringToArrayList(StringBuffer string,
            String separator) {
        ArrayList<String> keywordList = new ArrayList<>();
        if (string != null && string.length() > 0 && !string.toString().trim().equals("")) {
            Pattern p = Pattern.compile(separator);
            String[] list = p.split(string);
            for (String list1 : list) {
                if (list1.trim().length() > 0) {
                    keywordList.add(list1.trim());
                }
            }
        }
        return keywordList;
    }
}
