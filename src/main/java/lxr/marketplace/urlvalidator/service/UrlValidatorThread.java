package lxr.marketplace.urlvalidator.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLException;

import org.apache.log4j.Logger;

import lxr.marketplace.urlvalidator.domain.UrlStatus;
import lxr.marketplace.util.Common;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class UrlValidatorThread implements Runnable {

    private static Logger LOGGER = Logger.getLogger(UrlValidatorThread.class);
    static final int IMPORTANT_READ_TIME_OUT = 25000;
    static final int IMPORTANT_CONNECT_TIME_OUT = 30000;

    UrlStatus queryUrlStatus;
    String keyword;

    public UrlValidatorThread(UrlStatus inputUrl, String keyword) {
        this.queryUrlStatus = inputUrl;
        this.keyword = keyword;
    }

    @Override
    public void run() {
        getURLChainedRedirections(this.queryUrlStatus.getInputUrl());
    }

    public void getURLChainedRedirections(String currentUrl) {
        try {
            boolean isInvalidURL = Boolean.FALSE;
            boolean checkHttpsStatus = Boolean.FALSE;
//        if (!(currentUrl == null || currentUrl.equals(""))) {
            if (currentUrl != null && !currentUrl.trim().isEmpty()) {
                currentUrl = currentUrl.replaceAll("[\\n\\t\\s]", " ").replace("&nbsp;", " ").replace("&amp;", "&").trim();
                StringBuilder redirectedUrl = new StringBuilder("");
                String httpCurrentUrl = "";
                List<List<String>> cookieContainer = new ArrayList<>();
                int statusCode = 0;
                if (currentUrl.startsWith("http://")) {
                    httpCurrentUrl = currentUrl;
                    this.queryUrlStatus.setInputUrl(httpCurrentUrl);
                    statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer);
                } else if (!currentUrl.startsWith("https://")) {
                    httpCurrentUrl = "http://" + currentUrl;
                    this.queryUrlStatus.setInputUrl(httpCurrentUrl);
                    statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer);
                } else {
                    checkHttpsStatus = Boolean.TRUE;
                }

                if ((!redirectedUrl.toString().isEmpty() || statusCode == 200) && statusCode != 0) {
                    if (!redirectedUrl.toString().equals("")) {
                        this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                    } else {
                        this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                    }
                    /**/
                    String metaURL = getMetaContnetURL(this.queryUrlStatus.getDestinationURL());
                    if (metaURL != null) {
                        this.queryUrlStatus.setDestinationURL(appendRootDomain(this.queryUrlStatus.getDestinationURL(), metaURL));
                    }
                    /*Setting the Final Status code.*/
                    this.queryUrlStatus.setFinalStatusCode(statusCode);
                    this.queryUrlStatus.setStatus(UrlValidatorService.WORKING);
                    if (this.keyword != null && !this.keyword.equals("")) {
                        /*this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(redirectedUrl.toString(), this.keyword));*/
                        this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(this.queryUrlStatus.getDestinationURL(), this.keyword));
                    }
//                } else if ((statusCode >= 301 || statusCode <= 307) && statusCode != 0) {
                } else if (statusCode <= 307 && statusCode != 0) {
                    switch (statusCode) {
                        case HttpURLConnection.HTTP_MOVED_TEMP:
                            if (!redirectedUrl.toString().equals("")) {
                                this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                            } else {
                                this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                            }
                            /*Setting the Final Status code.*/
                            this.queryUrlStatus.setFinalStatusCode(statusCode);
                            this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_TEMPORARY_REDIRECTION);
//                            checkHttpsStatus = Boolean.TRUE;
                            break;
                        case HttpURLConnection.HTTP_MOVED_PERM:
                            if (!redirectedUrl.toString().equals("")) {
                                this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                            } else {
                                this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                            }
                            /*Setting the Final Status code.*/
                            this.queryUrlStatus.setFinalStatusCode(statusCode);
                            this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_PERMANENT_REDIRECTION);
//                            checkHttpsStatus = Boolean.TRUE;
                            break;
                        default:
//                            if (!redirectedUrl.toString().equals("")) {
//                                this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
//                            } else {
//                                this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
//                            }
//                            /*Setting the Final Status code.*/
//                            this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                            this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
//                            this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                            break;
                    }
                     checkHttpsStatus = Boolean.TRUE;
                } else if (statusCode != 200 && statusCode != 0) {
                    if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                        httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                        statusCode = getStatusCode(httpCurrentUrl, redirectedUrl, cookieContainer);
                        if (!redirectedUrl.toString().equals("") || statusCode == 200 && statusCode != 0) {
                            if (!redirectedUrl.toString().equals("")) {
                                this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                            } else {
                                this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                            }
                            /*Setting the Final Status code.*/
                            this.queryUrlStatus.setFinalStatusCode(statusCode);
                            this.queryUrlStatus.setStatus(UrlValidatorService.WORKING);
                            if (this.keyword != null && !this.keyword.equals("")) {
//                            this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(redirectedUrl.toString(), this.keyword));
                                this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(this.queryUrlStatus.getDestinationURL(), this.keyword));
                            }
//                        } else if (statusCode >= 301 || statusCode <= 307 && statusCode != 0) {
                        } else if (statusCode <= 307 && statusCode != 0) {
                            switch (statusCode) {
                                case HttpURLConnection.HTTP_MOVED_TEMP:
                                    if (!redirectedUrl.toString().equals("")) {
                                        this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                    } else {
                                        this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                                    }
                                    /*Setting the Final Status code.*/
                                    this.queryUrlStatus.setFinalStatusCode(statusCode);
                                    this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_TEMPORARY_REDIRECTION);
//                                    checkHttpsStatus = Boolean.TRUE;
                                    break;
                                case HttpURLConnection.HTTP_MOVED_PERM:
                                    if (!redirectedUrl.toString().equals("")) {
                                        this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                    } else {
                                        this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
                                    }
                                    /*Setting the Final Status code.*/
                                    this.queryUrlStatus.setFinalStatusCode(statusCode);
                                    this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_PERMANENT_REDIRECTION);
//                                    checkHttpsStatus = Boolean.TRUE;
                                    break;
//                                default:
//                                    if (!redirectedUrl.toString().equals("")) {
//                                        this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
//                                    } else {
//                                        this.queryUrlStatus.setDestinationURL(httpCurrentUrl);
//                                    }
//                                    /*Setting the Final Status code.*/
//                                    this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                    this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
//                                    this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                    break;
                            }
                             checkHttpsStatus = Boolean.TRUE;
                        }
                    }
                }
                if ((checkHttpsStatus && statusCode > 200) || (checkHttpsStatus && statusCode == 0)) {
                    statusCode = 0;
                    redirectedUrl = new StringBuilder("");
                    String httpsCurrentUrl = "";
                    if (currentUrl.startsWith("https://")) {
                        httpsCurrentUrl = currentUrl;
                        this.queryUrlStatus.setInputUrl(httpsCurrentUrl);
                        statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer);
                    } else if (!currentUrl.startsWith("http://")) {
                        httpsCurrentUrl = "https://" + currentUrl;
                        this.queryUrlStatus.setInputUrl(httpsCurrentUrl);
                        statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer);
                    }
                    if (!redirectedUrl.toString().equals("") || statusCode == 200) {
                        if (!redirectedUrl.toString().equals("")) {
                            this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                        } else {
                            this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                        }
                        /*Setting the Final Status code.*/
                        this.queryUrlStatus.setFinalStatusCode(statusCode);
                        this.queryUrlStatus.setStatus(UrlValidatorService.WORKING);
                        if (this.keyword != null && !this.keyword.equals("")) {
                            //this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(redirectedUrl.toString(), this.keyword));
                            this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(this.queryUrlStatus.getDestinationURL(), this.keyword));
                        }
//                    } else if ((statusCode >= 301 || statusCode <= 307) && statusCode != 0) {
                    } else if (statusCode <= 307 && statusCode != 0) {
                        switch (statusCode) {
                            case HttpURLConnection.HTTP_MOVED_TEMP:
                                if (!redirectedUrl.toString().equals("")) {
                                    this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                } else {
                                    this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                }
                                /*Setting the Final Status code.*/
                                this.queryUrlStatus.setFinalStatusCode(statusCode);
                                this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_TEMPORARY_REDIRECTION);
                                break;
                            case HttpURLConnection.HTTP_MOVED_PERM:
                                if (!redirectedUrl.toString().equals("")) {
                                    this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                } else {
                                    this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                }
                                /*Setting the Final Status code.*/
                                this.queryUrlStatus.setFinalStatusCode(statusCode);
                                this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_PERMANENT_REDIRECTION);
                                break;
//                            default:
//                                if (!redirectedUrl.toString().equals("")) {
//                                    this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
//                                } else {
//                                    this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
//                                }
//                                /*Setting the Final Status code.*/
//                                this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
//                                this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                break;
                        }
                    } else if (statusCode != 200) {
                        if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                            httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                            statusCode = getStatusCode(httpsCurrentUrl, redirectedUrl, cookieContainer);
                            if (!redirectedUrl.toString().equals("") || statusCode == 200) {
                                if (!redirectedUrl.toString().equals("")) {
                                    this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                } else {
                                    this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                }
                                /*Setting the Final Status code.*/
                                this.queryUrlStatus.setFinalStatusCode(statusCode);
                                this.queryUrlStatus.setStatus(UrlValidatorService.WORKING);
                                if (this.keyword != null && !this.keyword.equals("")) {
                                    /*this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(redirectedUrl.toString(), this.keyword));*/
                                    this.queryUrlStatus.setKeywordCount(Common.calculateKeywordCount(this.queryUrlStatus.getDestinationURL(), this.keyword));
                                }
//                            } else if ((statusCode >= 301 || statusCode <= 307) && statusCode != 0) {
                            } else if (statusCode <= 307 && statusCode != 0) {
                                switch (statusCode) {
                                    case HttpURLConnection.HTTP_MOVED_TEMP:
                                        if (!redirectedUrl.toString().equals("")) {
                                            this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                        } else {
                                            this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                        }
                                        /*Setting the Final Status code.*/
                                        this.queryUrlStatus.setFinalStatusCode(statusCode);
                                        this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_TEMPORARY_REDIRECTION);
                                        break;
                                    case HttpURLConnection.HTTP_MOVED_PERM:
                                        if (!redirectedUrl.toString().equals("")) {
                                            this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                        } else {
                                            this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                        }
                                        /*Setting the Final Status code.*/
                                        this.queryUrlStatus.setFinalStatusCode(statusCode);
                                        this.queryUrlStatus.setStatus(UrlValidatorService.WORKING_PERMANENT_REDIRECTION);
                                        break;
//                                    default:
//                                        if (!redirectedUrl.toString().equals("")) {
//                                            this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
//                                        } else {
//                                            this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
//                                        }
//                                        /*Setting the Final Status code.*/
//                                        this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                        this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                        this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
//                                        break;
                                }
                            } else {
                                if (!redirectedUrl.toString().equals("")) {
                                    this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                                } else {
                                    this.queryUrlStatus.setDestinationURL(httpsCurrentUrl);
                                }
                                /*Setting the Final Status code.*/
                                this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
//                                this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
                                this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
                            }
                        } else {
                            isInvalidURL = true;
                        }
                    } else {
                        isInvalidURL = true;
                    }

                    if (isInvalidURL) {
                        /*Setting the Final Status code.*/
                        if (!redirectedUrl.toString().equals("")) {
                            this.queryUrlStatus.setDestinationURL(redirectedUrl.toString());
                        } else {
                            this.queryUrlStatus.setDestinationURL(currentUrl);
                        }
                        /*Setting the Final Status code.*/
//                        this.queryUrlStatus.setFinalStatusCode(statusCode);
                        this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
//                        this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
                        this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
                    }
                }
            }
        } catch (Exception e) {
            this.queryUrlStatus.setInputUrl(currentUrl);
            this.queryUrlStatus.setStatus(UrlValidatorService.FAILED_TO_CHECK);
            this.queryUrlStatus.setStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
            this.queryUrlStatus.setFinalStatusCode(UrlValidatorService.FAILED_STATUS_CODE);
            LOGGER.error("Exception in fecthing URLStats for query URL:: " + currentUrl + ", cause:: " + e);
        }
    }

    public int getStatusCode(String currentUrl, StringBuilder redirectedUrl, List<List<String>> cookieContainer) {
        int responseCode = 0;
        HttpURLConnection currentUrlConn;
        int iteration = 0;
        LinkedHashMap<String, Integer> chainedRedirection = new LinkedHashMap<>();
        boolean isCertUpdated = Boolean.FALSE;
        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 5) {
            try {
                URL currentUrlObj = new URL(currentUrl);
                currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
                currentUrlConn.setInstanceFollowRedirects(Boolean.TRUE);
                currentUrlConn.setConnectTimeout(IMPORTANT_CONNECT_TIME_OUT);
                currentUrlConn.setReadTimeout(IMPORTANT_READ_TIME_OUT);
                currentUrlConn.setRequestProperty("User-Agent", UrlValidatorService.USER_AGENT);
                if (cookieContainer != null && cookieContainer.size() > 0
                        && cookieContainer.get(0) != null && cookieContainer.get(0).size() > 0) {
                    for (String cookie : cookieContainer.get(0)) {
                        currentUrlConn.addRequestProperty("Cookie", cookie);
                    }
                }
                responseCode = currentUrlConn.getResponseCode();
                List<String> tempCookies = currentUrlConn.getHeaderFields().get("Set-Cookie");
                if (tempCookies != null && tempCookies.size() > 0 && cookieContainer != null) {
                    if (cookieContainer.size() > 0) {
                        cookieContainer.set(0, tempCookies);
                    } else {
                        cookieContainer.add(tempCookies);
                    }
                }
                LOGGER.debug("CurrentUrl: " + currentUrl + ", responseCode: " + responseCode + ", iteration: " + iteration);
                if (iteration != 0) {
                    chainedRedirection.put(currentUrl, responseCode);
                } else if (iteration == 0) {
                    this.queryUrlStatus.setStatusCode(responseCode);
                }
                if (responseCode / 100 == 3) {
                    if (iteration != 4) {
                        String redirectedLocation = currentUrlConn.getHeaderField("Location");
                        String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
                        if (redirectedLocation != null) {
                            if (!(redirectedLocation.startsWith("http"))) {
                                if (redirectedLocation.startsWith("/")) {
                                    currentUrl = tempUrl + redirectedLocation;
                                } else if (!(redirectedLocation.startsWith("/"))) {
                                    if (currentUrl.endsWith("/")) {
                                        currentUrl = currentUrl + redirectedLocation;
                                    } else {
                                        currentUrl = currentUrl + "/" + redirectedLocation;
                                    }
                                }
                            } else {
                                currentUrl = redirectedLocation;
                            }
                        }
                    } else {
                        redirectedUrl.append("");
                    }
                } else if (responseCode == 200 && iteration != 0) {
                    if (redirectedUrl.toString().equals("")) {
                        redirectedUrl.append(currentUrl);
                    } else if (!redirectedUrl.toString().equals("")) {
                        redirectedUrl = new StringBuilder("");
                        redirectedUrl.append(currentUrl);
                    }
                }
            } catch (SSLException e) {
                LOGGER.error("SSLException produced by failed SSL-related operation in getStatusCode when connection to " + currentUrl + ", Cause: " + e);
                /*To update the SSL certification for a website/ URL */
                if (!isCertUpdated) {
                    try {
                        isCertUpdated = Common.sslValidation(currentUrl);
                        LOGGER.info("isCertUpdated to handle the SSLProtocolException for the URL:: " + currentUrl + ", and status is:: " + isCertUpdated);
                    } catch (Exception ex) {
                        isCertUpdated = Boolean.TRUE;
                        LOGGER.error("Exception in creating SSL | PKIX certification: " + currentUrl + ", terminating the status Cause:: " + ex);
                    }
                }
            } catch (IOException e) {
                /*
                -> java.net.NoRouteToHostException: No route to host 
                -> java.net.SocketTimeoutException: Read timed out */
                LOGGER.error("IOException in getStatusCode when connection to " + currentUrl + ", at iteration:: " + iteration + ", Cause: " + e);
                /*Skip the actually iterations of current thread to get status code*/
                iteration = iteration + 3;
            } catch (Exception e) {
                LOGGER.error("Exception in getStatusCode when connection to " + currentUrl + ", at iteration:: " + iteration + ", Cause: " + e);
            }
            iteration++;
        }
        this.queryUrlStatus.setChainedRedirectsMap(chainedRedirection);
        return responseCode;
    }

    /*Returns URl from meta content for query URL.*/
    public String getMetaContnetURL(String queryURL) {
        String metaUrl = null;
        Document document = null;
        Elements refresh = null;
        try {
            if (queryURL != null && !queryURL.isEmpty()) {
                document = Jsoup.connect(queryURL).get();
            }
            if (document != null) {
                refresh = document.head().select("meta[http-equiv=refresh]");
            }
            if (refresh != null && !refresh.isEmpty()) {
                Element element = refresh.get(0);
                String content = null;
                Pattern pattern = null;
                Matcher matcher = null;
                if (element != null && element.hasAttr("content")) {
                    content = element.attr("content");
                }
                // split the content here
                pattern = Pattern.compile("^.*URL=(.+)$", Pattern.CASE_INSENSITIVE);
                if (content != null && !content.trim().isEmpty()) {
                    matcher = pattern.matcher(content);
                }
                if (matcher != null && matcher.matches() && matcher.groupCount() > 0) {
                    metaUrl = matcher.group(1);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getMetaContnetURL: ", e);
        }
        return (metaUrl != null && !metaUrl.trim().isEmpty() ? metaUrl.trim() : null);
    }

    public String appendRootDomain(String rootDomainURL, String subDomainURL) {
        if (rootDomainURL != null && subDomainURL != null) {
            if (rootDomainURL.endsWith("/") && subDomainURL.startsWith("/")) {
                rootDomainURL = rootDomainURL.substring(0, rootDomainURL.length() - 1);
                rootDomainURL = rootDomainURL.concat(subDomainURL);
            } else {
                rootDomainURL = rootDomainURL.concat(subDomainURL);
            }
        }
        return rootDomainURL;
    }
}
