package lxr.marketplace.urlvalidator.controller;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.session.Session;
import lxr.marketplace.urlvalidator.domain.UrlStatus;
import lxr.marketplace.urlvalidator.domain.UrlValidator;
import lxr.marketplace.urlvalidator.service.UrlValidatorService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class UrlValidatorController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(UrlValidatorController.class);
    //UrlValidatorService urlValidatorService;
    String uploadFolder;
    String downloadFolder;
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    LoginService loginService;
    private int toolAccessLimit = 100;

    @Autowired
    private UrlValidatorService urlValidatorService;

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public UrlValidatorController() {
        setCommandClass(UrlValidator.class);
        setCommandName("urlValidator");
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {

        HttpSession session = request.getSession();
        ArrayList<UrlStatus> urlStatusList;

        UrlValidator urlValidator = (UrlValidator) command;
        String existingColumn = "";
        /*
		 * executed if upload button is clicked. It uploads and saves the file
		 * on disk
         */
        urlValidatorService = new UrlValidatorService();
        Login user = (Login) session.getAttribute("user");
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(1);
            toolObj.setToolName("URL Auditor");
            toolObj.setToolLink("url-auditor-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.URL_AUDITOR);
            /* Applying the tool access limit filter for local user */
            toolAccessLimit = (user.isIslocal() && !user.isDeleted()) ? Common.LOCAL_USER_TOOL_LIMIT : Common.TOOL_ACCESS_LIMIT;
            if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
                session.removeAttribute("columnList");
                MultipartFile multipartFile = urlValidator.getUploadedFile();
                String fileName = Common.saveFile(multipartFile, uploadFolder);
                ArrayList<String> columns = urlValidatorService.getListByOption(uploadFolder.concat(fileName), "columnList", 0, true, toolAccessLimit);
                urlValidator.setSelFile(fileName);
                urlValidator.setColumnList(columns);
                session.setAttribute("columnList", columns);
                urlValidator.setManaullyURL(new StringBuffer(""));
                session.setAttribute("urlValidator", urlValidator);
                logger.debug("Uploading of file finished at : " + Calendar.getInstance().getTime());
                urlValidatorService.readFile(columns, request, response, toolAccessLimit);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "getResult")) {
                boolean manualAnalysis = false;
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    if (session.getAttribute("manualAnalysis") != null) {
                        manualAnalysis = (boolean) session.getAttribute("manualAnalysis");
                    }
                    UrlValidator urlValidatorOld = (UrlValidator) session.getAttribute("urlValidator");
                    if (urlValidatorOld == null) {
                        urlValidatorOld = urlValidator;
                    }
                    ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", urlValidatorOld);
                    if (urlValidatorOld != null 
                            && ((urlValidatorOld.getUrlStatusList()!= null && urlValidatorOld.getUrlStatusList().isEmpty()) ||
                            (urlValidatorOld.getUrlStatusList() == null))) {
                        removeSessionObjects(session);
                        mAndV.addObject("toolAccessLimit", toolAccessLimit);
                        mAndV.addObject("result", "hide");
                        return mAndV;
                    } else {
                        mAndV.addObject("manual", manualAnalysis);
                        mAndV.addObject("result", "show");
                        mAndV.addObject("oldFile", "same");
                        mAndV.addObject("toolAccessLimit", toolAccessLimit);
                    }
                    return mAndV;
                }
                int selCol = 0;
                ArrayList<String> columns;
                String selectedColName = null;
                String keyword = urlValidator.getKeyword();
                UrlValidator urlValidatorOld = (UrlValidator) session.getAttribute("urlValidator");
                Session userSession = (Session) session.getAttribute("userSession");
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
                if (WebUtils.hasSubmitParameter(request, "column")) {
                    existingColumn = WebUtils.findParameterValue(request, "column");
                }
                if (urlValidatorOld == null) {
                    urlValidatorOld = new UrlValidator();
                }
                if (!(existingColumn.trim().equals("")) && (urlValidatorOld.getSelFile() != null && !urlValidatorOld.getSelFile().equals("")) && (urlValidator.getDestUrlCol() != null && !urlValidator.getDestUrlCol().equals(""))) {
                    boolean headerRow = urlValidator.isHeaderRow();
                    String selFile = urlValidatorOld.getSelFile();
                    String selColum = urlValidator.getDestUrlCol();
                    urlValidator.setDestUrlCol(selColum);
                    logger.debug("SelCol: " + selCol + ", headerRow: " + headerRow + ", keyword:" + keyword);
                    urlStatusList = urlValidatorService.getUrlList(uploadFolder.concat(selFile), selColum, keyword, headerRow, toolAccessLimit);
                    if (urlStatusList != null && !urlStatusList.isEmpty()) {
                        urlValidatorOld.setUrlStatusList(urlStatusList);
                        urlValidatorOld.setHeaderRow(headerRow);
                        session.removeAttribute("selCol");
                        selCol = Integer.valueOf(urlValidator.getDestUrlCol());
                        session.setAttribute("selCol", selCol);
                        //New Code
                        if (session.getAttribute("columnList") != null) {
                            columns = (ArrayList<String>) session.getAttribute("columnList");
                            if (columns.size() > selCol) {
                                selectedColName = columns.get(selCol);
                                logger.debug("Selected coloum:  " + selectedColName);
                            }
                        }
                        session.setAttribute("urlAuditiorStatusList", urlStatusList);
                    }
                } else {
                    urlStatusList = urlValidatorService.getUrlStatus(urlValidator, toolAccessLimit);
                    if (urlStatusList != null && !urlStatusList.isEmpty()) {
                        session.setAttribute("urlAuditiorStatusList", urlStatusList);
                        urlValidatorOld.setManaullyURL(urlValidator.getManaullyURL());
                        urlValidatorOld.setKeyword(urlValidator.getKeyword());
                        urlValidatorOld.setUrlStatusList(urlStatusList);
                        manualAnalysis = true;
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", urlValidatorOld);
                boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
                if (urlStatusList != null && !urlStatusList.isEmpty()) {
                    /*Fetching the count of working, non working and total URL's. */
                    urlValidatorService.getCounts(urlStatusList, urlValidatorOld);
                    /*Setting result object of command class in session.*/
                    session.setAttribute("urlValidator", urlValidatorOld);

                    if (urlStatusList.size() >= Common.LOCAL_USER_TOOL_LIMIT && userLoggedIn) {
                        String dwnfileName = urlValidatorService.generateExcelesFile(urlStatusList, downloadFolder, urlValidatorOld.getKeyword(), urlValidatorOld);
                        Common.sendReportThroughMail(request, downloadFolder, dwnfileName, user.getUserName(), toolObj.getToolName());
                        logger.info("Auto sending this tool analysis report through mail to user email: " + user.getUserName());
                    }
                    mAndV.addObject("result", "show");
                    mAndV.addObject("selCol", selCol);
                } else {
                    mAndV.addObject("result", "hide");
                    mAndV.addObject("clearAll", "show");
                }
                mAndV.addObject("manual", manualAnalysis);
                mAndV.addObject("toolAccessLimit", toolAccessLimit);
                /*mAndV.addObject("oldFile", "same");*/
                if (selectedColName != null) {
                    mAndV.addObject("selectedColName", selectedColName);
                }
                session.setAttribute("manualAnalysis", manualAnalysis);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                /* Executed for download request event. It downloads the file with results*/
                String fileType = "", fileName = "";
                UrlValidator urlValidatorOld = (UrlValidator) session.getAttribute("urlValidator");
                ArrayList<UrlStatus> urlList = (ArrayList<UrlStatus>) session.getAttribute("urlAuditiorStatusList");
                boolean isFileGenerated = false;
                if (urlValidatorOld != null && urlList != null) {
                    String searchTerm = urlValidatorOld.getKeyword();
                    String format = WebUtils.findParameterValue(request, "download");
                    if (format.equalsIgnoreCase("xls")) {
                        fileType = "";
                        fileName = urlValidatorService.createExcelFile(urlList, downloadFolder, searchTerm, urlValidatorOld);
                    } else if (format.equalsIgnoreCase("xlsx")) {
                        fileType = "xlsx";
                        fileName = urlValidatorService.generateExcelesFile(urlList, downloadFolder, searchTerm, urlValidatorOld);
                    } else if (format.equalsIgnoreCase("csv")) {
                        fileType = "csv";
                        fileName = urlValidatorService.createCsvFile(urlList, downloadFolder, searchTerm, urlValidatorOld);
                    } else if (format.equalsIgnoreCase("tsv")) {
                        fileType = "tsv";
                        fileName = urlValidatorService.createTsvFile(urlList, downloadFolder, searchTerm, urlValidatorOld);
                    } else {
                        fileType = "";
                        fileName = urlValidatorService.createExcelFile(urlList, downloadFolder, searchTerm, urlValidatorOld);
                    }
                    String downParam = request.getParameter("download");
                    if (fileName != null && !fileName.equals("")) {
                        isFileGenerated = true;
                        String justFileName = fileName.substring(downloadFolder.length());
                        if (downParam.equalsIgnoreCase("'sendmail'")) {
                            logger.info("Sending report through mail");
                            String toAddrs = request.getParameter("email");
                            String toolName = "URL Auditor";
                            Common.sendReportThroughMail(request, downloadFolder, justFileName, toAddrs, toolName);
                            return null;
                        } else if (!fileName.equals("")) {
                            Common.downloadReport(response, downloadFolder, justFileName, fileType);
                        }
                        return null;
                    }
                }
                if (!isFileGenerated) {
                    ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", urlValidator);
                    mAndV.addObject("result", "hide");
                    mAndV.addObject("clearAll", "show");
                    mAndV.addObject("toolAccessLimit", toolAccessLimit);
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                removeSessionObjects(session);
                ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", urlValidator);
                mAndV.addObject("result", "hide");
                mAndV.addObject("clearAll", "show");
                mAndV.addObject("toolAccessLimit", toolAccessLimit);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "clearResults")) {
                removeSessionObjects(session);
                JSONArray arr = null;
                PrintWriter writer = null;
                try {
                    writer = response.getWriter();
                    arr = new JSONArray();
                    arr.add("removed");
                    writer.print(arr);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    writer.print("{success: false}");
                } finally {
                    writer.flush();
                    writer.close();

                }
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                boolean manualAnalysis = false;
                if (session.getAttribute("manualAnalysis") != null) {
                    manualAnalysis = (boolean) session.getAttribute("manualAnalysis");
                }
                UrlValidator urlValidatorOld = (UrlValidator) session.getAttribute("urlValidator");
                if (urlValidatorOld == null) {
                    urlValidatorOld = urlValidator;
                }
                int selCol = 0;
                if (session.getAttribute("selCol") != null) {
                    selCol = (int) session.getAttribute("selCol");
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", urlValidatorOld);
                mAndV.addObject("manual", manualAnalysis);
                mAndV.addObject("result", "show");
                mAndV.addObject("selCol", selCol);
                return mAndV;
            } else {
                removeSessionObjects(session);
                ModelAndView mAndV = new ModelAndView(getFormView(), "urlValidator", new UrlValidator());
                mAndV.addObject("toolAccessLimit", toolAccessLimit);
                mAndV.addObject("result", "hide");
                return mAndV;
            }
        } else {
            removeSessionObjects(session);
            ModelAndView mAndV = new ModelAndView("urlvalidator/UrlDescription", "urlStatusList", null);
            mAndV.addObject("toolAccessLimit", toolAccessLimit);
            mAndV.addObject("result", "hide");
            return mAndV;
        }
        removeSessionObjects(session);
        ModelAndView mAndV = new ModelAndView("urlvalidator/UrlDescription", "urlStatusList", null);
        mAndV.addObject("toolAccessLimit", toolAccessLimit);
        mAndV.addObject("result", "hide");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        UrlValidator urlValidatorForm = (UrlValidator) session.getAttribute("urlValidator");
        if (urlValidatorForm == null) {
            urlValidatorForm = new UrlValidator();
        }
        return urlValidatorForm;

    }

    @Override
    protected Map<String, Map<Integer, String>> referenceData(HttpServletRequest request) throws Exception {
        Map<String, Map<Integer, String>> referenceData = new HashMap<>();
        // Data referencing for column dropdown box
        HttpSession session = request.getSession();
        Map<Integer, String> column = new LinkedHashMap<>();
        UrlValidator urlValidatorForm = (UrlValidator) session.getAttribute("urlValidator");
        if (urlValidatorForm != null) {
            ArrayList<String> columnList = (ArrayList<String>) urlValidatorForm.getColumnList();
            if (columnList != null) {
                for (int i = 0; i < columnList.size(); i++) {
                    column.put(i, columnList.get(i));
                }
            }
            referenceData.put("columnList", column);
        }
        return referenceData;
    }

    public void removeSessionObjects(HttpSession session) {
        session.removeAttribute("urlValidator");
        session.removeAttribute("urlAuditiorStatusList");
        session.removeAttribute("columnList");
        session.removeAttribute("manualAnalysis");
        session.removeAttribute("selCol");
        session.setAttribute("urlValidator", null);
    }
}
