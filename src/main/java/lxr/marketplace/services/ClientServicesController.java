package lxr.marketplace.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.user.Login;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class ClientServicesController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(ClientServicesController.class);

    private String supportMail;
    private String udayanId;

    public void setSupportMail(String supportMail) {
        this.supportMail = supportMail;
    }

    public void setUdayanId(String udayanId) {
        this.udayanId = udayanId;
    }

    public ClientServicesController() {
        setCommandClass(Services.class);
        setCommandName("services");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Services services = (Services) command;
        Login user = (Login) session.getAttribute("user");
        boolean messageSent;
        if (user != null) {
            session.setAttribute("service", services);
            if (WebUtils.hasSubmitParameter(request, "submit")) {
                String subject = EmailBodyCreator.subjectOnClientServices;
                String bodyText = EmailBodyCreator.bodyForClientServices(services);
                messageSent = SendMail.sendMail(supportMail, supportMail, udayanId, subject, bodyText);
                ModelAndView mAndV = new ModelAndView(getFormView(), "services", services);
                if (messageSent) {
                    String subjectLine = EmailBodyCreator.subjectOnClientServices;
                    String bodyTextLine = EmailBodyCreator.bodyForClient();
                    SendMail.sendMail(supportMail, services.getEmail(), subjectLine, bodyTextLine);
                    session.removeAttribute("messageSent");
                    session.setAttribute("messageSent", messageSent);
                    mAndV.addObject("Mailsent", "Your information was sent successfully.\n We will get back to you shortly.");
                }
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "cancel") || request.getParameter("cancel") != null) {
                session.removeAttribute("service");
                session.removeAttribute("messageSent");
                ModelAndView mAndV = new ModelAndView(getFormView(), "services", services);
                return mAndV;
            }
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "services", services);
        return mAndV;

    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        Services serviceForm = (Services) session.getAttribute("service");
        if (serviceForm == null) {
            serviceForm = new Services();
        }
        return serviceForm;
    }

}
