package lxr.marketplace.services;

public class Services {
	
	String name;
	String title;
	String company;
	String url;
	String email;
	String phoneNo; 
	String monSemSpend;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getMonSemSpend() {
		return monSemSpend;
	}
	public void setMonSemSpend(String monSemSpend) {
		this.monSemSpend = monSemSpend;
	}
	
}
