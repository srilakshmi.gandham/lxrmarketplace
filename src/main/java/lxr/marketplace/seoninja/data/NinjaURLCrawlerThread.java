package lxr.marketplace.seoninja.data;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import crawlercommons.robots.BaseRobotRules;

public class NinjaURLCrawlerThread implements Runnable{
	private static Logger logger = Logger.getLogger(NinjaURLCrawlerThread.class);
	private long domainId;
	private NinjaURL ninjaURL;
	private URLStatsService urlStatsService;
	private NinjaWebStats ninjaWebStats;
	private static BaseRobotRules baseRobotRules;
	
	Set<NinjaURL> crawlingURLs;
	Set<NinjaURL> urlQueue;

	public void setUrlStatsService(URLStatsService urlStatsService) {
		this.urlStatsService = urlStatsService;
	}
	
	public void setNinjaURL(NinjaURL ninjaURL) {
		this.ninjaURL = ninjaURL;
	}
	public static BaseRobotRules getBaseRobotRules() {
		return baseRobotRules;
	}

	public static void setBaseRobotRules(BaseRobotRules baseRobotRules) {
		NinjaURLCrawlerThread.baseRobotRules = baseRobotRules;
	}

	public NinjaURLCrawlerThread(long domainId, NinjaURL ninjaURL, URLStatsService urlStatsService, 
			Set<NinjaURL> crawlingURLs, Set<NinjaURL> urlQueue, NinjaWebStats ninjaWebStats) {
		super();
		this.domainId = domainId;
		this.ninjaURL = ninjaURL;
		this.urlStatsService = urlStatsService;
		this.crawlingURLs = crawlingURLs;
		this.urlQueue = urlQueue;
		this.ninjaWebStats = ninjaWebStats;
	}

//	@Override
	public void run() {
		synchronized(ninjaURL){
			ninjaURL.setCrawlStatus(NinjaURL.CONNECTING);
			ninjaURL.notifyAll();
		}
		
		//check redirection, based on redirected url.. 
		//if not an internal url
			//no need to process
			//change its status as completed
		//if new redirected url already in crawling url list, 
			//no need to process
			//change its status as completed
		//otherwise add another ninjaURL with redirected URL and process. 
			//change status to completed for original url
		
		//if it is not redirected, change its status to processing from here
		StringBuilder pageHTML = new StringBuilder();
		String statusMessage = NinjaURLCrawlerUtil.checkURLAndGetHTML(ninjaURL, pageHTML);
    	if(!statusMessage.equals("too many redirection") && !statusMessage.equals("invalid") && 
    			!ninjaURL.getRedirectedURL().equals("")){
    		try {
				URI redirectedURI = new URI(ninjaURL.getRedirectedURL());
				if(redirectedURI.getPort() == 80){	//sometimes default port 80 appears in url after domain name
					String newRedirectedURL = ninjaURL.getRedirectedURL().replace(redirectedURI.getHost()+":80", 
							redirectedURI.getHost());
					ninjaURL.setRedirectedURL(newRedirectedURL);
				}
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
			boolean processDoc = true;
			boolean processRedirect = false;
			NinjaURL redirectedURL = null;
			if(!ninjaURL.getUrl().equals(ninjaURL.getRedirectedURL())){ //url is redirected
				String tempURL = ninjaURL.getUrl();
				String tempRedirectedURL = ninjaURL.getRedirectedURL();
				if(tempURL.startsWith("http://")){
					tempURL = tempURL.replace("http://", "");
				}else if(tempURL.startsWith("https://")){
					tempURL = tempURL.replace("https://", "");
				}
				
				if(tempRedirectedURL.startsWith("http://")){
					tempRedirectedURL = tempRedirectedURL.replace("http://", "");
				}else if(tempRedirectedURL.startsWith("https://")){
					tempRedirectedURL = tempRedirectedURL.replace("https://", "");
				}
				if(!tempURL.equals(tempRedirectedURL)){				
					String currentDomain = "", redirectedDomain = ""; 
					try {
						currentDomain = Common.getDomainName(ninjaURL.getUrl());
						redirectedDomain = Common.getDomainName(ninjaURL.getRedirectedURL());
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
					if(!currentDomain.equals(redirectedDomain)){ //redirected url is from some other domain
						synchronized(ninjaURL){
							ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
							ninjaURL.notifyAll();
						}
						processDoc = false;
					}
					if(processDoc){ // redirected url is from same domain
						synchronized(crawlingURLs){
							synchronized(ninjaURL){
								ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
								ninjaURL.notifyAll();
							}
							redirectedURL = new NinjaURL(ninjaURL.getRedirectedURL());
							if(crawlingURLs.contains(redirectedURL)){	// redirected url already completed or being processed in threadpool						
								processDoc = false;
							}else{ //otherwise add to crawling list and process the redirected url 
								redirectedURL.setCrawlStatus(NinjaURL.CONNECTING);
								redirectedURL.setRedirectedURL(ninjaURL.getRedirectedURL());
								redirectedURL.setUrlStatus(Common.getURLStatus(ninjaURL.getRedirectedURL()));
								crawlingURLs.add(redirectedURL);
								processRedirect = true;
							}
							crawlingURLs.notifyAll();
						}
					}
				}
			}
			if(processDoc){ //url is not redirected or  url is redirected but not being processed by any other thread
				Document urlDoc = null;
				URLStats urlStats = null;
				List<NinjaURL> internalURLs = new ArrayList<NinjaURL>(); 
				if(processRedirect){ //for redirected urls
					urlStats = new URLStats(domainId, redirectedURL.getRedirectedURL());
					urlStats.setFetchDate(ninjaWebStats.getFetchdate());
					urlStats.setStatusCode(redirectedURL.getUrlStatus());
					if(pageHTML != null && !pageHTML.toString().equals("")){
						urlDoc = Jsoup.parse(pageHTML.toString());
					}else{
						urlDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedURL);	
					}
					if(urlDoc != null){						
						urlStatsService.fetchStats(urlDoc, urlStats, internalURLs, ninjaWebStats.getDomain(), getBaseRobotRules());
						urlStatsService.insertURLStatsinDB(urlStats);
						synchronized(urlQueue){
							urlQueue.addAll(internalURLs);
							urlQueue.notifyAll();
						}
					}
					synchronized(redirectedURL){
						redirectedURL.setCrawlStatus(NinjaURL.COMPLETED);
						redirectedURL.notifyAll();
					}					
				}else{
					urlStats = new URLStats(domainId, ninjaURL.getRedirectedURL());
					urlStats.setFetchDate(ninjaWebStats.getFetchdate());
					urlStats.setStatusCode(ninjaURL.getUrlStatus());
					if(pageHTML != null && !pageHTML.toString().equals("")){
						urlDoc = Jsoup.parse(pageHTML.toString());
					}else{
						urlDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedURL);	
					}
					if(urlDoc != null){						
						urlStatsService.fetchStats(urlDoc, urlStats, internalURLs, ninjaWebStats.getDomain(), getBaseRobotRules());
						urlStatsService.insertURLStatsinDB(urlStats);
						synchronized(urlQueue){
							urlQueue.addAll(internalURLs);
							urlQueue.notifyAll();
						}
					}
					synchronized(ninjaURL){
						ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
						ninjaURL.notifyAll();
					}					
				}			
			}
		}else{
			logger.info("URL with too many redirection or 404: " + ninjaURL.getUrl());
			URLStats urlStats = new URLStats(domainId, ninjaURL.getUrl());
			urlStats.setFetchDate(ninjaWebStats.getFetchdate());
			urlStats.setStatusCode(ninjaURL.getUrlStatus());
			urlStatsService.insertURLStatsinDB(urlStats);
			synchronized(ninjaURL){
				ninjaURL.setCrawlStatus(NinjaURL.COMPLETED);
				ninjaURL.notifyAll();
			}
		}
	}
}
