package lxr.marketplace.seoninja.data;

import java.util.Calendar;
import java.util.Comparator;

public class URLStats implements Comparable<URLStats>{
	private long urlStatsId;
	private long domainId;
	private String url;
	private Calendar fetchDate;
	private int statusCode;
	private boolean titlePresent;
	private boolean metaKeywordPresent;
	private boolean metaDescPresent;
	private String title;
	private String metaDescription;
	private String metaKeywords;
	private int altTextedImgCount;
	private int totalImageCount;
	private int hTagScore;
	private String h1Text;
	private int iframeCount;
	private int internalLinkCount;
	private int internalLinkScore;
	private long wordCount;
	private int wordScore;
	private int titleScore;
	private int metaKeyScore;
	private int metaDescScore;
	private int keywordInURLScore;
	private int seoFriendlyURLScore;
	
	public URLStats(long domainId, String url) {
		super();
		this.domainId = domainId;
		this.url = url;
	}

	public long getUrlStatsId() {
		return urlStatsId;
	}

	public void setUrlStatsId(long urlStatsId) {
		this.urlStatsId = urlStatsId;
	}

	public long getDomainId() {
		return domainId;
	}

	public void setDomainId(long domainId) {
		this.domainId = domainId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Calendar getFetchDate() {
		return fetchDate;
	}

	public void setFetchDate(Calendar fetchDate) {
		this.fetchDate = fetchDate;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public boolean isTitlePresent() {
		return titlePresent;
	}

	public void setTitlePresent(boolean titlePresent) {
		this.titlePresent = titlePresent;
	}

	public boolean isMetaKeywordPresent() {
		return metaKeywordPresent;
	}

	public void setMetaKeywordPresent(boolean metaKeywordPresent) {
		this.metaKeywordPresent = metaKeywordPresent;
	}

	public boolean isMetaDescPresent() {
		return metaDescPresent;
	}

	public void setMetaDescPresent(boolean metaDescPresent) {
		this.metaDescPresent = metaDescPresent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}
    
	public int getTotalImageCount() {
		return totalImageCount;
	}

	public int getAltTextedImgCount() {
		return altTextedImgCount;
	}

	public void setAltTextedImgCount(int altTextedImgCount) {
		this.altTextedImgCount = altTextedImgCount;
	}

	public void setTotalImageCount(int totalImageCount) {
		this.totalImageCount = totalImageCount;
	}

	public int gethTagScore() {
		return hTagScore;
	}

	public void sethTagScore(int hTagScore) {
		this.hTagScore = hTagScore;
	}

	public String getH1Text() {
		return h1Text;
	}

	public void setH1Text(String h1Text) {
		this.h1Text = h1Text;
	}

	public int getIframeCount() {
		return iframeCount;
	}

	public void setIframeCount(int iframeCount) {
		this.iframeCount = iframeCount;
	}

	public int getInternalLinkCount() {
		return internalLinkCount;
	}

	public void setInternalLinkCount(int internalLinkCount) {
		this.internalLinkCount = internalLinkCount;
	}

	public int getInternalLinkScore() {
		return internalLinkScore;
	}

	public void setInternalLinkScore(int internalLinkScore) {
		this.internalLinkScore = internalLinkScore;
	}

	public long getWordCount() {
		return wordCount;
	}

	public void setWordCount(long wordCount) {
		this.wordCount = wordCount;
	}

	public int getWordScore() {
		return wordScore;
	}

	public void setWordScore(int wordScore) {
		this.wordScore = wordScore;
	}

	public int getTitleScore() {
		return titleScore;
	}

	public void setTitleScore(int titleScore) {
		this.titleScore = titleScore;
	}

	public int getMetaKeyScore() {
		return metaKeyScore;
	}

	public void setMetaKeyScore(int metaKeyScore) {
		this.metaKeyScore = metaKeyScore;
	}

	public int getMetaDescScore() {
		return metaDescScore;
	}

	public void setMetaDescScore(int metaDescScore) {
		this.metaDescScore = metaDescScore;
	}

	public int getKeywordInURLScore() {
		return keywordInURLScore;
	}

	public void setKeywordInURLScore(int keywordInURLScore) {
		this.keywordInURLScore = keywordInURLScore;
	}

	public int getSeoFriendlyURLScore() {
		return seoFriendlyURLScore;
	}

	public void setSeoFriendlyURLScore(int seoFriendlyURLScore) {
		this.seoFriendlyURLScore = seoFriendlyURLScore;
	}

//	@Override
	public int compareTo(URLStats o) {
	    int sortVal1 = calculateSortVal(this);
        int sortVal2 = calculateSortVal(o);                     
        return sortVal2 - sortVal1; 	
	}
	
	public static Comparator<URLStats> StatusComparatorD = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
		    int sortVal1 = calculateSortVal(stat1);
		    int sortVal2 = calculateSortVal(stat2);		    		    
			return sortVal1 - sortVal2;	
		}
	};
	
	private static int calculateSortVal(URLStats urlStats){
	    int sortVal = 0;
	    if(urlStats.getStatusCode() == 408){
	        sortVal = 6;
        }else if((urlStats.getStatusCode() >= 400 && urlStats.getStatusCode() < 500)){
            sortVal = 7;
        }else if(urlStats.getStatusCode() >= 500 && urlStats.getStatusCode() < 600){
            sortVal = 5;
        }else if(urlStats.getStatusCode() == 0){
            sortVal = 4;
        }else if(urlStats.getStatusCode() == 301){
            sortVal = 2;
        }else if(urlStats.getStatusCode() >= 300 && urlStats.getStatusCode() < 400){
            sortVal = 3;
        }else{
            sortVal = 1;
        }
	    return sortVal;
	}
	public static Comparator<URLStats> URLComparatorU = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			return stat1.getUrl().compareTo(stat2.getUrl());
		}
	};
	public static Comparator<URLStats> URLComparatorD = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			return stat2.getUrl().compareTo(stat1.getUrl());
		}
	};
	
	public static Comparator<URLStats> TitleComparatorU = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getTitle() == null){
				stat1.setTitle("");
			}
			if(stat2.getTitle() == null){
				stat2.setTitle("");
			}
			return stat1.getTitle().compareTo(stat2.getTitle());
		}
	};
	public static Comparator<URLStats> TitleComparatorD = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getTitle() == null){
				stat1.setTitle("");
			}
			if(stat2.getTitle() == null){
				stat2.setTitle("");
			}
			return stat2.getTitle().compareTo(stat1.getTitle());
		}
	};
	
	public static Comparator<URLStats> MetaDescComparatorU = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getMetaDescription() == null){
				stat1.setMetaDescription("");
			}
			if(stat2.getMetaDescription() == null){
				stat2.setMetaDescription("");
			}
			return stat1.getMetaDescription().compareTo(stat2.getMetaDescription());
		}
	};
	public static Comparator<URLStats> MetaDescComparatorD = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getMetaDescription() == null){
				stat1.setMetaDescription("");
			}
			if(stat2.getMetaDescription() == null){
				stat2.setMetaDescription("");
			}
			return stat2.getMetaDescription().compareTo(stat1.getMetaDescription());
		}
	};
	
	public static Comparator<URLStats> MetaKeywordsComparatorU = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getMetaKeywords() == null){
				stat1.setMetaKeywords("");
			}
			if(stat2.getMetaKeywords() == null){
				stat2.setMetaKeywords("");
			}
			return stat1.getMetaKeywords().trim().compareTo(stat2.getMetaKeywords().trim());
		}
	};
	public static Comparator<URLStats> MetaKeywordsComparatorD = new Comparator<URLStats>() {
		public int compare(URLStats stat1, URLStats stat2) {
			if(stat1.getMetaKeywords() == null){
				stat1.setMetaKeywords("");
			}
			if(stat2.getMetaKeywords() == null){
				stat2.setMetaKeywords("");
			}
			return stat2.getMetaKeywords().trim().compareTo(stat1.getMetaKeywords().trim());
		}
	};
}
