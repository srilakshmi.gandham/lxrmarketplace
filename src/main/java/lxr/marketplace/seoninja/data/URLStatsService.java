package lxr.marketplace.seoninja.data;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lxr.marketplace.amazonpricetracker.Task;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import crawlercommons.robots.BaseRobotRules;

public class URLStatsService extends JdbcDaoSupport{
	private static Logger logger = Logger.getLogger(URLStatsService.class);
	public void fetchStats(Document document, URLStats urlStats, List<NinjaURL> internalURLs, String domain, BaseRobotRules baseRobotRules){
		if(document == null){
		    urlStats.setTitlePresent(false);
		    urlStats.setTitle("");
		    urlStats.setTitleScore(0);
		    urlStats.setMetaKeywordPresent(false); 
            urlStats.setMetaKeyScore(0);
            urlStats.setMetaKeywords("");
            urlStats.setMetaDescPresent(false);
            urlStats.setMetaDescription("");
            urlStats.setMetaDescScore(0);
            urlStats.setH1Text("");
            urlStats.sethTagScore(0);
            urlStats.setTotalImageCount(0);
            urlStats.setAltTextedImgCount(0);
            urlStats.setIframeCount(0);
            urlStats.setWordCount(0);
            urlStats.setWordScore(0);
            urlStats.setKeywordInURLScore(0);
            urlStats.setSeoFriendlyURLScore(checkSeoFriendlyURL(urlStats.getUrl())?100:0);
            urlStats.setInternalLinkCount(0);
            urlStats.setInternalLinkScore(0);
            return;		    
		}
	    //domainId, url, fetchDate, statusCode, titlePresent, title, titleScore, 
		//metaKeywordPresent, metaKeywords, metaDescScore, hTagScore, h1Text, wordScore, wordCount
		//iframeCount, totalImageCount, imgWithAltTextCount, internalLinkCount, internalLinkScore
//		logger.info("Fetching stats for " + urlStats.getUrl());
		domain = domain.trim();
		if(domain.startsWith("http://")){
			domain = domain.replace("http://", "");
		}else if(domain.startsWith("https://")){
			domain = domain.replace("https://", "");
		}
		
		if(domain.endsWith("/")){
			domain = domain.substring(0, domain.length() - 1);
		}
		
		Elements titleElement = document.select("head title");

		if(titleElement!= null && titleElement.first() != null && !titleElement.first().text().trim().equals("")){
			urlStats.setTitlePresent(true);
			String title = titleElement.first().text().trim();
			urlStats.setTitle(title);
			if(title.length() < 110){
				urlStats.setTitleScore((int) Math.round(50 * (Math.sin((title.length() - 33)
					/ (double) 21)) + 50));
			}else{
				urlStats.setTitleScore(25);
			}
		}else{
			urlStats.setTitleScore(0);
		}
		
		/*
		 * For Meta Keywords and Meta Description we are not checking the particular tag is inside head 
		 * tag or not. We found certain site, where meta description is inside body tag but when viewed
		 * sorce from browser it is coming perfectly
		 */
		String [] keywords = null;
		Elements metaKeywords = document.select("meta[name=keywords]");
		if(metaKeywords == null || metaKeywords.first() == null || metaKeywords.first().attr("content") == null){
			metaKeywords = document.select("meta[name=Keywords]");
		}
		if(metaKeywords == null || metaKeywords.first() == null || metaKeywords.first().attr("content") == null){
			metaKeywords = document.select("meta[http-equiv=keywords]");
		}
		if(metaKeywords == null || metaKeywords.first() == null || metaKeywords.first().attr("content") == null){
			metaKeywords = document.select("meta[http-equiv=Keywords]");
		}
		if(metaKeywords != null && metaKeywords.first() != null && metaKeywords.first().attr("content") != null 
				&& !metaKeywords.first().attr("content").trim().equals("")){
			urlStats.setMetaKeywordPresent(true); 
			urlStats.setMetaKeyScore(100);
			String metaKeys = metaKeywords.first().attr("content");
			urlStats.setMetaKeywords(metaKeys);
			keywords = metaKeys.split(",");
		}
		
		Elements metaDescription = document.select("meta[name=description]");
		if(metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null){
			metaDescription = document.select("meta[name=Description]");
		}
		if(metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null){
			metaDescription = document.select("meta[http-equiv=description]");
		}
		if(metaDescription == null || metaDescription.first() == null || metaDescription.first().attr("content") == null){
			metaDescription = document.select("meta[http-equiv=Description]");
		}
		if(metaDescription != null && metaDescription.first() != null && metaDescription.first().attr("content") != null 
				&& !metaDescription.first().attr("content").trim().equals("")){
			urlStats.setMetaDescPresent(true); 			
			String metaDesc = metaDescription.first().attr("content");
			urlStats.setMetaDescription(metaDesc);
			if(metaDesc.length() < 220){
				urlStats.setMetaDescScore((int) Math.round(100 * (Math.pow(Math.sin(metaDesc.length()/(double)96), 4))));
			}else{
				urlStats.setMetaDescScore(40);
			}
		}
		
		String h1Titles = fetchH1Titles(document);
		urlStats.setH1Text(h1Titles);
		urlStats.sethTagScore(calculateHTagScore(document, keywords));
																//total images, images with alt text
		Elements images = document.select("img");
		Elements imagesWithAlt = document.select("img[alt~=^(?!\\s*$).+]");
		urlStats.setTotalImageCount(images.size());
		urlStats.setAltTextedImgCount(imagesWithAlt.size());
																//iframe count
		Elements iframes = document.select("iframe");
		if(iframes != null){
			urlStats.setIframeCount(iframes.size());
		}
		
																//word count, word score
		Elements body = document.select("body");
		if(body != null && body.size() > 0){
			String bodyText = body.first().text();
			int wordCount = countWords(bodyText.trim());
			if(bodyText.trim().length() > 1){
				urlStats.setWordCount(wordCount);
			}else{
				wordCount = 0;
			}
			if(wordCount < 750){
				urlStats.setWordScore((int)Math.round(100 -100 * Math.pow(Math.cos(wordCount / (double)318), 4)));
			}else{
				urlStats.setWordScore((int)Math.round(100 - Math.log(Math.pow((wordCount - 727), 8))));
			}
		}
		
		urlStats.setKeywordInURLScore(checkKeywordInURL(keywords, urlStats.getUrl())?100:0);
		urlStats.setSeoFriendlyURLScore(checkSeoFriendlyURL(urlStats.getUrl())?100:0);
		
		Elements containedUrls = document.select("a[href]");
		if(containedUrls != null){
			for (Element link : containedUrls) {
				boolean dontAdd = false;
				String childUrl = link.attr("href").trim();
				if (childUrl.startsWith("#")) {
					dontAdd = true;
				} else {
					if (!childUrl.trim().equals("/") && !childUrl.trim().equals("")){
						childUrl = link.attr("abs:href");
					}else{
						dontAdd = true;
					}
				}
				if (!dontAdd) {
					try {
						childUrl = childUrl.replace(" ", "%20");
						URI childURI = new URI(childUrl);
						String urlDomain = childURI.getHost();
						if(childURI.getPort() == 80){
							childUrl = childUrl.replace(childURI.getHost()+":80", childURI.getHost());
						}
						if(urlDomain!= null && urlDomain.equals(domain)){
							NinjaURL childNinjaURL = new NinjaURL(childUrl, NinjaURL.TO_BE_ADDED);
							internalURLs.add(childNinjaURL);
						}
					} catch (URISyntaxException e) {
						logger.error("URISyntaxException in fetching URL Stats for " + childUrl + " Reason: "+e.getReason());
					}			
				}
			}
		}
		int internalLinkCount = internalURLs.size();
		urlStats.setInternalLinkCount(internalLinkCount);
		if(internalLinkCount < 250){
			int score = (int) Math.round(99 * Math.pow(Math.sin(internalLinkCount / (double)96), 2/(double)3));
			urlStats.setInternalLinkScore(score);
		}else{
			int score = (int) Math.round(63 * Math.pow(Math.E, -((internalLinkCount - 250)/(double)100)));
			urlStats.setInternalLinkScore(score);
		}
		Elements metaRefreshElements = document.select("head meta[http-equiv=refresh]");
		for(Element metaElement : metaRefreshElements){
			String refreshProp = metaElement.attr("content");
			if(refreshProp != null && !refreshProp.equals("")){
				String[] props = refreshProp.split(";\\s*");
				if(props.length > 0){
					for(String prop : props){
						if(prop.toLowerCase().contains("url")){
							String refreshLink = prop.replace("URL=", "").trim();
							String finalLink = refreshLink;
							if(finalLink != null && !finalLink.startsWith("http")){
								finalLink = "http://"+finalLink;
							}
							if(finalLink != null){
								boolean addLink = false;
								try{
									URL uri = new URL(finalLink);
									if(finalLink.contains(domain)){
										addLink = true;
									}else if(!uri.getHost().contains(".")){
										if(domain.endsWith("/") || refreshLink.startsWith("/")){
											finalLink = domain + refreshLink;
										}else{
											finalLink = domain  + "/" + refreshLink;
										}
										addLink = true;
									}
								}catch(Exception e){
									if(domain.endsWith("/") || refreshLink.startsWith("/")){
										finalLink = domain + refreshLink;
									}else{
										finalLink = domain  + "/" + refreshLink;
									}
									addLink = true;
								}
								if(addLink){
									NinjaURL refreshURL = new NinjaURL(finalLink, NinjaURL.TO_BE_ADDED);
									internalURLs.add(refreshURL);
								}
								break;
							}
						}
					}
				}
			}
			
		}
		NinjaURLCrawlerUtil.filterCrawlingURLs(internalURLs, baseRobotRules);
	}
	
	public boolean insertURLStatsinDB(final URLStats urlStats){
		boolean insertStatus = false;
		final String query = "INSERT INTO seo_url_stats (domain_id, url, date, status_code, has_title, " +
				"has_metakey, has_metadesc, title, metakey, metadesc, total_images_count, alttexted_image_count," +
				"h_tag_score, h1_texts, iframe_count, internal_link_count, internal_link_score, word_count," +
				"word_score, title_score, meta_key_score, meta_desc_score, keyword_in_url_score, seo_friendly_url_score) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			synchronized (this) {
				
				getJdbcTemplate().update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						PreparedStatement statement = con.prepareStatement(
								query, PreparedStatement.RETURN_GENERATED_KEYS);
						statement.setLong(1, urlStats.getDomainId());
						statement.setString(2, urlStats.getUrl());
						statement.setTimestamp(3, new java.sql.Timestamp(urlStats.getFetchDate().getTimeInMillis()));
						statement.setInt(4, urlStats.getStatusCode());
						statement.setBoolean(5, urlStats.isTitlePresent());
						statement.setBoolean(6, urlStats.isMetaKeywordPresent());
						statement.setBoolean(7, urlStats.isMetaDescPresent());
						if(urlStats.getTitle() == null || urlStats.getTitle().equals("")){
						    statement.setString(8, "");
                        }else{
                            try {
                            statement.setBytes(8, urlStats.getTitle().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(8, urlStats.getTitle());
                                e.printStackTrace();
                            }
                        }
						if(urlStats.getMetaKeywords() == null || urlStats.getMetaKeywords().equals("")){
						    statement.setString(9, "");
                        }else{
                            try {
                            statement.setBytes(9, urlStats.getMetaKeywords().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(9, urlStats.getMetaKeywords());
                                e.printStackTrace();
                            }
                        }
						
						if( urlStats.getMetaDescription() == null ||  urlStats.getMetaDescription().equals("")){
						    statement.setString(10, "");
                        }else{
                            try {
                            statement.setBytes(10, urlStats.getMetaDescription().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(10, urlStats.getMetaDescription());
                                e.printStackTrace();
                            }
                        }
//						 statement.setString(8, urlStats.getTitle());	
//						 statement.setString(9, urlStats.getMetaKeywords());
//						 statement.setString(10, urlStats.getMetaDescription());
						statement.setInt(11, urlStats.getTotalImageCount());
						statement.setInt(12, urlStats.getAltTextedImgCount());
						statement.setInt(13, urlStats.gethTagScore());
						if(urlStats.getH1Text() == null || urlStats.getH1Text().equals("")){
						    statement.setString(14, "");
						}else{
    						try {
                                statement.setBytes(14, urlStats.getH1Text().getBytes("UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                statement.setString(14, urlStats.getH1Text());
                                e.printStackTrace();
                            }
						}
//						statement.setString(14, urlStats.getH1Text());
						statement.setLong(15, urlStats.getIframeCount());
						statement.setLong(16, urlStats.getInternalLinkCount());
						statement.setLong(17, urlStats.getInternalLinkScore());
						statement.setLong(18, urlStats.getWordCount());
						statement.setLong(19, urlStats.getWordScore());
						statement.setLong(20, urlStats.getTitleScore());
						statement.setLong(21, urlStats.getMetaKeyScore());
						statement.setLong(22, urlStats.getMetaDescScore());
						statement.setLong(23, urlStats.getKeywordInURLScore());
						statement.setLong(24, urlStats.getSeoFriendlyURLScore());
						return statement;
					}
				}, keyHolder);
				urlStats.setUrlStatsId(keyHolder != null ? keyHolder.getKey().longValue() : 0);
			}
			insertStatus = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return insertStatus;
	}
	
	public boolean deletePreviousStats(Calendar currentday, long domainId){
		boolean deleted = false;
		String sql = "delete from seo_url_stats where date < ? and domain_id = ?";
		getJdbcTemplate().update(sql, new Object[]{new java.sql.Timestamp(currentday.getTimeInMillis()), domainId});
		return deleted;
	}
	
	private String fetchH1Titles(Document document){
		Elements h1Tags = document.select("h1");
		String h1Titles = "";
		if(h1Tags != null && h1Tags.size() > 0){
			for(Element h1Tag: h1Tags){
				if(!h1Tag.text().trim().matches("[\\p{Z}\\s]*")){
					h1Titles += h1Tag.text()+"<lxr>";
				}
			}
			if(h1Titles.length() >=5){
				h1Titles = h1Titles.substring(0, h1Titles.length() - 5);
			}
		}
		return h1Titles;
	}
	
	private int calculateHTagScore(Document document, String [] keywords){
		boolean h1Presence = false;
		boolean h2H3Presence = false;
		boolean keywordInH1 = false;
		boolean keywordInH2H3 = false;
		int hTagScore = 0;
		Elements h1Tags = document.select("h1");		
		if(h1Tags != null && h1Tags.size() > 0 ){
			for(Element h1Tag: h1Tags){
				if(!h1Tag.text().trim().equals("")){
					h1Presence = true;
					String h1Title = h1Tag.text().trim();
					keywordInH1 = presenceInKeywords(keywords, h1Title);					
				}
				if(keywordInH1){
					break;
				}
			}
		}
		Elements h2Tags = document.select("h2");		
		if(h2Tags != null && h2Tags.size() > 0){
			for(Element h2Tag: h2Tags){
				if(!h2Tag.text().trim().equals("")){
					h2H3Presence = true;
					String h2Title = h2Tag.text().trim();
					keywordInH2H3 = presenceInKeywords(keywords, h2Title);							
				}
				if(keywordInH2H3){
					break;
				}
			}
		}
		if(!keywordInH2H3){
			Elements h3Tags = document.select("h3");		
			if(h3Tags != null && h3Tags.size() > 0){
				for(Element h3Tag: h3Tags){
					if(!h3Tag.text().trim().equals("")){
						h2H3Presence = true;
						String h3Title = h3Tag.text().trim();
						keywordInH2H3 = presenceInKeywords(keywords, h3Title);											
					}
					if(keywordInH2H3){
						break;
					}
				}
			}
		}
		if(h1Presence){
			hTagScore += 30;
		}
		
		if(h2H3Presence){
			hTagScore += 20;
		}

		if(keywordInH2H3 && keywordInH1){
			hTagScore += 50;
		}else if(keywordInH1){
			hTagScore += 40;
		}
		return hTagScore;
	}
	
	private boolean presenceInKeywords(String [] keywords, String text){
		boolean present = false;
		if(keywords != null){
			for(String keyword: keywords){
				if(!keyword.trim().equals("")){
					text.toLowerCase().contains(keyword.toLowerCase());
					present = true;
					break;
				}
			}
		}
		return present;
	}
	
	private int countWords(String body){
	    logger.debug("BODY TEXT: " + body);
		String[] words = body.split("[\\p{Z}\\s]+");
		return words.length;
	}
	
	private boolean checkKeywordInURL(String [] keywords, String url){
		//TODO: needs more research on it, check with stop words
		boolean keywordPresence = false;	
		if(keywords != null){
			for(String keyword : keywords){
				if(!keyword.trim().equals("")){
					String []words = keyword.split("[\\p{Z}\\s]+"); 
					for(String word : words){
						if(!word.trim().equals("") && url.toLowerCase().contains(word.trim().toLowerCase())){
							keywordPresence = true;
							break;
						}
					}				
				}
				if(keywordPresence){
					break;
				}
			}
		}
		return keywordPresence;
	}
	
	private boolean checkSeoFriendlyURL(String url){		
		boolean seoFriendly = false;
		Pattern pattern = Pattern.compile("[\\p{L}\\p{N}]+-[\\p{L}\\p{N}]+");
		Matcher matcher = pattern.matcher(url);
		if(matcher.find()){
			seoFriendly = true;
		}
		return seoFriendly;
	}
	
    public List<Task> prevTaskForUser(long userId) {
        String query = "select task_id, user_id, domain_name, created_time_stamp, updated_time_stamp, status, result_file "
                + " from task where user_id = " + userId +" and result_file != \'Deleted\'";
        try {
            synchronized (this) {
                    List<Task> tasks = getJdbcTemplate().query(query, new RowMapper<Task>() {

                        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
                            Task task = new Task();

                            task.setId(rs.getLong("task_id"));                           
                            task.setUserId(rs.getLong("user_id"));
                            task.setDomain_name(rs.getString("domain_name"));
                            task.setResultFile(rs.getString("result_file"));
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(rs.getTimestamp("created_time_stamp").getTime());
                            task.setCreated_time_stamp(cal);
                            cal.setTimeInMillis(rs.getTimestamp("updated_time_stamp").getTime());
                            task.setUpdatedtimeStamp(cal);
                            task.setStatus(rs.getString("status"));
                            
                            return task;
                        }
                    });
                    return tasks;
                }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
	 
	public List<URLStats> fetchURLStats(final long domainId,final String domainName){
		final String query = "select url_stats_id, domain_id, url,date, status_code, has_title, " +
				"has_metakey, has_metadesc, title, metakey, metadesc, total_images_count, alttexted_image_count, " +
				"h_tag_score, h1_texts, iframe_count, internal_link_count, internal_link_score, word_count, " +
				"word_score, title_score, meta_key_score, meta_desc_score, keyword_in_url_score, " +
				"seo_friendly_url_score from seo_url_stats where domain_id=? ";
//		and group by domain_id
		logger.info("URL stats fetching query: "+query);
		logger.info("Domain Id : "+domainId);
		try{
			synchronized (this) {
				List<URLStats> urlStats1 = getJdbcTemplate().query(query,new Object[]{domainId},new RowMapper<URLStats>(){
					public URLStats mapRow(ResultSet rs, int rowNum) throws SQLException{
						URLStats urlStats = new URLStats(domainId, domainName);
						
								urlStats.setUrlStatsId(rs.getLong("url_stats_id"));
								urlStats.setDomainId(rs.getLong("domain_id"));
								urlStats.setUrl(rs.getString("url"));
								Calendar fetchDate = null;
								Timestamp fetchDateTime = rs.getTimestamp("date");
				                    if(fetchDateTime != null){
				                    	fetchDate = Calendar.getInstance();
				                    	fetchDate.setTimeInMillis(fetchDateTime.getTime());
				                    }
									urlStats.setFetchDate(fetchDate);
									urlStats.setStatusCode(rs.getInt("status_code"));
									urlStats.setTitlePresent(rs.getBoolean("has_title"));
									urlStats.setMetaKeywordPresent(rs.getBoolean("has_metakey"));
									urlStats.setMetaDescPresent(rs.getBoolean("has_metadesc"));
									urlStats.setTitle(rs.getString("title"));
									urlStats.setMetaKeywords(rs.getString("metakey"));
									urlStats.setMetaDescription(rs.getString("metadesc"));
									urlStats.setTotalImageCount(rs.getInt("total_images_count"));
									urlStats.setAltTextedImgCount(rs.getInt("alttexted_image_count"));
									urlStats.sethTagScore(rs.getInt("h_tag_score"));
									urlStats.setH1Text(rs.getString("h1_texts"));
									urlStats.setIframeCount(rs.getInt("iframe_count"));
									urlStats.setInternalLinkCount(rs.getInt("internal_link_count"));
									urlStats.setInternalLinkScore(rs.getInt("internal_link_score"));
									urlStats.setWordCount(rs.getLong("word_count"));
									urlStats.setWordScore(rs.getInt("word_score"));
									urlStats.setTitleScore(rs.getInt("title_score"));
									urlStats.setMetaKeyScore(rs.getInt("meta_key_score"));
									urlStats.setMetaDescScore(rs.getInt("meta_desc_score"));
									urlStats.setKeywordInURLScore(rs.getInt("keyword_in_url_score"));
									urlStats.setSeoFriendlyURLScore(rs.getInt("seo_friendly_url_score"));
								    
									return urlStats;
					}
				});
				return urlStats1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return null;		
	}
}
	

