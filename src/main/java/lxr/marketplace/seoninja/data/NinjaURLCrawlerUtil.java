package lxr.marketplace.seoninja.data;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import lxr.marketplace.util.Common;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import crawlercommons.robots.BaseRobotRules;
import crawlercommons.robots.SimpleRobotRulesParser;

public class NinjaURLCrawlerUtil {

    private static Logger logger = Logger.getLogger(NinjaURLCrawlerUtil.class);
    private static final String FAKE_ROBOTS_URL = "http://domain.com";

    public static Document createJsoupConnection(NinjaURL ninjaURL) {

        int timeout = 10000;
        int sllepOut = 2000;
        long timeoutIteration = 0;
        long noOfTime = 2;
        String userAgent = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
        /*Modified on oct-7 2015
	    			 * "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";*/
        Connection jsoupCon;
        Document document = null;
        do {
            timeoutIteration++;
            try {
                jsoupCon = Jsoup.connect(ninjaURL.getRedirectedURL());
                jsoupCon.timeout(timeout);
                jsoupCon.userAgent(userAgent);
                jsoupCon.followRedirects(true);
                document = jsoupCon.get();
                break;
            } catch (IOException ex) {
                logger.error("Exception to get URL Connection at iteration " + timeoutIteration + ". Trying to sleep. \nURL: " + ninjaURL.getRedirectedURL());
                try {
                    Thread.sleep(sllepOut * timeoutIteration);
                } catch (InterruptedException e) {
                    logger.error("Interrupted when slleping.. \nURL: " + ninjaURL.getRedirectedURL() + "  " + e.getMessage());
//						e.printStackTrace();
                }
            } catch (Exception e) {
                logger.error("Exception in fetching jsoup document for url " + ninjaURL.getUrl() + "\n Document: " + document + "  " + e.getMessage());
//					e.printStackTrace();
                break;
            }
        } while (timeoutIteration < noOfTime);
        return document;
    }

    public static String checkURL(NinjaURL ninjaURL) {
        String currentUrl = ninjaURL.getUrl();
        if (!(currentUrl.equals("") || currentUrl == null)) {
            StringBuilder redirectedUrl = new StringBuilder("");
            String httpCurrentUrl = "";
            int statusCode = 0;
            if (currentUrl.startsWith("http://")) {
                httpCurrentUrl = currentUrl;
                statusCode = Common.getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
            } else if (!currentUrl.startsWith("https://")) {
                httpCurrentUrl = "http://" + currentUrl;
                statusCode = Common.getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
            }
            ninjaURL.setUrlStatus(statusCode);
//				logger.info("Current URL: " + httpCurrentUrl + " Redirected URL: " + redirectedUrl+ " Status Code" + statusCode);
            if (!redirectedUrl.toString().equals("")) {
                ninjaURL.setRedirectedURL(redirectedUrl.toString());
                return redirectedUrl.toString();
            } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                return "too many redirection";
            } else if (statusCode == 200) {
                ninjaURL.setRedirectedURL(httpCurrentUrl);
                return httpCurrentUrl;
            } else if (statusCode != 200) {
                if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                    httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                    statusCode = Common.getStatusCodeVersion2(httpCurrentUrl, redirectedUrl);
                    ninjaURL.setUrlStatus(statusCode);
                    if (!redirectedUrl.toString().equals("")) {
                        ninjaURL.setRedirectedURL(redirectedUrl.toString());
                        return redirectedUrl.toString();
                    } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                        return "too many redirection";
                    } else if (statusCode == 200) {
                        ninjaURL.setRedirectedURL(httpCurrentUrl);
                        return httpCurrentUrl;
                    }
                }
            }
            statusCode = 0;
            redirectedUrl = new StringBuilder("");
            String httpsCurrentUrl = "";
            if (currentUrl.startsWith("https://")) {
                httpsCurrentUrl = currentUrl;
                statusCode = Common.getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
            } else if (!currentUrl.startsWith("http://")) {
                httpsCurrentUrl = "https://" + currentUrl;
                statusCode = Common.getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
            }
            ninjaURL.setUrlStatus(statusCode);
            if (!redirectedUrl.toString().equals("")) {
                ninjaURL.setRedirectedURL(redirectedUrl.toString());
                return redirectedUrl.toString();
            } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                return "too many redirection";
            } else if (statusCode == 200) {
                ninjaURL.setRedirectedURL(httpsCurrentUrl);
                return httpsCurrentUrl;
            } else {
                if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                    httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                    statusCode = Common.getStatusCodeVersion2(httpsCurrentUrl, redirectedUrl);
                    ninjaURL.setUrlStatus(statusCode);
                    if (!redirectedUrl.toString().equals("")) {
                        ninjaURL.setRedirectedURL(redirectedUrl.toString());
                        return redirectedUrl.toString();
                    } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                        return "too many redirection";
                    } else if (statusCode == 200) {
                        ninjaURL.setRedirectedURL(httpsCurrentUrl);
                        return httpsCurrentUrl;
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            }
        } else {
            return "invalid";
        }
    }

    public static String checkURLAndGetHTML(NinjaURL ninjaURL, StringBuilder pageHTML) {
        // Checking redirection to get URLs with too many redirection
        String currentUrl = ninjaURL.getUrl();
        if (!(currentUrl.equals("") || currentUrl == null)) {
            StringBuilder redirectedUrl = new StringBuilder("");
            String httpCurrentUrl = "";
            int statusCode = 0;
            if (currentUrl.startsWith("http://")) {
                httpCurrentUrl = currentUrl;
                statusCode = Common.getStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
            } else if (!currentUrl.startsWith("https://")) {
                httpCurrentUrl = "http://" + currentUrl;
                statusCode = Common.getStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
            }
            ninjaURL.setUrlStatus(statusCode);
            logger.debug("Current URL: " + httpCurrentUrl + " Redirected URL: " + redirectedUrl + " Status Code" + statusCode);
            if (!redirectedUrl.toString().equals("")) {
                ninjaURL.setRedirectedURL(redirectedUrl.toString());
                return redirectedUrl.toString();
            } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                return "too many redirection";
            } else if (statusCode == 200) {
                ninjaURL.setRedirectedURL(httpCurrentUrl);
                return httpCurrentUrl;
            } else if (statusCode != 200) {
                if (!httpCurrentUrl.equals("") && !httpCurrentUrl.contains("www.")) {
                    httpCurrentUrl = httpCurrentUrl.replace("http://", "http://www.");
                    statusCode = Common.getStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
                    ninjaURL.setUrlStatus(statusCode);
                    if (!redirectedUrl.toString().equals("")) {
                        ninjaURL.setRedirectedURL(redirectedUrl.toString());
                        return redirectedUrl.toString();
                    } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                        return "too many redirection";
                    } else if (statusCode == 200) {
                        ninjaURL.setRedirectedURL(httpCurrentUrl);
                        return httpCurrentUrl;
                    }
                }
            }
            statusCode = 0;
            redirectedUrl = new StringBuilder("");
            String httpsCurrentUrl = "";
            if (currentUrl.startsWith("https://")) {
                httpsCurrentUrl = currentUrl;
                statusCode = Common.getStatusCodeAndHTML(httpsCurrentUrl, redirectedUrl, pageHTML);
            } else if (!currentUrl.startsWith("http://")) {
                httpsCurrentUrl = "https://" + currentUrl;
                statusCode = Common.getStatusCodeAndHTML(httpsCurrentUrl, redirectedUrl, pageHTML);
            }
            ninjaURL.setUrlStatus(statusCode);
            if (!redirectedUrl.toString().equals("")) {
                ninjaURL.setRedirectedURL(redirectedUrl.toString());
                return redirectedUrl.toString();
            } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                return "too many redirection";
            } else if (statusCode == 200) {
                ninjaURL.setRedirectedURL(httpsCurrentUrl);
                return httpsCurrentUrl;
            } else {
                if (!httpsCurrentUrl.equals("") && !httpsCurrentUrl.contains("www.")) {
                    httpsCurrentUrl = httpsCurrentUrl.replace("https://", "https://www.");
                    statusCode = Common.getStatusCodeAndHTML(httpsCurrentUrl, redirectedUrl, pageHTML);
                    ninjaURL.setUrlStatus(statusCode);
                    if (!redirectedUrl.toString().equals("")) {
                        ninjaURL.setRedirectedURL(redirectedUrl.toString());
                        return redirectedUrl.toString();
                    } else if (statusCode == 301 || statusCode == 302 || statusCode == 306 || statusCode == 307) {
                        return "too many redirection";
                    } else if (statusCode == 200) {
                        ninjaURL.setRedirectedURL(httpsCurrentUrl);
                        return httpsCurrentUrl;
                    } else {
                        return "invalid";
                    }
                } else {
                    return "invalid";
                }
            }
        } else {
            return "invalid";
        }
    }

    public static String checkCurrentURLAndGetHTML(NinjaURL ninjaURL, StringBuilder pageHTML) {
        String returnString = null;
        String currentUrl = null;
        String httpCurrentUrl = null;
        int statusCode = 0;
        if (ninjaURL != null && (ninjaURL.getUrl() != null && !ninjaURL.getUrl().trim().isEmpty())) {
            currentUrl = ninjaURL.getUrl();
            StringBuilder redirectedUrl = new StringBuilder("");
            httpCurrentUrl = "";
            if (currentUrl.startsWith("http")) {
                httpCurrentUrl = currentUrl;
                statusCode = Common.getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
            } else {
                httpCurrentUrl = "http://" + currentUrl;
                statusCode = Common.getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML);
            }

            if (statusCode == 200) {  //URL is working properly
                ninjaURL.setRedirectedURL(httpCurrentUrl);
                ninjaURL.setUrlStatus(statusCode);
                returnString = httpCurrentUrl;
            } else if (statusCode >= 300 && statusCode < 400) {    //URL redirecting
                if (!redirectedUrl.toString().equals("")) {
                    ninjaURL.setUrlStatus(statusCode);
                    ninjaURL.setRedirectedURL(redirectedUrl.toString());
                    returnString = redirectedUrl.toString();
                } else {      //URL is redirected multiple times
                    ninjaURL.setUrlStatus(0);
                    ninjaURL.setRedirectedURL("");
                    returnString = "too many redirection";
                }
            } else {  //For Other Status Code of URL
                ninjaURL.setUrlStatus(statusCode);
                returnString = "invalid";
            }
        } else {
            ninjaURL.setUrlStatus(0);
            returnString = "invalid";
        }
        return returnString;
    }

    public static void filterCrawlingURLs(List<NinjaURL> urls, BaseRobotRules baseRobotRules) {
        String[] abondonedExts = new String[]{".pdf", ".docx", ".doc", ".png", ".jpeg", ".avi",
            ".jpg", ".xls", ".xlsx", ".csv", ".tsv", ".exe"};
        Iterator<NinjaURL> ninjaURLIterator = null;
        if (urls != null && !urls.isEmpty()) {
            ninjaURLIterator = urls.iterator();
            while (ninjaURLIterator.hasNext()) {
                NinjaURL url = ninjaURLIterator.next();
                for (String ext : abondonedExts) {
                    if ((baseRobotRules != null && !baseRobotRules.isAllowed(url.getUrl())) || url.getUrl().endsWith(ext)) {
                        ninjaURLIterator.remove();
                        break;
                    }
                }
            }
        }
    }

    public static BaseRobotRules fetchRobotRules(String crawlerName, String domain) {
        if (!domain.startsWith("http://") && !domain.startsWith("https://")) {
            domain = "http://" + domain;
        }
        String url = domain + (domain.endsWith("/") ? "" : "/") + "robots.txt";
        try {
            InputStream robotTxtStream = new URL(url).openStream();
            byte[] bytes = IOUtils.toByteArray(robotTxtStream);
            SimpleRobotRulesParser robotParser = new SimpleRobotRulesParser();
            BaseRobotRules baseRobotRules = robotParser.parseContent(FAKE_ROBOTS_URL, bytes, "text/plain", crawlerName);
            return baseRobotRules;
        } catch (MalformedURLException e) {
            logger.error("Exception in fetching robot rules. " + e.getMessage());
            return null;
        } catch (IOException e) {
            logger.error("Exception in fetching robot rules. " + e.getMessage());
            return null;
        } catch (Exception e) {
            logger.error("Exception in fetching robot rules. " + e.getMessage());
            return null;
        }
    }
}
