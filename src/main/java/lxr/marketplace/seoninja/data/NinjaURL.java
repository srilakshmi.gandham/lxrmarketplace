package lxr.marketplace.seoninja.data;

public class NinjaURL {
	public static final int TO_BE_ADDED = 1; 
	public static final int ADDED = 2; 
	public static final int CONNECTING = 3; 
	public static final int PROCESSING = 4; 
	public static final int COMPLETED = 5; 
	
	private String url;
	private int crawlStatus;
	private String redirectedURL;
	private int urlStatus;
	private String baseUrl;
        private int linkDepthId;
        
	public NinjaURL(String url, int crawlStatus) {
		super();
		this.url = url;
		this.crawlStatus = crawlStatus;
		redirectedURL = "";
	}
	public NinjaURL(String url) {
		super();
		this.url = url;
		this.crawlStatus = NinjaURL.TO_BE_ADDED;
		redirectedURL = "";
	}
	
	public int getUrlStatus() {
		return urlStatus;
	}
	public void setUrlStatus(int urlStatus) {
		this.urlStatus = urlStatus;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getCrawlStatus() {
		return crawlStatus;
	}
	public void setCrawlStatus(int crawlStatus) {
		this.crawlStatus = crawlStatus;
	}	
	public String getRedirectedURL() {
		return redirectedURL;
	}
	public void setRedirectedURL(String redirectedURL) {
		this.redirectedURL = redirectedURL;
	}
	
        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }
	
        @Override
	public boolean equals(Object object){
		boolean equal = false;
		if(object == null){
			return false;
		}
		if(object instanceof NinjaURL){
			String thatURL =  new String(((NinjaURL) object).getUrl());
			String thisURL = new String(url);
			if(thatURL.startsWith("http://")){
				thatURL = thatURL.replace("http://", "");
			}else if(thatURL.startsWith("https://")){
				thatURL = thatURL.replace("https://", "");
			}
			
			if(thisURL.startsWith("http://")){
				thisURL = thisURL.replace("http://", "");
			}else if(thisURL.startsWith("https://")){
				thisURL = thisURL.replace("https://", "");
			}
			
			if(thatURL.equals(thisURL)){
				equal = true;
			}
		}
		return equal;
	}
	
	public int hashCode(){
		String thisURL = new String(url);

		if(thisURL.startsWith("http://")){
			thisURL = thisURL.replace("http://", "");
		}else if(thisURL.startsWith("https://")){
			thisURL = thisURL.replace("https://", "");
		}
		return thisURL.length();
	}

    public int getLinkDepthId() {
        return linkDepthId;
    }

    public void setLinkDepthId(int linkDepthId) {
        this.linkDepthId = linkDepthId;
    }
        
        
}
