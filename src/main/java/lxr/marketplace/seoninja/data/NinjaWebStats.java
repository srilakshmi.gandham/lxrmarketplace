package lxr.marketplace.seoninja.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class NinjaWebStats {
	private long webStatsId;
	private long domainId;
	private String domain;
	private Calendar fetchdate;
	private int nonWorkingURLCount; 
	private int homeTitleScore;
	private int titleScore;
	private int homeMetaKeyScore;
	private int metaKeyScore;
	private int homeMetaDescScore;
	private int metaDescScore;
	private int imageScore;
	private int homeHTagScore;
	private int hTagScore;
	private int homeWordScore;
	private int wordScore;
	private int keywordInURLScore;
	
	private int homeLoadtimeScore;
	private float homeContentSize;
	private boolean wwwResolved;
	private boolean errorPagePresent;
	private boolean robotsTxtPresent;
	private boolean sitemapPresent;
	private int homeCodeTextRatio;
//	private boolean mobileCompatibility;
	private int seoFriendlyURLScore;
    private int mobileCompatableScore;
	
	private long backlinkCount;
	private int backlinkScore;
	private int backlinkQuality;
	private int nofollowPc;
	private int internalLinkScore;
	private int backlinkAncrTextPc;
	private int uniqueBacklinkAncTextPc;
	private int backlinkAncrStopwordsPc;	
	private int directoryListingScore;
	
	private long googlePlusShares;
	private long facebookShares;
	private long facebookTotal;
	private long pinterestShares;
	private long tweets;
	private long linkedinShares;
	
	private int socialScore;
	private int contentScore;
	private int technicalScore;
	private int linkScore;
	private int siteGraderScore;

	
	
	private TreeMap<Integer, Integer> sgParamScores;
	private TreeMap<Integer, Integer> contentSubParamScores;
	private TreeMap<Integer, Integer> technicalSubParamScores;
	private TreeMap<Integer, Integer> linkSubParamScores;
	private TreeMap<Integer, Long> socialSubParamScores;
	

	
	public NinjaWebStats(long domainId, String domain) {
		super();
		this.domainId = domainId;
		this.domain = domain;
	}
	
	public NinjaWebStats(long domainId) {
        super();
        this.domainId = domainId;
    }
	
	public NinjaWebStats(String domain) {
		super();
	    this.domain = domain;
    }


	public int getMobileCompatableScore() {
		return mobileCompatableScore;
	}


	public void setMobileCompatableScore(int mobileCompatableScore) {
		this.mobileCompatableScore = mobileCompatableScore;
	}


	public long getWebStatsId() {
		return webStatsId;
	}


	public void setWebStatsId(long webStatsId) {
		this.webStatsId = webStatsId;
	}


	public long getDomainId() {
		return domainId;
	}


	public void setDomainId(long domainId) {
		this.domainId = domainId;
	}


	public String getDomain() {
		return domain;
	}


	public void setDomain(String domain) {
		this.domain = domain;
	}


	public Calendar getFetchdate() {
		return fetchdate;
	}


	public void setFetchdate(Calendar fetchdate) {
		this.fetchdate = fetchdate;
	}


	public int getNonWorkingURLCount() {
		return nonWorkingURLCount;
	}


	public void setNonWorkingURLCount(int nonWorkingURLCount) {
		this.nonWorkingURLCount = nonWorkingURLCount;
	}


	public int getHomeTitleScore() {
		return homeTitleScore;
	}


	public void setHomeTitleScore(int homeTitleScore) {
		this.homeTitleScore = homeTitleScore;
	}


	public int getTitleScore() {
		return titleScore;
	}


	public void setTitleScore(int titleScore) {
		this.titleScore = titleScore;
	}


	public int getHomeMetaKeyScore() {
		return homeMetaKeyScore;
	}


	public void setHomeMetaKeyScore(int homeMetaKeyScore) {
		this.homeMetaKeyScore = homeMetaKeyScore;
	}


	public int getMetaKeyScore() {
		return metaKeyScore;
	}


	public void setMetaKeyScore(int metaKeyScore) {
		this.metaKeyScore = metaKeyScore;
	}


	public int getHomeMetaDescScore() {
		return homeMetaDescScore;
	}


	public void setHomeMetaDescScore(int homeMetaDescScore) {
		this.homeMetaDescScore = homeMetaDescScore;
	}


	public int getMetaDescScore() {
		return metaDescScore;
	}


	public void setMetaDescScore(int metaDescScore) {
		this.metaDescScore = metaDescScore;
	}


	public int getImageScore() {
		return imageScore;
	}


	public void setImageScore(int imageScore) {
		this.imageScore = imageScore;
	}


	public int getHomeHTagScore() {
		return homeHTagScore;
	}


	public void setHomeHTagScore(int homeHTagScore) {
		this.homeHTagScore = homeHTagScore;
	}


	public int gethTagScore() {
		return hTagScore;
	}


	public void sethTagScore(int hTagScore) {
		this.hTagScore = hTagScore;
	}


	public int getHomeWordScore() {
		return homeWordScore;
	}


	public void setHomeWordScore(int homeWordScore) {
		this.homeWordScore = homeWordScore;
	}


	public int getWordScore() {
		return wordScore;
	}


	public void setWordScore(int wordScore) {
		this.wordScore = wordScore;
	}


	public int getKeywordInURLScore() {
		return keywordInURLScore;
	}


	public void setKeywordInURLScore(int keywordInURLScore) {
		this.keywordInURLScore = keywordInURLScore;
	}


	public int getHomeLoadtimeScore() {
		return homeLoadtimeScore;
	}


	public void setHomeLoadtimeScore(int homeLoadtimeScore) {
		this.homeLoadtimeScore = homeLoadtimeScore;
	}


	public float getHomeContentSize() {
		return homeContentSize;
	}


	public void setHomeContentSize(float homeContentSize) {
		this.homeContentSize = homeContentSize;
	}


	public boolean isWwwResolved() {
		return wwwResolved;
	}


	public void setWwwResolved(boolean wwwResolved) {
		this.wwwResolved = wwwResolved;
	}


	public boolean isErrorPagePresent() {
		return errorPagePresent;
	}


	public void setErrorPagePresent(boolean errorPagePresent) {
		this.errorPagePresent = errorPagePresent;
	}


	public boolean isRobotsTxtPresent() {
		return robotsTxtPresent;
	}


	public void setRobotsTxtPresent(boolean robotsTxtPresent) {
		this.robotsTxtPresent = robotsTxtPresent;
	}


	public boolean isSitemapPresent() {
		return sitemapPresent;
	}


	public void setSitemapPresent(boolean sitemapPresent) {
		this.sitemapPresent = sitemapPresent;
	}


	public int getHomeCodeTextRatio() {
		return homeCodeTextRatio;
	}


	public void setHomeCodeTextRatio(int homeCodeTextRatio) {
		this.homeCodeTextRatio = homeCodeTextRatio;
	}


//	public boolean isMobileCompatibility() {
//		return mobileCompatibility;
//	}
//
//
//	public void setMobileCompatibility(boolean mobileCompatibility) {
//		this.mobileCompatibility = mobileCompatibility;
//	}


	public int getSeoFriendlyURLScore() {
		return seoFriendlyURLScore;
	}


	public void setSeoFriendlyURLScore(int seoFriendlyURLScore) {
		this.seoFriendlyURLScore = seoFriendlyURLScore;
	}


	public long getBacklinkCount() {
		return backlinkCount;
	}


	public void setBacklinkCount(long backlinkCount) {
		this.backlinkCount = backlinkCount;
	}


	public int getBacklinkScore() {
		return backlinkScore;
	}


	public void setBacklinkScore(int backlinkScore) {
		this.backlinkScore = backlinkScore;
	}


	public int getBacklinkQuality() {
		return backlinkQuality;
	}


	public void setBacklinkQuality(int backlinkQuality) {
		this.backlinkQuality = backlinkQuality;
	}


	public int getNofollowPc() {
		return nofollowPc;
	}


	public void setNofollowPc(int nofollowPc) {
		this.nofollowPc = nofollowPc;
	}


	public int getInternalLinkScore() {
		return internalLinkScore;
	}


	public void setInternalLinkScore(int internalLinkScore) {
		this.internalLinkScore = internalLinkScore;
	}


	public int getBacklinkAncrTextPc() {
		return backlinkAncrTextPc;
	}


	public void setBacklinkAncrTextPc(int backlinkAncrTextPc) {
		this.backlinkAncrTextPc = backlinkAncrTextPc;
	}


	public int getBacklinkAncrStopwordsPc() {
		return backlinkAncrStopwordsPc;
	}


	public void setBacklinkAncrStopwordsPc(int backlinkAncrStopwordsPc) {
		this.backlinkAncrStopwordsPc = backlinkAncrStopwordsPc;
	}


	public int getDirectoryListingScore() {
		return directoryListingScore;
	}


	public void setDirectoryListingScore(int directoryListingScore) {
		this.directoryListingScore = directoryListingScore;
	}


	public long getGooglePlusShares() {
		return googlePlusShares;
	}


	public void setGooglePlusShares(long googlePlusShares) {
		this.googlePlusShares = googlePlusShares;
	}


	public long getFacebookShares() {
		return facebookShares;
	}


	public void setFacebookShares(long facebookShares) {
		this.facebookShares = facebookShares;
	}


	public long getFacebookTotal() {
		return facebookTotal;
	}


	public void setFacebookTotal(long facebookTotal) {
		this.facebookTotal = facebookTotal;
	}


	public long getPinterestShares() {
		return pinterestShares;
	}


	public void setPinterestShares(long pinterestShares) {
		this.pinterestShares = pinterestShares;
	}


	public long getTweets() {
		return tweets;
	}


	public void setTweets(long tweets) {
		this.tweets = tweets;
	}


	public long getLinkedinShares() {
		return linkedinShares;
	}


	public void setLinkedinShares(long linkedinShares) {
		this.linkedinShares = linkedinShares;
	}


	public int getSocialScore() {
		return socialScore;
	}


	public void setSocialScore(int socialScore) {
		this.socialScore = socialScore;
	}


	public int getUniqueBacklinkAncTextPc() {
		return uniqueBacklinkAncTextPc;
	}

	public void setUniqueBacklinkAncTextPc(int uniqueBacklinkAncTextPc) {
		this.uniqueBacklinkAncTextPc = uniqueBacklinkAncTextPc;
	}


	public int getContentScore() {
		return contentScore;
	}


	public void setContentScore(int contentScore) {
		this.contentScore = contentScore;
	}


	public int getTechnicalScore() {
		return technicalScore;
	}


	public void setTechnicalScore(int technicalScore) {
		this.technicalScore = technicalScore;
	}


	public int getLinkScore() {
		return linkScore;
	}


	public void setLinkScore(int linkScore) {
		this.linkScore = linkScore;
	}


	public int getSiteGraderScore() {
		return siteGraderScore;
	}


	public void setSiteGraderScore(int siteGraderScore) {
		this.siteGraderScore = siteGraderScore;
	}
	
	public TreeMap<Integer, Integer> getSgParamScores() {
	    HashMap<Integer, Integer> scores = new HashMap<Integer, Integer>();
	    scores.put(1, contentScore);
	    scores.put(2, technicalScore);
	    scores.put(3, linkScore);
	    scores.put(4, socialScore);
	    ValueComparator bvc =  new ValueComparator(scores);
	    sgParamScores = new TreeMap<Integer, Integer>(bvc);
        return sgParamScores;
    }

    public TreeMap<Integer, Integer> getContentSubParamScores() {
        HashMap<Integer, Integer> scores = new HashMap<Integer, Integer>();
        scores.put(1, titleScore);
        scores.put(2, metaKeyScore);
        scores.put(3, imageScore);
        scores.put(4, metaDescScore);
        scores.put(5, hTagScore);
        scores.put(6, wordScore);
        scores.put(7, keywordInURLScore);
        ValueComparator bvc =  new ValueComparator(scores);
        contentSubParamScores = new TreeMap<Integer, Integer>(bvc);
        return contentSubParamScores;
    }

    public TreeMap<Integer, Integer> getTechnicalSubParamScores() {
        HashMap<Integer, Integer> scores = new HashMap<Integer, Integer>();
        scores.put(1, homeLoadtimeScore);
        scores.put(2, wwwResolved?100:0);
        scores.put(3, errorPagePresent?100:0);
        scores.put(4, robotsTxtPresent?100:0);
        scores.put(5, sitemapPresent?100:0);
        scores.put(6, homeCodeTextRatio);
        scores.put(7, seoFriendlyURLScore);
        scores.put(8, mobileCompatableScore);
        ValueComparator bvc =  new ValueComparator(scores);
        technicalSubParamScores = new TreeMap<Integer, Integer>(bvc);
        return technicalSubParamScores;
    }

    public TreeMap<Integer, Integer> getLinkSubParamScores() {
        HashMap<Integer, Integer> scores = new HashMap<Integer, Integer>();
        scores.put(1, backlinkScore);
        scores.put(2, nofollowPc);
        scores.put(3, internalLinkScore);
        scores.put(4, backlinkAncrTextPc);
        scores.put(5, uniqueBacklinkAncTextPc);
        ValueComparator bvc =  new ValueComparator(scores);
        linkSubParamScores = new TreeMap<Integer, Integer>(bvc);
        return linkSubParamScores;
    }

    public TreeMap<Integer, Long> getSocialSubParamScores() {
        HashMap<Integer, Long> scores = new HashMap<Integer, Long>();
        scores.put(1, googlePlusShares);
        scores.put(2, facebookTotal);
        scores.put(3, pinterestShares);
        scores.put(4, tweets);
        scores.put(5, linkedinShares);
        ValueComparatorLong bvc =  new ValueComparatorLong(scores);
        socialSubParamScores = new TreeMap<Integer, Long>(bvc);
        return socialSubParamScores;
    }
    
    @Override public String toString(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		String formattedDt = sdf.format(fetchdate.getTime());
		return "{\"backlinkAncrStopwordsPc\":"+backlinkAncrStopwordsPc+",\"backlinkAncrTextPc\":"+backlinkAncrTextPc+"," +
				"\"backlinkCount\":"+backlinkCount+",\"backlinkQuality\":"+backlinkQuality+",\"backlinkScore\":"+backlinkScore+"," +
				"\"contentScore\":"+contentScore+",\"directoryListingScore\":"+directoryListingScore+",\"domain\":\""+domain+"\"," +
			    "\"domainId\":"+domainId+",\"errorPagePresent\":"+errorPagePresent+",\"facebookShares\":"+facebookShares+",\"facebookTotal\":"+facebookTotal+"," +
			    "\"fetchdate\":\""+formattedDt+"\",\"googlePlusShares\":"+googlePlusShares+",\"hTagScore\":"+hTagScore+",\"homeCodeTextRatio\":"+homeCodeTextRatio+"," +
			    "\"homeContentSize\":"+homeContentSize+",\"homeHTagScore\":"+homeHTagScore+",\"homeLoadtimeScore\":"+homeLoadtimeScore+",\"homeMetaDescScore\":"+homeMetaDescScore+"," +
			    "\"homeMetaKeyScore\":"+homeMetaKeyScore+",\"homeTitleScore\":"+homeTitleScore+",\"homeWordScore\":"+homeWordScore+"," +
			    "\"imageScore\":"+imageScore+",\"internalLinkScore\":"+internalLinkScore+",\"keywordInURLScore\":"+keywordInURLScore+"," +
			    "\"linkScore\":"+linkScore+",\"linkedinShares\":"+linkedinShares+",\"metaDescScore\":"+metaDescScore+"," +
			    "\"metaKeyScore\":"+metaKeyScore+",\"mobileCompatableScore\":"+mobileCompatableScore+",\"nofollowPc\":"+nofollowPc+"," +
			    "\"nonWorkingURLCount\":"+nonWorkingURLCount+",\"pinterestShares\":"+pinterestShares+",\"robotsTxtPresent\":"+robotsTxtPresent+"," +
			    "\"seoFriendlyURLScore\":"+seoFriendlyURLScore+",\"siteGraderScore\":"+siteGraderScore+",\"sitemapPresent\":"+sitemapPresent+"," +
			    "\"socialScore\":"+socialScore+",\"technicalScore\":"+technicalScore+",\"titleScore\":"+titleScore+",\"tweets\":"+tweets+"," +
			    "\"uniqueBacklinkAncTextPc\":"+uniqueBacklinkAncTextPc+",\"webStatsId\":"+webStatsId+",\"wordScore\":"+wordScore+"," +
			    "\"wwwResolved\":"+wwwResolved+",\"screenshot\":\"/lxrseo/screenshot/"+domainId+".jpg\"}";
	}
    
    public int countSiteErrors(){
        int errorCount = 0;
        errorCount += titleScore < 30 ? 1 : 0;
        errorCount += metaKeyScore < 30 ? 1 : 0;
        errorCount += imageScore < 30 ? 1 : 0;
        errorCount += metaDescScore < 30 ? 1 : 0;
        errorCount += hTagScore < 30 ? 1 : 0;
        errorCount += wordScore < 30 ? 1 : 0;
        errorCount += keywordInURLScore < 30 ? 1 : 0;

        errorCount += homeLoadtimeScore < 30 ? 1 : 0;
        errorCount += wwwResolved ? 0 : 1;
        errorCount += errorPagePresent ? 0 : 1;
        errorCount += robotsTxtPresent ? 0 : 1;
        errorCount += sitemapPresent ? 0 : 1;
        errorCount += homeCodeTextRatio < 30 ? 1 : 0;
        errorCount += seoFriendlyURLScore < 30 ? 1 : 0;
        errorCount += mobileCompatableScore < 30 ? 1 : 0;

        errorCount += backlinkScore < 30 ? 1 : 0;
        errorCount += nofollowPc < 30 ? 1 : 0;
        errorCount += internalLinkScore < 30 ? 1 : 0;
        errorCount += backlinkAncrTextPc < 30 ? 1 : 0;
        errorCount += uniqueBacklinkAncTextPc < 30 ? 1 : 0;
        return errorCount;
    }
}

class ValueComparator implements Comparator<Integer> {

    Map<Integer, Integer> base;

    ValueComparator(Map<Integer, Integer> base) {
        this.base = base;
    }

//    @Override
    public int compare(Integer a, Integer b) {
        if (base.get(a) >= base.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
}

class ValueComparatorLong implements Comparator<Integer> {

    Map<Integer, Long> base;

    ValueComparatorLong(Map<Integer, Long> base) {
        this.base = base;
    }

//    @Override
    public int compare(Integer a, Integer b) {
        if (base.get(a) >= base.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
}
