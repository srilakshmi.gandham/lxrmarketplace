/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import static lxr.marketplace.lxrmpayments.MarketplacePaymentService.addAmtDetailsCell;
import static lxr.marketplace.lxrmpayments.MarketplacePaymentService.addtotalAmtCell;
import lxr.marketplace.mobile.user.PaymentRestController;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class PaymentService {

    private static final Logger LOGGER = Logger.getLogger(PaymentService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    PaymentRestController paymentRestController;

    @Autowired
    private MessageSource messageSource;

    public LXRMTransaction processPaymentForLXRMService(PaymentModel paymentModel, StringBuilder failurMessage, String invoicePath) {
        long paymentRecordId = 0;
        LXRMTransaction lxrTransaction = null;
        try {
            lxrTransaction = paymentRestController.paymentWithACard(paymentModel);
            if (lxrTransaction != null && lxrTransaction.getState() != null && lxrTransaction.getState().equals(GatewayService.CAPTURE_INTENT)) {
                String fileName = generateInvoiceForPayment(lxrTransaction, paymentModel, invoicePath);
                if (fileName != null) {
                    lxrTransaction.setInvoiceFilePath(fileName);
                }
                paymentRecordId = saveGateWayTransaction(lxrTransaction, paymentModel);
                lxrTransaction.setPaymentId(paymentRecordId);
                try {
                    if (paymentRecordId > 0 && paymentModel.isSendInvoice()
                            && (paymentModel.getBillingEmailId() != null && !paymentModel.getBillingEmailId().isEmpty())
                            && (paymentModel.getBillingUserName() != null && !paymentModel.getBillingUserName().isEmpty())) {
                        lxrTransaction.setLxrmUserId(paymentModel.getPayerUserId());
                        LOGGER.info("Transcation status is saved successfully and paymentRecordId ::" + paymentRecordId + ", Billing user name id:" + paymentModel.getBillingUserName() + ", email id:: " + paymentModel.getBillingEmailId());
                        String subjectLine = null;
                        String bodyText = null;

                        if (paymentModel.getPayerUserId() > 0) {
                            /*LXRMTools and Mobile APPS*/
                            subjectLine = EmailBodyCreator.subjectOnPaymnetSuccess;
                            bodyText = EmailBodyCreator.bodyForPaymnetSuccess(paymentModel.getBillingUserName(), paymentModel.getToolName(), paymentModel.getToolId(), Common.convertLocaleDateTimeToDate(lxrTransaction.getCreatedDate()));
                        } else {
                            /*Only for Dashly apps*/
                            subjectLine = paymentModel.getToolName().concat(EmailBodyCreator.custiomizeSubject);
                            bodyText = EmailBodyCreator.bodyForPaymnetSuccessV2(paymentModel, messageSource.getMessage("MobieApps.SupportMail", null, Locale.US));
                        }
                        if (bodyText != null && subjectLine != null) {
                            SendMail.sendMailWithAttachMent(invoicePath, fileName, paymentModel.getBillingUserName(), paymentModel.getBillingEmailId(), subjectLine, bodyText, Boolean.TRUE);
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception in sending invoice to user for Payment Record Id::" + paymentRecordId + ", Billing user name id:" + paymentModel.getBillingUserName() + ", email id:: " + paymentModel.getBillingEmailId() + "\n" + e);
                }

            } else if (lxrTransaction != null) {
                failurMessage.append(lxrTransaction.getFailureMessage());
            }
        } catch (Exception ex) {
            failurMessage.append("Oh snap! ".concat(GatewayService.AUTH_FAILURE).concat(" Please try again."));
            LOGGER.error("Exception in processPaymentForService cause:: ", ex);
        }
        return lxrTransaction;
    }

    private long saveGateWayTransaction(LXRMTransaction lXRMTransaction, PaymentModel paymentModel) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        if (paymentModel.isSaveUserId()) {
            final String lxrmToolsQuery = "INSERT INTO lxrm_payment_transaction_stats (transaction_id, created_time, updated_time,"
                    + " state, intent, tool_id, currency, amount, description,"
                    + " is_card, payer_user_id, invoice_file_path, payment_service) VALUES "
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
            try {
                jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(lxrmToolsQuery, PreparedStatement.RETURN_GENERATED_KEYS);
                    statement.setString(1, lXRMTransaction.getGatewayTransactionId());
                    statement.setTimestamp(2, Timestamp.valueOf(paymentModel.getPaymentDate()));
                    statement.setTimestamp(3, Timestamp.valueOf(lXRMTransaction.getCreatedDate()));

                    statement.setString(4, "completed");
                    statement.setString(5, lXRMTransaction.getState());

                    statement.setInt(6, paymentModel.getToolId());
                    statement.setString(7, paymentModel.getCurrencyType() != null ? paymentModel.getCurrencyType() : null);
                    statement.setDouble(8, Double.parseDouble(lXRMTransaction.getAmount()));

                    statement.setString(9, paymentModel.getDescription() != null ? paymentModel.getDescription() : null);

                    statement.setBoolean(10, Boolean.TRUE);
                    statement.setLong(11, paymentModel.getPayerUserId());

                    statement.setString(12, lXRMTransaction.getInvoiceFilePath() != null ? lXRMTransaction.getInvoiceFilePath() : null);
                    statement.setString(13, GatewayService.GATEWAY_BRAINTREE);
                    return statement;
                }, keyHolder);
            } catch (DataAccessException ex) {
                LOGGER.error("DataAccessException in saveGateWayTransaction cause::", ex);
            } catch (Exception e) {
                LOGGER.error("Exception on saveGateWayTransaction cause:: ", e);
            }
        } else {
            final String dashlyAppsQuery = "INSERT INTO lxrm_payment_transaction_stats (transaction_id, created_time, updated_time,"
                    + " state, intent, tool_id, currency, amount, description,"
                    + " is_card, invoice_file_path, payment_service) VALUES "
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
            try {
                jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(dashlyAppsQuery, PreparedStatement.RETURN_GENERATED_KEYS);
                    statement.setString(1, lXRMTransaction.getGatewayTransactionId());
                    statement.setTimestamp(2, Timestamp.valueOf(paymentModel.getPaymentDate()));
                    statement.setTimestamp(3, Timestamp.valueOf(lXRMTransaction.getCreatedDate()));

                    statement.setString(4, "completed");
                    statement.setString(5, lXRMTransaction.getState());

                    statement.setInt(6, paymentModel.getToolId());
                    statement.setString(7, paymentModel.getCurrencyType() != null ? paymentModel.getCurrencyType() : null);
                    statement.setDouble(8, Double.parseDouble(lXRMTransaction.getAmount()));

                    statement.setString(9, paymentModel.getDescription() != null ? paymentModel.getDescription() : null);

                    statement.setBoolean(10, Boolean.TRUE);
                    statement.setString(11, lXRMTransaction.getInvoiceFilePath() != null ? lXRMTransaction.getInvoiceFilePath() : null);
                    statement.setString(12, GatewayService.GATEWAY_BRAINTREE);
                    return statement;
                }, keyHolder);
            } catch (DataAccessException ex) {
                LOGGER.error("DataAccessException in saveGateWayTransaction cause::", ex);
            } catch (Exception e) {
                LOGGER.error("Exception on saveGateWayTransaction cause:: ", e);
            }
        }
        return (Long) keyHolder.getKey();
    }

    public String getPaymentCardType(String paymentCardNo) {
        String type = null;
        if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.Visa", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "VISA";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.Mastercard", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "MASTERCARD";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.AmericanExpress", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "AMERICAN_EXPRESS";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.DinersClub", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "DINERS_CLUB";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.Discover", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "DISCOVER";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.JCB", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "JCB";
        } else if (Pattern.compile(messageSource.getMessage("Payment.Card.Validation.Pattern.Maestro", null, Locale.US)).matcher(paymentCardNo).find()) {
            type = "Maestro";
        }
        return type;
    }

    public String generateInvoiceForPayment(LXRMTransaction lXRMTransaction, PaymentModel paymentModel, String tragetFolder) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy");
        try {
            if (paymentModel.getDatePattern() != null) {
                formatter = DateTimeFormatter.ofPattern(paymentModel.getDatePattern());
            }
            DecimalFormat df = new DecimalFormat("#.00");
            String amount = df.format(Double.parseDouble(lXRMTransaction.getAmount()));
            BaseColor white = new BaseColor(249, 249, 249);
            BaseColor gray = new BaseColor(100, 100, 100);
            BaseColor orange = new BaseColor(255, 111, 13);
            BaseColor lightGray = new BaseColor(120, 120, 120);
            Font reportNameFont = new Font(FontFactory.getFont("HELVETICA", 17f, Font.BOLD, gray)); // black color
            Font subElementFont = new Font(FontFactory.getFont("HELVETICA", 14f, Font.BOLD, white));
            Font subElementLightGrayFont = new Font(FontFactory.getFont("HELVETICA", 12f, Font.BOLD, lightGray));
            Font lightGrayFont = new Font(FontFactory.getFont("HELVETICA", 12f, Font.NORMAL, lightGray));
            Font smallLightGrayFont = new Font(FontFactory.getFont("HELVETICA", 10f, Font.NORMAL, lightGray));
            String tempUserName = paymentModel.getPayerUserId() != 0 ? String.valueOf(paymentModel.getPayerUserId()) : (paymentModel.getBillingUserName().replaceAll(" ", "").trim());
            String filename = Common.createFileName("LXRMarketplace_Invoice_" + tempUserName) + ".pdf";
            String filePath = tragetFolder + filename;
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter writer = PdfWriter.getInstance(pdfDoc, stream);
            /*writer.setPageEvent(event);
            DateFormat sdf = new SimpleDateFormat("MMM dd, YYYY");*/

            pdfDoc.setMargins(25, 25, 75, 40);
            pdfDoc.open();
            PdfPTable header = new PdfPTable(2);
            header.setWidthPercentage(99.9f);
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/lxrmarketplace_v3.png");
            img.scaleToFit(120f, 120f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(10f);
            logo.setHorizontalAlignment(Element.ALIGN_RIGHT);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);

            PdfPCell headingNameCell = new PdfPCell();
            headingNameCell.setBorder(Rectangle.NO_BORDER);
            headingNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            Phrase headingName = new Phrase("INVOICE", reportNameFont);
            headingNameCell.addElement(headingName);
            header.addCell(headingNameCell);
            header.addCell(logo);
            header.completeRow();
            /*Calendar today = Calendar.getInstance();
            PdfPCell dateCell = new PdfPCell(new Phrase("Date: " + sdf.format(today.getTime()), lightGrayFont));*/
            PdfPCell dateCell = new PdfPCell(new Phrase("Date: " + formatter.format(lXRMTransaction.getCreatedDate()), lightGrayFont));
            dateCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(dateCell);

            PdfPCell addressCell = new PdfPCell(new Phrase("3 Independence Way, Suite #203\n" + "Princeton, NJ08540, US\n" + "support@lxrmarketplace.com", smallLightGrayFont));
            addressCell.setBorder(Rectangle.NO_BORDER);
            addressCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            header.addCell(addressCell);

//            PdfPCell toAddCell = new PdfPCell(new Phrase("Billed To:\n\n" + paymentModel.getBillingUserName().trim() + "\n", lightGrayFont));
            PdfPCell toAddCell = new PdfPCell(new Phrase("Billed To:\n" + paymentModel.getBillingUserName().trim() + "\n", lightGrayFont));
            toAddCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(toAddCell);
            PdfPCell emptyCell = new PdfPCell(new Phrase(""));
            emptyCell.setBorder(Rectangle.NO_BORDER);
            header.addCell(emptyCell);

            PdfPTable invoiceAmtTable = new PdfPTable(1);
            invoiceAmtTable.setSpacingBefore(30f);
            invoiceAmtTable.setWidthPercentage(99.9f);
            PdfPCell amountCell = new PdfPCell(new Phrase("Amount: $" + lXRMTransaction.getAmount(), subElementFont));
            amountCell.setPaddingLeft(10f);
            amountCell.setMinimumHeight(50f);
            amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            amountCell.setBorder(Rectangle.NO_BORDER);
            amountCell.setBackgroundColor(orange);

            invoiceAmtTable.addCell(amountCell);

            float[] amtWidths = {1f, 3.5f, 3.5f, 2f};
            PdfPTable amtDetailsTable = new PdfPTable(amtWidths);
            amtDetailsTable.setWidthPercentage(99.9f);
            amtDetailsTable.addCell(addAmtDetailsCell("S No.", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Service", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Plan", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell("Amount", false, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell(String.valueOf(1), true, "center", subElementLightGrayFont));
            amtDetailsTable.addCell(addAmtDetailsCell(paymentModel.getDescription() != null ? paymentModel.getDescription() : paymentModel.getToolName(), true, "center", subElementLightGrayFont));
            if (paymentModel.getPlan() != null && !paymentModel.getPlan().equalsIgnoreCase("NA")) {
                amtDetailsTable.addCell(addAmtDetailsCell(paymentModel.getPlan(), true, "center", subElementLightGrayFont));
            } else {
                amtDetailsTable.addCell(addAmtDetailsCell("-", true, "center", reportNameFont));
            }
            amtDetailsTable.addCell(addAmtDetailsCell("$" + amount, true, "right", subElementLightGrayFont));

            float[] subTotalAmtWidths = {5f, 3f, 2f};
            PdfPTable subTotalTable = new PdfPTable(subTotalAmtWidths);
            subTotalTable.setSpacingBefore(2f);
            subTotalTable.setWidthPercentage(99.9f);
            subTotalTable.addCell(addtotalAmtCell("", "", false, subElementLightGrayFont));
            subTotalTable.addCell(addtotalAmtCell("Sub Total", "Tax", false, subElementLightGrayFont));
            subTotalTable.addCell(addtotalAmtCell("$" + amount, "$0.00", true, subElementLightGrayFont));

            float[] totalAmtWidths = {6f, 4f};
            PdfPTable totalAmt = new PdfPTable(totalAmtWidths);
            totalAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
            totalAmt.setSpacingBefore(3f);
            totalAmt.setWidthPercentage(48);

            PdfPCell nameCell = new PdfPCell(new Phrase("Total Amount", subElementFont));
            nameCell.setMinimumHeight(30f);
            nameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            nameCell.setPaddingLeft(5f);
            nameCell.setBorder(Rectangle.NO_BORDER);
            nameCell.setBackgroundColor(orange);

            PdfPCell amtCell = new PdfPCell(new Phrase("$" + amount, subElementFont));
            amtCell.setBorder(Rectangle.NO_BORDER);
            amtCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            amtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            amtCell.setPaddingRight(10f);
            amtCell.setBackgroundColor(orange);
            totalAmt.addCell(nameCell);
            totalAmt.addCell(amtCell);

            PdfPTable payMethodTable = new PdfPTable(1);
            payMethodTable.setHorizontalAlignment(Element.ALIGN_LEFT);
            payMethodTable.setSpacingBefore(3f);
            payMethodTable.setWidthPercentage(48);

            PdfPCell payHeadingCell = new PdfPCell(new Phrase("PAYMENT METHOD", new Font(FontFactory.getFont("HELVETICA", 14f, Font.BOLD, gray))));
            payHeadingCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(payHeadingCell);
            PdfPCell creditCardCell = new PdfPCell();
//            creditCardCell.addElement(new Phrase("Credit/Debit Card No", lightGrayFont));
            creditCardCell.addElement(new Phrase("Payment Card No", lightGrayFont));
            String cardNo = paymentModel.getCardNo();
            cardNo = (cardNo.substring(0, 4).concat("******")).concat(cardNo.substring(12, cardNo.length()));
            creditCardCell.addElement(new Phrase(cardNo, lightGrayFont));
            creditCardCell.setPaddingTop(10f);
            creditCardCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(creditCardCell);
            PdfPCell transctnIdCell = new PdfPCell();
            transctnIdCell.addElement(new Phrase("Transaction ID", lightGrayFont));
            transctnIdCell.addElement(new Phrase(lXRMTransaction.getGatewayTransactionId(), lightGrayFont));
            transctnIdCell.setBorder(Rectangle.NO_BORDER);
            payMethodTable.addCell(transctnIdCell);

            PdfPTable termsTable = new PdfPTable(1);
            termsTable.setWidthPercentage(99.9f);
            termsTable.setSpacingBefore(25f);
            PdfPCell termsCell = new PdfPCell();
            termsCell.setBorder(Rectangle.NO_BORDER);
//            termsCell.addElement(new Phrase(new Phrase("Terms:", smallLightGrayFont)));
            termsCell.addElement(new Phrase(new Phrase("Notes:", subElementLightGrayFont)));
            termsCell.addElement(new Phrase("* All the amounts and taxes shown are in US Dollars.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* Your bank/card statement will show a charge from NetElixir.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* This is a system generated invoice. No signature and/or stamp is required.", smallLightGrayFont));
            termsCell.addElement(new Phrase("* In case of overdue/ defaults, the right to deactivate your services, is reserved.", smallLightGrayFont));
            termsTable.addCell(termsCell);

            pdfDoc.add(header);
            pdfDoc.add(invoiceAmtTable);
            pdfDoc.add(amtDetailsTable);
            pdfDoc.add(subTotalTable);
            pdfDoc.add(totalAmt);
            pdfDoc.add(payMethodTable);
            pdfDoc.add(termsTable);
            pdfDoc.close();

            return filename;
        } catch (DocumentException | IOException e) {
            LOGGER.error("DocumentException | IOException in generating Invoice for Billing user name id:" + paymentModel.getBillingUserName() + ", email id:: " + paymentModel.getBillingEmailId() + "\n" + e);
        } catch (Exception e) {
            LOGGER.error("Exception in generating Invoice for Billing user name id:" + paymentModel.getBillingUserName() + ", email id:: " + paymentModel.getBillingEmailId() + "\n" + e);
        }
        return null;
    }

    public PaymentModel createLXRMCreditCard(HttpServletRequest request, Login user) {
        PaymentModel paymentModel = new PaymentModel();
        boolean inValidateInputs = Boolean.FALSE;

        if (request.getParameter("firstName") != null) {
            paymentModel.setFullName(request.getParameter("firstName").trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (request.getParameter("cardNo") != null) {
            paymentModel.setCardNo(request.getParameter("cardNo").trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (request.getParameter("cvv") != null) {
            paymentModel.setCvv(request.getParameter("cvv").trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (request.getParameter("expireMonth") != null) {
            paymentModel.setExpireMonth(Integer.parseInt(request.getParameter("expireMonth").trim()));
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (request.getParameter("expireYear") != null) {
            paymentModel.setExpireYear(Integer.parseInt(request.getParameter("expireYear").trim()));
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (request.getParameter("type") != null) {
            paymentModel.setCardType(request.getParameter("type").trim());
        }
        if (paymentModel.getCardNo() != null) {
            paymentModel.setCardType(getPaymentCardType(paymentModel.getCardNo()));
        }
        if (request.getParameter("billingName") != null) {
            paymentModel.setBillingUserName(request.getParameter("billingName").trim());
        } else if (user.getName() != null) {
            paymentModel.setBillingUserName(user.getName());
        }
        if (request.getParameter("billingEmail") != null) {
            paymentModel.setBillingEmailId(request.getParameter("billingEmail").trim());
        } else if (user.getUserName() != null) {
            paymentModel.setBillingEmailId(user.getUserName());
        }
        paymentModel.setPaymentDate(LocalDateTime.now());
        paymentModel.setPayerUserId(user.getId());
        paymentModel.setSaveUserId(Boolean.TRUE);
        if (paymentModel.getCardType() == null) {
            inValidateInputs = Boolean.TRUE;
        }
        /*Validating user details object, 
        to avoid empty data if user is auto login using cookies*/
        if (paymentModel.getPayerUserId() == 0) {
            inValidateInputs = Boolean.TRUE;
        }
        if (paymentModel.getBillingEmailId() == null) {
            inValidateInputs = Boolean.TRUE;
        }
        if (paymentModel.getBillingUserName() == null) {
            inValidateInputs = Boolean.TRUE;
        }
        if (inValidateInputs) {
            paymentModel = null;
        }
        return paymentModel;
    }

}
