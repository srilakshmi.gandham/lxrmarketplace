/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import java.time.LocalDate;

/**
 *
 * @author vemanna
 */
public class TransactionStats {

    private String paymentId;
    private String transactionId;
    private String amount;
    private String currency;
    private String state;
    private String intent;
    private String createTime;
    private String updateTime;
    private String AuthorizationStatus;
    private String failureMessage;

    public String getAuthorizationStatus() {
        return AuthorizationStatus;
    }

    public void setAuthorizationStatus(String AuthorizationStatus) {
        this.AuthorizationStatus = AuthorizationStatus;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    private long brainTreePaymentId;
    private LocalDate brainTreeCreateTime ;

    public long getBrainTreePaymentId() {
        return brainTreePaymentId;
    }

    public void setBrainTreePaymentId(long brainTreePaymentId) {
        this.brainTreePaymentId = brainTreePaymentId;
    }

    public LocalDate getBrainTreeCreateTime() {
        return brainTreeCreateTime;
    }

    public void setBrainTreeCreateTime(LocalDate brainTreeCreateTime) {
        this.brainTreeCreateTime = brainTreeCreateTime;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }
    
}