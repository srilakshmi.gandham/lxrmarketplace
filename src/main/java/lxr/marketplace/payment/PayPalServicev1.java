/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Authorization;
import com.paypal.api.payments.Capture;
import com.paypal.api.payments.CreditCard;
import com.paypal.api.payments.FundingInstrument;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Transaction;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.PayPalRESTException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class PayPalServicev1 implements GatewayService {

    private static Logger logger = Logger.getLogger(PayPalServicev1.class);

    @Autowired
    private Properties paypalProperties;

    @Override
    public TransactionStats chargeAmount(LxrMCreditCard lxrCreditCard, String totalAmount, String[] errorMessage) {
        TransactionStats transactionStats = null;
        try {
            Capture responseCapture = null;
            Authorization authorization = authorizeCreditCard(lxrCreditCard, totalAmount);
            if (authorization != null && authorization.getState().equals("authorized")) {
                APIContext apiContext = createApiContext();
                if (apiContext != null) {
                    Capture capture = new Capture();
                    Amount amount = new Amount();
                    amount.setCurrency(authorization.getAmount().getCurrency());
                    amount.setTotal(authorization.getAmount().getTotal());
                    capture.setAmount(amount);
                    capture.setIsFinalCapture(true);
                    try {
                        responseCapture = authorization.capture(apiContext, capture);
                        /* Create transactionStats object from Capture*/
                        transactionStats = createObjectFromCapture(responseCapture, GatewayService.CAPTURE_INTENT);
                    } catch (PayPalRESTException e) {
                        logger.error("Paypal Rest API Exception when capturing an authorized amount.\n"
                                + "Failure Message: " + e.getMessage());
                        errorMessage[0] = e.getMessage();
                    }
                }

            } else {
                errorMessage[0] = GatewayService.AUTH_FAILURE;
            }

        } catch (Exception e) {
            logger.info("Excepton while capture of amount", e);
        }
        return transactionStats;
    }

    private Authorization authorizeCreditCard(LxrMCreditCard lxrCreditCard, String totalAmount) {
        CreditCard creditCard = getPayPalCardFromLxrMCard(lxrCreditCard);
        StringBuilder sb = new StringBuilder();
        Amount amount = new Amount();
        amount.setCurrency("USD");
        amount.setTotal(totalAmount);
        Authorization authorization = getCardAuthorization(creditCard, amount, sb);
        if (authorization != null && authorization.getState().equals("authorized")) {
            CreditCard credit = saveCreditCard(lxrCreditCard);
            createLxrCreditCardObject(credit, lxrCreditCard);
        }

        return authorization;
    }

    private CreditCard getPayPalCardFromLxrMCard(LxrMCreditCard lxrCreditCard) {
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(lxrCreditCard.getCardNo());
        creditCard.setExpireMonth(lxrCreditCard.getExpireMonth());
        creditCard.setExpireYear(lxrCreditCard.getExpireYear());
        creditCard.setFirstName(lxrCreditCard.getFirstName());
        creditCard.setLastName(lxrCreditCard.getLastName());
        creditCard.setType(lxrCreditCard.getType());

        return creditCard;
    }

    private CreditCard saveCreditCard(LxrMCreditCard lxrCreditCard) {

        CreditCard savedCreditCard = null;
        String accessToken = createAccessToken();
        CreditCard creditCard = getPayPalCardFromLxrMCard(lxrCreditCard);
        try {
            savedCreditCard = creditCard.create(accessToken);
        } catch (PayPalRESTException ex) {
            logger.error("Exception in saving details in paypal.\nERROR MESSAGE: ", ex);
        }

        if (savedCreditCard != null) {
            logger.info("PayPal Details are saved for user  " + lxrCreditCard.getUserId());
        } else {
            logger.error("PayPal Details not saved for user  " + lxrCreditCard.getUserId());
        }
        return savedCreditCard;
    }

    private Authorization getCardAuthorization(CreditCard creditCard, Amount amount, StringBuilder sb) {
        APIContext apiContext = createApiContext();
        Authorization authorization = null;
        logger.info("Authorizing card for new user " + creditCard.getFirstName() + " with amount = $" + amount.getTotal());
        // ###Details
        // Let's you specify details of a payment amount.

        // ###Amount
        // Let's you specify a payment amount.
        // ###Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setDescription("LXRMarketplace authorization for new user" + creditCard.getFirstName());

        // The Payment creation API requires a list of
        // Transaction; add the created `Transaction`
        // to a List
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        // ###FundingInstrument
        // A resource representing a Payeer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        FundingInstrument fundingInstrument = new FundingInstrument();
        fundingInstrument.setCreditCard(creditCard);

        // The Payment creation API requires a list of
        // FundingInstrument; add the created `FundingInstrument`
        // to a List
        List<FundingInstrument> fundingInstruments = new ArrayList<>();
        fundingInstruments.add(fundingInstrument);

        // ###Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        Payer payer = new Payer();
        payer.setFundingInstruments(fundingInstruments);
        payer.setPaymentMethod("credit_card");

        // ###Payment
        // A Payment Resource; create one using
        // the above types and intent as 'authorize'
        Payment payment = new Payment();
        payment.setIntent("authorize");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        Payment responsePayment;
        int iteration = 3;
        boolean authorized = false;
        boolean otherException = false;
        while (iteration > 0 && !authorized && !otherException) {
            try {
                responsePayment = payment.create(apiContext);
                authorization = responsePayment.getTransactions().get(0).getRelatedResources().get(0).getAuthorization();
                logger.info("authorization" + authorization);
                authorized = true;
            } catch (PayPalRESTException e) {
                //TODO: needs exception handling
                sb.append(e.getMessage());
                logger.error("AUTHORIZATION ERROR MESSAGE: ", e);
                logger.error("!!Exception in authorizing details for user: " + creditCard.getFirstName());

                if (!e.getMessage().contains("Read timed out")) {//TODO: check for internal server error exception
                    otherException = true;
                    //TODO: add error message properly depending on exception
                } else {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e1) {
                        logger.error("Exception authorization thread: ", e1);
                    }
                }
            } catch (Exception e) {
                sb.append("Exception in authorising the Card");
                otherException = true;
            }
            iteration--;
        }
        return authorization;
    }

    private String createAccessToken() {
        String accessToken = null;
        if (paypalProperties != null) {
            Map<String, String> map = new HashMap<>();
            for (String key : paypalProperties.stringPropertyNames()) {
                map.put(key, paypalProperties.getProperty(key));
            }
            String clientId = paypalProperties.getProperty("clientID");
            String clientSecret = paypalProperties.getProperty("clientSecret");

            try {
                OAuthTokenCredential tokenCredential = new OAuthTokenCredential(clientId, clientSecret, map);
                accessToken = tokenCredential.getAccessToken();
            } catch (PayPalRESTException e1) {
                logger.error("Exception in creating accessToken for paypal request.", e1);
            } catch (NullPointerException e) {
                logger.error("Exception in creating accessToken for paypal request due to null value.", e);
            }
        }
        return accessToken;
    }

    /**
     * Creating API Context needed for requesting PayPal rest API It first
     * creates access token then creates apicontext from it
     *
     * @return
     */
    private APIContext createApiContext() {
        APIContext apiContext = null;
        String accessToken = createAccessToken();

        if (accessToken != null) {
            try {
                apiContext = new APIContext(accessToken);
            } catch (Exception e) {
                logger.error("PayPal apicontext is not created due to exception", e);
            }
        }
        return apiContext;
    }

    private TransactionStats createObjectFromCapture(Capture capture, String intent) {
        TransactionStats transactionStats = new TransactionStats();
        transactionStats.setTransactionId(capture.getId());
        transactionStats.setAmount(capture.getAmount().getTotal());
        transactionStats.setCurrency(capture.getAmount().getCurrency());
        transactionStats.setState(capture.getState());
        transactionStats.setIntent(intent);
        transactionStats.setUpdateTime(capture.getUpdateTime());
        transactionStats.setCreateTime(capture.getCreateTime());
        return transactionStats;
    }

    public LxrMCreditCard createLxrCreditCardObject(CreditCard creditCard, LxrMCreditCard lxrCreditCard) {

        lxrCreditCard.setPayPalCardId(creditCard.getId());
        lxrCreditCard.setCardNo(creditCard.getNumber());
        lxrCreditCard.setExpireMonth(creditCard.getExpireMonth());
        lxrCreditCard.setExpireYear(creditCard.getExpireYear());
        lxrCreditCard.setFirstName(creditCard.getFirstName());
        lxrCreditCard.setLastName(creditCard.getLastName());
        lxrCreditCard.setType(creditCard.getType());
        lxrCreditCard.setValidUntil(creditCard.getValidUntil());
        lxrCreditCard.setServiceProvider(LxrMCreditCard.PAYPAL);

        return lxrCreditCard;
    }

    @Override
    public PaymentModel authorizeUserPaymentCard(PaymentModel lxrCreditCard) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LXRMTransaction chargeAmount(PaymentModel lxrCreditCard) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}