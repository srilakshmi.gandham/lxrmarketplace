/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import lxr.marketplace.paypalpayments.LxrMCreditCard;

/**
 *
 * @author vemanna/Sagar
 */
public interface GatewayService {

    String AUTH_FAILURE = "authorization failure.";
    String CAPTURE_FAILURE = "capture failure.";
    String AUTH_INTENT = "authorization";
    String AUTHORIZED = "authorized";
    String CAPTURE_INTENT = "capture";

    String GATEWAY_BRAINTREE = "Braintree";
    String GATEWAY_PAYPAL = "Paypal";

    TransactionStats chargeAmount(LxrMCreditCard lxrCreditCard, String totalAmount, String[] errorMessage);

    PaymentModel authorizeUserPaymentCard(PaymentModel lxrCreditCard);

    LXRMTransaction chargeAmount(PaymentModel lxrCreditCard);

}