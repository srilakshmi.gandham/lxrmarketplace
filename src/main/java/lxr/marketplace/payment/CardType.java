/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import java.util.regex.Pattern;

/**
 *
 * @author vemanna
 */
public enum CardType {
    UNKNOWN,
    VISA("^4[0-9]{12}(?:[0-9]{3})?$"),
    MASTERCARD("^5[1-5][0-9]{14}$"),
    AMERICAN_EXPRESS("^3[47][0-9]{13}$"),
    DINERS_CLUB("^3(?:0[0-5]|[68][0-9])[0-9]{11}$"),
    DISCOVER("^6(?:011|5[0-9]{2})[0-9]{12}$"),
    JCB("^(?:2131|1800|35\\d{3})\\d{11}$");

    private final Pattern pattern;

    CardType() {
        this.pattern = null;
    }

    CardType(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    public static String detect(String cardNumber) {

        for (CardType cardType : CardType.values()) {
            if (null == cardType.pattern) {
                continue;
            }
            if (cardType.pattern.matcher(cardNumber).matches()) {
                switch (cardType) {
                    case VISA:
                        return "visa";
                    case MASTERCARD:
                        return "mastercard";
                    case AMERICAN_EXPRESS:
                        return "amex";
                    case JCB:
                        return "jcb";
                    case DINERS_CLUB:
                        return "dinnersclub";
                    case DISCOVER:
                        return "discover";
                    default:
                        return "unknown";
                }

            }
        }

        return "unknown";
    }
}