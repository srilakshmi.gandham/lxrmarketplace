/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import java.io.Serializable;

/**
 *
 * @author vidyasagar.korada
 */
public class BackLinkAPPPayment implements Serializable{

    private long userId;
    private int toolId;

    private String email;
    private String name;
    private String cardNumber;
    private String cvv;
    private double amount;
    private int expiryMonth;
    private int expiryYear;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getToolId() {
        return toolId;
    }

    public void setToolId(int toolId) {
        this.toolId = toolId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(int expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public int getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(int expiryYear) {
        this.expiryYear = expiryYear;
    }

    @Override
    public String toString() {
        return "BackLinkAPPPayment{" + "userId=" + userId + ", toolId=" + toolId + ", email=" + email + ", name=" + name + ", cardNumber=" + cardNumber + ", cvv=" + cvv + ", amount=" + amount + ", expiryMonth=" + expiryMonth + ", expiryYear=" + expiryYear + '}';
    }

  
}
