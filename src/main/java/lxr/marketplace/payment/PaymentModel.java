/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.payment;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author vidyasagar.korada
 */
public class PaymentModel implements Serializable {

    /*Transcation Parameters*/
    private String serviceProviderCardId;

    /*Payment Parameters*/
    private String fullName;

    private String cardNo;

    private String cardType;

    private String cvv;

    private int expireMonth;

    private int expireYear;

    private double price;

    private String promoCode;

    /*DAO parameters*/
    private String currencyType;

    private long payerUserId;

    private LocalDateTime paymentDate;

    /*Invoice parameters*/
    private String billingUserName;

    private String billingEmailId;

    private String description;

    private String plan;

    private int toolId;

    private String toolName;

    private boolean sendInvoice;
    
    /*Using for Invoice purpose.
    i.e date pattern is same for LXRM tools/ Mobile apps
    Different for dashly purpose
    Recommended by Manager*/
    private String datePattern;
    
    private boolean saveUserId;
    
      
    public String getServiceProviderCardId() {
        return serviceProviderCardId;
    }

    public void setServiceProviderCardId(String serviceProviderCardId) {
        this.serviceProviderCardId = serviceProviderCardId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public int getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(int expireMonth) {
        this.expireMonth = expireMonth;
    }

    public int getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(int expireYear) {
        this.expireYear = expireYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public long getPayerUserId() {
        return payerUserId;
    }

    public void setPayerUserId(long payerUserId) {
        this.payerUserId = payerUserId;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getBillingUserName() {
        return billingUserName;
    }

    public void setBillingUserName(String billingUserName) {
        this.billingUserName = billingUserName;
    }

    public String getBillingEmailId() {
        return billingEmailId;
    }

    public void setBillingEmailId(String billingEmailId) {
        this.billingEmailId = billingEmailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getToolId() {
        return toolId;
    }

    public void setToolId(int toolId) {
        this.toolId = toolId;
    }

    public boolean isSendInvoice() {
        return sendInvoice;
    }

    public void setSendInvoice(boolean sendInvoice) {
        this.sendInvoice = sendInvoice;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public boolean isSaveUserId() {
        return saveUserId;
    }

    public void setSaveUserId(boolean saveUserId) {
        this.saveUserId = saveUserId;
    }
    
    

}