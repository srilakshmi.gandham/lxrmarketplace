/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.home;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.knowledgebase.KnowledgeBase;
import lxr.marketplace.user.CookieService;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class HomePageController {

    private static Logger LOGGER = Logger.getLogger(HomePageController.class);

    @Autowired
    private HomePageService homePageService;
    @Autowired
    private CookieService cookieService;
    @Autowired
    private String domainName;
    @Autowired
    ToolService toolService;

    @RequestMapping(value = "tool-showcase.html", method = RequestMethod.POST)
    public @ResponseBody
    String onSubmit(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam("sortingValue") String sortByInfo) {

        if (sortByInfo != null) {
            Map<Integer, Tool> reSortTools = new LinkedHashMap<>();
            if (sortByInfo.equalsIgnoreCase("byRating") || sortByInfo.equalsIgnoreCase("byReview")
                    || sortByInfo.equalsIgnoreCase("byNewToolFirst") || sortByInfo.equalsIgnoreCase("byMostUsage")) {
                Map<Integer, Tool> totalTools = toolService.getToolsBySorting(sortByInfo);
                totalTools.entrySet().stream().forEach((entry) -> {
                    Tool tools = entry.getValue();
                    reSortTools.put(tools.getToolId(), tools);
                });
                session.setAttribute("sortbyInfo", sortByInfo);
                session.setAttribute("totalTools", reSortTools);
            } else if (sortByInfo.equalsIgnoreCase("byDefault")) {
                session.removeAttribute("sortbyInfo");
                session.removeAttribute("totalTools");
            }
        }
        return null;
    }

    @RequestMapping(value = "default-login.html", method = RequestMethod.POST)
    public @ResponseBody
    Object[] defaultUserLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ServletException {
        Object[] data = new Object[1];
        Cookie cookie = cookieService.getIsLoggedInCookie(response, request);
        if (cookie != null) {
            Long isLoginRememberId = new Long(new Random().nextInt(9999 - 1000) + 1000);
            if (isLoginRememberId > 0) {
                String userIdString = request.getParameter("userId");
                if (userIdString != null) {
                    data[0] = "success";
                    session.setAttribute("userLoggedIn", true);
                }
            }
        } else {
            data[0] = "";
        }
        return data;
    }

    @RequestMapping(value = "tool-qusetions-count.html", method = RequestMethod.POST)
    public @ResponseBody
    Map<Long, Integer> getToolsCount(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Map<Long, Integer> toolQuestionsCount = homePageService.getToolsQuestionCount();
        session.setAttribute("toolQuestionsCount", toolQuestionsCount);
        return toolQuestionsCount;
    }

    @RequestMapping(value = "related-tool-qusetions.html", method = RequestMethod.POST)
    public @ResponseBody
    List<KnowledgeBase> getRelatedToolQuestions(HttpServletRequest request, HttpServletResponse response,
            HttpSession session, Model model) {
        long toolId = Long.parseLong(request.getParameter("toolId"));
        List<KnowledgeBase> relatedToolQuestions = homePageService.getRetaledToolsQuestions(toolId);
        return relatedToolQuestions;
    }

    //When user comes from mail/admin page get the user related data
    @RequestMapping(value = "user-analysis-data.html", method = RequestMethod.GET)
    public @ResponseBody
    Object[] userAutoLogin(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) {
        Object[] data = new Object[6];
        SqlRowSet sqlRowSet = null;
        try {
            if (request.getParameter("user-id") != null && request.getParameter("tracking-id") != null) {
                long decriptedUserId;
                long decriptedWebsiteTrackingId;
                EncryptAndDecryptUsingDES encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
                decriptedUserId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(request.getParameter("user-id")));
                decriptedWebsiteTrackingId = Long.parseLong(encryptAndDecryptUsingDES.decrypt(request.getParameter("tracking-id")));
                session.setAttribute("user-id", decriptedUserId);
                session.setAttribute("tracking-id", decriptedWebsiteTrackingId);
                sqlRowSet = homePageService.checkUserIdAndTrackingId(decriptedUserId, decriptedWebsiteTrackingId);
                if (sqlRowSet != null) {
                    while (sqlRowSet.next()) {
                        data[0] = domainName + sqlRowSet.getString("tool_url") + "&userId=" + request.getParameter("user-id")
                                + "&tracking-id=" + request.getParameter("tracking-id");
                        data[1] = sqlRowSet.getString("website");
                        data[2] = sqlRowSet.getString("user_input");
                        data[3] = sqlRowSet.getString("email");
                        data[4] = sqlRowSet.getString("password");
                        data[5] = sqlRowSet.getString("name");
                    }
                } else {
                    data[0] = null;
                }
            } else if (request.getParameter("question-id") != null) {
                long questionId = Long.parseLong(request.getParameter("question-id"));
                session.setAttribute("question-id", questionId);
                sqlRowSet = homePageService.checkQuestionId(Long.parseLong(request.getParameter("question-id")));
                if (sqlRowSet != null) {
                    while (sqlRowSet.next()) {
                        data[0] = "";
                        data[1] = sqlRowSet.getString("domain");
                        data[2] = sqlRowSet.getString("other_inputs");
                    }
                } else {
                    data[0] = null;
                }
            }
        } catch (NumberFormatException | InvalidResultSetAccessException e) {
            data[0] = null;
            LOGGER.error(e);
        }
        session.setAttribute("requestFromMail", true);
        return data;
    }
}
