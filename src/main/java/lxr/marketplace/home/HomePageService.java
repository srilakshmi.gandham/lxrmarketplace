/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.home;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lxr.marketplace.admin.knowledgebase.KnowledgeBase;
import lxr.marketplace.feedback.FeedbackService;
import org.apache.log4j.Logger;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import java.sql.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class HomePageService {

    private static Logger logger = Logger.getLogger(FeedbackService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Map<Long, Integer> getToolsQuestionCount() {
        final Map<Long, Integer> toolQuestionsCount = new LinkedHashMap<>();
        String query = "select tool_id, count(*) from lxrm_knowledgebase_questions_tools_map qm, lxrm_knowledgebase_questions q "
                + "where qm.question_id = q.question_id and q.question_type = 2 and q.active = 1 group by qm.tool_id";
        try {
            SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(query);
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    toolQuestionsCount.put(sqlRowSet.getLong(1), sqlRowSet.getInt(2));
                }
            }
        } catch (Exception e) {
            logger.error("Exceptrion in fetching toolid and no. of questions", e);
        }
        return toolQuestionsCount;
    }

    public List<KnowledgeBase> getRetaledToolsQuestions(long toolId) {
        List knowledgeBaseList = new ArrayList<>();
        String query = "select q.* from lxrm_knowledgebase_questions_tools_map qm, lxrm_knowledgebase_questions q "
                + "where qm.question_id = q.question_id and q.question_type = 2 and q.active = 1 and qm.tool_id=" + toolId;
        try {

            knowledgeBaseList = this.jdbcTemplate.query(query, (ResultSet rs, int i) -> {
                KnowledgeBase knowledgeBase = new KnowledgeBase();
                knowledgeBase.setQuestionID(rs.getInt("question_id"));
                knowledgeBase.setQuestion(rs.getString("question"));
                String answer = new String(rs.getBytes("answer"));
                knowledgeBase.setAnswer(answer);
                knowledgeBase.setQuestionType(rs.getShort("question_type"));
                knowledgeBase.setActiveType(rs.getShort("active"));
                return knowledgeBase;
            });
        } catch (Exception e) {
            logger.error("Exceptrion in fetching tool related questions:", e);
        }
        return knowledgeBaseList;
    }

    public SqlRowSet checkUserIdAndTrackingId(long userId, long websiteTrackingId) {
        boolean userStatus = false;
        SqlRowSet sqlRowSet = null;
        String query = "SELECT count(*) FROM lxrm_websites_tracking WHERE lxrm_user_id = ? AND website_id = ?";
        Object[] params = {userId, websiteTrackingId};
        try {
            int count = jdbcTemplate.queryForObject(query, params, Integer.class);
            if (count > 0) {
                userStatus = true;
            }
        } catch (Exception e) {
            logger.error("Exception when checking the user info in lxrm_websites_tracking table: " + e.getMessage(), e);
        }
        if (userStatus) {
            query = "SELECT u.email, u.password, u.name, t.tool_url, t.lxrm_user_id, t.website_id, t.website, t.user_input "
                    + "FROM lxrm_websites_tracking t, user u WHERE id = ? AND lxrm_user_id = ? AND website_id = ?";
            sqlRowSet = jdbcTemplate.queryForRowSet(query, new Object[]{userId, userId, websiteTrackingId});
        }
        return sqlRowSet;
    }

    public SqlRowSet checkQuestionId(long questionId) {
        SqlRowSet sqlRowSet = null;
        String query = "SELECT other_inputs, domain FROM lxrm_ate_questions WHERE question_id = ?";
        Object[] params = {questionId};
        try {
            sqlRowSet = jdbcTemplate.queryForRowSet(query, params);
        } catch (Exception e) {
            logger.error("Exception when checking the user info in lxrm_ate_questions table: ", e);
        }
        return sqlRowSet;
    }
    
    
}
