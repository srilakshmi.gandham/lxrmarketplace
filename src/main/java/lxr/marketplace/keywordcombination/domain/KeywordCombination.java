package lxr.marketplace.keywordcombination.domain;

public class KeywordCombination {

    String colOne;
    String colTwo;
    String colThree;
    String prefix;
    String suffix;
    String[] matchType;
    String[] matchTypeNegative;
    String searchEngine;
    double maxBid;
    String url;
    boolean powerPost;
    String advanced;

    public String[] getMatchType() {
        if (matchType == null) {
            matchType = new String[3];
            matchType[0] = "Broad";
        }
        return matchType;
    }

    public void setMatchType(String[] matchType) {
        this.matchType = matchType;
    }

    public String[] getMatchTypeNegative() {
        return matchTypeNegative;
    }

    public void setMatchTypeNegative(String[] matchTypeNegative) {
        this.matchTypeNegative = matchTypeNegative;
    }

    public String getColOne() {
        return this.colOne;
    }

    public void setColOne(String colOne) {
        this.colOne = colOne;
    }

    public String getColTwo() {
        return this.colTwo;
    }

    public void setColTwo(String colTwo) {
        this.colTwo = colTwo;
    }

    public String getColThree() {
        return this.colThree;
    }

    public void setColThree(String colThree) {
        this.colThree = colThree;
    }

    public String getPrefix() {
        return (prefix == null) ? "" : prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix.trim();
    }

    public String getSuffix() {
        return (suffix == null) ? "" : suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix.trim();
    }

    public String getSearchEngine() {
        return (searchEngine == null) ? "" : searchEngine;
    }

    public void setSearchEngine(String searchEngine) {
        this.searchEngine = searchEngine;
    }

    public double getMaxBid() {
        return (maxBid == 0.00) ? 0.01 : maxBid;
    }

    public void setMaxBid(double maxBid) {
        this.maxBid = maxBid;
    }

    public String getUrl() {
        return (url == null) ? "" : url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPowerPost() {
        return powerPost;
    }

    public void setPowerPost(boolean powerPost) {
        this.powerPost = powerPost;
    }

    public String getAdvanced() {
        return advanced;
    }

    public void setAdvanced(String advanced) {
        this.advanced = advanced;
    }

}
