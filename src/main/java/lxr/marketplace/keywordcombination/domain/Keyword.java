package lxr.marketplace.keywordcombination.domain;

public class Keyword {
	String keyword;
	String matchType;
	boolean isNegative;
	double cpc;
	String url;
	String searchEng; 
	String status;
	
	
	public boolean isNegative() {
		return isNegative;
	}
	public void setNegative(boolean isNegative) {
		this.isNegative = isNegative;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public double getCpc() {
		return cpc;
	}
	public void setCpc(double cpc) {
		this.cpc = cpc;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setSearchEng(String searchEng) {
		this.searchEng = searchEng;
	}
	public String getSearchEng() {
		return (searchEng==null)?"":searchEng;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return (status==null)?"":status;
	}
}
