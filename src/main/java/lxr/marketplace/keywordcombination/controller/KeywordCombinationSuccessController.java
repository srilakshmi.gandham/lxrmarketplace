package lxr.marketplace.keywordcombination.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.keywordcombination.domain.Keyword;
import lxr.marketplace.keywordcombination.domain.KeywordCombination;
import lxr.marketplace.keywordcombination.service.KeywordCombinationService;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class KeywordCombinationSuccessController extends SimpleFormController{
	private static Logger logger = Logger.getLogger(KeywordCombinationSuccessController.class);
	KeywordCombinationService keywordCombinationService;
	LoginService loginService;
	String downloadFolder;
	
	public KeywordCombinationSuccessController(){
		setCommandClass(KeywordCombination.class);
		setCommandName("keywordCombination");
	}	
	
	public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }
	public void setKeywordCombinationService(KeywordCombinationService keywordCombinationService){
		this.keywordCombinationService = keywordCombinationService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,	HttpServletResponse response,
			Object command, BindException errors) throws Exception {
		HttpSession session = request.getSession();	
		Login user = (Login) session.getAttribute("user");
		boolean userLoggedIn = (Boolean)session.getAttribute("userLoggedIn");
		KeywordCombination keywordCombination = (KeywordCombination)command;
		KeywordCombination keywordCombination1 =(KeywordCombination)session.getAttribute("keywordCombination");
	//	logger.info("bean values are   :"+keywordCombination1.getColOne()+" " +keywordCombination1.getColTwo());
		Tool toolObj = new Tool();
		toolObj.setToolId(2);
		toolObj.setToolName("PPC Tool : Keyword Combinator");
		toolObj.setToolLink("ppc-keyword-combination-tool.html");
		session.setAttribute("toolObj",toolObj);
		session.setAttribute("currentTool", Common.KEYWORD_COMBINATOR);
       if (WebUtils.hasSubmitParameter(request,"download")){
			//String appPath = session.getServletContext().getInitParameter("path") + "temp/";
			ArrayList<Keyword> keywordList = (ArrayList<Keyword>)session.getAttribute("keywordList");
			logger.info("inside of download "+keywordList);
			String fileName = keywordCombinationService.createExcelFile(keywordList, downloadFolder);
	       /* File f = new File(fileName);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
			FileInputStream fis = new FileInputStream(f);
			FileCopyUtils.copy(fis, response.getOutputStream());*/
			String downParam = request.getParameter("download");
			String justFileName = fileName.substring(downloadFolder.length());
			if(downParam.equalsIgnoreCase("'download'")){
//		    	if(!userLoggedIn){
//		    		logger.info(" inside of userloggedin");
//		    		ModelAndView mAndV = new ModelAndView(getSuccessView(), "keywordCombination", keywordCombination1);
//					return mAndV;
//		    	}
				String fileType="";
				Common.downloadReport(response,downloadFolder, justFileName,fileType);
				return null;
			}else if(downParam.equalsIgnoreCase("'sendmail'")){
				logger.info("Sending report through mail");
				String toAddrs = request.getParameter("email").toString();
				String toolName = "PPC Keyword Combinator";//TODO: change needed
				Common.sendReportThroughMail(request, downloadFolder, justFileName, toAddrs, toolName);
				return null;
			}
			ModelAndView mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination1);
			return mAndV;
		}else if (WebUtils.hasSubmitParameter(request,"back")){
			response.sendRedirect("/keywordcombination.html?back=back");
		}
		return null;
        
 	}
	
}
