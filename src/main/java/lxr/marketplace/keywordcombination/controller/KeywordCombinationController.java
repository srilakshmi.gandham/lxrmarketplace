package lxr.marketplace.keywordcombination.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.keywordcombination.domain.Keyword;
import lxr.marketplace.keywordcombination.domain.KeywordCombination;
import lxr.marketplace.keywordcombination.service.KeywordCombinationService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class KeywordCombinationController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(KeywordCombinationController.class);
    KeywordCombinationService keywordCombinationService;
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    LoginService loginService;
    String downloadFolder;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    JSONObject userInputJson = null;

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public KeywordCombinationController() {
        setCommandClass(KeywordCombination.class);
        setCommandName("keywordCombination");
    }

    public void setKeywordCombinationService(KeywordCombinationService keywordCombinationService) {
        this.keywordCombinationService = keywordCombinationService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
            Object command, BindException errors) throws Exception {
        HttpSession session = request.getSession();
        KeywordCombination keywordCombination = (KeywordCombination) command;
        Login user = (Login) session.getAttribute("user");
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(2);
            toolObj.setToolName("PPC Tool : Keyword Combinator");
            toolObj.setToolLink("ppc-keyword-combination-tool.html");
            session.setAttribute("toolObj", toolObj);
            keywordCombinationService.generateKeywordCombination(keywordCombination);
            session.setAttribute("keywordCombination", keywordCombination);
            session.setAttribute("currentTool", Common.KEYWORD_COMBINATOR);
            ModelAndView mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
            if (WebUtils.hasSubmitParameter(request, "ViewResult")) {
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                    List<String> searchEngineList = new ArrayList<>();
                    searchEngineList.add("Show Results in Google AdWords Format");
                    searchEngineList.add("Show Results in Bing Ads Format");
                    mAndV.addObject("searchEngineList", searchEngineList);
                    mAndV.addObject("getSuccess", "success");
                    return mAndV;
                }

//                if the request is coming from tool page then only add the user inputs to JSONObject
//                if the request is coming from mail then do not add the user inputs to JSONObject
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && user != null && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    userInputJson.put("2", keywordCombination.getColOne());
                    userInputJson.put("3", keywordCombination.getColTwo());
                    userInputJson.put("4", keywordCombination.getColThree());
                    userInputJson.put("5", keywordCombination.getPrefix());
                    userInputJson.put("6", keywordCombination.getSuffix());
                    userInputJson.put("7", keywordCombination.getSearchEngine());
                    userInputJson.put("8", keywordCombination.getMaxBid());
                    userInputJson.put("9", keywordCombination.getUrl());
                    int i = 10;
                    String[] matchType = keywordCombination.getMatchType();
                    for (int j = 0; j < matchType.length; j++) {
                        userInputJson.put("" + i, matchType[j]);
                        i++;
                    }
                }

                Common.modifyDummyUserInToolorVideoUsage(request, response);
                Session userSession = (Session) session.getAttribute("userSession");
                userSession.addToolUsage(toolObj.getToolId());
                session.setAttribute("keywordList", keywordCombinationService.getKeywordList());
                session.setAttribute("keyComb", keywordCombination);
                mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                List<String> searchEngineList = new ArrayList<>();
                searchEngineList.add("Show Results in Google AdWords Format");
                searchEngineList.add("Show Results in Bing Ads Format");
                mAndV.addObject("searchEngineList", searchEngineList);
                mAndV.addObject("getSuccess", "success");

                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "clear")) {
                session.removeAttribute("keywordCombination");
                session.setAttribute("keywordCombination", null);
                mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                List<String> searchEngineList = new ArrayList<>();
                searchEngineList.add("Show Results in Google Adwords Format");
                searchEngineList.add("Show Results in Bing Ads Format");
                mAndV.addObject("clearAll", "clearAll");
                return mAndV.addObject("searchEngineList", searchEngineList);
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                ArrayList<Keyword> keywordList = (ArrayList<Keyword>) session.getAttribute("keywordList");
                logger.debug("inside of download " + keywordList);
//                String fileName = keywordCombinationService.createExcelFile(keywordList, downloadFolder);
//                String fileName = keywordCombinationService.createCSVFile(keywordList, downloadFolder);
//                String downParam = request.getParameter("download");
                try {
                    String fileName = null;
                    String fileType = "";

                    String downParam = request.getParameter("report-type");
                    if (downParam.equalsIgnoreCase("xls")) {
                        fileType = "xls";
                        fileName = keywordCombinationService.createExcelFile(keywordList, downloadFolder);
                        String justFileName = fileName.substring(downloadFolder.length());
                        Common.downloadReport(response, downloadFolder, justFileName, fileType);
                    } else if (downParam.equalsIgnoreCase("csv")) {
                        fileType = "csv";
                        fileName = keywordCombinationService.createCSVFile(keywordList, downloadFolder);
                        String justFileName = fileName.substring(downloadFolder.length());
                        Common.downloadReport(response, downloadFolder, justFileName, fileType);
                    } else if (downParam.equalsIgnoreCase("'sendmail'")) {
                        logger.info("Sending report through mail");
                        String toAddrs = request.getParameter("email");
                        fileName = keywordCombinationService.createCSVFile(keywordList, downloadFolder);
                        String dwnFileName = fileName.substring(downloadFolder.length());
                        String toolName = "PPC Keyword Combinator";//TODO: change needed
                        Common.sendReportThroughMail(request, downloadFolder, dwnFileName, toAddrs, toolName);
                        return null;
                    }

                } catch (Exception e) {
                    logger.error("Exception while downloading keyword combinator report: ", e);
                }
                mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "back")) {
                mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                List<String> searchEngineList = new ArrayList<>();
                searchEngineList.add("Show Results in Google AdWords Format");
                searchEngineList.add("Show Results in Bing Ads Format");
                return mAndV.addObject("searchEngineList", searchEngineList);
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                Session userSession = (Session) session.getAttribute("userSession");
                ArrayList<Keyword> keywordList = (ArrayList<Keyword>) session.getAttribute("keywordList");
                String toolIssues = "";
                String questionInfo = "";
                if (keywordList.size() > 0) {
                    questionInfo = "Need help in fixing following issues.\n\n";
                    questionInfo += "- " + "Audit keyword set for profitability." + "\n";
                    toolIssues += "You can target <span style='color:red;'>" + keywordList.size() + "</span> different keywords. "
                            + "We can help you analyze these keywords to help you find the most profitable options.";
                }
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                lxrmAskExpert.setDomain("");
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(Common.KEYWORD_COMBINATOR);
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), "", toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }

                return null;
            } else {
                if (!WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                    session.setAttribute("keywordCombination", null);
                    keywordCombination = new KeywordCombination();
                }
                mAndV = new ModelAndView(getFormView(), "keywordCombination", keywordCombination);
                List<String> searchEngineList = new ArrayList<>();
                searchEngineList.add("Show Results in Google Adwords Format");
                searchEngineList.add("Show Results in Bing Ads Format");
                return mAndV.addObject("searchEngineList", searchEngineList);
            }
        } else {
            ModelAndView mAndV = new ModelAndView("keywordcombination/KeyCombDescription", "keywordCombination", keywordCombination);
            return mAndV;

        }

    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Map referenceData(HttpServletRequest request) throws Exception {

        Map referenceData = new HashMap();

        //Data referencing for match type checkboxes
        List<String> matchTypeList = new ArrayList<>();
        matchTypeList.add("Broad");
        matchTypeList.add("Phrase");
        matchTypeList.add("Exact");
        referenceData.put("matchTypeList", matchTypeList);

        //Data referencing for match type negative checkboxes
        List<String> matchTypeNegativeList = new ArrayList<>();
        matchTypeNegativeList.add("Broad Negative");
        matchTypeNegativeList.add("Phrase Negative");
        matchTypeNegativeList.add("Exact Negative");
        referenceData.put("matchTypeNegativeList", matchTypeNegativeList);

        //Data referencing for Search Engines radiobuttons
        List<String> searchEngineList = new ArrayList<>();
        searchEngineList.add("Show Results in Google Adwords Format");
        searchEngineList.add("Show Results in Bing Ads Format");
        referenceData.put("searchEngineList", searchEngineList);

        return referenceData;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        KeywordCombination keywordCombination = (KeywordCombination) session.getAttribute("keywordCombination");
        if (keywordCombination == null) {
            keywordCombination = new KeywordCombination();
        }
        return keywordCombination;
    }

}
