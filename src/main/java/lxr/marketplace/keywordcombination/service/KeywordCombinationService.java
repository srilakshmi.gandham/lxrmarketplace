package lxr.marketplace.keywordcombination.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Collections;
import java.util.Set;

import org.apache.log4j.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import static lxr.marketplace.util.Common.*;

import lxr.marketplace.keywordcombination.domain.Keyword;
import lxr.marketplace.keywordcombination.domain.KeywordCombination;
import lxr.marketplace.util.Common;

public class KeywordCombinationService {

    private static Logger logger = Logger.getLogger(KeywordCombinationService.class);
    ArrayList<Keyword> keywordList;
    KeywordCombination keywordCombination;
    String[] matchTypes;
    String[] googleColHeaders = {"Keyword", "Match Type", "Status", "Max CPC", "Destination URL"};
    String[] msnColHeaders = {"Keyword", "Match Type", "Status", "Bid", "Param 1"};
    String[] colHeaders = {"Keyword", "Match Type", "", "", ""};
    Format formatter = new SimpleDateFormat("yyyy_MMM_dd_HHmmss_SSS");

    public ArrayList<Keyword> getKeywordList() {
        keywordList = Common.sort(keywordList);
        return keywordList;
    }

    public void setKeywordList(ArrayList<Keyword> keywordList) {
        this.keywordList = keywordList;
    }

    public KeywordCombination getKeywordCombination() {
        return keywordCombination;
    }

    public void setKeywordCombination(KeywordCombination keywordCombination) {
        this.keywordCombination = keywordCombination;
    }

    public KeywordCombinationService() {

    }

    public KeywordCombinationService(KeywordCombination keywordCombination) {
        this.keywordCombination = keywordCombination;
    }

    public void generateKeywordCombination(KeywordCombination keywordCombination) {
        ArrayList<String> colOneList;
        ArrayList<String> colTwoList;
        ArrayList<String> colThreeList;
        String prefix;
        String suffix;
        double maxBid;
        String destUrl;
        String searchEngine;

        String[] stringArray1 = new String[0];
        String[] stringArray2 = new String[0];
        String[] stringArray3 = new String[0];
        String[] stringArray11 = new String[0];
        String[] stringArray22 = new String[0];
        String[] stringArray33 = new String[0];
        this.keywordCombination = keywordCombination;
        this.matchTypes = keywordCombination.getMatchType();
        Keyword keyword;

        colOneList = convertStringToArrayList(keywordCombination.getColOne(), "\n");
        colTwoList = convertStringToArrayList(keywordCombination.getColTwo(), "\n");
        colThreeList = convertStringToArrayList(keywordCombination.getColThree(), "\n");

        // Generating Keyword Combinations
        stringArray1 = generateCombination(colOneList, colTwoList, colThreeList);
        stringArray11 = generateCombination(colOneList, colThreeList, colTwoList);

        stringArray2 = generateCombination(colTwoList, colOneList, colThreeList);
        stringArray22 = generateCombination(colTwoList, colThreeList, colOneList);

        stringArray3 = generateCombination(colThreeList, colTwoList, colOneList);
        stringArray33 = generateCombination(colThreeList, colOneList, colTwoList);

        String[] stringArray = new String[0];

        stringArray = concat(stringArray1, stringArray2, stringArray3);

        Set<String> keywords = new HashSet<>();
        Collections.addAll(keywords, stringArray1);
        Collections.addAll(keywords, stringArray11);
        Collections.addAll(keywords, stringArray2);
        Collections.addAll(keywords, stringArray22);
        Collections.addAll(keywords, stringArray3);
        Collections.addAll(keywords, stringArray33);
        stringArray = keywords.toArray(new String[]{});

        // adding prefix
        prefix = keywordCombination.getPrefix();
        if (prefix.length() > 0) {
            stringArray = concatPrefix(stringArray, prefix);
        }

        // adding suffix
        suffix = keywordCombination.getSuffix();
        if (suffix.length() > 0) {
            stringArray = concatSuffix(stringArray, suffix);
        }

        //adding max bid and destination URL 
        maxBid = keywordCombination.getMaxBid();
        destUrl = keywordCombination.getUrl();
        searchEngine = keywordCombination.getSearchEngine();

        // Generating Keywords
        keywordList = new ArrayList<>();
        if (matchTypes == null || matchTypes.length == 0) {
            for (String stringArray4 : stringArray) {
                keyword = new Keyword();
                keyword.setKeyword(stringArray4);
                keyword.setMatchType("Broad");
                if (!searchEngine.trim().equals("") && keywordCombination.isPowerPost()
                        && !keywordCombination.getAdvanced().equals("")) {
                    keyword.setCpc(maxBid);
                    keyword.setUrl(destUrl);
                    keyword.setSearchEng(searchEngine);
                    keyword.setStatus("Active");
                }
                keywordList.add(keyword);
            }
        } else {
            for (String matchType : matchTypes) {
                for (String stringArray4 : stringArray) {
                    keyword = new Keyword();
                    keyword.setKeyword(stringArray4);
                    keyword.setMatchType(matchType);
                    if (!searchEngine.trim().equals("") && keywordCombination.isPowerPost()
                            && !keywordCombination.getAdvanced().equals("")) {
                        keyword.setCpc(maxBid);
                        keyword.setUrl(destUrl);
                        keyword.setSearchEng(searchEngine);
                        keyword.setStatus("Active");
                    }
                    keywordList.add(keyword);
                }
            }
        }

    }

    public String[] concat1(String[] A, String[] B, String[] D) {
        String[] C = new String[A.length + B.length + D.length];
        System.arraycopy(A, 0, C, 0, A.length);
        System.arraycopy(B, 0, C, A.length, B.length);
        System.arraycopy(D, 0, C, B.length, D.length);
        return C;
    }

    public String[] concat(String[] A, String[] B, String[] C) {
        String[] result = new String[A.length + B.length + C.length];
        System.arraycopy(A, 0, result, 0, A.length);
        System.arraycopy(B, 0, result, A.length, B.length);
        System.arraycopy(C, 0, result, B.length, C.length);
        return result;
    }

    public String[] generateCombination(ArrayList<String> one,
            ArrayList<String> two, ArrayList<String> three) {

        String[] stringArray = (String[]) one.toArray(new String[one.size()]);
        if (two.size() > 0) {
            stringArray = cartesianJoin(stringArray, (String[]) two.toArray(new String[two.size()]));
        }
        if (three.size() > 0) {
            stringArray = cartesianJoin(stringArray, (String[]) three.toArray(new String[three.size()]));
        }

        return stringArray;
    }

    public String createExcelFile(ArrayList<Keyword> keywordList, String filePath) {
        try {
            String[] colValues;
            HSSFCell cell = null;
            HSSFWorkbook workBook = new HSSFWorkbook();
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            HSSFFont font = workBook.createFont();
            font.setBold(true);
            columnHeaderStyle.setFont(font);
            HSSFSheet sheet = workBook.createSheet();
            int rownum = -1;
            boolean headRow = true;
            int colCount = 2;
            for (Keyword keyword : keywordList) {
                if (!(keyword.getSearchEng().trim().equals(""))) {
                    colCount = 5;
                    if (keyword.getSearchEng().contains("Google")) {
                        colHeaders = googleColHeaders;
                        //}else if(keyword.getSearchEng().contains("Google")){
                    } else {
                        colHeaders = msnColHeaders;
                    }
                }
                if (headRow) {
                    HSSFRow headerRow = sheet.createRow(rownum + 1);
                    for (int i = 0; i < colCount; i++) {
                        cell = headerRow.createCell(i);
                        cell.setCellStyle(columnHeaderStyle);
                        cell.setCellValue(colHeaders[i]);
                    }
                    headRow = false;
                    rownum++;
                }
                HSSFRow row = sheet.createRow(rownum + 1);
                colValues = new String[colCount];
                colValues[0] = keyword.getKeyword();
                colValues[1] = keyword.getMatchType();
                if (colCount == 5) {
                    colValues[2] = keyword.getStatus();
                    colValues[3] = Double.toString(keyword.getCpc());
                    colValues[4] = keyword.getUrl();
                }
                for (int i = 0; i < colCount; i++) {
                    cell = row.createCell(i);
                    cell.setCellValue(colValues[i]);
                }
                rownum++;
            }

            String s = formatter.format(new Date());
            String fileName = filePath + "LXRMarketplace_Keyword_Combinator_" + s + "_"
                    + Calendar.getInstance().getTime().getTime() + ".xls";
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);

            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public String createCSVFile(ArrayList<Keyword> keywordList, String filePath) {
        PrintStream ps = null;
        FileOutputStream stream = null;
        String s = formatter.format(new Date());
        String fileName = filePath + "LXRMarketplace_Keyword_Combinator_" + s + "_" + Calendar.getInstance().getTime().getTime() + ".csv";
        try {
            stream = new FileOutputStream(fileName);
            ps = new PrintStream(stream);
            boolean header = true;
            int colCount = 2;
            for (Keyword keyword : keywordList) {
                if (!(keyword.getSearchEng().trim().equals(""))) {
                    colCount = 5;
                    if (keyword.getSearchEng().contains("Google")) {
                        colHeaders = googleColHeaders;
                    } else {
                        colHeaders = msnColHeaders;
                    }
                }
                if (header) {
                    for (int i = 0; i < colCount; i++) {
                        ps.print(colHeaders[i] + "\t");
                    }
                    ps.println();
                    header = false;
                }
                if (colCount != 5) {
                    ps.print(keyword.getKeyword() + "\t");
                    ps.print(keyword.getMatchType() + "\t");
                    ps.println();
                }
                if (colCount == 5) {
                    ps.append("hello world");
                    ps.print(keyword.getKeyword() + "\t");
                    ps.print(keyword.getMatchType() + "\t");
                    ps.print(keyword.getStatus() + "\t");
                    ps.append("");

                    ps.print(Double.toString(keyword.getCpc()) + "\t");
                    ps.print(keyword.getUrl() + "\t");
                    ps.println();
                }
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }
        return fileName;
    }
}
