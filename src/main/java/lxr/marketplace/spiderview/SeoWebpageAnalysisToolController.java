package lxr.marketplace.spiderview;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.lxrmpayments.PaidDownloadedReportDetails;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ToolService;
import org.json.JSONObject;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

@SuppressWarnings("deprecation")
public class SeoWebpageAnalysisToolController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(SeoWebpageAnalysisToolController.class);
    private EmailTemplateService emailTemplateService;
    private ToolService toolService;
    private SpiderViewService spiderViewService;
    private String downloadFolder;
    private MarketplacePaymentService marketplacePaymentService;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private MessageSource messageSource;
    JSONObject userInputJson = null;

    public void setMarketplacePaymentService(
            MarketplacePaymentService marketplacePaymentService) {
        this.marketplacePaymentService = marketplacePaymentService;
    }

    public void setSpiderViewService(SpiderViewService spiderViewService) {
        this.spiderViewService = spiderViewService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(
            EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public SeoWebpageAnalysisToolController() {
        setCommandClass(SpiderViewTool.class);
        setCommandName("spiderViewTool");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {
        String finalURL = null;
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        SpiderViewTool spiderView = (SpiderViewTool) command;
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (spiderView != null) {
            session.setAttribute("spiderViewTool", spiderView);
        }
        if (user != null) {
            session.setAttribute("currentTool", Common.SEO_WEBPAGE_ANALYSIS);
            if (WebUtils.hasSubmitParameter(request, "getResult") && spiderView != null) {
                String notWorkingUrl = null;
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                    if (session.getAttribute("notWorkingUrlSWA") == null) {
                        mAndV.addObject("getSuccess", "success");
                    }
                    return mAndV;
                }
                session.removeAttribute("loginrefresh");
                session.removeAttribute("toolObj");
                session.removeAttribute("spiderviewresults");
                session.removeAttribute("spiderURL");
                session.removeAttribute("urlStatusCode");
                session.removeAttribute("notWorkingUrl");
                finalURL = Common.getUrlValidation(spiderView.getUrl().trim(), session);
                session.setAttribute("spiderURL", spiderView.getUrl());
                session.setAttribute("Url", finalURL);
                logger.info("The finalURL:: " + finalURL + ", for queryURL:: " + spiderView.getUrl());
                if (finalURL.equals("invalid") || finalURL.equals("redirected")) {
//                    notWorkingUrl = "Your URL is not working, please check the URL.";
                    notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                    ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                    session.setAttribute("notWorkingUrlSWA", notWorkingUrl);
                    mAndV.addObject("notWorkingUrl", notWorkingUrl);
                    mAndV.addObject("getSuccess", "fail");
                    return mAndV;
                } else {
                    boolean jsoupconnerror = spiderViewService.getSpiderViewResults(spiderView, request, finalURL);
                    if (spiderViewService.getSvresult() != null) {
                        session.setAttribute("spiderviewresults", spiderViewService.getSvresult());
                    }
                    Session userSession = (Session) session.getAttribute("userSession");
                    userSession.addToolUsage(8);
                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }

                    //  if the request is coming from tool page then only add the user inputs to JSONObject
                    //  if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                        session.removeAttribute("requestFromMail");
                    }
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson = new JSONObject();
                    }
                    ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                    if (jsoupconnerror) {
//                        mAndV.addObject("connectionerror", " Unable to connect.");
                        notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                        mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                        session.setAttribute("notWorkingUrlSWA", notWorkingUrl);
                        mAndV.addObject("notWorkingUrl", notWorkingUrl);
                        mAndV.addObject("getSuccess", "fail");
                        return mAndV;
                    } else {
                        mAndV.addObject("getSuccess", "success");
                    }
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                logger.info("Downloading report for SeoWebpageAnalysis");
//                session.removeAttribute("toolObj");
                String tempUrl = "";
                double amount = 0;
                String buttName = "";
                session.removeAttribute("valsObjArray");
                session.removeAttribute("tempsuccessdownload");
                session.removeAttribute("tempdownloadpopuplogin");
                if (userLoggedIn) {
                    try {
                        String downParam = request.getParameter("download");
                        String dwnfileName = spiderViewService.createPdfFile(session, downloadFolder, user);
                        Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                        if (dwnfileName != null && !dwnfileName.trim().equals("")) {
                            if (downParam != null && !downParam.trim().equals("")) {
                                Object[] toolInfo = marketplacePaymentService.fetchDataFromToolTab(8, downParam);
                                if (toolInfo != null && toolInfo.length > 0) {
                                    amount = (Double) toolInfo[0];
                                    buttName = (String) toolInfo[1];
                                }
                                tempUrl = (String) session.getAttribute("Url");
                                Object[] vals = {dwnfileName, 8, user.getId(), createdTime, tempUrl, amount, buttName, Common.SEO_WEBPAGE_ANALYSIS, "/seo-webpage-analysis-tool.html", "NA"};
                                session.setAttribute("valsObjArray", vals);
                                logger.info("The Fianl redirect url for payment:: " + (url + "/lxrm-payment.html"));
                                response.sendRedirect(url + "/lxrm-payment.html");
                                return null;
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Exception in downloading file ", e);
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "downloadFile")) {
                if (userLoggedIn) {
                    String downParam = request.getParameter("downloadFile");
                    Object[] vals = (Object[]) session.getAttribute("valsObjArray");
                    if ((String) vals[0] != null) {
                        if (downParam.equalsIgnoreCase("'downloadFile'")) {
                            Common.downloadReport(response, downloadFolder, (String) vals[0], "pdf");
                        } else if (downParam.equalsIgnoreCase("'sendmail'")) {
                            String toAddrs = user.getUserName();
                            String toolName = "SEO Webpage Analysis";
                            Common.sendReportThroughMail(request, downloadFolder, (String) vals[0], toAddrs, toolName);
                            logger.info("Report sent throuth mail");
                            ModelAndView mAndV = new ModelAndView(new RedirectView("/lxrm-thanks.html?ms='ms'"), "spiderViewTool", spiderView);
                            return mAndV;
                        }
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                return mAndV;

            } else if (WebUtils.hasSubmitParameter(request, "prvDownloadFile")) {
                session.removeAttribute("toolObj");
                if (userLoggedIn) {
                    String fileName = request.getParameter("prvDownloadFile");
                    if (fileName != null && Common.checkFileExistance(downloadFolder + fileName)) {
                        Common.downloadReport(response, downloadFolder, fileName, "pdf");
                        return null;
                    } else {
                        List<PaidDownloadedReportDetails> updatedPaidDownloadedReportDetails;
                        PaidDownloadedReportDetails reportToBeDeleted;
                        /*Deleting report from paid download reports and updating new list in session.*/
                        if (session.getAttribute("paidDownloadedReportDetailsList") != null) {
                            updatedPaidDownloadedReportDetails = (List<PaidDownloadedReportDetails>) session.getAttribute("paidDownloadedReportDetailsList");
                            reportToBeDeleted = updatedPaidDownloadedReportDetails.stream().filter(report -> report.getReportName().equalsIgnoreCase(fileName)).collect(Collectors.toList()).get(0);
                            if (reportToBeDeleted != null) {
                                toolService.deletePaidDownladReports(reportToBeDeleted.getReportId());
                                updatedPaidDownloadedReportDetails = toolService.fetchPaidDownladReports(user.getId(), 8);
                                session.setAttribute("paidDownloadedReportDetailsList", updatedPaidDownloadedReportDetails);
                            }
                        }
                    }
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                mAndV.addObject("fileExistence", "Sorry the requested file is no longer existed.");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                /*Cancel the payment process*/
                ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
                mAndV.addObject("getSuccess", "success");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                Session userSession = (Session) session.getAttribute("userSession");
                SpiderViewResult spiderViewResults = null;
                if (session.getAttribute("spiderviewresults") != null) {
                    spiderViewResults = (SpiderViewResult) session.getAttribute("spiderviewresults");
                }
//                SpiderViewResult spiderViewResult = null;
//                spiderViewResult = (SpiderViewResult) session.getAttribute("spiderviewresults");
                if (spiderViewResults != null) {
                    String domain = request.getParameter("domain");
                    String toolIssuesMainContent = "";
                    String toolIssues = "";
                    String questionInfo = "";
                    //  For title tag info
                    List<String> titleData = spiderViewResults.getTitleTag();
                    int length;
                    boolean issueStatus = false;
                    if (titleData.size() > 0 && titleData.toString() != null && !titleData.toString().trim().equals("")) {
                        String stopWordsList = "";
                        for (int i = 1; i < titleData.size(); i++) {
                            switch (i) {
                                case 1:
                                    length = Integer.parseInt(titleData.get(i));
                                    if (length < 41 || length > 65) {
                                        toolIssues += "<li>Title tag is not an optimal length.</li>";
                                        issueStatus = true;
                                    }
                                    break;
                                case 2:
                                    if (Float.parseFloat(titleData.get(i)) < 60) {
                                        toolIssues += "<li>Title tag is not relevant to your page.</li>";
                                        issueStatus = true;
                                    }
                                    break;
                                default:
                                    stopWordsList = stopWordsList + titleData.get(i) + "," + " ";
                                    break;
                            }
                        }
                        if (!stopWordsList.equals("")) {
                            toolIssues += "<li>Title tag has stop words that are ignored by search engines.</li>";
                            issueStatus = true;
                        }
                    } else {
                        toolIssues += "<li>Title tag is missing</li>";
                        issueStatus = true;
                    }
                    //  For meta description info
                    List<String> metaDescription = null;
                    metaDescription = spiderViewResults.getMetaDescriptionTag();
                    if (metaDescription != null && !metaDescription.isEmpty() && metaDescription.toString() != null && !metaDescription.toString().trim().equals("")) {
                        String stopWordsList = "";
                        for (int i = 1; i < metaDescription.size(); i++) {
                            switch (i) {
                                case 1:
                                    if (Integer.parseInt(metaDescription.get(i)) <= 70 || Integer.parseInt(metaDescription.get(i)) >= 161) {
                                        toolIssues += "<li>Meta tag description does not have an optimal length.</li>";
                                        issueStatus = true;
                                    }
                                    break;
                                case 2:
                                    if (Float.parseFloat(metaDescription.get(i)) < 60) {
                                        toolIssues += "<li>Meta tag description is not relevant to your page.</li>";
                                        issueStatus = true;
                                    }
                                    break;
                                default:
                                    stopWordsList = stopWordsList + metaDescription.get(i) + "," + " ";
                                    break;
                            }
                        }
                        if (!stopWordsList.equals("")) {
                            toolIssues += "<li>Meta tag description has stop words that are ignored by search engines.</li>";
                            issueStatus = true;
                        }
                    } else {
                        toolIssues += "<li> Meta tag descriptions are missing.</li>";
                        issueStatus = true;
                    }
                    //  For missing H1 tags
                    Map<Integer, Elements> headingAndPhraseElements = null;
                    headingAndPhraseElements = spiderViewResults.getHeadingAndPhraseElements();
                    if (headingAndPhraseElements != null && !headingAndPhraseElements.isEmpty()) {
                        Iterator<Map.Entry<Integer, Elements>> tempHeadingAndPhrases = headingAndPhraseElements.entrySet().iterator();
                        while (tempHeadingAndPhrases.hasNext()) {
                            Map.Entry<Integer, Elements> headingAndPhrases = tempHeadingAndPhrases.next();
                            Elements headerValues = headingAndPhrases.getValue();
                            if (headingAndPhrases.getKey() == 1) {
                                if (headerValues.size() <= 0) {
                                    toolIssues += "<li>H1 tags are missing.</li>";
                                    issueStatus = true;
                                }
                            }
                        }
                    }
                    //  For Insufficient Content Quantity
                    long wordCount = spiderViewResults.getTotalWordCount();
                    if (wordCount <= 50) {
                        toolIssues += "<li>Insufficient content leading to low ranking.</li>";
                        issueStatus = true;
                    }
                    LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                    if (issueStatus) {
                        questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
                        questionInfo += "- Auditing on-page SEO factors for my website.";
                        toolIssuesMainContent += "<p>The following on-page factors could cause issues with your SEO:</p>";
                        toolIssuesMainContent += "<ul>";
                        toolIssuesMainContent += toolIssues;
                        toolIssuesMainContent += "</ul>";
                    }
                    lxrmAskExpert.setDomain(domain.trim());
                    if (userInputJson != null) {
                        lxrmAskExpert.setOtherInputs(userInputJson.toString());
                    }
                    lxrmAskExpert.setQuestion(questionInfo);
                    lxrmAskExpert.setIssues(toolIssuesMainContent);
                    lxrmAskExpert.setExpertInfo(questionInfo);
                    lxrmAskExpert.setToolName(Common.SEO_WEBPAGE_ANALYSIS);
                    lxrmAskExpertService.addInJson(lxrmAskExpert, response);

                    //  if the request is not from  mail then we should add this input to the session object
                    if (userInputJson != null && userSession != null) {
                        String toolUrl = "/seo-webpage-analysis-tool.html";
                        userSession.addUserAnalysisInfo(user.getId(), 8, domain, toolIssuesMainContent,
                                questionInfo, userInputJson.toString(), toolUrl);
                        userInputJson = null;
                    }
                }
                return null;
            }
            spiderView = new SpiderViewTool();
            ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
            session.removeAttribute("notWorkingUrl");
            return mAndV;
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "spiderViewTool", spiderView);
        session.removeAttribute("notWorkingUrl");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        SpiderViewTool spiderViewTool = (SpiderViewTool) session.getAttribute("spiderViewTool");
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        session.removeAttribute("paidDownloadedReportDetailsList");
        if (user != null && userLoggedIn) {
            List<PaidDownloadedReportDetails> paidDownloadedReportDetailsList = toolService.fetchPaidDownladReports(user.getId(), 8);
            session.setAttribute("paidDownloadedReportDetailsList", paidDownloadedReportDetailsList);
        }
        if (spiderViewTool == null) {
            spiderViewTool = new SpiderViewTool();
        }
        return spiderViewTool;
    }

}
