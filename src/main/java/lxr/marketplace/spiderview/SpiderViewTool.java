package lxr.marketplace.spiderview;

public class SpiderViewTool {

    String url;

    public String getUrl() {
        if (url != null) {
            url = url.trim();
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
