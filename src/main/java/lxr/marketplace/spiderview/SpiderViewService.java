package lxr.marketplace.spiderview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import org.jsoup.nodes.Document;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.findPhrase.PhraseStat;
import lxr.marketplace.util.relevancycalculator.RelevancyChecker;
import lxr.marketplace.util.stopwordsutility.StopWordsTrie;
import org.springframework.context.NoSuchMessageException;

public class SpiderViewService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(SpiderViewService.class);
    ApplicationContext context = ApplicationContextProvider.getApplicationContext();
    static final int READ_TIME_OUT = 200000;
    static final int CONNECT_TIME_OUT = 200000;

    SpiderViewResult svresult;

    public SpiderViewResult getSvresult() {
        return svresult;
    }

    public boolean getSpiderViewResults(SpiderViewTool svtool, HttpServletRequest request, String url) {
        boolean jsoupconnerror = false;
        Document doc = null;
        Elements metaTags;
        String metaTagName, description = null, title = null, bodyText;
        HttpSession session = request.getSession(true);
        svresult = new SpiderViewResult();
        List<String> stopWordsList = new ArrayList<>();
        WebApplicationContext context1 = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
        if (context1.getServletContext().getAttribute("stopwords") != null) {
            stopWordsList = (List<String>) context1.getServletContext().getAttribute("stopwords");
        }
        StringBuilder pageHTML = new StringBuilder("");
        Common.getStatusCodeAndHTML(url, new StringBuilder(""), pageHTML);
        if (!pageHTML.toString().equals("")) {
            doc = Common.getDocumentFromHTML(pageHTML.toString(), url);
        } else {
            jsoupconnerror = true;
        }

        if (doc != null) {
            if (doc.select("title").first() != null) {
                title = doc.select("title").first().text().trim();
            }
            if (title == null && doc.getElementsByTag("head").select("title").first() != null) {
                title = doc.getElementsByTag("head").select("title").first().text().trim();
            }
            metaTags = doc.select("meta");
            bodyText = doc.select("body").text();
            if (metaTags.size() > 0) {
                for (Element metaTag : metaTags) {
                    metaTagName = metaTag.attr("name");
                    if (metaTagName.equalsIgnoreCase("description")) {
                        description = metaTag.attr("content");
                    }
//                    Please don't delete below commented code, this is for our reference for fetching below metrics
//                    if (metaTagName.equalsIgnoreCase("keywords")) {
//                        keywords = metaTag.attr("content");
//                    }
//                    if (metaTagName.equalsIgnoreCase("robots")) {
//                        robots = metaTag.attr("content");
//                    }
//                    if (metaTagName.equalsIgnoreCase("author")) {
//                        author = metaTag.attr("content");
//                    }
//                    if (metaTagName.equalsIgnoreCase("copyright")) {
//                        copyright = metaTag.attr("content");
//                    }
                }
            }
            svresult.setHttpHeaders(getHttpHeaders(url, session));
            svresult.setTitleTag(getTitleTag(url, bodyText, title, stopWordsList));
            svresult.setMetaDescriptionTag(getMetaDescriptionTag(bodyText, description, stopWordsList));
            svresult.setTotalWordCount(Common.getTotalWordCount(bodyText));
            svresult.setAnchorLinks(getAnchorLinks(url, doc));
            svresult.setKeywordsFoundInAnchorTags(getKeywordsInAnchorTags(url, doc));
            svresult.setKeywordsFoundInAltAttributes(getKeywordsInAltAttributes(url, doc));
            svresult.setHeadingAndPhraseElements(getHeadingAndPhraseElements(url, doc));
            svresult.setPageDisplayedInSerachResults(getPageDisplayedInSerachResults(url, doc, description, title));
            svresult.setKeywordPhrses(Common.getMeaningFullPhrases(bodyText, stopWordsList));
            svresult.setTextRatio(gettexthtmlratio(doc));
            svresult.setMissingImageAltAttributes(getMissingImageAltAttributes(url, doc));
        } else {
            jsoupconnerror = true;
        }
        return jsoupconnerror;
    }

    /**
     * Text Ratio Metric
     *
     * @param doc
     * @return
     */
    public double gettexthtmlratio(Document doc) {
        String temp = Common.texthtmlratio(doc);
        /* The text Ratio for given url as a string insidein spider as a temp */
        temp = temp.replace('%', ' ');
        /* The text Ratio for given url as a string insidein spider after substring */
        double textHtmlratio = Double.parseDouble((temp).trim());
        /* The text Ratio for given url is  inside in insidein spider as a final textHtmlratio */
        return textHtmlratio;
    }

    public Map<String, String> getHttpHeaders(String inputUrl, HttpSession session) {
        Map<String, String> httpHeaders = new HashMap<>();
        StringBuilder finalURLStatus = new StringBuilder(" ");
        int webPageStatusCode = 0;
        StringBuilder redirectedUrl = new StringBuilder("");
        StringBuilder finalModifiedUrl = new StringBuilder("");
        String spiderURL = null;
        spiderURL = (String) session.getAttribute("spiderURL");
        try {
            if (spiderURL != null) {
                webPageStatusCode = Common.appendProtocalAndCheckStausCode(spiderURL.trim(), redirectedUrl, finalModifiedUrl);
            } else {
                webPageStatusCode = Common.appendProtocalAndCheckStausCode(inputUrl, redirectedUrl, finalModifiedUrl);
            }
        } catch (Exception e) {
            webPageStatusCode = 500;
            logger.error("Exception in appending protocal to string ", e);
        }
        if (webPageStatusCode == 200) {
            finalURLStatus.insert(0, "Yes Working (" + webPageStatusCode + ")");
        } else if (webPageStatusCode == 301) {
            finalURLStatus.insert(0, "Yes Permanent Redirection (" + webPageStatusCode + ")");
        } else if (webPageStatusCode >= 302 || webPageStatusCode < 400) {
            finalURLStatus.insert(0, "Yes Temporary Redirection (" + webPageStatusCode + ")");
        } else if (webPageStatusCode >= 400 || webPageStatusCode <= 600) {
            finalURLStatus.insert(0, "Yes Internal Server Error (" + webPageStatusCode + ")");
        }
        try {
            if (session.getAttribute("urlStatusCode") != null) {
                httpHeaders.put("HTTP Status", session.getAttribute("urlStatusCode").toString());
            } else {
                List<List<String>> cookieContainer = new ArrayList<>();
                int code = Common.getStatusCode(inputUrl, redirectedUrl, cookieContainer, session);
                httpHeaders.put("HTTP Status", String.valueOf(code));
            }

        } catch (Exception e) {
            logger.error("Exception in setting urlStatusCode in getHttpHeaders", e);
        }

        if ((webPageStatusCode != 0) && (webPageStatusCode != 200)) {
            httpHeaders.put("Redirection Status", finalURLStatus.toString().trim());
        } else {
            httpHeaders.put("Redirection Status", "No");
        }

        try {
            URL url = new URL(inputUrl);
            URLConnection conn = url.openConnection();
            for (int i = 0;; i++) {
                String headerName = conn.getHeaderFieldKey(i);
                String headerValue = conn.getHeaderField(i);
                if (headerName == null && headerValue == null) { // No more headers
                    break;
                } else if (headerName != null && headerValue != null) {
                    httpHeaders.put(headerName, headerValue);
                }
            }
        } catch (Exception e) {
            logger.error("Exception in fetching http headers data in getHttpHeaders", e);
        }
        return httpHeaders;
    }

    public List<String> getStopWords() {
        List<String> stopWords = new ArrayList<>();
        String query = "select * from stop_words";
        try {
            SqlRowSet tempStopWords = getJdbcTemplate().queryForRowSet(query);
            while (tempStopWords.next()) {
                stopWords.add(tempStopWords.getString("stop_word"));
            }
        } catch (Exception e) {
            logger.info("Exception while fetching stopwords list", e);
        }
        return stopWords;
    }

    public float getFormattedValue(float val) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Float.valueOf(twoDForm.format(val));
    }

    public List<String> getTitleTag(String inputUrl, String bodyText, String title, List<String> stopWordsList) {
        List<String> titleTag = new ArrayList<>();
        if (title != null && !title.trim().equals("")) {
            int length = title.length();
            titleTag.add(title);
            titleTag.add(Integer.toString(length));
            RelevancyChecker relevancyChecker = new RelevancyChecker(bodyText, title, stopWordsList);
            float relevancy = relevancyChecker.findRelevency();
            if (relevancy < 50.0) {
                relevancy = 50.0f;
            }
            float formattedRelevancy = getFormattedValue(relevancy);
            titleTag.add(Float.toString(formattedRelevancy));
            List<String> stopWords = StopWordsTrie.getMatchedStopwords(title, stopWordsList);
            if (stopWords.size() > 0) {
                for (int i = 0; i < stopWords.size(); i++) {
                    titleTag.add(stopWords.get(i));
                }
            }
        }
        return titleTag;
    }

    public List<String> getMetaDescriptionTag(String bodyText, String description, List<String> stopWordsList) {
        List<String> metaDescription = new ArrayList<>();
        if (description != null && !description.trim().equals("")) {
            int length = description.length();
            metaDescription.add(description);
            metaDescription.add(Integer.toString(length));
            RelevancyChecker relevancyChecker = new RelevancyChecker(bodyText, description, stopWordsList);
            float relevancy = relevancyChecker.findRelevency();
            if (relevancy < 50.0) {
                relevancy = 50.0f;
            }
            float formattedRelevancy = getFormattedValue(relevancy);
            metaDescription.add(Float.toString(formattedRelevancy));
            List<String> stopWords = StopWordsTrie.getMatchedStopwords(description, stopWordsList);
            if (stopWords.size() > 0) {
                for (int i = 0; i < stopWords.size(); i++) {
                    metaDescription.add(stopWords.get(i));
                }
            }
        }
        return metaDescription;
    }

    public List<String> getPageDisplayedInSerachResults(String inputUrl, Document doc, String description, String title) {
        List<String> pageInSerachResults = new ArrayList<>();
        pageInSerachResults.add(title);
        pageInSerachResults.add(description);
        pageInSerachResults.add(inputUrl);
        return pageInSerachResults;
    }

    public Map<Integer, Map<String, Integer>> getAnchorLinks(String inputUrl, Document doc) {
        Map<Integer, Map<String, Integer>> totalLinks = new HashMap<>();
        Map<String, Integer> internalLinks = new HashMap<>();
        Map<String, Integer> externalLinks = new HashMap<>();
        /* internalAnchorLinks  is removed from list for URLs Found in the page metric in tool with refrence to mail from Somabrata,Vemanna
                and added new functionality seprating social and extrnal link's.Map<String, Integer> internalAnchorLinks = new HashMap<String, Integer>();*/
        Map<String, Integer> socialLinks = new HashMap<>();
        Map<String, Integer> tempLinks = new HashMap<>();
        String[] snames = {"facebook", "twitter", "linkedin", "flickr", "pinterest", "plus.google", "youtube", "myspace", "deviantart", "livejournal", "tagged", "orkut", "cafemom", "ning", "meetup", "mylife", "multiply", "instagram"};
        try {
            URL tempUrl = new URL(inputUrl);
            Elements anchors = doc.select("a");
            if (anchors.size() > 0) {
                int noOfLinks = anchors.size();
                tempLinks.put("totalNoOfLinks", noOfLinks);
                for (Element anchor : anchors) {
                    String tempAnchor = anchor.attr("abs:href");
                    if (!tempAnchor.trim().equals("")) {
                        String domain = "";
                        String protocolDomain = "";
                        String mailDomain = "";
                        try {
                            domain = tempUrl.getProtocol() + "://" + tempUrl.getHost();
                            protocolDomain = tempUrl.getProtocol() + "://" + tempUrl.getHost().replaceAll("www.", " ").trim();
                            mailDomain = "@" + tempUrl.getHost().replaceAll("www.", " ").trim();
                        } catch (Exception e) {
                            domain = tempUrl.getHost();
                            protocolDomain = tempUrl.getHost().replaceAll("www.", " ").trim();
                            mailDomain = "@" + tempUrl.getHost().replaceAll("www.", " ").trim();
                            logger.error("Exception in preparing URL protocol", e);
                        }

                        if ((tempAnchor.startsWith(domain) || tempAnchor.startsWith(protocolDomain)) && !(tempAnchor.contains("#") || tempAnchor.contains("#!")) && !(tempAnchor.contains(mailDomain))) {
                            if (internalLinks.containsKey(tempAnchor)) {
                                Integer val = internalLinks.get(tempAnchor);
                                val++;
                                internalLinks.put(tempAnchor, val);
                            } else {
                                internalLinks.put(tempAnchor, 1);
                            }
                        }
                        if (!(tempAnchor.startsWith(domain) || tempAnchor.startsWith(protocolDomain))
                                && !(tempAnchor.contains("#") || tempAnchor.contains("#!"))
                                && !(tempAnchor.contains(mailDomain))
                                || tempAnchor.contains("facebook") || tempAnchor.contains("twitter")
                                || tempAnchor.contains("instagram") || tempAnchor.contains("pinterest")
                                || tempAnchor.contains("youtube") || tempAnchor.contains("plus.google")
                                || tempAnchor.contains("linkedin")) {
                            boolean status = false;
                            for (int i = 0; i < snames.length; i++) {
                                if (tempAnchor.contains(snames[i])) {
                                    status = true;
                                    break;
                                } else {
                                    status = false;
                                }
                            }
                            if (status) {
                                if (socialLinks.containsKey(tempAnchor)) {
                                    Integer val = socialLinks.get(tempAnchor);
                                    val++;
                                    socialLinks.put(tempAnchor, val);
                                } else {
                                    socialLinks.put(tempAnchor, 1);
                                }
                            } else if (externalLinks.containsKey(tempAnchor)) {
                                Integer val = externalLinks.get(tempAnchor);
                                val++;
                                externalLinks.put(tempAnchor, val);
                            } else {
                                externalLinks.put(tempAnchor, 1);
                            }
                        }
                    }
                }
            } else {
                tempLinks.put("totalNoOfLinks", 0);
            }
            totalLinks.put(1, tempLinks);
            totalLinks.put(2, internalLinks);
            totalLinks.put(3, externalLinks);
            totalLinks.put(4, socialLinks);
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException in getAnchorLinks", e);
        } catch (Exception e) {
            logger.error("Exception in getAnchorLinks", e);
        }
        return totalLinks;
    }

    public Map<String, Integer> getKeywordsInAnchorTags(String inputUrl, Document doc) {
        Map<String, Integer> tempKeywordsInAnchors = new HashMap<>();
        TreeMap<String, Integer> keywordsInAnchors = new TreeMap<>();
        try {
            Elements anchors = doc.select("a");
            String anchorText = null;
            if (anchors.size() > 0) {
                for (Element anchor : anchors) {
                    if (anchor != null) {
                        anchorText = anchor.text().toLowerCase();
                        if (!(anchorText.equals("null") && anchorText.trim().equals(" ") && anchorText.trim().equals(""))) {
                            anchorText = anchorText.replaceAll("[^a-zA-Z0-9\\s]+", "");
                            if (!anchorText.trim().equals("")) {
                                if (tempKeywordsInAnchors.containsKey(anchorText)) {
                                    Integer val = tempKeywordsInAnchors.get(anchorText);
                                    val++;
                                    tempKeywordsInAnchors.put(anchorText, val);
                                } else {
                                    tempKeywordsInAnchors.put(anchorText, 1);
                                }
                            }
                        }
                    }
                }
            }
            ValueComparator compareValues = new ValueComparator(tempKeywordsInAnchors);
            keywordsInAnchors = new TreeMap<>(compareValues);
            keywordsInAnchors.putAll(tempKeywordsInAnchors);
        } catch (Exception e) {
            logger.error("Exception in getKeywordsInAnchorTags", e);
        }
        return keywordsInAnchors;
    }

    public Map<String, Integer> getKeywordsInAltAttributes(String inputUrl, Document doc) {
        Map<String, Integer> tempKeywordsInAltAttribute = new HashMap<>();
        Elements imgTags = doc.select("img");
        if (imgTags.size() > 0) {
            for (Element img : imgTags) {
                if (img != null) {
                    String altText = img.attr("alt").toLowerCase().trim();
                    if (altText != null && !(altText.trim().equals("") && altText.trim().equals(" "))) {
                        altText = altText.replaceAll("[^a-zA-Z0-9\\s]+", "");
                        if (!altText.trim().equals("")) {
                            if (tempKeywordsInAltAttribute.containsKey(altText)) {
                                Integer val = tempKeywordsInAltAttribute.get(altText);
                                val++;
                                tempKeywordsInAltAttribute.put(altText, val);
                            } else {
                                tempKeywordsInAltAttribute.put(altText, 1);
                            }
                        }
                    }
                }
            }
        }
        ValueComparator compareValues = new ValueComparator(tempKeywordsInAltAttribute);
        TreeMap<String, Integer> keywordsInAltAttribute = new TreeMap<>(compareValues);
        keywordsInAltAttribute.putAll(tempKeywordsInAltAttribute);
        return keywordsInAltAttribute;
    }

    public Map<String, Integer> getMissingImageAltAttributes(String inputUrl, Document doc) {
        Map<String, Integer> missingImageAltAttribute = new HashMap<>();
        Elements imgTags = doc.select("img");
        if (imgTags.size() > 0) {
            for (Element img : imgTags) {
                if (img != null) {
                    String altText = img.attr("alt").toLowerCase().trim();
                    if (altText != null && !(altText.trim().equals("") && altText.trim().equals(" "))) {
                        altText = altText.replaceAll("[^a-zA-Z0-9\\s]+", "");
                        if (altText.trim().equals("")) {
                            img.attr("abs:src");
                            if (missingImageAltAttribute.containsKey(img.attr("abs:src"))) {
                                Integer val = missingImageAltAttribute.get(img.attr("abs:src"));
                                val++;
                                missingImageAltAttribute.put(img.attr("abs:src"), val);
                            } else {
                                missingImageAltAttribute.put(img.attr("abs:src"), 1);
                            }
                        }
                    }
                }
            }
        }
        return missingImageAltAttribute;
    }

    public Map<Integer, Elements> getHeadingAndPhraseElements(String inputUrl, Document doc) {
        Map<Integer, Elements> headingAndPhrases = new HashMap<>();
        headingAndPhrases.put(1, doc.select("h1"));
        headingAndPhrases.put(2, doc.select("h2"));
        headingAndPhrases.put(3, doc.select("h3"));
        headingAndPhrases.put(4, doc.select("h4"));
        headingAndPhrases.put(5, doc.select("h5"));
        headingAndPhrases.put(6, doc.select("h6"));
        headingAndPhrases.put(7, doc.select("strong"));
        headingAndPhrases.put(8, doc.select("b"));
        headingAndPhrases.put(9, doc.select("em"));
        headingAndPhrases.put(10, doc.select("i"));
        headingAndPhrases.put(11, doc.select("acronym"));
        headingAndPhrases.put(12, doc.select("dfn"));
        headingAndPhrases.put(13, doc.select("abbr"));
        return headingAndPhrases;
    }

    public String createPdfFile(HttpSession session, String targetFilePath, Login user) {
        SpiderViewResult spiderViewResults = (SpiderViewResult) session.getAttribute("spiderviewresults");
        Map<String, String> httpHeaders = spiderViewResults.getHttpHeaders();
        try {
            SpiderViewService service = new SpiderViewService();
            context = service.context;

            Font reportNameFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, Font.BOLD, new BaseColor(255, 110, 0)));
            Font orangeFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, new BaseColor(255, 110, 0)));
            Font blackFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0)));
            Font smallFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.5f, Font.BOLD, new BaseColor(0, 0, 0)));
            Font forBoldAnchor = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10f, Font.BOLD | Font.UNDERLINE, new BaseColor(0, 0, 0)));
            Font forNormalAnchor = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.8f, Font.NORMAL | Font.UNDERLINE, new BaseColor(0, 0, 0)));
            Font smallOrangeFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.8f, Font.BOLD, new BaseColor(255, 110, 0)));
            Font normalFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10.8f, Font.NORMAL));
            Font normalHeadingFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD));
            Font hpTableOrangeFont = new Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(255, 110, 0)));
            String repName = "LXRMarketplace_SEO_Webpage_Analysis_Report_" + user.getId();
            String reptName = Common.removeSpaces(repName);
            String filename = Common.createFileName(reptName) + ".pdf";
            String filePath = targetFilePath + filename;
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            logger.info(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("SEO Webpage Analysis Tool Report", reportNameFont);
            Chunk normalHeading = new Chunk("Review of: ", normalFont);
            Chunk domain = new Chunk(" \"" + (String) session.getAttribute("Url") + "\" ", blackFont);
            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));

            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            rpName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            domHeading.add(normalHeading);
            domHeading.add(domain);
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm");
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Date: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(10f);
            rpName.setPaddingBottom(10f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            imgTable.setSpacingBefore(10f);
            imgTable.setSpacingAfter(35f);

            String headElem = " Head Elements are one of the most important factors in achieving high search engine rankings. Please note that this analysis does not check your title, description, or keyword meta tags for "
                    + "certain issues such as excessive keyword repetition. Such practices can be seen as spamdexing techniques by search engines, triggering spam filters, and should be avoided.";
            addTitlePage(pdfDoc, "On-Page Factors", headElem, orangeFont, normalFont);
            pdfDoc.add(new Paragraph("Page Title :", blackFont));
            List<String> titleData = new ArrayList<>();
            titleData = spiderViewResults.getTitleTag();
            if (titleData.size() > 0 && titleData.toString() != null && !titleData.toString().trim().equals("")) {
                String stopWordsList = "";
                for (int i = 0; i < titleData.size(); i++) {
                    switch (i) {
                        case 0: {
                            Paragraph totalTextStyle = new Paragraph();
                            totalTextStyle.setSpacingAfter(5f);
                            totalTextStyle.setSpacingBefore(4f);
                            Phrase tempTitle = new Phrase();
                            Chunk c1 = new Chunk("Title :", smallFont);
                            Chunk c2 = new Chunk(titleData.get(i), normalFont);
                            tempTitle.add(c1);
                            tempTitle.add(c2);
                            totalTextStyle.add(new Paragraph(tempTitle));
                            pdfDoc.add(totalTextStyle);
                            break;
                        }
                        case 1: {
                            Paragraph totalTextStyle = new Paragraph();
                            totalTextStyle.setSpacingAfter(5f);
                            totalTextStyle.setSpacingBefore(4f);
                            Phrase titleLength = new Phrase();
                            titleLength.setFont(normalFont);
                            Object[] chars = new Integer[1];
                            chars[0] = Integer.parseInt(titleData.get(i));
                            Chunk c1 = null;
                            Chunk recom = null;
                            if ((Integer.parseInt(titleData.get(i)) >= 41) && (Integer.parseInt(titleData.get(i)) <= 65)) {
                                c1 = new Chunk(context.getMessage("message.title", chars, Locale.UK) + " ");
                                recom = new Chunk("good.", normalHeadingFont);
                            } else if ((Integer.parseInt(titleData.get(i)) <= 40) || (Integer.parseInt(titleData.get(i)) >= 66)) {
                                c1 = new Chunk(context.getMessage("message.title", chars, Locale.UK) + " ");
                                recom = new Chunk("not good.", normalHeadingFont);
                            }
                            titleLength.add(c1);
                            titleLength.add(recom);
                            totalTextStyle.add(new Paragraph(titleLength));
                            pdfDoc.add(totalTextStyle);
                            break;
                        }
                        case 2: {
                            Paragraph totalTextStyle = new Paragraph();
                            totalTextStyle.setSpacingAfter(5f);
                            totalTextStyle.setSpacingBefore(4f);
                            Phrase relevancy = new Phrase();
                            relevancy.setFont(normalFont);
                            Object[] chars = new Float[1];
                            chars[0] = Float.parseFloat((titleData.get(i)));
                            Chunk c1 = null;
                            Chunk recom = null;
                            if (Float.parseFloat(titleData.get(i)) == 100) {
                                c1 = new Chunk(context.getMessage("message.titlerelevancy", chars, Locale.UK) + " ");
                                recom = new Chunk("perfect.", normalHeadingFont);
                            } else if (Float.parseFloat(titleData.get(i)) >= 60) {
                                c1 = new Chunk(context.getMessage("message.titlerelevancy", chars, Locale.UK) + " ");
                                recom = new Chunk("good.", normalHeadingFont);
                            } else if (Float.parseFloat(titleData.get(i)) < 60) {
                                c1 = new Chunk(context.getMessage("message.titlerelevancy", chars, Locale.UK) + " ");
                                recom = new Chunk("not good.", normalHeadingFont);
                            }
                            relevancy.add(c1);
                            relevancy.add(recom);
                            totalTextStyle.add(new Paragraph(relevancy));
                            pdfDoc.add(totalTextStyle);
                            break;
                        }
                        default:
                            stopWordsList = stopWordsList + titleData.get(i) + "," + " ";
                            break;
                    }
                }
                if (stopWordsList.length() > 0) {
                    int coma = stopWordsList.lastIndexOf(",");
                    String stopwords = stopWordsList.substring(0, coma);
                    Paragraph totalTextStyle = new Paragraph();
                    totalTextStyle.add(new Paragraph(context.getMessage("message.titlestopwords", null, Locale.UK), normalFont));
                    totalTextStyle.setSpacingAfter(8f);
                    Phrase sw = new Phrase();
                    Chunk c1 = new Chunk("Stop Words: ", smallFont);
                    Chunk c2 = new Chunk(stopwords, normalFont);
                    sw.add(c1);
                    sw.add(c2);
                    pdfDoc.add(totalTextStyle);
                    pdfDoc.add(sw);
                }
            } else {
                Phrase warning = new Phrase();
                Chunk header = new Chunk("Warning :", smallFont);
                Chunk errorMsg = new Chunk("No title found.", normalFont);
                warning.add(header);
                warning.add(errorMsg);
                pdfDoc.add(warning);
            }
            addSubTitle(pdfDoc, "Meta Description :", blackFont);
            List<String> metaDescription = new ArrayList<>();
            metaDescription = spiderViewResults.getMetaDescriptionTag();
            if (metaDescription.size() > 0 && metaDescription.toString() != null && !metaDescription.toString().trim().equals("")) {
                String stopWordsList = "";
                for (int i = 0; i < metaDescription.size(); i++) {
                    if (metaDescription.get(i) != null && !metaDescription.get(i).equals("")) {
                        switch (i) {
                            case 0: {
                                Paragraph totalTextStyle = new Paragraph();
                                totalTextStyle.setSpacingAfter(5f);
                                totalTextStyle.setSpacingBefore(4f);
                                Phrase description = new Phrase();
                                Chunk c1 = new Chunk("Meta Description : ", smallFont);
                                Chunk c2 = new Chunk(metaDescription.get(i), normalFont);
                                description.add(c1);
                                description.add(c2);
                                totalTextStyle.add(new Paragraph(description));
                                pdfDoc.add(totalTextStyle);
                                break;
                            }
                            case 1: {
                                Paragraph totalTextStyle = new Paragraph();
                                totalTextStyle.setSpacingAfter(5f);
                                totalTextStyle.setSpacingBefore(4f);
                                Phrase length = new Phrase();
                                length.setFont(normalFont);
                                Object[] chars = new Integer[1];
                                chars[0] = Integer.parseInt(metaDescription.get(i));
                                Chunk c1 = null;
                                Chunk recom = null;
                                if (Integer.parseInt(metaDescription.get(i)) >= 71 && Integer.parseInt(metaDescription.get(i)) <= 160) {
                                    c1 = new Chunk(context.getMessage("message.description", chars, Locale.UK) + " ");
                                    recom = new Chunk("good.", normalHeadingFont);
                                } else if (Integer.parseInt(metaDescription.get(i)) <= 70 || Integer.parseInt(metaDescription.get(i)) >= 161) {
                                    c1 = new Chunk(context.getMessage("message.description", chars, Locale.UK) + " ");
                                    recom = new Chunk("not good.", normalHeadingFont);
                                }
                                length.add(c1);
                                length.add(recom);
                                totalTextStyle.add(new Paragraph(length));
                                pdfDoc.add(totalTextStyle);
                                break;
                            }
                            case 2: {
                                Paragraph totalTextStyle = new Paragraph();
                                totalTextStyle.setSpacingAfter(5f);
                                totalTextStyle.setSpacingBefore(4f);
                                Phrase relevancy = new Phrase();
                                relevancy.setFont(normalFont);
                                Object[] chars = new Float[1];
                                chars[0] = Float.parseFloat((metaDescription.get(i)));
                                Chunk c1 = null;
                                Chunk recom = null;
                                if (Float.parseFloat(metaDescription.get(i)) == 100) {
                                    c1 = new Chunk(context.getMessage("message.descriptionrelevancy", chars, Locale.UK) + " ");
                                    recom = new Chunk("perfect.", normalHeadingFont);
                                } else if (Float.parseFloat(metaDescription.get(i)) >= 60) {
                                    c1 = new Chunk(context.getMessage("message.descriptionrelevancy", chars, Locale.UK) + " ");
                                    recom = new Chunk("good.", normalHeadingFont);
                                } else if (Float.parseFloat(metaDescription.get(i)) < 60) {
                                    c1 = new Chunk(context.getMessage("message.descriptionrelevancy", chars, Locale.UK) + " ");
                                    recom = new Chunk("not good.", normalHeadingFont);
                                }
                                relevancy.add(c1);
                                relevancy.add(recom);
                                totalTextStyle.add(new Paragraph(relevancy));
                                pdfDoc.add(totalTextStyle);
                                break;
                            }
                            default:
                                stopWordsList = stopWordsList + metaDescription.get(i) + "," + " ";
                                break;
                        }
                    }
                }
                if (stopWordsList.length() > 0) {
                    Paragraph totalTextStyle = new Paragraph();
                    int coma = stopWordsList.lastIndexOf(",");
                    String stopwords = stopWordsList.substring(0, coma);
                    totalTextStyle.add(new Paragraph(context.getMessage("message.descriptionstopwords", null, Locale.UK), normalFont));
                    totalTextStyle.setSpacingAfter(8f);
                    Phrase sw = new Phrase();
                    Chunk c1 = new Chunk("Stop Words: ", smallFont);
                    Chunk c2 = new Chunk(stopwords, normalFont);
                    sw.add(c1);
                    sw.add(c2);
                    pdfDoc.add(totalTextStyle);
                    pdfDoc.add(sw);
                }
            } else {
                Phrase warning = new Phrase();
                Chunk header = new Chunk("Warning :", smallFont);
                Chunk errorMsg = new Chunk("No meta description tags found.", normalFont);
                warning.add(header);
                warning.add(errorMsg);
                pdfDoc.add(warning);
            }
            Map<Integer, Elements> headingAndPhraseElements = new HashMap<>();
            headingAndPhraseElements = spiderViewResults.getHeadingAndPhraseElements();
            if (headingAndPhraseElements.size() > 0) {
                float[] hAndPTableWidths = {30f, 60f};
                PdfPTable headingAndPhraseTable = new PdfPTable(hAndPTableWidths);
                headingAndPhraseTable.setWidthPercentage(99.9f);
                addSubTitle(pdfDoc, "Heading Tags (H-tags) :", blackFont);
                // The Heading Tags are the important elements that defines the  website HTML structure to search engines and visitors.It will help to appear  web pages in SERP.";
                PdfPCell[] hAndPhrases = new PdfPCell[2];
                Iterator<Map.Entry<Integer, Elements>> tempHeadingAndPhrases = headingAndPhraseElements.entrySet().iterator();
                try {
                    while (tempHeadingAndPhrases.hasNext()) {
                        Map.Entry<Integer, Elements> headingAndPhrases = tempHeadingAndPhrases.next();
                        Elements headerValues = headingAndPhrases.getValue();
                        com.itextpdf.text.List headingAndPhrasesList = new com.itextpdf.text.List(com.itextpdf.text.List.UNORDERED);
                        headingAndPhrasesList.setListSymbol("->" + " ");
                        PdfPCell cell2 = null;
                        String headerName = " ";
                        for (int i = 1; i <= 6; i++) {
                            if (headingAndPhrases.getKey() == i) {
                                headerName = "h" + i + "-Tag(s)";
                            }
                        }
                        if (headingAndPhrases.getKey() == 7) {
                            headerName = "strong-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 8) {
                            headerName = "b-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 9) {
                            headerName = "em-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 10) {
                            headerName = "i-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 11) {
                            headerName = "acronym-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 12) {
                            headerName = "dfn-Tag(s)";
                        }
                        if (headingAndPhrases.getKey() == 13) {
                            headerName = "abbr-Tag(s)";
                        }
                        if (headerValues.size() > 0) {
                            int incVal = 0;
                            Pattern p = Pattern.compile("^[\\p{Z}\\s]*$");
                            for (org.jsoup.nodes.Element hval : headerValues) {
                                String text = hval.text();
                                if (text != null) {
                                    Matcher m = p.matcher(text);
                                    if (hval.hasText() && !m.find()) {
                                        text = text.replaceAll("[\\p{Z}\\s]+", "");
                                        if (!text.trim().equals("") && !text.trim().equals(" ")) {
                                            int limit = incVal++;
                                            if (limit <= 20) {
                                                if (text.length() > 50) {
                                                    String tempText = text.substring(0, 50) + "....";
                                                    text = tempText;
                                                }
                                                headingAndPhrasesList.add(new ListItem(text, normalFont));
                                                cell2 = new PdfPCell();
                                                cell2.addElement(headingAndPhrasesList);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            headingAndPhrasesList.add(new ListItem("Not Found.", normalFont));
                            PdfPCell cell3 = new PdfPCell();
                            cell3.addElement(headingAndPhrasesList);
                            cell2 = cell3;
                        }
                        PdfPCell cell1 = new PdfPCell(new Phrase(headerName, normalHeadingFont));
                        for (int i = 0; i < hAndPhrases.length; i++) {
                            if (cell2 != null) {
                                if (i == 0) {
                                    hAndPhrases[i] = cell1;
                                } else if (i == 1) {
                                    hAndPhrases[i] = cell2;
                                }
                                hAndPhrases[i].setBorderColor(BaseColor.GRAY);
                                hAndPhrases[i].setBorderWidth(0.3f);
                                hAndPhrases[i].setPadding(8);
                                hAndPhrases[i].setSpaceCharRatio(6f);
                                headingAndPhraseTable.addCell(hAndPhrases[i]);
                            }
                        }
                        headingAndPhraseTable.completeRow();
                    }
                    pdfDoc.add(new Paragraph(" "));
                    pdfDoc.add(headingAndPhraseTable);
                } catch (Exception e) {
                    logger.error("Exception while constructing heading tags ", e);
                }
            } else {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                totalTextStyle.add(new Paragraph("No heading and Phrase elements found.", normalFont));
                pdfDoc.add(totalTextStyle);
            }
            addSubTitle(pdfDoc, "Sufficient Content Quantity (amount of text on page) :", blackFont);
            long wordCount = spiderViewResults.getTotalWordCount();
            if (wordCount > 0) {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                Phrase wcRecom = new Phrase();
                Phrase wc = new Phrase();
                Chunk c1 = new Chunk("Word Count : ", smallFont);
                DecimalFormat df = new DecimalFormat();
                df.getDecimalFormatSymbols().setGroupingSeparator(',');
                df.setGroupingSize(3);
                String totalWordCount = df.format(wordCount);
                Chunk c2 = new Chunk(totalWordCount, normalFont);
                wc.add(c1);
                wc.add(c2);
                pdfDoc.add(wc);
                Chunk recom = null;
                Chunk recomMsg = null;
                Object[] chars = new String[1];
                chars[0] = totalWordCount;
                if (wordCount > 50) {
                    recom = new Chunk(context.getMessage("message.seowordcount", chars, Locale.UK) + " ", normalFont);
                    recomMsg = new Chunk("good.", normalHeadingFont);
                } else if (wordCount <= 50) {
                    recom = new Chunk(context.getMessage("message.seowordcount", chars, Locale.UK) + " ", normalFont);
                    recomMsg = new Chunk("not good.", normalHeadingFont);
                }
                wcRecom.add(recom);
                wcRecom.add(recomMsg);
                totalTextStyle.add(new Paragraph(wcRecom));
                pdfDoc.add(totalTextStyle);
            }
            /**
             * Text Ratio Metric
             */
            addSubTitle(pdfDoc, "Text/Html Ratio :", blackFont);
            double textRatio = spiderViewResults.getTextRatio();
            if (textRatio > 0.0) {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                Phrase wcRecom = new Phrase();
                Phrase wc = new Phrase();
                Chunk c1 = new Chunk("Text/Html Ratio: ", smallFont);
                DecimalFormat df = new DecimalFormat();
                df.getDecimalFormatSymbols().setGroupingSeparator(',');
                df.setGroupingSize(3);
                String totaltextRatio = df.format(textRatio);
                Chunk c2 = new Chunk(totaltextRatio + " %", normalFont);
                wc.add(c1);
                wc.add(c2);
                pdfDoc.add(wc);
                Chunk recom = null;
                Object[] chars = new String[1];
                chars[0] = totaltextRatio;
                if ((textRatio >= 0.0) && (textRatio <= 10.0)) {
                    recom = new Chunk(context.getMessage("message.seotextratiobad", chars, Locale.UK) + " ", normalFont);
                } else if ((textRatio >= 11.0) && (textRatio <= 24.0)) {
                    recom = new Chunk(context.getMessage("message.seotextratiomod", chars, Locale.UK) + " ", normalFont);
                } else if (textRatio >= 25.0) {
                    recom = new Chunk(context.getMessage("message.seotextratiogood", chars, Locale.UK) + " ", normalFont);
                } else {
                    recom = new Chunk(context.getMessage("message.seonotextratio", chars, Locale.UK) + " ", normalFont);
                }
                wcRecom.add(recom);
                totalTextStyle.add(new Paragraph(wcRecom));
                pdfDoc.add(totalTextStyle);
            }
            //  The purpose of the alt attribute is to provide alternative text descriptions of your images. Keywords appearing in the Anchor text have a high relevance weight in Search Engines. It will help to appear  web pages in SERP."
            addSubTitle(pdfDoc, "Image Alt Attributes :", blackFont);
            float[] ancTableWidths = {60f, 30f};
            PdfPTable keywordsInAltAttributeTable = new PdfPTable(ancTableWidths);
            Map<String, Integer> keywordsINAltAttr = new HashMap<>();
            keywordsINAltAttr = spiderViewResults.getKeywordsFoundInAltAttributes();
            if (keywordsINAltAttr.size() > 0) {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                pdfDoc.add(totalTextStyle);
                keywordsInAltAttributeTable.setWidthPercentage(99.9f);
                PdfPCell[] kInAlt = new PdfPCell[2];
                Iterator<Map.Entry<String, Integer>> tempKeywordsINAlt = keywordsINAltAttr.entrySet().iterator();
                while (tempKeywordsINAlt.hasNext()) {
                    Map.Entry<String, Integer> keywordsINAlt = tempKeywordsINAlt.next();
                    if (keywordsINAlt.getKey() != null && !(keywordsINAlt.getKey().trim().equals(""))) {
                        PdfPCell cell1 = new PdfPCell(new Phrase(keywordsINAlt.getKey(), normalHeadingFont));
                        PdfPCell cell2 = new PdfPCell(new Phrase(keywordsINAlt.getValue().toString(), normalFont));
                        for (int i = 0; i < kInAlt.length; i++) {
                            if (i == 0) {
                                kInAlt[i] = cell1;
                            } else if (i == 1) {
                                kInAlt[i] = cell2;
                            }
                            kInAlt[i].setBorderColor(BaseColor.GRAY);
                            kInAlt[i].setBorderWidth(0.3f);
                            kInAlt[i].setPadding(8);
                            kInAlt[i].setSpaceCharRatio(6f);
                            keywordsInAltAttributeTable.addCell(kInAlt[i]);
                        }
                        keywordsInAltAttributeTable.completeRow();
                    }
                }
                pdfDoc.add(keywordsInAltAttributeTable);
            } else {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                totalTextStyle.add(new Paragraph("No image alt attributes found.", normalFont));
                pdfDoc.add(totalTextStyle);
            }

            // For Missing Alt tags in Images Attributes
            addSubTitle(pdfDoc, "Missing Image Alt Attributes :", blackFont);
            float[] miaTableWidths = {60f, 30f};
            PdfPTable missingImageAltAttributeTable = new PdfPTable(miaTableWidths);
            Map<String, Integer> missingImageAlts = new HashMap<>();
            missingImageAlts = spiderViewResults.getMissingImageAltAttributes();
            if (missingImageAlts.size() > 0) {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                pdfDoc.add(totalTextStyle);
                missingImageAltAttributeTable.setWidthPercentage(99.9f);
                PdfPCell url = new PdfPCell(new Phrase("Url", hpTableOrangeFont));
                url.setBorderColor(BaseColor.GRAY);
                url.setBorderWidth(0.2f);
                url.setPadding(8);
                url.setSpaceCharRatio(6f);
                PdfPCell count = new PdfPCell(new Phrase("Count", hpTableOrangeFont));
                count.setBorderColor(BaseColor.GRAY);
                count.setBorderWidth(0.2f);
                count.setPadding(8);
                count.setSpaceCharRatio(6f);
                missingImageAltAttributeTable.addCell(url);
                missingImageAltAttributeTable.addCell(count);
                missingImageAltAttributeTable.completeRow();
                Iterator<Map.Entry<String, Integer>> tempMissingAltTags = missingImageAlts.entrySet().iterator();
                while (tempMissingAltTags.hasNext()) {
                    Map.Entry<String, Integer> missingAltImage = tempMissingAltTags.next();
                    if (missingAltImage.getKey() != null && !(missingAltImage.getKey().trim().equals(""))) {
                        PdfPCell cell1 = new PdfPCell(new Phrase(missingAltImage.getKey(), normalHeadingFont));
                        PdfPCell cell2 = new PdfPCell(new Phrase(missingAltImage.getValue().toString(), normalFont));

                        cell1.setBorderColor(BaseColor.GRAY);
                        cell1.setBorderWidth(0.3f);
                        cell1.setPadding(8);
                        cell1.setSpaceCharRatio(6f);
                        missingImageAltAttributeTable.addCell(cell1);

                        cell2.setBorderColor(BaseColor.GRAY);
                        cell2.setBorderWidth(0.3f);
                        cell2.setPadding(8);
                        cell2.setSpaceCharRatio(6f);
                        missingImageAltAttributeTable.addCell(cell2);
                        missingImageAltAttributeTable.completeRow();
                    }
                }
                pdfDoc.add(missingImageAltAttributeTable);
            } else {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(4f);
                totalTextStyle.add(new Paragraph("Warning: No missing image alt attributes found.", normalFont));
                pdfDoc.add(totalTextStyle);
            }
            addTitlePage(pdfDoc, "The Page Displayed within Search Engine Results", "This is an example of how your site might appear as a listing within search results.", orangeFont, normalFont);
            List<String> pdInsr = new ArrayList<>();
            PdfPCell[] cells = new PdfPCell[2];

            pdInsr = spiderViewResults.getPageDisplayedInSerachResults();
            for (int i = 0; i < pdInsr.size(); i++) {
                Paragraph paragraph = new Paragraph();
                String primaryUrl = (String) session.getAttribute("Url");
                paragraph.setLeading(9f);
                switch (i) {
                    case 0: {
                        Anchor a_href;
                        if (pdInsr.get(i) != null && pdInsr.get(i).trim().equals("")) {
                            a_href = new Anchor("No title found.", forBoldAnchor);
                        } else {
                            a_href = new Anchor(pdInsr.get(i), forBoldAnchor);
                        }
                        a_href.setReference(primaryUrl);
                        paragraph.add(a_href);
                        pdfDoc.add(paragraph);
                        break;
                    }
                    case 1:
                        if (pdInsr.get(i) != null && !pdInsr.get(i).trim().equals("")) {
                            paragraph.add(new Paragraph(pdInsr.get(i), normalFont));
                            pdfDoc.add(paragraph);
                        }
                        break;
                    default: {
                        paragraph.setSpacingBefore(5f);
                        paragraph.setSpacingAfter(5f);
                        Anchor a_href = new Anchor(primaryUrl, forNormalAnchor);
                        a_href.setReference(primaryUrl);
                        paragraph.add(a_href);
                        pdfDoc.add(paragraph);
                        break;
                    }
                }
            }
            String keyPhrasesMsg = "Search engines and users are both seeking the targeted keywords in the text of the page. Employing keywords in the page is not only a best practice, but an essential part of SEO (and good user experience). BUT there is evidence that excessive use of keywords can negatively impact rankings and thus suggest moderation.";
            addTitlePage(pdfDoc, "Page Most Relevant Keywords/Keyphrases", keyPhrasesMsg, orangeFont, normalFont);
            List<Map<String, PhraseStat>> relevantKeywordsAndKeyphrases = new ArrayList<>();
            relevantKeywordsAndKeyphrases = spiderViewResults.getKeywordPhrses();
            if (relevantKeywordsAndKeyphrases != null) {
                for (int i = 0; i < relevantKeywordsAndKeyphrases.size(); i++) {
                    String wordsType = " ";
                    Paragraph totalTextStyle = new Paragraph();
                    totalTextStyle.setSpacingAfter(5f);
                    totalTextStyle.setSpacingBefore(7f);
                    float[] twoWidths = {60f, 30f};
                    PdfPTable singleWordTable = new PdfPTable(twoWidths);
                    singleWordTable.setWidthPercentage(99.9f);
                    if (i == 0) {
                        wordsType = "Single Keywords :";
                        totalTextStyle.add(new Paragraph(wordsType, smallOrangeFont));
                        Iterator<Map.Entry<String, PhraseStat>> singleWords = relevantKeywordsAndKeyphrases.get(i).entrySet().iterator();
                        if (relevantKeywordsAndKeyphrases.get(i).size() > 0) {
                            PdfPCell[] totalCells = new PdfPCell[2];
                            PdfPCell keywords = new PdfPCell(new Phrase("Keywords", hpTableOrangeFont));
                            PdfPCell count = new PdfPCell(new Phrase("Count", hpTableOrangeFont));
                            for (int k = 0; k < totalCells.length; k++) {
                                if (k == 0) {
                                    totalCells[k] = keywords;
                                } else if (k == 1) {
                                    totalCells[k] = count;
                                }
                                totalCells[k].setBorderColor(BaseColor.GRAY);
                                totalCells[k].setBorderWidth(0.2f);
                                totalCells[k].setPadding(8);
                                totalCells[k].setSpaceCharRatio(6f);
                                singleWordTable.addCell(totalCells[k]);
                            }
                            singleWordTable.completeRow();
                            while (singleWords.hasNext()) {
                                Map.Entry<String, PhraseStat> words = singleWords.next();
                                PhraseStat phraseStat = words.getValue();
                                PdfPCell[] cellsData = new PdfPCell[2];
                                String keywordName = (String) words.getKey();
                                if (keywordName != null && !keywordName.trim().equals("")) {
                                    PdfPCell name = new PdfPCell(new Phrase(keywordName, normalHeadingFont));
                                    PdfPCell value = new PdfPCell(new Phrase(Integer.toString(phraseStat.count), normalFont));
                                    for (int j = 0; j < cells.length; j++) {
                                        if (j == 0) {
                                            cellsData[j] = name;
                                        } else if (j == 1) {
                                            cellsData[j] = value;
                                        }
                                        cellsData[j].setBorderColor(BaseColor.GRAY);
                                        cellsData[j].setBorderWidth(0.2f);
                                        cellsData[j].setPadding(8);
                                        cellsData[j].setSpaceCharRatio(6f);
                                        singleWordTable.addCell(cellsData[j]);
                                    }
                                    singleWordTable.completeRow();
                                }
                            }
                            pdfDoc.add(totalTextStyle);
                            pdfDoc.add(new Paragraph(" "));
                            pdfDoc.add(singleWordTable);
                        } else {
                            Phrase warning = new Phrase();
                            Chunk header = new Chunk("Warning :", smallFont);
                            Chunk errorMsg = new Chunk("No meaningful Single Keywords found.", normalFont);
                            warning.add(header);
                            warning.add(errorMsg);
                            pdfDoc.add(warning);
                        }
                    } else {
                        switch (i) {
                            case 1:
                                wordsType = "Two Word Keyphrases ";
                                break;
                            case 2:
                                wordsType = "Three Word Keyphrases ";
                                break;
                            case 3:
                                wordsType = "Four Word Keyphrases ";
                                break;
                            default:
                                break;
                        }
                        totalTextStyle.add(new Paragraph(wordsType + " " + ":", smallOrangeFont));
                        pdfDoc.add(totalTextStyle);
                        Iterator<Map.Entry<String, PhraseStat>> multiWordPhrases = relevantKeywordsAndKeyphrases.get(i).entrySet().iterator();
                        float[] threeWidths = {60f, 15f, 15f};
                        PdfPTable multiWordTable = new PdfPTable(threeWidths);
                        multiWordTable.setWidthPercentage(99.9f);
                        if (relevantKeywordsAndKeyphrases.get(i).size() > 0) {
                            PdfPCell[] totalCells = new PdfPCell[3];
                            PdfPCell keywords = new PdfPCell(new Phrase("Keywords", hpTableOrangeFont));
                            PdfPCell rating = new PdfPCell(new Phrase("Rating", hpTableOrangeFont));
                            PdfPCell count = new PdfPCell(new Phrase("Count", hpTableOrangeFont));
                            for (int k = 0; k < totalCells.length; k++) {
                                switch (k) {
                                    case 0:
                                        totalCells[k] = keywords;
                                        break;
                                    case 1:
                                        totalCells[k] = rating;
                                        break;
                                    case 2:
                                        totalCells[k] = count;
                                        break;
                                    default:
                                        break;
                                }
                                totalCells[k].setBorderColor(BaseColor.GRAY);
                                totalCells[k].setBorderWidth(0.2f);
                                totalCells[k].setPadding(8);
                                totalCells[k].setSpaceCharRatio(6f);
                                multiWordTable.addCell(totalCells[k]);
                            }
                            multiWordTable.completeRow();
                            while (multiWordPhrases.hasNext()) {
                                Map.Entry<String, PhraseStat> words = multiWordPhrases.next();
                                PhraseStat phraseStat = words.getValue();
                                PdfPCell[] cellsData = new PdfPCell[3];
                                String keywordName = (String) words.getKey();
                                if (keywordName != null && !keywordName.trim().equals("")) {
                                    PdfPCell name = new PdfPCell(new Phrase(keywordName, normalHeadingFont));
                                    PdfPCell value1 = new PdfPCell(new Phrase(Float.toString(phraseStat.rating), normalFont));
                                    PdfPCell value2 = new PdfPCell(new Phrase(Integer.toString(phraseStat.count), normalFont));
                                    for (int j = 0; j < cellsData.length; j++) {
                                        switch (j) {
                                            case 0:
                                                cellsData[j] = name;
                                                break;
                                            case 1:
                                                cellsData[j] = value1;
                                                break;
                                            case 2:
                                                cellsData[j] = value2;
                                                break;
                                            default:
                                                break;
                                        }
                                        cellsData[j].setBorderColor(BaseColor.GRAY);
                                        cellsData[j].setBorderWidth(0.2f);
                                        cellsData[j].setPadding(8);
                                        cellsData[j].setSpaceCharRatio(6f);
                                        multiWordTable.addCell(cellsData[j]);
                                    }
                                    multiWordTable.completeRow();
                                }
                            }
                            pdfDoc.add(new Paragraph(" "));
                            pdfDoc.add(multiWordTable);
                        } else {
                            Phrase warning = new Phrase();
                            Chunk header = new Chunk("Warning :", smallFont);
                            Chunk errorMsg = new Chunk("No meaningful " + " " + wordsType + " " + "found.", normalFont);
                            warning.add(header);
                            warning.add(errorMsg);
                            pdfDoc.add(warning);
                        }
                    }
                }
            } else {
                Phrase warning = new Phrase();
                Chunk header = new Chunk("Warning :", smallFont);
                Chunk errorMsg = new Chunk("No keywords found.", normalFont);
                warning.add(header);
                warning.add(errorMsg);
                pdfDoc.add(warning);
            }
            String desc = "This section lists the URLs found within the page " + (String) session.getAttribute("Url") + " and the number of times each was found.Google used to index only about 100 kilobytes of a page. When we thought about how many links a page might reasonably have and still be under 100K, it seemed about right to recommend 100 links .";
            addTitlePage(pdfDoc, "URLs found in the page", desc, orangeFont, normalFont);
            Map<Integer, Map<String, Integer>> totalUrls = new HashMap<>();
            totalUrls = spiderViewResults.getAnchorLinks();
            Map<String, Integer> includingDuplicates = new HashMap<>();
            includingDuplicates = totalUrls.get(1);
            for (int i = 1; i <= totalUrls.size(); i++) {
                Paragraph totalTextStyle = new Paragraph();
                totalTextStyle.setSpacingAfter(5f);
                totalTextStyle.setSpacingBefore(7f);
                if (totalUrls.get(i).size() > 0) {
                    if (i == 1) {
                        int dups = 0;
                        Iterator<Map.Entry<String, Integer>> dupLinks = includingDuplicates.entrySet().iterator();
                        while (dupLinks.hasNext()) {
                            Map.Entry<String, Integer> tempILinks = dupLinks.next();
                            dups = tempILinks.getValue();
                        }
                        totalTextStyle.add(new Paragraph("Overview : ", smallFont));
                        pdfDoc.add(totalTextStyle);
                        int uniqueUrls = totalUrls.get(2).size() + totalUrls.get(3).size() + totalUrls.get(4).size();
                        pdfDoc.add(new Paragraph("Found" + " " + dups + " " + "URLs of which" + " " + uniqueUrls + " " + "are unique.", normalFont));
                    } else {
                        String tempLinkName = " ";
                        switch (i) {
                            case 2:
                                tempLinkName = "Internal Links: ";
                                break;
                            case 3:
                                tempLinkName = "External Links: ";
                                break;
                            case 4:
                                tempLinkName = "Social Links: ";
                                break;
                            default:
                                break;
                        }
                        totalTextStyle.add(new Paragraph(tempLinkName, smallFont));
                        pdfDoc.add(totalTextStyle);
                        Iterator<Map.Entry<String, Integer>> tempLinks = totalUrls.get(i).entrySet().iterator();
                        while (tempLinks.hasNext()) {
                            Map.Entry<String, Integer> ancLinks = tempLinks.next();
                            if (!ancLinks.getKey().equals("") && ancLinks.getKey() != null) {
                                com.itextpdf.text.List unorderedList = new com.itextpdf.text.List(com.itextpdf.text.List.UNORDERED);
                                Anchor a_href = new Anchor(ancLinks.getKey() + " " + "-" + " " + ancLinks.getValue(), forNormalAnchor);
                                a_href.setReference(ancLinks.getKey());
                                unorderedList.setListSymbol("->" + "  ");
                                Phrase totalAnc = new Phrase();
                                Chunk c1 = new Chunk(ancLinks.getKey() + " ", normalFont);
                                Chunk c2 = new Chunk("-" + " " + ancLinks.getValue(), normalFont);
                                try {
                                    c1.setAction(new PdfAction(new URL(ancLinks.getKey().trim())));
                                    c1.setUnderline(0.5f, -0.5f);
                                    c1.setLineHeight(20f);
                                    c2.setLineHeight(20f);
                                    c2.setAction(new PdfAction(new URL(ancLinks.getKey().trim())));
                                    totalAnc.add(c1);
                                    totalAnc.add(c2);
                                } catch (MalformedURLException me) {
                                    logger.error("MalformedURLException in PDF ", me);
                                }
                                unorderedList.add(new ListItem(totalAnc));
                                pdfDoc.add(unorderedList);
                            }
                        }
                    }
                } else {
                    com.itextpdf.text.List unorderedList = new com.itextpdf.text.List(com.itextpdf.text.List.UNORDERED);
                    unorderedList.setListSymbol("->" + "  ");
                    String tempLinkName = " ";
                    switch (i) {
                        case 2:
                            tempLinkName = "Internal Links:";
                            unorderedList.add(new ListItem("No Internal Links found.", normalFont));
                            break;
                        case 3:
                            tempLinkName = "ExternalLinks:";
                            unorderedList.add(new ListItem("No External Links found.", normalFont));
                            break;
                        case 4:
                            tempLinkName = "Social Links:";
                            unorderedList.add(new ListItem("No Social Links found.", normalFont));
                            break;
                        default:
                            break;
                    }
                    totalTextStyle.add(new Paragraph(tempLinkName, smallFont));
                    pdfDoc.add(totalTextStyle);
                    pdfDoc.add(unorderedList);
                }
            }
            //For Http Header Check
            String headersDef = "To ensure that search engines understand your web site structure correctly you should control each page's HTTP headers. Your server should handle all requests properly - by this you will both achieve a better crawl rate and higher rankings.";
            addTitlePage(pdfDoc, "HTTP Headers Check", headersDef, orangeFont, normalFont);
            float[] widths = {30f, 60f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100f);
            PdfPCell[] cellsHttp = new PdfPCell[2];
            if (httpHeaders.size() > 0) {
                Iterator<Map.Entry<String, String>> headerFields = httpHeaders.entrySet().iterator();
                while (headerFields.hasNext()) {
                    Map.Entry<String, String> headerName = headerFields.next();
                    if (!headerName.getKey().trim().equals("") && headerName.getKey() != null && !headerName.getValue().trim().equals("") && headerName.getValue() != null) {
                        PdfPCell cell1 = new PdfPCell(new Phrase(headerName.getKey(), normalHeadingFont));
                        PdfPCell cell2 = new PdfPCell(new Phrase(headerName.getValue(), normalFont));
                        table.completeRow();
                        for (int i = 0; i < cellsHttp.length; i++) {
                            if (i == 0) {
                                cellsHttp[i] = cell1;
                            } else if (i == 1) {
                                cellsHttp[i] = cell2;
                            }
                            cellsHttp[i].setBorderColor(BaseColor.GRAY);
                            cellsHttp[i].setBorderWidth(0.2f);
                            cellsHttp[i].setPadding(8);
                            cellsHttp[i].setSpaceCharRatio(6f);
                            table.addCell(cellsHttp[i]);
                        }
                        table.completeRow();
                    }
                }
                pdfDoc.add(table);
            } else {
                Phrase warning = new Phrase();
                Chunk header = new Chunk("Warning :", smallFont);
                Chunk errorMsg = new Chunk("No http headers found.", normalFont);
                warning.add(header);
                warning.add(errorMsg);
                pdfDoc.add(warning);
            }

            pdfDoc.close();
            return filename;
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException  :", e);
        } catch (DocumentException de) {
            logger.error("DocumentException  :", de);
        } catch (IOException | NumberFormatException | NoSuchMessageException e1) {
            logger.error("Exception  :", e1);
        }
        return null;
    }

    public void addTitlePage(com.itextpdf.text.Document pdfDoc, String heading, String description,
            Font orangeFont, Font normalFont) throws DocumentException {

        Paragraph paragraph = new Paragraph();
        paragraph.add(new Paragraph(heading, orangeFont));
        paragraph.setSpacingBefore(10f);
        paragraph.setSpacingAfter(30f);
        paragraph.add(new Paragraph(description, normalFont));
        paragraph.setSpacingBefore(13f);
        paragraph.setSpacingAfter(13f);
        pdfDoc.add(paragraph);
    }

    public void addSubTitle(com.itextpdf.text.Document pdfDoc, String heading, Font blackFont) throws DocumentException {
        Paragraph paragraph = new Paragraph();
        paragraph.setSpacingBefore(9f);
        paragraph.add(new Paragraph(heading, blackFont));
        paragraph.setSpacingAfter(5f);
        pdfDoc.add(paragraph);
    }

    class ValueComparator implements Comparator<String> {

        Map<String, Integer> data;

        public ValueComparator(Map<String, Integer> data) {
            this.data = data;
        }

        @Override
        public int compare(String a, String b) {
            if (data.get(a) >= data.get(b)) {
                return -1;
            } else {
                return 1;
            }
        }
    }

//    public static int checkCurrentURLAndGetHTML(String analysisUrl, StringBuilder pageHTML) {
//        String currentUrl = analysisUrl;
//        int statusCode = 0;
//        if (!(currentUrl == null || currentUrl.equals(""))) {
//            StringBuilder redirectedUrl = new StringBuilder("");
//            String httpCurrentUrl = "";
//
//            if (currentUrl.startsWith("http://") || currentUrl.startsWith("https://")) {
//                httpCurrentUrl = currentUrl;
//            } else {
//                httpCurrentUrl = "http://" + currentUrl;
//            }
//
//            List<List<String>> cookieContainer = new ArrayList<>();
//            statusCode = getCurrentStatusCodeAndHTML(httpCurrentUrl, redirectedUrl, pageHTML, cookieContainer);
//            logger.info("Status Code before redirection checking: " + statusCode + " currentUrl: " + currentUrl + " redirectedUrl: " + redirectedUrl.toString());
//
//            if (statusCode / 100 == 3) {
//                try {
//                    URL urlredirected = new URL(redirectedUrl.toString());
//                    URL urlCurrent = new URL(httpCurrentUrl);
//                    if (urlredirected.getHost().equals(urlCurrent.getHost())) {
//                        statusCode = getCurrentStatusCodeAndHTML(redirectedUrl.toString(), redirectedUrl, pageHTML, cookieContainer);
//                    }
//                } catch (MalformedURLException e) {
//                    logger.error(e);
//                }
//
//            }
//        }
//        return statusCode;
//    }
//
//    public static int getCurrentStatusCodeAndHTML(String currentUrl, StringBuilder redirectedUrl,
//            StringBuilder pageHTML, List<List<String>> cookieContainer) {
//
//        int responseCode = 0;
//        int currentResponseCode = 0;
//        HttpURLConnection currentUrlConn = null;
//        int iteration = 0;
//        while ((responseCode == 0 || responseCode / 100 == 3) && iteration < 5) {
//            try {
//                URL currentUrlObj = new URL(currentUrl);
//                if (!(currentUrlObj.equals(" ") || currentUrlObj.equals("null"))) {
//                    currentUrlConn = (HttpURLConnection) currentUrlObj.openConnection();
//                    currentUrlConn.setInstanceFollowRedirects(false);
//                    currentUrlConn.setConnectTimeout(CONNECT_TIME_OUT);
//                    currentUrlConn.setReadTimeout(READ_TIME_OUT);
//                    currentUrlConn.setRequestProperty("User-Agent", Common.USER_AGENT);
//
//                    responseCode = currentUrlConn.getResponseCode();
//                    List<String> tempCookies = currentUrlConn.getHeaderFields().get("Set-Cookie");
//                    if (tempCookies != null && tempCookies.size() > 0 && cookieContainer != null) {
//                        if (cookieContainer.size() > 0) {
//                            cookieContainer.set(0, tempCookies);
//                        } else {
//                            cookieContainer.add(tempCookies);
//                        }
//                    }
//                    if (iteration == 0) {
//                        currentResponseCode = responseCode;
//                    }
//                    logger.debug("currentUrl: " + currentUrl + "\tresponseCode: " + responseCode);
//                    if (responseCode / 100 == 3) {
//                        if (iteration != 4) {
//                            String redirectedLocation = currentUrlConn.getHeaderField("Location");
//                            String tempUrl = currentUrlObj.getProtocol() + "://" + currentUrlObj.getHost();
//                            if (redirectedLocation != null) {
//                                if (!(redirectedLocation.startsWith("http"))) {
//                                    if (redirectedLocation.startsWith("/")) {
//                                        currentUrl = tempUrl + redirectedLocation;
//                                    } else if (!(redirectedLocation.startsWith("/"))) {
//                                        if (currentUrl.endsWith("/")) {
//                                            currentUrl = currentUrl + redirectedLocation;
//                                        } else {
//                                            currentUrl = currentUrl + "/" + redirectedLocation;
//                                        }
//                                    }
//                                } else {
//                                    currentUrl = redirectedLocation;
//                                }
//                            }
//                        } else {
//                            redirectedUrl.append("");
//                        }
//                    } else if (responseCode == 200) {
//                        String line;
//                        pageHTML.setLength(0);
//                        String charset = "UTF-8";
//                        try {
//                            int cInd = currentUrlConn.getContentType().indexOf("charset=");
//                            if (cInd != -1) {
//                                charset = currentUrlConn.getContentType().substring(cInd + 8);
//                            }
//                        } catch (NullPointerException e) {
//                            logger.error(e);
//                        }
//                        BufferedReader reader = null;
//                        try {
//                            reader = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream(), charset));
//                        } catch (Exception e) {
//                            reader = new BufferedReader(new InputStreamReader(currentUrlConn.getInputStream(), "UTF-8"));
//                        }
//                        while ((line = reader.readLine()) != null) {
//                            pageHTML.append(line);
//                        }
//                        if (iteration != 0) {
//                            redirectedUrl.append(currentUrl);
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                logger.error("Exception in getCurrentStatusCodeAndHTML when connection to " + currentUrl + ", Cause: " + e);
//            }
//            iteration++;
//        }
//        return currentResponseCode;
//    }
}
