package lxr.marketplace.spiderview;

import java.util.List;
import java.util.Map;
import lxr.marketplace.util.findPhrase.PhraseStat;
import org.jsoup.select.Elements;

public class SpiderViewResult {

    Map<String, String> httpHeaders;
    List<String> titleTag;
    List<String> metaDescriptionTag;
    long totalWordCount;
    List<String> pageDisplayedInSerachResults;
    List<Map<String, PhraseStat>> KeywordPhrses;
    Map<Integer, Map<String, Integer>> anchorLinks;
    Map<String, Integer> keywordsFoundInAnchorTags;
    Map<String, Integer> keywordsFoundInAltAttributes;
    Map<Integer, Elements> headingAndPhraseElements;
    double textRatio;
    Map<String, Integer> missingImageAltAttributes;

    public double getTextRatio() {
        return textRatio;
    }

    public void setTextRatio(double textRatio) {
        this.textRatio = textRatio;
    }

    public Map<String, String> getHttpHeaders() {
        return httpHeaders;
    }

    public List<String> getTitleTag() {
        return titleTag;
    }

    public List<String> getMetaDescriptionTag() {
        return metaDescriptionTag;
    }

    public List<String> getPageDisplayedInSerachResults() {
        return pageDisplayedInSerachResults;
    }

    public List<Map<String, PhraseStat>> getKeywordPhrses() {
        return KeywordPhrses;
    }

    public Map<Integer, Map<String, Integer>> getAnchorLinks() {
        return anchorLinks;
    }

    public Map<String, Integer> getKeywordsFoundInAnchorTags() {
        return keywordsFoundInAnchorTags;
    }

    public Map<String, Integer> getKeywordsFoundInAltAttributes() {
        return keywordsFoundInAltAttributes;
    }

    public Map<Integer, Elements> getHeadingAndPhraseElements() {
        return headingAndPhraseElements;
    }

    public void setHttpHeaders(Map<String, String> httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    public void setTitleTag(List<String> titleTag) {
        this.titleTag = titleTag;
    }

    public void setMetaDescriptionTag(List<String> metaDescriptionTag) {
        this.metaDescriptionTag = metaDescriptionTag;
    }

    public void setPageDisplayedInSerachResults(
            List<String> pageDisplayedInSerachResults) {
        this.pageDisplayedInSerachResults = pageDisplayedInSerachResults;
    }

    public void setKeywordPhrses(List<Map<String, PhraseStat>> keywordPhrses) {
        KeywordPhrses = keywordPhrses;
    }

    public void setAnchorLinks(Map<Integer, Map<String, Integer>> anchorLinks) {
        this.anchorLinks = anchorLinks;
    }

    public void setKeywordsFoundInAnchorTags(
            Map<String, Integer> keywordsFoundInAnchorTags) {
        this.keywordsFoundInAnchorTags = keywordsFoundInAnchorTags;
    }

    public void setKeywordsFoundInAltAttributes(
            Map<String, Integer> keywordsFoundInAltAttributes) {
        this.keywordsFoundInAltAttributes = keywordsFoundInAltAttributes;
    }

    public void setHeadingAndPhraseElements(
            Map<Integer, Elements> headingAndPhraseElements) {
        this.headingAndPhraseElements = headingAndPhraseElements;
    }

    public long getTotalWordCount() {
        return totalWordCount;
    }

    public void setTotalWordCount(long totalWordCount) {
        this.totalWordCount = totalWordCount;
    }

    public Map<String, Integer> getMissingImageAltAttributes() {
        return missingImageAltAttributes;
    }

    public void setMissingImageAltAttributes(Map<String, Integer> missingImageAltAttributes) {
        this.missingImageAltAttributes = missingImageAltAttributes;
    }

}
