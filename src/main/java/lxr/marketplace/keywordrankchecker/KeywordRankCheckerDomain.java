package lxr.marketplace.keywordrankchecker;

import java.sql.Timestamp;

public class KeywordRankCheckerDomain {

	long domainId;
	String domain;
	long userId;
	boolean isUserDomain;
	Timestamp addedDate;
	
	public long getDomainId() {
		return domainId;
	}
	public void setDomainId(long domainId) {
		this.domainId = domainId;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isUserDomain() {
		return isUserDomain;
	}
	public void setUserDomain(boolean isUserDomain) {
		this.isUserDomain = isUserDomain;
	}
	public Timestamp getAddedDate() {
		return addedDate;
	}
	public void setAddedDate(Timestamp addedDate) {
		this.addedDate = addedDate;
	}
	
	
}
