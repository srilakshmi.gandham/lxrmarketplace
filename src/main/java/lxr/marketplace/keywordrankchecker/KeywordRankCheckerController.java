package lxr.marketplace.keywordrankchecker;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.home.HomePageService;
import lxr.marketplace.session.Session;
import lxr.marketplace.session.ToolUsage;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;
import net.sf.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

@SuppressWarnings("deprecation")
public class KeywordRankCheckerController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(KeywordRankCheckerController.class);
    EmailTemplateService emailTemplateService;
    ToolService toolService;
    String downloadFolder;
    KeywordRankCheckerService keywordRankCheckerService;
    private static final int GUEST_USER_KEYWOD_LIMIT = 5;
    private static final int REG_USER_KEYWOD_LIMIT = 5;
    private static final int GUEST_USER_DOMAIN_LIMIT = 4;
    private static final int REG_USER_DOMAIN_LIMIT = 4;

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    JSONObject userInputJson = null;
    private Boolean isToolInputChange = false;

    @Autowired
    private HomePageService homePageService;
    @Autowired
    private MessageSource messageSource;

    public KeywordRankCheckerService getKeywordRankCheckerService() {
        return keywordRankCheckerService;
    }

    public void setKeywordRankCheckerService(
            KeywordRankCheckerService keywordRankCheckerService) {
        this.keywordRankCheckerService = keywordRankCheckerService;
    }

    public static void setLogger(Logger logger) {
        KeywordRankCheckerController.logger = logger;
    }

    public static Logger getLogger() {
        return logger;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    public KeywordRankCheckerController() {
        setCommandClass(KeywordRankChecker.class);
        setCommandName("keywordRankCheckerTool");
    }

    @Override

    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        KeywordRankChecker keywordRankChecker = (KeywordRankChecker) command;
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(10);
            toolObj.setToolName("Weekly SEO Rank Checker");
            toolObj.setToolLink("weekly-keyword-rank-checker-tool.html");
            session.setAttribute("toolObj", toolObj);
            boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            Long userIdInRankUser = 0l;
            Session userSession = (Session) session.getAttribute("userSession");
            if (userSession.getUserId() > 0) {
                userIdInRankUser = keywordRankCheckerService.fetchRankUserIdFromRankUserLoggedIn(userSession.getUserId());
                if (userIdInRankUser == null) {
                    userIdInRankUser = 0l;
                }
            }

            Map<Integer, ToolUsage> toolUsageInfo = (Map<Integer, ToolUsage>) userSession.getToolUsage();
            session.setAttribute("currentTool", Common.KEYWORD_RANK_CHECKER);
            session.setAttribute("userId", user.getId());
            if (session.getAttribute("requestFromMail") != null && ((session.getAttribute("user-id") != null
                    && session.getAttribute("tracking-id") != null) || (session.getAttribute("question-id") != null))) {
                SqlRowSet sqlRowSet = null;
                if (session.getAttribute("user-id") != null && session.getAttribute("tracking-id") != null) {
                    long decriptedUserId;
                    long decriptedWebsiteTrackingId;
                    decriptedUserId = (Long) session.getAttribute("user-id");
                    decriptedWebsiteTrackingId = (Long) session.getAttribute("tracking-id");
                    sqlRowSet = homePageService.checkUserIdAndTrackingId(decriptedUserId, decriptedWebsiteTrackingId);
                } else if (session.getAttribute("question-id") != null) {
                    long questionId = (Long) session.getAttribute("question-id");
                    sqlRowSet = homePageService.checkQuestionId(questionId);
                }
                if (sqlRowSet != null) {
                    while (sqlRowSet.next()) {
                        String userInput = sqlRowSet.getString("other_inputs");
                        JSONObject userInputJsonObj = new JSONObject(userInput);
                        keywordRankChecker.setAlertStatus((Boolean) userInputJsonObj.get("2"));
                        keywordRankChecker.setWeeklyReport((Boolean) userInputJsonObj.get("3"));
                        keywordRankChecker.setToolUnsubscription((Boolean) userInputJsonObj.get("4"));
                        int i = 5, j;
                        CompDomainSettings compDomainSettings;
                        List<CompDomainSettings> compDomainSettingsList = new ArrayList<>();
                        userInputJsonObj.length();
//                        Iterate CompDomainSettingRows from json object and set CompDomainSettingRows into keywordRankChecker object
                        for (i = 5; i < userInputJsonObj.length() - 3; i++) {
                            compDomainSettings = new CompDomainSettings();
                            if (userInputJsonObj.has("cName" + Integer.toString(i))) {
                                if (i == 5) {
                                    keywordRankChecker.setUserdomain(userInputJsonObj.get("cName" + Integer.toString(i)).toString());
                                    compDomainSettings.setDomain(userInputJsonObj.get("cName" + Integer.toString(i)).toString());
                                    compDomainSettings.setDomainId((Integer) userInputJsonObj.get("cDomainId" + Integer.toString(i)));
                                    compDomainSettings.setUserDomain((Boolean) userInputJsonObj.get("cIsUserDomain" + Integer.toString(i)));
                                } else if (userInputJsonObj.get("cName" + Integer.toString(i)) != "") {
                                    compDomainSettings.setDomain(userInputJsonObj.get("cName" + Integer.toString(i)).toString());
                                    compDomainSettings.setDomainId((Integer) userInputJsonObj.get("cDomainId" + Integer.toString(i)));
                                    compDomainSettings.setUserDomain((Boolean) userInputJsonObj.get("cIsUserDomain" + Integer.toString(i)));
                                }
                                compDomainSettingsList.add(compDomainSettings);
                            } else {
                                break;
                            }
                        }
                        keywordRankChecker.setCompDomainSettingRows(compDomainSettingsList);

                        KeywordSettings keywordSettings;
                        List<KeywordSettings> keywordSettingsList = new ArrayList<>();
//                        Remaining keys length
                        int k = userInputJsonObj.length() - (3 + i);
//                        Iterate KeywordSettingsRows from json object and set KeywordSettingsRows into keywordRankChecker object
                        for (j = i; j < k; j++) {
                            if (userInputJsonObj.has("kName" + Integer.toString(i))) {
                                keywordSettings = new KeywordSettings();
                                if (userInputJsonObj.get("kName" + Integer.toString(i)) != "") {
                                    keywordSettings.setKeyword(userInputJsonObj.get("kName" + Integer.toString(i)).toString());
                                    keywordSettings.setKeywordId((Integer) userInputJsonObj.get("kId" + Integer.toString(i)));
                                    keywordSettings.setAlertOperator((Integer) userInputJsonObj.get("kOperator" + Integer.toString(i)));
                                    keywordSettings.setAlertValue(userInputJsonObj.get("kValue" + Integer.toString(i)).toString());
                                    keywordSettings.setGoogleCheckbox((Boolean) userInputJsonObj.get("kIsGoogle" + Integer.toString(i)));
                                    keywordSettings.setBingCheckbox((Boolean) userInputJsonObj.get("kIsBing" + Integer.toString(i)));
                                    keywordSettingsList.add(keywordSettings);
                                } else {
                                    keywordSettings.setKeyword("");
                                }
                                i++;
                            } else {
                                break;
                            }
                        }
                        keywordRankChecker.setKeywordSettingsRows(keywordSettingsList);
                    }
                }
            }
            if (WebUtils.hasSubmitParameter(request, "getResult") && ((userLoggedIn && userIdInRankUser == 0)
                    || !userLoggedIn)) {

                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    if (userLoggedIn) {
                        Long guestRankUserId = (Long) session.getAttribute("GuestRankUserId");
                        if (guestRankUserId != null) {
                            keywordRankCheckerService.updateGuestEntryToLoggedIn(guestRankUserId, user.getId());
                        } else {
                            logger.info("No Guest user rank id found in login refresh.");
                        }
                    }
                    session.setAttribute("loginrefresh", "loginrefresh");

                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                    Long guestRankUserId = (Long) session.getAttribute("GuestRankUserId");
                    if (userLoggedIn && guestRankUserId != null) {
                        mAndV.addObject("showSettings", "success");
                    } else {
                        mAndV.addObject("showSettings", "fail");
                    }
                    if (guestRankUserId != null) {
                        mAndV.addObject("showSuccess", "success");
                        mAndV.addObject("googleGraph", "true");
                    }

                    HashMap<String, String> geoLocation = Common.loadGeoLocations();
                    mAndV.addObject("geoLocation", geoLocation);
                    HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                    mAndV.addObject("loadLanguages", loadLanguages);
                    session.removeAttribute("GuestRankUserId");
                    return mAndV;
                }
                session.removeAttribute("rankCheckerURlATE");
                String userDomainValidation = Common.getUrlValidation(keywordRankChecker.getUserdomain().trim(), session);
                if (userDomainValidation.equals("invalid")) {
                    String notWorkingUrl = messageSource.getMessage("MultipleURL.error", null, Locale.US);
                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                    mAndV.addObject("notWorkingUrl", notWorkingUrl);
                    mAndV.addObject("invalidUrl", keywordRankChecker.getUserdomain().trim());
                    keywordRankChecker.setLanguage(keywordRankChecker.getLanguage());
                    keywordRankChecker.setLocation(keywordRankChecker.getLocation());
                    mAndV.addObject("showSettings", "fail");
                    mAndV.addObject("showSuccess", "fail");
                    HashMap<String, String> geoLocation = Common.loadGeoLocations();
                    mAndV.addObject("geoLocation", geoLocation);
                    HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                    mAndV.addObject("loadLanguages", loadLanguages);
                    mAndV.addObject("googleGraph", "false");
                    session.setAttribute("rankCheckerURlATE", true);
                    return mAndV;

                }
                List<String> compdomainsList = Common.convertStringToArrayList(keywordRankChecker.getCompDomains(), "\n");
                for (String compUrl : compdomainsList) {
                    String compUrlsValidation = Common.getUrlValidation(compUrl.trim(), session);
                    if (compUrlsValidation.equals("invalid")) {
                        String notWorkingUrl = messageSource.getMessage("MultipleURL.error", null, Locale.US);
                        ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                        mAndV.addObject("notWorkingUrl", notWorkingUrl);
                        mAndV.addObject("invalidUrl", compUrl.trim());
                        keywordRankChecker.setLanguage(keywordRankChecker.getLanguage());
                        keywordRankChecker.setLocation(keywordRankChecker.getLocation());
                        mAndV.addObject("showSettings", "fail");
                        mAndV.addObject("showSuccess", "fail");
                        HashMap<String, String> geoLocation = Common.loadGeoLocations();
                        mAndV.addObject("geoLocation", geoLocation);
                        HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                        mAndV.addObject("loadLanguages", loadLanguages);
                        mAndV.addObject("googleGraph", "false");
                        session.setAttribute("rankCheckerURlATE", true);
                        return mAndV;
                    }
                }
                if (user.getId() < 0) {
                    //convert the user to guest user on keyword rank checker usage.
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    user = (Login) session.getAttribute("user");
                }
                if (toolUsageInfo == null || (toolUsageInfo.get(toolObj.getToolId()) == null)) {
                    keywordRankCheckerService.deletePreviousRankUserdata(userSession.getUserId());
                }
                //   to delete guest users data
                if (toolUsageInfo != null && toolUsageInfo.get(toolObj.getToolId()) != null) {
                    Long rankUserId = keywordRankCheckerService.fetchRankUserIdFromRankUserGuest(userSession.getUserId());
                    if (rankUserId != null && rankUserId > 0) {
                        keywordRankCheckerService.deleteGuestUserDataByRankUserId(rankUserId);
                    }
                }
                userSession.addToolUsage(toolObj.getToolId());
                String market = Common.loadcustomSearchLanguages().get(keywordRankChecker.getLanguage()) + "-"
                        + Common.loadGeoLocations().get(keywordRankChecker.getLocation());
                String bingMarketValue = Common.loadFinalBingMarkets().get(market);
                Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                session.removeAttribute("sortedKeywordRows");
                session.removeAttribute("compDomainRows");
                session.removeAttribute("allKeywordChartData");
                session.removeAttribute("GuestRankUserId");
                Long rankUserId = keywordRankCheckerService.insertIntoRankCheckerUser(user.getId(), keywordRankChecker,
                        startTime, bingMarketValue, userLoggedIn);
                if (rankUserId != null) {
                    if (!userLoggedIn) {
                        session.setAttribute("GuestRankUserId", rankUserId);
                    }
                    String keyword = keywordRankChecker.getKeywords();
                    List<String> keywords = Common.convertStringToArrayList(keyword, "\n");
                    List<String> compdomains = Common.convertStringToArrayList(keywordRankChecker.getCompDomains(), "\n");
                    if (!userLoggedIn) {
                        if (keywords != null && keywords.size() > GUEST_USER_KEYWOD_LIMIT) {
                            keywords = keywords.subList(0, GUEST_USER_KEYWOD_LIMIT);
                        }
                        if (compdomains != null && compdomains.size() > GUEST_USER_DOMAIN_LIMIT - 1) {
                            compdomains = compdomains.subList(0, GUEST_USER_DOMAIN_LIMIT - 1);
                        }
                    } else {
                        if (keywords != null && keywords.size() > REG_USER_KEYWOD_LIMIT) {
                            keywords = keywords.subList(0, REG_USER_KEYWOD_LIMIT);
                        }
                        if (compdomains != null && compdomains.size() > REG_USER_DOMAIN_LIMIT - 1) {
                            compdomains = compdomains.subList(0, REG_USER_DOMAIN_LIMIT - 1);
                        }
                    }

                    keywordRankCheckerService.insertIntoRankCheckerKeyword(rankUserId, keywords, startTime);
                    List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(rankUserId);
                    session.setAttribute("keywordRows", keywordRows);

                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }

                    keywordRankCheckerService.insertIntoRankCheckerDomain(rankUserId,
                            keywordRankChecker.getUserdomain().trim(), true, startTime);

                    keywordRankCheckerService.insertDataIntoDomainTab(rankUserId, compdomains, startTime);
                    List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(rankUserId);
                    session.setAttribute("compDomainRows", compDomainRows);

                    List<String> domains = new ArrayList<>();
                    domains.addAll(compdomains);
                    domains.add(keywordRankChecker.getUserdomain().trim());

                    //for registered users
                    List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                    keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                    session.setAttribute("compDomainSettingRows", compDomainSettingRows);

                    //for registered users
                    List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                    keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                    session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                    session.setAttribute("keywordRankChecker", keywordRankChecker);

//                    if the request is coming from tool page then only add the user inputs to JSONObject
//                    if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                        session.removeAttribute("requestFromMail");
                    }
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        isToolInputChange = true;
                        userInputJson = new JSONObject();
                        userInputJson.put("2", keywordRankChecker.isAlertStatus());
                        userInputJson.put("3", keywordRankChecker.isWeeklyReport());
                        userInputJson.put("4", keywordRankChecker.isToolUnsubscription());
                        int i = 5;
                        List<CompDomainSettings> compDomainSettingsList = null;
                        if (keywordRankChecker.getCompDomainSettingRows() != null) {
                            compDomainSettingsList = keywordRankChecker.getCompDomainSettingRows();
                        }
                        for (CompDomainSettings compDomainSettings : compDomainSettingsList) {
                            userInputJson.put("cName" + i + "", compDomainSettings.getDomain());
                            userInputJson.put("cDomainId" + i + "", compDomainSettings.getDomainId());
                            userInputJson.put("cIsUserDomain" + i + "", compDomainSettings.isUserDomain());
                            i++;
                        }
                        List<KeywordSettings> keywordSettingsList = null;
                        if (keywordRankChecker.getKeywordSettingRows() != null) {
                            keywordSettingsList = keywordRankChecker.getKeywordSettingRows();
                        }
                        for (KeywordSettings keywordSettings : keywordSettingsList) {
                            userInputJson.put("kName" + i + "", keywordSettings.getKeyword());
                            userInputJson.put("kId" + i + "", keywordSettings.getKeywordId());
                            userInputJson.put("kOperator" + i + "", keywordSettings.getAlertOperator());
                            userInputJson.put("kValue" + i + "", keywordSettings.getAlertValue());
                            userInputJson.put("kIsGoogle" + i + "", keywordSettings.isGoogleCheckbox());
                            userInputJson.put("kIsBing" + i + "", keywordSettings.isBingCheckbox());
                            i++;
                        }
                    }

                    Map<Long, List<KeywordRankCheckerStats>> totalResultMap = keywordRankCheckerService
                            .getResults(keywordRows, compDomainRows, domains, keywordRankChecker, bingMarketValue);
                    session.setAttribute("totalResultMap", totalResultMap);
                    Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = keywordRankCheckerService
                            .createSortedMap(userLoggedIn, user.getId(), keywordRankChecker, compDomainRows, session);
                    List<KeywordSettings> sortedKeywordRows = keywordRankCheckerService.createSortedKeywordSettingsRows(keywordRows, session);
                    session.setAttribute("sortedKeywordRows", sortedKeywordRows);
                    session.setAttribute("keywordStatsMap", keywordStatsMap);

                    if (keywordStatsMap != null) {
                        Iterator<Map.Entry<Long, List<KeywordRankCheckerStats>>> statsMap = keywordStatsMap.entrySet().iterator();
                        while (statsMap.hasNext()) {
                            Map.Entry<Long, List<KeywordRankCheckerStats>> nextkv = statsMap.next();
                            if (nextkv.getKey() != null) {
                                List<KeywordRankCheckerStats> domainInfo = nextkv.getValue();
                                for (KeywordRankCheckerStats kw : domainInfo) {
                                    logger.debug("Keyword Rank Checker sorted domain id:" + nextkv.getKey() + "" + kw.getKeywordId() + " " + kw.getKeywordRankInG() + " " + kw.getKeywordRankInB() + " g progress" + kw.getProgressInG() + "b progress " + kw.getProgressInB());
                                }
                            }
                        }
                    }
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                if (userLoggedIn) {
                    mAndV.addObject("showSettings", "success");
                } else {
                    mAndV.addObject("showSettings", "fail");
                }
                mAndV.addObject("showSuccess", "success");
                mAndV.addObject("googleGraph", "true");

                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                mAndV.addObject("geoLocation", geoLocation);
                HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                mAndV.addObject("loadLanguages", loadLanguages);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                if (session.getAttribute("keywordRows") != null) {
                    session.removeAttribute("keywordRows");
                }
                session.setAttribute("keywordRows", keywordRows);
                List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                if (session.getAttribute("compDomainRows") != null) {
                    session.removeAttribute("compDomainRows");
                }
                session.setAttribute("compDomainRows", compDomainRows);
                List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                if (session.getAttribute("compDomainSettingRows") != null) {
                    session.removeAttribute("compDomainSettingRows");
                }
                session.setAttribute("compDomainSettingRows", compDomainSettingRows);

                //for registered users
                List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                session.setAttribute("keywordRankChecker", keywordRankChecker);
                KeywordRankChecker kwd = keywordRankCheckerService.getUserSettingsData(userIdInRankUser);
                keywordRankChecker.setAlertStatus(kwd.isAlertStatus());
                keywordRankChecker.setWeeklyReport(kwd.isWeeklyReport());
                keywordRankChecker.setToolUnsubscription(kwd.isToolUnsubscription());
                Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = keywordRankCheckerService
                        .createSortedMap(userLoggedIn, user.getId(), keywordRankChecker, compDomainRows, session);
                List<KeywordSettings> sortedKeywordRows = keywordRankCheckerService.createSortedKeywordSettingsRows(keywordRows, session);
                if (session.getAttribute("sortedKeywordRows") != null) {
                    session.removeAttribute("sortedKeywordRows");
                }
                session.setAttribute("sortedKeywordRows", sortedKeywordRows);
                session.setAttribute("keywordStatsMap", keywordStatsMap);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                if (userLoggedIn) {
                    mAndV.addObject("showSettings", "success");
                    mAndV.addObject("showSuccess", "success");
                    mAndV.addObject("googleGraph", "true");
                } else {
                    mAndV.addObject("showSettings", "fail");
                    mAndV.addObject("showSuccess", "fail");
                    mAndV.addObject("googleGraph", "false");
                }

                return mAndV;

            } else if (WebUtils.hasSubmitParameter(request, "rankCheckerATE")) {
                /*For Request of rankCheckerATE.*/
                String keywordDomain = request.getParameter("keywordDomain");
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                String toolIssues = "";
                String questionInfo = "";
                boolean inValidRankUrl = false;
                if (session.getAttribute("rankCheckerURlATE") != null) {
                    inValidRankUrl = (boolean) session.getAttribute("rankCheckerURlATE");
                }
                Long guestRankUserID = 0l;
                if (session.getAttribute("GuestRankUserId") != null) {
                    guestRankUserID = (Long) session.getAttribute("GuestRankUserId");
                }

                /*Analysis Name: Audit competitor websites and provide consultation on improving SEO.*/
                if (!inValidRankUrl) {
                    if ((userIdInRankUser != 0) || (guestRankUserID != 0l)) {
                        questionInfo = "Need help in fixing following issues with my website '" + keywordDomain + "'.\n\n";
                        toolIssues = "<ul>";
                        questionInfo += "- Auditing competitor webpages for relevent keywords.\n";
                        toolIssues += "<li>" + "Do you want a full audit of how your competitors are targeting your keywords?" + "</li>";
                        toolIssues += "</ul>";
                    }
                }
                lxrmAskExpert.setDomain(keywordDomain);
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(toolObj.getToolName());
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null && isToolInputChange) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), keywordDomain, toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }

                return null;
            } else if (WebUtils.hasSubmitParameter(request, "compDomId")) {
                long compDomId = Long.parseLong(request.getParameter("compDomId"));
                Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = (Map<Long, List<KeywordRankCheckerStats>>) session.getAttribute("keywordStatsMap");
                keywordRankCheckerService.getCompDomainResult(response, compDomId, keywordStatsMap);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                mAndV.addObject("googleGraph", "true");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "delete")) {  //to delete keyword  in settings page
                long keyId = Long.parseLong(WebUtils.findParameterValue(request, "delete"));
                logger.debug("In delete keyword id val :" + keyId);
                keywordRankCheckerService.updateKeywordStatus(keyId);
                List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                if (session.getAttribute("keywordRows") != null) {
                    session.removeAttribute("keywordRows");
                }
                session.setAttribute("keywordRows", keywordRows);
                List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                if (session.getAttribute("keywordSettingsRows") != null) {
                    session.removeAttribute("keywordSettingsRows");
                }
                session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                mAndV.addObject("Setings", "Setings");
                mAndV.addObject("googleGraph", "true");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "dateRange")) {
                String startDate = request.getParameter("stDate");
                String endDate = request.getParameter("endDate");
                long keywordId = Long.parseLong(request.getParameter("keywordId"));
                logger.info("In Date Range2 The keyword Id" + keywordId + "startDate" + startDate + "endDate" + endDate + "user id" + userIdInRankUser);
                List<KeywordSettings> sortedKeywordRows = (List<KeywordSettings>) session.getAttribute("sortedKeywordRows");
                List<CompDomainSettings> compDomainRows = (List<CompDomainSettings>) session.getAttribute("compDomainRows");

                try {
                    Map<Long, JSONArray> allKeywordChartData = keywordRankCheckerService.fetchKeywordsChartData(compDomainRows, sortedKeywordRows, userIdInRankUser, startDate, endDate);

                    if (allKeywordChartData != null) {
                        session.setAttribute("allKeywordChartData", allKeywordChartData);
                        JSONArray keywordChartData = allKeywordChartData.get(keywordId);
                    }

                    if (allKeywordChartData != null) {
                        JSONArray keywordChartData = allKeywordChartData.get(keywordId);
                        if (keywordChartData != null && keywordChartData.size() > 0) {
                            keywordRankCheckerService.jsonKeywordChartData(keywordChartData, response);
                        } else {
                            keywordRankCheckerService.jsonKeywordChartData(keywordChartData, response);
                        }
                    }
                } catch (Exception e) {
                    keywordRankCheckerService.jsonKeywordChartData(new JSONArray(), response);
                    logger.error(e.getMessage());
                }
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "keywordId")) {
                long keywordId = Long.parseLong(request.getParameter("keywordId"));
                Map<Long, JSONArray> allKeywordChartData = (Map<Long, JSONArray>) session.getAttribute("allKeywordChartData");
                if (allKeywordChartData != null) {
                    JSONArray keywordChartData = allKeywordChartData.get(keywordId);
                    if (keywordChartData != null && keywordChartData.size() > 0) {
                        keywordRankCheckerService.jsonKeywordChartData(keywordChartData, response);
                    } else {
                        keywordRankCheckerService.jsonKeywordChartData(keywordChartData, response);
                    }
                }
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "deleteCompDomain")) { //to delete competitor domain  in settings page
                long domainId = Long.parseLong(WebUtils.findParameterValue(request, "deleteCompDomain"));
                keywordRankCheckerService.updateCompDomainStatus(domainId);
                List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                if (session.getAttribute("compDomainRows") != null) {
                    session.removeAttribute("compDomainRows");
                }
                session.setAttribute("compDomainRows", compDomainRows);
                List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                if (session.getAttribute("compDomainSettingRows") != null) {
                    session.removeAttribute("compDomainSettingRows");
                }
                session.setAttribute("compDomainSettingRows", compDomainSettingRows);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                mAndV.addObject("Setings", "Setings");
                mAndV.addObject("googleGraph", "true");
                return mAndV;
            } else if (userLoggedIn && WebUtils.hasSubmitParameter(request, "saveSettingsData")) {
                Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                Map<Long, List<KeywordRankCheckerStats>> resultMap = new HashMap<>();
                List<CompDomainSettings> prvCompDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                List<KeywordSettings> prvKeywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                if (!keywordRankChecker.isToolUnsubscription()) {
                    List<CompDomainSettings> domains = keywordRankChecker.getCompDomainSettingRows();
//                                                    domains.set(0, prvCompDomainRows.get(0));
                    for (CompDomainSettings validateDomains : domains) {
                        String newCompUrl = "";
                        if (!validateDomains.getDomain().equals("") && validateDomains.isUserDomain() == false) {
                            newCompUrl = Common.getUrlValidation(validateDomains.getDomain().trim(), session);
                        }
                        if (newCompUrl.equals("invalid")) {
                            long invalidDomainId = validateDomains.getDomainId();
                            String notWorkingUrl = validateDomains.getDomain().trim() + " " + messageSource.getMessage("MultipleURL.error", null, Locale.US);
                            ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                            mAndV.addObject("compNotWorkingUrl", notWorkingUrl);
                            mAndV.addObject("invalidUrl", validateDomains.getDomain().trim());
                            mAndV.addObject("showSettings", "success");
                            mAndV.addObject("showSuccess", "false");
                            mAndV.addObject("googleGraph", "false");
                            mAndV.addObject("invalidDomainId", invalidDomainId);
                            return mAndV;

                        }
                    }
                    List<CompDomainSettings> newModifiedCompDomains = keywordRankCheckerService.compareCompDomainsData(userIdInRankUser, keywordRankChecker);
                    keywordRankCheckerService.compareKeywordsData(userIdInRankUser, keywordRankChecker);
                    keywordRankCheckerService.compareUserSettingsData(userIdInRankUser, keywordRankChecker, startTime);
                    List<KeywordSettings> modifiedKeywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                    List<CompDomainSettings> modifiedCompDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                    List<CompDomainSettings> newCompRows = new ArrayList<>();
                    List<KeywordSettings> newKeywordRows = new ArrayList<>();
                    List<KeywordSettings> oldKeywordRows = new ArrayList<>();
                    List<String> prvDomains = new ArrayList<>();
                    List<String> newDomains = new ArrayList<>();
                    List<String> totalModifiedDomains = new ArrayList<>();

                    if (prvCompDomainRows != null && prvCompDomainRows.size() > 0) {
                        prvCompDomainRows.stream().forEach((prvCmpD) -> {
                            prvDomains.add(prvCmpD.getDomain());
                        });
                    }

                    if (newModifiedCompDomains != null && newModifiedCompDomains.size() > 0) {
                        for (CompDomainSettings newMod : newModifiedCompDomains) {
                            newDomains.add(newMod.getDomain().trim());
                        }
                    }

                    if (modifiedCompDomainRows != null && modifiedCompDomainRows.size() > 0) {
                        modifiedCompDomainRows.stream().forEach((newCompRow) -> {
                            totalModifiedDomains.add(newCompRow.getDomain());
                        });
                        if (prvCompDomainRows != null && prvCompDomainRows.size() > 0) {
                            for (CompDomainSettings newCompRow : modifiedCompDomainRows) {
                                boolean found = false;
                                for (CompDomainSettings prvCompRow : prvCompDomainRows) {
                                    if (newCompRow.getDomainId() == prvCompRow.getDomainId()) {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found) {
                                    newCompRows.add(newCompRow);
                                }
                            }
                        }
                    }

                    if (modifiedKeywordRows != null && modifiedKeywordRows.size() > 0) {
                        if (prvKeywordRows != null && prvKeywordRows.size() > 0) {
                            for (KeywordSettings newKeywordRow : modifiedKeywordRows) {
                                boolean found = false;
                                for (KeywordSettings prvKeywordRow : prvKeywordRows) {
                                    if (newKeywordRow.getKeywordId() == prvKeywordRow.getKeywordId()) {
                                        found = true;
                                        oldKeywordRows.add(prvKeywordRow);
                                        break;
                                    }
                                }
                                if (!found) {
                                    newKeywordRows.add(newKeywordRow);
                                }
                            }
                        } else {
                            newKeywordRows = modifiedKeywordRows;
                        }
                    }

//                        if the request is coming from tool page then only add the user inputs to JSONObject
//                        if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                        session.removeAttribute("requestFromMail");
                    }
                    if (!requestFromMail && !user.isAdmin()) {
                        isToolInputChange = true;
                        userInputJson = new JSONObject();
                        userInputJson.put("2", keywordRankChecker.isAlertStatus());
                        userInputJson.put("3", keywordRankChecker.isWeeklyReport());
                        userInputJson.put("4", keywordRankChecker.isToolUnsubscription());
                        int i = 5;
                        List<CompDomainSettings> compDomainSettingsList = null;
                        if (keywordRankChecker.getCompDomainSettingRows() != null) {
                            compDomainSettingsList = keywordRankChecker.getCompDomainSettingRows();
                        }
                        for (CompDomainSettings compDomainSettings : compDomainSettingsList) {
                            userInputJson.put("cName" + i + "", compDomainSettings.getDomain());
                            userInputJson.put("cDomainId" + i + "", compDomainSettings.getDomainId());
                            userInputJson.put("cIsUserDomain" + i + "", compDomainSettings.isUserDomain());
                            i++;
                        }
                        List<KeywordSettings> keywordSettingsList = null;
                        if (keywordRankChecker.getKeywordSettingRows() != null) {
                            keywordSettingsList = keywordRankChecker.getKeywordSettingRows();
                        }
                        for (KeywordSettings keywordSettings : keywordSettingsList) {
                            userInputJson.put("kName" + i + "", keywordSettings.getKeyword());
                            userInputJson.put("kId" + i + "", keywordSettings.getKeywordId());
                            userInputJson.put("kOperator" + i + "", keywordSettings.getAlertOperator());
                            userInputJson.put("kValue" + i + "", keywordSettings.getAlertValue());
                            userInputJson.put("kIsGoogle" + i + "", keywordSettings.isGoogleCheckbox());
                            userInputJson.put("kIsBing" + i + "", keywordSettings.isBingCheckbox());
                            i++;
                        }
                    }

                    KeywordRankChecker tempKeywordRankChecker = keywordRankCheckerService.getUserSettingData(user.getId());
                    if (tempKeywordRankChecker != null) {
                        String market = Common.loadcustomSearchLanguages().get(tempKeywordRankChecker.getLanguage()) + "-" + Common.loadGeoLocations().get(tempKeywordRankChecker.getLocation());
                        String bingMarketVal = Common.loadFinalBingMarkets().get(market);
                        if (userLoggedIn) {
                            if ((newCompRows.size() > 0) && (newKeywordRows.size() > 0)) {
                                resultMap = keywordRankCheckerService.getResults(oldKeywordRows, newCompRows, newDomains, tempKeywordRankChecker, bingMarketVal);
                                resultMap = keywordRankCheckerService.getResults(newKeywordRows, modifiedCompDomainRows, totalModifiedDomains, tempKeywordRankChecker, bingMarketVal);
                            } else if (newCompRows.size() > 0) {
                                resultMap = keywordRankCheckerService.getResults(prvKeywordRows, newCompRows, newDomains, tempKeywordRankChecker, bingMarketVal);
                            } else if (newKeywordRows.size() > 0) {
                                resultMap = keywordRankCheckerService.getResults(newKeywordRows, prvCompDomainRows, prvDomains, tempKeywordRankChecker, bingMarketVal);
                            }
                        }
                    }

                    if (session.getAttribute("compDomainRows") != null) {
                        session.removeAttribute("compDomainRows");
                    }

                    session.setAttribute("compDomainRows", modifiedCompDomainRows);
                    Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = keywordRankCheckerService.createSortedMap(userLoggedIn, user.getId(), keywordRankChecker, modifiedCompDomainRows, session);
                    List<KeywordSettings> sortedKeywordRows = keywordRankCheckerService.createSortedKeywordSettingsRows(modifiedKeywordRows, session);
                    if (session.getAttribute("sortedKeywordRows") != null) {
                        session.removeAttribute("sortedKeywordRows");
                    }
                    session.setAttribute("sortedKeywordRows", sortedKeywordRows);
                    if (session.getAttribute("keywordStatsMap") != null) {
                        session.removeAttribute("keywordStatsMap");
                    }
                    session.setAttribute("keywordStatsMap", keywordStatsMap);
                    List<CompDomainSettings> compDomainSettingRows = null;
                    if (requestFromMail) {
                        newModifiedCompDomains = keywordRankChecker.getCompDomainSettingRows();
                        compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(newModifiedCompDomains, keywordRankChecker);
                    } else {
                        compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(modifiedCompDomainRows, keywordRankChecker);
                    }
                    keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                    if (session.getAttribute("compDomainSettingRows") != null) {
                        session.removeAttribute("compDomainSettingRows");
                    }
                    session.setAttribute("compDomainSettingRows", compDomainSettingRows);
                    List<KeywordSettings> keywordSettingsRows = null;
                    if (requestFromMail) {
                        newKeywordRows = keywordRankChecker.getKeywordSettingRows();
                        keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(newKeywordRows, keywordRankChecker);
                    } else {
                        keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(modifiedKeywordRows, keywordRankChecker);
                    }

                    keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                    session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                    session.setAttribute("keywordRankChecker", keywordRankChecker);
                    KeywordRankChecker keywordRankCheckerSettings = keywordRankCheckerService.getUserSettingData(userSession.getUserId());
                    session.setAttribute("userSettingData", keywordRankCheckerSettings);
                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                    if (userLoggedIn) {
                        mAndV.addObject("showSettings", "success");
                    } else {
                        mAndV.addObject("showSettings", "fail");
                    }
                    mAndV.addObject("showSuccess", "success");
                    mAndV.addObject("googleGraph", "true");
                    return mAndV;
                } else if (userIdInRankUser > 0) {
                    boolean toolUnsubscriptionChanged = keywordRankCheckerService.compareUserSettingsData(userIdInRankUser, keywordRankChecker, startTime);
                    KeywordRankChecker keywordRankCheckerSettings = keywordRankCheckerService.getUserSettingData(userSession.getUserId());
                    session.setAttribute("userSettingData", keywordRankCheckerSettings);
                    List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                    session.setAttribute("compDomainRows", compDomainRows);
                    List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                    session.setAttribute("keywordRows", keywordRows);
                    List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                    keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                    session.setAttribute("compDomainSettingRows", compDomainSettingRows);
                    List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                    keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                    session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                    session.setAttribute("keywordRankChecker", keywordRankChecker);
                    ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                    if (toolUnsubscriptionChanged) {
                        mAndV.addObject("sucMsg", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsubscribe option.");
                    }
                    if (userLoggedIn) {
                        mAndV.addObject("showSettings", "success");
                    } else {
                        mAndV.addObject("showSettings", "fail");
                    }
                    mAndV.addObject("showSuccess", "success");
                    mAndV.addObject("googleGraph", "true");
                    return mAndV;
                }

            } else if (userLoggedIn && WebUtils.hasSubmitParameter(request, "cancelSettingsData")) {
                logger.info("checking Key Rank Checker data for logged in user : " + user.getId() + "user id in rank user table " + userIdInRankUser);
                List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                if (session.getAttribute("keywordRows") != null) {
                    session.removeAttribute("keywordRows");
                }
                session.setAttribute("keywordRows", keywordRows);
                List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                if (session.getAttribute("compDomainRows") != null) {
                    session.removeAttribute("compDomainRows");
                }
                session.setAttribute("compDomainRows", compDomainRows);
                List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                if (session.getAttribute("compDomainSettingRows") != null) {
                    session.removeAttribute("compDomainSettingRows");
                }
                session.setAttribute("compDomainSettingRows", compDomainSettingRows);

                //for registered users
                List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                session.setAttribute("keywordRankChecker", keywordRankChecker);
                KeywordRankChecker kwd = keywordRankCheckerService.getUserSettingsData(userIdInRankUser);
                keywordRankChecker.setAlertStatus(kwd.isAlertStatus());
                keywordRankChecker.setWeeklyReport(kwd.isWeeklyReport());
                keywordRankChecker.setToolUnsubscription(kwd.isToolUnsubscription());
                Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = keywordRankCheckerService
                        .createSortedMap(userLoggedIn, user.getId(), keywordRankChecker, compDomainRows, session);
                List<KeywordSettings> sortedKeywordRows = keywordRankCheckerService.createSortedKeywordSettingsRows(keywordRows, session);
                if (session.getAttribute("sortedKeywordRows") != null) {
                    session.removeAttribute("sortedKeywordRows");
                }
                session.setAttribute("sortedKeywordRows", sortedKeywordRows);
                session.setAttribute("keywordStatsMap", keywordStatsMap);
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                if (userLoggedIn) {
                    mAndV.addObject("showSettings", "success");
                } else {
                    mAndV.addObject("showSettings", "fail");
                }
                mAndV.addObject("showSuccess", "success");
                mAndV.addObject("googleGraph", "true");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                removeSession(session);
                KeywordRankChecker keywordRankCheckerTool = new KeywordRankChecker();
                keywordRankCheckerTool.setLanguage("lang_en");
                keywordRankCheckerTool.setLocation("us");
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                mAndV.addObject("showSettings", "fail");
                mAndV.addObject("showSuccess", "fail");
                mAndV.addObject("googleGraph", "false");
                mAndV.addObject("clear", "show");
                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                mAndV.addObject("geoLocation", geoLocation);
                HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                mAndV.addObject("loadLanguages", loadLanguages);
                session.removeAttribute("rankCheckerURlATE");
                session.removeAttribute("GuestRankUserId");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "back")) {
                Long idInRankUser = keywordRankCheckerService.fetchRankUserIdFromRankUserGuest(userSession.getUserId());
                if (idInRankUser != null && idInRankUser > 0) {
                    List<CompDomainSettings> prvCompDomainRows = keywordRankCheckerService.createCompDomainRows(idInRankUser);
                    List<KeywordSettings> prvKeywordRows = keywordRankCheckerService.createKeywordRows(idInRankUser);
                    List<String> cmpDomains = new ArrayList<>();
                    List<String> keywords = new ArrayList<>();
                    for (CompDomainSettings cmpDomain : prvCompDomainRows) {
                        if (cmpDomain.isUserDomain()) {
                            keywordRankChecker.setUserdomain(cmpDomain.getDomain());
                        } else {
                            cmpDomains.add(cmpDomain.getDomain());
                        }
                    }
                    keywordRankChecker.setCompDomains(Common.convertListToString(cmpDomains));
                    for (KeywordSettings keywordrow : prvKeywordRows) {
                        keywords.add(keywordrow.getKeyword());
                    }
                    keywordRankChecker.setKeywords(Common.convertListToString(keywords));
                }
                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                mAndV.addObject("geoLocation", geoLocation);
                HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                mAndV.addObject("loadLanguages", loadLanguages);
                return mAndV;
            } else if (userLoggedIn && (userIdInRankUser > 0 || (toolUsageInfo != null && toolUsageInfo.get(10l) != null && userIdInRankUser > 0)) && request.getParameter("question-id") == null) {
                if (userIdInRankUser == 0) { // guest user used the tool and logged in after that
                    Long guestRankUserId = (Long) session.getAttribute("GuestRankUserId");
                    if (guestRankUserId != null) {
                        keywordRankCheckerService.updateGuestEntryToLoggedIn(guestRankUserId, user.getId());
                        session.removeAttribute("GuestRankUserId");
                        userIdInRankUser = guestRankUserId;
                    } else {
                        logger.info("No Guest user rank id found in login refresh.");
                    }
                } else if (userIdInRankUser > 0 && toolUsageInfo != null && toolUsageInfo.get(10l) != null) {
                    Long guestRankUserId = (Long) session.getAttribute("GuestRankUserId");

                    if (guestRankUserId != null) {
                        keywordRankCheckerService.deleteGuestUserDataByRankUserId(guestRankUserId);
                    } else {
                        logger.info("No Guest user rank id found in login refresh.");
                    }
                }
                logger.info("checking data for logged in user : " + user.getId() + ", user id in rank user table " + userIdInRankUser);
                List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(
                        userIdInRankUser);
                session.setAttribute("compDomainRows", compDomainRows);
                List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                session.setAttribute("keywordRows", keywordRows);

                //for registered users
                List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                session.setAttribute("compDomainSettingRows", compDomainSettingRows);
                List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                session.setAttribute("keywordRankChecker", keywordRankChecker);

                Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap = keywordRankCheckerService
                        .createSortedMap(userLoggedIn, user.getId(), keywordRankChecker, compDomainRows, session);
                session.setAttribute("keywordStatsMap", keywordStatsMap);
                List<KeywordSettings> sortedKeywordRows = keywordRankCheckerService.createSortedKeywordSettingsRows(keywordRows, session);
                session.setAttribute("sortedKeywordRows", sortedKeywordRows);
                userSession.addToolUsage(toolObj.getToolId());

//                if the request is coming from tool page then only add the user inputs to JSONObject
//                if the request is coming from mail then do not add the user inputs to JSONObject
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    userInputJson.put("2", keywordRankChecker.isAlertStatus());
                    userInputJson.put("3", keywordRankChecker.isWeeklyReport());
                    userInputJson.put("4", keywordRankChecker.isToolUnsubscription());
                    int i = 5;
                    List<CompDomainSettings> compDomainSettingsList = null;
                    if (keywordRankChecker.getCompDomainSettingRows() != null) {
                        compDomainSettingsList = keywordRankChecker.getCompDomainSettingRows();
                    }
                    for (CompDomainSettings compDomainSettings : compDomainSettingsList) {
                        userInputJson.put("cName" + i + "", compDomainSettings.getDomain());
                        userInputJson.put("cDomainId" + i + "", compDomainSettings.getDomainId());
                        userInputJson.put("cIsUserDomain" + i + "", compDomainSettings.isUserDomain());
                        i++;
                    }
                    List<KeywordSettings> keywordSettingsList = null;
                    if (keywordRankChecker.getKeywordSettingRows() != null) {
                        keywordSettingsList = keywordRankChecker.getKeywordSettingRows();
                    }
                    for (KeywordSettings keywordSettings : keywordSettingsList) {
                        userInputJson.put("kName" + i + "", keywordSettings.getKeyword());
                        userInputJson.put("kId" + i + "", keywordSettings.getKeywordId());
                        userInputJson.put("kOperator" + i + "", keywordSettings.getAlertOperator());
                        userInputJson.put("kValue" + i + "", keywordSettings.getAlertValue());
                        userInputJson.put("kIsGoogle" + i + "", keywordSettings.isGoogleCheckbox());
                        userInputJson.put("kIsBing" + i + "", keywordSettings.isBingCheckbox());
                        i++;
                    }
                }

                ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
                if (userLoggedIn) {
                    mAndV.addObject("showSettings", "success");
                } else {
                    mAndV.addObject("showSettings", "fail");
                }
                if (keywordRankChecker.isToolUnsubscription() == true) {
                    mAndV.addObject("sucMsg", "You have unsubscribed from the tool.To re-subscribe please uncheck the unsubscribe option.");
                }
                mAndV.addObject("showSuccess", "success");
                mAndV.addObject("googleGraph", "true");
                HashMap<String, String> geoLocation = Common.loadGeoLocations();
                mAndV.addObject("geoLocation", geoLocation);
                HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
                mAndV.addObject("loadLanguages", loadLanguages);
                return mAndV;
            }
        }
        removeSession(session);
        keywordRankChecker = new KeywordRankChecker();
        keywordRankChecker.setLanguage("lang_en");
        keywordRankChecker.setLocation("us");
        ModelAndView mAndV = new ModelAndView(getFormView(), "keywordRankCheckerTool", keywordRankChecker);
        mAndV.addObject("showSettings", "fail");
        mAndV.addObject("showSuccess", "fail");
        HashMap<String, String> geoLocation = Common.loadGeoLocations();
        mAndV.addObject("geoLocation", geoLocation);
        HashMap<String, String> loadLanguages = Common.loadcustomSearchLanguages();
        mAndV.addObject("loadLanguages", loadLanguages);
        mAndV.addObject("googleGraph", "false");
        return mAndV;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Long userIdInRankUser = 0l;
        HttpSession session = request.getSession();
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        Session userSession = (Session) session.getAttribute("userSession");
        KeywordRankChecker keywordRankChecker = (KeywordRankChecker) session.getAttribute("keywordRankChecker");

        if (userLoggedIn && userSession.getUserId() > 0) {
            if (userSession.getUserId() > 0) {
                userIdInRankUser = keywordRankCheckerService.fetchRankUserIdFromRankUserLoggedIn(userSession.getUserId());
            }
        }
        if (userIdInRankUser != null && userIdInRankUser > 0) {
            KeywordRankChecker keywordRankCheckerSettings = keywordRankCheckerService.getUserSettingData(userSession.getUserId());
            session.setAttribute("userSettingData", keywordRankCheckerSettings);
        }
        if (keywordRankChecker == null) {
            keywordRankChecker = new KeywordRankChecker();
            keywordRankChecker.setLanguage("lang_en");
            keywordRankChecker.setLocation("us");
            if (userIdInRankUser != null && userIdInRankUser > 0) {
                List<KeywordSettings> keywordRows = keywordRankCheckerService.createKeywordRows(userIdInRankUser);
                session.setAttribute("keywordRows", keywordRows);
                List<KeywordSettings> keywordSettingsRows = keywordRankCheckerService.createKeywordSettingsRows(keywordRows, keywordRankChecker);
                keywordRankChecker.setKeywordSettingsRows(keywordSettingsRows);
                session.setAttribute("keywordSettingsRows", keywordSettingsRows);
                List<CompDomainSettings> compDomainRows = keywordRankCheckerService.createCompDomainRows(userIdInRankUser);
                session.setAttribute("compDomainRows", compDomainRows);
                List<CompDomainSettings> compDomainSettingRows = keywordRankCheckerService.createCompDomainSettingRows(compDomainRows, keywordRankChecker);
                keywordRankChecker.setCompDomainSettingRows(compDomainSettingRows);
                session.setAttribute("compDomainSettingRows", compDomainSettingRows);
                keywordRankChecker = keywordRankCheckerService.getUserSettingData(userSession.getUserId());
            }
        }
        return keywordRankChecker;
    }

    public void removeSession(HttpSession session) {
        session.removeAttribute("compDomainSettingRows");
        session.removeAttribute("keywordStatsMap");
        session.removeAttribute("keywordRows");
        session.removeAttribute("keywordSettingsRows");
        session.removeAttribute("sortedKeywordSettingsRows");
        session.removeAttribute("statsChartMap");
        session.removeAttribute("compDomainRows");
        session.removeAttribute("keywordRankChecker");
    }
}
