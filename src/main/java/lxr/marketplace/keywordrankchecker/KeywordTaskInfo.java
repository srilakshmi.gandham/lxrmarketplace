package lxr.marketplace.keywordrankchecker;

public class KeywordTaskInfo {
	
	private String keyword;
	private int googleRank;
	private int bingRank;
    private String serachEngine;
    
    
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getGoogleRank() {
		return googleRank;
	}
	public void setGoogleRank(int googleRank) {
		this.googleRank = googleRank;
	}
	public int getBingRank() {
		return bingRank;
	}
	public void setBingRank(int bingRank) {
		this.bingRank = bingRank;
	}
	public String getSerachEngine() {
		return serachEngine;
	}
	public void setSerachEngine(String serachEngine) {
		this.serachEngine = serachEngine;
	}
    
    
}
