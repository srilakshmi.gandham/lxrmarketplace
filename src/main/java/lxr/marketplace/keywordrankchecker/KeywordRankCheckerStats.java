package lxr.marketplace.keywordrankchecker;

import java.io.Serializable;

public class KeywordRankCheckerStats implements Comparable<KeywordRankCheckerStats>,Serializable{

	long keywordId;

	int keywordRankInG;
	int keywordRankInB;
	int progressInG;
	int progressInB;
	

	public long getKeywordId() {
		return keywordId;
	}
	public int getProgressInG() {
		return progressInG;
	}
	public void setProgressInG(int progressInG) {
		this.progressInG = progressInG;
	}
	public int getProgressInB() {
		return progressInB;
	}
	public void setProgressInB(int progressInB) {
		this.progressInB = progressInB;
	}
	public void setKeywordId(long keywordId) {
		this.keywordId = keywordId;
	}
    public int getKeywordRankInG() {
		return keywordRankInG;
	}
	public void setKeywordRankInG(int keywordRankInG) {
		this.keywordRankInG = keywordRankInG;
	}
	public int getKeywordRankInB() {
		return keywordRankInB;
	}
	public void setKeywordRankInB(int keywordRankInB) {
		this.keywordRankInB = keywordRankInB;
	}
//	@Override
    public int compareTo(KeywordRankCheckerStats kwStats) {
	  if(keywordRankInG == kwStats.getKeywordRankInG()){
		  if(keywordRankInB == -1 && kwStats.getKeywordRankInB() == -1){
			  return 0;
		  }else if(keywordRankInB == -1){
			  return 101 - kwStats.getKeywordRankInB();
		  }else if(kwStats.getKeywordRankInB() == -1){
			  return keywordRankInB - 101;
		  }else{
			  return keywordRankInB - kwStats.getKeywordRankInB();
		  }
	  }else if(keywordRankInG == -1){
		  return 101 - kwStats.getKeywordRankInG();
	  }else if(kwStats.getKeywordRankInG() == -1){		  
		  return keywordRankInG - 101;
	  }else{
		  return keywordRankInG - kwStats.getKeywordRankInG();
	  }
	}
}
