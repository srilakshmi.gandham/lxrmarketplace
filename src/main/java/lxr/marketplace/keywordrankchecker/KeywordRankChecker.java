
package lxr.marketplace.keywordrankchecker;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class KeywordRankChecker {
    
	private List keywordSettingRows = LazyList.decorate(new ArrayList(),FactoryUtils.instantiateFactory(KeywordSettings.class));
	private List compDomainSettingRows = LazyList.decorate(new ArrayList(),FactoryUtils.instantiateFactory(CompDomainSettings.class));
	private String keywords;
	private String userdomain;
	private String compDomains;
	private long keyCount;
	private long domainCount;
	private boolean alertStatus;
	private boolean weeklyReport = true;
	private int alertOperator;
	private int alertValue;
	private String location;
	private Timestamp startDate;
	private String language;
	private boolean toolUnsubscription;
	
	public boolean isToolUnsubscription() {
		return toolUnsubscription;
	}
	public void setToolUnsubscription(boolean toolUnsubscription) {
		this.toolUnsubscription = toolUnsubscription;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isAlertStatus() {
		return alertStatus;
	}
	public void setAlertStatus(boolean alertStatus) {
		this.alertStatus = alertStatus;
	}
	public boolean isWeeklyReport() {
		return weeklyReport;
	}
	public void setWeeklyReport(boolean weeklyReport) {
		this.weeklyReport = weeklyReport;
	}
	
	public int getAlertOperator() {
		return alertOperator;
	}
	public void setAlertOperator(int alertOperator) {
		this.alertOperator = alertOperator;
	}
	public int getAlertValue() {
		return alertValue;
	}
	public void setAlertValue(int alertValue) {
		this.alertValue = alertValue;
	}
	public long getKeyCount() {
		return keyCount;
	}
	public void setKeyCount(long keyCount) {
		this.keyCount = keyCount;
	}
	public long getDomainCount() {
		return domainCount;
	}
	public void setDomainCount(long domainCount) {
		this.domainCount = domainCount;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getUserdomain() {
		return userdomain;
	}
	public void setUserdomain(String userdomain) {
		this.userdomain = userdomain;
	}
    public String getCompDomains() {
		return compDomains;
	}
	public void setCompDomains(String compDomains) {
		this.compDomains = compDomains;
	}
	public List getKeywordSettingRows() {
		return keywordSettingRows;
	}
	public void setKeywordSettingsRows(List keywordSettingRows) {
		this.keywordSettingRows = keywordSettingRows;
	}
	public List getCompDomainSettingRows() {
		return compDomainSettingRows;
	}
	public void setCompDomainSettingRows(List compDomainSettingRows) {
		this.compDomainSettingRows = compDomainSettingRows;
	}
    public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	

}
