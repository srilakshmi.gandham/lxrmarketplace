package lxr.marketplace.keywordrankchecker;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import javax.servlet.http.HttpServletRequest;

public class SeoRankCheckerInfoTask implements Runnable {

    private static final Logger logger = Logger.getLogger(SeoRankCheckerInfoTask.class);

    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.ctx = ctx;
    }

    private final long userId;
    private final long rankUserId;
    private final String email;
    private final boolean alertStatus;
    private final boolean weeklyReport;
    private final String geogrphicLoc;
    private final String language;
    private final Timestamp startDateInDb;
    private final String userName;
    HttpServletRequest request;

    public SeoRankCheckerInfoTask(long userId, long rankUserId, String email, String geogrphicLoc, String language, boolean weeklyReport, boolean alertStatus, Timestamp startDateInDb, String userName) {
        this.userId = userId;
        this.rankUserId = rankUserId;
        this.geogrphicLoc = geogrphicLoc;
        this.weeklyReport = weeklyReport;
        this.alertStatus = alertStatus;
        this.email = email;
        this.startDateInDb = startDateInDb;
        this.language = language;
        this.userName = userName;
    }

    @Override
    public void run() {

        try {
            logger.info("**********Fetching keyword data started for user **************" + email + "  " + userId);

            KeywordRankCheckerService krcService = (KeywordRankCheckerService) ctx.getBean("keywordRankCheckerService");
            String downloadFolder = (String) ctx.getBean("downloadFolder");

            List<CompDomainSettings> compDomainSettingRows = krcService.createCompDomainRows(rankUserId);
            List<KeywordSettings> keywordSettingRows = null;
            List<String> domains = new ArrayList<>();
            for (CompDomainSettings cmpDomain : compDomainSettingRows) {
                keywordSettingRows = krcService.createKeywordRowsforScheduler(rankUserId, cmpDomain.getDomainId());
                if (keywordSettingRows.size() > 0) {
                    domains.add(cmpDomain.getDomain());
                }

            }
            KeywordRankChecker keywordRankChecker = krcService.getUserSettingData(userId);
            String market = Common.loadcustomSearchLanguages().get(language) + "-" + Common.loadGeoLocations().get(geogrphicLoc);
            String bingMarketVal = Common.loadFinalBingMarkets().get(market);
            Map<Long, List<KeywordRankCheckerStats>> keywordStats = krcService.getResults(keywordSettingRows, compDomainSettingRows, domains, keywordRankChecker, bingMarketVal);

            Timestamp curdate = new Timestamp(Calendar.getInstance().getTimeInMillis());
            SimpleDateFormat forMailsDt = new SimpleDateFormat("MMM d, yyyy");

            if (alertStatus) {
                String userDomain = "";
                List<KeywordRankCheckerStats> userDomainKeywordList = null;
                for (CompDomainSettings cmpSetting : compDomainSettingRows) {
                    if (cmpSetting.isUserDomain()) {
                        userDomain = cmpSetting.getDomain();
                        userDomainKeywordList = keywordStats.get(cmpSetting.getDomainId());
                    }
                }
                if (userDomainKeywordList != null) {
                    List<KeywordTaskInfo> alertValueReachedKeywords = new ArrayList<>();
                    for (KeywordRankCheckerStats userDomainKeyword : userDomainKeywordList) {
                        if (keywordSettingRows != null) {
                            for (KeywordSettings keySetting : keywordSettingRows) {
                                boolean matched = false;
                                String searchEngine = null;
                                int keywordRankInG, keywordRankInB;
                                int alertVal = 0;
                                if (userDomainKeyword.getKeywordId() == keySetting.getKeywordId()) {
                                    int alertOperator = keySetting.getAlertOperator();
                                    if (keySetting.getAlertValue() != null && !keySetting.getAlertValue().trim().equals("")) {
                                        alertVal = Integer.parseInt(keySetting.getAlertValue());
                                    }
                                    if (userDomainKeyword.getKeywordRankInG() == -1) {
                                        keywordRankInG = 101;
                                    } else {
                                        keywordRankInG = userDomainKeyword.getKeywordRankInG();
                                    }
                                    if (userDomainKeyword.getKeywordRankInB() == -1) {
                                        keywordRankInB = 101;
                                    } else {
                                        keywordRankInB = userDomainKeyword.getKeywordRankInB();
                                    }
                                    if (alertVal > 0) {
                                        switch (alertOperator) {
                                            case 1:
                                                if (keySetting.isGoogleCheckbox() && keySetting.isBingCheckbox()) {
                                                    if (alertVal == keywordRankInG && alertVal == keywordRankInB) {
                                                        matched = true;
                                                        searchEngine = "Google,Bing";
                                                    } else if (alertVal == keywordRankInG) {
                                                        matched = true;
                                                        searchEngine = "Google";
                                                    } else if (alertVal == keywordRankInB) {
                                                        matched = true;
                                                        searchEngine = "Bing";
                                                    }
                                                } else if (keySetting.isGoogleCheckbox()) {
                                                    if (alertVal == keywordRankInG) {
                                                        matched = true;
                                                        searchEngine = "Google";
                                                    }
                                                } else if (keySetting.isBingCheckbox()) {
                                                    if (alertVal == keywordRankInB) {
                                                        matched = true;
                                                        searchEngine = "Bing";
                                                    }
                                                }
                                                if (matched == true && searchEngine != null) {
                                                    KeywordTaskInfo keywordTaskInfo = new KeywordTaskInfo();
                                                    keywordTaskInfo.setKeyword(keySetting.getKeyword());
                                                    keywordTaskInfo.setGoogleRank(userDomainKeyword.getKeywordRankInG());
                                                    keywordTaskInfo.setBingRank(userDomainKeyword.getKeywordRankInB());
                                                    keywordTaskInfo.setSerachEngine(searchEngine);
                                                    alertValueReachedKeywords.add(keywordTaskInfo);
                                                }
                                                break;
                                            case 2:
                                                //>
                                                if (keySetting.isGoogleCheckbox() == true && keySetting.isBingCheckbox() == true) {
                                                    if (keywordRankInG > alertVal && keywordRankInB > alertVal) {
                                                        matched = true;
                                                        searchEngine = "Google,Bing";
                                                    } else if (keywordRankInG > alertVal) {
                                                        matched = true;
                                                        searchEngine = "Google";
                                                    } else if (keywordRankInB > alertVal) {
                                                        matched = true;
                                                        searchEngine = "Bing";
                                                    }
                                                } else if (keySetting.isGoogleCheckbox()) {
                                                    if (keywordRankInG > alertVal) {
                                                        matched = true;
                                                        searchEngine = "Google";
                                                    }
                                                } else if (keySetting.isBingCheckbox()) {
                                                    if (keywordRankInB > alertVal) {
                                                        matched = true;
                                                        searchEngine = "Bing";
                                                    }
                                                }
                                                if (matched == true && searchEngine != null) {
                                                    KeywordTaskInfo keywordTaskInfo = new KeywordTaskInfo();
                                                    keywordTaskInfo.setKeyword(keySetting.getKeyword());
                                                    keywordTaskInfo.setGoogleRank(userDomainKeyword.getKeywordRankInG());
                                                    keywordTaskInfo.setBingRank(userDomainKeyword.getKeywordRankInB());
                                                    keywordTaskInfo.setSerachEngine(searchEngine);
                                                    alertValueReachedKeywords.add(keywordTaskInfo);
                                                }
                                                break;
                                            case 3:
                                                //<
                                                if (keySetting.isGoogleCheckbox() == true && keySetting.isBingCheckbox() == true) {
                                                    if (keywordRankInG < alertVal && keywordRankInB < alertVal) {
                                                        if (keywordRankInG != 0 && keywordRankInB != 0) {
                                                            matched = true;
                                                            searchEngine = "Google,Bing";
                                                        }
                                                    } else if (keywordRankInG < alertVal) {
                                                        if (keywordRankInG != 0) {
                                                            matched = true;
                                                            searchEngine = "Google";
                                                        }
                                                    } else if (keywordRankInB < alertVal) {
                                                        if (keywordRankInB != 0) {
                                                            matched = true;
                                                            searchEngine = "Bing";
                                                        }
                                                    }
                                                } else if (keySetting.isGoogleCheckbox()) {
                                                    if (keywordRankInG != 0) {
                                                        if (keywordRankInG < alertVal) {
                                                            matched = true;
                                                            searchEngine = "Google";
                                                        }
                                                    }
                                                } else if (keySetting.isBingCheckbox()) {
                                                    if (keywordRankInB != 0) {
                                                        if (keywordRankInB < alertVal) {
                                                            matched = true;
                                                            searchEngine = "Bing";
                                                        }
                                                    }
                                                }
                                                if (matched == true && searchEngine != null) {
                                                    KeywordTaskInfo keywordTaskInfo = new KeywordTaskInfo();
                                                    keywordTaskInfo.setKeyword(keySetting.getKeyword());
                                                    keywordTaskInfo.setGoogleRank(userDomainKeyword.getKeywordRankInG());
                                                    keywordTaskInfo.setBingRank(userDomainKeyword.getKeywordRankInB());
                                                    keywordTaskInfo.setSerachEngine(searchEngine);
                                                    alertValueReachedKeywords.add(keywordTaskInfo);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (alertValueReachedKeywords.size() > 0 && (this.email != null && !this.email.trim().isEmpty()) && (this.userName != null && !this.userName.trim().isEmpty())) {
                        SendMail.sendAlertMailToUser(userDomain, this.email, alertValueReachedKeywords, this.userName, forMailsDt.format(curdate));
                    }
                }
            }

            if (weeklyReport) {

                SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String tempCurDate = dtf.format(curdate);
                String tempStartDt = dtf.format(startDateInDb);
                Date startDate1 = null, endDate1 = null;
                try {
                    startDate1 = dtf.parse(tempStartDt);
                    endDate1 = dtf.parse(tempCurDate);
                } catch (ParseException e1) {
                    logger.error(e1);
                }
                long diff = krcService.getDateDiff(startDate1, endDate1);
                if (diff % 7 == 0) {
                    Timestamp seventhDt = krcService.subtractDays(curdate, 7);
                    String dwnfileName = null;
                    dwnfileName = krcService.createPdfFile(downloadFolder, rankUserId, compDomainSettingRows, keywordSettingRows, keywordStats, startDateInDb);
                    String waterMarkFile = null;
                    if (dwnfileName != null) {
                        waterMarkFile = krcService.addWaterMark(dwnfileName, downloadFolder);
                    }
                    if (waterMarkFile != null && (this.email != null && !this.email.trim().isEmpty()) && (this.userName != null && !this.userName.trim().isEmpty())) {
                        String toolName = " Weekly Keyword Rank Checker";
                        Common.sendWeeklyReportThroughMail(downloadFolder, waterMarkFile, this.email, toolName, this.userName, forMailsDt.format(seventhDt), forMailsDt.format(curdate));
                    }

                }
            }

            logger.info("**********Fetching keyword data completed for user SEO Rank Checker Tool **************" + email);
        } catch (BeansException | NumberFormatException e) {
            logger.error("Exception in SeoRankCheckerInfoTask run method cause:: ",e);
        }

    }

}
