package lxr.marketplace.keywordrankchecker;

import java.io.Serializable;

public class KeywordSettings  implements Serializable {
        
	    private long keywordId;
	    private String keyword;
	    private int alertOperator;
	    private String alertValue;
	    private boolean googleCheckbox;
		private boolean bingCheckbox; 
	    
		
       public String getAlertValue() {
			return alertValue;
		}
		public void setAlertValue(String alertValue) {
			this.alertValue = alertValue;
		}
	  public boolean isGoogleCheckbox() {
			return googleCheckbox;
		}
		public void setGoogleCheckbox(boolean googleCheckbox) {
			this.googleCheckbox = googleCheckbox;
		}
		public boolean isBingCheckbox() {
			return bingCheckbox;
		}
		public void setBingCheckbox(boolean bingCheckbox) {
			this.bingCheckbox = bingCheckbox;
		}
	   public int getAlertOperator() {
			return alertOperator;
		}
		public void setAlertOperator(int alertOperator) {
			this.alertOperator = alertOperator;
		}
//		public int getAlertValue() {
//			return alertValue;
//		}
//		public void setAlertValue(int alertValue) {
//			this.alertValue = alertValue;
//		}
		public String getKeyword() {
			return keyword;
		}
		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}
        public long getKeywordId() {
			return keywordId;
		 }
		public void setKeywordId(long keywordId) {
			this.keywordId = keywordId;
		 }
}
