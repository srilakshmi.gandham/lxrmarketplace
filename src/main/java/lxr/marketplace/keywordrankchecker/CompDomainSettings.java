package lxr.marketplace.keywordrankchecker;
import java.io.Serializable;
public class CompDomainSettings implements Serializable {

	private long domainId;
	private String domain;
	private boolean userDomain;
	
	public long getDomainId() {
		return domainId;
	}
	public void setDomainId(long domainId) {
		this.domainId = domainId;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public boolean isUserDomain() {
		return userDomain;
	}
	public void setUserDomain(boolean isUserDomain) {
		this.userDomain = isUserDomain;
	}
	
	
}
