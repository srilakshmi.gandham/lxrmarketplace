package lxr.marketplace.keywordrankchecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.apiaccess.BingSearchAPI;
import lxr.marketplace.apiaccess.GoogleCustomSearch;
import lxr.marketplace.apiaccess.lxrmbeans.SearchOrganicLink;

import lxr.marketplace.util.Common;
import net.sf.json.JSONArray;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import org.springframework.jdbc.InvalidResultSetAccessException;

public class KeywordRankCheckerService extends JdbcDaoSupport {

    private static final int REG_USER_KEYWOD_LIMIT = 5;
    private static final int REG_USER_DOMAIN_LIMIT = 4; //including user domain in count

    private BingSearchAPI bingSearchAPI;
    private GoogleCustomSearch googleCustomSearch;

    public void setBingSearchAPI(BingSearchAPI bingSearchAPI) {
        this.bingSearchAPI = bingSearchAPI;
    }

    public void setGoogleCustomSearch(GoogleCustomSearch googleCustomSearch) {
        this.googleCustomSearch = googleCustomSearch;
    }

    public void updateGuestEntryToLoggedIn(long rankUserId, long userId) {
        String query = "update rank_checker_user set is_logged_in = 1, user_id = ?  where id = ?";
        Object[] valLis = new Object[2];
        valLis[0] = userId;
        valLis[1] = rankUserId;
        try {
            getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.error("Exception for updating guest user entry: ", ex);
        }
    }

    public Long insertIntoRankCheckerUser(final long userId, final KeywordRankChecker keywordRankChecker,
            final Timestamp startTime, final String bingMarketValue, final boolean userLoggedIn) {
        final String query = "insert into rank_checker_user(user_id,alert_status , weekly_report,"
                + "geographic_loc, language, bingMarkVal, start_date, is_logged_in,"
                + "tool_unsubscription,tool_unsub_date,remark) values (?,?,?,?,?,?,?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"dom_id"});
                    statement.setLong(1, userId);
                    statement.setBoolean(2, keywordRankChecker.isAlertStatus());
                    statement.setBoolean(3, keywordRankChecker.isWeeklyReport());
                    statement.setString(4, keywordRankChecker.getLocation());
                    statement.setString(5, keywordRankChecker.getLanguage());
                    statement.setString(6, bingMarketValue);
                    statement.setTimestamp(7, startTime);
                    statement.setBoolean(8, userLoggedIn);
                    statement.setBoolean(9, false);
                    statement.setTimestamp(10, null);
                    statement.setString(11, "");
                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return (Long) keyHolder.getKey();
    }

    public long insertIntoRankCheckerDomain(final long rankUserId, final String domain, final boolean isUserDomain,
            final Timestamp startTime) {

        final String query = "insert into rank_checker_domain(domain, rank_user_id, is_userdomain, added_date,"
                + "is_deleted) values (?,?,?,?,?) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                getJdbcTemplate().update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(query, new String[]{"dom_id"});
                    try {
                        statement.setBytes(1, domain.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(1, domain);
                        logger.error("Excpetion in insertIntoRankCheckerDomain Method of KeywordRankCheckerService ",e);
                    }
                    statement.setLong(2, rankUserId);
                    statement.setBoolean(3, isUserDomain);
                    statement.setTimestamp(4, startTime);
                    statement.setBoolean(5, false);
                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return (Long) keyHolder.getKey();
    }

    public void insertDataIntoDomainTab(final long rankUserId, final List<String> domains, final Timestamp startTime) {
        final boolean isUserDomain = false;
        final String query = "insert into rank_checker_domain(domain, rank_user_id, is_userdomain, added_date, "
                + "is_deleted) values (?,?,?,?,?) ";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setString(1, domains.get(i));
                statement.setLong(2, rankUserId);
                statement.setBoolean(3, isUserDomain);
                statement.setTimestamp(4, startTime);
                statement.setBoolean(5, false);
                ;
            }

            @Override
            public int getBatchSize() {
                return domains.size();
            }
        });
    }

    public void insertIntoRankCheckerKeyword(final long rankUserId, final List<String> keywords, final Timestamp startTime) {
        final boolean isDeleted = false;
        final String query = "insert into rank_checker_keyword(rank_user_id, keyword, added_date, is_deleted,"
                + "alert_operator,alert_value,google_se,bing_se) values (?,?,?,?,?,?,?,?)";
        logger.info("Inserting keyword for user with rank_user_id = " + rankUserId);
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setLong(1, rankUserId);
                try {
                    statement.setBytes(2, keywords.get(i).trim().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    statement.setString(2, keywords.get(i));
                    logger.info("Excpetion in insertIntoRankCheckerDomain Method of KeywordRankCheckerService ");
                    logger.error(e.getMessage());
                }
                statement.setTimestamp(3, startTime);
                statement.setBoolean(4, isDeleted);
                statement.setInt(5, 0);
                statement.setString(6, "");
                statement.setBoolean(7, false);
                statement.setBoolean(8, false);
            }

            @Override
            public int getBatchSize() {
                return keywords.size();
            }
        });
    }

    public List<KeywordSettings> createKeywordRows(long rankUserId) {
        String query = "select key_id, keyword, alert_operator, alert_value, google_se, bing_se from rank_checker_keyword "
                + "where rank_user_id = ? and is_deleted = ? ";
        try {
            List<KeywordSettings> keywordSettings = getJdbcTemplate().query(query, new Object[]{rankUserId, 0}, (ResultSet rs, int rowNum) -> {
                KeywordSettings keywordSetting = new KeywordSettings();
                keywordSetting.setKeywordId(rs.getLong("key_id"));
                keywordSetting.setKeyword(rs.getString("keyword"));
                keywordSetting.setAlertOperator(rs.getInt("alert_operator"));
                keywordSetting.setAlertValue(rs.getString("alert_value"));
                keywordSetting.setGoogleCheckbox(rs.getBoolean("google_se"));
                keywordSetting.setBingCheckbox(rs.getBoolean("bing_se"));
                return keywordSetting;
            });
            return keywordSettings;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public List<KeywordSettings> createKeywordSettingsRows(List<KeywordSettings> keywordSettings,
            KeywordRankChecker keywordRankChecker) {
        keywordRankChecker.setKeyCount(keywordSettings.size());
        if (keywordSettings.size() < REG_USER_KEYWOD_LIMIT) {
            int j = 0;
            for (int i = keywordSettings.size() + 1; i <= REG_USER_KEYWOD_LIMIT; i++) {
                KeywordSettings keywordSetting = new KeywordSettings();
                keywordSetting.setKeywordId(j--);
                keywordSetting.setKeyword("");
                keywordSetting.setAlertOperator(0);
                keywordSetting.setAlertValue("");
                keywordSettings.add(keywordSetting);
            }
        }
        return keywordSettings;
    }

    public List<CompDomainSettings> createCompDomainRows(long rankUserId) {
        String query = "select dom_id ,domain, is_userdomain from rank_checker_domain where rank_user_id = ? "
                + "and is_deleted = ? order by dom_id ";
        try {
            List<CompDomainSettings> domSettings = getJdbcTemplate().query(query,
                    new Object[]{rankUserId, 0}, (ResultSet rs, int rowNum) -> {
                        CompDomainSettings domainSetting = new CompDomainSettings();
                        domainSetting.setDomainId(rs.getLong("dom_id"));
                        domainSetting.setDomain(rs.getString("domain"));
                        domainSetting.setUserDomain(rs.getBoolean("is_userdomain"));
                        return domainSetting;
                    });
            return domSettings;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    public List<CompDomainSettings> createCompDomainSettingRows(List<CompDomainSettings> domSettings,
            KeywordRankChecker keywordRankChecker) {
        keywordRankChecker.setDomainCount(domSettings.size());
        if (domSettings.size() < REG_USER_DOMAIN_LIMIT) {
            int j = 0;
            for (int i = domSettings.size() + 1; i <= REG_USER_DOMAIN_LIMIT; i++) {
                CompDomainSettings domainSetting = new CompDomainSettings();
                domainSetting.setDomainId(j--);
                domainSetting.setDomain("");
                domainSetting.setUserDomain(false);
                domSettings.add(domainSetting);
            }
        }
        return domSettings;
    }

    public int updateKeywordStatus(long keyId) {
        String query = "update rank_checker_keyword set is_deleted=? where key_id = ?";
        Object[] valLis = new Object[2];
        valLis[0] = 1;
        valLis[1] = keyId;
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return 0;
    }

    public int updateCompDomainStatus(long domainId) {
        String query = "update rank_checker_domain set is_deleted = ? where dom_id = ?";
        Object[] valLis = new Object[2];
        valLis[0] = 1;
        valLis[1] = domainId;
        try {
            return getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            logger.error(ex);
        }
        return 0;
    }

    public void insertIntoKeywordStats(Map<Long, List<KeywordRankCheckerStats>> totalResultMap) {
        final Timestamp date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String query = "insert into rank_checker_keyword_stats(keyword_id, domain_id, keyword_rank_g, "
                + "keyword_rank_b, date) values (?,?,?,?,?)";
        if (totalResultMap != null && totalResultMap.size() > 0) {
            Iterator<Map.Entry<Long, List<KeywordRankCheckerStats>>> statsData = totalResultMap.entrySet().iterator();
            while (statsData.hasNext()) {
                Map.Entry<Long, List<KeywordRankCheckerStats>> nextkv = statsData.next();
                final long domainId = nextkv.getKey();
                final List<KeywordRankCheckerStats> kwStats = nextkv.getValue();
                getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        KeywordRankCheckerStats kwdStatResults = kwStats.get(i);
                        ps.setLong(1, kwdStatResults.getKeywordId());
                        ps.setLong(2, domainId);
                        ps.setLong(3, kwdStatResults.getKeywordRankInG());
                        ps.setLong(4, kwdStatResults.getKeywordRankInB());
                        ps.setTimestamp(5, date);
                    }

                    @Override
                    public int getBatchSize() {
                        return kwStats.size();
                    }
                });
            }
        }
    }

    //for guest users
    public Map<Long, List<KeywordRankCheckerStats>> getStatsData(long rankUserId, KeywordRankChecker keywordRankChecker,
            String newDate, List<CompDomainSettings> compDomainRows) {
        final Map<Long, List<KeywordRankCheckerStats>> totalResultMap = new HashMap<>();
        if (compDomainRows != null && compDomainRows.size() > 0) {
            compDomainRows.stream().forEach((compDomain) -> {
                totalResultMap.put(compDomain.getDomainId(), new ArrayList<>());
            });
        }
        String query = "select a.*, b.domain_id, b.keyword_rank_g, b.keyword_rank_b from "
                + "(select key_id from rank_checker_keyword where rank_user_id = " + rankUserId + " and is_deleted = 0)a"
                + " left join (select * from rank_checker_keyword_stats where date_format(date, '%y-%m-%d') = "
                + "date_format('" + newDate + "','%y-%m-%d'))b on a.key_id = b.keyword_id";
        logger.info("map query" + query);
        SqlRowSet sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        if (sqlRowSet != null) {
            while (sqlRowSet.next()) {
                List<KeywordRankCheckerStats> statsList = totalResultMap.get(sqlRowSet.getLong("domain_id"));
                KeywordRankCheckerStats keywordRankCheckerStats = new KeywordRankCheckerStats();
                keywordRankCheckerStats.setKeywordId(sqlRowSet.getLong("key_id"));
                keywordRankCheckerStats.setKeywordRankInG(sqlRowSet.getInt("keyword_rank_g"));
                keywordRankCheckerStats.setKeywordRankInB(sqlRowSet.getInt("keyword_rank_b"));
                keywordRankCheckerStats.setProgressInG(5);
                keywordRankCheckerStats.setProgressInB(5);
                statsList.add(keywordRankCheckerStats);
            }
        }
        return totalResultMap;
    }

    //for registered users
    public Map<Long, List<KeywordRankCheckerStats>> getKeywordStatsData(long rankUserId,
            KeywordRankChecker keywordRankChecker, String newDate, List<CompDomainSettings> compDomainRows) {
        final Map<Long, List<KeywordRankCheckerStats>> totalResultMap = new HashMap<>();
        String query = "";
        if (compDomainRows != null && compDomainRows.size() > 0) {
            compDomainRows.stream().forEach((compDomain) -> {
                totalResultMap.put(compDomain.getDomainId(), new ArrayList<>());
            });
        }
//        if (islatest) { 
//            query = "select all_key.*, key_stat.keyword_rank_g, key_stat.keyword_rank_b, key_stat.date from "
//                + "(select a.key_id as keyword_id,b.dom_id as domain_id from "
//                + "(select key_id,rank_user_id from rank_checker_keyword where rank_user_id  = "+rankUserId+" and  is_deleted = 0)a "
//                + "JOIN "
//                + "(select dom_id,rank_user_id from rank_checker_domain  where rank_user_id  = "+rankUserId+" and  is_deleted = 0)b on a.rank_user_id = b.rank_user_id) all_key "
//                + "LEFT JOIN "
//                + "(select stats.* from rank_checker_keyword_stats stats,"
//                + "(select keyword_id, max(date) as max_date from rank_checker_keyword_stats where "
//                + "date_format(date,'%Y-%m-%d') <= date_format('"+newDate+"','%Y-%m-%d') and keyword_id in "
//                + "(select key_id from rank_checker_keyword where rank_user_id  = "+rankUserId+" and is_deleted = 0) group by keyword_id)max_d "
//                + "where stats.keyword_id = max_d.keyword_id and  stats.date = max_d.max_date and domain_id in "
//                + "(select dom_id from rank_checker_domain where rank_user_id  = "+rankUserId+" and  is_deleted = 0))key_stat "
//                + "on all_key.keyword_id = key_stat.keyword_id and all_key.domain_id = key_stat.domain_id";

        query = "SELECT all_key . *, key_stat.keyword_rank_g, key_stat.keyword_rank_b,key_stat.date "
                + " FROM(SELECT a.key_id AS keyword_id, b.dom_id AS domain_id FROM  (SELECT key_id, rank_user_id FROM "
                + " rank_checker_keyword WHERE rank_user_id = " + rankUserId + " AND is_deleted = 0) a "
                + " JOIN "
                + " (SELECT dom_id, rank_user_id FROM rank_checker_domain WHERE rank_user_id = " + rankUserId + " AND is_deleted = 0) b ON a.rank_user_id = b.rank_user_id) all_key "
                + " LEFT JOIN"
                + " (SELECT stats . * FROM rank_checker_keyword_stats stats, (SELECT  keyword_id, MAX(date) AS max_date  FROM  rank_checker_keyword_stats "
                + " WHERE DATE_FORMAT(date, '%Y-%m-%d') <= DATE_FORMAT('" + newDate + "', '%Y-%m-%d') AND keyword_id IN (SELECT   key_id FROM  rank_checker_keyword "
                + " WHERE rank_user_id = " + rankUserId + " AND is_deleted = 0) GROUP BY keyword_id , domain_id) max_d WHERE stats.keyword_id = max_d.keyword_id AND stats.date = max_d.max_date "
                + " AND domain_id IN (SELECT   dom_id FROM  rank_checker_domain WHERE rank_user_id = " + rankUserId + " AND is_deleted = 0)) key_stat ON all_key.keyword_id = key_stat.keyword_id "
                + " AND all_key.domain_id = key_stat.domain_id group by keyword_id , domain_id";

//        } else {
//            query =   " select s.domain_id, k.rank_user_id, s.keyword_id, s.keyword_rank_b, s.keyword_rank_g, s.date "
//                      + "from rank_checker_keyword_stats s,rank_checker_keyword k, rank_checker_domain d "
//                      + "where k.key_id=s.keyword_id and d.dom_id = s.domain_id and d.is_deleted=0 and "
//                    + "k.rank_user_id = "+rankUserId+" and k.is_deleted= 0 and s.date=(select max(sk.date) from rank_checker_keyword_stats sk,rank_checker_keyword kc "
//                      + "where kc.key_id=sk.keyword_id and kc.rank_user_id = "+rankUserId+" and k.is_deleted= 0 and s.keyword_id=sk.keyword_id "
//                      + "and sk.date <(select max(date) from rank_checker_keyword_stats sr,rank_checker_keyword kr where "
//                      + "sr.keyword_id=kr.key_id and kr.rank_user_id = "+rankUserId+" and k.is_deleted= 0 and sr.keyword_id=sk.keyword_id "
//                      + "group by sr.keyword_id) group by sk.keyword_id) group by s.keyword_id, s.domain_id ";
//            
//        }
        SqlRowSet sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        if (sqlRowSet != null) {
            while (sqlRowSet.next()) {
                if (sqlRowSet.getLong("domain_id") == 0) {
                    for (CompDomainSettings domain : compDomainRows) {
                        KeywordRankCheckerStats keywordRankCheckerStats = new KeywordRankCheckerStats();
                        keywordRankCheckerStats.setKeywordId(sqlRowSet.getLong("keyword_id"));
                        keywordRankCheckerStats.setKeywordRankInG(-2);
                        keywordRankCheckerStats.setKeywordRankInB(-2);
                        totalResultMap.get(domain.getDomainId()).add(keywordRankCheckerStats);
                    }
                } else {
                    List<KeywordRankCheckerStats> statsList = totalResultMap.get(sqlRowSet.getLong("domain_id"));
                    KeywordRankCheckerStats keywordRankCheckerStats = new KeywordRankCheckerStats();
                    keywordRankCheckerStats.setKeywordId(sqlRowSet.getLong("keyword_id"));
                    keywordRankCheckerStats.setKeywordRankInG(sqlRowSet.getInt("keyword_rank_g"));
                    keywordRankCheckerStats.setKeywordRankInB(sqlRowSet.getInt("keyword_rank_b"));
                    statsList.add(keywordRankCheckerStats);
                }
            }
        }
        return totalResultMap;
    }

    public Map<Long, List<KeywordRankCheckerStats>> compareStatsData(long rankUserId, KeywordRankChecker keywordRankChecker,
            List<CompDomainSettings> compDomainSettingRows) {
        long MILLISECONDS_IN_DAY = 1000l * 60l * 60l * 24l;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = Calendar.getInstance().getTime();
        String curFormatDt = df.format(curDate);
        Date prvDate = (new Date(curDate.getTime() - (7 * MILLISECONDS_IN_DAY)));
        String prvFormatDt = df.format(prvDate);
        Map<Long, List<KeywordRankCheckerStats>> curKeywordStatsData = getKeywordStatsData(rankUserId, keywordRankChecker, curFormatDt, compDomainSettingRows);
        Map<Long, List<KeywordRankCheckerStats>> prvKeywordStatsData = getKeywordStatsData(rankUserId, keywordRankChecker, prvFormatDt, compDomainSettingRows);
        for (CompDomainSettings domainRow : compDomainSettingRows) {
            List<KeywordRankCheckerStats> curList = curKeywordStatsData.get(domainRow.getDomainId());
            List<KeywordRankCheckerStats> prvList = prvKeywordStatsData.get(domainRow.getDomainId());
            if (curList != null && curList.size() > 0) {
                if (prvList != null && prvList.size() > 0) {
                    for (KeywordRankCheckerStats curStats : curList) {
                        for (KeywordRankCheckerStats prvStats : prvList) {
                            if (curStats.getKeywordId() == prvStats.getKeywordId()) {

                                if (curStats.getKeywordRankInG() < 0 && prvStats.getKeywordRankInG() < 0) {
                                    if (curStats.getKeywordRankInG() == -1 && prvStats.getKeywordRankInG() == -1) { // -1 means rank > 100
                                        curStats.setProgressInG(0);
                                    } else if (curStats.getKeywordRankInG() == -1 && prvStats.getKeywordRankInG() == -2) {
                                        curStats.setProgressInG(5);
                                    } else if (curStats.getKeywordRankInG() == -2 && prvStats.getKeywordRankInG() == -1) {
                                        curStats.setProgressInG(10);
                                    } else if (curStats.getKeywordRankInG() == -2 && prvStats.getKeywordRankInG() == -2) {
                                        curStats.setProgressInG(10);
                                    }
                                } else if (curStats.getKeywordRankInG() == -1 && prvStats.getKeywordRankInG() > 0) {
                                    curStats.setProgressInG(-1);
                                } else if (curStats.getKeywordRankInG() == -2 && prvStats.getKeywordRankInG() > 0) {
                                    curStats.setProgressInG(10); // 10 means no current stats
                                } else if (curStats.getKeywordRankInG() > 0 && prvStats.getKeywordRankInG() == -1) {
                                    curStats.setProgressInG(1);
                                } else if (curStats.getKeywordRankInG() > 0 && prvStats.getKeywordRankInG() == -2) {
                                    curStats.setProgressInG(5); // 5 means no previous stats
                                } else if (curStats.getKeywordRankInG() > 0 && prvStats.getKeywordRankInG() > 0) {
                                    int gVal = curStats.getKeywordRankInG() - prvStats.getKeywordRankInG();
                                    if (gVal == 0) {
                                        curStats.setProgressInG(0);
                                    } else if (gVal > 0) {
                                        curStats.setProgressInG(-1);   //1
                                    } else {
                                        curStats.setProgressInG(1);   //-1
                                    }
                                } else if (curStats.getKeywordRankInG() == 0) {
                                    curStats.setProgressInG(5);
                                }
                                if (curStats.getKeywordRankInB() < 0 && prvStats.getKeywordRankInB() < 0) {
                                    if (curStats.getKeywordRankInB() == -1 && prvStats.getKeywordRankInB() == -1) {
                                        curStats.setProgressInB(0);
                                    } else if (curStats.getKeywordRankInB() == -1 && prvStats.getKeywordRankInB() == -2) { //-2 means no stats data
                                        curStats.setProgressInB(5);
                                    } else if (curStats.getKeywordRankInB() == -2 && prvStats.getKeywordRankInB() == -1) {
                                        curStats.setProgressInB(10);
                                    } else if (curStats.getKeywordRankInG() == -2 && prvStats.getKeywordRankInG() == -2) {
                                        curStats.setProgressInG(10);
                                    }
                                } else if (curStats.getKeywordRankInB() == -1 && prvStats.getKeywordRankInB() > 0) {
                                    curStats.setProgressInB(-1);
                                } else if (curStats.getKeywordRankInG() == -2 && prvStats.getKeywordRankInG() > 0) {
                                    curStats.setProgressInG(10);
                                } else if (curStats.getKeywordRankInB() > 0 && prvStats.getKeywordRankInB() == -1) {
                                    curStats.setProgressInB(1);
                                } else if (curStats.getKeywordRankInB() > 0 && prvStats.getKeywordRankInB() == -2) {
                                    curStats.setProgressInB(5);
                                } else if (curStats.getKeywordRankInB() > 0 && prvStats.getKeywordRankInB() > 0) {
                                    int gVal = curStats.getKeywordRankInB() - prvStats.getKeywordRankInB();
                                    if (gVal == 0) {
                                        curStats.setProgressInB(0);
                                    } else if (gVal > 0) {
                                        curStats.setProgressInB(-1);
                                    } else {
                                        curStats.setProgressInB(1);
                                    }
                                } else if (curStats.getKeywordRankInB() == 0) {
                                    curStats.setProgressInB(5);
                                }
                            }
                        }
                        //TODO: CHECK IF THIS CURRENT KEYWORD NOT MATCHED WITH ANY PREVIOUS KEYWORD  
                    }
                } else {
                    for (KeywordRankCheckerStats curStats : curList) {
                        if (curStats.getKeywordRankInG() == 0) {
                            curStats.setProgressInG(5);
                        }
                        if (curStats.getKeywordRankInB() == 0) {
                            curStats.setProgressInB(5);
                        }
                        curStats.setProgressInB(5);
                        curStats.setProgressInG(5);
                    }
                }
            }
        }
        return curKeywordStatsData;
    }

    public Map<Long, List<KeywordRankCheckerStats>> createSortedMap(boolean userLoggedIn, long userId,
            KeywordRankChecker keywordRankChecker, List<CompDomainSettings> compDomainSettingRows, HttpSession session) {
        Map<Long, List<KeywordRankCheckerStats>> curKeywordStatsData = null;
        if (userLoggedIn) {
            Long rankUserId = fetchRankUserIdFromRankUserLoggedIn(userId);
            if (rankUserId != null && rankUserId > 0) {
                curKeywordStatsData = compareStatsData(rankUserId, keywordRankChecker, compDomainSettingRows);
            }
        } else if (!userLoggedIn) {
            Long rankUserId = fetchRankUserIdFromRankUserGuest(userId);
            if (rankUserId != null && rankUserId > 0) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date curDate = Calendar.getInstance().getTime();
                String curFormatDt = df.format(curDate);
                curKeywordStatsData = getStatsData(rankUserId, keywordRankChecker, curFormatDt, compDomainSettingRows);

            }
        }
        long userDomainId = 0;
        List<KeywordRankCheckerStats> userDomainKeywordsList = null;
        if (compDomainSettingRows != null && compDomainSettingRows.size() > 0) {
            for (CompDomainSettings compDomain : compDomainSettingRows) {
                if (compDomain.isUserDomain()) {
                    userDomainId = compDomain.getDomainId();
                    userDomainKeywordsList = curKeywordStatsData.get(userDomainId);
                    Collections.sort(userDomainKeywordsList);
                    session.setAttribute("userDomainKeywordsList", userDomainKeywordsList);
                }
            }
            if (curKeywordStatsData != null && curKeywordStatsData.size() > 0) {
                for (CompDomainSettings compDom : compDomainSettingRows) {
                    if (!compDom.isUserDomain()) {
                        List<KeywordRankCheckerStats> compDomainKeywordList = curKeywordStatsData.
                                get(compDom.getDomainId());
                        if (compDomainKeywordList != null && !compDomainKeywordList.equals("") && compDomainKeywordList.size() > 0) {
                            List<KeywordRankCheckerStats> newCompDomainKeywordList = new ArrayList<>();
                            if (userDomainKeywordsList != null) {
                                for (KeywordRankCheckerStats udKeywordStats : userDomainKeywordsList) {
                                    if (compDomainKeywordList != null) {
                                        for (KeywordRankCheckerStats compdkeywordStats : compDomainKeywordList) {
                                            if (udKeywordStats.getKeywordId() == compdkeywordStats.getKeywordId()) {
                                                newCompDomainKeywordList.add(compdkeywordStats);
                                            }
                                        }
                                    }
                                }
                            }
                            curKeywordStatsData.put(compDom.getDomainId(), newCompDomainKeywordList);
                        }
                    }
                }
            }
        }
        return curKeywordStatsData;
    }

    public void getCompDomainResult(HttpServletResponse response, long compDomainId,
            Map<Long, List<KeywordRankCheckerStats>> keywordStatsData) {
        List<KeywordRankCheckerStats> keywordStats = keywordStatsData.get(compDomainId);
        if (keywordStats != null && keywordStats.size() > 0) {
            KeywordRankCheckerStats[] data = keywordStats.toArray(new KeywordRankCheckerStats[keywordStats.size()]);
            for (KeywordRankCheckerStats kws : data) {
                logger.info("data " + kws.getKeywordRankInG() + " " + kws.getKeywordRankInB());
            }
            addInJson(data, response);
        }
    }

    public void addInJson(KeywordRankCheckerStats[] data, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            arr.add(0, data);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<KeywordSettings> createSortedKeywordSettingsRows(List<KeywordSettings> keywordSettingRows,
            HttpSession session) {
        List<KeywordRankCheckerStats> userDomainKeywordsList = (List<KeywordRankCheckerStats>) session
                .getAttribute("userDomainKeywordsList");
        List<KeywordSettings> sortedKeywordSettingRows = new ArrayList<>();
        if (userDomainKeywordsList != null && userDomainKeywordsList.size() > 0) {
            for (KeywordRankCheckerStats udKeywordStats : userDomainKeywordsList) {
                if (keywordSettingRows != null && keywordSettingRows.size() > 0) {
                    for (KeywordSettings keywordSettingRow : keywordSettingRows) {
                        if (udKeywordStats.getKeywordId() == keywordSettingRow.getKeywordId()) {
                            sortedKeywordSettingRows.add(keywordSettingRow);
                        }
                    }
                }
            }
        }
        return sortedKeywordSettingRows;
    }

    public Map<Long, List<KeywordRankCheckerStats>> getResults(List<KeywordSettings> keywordSettingsRows,
            List<CompDomainSettings> compDomainSettingRows, List<String> domains,
            KeywordRankChecker keywordRankChecker, String bingMarketValue) {
        Map<Long, List<KeywordRankCheckerStats>> totalResultMap = new HashMap<>();
        try {
            if (compDomainSettingRows != null && compDomainSettingRows.size() > 0) {
                for (CompDomainSettings compDomain : compDomainSettingRows) {
                    totalResultMap.put(compDomain.getDomainId(), new ArrayList<>());
                }
            }
            if (keywordSettingsRows != null && keywordSettingsRows.size() > 0) {
                HashMap<String, SearchOrganicLink> bingResults = null;
                int proxyHost = 0;
                for (int i = 0; i < keywordSettingsRows.size(); i++) {
                    KeywordSettings ksr = keywordSettingsRows.get(i);
                    proxyHost = i % 2;
                    if (!ksr.getKeyword().trim().equals("")) {
                        HashMap<String, SearchOrganicLink> googleResults = googleCustomSearch.getkeywordRankData(ksr.getKeyword().trim(), domains, keywordRankChecker.getLocation(), keywordRankChecker.getLanguage(), proxyHost);
                        if (bingMarketValue != null) {
                            bingResults = bingSearchAPI.getBingSearchDataV5(ksr.getKeyword(), domains, bingMarketValue);
                        }
                        if (compDomainSettingRows != null && compDomainSettingRows.size() > 0) {
                            for (CompDomainSettings compDomain : compDomainSettingRows) {
                                List<KeywordRankCheckerStats> keywordStatsList = totalResultMap.get(compDomain.getDomainId());
                                KeywordRankCheckerStats keywordRankCheckerStats = new KeywordRankCheckerStats();
                                keywordRankCheckerStats.setKeywordId(ksr.getKeywordId());
                                if (!compDomain.getDomain().trim().equals("")) {
                                    if (googleResults != null && googleResults.size() > 0) {
                                        SearchOrganicLink gPostionResult = googleResults.get(compDomain.getDomain());
                                        keywordRankCheckerStats.setKeywordRankInG(gPostionResult.getPosition());

                                    }
                                    if (bingResults != null && bingResults.size() > 0) {
                                        SearchOrganicLink bPostionResult = bingResults.get(compDomain.getDomain());
                                        keywordRankCheckerStats.setKeywordRankInB(bPostionResult.getPosition());
                                    }
                                    keywordStatsList.add(keywordRankCheckerStats);
                                }
                            }
                        }
                    }
                }
            }
            if (totalResultMap != null && totalResultMap.size() > 0) {
                insertIntoKeywordStats(totalResultMap);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return totalResultMap;
    }

    public Long fetchRankUserIdFromRankUserLoggedIn(long id) {
        String query = "select id from rank_checker_user  where user_id = ? and is_logged_in = 1";
        try {
            Long userID = getJdbcTemplate().query(query, new Object[]{id}, (ResultSet rs) -> {
                Long userId = null;
                try {
                    if (rs.next()) {
                        userId = rs.getLong("id");
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return userId;
            });
            return userID;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public Long fetchRankUserIdFromRankUserGuest(long id) {
        String query = "select id from rank_checker_user  where user_id = ? and is_logged_in = 0";
        try {
            Long finalRankUserID = getJdbcTemplate().query(query, new Object[]{id}, (ResultSet rs) -> {
                Long rankUserId = null;
                try {
                    if (rs.next()) {
                        rankUserId = rs.getLong("id");
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return rankUserId;
            });
            return finalRankUserID;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public void deleteFromRankCheckerUserByRankUserId(long rankUserId) {
        String query = "delete from rank_checker_user where id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{rankUserId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void deleteFromRankCheckerDomain(long rankUid) {
        String query = "delete from rank_checker_domain where rank_user_id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{rankUid});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public List<Integer> getListOfKeyIds(long rankUid) {
        String query = "select key_id from rank_checker_keyword where rank_user_id = ?";
        try {
            List<Integer> keyIdList = getJdbcTemplate().query(query, new Object[]{rankUid}, (ResultSet rs, int rowNum) -> {
                Integer keywordId;
                keywordId = rs.getInt("key_id");
                return keywordId;
            });
            return keyIdList;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public boolean deletePreviousRankUserdata(long Uid) {
        String query = "select id from rank_checker_user where user_id = ? and is_logged_in=0";
        boolean deletedGuestUsers = false;
        try {
            List<Integer> rankUserIdList = getJdbcTemplate().query(query, new Object[]{Uid}, (ResultSet rs, int rowNum) -> {
                Integer rankUserId;
                rankUserId = rs.getInt("id");
                return rankUserId;
            });
            if (rankUserIdList != null && rankUserIdList.size() > 0) {
                rankUserIdList.stream().forEach((rankUserId) -> {
                    deleteGuestUserDataByRankUserId(rankUserId);
                });
            }
            deletedGuestUsers = true;
            return deletedGuestUsers;
        } catch (Exception e) {
            logger.error(e);
        }
        return deletedGuestUsers;
    }

    public void deleteFromKeywordStats(long rankUid) {
        final List<Integer> keyIdList = getListOfKeyIds(rankUid);
        final String query = "delete from rank_checker_keyword_stats where keyword_id=?";
        logger.info("inside ofdeleteFromKeywordStats method ");
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, keyIdList.get(i));
            }

            @Override
            public int getBatchSize() {
                return keyIdList.size();
            }
        });
    }

    public void deleteFromRankCheckerKeyword(long rankUid) {
        String query = "delete from rank_checker_keyword where rank_user_id = ?";
        try {
            getJdbcTemplate().update(query, new Object[]{rankUid});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public void deleteGuestUserDataByRankUserId(long rankUserId) {
        deleteFromKeywordStats(rankUserId);
        deleteFromRankCheckerDomain(rankUserId);
        deleteFromRankCheckerKeyword(rankUserId);
        deleteFromRankCheckerUserByRankUserId(rankUserId);
    }

    public List<CompDomainSettings> compareCompDomainsData(long rankUserId, KeywordRankChecker keywordRankChecker) {
        Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        List<CompDomainSettings> currentCompDomainSettings = keywordRankChecker.getCompDomainSettingRows();
        List<CompDomainSettings> compRows = createCompDomainRows(rankUserId);
        List<CompDomainSettings> prvCompDomainSettingRows = createCompDomainSettingRows(compRows, keywordRankChecker);
        List<CompDomainSettings> newCompDomains = new ArrayList<>();
        List<Long> domIds = new ArrayList<>();
        boolean userDomainStatus = false;
        if (currentCompDomainSettings != null) {
            if (prvCompDomainSettingRows != null) {
                for (CompDomainSettings curCompDomain : currentCompDomainSettings) {
                    if (curCompDomain.getDomainId() > 0 && curCompDomain.isUserDomain() == false) {
                        for (CompDomainSettings prvCompDomain : prvCompDomainSettingRows) {
                            if (prvCompDomain.getDomainId() > 0 && prvCompDomain.isUserDomain() == false) {
                                if (curCompDomain.getDomainId() == prvCompDomain.getDomainId()) {
                                    if (curCompDomain.getDomain() != null && curCompDomain.getDomain().trim().equals("")) {
                                        // update the old data
                                        domIds.add(prvCompDomain.getDomainId());
                                    } else if (curCompDomain.getDomain() != null
                                            && !(curCompDomain.getDomain().equalsIgnoreCase(prvCompDomain.getDomain()))) {
                                        // insert new data in to the table & update the old data
                                        domIds.add(prvCompDomain.getDomainId());
                                        CompDomainSettings cmpDom = new CompDomainSettings();
                                        cmpDom.setDomain(curCompDomain.getDomain());
                                        cmpDom.setUserDomain(userDomainStatus);
                                        newCompDomains.add(cmpDom);
                                    }
                                    break;
                                }
                            }
                        }
                    } else if (curCompDomain.getDomainId() <= 0 && curCompDomain.isUserDomain() == false) {
                        if (curCompDomain.getDomain() != null && !curCompDomain.getDomain().trim().equals("")) {
                            CompDomainSettings cmpDom = new CompDomainSettings();
                            cmpDom.setDomain(curCompDomain.getDomain());
                            cmpDom.setUserDomain(userDomainStatus);
                            newCompDomains.add(cmpDom);
                        }
                    }
                }
                if (domIds != null && domIds.size() > 0) {
                    updateSettingsCompDomainData(domIds, rankUserId);
                }
            } else {
                for (CompDomainSettings curCompDomain : currentCompDomainSettings) {
                    if (curCompDomain.getDomain() != null && !curCompDomain.getDomain().trim().equals("") && curCompDomain.isUserDomain() == false) {
                        CompDomainSettings cmpDom = new CompDomainSettings();
                        cmpDom.setDomain(curCompDomain.getDomain());
                        cmpDom.setUserDomain(userDomainStatus);
                        newCompDomains.add(cmpDom);
                    }
                }
            }
            if (newCompDomains != null && newCompDomains.size() > 0) {
                insertSettingsDataInDomainTab(rankUserId, newCompDomains, userDomainStatus, startTime);
            }
        }
        return newCompDomains;
    }

    public List<KeywordSettings> compareKeywordsData(long rankUserId, KeywordRankChecker keywordRankChecker) {
        Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        List<KeywordSettings> currentKeywordSettingRows = keywordRankChecker.getKeywordSettingRows();
        List<KeywordSettings> keywordRows = createKeywordRows(rankUserId);
        List<KeywordSettings> prvKeywordSettingRows = createKeywordSettingsRows(keywordRows, keywordRankChecker);
        List<KeywordSettings> newKeywords = new ArrayList<>();
        List<KeywordSettings> editedKeywordData = new ArrayList<>();
        List<Long> keyIds = new ArrayList<>();

        if (currentKeywordSettingRows != null) {
            if (prvKeywordSettingRows != null) {
                for (KeywordSettings curKeywordData : currentKeywordSettingRows) {
                    if (curKeywordData.getKeywordId() > 0) {
                        for (KeywordSettings prvKeywordData : prvKeywordSettingRows) {
                            if (prvKeywordData.getKeywordId() > 0) {
                                if (curKeywordData.getKeywordId() == prvKeywordData.getKeywordId()) {
                                    if (curKeywordData.getKeyword() != null && curKeywordData.getKeyword().trim().equals("")) {
                                        keyIds.add(prvKeywordData.getKeywordId());
                                    } else if (curKeywordData.getKeyword() != null && !(curKeywordData.getKeyword().equalsIgnoreCase(prvKeywordData.getKeyword()))) {
                                        KeywordSettings newKeywordSettings = new KeywordSettings();
                                        newKeywordSettings.setAlertOperator(curKeywordData.getAlertOperator());
                                        newKeywordSettings.setAlertValue(curKeywordData.getAlertValue());
                                        newKeywordSettings.setGoogleCheckbox(curKeywordData.isGoogleCheckbox());
                                        newKeywordSettings.setBingCheckbox(curKeywordData.isBingCheckbox());
                                        keyIds.add(prvKeywordData.getKeywordId());
                                        newKeywordSettings.setKeyword(curKeywordData.getKeyword());
                                        newKeywords.add(newKeywordSettings);
                                    } else if (curKeywordData.getKeyword() != null && !curKeywordData.getKeyword().trim().equals("")) {
                                        boolean isChanged = false;
                                        if (curKeywordData.getAlertOperator() != prvKeywordData.getAlertOperator()) {
                                            isChanged = true;
                                        } else if (!curKeywordData.getAlertValue().trim().equals(prvKeywordData.getAlertValue().trim())) {
                                            isChanged = true;
                                        } else if (curKeywordData.isGoogleCheckbox() != prvKeywordData.isGoogleCheckbox()) {
                                            isChanged = true;

                                        } else if (curKeywordData.isBingCheckbox() != prvKeywordData.isBingCheckbox()) {
                                            isChanged = true;
                                        }
                                        if (isChanged) {
                                            KeywordSettings newKeywordSettings = new KeywordSettings();
                                            newKeywordSettings.setAlertOperator(curKeywordData.getAlertOperator());
                                            newKeywordSettings.setAlertValue(curKeywordData.getAlertValue());
                                            newKeywordSettings.setGoogleCheckbox(curKeywordData.isGoogleCheckbox());
                                            newKeywordSettings.setBingCheckbox(curKeywordData.isBingCheckbox());
                                            newKeywordSettings.setKeywordId(curKeywordData.getKeywordId());
                                            newKeywordSettings.setKeyword(curKeywordData.getKeyword());
                                            editedKeywordData.add(newKeywordSettings);
                                            updateKeywordsData(rankUserId, editedKeywordData, startTime);
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                    } else if (curKeywordData.getKeywordId() <= 0) {
                        if (curKeywordData.getKeyword() != null && !curKeywordData.getKeyword().trim().equals("")) {
                            KeywordSettings newKeywordSettings = new KeywordSettings();
                            newKeywordSettings.setAlertOperator(curKeywordData.getAlertOperator());
                            newKeywordSettings.setAlertValue(curKeywordData.getAlertValue());
                            newKeywordSettings.setGoogleCheckbox(curKeywordData.isGoogleCheckbox());
                            newKeywordSettings.setBingCheckbox(curKeywordData.isBingCheckbox());
                            newKeywordSettings.setKeyword(curKeywordData.getKeyword());
                            newKeywords.add(newKeywordSettings);
                        }
                    }
                }
                if (keyIds != null && keyIds.size() > 0) {
                    updateSettingsKeywordData(keyIds, rankUserId);
                }
            } else {
                for (KeywordSettings curKeywordData : currentKeywordSettingRows) {
                    if (curKeywordData.getKeyword() != null && !curKeywordData.getKeyword().trim().equals("")) {
                        KeywordSettings newKeywordSettings = new KeywordSettings();
                        newKeywordSettings.setAlertOperator(curKeywordData.getAlertOperator());
                        newKeywordSettings.setAlertValue(curKeywordData.getAlertValue());
                        newKeywordSettings.setGoogleCheckbox(curKeywordData.isGoogleCheckbox());
                        newKeywordSettings.setBingCheckbox(curKeywordData.isBingCheckbox());
                        newKeywordSettings.setKeyword(curKeywordData.getKeyword());
                        newKeywords.add(newKeywordSettings);
                    }
                }
            }
            if (newKeywords != null && newKeywords.size() > 0) {
                insertSettingsDataInKeywordTab(rankUserId, newKeywords, startTime);
            }
        }
        return newKeywords;
    }

    public int updateKeywordsData(final long rankUserId, final List<KeywordSettings> keywordSettings, final Timestamp startTime) {
        final String query = "update rank_checker_keyword set added_date= ?,alert_operator = ?,alert_value = ?,google_se = ?,bing_se = ? where key_id = ? and rank_user_id = ?";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setTimestamp(1, startTime);
                statement.setInt(2, keywordSettings.get(i).getAlertOperator());
                statement.setString(3, keywordSettings.get(i).getAlertValue());
                statement.setBoolean(4, keywordSettings.get(i).isGoogleCheckbox());
                statement.setBoolean(5, keywordSettings.get(i).isBingCheckbox());
                statement.setLong(6, keywordSettings.get(i).getKeywordId());
                statement.setLong(7, rankUserId);

            }

            @Override
            public int getBatchSize() {
                return keywordSettings.size();
            }
        });
        return 0;
    }

    public void insertSettingsDataInKeywordTab(final long rankUserId, final List<KeywordSettings> keywordSettings, final Timestamp startTime) {
        final boolean isDeleted = false;
        final String query = "insert into rank_checker_keyword(rank_user_id, keyword, added_date, "
                + "is_deleted, alert_operator, alert_value, google_se, bing_se) values (?,?,?,?,?,?,?,?)";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setLong(1, rankUserId);
                statement.setString(2, keywordSettings.get(i).getKeyword().trim());
                statement.setTimestamp(3, startTime);
                statement.setBoolean(4, isDeleted);
                statement.setInt(5, keywordSettings.get(i).getAlertOperator());
                statement.setString(6, keywordSettings.get(i).getAlertValue());
                statement.setBoolean(7, keywordSettings.get(i).isGoogleCheckbox());
                statement.setBoolean(8, keywordSettings.get(i).isBingCheckbox());
            }

            @Override
            public int getBatchSize() {
                return keywordSettings.size();
            }
        });
    }

    public void insertSettingsDataInDomainTab(final long rankUserId, final List<CompDomainSettings> compDomainSettings, final boolean userDomain, final Timestamp startTime) {
        final boolean isDeleted = false;
        final String query = "insert into rank_checker_domain(domain, rank_user_id, is_userdomain, added_date, is_deleted) values (?,?,?,?,?) ";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setString(1, compDomainSettings.get(i).getDomain());
                statement.setLong(2, rankUserId);
                statement.setBoolean(3, userDomain);
                statement.setTimestamp(4, startTime);
                statement.setBoolean(5, isDeleted);
            }

            @Override
            public int getBatchSize() {
                return compDomainSettings.size();
            }
        });
    }

    public void updateSettingsKeywordData(final List<Long> KeyIds, final long rankUserId) {

        final String query = "update rank_checker_keyword set is_deleted=? where key_id = ? and rank_user_id = ?";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setBoolean(1, true);
                statement.setLong(2, KeyIds.get(i));
                statement.setLong(3, rankUserId);
            }

            @Override
            public int getBatchSize() {
                return KeyIds.size();
            }
        });
    }

    public void updateSettingsCompDomainData(final List<Long> domIds, final long rankUserId) {

        final String query = "update rank_checker_domain set is_deleted = ? where dom_id = ? and rank_user_id = ?";
        getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setBoolean(1, true);
                statement.setLong(2, domIds.get(i));
                statement.setLong(3, rankUserId);

            }

            @Override
            public int getBatchSize() {
                return domIds.size();
            }
        });
    }

    //For Area chart
    public Map<Long, String> createStatsChartMap(List<CompDomainSettings> compDomainRows,
            List<KeywordSettings> keywordRows, long rankUserId, String startDate, String endDate) {
        Map<Long, String> keywordStatsMap = new HashMap<>();
        Map<Long, String[]> dataStringMap = new HashMap<>();
        Map<Long, Calendar> visitedDateMap = new HashMap<>();
        Map<Long, Integer> maxKeyRank = new HashMap<>();
        long dom_id = 0;
        for (CompDomainSettings cmp : compDomainRows) {
            if (cmp.isUserDomain()) {
                dom_id = cmp.getDomainId();
                logger.info("in charts domain id is : " + cmp.getDomainId());
            }
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d");
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate1 = null, endDate1 = null;
        try {
            startDate1 = dtf.parse(startDate);
            endDate1 = dtf.parse(endDate);
        } catch (ParseException e1) {
            logger.error(e1);
        }
        Date tempDate = new Date();

        long diff = getDateDiff(startDate1, endDate1);
        diff++;
        String formattedStartDt = dtf.format(startDate1);
        String StartDt = df.format(startDate1);
        String endDt = df.format(endDate1);
        logger.info("format StartDt : " + StartDt + " endDt: " + endDt + " diff: " + diff);
        if (keywordRows != null) {
            for (KeywordSettings kw : keywordRows) {
                keywordStatsMap.put(kw.getKeywordId(), "");
                dataStringMap.put(kw.getKeywordId(), new String[]{"", ""});
                Calendar visitedDay = Calendar.getInstance();
                visitedDay.setTime(startDate1);
                visitedDateMap.put(kw.getKeywordId(), visitedDay);
                maxKeyRank.put(kw.getKeywordId(), new Integer(0));
            }
        }

        SqlRowSet sqlRowSet = getStatsDataForChart(dom_id, rankUserId, StartDt, endDt);

        String statsXml = "";
        StringBuilder catxml = new StringBuilder();
        StringBuilder googlexml = new StringBuilder();
        StringBuilder bingxml = new StringBuilder();
        boolean containData = sqlRowSet.isBeforeFirst();
        if (containData) {
            statsXml = "<graph bgColor='FFFFF0' slantLabels ='1' rotateLabels='1' xAxisName='Date' yAxisName='Rank' hovercapbg='FFECAA' showShadow='0' hovercapborder='F47E00' "
                    + "areaBorderThickness='1' divLineDecimalPrecision='0' limitsDecimalPrecision='0' decimalPrecision='0'"
                    + " formatNumberScale='0' yAxisMinValue='0' showvalues='0' showDivLineValues='1' canvasBorderColor='C0C0C0' canvasBorderThickness='1' ";
            googlexml.append("<dataset seriesName='Google' color='#FF6600' >");
            bingxml.append("<dataset seriesname='Bing' color='#A4C639' >");
            catxml.append("<categories>");
            long rem, maxNoLabels = 7;
            if (diff < maxNoLabels) {
                maxNoLabels = diff;
            }
            rem = (long) Math.ceil((double) diff / maxNoLabels);
            try {
                tempDate = dtf.parse(formattedStartDt);
                logger.info("parse date " + tempDate);
            } catch (ParseException e) {
                logger.error(e);
            }
            for (int i = 0; i < diff - 1; i++) {
                String displayDate = sdf.format(tempDate);
                if (i % rem == 0) {
                    catxml.append("<category name='" + displayDate + "' showName= '1' />");
                } else {
                    catxml.append("<category name='" + displayDate + "' showName= '0' />");
                }
                tempDate = addDays(tempDate, 1);
            }
            String displayDate = sdf.format(tempDate);
            catxml.append("<category name='" + displayDate + "' showName= '1' />");
            catxml.append("</categories>");
            while (sqlRowSet.next()) {
                long keyId = sqlRowSet.getLong("keyword_id");
                int gRank = sqlRowSet.getInt("keyword_rank_g");
                int bRank = sqlRowSet.getInt("keyword_rank_b");
                if (gRank == -1) {
                    gRank = 101;
                }
                if (bRank == -1) {
                    bRank = 101;
                }
                if (maxKeyRank.get(keyId) < gRank) {
                    maxKeyRank.put(keyId, gRank);
                }
                if (maxKeyRank.get(keyId) < bRank) {
                    maxKeyRank.put(keyId, bRank);
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(sqlRowSet.getTimestamp("date"));
                Calendar visitedDay = visitedDateMap.get(keyId);
                String[] dataStrings = dataStringMap.get(keyId);
                boolean matched = false;
                while (visitedDay.before(calendar)) {
                    if (visitedDay.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                            && visitedDay.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                            && visitedDay.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
                        matched = true;
                        if (gRank > 100) {
                            dataStrings[0] += "<set  value=''></set>";
                        } else {
                            dataStrings[0] += "<set  value='" + gRank + "'></set>";
                        }
                        if (bRank > 100) {
                            dataStrings[1] += "<set  value=''></set>";
                        } else {
                            dataStrings[1] += "<set  value='" + bRank + "'></set>";
                        }
                    } else {
                        dataStrings[0] += "<set  value='0'></set>";
                        dataStrings[1] += "<set  value='0'></set>";
                    }
                    visitedDay.add(Calendar.DAY_OF_YEAR, 1);
                }
                if (!matched) {
                    if (gRank > 100) {
                        dataStrings[0] += "<set  value=''></set>";
                    } else {
                        dataStrings[0] += "<set  value='" + gRank + "'></set>";
                    }
                    if (bRank > 100) {
                        dataStrings[1] += "<set  value=''></set>";
                    } else {
                        dataStrings[1] += "<set  value='" + bRank + "'></set>";
                    }
                    visitedDay.add(Calendar.DAY_OF_YEAR, 1);
                }
            }
            if (keywordRows != null) {
                for (KeywordSettings kw : keywordRows) {
                    String[] dataStrings = dataStringMap.get(kw.getKeywordId());
                    String statsXmlEnd = "";
                    if (maxKeyRank.get(kw.getKeywordId()) > 100) {
                        statsXmlEnd += " yAxisMaxValue='100.000001'";
                    } else if (maxKeyRank.get(kw.getKeywordId()) < 5) {
                        statsXmlEnd += " yAxisMaxValue='5'";
                    }
                    statsXmlEnd += " caption='Daily Keyword Rank Trend (" + kw.getKeyword() + ")'>";
                    String finalxml = statsXml + statsXmlEnd + catxml.toString() + googlexml.toString()
                            + dataStrings[0] + "</dataset>" + bingxml.toString() + dataStrings[1] + "</dataset></graph>";
                    logger.info("Complete chart xml: " + finalxml);
                    keywordStatsMap.put(kw.getKeywordId(), finalxml);
                }
            }
            logger.info("chart xml is : " + statsXml);
        } else {
            keywordStatsMap.put(0l, "<graph></graph>");
        }
        return keywordStatsMap;
    }

    public Map<Long, JSONArray> fetchKeywordsChartData(List<CompDomainSettings> compDomainRows, List<KeywordSettings> sortedKeywordRows,
            long rankUserId, String startDate, String endDate) throws Exception {

        Map<Long, JSONArray> allKeywordChartData = new HashMap<>();
        Map<Long, Calendar> visitedDateMap = new HashMap<>();
        Map<Long, Integer> tempGRank = new HashMap<>();
        Map<Long, Integer> tempBRank = new HashMap<>();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat graphDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate1 = null, endDate1 = null;

        long dom_id = 0;
        for (CompDomainSettings cmp : compDomainRows) {
            if (cmp.isUserDomain()) {
                dom_id = cmp.getDomainId();
            }
        }

        try {
            startDate1 = dtf.parse(startDate);
            endDate1 = dtf.parse(endDate);
        } catch (ParseException e1) {
            logger.error("Excpetion in Date Parsing of fetchKeywordsChartData method of keyword rank service:", e1);

        }
        String startDt = df.format(startDate1);
        String endDt = df.format(endDate1);

        Timestamp campCreatedTime = fetchCampaignCreatedDate(rankUserId);
        Date campCreatedDate = new Date(campCreatedTime.getTime());
        Calendar campDatPointcal = Calendar.getInstance();
        campDatPointcal.setTime(startDate1);
        long daysDiff = getDateDiffVersion2(campCreatedDate, startDate1);

        int removeDays = (int) (daysDiff % 7);
        if (removeDays == 0) {
            startDate1 = campDatPointcal.getTime();
        } else if (removeDays > 0) {
            campDatPointcal.add(Calendar.DAY_OF_YEAR, (7 - removeDays));
            startDate1 = campDatPointcal.getTime();
        } else if (removeDays < 0) {
            removeDays = Math.abs(removeDays);
            campDatPointcal.add(Calendar.DAY_OF_YEAR, removeDays);
            startDate1 = campDatPointcal.getTime();
        }

        if (sortedKeywordRows != null) {
            for (KeywordSettings kw : sortedKeywordRows) {
                allKeywordChartData.put(kw.getKeywordId(), new JSONArray());
                tempGRank.put(kw.getKeywordId(), 0);
                tempBRank.put(kw.getKeywordId(), 0);
                Calendar tempVisitedDay = Calendar.getInstance();
                tempVisitedDay.setTime(campDatPointcal.getTime());
                visitedDateMap.put(kw.getKeywordId(), tempVisitedDay);
            }
        }
        SqlRowSet sqlRowSet = getStatsDataForChart(dom_id, rankUserId, startDt, endDt);
        boolean containData = sqlRowSet.isBeforeFirst();
        Date visitedDayEnd = null;
        if (containData) {

            while (sqlRowSet.next()) {

                long keyId = sqlRowSet.getLong("keyword_id");
                int gRank = sqlRowSet.getInt("keyword_rank_g");
                int bRank = sqlRowSet.getInt("keyword_rank_b");

                if (gRank == -1) {
                    gRank = 101;
                }
                if (bRank == -1) {
                    bRank = 101;
                }
                Date dataDate = sqlRowSet.getTimestamp("date");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dataDate);
                Calendar visitedDay = visitedDateMap.get(keyId);
                JSONArray keywordChartData = allKeywordChartData.get(keyId);

                if (visitedDay.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                        && visitedDay.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                        && visitedDay.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
                    visitedDayEnd = visitedDay.getTime();
                    JSONArray keywordChartRow = new JSONArray();
                    keywordChartRow.add(graphDateFormat.format(visitedDay.getTime()));
                    if (gRank > 100) {
                        keywordChartRow.add("");
                    } else {
                        keywordChartRow.add(gRank);
                    }
                    if (bRank > 100) {
                        keywordChartRow.add("");
                    } else {
                        keywordChartRow.add(bRank);
                    }
                    keywordChartData.add(keywordChartRow);
//	                        visitedDay.add(Calendar.DAY_OF_YEAR,1);
                    visitedDay.add(Calendar.DAY_OF_YEAR, 7);

                } else if (visitedDay.before(calendar)) {
                    while (visitedDay.before(calendar)) {
                        visitedDayEnd = visitedDay.getTime();
                        JSONArray keywordChartRow = new JSONArray();
                        int tGrank;
                        int tBrank;
                        if (visitedDay.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                                && visitedDay.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                                && visitedDay.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
                            tGrank = gRank;
                            tBrank = bRank;
                        } else {
                            tGrank = tempGRank.get(keyId);
                            tBrank = tempBRank.get(keyId);
                        }
                        keywordChartRow.add(graphDateFormat.format(visitedDay.getTime()));
                        if (tGrank > 100) {
                            keywordChartRow.add("");
                        } else {
                            keywordChartRow.add(tGrank);
                        }
                        if (tBrank > 100) {
                            keywordChartRow.add("");
                        } else {
                            keywordChartRow.add(tBrank);
                        }
                        keywordChartData.add(keywordChartRow);
//	                        visitedDay.add(Calendar.DAY_OF_YEAR, 1);
                        visitedDay.add(Calendar.DAY_OF_YEAR, 7);
                    }
                }

                tempGRank.put(keyId, gRank);
                tempBRank.put(keyId, bRank);

            }
            for (KeywordSettings kw : sortedKeywordRows) {

                Calendar visitedDayCal = Calendar.getInstance();
                visitedDayCal.setTime(visitedDayEnd);
                visitedDayCal.add(Calendar.DAY_OF_YEAR, 7);
//	                visitedDayCal.add(Calendar.DAY_OF_YEAR,1);
                Calendar endDataPoint = Calendar.getInstance();
                endDataPoint.setTime(endDate1);
                JSONArray keywordChartData = allKeywordChartData.get(kw.getKeywordId());
//	              logger.info("keyword: "+kw.getKeywordId()+"Chart Data size: "+keywordChartData.size());
//	              JSONArray lastDayRank = keywordChartData.getJSONArray(keywordChartData.size()-1);
//	              int gRank = Integer.parseInt(String.valueOf(lastDayRank.get(1)));
//	              int bRank = Integer.parseInt(String.valueOf(lastDayRank.get(2)));
                int gRank = tempGRank.get(kw.getKeywordId());
                int bRank = tempBRank.get(kw.getKeywordId());
                while (visitedDayCal.before(endDataPoint)) {
                    JSONArray keywordChartRow = new JSONArray();
                    keywordChartRow.add(graphDateFormat.format(visitedDayCal.getTime()));
                    if (gRank > 100) {
                        keywordChartRow.add("");
                    } else {
                        keywordChartRow.add(gRank);
                    }

                    if (bRank > 100) {
                        keywordChartRow.add("");
                    } else {
                        keywordChartRow.add(bRank);
                    }
                    keywordChartData.add(keywordChartRow);
                    visitedDayCal.add(Calendar.DAY_OF_YEAR, 7);
                }
            }

        }
        return allKeywordChartData;
    }

    public KeywordRankChecker getUserStartDate(long userId) {
        String query = "select start_date from rank_checker_user where user_id = ? and is_logged_in = 1";
        try {
            KeywordRankChecker keywordRankChecker = getJdbcTemplate().query(query, new Object[]{userId}, (ResultSet rs) -> {
                KeywordRankChecker krc = null;
                try {
                    if (rs.next()) {
                        krc = new KeywordRankChecker();
                        krc.setStartDate(rs.getTimestamp("start_date"));
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return krc;
            });
            return keywordRankChecker;

        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public KeywordRankChecker getUserSettingData(long userId) {
        String query = "select * from rank_checker_user where user_id = ? and is_logged_in=1";
        try {
            KeywordRankChecker keywordRankChecker = getJdbcTemplate().query(query, new Object[]{userId}, (ResultSet rs) -> {
                KeywordRankChecker krc = null;
                try {
                    if (rs.next()) {
                        krc = new KeywordRankChecker();
                        krc.setAlertStatus(rs.getBoolean("alert_status"));
                        krc.setWeeklyReport(rs.getBoolean("weekly_report"));
                        krc.setLocation(rs.getString("geographic_loc"));
                        krc.setLanguage(rs.getString("language"));
                        krc.setToolUnsubscription(rs.getBoolean("tool_unsubscription"));
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return krc;
            });
            return keywordRankChecker;

        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }

    public KeywordRankChecker getUserSettingsData(long rankUserId) {
        String query = "select  alert_status,weekly_report,tool_unsubscription from rank_checker_user where id =?";
        try {
            KeywordRankChecker keywordRankChecker = getJdbcTemplate().query(query, new Object[]{rankUserId}, (ResultSet rs) -> {
                KeywordRankChecker krc = null;
                try {
                    if (rs.next()) {
                        krc = new KeywordRankChecker();
                        krc.setAlertStatus(rs.getBoolean("alert_status"));
                        krc.setWeeklyReport(rs.getBoolean("weekly_report"));
                        krc.setToolUnsubscription(rs.getBoolean("tool_unsubscription"));
                    }
                } catch (SQLException e) {
                    logger.error(e);
                }
                return krc;
            });
            return keywordRankChecker;

        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;

    }

    public boolean compareUserSettingsData(long rankUserId, KeywordRankChecker keywordRankChecker, Timestamp updatedTime) {
        KeywordRankChecker kwd = getUserSettingsData(rankUserId);
        boolean changed = false;
        boolean toolUnsubscription = false;
        if (kwd != null) {
            if (kwd.isAlertStatus() != keywordRankChecker.isAlertStatus()) {
                changed = true;
            }
            if (kwd.isWeeklyReport() != keywordRankChecker.isWeeklyReport()) {
                changed = true;
            }
            if (kwd.isToolUnsubscription() != keywordRankChecker.isToolUnsubscription()) {
                changed = true;
                toolUnsubscription = true;
            }
            if (changed) {
                updateUserSettings(rankUserId, keywordRankChecker, updatedTime);
            }
        }
        return toolUnsubscription;

    }

    public void updateUserSettings(final long rankUserId, KeywordRankChecker keywordRankChecker, Timestamp updatedTime) {
        String unsubMsg = "";
        Timestamp toonUnsubTime = null;
        final String query = "update rank_checker_user set alert_status=?,weekly_report=?,"
                + "tool_unsubscription = ? ,tool_unsub_date =?,remark =?  where id = ?";
        if (keywordRankChecker.isToolUnsubscription()) {
            unsubMsg = "User unsubscribed";
            toonUnsubTime = updatedTime;
        }
        try {
            getJdbcTemplate().update(query, new Object[]{keywordRankChecker.isAlertStatus(),
                keywordRankChecker.isWeeklyReport(), keywordRankChecker.isToolUnsubscription(),
                toonUnsubTime, unsubMsg, rankUserId});
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public long getDateDiff(Date firstDate, Date secondDate) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(firstDate);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(secondDate);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        // calendars are the same date
        if (cal1.equals(cal2)) {
            return 0;
        }
        // cal2 is before cal1 - make recursive call
        if (cal2.before(cal1)) {
            return -getDateDiff(secondDate, firstDate);
        }
        // cal1 is before cal2 - count days between
        long days = 0;
        while (cal1.before(cal2)) {
            cal1.add(Calendar.DATE, 1);
            days++;
        }
        return days;
    }

    public Date addDays(Date d1, long days) {
        return new Date((d1.getTime() + days * 86400000));
    }

    public SqlRowSet getStatsDataForChart(long domainId, long rankUserId, String startDate, String endDate) {
        SqlRowSet keywordStatsSqlRowSet;
        String query = "select date,keyword_id,keyword_rank_g,keyword_rank_b from rank_checker_keyword_stats "
                + "where domain_id =" + domainId + "  and keyword_id in (select  key_id from rank_checker_keyword "
                + "where rank_user_id = " + rankUserId + " and is_deleted = 0) and date_format(date,'%Y-%m-%d') between '"
                + startDate + "' and '" + endDate + "' order by date";
        keywordStatsSqlRowSet = getJdbcTemplate().queryForRowSet(query);
        return keywordStatsSqlRowSet;
    }

    public void addInJson3(String statsData,
            HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            arr.add(0, statsData);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public SqlRowSet createKeywordStatsStartDate(final long rankUserId) {

        SqlRowSet sqlRowSet = null;
        String query = "select rs .* from rank_checker_keyword_stats rs inner join"
                + "(select cs.keyword_id, min(cs.stats_id) as stats_id "
                + "from rank_checker_keyword_stats cs, rank_checker_keyword kc, rank_checker_domain rd "
                + "where cs.keyword_id = kc.key_id and cs.domain_id = rd.dom_id and rd.rank_user_id = " + rankUserId
                + " and kc.is_deleted = 0 AND rd.is_deleted = 0 AND rd.is_userdomain = 1 group by cs.keyword_id) rs1 "
                + "on (rs.keyword_id=rs1.keyword_id and rs.stats_id=rs1.stats_id)";

        try {
            sqlRowSet = getJdbcTemplate().queryForRowSet(query);
        } catch (Exception e) {
            logger.error(e);
        }
        return sqlRowSet;
    }

    public SqlRowSet createKeywordStatsCurrentNSeven(final long rankUserId, String startDat, String seventhDate) {
        SqlRowSet SqlRowSetCurrSev = null;
        String query = "SELECT * FROM rank_checker_keyword_stats WHERE "
                + "keyword_id IN (SELECT key_id FROM rank_checker_keyword WHERE rank_user_id = " + rankUserId + "  AND is_deleted = 0) AND "
                + "domain_id = (SELECT dom_id FROM rank_checker_domain WHERE rank_user_id = " + rankUserId + " AND is_deleted = 0 "
                + "AND is_userdomain = 1) AND date_format(date,'%Y-%m-%d') in ('" + startDat + "', '" + seventhDate + "')";
        try {
            SqlRowSetCurrSev = getJdbcTemplate().queryForRowSet(query);
        } catch (Exception e) {
            logger.error(e);
        }
        return SqlRowSetCurrSev;
    }

    public final String createPdfFile(String targetFilePath, long rankUserId, List<CompDomainSettings> compDomainRows, List<KeywordSettings> KeySettingRows, Map<Long, List<KeywordRankCheckerStats>> keywordStatsMap, Timestamp tempStartDt) {
        logger.info("TargetFilePath in createPdfFile" + targetFilePath + "   tempStartD is   " + tempStartDt);
        try {

            Font reportNameFont = new Font(FontFactory.getFont("Arial", 14, Font.BOLD, new BaseColor(255, 110, 0)));
            Font normalFont = new Font(FontFactory.getFont("Arial", 9.8f, Font.NORMAL));
            Font normalHeadingFont = new Font(FontFactory.getFont("Arial", 9f, Font.BOLD));
            Font smallFont = new Font(FontFactory.getFont("Arial", 10.5f, Font.BOLD, new BaseColor(0, 0, 0)));
            Font keywordFont = new Font(FontFactory.getFont("Arial", 10f, Font.BOLD, new BaseColor(255, 110, 0)));
            Font websiteRevFont = new Font(FontFactory.getFont("Arial", 12f, Font.BOLD, new BaseColor(0, 0, 0)));
            String repName = "LXRMarketplace_SEO_Rank_Checker_Weekly_Report";
//            String reptName = Common.removeSpaces(repName);
            String filename = Common.createFileName(repName) + ".pdf";
            String filePath = targetFilePath + filename;
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4, 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            Map<Long, long[][]> reportMap = null;
            Timestamp startDat = null;
            Timestamp seventhDate = null;
            SimpleDateFormat sdf = null;
            String startDat1 = null;
            String seventhDate1 = null;
            SimpleDateFormat sdf1 = null;
            String tempStartDt1 = null;
            try {
                reportMap = new HashMap<>();
                startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
                seventhDate = (Timestamp) subtractDays(startDat, 7);	//seventh dt								
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                startDat1 = sdf.format(startDat);
                seventhDate1 = sdf.format(seventhDate);
                sdf1 = new SimpleDateFormat("MMMM d, yyyy");
                tempStartDt1 = sdf1.format(tempStartDt);  //base date
            } catch (Exception e) {
                logger.error("Exception in time:", e);

            }
            SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String curDate = dtf.format(startDat);
            String baseDate = dtf.format(tempStartDt);
            Date baseDt = null, curDt = null;
            Date compareDt1 = null, compareDt2 = null;
            try {
                baseDt = dtf.parse(baseDate);
                curDt = dtf.parse(curDate);
            } catch (ParseException e1) {
                logger.error("ParseException" + e1.getMessage());
            }
            long diff = getDateDiff(baseDt, curDt);
            int noOfCols = 0;
            PdfPTable multiWordTable;
            if (diff > 7) {
                noOfCols = 8;
                float[] threeWidths = {25f, 25f, 24f, 24f, 24f, 24f, 24f, 24f};
                multiWordTable = new PdfPTable(threeWidths);
            } else {
                noOfCols = 6;
                float[] threeWidths = {25f, 25f, 24f, 24f, 24f, 24f};
                multiWordTable = new PdfPTable(threeWidths);
            }
            logger.info(tempStartDt1);
            if (compDomainRows != null && compDomainRows.size() > 0) {
                if (KeySettingRows != null && KeySettingRows.size() > 0) {
                    for (CompDomainSettings cmp : compDomainRows) {
                        if (cmp.getDomain() != "" && cmp.getDomain() != null) {
                            if (cmp.isUserDomain() == true) {
                                long domainId = cmp.getDomainId();
                                List<KeywordRankCheckerStats> keywords = keywordStatsMap.get(domainId);
                                Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
                                img.scaleToFit(90f, 50f);
                                Annotation anno = new Annotation(0f, 0f, 0f, 0f, "http://www.lxrmarketplace.com");
                                img.setAnnotation(anno);
                                Phrase mainHeading = new Phrase();
                                Chunk tempReportName = new Chunk("Weekly Keyword Rank Checker Report", reportNameFont);
                                tempReportName.setLineHeight(20f);
                                Chunk normalHeading = new Chunk(Chunk.NEWLINE + "Website Reviewed : ", websiteRevFont);
                                normalHeading.setLineHeight(20f);
                                String compDomin = (String) cmp.getDomain();
                                Chunk domain = new Chunk(compDomin, websiteRevFont);
                                domain.setLineHeight(20f);
                                mainHeading.add(tempReportName);
                                mainHeading.add(normalHeading);
                                mainHeading.add(domain);
                                PdfPTable imgTable = new PdfPTable(2);
                                imgTable.setWidthPercentage(99.9f);
                                PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
                                PdfPCell rpName = new PdfPCell(mainHeading);
                                rpName.setVerticalAlignment(Element.ALIGN_LEFT);
                                logo.setBorder(Rectangle.NO_BORDER);
                                rpName.setBorder(Rectangle.NO_BORDER);
                                logo.setPaddingBottom(10f);
                                rpName.setPaddingBottom(10f);
                                logo.setPaddingLeft(30f);
                                imgTable.addCell(logo);
                                imgTable.addCell(rpName);
                                pdfDoc.add(imgTable);

                                multiWordTable.setWidthPercentage(99.9f);
                                String DmnPosition = "     Domain Position";
                                String userDomainB = DmnPosition + "\n" + "Start Date : " + tempStartDt1;
                                String userDomainC = DmnPosition + "\n" + "     This week";
                                String userDomainS = DmnPosition + "\n" + "     Last week";
                                Image img1 = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/google.png");
                                img1.scaleToFit(15f, 15f);
                                Image img4 = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/bing.jpg");
                                img4.scaleToFit(15f, 15f);

                                PdfPCell[] totalCells = new PdfPCell[noOfCols];
                                PdfPCell keyword = new PdfPCell(new Phrase("        Your  Keywords", keywordFont));
                                keyword.setRowspan(2);
                                keyword.setColspan(2);
                                keyword.setPaddingTop(65);
                                keyword.setPaddingLeft(50);
                                PdfPCell userDomai = new PdfPCell(new Phrase(userDomainB, normalHeadingFont)); //base date
                                userDomai.setColspan(2);
                                PdfPCell userDomai1 = new PdfPCell(new Phrase(userDomainC, normalHeadingFont)); //current date
                                userDomai1.setColspan(2);
                                PdfPCell userDomai2 = null;
                                if (diff > 7) {
                                    userDomai2 = new PdfPCell(new Phrase(userDomainS, normalHeadingFont));//seventh date
                                    userDomai2.setColspan(2);
                                }

                                float[] twoWidths = {30f, 70f};
                                PdfPTable imgText = new PdfPTable(twoWidths);
                                Phrase text1 = new Phrase("Google", smallFont);
                                PdfPCell img2 = new PdfPCell(img1);
                                PdfPCell text = new PdfPCell(text1);
                                img2.setVerticalAlignment(Element.ALIGN_CENTER);
                                text.setVerticalAlignment(Element.ALIGN_RIGHT);
                                text.setPaddingBottom(10f);
                                img2.setPaddingTop(3f);
                                img2.setPaddingRight(0f);
                                img2.setPaddingLeft(0f);
                                img2.setBorder(Rectangle.NO_BORDER);
                                text.setBorder(Rectangle.NO_BORDER);
                                imgText.addCell(img2);
                                imgText.addCell(text);

                                PdfPTable imgText1 = new PdfPTable(twoWidths);
                                Phrase text2 = new Phrase("Bing", smallFont);
                                PdfPCell img3 = new PdfPCell(img4);
                                PdfPCell text3 = new PdfPCell(text2);
                                img3.setVerticalAlignment(Element.ALIGN_CENTER);
                                text3.setVerticalAlignment(Element.ALIGN_RIGHT);
                                text.setPaddingBottom(10f);
                                img3.setPaddingTop(3f);
                                img3.setPaddingRight(0f);
                                img3.setPaddingLeft(0f);
                                img3.setBorder(Rectangle.NO_BORDER);
                                text3.setBorder(Rectangle.NO_BORDER);
                                imgText1.addCell(img3);
                                imgText1.addCell(text3);
                                boolean kValIsNotThree = true;
                                for (int k = 0; k < 4; k++) {

                                    switch (k) {
                                        case 0:
                                            totalCells[k] = keyword;
                                            break;
                                        case 1:
                                            totalCells[k] = userDomai;
                                            break;
                                        case 2:
                                            totalCells[k] = userDomai1;
                                            break;
                                        case 3:
                                            kValIsNotThree = false;
                                            if (diff > 7) {
                                                kValIsNotThree = true;
                                                totalCells[k] = userDomai2;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    if (kValIsNotThree) {
                                        totalCells[k]
                                                .setBorderColor(BaseColor.BLACK);
                                        totalCells[k].setBorderWidth(0.2f);//									
                                        totalCells[k].setPaddingLeft(10f);
                                        totalCells[k].setPaddingRight(3f);
                                        totalCells[k].setPaddingBottom(4);
                                        totalCells[k].setPaddingTop(8);
                                        totalCells[k].setSpaceCharRatio(6f);
                                        multiWordTable.addCell(totalCells[k]);

                                    }
                                }
                                multiWordTable.completeRow();
                                multiWordTable.addCell(imgText);
                                multiWordTable.addCell(imgText1);
                                multiWordTable.addCell(imgText);
                                multiWordTable.addCell(imgText1);
                                if (diff > 7) {
                                    multiWordTable.addCell(imgText);
                                    multiWordTable.addCell(imgText1);
                                }
                                SqlRowSet baseSqlRowSet;
                                SqlRowSet SqlRowSetCurrSev;
                                try {
                                    baseSqlRowSet = createKeywordStatsStartDate(rankUserId);
                                    SqlRowSetCurrSev = createKeywordStatsCurrentNSeven(rankUserId, startDat1, seventhDate1);
                                    if (baseSqlRowSet != null) {
                                        while (baseSqlRowSet.next()) {

                                            long keywordid = baseSqlRowSet.getLong("keyword_id");
                                            long basGRank = baseSqlRowSet.getLong("keyword_rank_g");
                                            long basBRank = baseSqlRowSet.getLong("keyword_rank_b");
                                            Date t = baseSqlRowSet.getTimestamp("date");
                                            String baseDe = sdf.format(t);
                                            String baseDte = sdf.format(baseDt);
                                            compareDt1 = sdf.parse(baseDe);
                                            compareDt2 = sdf.parse(baseDte);
                                            long reportranks[][] = new long[3][3];
                                            if (compareDt2.equals(compareDt1)) {
                                                reportranks[0][0] = basGRank;
                                                reportranks[0][1] = basBRank;
                                            } else {
                                                reportranks[0][0] = 0;
                                                reportranks[0][1] = 0;
                                            }
                                            reportMap.put(keywordid, reportranks);

                                            PdfPCell keywordName = new PdfPCell(new Phrase("keyword"));
                                            keywordName.setColspan(2);
                                        }
                                    }

                                    if (SqlRowSetCurrSev != null) {
                                        while (SqlRowSetCurrSev.next()) {
                                            Timestamp date = SqlRowSetCurrSev.getTimestamp("date");
                                            String date1 = sdf.format(date);
                                            if (date1.equals(startDat1)) {
                                                long keywordid = SqlRowSetCurrSev.getLong("keyword_id");
                                                long reportranks[][] = reportMap.get(keywordid);
                                                long curGRank = SqlRowSetCurrSev.getLong("keyword_rank_g");
                                                long curBRank = SqlRowSetCurrSev.getLong("keyword_rank_b");

                                                reportranks[2][0] = curGRank;
                                                reportranks[2][1] = curBRank;
                                            } else if (date1.equals(seventhDate1)) {

                                                long keywordid = SqlRowSetCurrSev.getLong("keyword_id");
                                                long reportranks[][] = reportMap.get(keywordid);
                                                long seventGRank = SqlRowSetCurrSev.getLong("keyword_rank_g");
                                                long seventBRank = SqlRowSetCurrSev.getLong("keyword_rank_b");

                                                reportranks[1][0] = seventGRank;
                                                reportranks[1][1] = seventBRank;
                                            }
                                        }
                                    }

                                    for (KeywordSettings kw : KeySettingRows) {
                                        long[][] rankValues = reportMap.get(kw.getKeywordId());
                                        if (rankValues != null) {
                                            String keywordname = kw.getKeyword();
                                            Long bGRank = rankValues[0][0];
                                            Long bBRank = rankValues[0][1];
                                            Long cGRank = rankValues[2][0];
                                            Long cBRank = rankValues[2][1];
                                            Long sGRank = rankValues[1][0];
                                            Long sBRank = rankValues[1][1];
                                            String gVal = "";

                                            PdfPCell keywordname1 = new PdfPCell(new Phrase(keywordname, normalFont));
                                            keywordname1.setColspan(2);

                                            PdfPCell bGRank1 = new PdfPCell(new Phrase("" + bGRank, normalFont));
                                            PdfPCell bBRank1 = new PdfPCell(new Phrase("" + bBRank, normalFont));
                                            PdfPCell cGRank1 = new PdfPCell(new Phrase("" + cGRank, normalFont));
                                            PdfPCell cBRank1 = new PdfPCell(new Phrase("" + cBRank, normalFont));
                                            PdfPCell sGRank1 = new PdfPCell(new Phrase("" + sGRank, normalFont));
                                            PdfPCell sBRank1 = new PdfPCell(new Phrase("" + sBRank, normalFont));

                                            if (bGRank == -1) {
                                                gVal = ">100";
                                                bGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (bBRank == -1) {
                                                gVal = ">100";
                                                bBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (cGRank == -1) {
                                                gVal = ">100";
                                                cGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (cBRank == -1) {
                                                gVal = ">100";
                                                cBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (sGRank == -1) {
                                                gVal = ">100";
                                                sGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (sBRank == -1) {
                                                gVal = ">100";
                                                sBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (bGRank == 0) {
                                                gVal = "-";
                                                bGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (bBRank == 0) {
                                                gVal = "-";
                                                bBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (cGRank == 0) {
                                                gVal = "-";
                                                cGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (cBRank == 0) {
                                                gVal = "-";
                                                cBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (sGRank == 0) {
                                                gVal = "-";
                                                sGRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }
                                            if (sBRank == 0) {
                                                gVal = "-";
                                                sBRank1 = new PdfPCell(new Phrase(gVal, normalFont));
                                            }

                                            PdfPCell[] totalValues = new PdfPCell[7];

                                            for (int k = 0; k < 7; k++) {
                                                boolean sevDayData = true;
                                                switch (k) {
                                                    case 0:
                                                        totalValues[k] = keywordname1;
                                                        break;
                                                    case 1:
                                                        totalValues[k] = bGRank1;
                                                        break;
                                                    case 2:
                                                        totalValues[k] = bBRank1;
                                                        break;
                                                    case 3:
                                                        totalValues[k] = cGRank1;
                                                        break;
                                                    case 4:
                                                        totalValues[k] = cBRank1;
                                                        break;
                                                    default:
                                                        sevDayData = false;
                                                        if (diff > 7) {
                                                            sevDayData = true;
                                                            if (k == 5) {
                                                                totalValues[k] = sGRank1;
                                                            } else if (k == 6) {
                                                                totalValues[k] = sBRank1;
                                                            }
                                                        }
                                                        break;
                                                }

                                                if (sevDayData) {
                                                    totalValues[k]
                                                            .setBorderColor(BaseColor.BLACK);
                                                    totalValues[k].setBorderWidth(0.2f);
                                                    totalValues[k].setPaddingLeft(10);
                                                    totalValues[k].setPaddingRight(2);
                                                    totalValues[k].setPaddingLeft(2);
                                                    totalValues[k].setPadding(13);
                                                    totalValues[k].setVerticalAlignment(Element.ALIGN_JUSTIFIED);
                                                    totalValues[k].setSpaceCharRatio(6f);
                                                    multiWordTable.addCell(totalValues[k]);
                                                }
                                            }
                                            multiWordTable.completeRow();

                                        }

                                    }
                                    multiWordTable.completeRow();
                                    pdfDoc.add(multiWordTable);

                                } catch (InvalidResultSetAccessException | ParseException | DocumentException e) {
                                    logger.error(e);

                                }
                            }
                        }
                    }

                    pdfDoc.close();
                    logger.info("File name  in pdf creation:" + filename);
                    return filename;
                }
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (DocumentException | IOException de) {
            logger.error(de);
        }
        return null;
    }

    public String addWaterMark(String fileName, String downloadFolder) {
        try {

            PdfReader Read_PDF_To_Watermark = new PdfReader(downloadFolder + fileName);
            fileName = Common.createFileName("LXRMarketplace_SEO_Rank_Checker_Report");
            fileName = fileName + ".pdf";
            PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, new FileOutputStream("/disk2/lxrmarketplace/download/" + fileName + ""));
            PdfContentByte underContent1 = stamp.getUnderContent(1);

            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            PdfGState gs = new PdfGState();
            gs.setFillOpacity(0.8f);
            underContent1.beginText();
            underContent1.setGState(gs);
            underContent1.setFontAndSize(bf, 40);
            underContent1.setRGBColorFill(224, 224, 224);
            underContent1.showTextAligned(Element.ALIGN_CENTER, "https://lxrmarketplace.com", 330, 650, 0);
            underContent1.endText();
            stamp.close();
        } catch (IOException | DocumentException i1) {
            logger.error(i1);
        }

        logger.info("fileName: " + fileName);
        return fileName;
    }

    public Timestamp subtractDays(Timestamp d1, long days) {
        return new Timestamp((d1.getTime() - days * 86400000));
    }

    public void jsonKeywordChartData(JSONArray keywordChartData, HttpServletResponse response) {
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            writer.print(keywordChartData);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public long getDateDiffVersion2(Date firstDate, Date secondDate) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(firstDate);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(secondDate);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        // calendars are the same date
        if (cal1.equals(cal2)) {
            return 0;
        }
        // cal2 is before cal1 - make recursive call
        if (cal2.before(cal1)) {
            return -getDateDiffVersion2(secondDate, firstDate);
        }
        // cal1 is before cal2 - count days between
        long days = 0;
        while (cal1.before(cal2)) {
            cal1.add(Calendar.DATE, 1);
            days++;
        }
        logger.info("days : " + days);
        return days;
    }

    public Timestamp fetchCampaignCreatedDate(long campId) {
        String query = "select start_date from rank_checker_user where id = ?";
        java.sql.Timestamp campCreatedDate = null;
        try {
            campCreatedDate = (Timestamp) getJdbcTemplate().queryForObject(query, new Object[]{campId}, Timestamp.class);
        } catch (Exception e) {
            logger.error(e);
        }

        return campCreatedDate;
    }

    public boolean checkKeywordDataFetchStatus(long domainId, long campId, Calendar cal) {
        boolean present = false;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String formattedDate = df.format(cal.getTime());
            if (formattedDate != null) {
                String query = "select count(*) from rank_checker_keyword_stats where domain_id = " + domainId
                        + " and date_format(date,'%Y-%m-%d') = date_format('" + formattedDate + "','%Y-%m-%d') "
                        + "and keyword_id in (select key_id from rank_checker_keyword where rank_user_id = " + campId
                        + " and  is_deleted = 0)";
                int rowCount = getJdbcTemplate().queryForInt(query);
                if (rowCount > 0) {
                    present = true;
                }
            }
        } catch (Exception e) {
            logger.error("Error in checking keyword fetching stats!! " + e);
        }
        return present;
    }

    public List<KeywordSettings> createKeywordRowsforScheduler(long rankUserId, long domainId) {

        String weeklyQuery = " select key_id,keyword,alert_operator,alert_value,google_se, bing_se "
                + " from rank_checker_keyword  where rank_user_id =  ? and "
                + " ((SELECT MOD((select datediff(?,(select start_date from rank_checker_user where id = ?))), 7)) = 0 "
                + " OR "
                + " (SELECT DATEDIFF(?,(select date(added_date) from rank_checker_domain where dom_Id =? ))= 1) and is_deleted = 0 )"
                + " UNION "
                + " select key_id,keyword,alert_operator,alert_value,google_se, bing_se from rank_checker_keyword "
                + " where rank_user_id =? and is_deleted =0 "
                + " and DATE(added_date) = SUBDATE(?,1)";

        logger.info("In side scheduler for createKeywordRows " + rankUserId + "weekly query is " + weeklyQuery);
        try {
            SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
            String date = dtf.format(Calendar.getInstance().getTimeInMillis());

            List<KeywordSettings> keywordSettings = getJdbcTemplate().query(weeklyQuery, new Object[]{rankUserId, date, rankUserId, date, domainId, rankUserId, date}, (ResultSet rs, int rowNum) -> {
                KeywordSettings keywordSetting = new KeywordSettings();
                keywordSetting.setKeywordId(rs.getLong("key_id"));
                keywordSetting.setKeyword(rs.getString("keyword"));
                keywordSetting.setAlertOperator(rs.getInt("alert_operator"));
                keywordSetting.setAlertValue(rs.getString("alert_value"));
                keywordSetting.setGoogleCheckbox(rs.getBoolean("google_se"));
                keywordSetting.setBingCheckbox(rs.getBoolean("bing_se"));
                return keywordSetting;
            });
            return keywordSettings;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }
}
