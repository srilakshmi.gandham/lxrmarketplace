package lxr.marketplace.admin.knowledgebase;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.util.Common;


import lxr.marketplace.util.Tool;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;


@Service
public class KnowledgeBaseService {
    
    private static final Logger logger = Logger.getLogger(KnowledgeBaseService.class);
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    /*To Select all uptodate questions from Table:lxrm_knowledgebase_questions */
    public List<KnowledgeBase> getKnowledgeBaseQuestions(){
          List<KnowledgeBase> knowledgeBaseQuestions = null;
          try {
               String select_Questions_Query = "select Q.* from lxrm_knowledgebase_questions Q";
		knowledgeBaseQuestions = this.jdbcTemplate.query(select_Questions_Query,new RowMapper<KnowledgeBase>() {
                @Override
                public KnowledgeBase mapRow(ResultSet rs, int i) throws SQLException {
                                KnowledgeBase knowledgeBase = new KnowledgeBase();
                                knowledgeBase.setQuestionID(rs.getInt("question_id"));
                                knowledgeBase.setQuestionType(rs.getShort("question_type"));
                                knowledgeBase.setActiveType(rs.getShort("active"));
                                if(rs.getDate("last_updated") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("last_updated")));    
                                }else if(rs.getDate("added_date") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("added_date")));
                                }
                                knowledgeBase.setQuestion(rs.getString("question").trim());
                                String answer;
                                try{
                                    answer =new String(rs.getBytes("answer"));
                                }catch(Exception e){
                                    answer =rs.getString("answer");
                                    logger.error("Exception converting bytes into string "+e.getMessage());
                                }
                                knowledgeBase.setAnswer(answer.trim());
                                knowledgeBase.setShortAnswer(getShortAnswer(answer.trim()));
                    return knowledgeBase;
                    }
                });
               return knowledgeBaseQuestions;
        } catch (Exception e) {
            logger.error("Exception in fetching getKnowledgeBaseQuestions:" + e);
        }
        return null;
    }
    /*Inserting toolIDS for each  KnowledgeBase Question by using Table:lxrm_knowledgebase_questions primarykey 
    in Table:lxrm_knowledgebase_questions_tools_map*/
     public  boolean insertLXRMToolsIdsForQuestionId(final KnowledgeBase knowledgeBase,int requestType){
        try{
            
            final String tool_Mapping_Query="insert into lxrm_knowledgebase_questions_tools_map(tool_id,question_id) values(?,?)";
            logger.info("tool_Mapping_Query is "+tool_Mapping_Query);
            final List<Long>  toolIds;
            toolIds = knowledgeBase.getMultipleTooldID();
            
            /*Inserting question into lxrm_knowledgebase_questions  table.*/
            final long lxrm_question_id;
            if(requestType == 1){
            lxrm_question_id = (long)addKnowledgeBaseQuestion(knowledgeBase);
            logger.info("In method of insertLXRMToolsIdsForQuestionId to add new question id is "+lxrm_question_id);
            }else if(requestType == 2){
            lxrm_question_id = knowledgeBase.getQuestionID();
            logger.info("In metod of insertLXRMToolsIdsForQuestionId, to update the tools list id's of question content and question id is "+knowledgeBase.getQuestionID());
            }else{
                lxrm_question_id = 0;    
            }
            if( lxrm_question_id != 0){
                    synchronized (this) {
                     this.jdbcTemplate.batchUpdate(tool_Mapping_Query, new BatchPreparedStatementSetter() {
                                    @Override
                                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                                        long toolID=((long)toolIds.get(i));
                                        statement.setLong(1,toolID);
                                        statement.setLong(2,lxrm_question_id);
                                }
                                    @Override
                                public int getBatchSize() {
                                        return toolIds.size();
                                    }
                                });
                    }// End of synchronized
                    return true;
            }else{
                logger.info("Question is not added/updated to knoledge Base");
                return false;
            }
        }catch(Exception e){
            logger.error("Exception in insertKnowledgeBaseRowofLXRMTools"+e.getMessage());
        }
        return false;
    }
    /*To add a question in Table:lxrm_knowledgebase_questions */
    public Long addKnowledgeBaseQuestion(final KnowledgeBase knowledgeBase) {
                Timestamp question_Added_Date = new Timestamp(Calendar.getInstance().getTimeInMillis());
		final String insertQuery = "insert into lxrm_knowledgebase_questions(question,answer,question_type,active,added_date) values (?,?,?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			synchronized (this) {
				 this.jdbcTemplate.update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(
							java.sql.Connection con) throws SQLException {
						PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
								insertQuery,new String[] { "id" });
                                                            if (knowledgeBase.getQuestion()== null || knowledgeBase.getQuestion().equals("")) {
                                                                        statement.setString(1, "");
                                                            } else {
                                                                try {
                                                                    statement.setBytes(1,knowledgeBase.getQuestion().trim().getBytes("UTF-8"));
                                                                } catch (UnsupportedEncodingException e) {
                                                                    statement.setString(1,knowledgeBase.getQuestion());
                                                                    logger.error("Exception on inserting Knowledge Base Question:" + e.getMessage());
                                                                }
                                                            }

                                                            if (knowledgeBase.getAnswer()== null || knowledgeBase.getAnswer().equals("")) {
                                                                statement.setString(2,"");
                                                            } else {
                                                                try {
                                                                    statement.setBytes(2,knowledgeBase.getAnswer().trim().getBytes("UTF-8"));
                                                                } catch (UnsupportedEncodingException e) {
                                                                    statement.setString(2,knowledgeBase.getAnswer());
                                                                    logger.error("Exception on inserting Knowledge Base Answer : " + e.getMessage());
                                                                }
                                                            }
                                                            
                                                            statement.setShort(3,knowledgeBase.getQuestionType());
                                                            statement.setShort(4,knowledgeBase.getActiveType());
                                                            statement.setTimestamp(5,question_Added_Date);
						return statement;
					}
				}, keyHolder);
			}
		} catch (Exception ex) {
			logger.error("Exception in addKnowledgeBaseQuestion"+ex.getMessage());	
		}
                
		return (Long) keyHolder.getKey();
	}
    
    /*To delete a question from Table:lxrm_knowledgebase_questions*/
    public void deleteKnowledgeBaseQuestions(final long[] selectedQuestionsIds){
        final String updateInactiveStatus = "update lxrm_knowledgebase_questions set active = ?,last_updated = ? where question_id = ?";
        Timestamp last_Updated_Date = new Timestamp(Calendar.getInstance().getTimeInMillis());
        logger.info("deleteKnowledgeBaseQuestions query :" + updateInactiveStatus);
        try{
                this.jdbcTemplate.batchUpdate(updateInactiveStatus, new BatchPreparedStatementSetter() {
                            @Override
                            public void setValues(PreparedStatement statement, int i) throws SQLException {
                                statement.setInt(1,2);
                                statement.setTimestamp(2,last_Updated_Date);
                                statement.setLong(3,selectedQuestionsIds[i]);
                            }
                            @Override
                        public int getBatchSize() {
                            logger.info("The Count of total questions updated  the inactive status in lxrm_knowledgebase_question"+selectedQuestionsIds.length);
                            return selectedQuestionsIds.length;
                            }
                        });
            }catch(Exception e){
            logger.error("Exception in deletKnowledgeBaseQuestion"+e.getMessage());    
            logger.info("Making inactive of knowledgeBase question list is failed");
            }
    }
    /*Delete the tooldIDS list based on selected questionsIDS in jsp from Table:lxrm_knowledgebase_questions_tools_map*/
    public void deleteToolIDSList(final long[] selectedQuestionsIds){
          final String deletedToolIdsQuery = "delete from lxrm_knowledgebase_questions_tools_map where question_id = ?";
                logger.info("deletedToolIdsQuery of  lxrm_knowledgebase_questions_tools_map :" + deletedToolIdsQuery);
                try{
                        this.jdbcTemplate.batchUpdate(deletedToolIdsQuery, new BatchPreparedStatementSetter() {
                                    @Override
                                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                                        statement.setLong(1,selectedQuestionsIds[i]);
                                    }
                                    @Override
                                public int getBatchSize() {
                                        return selectedQuestionsIds.length;
                                    }
                                });
                    }catch(Exception e){
                    logger.error("Exception in deleteTooldISList"+e.getMessage());    
                    logger.info("Request -->updateKnowledgeCont, updation the tools list id's of question content is failed");
                    }
                logger.info("Request -->updateKnowledgeCont, updation the tools list id's of question content is successfully");
    }
    
    /*Fetch a list of questions based on selected toolID*/
    public List<KnowledgeBase> selectQuestionsBasedOnToolID(final long selectedToolID){
        List<KnowledgeBase> knowledgeBaseList = null;
        try{
            String select_Questions_Query =null;
                        if(selectedToolID == 0){
                            select_Questions_Query = "select Q.* from lxrm_knowledgebase_questions Q";
                        }else{
                            select_Questions_Query = "SELECT Q.* from lxrm_knowledgebase_questions_tools_map QM,lxrm_knowledgebase_questions Q "
                                                                + "where QM.question_id = Q.question_id and QM.tool_id ="+selectedToolID+"";
                        }
            
            
		knowledgeBaseList = this.jdbcTemplate.query(select_Questions_Query,new RowMapper<KnowledgeBase>() {
                @Override
                public KnowledgeBase mapRow(ResultSet rs, int i) throws SQLException {
                                KnowledgeBase knowledgeBase = new KnowledgeBase();
                                knowledgeBase.setQuestionID(rs.getInt("question_id"));
                                knowledgeBase.setQuestionType(rs.getShort("question_type"));
                                knowledgeBase.setActiveType(rs.getShort("active"));
                                if(rs.getDate("last_updated") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("last_updated")));    
                                }else if(rs.getDate("added_date") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("added_date")));
                                }
                                knowledgeBase.setQuestion(rs.getString("question").trim());
                                String answer;
                                try{
                                    answer =new String(rs.getBytes("answer"));
                                }catch(Exception e){
                                    answer =rs.getString("answer");
                                    logger.error("Exception converting bytes into string "+e.getMessage());
                                }
                                knowledgeBase.setAnswer(answer.trim());
                                knowledgeBase.setShortAnswer(getShortAnswer(answer.trim()));
                    return knowledgeBase;
                    }
                });
        }catch(Exception e){
            logger.error("Exception in selectQuestionsBasedOnToolID"+e.getMessage());
        }
        return knowledgeBaseList;
    }
    /*Select question content based on selected questionID*/
    public List<KnowledgeBase> selectQuestionContentToModify(final int selectedQuestionID){
        List<KnowledgeBase> knowledgeBaseList = null;
        try{
            String select_Questions_Query =null;
                select_Questions_Query ="SELECT QM.tool_id,Q.* from lxrm_knowledgebase_questions_tools_map QM,lxrm_knowledgebase_questions Q "
                                                    + "where QM.question_id = Q.question_id and  Q.question_id ="+selectedQuestionID+" ";
		knowledgeBaseList = this.jdbcTemplate.query(select_Questions_Query,new RowMapper<KnowledgeBase>() {
                @Override
                public KnowledgeBase mapRow(ResultSet rs, int i) throws SQLException {
                                KnowledgeBase knowledgeBase = new KnowledgeBase();
                                knowledgeBase.setQuestionID(rs.getInt("question_id"));
                                knowledgeBase.setQuestionType(rs.getShort("question_type"));
                                knowledgeBase.setActiveType(rs.getShort("active"));
                                if(rs.getDate("last_updated") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("last_updated")));    
                                }else if(rs.getDate("added_date") != null){
                                    knowledgeBase.setLast_Updated(convertTimeStampintoDate(rs.getTimestamp("added_date")));
                                }
                                knowledgeBase.setQuestion(rs.getString("question").trim());
                                String answer;
                                try{
                                    answer =new String(rs.getBytes("answer"));
                                }catch(Exception e){
                                    answer =rs.getString("answer");
                                    logger.error("Exception converting bytes into string "+e.getMessage());
                                }
                                knowledgeBase.setAnswer(answer.trim());
                                knowledgeBase.setShortAnswer(getShortAnswer(answer.trim()));
                                knowledgeBase.setTooldID(rs.getLong("tool_id"));
                    return knowledgeBase;
                    }
                });
        }catch(Exception e){
            logger.error("Exception in selectQuestionsBasedOnToolID"+e.getMessage());
        }
        return knowledgeBaseList;
    }
    
    /*Update the content in question content in */
    public boolean updateQuestionContent(final KnowledgeBase updatedKnowledgeBase){
        
        Timestamp updateddate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        final String updateQuestionQuery="update lxrm_knowledgebase_questions set question = ?,answer = ?,question_type = ?,active = ?,last_updated = ? where question_id = ?";
        logger.info("In updateKnowledgeBaseRow query :" + updateQuestionQuery);
        try{
                synchronized (this) {
				 this.jdbcTemplate.update(new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(
							java.sql.Connection con) throws SQLException {
                                            PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
								updateQuestionQuery,new String[] { "id" });
                                                            if (updatedKnowledgeBase.getQuestion()== null || updatedKnowledgeBase.getQuestion().equals("")) {
                                                                        statement.setString(1, "");
                                                            } else {
                                                                try {
                                                                    statement.setBytes(1,updatedKnowledgeBase.getQuestion().trim().getBytes("UTF-8"));
                                                                } catch (UnsupportedEncodingException e) {
                                                                    statement.setString(1,updatedKnowledgeBase.getQuestion());
                                                                    logger.error("Exception on inserting Knowledge Base Question while updating :" + e.getMessage());
                                                                }
                                                            }

                                                            if (updatedKnowledgeBase.getAnswer()== null || updatedKnowledgeBase.getAnswer().equals("")) {
                                                                statement.setString(2,"");
                                                            } else {
                                                                try {
                                                                    statement.setBytes(2,updatedKnowledgeBase.getAnswer().trim().getBytes("UTF-8"));
                                                                } catch (UnsupportedEncodingException e) {
                                                                    statement.setString(2,updatedKnowledgeBase.getAnswer());
                                                                    logger.error("Exception on inserting Knowledge Base Answer while updating : " + e.getMessage());
                                                                }
                                                            }
                                                            
                                statement.setShort(3, updatedKnowledgeBase.getQuestionType());
                                statement.setShort(4, updatedKnowledgeBase.getActiveType());
                                statement.setTimestamp(5, updateddate);
                                statement.setLong(6,updatedKnowledgeBase.getQuestionID());
                                return statement;
                        }
                    });
                }
                logger.info("Updating question content is sucessfully");
                return true;
            }catch(Exception e){
            logger.error("Exception in updateing QuestionContent"+e.getMessage());    
            }
        return false;
    }
    /*Get uptodate list of tools from Table:tool */
    public List<Tool> getToolsOfLXRMarketplace(){
         List<Tool> lxrMarketplaceToolsList = null;
        try {
            String query = "select * from tool  where is_deleted = 0 and id != 32 and show_in_home = 1 order by name";
		lxrMarketplaceToolsList = this.jdbcTemplate.query(query,new RowMapper<Tool>() {
                @Override
                public Tool mapRow(ResultSet rs, int i) throws SQLException {
                                Tool lxrMarketplaceTool = new Tool();
                                lxrMarketplaceTool.setToolId(rs.getInt("id"));
                                lxrMarketplaceTool.setToolName(rs.getString("name"));
                    return lxrMarketplaceTool;
                    }
                });
        } catch (Exception e) {
            logger.error("Exception in getToolsOfLXRMarketplace:"+e.getMessage());
        }
        return lxrMarketplaceToolsList;
    }
     
     public Date convertTimeStampintoDate(Timestamp timestamp){
                    Date currentDate=null;
                                              try{
                                                //long milliSeconds=timestamp.getTime();
                                                 currentDate = new Date(timestamp.getTime());
                                                }catch(Exception e){
                                                    logger.error("Exception in convertTimeStampintoDate is"+e.getMessage());
                                                }
                    return currentDate;
    }
     
     public String getShortAnswer(String knowledgeAnswer){
         String shortAnswer;
         try{
             String htmlFilteredAnswer = Common.convertHtmlToText(knowledgeAnswer);
             if(htmlFilteredAnswer.length() >= 251){
                    shortAnswer = htmlFilteredAnswer.substring(0,250)+"...";
             }else{
                 shortAnswer = htmlFilteredAnswer;
             }
            return shortAnswer;               
         }catch(Exception e){
             shortAnswer = "";
             logger.error("Exception in getShortAnswer",e);
         }
         return shortAnswer;
     }
}



