/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.knowledgebase;

import java.util.Comparator;
import java.sql.Date;
import java.util.List;


public class KnowledgeBase implements Comparable<KnowledgeBase>{
     
private  String question;
private  String answer;
private  int questionID;
private  List<Long> multipleTooldID;
private  short  questionType;
private  short activeType;
private  Date  last_Updated;
private long userSelectedToolID;
//private int userSelectedType;
//private int userSelectedActive;
//private  long[] selected_question;
private long tooldID;
private List<Long> tooldIDS;

private String shortAnswer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public Date getLast_Updated() {
        return last_Updated;
    }

    public void setLast_Updated(Date last_Updated) {
        this.last_Updated = last_Updated;
    }


   

    public List<Long> getMultipleTooldID() {
        return multipleTooldID;
    }

    public void setMultipleTooldID(List<Long> multipleTooldID) {
        this.multipleTooldID = multipleTooldID;
    }

   

    public long getTooldID() {
        return tooldID;
    }

    public void setTooldID(long tooldID) {
        this.tooldID = tooldID;
    }

    public short getQuestionType() {
        return questionType;
    }

    public void setQuestionType(short questionType) {
        this.questionType = questionType;
    }

    public short getActiveType() {
        return activeType;
    }

    public void setActiveType(short activeType) {
        this.activeType = activeType;
    }


    public List<Long> getTooldIDS() {
        return tooldIDS;
    }

    public void setTooldIDS(List<Long> tooldIDS) {
        this.tooldIDS = tooldIDS;
    }

    public long getUserSelectedToolID() {
        return userSelectedToolID;
    }

    public void setUserSelectedToolID(long userSelectedToolID) {
        this.userSelectedToolID = userSelectedToolID;
    }

    public String getShortAnswer() {
        return shortAnswer;
    }

    public void setShortAnswer(String shortAnswer) {
        this.shortAnswer = shortAnswer;
    }

    
    
public static Comparator<KnowledgeBase> questionComparatorU = (KnowledgeBase question1, KnowledgeBase question2) -> {
    if(question1.getQuestion()== null){
        question1.setQuestion("");
    }
    if(question2.getQuestion() == null){
        question2.setQuestion("");
    }
    return question1.getQuestion().compareTo(question2.getQuestion());
};

public static Comparator<KnowledgeBase> questionComparatorD = (KnowledgeBase question1, KnowledgeBase question2) -> {
    if(question1.getQuestion()== null){
        question1.setQuestion("");
    }
    if(question2.getQuestion() == null){
        question2.setQuestion("");
    }
    return question2.getQuestion().compareTo(question1.getQuestion());
};

public static Comparator<KnowledgeBase> answerComparatorD = (KnowledgeBase question1, KnowledgeBase question2) -> {
    if(question1.getAnswer()== null){
        question1.setAnswer("");
    }
    if(question2.getAnswer()== null){
        question2.setAnswer("");
    }
    return question2.getAnswer().compareTo(question1.getAnswer());
};

public static Comparator<KnowledgeBase> answerComparatorU = (KnowledgeBase question1, KnowledgeBase question2) -> {
    if(question1.getAnswer()== null){
        question1.setAnswer("");
    }
    if(question2.getAnswer()== null){
        question2.setAnswer("");
    }
    return question1.getAnswer().compareTo(question2.getAnswer());
};

public static Comparator<KnowledgeBase> upDateComparatorU = (KnowledgeBase question1, KnowledgeBase question2) -> {
    int result = 0;
    try{
        if((question1.getLast_Updated() == null) && (question2.getLast_Updated() == null)){
            result = 0;
        }
        if((question1.getLast_Updated() == null) && (question2.getLast_Updated() != null)){
            result = -1;
        }
        if((question2.getLast_Updated() == null) && (question1.getLast_Updated() != null)){
            result = 1;
        }
        return question1.getLast_Updated().compareTo(question2.getLast_Updated());
    }catch(Exception e){
    }
    return result;
};


public static Comparator<KnowledgeBase> upDateComparatorD = (KnowledgeBase question1, KnowledgeBase question2) -> {
  int result = 0;
    try{
        if((question1.getLast_Updated() == null) && (question2.getLast_Updated() == null)){
            result = 0;
        }
        if((question1.getLast_Updated() == null) && (question2.getLast_Updated() != null)){
            result = 1;
        }
        if((question2.getLast_Updated() == null) && (question1.getLast_Updated() != null)){
            result = -1;
        }
        return question2.getLast_Updated().compareTo(question1.getLast_Updated());
    }catch(Exception e){
    }
    return result;
};


public static Comparator<KnowledgeBase> questionTypeComparatorU = (KnowledgeBase question1, KnowledgeBase question2) -> {
				return question1.getQuestionType()- question2.getQuestionType();
};
public static Comparator<KnowledgeBase> questionTypeComparatorD = (KnowledgeBase question1, KnowledgeBase question2) -> {
                    return question2.getQuestionType()- question1.getQuestionType();
};

public static Comparator<KnowledgeBase> questionActiveComparatorU = (KnowledgeBase question1, KnowledgeBase question2) -> {
    return question1.getActiveType()- question2.getActiveType();
};
public static Comparator<KnowledgeBase> questionActiveComparatorD = (KnowledgeBase question1, KnowledgeBase question2) -> {
        return question2.getActiveType()- question1.getActiveType();
        };



    @Override
    public int compareTo(KnowledgeBase o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
