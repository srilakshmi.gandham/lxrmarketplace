/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.knowledgebase;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.stream.Collectors.toList;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Tool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author NE16T1213.
 */
@Controller
@RequestMapping("/knowledge-base.html")
public class KnowledgeBaseController {

    private static final Logger logger = Logger.getLogger(KnowledgeBaseController.class);

    @Autowired
    private KnowledgeBaseService knowledgeBaseService;
    Login user = null;
    ExpertUser expertUser = null;
    boolean userLoggedIn = false;

    /*Page is loading*/
    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response, ModelMap model, @ModelAttribute("knowledgeBase") KnowledgeBase knowledgeBase) {
        HttpSession session = request.getSession();

        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                user = (Login) session.getAttribute("user");
                if (user.isAdmin()) {
                    expertUser = (ExpertUser) session.getAttribute("expertUser");
                }

            }
        }
        /* Preparing the list of existed questions.*/
        List<KnowledgeBase> knowledgeBaseQuestions = knowledgeBaseService.getKnowledgeBaseQuestions();
        session.setAttribute("knowledgeBaseQuestions", knowledgeBaseQuestions);

        /*Preparing the list of tool names and id for user to add question or  to apply filter based on tool*/
        List<Tool> lxrMarketplaceToolsList = knowledgeBaseService.getToolsOfLXRMarketplace();
        model.addAttribute("lxrMarketplaceToolsList", lxrMarketplaceToolsList);
        session.setAttribute("lxrMarketplaceToolsList", lxrMarketplaceToolsList);

        model.addAttribute("buildQuestionsRows", true);
        session.removeAttribute("knowledgeBaseFilterdResult");

        return "view/admin/knowledgebase/knowledgeBase";
    }

    /*To add Question for a tools  */
    @RequestMapping(params = "addQuestion=addQuestion", method = RequestMethod.POST)
    public String addQuestiontoKnowledgeBase(HttpServletRequest request, HttpServletResponse response, ModelMap model,
            @ModelAttribute("knowledgeBase") KnowledgeBase knowledgeBase) throws IOException {
        HttpSession session = request.getSession();

        if (userLoggedIn && user.isAdmin()) {
            /* Preparing the list of existed questions.*/
            logger.info("Request -->addQuestion, To add new  question to knowledge base ");
            boolean questionAddedStatus = false;
            if (knowledgeBase.getQuestion() != null && knowledgeBase.getAnswer() != null) {
                questionAddedStatus = knowledgeBaseService.insertLXRMToolsIdsForQuestionId(knowledgeBase, 1);
            }
            if (questionAddedStatus) {
                logger.info("Request -->addQuestion, Added new  question to knowledge base successfully");
                session.setAttribute("questionStatus", "Question added successfully.");
            } else {
                logger.info("Request -->addQuestion, Adding new  question to knowledge base failed");
                session.setAttribute("questionStatus", "Question adding is failed due to heavy content.");
            }

            session.removeAttribute("knowledgeBaseFilterdResult");
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/knowledge-base.html");
            return null;
        } else {
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }

    }

    /*To delete a question and respective toolIDS from a database.*/
    @RequestMapping(params = "deleteQuestion=deleteQuestion")
    @ResponseBody
    public Object[] deleteStatusKnowledgeBaseQuestion(HttpServletRequest request, HttpServletResponse response, @RequestParam("selQuestion") long[] selectedQuestionArray) throws IOException {
        HttpSession session = request.getSession();
        Object[] resposneBody = new Object[1];

        logger.info("Request -->deleteQuestion, To inactive the questions from selected list");
        /*To inactive the questions from selected list;*/
        knowledgeBaseService.deleteKnowledgeBaseQuestions(selectedQuestionArray);
        /*knowledgeBaseService.deleteToolIDSList(selectedQuestionArray);*/

        session.removeAttribute("knowledgeBaseFilterdResult");
        /* Preparing the list of existed questions.*/
        List<KnowledgeBase> knowledgeBaseQuestions = knowledgeBaseService.getKnowledgeBaseQuestions();
        session.setAttribute("knowledgeBaseQuestions", knowledgeBaseQuestions);

        resposneBody[0] = knowledgeBaseQuestions;
        return resposneBody;
    }

    /*To modify the existed content of  question of a tool.*/
    @RequestMapping(params = "updateKnowledgeCont=updateKnowledgeCont")
    public String modifyQuestionInKnowledgeBase(HttpServletRequest request, HttpServletResponse response, ModelMap model,
            @ModelAttribute("knowledgeBase") KnowledgeBase knowledgeBase) throws IOException {
        HttpSession session = request.getSession();
        if (userLoggedIn && user.isAdmin()) {
            long[] selectedQuestionsIds = new long[1];
            selectedQuestionsIds[0] = knowledgeBase.getQuestionID();
            try {
                logger.info("Request -->updateKnowledgeCont, To update the question content of knowledge Base");
                boolean updatedStatus = false;
                if (knowledgeBase.getQuestion() != null && knowledgeBase.getAnswer() != null && knowledgeBase.getQuestionID() != 0) {
                    updatedStatus = knowledgeBaseService.updateQuestionContent(knowledgeBase);
                }
                if (updatedStatus) {
                    knowledgeBaseService.deleteToolIDSList(selectedQuestionsIds);
                    knowledgeBaseService.insertLXRMToolsIdsForQuestionId(knowledgeBase, 2);
                    logger.info("Request -->updateKnowledgeCont, updation the question content of knowledge Base is successfully");
                    session.setAttribute("questionStatus", "Question updated successfully.");
                } else {
                    session.setAttribute("questionStatus", "Question updation is failed due to heavy content.");
                    logger.info("Request -->updateKnowledgeCont, updation the question content of knowledge Base is failed");
                }
            } catch (Exception e) {
                logger.error("Exception in updating knowledge base question content" + e.getMessage());
            }

            session.removeAttribute("knowledgeBaseFilterdResult");

            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/knowledge-base.html");
            return null;
        } else {
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }

    }

    /* Select the existed content of a  question based on selected question Id to modify.*/
    @RequestMapping(params = "modifyQuestion=modifyQuestion")
    @ResponseBody
    public Object[] selectKnowledgeBaseQuestion(HttpServletRequest request, HttpServletResponse response,
            int selectedQuestionID) throws IOException {
        if (userLoggedIn && user.isAdmin()) {
            List<KnowledgeBase> selectedQuestion = knowledgeBaseService.selectQuestionContentToModify(selectedQuestionID);
            Object[] responseData = new Object[6];
            responseData[0] = selectedQuestion.get(0).getQuestionID();
            responseData[1] = selectedQuestion.get(0).getQuestion();
            responseData[2] = selectedQuestion.get(0).getAnswer();
            responseData[4] = selectedQuestion.get(0).getQuestionType();
            responseData[5] = selectedQuestion.get(0).getActiveType();
            List<Long> toolIds = new ArrayList<>();
            selectedQuestion.stream().filter((knowledgeBase) -> (!toolIds.contains(knowledgeBase.getTooldID()))).forEach((knowledgeBase) -> {
                toolIds.add(knowledgeBase.getTooldID());
            });
            responseData[3] = toolIds.toArray();
            return responseData;
        } else {
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }

    }

    @RequestMapping(params = "applyToolFilter=applyToolFilter", method = RequestMethod.POST)
    public String applyUserSelectToolFilterOnKnowledgeBaseList(HttpServletRequest request, HttpServletResponse response, ModelMap model,
            @ModelAttribute("knowledgeBase") KnowledgeBase knowledgeBase) throws IOException {
        HttpSession session = request.getSession();

        /* Preparing the list of existed questions.*/
        List<KnowledgeBase> knowledgeBaseQuestions = knowledgeBaseService.selectQuestionsBasedOnToolID(knowledgeBase.getUserSelectedToolID());
        session.setAttribute("knowledgeBaseQuestions", knowledgeBaseQuestions);

        /*Preparing the list of tool names and id for user to add question or  to apply filter based on tool*/
        List<Tool> lxrMarketplaceToolsList = knowledgeBaseService.getToolsOfLXRMarketplace();
        model.addAttribute("lxrMarketplaceToolsList", lxrMarketplaceToolsList);
        session.setAttribute("lxrMarketplaceToolsList", lxrMarketplaceToolsList);

        session.removeAttribute("knowledgeBaseFilterdResult");
        model.addAttribute("buildQuestionsRows", true);

        return "view/admin/knowledgebase/knowledgeBase";
    }

    /* Filtering of Questions based on Paid/Unpaid Type.*/
    @RequestMapping(params = "applyTypeFilter=applyTypeFilter")
    @ResponseBody
    public Object[] applyTypeFiltersOnQuestions(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("paidType") int paidType,
            @RequestParam("unpaidType") int unpaidType,
            @RequestParam("activeType") int activeType,
            @RequestParam("inactiveType") int inactiveType,
            @RequestParam("toolId") long toolId) {
        Object[] responseData = new Object[1];
        HttpSession session = request.getSession();
        List<KnowledgeBase> knowledgeBaseQuestions = null;
        List<KnowledgeBase> knowledgeBaseFilterdResult = null;
        try {
            if (session.getAttribute("knowledgeBaseQuestions") != null) {
                knowledgeBaseQuestions = (List<KnowledgeBase>) session.getAttribute("knowledgeBaseQuestions");
            } else {
                knowledgeBaseQuestions = knowledgeBaseService.selectQuestionsBasedOnToolID(toolId);
            }
            knowledgeBaseFilterdResult = knowledgeBaseQuestions.stream()
                    .filter(kBL -> (kBL.getQuestionType() == paidType) || (kBL.getQuestionType() == unpaidType))
                    .filter(kBL -> (kBL.getActiveType() == activeType) || (kBL.getActiveType() == inactiveType))
                    .collect(toList());
            responseData[0] = knowledgeBaseFilterdResult;
            session.setAttribute("knowledgeBaseFilterdResult", knowledgeBaseFilterdResult);
            return responseData;
        } catch (Exception e) {
            logger.error("Exception in applyTypeFiltersOnQuestions" + e.getMessage());
        }

        return null;
    }

    /*Apply sort up/down filters on knowledgeBase question*/
    @RequestMapping(params = "filter = filterData")
    @ResponseBody
    public Object[] applySortFiltersOnTable(HttpServletRequest request, HttpServletResponse response, @RequestParam("sort") String sortOperation,
            @RequestParam("way") String sortType) {
        try {

            Object[] data = new Object[1];
            HttpSession session = request.getSession();
            List<KnowledgeBase> knowledgeBaseList = null;
            if (session.getAttribute("knowledgeBaseFilterdResult") != null) {
                knowledgeBaseList = (List<KnowledgeBase>) session.getAttribute("knowledgeBaseFilterdResult");
            } else if (knowledgeBaseList == null) {
                if (session.getAttribute("knowledgeBaseQuestions") != null) {
                    knowledgeBaseList = (List<KnowledgeBase>) session.getAttribute("knowledgeBaseQuestions");
                } else {
                    knowledgeBaseList = knowledgeBaseService.getKnowledgeBaseQuestions();
                }
            } else if (knowledgeBaseList == null) {
                knowledgeBaseList = knowledgeBaseService.getKnowledgeBaseQuestions();
            }
            if (sortOperation.equalsIgnoreCase("sortQuestion")) {
                if (sortType.equals("up")) {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionComparatorU);
                } else {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionComparatorD);
                }
            } else if (sortOperation.equalsIgnoreCase("sortAnswer")) {
                if (sortType.equals("up")) {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.answerComparatorU);
                } else {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.answerComparatorD);
                }
            } else if (sortOperation.equalsIgnoreCase("sortLastUpdate")) {
                if (sortType.equals("up")) {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.upDateComparatorU);
                } else {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.upDateComparatorD);
                }
            } else if (sortOperation.equalsIgnoreCase("sortType")) {
                if (sortType.equals("up")) {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionTypeComparatorU);

                } else {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionTypeComparatorD);
                }
            } else if (sortOperation.equalsIgnoreCase("sortActive")) {
                if (sortType.equals("up")) {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionActiveComparatorU);
                } else {
                    Collections.sort(knowledgeBaseList, KnowledgeBase.questionActiveComparatorD);
                }
            }
            data[0] = knowledgeBaseList;
            return data;
        } catch (Exception e) {
            logger.error("applySortFiltersOnTable", e);
        }
        return null;
    }

    /*Converting questions list into JSON format*/
    @RequestMapping(params = "questionList=questionList")
    @ResponseBody
    public Object[] knowledgeBaseQuestionList(HttpServletRequest request, HttpServletResponse response) {
        try {
            Object[] responseData = new Object[2];
            HttpSession session = request.getSession();
            List<KnowledgeBase> knowledgeBaseQuestions = null;

            if (session.getAttribute("knowledgeBaseQuestions") != null) {
                knowledgeBaseQuestions = (List<KnowledgeBase>) session.getAttribute("knowledgeBaseQuestions");
            } else {
                knowledgeBaseQuestions = knowledgeBaseService.getKnowledgeBaseQuestions();
                session.setAttribute("knowledgeBaseQuestions", knowledgeBaseQuestions);
            }

            responseData[0] = knowledgeBaseQuestions;
            return responseData;
        } catch (Exception e) {
            logger.error("Exception in knowledgeBaseQuestionList" + e.getMessage());
        }
        return null;
    }

    @RequestMapping(value = "/knowledge-base.html", params = "action=menu", method = RequestMethod.POST)
    public @ResponseBody
    String navigationToMenuBar(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("menuOption") String menuOption) {
        String naviagtionUrl = null;
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("admin")) {
            switch (menuOption) {
                case "dashBoard":
                    naviagtionUrl = url + "/dashboard.html";
                    break;
                case "autoMail":
                    naviagtionUrl = url + "/automail.html";
                    break;
                case "settings":
                    naviagtionUrl = url + "/settings.html";
                    break;
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "expertUser":
                    naviagtionUrl = url + "/expert-user.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("reviewer")) {

            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("expert")) {
            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else {
            naviagtionUrl = url;
        }
        return naviagtionUrl;
    }
}
