/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Types;
import lxr.marketplace.util.Common;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;

/**
 *
 * @author anil
 */
@Service
public class ATEReportsService {
    
    private static Logger logger = Logger.getLogger(ATEReportsService.class);
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    private String downloadFolder;
    
    public String getExpertsReport(ATEReport ateReport){
        String query = "";
        String fileName = "";
        Object[] params = null;
        if(ateReport.getExpertName().equals("0")){
            query = "select e.name,e.role, count(k.question_id) as total_assigned_questions, "
                    + "count(case when f.rating =2 then e.expert_id end) as poor, "
                    + "count(case when f.rating =3 then e.expert_id end) as fair, "
                    + "count(case when f.rating =4 then e.expert_id end) as good, "
                    + "count(case when f.rating =5 then e.expert_id end) as excellent, "
                    + "count(case when f.rating is null then e.expert_id end) as un_rated_questions "
                    + "from (select q.question_id, q.created_date, q.expert_id from lxrm_ate_questions q "
                    + "where date_format(q.created_date, '%Y-%m-%d') between date_format(?, '%Y-%m-%d') "
                    + "and date_format(?, '%Y-%m-%d') and q.expert_id != 0) k "
                    + "left join lxrm_ate_questions_feedback f on k.question_id=f.question_id  "
                    + "left join lxrm_ate_experts e on k.expert_id=e.expert_id group by e.expert_id";
            params = new Object[]{ateReport.getStartDate(), ateReport.getEndDate()};
        }else{
            query = "select e.name,e.role, count(k.question_id) as total_assigned_questions, "
                    + "count(case when f.rating =2 then e.expert_id end) as poor, "
                    + "count(case when f.rating =3 then e.expert_id end) as fair, "
                    + "count(case when f.rating =4 then e.expert_id end) as good, "
                    + "count(case when f.rating =5 then e.expert_id end) as excellent, "
                    + "count(case when f.rating is null then e.expert_id end) as un_rated_questions "
                    + "from (select q.question_id,q.created_date from lxrm_ate_questions q "
                    + "where q.expert_id = ? and date_format(q.created_date, '%Y-%m-%d') "
                    + "between date_format(?, '%Y-%m-%d') and date_format(?, '%Y-%m-%d')) k "
                    + "left join lxrm_ate_questions_feedback f on k.question_id=f.question_id "
                    + "left join lxrm_ate_experts e on e.expert_id = ? group by e.expert_id";
            params = new Object[]{ateReport.getExpertName(), ateReport.getStartDate(), ateReport.getEndDate(), ateReport.getExpertName()};
        }
        
        try {
            logger.debug("LXRM User Website Tracking Report: " + query + "_" + ateReport.getStartDate()+"_"+ ateReport.getEndDate());
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query, params);
            String reportName = "Ask The Expert - Ratings Report";
            fileName = getATEExpertsRatingReport(sqlRowSet, downloadFolder, ateReport.getStartDate(), ateReport.getEndDate(), reportName);
            return fileName;
        } catch (DataAccessException e) {
            logger.error("Exception: "+e);
        }
        return null;
    }
    
//    For get expert rating info reports
    public String getATEExpertsRatingReport(SqlRowSet sqlRowSet, String filePath,
            String startDate, String endDate, String repName) {
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetaData = sqlRowSet.getMetaData();

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet(repName);
            HSSFFont font = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[] = new int[]{16, 24, 24, 24, 24, 24, 28, 28, 15};

// Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty.setFont(font);

// Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            sheet.createFreezePane(0, 4);

            CellStyle dateStyle = workBook.createCellStyle();
            
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy HH:mm:ss"));

//Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Type");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(startDate+" to "+endDate);
            cell.setCellStyle(heaCelSty);
            rowCnt = 2;

//Report Columns
            colCount = sqlRowSetMetaData.getColumnCount();
            rows = sheet.createRow(rowCnt+1);
            rows.setHeight((short) 500); 
            cell = rows.createCell(0); cell.setCellValue("Name"); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(1); cell.setCellValue("Role"); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(2); cell.setCellValue("Total Assigned Questions"); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(3); cell.setCellValue("Questions Rated \"Poor\""); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(4); cell.setCellValue("Questions Rated \"Fair\""); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(5); cell.setCellValue("Questions Rated \"Good\""); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(6); cell.setCellValue("Questions Rated \"Excellent\""); cell.setCellStyle(columnHeaderStyle); 
            cell = rows.createCell(7); cell.setCellValue("No.of Unrated Questions"); cell.setCellStyle(columnHeaderStyle); 
            rowCnt = 4;
//Report Data
            boolean isContainData = sqlRowSet.isBeforeFirst();
            if (isContainData) {
                while (sqlRowSet.next()) {
                    rows = sheet.createRow(rowCnt);
                    int tempCount=0;
                    for (int colInd = 1; colInd <= colCount; colInd++) {
                        cell = rows.createCell(tempCount);
                        tempCount++;
                        if (sqlRowSetMetaData.getColumnType(colInd) == Types.BIGINT) {
                            cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetaData.getColumnName(colInd)));
                            numericStyle.setDataFormat(format.getFormat("0"));
                            cell.setCellStyle(numericStyle);
                        }else {
                            String colVal = sqlRowSet.getString(sqlRowSetMetaData.getColumnName(colInd));
                            if(sqlRowSetMetaData.getColumnName(colInd).equalsIgnoreCase("name") || sqlRowSetMetaData.getColumnName(colInd).equalsIgnoreCase("role")){
        //                        WordUtils.capitalize("") is for capitalize each word in a string
                                colVal = WordUtils.capitalize(colVal);
                            }
                            cell.setCellValue(colVal);
                        }
                    }
                    rowCnt++;
                }
            }
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            String reptName = Common.removeSpaces(repName);
            String fileName = "";
            fileName = filePath + Common.createFileName(reptName) + ".xls";
            logger.info(" Report generated in .xls format");
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (IOException | InvalidResultSetAccessException e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
