/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.ate.answers.ExpertQuestionsService;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.user.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author anil
 */
@Controller
public class ATEReportsController {
    
    @Autowired
    private ExpertQuestionsService expertQuestionsService;
    
    @Autowired
    private ATEReportsService ateReportsService;
    
    @RequestMapping(value="ate-reports.html")
    public String navigationToExpertUser(Model model, @ModelAttribute("ateReport") ATEReport ateReport){
        ateReport = new ATEReport();
        model.addAttribute(ateReport);
        return "view/admin/ateReport/ateReports";
    }
//    To get the expert list on page loading time
    @ModelAttribute("experts")
    private HashMap<Long, String> getExperts(HttpSession session) {
        List<ExpertUser> expertUserList = expertQuestionsService.getAllExperts();
        session.setAttribute("expertUserList", expertUserList);
        return expertQuestionsService.getExpertsInSelectorFormat(expertUserList);
    }
    
    @RequestMapping(value="ate-reports.html", params = {"report=download"}, method = RequestMethod.POST)
    public String downloadATEReport(HttpSession session, Model model, HttpServletResponse response, 
            @ModelAttribute("ateReport") ATEReport ateReport) {
        boolean userLoggedIn = false;
        if(session.getAttribute("userLoggedIn") != null){
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        }
        Login user = null;
        if(session.getAttribute("userLoggedIn") != null){
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        }
        if(session.getAttribute("user") != null){
            user = (Login) session.getAttribute("user");
        }
//        ateReport = new ATEReport();
        model.addAttribute(ateReport);
        String fileName = "";
        if (userLoggedIn && user != null && user.isAdmin()) {
            fileName = ateReportsService.getExpertsReport(ateReport);
        }
        try{
            File f = new File(fileName);
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
            FileInputStream fis = new FileInputStream(f);
            FileCopyUtils.copy(fis, response.getOutputStream());
        }catch(FileNotFoundException e){
//            return 
        }catch(IOException e){
            
        }
        return null;
    }
}
