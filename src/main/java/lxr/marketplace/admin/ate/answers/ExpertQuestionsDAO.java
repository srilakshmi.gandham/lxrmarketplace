/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.answers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import static java.sql.Types.NULL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.admin.expertuser.ExpertUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vemanna
 */
@Repository
public class ExpertQuestionsDAO {

    private static final Logger logger = Logger.getLogger(ExpertQuestionsDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    String query;

    public SqlRowSet fetchingHighLevelInfo(long expertId) {

        query = "select * from "
                + "(select count(*) as un_assigned_questions from lxrm_ate_questions where expert_id is NULL)a, "
                + "(select count(*) as assigned_questions from lxrm_ate_questions  where (expert_id = ? or reviewer_id = ?) and is_published=0)b";
        SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query, expertId, expertId);

        return sqlRowSet;

    }

    /**
     *
     * @param filteredType
     * @param filteredQuery
     * @return List<ExpertQuestionsDTO>
     */
    List<ExpertQuestionsDTO> fetchingQuestions(long expertId, int filteredType, String filteredQuery) {
        List<ExpertQuestionsDTO> expertQuestionsList = null;

        switch (filteredType) {
            case 1:
                //Unassigned & My Open Questions (Default Option)
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.is_published = 0 and ("
                        + "q.expert_id = " + expertId + " or q.reviewer_id = " + expertId + " or " + " expert_id is null) "
                        + "and q.tool_id = t.id and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 2:
                //My Open Questions
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.is_published = 0 "
                        + "and (q.expert_id = " + expertId + " or q.reviewer_id = " + expertId + ") "
                        + "and q.tool_id = t.id and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 3:
                //Unassigned Questions
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.expert_id is null "
                        + "and q.tool_id = t.id  and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 4:
                //All Questions
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.tool_id = t.id  "
                        + "and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 5:
                //Search by Email
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where  u.email "
                        + "like '%" + filteredQuery + "%' and q.tool_id = t.id  and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 6:
                //Search by Question
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.question "
                        + "like '%" + filteredQuery + "%'and q.tool_id = t.id  and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 7:
                //Search by Tool
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where t.name "
                        + "like '%" + filteredQuery + "%'and q.tool_id = t.id  and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;

            case 8:
                //My Answered Questions
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.answer != '' "
                        + "and (q.expert_id = " + expertId + " or q.reviewer_id = " + expertId + ") and q.tool_id = t.id and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;

            case 9:
                //Search by Status
                String subQuery = "";
                if (filteredQuery.equalsIgnoreCase("Unassigned")) {
                    subQuery = " q.expert_id is null and  q.reviewer_id is null and q.is_published = 0 ";
                } else if (filteredQuery.equalsIgnoreCase("Assigned")) {
                    subQuery = " q.expert_id is not null and  q.reviewer_id = 0 and q.is_published = 0 ";
                } else if (filteredQuery.equalsIgnoreCase("In Review")) {
                    subQuery = " q.reviewer_id is not null and  q.reviewer_id != 0 and q.is_published = 0 ";
                } else if (filteredQuery.equalsIgnoreCase("Answered")) {
                    subQuery = " q.is_published = 1 ";
                }
                query = "select a.*, exprt.name as expert_name, afb.rating from ( "
                        + "(select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where " + subQuery
                        + "and q.tool_id = t.id and u.id = q.user_id group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";
                break;
            case 10:
                //All Unanswered Questions
                query = "select a.*, exprt.name as expert_name, afb.rating from ("
                        + " (select q.created_date,q.question_id, q.tool_id, q.question,q.expert_id, q.reviewer_id, q.is_published, "
                        + "t.name as tool_name, u.email from lxrm_ate_questions q, tool t, user u where q.expert_id is not null and "
                        + "q.reviewer_id != 0 and q.is_published = 0 and q.tool_id = t.id  and u.id = q.user_id "
                        + "group by q.question_id)a "
                        + "left join "
                        + "lxrm_ate_experts exprt on a.expert_id = exprt.expert_id "
                        + "left join "
                        + "lxrm_ate_questions_feedback afb  on a.question_id = afb.question_id)ORDER BY a.created_date DESC";

        }
        logger.info("Fetching ATE questions query: " + query);

        SimpleDateFormat sdFormat = new SimpleDateFormat("dd-MMM-yyy");
        expertQuestionsList = this.jdbcTemplate.query(query, (ResultSet rs, int i) -> {
            ExpertQuestionsDTO expertQuestionsDTO = new ExpertQuestionsDTO();

            expertQuestionsDTO.setQuestionId(rs.getLong("question_id"));
            expertQuestionsDTO.setQuestion(rs.getString("question"));

            Timestamp timeStamp = rs.getTimestamp("created_date");
            expertQuestionsDTO.setDate(sdFormat.format(timeStamp));
            expertQuestionsDTO.setToolName(rs.getString("tool_name"));
            expertQuestionsDTO.setUserEmail(rs.getString("email"));

            if (rs.getString("expert_name") != null) {
                expertQuestionsDTO.setExpertName(rs.getString("expert_name"));
            } else {
                expertQuestionsDTO.setExpertName("Unassigned");
            }

            if (rs.getBoolean("is_published")) {
                expertQuestionsDTO.setStatus("Answered");
            } else if (rs.getLong("expert_id") != 0 && rs.getLong("reviewer_id") != 0) {
                expertQuestionsDTO.setStatus("In Review");
            } else if (rs.getLong("expert_id") != 0 && rs.getLong("reviewer_id") == 0) {
                expertQuestionsDTO.setStatus("Assigned");
            } else if (rs.getLong("expert_id") == 0 && rs.getLong("reviewer_id") == 0) {
                expertQuestionsDTO.setStatus("Unassigned");
            }

            if (rs.getString("rating") != null) {
                expertQuestionsDTO.setRating(rs.getInt("rating"));
            }

            return expertQuestionsDTO;
        });
        return expertQuestionsList;
    }

    public List<ExpertUser> fetchingAllExpertsList() {

        query = "select expert_id, name, email, role from lxrm_ate_experts where is_deleted = 0";
        List<ExpertUser> expertUsersList = null;

        expertUsersList = this.jdbcTemplate.query(query, (ResultSet rs, int i) -> {
            ExpertUser expertUser = new ExpertUser();
            expertUser.setExpertId(rs.getLong("expert_id"));
            expertUser.setName(rs.getString("name"));
            expertUser.setEmail(rs.getString("email"));
            expertUser.setRole(rs.getString("role"));
            return expertUser;
        });
        return expertUsersList;
    }

    public ExpertQuestionEditDTO fetchingQuestion(long questionId) {

        query = "select u.email, u.name, t.name as tool_name, t.tool_link, q.domain, q.other_inputs, q.question_id, q.question, q.steps, q.answer, q.expert_id, "
                + "q.tool_id, q.reviewer_id, q.is_published, q.file_name from lxrm_ate_questions q, tool t, user u where q.question_id= ? and "
                + "q.tool_id = t.id and q.user_id = u.id group by q.question_id;";
        ExpertQuestionEditDTO expertQuestionEditDTO = this.jdbcTemplate.queryForObject(query, new Object[]{questionId}, new RowMapper<ExpertQuestionEditDTO>() {
            @Override
            public ExpertQuestionEditDTO mapRow(ResultSet rs, int i) throws SQLException {
                ExpertQuestionEditDTO expertQuestionEdit = new ExpertQuestionEditDTO();

                String answer = null;
                try {
                    if (rs.getBytes("answer") != null) {
                        answer = new String(rs.getBytes("answer"));
                    }
                } catch (Exception e) {
                    answer = rs.getString("answer");
                    logger.error("Exception converting bytes into string " + e.getMessage());
                }

                String steps = null;
                try {
                    if (rs.getBytes("steps") != null) {
                        steps = new String(rs.getBytes("steps"));
                    }
                } catch (Exception e) {
                    steps = rs.getString("steps");
                    logger.error("Exception converting bytes into string " + e.getMessage());
                }

                expertQuestionEdit.setAnswerTemplate(answer);
                expertQuestionEdit.setSteps(steps);
                expertQuestionEdit.setToolId(rs.getLong("tool_id"));
                expertQuestionEdit.setWebpage(rs.getString("domain"));
                expertQuestionEdit.setOtherInputs(rs.getString("other_inputs"));
                expertQuestionEdit.setToolLink(rs.getString("tool_link"));
                long expertId = rs.getLong("expert_id");
                long reviewerId = rs.getLong("reviewer_id");
                expertQuestionEdit.setAssigneeId(expertId);
                expertQuestionEdit.setReviewerId(reviewerId);
                expertQuestionEdit.setAttachedFile(rs.getString("file_name"));

                ExpertQuestionsDTO expertQuestionsDTO = new ExpertQuestionsDTO();
                expertQuestionsDTO.setQuestion(rs.getString("question"));
                expertQuestionsDTO.setQuestionId(rs.getLong("question_id"));
                expertQuestionsDTO.setToolName(rs.getString("tool_name"));
                expertQuestionsDTO.setUserEmail(rs.getString("email"));
                expertQuestionsDTO.setUserName(rs.getString("name"));

                if (rs.getBoolean("is_published")) {
                    expertQuestionsDTO.setStatus("published");
                    expertQuestionEdit.setPreviousId(reviewerId);
                } else if (expertId != 0 && reviewerId != 0) {
                    expertQuestionsDTO.setStatus("inreview");
                    expertQuestionEdit.setPreviousId(reviewerId);
                } else if (expertId != 0 && reviewerId == 0) {
                    expertQuestionsDTO.setStatus("assigned");
                    expertQuestionEdit.setPreviousId(expertId);
                } else if (expertId == 0 && reviewerId == 0) {
                    expertQuestionsDTO.setStatus("unassigned");
                    expertQuestionEdit.setPreviousId(-1);
                }

                expertQuestionEdit.setExpertQuestionsDTO(expertQuestionsDTO);

                return expertQuestionEdit;
            }

        });
        return expertQuestionEditDTO;
    }

    public SqlRowSet getAnswerTemplate(String question, long toolId) {
        query = "select a.* from (SELECT steps,answer, "
                + "MATCH (question) AGAINST ('" + question + "' IN NATURAL LANGUAGE MODE) AS score "
                + "FROM lxrm_ate_answer_templates where tool_id = " + toolId + " order by score desc limit 1)a where a.score > 0";

        SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
        return sqlRowSet;
    }

    public int saveAnser(long questionId, String status, Calendar cal, String steps, String answer, String attachedFileName) {
        Object[] data;
        if (status.equalsIgnoreCase("published")) {
            query = "update lxrm_ate_questions SET updated_date = ?, steps = ?, answer = ?, file_name = ? where question_id= ?";
            data = new Object[]{cal, steps, answer, attachedFileName, questionId};
        } else {
            query = "update lxrm_ate_questions SET steps = ?, answer = ?, file_name = ? where question_id= ?";
            data = new Object[]{steps, answer, attachedFileName, questionId};
        }

        int updatingRows = this.jdbcTemplate.update(query, data);
        return updatingRows;
    }

    public int updateExpertAssignee(long quesId, long assigneeId) {
        query = "update lxrm_ate_questions SET expert_id= ?, reviewer_id= ? where question_id= ?";
        return this.jdbcTemplate.update(query, assigneeId, NULL, quesId);
    }

    public int updateReviewerAndAnswer(long quesId, long reviewerId) {
        query = "update lxrm_ate_questions set reviewer_id = ? where question_id= ?";
        return this.jdbcTemplate.update(query, reviewerId, quesId);
    }

    public void updateAnsewerDetails(long questionId, Calendar cal, boolean isPublished) {
        query = "update lxrm_ate_questions set answer_date = ?, is_published = ? where question_id= ?";
        this.jdbcTemplate.update(query, cal, isPublished, questionId);
    }

    public void insertAnswerHistory(long questionId, String answer, Timestamp timeStamp) {
        query = "INSERT INTO lxrm_ate_answers_history(question_id, answer,created_date) values( ?, ?, ?);";
        this.jdbcTemplate.update(query, questionId, answer, timeStamp);
    }

    public void updateStepsAndAnswer(long questionId, String steps, String answerTemplate) {
        query = "update lxrm_ate_questions set steps = ?, answer = ? where question_id= ?";
        this.jdbcTemplate.update(query, steps, answerTemplate, questionId);
    }

    public boolean deletdAttachedFile(long questionId) {
        query = "update lxrm_ate_questions set file_name = ? where question_id= ?";
        int updatingRows = this.jdbcTemplate.update(query, null, questionId);
        return updatingRows > 0;
    }
}
