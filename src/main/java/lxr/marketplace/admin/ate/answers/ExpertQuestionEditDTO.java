/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.answers;

/**
 *
 * @author vemanna
 */
public class ExpertQuestionEditDTO {

    private ExpertQuestionsDTO expertQuestionsDTO;

    private long assigneeId;
    private long reviewerId;
    private long previousId;
    private String webpage;
    private String otherInputs;
    private String steps;
    private String answerTemplate;
    private long toolId;
    private String toolLink;
    private String attachedFile;

    public ExpertQuestionsDTO getExpertQuestionsDTO() {
        return expertQuestionsDTO;
    }

    public void setExpertQuestionsDTO(ExpertQuestionsDTO expertQuestionsDTO) {
        this.expertQuestionsDTO = expertQuestionsDTO;
    }

    public long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(long assigneeId) {
        this.assigneeId = assigneeId;
    }

    public long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public long getPreviousId() {
        return previousId;
    }

    public void setPreviousId(long previousId) {
        this.previousId = previousId;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public String getOtherInputs() {
        return otherInputs;
    }

    public void setOtherInputs(String otherInputs) {
        this.otherInputs = otherInputs;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getAnswerTemplate() {
        return answerTemplate;
    }

    public void setAnswerTemplate(String answerTemplate) {
        this.answerTemplate = answerTemplate;
    }

    public long getToolId() {
        return toolId;
    }

    public void setToolId(long toolId) {
        this.toolId = toolId;
    }

    public String getToolLink() {
        return toolLink;
    }

    public void setToolLink(String toolLink) {
        this.toolLink = toolLink;
    }

    public String getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(String attachedFile) {
        this.attachedFile = attachedFile;
    }

    
    
}
