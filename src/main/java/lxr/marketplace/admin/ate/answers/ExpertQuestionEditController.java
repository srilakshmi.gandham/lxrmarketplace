/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.answers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.user.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author vemanna
 */
@Controller
public class ExpertQuestionEditController {

    @Autowired
    ExpertQuestionsService expertQuestionsService;

    ExpertUser currentExpertUser = null;
    Login user = null;
    boolean userLoggedIn = false;

    @RequestMapping(value = "/ate-edit-question.html", method = RequestMethod.GET)
    public String inIt(HttpServletResponse response, HttpSession session, Model model) throws IOException {
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                user = (Login) session.getAttribute("user");
                if (user.isAdmin()) {
                    model.addAttribute("expertQuestionEditDTO", new ExpertQuestionEditDTO());
                    currentExpertUser = (ExpertUser) session.getAttribute("expertUser");
                }
            }
        }
        if (user == null || !user.isAdmin()) {
            response.sendRedirect("/");
            return null;
        }

        return "view/admin/ateQuestions/ateQuestionEdit";
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "fetch=ate-answer-template", method = RequestMethod.GET)
    @ResponseBody
    public ExpertQuestionEditDTO getAnswerTemplate(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("questionId") long questionId) {
        ExpertQuestionEditDTO expertQuestionEditDTO = expertQuestionsService.fetchingAnswerTemplate(questionId);
        session.setAttribute("expertQuestionEditDTO", expertQuestionEditDTO);
        return expertQuestionEditDTO;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "click=save", method = RequestMethod.POST)
    @ResponseBody
    public Object[] saveAnswerTemplate(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("quesId") long questionId, @RequestParam("steps") String steps, @RequestParam("answer") String answer,
            @RequestParam("attachedFile") MultipartFile multipartFile) {
        Object[] data = new Object[2];
        ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
        String msg;
        String fileName = " ";
        boolean isUpdated = expertQuestionsService.saveAnserTemplate(questionId, expertQuestionEditDTO.getExpertQuestionsDTO().getStatus(), steps, answer, multipartFile);
        if (isUpdated) {
            expertQuestionEditDTO.setAnswerTemplate(answer);
            if (multipartFile != null && multipartFile.getSize() != 1 && !multipartFile.getOriginalFilename().equals("emptyFile")) {
                expertQuestionEditDTO.setAttachedFile(multipartFile.getOriginalFilename());
                fileName = multipartFile.getOriginalFilename();
            }

            msg = "Your changes successfully saved.";
        } else {
            msg = "Your changes are not saved, please try again.";
        }
        data[0] = msg;
        data[1] = fileName;
        return data;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "click=assignee", method = RequestMethod.POST)
    public @ResponseBody
    Object[] setAssignee(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("quesId") long quesId, @RequestParam("assignee") long assigneeId, @RequestParam("previousId") long previousAssigneeId,
            Model model) {
        Object[] data = new Object[3];
        ExpertUser expertUser = getExpertUser(session, assigneeId);
        if (assigneeId == previousAssigneeId && expertUser.getRole().equals("expert")) {
            boolean isUpdatedRow = expertQuestionsService.updateExpertAssignee(quesId, assigneeId);
            if (isUpdatedRow) {
                data[0] = "Your changes successfully saved.";
            } else {
                data[0] = "Your changes are not saved, please try again.";
            }
        } else if (expertUser != null) {
            String assigneeName = expertUser.getName();
            String assigneeEmail = expertUser.getEmail();
            String assidnedName = currentExpertUser.getName();
            String assidnedEmail = currentExpertUser.getEmail();
            ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
            String question = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestion();
            String toolName = expertQuestionEditDTO.getExpertQuestionsDTO().getToolName();
            expertQuestionsService.sendMailToAssignee(assigneeName, assigneeEmail, assidnedName, assidnedEmail, question, toolName);
            boolean isUpdatedRow = expertQuestionsService.updateExpertAssignee(quesId, assigneeId);
            if (isUpdatedRow) {
                data[0] = "Assignee has updated successfully.";
                data[1] = 1;
                data[2] = assigneeId;
            } else {
                data[0] = "Assignee has not updated. please try again later.";
            }
        } else {
            data[0] = "Assignee has not updated. please try again later.";
        }

        return data;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "click=review", method = RequestMethod.POST)
    public @ResponseBody
    Object[] setReviewer(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("quesId") long quesId, @RequestParam("reviewer") long reviewerId, @RequestParam("previousId") long previousReviewerId) {
        Object[] data = new Object[3];
        if (reviewerId == previousReviewerId) {
            boolean isUpdatedRow = expertQuestionsService.updateReviewerAndAnswer(quesId, reviewerId);
            if (isUpdatedRow) {
                data[0] = "Your changes successfully saved.";
            } else {
                data[0] = "Your changes are not saved, please try again.";
            }
        } else {
            ExpertUser expertUser = getExpertUser(session, reviewerId);
            if (expertUser != null) {
                if (expertUser.getExpertId() == reviewerId && !expertUser.getRole().equals("expert")) {
                    String reviewerName = expertUser.getName();
                    String reviewerEmail = expertUser.getEmail();
                    String assidnedName = currentExpertUser.getName();
                    String assidnedEmail = currentExpertUser.getEmail();
                    ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
                    String question = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestion();
                    String toolName = expertQuestionEditDTO.getExpertQuestionsDTO().getToolName();
                    expertQuestionsService.sendMailToReviewer(reviewerName, reviewerEmail, assidnedName, assidnedEmail, question, toolName);
                    boolean isUpdatedRow = expertQuestionsService.updateReviewerAndAnswer(quesId, reviewerId);
                    if (isUpdatedRow) {
                        data[0] = "Reviewer has updated successfully.";
                        data[1] = 1;
                        data[2] = reviewerId;
                    } else {
                        data[0] = "Reviewer has not updated. Please try again.";
                    }
                } else if (expertUser.getExpertId() == reviewerId && expertUser.getRole().equals("expert")) {
                    data[0] = "Please select a Reviewer to send the question for review.";
                }
            } else {
                data[0] = "Reviewer has not updated. Please try again.";
            }

        }

        return data;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "click=send-user", method = RequestMethod.POST)
    @ResponseBody
    public String sendAnswerTemplate(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
        String userName = expertQuestionEditDTO.getExpertQuestionsDTO().getUserName();
        String userEmail = expertQuestionEditDTO.getExpertQuestionsDTO().getUserEmail();
        String question = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestion();
        String answer = expertQuestionEditDTO.getAnswerTemplate();
        String attacheFileName = expertQuestionEditDTO.getAttachedFile();
        long questionId = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestionId();

        boolean isMailSend = expertQuestionsService.sendATEMail(userName, userEmail, questionId, question, answer, attacheFileName, false);
        if (isMailSend) {
            expertQuestionsService.updateSendAnsewerDetails(expertQuestionEditDTO.getExpertQuestionsDTO().getQuestionId());
            return "success";
        }
        return "fail";
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "click=send-mail-admin", method = RequestMethod.POST)
    @ResponseBody
    public String sendAnswerTemplateToAdmin(HttpSession session) {
        ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
        String question = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestion();
        String answerTemplate = expertQuestionEditDTO.getAnswerTemplate();
        String attacheFileName = expertQuestionEditDTO.getAttachedFile();
        long questionId = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestionId();
        ExpertUser expertUser = null;
        if (session.getAttribute("expertUser") != null) {
            expertUser = ((ExpertUser) session.getAttribute("expertUser"));
            if (expertQuestionsService.sendATEMail(expertUser.getName(), expertUser.getEmail(), questionId, question, answerTemplate, attacheFileName, true)) {
                return "success";
            }
        }
        return "fail";
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "delete=file", method = RequestMethod.POST)
    @ResponseBody
    public boolean deleteFile(HttpServletResponse response, HttpSession session, @RequestParam("questionId") long questionId) {
        boolean isDeleted = expertQuestionsService.deleteFile(questionId);
        if (isDeleted) {
            ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
            expertQuestionEditDTO.setAttachedFile(null);
            session.setAttribute("expertQuestionEditDTO", expertQuestionEditDTO);
        }
        return isDeleted;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "download=attachment", method = RequestMethod.POST)
    public String downloadAttachedFile(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        ExpertQuestionEditDTO expertQuestionEditDTO = (ExpertQuestionEditDTO) session.getAttribute("expertQuestionEditDTO");
        expertQuestionsService.downloadFile(response, expertQuestionEditDTO.getAttachedFile());
        return null;
    }

    @RequestMapping(value = "/ate-edit-question.html", params = "action=menu", method = RequestMethod.POST)
    public @ResponseBody
    String navigationToMenuBar(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("menuOption") String menuOption) {
        String naviagtionUrl = null;
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (userLoggedIn && user.isAdmin() && currentExpertUser.getRole().equals("admin")) {
            switch (menuOption) {
                case "dashBoard":
                    naviagtionUrl = url + "/dashboard.html";
                    break;
                case "autoMail":
                    naviagtionUrl = url + "/automail.html";
                    break;
                case "settings":
                    naviagtionUrl = url + "/settings.html";
                    break;
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "expertUser":
                    naviagtionUrl = url + "/expert-user.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
                case "pluginAdoption":
                    naviagtionUrl = url + "/plugin-adoption.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && currentExpertUser.getRole().equals("reviewer")) {

            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && currentExpertUser.getRole().equals("expert")) {
            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else {
            naviagtionUrl = url;
        }

        return naviagtionUrl;

    }

    @ModelAttribute("experts")
    public HashMap<Long, String> getExperts(HttpSession session) {
        List<ExpertUser> expertUserList = expertQuestionsService.getAllExperts();
        session.setAttribute("expertUserList", expertUserList);
        return expertQuestionsService.getExpertsInSelectorFormat(expertUserList);
    }

    public ExpertUser getExpertUser(HttpSession session, long expertUserId) {
        List<ExpertUser> expertUserList = (List<ExpertUser>) session.getAttribute("expertUserList");
        ExpertUser expertUser = null;
        for (ExpertUser expert : expertUserList) {
            if (expert.getExpertId() == expertUserId) {
                expertUser = expert;
            }
        }
        return expertUser;
    }

    public String getURL(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    }
}
