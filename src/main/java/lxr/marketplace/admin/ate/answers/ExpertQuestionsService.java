/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.answers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author vemanna
 */
@Service
public class ExpertQuestionsService {

    private static final Logger logger = Logger.getLogger(ExpertQuestionsService.class);

    @Autowired
    ExpertQuestionsDAO expertQuestionsDAO;
    @Autowired
    private String supportMail;
    @Autowired
    private String domainName;
    @Autowired
    private String ateImagesFolder;

    public int[] getHighLevelInfo(long expertId) {

        int[] data = new int[2];
        SqlRowSet sqlRowSet = expertQuestionsDAO.fetchingHighLevelInfo(expertId);
        boolean isContainData = sqlRowSet.isBeforeFirst();
        if (isContainData) {
            while (sqlRowSet.next()) {
                data[0] = sqlRowSet.getInt("un_assigned_questions");
                data[1] = sqlRowSet.getInt("assigned_questions");
            }
        }
        return data;
    }

    public List<ExpertQuestionsDTO> getAllQuestions(long expertId, int filteredType, String filteredQuery) {
        return expertQuestionsDAO.fetchingQuestions(expertId, filteredType, filteredQuery);
    }

    public List<ExpertUser> getAllExperts() {
        return expertQuestionsDAO.fetchingAllExpertsList();
    }

    public HashMap<Long, String> getExpertsInSelectorFormat(List<ExpertUser> expertUserList) {
        HashMap<Long, String> hashMapList = new HashMap<>();
        expertUserList.stream().forEach((expertUser) -> {
            long expertId = expertUser.getExpertId();
            String expert = expertUser.getName() + " (" + expertUser.getRole() + ")";
            hashMapList.put(expertId, expert);
        });
        return hashMapList;
    }

    public ExpertQuestionEditDTO fetchingAnswerTemplate(long questionId) {
        ExpertQuestionEditDTO expertQuestionEditDTO = expertQuestionsDAO.fetchingQuestion(questionId);
        if ("".equals(expertQuestionEditDTO.getAnswerTemplate()) || expertQuestionEditDTO.getAnswerTemplate().equals("null")) {
            String question = expertQuestionEditDTO.getExpertQuestionsDTO().getQuestion().replace("'", "");
            SqlRowSet sqlRowSet = expertQuestionsDAO.getAnswerTemplate(question, expertQuestionEditDTO.getToolId());
            boolean isContainData = sqlRowSet.isBeforeFirst();
            if (isContainData) {
                while (sqlRowSet.next()) {
                    String steps = new String((byte[]) sqlRowSet.getObject("steps"));
                    String answer = new String((byte[]) sqlRowSet.getObject("answer"));
                    expertQuestionEditDTO.setSteps(steps);
                    expertQuestionEditDTO.setAnswerTemplate(answer);
                }
                expertQuestionsDAO.updateStepsAndAnswer(expertQuestionEditDTO.getExpertQuestionsDTO().getQuestionId(), expertQuestionEditDTO.getSteps(), expertQuestionEditDTO.getAnswerTemplate());
            } else {
                expertQuestionEditDTO.setSteps("No steps found.");
                expertQuestionEditDTO.setAnswerTemplate("");
            }
        }
        return expertQuestionEditDTO;
    }

    public boolean saveAnserTemplate(long questionId, String status, String steps, String ansTemplate, MultipartFile multipartFile) {
        Calendar cal = Calendar.getInstance();
        String answerTemplate = decodeImagesFromAnswerTemplate(ansTemplate, questionId);
        String attachedFileName = saveFile(multipartFile);
        int updatedRows = expertQuestionsDAO.saveAnser(questionId, status, cal, steps, answerTemplate, attachedFileName);
        return updatedRows > 0;
    }

    public boolean sendATEMail(String name, String mailId, long questionId, String question, String ansTemplate, String attacheFileName,
            boolean isAdmin) {
        String answerTemplate = decodeImagesFromAnswerTemplate(ansTemplate, questionId);
        String body = EmailBodyCreator.bodyForExpertAnswerTestMail(name, question, answerTemplate);
        boolean mailSent = false;
        if (isAdmin) {
            String subject = EmailBodyCreator.subjectOnExpertAnswerTestMail;
            if (attacheFileName == null) {
                mailSent = SendMail.sendMail(supportMail, mailId, subject, body);
            } else {
                String[] attachFile = new String[]{attacheFileName};
                mailSent = SendMail.sendMailWithAttachments(mailId, supportMail, subject, body, "", ateImagesFolder, attachFile);
            }

        } else {
            String subject = EmailBodyCreator.subjectOnExpertAnswerToUser;
            if (attacheFileName == null) {
                mailSent = SendMail.sendMail(supportMail, mailId, supportMail, subject, body);
            } else {
                String[] attachFile = new String[]{attacheFileName};
                String[] toCCAddress = new String[]{supportMail};
                mailSent = SendMail.sendMailWithAttachments(mailId, supportMail, subject, body, ateImagesFolder, attachFile, toCCAddress);
            }

        }

        return mailSent;
    }

    public boolean updateExpertAssignee(long quesId, long assigneeId) {
        int updatedRows = expertQuestionsDAO.updateExpertAssignee(quesId, assigneeId);
        return updatedRows > 0;
    }

    public void sendMailToAssignee(String assigneeName, String assigneeEmail, String assidnedName, String assidnedEmail,
            String question, String toolName) {
        String subject = EmailBodyCreator.subjectOnExpertQuestionAssigned;
        String body = EmailBodyCreator.bodyForExpertQuestionAssigned(assigneeName, assidnedName, question, toolName);
        SendMail.sendMail(supportMail, assigneeEmail, assidnedEmail, subject, body);
    }

    public void sendMailToReviewer(String reviewerName, String reviewerEmail, String assidnedName, String assidnedEmail,
            String question, String toolName) {
        String subject = EmailBodyCreator.subjectOnExpertQuestionReview;
        String body = EmailBodyCreator.bodyForExpertQuestionReview(reviewerName, assidnedName, question, toolName);
        SendMail.sendMail(supportMail, reviewerEmail, assidnedEmail, subject, body);
    }

    public boolean updateReviewerAndAnswer(long quesId, long reviewerId) {
        int updatedRows = expertQuestionsDAO.updateReviewerAndAnswer(quesId, reviewerId);
        return updatedRows > 0;
    }

    public void updateSendAnsewerDetails(long questionId) {
        Calendar cal = Calendar.getInstance();
        expertQuestionsDAO.updateAnsewerDetails(questionId, cal, true);
    }

    public void insertAnswerHistory(long questionId, String answer) {
        Timestamp timeStamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        expertQuestionsDAO.insertAnswerHistory(questionId, answer, timeStamp);
    }

    public String decodeImagesFromAnswerTemplate(String answer, long questionId) {

        //Regulear expression to idetify images tag of HTMl in a string
        Pattern imagePattern = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");
        ///To identify base64 Matcher
        Matcher binaryMatcher = imagePattern.matcher(answer);
        //Index the images to save the image from the string.
        int i = 0;
        //Get the list of images in answer template for  source of base64
        while (binaryMatcher.find()) {
            i++;
            //Skip the image which source is already saved in local disk/ decoded
            if (binaryMatcher.group(1).contains("data:image/")) {
                //Split the value of source attribute of image tag
                String imageType = Common.between(binaryMatcher.group(1), "data:image/", ";");
                String base64Code[] = binaryMatcher.group(1).split(",");
                //The base 64 enocded code of image
                String encodedString = base64Code[1];
                byte[] encodedData = Base64.getDecoder().decode(encodedString.getBytes());
                //Saving in /disk2/lxrmarketplace/ate_images
                String localPath = ateImagesFolder + "/" + questionId + "_" + i + "." + imageType;
                OutputStream stream = null;
                try {
                    stream = new FileOutputStream(localPath);
                    stream.write(encodedData);
                } catch (FileNotFoundException ex) {
                    logger.error("Exception on converting image from base64: ", ex);
                } catch (IOException iex) {
                    logger.error("Exception on converting image from base64: ", iex);
                }
                //Replaceing image source path of encoded data with new saved path
                if (answer.contains("<img src=\"data:image/" + imageType + ";base64," + encodedString + "\">")) {
                    answer = answer.replace("<img src=\"data:image/" + imageType + ";base64," + encodedString + "\">",
                            "<img src=\"" + domainName + localPath.substring(localPath.indexOf("/ate_images"), localPath.length()) + "\">");
                }
            }
        }
        return answer;
    }

    private String saveFile(MultipartFile multipartFile) {
        String filename = null;
        if (multipartFile != null && multipartFile.getSize() != 1 && !multipartFile.getOriginalFilename().equals("emptyFile")) {
            File oldFile = new File(ateImagesFolder + "/" + multipartFile.getOriginalFilename());
            oldFile.delete();
            try {
                FileCopyUtils.copy(multipartFile.getBytes(), new File(ateImagesFolder + "/" + multipartFile.getOriginalFilename()));
                filename = multipartFile.getOriginalFilename();
            } catch (IOException ex) {
                logger.error("Exception on file saveing in disk2:", ex);
            }

        }
        return filename;
    }

    public void downloadFile(HttpServletResponse response, String attachedFilename) {
        if (attachedFilename != null) {
            String fileType = FilenameUtils.getExtension(ateImagesFolder + "/" + attachedFilename);
            Common.downloadReport(response, ateImagesFolder + "/", attachedFilename, fileType);
        }
    }

    public boolean deleteFile(long questionId) {
        return expertQuestionsDAO.deletdAttachedFile(questionId);
    }
}
