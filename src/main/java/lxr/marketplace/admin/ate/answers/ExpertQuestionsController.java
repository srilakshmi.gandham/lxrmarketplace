/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.ate.answers;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.user.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
public class ExpertQuestionsController {

    @Autowired
    ExpertQuestionsService expertQuestionsService;
    Login user = null;
    ExpertUser expertUser = null;
    boolean userLoggedIn = false;

    @RequestMapping(value = "/ate-question.html", method = RequestMethod.GET)
    private String inIt(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws IOException {
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                user = (Login) session.getAttribute("user");
                if (user.isAdmin() && session.getAttribute("expertUser") != null) {
                    expertUser = (ExpertUser) session.getAttribute("expertUser");
                    int[] data = expertQuestionsService.getHighLevelInfo(expertUser.getExpertId());
                    model.addAttribute("unAssignedQuestions", data[0]);
                    model.addAttribute("assignedQuestions", data[1]);
                }
            }
        }

        if (!userLoggedIn || user == null || !user.isAdmin()) {
            response.sendRedirect("/");
            return null;
        }

        return "view/admin/ateQuestions/ateQuestionsList";
    }

    @RequestMapping(value = "/ate-question.html", params = "fetch=ate-questions", method = RequestMethod.GET)
    @ResponseBody
    public List<ExpertQuestionsDTO> getAllQuestions(HttpServletRequest request, HttpServletResponse response,
            HttpSession session, @RequestParam("filteredType") int filteredType, @RequestParam("filteredQuery") String filteredQuery) {

        List<ExpertQuestionsDTO> questionsList = null;
        if (expertUser != null) {
            questionsList = expertQuestionsService.getAllQuestions(expertUser.getExpertId(), filteredType, filteredQuery);
        }
        return questionsList;
    }

    @RequestMapping(value = "/ate-question.html", params = "action=menu", method = RequestMethod.POST)
    public @ResponseBody
    String navigationToMenuBar(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("menuOption") String menuOption) {
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                user = (Login) session.getAttribute("user");
                if (user.isAdmin() && session.getAttribute("expertUser") != null) {
                    expertUser = (ExpertUser) session.getAttribute("expertUser");
                }
            }
        }

        String naviagtionUrl = null;
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (userLoggedIn && user.isAdmin() && (expertUser == null || expertUser.getRole().equals("admin"))) {
            switch (menuOption) {
                case "dashBoard":
                    naviagtionUrl = url + "/dashboard.html";
                    break;
                case "autoMail":
                    naviagtionUrl = url + "/automail.html";
                    break;
                case "settings":
                    naviagtionUrl = url + "/settings.html";
                    break;
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "expertUser":
                    naviagtionUrl = url + "/expert-user.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
                case "pluginAdoption":
                    naviagtionUrl = url + "/plugin-adoption.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("reviewer")) {

            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("expert")) {
            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else {
            naviagtionUrl = url;
        }

        return naviagtionUrl;

    }

}
