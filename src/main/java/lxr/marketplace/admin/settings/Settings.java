package lxr.marketplace.admin.settings;

public class Settings {
	private int filterId;
	private String filterName;
	private String filterNameDomain;
	private int filterOption;
	private int restrictionType;
	private String[] deleted;
//	SettingsRow settingsRow;
//	List<SettingsRow> filterRecords;
//	String ipOrDomain;
	
	public String getFilterName() {
		if(filterName != null){
			filterName = filterName.trim();
			}
		return filterName;
	}
	public String getFilterNameDomain() {
		if(filterNameDomain != null){
			filterNameDomain = filterNameDomain.trim();
			}
		return filterNameDomain;
	}
	public void setFilterNameDomain(String filterNameDomain) {
		this.filterNameDomain = filterNameDomain.trim();
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName.trim();
	}
	public int getFilterId() {
		return filterId;
	}
	public void setFilterId(int filterId) {
		this.filterId = filterId;
	}
	public String[] getDeleted() {
		return deleted;
	}
	public void setDeleted(String[] deleted) {
		this.deleted = deleted;
	}
	public int getFilterOption() {
		return filterOption;
	}
	public void setFilterOption(int filterOption) {
		this.filterOption = filterOption;
	}
	public int getRestrictionType() {
		return restrictionType;
	}
	public void setRestrictionType(int restrictionType) {
		this.restrictionType = restrictionType;
	}
}
