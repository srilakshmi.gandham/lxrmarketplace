package lxr.marketplace.admin.settings;

import java.util.ArrayList;
import java.util.List;


import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class URLRedirectionForm {
    private List redirections = 
            LazyList.decorate(new ArrayList<URLRedirection>(),FactoryUtils.instantiateFactory(URLRedirection.class));
    private String newUrl;
    private String newReUrl;
    
    public URLRedirectionForm(){super();}
    
    URLRedirectionForm(String newUrl, String newReUrl){
        this.newUrl = newUrl;
        this.newReUrl = newReUrl;
    }
    public List<URLRedirection> getRedirections() {
        return redirections;
    }

    public void setRedirections(List<URLRedirection> redirections) {
        this.redirections = redirections;
    }

    public String getNewUrl() {
        return newUrl;
    }

    public void setNewUrl(String newUrl) {
        this.newUrl = newUrl;
    }

    public String getNewReUrl() {
        return newReUrl;
    }

    public void setNewReUrl(String newReUrl) {
        this.newReUrl = newReUrl;
    }
}
