package lxr.marketplace.admin.settings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class URLRedirectionService extends JdbcDaoSupport {

    public List<URLRedirection> fetchAdminURLRedirections() {
        String query = "SELECT * FROM admin_redirect ORDER BY status DESC, added_date DESC";
        List<URLRedirection> uRLRedirections = null;
        try {
            uRLRedirections = getJdbcTemplate().query(
                    query, new RowMapper<URLRedirection>() {
                        public URLRedirection mapRow(ResultSet rs, int rowNum)throws SQLException {
                            URLRedirection urlR = new URLRedirection();
                            urlR.setId(rs.getLong("id"));
                            urlR.setUrl(rs.getString("URL"));
                            urlR.setRedirectionURL(rs.getString("redirect_URL"));
                            urlR.setStatus(rs.getBoolean("status"));
                            return urlR;
                        }
                    });          
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return uRLRedirections;
    }
    
    public boolean insertAdminURLRedirection(URLRedirection uRLRedirection){
        logger.info("Inserting new Redirection URL from Admin Panel");
        boolean inserted = false;
        String insertQuery = "INSERT INTO admin_redirect (URL, redirect_URL, added_date, status) VALUES (?, ?, ?, ?)";
        Object[] valLis = new Object[4];

        valLis[0] = uRLRedirection.getUrl();
        valLis[1] = uRLRedirection.getRedirectionURL();
        valLis[2] = new java.sql.Timestamp(uRLRedirection.getAddedDate().getTimeInMillis());
        valLis[3] = uRLRedirection.isStatus();
        
        try {
            getJdbcTemplate().update(insertQuery, valLis);
            inserted = true;
            logger.info("New Redirection URL from Admin Panel is inserted");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inserted;
    }
    
    public boolean updateAdminURLRedirectionStatus(URLRedirection uRLRedirection){
        boolean updated = false;
        String updateQuery = "UPDATE admin_redirect SET status="+ uRLRedirection.isStatus() +" WHERE id = " +
                uRLRedirection.getId();
        try {
            getJdbcTemplate().update(updateQuery);
            updated = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return updated;
    }
}
