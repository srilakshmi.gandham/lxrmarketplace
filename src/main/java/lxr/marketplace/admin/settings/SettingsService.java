package lxr.marketplace.admin.settings;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;



public class SettingsService extends JdbcDaoSupport {
	private static Logger logger = Logger.getLogger(SettingsService.class);
	
	
	public List<Settings> getTotalFilterRecords(HttpSession session){
		logger.info("in setting service class"+getJdbcTemplate());
		String query;
		List<Settings> filterRecords;
		query = "select * from  localuser_filter where is_deleted = 0";
		 try {
			     filterRecords = getJdbcTemplate().query(query, new RowMapper<Settings>() {			    
				 public Settings mapRow(ResultSet rs, int rowNum) throws SQLException {
					 Settings settings = new Settings();
					 settings.setFilterId(rs.getInt("filter_id"));
					 settings.setFilterName(rs.getString("filter"));
					 settings.setFilterOption(rs.getInt("filter_option"));
					 if (rs.getInt("filter_option") == 1) {
						 settings.setFilterName(rs.getString("filter")); 
						 settings.setFilterNameDomain("");
					 } else if (rs.getInt("filter_option") == 2) {
						 settings.setFilterNameDomain(rs.getString("filter")); 
						 settings.setFilterName("");
					 }
					 settings.setRestrictionType(rs.getInt("restriction_type"));
//					 settings.setDeleted(rs.getBoolean("is_deleted"));
					 return settings;
				 }
			 });
			     return filterRecords;
          }catch (Exception e) {	           
	            e.printStackTrace();
//	            logger.error(e);
//	            logger.error(e.getCause());
	            return null;
	     }  
	  }
	public boolean updateFilter(Settings settings, HttpServletRequest request){
		String query = "select count(filter_id) from localuser_filter where filter=? and filter_id <> "+settings.getFilterId()+" and is_deleted=0";	
		Object [] args = {settings.getFilterName()+settings.getFilterNameDomain()};
		int [] argTypes = {Types.VARCHAR};
//		logger.info("Check Existance query: " +query);		
		if(!checkExistance(query, args, argTypes)){
			updateFiltertoDB(settings);
			return true;	
	    }
		return false;
  }
	public boolean checkExistance(String sql, Object [] args, int [] argTypes){
		 int n = getJdbcTemplate().queryForInt(sql, args, argTypes);
		 if(n == 0){
			 return false;
		 }else{
			 return true;
		 }
	 }
	 public void updateFiltertoDB(final Settings settings){
		 try {
		 logger.info("in updateFiltertoDB()");
		 final String query = "update localuser_filter set filter=?, filter_option = ?, restriction_type = ? where filter_id = ?";
		 Object[] valLis = new Object[4];
		 valLis[0]="";
		 if(settings.getFilterOption() == 1){
			 valLis[0] =  settings.getFilterName();
		 }else if(settings.getFilterOption() == 2){
			 valLis[0] = settings.getFilterNameDomain();
		 }
	     valLis[1] = settings.getFilterOption();
	     valLis[2] = settings.getRestrictionType();
	     valLis[3] = settings.getFilterId();
	     
	     
	            getJdbcTemplate().update(query, valLis);
	     } catch (Exception ex) {
	            ex.printStackTrace();
	     }
	 }
		public boolean addNewFilter(Settings settings, HttpServletRequest request){		
			String query = "select count(filter_id) from localuser_filter where filter=? and is_deleted=0";	
			Object [] args = {settings.getFilterName()+settings.getFilterNameDomain()};
			int [] argTypes = {Types.VARCHAR};
			if(!checkExistance(query, args, argTypes)){
				addFiltertoDB(settings);
				return true;
			}
			return false;
		}
	 public void addFiltertoDB(final Settings settings){
		try {		 
			 String filtName = settings.getFilterName();
			 String[] filtNames = null;
			 if(filtName != null && !filtName.equals("")){
				 filtNames = filtName.split("\n");
				 if(filtNames != null){
						insertBatch(settings, filtNames, 1);
				 }
			 }
			String filtDomainName = settings.getFilterNameDomain();
			if(filtDomainName != null && !filtDomainName.equals("")){
				String[]  filtDomainNames = filtDomainName.split("\n");
				 if(filtDomainNames != null){
					 insertBatch(settings, filtDomainNames, 2);	
			 }
			}	    
		            
		    } catch (Exception ex) {
		       ex.printStackTrace();
		   }
		
		 }
	 
	 public void updateDeletedStatustoDB(final Settings settings,HttpSession session,String[] deletedIds){
		try {	
			logger.info("updateDeletedStatustoDB method");
			if(deletedIds != null && deletedIds.length > 0){
				deleteBatch(settings,deletedIds);
			}
	            
		    }catch (Exception ex) {
		       ex.printStackTrace();
		   }
		 }
		 
		 public void insertBatch(final Settings settings, final String[] filtNames, final int filterOption) {
			 final String query = "insert into localuser_filter (filter, filter_option, restriction_type) values (?,?,?)";
				 
			 getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
				 
				 	public void setValues(PreparedStatement ps, int i) throws SQLException {
							
						ps.setString(1, filtNames[i]);
						ps.setInt(2, filterOption);
						ps.setInt(3, settings.getRestrictionType() );
					}
				 
				 	public int getBatchSize() {
						return filtNames.length;
					}
				  });
		 }
		 public void deleteBatch(final Settings settings, final String[] deletedIds) {
			 final String query = "update localuser_filter set is_deleted = ? where filter_id = ?";
			logger.info("in deleteBatch() ");	 
			 getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
				 
				    public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setBoolean(1,true);
						ps.setInt(2,Integer.parseInt(deletedIds[i]));
					}
				 
				    public int getBatchSize() {
						return deletedIds.length;
					}
				  });
		     }

			public List<String> getDiscardedIps() {
				List<String> discardedIps = new ArrayList<String>();
				String query = "select * from localuser_filter where restriction_type = 3 and filter_option=1 and is_deleted=0";
				logger.info("in getDiscardedIps( )");
				try {
					SqlRowSet tempDiscardedIps = getJdbcTemplate().queryForRowSet(query);
					if(tempDiscardedIps != null){
					while (tempDiscardedIps.next()) {
						discardedIps.add(tempDiscardedIps.getString("filter"));
					}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return discardedIps;
			}
}
