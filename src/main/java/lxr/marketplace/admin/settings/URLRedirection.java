package lxr.marketplace.admin.settings;

import java.util.Calendar;

public class URLRedirection {
    private long id;
    private String url;
    private String redirectionURL;
    private boolean status;
    private Calendar addedDate;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getRedirectionURL() {
        return redirectionURL;
    }
    public void setRedirectionURL(String redirectionURL) {
        this.redirectionURL = redirectionURL;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public Calendar getAddedDate() {
        return addedDate;
    }
    public void setAddedDate(Calendar addedDate) {
        this.addedDate = addedDate;
    }   
}
