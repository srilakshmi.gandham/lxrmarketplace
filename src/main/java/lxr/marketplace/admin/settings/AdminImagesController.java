package lxr.marketplace.admin.settings;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.report.Report;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class AdminImagesController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(AdminImagesController.class);
    private LoginService loginService;
    private AdminImageService adminImageService;
    String adminImagesFolder, adminTempImgsFolder;

    public LoginService getLoginService() {
        return loginService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public AdminImageService getAdminImageService() {
        return adminImageService;
    }

    public void setAdminImageService(AdminImageService adminImageService) {
        this.adminImageService = adminImageService;
    }

    public String getAdminImagesFolder() {
        return adminImagesFolder;
    }

    public void setAdminImagesFolder(String adminImagesFolder) {
        this.adminImagesFolder = adminImagesFolder;
    }

    public AdminImagesController() {
        setCommandClass(AdminImages.class);
        setCommandName("adminimages");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        logger.info("in side of automailImages");
        AdminImages adminimages = (AdminImages) command;
        Login user = (Login) session.getAttribute("user");
        String url = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort();
        session.setAttribute("domain", url);
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            String fileName = "";
            if (session.getAttribute("fileName") != null) {
                fileName = session.getAttribute("fileName").toString();
            }
            session.setAttribute("adminimages", adminimages);
            if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.info("moving to dashboard");
                ModelAndView mAndV = new ModelAndView("view/admin/dashboard/Dashboard");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "report")) {
                logger.info("moving to report tab");
                Report report = new Report();
                report = new Report();
                ModelAndView mAndV = new ModelAndView("view/admin/report/Report", "report", report);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "automail")) {
                response.sendRedirect(url + "/automail.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
                logger.info("Uploading of file started at : " + Calendar.getInstance().getTime());
                fileName = adminImageService.saveFile(adminImagesFolder, request, response);
                if (fileName != null) {
                    session.setAttribute("fileName", fileName);
                    logger.info(fileName + "filename");
                    String tempFile = adminImagesFolder + fileName;
                    Set<String> totalImgNames = new HashSet<String>();
                    totalImgNames.add(tempFile);
                    session.setAttribute("totalImgNames", totalImgNames);
                }
                String[] attachedFiles = adminImageService.getListOfFiles();
                if (session.getAttribute("attachedFiles") != null) {
                    session.removeAttribute("attachedFiles");
                }
                session.setAttribute("attachedFiles", attachedFiles);
            } else if (WebUtils.hasSubmitParameter(request, "getAllFiles")) {
                logger.info("in getallfiles ");
                String[] attachedFiles = adminImageService.getListOfFiles();
                if (session.getAttribute("attachedFiles") != null) {
                    session.removeAttribute("attachedFiles");
                }
                session.setAttribute("attachedFiles", attachedFiles);
                ModelAndView mAndV = new ModelAndView("view/admin/settings/AdminImages", "adminimages", adminimages);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "removeFile")) {
                String fileName1 = WebUtils.findParameterValue(request, "removeFile");
                adminImageService.deleteFile(fileName1, adminImagesFolder);
                String[] attachedFiles = adminImageService.getListOfFiles();
                if (session.getAttribute("attachedFiles") != null) {
                    session.removeAttribute("attachedFiles");
                }
                session.setAttribute("attachedFiles", attachedFiles);
                ModelAndView mAndV = new ModelAndView("view/admin/settings/AdminImages", "adminimages", adminimages);
                return mAndV;
            }
        } else {
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }
        ModelAndView mAndV = new ModelAndView("view/admin/settings/AdminImages", "adminimages", adminimages);
        return mAndV;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        AdminImages adminimages = (AdminImages) session.getAttribute("adminimages");
        if (adminimages == null) {
            adminimages = new AdminImages();
            String[] attachedFiles = adminImageService.getListOfFiles();
            if (session.getAttribute("attachedFiles") != null) {
                session.removeAttribute("attachedFiles");
            }
            session.setAttribute("attachedFiles", attachedFiles);
        }
        return adminimages;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

}
