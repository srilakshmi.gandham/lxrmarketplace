package lxr.marketplace.admin.settings;

import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.report.Report;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class SettingsController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(SettingsController.class);
    private LoginService loginService;
    private SettingsService settingsService;

    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public SettingsController() {
        setCommandClass(Settings.class);
        setCommandName("settings");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Settings settings = (Settings) command;
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            session.setAttribute("settings", settings);
            if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.info("moving to dashboard");
                ModelAndView mAndV = new ModelAndView("view/admin/dashboard/Dashboard");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "report")) {
                logger.info("moving to report tab");
                Report report = new Report();
                report = new Report();
                ModelAndView mAndV = new ModelAndView("view/admin/report/Report", "report", report);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "automail")) {
                logger.info("moving to automail tab");
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "saveFilter")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                String param = WebUtils.findParameterValue(request, "saveFilter");
                if (param.equals("update")) {
                    logger.info("in update");
                    settingsService.updateFiltertoDB(settings);
                } else if (param.equals("savenew")) {//for adding new template
                    settingsService.addFiltertoDB(settings);
                }
                List<Settings> totRecords1 = settingsService.getTotalFilterRecords(session);
                if (session.getAttribute("totRecords") != null) {
                    session.removeAttribute("totRecords");
                    session.setAttribute("totRecords", totRecords1);
                } else {
                    session.setAttribute("totRecords", totRecords1);
                }
                response.sendRedirect(url + "/settings.html");
            } else if (WebUtils.hasSubmitParameter(request, "reload")) {
                ServletContext ctx = request.getServletContext();
//                    logger.info((List)ctx.getAttribute("localIps"));
                ctx.setAttribute("localIps", loginService.fetchFiltersForUsersByIp());
                ctx.setAttribute("discardedIps", settingsService.getDiscardedIps());
                logger.info((List) ctx.getAttribute("localIps"));
                logger.info((List) ctx.getAttribute("discardedIps"));
            } else if (WebUtils.hasSubmitParameter(request, "delete")) {
                logger.info("temId is= " + settings.getDeleted()[0]);
                String[] deletedIds = settings.getDeleted();
                settingsService.updateDeletedStatustoDB(settings, session, deletedIds);
                List<Settings> totRecords2 = settingsService.getTotalFilterRecords(session);
                if (session.getAttribute("totRecords") != null) {
                    session.removeAttribute("totRecords");
                    session.setAttribute("totRecords", totRecords2);
                } else {
                    session.setAttribute("totRecords", totRecords2);
                }
            }
        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }
        ModelAndView mAndV = new ModelAndView("view/admin/settings/Settings",
                "settings", settings);
        return mAndV;
    }

    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        Settings settings = (Settings) session.getAttribute("settings");
        if (settings == null) {
            settings = new Settings();
            List<Settings> totRecords = settingsService.getTotalFilterRecords(session);
            session.setAttribute("totRecords", totRecords);
        }
        return settings;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

}
