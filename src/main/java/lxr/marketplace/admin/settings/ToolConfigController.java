package lxr.marketplace.admin.settings;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.report.Report;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class ToolConfigController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(ToolConfigController.class);
    private ToolService toolService;

    public void setToolConfigService(ToolConfigService toolConfigService) {
    }

    public void setLoginService(LoginService loginService) {
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    public ToolConfigController() {
        setCommandClass(ToolConfig.class);
        setCommandName("toolconfig");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        ToolConfig toolconfig = (ToolConfig) command;
        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            session.setAttribute("toolconfig", toolconfig);
            if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.debug("moving to dashboard");
                ModelAndView mAndV = new ModelAndView(
                        "view/admin/dashboard/Dashboard");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "report")) {
                logger.debug("moving to report tab");
                Report report = new Report();
                report = new Report();
                ModelAndView mAndV = new ModelAndView(
                        "view/admin/report/Report", "report", report);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "automail")) {
                logger.debug("moving to automail tab");
                String url = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "reload")) {
                ServletContext ctx = request.getServletContext();
                logger.debug((Map<Long, Tool>) ctx.getAttribute("tools"));
                ctx.setAttribute("tools", toolService.populateTools());
                ctx.setAttribute("relatedTools", toolService.populateRelatedTools());
            }
        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }
        ModelAndView mAndV = new ModelAndView("view/admin/settings/ToolConfig", "toolconfig", toolconfig);
        return mAndV;
    }

    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        ToolConfig toolconfig = (ToolConfig) session.getAttribute("toolconfig");
        if (toolconfig == null) {
            toolconfig = new ToolConfig();
        }
        return toolconfig;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }
}
