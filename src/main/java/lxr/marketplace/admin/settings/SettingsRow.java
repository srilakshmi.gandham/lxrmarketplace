package lxr.marketplace.admin.settings;

public class SettingsRow {
	String filterName;
	int filterId;
	int filterOption;
	int restrictionType;
	boolean deleted;
	public String getFilterName() {
		return filterName;
	}
	public int getFilterId() {
		return filterId;
	}
	public int getFilterOption() {
		return filterOption;
	}
	public int getRestrictionType() {
		return restrictionType;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	public void setFilterId(int filterId) {
		this.filterId = filterId;
	}
	public void setFilterOption(int filterOption) {
		this.filterOption = filterOption;
	}
	public void setRestrictionType(int restrictionType) {
		this.restrictionType = restrictionType;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	

}
