package lxr.marketplace.admin.settings;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.report.Report;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class URLRedirectionController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(ToolConfigController.class);
    private URLRedirectionService uRLRedirectionService;

    public void setuRLRedirectionService(URLRedirectionService uRLRedirectionService) {
        this.uRLRedirectionService = uRLRedirectionService;
    }

    public URLRedirectionController() {
        setCommandClass(URLRedirectionForm.class);
        setCommandName("urlredirectionform");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors) throws Exception {
        HttpSession session = request.getSession(true);
        URLRedirectionForm urlredirectionform = (URLRedirectionForm) command;
        Login user = (Login) session.getAttribute("user");
        ModelAndView mAndV = null;
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            session.setAttribute("urlredirectionform", urlredirectionform);
            if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.debug("moving to dashboard");
                mAndV = new ModelAndView("view/admin/dashboard/Dashboard");
            } else if (WebUtils.hasSubmitParameter(request, "report")) {
                logger.debug("moving to report tab");
                Report report = new Report();
                report = new Report();
                mAndV = new ModelAndView("view/admin/report/Report", "report", report);
            } else if (WebUtils.hasSubmitParameter(request, "automail")) {
                logger.debug("moving to automail tab");
                String url = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "action")) {
                logger.debug("New URL: " + urlredirectionform.getNewUrl() + " Redirection URL: "
                        + urlredirectionform.getNewReUrl());
                String actionParam = request.getParameter("action");
                mAndV = new ModelAndView("view/admin/settings/URLRedirection", "urlredirectionform", urlredirectionform);
                if (actionParam != null && actionParam.equals("addNew")) {
                    String appUrl = request.getScheme() + "://" + request.getServerName() + ":"
                            + request.getServerPort();

                    String tempNewReURL = urlredirectionform.getNewReUrl().trim();
                    String formattedNewReURL;
                    if (!tempNewReURL.contains("http://")) {
                        tempNewReURL = "http://" + tempNewReURL;
                    }
                    try {
                        URL testURL = new URL(tempNewReURL);
                        if (testURL.getPath() != null && !testURL.getPath().equals("")) {
                            if (testURL.getQuery() != null && !testURL.getQuery().equals("null")) {
                                formattedNewReURL = testURL.getPath() + "?" + testURL.getQuery();
                            } else {
                                formattedNewReURL = testURL.getPath();
                            }
                        } else if (!urlredirectionform.getNewReUrl().trim().startsWith("/")) {
                            formattedNewReURL = "/" + urlredirectionform.getNewReUrl().trim();
                        } else {
                            formattedNewReURL = urlredirectionform.getNewReUrl().trim();
                        }
                    } catch (MalformedURLException e) {
                        if (!urlredirectionform.getNewReUrl().trim().startsWith("/")) {
                            formattedNewReURL = "/" + urlredirectionform.getNewReUrl().trim();
                        } else {
                            formattedNewReURL = urlredirectionform.getNewReUrl().trim();
                        }
                    }
                    String newReUrl = appUrl + formattedNewReURL;
                    logger.debug("Formatted URL: " + formattedNewReURL);
                    int statusCode = Common.getFirstURLStatusCode(newReUrl);
                    if (statusCode == 0 || statusCode >= 400) {
                        mAndV.addObject("messageFailure", "Redirection URL is not working.");
                    } else {
                        URLRedirection url = new URLRedirection();
                        String tempNewURL = urlredirectionform.getNewUrl().trim();
                        String formattedNewURL;
                        if (!tempNewURL.contains("http://")) {
                            tempNewURL = "http://" + tempNewURL;
                        }
                        try {
                            URL testURL = new URL(tempNewURL);
                            if (testURL.getPath() != null && !testURL.getPath().equals("")) {
                                formattedNewURL = testURL.getPath();
                            } else if (!urlredirectionform.getNewUrl().trim().startsWith("/")) {
                                formattedNewURL = "/" + urlredirectionform.getNewReUrl().trim();
                            } else {
                                formattedNewURL = urlredirectionform.getNewReUrl().trim();
                            }
                        } catch (MalformedURLException e) {
                            if (!urlredirectionform.getNewUrl().trim().startsWith("/")) {
                                formattedNewURL = "/" + urlredirectionform.getNewUrl().trim();
                            } else {
                                formattedNewURL = urlredirectionform.getNewUrl().trim();
                            }
                        }
                        if (formattedNewURL.contains("?")) {
                            formattedNewURL = formattedNewURL.substring(0, formattedNewURL.indexOf('?'));
                        }

                        url.setUrl(formattedNewURL);
                        url.setRedirectionURL(formattedNewReURL);
                        url.setAddedDate(Calendar.getInstance());
                        url.setStatus(true);
                        uRLRedirectionService.insertAdminURLRedirection(url);
                        List<URLRedirection> redirections = uRLRedirectionService.fetchAdminURLRedirections();

                        urlredirectionform.setRedirections(redirections);
                        urlredirectionform.setNewUrl("");
                        urlredirectionform.setNewReUrl("");

                        mAndV.addObject("messageSuccess", "Redirection URL is successfully updated.");

                        ServletContext ctx = request.getServletContext();
                        ctx.setAttribute("adminURLRedirections", redirections);
                    }
                } else if (actionParam != null && actionParam.equals("changeStatus")) {
                    String urlIdString = request.getParameter("urlId");
                    try {
                        long id = Long.parseLong(urlIdString);
                        for (URLRedirection url : urlredirectionform.getRedirections()) {
                            if (url.getId() == id) {
                                if (url.isStatus()) {
                                    url.setStatus(false);
                                } else {
                                    url.setStatus(true);
                                }
                                uRLRedirectionService.updateAdminURLRedirectionStatus(url);
                                List<URLRedirection> redirections = uRLRedirectionService.fetchAdminURLRedirections();
                                ServletContext ctx = request.getServletContext();
                                ctx.setAttribute("adminURLRedirections", redirections);
                            }
                        }
                    } catch (NumberFormatException e) {
                        logger.error("Invalid Request: no url Id to change status");
                    }
                    mAndV = null;
                }
            } else {
                mAndV = new ModelAndView("view/admin/settings/URLRedirection", "urlredirectionform", urlredirectionform);
            }
        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
        }
        return mAndV;
    }

    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        URLRedirectionForm urlredirectionform = (URLRedirectionForm) session.getAttribute("urlredirectionform");
        if (urlredirectionform == null) {
            List<URLRedirection> redirections = uRLRedirectionService.fetchAdminURLRedirections();
            urlredirectionform = new URLRedirectionForm("", "");
            urlredirectionform.setRedirections(redirections);
        }
        return urlredirectionform;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }
}
