package lxr.marketplace.admin.settings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//
//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.FileItemFactory;
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.IOUtils;


import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class AdminImageService extends JdbcDaoSupport{
	private static Logger logger = Logger.getLogger(AdminImageService.class);
	
	  String adminImagesFolder , adminTempImgsFolder;

    public String getAdminImagesFolder() {
		return adminImagesFolder;
	}

   public void setAdminImagesFolder(String adminImagesFolder) {
		this.adminImagesFolder = adminImagesFolder;
	}

	public static Logger getLogger() {
		return logger;
	}
	
	public String saveFile(String realPath, HttpServletRequest request,
			HttpServletResponse response) {
		final String headerFileName = "X-File-Name";
		String fileName = "";
		InputStream is = null;
		FileOutputStream fos = null;
		String [] files = getListOfFiles();
		boolean alreadyExist = false;
		if (request.getHeader(headerFileName) != null) {
			fileName = request.getHeader(headerFileName);
			logger.info("uploaded file name: " + fileName);
			if (fileName.trim().contains(" ")) {
				fileName = fileName.replace(" ", "%");
			}
			fileName = decodeFileName(fileName);
			for(String file : files){
				if(file.equals(fileName)){
					alreadyExist = true;
				}
			}if(!alreadyExist){
				try {
					is = request.getInputStream();
					File f = new File(realPath + fileName);
					fos = new FileOutputStream(f);
					IOUtils.copy(is, fos);
					response.setContentType("application/json");
					response.setStatus(HttpServletResponse.SC_OK);
				} catch (Exception e) {
					logger.error(e.getMessage());
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				} finally {
					try {
						fos.close();
						is.close();
					} catch (IOException ignored) {
					}
				}
			}
		} else if (request.getParameter("fileName") != null) {
			fileName = request.getParameter("fileName");
			logger.info("Uploaded File Name: " + fileName);

			response.setContentType("text/html");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "No-cache");
			response.setDateHeader("Expires", 0);
			final long permitedSize = 314572800;
			long fileSize;
			try {
				boolean isMultipart = ServletFileUpload
						.isMultipartContent(request);
				if (isMultipart) {
					FileItemFactory factory = new DiskFileItemFactory();
					ServletFileUpload upload = new ServletFileUpload(factory);
					List items = upload.parseRequest(request);
					for (int i = 0; i < items.size(); i++) {
						FileItem fileItem = (FileItem) items.get(i);
						if (fileItem.isFormField()) {
							//String any_parameter = fileItem.getString();
						} else {
							fileName = fileItem.getName();
							fileName = decodeFileName(fileName);
							for(String file : files){
								if(file.equals(fileName)){
									alreadyExist = true;
								}
							}
							if(!alreadyExist){
								fileSize = fileItem.getSize();
								if (fileSize <= permitedSize) {
									File uploadedFile = new File(realPath + fileName);
									fileItem.write(uploadedFile);
								}else{
									logger.info("file is too big");
									String message = "File size of '"+fileName+"' is more than the maximum file size limit";
									addInJson(message, response);
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		if(alreadyExist){
			String message = "A file with name '"+fileName+"' already attached to this template mail.";
			addInJson(message, response);
		}
		return fileName;
	}
	
	public String[] getListOfFiles(){
		String[] attachments = null;
		File source = new File(adminImagesFolder);
		if(source.isDirectory()){
			attachments = source.list();
		}		
		return attachments;
	}
	
	public String decodeFileName(String fileName){
		String newfile = null;
		ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        fileName = fileName.replaceAll("'","\\\\'");
        try {
        	newfile = (String) engine.eval("decodeURIComponent('"+fileName+"')");
			//engine.eval("print(encodeURIComponent("+fileName+"))");
			//logger.info(engine.eval("print(encodeURIComponent("+fileName+"))"));
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return newfile;
	}
	
	public void deleteFile(String fileName , String folder){
		String newfile= decodeFileName(fileName);
		
		File f = new File(folder+newfile);
		if(f.exists()){
			f.delete();
        }else{
        	logger.info("There is no file to remove from folder with name "+ newfile);
        }
	}
	

	
   public void addInJson(String message, HttpServletResponse response){
        JSONArray arr = null;
		arr = new JSONArray();
		PrintWriter writer = null;
		
		try {
			writer = response.getWriter();
	        arr.add(0, message);
	        writer.print(arr);    
		} catch (Exception e) {
			logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");         
		}finally {
			writer.flush();
            writer.close();        
		}
	}

}
