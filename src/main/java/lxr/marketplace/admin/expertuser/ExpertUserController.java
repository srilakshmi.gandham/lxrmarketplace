/**
 * @author NE16T1213
 */
package lxr.marketplace.admin.expertuser;

import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.user.Login;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class ExpertUserController {

    private static final Logger logger = Logger.getLogger(ExpertUserController.class);

    @Autowired
    private ExpertUserService expertUserService;

    Login user = null;
    ExpertUser expertUser = null;
    boolean userLoggedIn = false;

    /*
    * Request to handle the navigation to expert user page.
    * @return : Expert user's page.
     */
    @RequestMapping(value = "/expert-user.html", method = RequestMethod.GET)
    public String navigationToExpertUser(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
        if (session.getAttribute("userLoggedIn") != null) {
            userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn) {
                user = (Login) session.getAttribute("user");
                if (user.isAdmin()) {
                    expertUser = (ExpertUser) session.getAttribute("expertUser");
                }

            }
        }
        if (!userLoggedIn || user == null || !user.isAdmin()) {
            response.sendRedirect("/");
            return null;
        }

        return "view/admin/expertUser/expertUser";
    }

    /*
    * Request to handle to add/edit the expert user data.
    * @return :  Expert user page.
     */
    @RequestMapping(value = "/expert-user.html", params = "requestMethod=adminUserFeed",
            method = RequestMethod.POST)
    public @ResponseBody
    Object[] saveExpertUserFeed(HttpSession session, Model model, HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam("name") String expertName, @RequestParam("email") String expertEmail,
            @RequestParam("role") String expertRole, @RequestParam("updating") boolean isUpdating,
            @RequestParam("expertId") long expertID) {
        String lxrmHostingURL = request.getScheme() + "://" + request.getServerName();
        List<ExpertUser> expertUsersList = null;
        String adminMessage = null;
        ExpertUser existedUser = null;
        Object[] responseData = new Object[2];
        if (session.getAttribute("expertUsersList") != null) {
            expertUsersList = (List<ExpertUser>) session.getAttribute("expertUsersList");
        } else {
            expertUsersList = expertUserService.getExpertUserList();
            session.setAttribute("expertUsersList", expertUsersList);
        }

        existedUser = expertUsersList.stream()
                .filter(expertUserFeed -> expertUserFeed.getEmail().equalsIgnoreCase(expertEmail))
                .findAny().orElse(null);
        /*Searching for expert user is existed or not.*/
        if (!isUpdating) {
            if (existedUser != null) {
                responseData[0] = "User Existed";
                return responseData;
            } else {
                existedUser = new ExpertUser();
                existedUser.setName(expertName);
                existedUser.setEmail(expertEmail);
                existedUser.setRole(expertRole);
            }
        } else if (isUpdating) {
            if (existedUser != null) {
                /*Checking for user name is modified or not.*/
                if ((!existedUser.getName().equalsIgnoreCase(expertName))
                        && (existedUser.getExpertId() == expertID)) {
                    if (expertUserService.updateLXRMUserName(expertName, expertEmail)) {
                        logger.info("Successfully Updated the user name as " + expertName + ""
                                + "in user table of LXRMarketplace for expert user" + existedUser.getExpertId());
                    } else {
                        logger.info("Updation failed the user name as " + expertName
                                + "in user table of LXRMarketplace for expert user" + existedUser.getExpertId());
                    }
                }
            }
            existedUser.setUpdating(isUpdating);
            existedUser.setName(expertName);
            existedUser.setRole(expertRole);
            existedUser.setExpertId(expertID);
        }
        session.removeAttribute("adminMessage");

        /* Inserting/Updating expert user data.*/
        adminMessage = expertUserService.updateExpertUserData(existedUser, lxrmHostingURL);
        session.setAttribute("adminMessage", adminMessage);
        responseData[0] = adminMessage;
        responseData[1] = lxrmHostingURL + ":" + request.getServerPort();
        return responseData;
    }

    /*
    * Request to handle the fetch expert users.
    * @return : List of ExpertUsers data.
     */
    @RequestMapping(value = "/expert-user.html", params = "requestmethod=expertUsersList",
            method = RequestMethod.GET)
    @ResponseBody
    public List<ExpertUser> getExpertAnswers(HttpServletRequest request,
            HttpServletResponse response, HttpSession session) {
        List<ExpertUser> expertUsersList = expertUserService.getExpertUserList();
        session.setAttribute("expertUsersList", expertUsersList);
        return expertUsersList;
    }

    /*
    * Request to handle the view expert user data.
    * @return : Selected Expert User data.
     */
    @RequestMapping(value = "/expert-user.html", params = "requestmethod=viewExpertUser",
            method = RequestMethod.GET)
    @ResponseBody
    public ExpertUser viewExpertUserContent(HttpServletRequest request, HttpServletResponse response,
            HttpSession session, @RequestParam("selectedExpertID") long expertUserId) {
        try {
            Login user = (Login) session.getAttribute("user");
            boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
            if (userLoggedIn && user.isAdmin()) {
                List<ExpertUser> expertUsersList = null;
                ExpertUser viewExpertUser = null;
                /*Searching for expert user is existed or not.*/
                if (session.getAttribute("expertUsersList") != null) {
                    expertUsersList = (List<ExpertUser>) session.getAttribute("expertUsersList");
                } else {
                    expertUsersList = expertUserService.getExpertUserList();
                }

                viewExpertUser = expertUsersList.stream()
                        .filter(expertUser -> (expertUser.getExpertId() == expertUserId))
                        .findAny().orElse(null);
                if (viewExpertUser != null) {
                    return viewExpertUser;
                }

            } else {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/lxrmarketplace.html");
                return null;
            }
        } catch (IOException ie) {
            logger.error("Exception in viewExpertUserContent" + ie);
        }
        return null;
    }

    /*
    * Request to handle the navigation to dashboard page.
    * @return : Dashboard Page.
     */
    @RequestMapping(value = "/expert-user.html", params = "action=menu", method = RequestMethod.POST)
    public @ResponseBody
    String navigationToMenuBar(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("menuOption") String menuOption) {
        String naviagtionUrl = null;
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("admin")) {
            switch (menuOption) {
                case "dashBoard":
                    naviagtionUrl = url + "/dashboard.html";
                    break;
                case "autoMail":
                    naviagtionUrl = url + "/automail.html";
                    break;
                case "settings":
                    naviagtionUrl = url + "/settings.html";
                    break;
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("reviewer")) {

            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "reports":
                    naviagtionUrl = url + "/dashboard.html?report=report";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else if (userLoggedIn && user.isAdmin() && expertUser.getRole().equals("expert")) {
            switch (menuOption) {
                case "knowledgeBase":
                    naviagtionUrl = url + "/knowledge-base.html";
                    break;
                case "expertAnswer":
                    naviagtionUrl = url + "/ate-question.html";
                    break;
                case "ateReports":
                    naviagtionUrl = url + "/ate-reports.html";
                    break;
            }
        } else {
            naviagtionUrl = url;
        }
        return naviagtionUrl;
    }

}
