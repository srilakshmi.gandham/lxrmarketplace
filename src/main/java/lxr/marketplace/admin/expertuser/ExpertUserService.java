/**
 * @author NE16T1213
 */
package lxr.marketplace.admin.expertuser;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ExpertUserService {

    private static final Logger logger = Logger.getLogger(ExpertUserService.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    private SignupService signupService;
    @Autowired
    private String supportMail;

    /*Fetching the expert users data.*/
    public List<ExpertUser> getExpertUserList() {
        List<ExpertUser> expertAnswers = null;
        try {
            String filterQuery = "select expert_id,name,email,role "
                    + "from lxrm_ate_experts where is_deleted = 0";
            expertAnswers = this.jdbcTemplate.query(filterQuery, (ResultSet rs, int i) -> {
                ExpertUser expertAnswer = new ExpertUser();
                expertAnswer.setExpertId(rs.getLong("expert_id"));
                expertAnswer.setName(rs.getString("name"));
                expertAnswer.setEmail(rs.getString("email"));
                expertAnswer.setRole(rs.getString("role"));
                return expertAnswer;
            });
            return expertAnswers;
        } catch (Exception e) {
            logger.error("Exception in fetching getExpertUserList", e);
        }
        if (expertAnswers == null) {
            expertAnswers = new ArrayList<>();
        }
        return expertAnswers;
    }

    /*Updating the expert user data.*/
    public String updateExpertUserData(ExpertUser expertUserFeed, String lxrmHostingURL) {

        String userMessage = null;
        if (!expertUserFeed.isUpdating()) {
            /*Searching for user in lxrmarketplace user table is existed or not with email.*/
            String serachUserMessage = serachUserinLXRMarketplace(expertUserFeed, lxrmHostingURL);
            if (serachUserMessage != null) {
                if (serachUserMessage.equalsIgnoreCase("User Updated")) {
                    userMessage = "New  user is created in LXRMarketpalce "
                            + "and expert user is added successfully.";
                } else {
                    userMessage = "Expert user is added successfully.";
                }
            }
        } else if (expertUserFeed.isUpdating()) {
            userMessage = "Expert user updated.";
        }

        insertAndUpdateExpertAnswer(expertUserFeed);
        return userMessage;
    }

    /* Constructing the query for inserting the expert user data for add/edit operation.*/
    public boolean insertAndUpdateExpertAnswer(ExpertUser expertUserFeed) {
        String expertQuery;
        Date insert_date = null;
        if (expertUserFeed.isUpdating()) {
            /*Updating the expert user data.*/
            expertQuery = "update lxrm_ate_experts  SET role = ? where expert_id = ?";
            Object[] expertParams = {expertUserFeed.getRole(), expertUserFeed.getExpertId()};
            return insertUserFeed(expertQuery, expertParams);
        } else {
            /*Inserting new expert user data.*/
            insert_date = new Date(Calendar.getInstance().getTimeInMillis());
            expertQuery = "insert into lxrm_ate_experts  (name,email,role,insert_date,"
                    + "is_deleted) values(?,?,?,?,?)";
            Object[] expertParams = {expertUserFeed.getName(), expertUserFeed.getEmail(),
                expertUserFeed.getRole(), insert_date, 0};
            return insertUserFeed(expertQuery, expertParams);
        }
    }

    /* Common Inserting the expert user data for add/edit operation.
       Updating the guest user details for LXRMarketplace user.*/
    public boolean insertUserFeed(String query, Object[] params) {
        boolean insertStatus = false;
        try {
            jdbcTemplate.update(query, params);
            insertStatus = true;
        } catch (Exception e) {
            logger.info("Exception in insertUserFeed" + e.getMessage());
        }
        return insertStatus;
    }

    /* Searching for the user in table (LXRMarkertplace: user) for auto register if email is not exist.*/
    public String serachUserinLXRMarketplace(ExpertUser expertUserFeed, String lxrmHostingUrl) {
        /* Search for user  with emailId in user table.*/
        Signup existingUser = signupService.getUserDetails(expertUserFeed.getEmail(), 0);
        boolean islocal = false;
        String lxrmExpertUserquery = null;
        if (existingUser == null) {
            /* Searching as a guest user  with emailId in user table.*/
            existingUser = signupService.getUserDetails(expertUserFeed.getEmail(), 1);
            Login user = null;
            if (existingUser != null) {
                /* EmailId is existed in lxrmarketplace as guest user.
                   Updating the guest user status as permenent user.*/
                user = new Login();
                user.setId(existingUser.getId());
                user.setName(expertUserFeed.getName());
                user.setUserName(expertUserFeed.getEmail());
                user.setPassword(Common.autoGeneratePassword());
                user.setDomainName(existingUser.getDomainName());
                user.setActive(existingUser.isActive());
                user.setAdmin(true);
                if (existingUser.getActivationTime() != null) {
                    user.setActivationDate(existingUser.getActivationTime());
                } else {
                    user.setActivationDate(Calendar.getInstance());
                }
                if (existingUser.getPrivacyPolicyAgreedTime() != null) {
                    user.setPrivacyPolicyAgreedTime(existingUser.getPrivacyPolicyAgreedTime());
                } else {
                    user.setPrivacyPolicyAgreedTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
                }
                if (user.getUserName().contains("netelixir") || user.getUserName().contains("NETELIXIR")) {
                    islocal = true;
                }
                lxrmExpertUserquery = "update user SET email=?, name = ?, password = ?,is_admin = ?, "
                        + "act_date = ?, privacy_policy_agreed_time=?, is_local=?, activation_id=? where id = ?";
                Object[] params = {user.getUserName(), user.getName(), user.getPassword(), user.isAdmin(),
                    user.getActivationDate(),user.getPrivacyPolicyAgreedTime(), islocal, Common.generateActivationId(), user.getId()};
                if (insertUserFeed(lxrmExpertUserquery, params)) {
                    String subject = EmailBodyCreator.subjectForSendUserDetails;
                    String displayTxt = EmailBodyCreator.bodyForSendUserDetails(user.getUserName(),
                            user.getPassword(), user.getName());
                    SendMail.sendMail(supportMail, expertUserFeed.getEmail(), subject, displayTxt);
                }
                return "User Updated";
            } else if (existingUser == null) {
                /* EmailId is not existed in lxrmarketplace as user/guest user. 
                 * Creating a user in lxrmarketplace with emailId.*/
                Timestamp regTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                String userPassword = Common.autoGeneratePassword();
                long activationId = Common.generateActivationId();
                if (expertUserFeed.getEmail().contains("netelixir") || expertUserFeed.getEmail().contains("NETELIXIR")) {
                    islocal = true;
                }
                lxrmExpertUserquery = "insert into user (name, email, password, is_active,is_admin,"
                        + "activation_id,reg_date,act_date,privacy_policy_agreed_time,is_local) "
                        + "values (?, ?, ?, ?, ?, ?, ?, ?,?,?)";
                Object[] lxrmUserParams = {expertUserFeed.getName(), expertUserFeed.getEmail(),
                    userPassword, 1, 1, activationId, regTime, regTime,regTime,
                    islocal};
                /*Inserting new user in lxrm user table for admin entered feed.*/
                insertUserFeed(lxrmExpertUserquery, lxrmUserParams);
                existingUser = new Signup();
                existingUser.setName(expertUserFeed.getName());
                existingUser.setEmail(expertUserFeed.getEmail());
                existingUser.setActivationId(activationId);
                existingUser.setPassword(userPassword);
                /*Sending activation mail for user.*/
                sendActivationMailofLXRMUser(existingUser, lxrmHostingUrl);
                return "User Updated";
            }
        } else {
            lxrmExpertUserquery = "update user  SET is_admin = ?"
                    + " where email = ?";
            Object[] expertParams = {1, expertUserFeed.getEmail()};
            insertUserFeed(lxrmExpertUserquery, expertParams);
            expertUserFeed.setName(existingUser.getName());
            return "User Existed.";
        }
        return null;
    }

    /* Send activation mail to registered user in lxrmarketplace .*/
    public void sendActivationMailofLXRMUser(Signup lxrmUser, String lxrmHostingURL) {
        String subject = EmailBodyCreator.subjectOnWelcome;
        String bodyText = EmailBodyCreator.bodyForAutoSignup(lxrmUser);
        SendMail.sendMail(supportMail, lxrmUser.getEmail(), subject, bodyText);
 
    }

    public boolean updateLXRMUserName(String userName, String userEmail) {
        String userQuery = "update user set name = ?  where email = ?";
        Object[] lxrmUserParams = {userName, userEmail};
        return insertUserFeed(userQuery, lxrmUserParams);
    }

    public ExpertUser findExpertUser(String userName) {
        ExpertUser expertUser = null;
        String query = "SELECT * FROM lxrm_ate_experts WHERE email = ?";
        try {
            expertUser = this.jdbcTemplate.queryForObject(query, new Object[]{userName}, (ResultSet rs, int i) -> {
                ExpertUser expertUser1 = new ExpertUser();
                expertUser1.setExpertId(rs.getLong("expert_id"));
                expertUser1.setEmail(rs.getString("email"));
                expertUser1.setName(rs.getString("name"));
                expertUser1.setRole(rs.getString("role"));
                expertUser1.setIsDeleted(rs.getBoolean("is_deleted"));
                return expertUser1;
            });
        } catch (EmptyResultDataAccessException e) {
            logger.error("Exception on fetching expert user:", e);
        }

        return expertUser;
    }
}
