/**
 * @author NE16T1213
 */
package lxr.marketplace.admin.expertuser;


public class ExpertUser {

    private String name;
    private String email;
    private String role;
    private boolean updating;
    private long expertId;
    private boolean isDeleted;
    
    public ExpertUser() {
        
    }

    ExpertUser(long expertId,String name, String email, String role, boolean updating) {
        this.expertId =  expertId;
        this.name = name;
        this.email = email;
        this.role = role;
        this.updating = updating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isUpdating() {
        return updating;
    }

    public void setUpdating(boolean updating) {
        this.updating = updating;
    }

    public long getExpertId() {
        return expertId;
    }

    public void setExpertId(long expertId) {
        this.expertId = expertId;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
