package lxr.marketplace.admin.automail;


public class VideoUsage {

		long id;
		long sessionId;
		long videoId;
		int count;

		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public long getSessionId() {
			return sessionId;
		}
		public void setSessionId(long sessionId) {
			this.sessionId = sessionId;
		}
		public long getVideoId() {
			return videoId;
		}
		public void setVideoId(long videoId) {
			this.videoId = videoId;
		}
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
			
		

}
