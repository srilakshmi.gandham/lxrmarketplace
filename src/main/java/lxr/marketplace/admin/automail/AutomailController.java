package lxr.marketplace.admin.automail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;
import lxr.marketplace.admin.automail.Automail;
import lxr.marketplace.admin.automail.AutomailController;
import lxr.marketplace.admin.automail.AutomailService;
import lxr.marketplace.admin.report.Report;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;

@SuppressWarnings("deprecation")
public class AutomailController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(AutomailController.class);
    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();

    AutomailService automailService;
    AttachmentService attachmentService;
    LoginService loginService;
    String automailUserListFolder;
    String attachmentFolder;
    EmailTrackingInfoService emailTrackingInfoService;

    public void setEmailTrackingInfoService(
            EmailTrackingInfoService emailTrackingInfoService) {
        this.emailTrackingInfoService = emailTrackingInfoService;
    }

    public void setAutomailUserListFolder(String automailUserListFolder) {
        this.automailUserListFolder = automailUserListFolder;
    }

    public void setAttachmentFolder(String attachmentFolder) {
        this.attachmentFolder = attachmentFolder;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public AttachmentService getAttachmentService() {
        return attachmentService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    public AutomailService getAutomailService() {
        return automailService;
    }

    public void setAutomailService(AutomailService automailService) {
        this.automailService = automailService;
    }

    public AutomailController() {
        setCommandClass(Automail.class);
        setCommandName("automail");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Automail automail = (Automail) command;
        Login user = (Login) session.getAttribute("user");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        boolean mailsent = false;
        //automailService.addDemoTemplate();
        if (userLoggedIn && user.isAdmin()) {
            session.setAttribute("automail", automail);
            session.removeAttribute("existingTemplate");
            if (WebUtils.hasSubmitParameter(request, "report")) {
                logger.info("moving to report tab");
                Report report = new Report();
                report = new Report();
                ModelAndView mAndV = new ModelAndView("view/admin/report/Report", "report", report);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.info("moving to dashboard");
                ModelAndView mAndV = new ModelAndView("view/admin/dashboard/Dashboard");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "settings")) {
                logger.info("moving to settings tab");
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/settings.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "apply")) {
                List<FormRow> formRows = automail.getFormRows();
                Map<String, Login> userMap = null;
                if (formRows.size() > 0) {
                    automailService.modifyTemplatesOnApply(formRows, request);

                    //@DEPENDS ON USER OPTION SEND MAIL ....
                    EmailTemplate et = automail.getEmailTemplate();
                    List<Login> usersList = null;
                    if (et.getSendMailOption() == 1) {
                        List<Login> allUsersList = loginService.getUsersList();
                        usersList = allUsersList;
                    } else if (et.getSendMailOption() == 2) {
                        List<Login> allUsersListExceptLocalUsers = loginService.getAllUsersExceptLocalUser();
                        usersList = allUsersListExceptLocalUsers;
                    } else if (et.getSendMailOption() == 3) {
                        List<Login> listOfUsers = (List<Login>) session.getAttribute("listOfUsers");
                        if (listOfUsers == null && listOfUsers.size() < 0) {
                            automailService.getUserListFromUploadedFile(session, automailUserListFolder + "temporary/" + et.getUserListFile());
                            userMap = (Map<String, Login>) session.getAttribute("userMap");
                            listOfUsers = (List<Login>) session.getAttribute("listOfUsers");
                        }
                        usersList = listOfUsers;
                    }
                    if (usersList != null && usersList.size() > 0) {
                        automailService.sendOnDemandMails(request, usersList, userMap);
                    }
                    logger.info("applying status changes in database");
                    automailService.updateStatus(request);
                }
                formRows = automailService.createFormRows(request);
                automail.setFormRows(formRows);

            } else if (WebUtils.hasSubmitParameter(request, "delete")) {
                logger.info("Deleteing templates");
                List<FormRow> formRows = automail.getFormRows();
                /*for(FormRow fr:formRows)
				{
					System.out.println(fr.isDeleted());
				}*/
                if (formRows.size() > 0) {
                    automailService.modifyTemplatesOnDelete(formRows, request);
                    logger.info("applying Deletion status changes in database");
                    automailService.updateDeletionStatus(request);
                }
                formRows = automailService.createFormRows(request);
                automail.setFormRows(formRows);
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
            } else if (WebUtils.hasSubmitParameter(request, "uploadFile")) {
                attachmentService.addFileInTemporary(attachmentFolder, request, response);
            } else if (WebUtils.hasSubmitParameter(request, "removeFile")) {
                String fileName = WebUtils.findParameterValue(request, "removeFile");
                attachmentService.deleteFileFromTemporary(fileName);
                String[] attachedFiles = attachmentService.getListOfFiles(attachmentFolder);
                automailService.addInJson(attachedFiles, response);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "uploadUserListFile")) {
                session.removeAttribute("listOfUsers");
                session.removeAttribute("notPresentEmails");
                session.removeAttribute("presentedEmails");
//				Map<Long,String> userMap = loginService.getAllUsersMap();
                attachmentService.deleteTemporaryFolder(automailUserListFolder);
                attachmentService.createTemporaryFolder(automailUserListFolder);
                String filename = attachmentService.addFileInTemporary(automailUserListFolder, request, response);
                automailService.getUserListFromUploadedFile(session, automailUserListFolder + "temporary/" + filename);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "saveTemplate")) {
                String param = WebUtils.findParameterValue(request, "saveTemplate");
                automailService.modifyEmailTemplate(automail, request);
                EmailTemplate et = automail.getEmailTemplate();
                boolean changed = false;
                if (param.equals("update")) {
                    et.setUpdationDate(Calendar.getInstance());
                    changed = automailService.updateTemplate(et, request);
                    if (et.getSendMailOption() != 3) {
                        attachmentService.deleteTemporaryFolder(automailUserListFolder);
                    }

                } else if (param.equals("savenew")) {//for adding new template
                    changed = automailService.addNewTemplate(et, request);
                    if (et.getCategory() == 3) {
                        et.setStatus(true);
                    }
                    if (changed) {
                        long id = automailService.getActiveTemplateIdByName(et.getTemplateName());
                        et.setId(id);
                    }
                }
                if (changed) {
                    attachmentService.replaceAttachmentFolderByTemporary(attachmentFolder, et.getId());
                    attachmentService.replaceAttachmentFolderByTemporary(automailUserListFolder, et.getId());
                    if (WebUtils.hasSubmitParameter(request, "sendMail") && et.getCategory() == 2) {
                        List<Login> usersList = null;
                        Map<String, Login> userMap = null;
                        //@DEPENDS ON USER OPTION SEND MAIL
                        if (et.getSendMailOption() == 1) {
                            List<Login> allUsersList = loginService.getUsersList();
                            usersList = allUsersList;
                        } else if (et.getSendMailOption() == 2) {
                            List<Login> allUsersListExceptLocalUsers = loginService.getAllUsersExceptLocalUser();
                            usersList = allUsersListExceptLocalUsers;
                        } else if (et.getSendMailOption() == 3) {
                            List<Login> listOfUsers = (List<Login>) session.getAttribute("listOfUsers");
                            if (listOfUsers == null || listOfUsers.size() < 0) {
                                automailService.getUserListFromUploadedFile(session, automailUserListFolder + "temporary/" + et.getUserListFile());
                                listOfUsers = (List<Login>) session.getAttribute("listOfUsers");
                            }
                            userMap = (Map<String, Login>) session.getAttribute("userMap");
                            usersList = listOfUsers;
                        }
                        if (usersList != null && usersList.size() > 0) {
                            automailService.sendMailIfOnDemand(et, usersList, userMap);
                        }
                        Map<Long, String> templates = loginService.fetchTemplatesData(session);
                        //session.setAttribute("templates",templates);
                    }
                    List<FormRow> formRows = automailService.createFormRows(request);
                    automail.setFormRows(formRows);
                    automail.setEmailTemplate(new EmailTemplate());
                } else {
                    logger.info("Template updation is not successfull");
                    String existingTemplate = et.getTemplateName();
                    ModelAndView mAndV = new ModelAndView(getFormView(), "automail", automail);
                    mAndV.addObject("existingTemplate", existingTemplate);
                    session.setAttribute("existingTemplate", existingTemplate);
                    return mAndV;
                }
            } else if (WebUtils.hasSubmitParameter(request, "userDetailsSucessMsg")) {
                Integer[] totalUserDetails = null;
                if (session.getAttribute("totalUserDetails") != null) {
                    totalUserDetails = (Integer[]) session.getAttribute("totalUserDetails");
                }
                automailService.addUserDetailsInJson(totalUserDetails, response);
            } else if (WebUtils.hasSubmitParameter(request, "sendTestmail")) {
                logger.info("testmail:" + automail.testmail);
//				String fromAddr = (String)ctx.getBean("teamMail");
                String[] attachments;
                String existing = WebUtils.findParameterValue(request, "existing");
                //logger.info("existing"+existing);
                EmailTemplate et = automail.getEmailTemplate();
                automailService.modifyEmailTemplate(automail, request);
                attachments = attachmentService.getListOfFiles(attachmentFolder);
                logger.debug("attachments:" + attachments.length);
                String filepath = ctx.getBean("attachmentFolder") + "temporary";
                String fromAddress = automailService.ftechFromAddress(et);
                if (fromAddress == null) {
                    fromAddress = (String) ctx.getBean("teamMail");
                }
                String bodyText = et.getMailBody();
                bodyText = emailTrackingInfoService.appendUserNameVersion2(user, bodyText, et);
                mailsent = SendMail.sendMailWithAttachments(automail.testmail, fromAddress, et.getMailSubject(), bodyText, user.getName(), filepath, attachments);
                ModelAndView mAndV = new ModelAndView(getFormView(), "automail", automail);
                if (mailsent) {
                    mAndV.addObject("mailsent", "Mail sent Successfully");
                } else {
                    mAndV.addObject("mailsent", "Unable to Send Mail");
                }
                if (existing != null && existing.equalsIgnoreCase("existing")) {
                    //logger.info("existing template : "+existing);
                    mAndV.addObject("testmailsent", "existing");
                } else {
                    mAndV.addObject("testmailsent", "notexisting");
                }

                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "showtemplate")) {
                long id = Long.parseLong(WebUtils.findParameterValue(request, "showtemplate"));
                List<EmailTemplate> allTemplates = automailService.getTemplatesForUI();
                session.setAttribute("allTemplates", allTemplates);
                for (EmailTemplate et : allTemplates) {
                    if (et.getId() == id) {
                        automail.setEmailTemplate(et);
                        if (et.getCategory() == 3) {
                            automail.setFromDate(sdf.format(et.getStartDay().getTime()));
                            automail.setToDate(sdf.format(et.getEndDay().getTime()));
//							automail.setSenOnToday(et.isSendOnToday());
                        }
                        break;
                    }
                }
                attachmentService.copyFromAttachmentToTemporary(attachmentFolder, id);
                attachmentService.copyFromAttachmentToTemporary(automailUserListFolder, id);
                /*
				String [] attachments = attachmentService.getListOfFiles();
				if(attachments != null){
					for(int i = 0; i < attachments.length; i++){
						System.out.println("Attached File "+i+": "+attachments[i]);
					}
				}
                 */
            } else if (WebUtils.hasSubmitParameter(request, "getAllFiles")) {
                String[] attachedFiles = attachmentService.getListOfFiles(attachmentFolder);
                automailService.addInJson(attachedFiles, response);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "getUserFile")) {
                String[] attachedFiles = attachmentService.getListOfFiles(automailUserListFolder);
                automailService.addInJson(attachedFiles, response);
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "cancel")) {
                attachmentService.deleteTemporaryFolder(attachmentFolder);
                attachmentService.deleteTemporaryFolder(automailUserListFolder);
                automail.setEmailTemplate(new EmailTemplate());
                automail.setFromDate("");
                automail.setToDate("");
            } else if (WebUtils.hasSubmitParameter(request, "showNewTemplate")) {
                attachmentService.createTemporaryFolder(attachmentFolder);
                attachmentService.createTemporaryFolder(automailUserListFolder);
                EmailTemplate et = new EmailTemplate();
                et.setCategory(1);
                et.setSendMailOption(2);
                automail.setEmailTemplate(et);
            }
        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            //logger.info("REDIRECT URL: "+url);
            return null;
        }
        ModelAndView mAndV = new ModelAndView("view/admin/automail/Automail", "automail", automail);
        return mAndV;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        Automail automail = (Automail) session.getAttribute("automail");
        if (automail == null) {
            automail = new Automail();
            List<EmailTemplate> allTemplates = automailService.getTemplatesForUI();
            session.setAttribute("allTemplates", allTemplates);
            List<FormRow> formRows = automailService.createFormRows(request);
            //automailService.sortByCategory(formRows, request);
            EmailTemplate et = new EmailTemplate();
            automail.setFormRows(formRows);
            automail.setEmailTemplate(et);
//			automail.setSenOnToday(et.isSendOnToday());
        } else {
            List<EmailTemplate> allTemplates = automailService.getTemplatesForUI();
            session.setAttribute("allTemplates", allTemplates);
        }
        return automail;
    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

}
