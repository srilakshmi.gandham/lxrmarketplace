package lxr.marketplace.admin.automail;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.sql.Time;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.SendMail;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.web.util.WebUtils;

public class AutomailService {

    private static Logger logger = Logger.getLogger(AutomailService.class);
    EmailTemplateService emailTemplateService;
    LoginService loginService;
    EmailTrackingInfoService emailTrackingInfoService;
    ApplicationContext context = ApplicationContextProvider.getApplicationContext();

    public EmailTrackingInfoService getEmailTrackingInfoService() {
        return emailTrackingInfoService;
    }

    public void setEmailTrackingInfoService(
            EmailTrackingInfoService emailTrackingInfoService) {
        this.emailTrackingInfoService = emailTrackingInfoService;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public void sendMailIfOnDemand(EmailTemplate emailTemplate, List<Login> usersList, Map<String, Login> userMap) {
//		String userQuery = "select * from user where is_active=1 and is_admin =0 and unsubscribe=0 and email is not null ";
//		List<Login> allUsers = loginService.getUsersByQuery(userQuery);

        emailTrackingInfoService.addEventtoDB(emailTemplate);
        String fromAddress = ftechFromAddress(emailTemplate);
        if (fromAddress != null && !fromAddress.trim().equals("")) {
            new SendOnDemands(emailTemplate, usersList, fromAddress, userMap);//creating thread for sending mail
        } else {
            logger.debug("From address in null");
        }
        emailTemplate.setStatus(false);
        String query = "update mail_templates set status=0 where id = " + emailTemplate.getId();
        emailTemplateService.updateStatusInDB(query);
        if (usersList.size() > 0) {
            emailTemplate.setLastMailSent(Calendar.getInstance());
            emailTemplateService.updateLastMailSentDate(emailTemplate);
        }
    }

    public String ftechFromAddress(EmailTemplate emailTemplate) {
        if (context == null) {
            logger.debug("context is null taking from service obj........");
            AutomailService service = new AutomailService();
            context = service.context;
        }
        String fromAddress;
        switch (emailTemplate.getFromAddress()) {
            case 1:
                fromAddress = (String) context.getBean("teamMail");
                break;
            case 2:
                fromAddress = (String) context.getBean("netelixirinfo");
                break;
            case 3:
                fromAddress = (String) context.getBean("supportMail");
                break;
            case 4:
                fromAddress = (String) context.getBean("lxrseoinfo");
                break;
            case 5:
                fromAddress = (String) context.getBean("lxrseosupport");
                break;
            default:
                fromAddress = "";
                break;
        }
        return fromAddress;

    }

    public boolean updateTemplate(EmailTemplate emailTemplate, HttpServletRequest request) {
        String query = "select count(id) from mail_templates where temp_name=? and id <> " + emailTemplate.getId() + " and is_deleted=0";
        Object[] args = {emailTemplate.getTemplateName()};
        int[] argTypes = {Types.VARCHAR};
        logger.info("Check Existance query: " + query);

        if (!emailTemplateService.checkExistance(query, args, argTypes)) {
            emailTemplate.setMailBody(appendtrackParamsToMailBody(emailTemplate.getMailBody()));
            if (emailTemplate.getCategory() == 3) {
                emailTemplate.setStatus(true);
            }
            emailTemplateService.updateTemplatetoDB(emailTemplate);
            return true;
        }
        return false;
    }

    public boolean addNewTemplate(EmailTemplate emailTemplate, HttpServletRequest request) {
        String query = "select count(id) from mail_templates where temp_name=? and is_deleted=0";
        Object[] args = {emailTemplate.getTemplateName()};
        int[] argTypes = {Types.VARCHAR};
        if (!emailTemplateService.checkExistance(query, args, argTypes)) {
            emailTemplate.setMailBody(appendtrackParamsToMailBody(emailTemplate.getMailBody()));
            if (emailTemplate.getCategory() == 3) {
                emailTemplate.setStatus(true);
            }
            emailTemplateService.addTemplatetoDB(emailTemplate);
            return true;
        }
        return false;
    }

    public void addDemoTemplate() {
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setTemplateName("test template");
        emailTemplate.setStatus(false);
        emailTemplate.setMailSubject("do Not mess with us!!");
        emailTemplate.setMailBody("hi\n\n\t iouoiu \n\tjkssssssssssssss 782348 3728 ^%$&^#$&^ .\nThanks & Regards\n");
        emailTemplate.setCategory(3);
        emailTemplate.setDaysGap(5);
        emailTemplate.setEvent(3);
        emailTemplate.setStartDay(Calendar.getInstance());
        emailTemplate.setEndDay(Calendar.getInstance());
        emailTemplate.setTimeOfDay(new Time(3600000));
        emailTemplate.setDeleted(false);
        emailTemplate.setCreationDate(Calendar.getInstance());
        emailTemplate.setUpdationDate(Calendar.getInstance());
        emailTemplateService.addTemplatetoDB(emailTemplate);
    }

    public List<EmailTemplate> getTemplatesForUI() {
        String query = "select * from mail_templates where is_deleted = 0 order by id desc";
        return emailTemplateService.extractTemplatesfromDB(query);
    }

    public void sendOnDemandMails(HttpServletRequest request, List<Login> usersList, Map<String, Login> userMap) {
        HttpSession session = request.getSession(true);
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> allTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            for (EmailTemplate emailTemplate : allTemplates) {
                if (emailTemplate.getCategory() == 2 && emailTemplate.isStatus()) {
                    sendMailIfOnDemand(emailTemplate, usersList, userMap);
                }
            }
            session.removeAttribute("allTemplates");
            session.setAttribute("allTemplates", allTemplates);
        } else {
//			logger.error("no available email template in session");
        }

    }

    public void modifyTemplatesOnApply(List<FormRow> formRows, HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> emailTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            for (int i = 0, j = 0; i < emailTemplates.size() || j < formRows.size();) {
                EmailTemplate temp = emailTemplates.get(i);
                FormRow fr = formRows.get(j);
                if (temp.getId() < fr.getTempId()) {
                    logger.error("ui data unsynchronized with data from database");
                    i++;
                } else if (temp.getId() > fr.getTempId()) {
                    logger.error("ui data unsynchronized with data from database");
                    j++;
                } else {
                    //emailTemplates.get(i).setStatus(formRows.get(i).isStatus()); 
                    if (temp.isStatus() != fr.isStatus()) {
                        temp.setStatus(fr.isStatus());
                        temp.setUpdationDate(Calendar.getInstance());
                    }
                    i++;
                    j++;
                }
            }
            session.removeAttribute("allTemplates");
            session.setAttribute("allTemplates", emailTemplates);
        }

    }

    public void modifyTemplatesOnDelete(List<FormRow> formRows, HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> emailTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            for (int i = 0, j = 0; i < emailTemplates.size() || j < formRows.size();) {
                EmailTemplate temp = emailTemplates.get(i);
                FormRow fr = formRows.get(j);
                if (temp.getId() < fr.getTempId()) {
                    logger.error("ui data unsynchronized with data from database");
                    i++;
                } else if (temp.getId() > fr.getTempId()) {
                    logger.error("ui data unsynchronized with data from database");
                    j++;
                } else {
                    //emailTemplates.get(i).setStatus(formRows.get(i).isStatus()); 
                    if (temp.isDeleted() != fr.isDeleted()) {
                        temp.setDeleted(fr.isDeleted());
                        temp.setUpdationDate(Calendar.getInstance());
                    }
                    i++;
                    j++;
                }
            }
            session.removeAttribute("allTemplates");
            session.setAttribute("allTemplates", emailTemplates);
        }

    }

    public List<FormRow> createFormRows(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        List<EmailTemplate> allTemplates = getTemplatesForUI();
        session.setAttribute("allTemplates", allTemplates);
        List<FormRow> formRows = new ArrayList<FormRow>();

        for (EmailTemplate temp : allTemplates) {
            FormRow fr = new FormRow();
            fr.setTempId(temp.getId());
            fr.setTemplateName(temp.getTemplateName());
            fr.setStatus(temp.isStatus());
            fr.setMailSubject(temp.getMailSubject());
            fr.setCategory(temp.getCategory());
            fr.setDeleted(temp.isDeleted());
            fr.setSendMailOption(temp.getSendMailOption());
            fr.setFromAddress(temp.getFromAddress());
            fr.setLogo(temp.getLogo());
            fr.setUrl(temp.getUrl());
            fr.setPersonalAddress(temp.getPersonalAddress());
            fr.setSendOnToday(temp.isSendOnToday());
            if (temp.getLastMailSent() != null) {
                SimpleDateFormat simpleDF = new SimpleDateFormat("MMM dd, yyyy hh:mmaaa");
                fr.setLastMailSent(simpleDF.format(temp.getLastMailSent().getTime()));
            }
            formRows.add(fr);
        }

        return formRows;
    }

    public void updateStatus(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        SimpleDateFormat simpleDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String query = "UPDATE mail_templates SET status = CASE id";
        String query2 = " updation_date = CASE id";
        String listOfIds = "where id in(";
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> allTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            for (EmailTemplate temp : allTemplates) {
                query += " WHEN " + temp.getId() + " THEN " + temp.isStatus();
                query2 += " WHEN " + temp.getId() + " THEN '" + simpleDF.format(temp.getUpdationDate().getTime()) + "'";
                listOfIds += temp.getId() + ", ";
            }
            listOfIds = listOfIds.substring(0, listOfIds.lastIndexOf(", "));
        }

        //query += " ELSE status END ";
        query += " END," + query2 + " END ";
        listOfIds += ")";
        query += listOfIds;
        logger.info("Query for updating: " + query);
        emailTemplateService.updateStatusInDB(query);
    }

    public void updateDeletionStatus(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        SimpleDateFormat simpleDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String query = "UPDATE mail_templates SET is_deleted = CASE id";
        String query1 = " status = CASE id";
        String query2 = " updation_date = CASE id";
        String listOfIds = "where id in(";
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> allTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            for (EmailTemplate temp : allTemplates) {
                boolean st = temp.isStatus();
                if (temp.isDeleted()) {
                    st = false;
                }
                query += " WHEN " + temp.getId() + " THEN " + temp.isDeleted();
                query1 += " WHEN " + temp.getId() + " THEN " + st;
                query2 += " WHEN " + temp.getId() + " THEN '" + simpleDF.format(temp.getUpdationDate().getTime()) + "'";
                listOfIds += temp.getId() + ", ";
            }
            listOfIds = listOfIds.substring(0, listOfIds.lastIndexOf(", "));
        }
        //query += " ELSE status END ";
        query += " END," + query1 + " END," + query2 + " END ";
        listOfIds += ")";
        query += listOfIds;
        logger.info("Query for updating: " + query);
        emailTemplateService.updateStatusInDB(query);
    }

    public void sortByCategory(List<FormRow> formRows, HttpServletRequest request) {
        Collections.sort(formRows, new CategoryComparator());
        HttpSession session = request.getSession(true);
        if (session.getAttribute("allTemplates") != null) {
            List<EmailTemplate> allTemplates = (List<EmailTemplate>) session.getAttribute("allTemplates");
            Collections.sort(allTemplates, new CategoryComparator());
        }
    }

    public void modifyEmailTemplate(Automail automail, HttpServletRequest request) {
        EmailTemplate et = automail.getEmailTemplate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        et.setTemplateName(et.getTemplateName().trim());
        if (et.getCategory() == 3 || et.getCategory() == 4) {
            String tod = "00:00";
            if (WebUtils.hasSubmitParameter(request, "tod")) {
                tod = WebUtils.findParameterValue(request, "tod");
            }
            String[] hm = new String[2];
            hm = tod.split(":");
            Time timeOfDay = new Time((Integer.parseInt(hm[0]) * 60 + (Integer.parseInt(hm[1]))) * 60 * 1000);
            et.setTimeOfDay(timeOfDay);
        } else {
            et.setTimeOfDay(null);
        }
        if (et.getCategory() == 3) {
            Date startDay;
            try {
                startDay = sdf.parse(automail.getFromDate());
            } catch (ParseException e) {
                startDay = null;
                e.printStackTrace();
            }
            Date endDay;
            try {
                endDay = sdf.parse(automail.getToDate());
            } catch (ParseException e) {
                endDay = null;
                e.printStackTrace();
            }
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startDay);
            et.setStartDay(cal1);

            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(endDay);
            et.setEndDay(cal2);
        } else {
            et.setStartDay(null);
            et.setEndDay(null);
        }
        if (et.getCategory() == 1 || et.getCategory() == 2) {
            et.setDaysGap(0);
        }
        if (et.getCategory() == 2 || et.getCategory() == 3) {
            et.setEvent(0);
        }
    }

    public long getActiveTemplateIdByName(String name) {
        String query = "select id from mail_templates where temp_name = ? and is_deleted = 0";
        Object[] args = {name};
        int[] argTypes = {java.sql.Types.VARCHAR};
        return emailTemplateService.getTemplateId(query, args, argTypes);
    }

    public void addInJson(String[] attachedFiles, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;

        try {
            writer = response.getWriter();
            for (int i = 0; i < attachedFiles.length; i++) {
                arr.add(i, attachedFiles[i]);
            }
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

    public String appendtrackParamsToMailBody(String mailbody) {
        Document mailDoc = null;
        Elements anchors = null;
        try {
            mailDoc = Jsoup.parse(mailbody);
            anchors = mailDoc.select("a");
            for (Element link : anchors) {
                String href = link.attr("href");
                //logger.info("ampersand******************: "+ href);
                if (!href.contains("lm_mail")) {
                    if (href.contains("?")) {
                        href = href.concat("&lm_mail");
                    } else {
                        href = href.concat("?lm_mail");
                    }
                }
                link.attr("href", href);
                logger.info("link chnages:" + link.text());
            }
            //logger.info("print doc:"+ mailDoc.toString());
        } catch (Exception e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
        //mailbody = mailbody.replaceAll(".html\">",".html?lm_mail\">" ).replaceAll(".com/\">", ".com/?lm_mail\">");
        //mailbody = mailbody.replaceAll(".com\">", ".com?lm_mail\">");
        return mailDoc.toString();
    }

    //to read xls file values
    @SuppressWarnings({"rawtypes", "deprecation"})
    public Set<String> validateUploadedFile(String filename, HttpSession session) {
        logger.info("in side of validateUploadedFile");
        Set<String> emalis = new HashSet<String>();
        Map<String, Login> userMap = new HashMap<String, Login>();
        FileInputStream file;
        try {
            file = new FileInputStream(new File(filename));
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);

            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                HSSFRow row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();
                Login login = new Login();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    if (cell.getColumnIndex() == 0) {
                        if (CellType.NUMERIC == cell.getCellTypeEnum()) {
                            if (!(cell.getNumericCellValue() + "").equalsIgnoreCase("UserId")
                                    && !(cell.getNumericCellValue() + "").equalsIgnoreCase("Email")
                                    && !(cell.getNumericCellValue() + "").equalsIgnoreCase("Name")) {
                                int userId = (int) cell.getNumericCellValue();
                                login.setId(userId);
                            }
                        }
                    } else if (cell.getColumnIndex() == 1) {
                        if (CellType.NUMERIC == cell.getCellTypeEnum()) {
                            if (!(cell.getStringCellValue()).equalsIgnoreCase("Email")
                                    && !(cell.getStringCellValue()).equalsIgnoreCase("UserId")
                                    && !(cell.getStringCellValue()).equalsIgnoreCase("Name")) {

                                if (!cell.getStringCellValue().trim().equals("")) {
                                    emalis.add(cell.getStringCellValue());
                                    login.setUserName(cell.getStringCellValue());
//		                    			userMap.put(login.getUserName(),login); 
                                }
                            }
                        }
                    } else if (cell.getColumnIndex() == 2) {
                        if (CellType.STRING == cell.getCellTypeEnum()) {
                            if (!(cell.getStringCellValue()).equalsIgnoreCase("Email")
                                    && !(cell.getStringCellValue()).equalsIgnoreCase("UserId")
                                    && !(cell.getStringCellValue()).equalsIgnoreCase("Name")) {
                                if (!cell.getStringCellValue().trim().equals("")) {
//		                    			login = userMap.get(login.getUserName());
                                    login.setName(cell.getStringCellValue());
                                }
                            }
                        }
                    }
                }
                userMap.put(login.getUserName(), login);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (userMap != null && userMap.size() > 0) {
            logger.info("usermap size is ............" + userMap.size());
            session.setAttribute("userMap", userMap);
        }
        return emalis;

    }

    public String createEmailsQuery(Set<String> emails) {
        String query = "select * from user where email in (";
        if (emails != null && emails.size() > 0) {
            Iterator<String> iter = emails.iterator();
            while (iter.hasNext()) {
                query += "'" + iter.next() + "',";
            }
            query = query.substring(0, query.lastIndexOf(','));
            query += ")";
            logger.info("email query is....." + query);
            return query;
        } else {
            return null;
        }
    }

    public void getUserListFromUploadedFile(HttpSession session, String filename) {
        session.removeAttribute("userMap");
        Set<String> emailsList = validateUploadedFile(filename, session);
        String query = createEmailsQuery(emailsList);
        List<Login> unsubscribedUsers = new ArrayList<Login>();
        Integer[] totalUserDetails = new Integer[4];
        int totCount = 0;
        session.removeAttribute("totalUserDetails");
        session.removeAttribute("listOfUsers");
        if (query != null) {
            List<Login> listOfUsers = loginService.getUsersByQuery(query);
            if (listOfUsers != null) {
                Set<String> presentedEmails = new HashSet<String>();
                for (Login login : listOfUsers) {
                    presentedEmails.add(login.getUserName());
                }
                Set<String> notPresentEmails = new HashSet<String>(emailsList);
                notPresentEmails.removeAll(presentedEmails);

                if (notPresentEmails != null && notPresentEmails.size() > 0) {
                    Iterator<String> iter = notPresentEmails.iterator();
                    while (iter.hasNext()) {
                        Login login = new Login();
                        login.setId(0);
                        login.setUnsubscribed(false);
                        login.setUserName(iter.next());
                        listOfUsers.add(login);
                    }
                }
                totCount = listOfUsers.size();
                for (Login login : listOfUsers) {
                    if (login.isUnsubscribed()) {
                        unsubscribedUsers.add(login);
                    }
                }
                if (unsubscribedUsers != null && unsubscribedUsers.size() > 0) {
                    listOfUsers.removeAll(unsubscribedUsers);
                }

                totalUserDetails[0] = totCount;
                totalUserDetails[1] = presentedEmails.size();
                totalUserDetails[2] = notPresentEmails.size();
                totalUserDetails[3] = unsubscribedUsers.size();
                session.setAttribute("totalUserDetails", totalUserDetails);
                session.setAttribute("listOfUsers", listOfUsers);
            }
        }
    }

    public void addUserDetailsInJson(Integer[] totalUserDetails, HttpServletResponse response) {
        JSONArray arr = null;
        arr = new JSONArray();
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            if (totalUserDetails != null && totalUserDetails.length > 0) {
                logger.info("totalUserDetails........." + totalUserDetails.length);
                for (int i = 0; i < totalUserDetails.length; i++) {
                    arr.add(i, (int) totalUserDetails[i]);
                }
            }
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }
}

class CategoryComparator implements Comparator<Object> {

    public int compare(Object obj1, Object obj2) {
        int cat1 = 0, cat2 = 0;
        if (obj1 instanceof FormRow) {
            cat1 = ((FormRow) obj1).getCategory();
            cat2 = ((FormRow) obj2).getCategory();
        } else if (obj1 instanceof EmailTemplate) {
            cat1 = ((EmailTemplate) obj1).getCategory();
            cat2 = ((EmailTemplate) obj2).getCategory();
        }
        if (cat1 > cat2) {
            return 1;
        } else if (cat1 < cat2) {
            return -1;
        } else {
            return 0;
        }
    }
}

class SendOnDemands extends Thread {

    EmailTemplate onDemandTemplate;
    List<Login> allUsersCopy;
    String fromAddress;
    Map<String, Login> useMap;

    SendOnDemands(EmailTemplate onDemandTemplate, List<Login> allUsersCopy, String fromAddress, Map<String, Login> useMap) {
        this.onDemandTemplate = onDemandTemplate;
        this.allUsersCopy = allUsersCopy;
        this.fromAddress = fromAddress;
        this.useMap = useMap;
        start();
    }

    public void run() {
        Set<String> emails = new HashSet<String>();
        for (Login user : allUsersCopy) {
            try {
                String trimmedEmail = user.getUserName();
                if (trimmedEmail.contains("<") && trimmedEmail.contains(">")) {
                    trimmedEmail = trimmedEmail.substring(trimmedEmail.indexOf('<') + 1, trimmedEmail.indexOf('>'));
                }
                if (!emails.contains(trimmedEmail)) {
                    user.setUserName(trimmedEmail);
                    if (useMap != null && useMap.size() > 0) {
                        Login login = useMap.get(trimmedEmail);
                        System.out.println("trimmedEmail..........." + trimmedEmail);
                        if (login != null) {
                            System.out.println("trimmedEmail.  and login obj is.........." + trimmedEmail + "---------" + login.getName());
                            if (login.getName() != null && !login.getName().trim().equals("")) {
                                user.setName(login.getName());
                            }
                        }

                    }
                    emails.add(trimmedEmail);
                    new SendMail().sendAdminMail(onDemandTemplate, user, fromAddress);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
