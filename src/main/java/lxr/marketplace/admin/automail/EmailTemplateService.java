package lxr.marketplace.admin.automail;

import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class EmailTemplateService extends JdbcDaoSupport {
	 private static Logger logger = Logger.getLogger(EmailTemplateService.class);
	 
	 public void addTemplatetoDB(final EmailTemplate emailTemplate){
		 //logger.info("jdbc template: "+getJdbcTemplate());
		 final String query = "insert into mail_templates (temp_name, status, mail_subject, " +
		 		"mail_body, category, days_gap, event, start_date, end_date, time_of_day, " +
		 		"is_deleted, creation_date, updation_date, last_mail_sent,send_mail_option,upload_file_name,from_address,logo," +
		 		"url,personal_address,send_mail_today) values" +
		 		"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 Object[] valLis = new Object[21];
		 
		 valLis[0] = emailTemplate.getTemplateName();
	     valLis[1] = emailTemplate.isStatus();
	     try {
	         valLis[2] = emailTemplate.getMailSubject().getBytes("UTF-8");
         } catch (UnsupportedEncodingException e) {
            logger.error("UNABLE to encode mail subject with UTF-8");
            valLis[2] = emailTemplate.getMailSubject();
            e.printStackTrace();
         }
         try {
            valLis[3] = emailTemplate.getMailBody().getBytes("UTF-8");
         } catch (UnsupportedEncodingException e) {
            logger.error("UNABLE to encode mail body with UTF-8");
            valLis[3] = emailTemplate.getMailBody();
            e.printStackTrace();
         }
	     valLis[4] = emailTemplate.getCategory();
	     valLis[5] = emailTemplate.getDaysGap();
	     valLis[6] = emailTemplate.getEvent();
	     valLis[7] = (emailTemplate.getStartDay() == null) ?null : new java.sql.Date(emailTemplate.getStartDay().getTimeInMillis());
	     valLis[8] = (emailTemplate.getEndDay() == null)? null : new java.sql.Date(emailTemplate.getEndDay().getTimeInMillis());
	     valLis[9] = (emailTemplate.getTimeOfDay() == null)? null : new java.sql.Time(emailTemplate.getTimeOfDay().getTime());
	     valLis[10] = emailTemplate.isDeleted();
	     valLis[11] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
	     valLis[12] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
	     valLis[13] = (emailTemplate.getLastMailSent() == null) ? null : new java.sql.Timestamp(emailTemplate.getLastMailSent().getTimeInMillis());
	     valLis[14] = emailTemplate.getSendMailOption();
	     valLis[15] = emailTemplate.getUserListFile();
	     valLis[16] = emailTemplate.getFromAddress();
	     valLis[17] = emailTemplate.getLogo();
	     valLis[18] = emailTemplate.getUrl();
	     valLis[19] = emailTemplate.getPersonalAddress();
	     valLis[20] = emailTemplate.isSendOnToday();
	     try {
	            getJdbcTemplate().update(query, valLis);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	 }
	 public void updateTemplatetoDB(final EmailTemplate emailTemplate){
		 final String query = "update mail_templates set temp_name=?, status = ?, mail_subject = ?, " +
		 		"mail_body = ?, category = ?, days_gap = ?, event = ?, start_date = ?, end_date = ?, " +
		 		"time_of_day = ?, is_deleted = ?, updation_date = ?, send_mail_option =?, upload_file_name =?, from_address=?," +
		 		"logo = ?, url = ?, personal_address = ?,send_mail_today = ?" +
		 		" where id = ?";
		 Object[] valLis = new Object[20];
		 
		 valLis[0] = emailTemplate.getTemplateName();
	     valLis[1] = emailTemplate.isStatus();
	     try {
            valLis[2] = emailTemplate.getMailSubject().getBytes("UTF-8");
	     } catch (UnsupportedEncodingException e) {
            logger.error("UNABLE to encode mail subject with UTF-8");
            valLis[2] = emailTemplate.getMailSubject();
            e.printStackTrace();
	     }
	     try {
            valLis[3] = emailTemplate.getMailBody().getBytes("UTF-8");
	     } catch (UnsupportedEncodingException e) {
            logger.error("UNABLE to encode mail body with UTF-8");
            valLis[3] = emailTemplate.getMailBody();
            e.printStackTrace();
	     }
	     valLis[4] = emailTemplate.getCategory();
	     valLis[5] = emailTemplate.getDaysGap();
	     valLis[6] = emailTemplate.getEvent();
	     valLis[7] = (emailTemplate.getStartDay() == null) ?null : new java.sql.Date(emailTemplate.getStartDay().getTimeInMillis());
	     valLis[8] = (emailTemplate.getEndDay() == null)? null : new java.sql.Date(emailTemplate.getEndDay().getTimeInMillis());
	     valLis[9] = (emailTemplate.getTimeOfDay() == null)? null : new java.sql.Time(emailTemplate.getTimeOfDay().getTime());
	     valLis[10] = emailTemplate.isDeleted();
	     valLis[11] = new java.sql.Timestamp(emailTemplate.getUpdationDate().getTimeInMillis());
	     valLis[12] = emailTemplate.getSendMailOption();
	     valLis[13] = emailTemplate.getUserListFile();
	     valLis[14] = emailTemplate.getFromAddress();
	     valLis[15] = emailTemplate.getLogo();
	     valLis[16] = emailTemplate.getUrl();
	     valLis[17] = emailTemplate.getPersonalAddress();
	     valLis[18] = emailTemplate.isSendOnToday();
	     valLis[19] = emailTemplate.getId();
	     
//	     emailTemplate.setSendOnToday(rs.getBoolean("send_mail_today")); 
	     
	     try {
	            getJdbcTemplate().update(query, valLis);
	     } catch (Exception ex) {
	            ex.printStackTrace();
	     }
	 }
	 public void updateTemplateStatus(final EmailTemplate emailTemplate){
		 final String query = "update mail_templates set status = ? where id = ?";
		 Object[] valLis = new Object[2];

	     valLis[0] = emailTemplate.isStatus();
	     valLis[1] = emailTemplate.getId();
	     
	     try {
	            getJdbcTemplate().update(query, valLis);
	     } catch (Exception ex) {
	            ex.printStackTrace();
	     }
	 }
	 public void updateLastMailSentDate(final EmailTemplate emailTemplate){
		 final String query = "update mail_templates set last_mail_sent = ? where id = ?";
		 try {
	            getJdbcTemplate().update(query,new java.sql.Timestamp(emailTemplate.getLastMailSent().getTimeInMillis()),emailTemplate.getId());
	     } catch (Exception ex) {
	            ex.printStackTrace();
	     }
	 }
	 public List<EmailTemplate> extractTemplatesfromDB(String query){
		 try {
			 List<EmailTemplate> emailTemplates = getJdbcTemplate().query(query, new RowMapper<EmailTemplate>() {			    
				 public EmailTemplate mapRow(ResultSet rs, int rowNum) throws SQLException {
					 EmailTemplate emailTemplate = new EmailTemplate();
					 
					 emailTemplate.setId(rs.getLong("id"));
					 emailTemplate.setTemplateName(rs.getString("temp_name"));
					 emailTemplate.setStatus(rs.getBoolean("status"));
					 emailTemplate.setMailSubject(rs.getString("mail_subject"));
					 
					 Blob blob = rs.getBlob("mail_body");
					 byte[] bdata = blob.getBytes(1, (int)blob.length());
					 try {
                        emailTemplate.setMailBody(new String(bdata,"UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        emailTemplate.setMailBody(new String(bdata));
                        e.printStackTrace();
                    }
					 emailTemplate.setCategory(rs.getInt("category"));
					 emailTemplate.setDaysGap(rs.getInt("days_gap"));
					 emailTemplate.setEvent(rs.getInt("event"));
					 
					 Calendar cal1 = Calendar.getInstance();
					 Timestamp a = rs.getTimestamp("start_date");
					 if(a!=null){
						 cal1.setTimeInMillis(a.getTime());
						 emailTemplate.setStartDay(cal1);
					 }
					 Calendar cal2 = Calendar.getInstance();
					 a = rs.getTimestamp("end_date");
					 if(a!=null){
						 cal2.setTimeInMillis(a.getTime());
						 emailTemplate.setEndDay(cal2);
					 }
					 emailTemplate.setTimeOfDay(rs.getTime("time_of_day"));
					 emailTemplate.setDeleted(rs.getBoolean("is_deleted"));
					 
					 Calendar cal3 = Calendar.getInstance();
					 Timestamp timestamp = rs.getTimestamp("last_mail_sent");
					 if(timestamp != null){
						 cal3.setTimeInMillis(timestamp.getTime());
						 emailTemplate.setLastMailSent(cal3);
					 }
					 Calendar cal4 = Calendar.getInstance();
					 timestamp = rs.getTimestamp("updation_date");
					 if(timestamp != null){
						 cal4.setTimeInMillis(timestamp.getTime());
						 emailTemplate.setUpdationDate(cal4);
					 }
					 emailTemplate.setSendMailOption(rs.getInt("send_mail_option")); 
					 emailTemplate.setUserListFile(rs.getString("upload_file_name")); 
					 emailTemplate.setFromAddress(rs.getInt("from_address")); 
					 emailTemplate.setLogo(rs.getInt("logo")); 
					 emailTemplate.setUrl(rs.getString("url")); 
					 emailTemplate.setPersonalAddress(rs.getString("personal_address"));
					 emailTemplate.setSendOnToday(rs.getBoolean("send_mail_today")); 
					 return emailTemplate;
				 }
			 });
			 return emailTemplates;
		 }catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            return null;
	     }        
	 }
	 public void updateStatusInDB(String query){
		 try {
	            getJdbcTemplate().update(query);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	 }
	 
	 
	 public EmailTemplate getTemplateData(long tempId){
		 String query = "select id,temp_name,mail_subject,mail_body,from_address from mail_templates  where id=? and status=1 ";
		 try{
			 @SuppressWarnings("unchecked")
			EmailTemplate emailTemplate = (EmailTemplate)getJdbcTemplate().queryForObject(query, new Object[] { tempId },
		                new RowMapper() {
				 EmailTemplate emailTemplate;
                 public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                	 emailTemplate = new EmailTemplate();
                	 emailTemplate.setId(rs.getLong("id"));
                     emailTemplate.setTemplateName(rs.getString(2));
                	 emailTemplate.setMailSubject(rs.getString(3));
                	 Blob blob = rs.getBlob(4);
					 byte[] bdata = blob.getBytes(1, (int)blob.length());
					 emailTemplate.setMailBody(new String(bdata));
					 emailTemplate.setFromAddress(rs.getInt(5));
					 emailTemplate.setSendOnToday(rs.getBoolean("send_mail_today")); 
					 return emailTemplate;
                 }
            });
			 return emailTemplate;
		 }catch (Exception e) {
	            logger.error(e.getMessage());
	            logger.error(e.getCause());
	            return null;
	    } 
		
	 }
	 public boolean checkExistance2(String query){
		 int n = getJdbcTemplate().queryForInt(query);
		 if(n == 0){
			 return false;
		 }else{
			 return true;
		 }
	 }
	 public boolean checkExistance(String sql, Object [] args, int [] argTypes){
		 int n = getJdbcTemplate().queryForInt(sql, args, argTypes);
		 if(n == 0){
			 return false;
		 }else{
			 return true;
		 }
	 }
	 public List<EmailTemplate> getTemplatesForScheduler(final Time time){
		 String query = "select * from mail_templates where status = 1 and is_deleted = 0 and time_of_day ='"+time+"'"; 
		 logger.info(query);
		 return extractTemplatesfromDB(query);
	 }
	 public List<EmailTemplate> getTemplatesByInstantEvent(final int event){
		 String query = "select * from mail_templates where status = 1 and " +
		 		"is_deleted = 0 and category = 1 and event = "+event; 
		 logger.info(query);
		 return extractTemplatesfromDB(query);
	 }
	 public long getTemplateId(String sql, Object [] args, int [] argTypes){
		 return getJdbcTemplate().queryForLong(sql, args, argTypes);
	 }	 
	 
}
