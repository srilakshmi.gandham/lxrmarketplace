package lxr.marketplace.admin.automail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.FileItemFactory;
//import org.apache.tomcat.util.http.fileupload.IOUtils;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.IOUtils;

public class AttachmentService {
	private static Logger logger = Logger.getLogger(AttachmentService.class);
	String attachmentFolder;
	public String getAttachmentFolder() {
		return attachmentFolder;
	}
	public void setAttachmentFolder(String attachmentFolder) {
		this.attachmentFolder = attachmentFolder;
	}
	String temp = "temporary";
	final String headerFileName = "X-File-Name";
	
	public String saveFile(String realPath, HttpServletRequest request,
			HttpServletResponse response,String mainFolderPath) {
		String fileName = "";
		InputStream is = null;
		FileOutputStream fos = null;
		String [] files = getListOfFiles(mainFolderPath);
		boolean alreadyExist = false;
		if (request.getHeader(headerFileName) != null) {
			fileName = request.getHeader(headerFileName);
			logger.info("uploaded file name: " + fileName);
			if (fileName.trim().contains(" ")) {
				fileName = fileName.replace(" ", "%");
			}
			fileName = decodeFileName(fileName);
			if(files != null){
				for(String file : files){
					if(file.equals(fileName)){
						alreadyExist = true;
					}
				}
			}
			if(!alreadyExist){
				try {
					is = request.getInputStream();
					File f = new File(realPath + fileName);
					fos = new FileOutputStream(f);
					IOUtils.copy(is, fos);
					response.setContentType("application/json");
					response.setStatus(HttpServletResponse.SC_OK);
				} catch (Exception e) {
					logger.error(e.getMessage());
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				} finally {
					try {
						fos.close();
						is.close();
					} catch (IOException ignored) {
					}
				}
			}
		} else if (request.getParameter("fileName") != null) {
			fileName = request.getParameter("fileName");
			logger.info("Uploaded File Name:....... " + fileName);

			response.setContentType("text/html");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "No-cache");
			response.setDateHeader("Expires", 0);
			final long permitedSize = 314572800;
			long fileSize;
			try {
				boolean isMultipart = ServletFileUpload
						.isMultipartContent(request);
				if (isMultipart) {
					FileItemFactory factory = new DiskFileItemFactory();
					ServletFileUpload upload = new ServletFileUpload(factory);
					List items = upload.parseRequest(request);
					for (int i = 0; i < items.size(); i++) {
						FileItem fileItem = (FileItem) items.get(i);
						if (fileItem.isFormField()) {
							//String any_parameter = fileItem.getString();
						} else {
							fileName = fileItem.getName();
							fileName = decodeFileName(fileName);
							for(String file : files){
								if(file.equals(fileName)){
									alreadyExist = true;
								}
							}
							if(!alreadyExist){
								fileSize = fileItem.getSize();
								if (fileSize <= permitedSize) {
									File uploadedFile = new File(realPath + fileName);
									fileItem.write(uploadedFile);
								}else{
									logger.info("file is too big");
									String message = "File size of '"+fileName+"' is more than the maximum file size limit";
									addInJson(message, response);
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		if(alreadyExist){
			String message = "A file with name '"+fileName+"' already attached to this template mail.";
			addInJson(message, response);
		}
		return fileName;
	}
	public boolean createAttachmentFolder(long id){
		boolean created = false;
		String path = attachmentFolder+id;
		File f=new File(path);
		if(!f.isDirectory()){
			created = f.mkdir();
        }
		return created;
	}
	public void deleteFileFromTemporary(String fileName){
		String newfile= decodeFileName(fileName);
		
		File f = new File(attachmentFolder+temp+"/"+newfile);
		if(f.exists()){
			f.delete();
        }else{
        	logger.info("There is no file to remove from temporary folder with name "+ newfile);
        }
	}
/*	
	public void addFileInTemporary2(HttpServletRequest request, HttpServletResponse response){
		String [] files = getListOfFiles();
		String fileName = null;
		if (request.getHeader(headerFileName) != null) {
			fileName = request.getHeader(headerFileName);
		}else if (request.getParameter("fileName") != null) {
			fileName = request.getHeader(headerFileName);
		}
		fileName = decodeFileName(fileName);
		boolean alreadyExist = false;
		for(String file : files){
			if(file.equals(fileName)){
				alreadyExist = true;
			}
		}
		if(!alreadyExist){
			saveFile(attachmentFolder+temp+"/", request, response);
		}else{
			String message = "A file with name '"+fileName+"' already attached to this template mail.";
			addInJson(message, response);
		}
	}*/
	public String addFileInTemporary(String filePath,HttpServletRequest request, HttpServletResponse response){
		String fileName = saveFile(filePath+temp+"/", request, response,filePath);
		return fileName;
	}
	
	
	public String[] getListOfFiles(String filePath){
		String[] attachments = null;
		File source = new File(filePath+temp);
		if(source.isDirectory()){
			attachments = source.list();
		}		
		return attachments;
	}
	public String[] getListOfFilesById(long id){
		String[] attachments = null;
		logger.info(attachmentFolder+id + "path");
		File source = new File(attachmentFolder+id);
		if(source.isDirectory()){
			attachments = source.list();
		}		
		return attachments;
	}
	
	public void createTemporaryFolder(String filePath){
		File f = new File(filePath + temp);
		if(f.isDirectory()){
			deleteDirectory(f);
        }
		f.mkdir();
	}
	
	public boolean deleteDirectory(File f){
		if(f.isDirectory()){
			String[] children = f.list();
	        for (int i=0; i<children.length; i++) {
	           File childPath = new File(f, children[i]);
	           childPath.delete();
	        }
        }
		return f.delete();
	}
	public boolean deleteTemporaryFolder(String filePath){
		File f = new File(filePath+temp);
		if(f.isDirectory()){
			String[] children = f.list();
	        for (int i=0; i<children.length; i++) {
	           File childPath = new File(f, children[i]);
	           childPath.delete();
	        }
        }
		return f.delete();
	}
	public void copyFromAttachmentToTemporary(String filePath,long id){
		createTemporaryFolder(filePath);
		File source = new File(filePath+id);
		File desc = new File(filePath+temp);
		if(source.isDirectory()){
			try {
			    FileUtils.copyDirectory(source, desc);
			} catch (IOException e) {
			    e.printStackTrace();
			}
			
		}
	}
	public void replaceAttachmentFolderByTemporary(String filePath,long id){
		File source = new File(filePath+temp);
		File desc = new File(filePath+id);
		if(desc.isDirectory()){
			deleteDirectory(desc);
		}
		if(source.isDirectory()){
			if(source.list().length > 0){
				desc.mkdir();
				try {
				    FileUtils.copyDirectory(source, desc);
				} catch (IOException e) {
				    e.printStackTrace();
				}
			}
			deleteDirectory(source);
		}
	}
	public String decodeFileName(String fileName){
		String newfile = null;
		ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        fileName = fileName.replaceAll("'","\\\\'");
        try {
        	newfile = (String) engine.eval("decodeURIComponent('"+fileName+"')");
			//engine.eval("print(encodeURIComponent("+fileName+"))");
			//logger.info(engine.eval("print(encodeURIComponent("+fileName+"))"));
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return newfile;
	}
	public void addInJson(String message, HttpServletResponse response){
        JSONArray arr = null;
		arr = new JSONArray();
		PrintWriter writer = null;
		
		try {
			writer = response.getWriter();
	        arr.add(0, message);
	        writer.print(arr);    
		} catch (Exception e) {
			logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");         
		}finally {
			writer.flush();
            writer.close();        
		}
	}
	
	public boolean validateUserListFile(String filePath,String fileName){
		
		
		return true;
	}
}
