package lxr.marketplace.admin.automail;

public class FormRow {
	private long tempId;
	private String templateName;
	private int category;
	private boolean status;
	private String mailSubject;
	boolean deleted;
	private String lastMailSent;
	private int sendMailOption;
	private String userListFile;
	private int fromAddress;
	private int logo;
	private String url;
	private String personalAddress;
	private boolean sendOnToday;
	
	
	public boolean isSendOnToday() {
		return sendOnToday;
	}
	public void setSendOnToday(boolean sendOnToday) {
		this.sendOnToday = sendOnToday;
	}
	public int getLogo() {
		return logo;
	}
	public void setLogo(int logo) {
		this.logo = logo;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPersonalAddress() {
		return personalAddress;
	}
	public void setPersonalAddress(String personalAddress) {
		this.personalAddress = personalAddress;
	}
	public int getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(int fromAddress) {
		this.fromAddress = fromAddress;
	}
	public int getSendMailOption() {
		return sendMailOption;
	}
	public void setSendMailOption(int sendMailOption) {
		this.sendMailOption = sendMailOption;
	}
	public String getUserListFile() {
		return userListFile;
	}
	public void setUserListFile(String userListFile) {
		this.userListFile = userListFile;
	}
	public String getLastMailSent() {
		return lastMailSent;
	}
	public void setLastMailSent(String lastMailSent) {
		this.lastMailSent = lastMailSent;
	}
	public long getTempId() {
		return tempId;
	}
	public void setTempId(long tempId) {
		this.tempId = tempId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
