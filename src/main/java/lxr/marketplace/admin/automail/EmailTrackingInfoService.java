package lxr.marketplace.admin.automail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.user.Login;

public class EmailTrackingInfoService  extends JdbcDaoSupport{
	private static Logger logger = Logger.getLogger(EmailTrackingInfoService.class);  
	private long eventdetailId;
	ApplicationContext ctx= ApplicationContextProvider.getApplicationContext();
	
	public synchronized String appendUserName(Login user,String bodyText,EmailTemplate emailTemplate){
		  if(ctx==null){
			  EmailTrackingInfoService service = new EmailTrackingInfoService();
			  ctx = service.ctx;
		  }
		  String applicationUrl = (String)ctx.getBean("domainName");
//		  applicationUrl = "http://www.lxrmarketplace.com";
		  String appendImage = "";
		  String personalAddress = "";
		  String trackPrams="";
		  String tempUrl = "";
		  if((emailTemplate.getLogo() == 1 || emailTemplate.getLogo() == 2) &&
				  emailTemplate.getUrl()!=null && !emailTemplate.getUrl().trim().equals("")){
			  tempUrl = emailTemplate.getUrl();
			  if(emailTemplate.getUrl().endsWith("/")){
				  tempUrl = emailTemplate.getUrl().substring(0, emailTemplate.getUrl().length()-1);
				  logger.debug("............"+tempUrl);
			  }
			  if(emailTemplate.getUrl().contains("?")){
				  trackPrams="&lm_uid="+user.getId()+"&eve_id="+eventdetailId;
			  }else{
				  trackPrams="?lm_uid="+user.getId()+"&eve_id="+eventdetailId;
			  }
		 }
		if(emailTemplate.getLogo() == 1){
			appendImage = "<a href=\""+tempUrl+trackPrams+"\"> " +
	                		"<img src='"+applicationUrl+"/css/0/images/LXRMarketplace.png' " +
	                		"alt='lxrmarketplace.com' /></a>";
		}else if(emailTemplate.getLogo()== 2){
			appendImage = "<a href=\""+tempUrl+trackPrams+"\"> " +
            		"<img src='"+applicationUrl+"/css/0/images/SEONinja/lxr-seo-logo.png' " +
            		"alt='lxrseo.com' /></a>";
		}
    	if(user.getName()!=null && !user.getName().trim().equals("")){
    		if(emailTemplate.getPersonalAddress() != null && !emailTemplate.getPersonalAddress().trim().equals("")){
    			if(emailTemplate.getPersonalAddress().contains("<user>")){
    				personalAddress = emailTemplate.getPersonalAddress().replace("<user>", user.getName())+",";
    			}
    		}
    		bodyText = bodyText.replace("<body>","<body style=\"font-style=arial\">"+appendImage+"<br><br>"+personalAddress+"<br><br>");
    	}
    	return bodyText;
	}
	
	
	public synchronized String appendUserNameVersion2(Login user,String bodyText,EmailTemplate emailTemplate){
		  if(ctx==null){
			  EmailTrackingInfoService service = new EmailTrackingInfoService();
			  ctx = service.ctx;
		  }
		  String applicationUrl = (String)ctx.getBean("domainName");
//		  applicationUrl = "http://www.lxrmarketplace.com";
		  String appendImage = "";
		  String personalAddress = "";
		  if(emailTemplate.getLogo() == 1){
			appendImage = "<a href=\""+emailTemplate.getUrl()+"\"> " +
	                		"<img src='"+applicationUrl+"/css/0/images/LXRMarketplace.png' " +
	                		"alt='lxrmarketplace.com' /></a>";
		}else if(emailTemplate.getLogo()== 2){
			appendImage = "<a href=\""+emailTemplate.getUrl()+"\"> " +
          		"<img src='"+applicationUrl+"/css/0/images/SEONinja/lxr-seo-logo.png' " +
          		"alt='lxrseo.com' /></a>";
		}
  	if(user.getName()!=null && !user.getName().trim().equals("")){
  		if(emailTemplate.getPersonalAddress() != null && !emailTemplate.getPersonalAddress().trim().equals("")){
  			if(emailTemplate.getPersonalAddress().contains("<user>")){
  				personalAddress = emailTemplate.getPersonalAddress().replace("<user>", user.getName())+",";
  			}
  		}
  		bodyText = bodyText.replace(bodyText,"<body>"+appendImage+"<br><br>"+personalAddress+"<br><br>"+bodyText);
  	}
  	return bodyText;
	}
	
    public synchronized String appendTrackingParameters(String appUrl,Login user,String bodyText){
    	String trackPrams="lm_uid="+user.getId()+"&eve_id="+eventdetailId;
    	String appendimage = "<img style=\"width:1px;height:1px;\" src=\""+appUrl+"/lxrmarketplace.html?pixel.gif&"+trackPrams+"\"/>";
    	bodyText = bodyText.replaceAll("lm_mail",trackPrams);
    	bodyText = bodyText.replace("</body>",appendimage+"</body>");
    	if(!user.isUnsubscribed()){
    		bodyText = addUnSubscriptionlink(appUrl,user,bodyText);
    	}
    	return bodyText;
	}
    
    public synchronized String addUnSubscriptionlink(String appUrl,Login user,String bodyText){
    	String unsubscribe="lm_uid="+user.getId()+"&email="+user.getUserName()+"&lxrmp";
    	String appendUnSubscribeLink = "<br><a target=\"_blank\" href="+appUrl+"/useractivation.html?unsubscribe&"+unsubscribe+">Unsubscribe here</a>";
    	bodyText = bodyText.replace("</body>",appendUnSubscribeLink+"</body>");
    	return bodyText;
	}
    
    public synchronized Long addEventtoDB(final EmailTemplate emailTemplate){
    	long eventDetaiId=0;
    	if(emailTemplate.getCategory() == 1 || emailTemplate.getCategory() == 4 ){
    		eventDetaiId =fetchEventDetaiId(emailTemplate.getId());
    	 }
    	if(eventDetaiId<=0){
		 final String query = "insert into event_details(mail_template_id, sent_date) values(?,?)";
		 KeyHolder keyHolder = new GeneratedKeyHolder();
		 logger.info(query);
		 try {
	            synchronized(this) {
	                getJdbcTemplate().update(new PreparedStatementCreator() {
	                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
	                        PreparedStatement statement = (PreparedStatement) con.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
	                        statement.setInt(1, (int)emailTemplate.getId());
	                        statement.setTimestamp(2, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
	                        return statement;
	                    }
	                }, keyHolder);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        eventdetailId = (Long) keyHolder.getKey();
	        return (Long) keyHolder.getKey();
	    	}
    	else{
    		return eventDetaiId;
    	}
    }
    public long fetchEventDetaiId(long templateId){
    	long eventDetailId=0;
    	String query = "select event_detail_id from event_details where mail_template_id="+templateId;
    	 try {
    		 eventDetailId = getJdbcTemplate().queryForLong(query);
    		 logger.info("eventDetailId : "+eventDetailId);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
    	return eventDetailId;
    }
    public synchronized void insertUserEvents(Login user){
    	String query = "insert into user_event(event_detail_id, user_id) values(?,?)";
    	Object[] valLis = new Object[2];
   	    valLis[0] = eventdetailId;
		valLis[1] = user.getId();
	        try {
	            getJdbcTemplate().update(query, valLis);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
    	
    }
    
    public int findEventId(long eventDetailId,long userId,String action){
    	int count = 0;
    	String query=null;
    	if(action.equalsIgnoreCase("opens")){
    	    query = "select count(user_id) from event_track_details where event_detail_id="+eventDetailId
    	            +" and user_id="+userId;
    	}else if(action.equalsIgnoreCase("visits")){
    	    query = "select if(no_of_visits=0,-1,no_of_visits) from event_track_details where " +
    	    		"event_detail_id="+eventDetailId+" and user_id="+userId+" and no_of_visits>=0 ";		
    	}
    	try{
    	    count = getJdbcTemplate().queryForInt(query);
    	}catch (Exception e) {
		    logger.error(e.getMessage());
		}
    	return count;
    }
    
    
    public void insertOpens(long eventdetailId,long user_Id){
    	int count = findEventId(eventdetailId,user_Id,"opens");
    	String query=null;
    	if(count>=1){
    	// query ="update email_tracking(visited_date,no_of_visits) values(?,?)";
    	}else{
    	 query ="insert into event_track_details(event_detail_id,user_id,opened_date) values(?,?,?)"; 
    	
    	Object[] valLis = new Object[3];
    	 
    	 valLis[0] = eventdetailId;
    	 valLis[1] = user_Id;
		 valLis[2] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
	    
	        try {
	            getJdbcTemplate().update(query, valLis);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
    	}
    }
    
    public void insertOrUpdateVisits(long eventdetailId,long userId){
    	int count = findEventId(eventdetailId,userId,"visits");
    	String query=null;
    	Object[] valLis=null;
    	if(count > 0 || count == -1 ){
    	    query ="update event_track_details set visited_date=?, no_of_visits=?  where event_detail_id="+eventdetailId+" and user_id="+userId;
    	 
    	    valLis = new Object[2];
    	    valLis[0] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
    	    if(count == -1){
    	        valLis[1] = 1;
    	    }else{
    	        valLis[1] = ++count;
    	    }
    	}else if(count == 0){
    	    query ="insert into event_track_details(event_detail_id,user_id,opened_date,visited_date,no_of_visits) values(?,?,?,?,?)"; 
    	
    	    valLis = new Object[5];
    	    valLis[0] = eventdetailId;
    	    valLis[1] = userId;	 
    	    valLis[2] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
    	    valLis[3] = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());
    	    valLis[4] = 1;
    	}
        try {
            getJdbcTemplate().update(query, valLis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    	
    }
    
    
    }
