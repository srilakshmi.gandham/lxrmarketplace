package lxr.marketplace.admin.automail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

public class Automail {
	private List formRows = 
	    LazyList.decorate(new ArrayList(),FactoryUtils.instantiateFactory(FormRow.class));
	private EmailTemplate emailTemplate;
	String fromDate;
	String toDate;
	String testmail;

	
//	private int sendMailOption;
//	private String userListFile;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List getFormRows() {
		return formRows;
	}
	public void setFormRows(List formRows) {
		this.formRows = formRows;
	}
	public EmailTemplate getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(EmailTemplate emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	public String getTestmail() {
		return testmail;
	}
	public void setTestmail(String testmail) {
		this.testmail = testmail;
	}
	
}
