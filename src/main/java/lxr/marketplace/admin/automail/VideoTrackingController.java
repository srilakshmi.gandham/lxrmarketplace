package lxr.marketplace.admin.automail;

import java.net.BindException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;
import lxr.marketplace.session.Session;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ui.UserReviewDisplayController;


public class VideoTrackingController extends SimpleFormController{
	
	public VideoTrackingController() {
		setCommandClass(VideoUsage.class);
		setCommandName("videousage");
	}

	
	
	private static Logger logger = Logger.getLogger(UserReviewDisplayController.class);
	
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		HttpSession session = request.getSession();
		if(WebUtils.hasSubmitParameter(request, "toolvideotrack")){
		Common.modifyDummyUserInToolorVideoUsage(request, response);
		Session userSession = (Session) session.getAttribute("userSession");
		userSession.addVideoUsage(Long.parseLong(request.getParameter("toolvideotrack")));
		logger.info("video tracking");
		return null;
		}
		return null;
	}
}
