package lxr.marketplace.admin.automail;

import java.sql.Time;
import java.util.Calendar;

public class EmailTemplate {
	private long id;
	private String templateName;
	private boolean status;
	private String mailSubject;
	private String mailBody;
	private int category;
	private int daysGap;
	private int event;
	private Calendar startDay;
	private Calendar endDay;
	private Time timeOfDay;
	private boolean deleted;
	private Calendar lastMailSent;
	private Calendar creationDate;
	private Calendar updationDate;
	private int sendMailOption;
	private String userListFile;
	private int fromAddress;
	private int logo;
	private String url;
	private String personalAddress;
	private boolean sendOnToday;
	
	
	public boolean isSendOnToday() {
		return sendOnToday;
	}
	public void setSendOnToday(boolean sendOnToday) {
		this.sendOnToday = sendOnToday;
	}
	public int getLogo() {
		return logo;
	}
	public void setLogo(int logo) {
		this.logo = logo;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPersonalAddress() {
		return personalAddress;
	}
	public void setPersonalAddress(String personalAddress) {
		this.personalAddress = personalAddress;
	}
	public int getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(int fromAddress) {
		this.fromAddress = fromAddress;
	}
	public int getSendMailOption() {
		return sendMailOption;
	}
	public void setSendMailOption(int sendMailOption) {
		this.sendMailOption = sendMailOption;
	}
	public String getUserListFile() {
		return userListFile;
	}
	public void setUserListFile(String userListFile) {
		this.userListFile = userListFile;
	}
	public Calendar getLastMailSent() {
		return lastMailSent;
	}
	public void setLastMailSent(Calendar lastMailSent) {
		this.lastMailSent = lastMailSent;
	}
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	public Calendar getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Calendar updationDate) {
		this.updationDate = updationDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getDaysGap() {
		return daysGap;
	}
	public void setDaysGap(int daysGap) {
		this.daysGap = daysGap;
	}
	public int getEvent() {
		return event;
	}
	public void setEvent(int event) {
		this.event = event;
	}
	public Calendar getStartDay() {
		return startDay;
	}
	public void setStartDay(Calendar startDay) {
		this.startDay = startDay;
	}
	public Calendar getEndDay() {
		return endDay;
	}
	public void setEndDay(Calendar endDay) {
		this.endDay = endDay;
	}
	public Time getTimeOfDay() {
		return timeOfDay;
	}
	public void setTimeOfDay(Time timeOfDay) {
		this.timeOfDay = timeOfDay;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
