package lxr.marketplace.admin.report;

public class Report {
	private int reportType;
	private int dateRangeOption;
	private String fromDate;
	private String toDate;
	private int fileType;
	private boolean includeLocal;
	private boolean includeVideo;

	public boolean isIncludeVideo() {
		return includeVideo;
	}
	public void setIncludeVideo(boolean includeVideo) {
		this.includeVideo = includeVideo;
	}
	public boolean isIncludeLocal() {
		return includeLocal;
	}
	public void setIncludeLocal(boolean includeLocal) {
		this.includeLocal = includeLocal;
	}
	public void setFromDate(String fromDate){
		this.fromDate = fromDate;
	}
	public String getFromDate(){
		return this.fromDate;
	}
	
	public void setToDate(String toDate){
		this.toDate = toDate;
	}
	public String getToDate(){
		return this.toDate;
	}
	
	public void setReportType(int reportType){
		this.reportType = reportType;
	}
	public int getReportType(){
		return this.reportType;
	}
	
	public void setFileType(int fileType){
		this.fileType = fileType;
	}
	public int getFileType(){
		return this.fileType;
	}
	
	public void setDateRangeOption(int dateRangeOption){
		this.dateRangeOption = dateRangeOption;
	}
	public int getDateRangeOption(){
		return this.dateRangeOption;
	}
	
}
