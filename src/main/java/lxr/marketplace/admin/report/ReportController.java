package lxr.marketplace.admin.report;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.user.Login;

import org.apache.log4j.Logger;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import lxr.marketplace.util.Common;

@SuppressWarnings("deprecation")
public class ReportController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(ReportController.class);
    ReportService reportService;
    String uploadFolder;
    String downloadFolder;

    public ReportController() {
        setCommandClass(Report.class);
        setCommandName("report");
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public void setDownloadFolder(String downloadFolder) {
        this.downloadFolder = downloadFolder;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Report report = (Report) command;

        logger.debug("In Report Controller");
        int repType = report.getReportType();
        boolean tdate = true;
        String fileName = "";
        // String appPath =
        // session.getServletContext().getInitParameter("path")+ "temp/";
        Login user = (Login) session.getAttribute("user");

        String fromDateInDMonY = report.getFromDate();
        String toDateInDMonY = report.getToDate();

        String fromDateInYmd = Common.changeDateStringFormat(fromDateInDMonY,
                "dd-MMM-yyyy", "yyyy-MM-dd HH:mm:ss");
        String toDateInYmd = Common.changeDateStringFormatWithTime(
                toDateInDMonY, "dd-MMM-yyyy", "yyyy-MM-dd HH:mm:ss");
        String dateRange = fromDateInDMonY + " to " + toDateInDMonY;
        logger.info("local" + report.isIncludeLocal() + ", from date:" + fromDateInYmd + ", to date:" + toDateInYmd);
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            session.setAttribute("report", report);
            if (WebUtils.hasSubmitParameter(request, "download")) {
                int dwnldTyp = report.getFileType();
                boolean includevideousage = report.isIncludeVideo();
                if (repType == 1) {
                    fileName = reportService.toolUsageDwnld(fromDateInYmd,
                            toDateInYmd, downloadFolder, dwnldTyp, dateRange, report.isIncludeLocal(), includevideousage);
                } else if (repType == 2) {
                    fileName = reportService.userBehaviourDwnld(fromDateInYmd,
                            toDateInYmd, downloadFolder, dwnldTyp, dateRange, report.isIncludeLocal());
                } else if (repType == 3) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.activeUserDwnld(fromDateInYmd,
                            toDateInYmd, downloadFolder, dwnldTyp, dateRange,
                            tdate, report.isIncludeLocal());
                } else if (repType == 4) {
                    String eventdetailId = request.getParameter("eventDetaiId");
                    if (eventdetailId != null) {
                        long event_detail_id = Long.parseLong(eventdetailId);
                        fileName = reportService.userSummaryDetails(downloadFolder, dwnldTyp, event_detail_id, report.isIncludeLocal());
                    } else {
                        ModelAndView mAndV = new ModelAndView("view/admin/report/Report", "report",  new Report());
                        return mAndV;
                    }
                } else if (repType == 9) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.lxrmSalesReport(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 11) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.reportLXRPluginUsage(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 12) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.reportLXRPluginPayment(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 13) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.lxrPluginUserBehaviourReport(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 14) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.seoTipUsresDetails(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 15) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.getAskTheExpertPayedUsersList(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 16) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.getUsersWebsiteTrackingInfoList(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                } else if (repType == 17) {
                    if (report.getDateRangeOption() == 6) {
                        tdate = true;
                    } else {
                        tdate = false;
                    }
                    fileName = reportService.subscriptionReport(fromDateInYmd, toDateInYmd, downloadFolder, dwnldTyp, dateRange, tdate, report.isIncludeLocal());
                }
                session.setAttribute("report", report);
                File f = new File(fileName);
                logger.info("filename: " + fileName);
                // response.setContentType("xls");
                if (dwnldTyp == 1) {
                    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                } else {
                    response.setContentType("application/vnd.ms-excel");
                }
                response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
                FileInputStream fis = new FileInputStream(f);
                FileCopyUtils.copy(fis, response.getOutputStream());

                return null;
            } else if (WebUtils.hasSubmitParameter(request, "dashboard")) {
                logger.info("moving to dashboard");
                ModelAndView mAndV = new ModelAndView(
                        "view/admin/dashboard/Dashboard");
                return mAndV;

            } else if (WebUtils.hasSubmitParameter(request, "automail")) {
                logger.info("moving to automail tab");
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "settings")) {
                logger.info("moving to Settings tab");
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/settings.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "ateReports")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-reports.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "pluginAdoption")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/plugin-adoption.html");
                return null;
            }
            ModelAndView mAndV = new ModelAndView("view/admin/report/Report", "report", report);
            return mAndV;
        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }
    }

    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        HttpSession session = request.getSession();
        Report report = (Report) session.getAttribute("report");
        if (report == null) {
            report = new Report();
            String today = (new SimpleDateFormat("dd-MMM-yyyy"))
                    .format(new Date());

            report.setReportType(0);
            report.setDateRangeOption(0);
            report.setFromDate(today);
            report.setToDate(today);
            report.setFileType(2);
            report.setIncludeLocal(false);
        }
        return report;

    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

}
