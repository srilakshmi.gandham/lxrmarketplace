package lxr.marketplace.admin.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Date;
import java.sql.Types;
import java.util.Calendar;

import lxr.marketplace.util.Common;
import lxr.marketplace.util.encryption.EncryptAndDecryptUsingDES;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.InvalidResultSetAccessException;

public class ReportService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(ReportService.class);

    @Autowired
    private String domainName;

    public String lxrseoRevenueReport(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 8;
        String subquery = "";
        if (!includeLocal) {
            subquery = " and is_local = 0 ";
        }
        if (tdate) {
            fromDate = "2013-11-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRSEO User Payment Report";

        String query = "SELECT user.id AS 'User Id', user.name AS 'User Name', user.email AS 'Email', "
                + "A.times AS 'No. of times user paid', A.total_amount AS 'Total Amount($)' FROM "
                + "(SELECT payer_user_id, SUM(CASE WHEN intent='refund' THEN -1 ELSE 1 END) AS times, "
                + "SUM(CASE WHEN intent='refund' THEN -(amount) ELSE amount END) AS total_amount FROM "
                + "paypal_transaction_stats WHERE (intent = 'sale' OR intent = 'capture' OR intent = 'refund') "
                + "AND (state = 'approved' OR state = 'completed') AND tool_id = 12 AND DATE_FORMAT"
                + "(created_time, '%Y-%m-%d') BETWEEN DATE_FORMAT(?,'%Y-%m-%d') AND "
                + "DATE_FORMAT(?,'%Y-%m-%d') GROUP BY payer_user_id)A JOIN (SELECT * FROM user WHERE deleted = 0  "
                + subquery + ")user WHERE A.payer_user_id = user.id ";

        try {
            logger.info("LXRSEO Revenue Report:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = createXLSXFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = createXLSFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }

    public String lxrmSalesReport(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {
        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 9;
        String subquery = "";
        String query = "";
        if (!includeLocal) {
            subquery = " and is_local = 0 ";
        }
        String repName = "LXRMarketplace Sales Report";

        query = "select total.* from (select  lxrm.payment_date as Payment_Date,coalesce(lxrm.web_analyzer,0) as 'SEO Webpage Analysis Amount($)',"
                + " coalesce(lxrm.inbound,0) as 'Inbound Link Checker Amount($)',coalesce(lxrseo.lxrseoamt,0) as 'LXRSEO Amount($)' from"
                + " (select DATE_FORMAT(A.created_time,'%Y-%m-%d') as payment_date, "
                + " SUM(CASE  WHEN tool_id = 8 AND intent='refund' THEN -(amount) WHEN tool_id = 8 THEN amount END )as web_analyzer,"
                + " SUM(CASE WHEN tool_id = 20 AND intent='refund' THEN -(amount) WHEN tool_id = 20 THEN amount END ) as inbound "
                + " from (select payer_user_id,tool_id,amount,created_time,intent "
                + " from  lxrm_payment_transaction_stats where created_time between"
                + "  ? and ?)A JOIN (select id from user where deleted = 0 " + subquery + ")B"
                + " on A.payer_user_id = B.id group by DATE_FORMAT(created_time,'%Y-%m-%d'))lxrm"
                + " LEFT JOIN (select DATE_FORMAT(C.created_time,'%Y-%m-%d') as payment_date1, "
                + " SUM(CASE WHEN intent='refund' THEN -(amount) ELSE amount END) AS lxrseoamt"
                + " from (select payer_user_id,tool_id,amount,intent,created_time"
                + " from  paypal_transaction_stats WHERE (intent = 'sale' OR intent = 'capture' OR intent = 'refund') "
                + " AND (state = 'approved' OR state = 'completed') AND tool_id = 12"
                + " AND created_time between ? and ?)C "
                + " JOIN (select id from user where deleted = 0 " + subquery + ")D"
                + " on C.payer_user_id = D.id group by DATE_FORMAT(created_time,'%Y-%m-%d'))lxrseo"
                + " on DATE_FORMAT(lxrm.payment_date,'%Y-%m-%d')= DATE_FORMAT(lxrseo.payment_date1,'%Y-%m-%d')"
                + " UNION"
                + " select  lxrseo.payment_date1 as Payment_Date, coalesce(lxrm.web_analyzer,0) as 'SEO Webpage Analysis Amount($)',"
                + " coalesce(lxrm.inbound,0) as 'Inbound Link Checker Amount($)' , coalesce(lxrseo.lxrseoamt,0) as 'LXRSEO Amount($)' from "
                + " (select DATE_FORMAT(A.created_time,'%Y-%m-%d') as payment_date, "
                + " SUM(CASE  WHEN tool_id = 8 AND intent='refund' THEN -(amount) WHEN tool_id = 8 THEN amount END )as web_analyzer,"
                + " SUM(CASE WHEN tool_id = 20 AND intent='refund' THEN -(amount) WHEN tool_id = 20 THEN amount END ) as inbound  "
                + " from (select payer_user_id,tool_id,amount,created_time,intent"
                + " from  lxrm_payment_transaction_stats where created_time between ? and ?)A "
                + " JOIN (select id from user where deleted = 0 " + subquery + ")B on A.payer_user_id = B.id group by DATE_FORMAT(created_time,'%Y-%m-%d'))lxrm"
                + " RIGHT JOIN (select DATE_FORMAT(C.created_time,'%Y-%m-%d') as payment_date1, "
                + " SUM(CASE WHEN intent='refund' THEN -(amount) ELSE amount END) AS lxrseoamt"
                + " from (select payer_user_id,tool_id,amount,intent,created_time"
                + " from  paypal_transaction_stats WHERE (intent = 'sale' OR intent = 'capture' OR intent = 'refund') "
                + " AND (state = 'approved' OR state = 'completed') AND tool_id = 12 AND created_time "
                + " between ? and ?)C JOIN (select id from user where deleted = 0 " + subquery + ")D"
                + " on C.payer_user_id = D.id group by DATE_FORMAT(created_time,'%Y-%m-%d'))lxrseo"
                + " on DATE_FORMAT(lxrm.payment_date,'%Y-%m-%d')= DATE_FORMAT(lxrseo.payment_date1,'%Y-%m-%d'))total"
                + " ORDER BY total.Payment_Date ASC";
        try {
            logger.info("LXRMarketplace:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate, fromDate, toDate, fromDate, toDate, fromDate, toDate});
            logger.debug("sqlrowset is  :" + sqlRowSet);
            if (dwnldTyp == 1) {
                flag = createXLSXFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = createXLSFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
        } catch (Exception e) {
            logger.error("Exception for lxrmSalesReport",e);
            
        }
        return flag;
    }

    public String toolUsageDwnld(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean includeLocal, boolean includevideousage) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 1;
        String subquery = "";
        String query = "";
        if (!includeLocal) {

            subquery = " and (select is_local from user where id=tot_session.user_id)=0 ";
        }
        String repName = "Tool Usage Report";

        if (!includevideousage) {
            query = " select u.id as 'User Id',u.name as 'Name',u.email as 'Email Id',date_format(u.reg_date,'%d-%b-%y') as 'First Visited Date',tot_session.Login_Date as 'Tool Usage Date',time(tot_session.Session_Start_Time) as 'Session Start Time',"
                    + " time(tot_session.Session_End_Time) as 'Session End Time',tot_session.duration as 'Time Spent',tot_session.ROI_Usage_Session as 'ROI Calculator Usage / Session',tot_session.KeywordCombination_Usage_Session as 'Keyword Combinator Usage / Session',"
                    + " tot_session.URL_Auditor_Usage_Session as 'URL Auditor Usage / Session',tot_session.KeyPerformance  as 'Keyword Performance Analyzer Usage / Session' ,tot_session.Sitemap_Builder as 'Sitemap Builder Usage / Session',"
                    + " tot_session.Competitor_Analysis   as 'Competitor Analysis Usage / Session' ,tot_session.Spider_View  as 'SEO Webpage Analysis Usage / Session' ,tot_session.Comp_web_Monitor   as 'Competitor Webpage Monitor Usage / Session',tot_session.keywrd_Rank_Checker as 'Weekly Keyword Rank Checker Usage / Session',"
                    + " tot_session.site_grader as 'Site Grader Usage / Session',tot_session.metatag_genarator as 'Meta Tag Generator Usage / Session',tot_session.Inbound_link_checker as 'Inbound Link Checker Usage / Session',tot_session.Dashboard as 'LXR SEO Dashboard Usage / Session',tot_session.competitor_research_tool as 'Competitor Research Usage / Session',"
                    + " tot_session.website_auditor_tool as 'Website Auditor Usage / Session',tot_session.Lxrm_SEO_Inbound_link_checker as 'SEO Inbound Link Checker Usage / Session' ,"
                    + " tot_session.Domain_Age_Checker as 'Domain Age Checker Usage / Session',tot_session.Robots_Txt_Generator as 'Robots.Txt Generator Usage / Session', tot_session.Robots_Txt_Validator as 'Robots.Txt Validator Usage / Session',"
                    + " tot_session.Lxrm_Meta_Tag_Generator  as 'Lxrm Meta Tag Generator Usage / Session', tot_session.Lxrm_DNS_Lookup as 'DNS Lookup Usage / Session',"
                    + " tot_session.Lxrm_Social_Media_Tool as 'Social Media Tool Usage / Session', tot_session.Top_Ranked_Websites_byKeyword as 'Top Ranked Websites by keyword Tool Usage / Session',"
                    + " tot_session.Broken_Link_Checker as 'Broken Link Checker Tool Usage / Session',tot_session.Most_Used_Keywords as 'Most Used Keywords Tool Usage / Session',tot_session.PageSpeed_Insights as 'PageSpeed Insights Tool Usage / Session', "
                    + " tot_session.SEO_Penalty_Checker as 'SEO Penalty Checker Tool Usage / Session', tot_session.Structured_Data_Generator as 'Structured Data Generator Tool Usage / Session', "
                    + " tot_session.Micro_Influencer_Generator as 'Micro Influencer Generator Tool Usage / Session' from "
                    + " (select id,user_id,duration,date_format(s.start_time,'%d-%b-%Y') as 'Login_Date',s.start_time as 'Session_Start_Time', s.end_time as 'Session_End_Time',test.* from "
                    + " (select session_id,sum(if(tool_id = '1',count,0)) as 'URL_Auditor_Usage_Session',sum(if(tool_id = '2',count,0))  as 'KeywordCombination_Usage_Session' ,	sum(if(tool_id = '3',count,0)) as 'ROI_Usage_Session',sum(if(tool_id = '4',count,0)) as  KeyPerformance ,"
                    + " sum(if(tool_id = '5',count,0)) as 'Amazon_Price_Tracker',sum(if(tool_id = '6',count,0)) as 'Sitemap_Builder',sum(if(tool_id = '7',count,0)) as 'Competitor_Analysis' ,sum(if(tool_id = '8',count,0)) as 'Spider_View' ,"
                    + " sum(if(tool_id = '9',count,0)) as 'Comp_web_Monitor',sum(if(tool_id = '10',count,0)) as 'keywrd_Rank_Checker',sum(if(tool_id = '11',count,0)) as site_grader,sum(if(tool_id = '13',count,0)) as metatag_genarator,sum(if(tool_id = '14',count,0)) as Inbound_link_checker,"
                    + " sum(if(tool_id = '15',count,0)) as Dashboard,sum(if(tool_id = '16',count,0)) as competitor_research_tool,sum(if(tool_id = '17',count,0)) as website_auditor_tool,"
                    + " sum(if(tool_id = '20',count,0)) as 'Lxrm_SEO_Inbound_link_checker',"
                    + " sum(if(tool_id = '21',count,0)) as 'Domain_Age_Checker',"
                    + " sum(if(tool_id = '24',count,0)) as 'Robots_Txt_Generator', sum(if(tool_id = '25',count,0)) as 'Robots_Txt_Validator',"
                    + " sum(if(tool_id = '27',count,0)) as 'Lxrm_Meta_Tag_Generator', sum(if(tool_id = '28',count,0)) as 'Lxrm_DNS_Lookup',"
                    + " sum(if(tool_id = '29',count,0)) as 'Lxrm_Social_Media_Tool', sum(if(tool_id = '30',count,0)) as 'Top_Ranked_Websites_byKeyword',"
                    + " sum(if(tool_id = '31',count,0)) as 'Broken_Link_Checker',sum(if(tool_id = '34',count,0)) as 'Most_Used_Keywords',sum(if(tool_id = '35',count,0)) as 'PageSpeed_Insights', "
                    + " sum(if(tool_id = '36',count,0)) as 'SEO_Penalty_Checker', sum(if(tool_id = '37',count,0)) as 'Structured_Data_Generator', "
                    + " sum(if(tool_id = '40',count,0)) as 'Micro_Influencer_Generator' from"
                    + " tool_usage group by session_id) test , session s where s.id = test.session_id  and s.end_time<>'0000-00-00 00:00:00'"
                    + " group by session_id) tot_session, user u where u.id= tot_session.user_id and tot_session.Session_Start_Time"
                    + " between ? and ? " + subquery + " and u.is_admin!=1 order by u.id,tot_session.Session_Start_Time";

            logger.debug("ToolUsageReport excluding video usage :" + query + "_" + fromDate + toDate);
        } else {
            query = "select u.id as 'User Id',u.name as 'Name',u.email as 'Email Id',date_format(u.reg_date,'%d-%b-%y') as 'First Visited Date',"
                    + " tot_session.Login_Date as 'Tool Usage Date',time(tot_session.Session_Start_Time) as 'Session Start Time', "
                    + " time(tot_session.Session_End_Time) as 'Session End Time',  tot_session.duration as 'Time Spent',"
                    + " tot_session.ROI_TUsage as 'ROI Calculator Usage / Session', tot_session.KCB_TUsage as 'Keyword Combinator Usage / Session', "
                    + " tot_session.KCB_VUsage as 'Keyword Combinator Video Usage  / Session', tot_session.URL_TUsage as 'URL Auditor Usage / Session', "
                    + " tot_session.URL_VUsage as 'URL Auditor Video Usage / Session', tot_session.KPA_TUsage   as 'Keyword Performance Analyzer / Session' , "
                    + " tot_session.KPA_VUsage as 'Keyword Performance Analyzer Video Usage / Session' , "
                    + " tot_session.SMB_TUsage as 'Sitemap Builder Usage / Session' , "
                    + " tot_session.SMB_VUsage as 'Sitemap Builder Video Usage / Session',tot_session.CPA_TUsage as 'Competitor Analysis Usage / Session' , "
                    + " tot_session.CPA_VUsage as 'Competitor Analysis Video Usage / Session' , tot_session.SPV_TUsage as 'SEO Webpage Analysis Usage / Session' , "
                    + " tot_session.SPV_VUsage as 'SEO Webpage Analysis Video Usage / Session' "
                    + " from (select id,user_id,duration,date_format(s.start_time,'%d-%b-%Y') as 'Login_Date',s.start_time as 'Session_Start_Time', "
                    + " s.end_time as 'Session_End_Time',test.* from  (select * from "
                    + " (select session_id, 0 as URL_TUsage,  sum(if(video_id = '1',count,0)) as URL_VUsage, "
                    + " 0 as KCB_TUsage, sum(if(video_id = '2',count,0)) as KCB_VUsage , "
                    + " 0 as ROI_TUsage, sum(if(video_id= '3',count,0))  as ROI_VUsage, "
                    + " 0 as KPA_TUsage, sum(if(video_id = '4',count,0)) as KPA_VUsage, "
                    + " 0 as APT_TUsage, sum(if(video_id= '5',count,0))  as APT_VUsage, "
                    + " 0 as SMB_TUsage, sum(if(video_id = '6',count,0)) as SMB_VUsage, "
                    + " 0 as CPA_TUsage, sum(if(video_id = '7',count,0)) as CPA_VUsage, "
                    + " 0 as SPV_TUsage, sum(if(video_id = '8',count,0)) as SPV_VUsage "
                    + " from video_usage where session_id not in (select session_id from tool_usage) group by session_id "
                    + " Union "
                    + " select session_id, sum(if(tool_id = '1',count,0)) as  URL_TUsage,  "
                    + " 0 as URL_VUsage, sum(if(tool_id = '2',count,0)) as KCB_TUsage, "
                    + " 0 as KCB_VUsage ,sum(if(tool_id = '3',count,0)) as ROI_TUsage, "
                    + " 0 as ROI_VUsage, sum(if(tool_id = '4',count,0)) as KPA_TUsage, "
                    + " 0 as KPA_VUsage, sum(if(tool_id = '5',count,0)) as APT_TUsage, "
                    + " 0 as APT_VUsage, sum(if(tool_id = '6',count,0)) as SMB_TUsage, "
                    + " 0 as SMB_VUsage, sum(if(tool_id = '7',count,0)) as CPA_TUsage, 0 as CPA_VUsage, "
                    + " sum(if(tool_id = '8',count,0)) as SPV_TUsage, 0 as SPV_VUsage "
                    + " from tool_usage where session_id not in (select session_id from video_usage) group by session_id "
                    + " union select  c.session_id, c.URL_TUsage, d.URL_VUsage, c.KCB_TUsage,"
                    + " d.KCB_VUsage, c.ROI_TUsage,d.ROI_VUsage, c.KPA_TUsage, d.KPA_VUsage, "
                    + " c.APT_TUsage, d.APT_VUsage, c.SMB_TUsage, d.SMB_VUsage, c.CPA_TUsage, d.CPA_VUsage , c.SPV_TUsage, d.SPV_VUsage from (select  a.session_id , "
                    + " sum(if(a.tool_id = '1',a.count,0)) as URL_TUsage, sum(if(a.tool_id = '2',a.count,0)) as KCB_TUsage, "
                    + " sum(if(a.tool_id = '3',a.count,0)) as ROI_TUsage, sum(if(a.tool_id = '4',a.count,0)) as KPA_TUsage, "
                    + " sum(if(a.tool_id = '5',a.count,0)) as APT_TUsage, sum(if(a.tool_id = '6',a.count,0)) as SMB_TUsage,"
                    + " sum(if(a.tool_id = '7',a.count,0)) as CPA_TUsage ,sum(if(a.tool_id = '8',a.count,0)) as SPV_TUsage "
                    + " from tool_usage a  where a.session_id in (select  distinct(a.session_id) from tool_usage a , "
                    + " video_usage b where a.session_id=b.session_id)  group by a.session_id) c "
                    + " join (select  b.session_id , sum(if(b.video_id = '1',b.count,0)) as URL_VUsage, "
                    + " sum(if(b.video_id = '2',b.count,0)) as KCB_VUsage, sum(if(b.video_id = '3',b.count,0)) as ROI_VUsage, "
                    + " sum(if(b.video_id = '4',b.count,0)) as KPA_VUsage, sum(if(b.video_id = '5',b.count,0)) as APT_VUsage, "
                    + " sum(if(b.video_id = '6',b.count,0)) as SMB_VUsage,sum(if(b.video_id = '7',b.count,0)) as CPA_VUsage, "
                    + " sum(if(b.video_id = '8',b.count,0)) as SPV_VUsage from video_usage b where b.session_id in "
                    + " (select  distinct(a.session_id) from tool_usage a , video_usage b where a.session_id=b.session_id) "
                    + " group by b.session_id)d on c.session_id=d.session_id  group by session_id) e order by session_id ) test , "
                    + " session s where s.id = test.session_id  and s.end_time<>'0000-00-00 00:00:00'  group by session_id) tot_session, "
                    + " user u where u.id= tot_session.user_id and tot_session.Session_Start_Time  between ? and ? "
                    + "  " + subquery + " and u.is_admin!=1 order by u.id, tot_session.Session_Start_Time";
        }
        try {
            logger.debug("ToolUsageReport:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query,
                    new Object[]{fromDate, toDate});
            logger.debug("sqlrowset is  :" + sqlRowSet);
            if (dwnldTyp == 1) {
                flag = createXLSXFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = createXLSFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
        } catch (Exception e) {
            logger.error("Exception in toolUsageDwnld cause:", e);
        }
        return flag;
    }
//
    public String userBehaviourDwnld(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 2;
        String subquery = "";
        if (!includeLocal) {
            subquery = " and (select is_local from user where id=s.user_id)=0 ";
        }
        String repName = "User Behaviour Report";
        String query = "select w.*,date_format(h.lastVstdDate,'%d-%b-%Y') as 'Last Visited Date' from"
                + "(select  u.id as 'User_Id',u.name as 'Name',u.email as 'Email Id',date_format(u.reg_date,'%d-%b-%Y') as 'First Visited Date',"
                + "a.No_ofSession as 'Number of Sessions',a.Total_Session_Time as 'Time Spent',(a.Total_Session_Time/a.No_ofSession) as 'Average Session Time',"
                + "a.ROI_Usage as 'ROI Calculator Usage', a.KeywordCombination_Usage as 'Keyword Combinator Usage',a.URL_Auditor_Usage as 'URL Auditor Usage',"
                + "a.KeyPerformance as 'Keyword Performance Analyzer Usage', a.Sitemap_Builder_Usage as 'Sitemap Builder Usage',a.Competitor_Analysis_Usage as 'Competitor Analysis Usage' ,"
                + "a.Spider_View_Usage as 'SEO Webpage Analysis Usage',a.Comp_web_Monitor_Usage as 'Competitor Webpage Monitor Usage',a.Key_rank_Usage as 'Weekly Keyword Rank Checker Usage',"
                + "a.sitegrader as 'Site Grader Usage',a.Dashboard as 'LXR SEO Dashboard',a.metatag_genarator as 'Meta Tag Generator Usage',"
                + "a.Inbound_link_checker as 'Inbound Link Checker Usage',a.competitor_research_tool as 'Competitor Research Usage',a.website_auditor_tool as 'Website Auditor Usage',"
                + "a.lxrm_inbound_link_checker as 'SEO Inbound Link Checker Usage',a.Domain_Age_Checker as 'Domain Age Checker Usage',"
                + "a.Robots_Txt_Generator as 'Robots.txt Generator Usage',a.Robots_Txt_Validator as 'Robots.txt Validator Usage',a.Lxrm_Meta_Tag_Generator as 'Lxrm Meta Tag Generator Usage', "
                + "a.DNS_Lookup_tool as 'DNS Lookup Tool Usage', a.Social_Media_Tool as 'Social Media Tool Usage' ,a.Top_Ranked_Websites_byKeyword as 'Top Ranked Websites by Keyword Tool Usage',"
                + "a.Broken_Link_Checker as 'Broken Link Checker Tool Usage',a.Most_Used_Keywords as 'Most Used Keywords Tool',a.PageSpeed_Insights as 'PageSpeed Insights Tool', "
                + "a.SEO_Penalty_Checker as 'SEO Penalty Checker Tool',a.Structured_Data_Generator as 'Structured Data Generator Tool' "
                + "from (select user_id,count(user_id) as 'No_ofSession', sum(duration) as 'Total_Session_Time', "
                + "sum(URL_Auditor_Usage) as 'URL_Auditor_Usage',sum(KeywordCombination_Usage) as 'KeywordCombination_Usage',sum(ROI_Usage) as 'ROI_Usage',"
                + "sum(KeyPerformance) as KeyPerformance,sum(Amazon_Price_Tracker) as Amazon_Price_Tracker_Usage,sum(Sitemap_Builder) as  Sitemap_Builder_Usage,"
                + "sum(Competitor_Analysis) as  Competitor_Analysis_Usage,sum(Spider_View) as  Spider_View_Usage,sum(Comp_web_Monitor) as  Comp_web_Monitor_Usage,"
                + "sum(keyword_rank_checker) as Key_rank_Usage,sum(site_grader) as sitegrader,sum(Dashboard) as Dashboard,sum(metatag_genarator) as metatag_genarator,"
                + "sum(Inbound_link_checker) as Inbound_link_checker,sum(competitor_research_tool) as competitor_research_tool,sum(website_auditor_tool) as website_auditor_tool, "
                + "sum(lxrm_inbound_link_checker) as lxrm_inbound_link_checker,sum(Domain_Age_Checker) as Domain_Age_Checker,sum(Robots_Txt_Generator) as Robots_Txt_Generator,"
                + "sum(Robots_Txt_Validator) as Robots_Txt_Validator,sum(Lxrm_meta_tag_generator) as Lxrm_Meta_Tag_Generator,sum(dns_lookup_tool) as DNS_Lookup_tool, "
                + "sum(social_media_tool) as Social_Media_Tool,sum(Top_Ranked_Websites_byKeyword) as Top_Ranked_Websites_byKeyword,sum(Broken_Link_Checker) as Broken_Link_Checker,"
                + "sum(Most_Used_Keywords) as Most_Used_Keywords,sum(PageSpeed_Insights) as PageSpeed_Insights,sum(SEO_Penalty_Checker) as SEO_Penalty_Checker,"
                + "sum(Structured_Data_Generator) as Structured_Data_Generator, "
                + "sum(Micro_Influencer_Generator) as Micro_Influencer_Generator from (select user_id,duration,test.* from (select session_id,"
                + "sum(if(tool_id = '1',count,0))  as 'URL_Auditor_Usage',sum(if(tool_id = '2',count,0))  as 'KeywordCombination_Usage',"
                + "sum(if(tool_id = '3',count,0))  as 'ROI_Usage' ,sum(if(tool_id = '4',count,0)) as  KeyPerformance,sum(if(tool_id = '5',count,0)) as  Amazon_Price_Tracker,"
                + "sum(if(tool_id = '6',count,0)) as  Sitemap_Builder,sum(if(tool_id = '7',count,0)) as Competitor_Analysis,sum(if(tool_id = '8',count,0)) as Spider_View ,"
                + "sum(if(tool_id = '9',count,0)) as Comp_web_Monitor,sum(if(tool_id = '10',count,0)) as keyword_rank_checker, sum(if(tool_id = '11',count,0)) as site_grader,"
                + "sum(if(tool_id = '13',count,0)) as metatag_genarator,sum(if(tool_id = '14',count,0)) as Inbound_link_checker,sum(if(tool_id = '15',count,0)) as Dashboard,"
                + "sum(if(tool_id = '16',count,0)) as competitor_research_tool,sum(if(tool_id = '17',count,0)) as website_auditor_tool,sum(if(tool_id = '20',count,0)) as lxrm_inbound_link_checker,"
                + "sum(if(tool_id = '21',count,0)) as Domain_Age_Checker,sum(if(tool_id = '24',count,0)) as Robots_Txt_Generator,sum(if(tool_id = '25',count,0)) as Robots_Txt_Validator, "
                + "sum(if(tool_id = '27',count,0)) as Lxrm_meta_tag_generator,sum(if(tool_id = '28',count,0)) as dns_lookup_tool,sum(if(tool_id = '29',count,0)) as social_media_tool, "
                + "sum(if(tool_id = '30',count,0)) as Top_Ranked_Websites_byKeyword,sum(if(tool_id = '31',count,0)) as Broken_Link_Checker, "
                + "sum(if(tool_id = '34',count,0)) as Most_Used_Keywords,sum(if(tool_id = '35',count,0)) as PageSpeed_Insights, "
                + "sum(if(tool_id = '36',count,0)) as SEO_Penalty_Checker,sum(if(tool_id = '37',count,0)) as Structured_Data_Generator, "
                + "sum(if(tool_id = '40',count,0)) as Micro_Influencer_Generator "
                + "from tool_usage group by session_id order by id) test, session s where s.id =  test.session_id and s.start_time  > ?  and end_time < ?  "
                + subquery + "  and s.end_time <> '0000-00-00 00:00:00' ) c group by user_id) a ,  user u where a.user_id = u.id and u.is_admin = 0) w "
                + "left join(select user_id,max(end_time) as lastVstdDate from session "
                + "where start_time  > ? and end_time < ? group by user_id) h  on w.User_Id = h.user_id";

        try {
            logger.info("UserBehaviourReport:" + query + ", fromDate: " + fromDate + ", toDate: " + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate, fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = createXLSXFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = createXLSFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
        } catch (Exception e) {
            logger.error("Exception hor preparing userBehaviourDwnld: ", e);
        }
        return flag;
    }

    public String activeUserDwnld(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        String query;
        int repTyp = 3;
        String subquery = "";
        if (!includeLocal) {
            subquery = " and is_local=0 ";
        }
        String repName = "Users Report";

        if (!tdate) {
//			query = "select id as 'User Id',name as Name,email as 'Email Id',"
//					+ " date_format(reg_date,'%d-%b-%Y') as 'Registration Date',is_Active as 'Activation Status', "
//					+ "date_format(act_date,'%d-%b-%Y') as 'Activation Date' from user"
//					+ " where reg_date between ? and ?  and is_admin!=1 " + subquery + " order by id";
//			query = "select uid as 'User Id',name as 'Name',email as 'Email Id', reg_date as 'First Visited Date',"
//                +"  act_date as 'Registration Date',sum(ucount) as 'No. of Tools Used' "
//                + " from (select U.*, TU.* from (select id as 'uid',name,email, date_format(reg_date,'%d-%b-%Y') as 'reg_date', "
//                + " is_active, date_format(act_date,'%d-%b-%Y') as 'act_date' from user "
//                + "where reg_date between ? and ?  and is_admin!= 1   " + subquery + ") U "
//                + "left join session S on U.uid=S.user_id left join (select session_id, sum(count) as 'ucount' from tool_usage where tool_id != 5 group by session_id) TU " 
//                + "on TU.session_id=S.id) X group by uid, name, email, reg_date, is_active,act_date order by uid" ;
            query = "select uid as 'User Id',name as 'Name',email as 'Email Id', reg_date as 'First Visited Date',act_date as 'Registration Date',  date_format(lastVstdDate,'%d-%b-%Y') as 'Last Visited Date',"
                    + " sum(ucount) as 'No. of Tools Used'  from (select U.*, TU.*,f.* from (select id as 'uid',name,email, date_format(reg_date,'%d-%b-%Y') as 'reg_date',"
                    + " is_active, date_format(act_date,'%d-%b-%Y') as 'act_date' from user where reg_date between ? and ?  and is_admin!= 1 "
                    + "  " + subquery + " ) U left join session  S "
                    + " on U.uid=S.user_id left join (select session_id, sum(count) as 'ucount' from tool_usage where tool_id != 5 group by session_id) TU on TU.session_id=S.id left join"
                    + " (select id,user_id,max(end_time) as lastVstdDate  from session s where end_time between ? and ? group by  user_id ) f on TU.session_id = f.id ) X "
                    + " group by uid, name, email, reg_date, is_active,act_date order by uid";
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate, fromDate, toDate});
        } else {
//			query = "select id as 'User Id',name as Name,email as 'Email Id',"
//					+ " date_format(reg_date,'%d-%b-%Y') as 'Registration Date', is_Active as 'Activation Status', "
//					+ "date_format(act_date,'%d-%b-%Y') as 'Activation Date' from user"
//					+ " where reg_date <= ?  and is_admin!= 1 " + subquery + " order by id";
//			query = "select uid as 'User Id',name as 'Name',email as 'Email Id', reg_date as 'First Visited Date',"
//                    +" act_date as 'Registration Date',sum(ucount) as 'No. of Tools Used' "
//                    + " from (select U.*, TU.* from (select id as 'uid',name,email, date_format(reg_date,'%d-%b-%Y') as 'reg_date', "
//                    + " is_active, date_format(act_date,'%d-%b-%Y') as 'act_date' from user "
//                    + "where reg_date <= ?  and is_admin!= 1  " + subquery + ") U "
//                    + "left join session S on U.uid=S.user_id left join (select session_id, sum(count) as 'ucount' from tool_usage where tool_id != 5 group by session_id) TU " 
//                    + "on TU.session_id=S.id) X group by uid, name, email, reg_date, is_active,act_date order by uid" ;
            query = "select uid as 'User Id',name as 'Name',email as 'Email Id', reg_date as 'First Visited Date', act_date as 'Registration Date', date_format(lastVstdDate,'%d-%b-%Y') as 'Last Visited Date',"
                    + " sum(ucount) as 'No. of Tools Used'  from (select U.*, TU.*,f.* from (select id as 'uid',name,email, date_format(reg_date,'%d-%b-%Y') as 'reg_date',"
                    + " is_active, date_format(act_date,'%d-%b-%Y') as 'act_date' from user where reg_date <= ?  and is_admin!= 1 "
                    + "  " + subquery + " ) U left join session  S "
                    + " on U.uid=S.user_id left join (select session_id, sum(count) as 'ucount' from tool_usage where tool_id != 5 group by session_id) TU on TU.session_id=S.id left join"
                    + " (select id,user_id,max(end_time) as lastVstdDate  from session s where end_time < ? group by  user_id ) f on TU.session_id = f.id ) X "
                    + " group by uid, name, email, reg_date, is_active,act_date order by uid";

            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{toDate, toDate});
        }

        try {

            logger.info("ActiveUsersReport:" + query + ", fromDate: " + fromDate + ", toDate: " + toDate);
            if (dwnldTyp == 1) {
                flag = createXLSXFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = createXLSFile(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }

    /* called by controller to create a file in excel(.xlsx format) */
    public String createXLSXFile(SqlRowSet sqlRowSet, String filePath,
            String dateRange, String repName, int repTyp) {
        String fileName = null;
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            XSSFWorkbook workBook = new XSSFWorkbook();
            XSSFSheet sheet = workBook.createSheet(repName);
            XSSFCellStyle heaCelSty;
            XSSFFont font = null;
            XSSFRow rows;
            XSSFCell cell = null;
            XSSFCellStyle columnHeaderStyle = null;
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[];
            /*Common font style for header and coloum*/
            font = workBook.createFont();
            font.setBold(true);
            font.setFontHeightInPoints((short) 10);
            font.setFontName(HSSFFont.FONT_ARIAL);
            
            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(font);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            sheet.createFreezePane(0, 4);

            //style for date
            CellStyle dateStyle = workBook.createCellStyle();
            colWidth = new int[]{8, 5, 6, 15, 20, 20, 15, 9, 8, 5, 5, 5, 9};
            if (repTyp == 1) {
                colWidth = new int[]{10, 24, 24, 11, 11, 10, 13, 18, 20, 22, 22, 18, 18, 18, 22, 18, 18, 18, 18, 18, 18, 18, 18, 18, 20, 18, 18, 20, 22, 20, 22, 32, 25, 35, 35, 35, 35, 35,35, 35, 35, 35, 35};
            } else if (repTyp == 2) {
                colWidth = new int[]{10, 24, 22, 22, 22, 17, 14, 17, 24, 24, 24, 20, 24, 24, 34, 34, 34, 24, 24, 20, 24, 18, 18, 18, 18, 18, 18, 18, 18, 18, 25, 20, 22, 32, 25,25,25,25,25,25,25,25,25,25,25};
            } else if (repTyp == 3) {
                colWidth = new int[]{10, 22, 22, 16, 15, 15, 15, 15, 8, 5, 5, 5};
            } else if (repTyp == 5) {
                colWidth = new int[]{22, 20, 20, 22, 15, 15, 15, 15, 8, 5, 5, 5};
            } else if (repTyp == 6) {
                colWidth = new int[]{18, 50, 15, 20};
            } else if (repTyp == 7) {
                colWidth = new int[]{15, 25, 25, 12, 10, 12, 8, 8, 8, 10, 10, 10, 10, 12, 10, 10, 12, 10, 12, 10, 12, 20, 12, 15, 15, 15, 15};
            } else if (repTyp == 8) {
                colWidth = new int[]{15, 25, 25, 15, 15, 10, 10, 10};
            } else if (repTyp == 9) {
                colWidth = new int[]{15, 35, 30, 30, 30};
            } else if (repTyp == 11) {
                colWidth = new int[]{20, 24, 24, 22, 16, 24, 16, 22, 20, 20, 17, 12};
            }
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy h:mm:ss"));
            logger.info("Generating Report in .xlsx format");

            //Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Type");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(dateRange);
            cell.setCellStyle(heaCelSty);
            rowCnt = 2;

            //Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            rows = sheet.createRow(rowCnt + 1);
            rows.setHeight((short) 500);
            for (int i = 1; i <= colCount; i++) {
                cell = rows.createCell(i - 1);
                cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i));
                cell.setCellStyle(columnHeaderStyle);
            }

            rowCnt = 4;//Report Data
            while (sqlRowSet.next()) {
                rows = sheet.createRow(rowCnt);
                for (int colInd = 1; colInd <= colCount; colInd++) {
                    cell = rows.createCell(colInd - 1);
                    switch (sqlRowSetMetDat.getColumnType(colInd)) {
                        case Types.DATE:
                            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat
                                    .getColumnName(colInd)));
                            break;
                        case Types.BIGINT:
                            cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat
                                    .getColumnName(colInd).toString()));
                            numericStyle.setDataFormat(format.getFormat("0"));
                            cell.setCellStyle(numericStyle);
                            break;
                        case Types.TIMESTAMP:
                            if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                                cell.setCellValue("Not Available.");
                            } else {
                                cell.setCellValue(sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)));
                                cell.setCellStyle(dateStyle);
                            }   break;
                        case Types.DECIMAL:
                            if (sqlRowSet.getLong(sqlRowSetMetDat
                                    .getColumnName(colInd)) != 0) {
                                cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat
                                        .getColumnName(colInd)));
                            } else {
                                cell.setCellValue("");
                            }   numericStyle.setDataFormat(format.getFormat("0"));
                            cell.setCellStyle(numericStyle);
                            break;
                        default:
                            String colLabel = sqlRowSetMetDat.getColumnLabel(colInd);
                            //						logger.info("column name is : "+colLabel );
                            boolean nullval = false;
                            if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                                nullval = true;
                            }   if (repTyp == 3) {
                                if (colLabel.equalsIgnoreCase("Activation Date") && nullval) {
                                    cell.setCellValue("Not Yet Activated");
                                } else if ((colLabel.equalsIgnoreCase("Name") || colLabel.equalsIgnoreCase("Email Id")) && nullval) {
                                    cell.setCellValue("Guest User");
                                } else if (nullval) {
                                    cell.setCellValue("Not Available.");
                                } else if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)).equalsIgnoreCase("true")) {
                                    cell.setCellValue("Yes");
                                } else if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)).equalsIgnoreCase("false")) {
                                    cell.setCellValue("No");
                                } else {
                                    cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)));
                                }
                            } else if (colLabel.equalsIgnoreCase("Email Id") && nullval) {
                                cell.setCellValue("Guest User");
                            } else if (nullval) {
                                cell.setCellValue("Not Available.");
                            } else if (colLabel.equalsIgnoreCase("Time Spent") || colLabel.equalsIgnoreCase("Average Session Time")) {
                                double val = sqlRowSet.getDouble((sqlRowSetMetDat.getColumnName(colInd)));
                                int hours = (int) (val / 3600),
                                        remainder = (int) (val % 3600),
                                        minutes = remainder / 60,
                                        seconds = remainder % 60;
                                String hh = (hours < 10 ? "0" : "") + hours,
                                        mm = (minutes < 10 ? "0" : "") + minutes,
                                        ss = (seconds < 10 ? "0" : "") + seconds;
                                cell.setCellValue(hh + ":" + mm + ":" + ss);
                            } else {
                                cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)));
                            }   break;
                    }
                }
                rowCnt++;
            }
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i,(int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            logger.info(" Report generated in .xlsx format");
            String reptName = Common.removeSpaces(repName);
            fileName = filePath + Common.createFileName(reptName) + ".xlsx";
            File file = new File(fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (Exception e) {
            logger.error("Exception for genertaing createXLSXFile: ",e);
        }
        return fileName;
    }

    /* called by controller to create a file in excel(.xls format) */
    public String createXLSFile(SqlRowSet sqlRowSet, String filePath,
            String dateRange, String repName, int repTyp) {
        String fileName = null;
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet(repName);
            HSSFFont font = null;
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = null;
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[];

            font = workBook.createFont();
            font.setBold(true);
            font.setFontHeightInPoints((short) 10);
            font.setFontName(HSSFFont.FONT_ARIAL);
            
            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(font);
            
            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            sheet.createFreezePane(0, 4);

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            colWidth = new int[]{8, 5, 6, 15, 20, 20, 15, 9, 8, 5, 5, 5};
            switch (repTyp) {
                case 1:
                    colWidth = new int[]{10, 24, 24, 11, 11, 11, 12, 15, 20, 22, 24, 18, 18, 24, 24, 20, 24, 18, 18, 18, 18, 18, 18, 18, 20, 18, 18, 20, 22, 20, 22, 32, 25, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35};
                    break;
                case 2:
                    colWidth = new int[]{10, 24, 22, 22, 22, 17, 14, 17, 12, 17, 17, 17, 17, 24, 17, 17, 24, 24, 20, 24, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 20, 22, 32, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25};
                    break;
                case 3:
                    colWidth = new int[]{10, 18, 22, 16, 15, 15, 15, 15, 8, 5, 5, 5, 5};
                    break;
                case 5:
                    colWidth = new int[]{22, 20, 20, 22, 15, 15, 15, 15, 8, 5, 5, 5};
                    break;
                case 6:
                    colWidth = new int[]{18, 50, 15, 20};
                    break;
                case 7:
                    colWidth = new int[]{15, 25, 25, 12, 10, 12, 8, 8, 8, 10, 10, 10, 10, 12, 10, 10, 12, 10, 12, 10, 12, 20, 12, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15};
                    break;
                case 8:
                    colWidth = new int[]{15, 25, 25, 15, 15, 10, 10, 10};
                    break;
                case 9:
                    colWidth = new int[]{15, 35, 30, 30, 30};
                    break;
                case 11:
                    colWidth = new int[]{20, 24, 24, 22, 16, 24, 16, 22, 20, 20, 17, 12};
                    break;
                default:
                    break;
            }
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy h:mm:ss"));
            //Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Type");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            logger.info("Generating Report in .xls format and date range: " + dateRange);
            cell.setCellValue(dateRange);
            cell.setCellStyle(heaCelSty);
            rowCnt = 2;

            //Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            rows = sheet.createRow(rowCnt + 1);
            rows.setHeight((short) 500);
            for (int i = 1; i <= colCount; i++) {
                cell = rows.createCell(i - 1);
                cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i));
                cell.setCellStyle(columnHeaderStyle);
            }
            rowCnt = 4;

            //Report Data
            while (sqlRowSet.next()) {
                rows = sheet.createRow(rowCnt);
                for (int colInd = 1; colInd <= colCount; colInd++) {
                    cell = rows.createCell(colInd - 1);
                    switch (sqlRowSetMetDat.getColumnType(colInd)) {
                        case Types.DATE:
                            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat
                                    .getColumnName(colInd)));
                            break;
                        case Types.BIGINT:
                            cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat
                                    .getColumnName(colInd)));
                            numericStyle.setDataFormat(format.getFormat("0"));
                            cell.setCellStyle(numericStyle);
                            break;
                        case Types.TIMESTAMP:
                            if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                                cell.setCellValue("Not Available.");
                            } else {
                                cell.setCellValue(sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)));
                                cell.setCellStyle(dateStyle);
                            }   break;
                        case Types.DECIMAL:
                            if (sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)) != 0) {
                                cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                            } else {
                                cell.setCellValue("");
                            }   numericStyle.setDataFormat(format.getFormat("0"));
                            cell.setCellStyle(numericStyle);
                            break;
                        default:
                            String colLabel = sqlRowSetMetDat.getColumnLabel(colInd);
                            boolean nullval = false;
                            if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                                nullval = true;
                            }   if (repTyp == 3) {
                                if (colLabel.equalsIgnoreCase("Activation Date") && nullval) {
                                    cell.setCellValue("Not Yet Activated");
                                } else if ((colLabel.equalsIgnoreCase("Name") || colLabel.equalsIgnoreCase("Email Id")) && nullval) {
                                    cell.setCellValue("Guest User");
                                } else if (nullval) {
                                    cell.setCellValue("Not Available.");
                                } else if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)).equalsIgnoreCase("true")) {
                                    cell.setCellValue("Yes");
                                } else if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)).equalsIgnoreCase("false")) {
                                    cell.setCellValue("No");
                                } else {
                                    cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)));
                                }
                            } else if (colLabel.equalsIgnoreCase("Email Id") && nullval) {
                                cell.setCellValue("Guest User");
                            } else if (nullval) {
                                cell.setCellValue("Not Available.");
                            } else //							if(sqlRowSetMetDat.getColumnName(colInd).equalsIgnoreCase("Time Spent")){
                            {
                                if (colLabel.equalsIgnoreCase("Time Spent") || colLabel.equalsIgnoreCase("Average Session Time")) {
                                    double val = sqlRowSet.getDouble((sqlRowSetMetDat.getColumnName(colInd)));
                                    int hours = (int) (val / 3600),
                                            remainder = (int) (val % 3600),
                                            minutes = remainder / 60,
                                            seconds = remainder % 60;
                                    String hh = (hours < 10 ? "0" : "") + hours,
                                            mm = (minutes < 10 ? "0" : "") + minutes,
                                            ss = (seconds < 10 ? "0" : "") + seconds;
                                    cell.setCellValue(hh + ":" + mm + ":" + ss);
                                } else {
                                    cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)));
                                }
                            }   break;
                    }
                }
                rowCnt++;
            }
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            logger.info(" Report generated in .XLSFile format");
            String reptName = Common.removeSpaces(repName);
            fileName = filePath + Common.createFileName(reptName) + ".xls";
            File file = new File(fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
        } catch (Exception e) {
            logger.error("Exception for createXLSFile:",e);
        }
        return fileName;
    }

    //methods for tracking report data
    public String userSummaryDetails(String appPath, int dwnldTyp, long event_detail_id, boolean includeLocal) {
        //long event_detail_id=1;
        String subquery = "";
        if (!includeLocal) {
            subquery = "and user_id  not  in( select  id from user where is_local=1)";
        }
        String flag = "";
        String repName = "Email Performance Report";
        String query = "select   a.Template_Name,a.Sent_Date,b. Mail_Sent,c.Mail_Opened,"
                + "(if(b.Mail_Sent is null, 0, b. Mail_Sent)- if(Mail_Opened is null, 0, Mail_Opened) ) as  Mail_Not_Opened,"
                + "c.Unique_Visitors,( if(Mail_Opened is null, 0, Mail_Opened) - if(Unique_Visitors  is null,0,Unique_Visitors) ) as  Not_Visited,"
                + "c.Total_Visits  from  (select sent_date as Sent_Date,"
                + "(select  temp_name  from mail_templates where id = mail_template_id ) as Template_Name from event_details where event_detail_id=" + event_detail_id + ")a,"
                + "(select count(user_id) as Mail_Sent from user_event where event_detail_id=" + event_detail_id + ")b,"
                + "(select  sum(if(opened_date is not null, 1, 0))  as Mail_Opened,  sum(if(no_of_visits > 0, 1, 0)) as Unique_Visitors,"
                + "sum(no_of_visits) as Total_Visits from event_track_details where event_detail_id=" + event_detail_id + " " + subquery + " )c";
        logger.info("query" + query);

        SqlRowSet sqlRowSet = getJdbcTemplate().queryForRowSet(query);

        String query1 = "select  a.User_Id, name as Name, email as Email_Id  , a.Opened_Date,  a.Visited_Date, "
                + "a.No_Of_Visits, DATEDIFF(a.Visited_Date,"
                + "(select  sent_date from event_details where event_detail_id =" + event_detail_id + ")) as 'Response Time(in Days)' from (select  opened_date as Opened_Date,"
                + " user_id as User_Id, no_of_visits as No_Of_Visits, Visited_Date  from event_track_details where event_detail_id=" + event_detail_id + " " + subquery + ")a,"
                + " user where id= a.User_Id";
        logger.info("query11111111" + query1);

        SqlRowSet sqlRowSet1 = getJdbcTemplate().queryForRowSet(query1);

        try {

            if (dwnldTyp == 1) {
                flag = createSummaryXLSXFile(sqlRowSet, sqlRowSet1, appPath, repName);
            } else if (dwnldTyp == 2) {
                flag = createSummaryXLSFile(sqlRowSet, sqlRowSet1, appPath, repName);
            }
            return flag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;
    }

    public String createSummaryXLSFile(SqlRowSet sqlRowSet, SqlRowSet sqlRowSet1, String filePath, String repName) {
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet();
            HSSFFont font = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFRow rows;
            HSSFCell cell = null;

            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[];
            CellStyle dateStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            //style for date
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));
            dateStyle.setAlignment(HorizontalAlignment.CENTER);
            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty.setFont(font);
            //sheet.createFreezePane(0, 4);
            colWidth = new int[]{17, 17, 27, 18, 15, 15, 22, 15, 15, 5, 5, 5};
            //style for date
            logger.info("Generating Report in .xls format");

            //Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 2));
            cell.setCellStyle(heaCelSty);
            rowCnt = 1;
            heaCelSty.setAlignment(HorizontalAlignment.CENTER);

            // Column Heading Cell Style
            CellStyle columnHeaderStyle = workBook.createCellStyle();

            font = workBook.createFont();
            font.setBold(true);
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());

            rows = sheet.createRow(rowCnt++);
            cell = rows.createCell(0);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(1).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(1);
            sqlRowSet.next();
            cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)));
            sqlRowSet.beforeFirst();
            rows = sheet.createRow(rowCnt++);
            cell = rows.createCell(0);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(2).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(1);
            sqlRowSet.next();
            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(2)));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 2));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 2));
            sqlRowSet.beforeFirst();
            cell.setCellStyle(dateStyle);
            rowCnt++;

            //Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            sqlRowSet.beforeFirst();
            rowCnt = addXLSData(sqlRowSet, sheet, rowCnt, workBook, true, columnHeaderStyle, dateStyle, createHelper);
            rowCnt = rowCnt + 4;
            rowCnt = addXLSData(sqlRowSet1, sheet, rowCnt, workBook, false, columnHeaderStyle, dateStyle, createHelper);
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            logger.info(" Report generated in .xls format");
            String reptName = Common.removeSpaces(repName);
            String fileName = filePath + Common.createFileName(reptName) + ".xls";
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public int addXLSData(SqlRowSet sqlRowSet, HSSFSheet sheet, int rowCnt, HSSFWorkbook workBook, boolean isSummary, CellStyle columnHeaderStyle, CellStyle dateStyle, CreationHelper createHelper) {
        HSSFRow rows;
        HSSFCell cell;
        HSSFFont font;
        DataFormat format = workBook.createDataFormat();

        CellStyle numericStyle = workBook.createCellStyle();
        numericStyle.setAlignment(HorizontalAlignment.CENTER);
        //sheet.createFreezePane(0, 4);

        SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
        int colCount = sqlRowSetMetDat.getColumnCount();

        int start1, start2;
        if (isSummary) {
            start1 = 3;
            start2 = 1;
        } else {
            start1 = 1;
            start2 = 0;
        }
        rows = sheet.createRow(++rowCnt);
        rows.setHeight((short) 500);
        for (int i = start1; i <= colCount; i++) {
            cell = rows.createCell(i - start1 + start2);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
        }
        rowCnt++;
        while (sqlRowSet.next()) {
            rows = sheet.createRow(rowCnt);
            boolean novisit = false;
            if (isSummary) {
                cell = rows.createCell(0);
                cell.setCellValue("Number Of Users");
                cell.setCellStyle(columnHeaderStyle);

            }
            for (int colInd = start1; colInd <= colCount; colInd++) {
                cell = rows.createCell(colInd - start1 + start2);
                if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                    cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat
                            .getColumnName(colInd)));
                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                    if (colInd == 6 && novisit) {
                        cell.setCellValue("Not yet visited");
                    } else {
                        cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat
                                .getColumnName(colInd)));
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    }
                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                    if (sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                        cell.setCellValue(" ");
                        if (!isSummary && colInd == 5) {
                            novisit = true;
                        }
                    } else {
                        cell.setCellValue(sqlRowSet
                                .getTimestamp(sqlRowSetMetDat
                                        .getColumnName(colInd)));
                    }
                    cell.setCellStyle(dateStyle);
                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                    cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                    numericStyle.setDataFormat(format.getFormat("0"));
                    cell.setCellStyle(numericStyle);
                } else {
                    String colVal = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd));
                    boolean nullval = false;
                    if (colVal == null) {
                        nullval = true;
                    }
                    if (!nullval) {
                        cell.setCellValue(colVal);
                    } else {
                        cell.setCellValue("");
                    }
                }
            }
            rowCnt++;
        }
        return rowCnt;
    }

    /* called by controller to create a file in excel(.xlsx format) */
    public String createSummaryXLSXFile(SqlRowSet sqlRowSet, SqlRowSet sqlRowSet1, String filePath, String repName) {
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            XSSFWorkbook workBook = new XSSFWorkbook();
            XSSFSheet sheet = workBook.createSheet();
            XSSFCellStyle heaCelSty;
            XSSFFont font = workBook.createFont();
            XSSFRow rows;
            XSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[];
            CellStyle dateStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            //style for date
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));
            dateStyle.setAlignment(HorizontalAlignment.CENTER);

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty.setFont(font);
            heaCelSty.setAlignment(HorizontalAlignment.CENTER);
            colWidth = new int[]{17, 17, 27, 18, 15, 15, 22, 15, 15, 5, 5, 5};
            //dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy h:mm:ss"));
            logger.info("Generating Report in .xlsx format");

            //Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 2));
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellStyle(heaCelSty);
            rowCnt = 1;

            // Column Heading Cell Style
            XSSFCellStyle columnHeaderStyle = workBook.createCellStyle();

            font = workBook.createFont();
            font.setBold(true);
//	   		font.setFontHeightInPoints((short) 10);
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());

            rows = sheet.createRow(rowCnt++);
            cell = rows.createCell(0);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(1).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(1);
            sqlRowSet.next();
            cell.setCellValue(sqlRowSet.getString(sqlRowSetMetDat.getColumnName(1)));
            sqlRowSet.beforeFirst();
            rows = sheet.createRow(rowCnt++);
            cell = rows.createCell(0);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(2).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(1);
            sqlRowSet.next();
            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(2)));
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 2));
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 2));
            sqlRowSet.beforeFirst();
            cell.setCellStyle(dateStyle);
            rowCnt++;

            //Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            rowCnt = addXLSXData(sqlRowSet, sheet, rowCnt, workBook, true, columnHeaderStyle, dateStyle, createHelper);
            rowCnt = rowCnt + 4;
            rowCnt = addXLSXData(sqlRowSet1, sheet, rowCnt, workBook, false, columnHeaderStyle, dateStyle, createHelper);

            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i,
                        (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            logger.info(" Report generated in .xlsx format");
            String reptName = Common.removeSpaces(repName);
            String fileName = filePath + Common.createFileName(reptName) + ".xlsx";
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (InvalidResultSetAccessException | IOException e) {
            logger.error(e);
        }
        return null;
    }

    public int addXLSXData(SqlRowSet sqlRowSet, XSSFSheet sheet, int rowCnt, XSSFWorkbook workBook, boolean isSummary, XSSFCellStyle columnHeaderStyle, CellStyle dateStyle, CreationHelper createHelper) {
        XSSFRow rows;
        XSSFCell cell;
        XSSFFont font;
        CellStyle numericStyle = workBook.createCellStyle();
        numericStyle.setAlignment(HorizontalAlignment.CENTER);
        DataFormat format = workBook.createDataFormat();

        //sheet.createFreezePane(0, 4);
        SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();
        int colCount = sqlRowSetMetDat.getColumnCount();

        int start1, start2;
        if (isSummary) {
            start1 = 3;
            start2 = 1;
        } else {
            start1 = 1;
            start2 = 0;
        }
        rows = sheet.createRow(++rowCnt);
        rows.setHeight((short) 500);
        for (int i = start1; i <= colCount; i++) {
            cell = rows.createCell(i - start1 + start2);
            cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i).replaceAll("_", " "));
            cell.setCellStyle(columnHeaderStyle);
        }
        rowCnt++;
        while (sqlRowSet.next()) {
            rows = sheet.createRow(rowCnt);
            boolean novisit = false;
            if (isSummary) {
                cell = rows.createCell(0);
                cell.setCellValue("Number Of Users");
                cell.setCellStyle(columnHeaderStyle);

            }
            for (int colInd = start1; colInd <= colCount; colInd++) {
                cell = rows.createCell(colInd - start1 + start2);

                if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                    Date date = sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(colInd));
                    cell.setCellValue(date);
                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                    if (colInd == 6 && novisit) {
                        cell.setCellValue("Not yet visited");
                    } else {
                        cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat
                                .getColumnName(colInd)));
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    }

                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                    if (sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                        cell.setCellValue(" ");
                        if (!isSummary && colInd == 5) {
                            novisit = true;
                        }
                    } else {
                        cell.setCellValue(sqlRowSet
                                .getTimestamp(sqlRowSetMetDat
                                        .getColumnName(colInd)));
                    }
                    cell.setCellStyle(dateStyle);
                } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                    cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                    numericStyle.setDataFormat(format.getFormat("0"));
                    cell.setCellStyle(numericStyle);
                } else {
                    String colVal = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd));
                    boolean nullval = false;
                    if (colVal == null) {
                        nullval = true;
                    }
                    if (!nullval) {
                        cell.setCellValue(colVal);
                    } else {
                        cell.setCellValue("");
                    }
                }
            }
            rowCnt++;
        }
        return rowCnt;
    }

    public String reportLXRPluginUsage(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 11;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND u.is_local = 0 AND u.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRPlugin User Report";

        String query = "select u.lxrplugin_user_id, u.name, u.email, date_format(u.created_date,'%Y-%m-%d'), date_format(d.download_date,'%Y-%m-%d') , "
                + "case when d.installation_date is NULL then 'Not installed' else 'Installed' end as installation_status, "
                + "date_format(d.installation_date,'%Y-%m-%d'), d.access_token , u.country, u.region_name, "
                + "case when date(v.end_date) < current_date() or date(v.end_date) is null then 'Inactive' else 'Active' end as status "
                + "from lxrplugin_users u left join lxrplugin_downloads d on u.lxrplugin_user_id = d.lxrplugin_user_id "
                + "AND d.download_id in(select max(ld.download_id) from lxrplugin_downloads ld group by ld.lxrplugin_user_id) "
                + "and d.is_deleted = 0 "
                + "left join lxrplugin_validity v on u.lxrplugin_user_id = v.lxrplugin_user_id AND "
                + "v.validity_id in (select max(sv.validity_id) from lxrplugin_validity sv group by sv.lxrplugin_user_id) "
                + "where date_format(u.created_date, '%Y-%m-%d') between date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') " + subquery
                + " group by u.lxrplugin_user_id";

        try {
            logger.info("LXRPlugin Users Report:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = generateXLSXReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error("Exception" + e);
        }
        return null;
    }

    public String reportLXRPluginPayment(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 12;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND u.is_local = 0 AND u.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2013-11-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRPlugin User Payment Report";

//        String query1 = "select u.lxrplugin_user_id, u.name, u.email, u.created_date, u.country, u.region_name, u.pricing_plan_id,"
//                + "t.created_time, t.amount, c.renew_license,  v.pricing_plan_id from lxrplugin_users u, lxrplugin_downloads d,"
//                + "lxrplugin_cards c,  lxrplugin_transactions t, lxrplugin_validity v "
//                + "where u.lxrplugin_user_id = t.lxrplugin_user_id and u.lxrplugin_user_id = c.lxrplugin_user_id "
//                + "and u.lxrplugin_user_id = v.lxrplugin_user_id group by u.lxrplugin_user_id ";
        String query = "select u.lxrplugin_user_id,u.name, u.email, date_format(u.created_date,'%Y-%m-%d') as signup_date, "
                + "date_format(ts.created_time,'%Y-%m-%d') as payment_date, "
                + "case when c.renew_license is true then DATE_ADD(date_format(v.end_date,'%Y-%m-%d'), INTERVAL 1 DAY) else null end as next_payment_date, "
                + "ts.amount, c.renew_license, p.package, p.payment_frequency,  u.country,u.region_name, "
                + "case when date(v.end_date) < current_date() or date(v.end_date) is null then 'Inactive' else 'Active' end as status "
                + "from lxrplugin_users u,lxrplugin_transactions ts,lxrplugin_cards c, lxrplugin_validity v, "
                + "lxrplugin_pricing_plans p where date_format(ts.created_time, '%Y-%m-%d') between "
                + "date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') "
                + "and ts.state='completed' and ts.intent = 'capture' and date(v.start_date)  = date(ts.created_time) "
                + "and u.lxrplugin_user_id = ts.lxrplugin_user_id and c.lxrplugin_user_id = u.lxrplugin_user_id "
                + "and v.lxrplugin_user_id = ts.lxrplugin_user_id and date_add(date(v.start_date),interval 6 day)!=date(v.end_date) "
                + "and c.is_deleted=0 and p.pricing_plan_id = v.pricing_plan_id " + subquery
                + " group by v.validity_id ";

        try {
            logger.info("LXRPlugin User Payment Report: " + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = generateXLSXReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        return null;

    }

    public String generateXLSReport(SqlRowSet sqlRowSet, String filePath,
            String dateRange, String repName, int repTyp) {
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet(repName);
            HSSFFont font = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[] = null;

            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            font.setFontHeightInPoints((short) 10);
            font.setFontName(HSSFFont.FONT_ARIAL);
            
            heaCelSty.setFont(font);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            sheet.createFreezePane(0, 4);

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            switch (repTyp) {
                case 12:
                    colWidth = new int[]{16, 24, 24, 24, 20, 20, 12, 15, 15, 15, 20, 20, 15, 20, 20};
                    break;
                case 11:
                    colWidth = new int[]{16, 24, 24, 24, 24, 16, 24, 22, 20, 20, 17, 12};
                    break;
                case 13:
                    colWidth = new int[]{16, 24, 24, 24, 24, 24, 20, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                case 14:
                    colWidth = new int[]{16, 24, 24, 24, 24, 24, 20, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                case 15:
                    colWidth = new int[]{10, 24, 30, 24, 24, 12, 24, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                case 17:
                    colWidth = new int[]{24, 24, 30, 24};
                    break;
                default:
                    break;
            }
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy HH:mm:ss"));
            logger.info("Generating Report in .xls format");

            //Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Type");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            logger.info("date range is : " + dateRange);
            cell.setCellValue(dateRange);
            cell.setCellStyle(heaCelSty);
            rowCnt = 2;

            //Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            rows = sheet.createRow(rowCnt + 1);
            if (repTyp == 12) {
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                cell.setCellValue("User Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(1);
                cell.setCellValue("Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(2);
                cell.setCellValue("Email Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(3);
                cell.setCellValue("Signup Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(4);
                cell.setCellValue("Payment Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(5);
                cell.setCellValue("Next Payment Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(6);
                cell.setCellValue("Revenue");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(7);
                cell.setCellValue("Auto Payment");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(8);
                cell.setCellValue("Package Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(9);
                cell.setCellValue("Package Details");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(10);
                cell.setCellValue("Country");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(11);
                cell.setCellValue("Region");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(12);
                cell.setCellValue("Current Status");
                cell.setCellStyle(columnHeaderStyle);
            } else if (repTyp == 11) {
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(0);
                cell.setCellValue("User Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(1);
                cell.setCellValue("Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(2);
                cell.setCellValue("Email Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(3);
                cell.setCellValue("Signup Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(4);
                cell.setCellValue("Download Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(5);
                cell.setCellValue("Installation Status");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(6);
                cell.setCellValue("Installation Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(7);
                cell.setCellValue("Installation Code");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(8);
                cell.setCellValue("Country");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(9);
                cell.setCellValue("Region");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(10);
                cell.setCellValue("Current Status");
                cell.setCellStyle(columnHeaderStyle);
            } else {
                colCount = sqlRowSetMetDat.getColumnCount();
                rows = sheet.createRow(rowCnt + 1);
                rows.setHeight((short) 700);
                for (int i = 1; i <= colCount; i++) {
                    cell = rows.createCell(i - 1);
                    cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i));
                    cell.setCellStyle(columnHeaderStyle);
                }
            }
            rowCnt = 4;
//Report Data
            while (sqlRowSet.next()) {
                String packageType = "Not Available";
                String autoPayment = "";
                if (repTyp == 12) {
                    if (sqlRowSet.getInt("package") != 0) {
                        int packValue = sqlRowSet.getInt("package");
                        if (packValue == 1) {
                            packageType = "Starter";
                        } else if (packValue == 2) {
                            packageType = "Pro49";
                        } else if (packValue == 3) {
                            packageType = "Pro99";
                        }
                    }
                    if (sqlRowSet.getBoolean("renew_license")) {
                        autoPayment = "Yes";
                    } else {
                        autoPayment = "No";
                    }
                }
                rows = sheet.createRow(rowCnt);
                int tempCount = 0;
                for (int colInd = 1; colInd <= colCount; colInd++) {
                    cell = rows.createCell(tempCount);
                    tempCount++;
                    if (colInd == 9 && repTyp == 12) {
                        cell.setCellValue(packageType);
                    } else if (colInd == 8 && repTyp == 12) {
                        cell.setCellValue(autoPayment);
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                        if (sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                            cell.setCellValue("Not Available");
                        } else {
                            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(colInd)));
                        }
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                        cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                        if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                            cell.setCellValue("Not Available");
                        } else {
                            cell.setCellValue(sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)));
                            cell.setCellStyle(dateStyle);
                        }
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                        if (sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)) != 0) {
                            cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                        }
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    } else {
                        String colVal = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd));
                        boolean nullval = false;
                        if (colVal == null || colVal.equals("")) {
                            nullval = true;
                        }
                        if (!nullval) {
                            cell.setCellValue(colVal);
                        } else if (nullval && repTyp == 17) {
                            cell.setCellValue("");
                        } else {
                            cell.setCellValue("Not Available");
                        }
                    }
                }
                rowCnt++;
            }
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            String reptName = Common.removeSpaces(repName);
            String fileName = "";
            fileName = filePath + Common.createFileName(reptName) + ".xls";
            logger.info(" Report generated in .xls format");
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public String generateXLSXReport(SqlRowSet sqlRowSet, String filePath,
            String dateRange, String repName, int repTyp) {
        try {
            int colCount;
            int rowCnt = -1;
            SqlRowSetMetaData sqlRowSetMetDat = sqlRowSet.getMetaData();

            XSSFWorkbook workBook = new XSSFWorkbook();
            XSSFSheet sheet = workBook.createSheet();
            XSSFCellStyle heaCelSty;
            XSSFFont font = null;
            XSSFRow rows;
            XSSFCell cell = null;
            XSSFCellStyle columnHeaderStyle = null;
            CreationHelper createHelper = workBook.getCreationHelper();
            CellStyle numericStyle = workBook.createCellStyle();
            DataFormat format = workBook.createDataFormat();
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[] = null;

// Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(true);
            font.setFontHeightInPoints((short) 10);
            font.setFontName(HSSFFont.FONT_ARIAL);
            heaCelSty.setFont(font);

// Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            sheet.createFreezePane(0, 4);

//Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            switch (repTyp) {
                case 12:
                    colWidth = new int[]{16, 24, 24, 24, 20, 20, 12, 15, 15, 15, 20, 20, 15, 20, 20};
                    break;
                case 11:
                    colWidth = new int[]{16, 24, 24, 24, 24, 16, 24, 22, 20, 20, 17, 12};
                    break;
                case 13:
                    colWidth = new int[]{16, 24, 24, 24, 24, 24, 20, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                case 14:
                    colWidth = new int[]{16, 24, 24, 24, 24, 24, 20, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                case 15:
                    colWidth = new int[]{10, 24, 30, 24, 24, 12, 24, 20, 17, 20, 20, 24, 30, 30, 12};
                    break;
                default:
                    break;
            }
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy HH:mm:ss"));
            logger.info("Generating Report in .xls format");

//Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Type");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            logger.info("date range is : " + dateRange);
            cell.setCellValue(dateRange);
            cell.setCellStyle(heaCelSty);
            rowCnt = 2;

//Report Columns
            colCount = sqlRowSetMetDat.getColumnCount();
            rows = sheet.createRow(rowCnt + 1);
            if (repTyp == 12) {
                rows.setHeight((short) 500);
                cell = rows.createCell(0);
                cell.setCellValue("User Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(1);
                cell.setCellValue("Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(2);
                cell.setCellValue("Email Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(3);
                cell.setCellValue("Signup Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(4);
                cell.setCellValue("Payment Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(5);
                cell.setCellValue("Next Payment Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(6);
                cell.setCellValue("Revenue");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(7);
                cell.setCellValue("Auto Payment");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(8);
                cell.setCellValue("Package Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(9);
                cell.setCellValue("Package Details");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(10);
                cell.setCellValue("Country");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(11);
                cell.setCellValue("Region");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(12);
                cell.setCellValue("Current Status");
                cell.setCellStyle(columnHeaderStyle);
            } else if (repTyp == 11) {
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(0);
                cell.setCellValue("User Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(1);
                cell.setCellValue("Name");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(2);
                cell.setCellValue("Email Id");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(3);
                cell.setCellValue("Signup Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(4);
                cell.setCellValue("Download Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(5);
                cell.setCellValue("Installation Status");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(6);
                cell.setCellValue("Installation Date");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(7);
                cell.setCellValue("Installation Code");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(8);
                cell.setCellValue("Country");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(9);
                cell.setCellValue("Region");
                cell.setCellStyle(columnHeaderStyle);
                cell = rows.createCell(10);
                cell.setCellValue("Current Status");
                cell.setCellStyle(columnHeaderStyle);
            } else {
                colCount = sqlRowSetMetDat.getColumnCount();
                rows = sheet.createRow(rowCnt + 1);
                rows.setHeight((short) 700);
                for (int i = 1; i <= colCount; i++) {
                    cell = rows.createCell(i - 1);
                    cell.setCellValue(sqlRowSetMetDat.getColumnLabel(i));
                    cell.setCellStyle(columnHeaderStyle);
                }
            }
            rowCnt = 4;
//Report Data
            while (sqlRowSet.next()) {
                String packageType = "Not Available";
                String autoPayment = "";
                if (repTyp == 12) {
                    if (sqlRowSet.getInt("package") != 0) {
                        int packValue = sqlRowSet.getInt("package");
                        switch (packValue) {
                            case 1:
                                packageType = "Starter";
                                break;
                            case 2:
                                packageType = "Pro49";
                                break;
                            case 3:
                                packageType = "Pro99";
                                break;
                            default:
                                break;
                        }
                    }
                    if (sqlRowSet.getBoolean("renew_license")) {
                        autoPayment = "Yes";
                    } else {
                        autoPayment = "No";
                    }
                }
                rows = sheet.createRow(rowCnt);
                int tempCount = 0;
                for (int colInd = 1; colInd <= colCount; colInd++) {
                    cell = rows.createCell(tempCount);
                    tempCount++;
                    if (colInd == 9 && repTyp == 12) {
                        cell.setCellValue(packageType);
                    } else if (colInd == 8 && repTyp == 12) {
                        cell.setCellValue(autoPayment);
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DATE) {
                        if (sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                            cell.setCellValue("Not Available");
                        } else {
                            cell.setCellValue(sqlRowSet.getDate(sqlRowSetMetDat.getColumnName(colInd)));
                        }
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.BIGINT) {
                        cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.TIMESTAMP) {
                        if (sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd)) == null) {
                            cell.setCellValue("Not Available");
                        } else {
                            cell.setCellValue(sqlRowSet.getTimestamp(sqlRowSetMetDat.getColumnName(colInd)));
                            cell.setCellStyle(dateStyle);
                        }
                    } else if (sqlRowSetMetDat.getColumnType(colInd) == Types.DECIMAL) {
                        if (sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)) != 0) {
                            cell.setCellValue(sqlRowSet.getLong(sqlRowSetMetDat.getColumnName(colInd)));
                        }
                        numericStyle.setDataFormat(format.getFormat("0"));
                        cell.setCellStyle(numericStyle);
                    } else {
                        String colVal = sqlRowSet.getString(sqlRowSetMetDat.getColumnName(colInd));
                        boolean nullval = false;
                        if (colVal == null || colVal.equals("")) {
                            nullval = true;
                        }
                        if (!nullval) {
                            cell.setCellValue(colVal);
                        } else {
                            cell.setCellValue("Not Available");
                        }
                    }
                }
                rowCnt++;
            }
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }
            String reptName = Common.removeSpaces(repName);
            String fileName = "";
            logger.info(" Report generated in .xlsx format");
            fileName = filePath + Common.createFileName(reptName) + ".xlsx";
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public String lxrPluginUserBehaviourReport(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 13;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND u.is_local = 0 AND u.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRPlugin User Behaviour Report";

        String query = "select u.lxrplugin_user_id as 'User Id', u.name as 'Name', u.email as 'Email Id', "
                + "date_format(u.created_date,'%Y-%m-%d') as 'Sign up Date', date_format(d.download_date,'%Y-%m-%d') as 'Download Date', "
                + "date_format(d.installation_date,'%Y-%m-%d') as 'Installation Date', u.country as 'Country', u.region_name as 'Region', "
                + "case when date(v.end_date) < current_date() or date(v.end_date) is null then 'Inactive' else 'Active' end as 'Current Status' , "
                + "case when (d.client_logo) = 1 then 'Yes' else 'No' end as 'Client Logo', "
                + "case when d.agency_logo = 1 then 'Yes' else 'No' end as 'Agency Logo', "
                + "rcb.report_generated_time as 'Report Generated Time', "
                + "rcb.report_category_name as 'Report Category', rcb.report_name as 'Report Name' "
                + "from lxrplugin_users u left join (select * from lxrplugin_downloads group by lxrplugin_user_id) d on u.lxrplugin_user_id = d.lxrplugin_user_id "
                + "and d.is_deleted = 0 left join lxrplugin_validity v on u.lxrplugin_user_id = v.lxrplugin_user_id AND v.validity_id "
                + "in (select max(sv.validity_id) from lxrplugin_validity sv group by sv.lxrplugin_user_id), "
                + "(select ub.lxrplugin_user_id,ub.report_generated_time, rc.report_category_name, r.report_name "
                + "from lxrplugin_user_behaviour ub, lxrplugin_report_category rc, lxrplugin_report r "
                + "where ub.report_category_id= rc.report_category_id and r.report_id = ub.report_id and "
                + "r.report_category_id = ub.report_category_id) as rcb where rcb.lxrplugin_user_id = u.lxrplugin_user_id "
                + "and date(rcb.report_generated_time) between date_format(?,'%Y-%m-%d') and date_format(?,'%Y-%m-%d') " + subquery;
        try {
            logger.info("LXRPlugin User Behaviour Report:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = generateXLSXReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return null;
    }

    public String seoTipUsresDetails(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 14;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND ua.is_local = 0 AND ua.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "SEO User Tip Report";

        String query = "SELECT ua.id as 'User Id', ua.name as 'Name', ua.email as 'Email Id', "
                + "date_format(ua.activation_date,'%Y-%m-%d') as 'Activation Date', "
                + "case when ua.unsubscribe = 1 then 'Yes' else 'No' end as 'Unsubscribe', u.country as 'Country', "
                + "u.region_name as 'Region' FROM lxrm_user_aquisition ua left join lxrplugin_users u on ua.email = u.email "
                + "where date(ua.activation_date) between date_format(? ,'%Y-%m-%d') and "
                + "date_format(? ,'%Y-%m-%d') ";
        try {
            logger.info("SEO User Tip Report:" + query + ", fromDate:" + fromDate +", toDate:"+ toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = generateXLSXReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
        } catch (Exception e) {
            logger.error("Exception for seoTipUsresDetails: " + e);
        }
        return flag;
    }

    public String getAskTheExpertPayedUsersList(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {

        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 15;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND u.is_local = 0 AND u.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRM_Ask_the_Expert_Payment_Report";
        String query = "SELECT u.id as 'User ID', u.name as 'User Name', u.email as 'Email', p.transaction_id as 'Transaction Id', "
                + "p.created_time as 'Payment Date', p.amount as 'Amount($)', q.domain as 'Domain' "
                + "FROM lxrm_ate_questions q, lxrm_payment_transaction_stats p left join user u on p.payer_user_id = u.id "
                + "WHERE p.payment_id = q.payment_transaction_id and date(q.created_date) between "
                + "date_format(? ,'%Y-%m-%d') and date_format(? ,'%Y-%m-%d') " + subquery;
        try {
            logger.info("LXRM Ask the Expert Payment Report:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            if (dwnldTyp == 1) {
                flag = generateXLSXReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            } else if (dwnldTyp == 2) {
                flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            }
            return flag;
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return null;
    }

    public String getUsersWebsiteTrackingInfoList(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {
        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 16;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND u.is_local = 0 AND u.email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRM_Ask_the_Expert_Payment_Report";

        String query = "SELECT t.lxrm_user_id, u.name, u.email, t.created_date, t.website_id, t.website, "
                + "t.lxrm_tool_id, t.issues, t.suggested_question, t.tool_url FROM lxrm_websites_tracking t, user u "
                + "WHERE u.id = t.lxrm_user_id AND DATE_FORMAT(t.created_date, '%Y-%m-%d') "
                + "BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d') " + subquery;
        try {
            logger.info("LXRM User Website Tracking Report: " + query + "_" + fromDate + "_" + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            flag = createCSVFile(fromDate, toDate, sqlRowSet, appPath);
            return flag;
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return null;
    }

    public String createCSVFile(String fromDate, String toDate, SqlRowSet sqlRowSet, String filePath) {
        String fileName = "";
        PrintStream ps = null;
        FileOutputStream stream = null;
        String[] colHeaders = {"Name", "Email Id", "User Website", "Text For Expert Popup", "Text To Be Used in Question", "Ask Expert Button Link"};
        try {
            fileName = filePath + "LXRMarketplace_UserAnalysisReport_" + Calendar.getInstance().getTime().getTime() + ".csv";
            stream = new FileOutputStream(fileName);
            ps = new PrintStream(stream);
            ps.print("Report Type" + "\t");
            ps.print("Ask The Expert - User Data");
            ps.println();
            ps.print("Date Range" + "\t");
            ps.print(fromDate + " - " + toDate);
            ps.println();
            ps.println();
            for (int i = 0; i < colHeaders.length; i++) {
                ps.print(colHeaders[i] + "\t");
            }
            EncryptAndDecryptUsingDES encryptAndDecryptUsingDES = new EncryptAndDecryptUsingDES();
            if (sqlRowSet != null) {
                while (sqlRowSet.next()) {
                    ps.println();
                    long userId = sqlRowSet.getLong("lxrm_user_id");
                    long websiteTrackingId = sqlRowSet.getLong("website_id");
                    String encriptedUserId = encryptAndDecryptUsingDES.encrypt(String.valueOf(userId));
                    String encriptedTrackingId = encryptAndDecryptUsingDES.encrypt(String.valueOf(websiteTrackingId));
                    if (sqlRowSet.getString("name") != null && !sqlRowSet.getString("name").equals("")) {
//                        WordUtils.capitalize("") is for capitalize each word in a string
                        ps.print(WordUtils.capitalize(sqlRowSet.getString("name")) + "\t");
                    } else {
                        ps.print("Guest User" + "\t");
                    }
                    if (sqlRowSet.getString("email") != null && !sqlRowSet.getString("email").equals("")) {
                        ps.print(sqlRowSet.getString("email") + "\t");
                    } else {
                        ps.print("-" + "\t");
                    }
                    ps.print(sqlRowSet.getString("website") + "\t");

                    if (sqlRowSet.getString("issues") != null && !sqlRowSet.getString("issues").equals("")) {
                        String toolIssues = sqlRowSet.getString("issues");
                        toolIssues = toolIssues.replaceAll(",", "&#44");
                        toolIssues = toolIssues.replaceAll(";", "&#59");
                        ps.print(toolIssues + "\t");
                    } else {
                        ps.print("-" + "\t");
                    }
                    String suggestedQuestion = sqlRowSet.getString("suggested_question");
                    if (suggestedQuestion != null) {
                        suggestedQuestion = suggestedQuestion.replaceAll("", "");
                        suggestedQuestion = suggestedQuestion.replaceAll(",", "&#44");
                        suggestedQuestion = suggestedQuestion.replaceAll(";", "&#59");
                        suggestedQuestion = suggestedQuestion.trim();
                    }
                    ps.print(suggestedQuestion + "\t");
                    if (sqlRowSet.getString("tool_url") != null && !sqlRowSet.getString("tool_url").equals("")) {
                        String toolUrl = domainName + sqlRowSet.getString("tool_url") + "?user-id=" + encriptedUserId
                                + "&tracking-id=" + encriptedTrackingId + "#tool";
                        ps.print(toolUrl);
                    } else {
                        ps.print("-");
                    }
                }
            }
            return fileName;
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } finally {
            ps.flush();
            ps.close();
        }
        return fileName;
    }

    public String subscriptionReport(String fromDate, String toDate,
            String appPath, int dwnldTyp, String dateRange, boolean tdate, boolean includeLocal) {
        SqlRowSet sqlRowSet;
        String flag = "";
        int repTyp = 17;
        String subquery = "";
        if (!includeLocal) {
            subquery = " AND email NOT LIKE '%@netelixir.com%' ";
        }
        if (tdate) {
            fromDate = "2016-01-01 00:00:00"; //Page Tracking Starts
        }
        String repName = "LXRM_Subscriptions_Report";

        String query = "select name as Name, email as Email, aquisition_type as Aquisition_Type from lxrm_user_aquisition "
                + "where date(activation_date) between ? and ?" + subquery;

        try {
            logger.info("LXRM Subscription Report:" + query + "_" + fromDate + toDate);
            sqlRowSet = getJdbcTemplate().queryForRowSet(query, new Object[]{fromDate, toDate});
            flag = generateXLSReport(sqlRowSet, appPath, dateRange, repName, repTyp);
            return flag;
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return null;
    }
}
