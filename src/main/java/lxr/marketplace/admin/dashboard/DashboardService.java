package lxr.marketplace.admin.dashboard;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.ToolUsageInfo;
import net.sf.json.JSONArray;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

public class DashboardService extends JdbcDaoSupport {
	private static Logger logger = Logger.getLogger(DashboardService.class);
	String startDate;
	String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getActivations(String subquery) {

		String query = "select count(id), date_format(act_date, '%Y-%m-%d')  as 'act_day' from user "
				+ " where act_date between ? and ? "
				+ subquery
				+ " group by date_format(act_date, '%Y-%m-%d') order by date_format(act_date, '%Y-%m-%d')  ";
		return query;
	}

	public String getSignUps(String subquery) {
		String query = "select count(id), date_format(reg_date, '%Y-%m-%d')  as 'reg_day' from user "
				+ " where reg_date between ? and ? "
				+ subquery
				+ " group by date_format(reg_date, '%Y-%m-%d') "
				+ "order by date_format(reg_date, '%Y-%m-%d') ";
		return query;
	}

	public long GenerateLables() throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate1 = df.parse(startDate);
		Date endDate1 = df.parse(endDate);
		// logger.info(startDate1 + "startDate1 " + endDate1 + "endDate1" );
		long diff = getDateDiff(startDate1, endDate1);
		// logger.info("diff: "+diff);
		return diff;
	}

	public static long getDateDiff(Date firstDate, Date secondDate) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(firstDate);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(secondDate);
		// calendars are the same date
		if (cal1.equals(cal2)) {
			return 0;
		}
		// cal2 is before cal1 - make recursive call
		if (cal2.before(cal1)) {
			return -getDateDiff(secondDate, firstDate);
		}
		// cal1 is before cal2 - count days between
		long days = 0;
		while (cal1.before(cal2)) {
			cal1.add(Calendar.DATE, 1);
			days++;
		}
		return days;
	}

	public static Date addDays(Date d1, long days) {
		return new Date((d1.getTime() + days * 86400000));
	}

	public void getGoalConversionXml(HttpServletResponse response, long diff,
			boolean includeLocal) {
		String subquery = "";
		if (!includeLocal) {
			subquery = " and is_local=0 ";
		}
		SqlRowSet sqlRowSetact = null;
		SqlRowSet sqlRowSetsgnUP = null;
		try {
			sqlRowSetact = getJdbcTemplate().queryForRowSet(
					getActivations(subquery),
					new Object[] { startDate, endDate });
			sqlRowSetsgnUP = getJdbcTemplate().queryForRowSet(
					getSignUps(subquery), new Object[] { startDate, endDate });

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}

		try {
			StringBuilder goalConversionXml = new StringBuilder();
			StringBuilder catxml = new StringBuilder();
			StringBuilder actvxml = new StringBuilder();
			StringBuilder signUpxml = new StringBuilder();
			// goalConversionXml.append("<graph caption='Goal Conversion Rate' xAxisName='' yAxisName='' showValues= '0' hoverCapSepChar=' ' showLabels ='1' labelDisplay='ROTATE' rotateNames='1' slantLabels='1'  >");
			goalConversionXml
					.append("<graph xAxisName='' yAxisName='' showValues= '0' showShadow='0' "
							+ "hoverCapSepChar=' ' showLabels ='1' rotateNames='1' canvasBorderColor='C0C0C0' "
							+ "canvasBorderThickness='1'"
							+ "chartRightMargin='5' chartLeftMargin='5' chartTopMargin='5' chartBottomMargin='1' "
							+ "divLineDecimalPrecision='1' limitsDecimalPrecision='0' divLineThickness='0.5' lineThickness='2'>");
			actvxml.append("<dataset seriesname='No.of Registered Users' color='#FF6600' anchorRadius='2' "
					+ "anchorSides='20' anchorBgColor='#FF6600' alpha='70'>");
			signUpxml
					.append("<dataset seriesname='No.of Users' color='#0070C0' anchorRadius='2' anchorSides='20' "
							+ "anchorBgColor='#0070C0' alpha='70'>");
			catxml.append("<categories>");
			SqlRowSetMetaData sqlRowSetMetDatActv = sqlRowSetact.getMetaData();
			SqlRowSetMetaData sqlRowSetMetDatSgnUP = sqlRowSetsgnUP
					.getMetaData();
			SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat displayFormat = new SimpleDateFormat("dd MMM ");
			String actxmlDate = "";
			String sgnUpxmlDate = "";
			Date tempDate = new Date();
			boolean actEnd = false, sgnEnd = false;
			sqlRowSetact.first();
			sqlRowSetsgnUP.first();
			if (sqlRowSetact.getRow() == 0) {
				actEnd = true;
			}
			if (sqlRowSetsgnUP.getRow() == 0) {
				sgnEnd = true;
			}
			long actData = 0;
			long signUpData = 0;
			long percent = 0;

			if (!(actEnd && sgnEnd)) {
				long rem, maxNoLabels = 10;
				if (diff < maxNoLabels) {
					maxNoLabels = diff;
				}
				rem = (long) Math.ceil((double) diff / maxNoLabels);
				tempDate = dtf.parse(startDate);
				for (int i = 0; i <= diff; i++) {
					String strDate = sdf.format(tempDate);
					String displayDate = displayFormat.format(tempDate);
					if (i % rem == 0) {
						catxml.append("<category name='" + displayDate
								+ "' showName= '1' />");
					} else {
						catxml.append("<category name='" + displayDate
								+ "' showName= '0' />");
					}
					if (!actEnd) {
						actxmlDate = sqlRowSetact.getString(sqlRowSetMetDatActv
								.getColumnName(2).toString());
					}
					if (strDate.equals(actxmlDate)) {
						String actxmlData = sqlRowSetact
								.getString(sqlRowSetMetDatActv.getColumnName(1)
										.toString());
						if (actxmlData != null && !actxmlData.equals("0")) {
							actvxml.append("<set  value='" + actxmlData
									+ "' ></set>");
							// logger.info("actxmlData0 " + actxmlData +
							// "actxmlDate :" +actxmlDate);
							actData = actData + Long.parseLong(actxmlData);
						} else {
							actvxml.append("<set  value='" + 0 + " '></set>");
						}
						if (sqlRowSetact.isLast()) {
							actEnd = true;
						} else {
							sqlRowSetact.next();
						}
					} else {
						actvxml.append("<set  value='" + 0 + "'></set>");
					}
					if (!sgnEnd) {
						sgnUpxmlDate = sqlRowSetsgnUP
								.getString(sqlRowSetMetDatSgnUP
										.getColumnName(2).toString());
					}
					if (strDate.equals(sgnUpxmlDate)) {
						String SgnUPxmlData = sqlRowSetsgnUP
								.getString(sqlRowSetMetDatSgnUP
										.getColumnName(1).toString());
						if (SgnUPxmlData != null && !SgnUPxmlData.equals("0")) {
							signUpxml.append("<set  value='" + SgnUPxmlData
									+ "'  ></set>");
							// logger.info("SgnUPxmlData " + SgnUPxmlData +
							// "sgnUpxmlDate :" +sgnUpxmlDate);
							signUpData = signUpData + Long.parseLong(SgnUPxmlData);
						} else {
							signUpxml.append("<set  value='" + 0 + "'></set>");
						}

						if (sqlRowSetsgnUP.isLast()) {
							sgnEnd = true;
						} else {
							sqlRowSetsgnUP.next();
						}
					} else {
						signUpxml.append("<set  value='" + 0 + "'></set>");
					}
					tempDate = addDays(tempDate, 1);
				}
				actvxml.append("</dataset>");
				signUpxml.append("</dataset>");
				catxml.append("</categories>");
				goalConversionXml.append(catxml).append(actvxml).append(signUpxml).append("</graph>");
				// logger.info("signUpData :" + signUpData + "actData :" +
				// actData);
//				percent = getPercentage(actData, signUpData);
				// logger.info("percent" + percent);
				addInJson3(percent, goalConversionXml.toString(), response);
			} else {
				addInJson3(percent, "<graph></graph>", response);
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}

	public long getPercentage(long a, long b) {
		if (b != 0l) {
			return (long) Math.round((((double) a / b) * 100));
		} else {
			return 0;
		}
	}

	public void addInJson3(long data, String stringData,HttpServletResponse response) {
		JSONArray arr = null;
		arr = new JSONArray();
		PrintWriter writer = null;

		try {
			writer = response.getWriter();
			arr.add(0, data);
			arr.add(1, stringData);
			writer.print(arr);
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.print("{success: false}");
		} finally {
			writer.flush();
			writer.close();
		}
	}

	public void numberofTimesToolUsed(HttpServletResponse response,
			boolean includeLocal) throws IOException {

		/********
		 * DO NOT DELETE THIS QUERY
		 * ----------------------------------------------------------------
		 * 
		 * String query =
		 * "select ifnull(sum(if(tool_id = '1',count,0)),0) as 'URL Auditor', "
		 * +
		 * " ifnull(sum(if(tool_id = '2',count,0)),0) as 'Keyword Combinator' ,"
		 * + " ifnull(sum(if(tool_id = '3',count,0)),0) as 'ROI Calculator'," +
		 * " ifnull(sum(if(tool_id = '4',count,0)),0) as 'Keyword Performance Analyzer' ,"
		 * +
		 * " ifnull(sum(if(tool_id = '5',count,0)),0) as 'Amazon Price Analyzer' "
		 * +
		 * " from tool_usage   where (select min(id) from session where start_time between ? and ? )<= "
		 * +
		 * " tool_usage.session_id  and tool_usage.session_id<= (select max(id) from session where start_time between ? and ? ) "
		 * ;
		 * 
		 * String query =
		 * "select sum(roiUsed), sum(keyCombUsed), sum(urlUsed), sum(keyPerfUsed), sum(amzTrackUsed), count(user_id) from "
		 * + "(select s.user_id, if(sum(URL_Auditor_Usage)>0,1,0) as urlUsed, "
		 * +
		 * "if(sum(KeywordCombination_Usage)>0,1,0) as keyCombUsed, if(sum(ROI_Usage)>0,1,0) as roiUsed, "
		 * +
		 * "if(sum(KeyPerformance)>0,1,0) as keyPerfUsed, if(sum(Amazon_Price_Tracker) > 0 ,1,0) as "
		 * +
		 * "amzTrackUsed from(select session_id, sum(if(tool_id = '1',1,0))  as URL_Auditor_Usage, "
		 * +
		 * "sum(if(tool_id = '2',1,0)) as KeywordCombination_Usage, sum(if(tool_id = '3',1,0)) as ROI_Usage, "
		 * +
		 * "sum(if(tool_id = '4',1,0)) as KeyPerformance, sum(if(tool_id = '5',1,0)) as  Amazon_Price_Tracker "
		 * +
		 * "from tool_usage group by session_id)toolPerSession, session s where toolPerSession.session_id=s.id "
		 * +
		 * "and s.start_time >= ? and s.end_time <= ? group by s.user_id)toolUsed"
		 * ;
		 */

		ArrayList<Long> dataList = new ArrayList<Long>();
		ArrayList<String> toolNameList = new ArrayList<String>();
		String subquery = "";
		if (!includeLocal) {
			subquery = " and (select is_local from user where id=user_id)=0 ";
		}
		String toolUsageQuery = "select tool_id, count(distinct user_id) as 'usage' from tool_usage, "
				+ "session where tool_usage.session_id = session.id and session.start_time between ? and ? " +
				"and end_time<>'0000-00-00 00:00:00'" +
				  subquery + " group by tool_id order by tool_id";
                String toolInfoQuery = null;
		/*selecting toolIdss related to LXRM and not consdering on tool related  LXRSEO Modified Aug 10,2017
                select id, name from tool order by id*/
                toolInfoQuery = "select id, name from tool where is_deleted = 0 and show_in_home = 1 or cat_id not in (0,3,2,6) or id = 0 order by id";
		String totalToolUserQuery = "select count(distinct user_id) from tool_usage, session "
				+ "where tool_usage.session_id = session.id and session.start_time between ? and ? " +
				" and end_time<>'0000-00-00 00:00:00'"
				+ subquery;
		String totalUserQuery = "select count(distinct user_id) from session where "
				+ "start_time between ? and ? and end_time<>'0000-00-00 00:00:00'" + subquery;

		SqlRowSet toolUsageRowSet = null;
		SqlRowSet toolInfoRowSet = null;
		int totalToolUser = 0;
		int datatotal = 0;

		try {
			datatotal = getJdbcTemplate().queryForInt(totalUserQuery,
					new Object[] { startDate, endDate });
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}
		if (datatotal == 0) {
			addInJson(null, "<chart></chart>", "", response);
		} else {

			try {
				toolUsageRowSet = getJdbcTemplate().queryForRowSet(
						toolUsageQuery, new Object[] { startDate, endDate });
				toolInfoRowSet = getJdbcTemplate()
						.queryForRowSet(toolInfoQuery);
				totalToolUser = getJdbcTemplate()
						.queryForInt(totalToolUserQuery,
								new Object[] { startDate, endDate });
			} catch (Exception e) {
				logger.error(e.getMessage());
				logger.error(e.getCause());
			}

			SqlRowSetMetaData toolUsageRowSetMetaData = toolUsageRowSet
					.getMetaData();
			SqlRowSetMetaData toolInfoRowSetMetaData = toolInfoRowSet
					.getMetaData();
			boolean toolUsageEnd = false;
			toolUsageRowSet.first();
			if (toolUsageRowSet.getRow() == 0) {
				toolUsageEnd = true;
			}
			while (toolInfoRowSet.next()) {
				int infoId = Integer.parseInt(toolInfoRowSet
						.getString(toolInfoRowSetMetaData.getColumnName(1)
								.toString()));
				if (!toolUsageEnd) {
					int usageId = Integer.parseInt(toolUsageRowSet
							.getString(toolUsageRowSetMetaData.getColumnName(1)
									.toString()));
					if (usageId == infoId) {
						dataList.add(Long.parseLong(toolUsageRowSet
								.getString(toolUsageRowSetMetaData
										.getColumnName(2).toString())));
						if (!toolUsageRowSet.next()) {
							toolUsageEnd = true;
						}
					} else {
						dataList.add(0l);
					}
				} else {
					dataList.add(0l);
				}
				toolNameList.add(toolInfoRowSet
						.getString(toolInfoRowSetMetaData.getColumnName(2)
								.toString()));
			}

			StringBuilder numberoftimestoolusedXml = new StringBuilder();
//			numberoftimestoolusedXml
//					.append("<chart decimalPrecision='1' showBorder='0' "
//							+ "numberSuffix='%25' borderThickness='1' divLineColor='white' borderColor='#C0C0C0' "
//							+ "showAlternateHGridColor='0' showPlotBorder='0' bgColor='#ffffff' "
//							+ "plotGradientColor='#55880A' baseFontColor='silver' baseFontSize='9' "
//							+ "canvasBorderColor='C0C0C0' canvasBorderThickness='1' chartRightMargin='5' chartLeftMargin='5' "
//							+ "chartTopMargin='5' chartBottomMargin='1'>");
			numberoftimestoolusedXml
            .append("<chart decimalPrecision='1' showBorder='0' "
                    + "numberSuffix='%25' plotgradientcolor=' ' bgcolor='FFFFFF' showalternatehgridcolor='0' " +
                    "showplotborder='0' showvalues='1' divlinecolor='CCCCCC' showcanvasborder='0' " +
                    "captionpadding='30' labeldisplay='ROTATE' tooltipbgcolor='FFFFFF' tooltipcolor='222222' tooltipbordercolor='aaaaaa' " +
                    "palettecolors='ffffff' canvasborderalpha='0' chartRightMargin='5' chartLeftMargin='5' "
                    + "chartTopMargin='5' chartBottomMargin='1'>");
			for (int i = 0; i < toolNameList.size(); i++) {
				if(!toolNameList.get(i).trim().equals("LXR SEO") && !toolNameList.get(i).trim().contains("Dashly")){
				numberoftimestoolusedXml
						.append("<set label='"
								+ toolNameList.get(i)
								+ "' value ='"
								+ Math.round((((double) dataList.get(i) / datatotal) * 100))
								+ "' color='#8FC916' />");
				}
			}
			numberoftimestoolusedXml
					.append("<set label='Visitors' value ='"
							+ Math.round((((double) (datatotal - totalToolUser) / datatotal) * 100))
							+ "' color='#8FC916' />");
			numberoftimestoolusedXml.append("</chart>");
			Long[] data = dataList.toArray(new Long[dataList.size()]);
			String[] toolNames = toolNameList.toArray(new String[toolNameList
					.size()]);
			String maxUsedTool;
			if (totalToolUser == 0) {
				maxUsedTool = "-";
			} else {
				maxUsedTool = findMaxUsedTools(data, toolNames);
			}
			addInJson(null, numberoftimestoolusedXml.toString(), maxUsedTool,
					response);
		}
	}

	public String findMaxUsedTools(Long[] data, String[] toolNames) {
		String maxToolUsed = "";
		long max = data[0];
		int[] ids = new int[data.length];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = -1;
		}
		int count = 0;
		ids[count++] = 0;
		for (int i = 1; i < data.length; i++) {
			if (data[i] > max) {
				max = data[i];
				count = 0;
				ids[count++] = i;
			} else if (data[i] == max) {
				ids[count++] = i;
			}
		}
		for (int i = 0; i < count; i++) {
			maxToolUsed += toolNames[ids[i]] + ", ";
		}
		return maxToolUsed.substring(0, maxToolUsed.lastIndexOf(", "));
	}

	public void NewVistorstoTotalVisitors(HttpServletResponse response,
			boolean includeLocal) {
		String totalVisitsQry, newVisitsQry, noToolUsageQry;
		long totalVisitsCount = 0, newVisitsCount = 0 , noToolUsageCount = 0;
		String condition = (!includeLocal ? " and u.is_local = 0 " : " ");

		totalVisitsQry = "select count(s.id) from session s, user u where s.start_time between ? and ?"
			              + " and s.user_id = u.id and u.deleted = 0 " + condition;
		newVisitsQry = "select count(distinct u.id) from user u, session s where u.reg_date between ? and ? " +
				"and s.user_id = u.id  and u.deleted = 0 " + condition;
		noToolUsageQry = "select count(distinct s.id) from session s, user u where u.id = s.user_id and s.id not in" +
				"(select tool_usage.session_id from tool_usage) " +
				"and s.start_time between ? and ?  and s.end_time<>'0000-00-00 00:00:00' and u.deleted = 0 " + condition;
		try {
			totalVisitsCount = getJdbcTemplate().queryForLong(totalVisitsQry,
					new Object[] { startDate, endDate });
			newVisitsCount = getJdbcTemplate().queryForLong(newVisitsQry, new Object[] { startDate, endDate });
			noToolUsageCount = getJdbcTemplate().queryForLong(noToolUsageQry, new Object[] { startDate, endDate });
		} catch (Exception e) {
			logger.error("Exception in new vistorsto total visitors:",e);
		}
		
		long[] data = { newVisitsCount, totalVisitsCount - newVisitsCount,
				totalVisitsCount, noToolUsageCount };

		if (totalVisitsCount > 0) {
			StringBuilder newVisitorsXml = new StringBuilder();
			newVisitorsXml
					.append("<graph caption='' showShadow='1' pieBorderThickness='0' enableRotation ='1' "
							+ "pieRadius='68' decimalPrecision='0' showValues='0'  showPercentageInLabel='0' baseFontSize='12' "
							+ "skipOverlapLabels='1' hoverCapBgColor='ffffff' pieBorderAlpha='50' animation='0' "
							+ "nameTBDistance='4' hoverCapSepChar=' ' bgAlpha='0'>");
			newVisitorsXml.append("<set  value='" + data[0]
					+ "' isSliced='0'  color='#058DC7'></set>");
			newVisitorsXml.append("<set  value='" + data[1]
					+ "' isSliced='0'  color='#50B432'></set>");
			newVisitorsXml.append("</graph>");
			addInJson(data, newVisitorsXml.toString(), "", response);
		} else {
			addInJson(data, "<graph></graph>", "", response);
		}
	}

	public void noOfUsers(HttpServletResponse response, boolean includeLocal) {
		String subquery = (!includeLocal ? " and u.is_local = 0 " : " ");

		String queryTotalUser = "select count(distinct u.id) from user u, session s where u.reg_date between ? and ? "
			+ " and u.id = s.user_id " + subquery;
	
	    String regUser = "select count(id) from user u where u.act_date between ? and ? "
			+ subquery;

	    String repUser = "select count(u.id) from user u, ("
			+ " select distinct f.user_id as id from session f, session s"
			+ " where (f.user_id = s.user_id and f.id != s.id and f.start_time between ? and  ? "
			+ " and TIMESTAMPDIFF(SECOND,s.start_time, f.start_time) > 172800 )) us where  u.id = us.id "
			+ subquery;
		
	    String repRegUser ="select count(u.id) from user u, (select distinct f.user_id as uid from session f, " +
	    		"session s  where (f.user_id = s.user_id  and f.id != s.id and f.start_time between ? and ? " +
			" and TIMESTAMPDIFF(SECOND,s.start_time, f.start_time) > 172800 )) us where u.id = us.uid and " +
			"u.act_date between ? and ? "+ subquery;

		int totalUser = 0;
		int regUserCount = 0;
		int repUserCount = 0;
		int repRegUserCount = 0;

		try {
			totalUser = getJdbcTemplate().queryForInt(queryTotalUser,
					new Object[] { startDate, endDate });
			regUserCount = getJdbcTemplate().queryForInt(regUser,
					new Object[] { startDate, endDate });
			logger.info("start date is  :"+startDate);
			logger.info("end date is  :"+endDate);
			repUserCount = getJdbcTemplate().queryForInt(repUser,
					new Object[] { startDate, endDate });
			repRegUserCount = getJdbcTemplate().queryForInt(repRegUser,
					new Object[] { startDate, endDate, startDate, endDate });
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}
		long[] data = { totalUser, regUserCount, repUserCount, repRegUserCount };
		addInJson(data, "", "", response);
	}
	
	public void userBehaviorQueries(HttpServletResponse response, boolean includeLocal){
	    String subquery = (!includeLocal ? " AND is_local = 0 " : " ");
	    
	    String totalUsersVisitedQuery = "SELECT  COUNT(DISTINCT A.user_id) FROM (SELECT * FROM session WHERE " +
	    		"start_time BETWEEN ? AND  ? )A , (SELECT * FROM user WHERE deleted = 0 " + subquery 
	    		+")B WHERE A.user_id = B.id";
	    
	    /* Users visited for the first time in selected date range */
	    String newUsersQuery = "SELECT  COUNT(*) FROM user WHERE reg_date BETWEEN ? AND ? " +
	    		"and deleted = 0 " + subquery;
	    
	    /* Users visited for the first time and also registered in selected date range */
	    String newUsersRegisteredQuery = "SELECT  COUNT(*) FROM user WHERE reg_date BETWEEN ? AND ? AND " +
	    		"act_date BETWEEN ? AND ? AND deleted = 0 " + subquery;
	    
	    /* Users visited for the first time in selected date range but registered after the date range*/
	    String newUsersRegisteredLaterQuery = "SELECT  COUNT(*) FROM user WHERE reg_date BETWEEN " +
	    		"? AND ? AND act_date > ? AND deleted = 0 " + subquery;
	    
	    /* Users visited for the first time in selected date range and have at least two 
	     * sessions with 1 day gap in selected date range */
	    String newRepeatedUsersQuery = "SELECT  COUNT(DISTINCT A.id) FROM (SELECT DISTINCT f.user_id " +
	    		"AS id FROM session f, session s WHERE f.user_id = s.user_id AND f.id != s.id AND " +
	    		"f.start_time BETWEEN ? AND ?  AND TIMESTAMPDIFF(SECOND,s.start_time, f.start_time) " +
	    		"> 86400)A, (SELECT * FROM user where reg_date BETWEEN ? AND ? AND deleted = 0 " +
	    		subquery + ")B WHERE A.id = B.id";
	    
	    /* Users visited for the first time, as well as registered in selected date range and also have 
	     * at least two sessions with 1 day gap in selected date range */
	    String newRepeatedRegisteredUsersQuery = "SELECT  COUNT(DISTINCT A.id) FROM (SELECT DISTINCT f.user_id " +
                "AS id FROM session f, session s WHERE f.user_id = s.user_id AND f.id != s.id AND " +
                "f.start_time BETWEEN ? AND ? AND TIMESTAMPDIFF(SECOND,s.start_time, f.start_time) " +
                "> 86400)A, (SELECT * FROM user where reg_date BETWEEN ? AND ? AND act_date " +
                "BETWEEN ? AND ? AND deleted = 0" + subquery + ")B WHERE A.id = B.id";
	    
	    /* Users who visited in the selected date range and also visited earlier*/ 
	    String returningUsersQuery = "SELECT COUNT(*) FROM user WHERE reg_date < ? AND id IN (SELECT " +
	    		"DISTINCT user_id FROM session  WHERE start_time BETWEEN ? AND ?) AND deleted = 0" + subquery;
	    
	    
	    /* Users who registered earlier and visited in the selected date range*/ 
        String retRegisteredEarlierUsersQuery = "SELECT COUNT(*) FROM user WHERE reg_date < ? AND id IN (SELECT " +
                "DISTINCT user_id FROM session  WHERE start_time BETWEEN ? AND ?) AND deleted = 0 " +
                "AND act_date < ?" + subquery;
        
        /* Users who visited earlier but registered in the selected date range*/ 
        String retRegisteredNowUsersQuery = "SELECT COUNT(*) FROM user WHERE reg_date < ? AND id IN (SELECT " +
                "DISTINCT user_id FROM session  WHERE start_time BETWEEN ? AND ?) AND deleted = 0 " +
                "AND act_date BETWEEN ? AND ?" + subquery;
	    
	    logger.debug("start date  :"+startDate + " end date  :"+endDate);
	    logger.debug("totalUsersVisitedQuery: " + totalUsersVisitedQuery);
	    logger.debug("newUsersQuery: " + newUsersQuery);
	    logger.debug("newUsersRegisteredQuery: " + newUsersRegisteredQuery);
	    logger.debug("newUsersRegisteredLaterQuery: " + newUsersRegisteredLaterQuery);
	    logger.debug("newRepeatedUsersQuery: " + newRepeatedUsersQuery);
	    logger.debug("newRepeatedRegisteredUsersQuery: " + newRepeatedRegisteredUsersQuery);
	    long totalUsersVisited = 0;
	    long newUsers = 0;
        long newUsersRegistered = 0;
        long newUsersRegisteredLater = 0;
        long newRepeatedUsers = 0;
        long newRepeatedRegisteredUsers = 0;
        long returningUsers = 0;
        long retRegisteredEarlierUsers = 0;
        long retRegisteredNowUsers = 0;
        try {
            totalUsersVisited = getJdbcTemplate().queryForLong(totalUsersVisitedQuery, 
                    new Object[] {startDate, endDate});
            newUsers = getJdbcTemplate().queryForLong(newUsersQuery,new Object[] {startDate, endDate});
            newUsersRegistered = getJdbcTemplate().queryForLong(newUsersRegisteredQuery,
                    new Object[] {startDate, endDate, startDate, endDate});           
            newUsersRegisteredLater = getJdbcTemplate().queryForLong(newUsersRegisteredLaterQuery,
                    new Object[] {startDate, endDate, endDate});
            newRepeatedUsers = getJdbcTemplate().queryForLong(newRepeatedUsersQuery,
                    new Object[] {startDate, endDate, startDate, endDate});
            newRepeatedRegisteredUsers = getJdbcTemplate().queryForLong(newRepeatedRegisteredUsersQuery,
                    new Object[] {startDate, endDate, startDate, endDate, startDate, endDate});
            returningUsers = getJdbcTemplate().queryForLong(returningUsersQuery,
                    new Object[] {startDate, startDate, endDate});
            retRegisteredEarlierUsers = getJdbcTemplate().queryForLong(retRegisteredEarlierUsersQuery,
                    new Object[] {startDate, startDate, endDate, startDate});
            retRegisteredNowUsers = getJdbcTemplate().queryForLong(retRegisteredNowUsersQuery,
                    new Object[] {startDate, startDate, endDate, startDate, endDate});
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        logger.debug("totalUsersVisited: " + totalUsersVisited + " newUsers: " + newUsers + 
                " newUsersRegistered: " + newUsersRegistered + " newUsersRegisteredLater: " + 
                newUsersRegisteredLater + " newRepeatedUsers: " + newRepeatedUsers 
                + " newRepeatedRegisteredUsers: " + newRepeatedRegisteredUsers + " returningUsers: " 
                + returningUsers);
        long[] data = {totalUsersVisited, newUsers, newUsersRegistered, newUsersRegisteredLater, 
                newRepeatedUsers, newRepeatedRegisteredUsers, returningUsers, retRegisteredEarlierUsers, 
                retRegisteredNowUsers, returningUsers - (retRegisteredEarlierUsers + retRegisteredNowUsers)};
        addInJson(data, "", "", response);
	}
	
    public void userBehaviorToolUsageV1(HttpServletResponse response, boolean includeLocal) {
        String subquery = (!includeLocal ? " AND is_local = 0 " : " ");
        String newRepeatedToolUserQuery = "SELECT COUNT(*) FROM user WHERE id in (SELECT D.user_id FROM " +
        		"(SELECT C.user_id FROM (SELECT user_id, max(start_time) as max_t, min(start_time) as min_t " +
        		"FROM session WHERE start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00' GROUP BY user_id)C WHERE " +
        		"TIMESTAMPDIFF(SECOND, C.min_t, C.max_t) >= 86400)D, (SELECT DISTINCT A.user_id FROM " +
        		"(SELECT * FROM session WHERE start_time BETWEEN ? AND  ?)A, tool_usage B WHERE " +
        		"A.id = B.session_id)E WHERE D.user_id = E.user_id) AND reg_date " +
        		"BETWEEN ? AND  ? AND deleted = 0 "+subquery;
        
        String newNonRepeatedToolUserQuery = "SELECT COUNT(*) FROM user WHERE id in (SELECT D.user_id FROM " +
                "(SELECT C.user_id FROM (SELECT user_id, max(start_time) as max_t, min(start_time) as min_t " +
                "FROM session WHERE start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00' GROUP BY user_id)C WHERE " +
                "TIMESTAMPDIFF(SECOND, C.min_t, C.max_t) < 86400)D, (SELECT DISTINCT A.user_id FROM " +
                "(SELECT * FROM session WHERE start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00')A, tool_usage B WHERE " +
                "A.id = B.session_id)E WHERE D.user_id = E.user_id) AND reg_date " +
                "BETWEEN ? AND  ? AND deleted = 0 "+subquery;
        
        String returningToolUserQuery ="SELECT COUNT(C.user_id) FROM (SELECT DISTINCT A.user_id FROM " +
        		"(SELECT * FROM session WHERE start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00')A , " +
        		"tool_usage B WHERE A.id = B.session_id GROUP BY user_id)C , (SELECT * FROM user WHERE reg_date " +
        		"< ? AND deleted = 0 " + subquery + ")D WHERE C.user_id = D.id";

        long newRepeatedToolUser = 0;
        long newNonRepeatedToolUser = 0;
        long returningToolUser = 0;
        logger.debug("start date  :"+startDate + " end date  :"+endDate);
        try {
            newRepeatedToolUser = getJdbcTemplate().queryForLong(newRepeatedToolUserQuery,
                    new Object[] {startDate, endDate, startDate, endDate, startDate, endDate});
            newNonRepeatedToolUser = getJdbcTemplate().queryForLong(newNonRepeatedToolUserQuery,
                    new Object[] {startDate, endDate, startDate, endDate, startDate, endDate});
            returningToolUser = getJdbcTemplate().queryForLong(returningToolUserQuery,
                    new Object[] { startDate, endDate, startDate});
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        long totalToolUser = newRepeatedToolUser + newNonRepeatedToolUser + returningToolUser;
        long[] data = {totalToolUser, newRepeatedToolUser, newNonRepeatedToolUser, returningToolUser};
        addInJson(data, "", "", response);
    }	
    
    public void userBehaviorToolUsageV2(HttpServletResponse response, boolean includeLocal) {
        String subquery = (!includeLocal ? " AND is_local = 0 " : " ");
        
        String totalVisitorsQuery1 = "SELECT  COUNT(DISTINCT A.user_id) FROM (SELECT * FROM session WHERE " +
                "start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00')A , (SELECT * FROM user WHERE deleted = 0  AND is_local = 0 " + subquery 
                +")B WHERE A.user_id = B.id";
        
        String totalVisitorsQuery = "select count(DISTINCT s.user_id) from session s,user u where s.user_id = u.id and "
                + " s.start_time BETWEEN ? AND  ? AND end_time<>'0000-00-00 00:00:00' and u.deleted = 0" + subquery;
        
        String regUserQuery = "select count(id) from user u where u.act_date between ? and ? " + subquery;
        
        String newUserQuery = "SELECT COUNT(C.user_id) FROM (SELECT user_id, min(start_time) " +
        		"first_tool_usage FROM session WHERE user_id IN (SELECT D.user_id FROM (SELECT " +
        		"DISTINCT A.user_id FROM (SELECT * FROM session WHERE start_time BETWEEN ? AND ?AND end_time<>'0000-00-00 00:00:00' )A, " +
        		"tool_usage B WHERE A.id = B.session_id)D, user E WHERE D.user_id = E.id AND " +
        		"deleted = 0 " + subquery + ") AND id in (SELECT session_id FROM tool_usage) GROUP BY user_id)C " +
        		"WHERE C.first_tool_usage >= ?";
        
        String repeatedUserQuery = "SELECT COUNT(C.user_id) FROM (SELECT user_id, min(start_time) " +
                "first_tool_usage FROM session WHERE user_id IN (SELECT D.user_id FROM (SELECT " +
                "DISTINCT A.user_id FROM (SELECT * FROM session WHERE start_time BETWEEN ? AND ? AND end_time<>'0000-00-00 00:00:00')A, " +
                "tool_usage B WHERE A.id = B.session_id)D, user E WHERE D.user_id = E.id AND " +
                "deleted = 0 " + subquery + ") AND id in (SELECT session_id FROM tool_usage) GROUP BY user_id)C " +
                "WHERE C.first_tool_usage < ?";
        long totalVisitors = 0;
        long regUsers = 0;
        long newUsers = 0;
        long repeatedUsers = 0;
        logger.debug("start date  :"+startDate + " end date  :"+endDate);
        try {
            totalVisitors = getJdbcTemplate().queryForLong(totalVisitorsQuery, 
                    new Object[] {startDate, endDate});
            regUsers = getJdbcTemplate().queryForLong(regUserQuery, new Object[] {startDate, endDate});
            newUsers = getJdbcTemplate().queryForLong(newUserQuery,
                    new Object[] {startDate, endDate, startDate});
            repeatedUsers = getJdbcTemplate().queryForLong(repeatedUserQuery,
                    new Object[] {startDate, endDate, startDate});
        }catch(Exception e){
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }
        long totalUsers = newUsers + repeatedUsers;
        long[] data = {totalVisitors, regUsers, totalUsers, newUsers, repeatedUsers};
        addInJson(data, "", "", response);
    }
    
	public void noOfToolsUsed(HttpServletResponse response, boolean includeLocal) {
		String subquery = "", subQuery2 = "";
		if (!includeLocal) {
			subquery = " and (select is_local from user where id=user_id)=0 ";
			subQuery2 = subquery = " and (select is_local from user where id=session.user_id)=0 ";
		}
		String query = "select sum(if(total.No_Tool_use = 1, 1, 0)) as 1_tool_user,"
				+ "sum(if(total.No_Tool_use = 2, 1, 0)) as 2_tool_user, "
				+ "sum(if(total.No_Tool_use = 3, 1, 0)) as 3_tool_user , "
				+ "sum(if(total.No_Tool_use between 4 and 6, 1, 0)) as 4_6_tool_user, "
				+ "sum(if(total.No_Tool_use > 6, 1, 0)) as 6Up_tool_user , b.0_tool_user from "
				+ "(select session.user_id, count(distinct tool_id) as No_Tool_use from tool_usage, "
				+ "session where tool_usage.session_id = session.id and session.start_time "
				+ "between ? and ? and session.end_time<>'0000-00-00 00:00:00'" + subquery + " group by user_id)total," 
			    + "(select count(distinct session.user_id) as 0_tool_user from session" 
			    + "  where  user_id not in(select session.user_id from tool_usage, session where " +
			    "tool_usage.session_id = session.id and session.start_time between ? and ? and session.end_time<>'0000-00-00 00:00:00'" + subQuery2 + ")" 
			    + " and session.start_time between ? and ?  and session.end_time<>'0000-00-00 00:00:00' " + subquery + ")b";

		Object[] args = { startDate, endDate, startDate, endDate, startDate, endDate};
                logger.info("startDate:"+startDate+", endDate:"+endDate);
                logger.info("query: "+query);
                logger.info("subQuery2: "+subQuery2);
                logger.info("subquery: "+subquery);
                
		long[] data = getData(6, query, args);
		String MaxNoOfTools = findMaxNoOfTools(data);
		addInJson(data, MaxNoOfTools, "", response);
	}
	        
	public String findMaxNoOfTools(long[] data) {
		if (isAllZero(data)) {
			return " - ";
		}
		String MaxNoOfTools = "";
		int[] ids = new int[data.length];
		int count = findAllMaxIds(ids, data);
		for (int i = 0; i < count; i++) {
			switch (ids[i]) {
			case 0:
				MaxNoOfTools += "1 Tool, ";
				break;
			case 1:
				MaxNoOfTools += "2 Tools, ";
				break;
			case 2:
				MaxNoOfTools += "3 Tools, ";
				break;
			case 3:
				MaxNoOfTools += "4 - 6 Tools, ";
				break;
			case 4:
				MaxNoOfTools += "6+ Tools, ";
				break;
			case 5:
				MaxNoOfTools += "Visitors, ";
				break;
			default:
				MaxNoOfTools += ", ";
			}
		}
		return MaxNoOfTools.substring(0, MaxNoOfTools.lastIndexOf(", "));
	}

	public void timeSpentPerSession(HttpServletResponse response,
			boolean includeLocal) {
		String subquery = "";
		if (!includeLocal) {
			subquery = " and (select is_local from user where id=user_id)=0 ";
		}
		String query = "select  sum(if(duration between 0.00 and 60.00,1,0)) as 'case1',"
				+ " ifnull(sum(if(duration between 61.00 and 300.00,1,0)),0) as 'case2', "
				+ " ifnull(sum(if(duration between 301.00 and 600.00,1,0)),0) as 'case3',"
				+ " ifnull(sum(if(duration between 601.00 and 1200.00,1,0)),0) as 'case4' , "
				+ " ifnull(sum(if(duration >= 1200 ,1,0)),0) as 'case5' "
				+ " from session  where  start_time between ? and ? "
				+ subquery;
		Object[] args = { startDate, endDate };
		long[] data = getData(5, query, args);
		String mostSessionTime = mostTimeSpent(data);
		addInJson(data, mostSessionTime, "", response);

	}

	public String mostTimeSpent(long[] data) {

		if (isAllZero(data)) {
			return " - ";
		}
		String mostSessionTime = "";
		int[] ids = new int[data.length];
		int count = findAllMaxIds(ids, data);
		for (int i = 0; i < count; i++) {
			switch (ids[i]) {
			case 0:
				mostSessionTime += "0-60 seconds, ";
				break;
			case 1:
				mostSessionTime += "61-300 seconds, ";
				break;
			case 2:
				mostSessionTime += "301-600 seconds, ";
				break;
			case 3:
				mostSessionTime += "601-1200 seconds, ";
				break;
			case 4:
				mostSessionTime += "1200+ seconds, ";
				break;
			default:
				mostSessionTime += ", ";
			}
		}
		return mostSessionTime.substring(0, mostSessionTime.lastIndexOf(", "));
	}

	public void sessionsWithinTimeRange(HttpServletResponse response,
			boolean includeLocal) {
		String subquery = "";
		if (!includeLocal) {
			subquery = " and (select is_local from user where id=user_id)=0 ";
		}
		String query = "select "
				+ " ifnull(sum(if(test.no_of_sessions  between 1 and 1,1,0)),0) as 'case1', "
				+ " ifnull(sum(if(test.no_of_sessions  between 2 and 3,1,0)),0) as 'case2', "
				+ " ifnull(sum(if(test.no_of_sessions  between 4 and 6,1,0)),0) as 'case3', "
				+ " ifnull(sum(if(test.no_of_sessions  between 7 and 10,1,0)),0) as 'case4', "
				+ " ifnull(sum(if(test.no_of_sessions  > 10 ,1,0)),0) as 'case5' from ( "
				+ " select user_id ,count(user_id) as no_of_sessions from session where start_time between "
				+ " ? and ? " + subquery + "  group by user_id) test ";
		Object[] args = { startDate, endDate };
		long[] data = getData(5, query, args);
		String mostNoSession = mostNoOfSession(data);
		addInJson(data, mostNoSession, "", response);
	}

	public String mostNoOfSession(long[] data) {
		if (isAllZero(data)) {
			return " - ";
		}
		String mostNoSession = "";
		int[] ids = new int[data.length];
		int count = findAllMaxIds(ids, data);

		for (int i = 0; i < count; i++) {
			switch (ids[i]) {
			case 0:
				mostNoSession += "1 Session, ";
				break;
			case 1:
				mostNoSession += "2-3 Sessions, ";
				break;
			case 2:
				mostNoSession += "4-6 Sessions, ";
				break;
			case 3:
				mostNoSession += "7-10 Sessions, ";
				break;
			case 4:
				mostNoSession += "10+ Sessions, ";
				break;
			default:
				mostNoSession += ", ";
			}
		}
		return mostNoSession.substring(0, mostNoSession.lastIndexOf(", "));
	}

	public void durationLastVisitFromNow(HttpServletResponse response,
			boolean includeLocal) {
		String subquery = "";
		if (!includeLocal) {
			subquery = " and (select is_local from user where id=user_id)=0 ";
		}
		String query = "select sum(if(z_session.last_session_diff < 24, 1, 0)) as less_1, "
				+ "sum(if(z_session.last_session_diff between 24 and 167 , 1, 0)) as less_7, "
				+ "sum(if(z_session.last_session_diff between 168 and 359 , 1, 0)) as less_15, "
				+ "sum(if(z_session.last_session_diff between 360 and 719 , 1, 0)) as less_30, "
				+ "sum(if(z_session.last_session_diff >= 720 , 1, 0)) as more_30 from"
				+ "(select user_id, timestampdiff(hour, max(end_time), ?) as last_session_diff "
				+ "from session where start_time between ? and ? "
				+ subquery
				+ "  group by user_id)z_session";
		Object[] args = { endDate, startDate, endDate };
		long[] data = getData(5, query, args);
		String diffPrevVisit = diffFromPrevVisit(data);
		addInJson(data, diffPrevVisit, "", response);
	}

	public String diffFromPrevVisit(long[] data) {
		if (isAllZero(data)) {
			return " - ";
		}
		String diffPrevVisit = "";

		int[] ids = new int[data.length];
		int count = findAllMaxIds(ids, data);
		for (int i = 0; i < count; i++) {
			switch (ids[i]) {
			case 0:
				diffPrevVisit += "Less than 1 day, ";
				break;
			case 1:
				diffPrevVisit += "1 - 7 days, ";
				break;
			case 2:
				diffPrevVisit += "7 - 15 days, ";
				break;
			case 3:
				diffPrevVisit += "15 - 30 days, ";
				break;
			case 4:
				diffPrevVisit += "More than 30 days, ";
				break;
			default:
				diffPrevVisit += ", ";
			}
		}
		return diffPrevVisit.substring(0, diffPrevVisit.lastIndexOf(", "));
	}

	public void addInJson(long[] data, String stringData, String otherVal,
			HttpServletResponse response) {
		JSONArray arr = null;
		arr = new JSONArray();
		PrintWriter writer = null;

		try {
			writer = response.getWriter();
			arr.add(0, data);
			arr.add(1, stringData);
			arr.add(2, otherVal);
			writer.print(arr);
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.print("{success: false}");
		} finally {
			writer.flush();
			writer.close();
		}
	}
	
	public void addInJsonObj(String[][] data, HttpServletResponse response,String toolname) {
		JSONArray arr = null;
		arr = new JSONArray();
		PrintWriter writer = null;

		try {
			writer = response.getWriter();
			arr.add(0, data);
			arr.add(1, toolname);
			writer.print(arr);
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.print("{success: false}");
		} finally {
			writer.flush();
			writer.close();
		}
	}


	public long[] getData(int noColumn, String query, Object[] args) {
		long[] data = new long[noColumn];

		List b = getJdbcTemplate().queryForList(query, args);

		String a = ((Object) b.get(0)).toString();
		logger.info(a);
		int fromIndex = 0, count = 0, endIndex = 0;
		String val;
		while (true && count < noColumn && endIndex >= 0) {
			fromIndex = a.indexOf('=', fromIndex);
			endIndex = a.indexOf(',', fromIndex);
			if (fromIndex >= 0 && endIndex >= 0) {
				val = a.substring(++fromIndex, endIndex);
				data[count++] = Long.parseLong(val.equals("null") ? "0" : val);
			} else {
				val = a.substring(++fromIndex, a.length() - 1);
				data[count++] = Long.parseLong(val.equals("null") ? "0" : val);
			}
		}
		return data;
	}

	public boolean isAllZero(long[] data) {
		for (long a : data) {
			if (a != 0)
				return false;
		}
		return true;
	}

	public int findAllMaxIds(int[] ids, long[] data) {
		long max = data[0];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = -1;
		}
		int count = 0;
		ids[count++] = 0;
		for (int i = 1; i < data.length; i++) {
			if (data[i] > max) {
				max = data[i];
				count = 0;
				ids[count++] = i;
			} else if (data[i] == max) {
				ids[count++] = i;
			}
		}
		return count;
	}
	
	
	//To get top used tools
	public void getTopUsedTools(boolean includeLocal,HttpServletResponse response){
		String[][] toolUsgArr = null;
		String subquery = "";
		if (!includeLocal) {
			subquery = " and is_local=0 ";
		}
		String query= "select tool.id as tool_id,tool.name,tot_sessions.count from " +
				"(select tu.tool_id, sum(tu.count) as count from tool_usage tu join " +
				"(select s.id from (select * from session where start_time " +
				"between ? and ? and end_time<>'0000-00-00 00:00:00') s join (select * from user where " +
				"is_admin = 0  "+subquery+")u on s.user_id = u.id)sessions on " +
				"tu.session_id = sessions.id group by tool_id) tot_sessions Join (select id, name from tool where is_deleted = 0 and show_in_home = 1 or cat_id not in (0,3,2,6) or id = 0 order by id) tool " +
				"on tool.id = tot_sessions.tool_id order by tot_sessions.count desc";
		logger.info("query......."+query);
		List<ToolUsageInfo> toolUsageInfoList = getToolUsage(query);
//		logger.info("toolUsageInfoList......."+toolUsageInfoList.size());
		if(toolUsageInfoList!=null && toolUsageInfoList.size() > 0){
			toolUsgArr = new String[toolUsageInfoList.size()][2];
			for(int i=0;i<toolUsageInfoList.size();i++){
				ToolUsageInfo tempInfo = toolUsageInfoList.get(i);
				toolUsgArr[i][0] = tempInfo.getToolName();
				toolUsgArr[i][1] = tempInfo.getUsageCount()+"";
			}
		}
		String toolName = findMaxUsedTool(toolUsgArr);
//		logger.info("toolName.................."+toolName);
		addInJsonObj(toolUsgArr,response,toolName);
   }
	
	public String findMaxUsedTool(String[][] data) {
		String toolName ="";
		if(data!=null && data.length > 0){
		long max = Long.parseLong(data[0][1]);
			for (int i = 0; i < data.length; i++) {
				if (Long.parseLong(data[i][1]) > max) {
					max = Long.parseLong(data[i][1]);
					toolName = data[i][0];
				}else if(Long.parseLong(data[i][1]) == max){
					toolName = data[i][0]+","+toolName;
				}
			}
//			finalToolName = toolName.substring(0,toolName.lastIndexOf(","));
		}
		return toolName;
	}
	
	
	
        public List<ToolUsageInfo> getToolUsage(String query){
		 try {
			 List<ToolUsageInfo> toolUsageObjs = getJdbcTemplate().query(query, new Object[] { startDate, endDate}, (ResultSet rs, int rowNum) -> {
                             ToolUsageInfo toolUsgObj = new ToolUsageInfo();
                             toolUsgObj.setToolId(rs.getLong("tool_id"));
                             toolUsgObj.setToolName(rs.getString("name"));
                             toolUsgObj.setUsageCount(rs.getLong("count"));
                             return toolUsgObj;
                         });
			 return toolUsageObjs;
		 }catch (Exception e) {
           logger.error(e.getMessage());
           logger.error(e.getCause());
           return null;
	     }        
	 }
	
	public String getMonthlyActivations(String subQuery){
		String query="select count(id) as 'usage',month(reg_date) as mont,YEAR(reg_date) as yea," +
				" date_format(reg_date, '%Y-%m-%d')  as 'reg_day' from user where " +
				"reg_date between ? and ? "+subQuery +
				"and is_admin = 0 group by yea,mont order by yea,mont";
		logger.info("getMonthlyActivations"+query);
		return query;
	}
	
	public String getMonthlySignups(String subQuery){
		String query="select count(id) as 'usage',month(act_date) as mont,YEAR(act_date) as yea," +
				" date_format(act_date, '%Y-%m-%d')  as 'act_day' from user where " +
				"act_date between ? and ? "+subQuery +
				"and is_admin = 0 group by yea,mont order by yea,mont";
		return query;
	}

	public void getMonthlyUsersXml(HttpServletResponse response, long diff,
			boolean includeLocal) {
		String subquery = "";
		if (!includeLocal) {
			subquery = " and is_local=0 ";
		}
		SqlRowSet sqlRowSetact = null;
		SqlRowSet sqlRowSetsgnUP = null;
		try {
			sqlRowSetact = getJdbcTemplate().queryForRowSet(
					getMonthlyActivations(subquery),
					new Object[] { startDate, endDate });
			logger.info("getMonthlyActivations"+startDate+"--"+endDate);
			sqlRowSetsgnUP = getJdbcTemplate().queryForRowSet(
					getMonthlySignups(subquery), new Object[] { startDate, endDate });

		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getCause());
		}

		try {
			StringBuilder monthlyGoalConversionXml = new StringBuilder();
			StringBuilder catxml = new StringBuilder();
			StringBuilder actvxml = new StringBuilder();
			StringBuilder signUpxml = new StringBuilder();
			// goalConversionXml.append("<graph caption='Goal Conversion Rate' xAxisName='' yAxisName='' showValues= '0' hoverCapSepChar=' ' showLabels ='1' labelDisplay='ROTATE' rotateNames='1' slantLabels='1'  >");
			monthlyGoalConversionXml.append("<graph xAxisName='' yAxisName='' showValues= '0' showShadow='0' "
							+ "hoverCapSepChar=' ' showLabels ='1' rotateNames='1' canvasBorderColor='C0C0C0' "
							+ "canvasBorderThickness='1'"
							+ "chartRightMargin='5' chartLeftMargin='5' chartTopMargin='5' chartBottomMargin='1' "
							+ "divLineDecimalPrecision='1' limitsDecimalPrecision='0' divLineThickness='0.5' lineThickness='2'>");
			signUpxml.append("<dataset seriesname='No.of Registered Users' color='#FF6600' anchorRadius='2' "
					+ "anchorSides='20' anchorBgColor='#FF6600' alpha='70'>");
			actvxml.append("<dataset seriesname='No.of Users' color='#0070C0' anchorRadius='2' anchorSides='20' "
							+ "anchorBgColor='#0070C0' alpha='70'>");
			catxml.append("<categories>");
			String actxmlDate = "";
			String sgnUpxmlDate = "";
			String monthYearDt = "";
			long actData = 0;
			long signUpData = 0;
			long percent = 0;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
			SimpleDateFormat displayFormat = new SimpleDateFormat("MMM/yyyy ");
			SimpleDateFormat monthYearFormat = new SimpleDateFormat("MM/yyyy ");
			
			
			boolean sgnEnd = false;
			sqlRowSetsgnUP.first();
			if (sqlRowSetsgnUP.getRow() == 0) {
				sgnEnd = true;
			}
			 while(sqlRowSetact.next()) {
			    String myRegDate = sqlRowSetact.getString("reg_day");
			    Date tempDate = sdf.parse(myRegDate);
			    String monthYearDate = monthYearFormat.format(tempDate);
				String displayDate = displayFormat.format(tempDate);
			    catxml.append("<category name='" + displayDate+ "' showName= '1' />");
				actxmlDate = myRegDate;
				
				String actxmlData = sqlRowSetact.getString("usage");
				logger.info("in while actxmlData"+actxmlData);
				if (actxmlData != null && !actxmlData.equals("0")) {
					actvxml.append("<set  value='" + actxmlData+ "' ></set>");
					actData = actData + Long.parseLong(actxmlData);
				} else {
					actvxml.append("<set  value='" + 0 + " '></set>");
				}
				
				if (!sgnEnd) {
					sgnUpxmlDate = sqlRowSetsgnUP.getString("act_day");
					 Date sgnDate = sdf.parse(myRegDate);
					 monthYearDt = monthYearFormat.format(sgnDate);
				}
				if (monthYearDate.equals(monthYearDt)) {
					String SgnUPxmlData = sqlRowSetsgnUP.getString("usage");
					if (SgnUPxmlData != null && !SgnUPxmlData.equals("0")) {
						signUpxml.append("<set  value='" + SgnUPxmlData
								+ "'  ></set>");
						// logger.info("SgnUPxmlData " + SgnUPxmlData +
						// "sgnUpxmlDate :" +sgnUpxmlDate);
						signUpData = signUpData + Long.parseLong(SgnUPxmlData);
					} else {
						signUpxml.append("<set  value='" + 0 + "'></set>");
					}

					if (sqlRowSetsgnUP.isLast()) {
						sgnEnd = true;
					} else {
						sqlRowSetsgnUP.next();
					}
				} else {
					signUpxml.append("<set  value='" + 0 + "'></set>");
				}

		}
		actvxml.append("</dataset>");
		signUpxml.append("</dataset>");
		catxml.append("</categories>");
		monthlyGoalConversionXml.append(catxml).append(signUpxml).append(actvxml).append("</graph>");
		logger.info("monthlyGoalConversionXml"+monthlyGoalConversionXml);
		percent = getPercentage(actData, signUpData);
		addInJson3(percent, monthlyGoalConversionXml.toString(), response);
		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}
	
}