package lxr.marketplace.admin.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.admin.report.Report;
import lxr.marketplace.user.Login;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class DashboardController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(DashboardController.class);
    DashboardService dashboardService;

    public DashboardService getDashboardService() {
        return dashboardService;
    }

    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    public DashboardController() {
        setCommandClass(Dashboard.class);
        setCommandName("dashboard");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors)
            throws Exception {
        HttpSession session = request.getSession(true);
        Dashboard dashboard = (Dashboard) command;
        long diff;
        String startDate = "2012-1-11 00:00:00";
        String endDate = "2012-2-14 23:59:59";

        Login user = (Login) session.getAttribute("user");
        boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
        if (userLoggedIn && user.isAdmin()) {
            if (WebUtils.hasSubmitParameter(request, "report")) {
                Report report = new Report();
                report = new Report();
                ModelAndView mAndV = new ModelAndView(getSuccessView(), "report", report);
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "automail")) {

                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/automail.html");
                return null;

            } else if (WebUtils.hasSubmitParameter(request, "settings")) {
                logger.info("moving to settings tab");
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/settings.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "block")) {
                boolean includeLocal = Boolean.valueOf(request.getParameter("local"));
                String block = request.getParameter("block");
                startDate = request.getParameter("startDate");
                endDate = request.getParameter("endDate");
                dashboardService.setStartDate(startDate);
                dashboardService.setEndDate(endDate);
                if (block.equals("1")) {
                    dashboardService.NewVistorstoTotalVisitors(response, includeLocal);
                } else if (block.equals("2")) {
                    dashboardService.userBehaviorToolUsageV2(response, includeLocal);
                } else if (block.equals("3")) {
                    diff = dashboardService.GenerateLables();
                    dashboardService.getGoalConversionXml(response, diff, includeLocal);
                } else if (block.equals("4")) {
                    dashboardService.numberofTimesToolUsed(response, includeLocal);
                } else if (block.equals("5")) {
                    dashboardService.noOfToolsUsed(response, includeLocal);
                } else if (block.equals("6")) {
                    dashboardService.timeSpentPerSession(response, includeLocal);
                } else if (block.equals("7")) {
                    dashboardService.sessionsWithinTimeRange(response, includeLocal);
                } else if (block.equals("8")) {
                    dashboardService.durationLastVisitFromNow(response, includeLocal);
                } else if (block.equals("9")) {
                    diff = dashboardService.GenerateLables();
                    dashboardService.getMonthlyUsersXml(response, diff, includeLocal);
                } else if (block.equals("10")) {
                    dashboardService.getTopUsedTools(includeLocal, response);
                } else {
                    return null;
                }
            } else if (WebUtils.hasSubmitParameter(request, "knowledgeBase")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/knowledge-base.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertAnswer")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/ate-question.html");
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "expertUsers")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/expert-user.html");
                return null;
            }else if (WebUtils.hasSubmitParameter(request, "pluginAdoption")) {
                String url = request.getScheme() + "://" + request.getServerName()
                        + ":" + request.getServerPort();
                response.sendRedirect(url + "/plugin-adoption.html");
                return null;
            }

        } else {
            String url = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort();
            response.sendRedirect(url + "/lxrmarketplace.html");
            return null;
        }
        logger.debug("viewing dashboard jsp");
        ModelAndView mAndV = new ModelAndView("view/admin/dashboard/Dashboard", "dashboard", dashboard);
        return mAndV;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {
        Dashboard dashboard = new Dashboard();
        return dashboard;

    }

    @Override
    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }
}
