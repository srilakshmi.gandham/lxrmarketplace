/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
//import org.jfree.ui.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class PluginAdoptionService {

    private static final Logger logger = Logger.getLogger(PluginAdoptionService.class);

    @Autowired
    PluginAdoptionDAO pluginAdoptionDAO;
    @Autowired
    private String downloadFolder;
    @Autowired
    String supportMail;
    String message = "";

    //Adding user into lxrplugin_adoption table
    public String saveAccount(PluginAdoptionDTO pluginAdoptionDTO) {

        long accountId = 0;
        //Checking account is already added or not
        pluginAdoptionDTO.setCustomerId(String.join("", pluginAdoptionDTO.getCustomerId().trim().split("-")));
        boolean isAccountFound = pluginAdoptionDAO.verifyingAccount(pluginAdoptionDTO.getCustomerId());
        if (!isAccountFound) {
            //Checking manager is existing user or new user
            long mangerId = pluginAdoptionDAO.getPluginUserId(pluginAdoptionDTO.getManagerEmail().trim());
            if (mangerId == 0) {
                mangerId = getPluginUserIdForNewUser(pluginAdoptionDTO.getManagerEmail().trim());
            }

            //Inserting account details
            if (mangerId != 0) {
                Calendar cal = Calendar.getInstance();
                String date = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
                accountId = pluginAdoptionDAO.saveAccountDetails(pluginAdoptionDTO.getAccountName().trim(), pluginAdoptionDTO.getCustomerId().trim(), mangerId, date);
            }

            if (accountId != 0) {
                List<Long> teamIds = new ArrayList<>();
                //Checking team member is existing user or new user
                List<PluginAdoptionTeamDTO> pluginAdoptionTeamList = pluginAdoptionDTO.getPluginAdoptionTeamDTO();
                if (pluginAdoptionTeamList != null) {

                    /*To Remove all the empty string objects from a list*/
                    pluginAdoptionTeamList.removeIf(email -> (email.getTeamMemberEmail() == null || email.getTeamMemberEmail().isEmpty()));

                    pluginAdoptionTeamList.stream().forEach((pluginAdoptionTeamDTO) -> {
                        String teamMemberMailId = pluginAdoptionTeamDTO.getTeamMemberEmail().trim();
                        if (teamMemberMailId != null) {
                            long teamId = pluginAdoptionDAO.getPluginUserId(pluginAdoptionTeamDTO.getTeamMemberEmail().trim());
                            if (teamId == 0) {
                                teamId = getPluginUserIdForNewUser(pluginAdoptionTeamDTO.getTeamMemberEmail().trim());
                            }
                            teamIds.add(teamId);
                        }
                    });
                    //Inserting account team member details
                    if (teamIds.size() > 0) {
                        pluginAdoptionDAO.insertAccountTeamDetails(accountId, teamIds);
                    }
                }
            }
            message = "Account added successfully.";
        } else {
            message = "This account is already added.";
        }

        return message;
    }

    public String deleteAccount(long[] selectedAccountd) {
        int deletedCount = pluginAdoptionDAO.deleteAccountDetails(selectedAccountd);
        if (deletedCount > 0) {
            message = "Account(s) deleted successfully.";
        } else {
            message = "Sorry! Account(s) not deleted. Please try again.";
        }
        return message;
    }

    public String updateAccount(PluginAdoptionDTO updatePluginAdoptionDTO, Map<Long, PluginAdoptionDTO> allAccounts) {

        //Fetching account Id
        long accountId = updatePluginAdoptionDTO.getAccountId();
        //Getting all account details for specific updating account
        PluginAdoptionDTO oldPluginAdoptionDTO = allAccounts.get(accountId);

        if (updatePluginAdoptionDTO.equals(oldPluginAdoptionDTO)) {
            return updatingTeamMembersData(updatePluginAdoptionDTO, oldPluginAdoptionDTO, accountId);
        } else {
            //Updating account details
            String updatingCusomerId = String.join("", updatePluginAdoptionDTO.getCustomerId().split("-"));
            String oldCusomerId = String.join("", oldPluginAdoptionDTO.getCustomerId().split("-"));
            if (!oldCusomerId.equals(updatingCusomerId)) {
                //Checking account is already added or not
                boolean isAccountFound = pluginAdoptionDAO.verifyingAccount(updatingCusomerId);
                if (isAccountFound) {
                    return "The account id is mapped with another account. Please check your account Id.";
                }
            }
            String updatingMangerEmail = updatePluginAdoptionDTO.getManagerEmail().trim();
            //Checking manager is existing user or new user
            long mangerId = pluginAdoptionDAO.getPluginUserId(updatingMangerEmail);
            if (mangerId == 0) {
                mangerId = getPluginUserIdForNewUser(updatingMangerEmail);
            }
            updatingTeamMembersData(updatePluginAdoptionDTO, oldPluginAdoptionDTO, accountId);
            boolean updateAccInfo = pluginAdoptionDAO.updateAccountDetails(updatePluginAdoptionDTO.getAccountName().trim(), updatingCusomerId.trim(),
                    mangerId, updatePluginAdoptionDTO.getAccountId());

            if (updateAccInfo) {
                return "Account details updated successfully.";
            } else {
                return "Account details not updated, Please try again.";
            }

        }
    }

    private String updatingTeamMembersData(PluginAdoptionDTO updatePluginAdoptionDTO,
            PluginAdoptionDTO oldPluginAdoptionDTO, long accountId) {
        String msg = "You did not make any changes. Please try again once.";
        List<PluginAdoptionTeamDTO> updatePluginAdoptionTeam = updatePluginAdoptionDTO.getPluginAdoptionTeamDTO();
        List<PluginAdoptionTeamDTO> oldPluginAdoptionTeam = oldPluginAdoptionDTO.getPluginAdoptionTeamDTO();

        updatePluginAdoptionTeam.removeIf(email -> (email.getTeamMemberEmail() == null || email.getTeamMemberEmail().isEmpty()));

        //Deleting team members from account
        Set<String> updatedTeamMembers = updatePluginAdoptionTeam.stream()
                .map(PluginAdoptionTeamDTO::getTeamMemberEmail)
                .collect(Collectors.toSet());

        List<PluginAdoptionTeamDTO> deletedTeamMembers = oldPluginAdoptionTeam.stream()
                .filter(e -> !updatedTeamMembers.contains(e.getTeamMemberEmail().trim()))
                .collect(Collectors.toList());

        if (deletedTeamMembers.size() > 0) {
            List<Long> removedTeamMemberIds = deletedTeamMembers.stream()
                    .map(p -> p.getTeamMemberId())
                    .collect(Collectors.toList());
            int removedTeamMemberscount = pluginAdoptionDAO.deleteTeamMembers(removedTeamMemberIds, accountId);
            if (removedTeamMemberscount > 0) {
                msg = "Account details updated successfully.";
            } else {
                msg = "Account details not updated, Please try again.";
            }
        }

        //Adding new team memebrs to account
        Set<String> oldTeamMembers = oldPluginAdoptionTeam.stream()
                .map(PluginAdoptionTeamDTO::getTeamMemberEmail)
                .collect(Collectors.toSet());

        List<PluginAdoptionTeamDTO> addingTeamMembers = updatePluginAdoptionTeam.stream()
                .filter(e -> !oldTeamMembers.contains(e.getTeamMemberEmail().trim()))
                .collect(Collectors.toList());

        if (addingTeamMembers.size() > 0) {
            List<Long> teamIds = new ArrayList<>();
            for (PluginAdoptionTeamDTO pluginAdoptionTeamDTO : addingTeamMembers) {
                String teamMemberEmail = pluginAdoptionTeamDTO.getTeamMemberEmail().trim();
                if (teamMemberEmail != null && !teamMemberEmail.equals("")) {
                    long teamId = pluginAdoptionDAO.getPluginUserId(teamMemberEmail);
                    if (teamId == 0) {
                        teamId = getPluginUserIdForNewUser(teamMemberEmail);
                    }
                    teamIds.add(teamId);
                }
            }
            //Inserting account team member details
            if (teamIds.size() > 0) {
                int savedNewTeamMembersCount = pluginAdoptionDAO.insertAccountTeamDetails(accountId, teamIds);
                if (savedNewTeamMembersCount > 0) {
                    msg = "Account details updated successfully.";
                } else {
                    msg = "Account details not updated, Please try again.";
                }
            }
        }
        return msg;
    }

    private long getPluginUserIdForNewUser(String email) {
        String name = email.split("@")[0];
        String password = Common.generatePassword();
        Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        long pluginUserId = pluginAdoptionDAO.insertingNewUser(name, email, password, createdTime, 5);
        String subject = EmailBodyCreator.subjectOnLxrPluginForSignUp;
        String body = EmailBodyCreator.bodyOnLxrPluginForSignUp(name, email, password);
        SendMail.sendMail(supportMail, email, subject, body);
        return pluginUserId;
    }

    public Map<Long, PluginAdoptionDTO> getAccounts() {
        return pluginAdoptionDAO.fetchingAllAccounts();
    }

    public String doActiveOrInactive(long accountId, boolean state) {
        int accStatusCount = pluginAdoptionDAO.changeAccountStatus(accountId, state);
        if (accStatusCount > 0) {
            if (state) {
                message = "Account status has been changed as Active.";
            } else {
                message = "Account status has been changed as InActive.";
            }
        } else {
            message = "Sorry! Account status not changed";
        }
        return message;
    }

    public String generatePluginAdoptionConsolidateReoprt(String downloadPeriod, String startDate, String endDate) {
        Calendar calendar = null;
        Date firstDateOfPreviousMonth = null;
        Date lastDateOfPreviousMonth = null;
        SimpleDateFormat dateParserFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (downloadPeriod.equals("lastMonth")) {
            calendar = Calendar.getInstance();
            /*Selecting previous month.*/
            calendar.add(Calendar.MONTH, -1);
            /*Selecting previous month first date.*/
            calendar.set(Calendar.DATE, 1);
            firstDateOfPreviousMonth = calendar.getTime();
            startDate = dateParserFormat.format(firstDateOfPreviousMonth);

            calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DATE, 1);
            /* set actual maximum date of previous month*/
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            lastDateOfPreviousMonth = calendar.getTime();
            endDate = dateParserFormat.format(lastDateOfPreviousMonth);

        } else if (downloadPeriod.equals("thisMonth")) {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            lastDateOfPreviousMonth = calendar.getTime();
            endDate = dateParserFormat.format(lastDateOfPreviousMonth);

            calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            firstDateOfPreviousMonth = calendar.getTime();
            startDate = dateParserFormat.format(firstDateOfPreviousMonth);
        }
        SqlRowSet sqlRowSet = pluginAdoptionDAO.getPluginReportRowSet(startDate, endDate);
        String pluginReportPath = getPluginAdoptionReportExcellFormat(sqlRowSet, downloadFolder, startDate, endDate, "LXRPlugin_Adoption_Report");
        return pluginReportPath;
    }

    public String getPluginAdoptionReportExcellFormat(SqlRowSet sqlRowSet, String filePath,
            String startDate, String endDate, String repName) {
        try {
            int colCount = 0;
            int rowCount = -1;

            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet(repName);
            HSSFFont font = workBook.createFont();
            HSSFCellStyle heaCelSty;

            CreationHelper createHelper = workBook.getCreationHelper();

            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            int colWidth[] = new int[]{35, 35, 20, 20, 20, 20, 20, 20, 20, 20, 20};

// Heading Cell Style
            font = workBook.createFont();
            font.setBold(true);
            heaCelSty = workBook.createCellStyle();
            heaCelSty.setFont(font);

// Column Heading Cell Style
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle = workBook.createCellStyle();
            columnHeaderStyle.setFont(font);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            columnHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            columnHeaderStyle.setBorderBottom(BorderStyle.THIN);
            columnHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderRight(BorderStyle.THIN);
            columnHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderLeft(BorderStyle.THIN);
            columnHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            columnHeaderStyle.setBorderTop(BorderStyle.THIN);
            columnHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

//            sheet.createFreezePane(0, 4);
            CellStyle dateStyle = workBook.createCellStyle();

            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy HH:mm:ss"));

//Report Headings
            rows = sheet.createRow(0);
            cell = rows.createCell(0);
            cell.setCellValue("Report Name");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(repName);
            cell.setCellStyle(heaCelSty);

            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            cell.setCellValue("Date Range");
            cell.setCellStyle(heaCelSty);
            cell = rows.createCell(1);
            cell.setCellValue(startDate + " to " + endDate);
            cell.setCellStyle(heaCelSty);
            rowCount = 2;

//Report Columns
            rows = sheet.createRow(rowCount + 1);
            rows.setHeight((short) 500);
            cell = rows.createCell(0);
            cell.setCellValue("Account Name");
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(1);
            cell.setCellValue("Manager Email");
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(2);
            cell.setCellValue("Total Number of Analysis Reports Generated");
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(3);
            cell.setCellValue("Total % of Analysis Reports Generated");
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(4);
            cell.setCellValue("Total Number of Reports Generated");
            cell.setCellStyle(columnHeaderStyle);
            cell = rows.createCell(5);
            cell.setCellValue("Total Number of times the Reports Generated");
            cell.setCellStyle(columnHeaderStyle);

            HSSFFont urlHeaderFont = workBook.createFont();
            urlHeaderFont.setFontHeightInPoints((short) 10);
            urlHeaderFont.setFontName(HSSFFont.FONT_ARIAL);

            HSSFCellStyle noDataStyle = workBook.createCellStyle();
            noDataStyle.setFont(urlHeaderFont);
            noDataStyle.setWrapText(true);
            noDataStyle.setAlignment(HorizontalAlignment.CENTER);
            noDataStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            noDataStyle.setBorderBottom(BorderStyle.THIN);
            noDataStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            noDataStyle.setBorderRight(BorderStyle.THIN);
            noDataStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            noDataStyle.setBorderLeft(BorderStyle.THIN);
            noDataStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle leftColoumnStyle = workBook.createCellStyle();
            leftColoumnStyle.setWrapText(true);
            leftColoumnStyle.setAlignment(HorizontalAlignment.CENTER);
            leftColoumnStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            leftColoumnStyle.setBorderLeft(BorderStyle.THIN);
            leftColoumnStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle rightColoumnStyle = workBook.createCellStyle();
            rightColoumnStyle.setWrapText(true);
            rightColoumnStyle.setAlignment(HorizontalAlignment.RIGHT);
            rightColoumnStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            rightColoumnStyle.setBorderRight(BorderStyle.THIN);
            rightColoumnStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle columnDataStyle = null;
            columnDataStyle = workBook.createCellStyle();
            columnDataStyle.setWrapText(true);
            columnDataStyle.setAlignment(HorizontalAlignment.RIGHT);

            HSSFCellStyle leftAlignStyle = workBook.createCellStyle();
            leftAlignStyle.setWrapText(true);
            leftAlignStyle.setAlignment(HorizontalAlignment.LEFT);

            HSSFCellStyle lastRowLeftStyle = workBook.createCellStyle();
            lastRowLeftStyle.setWrapText(true);
            lastRowLeftStyle.setAlignment(HorizontalAlignment.LEFT);
            lastRowLeftStyle.setBorderBottom(BorderStyle.THIN);
            lastRowLeftStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle lastRowRightStyle = workBook.createCellStyle();
            lastRowRightStyle.setWrapText(true);
            lastRowRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            lastRowRightStyle.setBorderBottom(BorderStyle.THIN);
            lastRowRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            lastRowRightStyle.setBorderRight(BorderStyle.THIN);
            lastRowRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

            HSSFCellStyle lastRowMiddleColoumStyle = workBook.createCellStyle();
            lastRowMiddleColoumStyle.setWrapText(true);
            lastRowMiddleColoumStyle.setAlignment(HorizontalAlignment.RIGHT);
            lastRowMiddleColoumStyle.setBorderBottom(BorderStyle.THIN);
            lastRowMiddleColoumStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());

            rowCount = 4;
            
            SqlRowSetMetaData sqlRowSetMetaData = null;
            boolean isDataPresent = true;
            if (sqlRowSet != null) {
                sqlRowSetMetaData = sqlRowSet.getMetaData();
                colCount = sqlRowSetMetaData.getColumnCount();
//Report Data
                boolean isContainData = sqlRowSet.isBeforeFirst();

                if (isContainData) {
                    while (sqlRowSet.next()) {
                        rows = sheet.createRow(rowCount);
                        cell = rows.createCell(0);
                        cell.setCellValue(sqlRowSet.getString("account_name"));

                        cell = rows.createCell(1);
                        cell.setCellValue(sqlRowSet.getString("manager_email"));
                        cell.setCellStyle(leftAlignStyle);

                        cell = rows.createCell(2);
                        cell.setCellValue(sqlRowSet.getLong("analysis_reports_usage_count"));
                        cell.setCellStyle(columnDataStyle);
                        
                        cell = rows.createCell(3);
                        cell.setCellValue(((int) ((sqlRowSet.getLong("analysis_reports_usage_count") * 100) / 20)) + "%");
                        cell.setCellStyle(columnDataStyle);

                        cell = rows.createCell(4);
                        cell.setCellValue(sqlRowSet.getLong("total_number_of_reports_generated"));
                        cell.setCellStyle(columnDataStyle);

                        cell = rows.createCell(5);
                        cell.setCellValue(sqlRowSet.getLong("total_number_of_times_reports_generated"));
                        cell.setCellStyle(rightColoumnStyle);

                        rowCount++;
                    }
                } else {
                    isDataPresent = false;
                }

            } else {
                isDataPresent = false;
            }

            if (!isDataPresent) {
                colCount = 5;
                rows = sheet.createRow(rowCount);
                cell = rows.createCell(0);
                rows.setHeight((short) 600);
                sheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 5));
                mergeHSSFCell(rows, noDataStyle, 0, 5);
                cell.setCellValue("No Data Available.");
                cell.setCellStyle(noDataStyle);
                rowCount++;
            } else {
                rows = sheet.getRow(--rowCount);
                /*Applying Styles when data existed*/
                for (int i = 0; i <= 5; i++) {
                    cell = rows.getCell(i);
                    switch (i) {
                        case 0:
                            cell.setCellStyle(lastRowLeftStyle);
                            break;
                        case 1:
                            cell.setCellStyle(lastRowLeftStyle);
                            break;
                        case 2:
                            cell.setCellStyle(lastRowMiddleColoumStyle);
                            break;
                        case 3:
                            cell.setCellStyle(lastRowMiddleColoumStyle);
                            break;
                        case 4:
                            cell.setCellStyle(lastRowMiddleColoumStyle);
                            break;
                        case 5:
                            cell.setCellStyle(lastRowRightStyle);
                            break;
                    }
                }
            }
            colCount = 6;
            for (int i = 0; i <= colCount; i++) {
                sheet.setColumnWidth(i, (int) (441.3793d + 256d * (colWidth[i] - 1d)));
            }

            String reptName = Common.removeSpaces(repName);
            String fileName = "";
            fileName = filePath + Common.createFileName(reptName) + ".xls";
            logger.info(" Report generated in .xls format");
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            workBook.write(stream);
            stream.flush();
            stream.close();
            file = null;
            return fileName;
        } catch (Exception e) {
            logger.error("Exception in PluginAdoptionReportExcellFormat", e);
        }
        return null;
    }

    public Map<Long, PluginAdoptionDTO> fethingWeeklyAccountUsageData() {
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String endDate = sdFormat.format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, -6);
        String startDate = sdFormat.format(cal.getTime());
        return pluginAdoptionDAO.fetchingWeeklyUsageStats(startDate, endDate);
    }

    /* Merges two columns of specified range for row.*/
    public void mergeHSSFCell(HSSFRow rows, HSSFCellStyle style, int startCellNo, int endCellNo) {
        HSSFCell cell;
        for (int i = startCellNo; i <= endCellNo; i++) {
            cell = rows.createCell(i);
            cell.setCellStyle(style);
        }
    }
}
