/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.plugin;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vemanna
 */
@Repository
public class PluginAdoptionDAO {

    private static final Logger logger = Logger.getLogger(PluginAdoptionDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    String query = "";

    public long getPluginUserId(String email) {
        long pluginUserId = 0;
        query = "select lxrplugin_user_id from lxrplugin_users where email = ?";
        try {
            pluginUserId = this.jdbcTemplate.queryForObject(query, new Object[]{email}, Long.class);
        } catch (IncorrectResultSizeDataAccessException e) {
            logger.error("IncorrectResultSizeDataAccessException on fetching plugin user id:", e);
        }
        return pluginUserId;
    }

    public int deleteAccountDetails(final long[] accounts) {
        query = "update lxrplugin_accounts set is_deleted = ? where account_id= ? ";
        int[] count = this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setBoolean(1, true);
                statement.setLong(2, accounts[i]);
            }

            @Override
            public int getBatchSize() {
                return accounts.length;
            }
        });
        return count.length;
    }

    public int insertAccountTeamDetails(long accountId, List<Long> teamIds) {
        query = "insert into lxrplugin_accounts_team(account_id,team_member_id) values(?,?)";
        int count[] = this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setLong(1, accountId);
                ps.setLong(2, teamIds.get(i));
            }

            @Override
            public int getBatchSize() {
                return teamIds.size();
            }
        });
        return count.length;
    }

    public long saveAccountDetails(String accountName, String customerId, long mangerId, String date) {
        query = "insert into lxrplugin_accounts (account_name, customer_id, manager_id, created_date) values(?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.jdbcTemplate.update((Connection con) -> {
            PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"account_id"});
            statement.setString(1, accountName);
            statement.setString(2, customerId);
            statement.setLong(3, mangerId);
            statement.setString(4, date);
            return statement;
        }, keyHolder);
        return (Long) keyHolder.getKey();
    }

    public long insertingNewUser(String name, String email, String password, Timestamp creaTedTime, int pricingPlanId) {
        query = "insert into lxrplugin_users (name, email, password, permission, created_date, "
                + "pricing_plan_id, country, region_name, is_local) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {

                this.jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"lxrplugin_user_id"});

                    try {
                        statement.setBytes(1, name.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(1, name);
                        logger.error("Exception on inserting LXRPlugin user name:" + e);
                    }

                    try {
                        statement.setBytes(2, email.toLowerCase().getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(2, email.toLowerCase());
                        logger.error("Exception on inserting LXRPlugin email: " + e);
                    }

                    try {
                        statement.setBytes(3, password.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(3, password);
                        logger.error("Exception on inserting LXRPlugin password:" + e);

                    }

                    statement.setBoolean(4, true);
                    statement.setTimestamp(5, creaTedTime);
                    statement.setInt(6, pricingPlanId);
                    statement.setString(7, "India");
                    statement.setString(8, "Telangana");
                    statement.setInt(9, 1);

                    return statement;
                }, keyHolder);
            }
        } catch (Exception ex) {
            logger.error("Exception on inserting LXRPlugin user details: " + ex);
        }

        return (Long) keyHolder.getKey();
    }

    public Map<Long, PluginAdoptionDTO> fetchingAllAccounts() {

        Map<Long, PluginAdoptionDTO> pluginAdoptionAccountMap = null;

        query = "select ac.*,u1.lxrplugin_user_id as team_member_id, u1.email as team_member_email from "
                + "(select a.account_id, a.account_name, a.customer_id,a.active, u.email as manager_email from "
                + "lxrplugin_accounts a, lxrplugin_users u where a.manager_id = u.lxrplugin_user_id and is_deleted=0)ac "
                + "join "
                + "(select team_member_id,account_id from lxrplugin_accounts_team where is_deleted = 0)t on t.account_id = ac.account_id "
                + "join "
                + "lxrplugin_users u1 on t.team_member_id = u1.lxrplugin_user_id"
                + " union "
                + "select a.account_id, a.account_name, a.customer_id,a.active, u.email as manager_email,0 as team_member_id, "
                + "null as team_member_email from lxrplugin_accounts a, lxrplugin_users u where "
                + "a.manager_id = u.lxrplugin_user_id and is_deleted=0 and a.account_id not in "
                + "(select account_id from lxrplugin_accounts_team where is_deleted = 0)";

        SqlRowSet rs = this.jdbcTemplate.queryForRowSet(query);

        boolean isContainsData = rs.isBeforeFirst();
        if (isContainsData) {
            pluginAdoptionAccountMap = new HashMap<>();
            while (rs.next()) {
                long accountId = rs.getLong("account_id");
                if (pluginAdoptionAccountMap.containsKey(accountId)) {

                    PluginAdoptionTeamDTO pluginAdoptionTeamDTO = new PluginAdoptionTeamDTO();
                    pluginAdoptionTeamDTO.setTeamMemberId(rs.getLong("team_member_id"));
                    pluginAdoptionTeamDTO.setTeamMemberEmail(rs.getString("team_member_email"));

                    List<PluginAdoptionTeamDTO> pluginAdoptionTeamList = pluginAdoptionAccountMap.get(accountId).getPluginAdoptionTeamDTO();
                    pluginAdoptionTeamList.add(pluginAdoptionTeamDTO);

                } else {
                    PluginAdoptionDTO pluginAdoptionDTO = new PluginAdoptionDTO();
                    pluginAdoptionDTO.setAccountId(accountId);
                    pluginAdoptionDTO.setAccountName(rs.getString("account_name"));
                    pluginAdoptionDTO.setCustomerId(rs.getString("customer_id"));
                    pluginAdoptionDTO.setManagerEmail(rs.getString("manager_email"));
                    pluginAdoptionDTO.setAccountStatus(rs.getBoolean("active"));

                    if (rs.getString("team_member_email") != null) {
                        PluginAdoptionTeamDTO pluginAdoptionTeamDTO = new PluginAdoptionTeamDTO();
                        pluginAdoptionTeamDTO.setTeamMemberId(rs.getLong("team_member_id"));
                        pluginAdoptionTeamDTO.setTeamMemberEmail(rs.getString("team_member_email"));
                        List<PluginAdoptionTeamDTO> pluginAdoptionTeamList = new ArrayList<>();
                        pluginAdoptionTeamList.add(pluginAdoptionTeamDTO);
                        pluginAdoptionDTO.setPluginAdoptionTeamDTO(pluginAdoptionTeamList);
                    }

                    pluginAdoptionAccountMap.put(accountId, pluginAdoptionDTO);
                }
            }
        }

        return pluginAdoptionAccountMap;
    }

    public boolean verifyingAccount(String customerId) {
        int count = 0;
        query = "select count(*) from lxrplugin_accounts where customer_id = ? and is_deleted = 0";
        try {
            count = this.jdbcTemplate.queryForObject(query, new Object[]{customerId}, Integer.class);
        } catch (IncorrectResultSizeDataAccessException e) {
            logger.error("IncorrectResultSizeDataAccessException on fetching plugin customer/account Id:", e);
        }
        return count > 0;
    }

    public int changeAccountStatus(long accountId, boolean state) {
        query = "update lxrplugin_accounts set active= ? where account_id= ?";
        int count = this.jdbcTemplate.update(query, state, accountId);
        return count;
    }

    public int deleteTeamMembers(List<Long> ids, long accountId) {
        query = "update lxrplugin_accounts_team set is_deleted= ? where team_member_id= ? and account_id = ?";

        int[] count = this.jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement statement, int i) throws SQLException {
                statement.setBoolean(1, true);
                statement.setLong(2, ids.get(i));
                statement.setLong(3, accountId);
            }
            @Override
            public int getBatchSize() {
                return ids.size();
            }
        });
        return count.length;
    }

    public boolean updateAccountDetails(String accountName, String cusomerId, long mangerId, long accountId) {
        query = "update lxrplugin_accounts set account_name= ?, customer_id= ?, manager_id= ? where account_id= ?";
        int count = this.jdbcTemplate.update(query, accountName, cusomerId, mangerId, accountId);
        return count > 0;
    }

    public SqlRowSet getPluginReportRowSet(String startDate, String endDate) {
        query = "select k.account_id, k.account_name, k.manager_email,  "
                + "SUM(analysis_reports_usage_count) as analysis_reports_usage_count, "
                + "SUM(total_number_of_reports_generated) as total_number_of_reports_generated, "
                + "SUM(total_number_of_times_reports_generated) as total_number_of_times_reports_generated "
                + "from  (select a.account_id,a.account_name,u.email as manager_email, "
                + "SUM(COALESCE(ub1.total_number_of_reports_generated,0)) as total_number_of_reports_generated, "
                + "SUM(COALESCE(ub1.total_number_of_times_reports_generated,0)) as total_number_of_times_reports_generated, "
                + "SUM(COALESCE(ub1.analysis_reports_usage_count,0)) as analysis_reports_usage_count "
                + "from (select ac.account_id, ac.account_name, ac.customer_id, ac.manager_id from "
                + "lxrplugin_accounts ac where ac.is_deleted = 0 and ac.active=1)a left join "
                + "(select account_id,team_member_id from  lxrplugin_accounts_team "
                + "where is_deleted=0 group by team_member_id, account_id) t on ( t.account_id = a.account_id) left join "
                + "(select  ub.customer_id,ub.lxrplugin_user_id, "
                + "count(DISTINCT  ub.report_id, ub.report_category_id) as total_number_of_reports_generated, "
                + "count(ub.report_id) as total_number_of_times_reports_generated, "
                + "count(DISTINCT (case when ub.report_category_id in(2,8) then ub.report_id end )) as analysis_reports_usage_count "
                + "from lxrplugin_user_behaviour ub where date_format( ub.report_generated_time, '%y-%m-%d') "
                + "between date_format('" + startDate + "', '%y-%m-%d') and date_format('" + endDate + "', '%y-%m-%d') "
                + "group by  ub.customer_id,  ub.lxrplugin_user_id) ub1 on  "
                + "(ub1.customer_id = a.customer_id  and ub1.lxrplugin_user_id=t.team_member_id) left join "
                + "lxrplugin_users u on u.lxrplugin_user_id = a.manager_id group by a.account_id union "
                + "select a.account_id,a.account_name,u.email as manager_email, "
                + "SUM(COALESCE(ub1.total_number_of_reports_generated,0)) as total_number_of_reports_generated, "
                + "SUM(COALESCE(ub1.total_number_of_times_reports_generated,0)) as total_number_of_times_reports_generated, "
                + "SUM(COALESCE(ub1.analysis_reports_usage_count,0)) as analysis_reports_usage_count "
                + "from (select ac.account_id, ac.account_name, ac.customer_id, ac.manager_id from "
                + "lxrplugin_accounts ac where ac.is_deleted = 0 and ac.active=1)a left join "
                + "(select account_id, manager_id from lxrplugin_accounts where is_deleted=0 and active=1) ta "
                + "on (ta.account_id = a.account_id ) left join "
                + "(select  ub.customer_id,ub.lxrplugin_user_id, "
                + "count(DISTINCT  ub.report_id, ub.report_category_id) as total_number_of_reports_generated, "
                + "count(ub.report_id) as total_number_of_times_reports_generated, "
                + "count(DISTINCT (case when ub.report_category_id in(2,8) then ub.report_id end )) as analysis_reports_usage_count  "
                + "from lxrplugin_user_behaviour ub where date_format( ub.report_generated_time, '%y-%m-%d') "
                + "between date_format('" + startDate + "', '%y-%m-%d') and date_format('" + endDate + "', '%y-%m-%d') "
                + "group by  ub.customer_id,  ub.lxrplugin_user_id) ub1 on (ub1.customer_id = a.customer_id  and ub1.lxrplugin_user_id=ta.manager_id) left join "
                + "lxrplugin_users u on u.lxrplugin_user_id = a.manager_id group by a.account_id )k group by k.account_id";

        try {
            SqlRowSet sqlRowSet = this.jdbcTemplate.queryForRowSet(query);
            return sqlRowSet;
        } catch (Exception e) {
            logger.error("Exception in preparing Plugin Report RowSet: " + e);
        }
        return null;
    }

    public Map<Long, PluginAdoptionDTO> fetchingWeeklyUsageStats(String startDate, String endDate) {

        Map<Long, PluginAdoptionDTO> pluginAdoptionAccountMap = null;

        query = "select a.account_id, a.account_name, a.customer_id,COALESCE(ub.analysis_reports_count,0) analysis_reports_count, "
                + "u.email as manager_email, null as team_member_email from "
                + "(select ac.account_id, ac.account_name, ac.customer_id, ac.manager_id from "
                + "lxrplugin_accounts ac where ac.is_deleted = 0 and ac.active=1) a left join "
                + "(select manager_id,account_id from lxrplugin_accounts where is_deleted=0 and active=1 group by account_id) ta "
                + "on (ta.account_id =a.account_id) left join "
                + "(select lxrplugin_user_id,customer_id,count(DISTINCT  report_id) as analysis_reports_count from "
                + "lxrplugin_user_behaviour where report_category_id in(2,8) and date_format( report_generated_time, '%y-%m-%d') "
                + "between date_format('" + startDate + "', '%y-%m-%d') and date_format('" + endDate + " ', '%y-%m-%d') "
                + "group by  customer_id,  lxrplugin_user_id) ub on "
                + "(ub.customer_id=a.customer_id and ub.lxrplugin_user_id = ta.manager_id) left join "
                + "lxrplugin_users u on u.lxrplugin_user_id = a.manager_id union "
                + "select a.account_id, a.account_name, a.customer_id,COALESCE(ub.analysis_reports_count,0) analysis_reports_count, "
                + "u.email as manager_email,u1.email as team_member_email from "
                + "(select ac.account_id, ac.account_name, ac.customer_id, ac.manager_id from "
                + "lxrplugin_accounts ac where ac.is_deleted = 0 and ac.active=1) a join "
                + "(select team_member_id,account_id from lxrplugin_accounts_team where is_deleted=0) tm "
                + "on (tm.account_id =a.account_id) left join "
                + "(select lxrplugin_user_id,customer_id,count(DISTINCT  report_id) as analysis_reports_count from "
                + "lxrplugin_user_behaviour where report_category_id in(2,8) and date_format( report_generated_time, '%y-%m-%d') "
                + "between date_format(' " + startDate + "', '%y-%m-%d') and date_format('" + endDate + " ', '%y-%m-%d') "
                + "group by  customer_id,  lxrplugin_user_id) ub on "
                + "(ub.customer_id=a.customer_id and ub.lxrplugin_user_id = tm.team_member_id) left join "
                + "lxrplugin_users u on u.lxrplugin_user_id = a.manager_id left join "
                + "lxrplugin_users u1 on u1.lxrplugin_user_id = tm.team_member_id";

        SqlRowSet rs = this.jdbcTemplate.queryForRowSet(query);

        boolean isContainsData = rs.isBeforeFirst();
        if (isContainsData) {
            pluginAdoptionAccountMap = new HashMap<>();
            while (rs.next()) {
                long accountId = rs.getLong("account_id");
                if (pluginAdoptionAccountMap.containsKey(accountId)) {

                    PluginAdoptionDTO pluginAdoptionDTO = pluginAdoptionAccountMap.get(accountId);
                    int reportUsageCount = pluginAdoptionAccountMap.get(accountId).getReportsUsageCount() + rs.getInt("analysis_reports_count");
                    pluginAdoptionDTO.setReportsUsageCount(reportUsageCount);

                    String teamMemberEmail = rs.getString("team_member_email");
                    if (teamMemberEmail != null && !"".equals(teamMemberEmail)) {
                        PluginAdoptionTeamDTO pluginAdoptionTeamDTO = new PluginAdoptionTeamDTO();
                        pluginAdoptionTeamDTO.setTeamMemberEmail(teamMemberEmail);
                        List<PluginAdoptionTeamDTO> pluginAdoptionTeamList;
                        if (pluginAdoptionDTO.getPluginAdoptionTeamDTO() != null) {
                            pluginAdoptionTeamList = pluginAdoptionDTO.getPluginAdoptionTeamDTO();
                            pluginAdoptionTeamList.add(pluginAdoptionTeamDTO);
                        } else {
                            pluginAdoptionTeamList = new ArrayList<>();
                            pluginAdoptionTeamList.add(pluginAdoptionTeamDTO);
                            pluginAdoptionDTO.setPluginAdoptionTeamDTO(pluginAdoptionTeamList);
                        }

                    }

                } else {
                    PluginAdoptionDTO pluginAdoptionDTO = new PluginAdoptionDTO();
                    pluginAdoptionDTO.setAccountId(accountId);
                    pluginAdoptionDTO.setAccountName(rs.getString("account_name"));
                    pluginAdoptionDTO.setManagerEmail(rs.getString("manager_email"));
                    pluginAdoptionDTO.setReportsUsageCount(rs.getInt("analysis_reports_count"));

                    String teamMemberEmail = rs.getString("team_member_email");
                    if (teamMemberEmail != null && !"".equals(teamMemberEmail)) {
                        PluginAdoptionTeamDTO pluginAdoptionTeamDTO = new PluginAdoptionTeamDTO();
                        pluginAdoptionTeamDTO.setTeamMemberEmail(rs.getString("team_member_email"));
                        List<PluginAdoptionTeamDTO> pluginAdoptionTeamList = new ArrayList<>();
                        pluginAdoptionTeamList.add(pluginAdoptionTeamDTO);
                        pluginAdoptionDTO.setPluginAdoptionTeamDTO(pluginAdoptionTeamList);
                    }
                    pluginAdoptionAccountMap.put(accountId, pluginAdoptionDTO);
                }
            }
        }

        return pluginAdoptionAccountMap;
    }
}
