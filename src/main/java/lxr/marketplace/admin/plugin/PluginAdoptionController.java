/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
public class PluginAdoptionController {

    private static Logger logger = Logger.getLogger(PluginAdoptionController.class);

    @Autowired
    PluginAdoptionService pluginAdoptionService;

    @RequestMapping(value = "/plugin-adoption.html", method = RequestMethod.GET)
    public String getInit(Model model) {
        PluginAdoptionDTO pluginAdoptionDTO = new PluginAdoptionDTO();
        model.addAttribute("pluginAdoptionDTO", pluginAdoptionDTO);
        return "view/admin/pluginAdoption/pluginAdoption";
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=getAccounts", method = RequestMethod.GET)
    public @ResponseBody
    Map<Long, PluginAdoptionDTO> getAllAcounts(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        Map<Long, PluginAdoptionDTO> accounts = pluginAdoptionService.getAccounts();
        session.setAttribute("accounts", accounts);
        return accounts;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=save-account", method = RequestMethod.POST)
    public @ResponseBody
    String addAccount(HttpSession session, HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("pluginAdoptionDTO") PluginAdoptionDTO pluginAdoption) {
        String message = "";
        message = pluginAdoptionService.saveAccount(pluginAdoption);
        return message;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=delete", method = RequestMethod.POST)
    public @ResponseBody
    String deleteAccount(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam("accountsDeleteArray") int[] accountsDeleteArray) {
        long[] longArray = Arrays.stream(accountsDeleteArray).asLongStream().toArray();
        String message = pluginAdoptionService.deleteAccount(longArray);
        return message;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=update", method = RequestMethod.POST)
    public @ResponseBody
    String updateAccount(HttpSession session, HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("pluginAdoptionDTO") PluginAdoptionDTO updatePluginAdoptionDTO) {
        Map<Long, PluginAdoptionDTO> allAccounts = (Map<Long, PluginAdoptionDTO>) session.getAttribute("accounts");
        String message = pluginAdoptionService.updateAccount(updatePluginAdoptionDTO, allAccounts);
        return message;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=active-inactive", method = RequestMethod.GET)
    public @ResponseBody
    String changeAccStatus(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("acccountId") long accountId, @RequestParam("acccountStatus") boolean state) {
        String message = pluginAdoptionService.doActiveOrInactive(accountId, state);
        return message;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = "action=edit", method = RequestMethod.GET)
    public @ResponseBody
    PluginAdoptionDTO eidtAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestParam("acccountId") long accountId) {
        Map<Long, PluginAdoptionDTO> accounts = (Map<Long, PluginAdoptionDTO>) session.getAttribute("accounts");
        PluginAdoptionDTO pluginAdoptionDTO = accounts.get(accountId);
        return pluginAdoptionDTO;
    }

    @RequestMapping(value = "/plugin-adoption.html", params = {"action=download"}, method = RequestMethod.POST)
    public String downloadPluginReports(HttpSession session, Model model, HttpServletResponse response,
            @RequestParam("pluginReportPeriod") String downloadType,
            @RequestParam("startPluginDate") String startDate,
            @RequestParam("endPluginDate") String endDate) {
        String fileName = null;
        fileName = pluginAdoptionService.generatePluginAdoptionConsolidateReoprt(downloadType, startDate, endDate);
        boolean reportGenerated = false;
        try {
            if (fileName != null) {
                File file = new File(fileName);
                reportGenerated = true;
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
                FileInputStream fis = new FileInputStream(file);
                FileCopyUtils.copy(fis, response.getOutputStream());
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException: ", e);
        } catch (IOException e) {
            logger.error("IOException: ", e);
        } catch (Exception e) {
            logger.error("Exception: ", e);
        }
        if (!reportGenerated) {
            PluginAdoptionDTO pluginAdoptionDTO = new PluginAdoptionDTO();
            model.addAttribute("pluginAdoptionDTO", pluginAdoptionDTO);
            return "view/admin/pluginAdoption/pluginAdoption";
        }
        return null;
    }

}
