/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.plugin;

/**
 *
 * @author vemanna
 */
public class PluginAdoptionTeamDTO {

    public PluginAdoptionTeamDTO() {
    }

    private long teamMemberId;
    private String teamMemberEmail;

    public long getTeamMemberId() {
        return teamMemberId;
    }

    public void setTeamMemberId(long teamMemberId) {
        this.teamMemberId = teamMemberId;
    }

    public String getTeamMemberEmail() {
        if(teamMemberEmail != null){
            return teamMemberEmail.trim();
        }
        return teamMemberEmail;
    }

    public void setTeamMemberEmail(String teamMemberEmail) {
        this.teamMemberEmail = teamMemberEmail;
    }

}
