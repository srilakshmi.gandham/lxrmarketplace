/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.admin.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

/**
 *
 * @author vemanna
 */
public class PluginAdoptionDTO {

    public List<PluginAdoptionTeamDTO> pluginAdoptionTeamDTO = LazyList.decorate(new ArrayList(), FactoryUtils.instantiateFactory(PluginAdoptionTeamDTO.class));
    private long accountId;
    public String accountName;
    public String customerId;
    public String managerEmail;
    public boolean accountStatus;
    public int reportsUsageCount;

    public boolean isAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(boolean accountStatus) {
        this.accountStatus = accountStatus;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public List<PluginAdoptionTeamDTO> getPluginAdoptionTeamDTO() {
        return pluginAdoptionTeamDTO;
    }

    public void setPluginAdoptionTeamDTO(List<PluginAdoptionTeamDTO> pluginAdoptionTeamDTO) {
        this.pluginAdoptionTeamDTO = pluginAdoptionTeamDTO;
    }

    public int getReportsUsageCount() {
        return reportsUsageCount;
    }

    public void setReportsUsageCount(int reportsUsageCount) {
        this.reportsUsageCount = reportsUsageCount;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (object instanceof PluginAdoptionDTO) {
            PluginAdoptionDTO pluginAdoptionDTO = (PluginAdoptionDTO) object;
            if (pluginAdoptionDTO.getAccountName().trim().equals(this.accountName.trim()) && pluginAdoptionDTO.getCustomerId().trim().equals(this.customerId.trim())
                    && pluginAdoptionDTO.getManagerEmail().trim().equals(this.managerEmail.trim())) {
                return true;

            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.pluginAdoptionTeamDTO);
        hash = 41 * hash + (int) (this.accountId ^ (this.accountId >>> 32));
        hash = 41 * hash + Objects.hashCode(this.accountName);
        hash = 41 * hash + Objects.hashCode(this.customerId);
        hash = 41 * hash + Objects.hashCode(this.managerEmail);
        return hash;
    }

}
