package lxr.marketplace.brokenlinkchecker;

import java.util.List;

import lxr.marketplace.crawler.LxrmUrlStats;

public class BrokenLinkCheckerResults {
    
    private static final String NO_STATUS = "Status Code Not Found";
//    private static final String NO_STATUS = "Status Code Not Found";

    private List<LxrmUrlStats> lxrmurlStatsList;
    private int workingUrlCount;
    private int notWorkingUrlCount;
    private String finalDomain;
    private int permanentRedirectingUrls;
    private int temporaryRedirectingUrls;
    private int tooManyRedirectionUrls;

    public int getPermanentRedirectingUrls() {
        return permanentRedirectingUrls;
    }

    public void setPermanentRedirectingUrls(int permanentRedirectingUrls) {
        this.permanentRedirectingUrls = permanentRedirectingUrls;
    }

    public String getFinalDomain() {
        return finalDomain;
    }

    public void setFinalDomain(String finalDomain) {
        this.finalDomain = finalDomain;
    }

    public List<LxrmUrlStats> getLxrmurlStatsList() {
        return lxrmurlStatsList;
    }

    public void setLxrmurlStatsList(List<LxrmUrlStats> lxrmurlStatsList) {
        this.lxrmurlStatsList = lxrmurlStatsList;
    }

    public int getWorkingUrlCount() {
        return workingUrlCount;
    }

    public void setWorkingUrlCount(int workingUrlCount) {
        this.workingUrlCount = workingUrlCount;
    }

    public int getNotWorkingUrlCount() {
        return notWorkingUrlCount;
    }

    public void setNotWorkingUrlCount(int notWorkingUrlCount) {
        this.notWorkingUrlCount = notWorkingUrlCount;
    }

    public int getTemporaryRedirectingUrls() {
        return temporaryRedirectingUrls;
    }

    public void setTemporaryRedirectingUrls(int temporaryRedirectingUrls) {
        this.temporaryRedirectingUrls = temporaryRedirectingUrls;
    }

    public int getTooManyRedirectionUrls() {
        return tooManyRedirectionUrls;
    }

    public void setTooManyRedirectionUrls(int tooManyRedirectionUrls) {
        this.tooManyRedirectionUrls = tooManyRedirectionUrls;
    }

}
