package lxr.marketplace.brokenlinkchecker;


import java.util.Comparator;
import java.util.Map;

public class BrokenLinkCheckerTool {

	private String domain;
	
	public BrokenLinkCheckerTool() {
	}

	public BrokenLinkCheckerTool(String domain) {
		super();
		this.domain = domain.trim();
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain.trim();
	}
	
	
}

class ValueComparator implements Comparator<Integer> {

    Map<Integer, Integer> base;

    ValueComparator(Map<Integer, Integer> base) {
        this.base = base;
    }

//    @Override
    public int compare(Integer a, Integer b) {
        if (base.get(a) >= base.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
}

class ValueComparatorLong implements Comparator<Integer> {

    Map<Integer, Long> base;

    ValueComparatorLong(Map<Integer, Long> base) {
        this.base = base;
    }

//    @Override
    public int compare(Integer a, Integer b) {
        if (base.get(a) >= base.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
	
}
