package lxr.marketplace.brokenlinkchecker;

import com.itextpdf.text.Annotation;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpSession;
import lxr.marketplace.crawler.LxrmUrlStats;
import lxr.marketplace.crawler.LxrmUrlStatsService;
import lxr.marketplace.seoninja.data.LXRSEOURLFetchThread;
import lxr.marketplace.seoninja.data.NinjaURL;
import lxr.marketplace.seoninja.data.NinjaURLCrawlerUtil;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.crawler.LxrmUrlFecthThread;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jsoup.nodes.Document;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import crawlercommons.robots.BaseRobotRules;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import lxr.marketplace.util.CommonReportGenerator;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class BrokenLinkCheckerService extends JdbcDaoSupport {

    private static Logger logger = Logger.getLogger(BrokenLinkCheckerService.class);

    private final int maxPoolSize = 100;
    private final long keepAliveTime = 600;
    private final LinkedBlockingQueue<Runnable> blockingQue = new LinkedBlockingQueue<>();
    private LxrmUrlStatsService lxrmUrlStatsService;

    public LxrmUrlStatsService getLxrmUrlStatsService() {
        return lxrmUrlStatsService;
    }

    public void setLxrmUrlStatsService(LxrmUrlStatsService lxrmUrlStatsService) {
        this.lxrmUrlStatsService = lxrmUrlStatsService;
    }

    public void fetchWebsiteData(List<LxrmUrlStats> lxrmUrlStatsList, NinjaWebStats webStats, HttpSession session, int crawlingUrlLimit) {
        int urlLimit;
        int poolSize;

        urlLimit = crawlingUrlLimit;
        poolSize = 30;
        boolean UrlCrawlingStatus2 = false;
        logger.info("Site Crawling started for " + webStats.getDomain() + " ...");
        ThreadPoolExecutor siteGraderThreadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, blockingQue);
        NinjaURL domainUrl = new NinjaURL(webStats.getDomain());
        NinjaURL redirectedDomainUrl = null;
        Document domainDoc = null;
        Set<NinjaURL> crawlingURLs = new HashSet<>(); // shared set of urls being processed
        Set<NinjaURL> urlQueue = new HashSet<>(); //shared set of urls in queue to be processed
        String statusMessage = NinjaURLCrawlerUtil.checkURL(domainUrl);
        try {
            if (statusMessage.equals("too many redirection") || statusMessage.equals("invalid")
                    || domainUrl.getRedirectedURL().equals("")) {
                logger.info("No data fetched for site grader. closing...");
                return;
            }
            LxrmUrlStats homapageStats = new LxrmUrlStats(domainUrl.getRedirectedURL());

            //for non redirected URLs also there can be redirected URL in domainurl object with same value as primary URL 
            if (!domainUrl.getUrl().equals(domainUrl.getRedirectedURL())) {
                redirectedDomainUrl = new NinjaURL(domainUrl.getRedirectedURL(), NinjaURL.PROCESSING);

                int urlStatusCode = Common.getURLStatus(domainUrl.getRedirectedURL());
                logger.info("redirectedDomainUrl: " + domainUrl.getRedirectedURL() + " Status Code: " + urlStatusCode);
                redirectedDomainUrl.setUrlStatus(urlStatusCode);
                homapageStats.setStatusCode(urlStatusCode);
                redirectedDomainUrl.setRedirectedURL(domainUrl.getRedirectedURL());
                domainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                crawlingURLs.add(redirectedDomainUrl);
                domainDoc = NinjaURLCrawlerUtil.createJsoupConnection(redirectedDomainUrl);
            } else {
                crawlingURLs.add(domainUrl);
                logger.info("in else before createJsoupConnection....");
                domainDoc = NinjaURLCrawlerUtil.createJsoupConnection(domainUrl);
                homapageStats.setStatusCode(domainUrl.getUrlStatus());
            }

            BaseRobotRules baseRobotRules = NinjaURLCrawlerUtil.fetchRobotRules("Googlebot", webStats.getDomain());
//		NinjaURLCrawlerThread.setBaseRobotRules(baseRobotRules);
            LXRSEOURLFetchThread.setBaseRobotRules(baseRobotRules);

            List<NinjaURL> internalURLs = new ArrayList<>();

            lxrmUrlStatsService.fetchStats(domainDoc, homapageStats, internalURLs, webStats.getDomain(), baseRobotRules);
            lxrmUrlStatsService.addURLStatsInaList(lxrmUrlStatsList, homapageStats);
            try {
                session.setAttribute("brokenLinkFinishedUrls", lxrmUrlStatsList);
            } catch (Exception e) {
                logger.debug("Exception in while setting the total URLs count to session ", e);
            }
            synchronized (urlQueue) {
                urlQueue.addAll(internalURLs);
                urlQueue.notifyAll();
            }
            if (redirectedDomainUrl != null) {
                synchronized (redirectedDomainUrl) {
                    redirectedDomainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                    redirectedDomainUrl.notifyAll();
                }
            } else {
                synchronized (domainUrl) {
                    domainUrl.setCrawlStatus(NinjaURL.COMPLETED);
                    domainUrl.notifyAll();
                }
            }

            List<String> stopWordsList = new ArrayList<>();
            WebApplicationContext context1 = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
            if (context1.getServletContext().getAttribute("stopwords") != null) {
                stopWordsList = (List<String>) context1.getServletContext().getAttribute("stopwords");
            }
            boolean terminated = false;
            while (!terminated) {
                try {
                    synchronized (urlQueue) {
                        if (!urlQueue.isEmpty()) {
                            synchronized (crawlingURLs) {
                                for (NinjaURL url : urlQueue) {
                                    if (crawlingURLs.size() < urlLimit) {
                                        boolean added = crawlingURLs.add(url);
                                        if (added) {
                                            url.setCrawlStatus(NinjaURL.ADDED);
                                            LxrmUrlFecthThread urlThread = new LxrmUrlFecthThread(url, lxrmUrlStatsService, crawlingURLs, urlQueue, webStats, lxrmUrlStatsList, Common.BROKEN_LINK_CHECKER, stopWordsList);
                                            siteGraderThreadPool.execute(urlThread);
                                        }
                                    } else {
                                        break;
                                    }
                                }
                                crawlingURLs.notifyAll();
                            }
                        }
                        urlQueue.clear();
                        urlQueue.notifyAll();
                    }
                    int active = siteGraderThreadPool.getActiveCount();
                    int queued = siteGraderThreadPool.getQueue().size();
                    try {
                        session.setAttribute("blcTotUrlCnt", (active + queued));
                    } catch (Exception e) {
                        logger.error("Unable to set blcTotUrlCnt in session ", e);
                    }
                    if ((active + queued) > 0) {
                        synchronized (crawlingURLs) {
                            if (crawlingURLs.size() >= urlLimit && !siteGraderThreadPool.isShutdown()) {
                                logger.info("Shutting down sitegrader threadpool for '" + webStats.getDomain() + "'");
                                siteGraderThreadPool.shutdown();
                                //it won't take new tasks
                            }
                            crawlingURLs.notifyAll();
                        }
                        Thread.sleep(2000);
                    } else {
                        terminated = true;
                        UrlCrawlingStatus2 = true;
                        logger.info("Terminating sitegrader threadpool for '" + webStats.getDomain() + "' and the value of UrlCrawlingStatus2 in service class " + UrlCrawlingStatus2);
                        try {
                            session.setAttribute("UrlCrawlingStatus2", UrlCrawlingStatus2);
                        } catch (Exception e) {
                            logger.error("Unable to set UrlCrawlingStatus2 in session ", e);
                        }
                        if (!siteGraderThreadPool.isShutdown()) {
                            siteGraderThreadPool.shutdownNow();
                        }
                    }
                } catch (InterruptedException ex) {
                    logger.error("Site Grader threadpool execution interrupted.." + ex.getMessage());
                } catch (Exception ex) {
                    logger.error("Exception in Site Grader threadpool.." + ex.getMessage());
                }
            }

        } catch (Exception e) {
            logger.error("In fetchWebsiteData of BrokenLinkCheckerService error............ " + e.getMessage());
        }
    }

    public BrokenLinkCheckerResults finalUrlStatsList(List<LxrmUrlStats> lxrmUrlStatsList, String finalDomain) {
        BrokenLinkCheckerResults brokenLinkCheckerResults = new BrokenLinkCheckerResults();
        int notWorkingurls = 0;
        int permanentRedirectingUrls = 0;
        if (lxrmUrlStatsList != null && !lxrmUrlStatsList.isEmpty()) {
            for (LxrmUrlStats statsObj : lxrmUrlStatsList) {
                int statusCode = statsObj.getStatusCode();
                if (statusCode >= 400 && statusCode != 408 && statusCode < 500) {
                    statsObj.setStatusMsg("Not Working");
                    notWorkingurls = notWorkingurls + 1;
                } else if (statusCode == 408) {
                    statsObj.setStatusMsg("Unable to Connect");
                    notWorkingurls = notWorkingurls + 1;
                } else if (statusCode >= 500 && statusCode < 600) {
                    statsObj.setStatusMsg("Server Error");
                    notWorkingurls = notWorkingurls + 1;
                } else if (statusCode == 0) {
                    statsObj.setStatusMsg("Too Many Redirection");
                    notWorkingurls = notWorkingurls + 1;
                } else if (statusCode >= 300 && statusCode != 301 && statusCode < 400) {
                    statsObj.setStatusMsg("Temporary Redirection");
                    notWorkingurls = notWorkingurls + 1;
                } else if (statusCode == 301) {
                    statsObj.setStatusMsg("Permanent Redirection");
                    permanentRedirectingUrls = permanentRedirectingUrls + 1;
                } else {
                    statsObj.setStatusMsg("Working");
                }
            }
            brokenLinkCheckerResults.setLxrmurlStatsList(lxrmUrlStatsList);
            brokenLinkCheckerResults.setPermanentRedirectingUrls((int) lxrmUrlStatsList.stream().filter(l -> l.getStatusCode() == 301).count());
            brokenLinkCheckerResults.setNotWorkingUrlCount((int) lxrmUrlStatsList.stream().filter(l -> (l.getStatusCode() >= 400 && l.getStatusCode() != 408 && l.getStatusCode() < 500)).count());
            brokenLinkCheckerResults.setTemporaryRedirectingUrls((int) lxrmUrlStatsList.stream().filter(l -> l.getStatusCode() == 302).count());
            brokenLinkCheckerResults.setWorkingUrlCount((int) lxrmUrlStatsList.stream().filter(l -> l.getStatusCode() == 200).count());
            brokenLinkCheckerResults.setTooManyRedirectionUrls((int) lxrmUrlStatsList.stream().filter(l -> ((l.getStatusCode() == 0) || (l.getStatusCode() >= 500 && l.getStatusCode() < 600) || (l.getStatusCode() == 408))).count());
            brokenLinkCheckerResults.setFinalDomain(finalDomain);
        }
        return brokenLinkCheckerResults;
    }

    public String createBLCURLStatsXLSFile(Login user, BrokenLinkCheckerResults brokenLinkCheckerResults, String downloadfolder) {
        try {
//            BrokenLinkCheckerResults brokenLinkCheckerResults = (BrokenLinkCheckerResults) session.getAttribute("brokenLinkCheckerResults");
            List<LxrmUrlStats> urlStats = brokenLinkCheckerResults.getLxrmurlStatsList();
            HSSFCellStyle mainheadCellSty;
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFFont headfont = workBook.createFont();
            //To increase the  font-size
            headfont.setFontHeightInPoints((short) 20);
            headfont.setColor(HSSFColor.ORANGE.index);

            HSSFFont font = workBook.createFont();
            font.setBold(true);
            //for main heading
            mainheadCellSty = workBook.createCellStyle();
            mainheadCellSty.setVerticalAlignment(VerticalAlignment.CENTER);
            mainheadCellSty.setAlignment(HorizontalAlignment.CENTER);
            mainheadCellSty.setFont(font);

            HSSFSheet sheet = workBook.createSheet("Broken Link Checker Report");

            boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet, (short) 0, 0, (short) 1, 1);

//                
//		int rowCnt = 0;
//		if(logoAdded){
//			rowCnt = 5;
//		}
//                
            /*boolean logoAdded = CommonReportGenerator.addImage(workBook, sheet,(short) 0, 0, (short) 1, 1);*/
            HSSFFont font1 = workBook.createFont();
            HSSFCellStyle heaCelSty;
            HSSFCellStyle columnHeaderStyle = workBook.createCellStyle();
            HSSFCellStyle subColHeaderStyle = workBook.createCellStyle();
            CreationHelper createHelper = workBook.getCreationHelper();
            HSSFRow rows;
            HSSFCell cell = null;
            Runtime r = Runtime.getRuntime();
            r.freeMemory();
            //For logo and Reoprt name
            rows = sheet.createRow(0);
            cell = rows.createCell(1);
            cell.setCellValue("Broken Link Checker Report");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));
            mainheadCellSty.setFont(headfont);
            cell.setCellStyle(mainheadCellSty);
            rows.setHeight((short) 1000);

            // Number Cell Style
            HSSFCellStyle numberCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(false);
            numberCelSty.setFont(font);
            numberCelSty.setWrapText(true);
            numberCelSty.setAlignment(HorizontalAlignment.RIGHT);
            // Heading Cell Style
            heaCelSty = workBook.createCellStyle();
            font = workBook.createFont();
            font.setBold(false);
            heaCelSty.setFont(font);
            heaCelSty.setWrapText(true);

            // Column Heading Cell Style
            columnHeaderStyle = workBook.createCellStyle();
            font1 = workBook.createFont();
            font1.setBold(true);
            columnHeaderStyle.setFont(font1);
            columnHeaderStyle.setWrapText(true);
            columnHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            columnHeaderStyle.setAlignment(HorizontalAlignment.LEFT);

            subColHeaderStyle.setFont(font1);
            subColHeaderStyle.setWrapText(true);
            subColHeaderStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            subColHeaderStyle.setAlignment(HorizontalAlignment.LEFT);

            //Style for Date
            CellStyle dateStyle = workBook.createCellStyle();
            /*dateStyle.setDataFormat(createHelper.createDataFormat().getFormat(
                    "dd-mmm-yyyy"));*/
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("MMMM_dd_yy"));
            logger.info("Generating Report in .xls format");
            //Report final summary metrics
            rows = sheet.createRow(1);
            cell = rows.createCell(0);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 2));
            rows.setHeight((short) 500);
            cell.setCellValue("Domain Name: " + brokenLinkCheckerResults.getFinalDomain());
            cell.setCellStyle(subColHeaderStyle);

//            rows = sheet.createRow(2);
            cell = rows.createCell(3);
            rows.setHeight((short) 500);
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));
            rows.setHeight((short) 500);
            cell.setCellValue("Date: " + curDate);
            cell.setCellStyle(subColHeaderStyle);

            rows = sheet.createRow(2);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 4));
            cell.setCellValue("Total URL(s): " + (urlStats != null ? urlStats.size() : "NA"));
            cell.setCellStyle(subColHeaderStyle);
            rows = sheet.createRow(3);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 4));
            cell.setCellValue("Working URL(s): " + brokenLinkCheckerResults.getWorkingUrlCount());
            cell.setCellStyle(subColHeaderStyle);
            rows = sheet.createRow(4);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 4));
            cell.setCellValue("Permanent Redirection URL(s): " + brokenLinkCheckerResults.getPermanentRedirectingUrls());
            cell.setCellStyle(subColHeaderStyle);
            rows = sheet.createRow(5);
            cell = rows.createCell(0);
            rows.setHeight((short) 500);
            sheet.addMergedRegion(new CellRangeAddress(5, 5, 0, 4));
            cell.setCellValue("Not Working & Redirecting URL(s): " + (brokenLinkCheckerResults.getNotWorkingUrlCount()
                    + brokenLinkCheckerResults.getTemporaryRedirectingUrls() + brokenLinkCheckerResults.getTooManyRedirectionUrls()));
            cell.setCellStyle(subColHeaderStyle);

            //Report Headings
            rows = sheet.createRow(6);
            rows.setHeight((short) 500);
            cell = rows.createCell(0);
            cell.setCellValue("S.No");
            cell.setCellStyle(columnHeaderStyle);
            heaCelSty.setWrapText(true);

            cell = rows.createCell(1);
            cell.setCellValue("URL");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(2);
            cell.setCellValue("Status Code");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(3);
            cell.setCellValue("Status Message");
            cell.setCellStyle(columnHeaderStyle);

            cell = rows.createCell(4);
            cell.setCellValue("Base URL");
            cell.setCellStyle(columnHeaderStyle);

            logger.info("The sheet is before if " + sheet);
            if (urlStats != null && urlStats.size() > 0) {
                logger.debug("UrlStats size is :" + urlStats.size());
                try {
                    int x = 7, k = 0;
                    for (int j = 1; j < urlStats.size() + 1; j++) {
                        logger.debug("urlStats.get(j-1).getUrl()" + urlStats.get(j - 1).getUrl());
                        if (!urlStats.get(j - 1).getStatusMsg().equals("Working")) {
                            k = k + 1;
                            rows = sheet.createRow(x);
                            LxrmUrlStats lxrmUrlStats = urlStats.get(j - 1);
                            setHieghtToTheRow(lxrmUrlStats, rows);
                            for (int i = 0; i <= 4; i++) {
                                cell = rows.createCell(i);
                                heaCelSty.setAlignment(HorizontalAlignment.LEFT);
                                cell.setCellStyle(heaCelSty);
                                switch (i) {
                                    case 0:
//                                        cell = rows.createCell(i);
                                        cell.setCellValue(k);
//                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 1:
//                                        cell = rows.createCell(i);
                                        cell.setCellValue(lxrmUrlStats.getUrl());
//                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 2:
//                                        cell = rows.createCell(i);
                                        cell.setCellValue(((lxrmUrlStats.getStatusCode()) == 0 ? "-" : (lxrmUrlStats.getStatusCode()) + ""));
                                        if (lxrmUrlStats.getStatusCode() == 0) {
                                            numberCelSty.setAlignment(HorizontalAlignment.CENTER);
                                        } else {
                                            numberCelSty.setAlignment(HorizontalAlignment.RIGHT);
                                        }
                                        cell.setCellStyle(numberCelSty);
//                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 3:
//                                        cell = rows.createCell(i);
                                        cell.setCellValue(lxrmUrlStats.getStatusMsg());
//                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    case 4:
//                                        cell = rows.createCell(i);
                                        String baseURL = lxrmUrlStats.getBaseUrl();
                                        if (baseURL == null || baseURL.trim().equals("")) {
                                            baseURL = "-";
                                        }
                                        cell.setCellValue(baseURL);
//                                        cell.setCellStyle(heaCelSty);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            x = x + 1;
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception in creating UrlStats excel", e);
                }
                sheet.createFreezePane(0, 7);
            }
            int brokenLinksCount = brokenLinkCheckerResults.getPermanentRedirectingUrls() + brokenLinkCheckerResults.getNotWorkingUrlCount()
                    + brokenLinkCheckerResults.getTemporaryRedirectingUrls() + brokenLinkCheckerResults.getTooManyRedirectionUrls();
            if (brokenLinksCount == 0) {

                rows = sheet.createRow(7);
                cell = rows.createCell(0);
                rows.setHeight((short) 500);
                sheet.addMergedRegion(new CellRangeAddress(7, 7, 0, 4));
                cell.setCellValue("No Records Found");
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
                cell.setCellStyle(columnHeaderStyle);

                columnHeaderStyle = workBook.createCellStyle();
                font1 = workBook.createFont();
                font1.setBold(true);
                columnHeaderStyle.setFont(font1);
                columnHeaderStyle.setWrapText(true);
                columnHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            }

//            sheet.autoSizeColumn(5);
            sheet.setColumnWidth(0, 3500);
            sheet.setColumnWidth(1, 12500);
            sheet.setColumnWidth(2, 6000);
            sheet.setColumnWidth(3, 10000);
            sheet.setColumnWidth(4, 12500);

            logger.info(" Report generated in .xls format");

            String repName = "LXRMarketplace_Broken_Link_Checker_Report" + user.getId();
            String reptName = Common.removeSpaces(repName);
            String fileName = Common.createFileName(reptName) + ".xls";
            logger.info(" The reptName is ==" + reptName + "====fileName is ==" + fileName);
            File file = new File(downloadfolder + fileName);
            try (FileOutputStream stream = new FileOutputStream(file)) {
                workBook.write(stream);
                stream.flush();
            }
            return fileName;

        } catch (Exception e) {
            logger.error("exception in createBLCURLStatsXLSFile", e);
        }
        return null;
    }

    private void setHieghtToTheRow(LxrmUrlStats urlStats, HSSFRow rows) {
        logger.debug("Description" + urlStats.getUrl().length());
        if (urlStats.getUrl().length() > 150) {
            rows.setHeight((short) 1600);
        } else {
            rows.setHeight((short) 600);
        }
    }

    public PdfPCell createPdfCell(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 12f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public PdfPCell createPdfCellValues(String cellContent) {
        com.itextpdf.text.Font commonFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 12f, com.itextpdf.text.Font.NORMAL, new BaseColor(0, 0, 0)));
        PdfPCell newCell = null;
        newCell = new PdfPCell(new Phrase(cellContent, commonFont));
        newCell.setBorderColor(BaseColor.GRAY);
        newCell.setBorderWidth(0.1f);
        newCell.setPadding(6f);
        newCell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
        return newCell;
    }

    public String createBLCURLStatsPDFFile(Login user, BrokenLinkCheckerResults brokenLinkCheckerResults, String downloadfolder) throws DocumentException, FileNotFoundException, BadElementException, IOException, ParseException {

//        BrokenLinkCheckerResults brokenLinkCheckerResults = (BrokenLinkCheckerResults) session.getAttribute("brokenLinkCheckerResults");
        List<LxrmUrlStats> urlStats = brokenLinkCheckerResults.getLxrmurlStatsList();

        com.itextpdf.text.Font reportNameFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 13, com.itextpdf.text.Font.BOLD, new BaseColor(255, 110, 0)));
        com.itextpdf.text.Font blackFont = new com.itextpdf.text.Font(FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        com.itextpdf.text.Font elementDescFont = new com.itextpdf.text.Font(FontFactory.getFont("Arial", 10f, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));
        com.itextpdf.text.Font blackFontheadings = new com.itextpdf.text.Font(FontFactory.getFont("Aril", 11, com.itextpdf.text.Font.BOLD, new BaseColor(0, 0, 0)));

        String filename = "";
        try {
            String repName = "LXRMarketplace_Broken_Link_Checker_Report" + user.getId();
            String reptName = Common.removeSpaces(repName);
            filename = Common.createFileName(reptName) + ".pdf";
            logger.info("in side of createPdfFile. filename................. " + filename);
            String filePath = downloadfolder + filename;
            logger.info("in side of createPdfFile. filePath................. " + filePath);
            File file = new File(filePath);
            FileOutputStream stream = new FileOutputStream(file);
            com.itextpdf.text.Document pdfDoc = new com.itextpdf.text.Document(PageSize.A4.rotate(), 30, 30, 30, 30);
            PdfWriter.getInstance(pdfDoc, stream);
            pdfDoc.open();
            logger.info(" Report generated in .pdf format");
            Image img = com.itextpdf.text.Image.getInstance("/disk2/lxrmarketplace/images/LXRMarketplace.png");
            img.scaleToFit(90f, 50f);
            Annotation anno = new Annotation(0f, 0f, 0f, 0f, "https://lxrmarketplace.com");
            img.setAnnotation(anno);
            Phrase mainHeading = new Phrase();
            Chunk tempReportName = new Chunk("Broken Link Checker Report", reportNameFont);
            mainHeading.add(tempReportName);
            float[] headingWidths = {55f, 45f};
            PdfPTable imgTable = new PdfPTable(headingWidths);
            imgTable.setWidthPercentage(99.9f);
            PdfPCell logo = new PdfPCell(new Phrase(new Chunk(img, 0, 0, true)));
            float[] headingTabWidths = {99.9f};
            PdfPTable headTab = new PdfPTable(headingTabWidths);
            headTab.setWidthPercentage(99.9f);
            PdfPCell rpName = new PdfPCell(mainHeading);
            rpName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            rpName.setBorder(Rectangle.NO_BORDER);
            rpName.setPaddingRight(20f);
            headTab.addCell(rpName);
            headTab.completeRow();
            Phrase domHeading = new Phrase();
            PdfPCell domName = new PdfPCell(domHeading);
            domName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            domName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(domName);
            headTab.completeRow();
            Timestamp startDat = new Timestamp(Calendar.getInstance().getTimeInMillis()); //current dt
            SimpleDateFormat dtf = new SimpleDateFormat("MMMM dd, yyyy HH:mm z");
            dtf.setTimeZone(TimeZone.getTimeZone("GMT"));
            String curDate = dtf.format(startDat);
            PdfPCell dateName = new PdfPCell(new Phrase("Date: " + curDate, blackFont));
            dateName.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
            dateName.setBorder(Rectangle.NO_BORDER);
            headTab.addCell(dateName);
            headTab.completeRow();
            PdfPCell textForheading = new PdfPCell(headTab);
            textForheading.setBorder(Rectangle.NO_BORDER);
            logo.setBorder(Rectangle.NO_BORDER);
            logo.setPaddingBottom(27f);
            rpName.setPaddingBottom(27f);
            rpName.setPaddingTop(6f);
            imgTable.addCell(logo);
            imgTable.addCell(textForheading);
            pdfDoc.add(imgTable);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(Chunk.NEWLINE);
            // Report final summary metrics
            Phrase domainName = new Phrase(new Chunk("Domain Name: " + brokenLinkCheckerResults.getFinalDomain(), blackFontheadings));
            Phrase totalUrl = new Phrase(new Chunk("Total URL(s): " + urlStats.size(), blackFontheadings));
            Phrase workingUrl = new Phrase(new Chunk("Working URL(s): " + brokenLinkCheckerResults.getWorkingUrlCount(), blackFontheadings));
            Phrase permanentRedirectionURL = new Phrase(new Chunk("Permanent Redirection URL(s): " + brokenLinkCheckerResults.getPermanentRedirectingUrls(), blackFontheadings));
            Phrase notWorkingRedirectingURL = new Phrase(new Chunk("Not Working & Redirecting URL(s): "
                    + (brokenLinkCheckerResults.getNotWorkingUrlCount() + brokenLinkCheckerResults.getTemporaryRedirectingUrls()
                    + brokenLinkCheckerResults.getTooManyRedirectionUrls()), blackFontheadings));

            pdfDoc.add(domainName);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(totalUrl);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(workingUrl);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(permanentRedirectionURL);
            pdfDoc.add(Chunk.NEWLINE);
            pdfDoc.add(notWorkingRedirectingURL);
            pdfDoc.add(Chunk.NEWLINE);

            float[] resultTabWidths = {5f, 25f, 10f, 16f, 25f};
            PdfPTable googleResultTab = new PdfPTable(resultTabWidths);
            googleResultTab.setWidthPercentage(99.9f);
            pdfDoc.add(Chunk.NEWLINE);

            //Report Headings
            PdfPCell sNoHeader = createPdfCell("S.No");
            PdfPCell url = createPdfCell("URL");
            PdfPCell status = createPdfCell("Status Code");
            PdfPCell statusMessage = createPdfCell("Status Message");
            PdfPCell baseURL = createPdfCell("Base URL");

            googleResultTab.addCell(sNoHeader);
            googleResultTab.addCell(url);
            googleResultTab.addCell(status);
            googleResultTab.addCell(statusMessage);
            googleResultTab.addCell(baseURL);
            googleResultTab.completeRow();

            try {
                if (urlStats != null && urlStats.size() > 0) {
                    logger.info("back link size is" + urlStats.size());
                    int k = 0;
                    for (int j = 1; j < urlStats.size() + 1; j++) {
                        if (!urlStats.get(j - 1).getStatusMsg().equals("Working")) {
                            k = k + 1;
                            LxrmUrlStats lxrmUrlStats = urlStats.get(j - 1);
                            for (int i = 0; i <= 4; i++) {
                                switch (i) {
                                    case 0:
                                        PdfPCell sno = createPdfCellValues(k + "");
                                        googleResultTab.addCell(sno);
                                        break;
                                    case 1:
                                        PdfPCell referringUrl = createPdfCellValues(lxrmUrlStats.getUrl() + "");
                                        googleResultTab.addCell(referringUrl);
                                        break;
                                    case 2:
                                        String urlStatus = (lxrmUrlStats.getStatusCode() == 0 ? "-" : (lxrmUrlStats.getStatusCode())).toString();
                                        PdfPCell referringUrlRating = createPdfCellValues(urlStatus + " ");
                                        referringUrlRating.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                        if (lxrmUrlStats.getStatusCode() == 0) {
                                            referringUrlRating.setHorizontalAlignment(Element.ALIGN_CENTER);
                                        }
                                        googleResultTab.addCell(referringUrlRating);
                                        break;
                                    case 3:
                                        PdfPCell anchor = createPdfCellValues(lxrmUrlStats.getStatusMsg() + "");
                                        googleResultTab.addCell(anchor);
                                        break;
                                    case 4:
                                        PdfPCell lastChecked = createPdfCellValues(lxrmUrlStats.getBaseUrl() + "");
                                        if (lxrmUrlStats.getBaseUrl() == null || lxrmUrlStats.getBaseUrl().trim().equals("")) {
                                            lastChecked = new PdfPCell(new Phrase("-"));
                                            lastChecked.setHorizontalAlignment(Element.ALIGN_CENTER);
                                        }
                                        googleResultTab.addCell(lastChecked);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                    }
                }

            } catch (Exception e) {
                logger.error("Exception while generating Brokenlinkchecker PDF report", e);
            }
            googleResultTab.completeRow();
            int brokenLinksCount = brokenLinkCheckerResults.getPermanentRedirectingUrls() + brokenLinkCheckerResults.getNotWorkingUrlCount()
                    + brokenLinkCheckerResults.getTemporaryRedirectingUrls() + brokenLinkCheckerResults.getTooManyRedirectionUrls();
            if (brokenLinksCount == 0) {
                PdfPCell noRecords = new PdfPCell(new Phrase("No Records ", elementDescFont));
                noRecords.setBorderColor(BaseColor.GRAY);
                noRecords.setBorderWidth(0.1f);
                noRecords.setPadding(6f);
                noRecords.setColspan(5);
                noRecords.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
                googleResultTab.addCell(noRecords);
                googleResultTab.completeRow();

            }
            pdfDoc.add(googleResultTab);
            pdfDoc.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return filename;

    }
}
