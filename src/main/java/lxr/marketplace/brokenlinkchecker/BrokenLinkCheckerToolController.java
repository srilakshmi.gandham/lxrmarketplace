package lxr.marketplace.brokenlinkchecker;

import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.crawler.LxrmUrlStats;
import lxr.marketplace.seoninja.data.NinjaWebStats;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/broken-link-checker-tool.html")
public class BrokenLinkCheckerToolController {

    private static Logger logger = Logger.getLogger(BrokenLinkCheckerToolController.class);

    private final String downloadFolder;
    private final BrokenLinkCheckerService brokenLinkCheckerService;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private MessageSource messageSource;
    JSONObject userInputJson = null;
    private int toolAccessLimit;

    @Autowired
    public BrokenLinkCheckerToolController(BrokenLinkCheckerService brokenLinkCheckerService,
            String downloadFolder) {
        this.brokenLinkCheckerService = brokenLinkCheckerService;
        this.downloadFolder = downloadFolder;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String initForm(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @ModelAttribute("brokenLinkCheckerTool") BrokenLinkCheckerTool brokenLinkCheckerTool, ModelMap model) {

        model.addAttribute("brokenLinkCheckerTool", brokenLinkCheckerTool);
        if (((WebUtils.hasSubmitParameter(request, "getResult")) && (WebUtils.hasSubmitParameter(request, "loginRefresh")))
                || ((WebUtils.hasSubmitParameter(request, "backLink"))
                && (WebUtils.hasSubmitParameter(request, "loginRefresh")))) {
            session.setAttribute("loginrefresh", "loginrefresh");
            brokenLinkCheckerTool.setDomain((String) session.getAttribute("finalDomUrl"));
            model.addAttribute(brokenLinkCheckerTool);
            model.addAttribute("status", "success");
        } else if (((WebUtils.hasSubmitParameter(request, "Invalid")) && (WebUtils.hasSubmitParameter(request, "loginRefresh")))
                || WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
            session.setAttribute("loginrefresh", "loginrefresh");
            if (session.getAttribute("brokenLinkCheckerTool") != null) {
                brokenLinkCheckerTool = (BrokenLinkCheckerTool) session.getAttribute("brokenLinkCheckerTool");
            }
            model.addAttribute(brokenLinkCheckerTool);
        }
        return "views/brokenlinkchecker/brokenLinkChecker";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(HttpServletRequest request, HttpServletResponse response,
            ModelMap model, @ModelAttribute("brokenLinkCheckerTool") BrokenLinkCheckerTool brokenLinkCheckerTool,
            BindingResult result, HttpSession session) throws JSONException {

        Login user = (Login) session.getAttribute("user");

        if (brokenLinkCheckerTool != null) {
            session.setAttribute("brokenLinkCheckerTool", brokenLinkCheckerTool);
        }

        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(31);
            toolObj.setToolName("Broken Link Checker");
            toolObj.setToolLink("broken-link-checker-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.BROKEN_LINK_CHECKER);
            boolean UrlCrawlingStatus = false;
            /* Applying the tool access limit filter for local user */
            toolAccessLimit = (user.isIslocal() && !user.isDeleted()) ? Common.LOCAL_USER_TOOL_LIMIT : Common.BROKEN_LINK_CHECKER_TOOL_LIMIT;
            logger.debug("In POST-The user id for USER in BrokenLinkCheckerToolController" + user.getId());
            if (WebUtils.hasSubmitParameter(request, "getResult")) {
                boolean userLoggedIn = (Boolean) session.getAttribute("userLoggedIn");
                session.removeAttribute("UrlCrawlingStatus2");
                session.removeAttribute("backLinkUrl");
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    return "views/brokenlinkchecker/brokenLinkChecker";
                }
                session.setAttribute("domainUrl", brokenLinkCheckerTool.getDomain());
                List<LxrmUrlStats> lxrmUrlStatsList = new ArrayList<>();
                session.setAttribute("brokenLinkFinishedUrls", lxrmUrlStatsList);
                if (request.getParameter("backLinkURL") != null) {
                    String tempUrl = request.getParameter("backLinkURL").trim();
                    brokenLinkCheckerTool.setDomain(tempUrl);
                }
                String finalUrl = (brokenLinkCheckerTool.getDomain() != null && !brokenLinkCheckerTool.getDomain().trim().isEmpty()) ? Common.getUrlValidation(brokenLinkCheckerTool.getDomain().trim(), session) : "redirected";
                if (finalUrl.equals("invalid") || finalUrl.equals("redirected")) {
                    String backLinkUrl = messageSource.getMessage("URL.error", null, Locale.US);
                    session.setAttribute("backLinkUrl", backLinkUrl);
                    return "views/brokenlinkchecker/brokenLinkChecker";
                } else {

                    if (!finalUrl.startsWith("http")) {
                        finalUrl = "http://" + finalUrl;
                    }
                    URL myUrl = null;
                    try {
                        myUrl = new URL(finalUrl);
                    } catch (MalformedURLException e) {
                        logger.error("Exception in forming the url", e);
                    }
                    String finalDomUrl = null;
                    if (myUrl != null) {
                        finalDomUrl = myUrl.getHost().trim();
                        session.setAttribute("finalDomUrl", finalDomUrl);
                    }
                    if (finalDomUrl != null) {
                        NinjaWebStats ninjaWebStats = new NinjaWebStats(finalDomUrl);

                        brokenLinkCheckerService.fetchWebsiteData(lxrmUrlStatsList, ninjaWebStats, session, toolAccessLimit);
                        // For sorting the list based on status code in ascending order and get 0 status code having object top
                        lxrmUrlStatsList.sort((o1, o2) -> o2.getStatusCode() - o1.getStatusCode());

                        model.addAttribute("status", "success");
                        model.addAttribute(brokenLinkCheckerTool);
                        BrokenLinkCheckerResults brokenLinkCheckerResults = brokenLinkCheckerService.finalUrlStatsList(lxrmUrlStatsList, finalDomUrl);
                        if (request.getSession(true) != null) {
                            session.setAttribute("finalDomUrl", finalDomUrl);
                            session.setAttribute("lxrmUrlStatsList", lxrmUrlStatsList);
                            session.removeAttribute("brokenLinkCheckerResults");
                            session.setAttribute("brokenLinkCheckerResults", brokenLinkCheckerResults);
                            model.addAttribute("brokenLinkCheckerResults", brokenLinkCheckerResults);
                        }
                        if (user.isIslocal() && userLoggedIn) {
                            String dwnfileName = brokenLinkCheckerService.createBLCURLStatsXLSFile(user, brokenLinkCheckerResults, downloadFolder);
                            Common.sendReportThroughMail(request, downloadFolder, dwnfileName, user.getUserName(), toolObj.getToolName());
                            logger.info("sent report through mail for user: " + user.getUserName());
                        }
                    } else {
                        model.addAttribute("status", "fail");
                    }
                }
            } else if (WebUtils.hasSubmitParameter(request, "backLink")) {
                if (session.getAttribute("lxrmUrlStatsList") != null) {
                    List<LxrmUrlStats> lxrmUrlStatsList = (List<LxrmUrlStats>) session.getAttribute("lxrmUrlStatsList");
                    String finalDomUrl = (String) session.getAttribute("finalDomUrl");
                    BrokenLinkCheckerResults brokenLinkCheckerResults = brokenLinkCheckerService.finalUrlStatsList(lxrmUrlStatsList, finalDomUrl);
                    logger.debug("lxrmUrlStatsList==============" + lxrmUrlStatsList.size() + "brokenLinkCheckerResults is ==" + brokenLinkCheckerResults);
                    session.removeAttribute("brokenLinkCheckerResults");
                    session.setAttribute("brokenLinkCheckerResults", brokenLinkCheckerResults);
                    model.addAttribute("status", "success");
                    if (brokenLinkCheckerTool != null) {
                        brokenLinkCheckerTool.setDomain((String) session.getAttribute("finalDomUrl"));
                        model.addAttribute(brokenLinkCheckerTool);
                    }
                    Session userSession = (Session) session.getAttribute("userSession");
                    Common.modifyDummyUserInToolorVideoUsage(request, response);
                    userSession.addToolUsage(toolObj.getToolId());
                    if (user.getId() == -1) {
                        user = (Login) session.getAttribute("user");
                    }
                    // if the request is coming from tool page then only add the user inputs to JSONObject
                    // if the request is coming from mail then do not add the user inputs to JSONObject
                    boolean requestFromMail = false;
                    if (session.getAttribute("requestFromMail") != null) {
                        requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                        session.removeAttribute("requestFromMail");
                    }
                    if (!requestFromMail && user != null && !user.isAdmin()) {
                        userInputJson = new JSONObject();
                    }
                    model.addAttribute("domain", finalDomUrl);
                    return "views/brokenlinkchecker/brokenLinkChecker";
                }
            } else if (WebUtils.hasSubmitParameter(request, "getdata")) {
                int blcTotUrlCnt = 0;
                String crawlingStatus = "false";
                String backLinkUrl = "valid";
                List<LxrmUrlStats> brokenLinkFinishedUrls = new ArrayList<>();
                if (session.getAttribute("UrlCrawlingStatus2") != null) {
                    UrlCrawlingStatus = (Boolean) session.getAttribute("UrlCrawlingStatus2");
                }
                if (session.getAttribute("backLinkUrl") != null) {
                    backLinkUrl = (String) session.getAttribute("backLinkUrl");
                }
                if (UrlCrawlingStatus) {
                    crawlingStatus = "true";
                }
                if (session.getAttribute("blcTotUrlCnt") != null) {
                    blcTotUrlCnt = (Integer) session.getAttribute("blcTotUrlCnt");
                }
                JSONArray arr = new JSONArray();
                PrintWriter writer;
                try {
                    writer = response.getWriter();
                    if (session.getAttribute("brokenLinkFinishedUrls") != null) {
                        brokenLinkFinishedUrls = (List<LxrmUrlStats>) session.getAttribute("brokenLinkFinishedUrls");
                    }
                    if (brokenLinkFinishedUrls != null) {
                        arr.add(0, backLinkUrl);
                        arr.add(1, blcTotUrlCnt);/*to check thread is terminated or not*/
                        arr.add(2, brokenLinkFinishedUrls.size());/*to get link depth*/
                        arr.add(3, crawlingStatus);
                        writer.print(arr);
                        writer.flush();
                        writer.close();
                    }
                } catch (IOException e) {
                    logger.error("While crawling the website ", e);
                }

            } else if (WebUtils.hasSubmitParameter(request, "download")) {
                try {
                    BrokenLinkCheckerResults brokenLinkCheckerResults = (BrokenLinkCheckerResults) session.getAttribute("brokenLinkCheckerResults");
                    if (brokenLinkCheckerResults != null) {
                        String dwnfileName = "";
                        String downParam = request.getParameter("report-type");
                        if (downParam.equalsIgnoreCase("xls")) {
                            dwnfileName = brokenLinkCheckerService.createBLCURLStatsXLSFile(user, brokenLinkCheckerResults, downloadFolder);
                            Common.downloadReport(response, downloadFolder, dwnfileName, downParam);
                        } else if (downParam.equalsIgnoreCase("pdf")) {
                            dwnfileName = brokenLinkCheckerService.createBLCURLStatsPDFFile(user, brokenLinkCheckerResults, downloadFolder);
                            Common.downloadReport(response, downloadFolder, dwnfileName, downParam);
                        } else if (downParam.equalsIgnoreCase("sendmail")) {
                            String toAddrs = request.getParameter("email");
                            String toolName = "Broken Link Checker";
                            dwnfileName = brokenLinkCheckerService.createBLCURLStatsXLSFile(user, brokenLinkCheckerResults, downloadFolder);
                            Common.sendReportThroughMail(request, downloadFolder, dwnfileName, toAddrs, toolName);
                            logger.info("Mail sent for user the to toAddrs===" + toAddrs);
                            return null;
                        }
                        model.addAttribute("status", "success");
                    }

                } catch (DocumentException | IOException | ParseException e) {
                    logger.error("Exception in download Reports request ", e);
                }
                return null;
            } else if (WebUtils.hasSubmitParameter(request, "ate")) {
                /* Ask The Expert Block Start */
                Session userSession = (Session) session.getAttribute("userSession");
                String domain = request.getParameter("domain");
                BrokenLinkCheckerResults brokenLinkCheckerResults = null;
                if (session.getAttribute("brokenLinkCheckerResults") != null) {
                    brokenLinkCheckerResults = (BrokenLinkCheckerResults) session.getAttribute("brokenLinkCheckerResults");
                }
                if (brokenLinkCheckerResults != null) {
                    List<LxrmUrlStats> urlStats = brokenLinkCheckerResults.getLxrmurlStatsList();
                    long status302Count = urlStats.stream().filter(item -> item.getStatusCode() == 302).count();
                    long status404Count = urlStats.stream().filter(item -> item.getStatusCode() == 404).count();
                    long status500Count = urlStats.stream().filter(item -> item.getStatusCode() == 500).count();
                    long tooManyRedirectionCount = urlStats.stream().filter(item -> item.getStatusMsg().equals("Too Many Redirection")).count();
                    String toolIssues = "";
                    String questionInfo = "";
                    String status302Text = "";
                    String status404Text = "";
                    String status500Text = "";
                    String tooManyRedirectionText = "";
                    boolean issueStatus = false;
                    // For getting the no of urls having status 302
                    if (status302Count > 0) {
                        issueStatus = true;
                        status302Text = "<span style='color:red;'>" + status302Count + " </span> links could be using an improper redirect. "
                                + "Consider using 301 redirects to pass rank power or link juice to permanently redirected links.";
                    }
                    // For getting the no of urls having status 404
                    if (status404Count > 0) {
                        issueStatus = true;
                        status404Text = "<span style='color:red;'>" + status404Count + " </span> page(s) on your website are leading to a 404 error page. "
                                + "Too many error pages could result in a lower search engine ranking.";
                    }
                    // For getting the no of urls having status 500
                    if (status500Count > 0) {
                        issueStatus = true;
                        status500Text = "Your website's error messaging may be improperly set up. "
                                + "<span style='color:red;'>" + status500Count + " </span> page(s) are giving unexpected errors.";
                    }
                    // For getting the no of urls having Too Many Redirections
                    if (tooManyRedirectionCount > 0) {
                        issueStatus = true;
                        tooManyRedirectionText = "Visitors to your website may not be having the best user experience on your website. "
                                + "<span style='color:red;'>" + tooManyRedirectionCount + "</span> page(s) have too many redirections.";
                    }
                    if (issueStatus) {
                        questionInfo = "Need help in fixing following issues with my website '" + domain + "'.\n\n";
                        toolIssues = "<ul>";
                        if (status302Count > 0) {
                            questionInfo += "- Fixing 302 link redirections.\n";
                            toolIssues += "<li>" + status302Text + "</li>";
                        }
                        if (status404Count > 0) {
                            questionInfo += "- Fixing 404 missing page errors.\n";
                            toolIssues += "<li>" + status404Text + "</li>";
                        }
                        if (status500Count > 0) {
                            questionInfo += "- Fixing server 500 errors.\n";
                            toolIssues += "<li>" + status500Text + "</li>";
                        }
                        if (tooManyRedirectionCount > 0) {
                            questionInfo += "- Fixing redirections issues.\n";
                            toolIssues += "<li>" + tooManyRedirectionText + "</li>";
                        }
                        toolIssues += "</ul>";
                    }
                    LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                    lxrmAskExpert.setDomain(domain);
                    if (userInputJson != null) {
                        lxrmAskExpert.setOtherInputs(userInputJson.toString());
                    }
                    lxrmAskExpert.setQuestion(questionInfo);
                    lxrmAskExpert.setIssues(toolIssues);
                    lxrmAskExpert.setExpertInfo(questionInfo);
                    lxrmAskExpert.setToolName(Common.BROKEN_LINK_CHECKER);
                    lxrmAskExpertService.addInJson(lxrmAskExpert, response);

                    // if the request is not from  mail then we should add this input to the session object
                    if (userInputJson != null && userSession != null) {
                        String toolUrl = "/" + toolObj.getToolLink();
                        userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), domain, toolIssues,
                                questionInfo, userInputJson.toString(), toolUrl);
                        userInputJson = null;
                    }
                }
            }
            /* Ask The Expert Block End*/
        }
        return "views/brokenlinkchecker/brokenLinkChecker";
    }
}
