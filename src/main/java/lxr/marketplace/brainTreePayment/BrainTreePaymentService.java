/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.brainTreePayment;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.Date;
import org.apache.log4j.Logger;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.exceptions.NotFoundException;
import java.time.LocalDateTime;
import java.util.Locale;
import lxr.marketplace.exception.ResourceNotFoundException;
import lxr.marketplace.payment.LXRMTransaction;
import lxr.marketplace.payment.GatewayService;
import lxr.marketplace.payment.PaymentModel;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import org.springframework.stereotype.Component;

/**
 *
 * @author netelixir
 */
@Component
public class BrainTreePaymentService implements GatewayService {

    private static final Logger LOGGER = Logger.getLogger(BrainTreePaymentService.class);
    
     @Autowired
    MessageSource messageSource;

    private static final BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX, "m8c4vwm76947xsgj", "my8697kkz6bgwxjy", "959c9df74000d42f05e728a7f0f8ee95");

//    private static final BraintreeGateway gateway = new BraintreeGateway(
//            Environment.PRODUCTION,
//            "ndpkmygb9twwyr5p",
//            "jkpmxvwc2dj7tcc2",
//            "fa5e3fd40a1e553b5d4586f932f5796e"
//    );
    @Override
    public PaymentModel authorizeUserPaymentCard(PaymentModel paymentModel) {
        PaymentModel savedCard = null;
        CustomerRequest customerReqObj = getCustomerReqObj(paymentModel);
        Result<Customer> result = gateway.customer().create(customerReqObj);
        if (result != null && result.isSuccess()) {
            LOGGER.debug("Successfully fetching the card details");
            savedCard = createCardObjFromCustomer(result, paymentModel);
        } else {
            LOGGER.debug("Card authorization Failed");
            throw new ResourceNotFoundException((result != null && result.getMessage() != null && !result.getMessage().isEmpty()) ? result.getMessage() : AUTH_FAILURE);

        }
        return savedCard;
    }

    @Override
    public LXRMTransaction chargeAmount(PaymentModel paymentModel) {
        LXRMTransaction transactionStats = null;
        Result<Transaction> result = null;
        TransactionRequest transactionRequest = null;
        try {
            Customer customer = gateway.customer().find(paymentModel.getServiceProviderCardId());
            String paymentMethodToken = customer.getCreditCards().get(0).getToken();
            String totalAmount = String.valueOf(paymentModel.getPrice());
            transactionRequest = new TransactionRequest()
                    .amount(new BigDecimal(totalAmount))
                    .paymentMethodToken(paymentMethodToken).options().submitForSettlement(Boolean.TRUE).done();

            result = gateway.transaction().sale(transactionRequest);
            transactionStats = getTransactionStatsObj(result, paymentModel);

        } catch (NotFoundException e) {
            LOGGER.error("Unable to find stored card for user with ");
        } catch (Exception e) {
            LOGGER.error("Exception in brain tree transaction object");
        }
        if (transactionStats == null || result == null || transactionRequest == null) {
            transactionStats = new LXRMTransaction();
            transactionStats.setState(CAPTURE_FAILURE);
            transactionStats.setFailureMessage(messageSource.getMessage("lxrm.payment.validation.error", null, Locale.US).concat(CAPTURE_FAILURE));
        }
        return transactionStats;
    }

    private CustomerRequest getCustomerReqObj(PaymentModel paymentModel) {
        CustomerRequest request = new CustomerRequest()
                .firstName(paymentModel.getFullName())
                .email(paymentModel.getBillingEmailId())
                .creditCard()
                .cardholderName(paymentModel.getFullName())
                .cvv(paymentModel.getCvv())
                .number(paymentModel.getCardNo())
                .expirationMonth(String.valueOf(paymentModel.getExpireMonth()))
                .expirationYear(String.valueOf(paymentModel.getExpireYear()))
                .done();
//                .options().verificationAmount("1.00").verifyCard(Boolean.TRUE).done()
//                .done();
        return request;

    }

    private PaymentModel createCardObjFromCustomer(Result<Customer> result, PaymentModel paymentModel) {

        CreditCard cardDetails = result.getTarget().getCreditCards().get(0);
        paymentModel.setServiceProviderCardId(result.getTarget().getId());
        paymentModel.setCardNo(cardDetails.getMaskedNumber());
        paymentModel.setCardType(cardDetails.getCardType());
        paymentModel.setPaymentDate(LocalDateTime.now());
        return paymentModel;
    }

    private LXRMTransaction getTransactionStatsObj(Result<Transaction> transaction, PaymentModel paymentModel) {
        LXRMTransaction transactionStats = new LXRMTransaction();
        if (transaction != null && transaction.isSuccess()) {
            transactionStats.setGatewayTransactionId(transaction.getTarget().getId());
            Date date = transaction.getTarget().getCreatedAt().getTime();
            LocalDateTime localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            transactionStats.setCreatedDate(localDate);
            transactionStats.setAmount(String.valueOf(transaction.getTarget().getAmount()));
            transactionStats.setCurrency(paymentModel.getCurrencyType());
            transactionStats.setState(GatewayService.CAPTURE_INTENT);
//            transactionStats.setStatusCode(HttpStatus.SC_OK);
            transactionStats.setDescription(paymentModel.getDescription());
        } else {
            transactionStats.setFailureMessage((transaction != null && transaction.getMessage() != null && !transaction.getMessage().isEmpty()) ? messageSource.getMessage("lxrm.payment.validation.error", null, Locale.US).concat(transaction.getMessage()) : messageSource.getMessage("lxrm.payment.validation.error", null, Locale.US).concat(CAPTURE_FAILURE));
        }

        return transactionStats;
    }

    @Override
    public TransactionStats chargeAmount(LxrMCreditCard lxrCreditCard, String totalAmount, String[] errorMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
