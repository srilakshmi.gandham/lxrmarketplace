/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.hreflang;

import java.util.List;

/**
 *
 * @author anilkumar
 */
public class HrefLang {

    private String websiteURL;
    private List<HrefLangPage> hrefLangPageList;
    private boolean checked;
    private boolean xml;
    private boolean canonical;

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public List<HrefLangPage> getHrefLangPageList() {
        return hrefLangPageList;
    }

    public void setHrefLangPageList(List<HrefLangPage> hrefLangPageList) {
        this.hrefLangPageList = hrefLangPageList;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isXml() {
        return xml;
    }

    public void setXml(boolean xml) {
        this.xml = xml;
    }

    public boolean isCanonical() {
        return canonical;
    }

    public void setCanonical(boolean canonical) {
        this.canonical = canonical;
    }
    
}
