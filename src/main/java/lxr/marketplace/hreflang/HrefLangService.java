/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.hreflang;

import java.util.ArrayList;
import java.util.List;
import lxr.marketplace.util.Common;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

/**
 *
 * @author anilkumar
 */
@Service
public class HrefLangService {

    public String generateHrefLangLinks(HrefLang hrefLang) {
        String hrefLangContent = "";
        for (HrefLangPage hrefLangPage : hrefLang.getHrefLangPageList()) {
            hrefLangContent += "\n" + generateHrefLangLink(hrefLangPage);
        }
        return hrefLangContent.trim();
    }

    private String generateHrefLangLink(HrefLangPage hrefLangPage) {
        String hreflangCode = null;
        if (hrefLangPage.getLanguage() != null && !hrefLangPage.getLanguage().equals("") && !hrefLangPage.getLanguage().equals("default")
                && hrefLangPage.getCountry() != null && !hrefLangPage.getCountry().equals("") && !hrefLangPage.getCountry().equals("default")) {
            hreflangCode = hrefLangPage.getLanguage().replace("lang_", "") + "-" + hrefLangPage.getCountry();
        }
        return "<link rel=\"alternate\" hreflang=\"" + (hreflangCode != null ? hreflangCode : "x-default") + "\" href=\"" + hrefLangPage.getPageUrl() + "/\" />";
    }

    public String generateXMLHrefLangLinks(HrefLang hrefLang) {
        String xmlHrefLangContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">";
        for (HrefLangPage hrefLangPage : hrefLang.getHrefLangPageList()) {
            xmlHrefLangContent += "\n" + generateXMLHrefLangLink(hrefLangPage);
        }
        xmlHrefLangContent += "\n</urlset>";
        return xmlHrefLangContent;
    }

    private String generateXMLHrefLangLink(HrefLangPage hrefLangPage) {
        String hreflangCode = null;
        if (hrefLangPage.getLanguage() != null && !hrefLangPage.getLanguage().equals("") && !hrefLangPage.getLanguage().equals("default")
                && hrefLangPage.getCountry() != null && !hrefLangPage.getCountry().equals("") && !hrefLangPage.getCountry().equals("default")) {
            hreflangCode = hrefLangPage.getLanguage().replace("lang_", "") + "-" + hrefLangPage.getCountry();
        }
        String xmlHrefLangContent = "	<url>\n"
                + "		<loc>" + hrefLangPage.getPageUrl() + "</loc>\n"
                + "		<xhtml:link rel=\"alternate\" hreflang=\"" + (hreflangCode != null ? hreflangCode : "x-default") + "\" href=\"" + hrefLangPage.getPageUrl() + "\" />\n"
                //                + "		<xhtml:link rel=\"alternate\" hreflang=\"ab-ax\" href=\"" + hrefLangPage.getPageUrl() + "\" />\n"
                + "	</url>";
        return xmlHrefLangContent;
    }

    private List<HrefLangPage> checkHrefLangAttribute(Document document) {
        Elements hrefLangElements = document.select("link[hreflang]");
        List<HrefLangPage> hrefLangContent = new ArrayList();
        HrefLangPage hrefLangPage = null;
        if (hrefLangElements != null && hrefLangElements.first() != null && hrefLangElements.first().attr("hreflang") != null
                && !hrefLangElements.first().attr("hreflang").trim().equals("")) {
            for (Element element : hrefLangElements) {
                hrefLangPage = new HrefLangPage();
                String hrefLangValue = element.attr("hreflang");
                if (hrefLangValue != null && !hrefLangValue.equals("")) {
                    if (hrefLangValue.trim().equals("x-default")) {
                        hrefLangPage.setCountry("default");
                        hrefLangPage.setLanguage("default");
                    } else {
                        String[] langAndLocation = hrefLangValue.split("-");
                        if (langAndLocation != null) {
                            if (langAndLocation.length >= 1) {
                                hrefLangPage.setCountry(langAndLocation[0]);
                            }
                            if (langAndLocation.length >= 2) {
                                hrefLangPage.setLanguage(langAndLocation[1]);
                            }
                        }
                    }
                    hrefLangPage.setPageUrl(hrefLangElements.attr("href"));
                    hrefLangContent.add(hrefLangPage);
                }
            }
        }
        return hrefLangContent;
    }

    public List<HrefLangPage> checkHrefLangForWebsite(HrefLang hrefLang) {
        String websiteURL = hrefLang.getWebsiteURL().trim();
        StringBuilder pageHTML = new StringBuilder("");
        List<HrefLangPage> hrefLangPageList = null;
        int statusCode = Common.checkCurrentURLAndGetHTML(websiteURL, pageHTML);
        if (statusCode == 200) {
            if (!pageHTML.toString().equals("")) {
                String httpDomainUrl = "";
                if (websiteURL.startsWith("https://") || websiteURL.startsWith("http://")) {
                    httpDomainUrl = websiteURL;
                } else {
                    httpDomainUrl = "http://" + websiteURL;
                }
                Document document = Jsoup.parse(pageHTML.toString(), httpDomainUrl);
                if(document != null){
                    hrefLangPageList = checkHrefLangAttribute(document);
                    hrefLang.setCanonical(checkCanonicalURL(document));
                }else{
                    hrefLangPageList = new ArrayList<>();
                }
            }
        }
        return hrefLangPageList;
    }

    private boolean checkCanonicalURL(Document document) {
        boolean canonicalStatus = false;
        Elements containedUrls = document.select("head link[href]");
        if (containedUrls != null && containedUrls.size() > 0) {
            for (Element link : containedUrls) {
                String canonical = link.attr("rel").trim();
                String childUrl = link.attr("href").trim();
                if (canonical != null && canonical.equals("canonical") && !childUrl.equals("")) {
                    canonicalStatus = true;
                    break;
                }
            }
        }
        return canonicalStatus;
    }
}
