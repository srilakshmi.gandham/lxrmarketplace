/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.hreflang;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.session.Session;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anilkumar
 */
@Controller
@RequestMapping("/hreflang-tool.html")
public class HrefLangController {

    @Autowired
    private HrefLangService hrefLangService;

    @RequestMapping(method = RequestMethod.GET)
    public String hreflangPage(Model model, HttpSession session) {
        model.addAttribute("hrefLang", new HrefLang());
        session.removeAttribute("resultContent");
        session.removeAttribute("hrefLangPageList");
        session.removeAttribute("hrefLang");
        return "views/hreflang/hreflang";
    }

    @ModelAttribute
    public void addLanguagesAndCountries(Model model) {
        model.addAttribute("locations", Common.loadGeoLocations());
        model.addAttribute("languages", Common.loadcustomSearchLanguages());
    }

    @RequestMapping(method = RequestMethod.POST)
    public String hreflangResult(Model model, HttpSession session, HttpServletRequest request,
            HttpServletResponse response, @ModelAttribute("hrefLang") HrefLang hrefLang) {
        String resultContent = null;
        List<HrefLangPage> hrefLangPageList = null;
        if (hrefLang.isChecked()) {
            hrefLang.setHrefLangPageList(null);
            hrefLangPageList = hrefLangService.checkHrefLangForWebsite(hrefLang);
        } else if (hrefLang.isXml()) {
            resultContent = hrefLangService.generateXMLHrefLangLinks(hrefLang);
        } else {
            resultContent = hrefLangService.generateHrefLangLinks(hrefLang);
        }
        Tool toolObj = getToolInfo();
        Session userSession = (Session) session.getAttribute("userSession");
        Common.modifyDummyUserInToolorVideoUsage(request, response);
        userSession.addToolUsage(toolObj.getToolId());
        model.addAttribute("resultContent", resultContent);
        model.addAttribute("hrefLangPageList", hrefLangPageList);
        model.addAttribute("hrefLang", hrefLang);
        model.addAttribute("showATEPpopup", Boolean.TRUE);
        session.setAttribute("resultContent", resultContent);
        session.setAttribute("hrefLangPageList", hrefLangPageList);
        session.setAttribute("hrefLang", hrefLang);
        return "views/hreflang/hreflang";
    }

    @RequestMapping(params = {"backToSuccessPage"})
    public String hreflangBackToSuccess(Model model, HttpSession session) {
        String resultContent = (String) session.getAttribute("resultContent");
        HrefLang hrefLang = (HrefLang) session.getAttribute("hrefLang");
        List<HrefLangPage> hrefLangPageList = (List<HrefLangPage>) session.getAttribute("hrefLangPageList");
        model.addAttribute("resultContent", resultContent);
        model.addAttribute("hrefLangPageList", hrefLangPageList);
        model.addAttribute("showATEPpopup", Boolean.TRUE);
        model.addAttribute("hrefLang", hrefLang != null ? hrefLang : new HrefLang());
        return "views/hreflang/hreflang";
    }

    private Tool getToolInfo() {
        Tool toolObj = new Tool();
        toolObj.setToolId(42);
        toolObj.setToolName(Common.HREFLANG_GENERATOR_CHECKER);
        toolObj.setToolLink("hreflang-tool.html");
        return toolObj;
    }

    @RequestMapping(params = {"ate=ate"}, method = RequestMethod.POST)
    @ResponseBody
    public Object[] getAteResponse(@RequestParam("domain") String domain) {
        String toolIssues = "Want to get the full audit of your website on how Google will know about website?";
        String questionInfo = "- Auditing on-page SEO factors for my website.";
        LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
        lxrmAskExpert.setDomain(domain);
        lxrmAskExpert.setQuestion(questionInfo);
        lxrmAskExpert.setIssues(toolIssues);
        lxrmAskExpert.setExpertInfo(questionInfo);
        lxrmAskExpert.setToolName(Common.HREFLANG_GENERATOR_CHECKER);
        Object[] obj = new Object[1];
        obj[0] = lxrmAskExpert;
        return obj;
    }
}
