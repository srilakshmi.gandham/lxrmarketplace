package lxr.marketplace.robotstxtvalidator;

public class RobotsTxtValidator {
	private String domainUrl;
	private String robotsContent;
	private String errorContent;
	
	public String getDomainUrl() {
		return domainUrl;
	}
	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}
	public String getRobotsContent() {
		return robotsContent;
	}
	public void setRobotsContent(String robotsContent) {
		this.robotsContent = robotsContent;
	}
	public String getErrorContent() {
		return errorContent;
	}
	public void setErrorContent(String errorContent) {
		this.errorContent = errorContent;
	}
	
	
}
