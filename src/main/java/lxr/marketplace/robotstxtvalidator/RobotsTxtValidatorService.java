package lxr.marketplace.robotstxtvalidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.applicationlistener.ApplicationContextProvider;
import lxr.marketplace.util.Common;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class RobotsTxtValidatorService extends JdbcDaoSupport {

    static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";

    ApplicationContext context = ApplicationContextProvider.getApplicationContext();

    public String fetchRobotTxtFileContent(String finalURL) {
        String robotsTxtContent = "";
        try {
            URL robotURL = new URL(finalURL);
            URLConnection connectionObj = robotURL.openConnection();
            connectionObj.setConnectTimeout(50000);
            connectionObj.setReadTimeout(30000);
            connectionObj.setRequestProperty("User-Agent", USER_AGENT);
            BufferedReader in = new BufferedReader(new InputStreamReader(connectionObj.getInputStream()));
            String inputLine = "";
            if (in != null) {
                while ((inputLine = in.readLine()) != null) {
                    robotsTxtContent = robotsTxtContent.concat("\n" + inputLine);
                }
            }
            in.close();
        } catch (IOException e) {
            logger.error("IOException in fetchRobotTxtFileContent: ", e);
        } catch (Exception me) {
            logger.error("Exception in fetchRobotTxtFileContent: ", me);
        }
        return robotsTxtContent;
    }

    public String validateURL(String url) {

        if (!url.startsWith("http")) {
            url = "http://" + url;
            int responseCode = Common.getURLStatus(url);
            if (responseCode > 400) {
                url = "https://" + url;
                responseCode = Common.getURLStatus(url);
            }
        }
        return url;
    }

    //currently used method
    public Map<Integer, List<RobotsTxtValidatorResult>> validateRobotsTxtContent(String robotTxtCOntent) {
//		RegMatchFieldsVersion1("disallow: ",null,1,"",1,"","disallow");
        RobotsTxtValidatorService robotsTxtValidatorService = new RobotsTxtValidatorService();
        context = robotsTxtValidatorService.context;
        Map<String, List<String>> robotsDataMap = new HashMap<>();
        Map<Integer, List<RobotsTxtValidatorResult>> validationMap = new HashMap<>();
        Map<String, List<Integer>> uAList = new HashMap<>();
        List<Integer> lineNumList = null;
        int tempNum = 0;
        String tempLineData = "";
        String[] robotstxtArr = robotTxtCOntent.trim().split("\n");
        String userAgent = "", directive = "", dirVal = "";
        List<String> sitemaps = new ArrayList<>();
        if (robotstxtArr != null && robotstxtArr.length > 0) {
            for (int i = 0; i < robotstxtArr.length; i++) {
                String robotsTxtLineData = robotstxtArr[i].trim();
                if (!robotsTxtLineData.startsWith("#") && !robotsTxtLineData.equals("")) {
                    tempLineData = robotsTxtLineData;
                    tempNum = i;
                    break;
                }
            }
            if (!tempLineData.trim().toLowerCase().startsWith("user-agent")) {
                tempLineData = tempLineData.trim().toLowerCase();
                if (tempLineData.startsWith("allow") || tempLineData.startsWith("disallow")) {
                    addValuesToList(validationMap, tempNum, tempLineData, context.getMessage("robotsValidator.noUserAgent", null, Locale.UK), 1);
                } else {
                    Object[] firstDirObj = new String[1];
                    if (tempLineData.startsWith("sitemap") || tempLineData.startsWith("crawl-delay") || tempLineData.startsWith("host")) {
                        firstDirObj[0] = tempLineData;
                        addValuesToList(validationMap, tempNum, tempLineData, context.getMessage("robotsValidator.firstValIsUA", firstDirObj, Locale.UK), 2);
                    }
                }
            }

            for (int i = 0; i < robotstxtArr.length; i++) {
                String tempRobotObj = robotstxtArr[i].toLowerCase();
                String robotObj = robotstxtArr[i].toLowerCase().trim();
                String dirRobotsObj = robotstxtArr[i].trim();
                logger.debug("robotObj==============" + robotObj + "line number==" + i);
                if (!tempRobotObj.startsWith("#")) {
                    RegMatchFields(tempRobotObj, validationMap, i, robotObj, 2, context.getMessage("robotsValidator.recordBeginsWithSpace", null, Locale.UK));
                    if (robotsDataMap != null && robotsDataMap.size() > 0) {
                        logger.debug("robotsDataMap............." + robotsDataMap.size());
                        //FOR USER-AGENT
                        if (robotObj.startsWith("user-agent")) {
                            String userAgentVal = getPropertyValue(robotObj).toLowerCase();
                            if (!userAgentVal.trim().equals("")) {
                                RegMatchFieldsVersion1(robotObj, validationMap, i, robotObj, 2, "user-agent");
                            } else {
                                addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.userAgentErr", null, Locale.UK), 1);
                            }
                            userAgent = robotObj;
                            lineNumList = new ArrayList<>();
                            if (uAList != null && uAList.size() > 0) {
                                if (uAList.get(userAgent) != null) {
                                    lineNumList = uAList.get(userAgent);
                                    lineNumList.add(i);
                                } else {
                                    lineNumList.add(i);
                                }
                            } else {
                                lineNumList.add(i);
                            }
                            uAList.put(robotObj, lineNumList);
                            if (robotsDataMap.containsKey(userAgent)) {
                                Object[] firstDirObj = new String[1];
                                firstDirObj[0] = userAgentVal;
                                addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.duplicateUserAgent", firstDirObj, Locale.UK), 2);
                            } else {
                                List<String> robotDirectives = new ArrayList<String>();
                                robotsDataMap.put(userAgent, robotDirectives);
                            }
                        } else //FOR ALLOW/DISALLOW DIRETIVES
                         if (robotObj.startsWith("allow") || robotObj.startsWith("disallow")) {
                                String dirName = "";
                                dirVal = getADPropertyValue(dirRobotsObj).trim();
                                String robotTempDir = robotObj;
                                Pattern p = Pattern.compile("^[\\p{Z}\\s]*$");
                                Matcher m = p.matcher(dirVal); // get a matcher object
                                boolean findVal = m.matches();
                                logger.debug("in matched cond in validate method........" + findVal);
                                if (!m.matches()) {
                                    if (!dirVal.startsWith("/")) {
                                        addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.dirValSlash", null, Locale.UK), 2);
                                    }
                                }
                                if (robotObj.startsWith("allow")) {

                                    addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.allowDir", null, Locale.UK), 2);
                                    if (m.matches()) {
                                        robotTempDir = "allow: ";
                                    }
                                    RegMatchFieldsVersion1(robotTempDir, validationMap, i, robotObj + dirVal, 2, "allow");
                                    dirName = "allow:";
                                } else if (robotObj.startsWith("disallow")) {
                                    if (m.matches()) {
                                        robotTempDir = "disallow: ";
                                    }
                                    RegMatchFieldsVersion1(robotTempDir, validationMap, i, robotObj + dirVal, 2, "disallow");
                                    dirName = "disallow:";
                                }
                                directive = dirName + dirVal;
                                if (robotsDataMap.get(userAgent) != null) {
                                    List<String> robotDirectives = robotsDataMap.get(userAgent);
                                    if (robotDirectives != null && robotDirectives.size() > 0) {
                                        Object[] firstDirObj = new String[1];
                                        if (robotDirectives.contains(directive)) {
                                            firstDirObj[0] = directive;
                                            addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.duplicateA/Ddir", firstDirObj, Locale.UK), 2);
                                        } else {
                                            firstDirObj[0] = dirVal;
                                            if (robotDirectives.contains(dirVal)) {
                                                addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.samePathForA/D", firstDirObj, Locale.UK), 2);
                                            }
                                            robotDirectives.add(directive);
                                            robotsDataMap.put(userAgent, robotDirectives);
                                        }
                                    } else {
                                        robotDirectives.add(directive);
                                        robotsDataMap.put(userAgent, robotDirectives);
                                    }
                                }
                            }
                    } else {
                        List<String> robotDirectives = new ArrayList<String>();
                        //For USER-AGENT
                        userAgent = robotObj.trim().toLowerCase();
                        if (userAgent.startsWith("user-agent")) {
                            String userAgentVal = getPropertyValue(userAgent).toLowerCase();
                            if (userAgentVal.trim().equals("")) {
                                addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.userAgentErr", null, Locale.UK), 1);
                            } else {
                                RegMatchFieldsVersion1(robotObj, validationMap, i, robotObj, 2, "user-agent");
                            }
                            lineNumList = new ArrayList<>();
                            if (uAList != null && uAList.size() > 0) {
                                if (uAList.get(userAgent) != null) {
                                    lineNumList = uAList.get(userAgent);
                                    lineNumList.add(i);
                                } else {
                                    lineNumList.add(i);
                                }
                            } else {
                                lineNumList.add(i);
                            }
                            uAList.put(robotObj, lineNumList);
                            robotsDataMap.put(userAgent, robotDirectives);
                        }
                    }
                    //FOR SITEMAP
                    if (robotObj.startsWith("sitemap")) {
                        String sitemapval = getPropertyValue(robotObj).trim().toLowerCase();
                        logger.info("sitemapval......." + sitemapval);
                        boolean notValidSitemapURL = checkURLSyntax(sitemapval);
                        if (notValidSitemapURL) {
                            addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.sitemapUrlErr", null, Locale.UK), 1);
                        } else {
                            RegMatchFieldsVersion1(robotObj, validationMap, i, robotObj, 2, "sitemap");
                        }
                        if (sitemaps != null && sitemaps.size() > 0) {
                            if (sitemaps.contains(sitemapval)) {
                                Object[] firstDirObj = new String[1];
                                firstDirObj[0] = sitemapval;
                                addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.duplicateSitemapUrl", firstDirObj, Locale.UK), 2);
                            } else {
                                sitemaps.add(sitemapval);
                            }
                        } else {
                            sitemaps.add(sitemapval);
                        }
                    }

                    //FOR CRAWL-DELAY
                    if (robotObj.startsWith("crawl-delay")) {
                        addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.crawlDelay", null, Locale.UK), 2);
                        String crwlDelayVal = getPropertyValue(robotObj);
                        if (!crwlDelayVal.trim().equals("")) {
                            RegDigitMatch(crwlDelayVal, validationMap, i, robotObj, 1, context.getMessage("robotsValidator.crawlDelayValErr", null, Locale.UK));
                            RegMatchFieldsVersion1(robotObj, validationMap, i, robotObj, 2, "crawl-delay");
                        } else {
                            addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.crawlDelayValErr", null, Locale.UK), 2);
                        }
                    }

                    //FOR HOST
                    if (robotObj.startsWith("host")) {
                        addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.host", null, Locale.UK), 1);
                        String hostVal = getPropertyValue(robotObj);
                        if (!hostVal.trim().equals("")) {
                            RegMatchFieldsVersion1(robotObj, validationMap, i, robotObj, 2, "host");
                        } else {
                            addValuesToList(validationMap, i, robotObj, context.getMessage("robotsValidator.hostErr", null, Locale.UK), 1);
                        }
                    }
                }
            }
            if (uAList != null && uAList.size() > 0) {
                if (robotsDataMap != null && robotsDataMap.size() > 0) {
                    for (Entry<String, List<String>> entry : robotsDataMap.entrySet()) {
                        List<String> dirList = entry.getValue();
                        logger.debug("entry.getValue()........" + entry.getValue());
                        if (dirList == null || dirList.size() <= 0) {
                            List<Integer> lineNums = uAList.get((entry.getKey()));
                            for (int lineNum : lineNums) {
                                addValuesToList(validationMap, lineNum, "", context.getMessage("robotsValidator.emptyUserAgent", null, Locale.UK), 1);
                            }
                        } else {
                            for (String dir : dirList) {
                                if (!dir.contains("disallow")) {

                                }
                            }
                        }
                    }
                }
            }
        }

        return validationMap;
    }

    public boolean checkURLSyntax(String url) {
        boolean isNotValid = false;
        if (url != null && !url.trim().equals("")) {
            try {
                URL currentUrlObj = new URL(url.trim());
                currentUrlObj.toURI();
            } catch (URISyntaxException e) {
                isNotValid = true;
                logger.error("URISyntaxException in checkURLSyntax" + e.getMessage());
            } catch (Exception me) {
                isNotValid = true;
                logger.error("Exception in checkURLSyntax" + me.getMessage());
            }
        }
        return isNotValid;
    }

    public boolean checkIsUAPresent(String inputContent, String checkedText) {
        boolean isPresent = false;
        if (inputContent.trim().toLowerCase().contains(checkedText)) {
            isPresent = true;
        }
        return isPresent;
    }

    public void addValuesToList(Map<Integer, List<RobotsTxtValidatorResult>> validationMap,
            int lineNum, String lineData, String errorMsg, int errorType) {
        lineNum = lineNum + 1;
        logger.debug("error list........." + lineData + "............" + errorMsg + "lineNum............" + lineNum);
        List<RobotsTxtValidatorResult> validationList = null;
        if (validationMap != null && validationMap.size() > 0) {
            if (validationMap.get((int) lineNum) != null) {
                validationList = validationMap.get(lineNum);
            } else {
                validationList = new ArrayList<RobotsTxtValidatorResult>();
            }
            RobotsTxtValidatorResult robotsTxtValidatorResult = new RobotsTxtValidatorResult();
            robotsTxtValidatorResult.setLineNum(lineNum);
            robotsTxtValidatorResult.setErrorType(errorType);
            robotsTxtValidatorResult.setLineData(lineData);
            robotsTxtValidatorResult.setErrMessage(errorMsg);
            validationList.add(robotsTxtValidatorResult);
            validationMap.put(lineNum, validationList);
        } else {
            validationList = new ArrayList<RobotsTxtValidatorResult>();
            RobotsTxtValidatorResult robotsTxtValidatorResult = new RobotsTxtValidatorResult();
            robotsTxtValidatorResult.setLineNum(lineNum);
            robotsTxtValidatorResult.setErrorType(errorType);
            robotsTxtValidatorResult.setLineData(lineData);
            robotsTxtValidatorResult.setErrMessage(errorMsg);
            validationList.add(robotsTxtValidatorResult);
            validationMap.put(lineNum, validationList);
        }
    }

    public boolean checkIsTextPresent(String inputContent, String checkedText) {
        boolean isPresent = false;
        if (inputContent.trim().toLowerCase().startsWith(checkedText)) {
            isPresent = true;
        }
        return isPresent;
    }

    public String getPropertyValue(String inputContent) {
        String textValue = "";
        if (inputContent.indexOf(':') > 0) {
            logger.debug("inputContent.length()" + inputContent.length());
            textValue = inputContent.substring(inputContent.indexOf(':') + 1, inputContent.length());
        }
        return textValue;
    }

    public String getADPropertyValue(String inputContent) {
        String textValue = "";
        if (inputContent.indexOf(':') > 0) {
            logger.debug("inputContent.length()" + inputContent.length());
            textValue = inputContent.substring(inputContent.indexOf(':') + 1, inputContent.length());
        }
        logger.debug("textValue" + textValue);
        return textValue;
    }

    public void RegMatchFields(String inputContent, Map<Integer, List<RobotsTxtValidatorResult>> validationMap,
            int lineNum, String lineData, int errorType, String errorMsg) {
        String tempInputContent = inputContent.trim();
        Pattern validRule = Pattern.compile("^(user-agent|allow|disallow|sitemap|crawl-delay|host)+");
        if (validRule.matcher(tempInputContent).find()) {
            Pattern p = Pattern.compile("^\\s+");
            Matcher m = p.matcher(inputContent); // get a matcher object
            boolean findVal = m.find();
            logger.debug("in matched cond" + lineNum + "......." + findVal);
            if (findVal) {
                logger.debug("in matched cond" + lineNum + "......." + lineData + "......" + lineData + "..." + errorMsg + "....." + findVal);
                addValuesToList(validationMap, lineNum, lineData, errorMsg, errorType);
            }
        } else if (!tempInputContent.equals("")) {
            addValuesToList(validationMap, lineNum, lineData, context.getMessage("robotsValidator.invalidDir", null, Locale.UK), 1);
        }
    }

    public void RegMatchFieldsVersion1(String inputContent, Map<Integer, List<RobotsTxtValidatorResult>> validationMap,
            int lineNum, String lineData, int errorType, String dirname) {
        String errorMsg = context.getMessage("robotsValidator.noSpaceForRecordVal", null, Locale.UK);
        Pattern p = Pattern.compile("^(" + dirname + ":\\s)+");
//		Pattern pattBeginsWithSpace = Pattern.compile("^(\\s"+dirname+":)+");		
        Pattern pattValWithSpace = Pattern.compile("(" + dirname + ":\\s)+");
        boolean findVal = p.matcher(inputContent).find();
        logger.debug("in RegMatchFieldsVersion1 matched cond" + lineNum + "......." + findVal);
        if (!findVal) {
            findVal = pattValWithSpace.matcher(inputContent).find();
//    		logger.info("in RegMatchFieldsVersion1 matched cond after : space "+lineNum+"......."+findVal);
            if (!findVal) {
                addValuesToList(validationMap, lineNum, lineData, errorMsg, errorType);
            }
        }
    }

    public void spclCharsRegExpn(String inputContent, Map<Integer, List<RobotsTxtValidatorResult>> validationMap,
            int lineNum, String lineData, int errorType, String dirname) {
        String errorMsg = context.getMessage("robotsValidator.noSpaceForRecordVal", null, Locale.UK);
        Pattern p = Pattern.compile("^(" + dirname + "\\s:)+");
//			Pattern pattBeginsWithSpace = Pattern.compile("^(\\s"+dirname+":)+");		
        Pattern pattValWithSpace = Pattern.compile("" + dirname + ":\\s+");
        boolean findVal = p.matcher(inputContent).find();
        logger.debug("in RegMatchFieldsVersion1 matched cond" + lineNum + "......." + findVal);
        if (!findVal) {
            findVal = pattValWithSpace.matcher(inputContent).find();
            logger.debug("in RegMatchFieldsVersion1 matched cond after : space " + lineNum + "......." + findVal);
            if (!findVal) {
                addValuesToList(validationMap, lineNum, lineData, errorMsg, errorType);
            }
        }
    }

    public void RegDigitMatch(String inputContent, Map<Integer, List<RobotsTxtValidatorResult>> validationMap,
            int lineNum, String lineData, int errorType, String errorMsg) {
        String tempContent = inputContent.trim();
        Pattern p = Pattern.compile("^[0-9]+$");
        Matcher m = p.matcher(tempContent); // get a matcher object
        if (!m.matches()) {
            addValuesToList(validationMap, lineNum, lineData, errorMsg, errorType);
        }
    }

    public void addInJson2(String existingRobotsTxtContent, HttpServletResponse response) {
        String errorMsg = "";
        net.sf.json.JSONArray arr = null;
        arr = new net.sf.json.JSONArray();
        PrintWriter writer = null;
        if (existingRobotsTxtContent == null || existingRobotsTxtContent.trim().equals("")) {
            errorMsg = "Unable to fetch robots.txt for the Specified URL.";
        }
        try {
            writer = response.getWriter();
            arr.add(0, existingRobotsTxtContent);
            arr.add(1, errorMsg);
            writer.print(arr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writer.print("{success: false}");
        } finally {
            writer.flush();
            writer.close();
        }
    }

}
