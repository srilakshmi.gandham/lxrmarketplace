package lxr.marketplace.robotstxtvalidator;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.admin.automail.EmailTemplateService;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.session.Session;
import lxr.marketplace.user.Login;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.Tool;
import lxr.marketplace.util.ToolService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

@SuppressWarnings("deprecation")
public class RobotsTxtValidatorController extends SimpleFormController {

    private static Logger logger = Logger.getLogger(RobotsTxtValidatorController.class);
    private EmailTemplateService emailTemplateService;
    private ToolService toolService;
    private RobotsTxtValidatorService robotsTxtValidatorService;

    JSONObject userInputJson = null;

    public RobotsTxtValidatorService getRobotsTxtValidatorService() {
        return robotsTxtValidatorService;
    }

    public void setRobotsTxtValidatorService(
            RobotsTxtValidatorService robotsTxtValidatorService) {
        this.robotsTxtValidatorService = robotsTxtValidatorService;
    }

    public EmailTemplateService getEmailTemplateService() {
        return emailTemplateService;
    }

    public void setEmailTemplateService(
            EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    public ToolService getToolService() {
        return toolService;
    }

    public void setToolService(ToolService toolService) {
        this.toolService = toolService;
    }

    @Autowired
    private MessageSource messageSource;

    public RobotsTxtValidatorController() {
        setCommandClass(RobotsTxtValidator.class);
        setCommandName("robotsTxtValidatorTool");
    }
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command, BindException errors) throws Exception {
        HttpSession session = request.getSession();
        Login user = (Login) session.getAttribute("user");
        RobotsTxtValidator robotsTxtValidatorTool = (RobotsTxtValidator) command;
        if (robotsTxtValidatorTool != null) {
            session.setAttribute("robotsTxtValidatorTool", robotsTxtValidatorTool);
        }
        if (user != null) {
            Tool toolObj = new Tool();
            toolObj.setToolId(25);
            toolObj.setToolName("Robots.txt Validator Tool");
            toolObj.setToolLink("robots-txt-validator-tool.html");
            session.setAttribute("toolObj", toolObj);
            session.setAttribute("currentTool", Common.ROBOTS_TXT_VALIDATOR);
            String notWorkingUrl = null;
            if (WebUtils.hasSubmitParameter(request, "getResult")) {
                String finalURL = "", robotTxtCOntent = "";
                String valParam = request.getParameter("getResult");
                Session userSession = (Session) session.getAttribute("userSession");
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                    return mAndV;
                }
                session.removeAttribute("robotsValidatorATE");
                session.removeAttribute("robotsValidtorURLATE");

                if (valParam.equalsIgnoreCase("validateUrlCont")) {
                    ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                    if (robotsTxtValidatorTool != null && robotsTxtValidatorTool.getDomainUrl() != null && !robotsTxtValidatorTool.getDomainUrl().trim().isEmpty()) {
                        //Validate the URL
//                        finalURL = robotsTxtValidatorService.validateURL(robotsTxtValidatorTool.getDomainUrl());
                        finalURL = Common.getUrlValidation(robotsTxtValidatorTool.getDomainUrl().trim(), session);
                        if (finalURL.equals("invalid") || finalURL.equals("redirected")) {
                            session.removeAttribute("validationMap");
                            session.removeAttribute("noError");
                            session.setAttribute("robotValidatorInvalidUrl", true);
                            notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                            mAndV.addObject("notWorkingUrl", notWorkingUrl);
                            robotsTxtValidatorTool.setRobotsContent("");
                            session.removeAttribute("robotsValidatorATE");
                            return mAndV;
                        } else {
                            boolean robotsTxtPresent = Common.checkPresenceOfRobotsTxt(finalURL);
                            if (!robotsTxtPresent) {
                                /*Removing query params or request params from URL and preparing final query URL i.e protocol://HostName */
                                String tempQueryURL = Common.getDomainWithProtocol(finalURL);
                                if (!tempQueryURL.equals(finalURL)) {
                                    finalURL = tempQueryURL;
                                }
                            }
                            session.setAttribute("robotsValidtorURLATE", finalURL);
                            robotsTxtValidatorTool.setDomainUrl(finalURL);
                            if (finalURL.endsWith("/")) {
                                finalURL = finalURL + "robots.txt";
                            } else {
                                finalURL = finalURL + "/robots.txt";
                            }

                            robotTxtCOntent = robotsTxtValidatorService.fetchRobotTxtFileContent(finalURL).trim();
                            robotsTxtValidatorTool.setRobotsContent(robotTxtCOntent);
                            session.setAttribute("robotsValidatorATE", robotTxtCOntent);
                            session.removeAttribute("robotValidatorInvalidUrl");
                            if (robotTxtCOntent.trim().equals("")) {
                                session.removeAttribute("validationMap");
                                session.removeAttribute("noError");
                                session.setAttribute("robotValidatorInvalidUrl", true);
                                notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                                mAndV.addObject("notWorkingUrl", notWorkingUrl);
                                //mAndV.addObject("notWorkingUrl", "Unable to fetch robots.txt for the Specified URL.");
                                return mAndV;
                            }
                        }
                    }
                }
                Common.modifyDummyUserInToolorVideoUsage(request, response);
                userSession.addToolUsage(toolObj.getToolId());
                if (user.getId() == -1) {
                    user = (Login) session.getAttribute("user");
                }

                /*if the request is coming from tool page then only add the user inputs to JSONObject
                                        if the request is coming from mail then do not add the user inputs to JSONObject*/
                boolean requestFromMail = false;
                if (session.getAttribute("requestFromMail") != null) {
                    requestFromMail = (Boolean) session.getAttribute("requestFromMail");
                    session.removeAttribute("requestFromMail");
                }
                if (!requestFromMail && user != null && !user.isAdmin()) {
                    userInputJson = new JSONObject();
                    userInputJson.put("2", robotTxtCOntent);
                }

                session.removeAttribute("noError");
                session.removeAttribute("validationMap");

                if (robotsTxtValidatorTool != null && !"".equals(robotsTxtValidatorTool.getRobotsContent().trim())) {
                    session.removeAttribute("validationMap");
                    session.removeAttribute("noError");
                    Map<Integer, List<RobotsTxtValidatorResult>> validationMap = robotsTxtValidatorService.validateRobotsTxtContent(robotsTxtValidatorTool.getRobotsContent());
                    session.setAttribute("validationMap", validationMap);
                    if (validationMap == null || validationMap.size() <= 0) {
                        session.setAttribute("noError", true);
                    }
                }
                boolean validtorInvalidURl = false;
                if (session.getAttribute("robotValidatorInvalidUrl") != null) {
                    validtorInvalidURl = (Boolean) session.getAttribute("robotValidatorInvalidUrl");
                }
                if (validtorInvalidURl) {
                    robotsTxtValidatorTool.setDomainUrl("");
                }
                session.setAttribute("robotsTxtValidatorTool", robotsTxtValidatorTool);
                //session.setAttribute("robotsTxtValidatorToolATE",robotsTxtValidatorTool);
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                mAndV.addObject("getSuccess", "success");
//					mAndV.addObject("notWorkingUrl","");
                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "import")) {
                logger.debug("Query URL: " + robotsTxtValidatorTool.getDomainUrl());
                session.removeAttribute("validationMap");
                String finalURL = "", robotTxtCOntent = "";
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                if (robotsTxtValidatorTool.getDomainUrl() != null && !robotsTxtValidatorTool.getDomainUrl().equals("")) {
                    //Validate the URL
                    finalURL = robotsTxtValidatorService.validateURL(robotsTxtValidatorTool.getDomainUrl());
                    if (finalURL.endsWith("/")) {
                        finalURL = finalURL + "robots.txt";
                    } else {
                        finalURL = finalURL + "/robots.txt";
                    }
                    logger.info("RobotsTxtValidatorController finalURL is........." + finalURL);
                    session.removeAttribute("robotsValidatorATE");
                    robotTxtCOntent = robotsTxtValidatorService.fetchRobotTxtFileContent(finalURL);
                    robotsTxtValidatorTool.setRobotsContent(robotTxtCOntent);
                    session.setAttribute("robotsValidatorATE", robotTxtCOntent);
                    session.setAttribute("robotsTxtValidatorTool", robotsTxtValidatorTool);
                    if (robotTxtCOntent.trim().equals("")) {
                        notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                        mAndV.addObject("notWorkingUrl", notWorkingUrl);
                        /*mAndV.addObject("notWorkingUrl", "Unable to fetch robots.txt for the Specified URL.");*/
                    }
                }
                mAndV.addObject("getSuccess", "success");
                return mAndV;

            } else if (WebUtils.hasSubmitParameter(request, "clearAll")) {
                session.removeAttribute("validationMap");
                session.removeAttribute("noError");
                session.removeAttribute("robotsValidatorATE");
                session.removeAttribute("robotsTxtValidatorTool");
                session.removeAttribute("robotsValidtorURLATE");
                session.removeAttribute("robotValidatorInvalidUrl");
                robotsTxtValidatorTool = new RobotsTxtValidator();
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                mAndV.addObject("clearAll", "clearAll");
                mAndV.addObject("notWorkingUrl", "");

                return mAndV;
            } else if (WebUtils.hasSubmitParameter(request, "robotsValidATE")) {
                logger.info("For request handle robotsATE");
                /*To  Handle Ask Our Expert request*/
                Session userSession = (Session) session.getAttribute("userSession");
                String robotsValidURL = request.getParameter("robotsValidURL");

                /*To check issues related to analysis: Help in Creating Robots.txt.Refer Tool Based Analysis.*/
                boolean robotsDoesNotExisted = false;
                String preparRobotValidateTxt = "empty";
                if (session.getAttribute("robotsValidatorATE") != null) {
                    preparRobotValidateTxt = (String) session.getAttribute("robotsValidatorATE");
                }
                if (!preparRobotValidateTxt.equals("empty")) {
                    if (preparRobotValidateTxt.trim().equals("")) {
                        /* Robots.txt is not existed for url, issue existed for analysis: Help in Creating Robots.txt*/
                        robotsDoesNotExisted = true;
                        logger.info("Issue related to analysis: Help in Creating Robots.txt.of Robots.txt Validator .Refer Tool Based Analysis is exsited");
                    }
                }
                /*End of Help in Creating Robots.txt.*/
 /*To check issues related below analysis:
                                    1)Help in fixing Errors and Warnings in Robots.txt 
                                    Refer Tool Based Analysis sheet.*/
                int totalErrors = 0, totalWarnings = 0;
                RobotsTxtValidator robotsTxtValidatorToolATE = null;
                if (session.getAttribute("robotsTxtValidatorTool") != null) {
                    robotsTxtValidatorToolATE = (RobotsTxtValidator) session.getAttribute("robotsTxtValidatorTool");
                    if (robotsTxtValidatorToolATE.getRobotsContent() != null && !robotsTxtValidatorToolATE.getRobotsContent().trim().equals("")) {
                        session.removeAttribute("validationMap");
                        session.removeAttribute("noError");
                        Map<Integer, List<RobotsTxtValidatorResult>> validationMap = robotsTxtValidatorService.validateRobotsTxtContent(robotsTxtValidatorTool.getRobotsContent());
                        for (Map.Entry<Integer, List<RobotsTxtValidatorResult>> robotsValidateEntry : validationMap.entrySet()) {
                            List<RobotsTxtValidatorResult> validationList = robotsValidateEntry.getValue();
                            if (validationList != null && validationList.size() > 0) {
                                for (RobotsTxtValidatorResult resultObj : validationList) {
                                    if (resultObj.getErrorType() == 1) {
                                        totalErrors++;
                                    } else if (resultObj.getErrorType() == 2) {
                                        totalWarnings++;
                                    }
                                }/*End of resultObj.*/
                            }/* End of validationList. */
                        }/* End of robotsValidateEntry. */
                    }
                }
                LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
                String toolIssues = "";
                String questionInfo = "";
                if (robotsDoesNotExisted) {
                    questionInfo = "Need help in fixing following issues with my website '" + robotsValidURL + "'.\n\n";
                    toolIssues = "<ul>";
                    if (preparRobotValidateTxt.trim().equals("")) {
                        questionInfo += "- Creating Robots.txt file from my website.\n";
                        toolIssues += "<li>" + "Your website does not currently have a robots.txt file. "
                                + "Seach engines currently have access to index all of the pages on your website. "
                                + "</li>";
                    }
                    toolIssues += "</ul>";
                }
                if ((totalErrors != 0) || (totalWarnings != 0)) {
                    int totalCount = totalErrors + totalWarnings;
                    questionInfo = "Need help in fixing following issues with my website '" + robotsValidURL + "'.\n\n";
                    toolIssues = "<ul>";
                    questionInfo += "- Fixing errors and warnings from Robots.txt file.\n";
                    toolIssues += "<li>" + "Your robots.txt file has <span style='color:red;'>" + totalCount + "</span>  issue's."
                            + " Search engines may not correctly index your site due to improper formatting."
                            + "</li>";

                    toolIssues += "</ul>";
                }
                lxrmAskExpert.setDomain(robotsValidURL);
                if (userInputJson != null) {
                    lxrmAskExpert.setOtherInputs(userInputJson.toString());
                }
                lxrmAskExpert.setQuestion(questionInfo);
                lxrmAskExpert.setIssues(toolIssues);
                lxrmAskExpert.setExpertInfo(questionInfo);
                lxrmAskExpert.setToolName(toolObj.getToolName());
                lxrmAskExpertService.addInJson(lxrmAskExpert, response);

//                                    if the request is not from  mail then we should add this input to the session object
                if (userInputJson != null) {
                    String toolUrl = "/" + toolObj.getToolLink();
                    userSession.addUserAnalysisInfo(user.getId(), toolObj.getToolId(), robotsValidURL, toolIssues,
                            questionInfo, userInputJson.toString(), toolUrl);
                    userInputJson = null;
                }

                return null;
            }/*End of AskOurExpert*/ else if (WebUtils.hasSubmitParameter(request, "backToSuccessPage")) {
                RobotsTxtValidator robotsTxtValidatorToolATE = null;
                if (session.getAttribute("robotsTxtValidatorTool") != null) {
                    robotsTxtValidatorToolATE = (RobotsTxtValidator) session.getAttribute("robotsTxtValidatorTool");
                } else {
                    robotsTxtValidatorToolATE = new RobotsTxtValidator();
                }
                String tempRobotValidURl = "";
                if (session.getAttribute("robotsValidtorURLATE") != null) {
                    tempRobotValidURl = (String) session.getAttribute("robotsValidtorURLATE");
                }
                if (!tempRobotValidURl.equals("")) {
                    robotsTxtValidatorToolATE.setDomainUrl(tempRobotValidURl);
                } else {
                    tempRobotValidURl = robotsTxtValidatorToolATE.getDomainUrl();
                    if (tempRobotValidURl.contentEquals("robots.txt")) {
                        tempRobotValidURl = tempRobotValidURl.replaceAll("robots.txt", " ").trim();
                    }
                    robotsTxtValidatorToolATE.setDomainUrl(tempRobotValidURl);
                }
                String finalURL = "", robotTxtCOntent = "";
                if (WebUtils.hasSubmitParameter(request, "loginRefresh")) {
                    session.setAttribute("loginrefresh", "loginrefresh");
                    ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
                    return mAndV;
                }
                boolean validtorInvalidURl = false;
                if (session.getAttribute("robotValidatorInvalidUrl") != null) {
                    validtorInvalidURl = (Boolean) session.getAttribute("robotValidatorInvalidUrl");
                }
                if (validtorInvalidURl) {
                    if (robotsTxtValidatorToolATE.getDomainUrl() != null && !robotsTxtValidatorToolATE.getDomainUrl().equals("")) {
                        finalURL = robotsTxtValidatorService.validateURL(robotsTxtValidatorToolATE.getDomainUrl());
                        if (finalURL.endsWith("/")) {
                            finalURL = finalURL + "robots.txt";
                        } else {
                            finalURL = finalURL + "/robots.txt";
                        }
                        robotTxtCOntent = robotsTxtValidatorService.fetchRobotTxtFileContent(finalURL).trim();
                        robotsTxtValidatorTool.setRobotsContent(robotTxtCOntent);
                        if (robotTxtCOntent.trim().equals("")) {
                            session.removeAttribute("validationMap");
                            session.removeAttribute("noError");
                            ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorToolATE);
                            notWorkingUrl = messageSource.getMessage("URL.error", null, Locale.US);
                            mAndV.addObject("notWorkingUrl", notWorkingUrl);
//                            mAndV.addObject("notWorkingUrl", "Unable to fetch robots.txt for the Specified URL.");
                            return mAndV;
                        }
                    }
                }
                session.removeAttribute("noError");
                session.removeAttribute("validationMap");
                if (robotsTxtValidatorToolATE.getRobotsContent() != null && !robotsTxtValidatorToolATE.getRobotsContent().trim().equals("")) {
                    session.removeAttribute("validationMap");
                    session.removeAttribute("noError");
                    Map<Integer, List<RobotsTxtValidatorResult>> validationMap = robotsTxtValidatorService.validateRobotsTxtContent(robotsTxtValidatorToolATE.getRobotsContent());
                    session.setAttribute("validationMap", validationMap);
                    if (validationMap == null || validationMap.size() <= 0) {
                        session.setAttribute("noError", true);
                    }
                }
                session.setAttribute("robotsTxtValidatorTool", robotsTxtValidatorTool);
                ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorToolATE);
                mAndV.addObject("getSuccess", "success");
                return mAndV;
            }/*End of back to sucess page request.*/
            robotsTxtValidatorTool = new RobotsTxtValidator();
            session.removeAttribute("validationMap");
            session.removeAttribute("noError");
            session.removeAttribute("notWorkingUrl");
            ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
            return mAndV;
        }
        ModelAndView mAndV = new ModelAndView(getFormView(), "robotsTxtValidatorTool", robotsTxtValidatorTool);
        session.removeAttribute("notWorkingUrl");
        return mAndV;
    }

    protected boolean isFormSubmission(HttpServletRequest request) {
        return true;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        RobotsTxtValidator robotsTxtValidatorTool = (RobotsTxtValidator) session.getAttribute("robotsTxtValidatorTool");
        if (robotsTxtValidatorTool == null) {
            robotsTxtValidatorTool = new RobotsTxtValidator();
        }
        return robotsTxtValidatorTool;
    }

}
