/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

/**
 *
 * @author sagar
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/mobile-report-download")
public class MobileReportDownloadController {

    private static final Logger LOGGER = Logger.getLogger(MobileReportDownloadController.class);

    @Autowired
    private String downloadFolder;

    /**
     * Size of a byte buffer to read/write file
     */
    private static final int BUFFER_SIZE = 1096;

    /**
     * Method for handling file download request from client
     *
     * @param request
     * @param response
     * @param fileName
     *
     */
    @RequestMapping(method = RequestMethod.GET)
    public void generateServiceAnalysisReportForDownloadRequest(HttpServletRequest request,
            HttpServletResponse response, @RequestParam("fileName") String fileName) {
        LOGGER.info("The requested fileName:: " + fileName);
        boolean isReportDownloaded = Boolean.FALSE;
        String fileLocation = null;
        File downloadableFile = null;
        ServletOutputStream outStream = null;
        if (fileName != null && !fileName.trim().isEmpty()) {
            fileLocation = downloadFolder + fileName;
        }
        if (fileLocation != null) {
            downloadableFile = new File(fileLocation);
        }
        ServletContext context = request.getServletContext();
        if (downloadableFile != null) {
            try {
                try (FileInputStream inputStream = new FileInputStream(downloadableFile)) {
                    // get MIME type of the file
                    String mimeType = context.getMimeType(fileLocation);
                    if (mimeType == null) {
                        // set to binary type if MIME mapping not found
                        mimeType = "application/octet-stream";
                    }
                    // set content attributes for the response
                    response.setContentType(mimeType);
                    response.setContentLength((int) downloadableFile.length());

                    // set headers for the response
                    String headerKey = "Content-Disposition";
                    String headerValue = null;
                    if (downloadableFile.getName() != null && !downloadableFile.getName().trim().isEmpty()) {
                        headerValue = String.format("attachment; filename=\"%s\"", downloadableFile.getName());
                    }
                    if (headerValue != null) {
                        response.setHeader(headerKey, headerValue);
                    }
                    LOGGER.info("Preparing response object for mobile apps analysis report");
                    // get output stream of the response
                    outStream = response.getOutputStream();
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int bytesRead = -1;
                    // write bytes read from the input stream into the output stream
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }
                    inputStream.close();
                }
                outStream.close();
                isReportDownloaded = Boolean.TRUE;
            } catch (IOException e) {
                LOGGER.error("IOException in doDownload for mobile app reports cause:: ", e);
            }
        }
        LOGGER.info("Request to get report for app analysis report is completed and request is :: " + (isReportDownloaded ? "success" : "failed"));
    }
}
