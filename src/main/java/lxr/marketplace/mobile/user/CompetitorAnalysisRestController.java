/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpSession;
import lxr.marketplace.competitoranalysis.CompetitorAnalysisTool;
import lxr.marketplace.util.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author sagar
 */
@Controller
@RequestMapping("/mobile-competitor-analysis")
public class CompetitorAnalysisRestController {

//    private static final Logger LOGGER = Logger.getLogger(CompetitorAnalysisRestController.class);
    @Autowired
    private CompetitorAnalysisRestService competitorAnalysisRestService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> processCompetitorAnalysisRestServices(HttpSession session, @ModelAttribute("competitorAnalysisTool") CompetitorAnalysisTool competitorAnalysisObj) {
        Map<String, Object> resultHashMap = new LinkedHashMap<>();
        if (competitorAnalysisObj.getYourUrl() != null && competitorAnalysisObj.getDomainUrlList() != null && competitorAnalysisObj.getSearchPhrase() != null) {
            String userQueryURL = Common.getUrlValidation(competitorAnalysisObj.getYourUrl().trim(), session);
            String errorMessage = null;
            if (userQueryURL.equals("invalid") || userQueryURL.equals("redirected")) {
                errorMessage = competitorAnalysisObj.getYourUrl();
            }
            List<String> competitorURLs = new ArrayList();
            for (String competitorURL : competitorAnalysisObj.getDomainUrlList()) {
                if (competitorURL != null && !competitorURL.trim().isEmpty()) {
                    String competitorQueryURL = Common.getUrlValidation(competitorURL.trim(), session);
                    if (competitorQueryURL.equals("invalid") || competitorQueryURL.equals("redirected")) {
                        errorMessage = errorMessage != null ? errorMessage.concat(", ").concat(competitorURL) : competitorURL;
                    } else {
                        competitorURLs.add(competitorQueryURL);
                    }
                }
            }
            if (errorMessage == null) {
                if (competitorAnalysisObj.getSearchPhrase() != null && !competitorAnalysisObj.getSearchPhrase().trim().isEmpty()) {
                    competitorAnalysisObj.setSearchPhrase(competitorAnalysisObj.getSearchPhrase().trim());
                }
                resultHashMap = competitorAnalysisRestService.getToolAnalysis(userQueryURL.trim(), competitorURLs, competitorAnalysisObj.getSearchPhrase());
            } else {
                resultHashMap.put("success", 0);
                resultHashMap.put("message", errorMessage.concat(" " + messageSource.getMessage("MultipleURL.error", null, Locale.US)));
            }
        } else {
            resultHashMap.put("success", 0);
            resultHashMap.put("message", "Invalid inputs");
        }
        return resultHashMap;
    }

}
