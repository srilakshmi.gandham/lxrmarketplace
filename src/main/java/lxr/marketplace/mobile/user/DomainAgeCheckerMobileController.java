/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.WhoisDomainInfoFetch;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author anil
 */

@Controller
public class DomainAgeCheckerMobileController {

    private static Logger logger = Logger.getLogger(DomainAgeCheckerMobileController.class);

    @RequestMapping(value = "/domain-age-info.json")
    @ResponseBody
    public Object getDomainAgeInfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("domain") String domain) {
        JSONObject domainAgeInfoJson = new JSONObject();
        int statusCode = 0;
        try {
            if (!domain.equals("")) {
                StringBuilder redirectedUrl = new StringBuilder("");
		StringBuilder finalModifiedUrl = new StringBuilder("");
                statusCode = Common.appendProtocalAndCheckStausCode(domain, redirectedUrl, finalModifiedUrl);
                if (statusCode == 0 || statusCode >= 400) {
                    domainAgeInfoJson.put("error", "Your URL is not working, please check the URL.");
                } else {
                    domainAgeInfoJson = WhoisDomainInfoFetch.getJsonRoboWhois(domain);
                }
            } else {
                domainAgeInfoJson.put("error", "Please enter URL.");
            }
            addResultToJsonResp(domainAgeInfoJson, response);
        } catch (JSONException e) {
            logger.info("Exception when fetching the domain age for: " + domain + " - " + e.getMessage());
        }
        return null;
    }
    public void addResultToJsonResp(JSONObject result, HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            writer.print(result);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            writer.flush();
            writer.close();
        }
    }
}
