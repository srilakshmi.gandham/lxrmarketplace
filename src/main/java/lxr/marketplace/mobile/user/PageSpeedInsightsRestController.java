/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lxr.marketplace.pagespeedinsights.PageSpeedInsights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author vidyasagar.korada
 */
@Controller
@RequestMapping("/mobile-page-speed-insights-analysis")
public class PageSpeedInsightsRestController {

    @Autowired
    private PageSpeedInsightsRestService pageSpeedInsightsRestService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> submitGooglePageSpeedInsightsForURL(HttpSession session, HttpServletRequest request, @ModelAttribute("pageSpeedRestModel") PageSpeedRestModel pageSpeedRestModel) {
        Map<String, Object> pageSpeedResult = new LinkedHashMap<>();
        boolean invalidInputs = Boolean.FALSE;
        List<String> devicesList = null;
        List<String> categoriesList = null;
        /*Validations*/
        invalidInputs = (pageSpeedRestModel.getQueryWebPageURL() != null && !pageSpeedRestModel.getQueryWebPageURL().trim().equals("")) ? Boolean.FALSE : Boolean.TRUE;
        if (pageSpeedRestModel.getDevices() != null && pageSpeedRestModel.getDevices().length() > 0 && !pageSpeedRestModel.getDevices().trim().equals("")) {
            if (pageSpeedRestModel.getDevices().contains(",")) {
                devicesList = Arrays.asList(pageSpeedRestModel.getDevices().trim().split(","));
            } else {
                devicesList = new ArrayList<>();
                devicesList.add(pageSpeedRestModel.getDevices().trim());
            }
        } else {
            invalidInputs = Boolean.TRUE;
        }

        if (pageSpeedRestModel.getCategories() != null && pageSpeedRestModel.getCategories().length() > 0 && !pageSpeedRestModel.getCategories().trim().equals("")) {
            if (pageSpeedRestModel.getCategories().contains(",")) {
                categoriesList = Arrays.asList(pageSpeedRestModel.getCategories().trim().split(","));
            } else {
                categoriesList = new ArrayList<>();
                categoriesList.add(pageSpeedRestModel.getCategories().trim());
            }
        } else {
            invalidInputs = Boolean.TRUE;
        }
        if (!invalidInputs ) {
            List<PageSpeedInsights> pageSpeedInsightsModelList = null;
            String tempQueryWebPageURL = (!pageSpeedRestModel.getQueryWebPageURL().startsWith("http")) ? "http://".concat(pageSpeedRestModel.getQueryWebPageURL()) : pageSpeedRestModel.getQueryWebPageURL();
            pageSpeedInsightsModelList = pageSpeedInsightsRestService.pullGooglePageSpeedInsightsForURL(tempQueryWebPageURL, devicesList, categoriesList);
            if (pageSpeedInsightsModelList != null && !pageSpeedInsightsModelList.isEmpty()) {

                pageSpeedResult.put("Success", Boolean.TRUE);
                pageSpeedResult.put(pageSpeedRestModel.getQueryWebPageURL(), pageSpeedInsightsModelList);

//                String downloadFolder = "/disk2/lxrmarketplace/download/";
//                String fileName = "LXRMarket_PSI_" + LocalDate.now().toString();
//                String insightFileName = pageSpeedInsightReportService.generatePageSpeedInsightsAuditReports(pageSpeedInsightsModelList, downloadFolder, fileName);
//                Common.sendReportThroughMail(request, downloadFolder, insightFileName, "vidyasagar.korada@netelixir.com", "Sample PageSpeed Insights");
            } else {
                invalidInputs = Boolean.TRUE;
            }
        }
        if (invalidInputs) {
            pageSpeedResult.put("Success", Boolean.TRUE);
            pageSpeedResult.put("Error", "Invalid inputs");
        }
        return pageSpeedResult;
    }
}
