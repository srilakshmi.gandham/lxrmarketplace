/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

/**
 *
 * @author vidyasagar.korada
 */
public class PageSpeedRestModel {
    
    private String queryWebPageURL;
    /*Devices list Desktop/ Mobile */
    private String devices;
    /*Categories list Performance, Progressive Web App, Accessibility, Best Practices, and SEO  */
    private String categories;

    public String getQueryWebPageURL() {
        return queryWebPageURL;
    }

    public void setQueryWebPageURL(String queryWebPageURL) {
        this.queryWebPageURL = queryWebPageURL;
    }

    public String getDevices() {
        return devices;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }
    
    
    
}
