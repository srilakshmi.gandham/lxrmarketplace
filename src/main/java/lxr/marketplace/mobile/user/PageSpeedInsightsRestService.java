/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lxr.marketplace.pagespeedinsights.PageSpeedInsights;
import lxr.marketplace.pagespeedinsights.PageSpeedInsightsService;
import lxr.marketplace.pagespeedinsights.PageSpeedInsightsTask;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vidyasagar.korada
 */
@Service
public class PageSpeedInsightsRestService {

    private static final Logger LOGGER = Logger.getLogger(PageSpeedInsightsRestService.class);

    @Autowired
    private PageSpeedInsightsService pageSpeedInsightsService;

    /*
     Algorithm
     Input: webPageURL, DeviceList: Desktop/Mobile, category: performance,seo
     #1: Prepare Endpoint URL
     #2: Intialize Excuter services 
     */
    public List<PageSpeedInsights> pullGooglePageSpeedInsightsForURL(String queryWebPageURL,
            List<String> devicesList, List<String> categoryList) {

        List<PageSpeedInsights> pageSpeedInsightsList = new ArrayList<>();
        List<String> endPointURLList = pageSpeedInsightsService.getQueryEndPointURLs(queryWebPageURL, devicesList, categoryList);;
        if (endPointURLList != null && !endPointURLList.isEmpty()) {
            ExecutorService executorService = Executors.newFixedThreadPool(endPointURLList.size());
            CountDownLatch latchObj = new CountDownLatch(endPointURLList.size());
            List<Future<PageSpeedInsights>> pageSpeedInsightsFutureList = new ArrayList<>();
            if (!endPointURLList.isEmpty()) {
                endPointURLList.forEach((apiEndPointURL) -> {
                    pageSpeedInsightsFutureList.add(executorService.submit(new PageSpeedInsightsTask(apiEndPointURL, latchObj, pageSpeedInsightsService)));
                });
            }
            try {
                latchObj.await();
            } catch (InterruptedException ex) {
                LOGGER.error("Interrupted | Execution exception latch object count cause:", ex);
            }
            if (!pageSpeedInsightsFutureList.isEmpty()) {
                pageSpeedInsightsFutureList.forEach((pageSpeedInsights) -> {
                    if (pageSpeedInsights != null) {
                        try {
                            pageSpeedInsightsList.add(pageSpeedInsights.get());
                        } catch (InterruptedException | ExecutionException e) {
                            LOGGER.error("Interrupted | Execution exception pulling pagespeed insights object from future obj cause:", e);
                        }
                    }
                });
            }
            executorService.shutdown();
        }
        return pageSpeedInsightsList;
    }
}
