/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.dnslookup.DnsLookupResult;
import lxr.marketplace.util.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import lxr.marketplace.dnslookup.DnsLookupService;
import org.springframework.web.bind.annotation.RequestMethod;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author sagar
 */
@Controller
@RequestMapping(value = "/mobile-dnslookup")
public class DNSLookUPRestController {

    private static final Logger LOGGER = Logger.getLogger(DNSLookUPRestController.class);

    @Autowired
    private DnsLookupService dnsLookService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public JSONObject processDNSCheckerAPPService(HttpServletRequest request, HttpServletResponse response, @RequestParam("queryDomain") String queryDomain) throws IOException {
        JSONObject responseObj = new JSONObject();
        int success = 0;
        if (queryDomain != null && !queryDomain.equals("")) {
            /*int statusCode = 0;
            StringBuilder redirectedUrl = new StringBuilder("");
            StringBuilder finalModifiedUrl = new StringBuilder("");
            statusCode = Common.appendProtocalAndCheckStausCode(queryDomain, redirectedUrl, finalModifiedUrl);*/
            String finalURL = Common.getUrlValidation(queryDomain.trim(), null);
            LOGGER.info("DNSLookUP Service request for  query domain : " + queryDomain+", and redirected domain: "+finalURL);
//            if (statusCode == 0 || statusCode >= 400) {
            if (finalURL != null && (finalURL.equals("invalid") || finalURL.equals("redirected"))) {
                responseObj.put("error", messageSource.getMessage("URL.error", null, Locale.US));
            } else if (finalURL != null) {
                DnsLookupResult dnsLookUpResult = dnsLookService.getResult(finalURL);
                if (dnsLookUpResult != null) {
                    success = 1;
                    responseObj.put("DnsLookupResult", dnsLookService.getDNSResultJSON(dnsLookUpResult));
                } else {
                    responseObj.put("error", "Unable to analyze, please try again later.");
                }
            }
        } else {
            responseObj.put("error", "Please provide URL.");
        }
        responseObj.put("success", success);
        return responseObj;
    }
}
