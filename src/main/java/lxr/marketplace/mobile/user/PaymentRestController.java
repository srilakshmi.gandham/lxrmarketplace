/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import lxr.marketplace.payment.LXRMTransaction;
import lxr.marketplace.payment.PaymentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NE16T1213
 */
@Service
public class PaymentRestController {

    @Autowired
    PaymentRestService paymentRestService;

    public LXRMTransaction paymentWithACard(PaymentModel paymentModel) {
        LXRMTransaction lXRMTransaction = null;
        lXRMTransaction = paymentRestService.processPaymentWithUserCard(paymentModel);
        return lXRMTransaction;
    }
}
