/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.spiderview.SpiderViewResult;
import lxr.marketplace.spiderview.SpiderViewService;
import lxr.marketplace.spiderview.SpiderViewTool;
import lxr.marketplace.user.MobileUserService;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author netelixir
 */
@Controller
@RequestMapping("/mobileWebAnalysis.html")
public class WebPageAnalysisMobileController  {
static final String USER_AGENT = "Mozilla/5.0 (compatible; LXRbot/1.0;http://www.lxrmarketplace.com/,support@lxrmarketplace.com)";
	private static Logger logger = Logger.getLogger(WebPageAnalysisMobileController.class);
	private MobileUserService mobileUserService;
	private SpiderViewService spiderViewService;
        
        @Autowired
	public void setSpiderViewService(SpiderViewService spiderViewService) {
		this.spiderViewService = spiderViewService;
	}
        @Autowired
	public void setMobileUserService(MobileUserService mobileUserService) {
		this.mobileUserService = mobileUserService;
	}

        @RequestMapping(method = RequestMethod.GET) 
        public @ResponseBody SpiderViewResult getSiteGraderService(HttpServletRequest request, HttpServletResponse response){
            
            String finalURL = null;
            
            String domainUrl = request.getParameter("url");
            if(domainUrl != null){
                HttpSession session = request.getSession();
                SpiderViewTool spiderView = new SpiderViewTool();
                spiderView.setUrl(domainUrl);
                finalURL =Common.checkURLAndGetHTMLV2(domainUrl);
                if(finalURL.equals("too many redirection") || finalURL.equals("invalid") || 
    			finalURL.equals("")){
                    mobileUserService.addErrorToJsonResp(0, response);
                }
                else{
                    boolean jsoupconnerror = spiderViewService.getSpiderViewResults( spiderView, request , finalURL);
                    if(jsoupconnerror) {
                        mobileUserService.addErrorToJsonResp(0, response);
                    } else if(!jsoupconnerror && spiderViewService.getSvresult() != null){
//                        return spiderViewService.getSvresult();
                        mobileUserService.addDataToJsonResp(spiderViewService.getSvresult(), response);
                    }
                }
            }
            return null;
        }
}
