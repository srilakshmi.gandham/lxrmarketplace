/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.Locale;
import lxr.marketplace.payment.GatewayService;
import lxr.marketplace.payment.LXRMTransaction;
import lxr.marketplace.payment.PaymentModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author PaymentService
 */
@Service
public class PaymentRestService {

    private static final Logger LOGGER = Logger.getLogger(PaymentRestService.class);

    @Autowired
    @Qualifier("brainTreePaymentService")
    GatewayService paymentGateway;

    @Autowired
    MessageSource messageSource;

    LXRMTransaction processPaymentWithUserCard(PaymentModel paymentModel) {
        PaymentModel savedCard = null;
        LXRMTransaction lxrTransaction = null;
        String errorMessage = null;
        try {
            savedCard = paymentGateway.authorizeUserPaymentCard(paymentModel);
            if (savedCard != null) {
//            LOGGER.info("Authrozation success for: " + savedCard.getBillingUserName());
                lxrTransaction = chargeAmountForService(savedCard);
            }
        } catch (Exception e) {
            errorMessage = e.getMessage();
            LOGGER.error("Exception in processPaymentWithUserCard cause:: "+ e.getMessage());
        }
        if (lxrTransaction == null) {
            lxrTransaction = new LXRMTransaction();
            if (savedCard == null) {
                lxrTransaction.setState(GatewayService.AUTH_FAILURE);
                if (errorMessage == null) {
                    errorMessage = GatewayService.AUTH_FAILURE;
                }
            } else {
                lxrTransaction.setState(GatewayService.CAPTURE_FAILURE);
                if (lxrTransaction.getFailureMessage() == null) {
                    errorMessage = GatewayService.CAPTURE_FAILURE;
                }
            }
            lxrTransaction.setFailureMessage(messageSource.getMessage("lxrm.payment.validation.error", null, Locale.US).concat("  ").concat(errorMessage));
        }
        return lxrTransaction;
    }

    private LXRMTransaction chargeAmountForService(PaymentModel paymentModel) {
        LXRMTransaction lxrTransaction = paymentGateway.chargeAmount(paymentModel);
        return lxrTransaction;
    }

}
