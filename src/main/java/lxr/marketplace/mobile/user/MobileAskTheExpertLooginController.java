/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.user.Signup;
import lxr.marketplace.user.SignupService;
import lxr.marketplace.util.Common;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
@RequestMapping("/mobile-ate-user")
public class MobileAskTheExpertLooginController {

    private static Logger logger = Logger.getLogger(MobileAskTheExpertLooginController.class);

    @Autowired
    private SignupService signupService;

    //Mobile ATE login
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    void login(HttpServletRequest request, HttpServletResponse response, @RequestParam String name,
            @RequestParam String userName) throws JSONException {
        long userId = 0;
        // checking for existing user
        Signup existingUser = signupService.getUserDetails(userName, 0);
        if (existingUser == null) {
            //checking wheather the user is guest user or not
            existingUser = signupService.getUserDetails(userName, 1);
            boolean islocal = Common.checkLocal(request);
            Signup signUp = new Signup();
            signUp.setName(name);
            signUp.setEmail(userName);
            signUp.setPassword(Common.generatePassword());
            signUp.setActivationId(Common.generateActivationId());
            if (existingUser == null) {
                userId = signupService.insertNewUser(signUp, islocal);
            } else {
                userId = existingUser.getId();
                signUp.setId(userId);
                signupService.updateGuestUser(signUp, islocal);
            }
            String scheme = request.getScheme();
            String server = request.getServerName();
            String url = scheme + "://" + server;
            signupService.sendMail(signUp, url);
        } else {
            userId = existingUser.getId();
        }

        JSONObject obj = new JSONObject();
        obj.put("userId", userId);
        addJsonFormat(obj, response);

    }

//    //Mobile ATE signup
//    @RequestMapping(value = "/signup", method = RequestMethod.POST, produces = "application/json")
//    public @ResponseBody
//    void signUp(HttpServletRequest request, HttpServletResponse response, @ModelAttribute Signup signup) throws JSONException {
//        long userId = 0;
//        String message = "";
//        Signup existingUser = signupService.caseInsensitiveFind(signup.getEmail());
//        if (existingUser == null) {
//            boolean islocal = Common.checkLocal(request);
//            userId = signupService.insertNewUser(signup, islocal);
//        } else {
//            message = "There is an existing account associated with this email. ";
//        }
//
//        JSONObject obj = new JSONObject();
//        obj.put("message", message);
//        obj.put("userId", userId);
//
//        addJsonFormat(obj, response);
//    }
//
//    //Mobile ATE forgot password
//    @RequestMapping(value = "/forgot-password", method = RequestMethod.POST, produces = "application/json")
//    public @ResponseBody
//    void forgotPassword(HttpServletResponse response, @RequestParam String userName) throws JSONException {
//        Login user = forgotPasswordService.find(userName);
//        String message = "";
//        if (user != null) {
//            SendMail sendMail = new SendMail();
//            sendMail.SendMailForgotPasswordMail(userName, supportMail, "Password update", "", user.getName(), userName, user.getPassword());
//            message = "Password has been mailed. Please check your mail account for password. ";
//        } else {
//            message = "Your email is not found in our records. Please enter valid email.";
//        }
//
//        JSONObject obj = new JSONObject();
//        obj.put("message", message);
//        addJsonFormat(obj, response);
//    }
    private void addJsonFormat(JSONObject obj, HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            writer.print(obj);
        } catch (IOException e) {
            logger.error("Exception on converting json format:", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            writer.flush();
            writer.close();
        }
    }
}
