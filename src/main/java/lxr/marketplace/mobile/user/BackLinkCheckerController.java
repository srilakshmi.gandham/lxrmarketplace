/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefRefDomains;
import lxr.marketplace.inboundlinkchecker.BackLinksInsertingTask;
import lxr.marketplace.inboundlinkchecker.InboundLinkCheckerService;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ServiceAuthEncryption;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author santosh
 */
@Controller
public class BackLinkCheckerController {

    private static final Logger LOGGER = Logger.getLogger(BackLinkCheckerController.class);
    @Autowired
    private InboundLinkCheckerService inboundLinkCheckerService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/backlinks", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject getBacklinkData(HttpServletRequest request, HttpServletResponse response, @RequestParam("domain") String queryDomain,
            @RequestParam("type") String type) {

        long count;
        switch (type) {
            case "Mobile":
                count = Common.BACK_LINK_COUNT;
                break;
            case "LXRMarketplace":
                count = Common.BACK_LINK_COUNT;
                break;
            case "LXRSEO":
                count = Common.BACK_LINK_COUNT;
                break;
            default:
                count = Long.parseLong(type);
                break;
        }
        JSONObject backLinksData = new JSONObject();
        LOGGER.info("Backlinks request came for:" + queryDomain + " , count:" + count);
        try {
            if (queryDomain != null && !queryDomain.isEmpty()) {
//                  try {
//                    /*Encode the url's for Ecommerce type  to prevent 404 status code for working url.*/
//                    queryDomain = URLDecoder.decode(queryDomain, "UTF-8");
//                } catch (UnsupportedEncodingException e) {
//                    queryDomain = queryDomain;
//                    LOGGER.error("UnsupportedEncodingException in getCategoryString for queryPathURL: ", e);
//                }
                String encryptCode = request.getHeader("LXRM_SEO_BACKLINKS");
                if (encryptCode.equalsIgnoreCase(ServiceAuthEncryption.crypt(queryDomain))) {
                    /*
                    *domainName:contains value root domain only i.e netelixir.com.
                    *queryDomain contains value valid and final redirected domain i.e https://www.netelixir.com
                     */
 /*Not validating ULR for LXRSEO because they are already validated*/
                    if (!type.equalsIgnoreCase("LXRSEO") && count == Common.BACK_LINK_COUNT) {
                        HttpSession session = request.getSession();
                        queryDomain = Common.getUrlValidation(queryDomain, session);
                    }
                    if (queryDomain.equals("invalid") || queryDomain.equals("redirected")) {
                        backLinksData.put("error", messageSource.getMessage("URL.error", null, Locale.US));
                    } else {
                        String domainName = Common.getDomainFromUrl(queryDomain);

                        /*Searching in database is there existence record for root domain with domainName ex:netelixir.com*/
                        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
                        ahrefBackLinkDomainInfo = inboundLinkCheckerService.fetchDomainLevelData(domainName);
                        Calendar lastbackLinkFetchedDate = Calendar.getInstance();
                        Calendar lastRefDomainFetchedDate = null;
                        Calendar prevDate = Calendar.getInstance();
                        prevDate.add(Calendar.DAY_OF_MONTH, -7);

                        long domainId = 0;
                        int savedBacklinksCount = 0;
                        ArrayList<AhrefBacklink> backLinks = null;
                        boolean saveBackLinksData = Boolean.FALSE;
                        if (ahrefBackLinkDomainInfo != null) {
                            domainId = ahrefBackLinkDomainInfo.getDomainId();
                            if (ahrefBackLinkDomainInfo.getBackLinkFetchedDate() != null) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                    lastbackLinkFetchedDate.setTime(sdf.parse(ahrefBackLinkDomainInfo.getBackLinkFetchedDate()));
                                    if (type.equalsIgnoreCase("LXRSEO")) {
                                        lastRefDomainFetchedDate = Calendar.getInstance();
                                        if (ahrefBackLinkDomainInfo.getRefDomainFetchedDate() != null) {
                                            lastRefDomainFetchedDate.setTime(sdf.parse(ahrefBackLinkDomainInfo.getRefDomainFetchedDate()));
                                        }
                                    }
                                } catch (ParseException ex) {
                                    LOGGER.error("Exception when converting into calender object: ", ex);
                                }
                                backLinks = inboundLinkCheckerService.fetchBackLinkData(ahrefBackLinkDomainInfo.getDomainId(), count);
                                savedBacklinksCount = (backLinks != null) ? backLinks.size() : 0;
                            }
                        }

                        backLinks = new ArrayList<>();
                        /*Fetchin backlinks When domain is not present or backlinks count is less in DB or fetched backlinks is before 1 week 
                        First checking previous fetch date null before comparing comparing today's date with fecthed date*/
//                        if (ahrefBackLinkDomainInfo == null || ahrefBackLinkDomainInfo.getBackLinkFetchedDate() == null || lastbackLinkFetchedDate.before(prevDate) || ahrefBackLinkDomainInfo.getFetchedBackLinks() < count) {
                        if ((ahrefBackLinkDomainInfo == null)
                                || (ahrefBackLinkDomainInfo.getBackLinkFetchedDate() == null)
                                || (ahrefBackLinkDomainInfo.getFetchedBackLinks() < count)
                                || (lastbackLinkFetchedDate.before(prevDate))
                                || savedBacklinksCount == 0) {
                            if (ahrefBackLinkDomainInfo == null) {
                                /*Saving root domain in database.*/
                                domainId = inboundLinkCheckerService.insertBackLinkDomLevelData(domainName);
                            }
                            /**
                             * Considering domain URL with status code 200 to
                             * fetch backlinks. Passing root domain to API
                             * returning duplicate backlinks, to avoid this
                             * passing valid and final redirected domain.
                             * Modified on July 19,2018.
                             */
                            ahrefBackLinkDomainInfo = inboundLinkCheckerService.getbackLinkData(domainName, backLinks, count);
                            if (ahrefBackLinkDomainInfo != null) {
                                ahrefBackLinkDomainInfo.setDomainName(queryDomain);
                                saveBackLinksData = Boolean.TRUE;
                            }
                        } else {
                            backLinks = inboundLinkCheckerService.fetchBackLinkData(ahrefBackLinkDomainInfo.getDomainId(), count);
                            ahrefBackLinkDomainInfo.setDomainName(queryDomain);
                        }

                        if (ahrefBackLinkDomainInfo != null) {
                            boolean saveReferringDomainData = false;
                            //To store referring domains that contain backlinks to the target.
                            ArrayList<AhrefRefDomains> referringDomains = null;
                            if (type.equalsIgnoreCase("LXRSEO") && ((ahrefBackLinkDomainInfo.getRefDomainFetchedDate() == null)
                                    || (lastRefDomainFetchedDate != null && lastRefDomainFetchedDate.before(prevDate)))) {
                                referringDomains = new ArrayList<>();
                                String referringDomainFetchedDate = inboundLinkCheckerService.getReferringDomainData(queryDomain, referringDomains, 20);
                                if (referringDomainFetchedDate != null) {
                                    ahrefBackLinkDomainInfo.setRefDomainFetchedDate(referringDomainFetchedDate);
                                }
                                saveReferringDomainData = true;
                            } else if (type.equalsIgnoreCase("LXRSEO")) {
                                referringDomains = inboundLinkCheckerService.fetchReferringDomainData(ahrefBackLinkDomainInfo.getDomainId());
                            }

                            //Thread for inserting backlinks domain level data and backlinks, refeering domain data
                            if (saveBackLinksData || saveReferringDomainData) {
                                BackLinksInsertingTask backLinksInsertingTask = new BackLinksInsertingTask(inboundLinkCheckerService, ahrefBackLinkDomainInfo, backLinks, referringDomains, saveBackLinksData, domainId);
                                Thread backLinkInsertingThread = new Thread(backLinksInsertingTask);
                                backLinkInsertingThread.start();
                            }

                            backLinksData.put("metrics", ahrefBackLinkDomainInfo);
                            backLinksData.put("backlinksInfo", backLinks);
                            if (referringDomains != null) {
                                backLinksData.put("referringDomainInfo", referringDomains);
                            }
                            if (ahrefBackLinkDomainInfo.getAhrefsbackLinks() > 0 && (!type.equalsIgnoreCase("LXRSEO")) && count <= 100) {
                                backLinksData.put("backlinksPrice", inboundLinkCheckerService.getPriceListForBackLinks(ahrefBackLinkDomainInfo.getAhrefsbackLinks()));
                            }
                        } else {
//                            backLinksData.put("error", messageSource.getMessage("Ahref.API.error", new Object[]{"\""+domainName+"\""}, Locale.US));
                            backLinksData.put("error", messageSource.getMessage("Ahref.API.error", null, Locale.US));
                        }
                    }
                } else {
                    backLinksData.put("error", "Invalid Request.");
                }
            } else {
                backLinksData.put("error", "Please enter URL.");
            }

        } catch (JSONException e) {
            LOGGER.error("Exception in json object", e);
        }
        return backLinksData;
    }

}
