/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBackLinkDomainInfo;
import lxr.marketplace.apiaccess.lxrmbeans.AhrefBacklink;
import lxr.marketplace.inboundlinkchecker.InboundLinkCheckerService;
import lxr.marketplace.payment.BackLinkAPPPayment;
import lxr.marketplace.util.Common;
import lxr.marketplace.util.ToolService;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author sagar
 */
@Controller
@RequestMapping("/mobile-backlink")
public class MobileBacklinkController {

    private static final Logger LOGGER = Logger.getLogger(MobileBacklinkController.class);

    /*@Autowired
    private SignupService signupService;
    @Autowired
    private MobilePaymentService mobilePaymentService;*/
    @Autowired
    private InboundLinkCheckerService inboundLinkCheckerService;
    @Autowired
    private ToolService toolService;
    @Autowired
    private String downloadFolder;

    @Autowired
    private String domainName;

//    @Value("${lxrm.invoice.path}")
//    private String invoiceFolder;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JSONObject validateMobileBackLinkRequest(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute BackLinkAPPPayment backLinkAPPPayment, @RequestParam("backLinkCount") long backLinkCount, @RequestParam("domain") String queryDomain,
            @RequestParam(value = "toolName", required = false, defaultValue = "") String toolName) {
        LOGGER.info("BackLinkAPPPayment model request object for SEO backlink app:: " + backLinkAPPPayment + ", and requested backlink count:: " + backLinkCount + ", for query domain:: " + queryDomain);
        JSONObject toolResposne = new JSONObject();
        JSONObject tempResposne = null;
//        String hostingURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        long lxrmUserId = 0;
//        double amount = inboundLinkCheckerService.getPayableFormatAmount(mobilePayment.getPrice().trim());
        /*Step1: Validating amount and backlink count.*/
 /*Validating backlinks count for amount.*/
        //String requestedLinks = inboundLinkCheckerService.getPriceQueryForSelectedbacklinks(backLinkCount, amount);
        /*Step2: If amount > 0 and backlink count > 20
            1: Fetching UserId based on email, if user does not exists creating new user.
            2: Validating payment for requested amount.*/
//        if (backLinkCount > Common.BACK_LINK_COUNT && mobilePayment.getPrice() != 0) {
//            if (mobilePayment.getBillingEmailId()!= null && !mobilePayment.getBillingEmailId().isEmpty()) {
//                /*Searching for user based on email.*/
//                lxrmUserId = signupService.searchUserInMarketplace(mobilePayment.getBillingEmailId(), request.getScheme() + "://" + request.getServerName(), Common.checkLocal(request));
//                mobilePayment.setPayerUserId(lxrmUserId);
//            }
//            if (mobilePayment.getPayerUserId() >= 0 && mobilePayment.getBillingEmailId() != null && !mobilePayment.getBillingEmailId().isEmpty()) {
//            mobilePayment.setDescription("SEO Backlink Tool subscription capture");
//                tempResposne = mobilePaymentService.validateMobilePaymentRequest(mobilePayment, hostingURL, messageSource.getMessage("lxrm.invoice.path", null, Locale.US));
//                if (tempResposne != null && tempResposne.getBoolean("success")) {
//                    toolResposne.put("message", tempResposne.get("message"));
//                    toolResposne.put("userId", lxrmUserId);
//                }
//            }
//        }
        //Step3: Pulling backlinks from service.
        //Requesting backlinks from service
        List<AhrefBacklink> backLinks = null;
        AhrefBackLinkDomainInfo ahrefBackLinkDomainInfo = null;
        JSONObject backLinksData = null;
        String generatedReportName = null;
        String serviceHostingURL = null;
        if (domainName != null && !domainName.trim().isEmpty() && messageSource.getMessage("inboundLinkCheckerServiceURL", null, Locale.US) != null) {
            serviceHostingURL = domainName.concat(messageSource.getMessage("inboundLinkCheckerServiceURL", null, Locale.US));
        }
        if ((backLinkCount <= Common.BACK_LINK_COUNT || (backLinkAPPPayment.getAmount() != 0 && tempResposne != null && tempResposne.getBoolean("success"))) && serviceHostingURL != null) {
            backLinksData = inboundLinkCheckerService.backLinkDataService(String.valueOf(backLinkCount), queryDomain, serviceHostingURL);
        }
        try {
            if (backLinksData != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                ahrefBackLinkDomainInfo = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("metrics")), new TypeReference<AhrefBackLinkDomainInfo>() {
                });
                backLinks = objectMapper.readValue(objectMapper.writeValueAsString(backLinksData.get("backlinksInfo")), new TypeReference<List<AhrefBacklink>>() {
                });
            }
        } catch (JsonProcessingException e) {
            LOGGER.error("JsonProcessingException cause:: ", e);
        } catch (IOException e) {
            LOGGER.error("IOException cause:: ", e);
        }
        /*Step4: If payment sucess and requested baclinks sending invoice to user*/
 /*Payment success send invoice to user after*/
        if (tempResposne != null && tempResposne.getBoolean("success") && backLinks != null) {
//            mobilePaymentService.sendInvoiceToUser(lxrmUserId, mobilePayment.getToolId(), request.getServletContext(), tempResposne.getString("transactionId"), tempResposne.getString("transactionDate"), mobilePayment.getName(), mobilePayment.getAmount(), backLinks.size() + " Backlinks", mobilePayment.getCardNumber(), "SEO Backlink Tool",invoiceFolder);
        }
        if ((backLinkCount <= Common.BACK_LINK_COUNT) || (backLinkAPPPayment.getAmount() != 0 && tempResposne != null && tempResposne.getBoolean("success"))) {
            /*Step5:Generating report in Excell format*/
            if (ahrefBackLinkDomainInfo != null && backLinks != null && !backLinks.isEmpty()) {
                generatedReportName = inboundLinkCheckerService.createBackLinkXLSFile(queryDomain.trim(), downloadFolder, backLinks,
                        ahrefBackLinkDomainInfo, backLinkAPPPayment.getUserId(), "LXRMarketplace_SEO Backlink", "SEO Backlink Report");
            } else {
                LOGGER.error("Insufficent data to generate excell report for seo backnlink app, ahrefBackLinkDomainInfo::" + ahrefBackLinkDomainInfo + ", backLinks:: " + backLinks);
            }
        }
        if (generatedReportName != null) {
            toolResposne.put("success", true);
            toolResposne.put("reportName", generatedReportName);
        } else {
            toolResposne.put("success", false);
        }
        /*Step6:Sending report to user email.*/
        if (tempResposne != null && tempResposne.getBoolean("success") && generatedReportName != null) {
            /*Sending report through mail to user email*/
            Common.sendReportThroughMail(request, downloadFolder, generatedReportName, backLinkAPPPayment.getEmail(), Common.INBOUND_LINK_CHECKER);
            Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            Object[] vals = {generatedReportName, backLinkAPPPayment.getToolId(), lxrmUserId, createdTime, queryDomain.trim()};
            /*Saving the generated report to allow user to access his previous downloaded reoprts.
                Table Name: download_report_details*/
            toolService.insertDataInDownloadReportDetailsTab(vals);
        } else if (tempResposne != null) {
            toolResposne.put("message", tempResposne.get("message"));
        }
        if (tempResposne == null && backLinkCount > Common.BACK_LINK_COUNT) {
            toolResposne.put("success", false);
            toolResposne.put("message", "Validation failed");
        }
        return toolResposne;
    }
}
