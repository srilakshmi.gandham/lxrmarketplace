/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletContext;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.payment.CardType;
import lxr.marketplace.payment.GatewayService;
import lxr.marketplace.payment.LXRMTransaction;
import lxr.marketplace.payment.PaymentModel;
import lxr.marketplace.payment.PaymentService;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import lxr.marketplace.util.Tool;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class MobilePaymentService {

    private static final Logger LOGGER = Logger.getLogger(MobilePaymentService.class);
//    @Autowired
//    private PayPalServicev1 payPalService;

    @Autowired
    MarketplacePaymentService marketplacePaymentService;

    @Autowired
    private LoginService loginService;
//    @Autowired
//    private ApplicationContext applicationContext;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${lxrm.paymentservice.mobileapps}")
    private boolean activePaymentService;

    @Value("${lxrm.invoice.path}")
    private String invoiceFolder;

//    @Value("${lxrm.gateWayPayment.serviceURL}")
//    private String gateWayPaymentServiceURL;

//    private final DecimalFormat paymentFormat = new DecimalFormat(".##");
    private final SimpleDateFormat paymentTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public JSONObject validateMobilePaymentRequest(PaymentModel requestModel, String description, String hostingURL) {

        JSONObject paymentResponse = new JSONObject();
        if (activePaymentService) {
            /*Constructing credit card for validating*/
            PaymentModel paymentModel = generatePaymentModelForAPPs(requestModel);
            LXRMTransaction lXRMTransaction = null;
            StringBuilder failureMessage = new StringBuilder();
            if (paymentModel != null) {
                /*Validating and authorizing the card for required amount*/
                lXRMTransaction = paymentService.processPaymentForLXRMService(paymentModel, failureMessage, invoiceFolder);
                /*Initialize the response object in JSON format*/
                if (lXRMTransaction != null && lXRMTransaction.getState() != null && lXRMTransaction.getState().equals(GatewayService.CAPTURE_INTENT)) {

                    paymentResponse.put("success", Boolean.TRUE);
                    paymentResponse.put("message", messageSource.getMessage("lxrm.paymentservice.sucess", null, Locale.US));
                    paymentResponse.put("lXRMTransaction", lXRMTransaction);

//                    Login user = loginService.find(mobilePayment.getUserId());
//                    Object[] chars = new String[1];
//                    chars[0] = user != null ? user.getUserName() : "";
//                    paymentMessage = messageSource.getMessage("mobile.payment.success", chars, Locale.UK);
                } else {
                    paymentResponse.put("success", Boolean.FALSE);
                    paymentResponse.put("message", failureMessage.length() != 0 ? failureMessage.toString() : "Failed to Authorize this card through PayPal. Use a different Credit / Debit Card or Try Again.");
                }
            } else {
                paymentResponse.put("success", false);
                paymentResponse.put("message", "Oh snap! ".concat(GatewayService.AUTH_FAILURE).concat(" Please try again."));
            }
        }
        /*Temporarily payment service shutdown on April 03,2019 Suggested to avoid fraud transcations.*/
        if (!activePaymentService) {
            paymentResponse.put("success", false);
            paymentResponse.put("message", messageSource.getMessage("fraud.card.error", null, Locale.UK));
        }
        return paymentResponse;
    }

    public void sendInvoiceToUser(long userId, int toolId, ServletContext servletContext, String transactionId, String transactionDate, String customerName, String paymentAmount, String plan, String cardNo, String toolName, String invoicePath) {
        /*Generating date fromat to generate invoice*/
        Timestamp paymentTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        String updationTime = paymentTimeFormat.format(paymentTime);
        Date paymentDate = null;
        try {
            paymentDate = paymentTimeFormat.parse(updationTime);
        } catch (ParseException ex) {
            LOGGER.error("Exception when converting date format for payment:", ex);
        }
        /*Fetching tool name from servlet contenxt object*/
        String invoiceFileName = null;
        Login user = null;
//        toolName = getToolName(toolId, servletContext);
        invoiceFileName = marketplacePaymentService.createInvoiceFromCapture(transactionId, transactionDate, customerName, toolName, paymentAmount, plan, cardNo, invoicePath, userId);
        /*Sending Invoice to user emailId*/
        if (invoiceFileName != null) {
            user = loginService.find(userId);
            if (user != null) {
                String subjectLine = EmailBodyCreator.subjectOnPaymnetSuccess;
                String bodyText = EmailBodyCreator.bodyForPaymnetSuccess(user.getName(), toolName, toolId, paymentDate);
                SendMail.sendMailWithAttachMent(invoicePath, invoiceFileName, user.getName(), user.getUserName(), subjectLine, bodyText, true);
            }
        }
    }

    public boolean saveMobilePaymentPayPalCapture(TransactionStats transactionStats, long userId, long toolId, String description) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(transactionStats.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            LOGGER.error("Exception in getting transaction creation time for user with id = " + userId);
        }
        try {
            Date updatedTime = sdf.parse(transactionStats.getUpdateTime());
            updationTime = new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            LOGGER.error("Exception in getting transaction updation time for user with id = " + userId);
        }

        Object[] dataParams = new Object[]{creationTime, updationTime, transactionStats.getState(), transactionStats.getIntent(),
            toolId, transactionStats.getCurrency(), transactionStats.getAmount(), " ", description,
            userId, true, transactionStats.getTransactionId(), " "};

        return marketplacePaymentService.savePayPalTransactionInfo(dataParams, userId);
    }

    public String getToolName(int toolId, ServletContext servletContext) {
        Map<Integer, Tool> toolsMap = null;
        String toolName = null;
        Tool toolObject = null;
        if (servletContext != null) {
            toolsMap = (Map<Integer, Tool>) servletContext.getAttribute("tools");
        }
        if (toolsMap != null && toolsMap.get(toolId) != null) {
            toolObject = (Tool) toolsMap.get(toolId);
        }
        if (toolObject != null) {
            toolName = toolObject.getToolName();
        } else {
            String query = "select name from tool where id=" + toolId;
            toolName = jdbcTemplate.queryForObject(query, String.class);
        }
        return toolName;
    }

    public LxrMCreditCard generateLxrMCreditCard(MobilePayment mobilePayment) {
        LxrMCreditCard lxrMCreditCard = new LxrMCreditCard();
        try {
            lxrMCreditCard.setCardNo(mobilePayment.getCardNumber().trim());
            lxrMCreditCard.setCvv(mobilePayment.getCvv().trim());
            lxrMCreditCard.setCountryCode("us");
            lxrMCreditCard.setPostalCode("000000");
            lxrMCreditCard.setExpireMonth(mobilePayment.getExpiryMonth());
            lxrMCreditCard.setExpireYear(mobilePayment.getExpiryYear());
            lxrMCreditCard.setType(CardType.detect(mobilePayment.getCardNumber().trim()));
            if (mobilePayment.getName() != null) {
                String name = mobilePayment.getName().trim();
                String splittedFirstName = name.substring(0, name.lastIndexOf(' '));
                String splittedLastName = name.substring(name.lastIndexOf(' ') + 1, name.length());
                lxrMCreditCard.setFirstName(splittedFirstName);
                lxrMCreditCard.setLastName(splittedLastName);
            }
        } catch (StringIndexOutOfBoundsException | NullPointerException e) {
            lxrMCreditCard = null;
        }
        return lxrMCreditCard;
    }

    public PaymentModel generatePaymentModelForAPPs(PaymentModel requestModel) {
        PaymentModel paymentModel = new PaymentModel();
        boolean inValidateInputs = Boolean.FALSE;
        if (requestModel.getFullName() != null && !requestModel.getFullName().isEmpty()) {
            paymentModel.setFullName(requestModel.getFullName().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getCardNo() != null && !requestModel.getCardNo().isEmpty()) {
            paymentModel.setCardNo(requestModel.getCardNo().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getCvv() != null && !requestModel.getCvv().isEmpty()) {
            paymentModel.setCvv(requestModel.getCvv().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getExpireMonth() != 0) {
            paymentModel.setExpireMonth(requestModel.getExpireMonth());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getExpireYear() != 0) {
            paymentModel.setExpireYear(requestModel.getExpireYear());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getBillingUserName() != null && !requestModel.getBillingUserName().isEmpty()) {
            paymentModel.setBillingUserName(requestModel.getBillingUserName().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getBillingEmailId() != null && !requestModel.getBillingEmailId().isEmpty()) {
            paymentModel.setBillingEmailId(requestModel.getBillingEmailId().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getToolName() != null && !requestModel.getToolName().isEmpty()) {
            paymentModel.setToolName(requestModel.getToolName().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }

        if (requestModel.getDescription() != null && !requestModel.getDescription().isEmpty()) {
            paymentModel.setDescription(requestModel.getDescription().length() > 1023 ? requestModel.getDescription().trim().substring(0, 1022) : requestModel.getDescription().trim());
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (requestModel.getToolId() != 0) {
            paymentModel.setToolId(requestModel.getToolId());
        } else {
            inValidateInputs = Boolean.TRUE;
        }
        if (paymentModel.getCardNo() != null) {
            paymentModel.setCardType(paymentService.getPaymentCardType(paymentModel.getCardNo()));
        }
        if (paymentModel.getCardType() == null) {
            inValidateInputs = Boolean.TRUE;
        }
        if (requestModel.getPrice() == 0) {
            inValidateInputs = Boolean.TRUE;
        }
        paymentModel.setPrice(requestModel.getPrice());
        paymentModel.setToolId(requestModel.getToolId());
        paymentModel.setSendInvoice(requestModel.isSendInvoice());
        paymentModel.setCurrencyType(requestModel.getCurrencyType());
        paymentModel.setPaymentDate(LocalDateTime.now());
        paymentModel.setPlan(requestModel.getPlan());
        paymentModel.setDatePattern(requestModel.getDatePattern());
        paymentModel.setSaveUserId(requestModel.isSaveUserId());

        if (inValidateInputs) {
            paymentModel = null;
        }
        return paymentModel;
    }
}
