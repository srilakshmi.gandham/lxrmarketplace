/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.Collections;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
public class MobileUserV2Controller {

    private static final Logger LOGGER = Logger.getLogger(MobileUserV2Controller.class);

    @Autowired
    private MobileUserV2Service mobileUserV2Service;

    @RequestMapping(value = "/mobile/user/save", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map saveOrUpdateMobileUser(@ModelAttribute MobileUserV2 mobileUser) {

        LOGGER.info("Saving mobile user is:" + mobileUser.getName());
        //Checking email is present or not
        MobileUserV2 mobileUserV2 = null;
        if (mobileUser.getEmail() != null) {
            mobileUserV2 = mobileUserV2Service.getMobileUser(mobileUser.getEmail());
        }
        if (mobileUser.getName() == null || mobileUser.getName().trim().length() == 0) {
            mobileUser.setName(mobileUser.getEmail().substring(0, mobileUser.getEmail().indexOf("@")));
        }
        if (mobileUserV2 != null) {
            //Email is not found then inserting as a new user else checking that user loaction details present or not
            if (mobileUserV2.getCountry() == null) {
                //Loaction details are not present then updating those details for perticular user
                mobileUser.setUserId(mobileUserV2.getUserId());
                mobileUserV2Service.updateMobileUserDetails(mobileUser);
            }
            return Collections.singletonMap("userId", mobileUserV2.getUserId());
        } else {
            return Collections.singletonMap("userId", mobileUserV2Service.saveMobileUser(mobileUser));
        }
    }

    @RequestMapping(value = "/mobile/tool/usage/save", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void saveMobileToolUsage(@RequestParam("userId") long userId, @RequestParam("toolId") int toolId,
            @RequestParam("input") String input) {
        mobileUserV2Service.saveMobileToolUsage(userId, toolId, input);
    }
}
