/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lxr.marketplace.util.SendEmailOptions;
import static lxr.marketplace.util.SendMail.sendEmailToUser;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vidyasagar.korada
 */
@Controller
public class SendEmailRestController {

    private static final Logger LOGGER = Logger.getLogger(SendEmailRestController.class);

    @RequestMapping(value = "/lxrm-send-email-service", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SendEmailOptions validateMobileBackLinkRequest(HttpServletRequest request, HttpServletResponse response, @ModelAttribute SendEmailOptions sendEmailOptions) {
        LOGGER.info("In validateMobileBackLinkRequest sendEmailOptions:: "+sendEmailOptions.toString());
        SendEmailOptions deilveredEmail = sendEmailToUser(sendEmailOptions);
        LOGGER.info("In validateMobileBackLinkRequest deilveredEmail:: "+sendEmailOptions.toString());
        return deilveredEmail;
    }

}