/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class MobileUserV2Service {

    private static final Logger LOGGER = Logger.getLogger(MobileUserV2Service.class);
    @Autowired
    JdbcTemplate jdbcTemplate;

    public MobileUserV2 getMobileUser(String email) {
        String query = "SELECT * FROM lxrm_mobile_users WHERE email=?";
        return jdbcTemplate.query(query, new Object[]{email.toLowerCase()}, (ResultSet rs) -> {
            if (rs.next()) {
                MobileUserV2 mobileUser = new MobileUserV2();
                mobileUser.setUserId(rs.getInt("user_id"));
                mobileUser.setName(rs.getString("name"));
                mobileUser.setEmail(rs.getString("email"));
                mobileUser.setCity(rs.getString("city"));
                mobileUser.setCountry(rs.getString("country"));
                return mobileUser;
            }
            return null;
        });

    }

    public void updateMobileUserDetails(MobileUserV2 mobileUser) {
        String query = "UPDATE lxrm_mobile_users SET city=?, country=?, platform=?, signup_type=? WHERE user_id=?";
        jdbcTemplate.update(query, new Object[]{(mobileUser.getCity() != null && (!mobileUser.getCity().equals("") && !mobileUser.getCity().equals("(null)")) ? mobileUser.getCity() : null), (mobileUser.getCountry() != null && (!mobileUser.getCountry().equals("") && !mobileUser.getCountry().equals("(null)")) ? mobileUser.getCountry() : null), mobileUser.getPlatform(), mobileUser.getSignupType(), mobileUser.getUserId()});

    }

    public long saveMobileUser(MobileUserV2 mobileUser) {
        String query = "insert into lxrm_mobile_users(name, email, created_date, city, country, platform, signup_type) values(?,?,?,?,?,?,?)";
        Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) con.prepareStatement(query, new String[]{"id"});
                    String name = mobileUser.getName() != null ? mobileUser.getName().trim() : mobileUser.getEmail().split("\\@")[0];
                    try {

                        statement.setBytes(1, name.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(1, name);
                        LOGGER.error("Exception on signup name: ", e);
                    }
                    try {
                        statement.setBytes(2, mobileUser.getEmail().toLowerCase().trim().getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        statement.setString(2, mobileUser.getEmail().toLowerCase());
                        LOGGER.error("Exception on signup name: ", e);
                    }
                    statement.setTimestamp(3, createdTime);
                    statement.setString(4, (mobileUser.getCity() != null && (!mobileUser.getCity().equals("") && !mobileUser.getCity().equals("(null)")) ? mobileUser.getCity() : null));
                    statement.setString(5, (mobileUser.getCountry() != null && (!mobileUser.getCountry().equals("") && !mobileUser.getCountry().equals("(null)")) ? mobileUser.getCountry() : null));
                    statement.setInt(6, mobileUser.getPlatform());
                    statement.setInt(7, mobileUser.getSignupType());

                    return statement;
                }, keyHolder);
            }
        } catch (DataAccessException e) {
            LOGGER.error("Exception on saving mobile user for:" + mobileUser.getEmail() + ", cause: " + e);
        }
        return (Long) keyHolder.getKey();
    }

    public void saveMobileToolUsage(long userId, int toolId, String input) {
        String query = "insert into lxrm_mobile_tool_usage(created_date, user_id, tool_id, input) values(?,?,?,?)";
        Timestamp createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        synchronized (this) {
            jdbcTemplate.update(query, createdTime, userId, toolId, input);
        }
    }

}
