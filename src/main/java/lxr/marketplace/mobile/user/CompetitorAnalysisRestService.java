/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lxr.marketplace.competitoranalysis.CompetitorAnalysisResult;
import lxr.marketplace.competitoranalysis.YourSiteThread;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author sagar
 */
@Service
public class CompetitorAnalysisRestService {

    private static final Logger LOGGER = Logger.getLogger(CompetitorAnalysisRestService.class);

    public Map<String, Object> getToolAnalysis(String userQueryURL, List<String> competitorQueryURL, String searchQuery) {
        Map<String, Object> resultHashMap = new LinkedHashMap<>();
        try {
            List<CompetitorAnalysisResult> competitorsResultList = new ArrayList();
            ExecutorService service = Executors.newFixedThreadPool(competitorQueryURL.size() + 1);
            CountDownLatch latchObj = new CountDownLatch(competitorQueryURL.size() + 1);
            Future<CompetitorAnalysisResult> yourAnalysisResultFuture = service.submit(new YourSiteThread(userQueryURL, searchQuery, latchObj));
            Future<CompetitorAnalysisResult> competitorAnalysisResultFuture;
            for (String domain : competitorQueryURL) {
                competitorAnalysisResultFuture = service.submit(new YourSiteThread(domain.trim(), searchQuery, latchObj));
                competitorsResultList.add(competitorAnalysisResultFuture.get());
            }
            latchObj.await();
            service.shutdown();

            if (yourAnalysisResultFuture != null) {
                resultHashMap.put("success", 1);
                resultHashMap.put("userSiteAnalysis", yourAnalysisResultFuture.get());
                resultHashMap.put("competitrSiteAnalysis", competitorsResultList);
            } else {
                resultHashMap.put("success", 0);
                resultHashMap.put("message", "Connection timeout");
            }
        } catch (InterruptedException | ExecutionException e) {
            resultHashMap.put("success", 0);
            LOGGER.error("InterruptedException in creating competitor analysis thread: ", e);
        }
        return resultHashMap;
    }

}
