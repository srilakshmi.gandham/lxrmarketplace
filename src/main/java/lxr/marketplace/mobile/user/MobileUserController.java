package lxr.marketplace.mobile.user;

import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lxr.marketplace.user.MobileUserService;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

@SuppressWarnings("deprecation")
public class MobileUserController extends SimpleFormController{
	private static Logger logger = Logger.getLogger(MobileUserController.class);
	private MobileUserService mobileUserService;

	public MobileUserService getMobileUserService() {
		return mobileUserService;
	}

	public void setMobileUserService(MobileUserService mobileUserService) {
		this.mobileUserService = mobileUserService;
	}
	
	public MobileUserController(){
		setCommandClass(MobileUser.class);
		setCommandName("mobileUser");
	}
	
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, 
			Object command, BindException errors)throws Exception {
		logger.debug("in side of MobileUserController........... ");
		long userId = 0;
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String toolName = request.getParameter("toolName");
		HttpSession session = request.getSession();
		final Timestamp startTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
		if(email != null && !email.trim().equals("")){
			email = email.trim();
			if(toolName!=null && !toolName.trim().equals("")){
				if(name!=null && !name.trim().equals("")){
					Long tempUid = mobileUserService.findUser(email,toolName);
    				if(tempUid!=null){
    					userId=(long)tempUid;
    				}
    				if(userId<=0){  // New User
						userId = mobileUserService.insertInMobileUserTab(name, email, startTime,toolName);
					}
				}else{
					mobileUserService.addErrorToResp("required field missing - name",response);
				}
				
			}else{
				mobileUserService.addErrorToResp("required field missing - tool name",response);
			}
		}else{
			mobileUserService.addErrorToResp("required field missing - email",response);
		}
	return null;
	}

	
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}
	
	protected Object formBackingObject(HttpServletRequest request,HttpServletResponse response)throws Exception {
		return null;
//		HttpSession session = request.getSession();
//		SeoNinjaHome seoNinjaHome = (SeoNinjaHome) session.getAttribute("seoNinjaHome");
//		SeoNinjaHome seoNinjaHome = null;
//		if (seoNinjaHome == null) {
//			seoNinjaHome = new SeoNinjaHome();
//		}
//		return seoNinjaHome;
	}

}
