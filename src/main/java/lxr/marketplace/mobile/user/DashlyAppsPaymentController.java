/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.user;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.payment.PaymentModel;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vidyasagar.korada
 */
@Controller
public class DashlyAppsPaymentController {

    @Autowired
    private MobilePaymentService mobilePaymentService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/lxrm-dashly-apps-payment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public JSONObject validateMobileBackLinkRequest(HttpServletRequest request, HttpServletResponse response, @ModelAttribute PaymentModel mobilePayment) {
        JSONObject responseJSON = new JSONObject();
        String hostingURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        JSONObject tempJSON = mobilePaymentService.validateMobilePaymentRequest(mobilePayment, hostingURL, messageSource.getMessage("lxrm.invoice.path", null, Locale.US));
        if (tempJSON != null) {
            if (tempJSON.has("success")) {
                responseJSON.put("success", tempJSON.getBoolean("success"));
            }
            if (tempJSON.has("message")) {
                responseJSON.put("message", tempJSON.getString("message"));
            }
            if (tempJSON.has("lXRMTransaction")) {
                if (responseJSON.has("success") && responseJSON.getBoolean("success")) {
                    responseJSON.put("lXRMTransaction", tempJSON.get("lXRMTransaction"));
                }
            }
        }
        return responseJSON;
    }

}
