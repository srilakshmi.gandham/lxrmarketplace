/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class AskTheExpertPriceService {

    private static Logger logger = Logger.getLogger(AskTheExpertPriceService.class);
    @Autowired
    JdbcTemplate jdbcTemplate;

    String query = "";

    public float getAskTherExpertPrice() {
        query = "select amount from tools_pricing where tool_id = 0";
        return this.jdbcTemplate.queryForObject(query, Float.class);
    }

    public String getPromoCode() {
        String promocode = null;
        Calendar cal = Calendar.getInstance();
        DateFormat dFormat = new SimpleDateFormat("YYYY-MM-dd");
        String todayDate = dFormat.format(cal.getTime());
        query = "select promo_code from lxrm_promos where start_date <= ?  and end_date >= ? ";
        try {
            promocode = this.jdbcTemplate.queryForObject(query, new Object[]{todayDate, todayDate}, String.class);
        } catch (EmptyResultDataAccessException e) {
            logger.error("Exception on fetching promo code:", e);
        }

        return promocode;
    }

}
