/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.askexpert.ATEUserMessages;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
@RequestMapping("/mobile-ate-questions")
public class AskTheExpertQuestions {

    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;

    @RequestMapping(value = "/all", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    List<ATEUserMessages> fetchingATEAnswers(HttpServletRequest request, HttpServletResponse response,
            @RequestParam long userId) {
        List<ATEUserMessages> allATEAnswers = lxrmAskExpertService.getATEAnswersList(userId);
        return allATEAnswers;
    }
}
