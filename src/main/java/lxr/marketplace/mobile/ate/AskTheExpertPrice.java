/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

/**
 *
 * @author vemanna
 */
public class AskTheExpertPrice {
    
    private float atePrice;
    private String promoCode;
    private boolean isPromoAvailable;

    public float getAtePrice() {
        return atePrice;
    }

    public void setAtePrice(float atePrice) {
        this.atePrice = atePrice;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public boolean isIsPromoAvailable() {
        return isPromoAvailable;
    }

    public void setIsPromoAvailable(boolean isPromoAvailable) {
        this.isPromoAvailable = isPromoAvailable;
    }
}
