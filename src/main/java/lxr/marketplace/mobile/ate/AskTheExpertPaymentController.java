/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.admin.ate.answers.ExpertQuestionsService;
import lxr.marketplace.admin.expertuser.ExpertUser;
import lxr.marketplace.askexpert.LxrmAskExpert;
import lxr.marketplace.askexpert.LxrmAskExpertService;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.payment.CardType;
import lxr.marketplace.payment.PayPalServicev1;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import lxr.marketplace.user.Login;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.EmailBodyCreator;
import lxr.marketplace.util.SendMail;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
@RequestMapping("/mobile-ate-payment")
public class AskTheExpertPaymentController {

    private static Logger logger = Logger.getLogger(AskTheExpertPaymentController.class);

    @Autowired
    private String supportMail;
    @Autowired
    private PayPalServicev1 payPalService;
    @Autowired
    private LxrmAskExpertService lxrmAskExpertService;
    @Autowired
    private ExpertQuestionsService expertQuestionsService;
    @Autowired
    AskTheExpertPaymentService askTheExpertPaymentService;
    @Autowired
    MarketplacePaymentService marketplacePaymentService;
    @Autowired
    private String downloadFolder;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/payment", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    void atePayment(HttpServletRequest request, HttpServletResponse response, @ModelAttribute AskTheExpertPayment askTheExpertPayment) throws JSONException {

        String message = "";
        boolean isPaymentSuccess = false, isPromoApplied = false, success = false;
        long paymentId = 0;

        isPromoApplied = askTheExpertPayment.isPromoApplied();
        double totalAmt = Double.parseDouble(askTheExpertPayment.getAmount());

        if (!isPromoApplied || totalAmt > 0.0) {
            LxrMCreditCard lxrmCard = createLxrMCreditCard(askTheExpertPayment);
            DecimalFormat df = new DecimalFormat("#.00");
            String amount = df.format(totalAmt);
            String[] errorMessage = {""};
            TransactionStats transactionStats = payPalService.chargeAmount(lxrmCard, amount, errorMessage);
            if (transactionStats != null && transactionStats.getState() != null && transactionStats.getState().equals("completed")) {
                isPaymentSuccess = true;
                long cardIdInDb = askTheExpertPaymentService.saveCreditCardInLxr(lxrmCard, askTheExpertPayment.getUserId());
                askTheExpertPaymentService.savePayPalCapture(transactionStats, askTheExpertPayment.getUserId(), 0, cardIdInDb);

                SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                Timestamp creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
                String updationTime = timeFormat.format(creationTime);
                Date updatedDate = null;
                try {
                    updatedDate = timeFormat.parse(updationTime);
                } catch (ParseException ex) {
                }
//                                            createInvoiceFromCapture(String captureId, String updatedTime,String customerName, String toolName,String amount,String plan,String cardNo)
                String fileName = marketplacePaymentService.createInvoiceFromCapture(transactionStats.getTransactionId(), transactionStats.getUpdateTime(),askTheExpertPayment.getName(), "Ask The Expert",amount, "NA",lxrmCard.getCardNo(),downloadFolder,askTheExpertPayment.getUserId());
                Login user = loginService.find(askTheExpertPayment.getUserId());
                String subjectLine = EmailBodyCreator.subjectOnPaymnetSuccess;
                String bodyText = EmailBodyCreator.bodyForPaymnetSuccess(user.getName(), "Ask The Expert", 0, updatedDate);
                SendMail.sendMailWithAttachMent(downloadFolder, fileName, user.getName(), user.getUserName(), subjectLine, bodyText,true);
                message = "Success! Thanks for your payment. A receipt has been sent to " + user.getUserName() + ". If you have any questions, please contact our Customer Support at support@lxrmarketplace.com";
            } else {
                message = errorMessage[0];
            }

        }

        if (isPaymentSuccess || isPromoApplied) {
            LxrmAskExpert lxrmAskExpert = new LxrmAskExpert();
            lxrmAskExpert.setToolId(askTheExpertPayment.getToolId());
            lxrmAskExpert.setQuestion(askTheExpertPayment.getQuestion());
            lxrmAskExpert.setDomain(askTheExpertPayment.getDomain());
            lxrmAskExpert.setOtherInputs("");
            lxrmAskExpert.setDeviceCategory("Mobile");
            String toolName = askTheExpertPaymentService.getToolName(askTheExpertPayment.getToolId());
            lxrmAskExpert.setToolName(toolName);

            if (isPaymentSuccess) {
                paymentId = lxrmAskExpertService.getPaymentId(askTheExpertPayment.getUserId());
            }

            lxrmAskExpertService.insertAfterPayment(lxrmAskExpert, paymentId, askTheExpertPayment.getUserId());
            String subject = EmailBodyCreator.subjectForAskExpertQuestionToOurTeam;
            String displayTxt = EmailBodyCreator.bodyForAskExpertQuestionToOurTeam(lxrmAskExpert.getQuestion(), lxrmAskExpert.getToolName());
            List<ExpertUser> expertUserList = expertQuestionsService.getAllExperts();
            String[] expertsMails = new String[expertUserList.size() - 1];
            int i = 0;
            for (ExpertUser expertUser : expertUserList) {
                if (!expertUser.getRole().equals("admin")) {
                    expertsMails[i] = expertUser.getEmail();
                    i++;
                }
            }
            SendMail.sendMail(supportMail, expertsMails, subject, displayTxt);
            success = true;

        }

        JSONObject obj = new JSONObject();
        obj.put("success", success);
        obj.put("message", message);
        addJsonFormat(obj, response);

    }

    private void addJsonFormat(JSONObject obj, HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            writer.print(obj);
        } catch (IOException e) {
            logger.error("Exception on converting json format:", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            writer.flush();
            writer.close();
        }
    }

    private LxrMCreditCard createLxrMCreditCard(AskTheExpertPayment askTheExpertPayment) {
        LxrMCreditCard lxrMCreditCard = new LxrMCreditCard();
        lxrMCreditCard.setCardNo(askTheExpertPayment.getCardNumber());
        lxrMCreditCard.setCvv(askTheExpertPayment.getCvv());
        lxrMCreditCard.setCountryCode(askTheExpertPayment.getCountry());
        lxrMCreditCard.setPostalCode(askTheExpertPayment.getPostalCode());
        lxrMCreditCard.setExpireMonth(askTheExpertPayment.getExpiryMonth());
        lxrMCreditCard.setExpireYear(askTheExpertPayment.getExpiryYear());

        String name = askTheExpertPayment.getName();
        String splittedFirstName = name.substring(0, name.lastIndexOf(' '));
        String splittedLastName = name.substring(name.lastIndexOf(' ') + 1, name.length());
        lxrMCreditCard.setFirstName(splittedFirstName);
        lxrMCreditCard.setLastName(splittedLastName);
        lxrMCreditCard.setType(CardType.detect(askTheExpertPayment.getCardNumber()));

        return lxrMCreditCard;

    }

}
