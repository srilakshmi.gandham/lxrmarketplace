/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author vemanna
 */
@Service
public class AskTheExpertPaymentService {

    private static Logger logger = Logger.getLogger(AskTheExpertPaymentService.class);

    @Autowired
    MarketplacePaymentService marketplacePaymentService;
    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean savePayPalCapture(TransactionStats transactionStats, long userId, long toolId, long cardId) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Timestamp creationTime = null;
        Timestamp updationTime = null;
        try {
            Date createdTime = sdf.parse(transactionStats.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction creation time for user with id = " + userId);
        }
        try {
            Date updatedTime = sdf.parse(transactionStats.getUpdateTime());
            updationTime = new Timestamp(updatedTime.getTime());
        } catch (Exception e) {
            updationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            logger.error("Exception in getting transaction updation time for user with id = " + userId);
        }

        Object[] dataParams = new Object[]{creationTime, updationTime, transactionStats.getState(), transactionStats.getIntent(),
            toolId, transactionStats.getCurrency(), transactionStats.getAmount(), "", "Ask The Expert subscription capture",
            cardId, userId, true, transactionStats.getTransactionId(), ""};

        return marketplacePaymentService.savePayPalTransactionInfo(dataParams, userId);
    }

    public long saveCreditCardInLxr(LxrMCreditCard lxrmCard, long userId) {

        final String query = "INSERT INTO lxrm_credit_card_info (user_id, card_no, first_name, "
                + "last_name, card_type, valid_untill, expire_month, expire_year, saving_time, is_deleted, "
                + "address_line_1, address_line_2, city, country_code, postal_code, state,cust_profile_id,"
                + "payment_profile_id, paypal_card_id) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        logger.info("Saving credit card in lxrmarketplace database for user with id = " + userId + " ...");
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            synchronized (this) {

                this.jdbcTemplate.update((Connection con) -> {
                    PreparedStatement statement = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                    statement.setLong(1, userId);
                    statement.setString(2, lxrmCard.getCardNo());
                    statement.setString(3, lxrmCard.getFirstName());
                    statement.setString(4, lxrmCard.getLastName());
                    statement.setString(5, lxrmCard.getType());
                    try {
                        Date validUntil = sdf.parse(lxrmCard.getValidUntil());
                        statement.setTimestamp(6, new Timestamp(validUntil.getTime()));
                    } catch (ParseException | SQLException e) {
                        statement.setTimestamp(7, null);
                        logger.error("Exception in getting 'valid until' field for credit card of "
                                + lxrmCard.getFirstName() + ". Saving the card without this info.", e);
                    }
                    try {
                        statement.setInt(7, lxrmCard.getExpireMonth());
                    } catch (Exception e) {
                        logger.error("Exception in parsing expire month for credit card of "
                                + lxrmCard.getFirstName() + "saving the card without this info.");
                        statement.setInt(7, 0);
                    }
                    try {
                        statement.setInt(8, lxrmCard.getExpireYear());
                    } catch (Exception e) {
                        logger.error("Exception in parsing expire year for credit card of "
                                + lxrmCard.getFirstName() + "saving the card without this info.");
                        statement.setInt(8, 0);
                    }
                    Calendar cal = Calendar.getInstance();
                    statement.setTimestamp(9, new Timestamp(cal.getTimeInMillis()));
                    statement.setBoolean(10, false);
                    statement.setString(11, "");
                    statement.setString(12, "");
                    statement.setString(13, "");
                    statement.setString(14, lxrmCard.getCountryCode());
                    statement.setString(15, lxrmCard.getPostalCode());
                    statement.setString(16, "");
                    statement.setString(17, "");
                    statement.setString(18, "");
                    statement.setString(19, lxrmCard.getPayPalCardId());
                    return statement;
                }, keyHolder);
            }
            logger.info("Mobile ate credit card is saved in database for user with id = " + userId);
            return (Long) keyHolder.getKey();
        } catch (Exception ex) {
            logger.error("Exception on saving mobile ate new creditcard:", ex);
            return 0;
        }
    }

    public String getToolName(long toolId) {
        String query = "select name from tool where id=" + toolId;
        return this.jdbcTemplate.queryForObject(query, String.class);

    }

}
