/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.mobile.ate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author vemanna
 */
@Controller
@RequestMapping("/mobile-ate-price")
public class AskTheExpertPriceController {

    private static Logger logger = Logger.getLogger(AskTheExpertPriceController.class);
    @Autowired
    private AskTheExpertPriceService askTheExpertPriceService;

    @RequestMapping(value = "/amount", method = RequestMethod.GET)
    private @ResponseBody
    AskTheExpertPrice getAskTheExpertPrice() {
        return askExpertPriceInfo();
    }

    private AskTheExpertPrice askExpertPriceInfo() {
        boolean isPromoCode = false;
        float atePrice = askTheExpertPriceService.getAskTherExpertPrice();
        String promoCode = askTheExpertPriceService.getPromoCode();
        if (promoCode != null) {
            isPromoCode = true;
        }

        AskTheExpertPrice askTheExpertPrice = new AskTheExpertPrice();
        askTheExpertPrice.setAtePrice(atePrice);
        askTheExpertPrice.setPromoCode(promoCode);
        askTheExpertPrice.setIsPromoAvailable(isPromoCode);

        return askTheExpertPrice;
    }
}
