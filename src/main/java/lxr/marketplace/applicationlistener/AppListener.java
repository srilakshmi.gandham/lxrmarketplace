package lxr.marketplace.applicationlistener;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import lxr.marketplace.admin.settings.SettingsService;
import lxr.marketplace.admin.settings.URLRedirection;
import lxr.marketplace.admin.settings.URLRedirectionService;
import lxr.marketplace.spiderview.SpiderViewService;
import lxr.marketplace.user.LoginService;
import lxr.marketplace.util.ToolService;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AppListener implements ServletContextListener {

    private final Logger logger = Logger.getLogger(AppListener.class);

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        logger.info("Adding application startup data into servlet context...");

        ServletContext ctx = sce.getServletContext();
        WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(ctx);
        ToolService toolsrv = (ToolService) springContext.getBean("toolService");
        URLRedirectionService uRLRedirectionService = (URLRedirectionService) springContext.getBean("uRLRedirectionService");
        ctx.setAttribute("tools", toolsrv.populateTools());
        ctx.setAttribute("relatedTools", toolsrv.populateRelatedTools());
        ctx.setAttribute("lxrmUserStatstics", toolsrv.getLXRMUserStatstics());
        LoginService lgsrv = (LoginService) springContext.getBean("loginService");
        ctx.setAttribute("localIps", lgsrv.fetchFiltersForUsersByIp());
        ctx.setAttribute("localDomains", lgsrv.fetchFiltersForUsersByDomain());
        SpiderViewService spiderViewService = (SpiderViewService) springContext.getBean("spiderViewService");
        ctx.setAttribute("stopwords", spiderViewService.getStopWords());
        SettingsService settingsService = (SettingsService) springContext.getBean("settingsService");
        ctx.setAttribute("discardedIps", settingsService.getDiscardedIps());
        List<URLRedirection> redirections = uRLRedirectionService.fetchAdminURLRedirections();
        ctx.setAttribute("adminURLRedirections", redirections);
        ctx.setAttribute("userAquisitionPopupId", toolsrv.getActiveUserAquisitionPopupId());

        logger.info("Application startup data are added into servlet context.");
    }

}
