/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.udaan;

import com.fasterxml.jackson.core.JsonProcessingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.payment.PayPalServicev1;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import net.sf.json.JSONObject;

/**
 *
 * @author NE16T1213-Sagar
 */
@Controller
@RequestMapping("/mobile-udaan-payment")
public class UdaanPaymentController {

    @Autowired
    private UdaanPaymentService udaanPaymentService;
    @Autowired
    MarketplacePaymentService marketplacePaymentService;
    @Autowired
    private PayPalServicev1 payPalService;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    JSONObject validateUdaanPayment(HttpServletRequest request, HttpServletResponse response, @ModelAttribute UdaanPayment udaanPayment) throws JsonProcessingException {

        /*Creating LxrMCreditCard object*/
        LxrMCreditCard lxrmCard = udaanPaymentService.generateLXRMCreditCard(udaanPayment);
        String[] errorMessage = {""};
        JSONObject paymentResponse = new JSONObject();
        if (lxrmCard != null) {
            TransactionStats transactionStats = payPalService.chargeAmount(lxrmCard, udaanPayment.getAmount().trim(), errorMessage);
            if (transactionStats != null && transactionStats.getState() != null && transactionStats.getState().equals("completed")) {
                transactionStats.setPaymentId(String.valueOf(udaanPaymentService.saveUdaanPayPalCapture(transactionStats)));
                paymentResponse.put("paymentStatus", true);
                paymentResponse.put("transactionStats", transactionStats);
            } else {
                paymentResponse.put("paymentStatus", false);
                paymentResponse.put("errorMessage", errorMessage[0]);
            }
        } else {
            paymentResponse.put("paymentStatus", false);
            paymentResponse.put("errorMessage", "Invalid card details");
        }
        return paymentResponse;
    }
}
