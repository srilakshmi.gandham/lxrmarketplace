/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxr.marketplace.udaan;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lxr.marketplace.lxrmpayments.MarketplacePaymentService;
import lxr.marketplace.payment.CardType;
import lxr.marketplace.payment.TransactionStats;
import lxr.marketplace.paypalpayments.LxrMCreditCard;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

/**
 *
 * @author NE16T1213-Sagar
 */
@Service
public class UdaanPaymentService {

    private final static Logger LOGGER = Logger.getLogger(UdaanPaymentService.class);

    @Autowired
    MarketplacePaymentService marketplacePaymentService;
    @Autowired
    JdbcTemplate jdbcTemplate;

    private final SimpleDateFormat transctionDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public long saveUdaanPayPalCapture(TransactionStats transactionStats) {
        Timestamp creationTime = null;
        try {
            Date createdTime = transctionDateFormat.parse(transactionStats.getCreateTime());
            creationTime = new Timestamp(createdTime.getTime());
        } catch (Exception e) {
            creationTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
            LOGGER.error("Exception in getting transaction creation time: ", e);
        }
        return saveUdaanPayPalTransactionInfo(creationTime, transactionStats);
    }

    /* FOR PAYPAL SPECIFIC METHOD */
    public long saveUdaanPayPalTransactionInfo(Timestamp creationTime, TransactionStats transactionStats) {
        final String query = "INSERT INTO lxrm_udaan_transaction_stats (created_time, "
                + "state, intent, currency, amount,description,"
                + "is_card, transaction_id) VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            synchronized (this) {
                jdbcTemplate.update((java.sql.Connection con) -> {
                    PreparedStatement statement = (PreparedStatement) ((java.sql.Connection) con).prepareStatement(
                            query, new String[]{"id"});
                    statement.setTimestamp(1, creationTime);
                    statement.setString(2, transactionStats.getState());
                    statement.setString(3, transactionStats.getIntent());
                    statement.setString(4, transactionStats.getCurrency());
                    statement.setString(5, transactionStats.getAmount());
                    statement.setString(6, "Udaan Fund");
                    statement.setBoolean(7, true);
                    statement.setString(8, transactionStats.getTransactionId());
                    return statement;
                }, keyHolder);
            }
            LOGGER.info("Inserted PayPal Transaction record in lxrm_udaan_transaction_stats");
        } catch (Exception ex) {
            LOGGER.error("Error while inserting card capture info in lxrm_payment_transaction_stats ", ex);
        }
        return (Long) keyHolder.getKey();
    }

    public LxrMCreditCard generateLXRMCreditCard(UdaanPayment udaanPayment) {
        LxrMCreditCard lxrMCreditCard = null;
        try {
            String name = udaanPayment.getName();
            String splittedFirstName = name.substring(0, name.lastIndexOf(' '));
            String splittedLastName = name.substring(name.lastIndexOf(' ') + 1, name.length());
            lxrMCreditCard = new LxrMCreditCard();
            lxrMCreditCard.setCardNo(udaanPayment.getCardNumber());
            lxrMCreditCard.setCvv(udaanPayment.getCvv());
            lxrMCreditCard.setExpireMonth(udaanPayment.getExpiryMonth());
            lxrMCreditCard.setExpireYear(udaanPayment.getExpiryYear());
            lxrMCreditCard.setFirstName(splittedFirstName);
            lxrMCreditCard.setLastName(splittedLastName);
            lxrMCreditCard.setType(CardType.detect(udaanPayment.getCardNumber()));
        } catch (Exception e) {
            LOGGER.error("Exception in generateing LXRMCreditCard:", e);
        }
        return lxrMCreditCard;
    }
}
